<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;
class AppKernel extends Kernel {
	private $parameterfile;
	 private $cachedircustom;
	 private $logdircustom; 
	 
	public function registerBundles() {
		$bundles = array(
			//core
			new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
			new Symfony\Bundle\SecurityBundle\SecurityBundle(),
			new Symfony\Bundle\TwigBundle\TwigBundle(),
			new Symfony\Bundle\MonologBundle\MonologBundle(),
			new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
			new Symfony\Bundle\AsseticBundle\AsseticBundle(),
			new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
			new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
			
			
			
			//badiu system
						
			new Badiu\System\UserBundle\BadiuSystemUserBundle(),
			new Badiu\System\CoreBundle\BadiuSystemCoreBundle(),
			new Badiu\System\AccessBundle\BadiuSystemAccessBundle(),
			new Badiu\System\ModuleBundle\BadiuSystemModuleBundle(),
			new Badiu\System\SchedulerBundle\BadiuSystemSchedulerBundle(),
			new Badiu\System\FileBundle\BadiuSystemFileBundle(),
			new Badiu\System\ComponentBundle\BadiuSystemComponentBundle(),
			new Badiu\System\LogBundle\BadiuSystemLogBundle(),
			new Badiu\System\EntityBundle\BadiuSystemEntityBundle(),
			
			
			//badiu theme
			new Badiu\Theme\CoreBundle\BadiuThemeCoreBundle(),
		
			
			//badiu auth
			new Badiu\Auth\ManualBundle\BadiuAuthManualBundle(),
			new Badiu\Auth\CoreBundle\BadiuAuthCoreBundle(),
			new Badiu\Auth\SsogovbrBundle\BadiuAuthSsogovbrBundle(),
			
			#badiu util
			new Badiu\Util\AddressBundle\BadiuUtilAddressBundle(),
			new Badiu\Util\HousingBundle\BadiuUtilHousingBundle(),
			new Badiu\Util\DocumentBundle\BadiuUtilDocumentBundle(),
			new Badiu\Util\CoreBundle\BadiuUtilCoreBundle(),
			
			
			
			
			#badiu admin
			new Badiu\Admin\EnterpriseBundle\BadiuAdminEnterpriseBundle(),
			new Badiu\Admin\CoreBundle\BadiuAdminCoreBundle(),
			new Badiu\Admin\GradeBundle\BadiuAdminGradeBundle(),
			new Badiu\Admin\ServerBundle\BadiuAdminServerBundle(),
			new Badiu\Admin\ProjectBundle\BadiuAdminProjectBundle(),
			new Badiu\Admin\CmsBundle\BadiuAdminCmsBundle(),
			new Badiu\Admin\ClientBundle\BadiuAdminClientBundle(),
			new Badiu\Admin\AttendanceBundle\BadiuAdminAttendanceBundle(),
			new Badiu\Admin\FormBundle\BadiuAdminFormBundle(),
            new Badiu\Admin\SelectionBundle\BadiuAdminSelectionBundle(),
            new Badiu\Admin\CertificateBundle\BadiuAdminCertificateBundle(),
            new Badiu\Admin\MessageBundle\BadiuAdminMessageBundle(),			
			new Badiu\Admin\CouponBundle\BadiuAdminCouponBundle(),
			
			#Badiu Moodle
			new Badiu\Moodle\CoreBundle\BadiuMoodleCoreBundle(),
			
			
			
			#badiu ams
			new Badiu\Ams\DisciplineBundle\BadiuAmsDisciplineBundle(),
			new Badiu\Ams\CourseBundle\BadiuAmsCourseBundle(),
			new Badiu\Ams\CurriculumBundle\BadiuAmsCurriculumBundle(),
			new Badiu\Ams\OfferBundle\BadiuAmsOfferBundle(),
			new Badiu\Ams\EnrolBundle\BadiuAmsEnrolBundle(),
			new Badiu\Ams\CoreBundle\BadiuAmsCoreBundle(),
			new Badiu\Ams\RoleBundle\BadiuAmsRoleBundle(),
			new Badiu\Ams\GradeBundle\BadiuAmsGradeBundle(),
			new Badiu\Ams\MyBundle\BadiuAmsMyBundle(),
			
			#badiu tms
			new Badiu\Tms\CoreBundle\BadiuTmsCoreBundle(),
			
			#badiu sync
			new Badiu\Sync\DbBundle\BadiuSyncDbBundle(),
			new Badiu\Sync\IsdataBundle\BadiuSyncIsdataBundle(),
			new Badiu\Sync\FreportBundle\BadiuSyncFreportBundle(),
           
		); 
		$localbundles=new Badiu\Local\CoreBundle\BadiuLocalCoreBundle();
		$bundles=$localbundles->addLocalBundles($bundles);
		
		if (in_array($this->getEnvironment(), array('dev', 'test'))) {
			$bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
			$bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
			$bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
		}

		return $bundles;
	}

	public function registerContainerConfiguration(LoaderInterface $loader) {
		$loader->load(__DIR__. '/config/config_' . $this->getEnvironment() . '.yml');
		if(!empty($this->parameterfile)){$loader->load($this->parameterfile);}
		
	}


	public function getCacheDir()
    {
		$cachedir=null;
		
		if(!empty($this->cachedircustom)){ $cachedir=$this->cachedircustom.'/'.$this->environment;;}
		else {$cachedir=$this->rootDir.'/data/cache/'.$this->environment;}
		return $cachedir;
    }
	
	
	public function getLogDir()
    {
		$logdir=null;
		if(!empty($this->logdircustom)){ $logdir=$this->logdircustom.'/'.$this->environment;}
		else {$logdir=$this->rootDir.'/data/logs/'.$this->environment;}
		return $logdir;
 
    }
	
	 public function initCutomFile($parameterfile){
		//$this->parameterfile = __DIR__.'/../config/custom.yml';}
		if(empty($parameterfile)){return null;}
		$this->parameterfile=$parameterfile;
		if(file_exists($this->parameterfile)){
			$cparam= Yaml::parse($this->parameterfile);
			if(is_array($cparam)){
				if(isset($cparam['parameters']['cachedir'])){$this->cachedircustom=$cparam['parameters']['cachedir'];}
				if(isset($cparam['parameters']['logdir'])){$this->logdircustom=$cparam['parameters']['logdir'];}
			}
		}else {$this->parameterfile=null;}
	 }
}
