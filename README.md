Sistema: GC2
Descrição: Sistema de Gestão Coorporativo de Cursos Integrado a Plataforma Moodle
Liguagem de programação: PHP
Base de dados: MySQL e Postgres
Framework: Symfony

Guia do usuário
https://app.badiu.com.br/admin/cms/resource/content/fview/item/version/index/20?vkey=111:GAG5JaAtS6VxE4fzQQmcGGTuHTCj3jbzbt0IWCeQSbZiLz&vrelease=1

Guia do desenvolvedor
https://app.badiu.com.br/admin/cms/resource/content/fview/item/version/index/21?vkey=112:jtd55nd64W0g4dRCXzTIzljSMgZceKyYYPamZ875Jmxulv&vrelease=1

Comunidade de usuários
https://comunidade.badiu.com.br/mod/forum/view.php?id=16