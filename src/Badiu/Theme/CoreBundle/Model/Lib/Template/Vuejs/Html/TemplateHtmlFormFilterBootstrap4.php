<div id="badiu-report-search">
<div class="card">
    <div class="card-header">Filtro</div>
    <div class="card-body">
        <!-- inicio filtro -->
        <div class="row" id="badiu-report-search-firtrow">
            <!-- star first column of filter -->

            <div class="col text-right">
                <div id="badiu-report-search-firtrow-elements">
                    <?php echo $fdata->firstelement; ?>
                </div>
                <button @click="search()"  type="button" class="btn btn-primary">Pesquisar</button>

               <a data-toggle="collapse" href="#badiu-report-search-othersrows">Busca avancada</a>
            </div>
        </div><!-- end first column of filter -->
        <div class="row collapse" id="badiu-report-search-othersrows">
            <!-- star first column of filter -->
            <div class="col-12">
            <?php  foreach ($fdata->othersgroups as $fgroup) { ?>
                <fieldset class="form-group">
                    <legend>
                        <a data-toggle="collapse" href="#<?php echo $fgroup->name; ?>" aria-controls="badiu-collapse" aria-expanded="false"><span class="glyphicon glyphicon-triangle-right"></span> <?php echo $fgroup->label; ?></a>
                    </legend>
                    <div class="collapse <?php echo $fgroup->status; ?>" id="<?php echo $fgroup->name; ?>">
                        <?php  foreach ($fgroup->elements as $element) { ?>
                            <?php echo $element; ?>
                        <?php }?>
                        
                    </div>
                </fieldset>
               <?php }?> <!-- foreach ($fdata->othersgroups as $fgroup) {-->
                
            </div>
            <div class="col-12">
				<button  @click="search()" type="button" class="btn btn-primary">Pesquisar</button>
			</div>
        </div><!-- end first column of filter -->


    </div><!-- end card body -->
    <!-- fim do filtro -->
</div> 
</div> 
<hr />