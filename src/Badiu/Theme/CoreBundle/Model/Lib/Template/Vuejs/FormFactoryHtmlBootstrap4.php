<?php

namespace Badiu\Theme\CoreBundle\Model\Lib\Template\Vuejs;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FormFactoryHtmlBootstrap4 extends BadiuModelLib {

    private $field;
	private $dprefix="";
    function __construct(Container $container) {
        parent::__construct($container);
    }

    function get($field = null, $showlabel = true) {
        if (!empty($field)) {
            $this->field = $field;
        }


        if (empty($this->field)) {
            return null;
        }
      
        if ($field->getType() == 'text') {
            $html = $this->makeText($field, $showlabel);
            return $html;
        } 
		if ($field->getType() == 'numberint') {
            $html = $this->makeNumberint($field, $showlabel);
            return $html;
        }if ($field->getType() == 'numberdouble') {
            $html = $this->makeNumberdouble($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'email') {
            $html = $this->makeEmail($field, $showlabel);
            return $html; 
        } else if ($field->getType() == 'password') {
            $html = $this->makePassword($field, $showlabel);
            return $html;
        } if ($field->getType() == 'textarea') {
            $html = $this->makeTextarea($field, $showlabel);
            return $html;
        }if ($field->getType() == 'badiueditor') {
            $html = $this->makeBadiuEditor($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'choice') {
            $html = $this->makeChoice($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'radio') {
            $html = $this->makeRadio($field, $showlabel); //add it to other bootstrap version
            return $html;
        }else if ($field->getType() == 'checkbox') {
            $html = $this->makeCheckbox($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'entity') {
            $html = $this->makeChoice($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'hidden') {
            $html = $this->makeHidden($field);
            return $html;
        }else if ($field->getType() == 'date') {
            $html = $this->makeDate($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'datetime') {
            $html = $this->makeDatetime($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'datetime') {
            $html = $this->makeText($field, $showlabel);
            return $html;
        }
        else if ($field->getType() == 'badiudatedefault') {
            $html = $this->makeDatedefault($field, $showlabel);
            return $html;
        }
        else if ($field->getType() == 'badiudatetimedefault') {
            $html = $this->makeDatetimedefault($field, $showlabel);
            return $html;
        }
        else if ($field->getType() == 'badiufilterdate') {
            $html = $this->makeFilterdate($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'badiufilternumber') {
            $html = $this->makeFilternumber($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'badiutimeperiod') {
            $html = $this->makeTimePeriod($field, $showlabel);
            return $html;
        }/*else if ($field->getType() == 'badiufulltimeperiod') {
            $html = $this->makeFullTimePeriod($field, $showlabel);
            return $html;
        }*/
        else if ($field->getType() == 'badiuhourtime') {
            $html = $this->makeHourtime($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'badiuhour') {
            $html = $this->makeHour($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'badiuchoicetext') {
            $html = $this->makeChoicetext($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'badiuchoiceautocomplete') {
            $html =$this->makeChoiceautocomplete($field, $showlabel);
            return $html;
        }
       
       /* else if ($field->getType() == 'badiulmsmoodle') {
            $html = $this->makeLmsMoodle($field, false);
            return $html;
        } */
        else if ($field->getType() == 'badiuautocomplete') {
            $html = $this->makeAutocomplete($field, $showlabel,'forceselectone');
            return $html;
        } 
		else if ($field->getType() == 'badiuautocompleteforceselectone') {
            $html = $this->makeAutocomplete($field, $showlabel,'forceselectone');
            return $html;
        } 
		else if ($field->getType() == 'badiuautocompleteselectone') {
            $html = $this->makeAutocomplete($field, $showlabel,'selectone');
            return $html;
        }
		else if ($field->getType() == 'badiuautocompleteselectmultiple') {
            $html = $this->makeAutocomplete($field, $showlabel,'selectmultiple');
            return $html;
        } else if ($field->getType() == 'badiuautocompleteforceselectmultiple') {
            $html = $this->makeAutocomplete($field, $showlabel,'forceselectmultiple');
            return $html;
        }		
		else if ($field->getType() == 'badiufileimage') {
            $html = $this->makeFileimage($field, $showlabel);
            return $html;
        } else if ($field->getType() == 'badiufile') {
            $html = $this->makeFile($field, $showlabel);
            return $html;
        } 
    }

    function makeText($field, $showlabel = true) {
		
        $html = "";
        $html .= "<div id=\"item_" . $field->getName() . "\">";
       $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required </label>";
         } 
         $html .=$this->addRequiredMessage($field);
         $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\" name=\"" . $field->getName() . "\" " . $field->getAction() . ">";
         $html .= "</div>";


        return $html;
    } 
    
    function makeNumberint($field, $showlabel = true) {
		
        $html = "";
        $html .= "<div id=\"item_" . $field->getName() . "\">";
       $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required </label>";
         } 
         $html .=$this->addRequiredMessage($field);
         $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\" name=\"" . $field->getName() . "\" " . $field->getAction() . ">";
         $html .= "</div>";


        return $html;
    }
	function makeNumberdouble($field, $showlabel = true) {
		
        $html = "";
        $html .= "<div id=\"item_" . $field->getName() . "\">";
       $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required </label>";
         } 
         $html .=$this->addRequiredMessage($field);
         $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\" name=\"" . $field->getName() . "\" " . $field->getAction() . ">";
         $html .= "</div>";


        return $html;
    }
    function makeDate($field, $showlabel = true) {
        $html = "";
        //clean badiudate badiudatetime of css
        $css=$field->getCssClasss();
       $css= str_replace("badiudatetime","",$css);
       $css= str_replace("badiudate","",$css);
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
         if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
       
             $html .=$this->addRequiredMessage($field);
            $html .= "<vuejs-datepicker class=\"" .$css . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiutdate\" name=\"" . $field->getName() . "_badiutdate\" :format=\"defaultDatepickerFormat\" :clear-button=\"true\" ></vuejs-datepicker>";
       $html .= "</div>";

 
        return $html;
    }
    function makeDatetime($field, $showlabel = true) {
        $html = "";
        //clean badiudate badiudatetime of css
        $css=$field->getCssClasss();
       $css= str_replace("badiudatetime","",$css);
       $css= str_replace("badiudate","",$css);
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
         if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
       
        $html .=$this->addRequiredMessage($field);
        $html .= "<badiudatetime class=\"" .$css . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiutdate\" name=\"" . $field->getName() . "_badiutdate\" format=\"d/m/Y H:i\" lang=\"pt\"></badiudatetime>";
        $html .= "</div>";
        return $html;
    }

    function makeDatedefault($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
         if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
       
        $html .=$this->addRequiredMessage($field);
       $html .= "<input type=\"text\"  class=\"" .$field->getCssClasss(). "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiudatedefault\" name=\"" . $field->getName() . "_badiudatedefault\"  >";
       $html .= "</div>";

 
        return $html;
    }
    function makeDatetimedefault($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
         if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
       
        $html .=$this->addRequiredMessage($field);
       $html .= "<input type=\"text\"  class=\"" .$field->getCssClasss(). "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiudatetimedefault\" name=\"" . $field->getName() . "_badiudatetimedefault\"  >";
       $html .= "</div>";

 
        return $html;
    }
function makeEmail($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
    
         $html .=$this->addRequiredMessage($field);
        $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\" name=\"" . $field->getName() . "\" >";
        $html .= "</div>";
      return $html;
    }
    function makePassword($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
       
        $html .=$this->addRequiredMessage($field);
        $html .= "<input type=\"password\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\" name=\"" . $field->getName() . "\"  " . $field->getAction() . " >";
        $html .= "</div>";
        return $html;
    }
    function makeHidden($field) {
        $html = "";
         $html .= "<input type=\"hidden\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\"  name=\"" . $field->getName() . "\" >";
        return $html;
    }
    function makeTextarea($field, $showlabel = true) {
        $html = "";
        $hascssckeditor=false;
        $pos=stripos($field->getCssClasss(), "ckeditor");
        if($pos!== false){$hascssckeditor=true;}
        if($hascssckeditor){
            return $this->makeBadiuEditor($field, $showlabel);
        }
        $config=$field->getConfig();
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . " $required </label>";
        }
        $html .=$this->addRequiredMessage($field);
        if($config=='tinymce'){
            $html .=  "<vue-tinymce  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\" toolbar=\"fullscreen | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager media\"></vue-tinymce>";
        }else{
            $html .= "<textarea   class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\" name=\"" . $field->getName() . "\" ></textarea>";
            $html .= "</div>";
        }
       
        return $html;
    }
     function makeBadiuEditor($field, $showlabel = true) {
        $html = "";
        //remove ckeditor it is deprected
        //this method is colled in function makeTextarea($field, $showlabel = true)
        $css=$field->getCssClasss();
        $css=str_replace("ckeditor","",$css); 
        $config=$field->getConfig();
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . " $required </label>";
        }
        $html .=$this->addRequiredMessage($field);
        //_badiutexteditor
        $html .=  "<vue-tinymce  class=\"" . $css. "\" id=\"" . $field->getName() . "\"  v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\" toolbar=\"| fullscreen code | fontselect fontsizeselect formatselect | bold italic forecolor backcolor | link image | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |insertfile undo redo \"></vue-tinymce>";
        $html .=  "<input type=\"file\" ref=\"badiufileup\"  style=\"display: none;\" id=\"badiutextediorfileupload\">";
        $html .= "</div>";
        
       
        return $html;
    }
    
    function makeChoice($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
        
            $html .=$this->addRequiredMessage($field);
            $html .= "<select   class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "\" >";
            $html .= "<option value=\"\">---</option>";
            $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
            $html .= "</select>"; 
            $html .= "</div>";
          return $html;
    }
    function makeRadio($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label><br />";
        }

             $html .=$this->addRequiredMessage($field);
             $html .="<div class=\"row\">";
             $html .= "<div class=\"col\">";
             foreach ($field->getChoicelistvalues() as $key => $value) {
                  $id=$field->getName()."_".$key;
                 
                     $html .= "<em><input type=\"radio\" id=\"$id\"  value=\"$key\"  v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\"> <label>$value</label> </em><br />";
                
             }
             $html .= "</div>";
             $html .= "</div>";
             $html .= "</div>";
        
        return $html;
    }
     function makeCheckbox($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label><br />";
        }

             $html .=$this->addRequiredMessage($field);
             $html .="<div class=\"row\">";
             $html .= "<div class=\"col\">";
             foreach ($field->getChoicelistvalues() as $key => $value) {
                  $id=$field->getName()."_".$key;
                 
                     $html .= "<input type=\"checkbox\" id=\"$id\" value=\"$key\"  v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\"> <label for=\"jack\">$value</label> &nbsp;&nbsp;";
                
             }
             $html .= "</div>";
             $html .= "</div>";
             $html .= "</div>";
        
        return $html;
    }
    function makeFilterdate($field, $showlabel = true) {
        $html = "";
        
        $value=$field->getValue();
        $date1=$this->getUtildata()->getVaueOfArray($value,'date1');
        $date2=$this->getUtildata()->getVaueOfArray($value,'date2');
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "</label>";
        }
       
             $html .= "<div class=\"row\" id=\"" . $field->getName() . "\">";
               $html .=$this->addRequiredMessage($field);
                $html .= "<div class=\"col-2\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiudateoperator\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiudateoperator\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() ."_badiudateoperator\" >";
                    $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
             
              $html .= "<div class=\"col-3\">";
                   $html .= "<vuejs-datepicker  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiudate1\"  placeholder=\"".$field->getPlaceholder()."\"  v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiudate1\" value=\"$date1\" name=\"" . $field->getName() . "_badiudate1\" :format=\"defaultDatepickerFormat\" :clear-button=\"true\" ></vuejs-datepicker>";
                $html .= "</div>";
                
                $html .= "<div class=\"col-1\"  v-show=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiudateoperator!='on'\">";
                $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuhour1\"   v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiuhour1\"  options=\"badiudefaultformoptions.hour\"  name=\"" . $field->getName() ."_badiuhour1\" >";
                $html .= "<option v-for=\"item in badiudefaultformoptions.hour\" :value=\"item.value\">{{item.text }}</option>";
                $html .= "</select>";
                $html .= "</div>";

                $html .= "<div class=\"col-1\"  v-show=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiudateoperator!='on'\">";
                $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuminute1\"   v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiuminute1\"  options=\"badiudefaultformoptions.minute\"  name=\"" . $field->getName() ."_badiuminute1\" >";
                $html .= "<option v-for=\"item in badiudefaultformoptions.minute\" :value=\"item.value\">{{item.text }}</option>";
                $html .= "</select>";
                $html .= "</div>";

                // v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiudate2\"
                $html .= "<div class=\"col-3\"  v-show=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiudateoperator=='between'\">";
                   // $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiudate2\"  placeholder=\"".$field->getPlaceholder()."\"  value=\"$date2\"  name=\"" . $field->getName() . "_badiudate2\">";
                   $html .= "<vuejs-datepicker  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiudate2\"  placeholder=\"".$field->getPlaceholder()."\"   v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiudate2\"  name=\"" . $field->getName() . "_badiudate2\"  :format=\"defaultDatepickerFormat\" :clear-button=\"true\" ></vuejs-datepicker>";
                $html .= "</div>";
    

                $html .= "<div class=\"col-1\"  v-show=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiudateoperator=='between'\">";
                $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuhour2\"   v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiuhour2\"  options=\"badiudefaultformoptions.hour\"  name=\"" . $field->getName() ."_badiuhour2\" >";
                $html .= "<option v-for=\"item in badiudefaultformoptions.hour\" :value=\"item.value\">{{item.text }}</option>";
                $html .= "</select>";
                $html .= "</div>";

                $html .= "<div class=\"col-1\"  v-show=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiudateoperator=='between'\">";
                $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuminute2\"   v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiuminute2\"  options=\"badiudefaultformoptions.minute\"  name=\"" . $field->getName() ."_badiuminute2\" >";
                $html .= "<option v-for=\"item in badiudefaultformoptions.minute\" :value=\"item.value\">{{item.text }}</option>";
                $html .= "</select>";
                $html .= "</div>";
        
             $html .= "</div>";

        return $html;
    }
 
function makeFilternumber($field, $showlabel = true) {
        $html = "";
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "</label>";
        }
       
             $html .= "<div class=\"row\" id=\"" . $field->getName() . "\">";
               $html .=$this->addRequiredMessage($field);
                $html .= "<div class=\"col-4\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiunumberoperator\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiunumberoperator\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiunumberoperator\" >";
                    $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
               
                $html .= "<div class=\"col-4\">";
                    $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiunumber1\"  placeholder=\"".$field->getPlaceholder()."\"   v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiunumber1\" name=\"" . $field->getName() . "_badiunumber1\">";
                $html .= "</div>";
                
                // v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiunumber2\"
                $html .= "<div class=\"col-4\"  v-show=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiunumberoperator=='between'\">";
                    $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiunumber2\"  placeholder=\"".$field->getPlaceholder()."\"  v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiunumber2\"  name=\"" . $field->getName() . "_badiunumber2\">";
                $html .= "</div>";
             $html .= "</div>";
       
             $html .= "</div>";

        return $html;
    }
    function makeTimePeriod($field, $showlabel = true) {
       // print_r($field);exit;
        $html = "";//badiutimeperiod
        $config=$field->getConfig();
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "</label>";
        }
        if(empty($config)){
             $html .= "<div class=\"row\"  id=\"" . $field->getName() . "\">";
        
               
             $html .=$this->addRequiredMessage($field);
                $html .= "<div class=\"col-8\">";
                $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiutimeperiodnumber\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiutimeperiodnumber\"  name=\"" . $field->getName() ."_badiutimeperiodnumber\"  >";
                $html .= "</div>";
                
               
                $html .= "<div class=\"col-4\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiutimeperiodunittime\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiutimeperiodunittime\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiutimeperiodunittime\" >";
                    $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
               $html .= "</div>";  
        } else{
            $html .= "<div class=\"row\"  id=\"" . $field->getName() . "\">";
        
            $html .=$this->addRequiredMessage($field);
               $html .= "<div class=\"col-4\">";
               $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiutimeperiodoperator\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiutimeperiodoperator\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiutimeperiodoperator\" >";
                   $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "_operator\" :value=\"item.value\">{{item.text }}</option>";
                   $html .= "</select>";
               $html .= "</div>";
               
              $html .= "<div class=\"col-4\">";
               $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiutimeperiodnumber\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiutimeperiodnumber\"  name=\"" . $field->getName() ."_badiutimeperiodnumber\"  >";
               $html .= "</div>";
               $html .= "<div class=\"col-4\">";
                   $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiutimeperiodunittime\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiutimeperiodunittime\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiutimeperiodunittime\" >";
                   $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                   $html .= "</select>";
               $html .= "</div>";
              $html .= "</div>";
        }
        $html .= "</div>";   
        


        return $html;
    }

   /* function makeFullTimePeriod($field, $showlabel = true) {
        $html = "";//badiutimeperiod
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "</label>";
        }
      
             $html .= "<div class=\"row\"  id=\"" . $field->getName() . "\">";
        
               
             $html .=$this->addRequiredMessage($field);
                $html .= "<div class=\"col-4\">";
                $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiufulltimeperiodoperator\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiufulltimeperiodoperator\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() ."_badiufulltimeperiodoperator\" >";
                $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                 $html .= "</select>";
                $html .= "</div>";

                $html .= "<div class=\"col-4\">";
                $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiutimeperiocdnumber\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiufulltimeperiodnumber\"  name=\"" . $field->getName() ."_badiufulltimeperiodnumber\"  >";
                $html .= "</div>";
                
               
                $html .= "<div class=\"col-4\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiufulltimeperiodunittime\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiufulltimeperiodunittime\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiufulltimeperiodunittime\" >";
                    $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
               $html .= "</div>";  
             
              
        


        return $html;
    }*/
     function makeHour($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . " $required</label>";
        }
     
             $html .= "<div class=\"row\"  id=\"" . $field->getName() . "\">";
               $html .=$this->addRequiredMessage($field);
              $html .= "<div class=\"col-2\">";
                    $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiutimehour\"  placeholder=\"Hora\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiutimehour\"  name=\"" . $field->getName() ."_badiutimehour\"  >";
                $html .= "</div>";
              
                $html .= "<div class=\"col-10\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiutimeminute\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiutimeminute\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiutimeminute\" >";
                    $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
                $html .= "</div>";
                $html .= "</div>";
       return $html;
    }
     function makeHourtime($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . " $required</label>";
        }
      
             $html .= "<div class=\"row\"  id=\"" . $field->getName() . "\">";
               $html .=$this->addRequiredMessage($field);
              $html .= "<div class=\"col-2\">";
                     $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuhourtimehour\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiuhourtimehour\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "_hour\"  name=\"" . $field->getName() . "_badiuhourtimehour\" >";
                    $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "_hour\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
              
                $html .= "<div class=\"col-10\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuhourtimeminute\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiuhourtimeminute\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "_minute\"  name=\"" . $field->getName() . "_badiuhourtimeminute\" >";
                    $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "_minute\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
            $html .= "</div>";   
            $html .= "</div>";
        return $html;
    }
      function makeChoicetext($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
        
             $html .= "<div class=\"row\"  id=\"" . $field->getName() . "\">";
               $html .=$this->addRequiredMessage($field);
                $html .= "<div class=\"col-4\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiutchoicetextfield1\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiuchoicetextfield1\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiuchoicetextfield1\" >";
                    $html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
                
                $html .= "<div class=\"col-8\">";
                    $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiuchoicetextfield2\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "_badiuchoicetextfield2\"  name=\"" . $field->getName() ."_badiuchoicetextfield2\"  >";
                $html .= "</div>";
             $html .= "</div>"; 
             $html .= "</div>";
       return $html;
    }
	
	 function makeChoiceautocomplete($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
        
             $html .= "<div class=\"row\"  id=\"" . $field->getName() . "\">";
               $html .=$this->addRequiredMessage($field);
                $html .= "<div class=\"col-4\">";
                    $html .= "<select   class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "v1\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "v1\"  options=\"".$this->getDprefix()."badiuformoptions." . $field->getName() . "v1\"  name=\"" . $field->getName() . "v1\" >";
					$html .= "<option value=\"\">---</option>";
					$html .= "<option v-for=\"item in ".$this->getDprefix()." badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
					$html .= "</select>";
                $html .= "</div>";
                
                $html .= "<div class=\"col-8\">";
				 
				 //for forceselect one
				   $badiuSession = $this->getContainer()->get('badiu.system.access.session');
					$autocompleteselectonehiddeidinlist=$badiuSession->getValue('badiu.theme.core.form.param.config.autocompleteselectonehiddeidinlist');
    
	
					$html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\" @input=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_istyping = true\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_query\" v-if=\"!".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_valueselected\"  @input=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_istyping = true\" name=\"" . $field->getName() . "\" >";
					$html .= " <div v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_valueselected\" class=\"alert alert-info\"> ";
					$html .= "<span v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_valueselected\" v-html=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_valueselected\" ></span>";
					$html .= " &nbsp;&nbsp; <a   v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_valueselected\" @click.prevent=\"" . $field->getName() ."_removeitemselected()\" >x<span class=\"glyphicon glyphicon-remove\"></span></a>";
					$html .= "  </div>";
					$html .= " <div align=\"center\" v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_isloading\">";
					$html .= "   <span>Procurando...</span>";
					$html .= "  </div>";

					$html .= " <div class=\"list-group\" v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_isshowlist\">";                                    
					if($autocompleteselectonehiddeidinlist){$html .= "  <a v-for=\"sitem in ".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_searchresult\"  @click.prevent=\"" . $field->getName() ."_itemselected(sitem)\"   href=\"#\" class=\"list-group-item\"> {{sitem.name}}</a>  ";}
					else {$html .= "  <a v-for=\"sitem in ".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_searchresult\"  @click.prevent=\"" . $field->getName() ."_itemselected(sitem)\"   href=\"#\" class=\"list-group-item\">{{sitem.id}} - {{sitem.name}}</a>  ";}
					$html .= " </div>";
			 
                  
             $html .= "</div>"; 
             $html .= "</div>";
       return $html;
    }
   /*
    function makeLmsMoodle($field, $showlabel = false) {
         $html = "";
         if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "</label>";
        }
          $html .= "<div> <a @click.prevent=\"showModalToSelectMoodleItem()\"> moodle </a> </div>";
             $html .= "<input type=\"hidden\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\" v-model=\"".$this->getDprefix()."badiuform." . $field->getName() . "\"  name=\"" . $field->getName() . "\" >";
        
        return $html ;
     }*/
     
	 /**
	 $type 	
		selectone - select one option
		forceselectone - select one options only in closed list
		selectmultiple - select multiple  options
		forceselectmultiple - select multiple options only in closed list
	 */
      function makeAutocomplete($field, $showlabel = false,$type='forceselectone') {
		    $badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$autocompleteselectonehiddeidinlist=$badiuSession->getValue('badiu.theme.core.form.param.config.autocompleteselectonehiddeidinlist');
         $html = "";
         $required=$this->addRequiredInfo($field);
         $html .= "<div id=\"item_" . $field->getName() . "\">";
         if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
           $html .=$this->addRequiredMessage($field); 
			if($type=='forceselectone'){
				$html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\" @input=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_istyping = true\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_query\" v-if=\"!".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_valueselected\"  @input=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_istyping = true\" name=\"" . $field->getName() . "\" >";
				$html .= " <div v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_valueselected\" class=\"alert alert-info\"> ";
				$html .= "<span v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_valueselected\" v-html=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_valueselected\" ></span>";
				$html .= " &nbsp;&nbsp; <a   v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_valueselected\" @click.prevent=\"" . $field->getName() ."_removeitemselected()\" >x<span class=\"glyphicon glyphicon-remove\"></span></a>";
				$html .= "  </div>";
				$html .= " <div align=\"center\" v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_isloading\">";
				$html .= "   <span>Procurando...</span>";
				$html .= "  </div>";

				$html .= " <div class=\"list-group\" v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_isshowlist\">";                                    
				if($autocompleteselectonehiddeidinlist){$html .= "  <a v-for=\"sitem in ".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_searchresult\"  @click.prevent=\"" . $field->getName() ."_itemselected(sitem)\"   href=\"#\" class=\"list-group-item\"> {{sitem.name}}</a>  ";}
				else {$html .= "  <a v-for=\"sitem in ".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_searchresult\"  @click.prevent=\"" . $field->getName() ."_itemselected(sitem)\"   href=\"#\" class=\"list-group-item\">{{sitem.id}} - {{sitem.name}}</a>  ";}
				$html .= " </div>";
			 
			 }else if($type=='selectmultiple'){
				
				$html .= "<div v-if=\"".$this->getDprefix()."badiuform." . $field->getName() . "\"><span class=\"badge badge-primary mr-1\"  v-for=\"(item, index) in ".$this->getDprefix()."badiuform." . $field->getName() . "\" >{{item}} <a  @click=\"". $field->getName() . "_itemremoved(index)\"><i class=\"far fa-times-circle\"></i></a></span></div> ";
				$html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\" @input=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_istyping = true\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_query\"   @input=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_istyping = true\" name=\"" . $field->getName() . "\" @keyup.enter=\"". $field->getName() . "_itemtyped()\" >";
				
				$html .= " <div align=\"center\" v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_isloading\">";
				$html .= "   <span>Procurando...</span>";
				$html .= "  </div>";

				$html .= " <div class=\"list-group\" v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_isshowlist\">";                                    
				$html .= "  <a v-for=\"sitem in ".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_searchresult\"  @click.prevent=\"" . $field->getName() ."_itemselected(sitem)\"   href=\"#\" class=\"list-group-item\"> {{sitem.name}}</a>  ";                                  
				$html .= " </div>";
			 }else if($type=='selectone'){
				
				$html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\" @input=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_istyping = true\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_query\"   @input=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_istyping = true\" name=\"" . $field->getName() . "\" " . $field->getAction() . " >";
				
				$html .= " <div align=\"center\" v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_isloading\">";
				$html .= "   <span>Procurando...</span>";
				$html .= "  </div>";

				$html .= " <div class=\"list-group\" v-if=\"".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_isshowlist\">";                                    
				$html .= "  <a v-for=\"sitem in ".$this->getDprefix()."badiuautocomplete." . $field->getName() . "_searchresult\"  @click.prevent=\"" . $field->getName() ."_itemselected(sitem)\"   href=\"#\" class=\"list-group-item\"> {{sitem.name}}</a>  ";                                  
				$html .= " </div>";
			 }
			 $html .= "</div>";
			 
			 
         return $html ;
     }
     
	 
     
	 function makeFileimage($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
        $html .= "<div v-if=\"".$this->getDprefix()."badiuform." . $field->getName() ."_badiufileimagefullurl\"> <img :src=\"".$this->getDprefix()."badiuform." . $field->getName() ."_badiufileimagefullurl\" class=\"img-responsive\"  style=\"max-height: 400px;\" /> <a @click.prevent=\"removeFileUpload('" . $field->getName() ."','" . $field->getName() ."_badiufileimagefullurl','" . $field->getName() ."_badiufileimagename')\"><i class=\"far fa-times-circle\"></i></a></div><br>";
         $html .=$this->addRequiredMessage($field); 
        $html .= "<input  v-if=\"!badiuform." . $field->getName() ."_badiufileimagefullurl\" type=\"file\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" @change=\"" . $field->getName() ."_badiufileimageuploadonchange\" name=\"" . $field->getName() . "\"  >";
         $html .= "<div v-if=\"!badiuform." . $field->getName() ."_badiufileimagefullurl && badiuform." . $field->getName() ."_badiufileuploadprogress\">{{badiuform." . $field->getName() ."_badiufileuploadprogress}} %<br/><progress  v-bind:value=\"badiuform." . $field->getName() ."_badiufileuploadprogress\" max=\"100\"></progress></div>";
		 $html .= "</div>";
      return $html;
    }
	
	function makeFile($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        $html .= "<div id=\"item_" . $field->getName() . "\">";
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
        $html .= "<div v-if=\"".$this->getDprefix()."badiuform." . $field->getName() ."_badiufilefullurl\"> <a v-bind:href=\"".$this->getDprefix()."badiuform." . $field->getName() ."_badiufilefullurl\"  target=\"_blank\" /> {{".$this->getDprefix()."badiuform." . $field->getName() ."_badiufilename}} </a> <a @click.prevent=\"removeFileUpload('" . $field->getName() ."','" . $field->getName() ."_badiufilefullurl','" . $field->getName() ."_badiufilename')\"><i class=\"far fa-times-circle\"></i></a></div><br>";
         $html .=$this->addRequiredMessage($field); 
        $html .= "<input  v-if=\"!badiuform." . $field->getName() ."_badiufilefullurl\" type=\"file\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" @change=\"" . $field->getName() ."_badiufileuploadonchange\" name=\"" . $field->getName() . "\"  >";
		 $html .= "<div v-if=\"!badiuform." . $field->getName() ."_badiufilefullurl && badiuform." . $field->getName() ."_badiufileuploadprogress\">{{badiuform." . $field->getName() ."_badiufileuploadprogress}} %<br/><progress  v-bind:value=\"badiuform." . $field->getName() ."_badiufileuploadprogress\" max=\"100\"></progress></div>";
		 $html .= "</div>";
      return $html;
    }
      function addRequiredMessage($field) {
          $html="";
             $html .= "<div v-show=\"".$this->getDprefix()."badiuaerrorcontrol." . $field->getName() . "\" class=\"alert alert-danger\">";
             $html .= " <div v-html=\"".$this->getDprefix()."badiuaerrormessage." . $field->getName() . "\"></div>";
             $html .= "</div>";
             return $html;
           
      }
      
 function addRequiredInfo($field) {
          $html="";
          $required=$field->getRequired();
         // if($required){
               $html="<i  v-show=\"".$this->getDprefix()."badiurequiredcontrol." . $field->getName() . "\" class=\"fa fa-asterisk text-danger\"></i>";
         // }
           return $html;
           
      }  

  
    function getField() {
        return $this->field;
    }

    function setField($field) {
        $this->field = $field;
    }

   function getDprefix() {
        return $this->dprefix;
    }

    function setDprefix($dprefix) {
        $this->dprefix = $dprefix;
    }


}
