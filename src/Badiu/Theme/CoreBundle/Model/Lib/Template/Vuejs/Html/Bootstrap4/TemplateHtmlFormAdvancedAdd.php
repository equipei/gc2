<div>
<?php
/*echo "<pre>";
print_r($fdata->othersgroups);
echo "</pre>";exit;*/
?>
<div class="row">

	<div v-show="formcontrol.haserror && formcontrol.typemessage=='error'" class="col alert alert-danger">
		<div v-html="formcontrol.message"></div>
	</div>
	<div v-show="formcontrol.haserror && formcontrol.typemessage=='info'" class="col alert alert-info">
		<div v-html="formcontrol.message"></div>
	</div>
 </div> 
 
 <div class="row">
	<div v-show="formcontrol.status=='success'" class="col alert  alert-success">
		<div v-html="formcontrol.message"></div>
	</div>
 </div> 
 
<div class="row">
<div class="col" v-show="formcontrol.status=='open'">
	
	<?php  foreach ($fdata->othersgroups as $fgroup) { ?>
        <fieldset class="form-group" id="<?php echo $fgroup->name; ?>-fieldset">
            <legend>
                <a data-toggle="collapse" href="#<?php echo $fgroup->name; ?>" aria-controls="badiu-collapse" aria-expanded="false"><span class="glyphicon glyphicon-triangle-right"></span> <?php echo $fgroup->label; ?></a>
            </legend>
            <div class="collapse <?php echo $fgroup->status; ?>" id="<?php echo $fgroup->name; ?>">
				<?php if(!empty($fdata->bootstrapcolumns)) {?><div class="row"><?php }?>
				  
					<?php  foreach ($fgroup->elements as $fkey => $element) { ?>
						<?php if(!empty($fdata->bootstrapcolumns)) {echo "<div class=\"col-sm-6 col-md-$fdata->bootstrapcolumns\" id=\"fgitem_$fkey\">";}?>
						<?php echo $element; ?>
						<?php if(!empty($fdata->bootstrapcolumns)) {echo "</div>";}?>
					<?php }?>
				  
				<?php if(!empty($fdata->bootstrapcolumns)) {?></div><?php }?>
            </div>
        </fieldset>
    <?php }?> <!-- foreach ($fdata->othersgroups as $fgroup) {-->

	</div>
	</div> <!-- end div row-->
	<?php if($fdata->config->showsubmitrow){?>
	<div class="row" v-show="formcontrol.status=='open'">
		<div class="col">
				<button  @click="send()" type="button" class="btn btn-primary"><?php echo $fdata->proccessbuttonlabel;?></button><?php echo $fdata->iconprocessing; ?>
		</div>
	</div>
	<?php }?>

</div>