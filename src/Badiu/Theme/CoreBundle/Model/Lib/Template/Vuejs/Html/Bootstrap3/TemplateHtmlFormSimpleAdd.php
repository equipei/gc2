<div>

<div class="row">
	<div v-show="formcontrol.haserror" class="col alert alert-danger">
		<div v-html="formcontrol.message"></div>
	</div>
 </div> 
 
 <div class="row">
	<div v-show="formcontrol.status=='success'" class="col alert  alert-success">
		<div v-html="formcontrol.message"></div>
	</div>
 </div> 
 
<div class="row">
<div class="col" v-show="formcontrol.status=='open'">

	<?php  foreach ($fdata->elements as $element) { ?>
             <?php echo $element; ?>
    <?php }?>
           
    
	</div>
	</div> <!-- end div row-->
	<?php if($fdata->config->showsubmitrow){?>
	<div class="row" v-show="formcontrol.status=='open'">
		<div class="col">
				<button  @click="send()" type="button" class="btn btn-primary"><?php echo $fdata->proccessbuttonlabel;?></button><?php echo $fdata->iconprocessing; ?>
		</div>
	</div>
	<?php }?>

</div>