<?php

namespace Badiu\Theme\CoreBundle\Model\Lib\Template\Vuejs;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FormFactoryComponent extends BadiuModelLib {

  

    function __construct(Container $container) {
        parent::__construct($container);
    }

    function makeChoiceList($jsvarname, $service, $function = 'getFormChoice', $converlist = true) {
        $list = null;
        if (!$this->getContainer()->has($service)) {
            return "";
        }
       
        $data = $this->getContainer()->get($service);
        $data->setSessionhashkey($this->getSessionhashkey());
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        if ($function == 'getFormChoice' || $function == 'getFormChoiceShortname') {
            $list = $data->$function($badiuSession->get()->getEntity());
        } else {
            $list = $data->$function();
        }
       
        if ($converlist) {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->convertBdArrayToListOptions($list);
        }
        
        if(empty($list) || !is_array($list)){ return "";}
        $houtjs = "";
      
            $houtjs .= $jsvarname . ": [";
            $contcl = 0;
            $sepatator = ",";
            $houtjs .= "{ text: '---', value: '', valuekey: '' }";
			
			foreach ($list as $clkey => $clvalue) {
                    $contcl++;  
					$vkey="";
					$text=$clvalue;
					if(is_array($clvalue)){
						$text= $this->getUtildata()->getVaueOfArray($clvalue, 'name');
						$vkey= $this->getUtildata()->getVaueOfArray($clvalue, 'shortname');
						$vk="'".$vkey."'";
						$vkey=", valuekey: $vk";
					}
                    $text="'".$text."'";
                    $v="'".$clkey."'";
                    if ($contcl == 0) {$sepatator = ",";}
                    else {$sepatator = ",";}
                    $houtjs.= "$sepatator{ text: $text, value: $v $vkey }";
                    
             }
			 
              

            $houtjs .= "],";
           
      return  $houtjs;
    }

}
