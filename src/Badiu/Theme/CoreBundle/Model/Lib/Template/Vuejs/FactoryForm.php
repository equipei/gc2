<?php
namespace Badiu\Theme\CoreBundle\Model\Lib\Template\Vuejs;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryForm extends BadiuModelLib {

    private $config;
    private $page;
    private $isadvancedform=true;
	public function __construct(Container $container) {
        parent::__construct($container);
        $this->initconfig(); 
    }
  
    function exec($type='filter'){
        $this->checkIsadvancedform();
        $html=null;
       
        if($this->isadvancedform){ 
            if($type=='filter'){$html=$this->advancedFilterForm();} 
            else if($type=='add'){$html=$this->advancedAddForm();}
        }
       
        else {
            if($type=='filter'){$html=$this->simpleFilterForm(); }
            else if($type=='add'){$html=$this->simpleAddForm();}
        }
        return $html;
    }
    function initconfig(){
        $this->config=new \stdClass(); 
        $this->config->showsubmitrow=true;
		$this->config->showfirstitem=true;
		$this->config->onlyform=false;
		$this->config->formnumber="";
    }
    function execScheduler($isclone=false){
        $this->checkIsadvancedform();
       $html=null;
        if($this->isadvancedform){ 
            $html=$this->advancedSchedulerEditForm($isclone);
        }
        else {
          $html=$this->simpleSchedulerEditForm($isclone); 
        }
        return $html;
    }
    function simpleFilterForm(){

    }
    function simpleAddForm(){
        $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
        $utilapp->setSessionhashkey($this->getSessionhashkey());
        $templateurl=null;
        if($this->page->getViewsytemtype()=='bootstrap4'){ $templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap4/TemplateHtmlFormSimpleAdd.php";}
        else if($this->page->getViewsytemtype()=='bootstrap3'){$templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap3/TemplateHtmlFormSimpleAdd.php";}
        else if($this->page->getViewsytemtype()=='bootstrap2'){$templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap2/TemplateHtmlFormSimpleAdd.php";}
        $fdata=$this->makeSimpleFormData();
        $fdata->iconprocessing=$this->getContainer()->get('badiu.theme.core.lib.template.vuejs.factoryutil')->iconProcess();
        $fdata->proccessbuttonlabel=$this->formButtonLabel();
        $html="";
		$container=$this->getContainer();
		$translator=$container->get('translator');
        ob_start();
		$templateurl=$utilapp->getFilePath($templateurl,"Model");
        include($templateurl);
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    function advancedFilterForm(){
        $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
        $utilapp->setSessionhashkey($this->getSessionhashkey());
        $templateurl=null;
        if($this->page->getViewsytemtype()=='bootstrap4'){ $templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap4/TemplateHtmlFormAdvancedFilter.php";}
        else if($this->page->getViewsytemtype()=='bootstrap3'){$templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap3/TemplateHtmlFormAdvancedFilter.php";}
        else if($this->page->getViewsytemtype()=='bootstrap2'){$templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap2/TemplateHtmlFormAdvancedFilter.php";}
        $fdata=$this->makeAdvancedFilterFormData();
        $html="";
		$container=$this->getContainer();
		$translator=$container->get('translator');
        ob_start();

        $templateurl=$utilapp->getFilePath($templateurl,"Model");
        
        include($templateurl);
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    function advancedAddForm(){
        $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
        $utilapp->setSessionhashkey($this->getSessionhashkey());
        $templateurl=null;
        if($this->page->getViewsytemtype()=='bootstrap4'){ $templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap4/TemplateHtmlFormAdvancedAdd.php";}
       else if($this->page->getViewsytemtype()=='bootstrap3'){$templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap3/TemplateHtmlFormAdvancedAdd.php";}
       else if($this->page->getViewsytemtype()=='bootstrap2'){$templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap2/TemplateHtmlFormAdvancedAdd.php";}
      

        $fdata=$this->makeAdvancedAddFormData();
        $fdata->iconprocessing=$this->getContainer()->get('badiu.theme.core.lib.template.vuejs.factoryutil')->iconProcess();
        $fdata->proccessbuttonlabel=$this->formButtonLabel();
        $html="";
        ob_start();
        $templateurl=$utilapp->getFilePath($templateurl,"Model");
        
        include($templateurl);
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    function simpleSchedulerEditForm($isclone=false){

    }
    function advancedSchedulerEditForm($isclone=false){
        $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
        $utilapp->setSessionhashkey($this->getSessionhashkey());
        $templateurl=null;
        if($this->page->getViewsytemtype()=='bootstrap4'){ $templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap4/TemplateHtmlFormSchedulerEdit.php";}
       else if($this->page->getViewsytemtype()=='bootstrap3'){$templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap3/TemplateHtmlFormSchedulerEdit.php";}
       else if($this->page->getViewsytemtype()=='bootstrap2'){$templateurl="BadiuThemeCoreBundle:Model/Lib/Template/Vuejs/Html/Bootstrap2/TemplateHtmlFormSchedulerEdit.php";}
      

        $fdata=$this->makeAdvancedSchedulerEditData($isclone);
        $fdata->iconprocessing=$this->getContainer()->get('badiu.theme.core.lib.template.vuejs.factoryutil')->iconProcess("scheduleformcontrol.processing");
        $fdata->proccessbuttonlabelsavefinish=$this->getTranslator()->trans('badiu.system.scheduler.report.savefinish');
        $fdata->proccessbuttonlabelgoback=$this->getTranslator()->trans('badiu.system.scheduler.report.back');
        $fdata->proccessbuttonlabelcancel=$this->getTranslator()->trans('badiu.system.scheduler.report.cancel');
       
        $html="";
        ob_start();
        $templateurl=$utilapp->getFilePath($templateurl,"Model");
        
        include($templateurl);
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    public function checkIsadvancedform(){
		$fn= $this->getUtildata()->getVaueOfArray($this->config, "formnumber");
        $formconfig = $this->getUtildata()->getVaueOfArray($this->page->getData(), "badiu_formconfig$fn");
        $formgroup = $this->getUtildata()->getVaueOfArray($formconfig, 'formgroup');
        if(empty($formgroup) || !is_array($formgroup)){$this->isadvancedform=false;}

    }

    public function makeAdvancedFilterFormData(){
        $fdata=new \stdClass();
        $fdata->firstgroup='';
        $fdata->firstelement='';
        $fdata->othersgroups=array();
        $fdata->config=$this->getConfig();
        $formfieldto = $this->getContainer()->get('badiu.system.core.lib.form.field');
        $formfactoryhtmllib=null; 
        if($this->page->getViewsytemtype()=='bootstrap4'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap4');}
        else if($this->page->getViewsytemtype()=='bootstrap3'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap3');}
        else if($this->page->getViewsytemtype()=='bootstrap2'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap2');}
        $fn= $this->getUtildata()->getVaueOfArray($this->config, "formnumber");
       $formconfig =$this->getUtildata()->getVaueOfArray($this->page->getData(), "badiu_formconfig$fn");
		$splitfieldscolumn=$this->getUtildata()->getVaueOfArray($formconfig, 'formconfig.splitfieldscolumn',true);
		$bootstrapcolumns="";
		if(!empty($splitfieldscolumn)){$bootstrapcolumns=12/$splitfieldscolumn;}
		$fdata->splitfieldscolumn=$splitfieldscolumn;
		$fdata->bootstrapcolumns=$bootstrapcolumns;
		
		
        $formconfig =$this->getUtildata()->getVaueOfArray($this->page->getData(), "badiu_formconfig$fn");

        $formgroup = $this->getUtildata()->getVaueOfArray($formconfig, 'formgroup');
        $formfields = $this->getUtildata()->getVaueOfArray($formconfig, 'formfields');

        $cont=0;
        foreach ($formgroup as $fgroup) {
            $groupnameoriginal = $this->getUtildata()->getVaueOfArray($fgroup, 'name');
            $groupname = $this->getUtildata()->getVaueOfArray($fgroup, 'name');
            $grouplabel = $this->getUtildata()->getVaueOfArray($fgroup, 'label');
            $groupname =str_replace(".","-",$groupname);
            if($groupname!='_badiuschedulerformclone'){
            if($cont==0 && $this->config->showfirstitem){
                $fdata->firstgroup=$groupname;
                $confirstfield=0;
                foreach ($formfields as $field) {
                    if($confirstfield==0){
                        $formfieldto->init($field);
                        $fdata->firstelement=$formfactoryhtmllib->get($formfieldto,false);
                        
                        break;
                    }
                    $confirstfield++;
                }
            } //end  if($cont==0){
            else  if($cont > 0 && $this->config->showfirstitem){
                $listgroup=new \stdClass();
                $listgroup->name=$groupname;
                $listgroup->label=$grouplabel;
                $listgroup->elements=array();
                $listgroup->status="";
                if($cont==1){
                    if($this->page->getViewsytemtype()=='bootstrap2'){$listgroup->status=" in";}
                    else if($this->page->getViewsytemtype()=='bootstrap4'){$listgroup->status=" show ";} 
                }
                foreach ($formfields as $field) {
                    $fgroup =  $this->getUtildata()->getVaueOfArray($field, 'group');
                    if($fgroup == $groupnameoriginal){
						$ffieldname = $this->getUtildata()->getVaueOfArray($field, 'name');
                        $formfieldto->init($field);
                        $element = $formfactoryhtmllib->get($formfieldto);
                        //array_push($listgroup->elements,$element);
						$listgroup->elements[$ffieldname]=$element;
                    }

                }
              array_push($fdata->othersgroups,$listgroup);
                
            } else if (!$this->config->showfirstitem){
				
				$listgroup=new \stdClass();
                $listgroup->name=$groupname;
                $listgroup->label=$grouplabel;
                $listgroup->elements=array();
                $listgroup->status="";
                if($cont==0){
                    if($this->page->getViewsytemtype()=='bootstrap2'){$listgroup->status=" in";}
                    else if($this->page->getViewsytemtype()=='bootstrap4'){$listgroup->status=" show ";} 
                }
                foreach ($formfields as $field) {
                    $fgroup =  $this->getUtildata()->getVaueOfArray($field, 'group');
                    if($fgroup == $groupnameoriginal){
						$ffieldname = $this->getUtildata()->getVaueOfArray($field, 'name');
                        $formfieldto->init($field);
                        $element = $formfactoryhtmllib->get($formfieldto);
                        //array_push($listgroup->elements,$element);
						$listgroup->elements[$ffieldname]=$element;
                    }

                }
              array_push($fdata->othersgroups,$listgroup);
			}
            $cont++;
         }
        }
		
        return $fdata;
    }
    public function makeAdvancedAddFormData(){
        $fdata=new \stdClass();
        $fdata->firstgroup='';
        $fdata->firstelement='';

        $fdata->othersgroups=array();
        $fdata->config=$this->getConfig();
        $formfieldto = $this->getContainer()->get('badiu.system.core.lib.form.field');
        $formfactoryhtmllib=null;
        if($this->page->getViewsytemtype()=='bootstrap4'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap4');}
        else if($this->page->getViewsytemtype()=='bootstrap3'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap3');}
        else if($this->page->getViewsytemtype()=='bootstrap2'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap2');}
		$fn= $this->getUtildata()->getVaueOfArray($this->config, "formnumber");
        
       
		
        $formconfig =$this->getUtildata()->getVaueOfArray($this->page->getData(), "badiu_formconfig$fn");
		
		$splitfieldscolumn=$this->getUtildata()->getVaueOfArray($formconfig, 'formconfig.splitfieldscolumn',true);
		$showall=$this->getUtildata()->getVaueOfArray($formconfig, 'formconfig.showall',true);
		$bootstrapcolumns="";
		if(!empty($splitfieldscolumn)){$bootstrapcolumns=12/$splitfieldscolumn;}
		$fdata->splitfieldscolumn=$splitfieldscolumn;
		$fdata->bootstrapcolumns=$bootstrapcolumns;
		
        $formgroup = $this->getUtildata()->getVaueOfArray($formconfig, 'formgroup');
        $formfields = $this->getUtildata()->getVaueOfArray($formconfig, 'formfields');

        $cont=0;
        foreach ($formgroup as $fgroup) {
            $groupnameoriginal = $this->getUtildata()->getVaueOfArray($fgroup, 'name');
            $groupname = $this->getUtildata()->getVaueOfArray($fgroup, 'name');
            $grouplabel = $this->getUtildata()->getVaueOfArray($fgroup, 'label');
            $groupname =str_replace(".","-",$groupname);
            
                $listgroup=new \stdClass();
                $listgroup->name=$groupname;
                $listgroup->label=$grouplabel;
                $listgroup->elements=array();
                $listgroup->status="";
				if($showall){ $listgroup->status=" show ";}
                if($cont==0){$listgroup->status=" show ";}
                foreach ($formfields as $field) {
                    $fgroup =  $this->getUtildata()->getVaueOfArray($field, 'group');
					$ffieldname = $this->getUtildata()->getVaueOfArray($field, 'name');
                    if($fgroup == $groupnameoriginal){
                        $formfieldto->init($field);
                        $element = $formfactoryhtmllib->get($formfieldto);
                        //array_push($listgroup->elements,$element);
						$listgroup->elements[$ffieldname]=$element;
                    }

                }
                array_push($fdata->othersgroups,$listgroup);
                $cont++;
            
        }
        return $fdata;
    }
    public function makeSimpleFormData(){
        $fdata=new \stdClass();
        $fdata->elements=array();
        $fdata->config=$this->getConfig();
        $formfieldto = $this->getContainer()->get('badiu.system.core.lib.form.field');
        $formfactoryhtmllib=null;
        if($this->page->getViewsytemtype()=='bootstrap4'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap4');}
        else if($this->page->getViewsytemtype()=='bootstrap3'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap3');}
        else if($this->page->getViewsytemtype()=='bootstrap2'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap2');}
        $fn= $this->getUtildata()->getVaueOfArray($this->config, "formnumber");
        
       

        $formconfig =$this->getUtildata()->getVaueOfArray($this->page->getData(), "badiu_formconfig$fn");
        $formfields = $this->getUtildata()->getVaueOfArray($formconfig, 'formfields');

        foreach ($formfields as $field) {
             $formfieldto->init($field);
             $element = $formfactoryhtmllib->get($formfieldto);
                array_push($fdata->elements,$element);
        }
        return $fdata;
    }
    public function makeAdvancedSchedulerEditData($isclone=false){
        $fdata=new \stdClass();
        $fdata->firstgroup='';
        $fdata->firstelement='';

        $fdata->othersgroups=array();
        $fdata->config=$this->getConfig();
        $formfieldto = $this->getContainer()->get('badiu.system.core.lib.form.field');
        $formfactoryhtmllib=null;
        if($this->page->getViewsytemtype()=='bootstrap4'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap4');}
        else if($this->page->getViewsytemtype()=='bootstrap3'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap3');}
        else if($this->page->getViewsytemtype()=='bootstrap2'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap2');}
       
         $fn= $this->getUtildata()->getVaueOfArray($this->config, "formnumber");
       

        $formconfig =$this->getUtildata()->getVaueOfArray($this->page->getData(), "badiu_formconfig$fn");
		$showall=$this->getUtildata()->getVaueOfArray($formconfig, 'formconfig.showall',true);
		
        $formgroup = $this->getUtildata()->getVaueOfArray($formconfig, 'formgroup');
        $formfields = $this->getUtildata()->getVaueOfArray($formconfig, 'formfields');

        $listoffirstelements=$this->getElementsOfFirstGroupData();
       if($isclone){$listofschedulerelements=$this->getSchedulerFormElementsData();}
        $cont=0;
        foreach ($formgroup as $fgroup) {
            $groupnameoriginal = $this->getUtildata()->getVaueOfArray($fgroup, 'name');
            $groupname = $this->getUtildata()->getVaueOfArray($fgroup, 'name');
            $grouplabel = $this->getUtildata()->getVaueOfArray($fgroup, 'label');
            $groupname =str_replace(".","-",$groupname);
            
            if($groupname!='_badiuschedulerformclone' && $cont > 0){
                $listgroup=new \stdClass();
                $listgroup->name=$groupname;
                $listgroup->label=$grouplabel;
                $listgroup->elements=array();
                $listgroup->status="";
                if($showall){ $listgroup->status=" show ";}
                if($cont==1){
                    if($isclone){ foreach ($listofschedulerelements as $el) {array_push($listgroup->elements,$el); }}
                    foreach ($listoffirstelements as $el) {array_push($listgroup->elements,$el); }
                    if($this->page->getViewsytemtype()=='bootstrap2'){$listgroup->status=" in";}
                    else if($this->page->getViewsytemtype()=='bootstrap4'){$listgroup->status=" show ";} 
                  
               }
                foreach ($formfields as $field) {
                    $fgroup =  $this->getUtildata()->getVaueOfArray($field, 'group');
					$ffieldname = $this->getUtildata()->getVaueOfArray($field, 'name');
                    if($fgroup == $groupnameoriginal){
                        $formfieldto->init($field);
                        $element = $formfactoryhtmllib->get($formfieldto);
                        //array_push($listgroup->elements,$element);
						$listgroup->elements[$ffieldname]=$element;
                    }

                }
               
              array_push($fdata->othersgroups,$listgroup);
             
            }
            $cont++;
        }
        return $fdata;
    }

    public function getElementsOfFirstGroupData(){
         $elements=array();
        
        $formfieldto = $this->getContainer()->get('badiu.system.core.lib.form.field');
        $formfactoryhtmllib=null;
        if($this->page->getViewsytemtype()=='bootstrap4'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap4');}
        else if($this->page->getViewsytemtype()=='bootstrap3'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap3');}
        else if($this->page->getViewsytemtype()=='bootstrap2'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap2');}
        $fn= $this->getUtildata()->getVaueOfArray($this->config, "formnumber");
        
       

        $formconfig =$this->getUtildata()->getVaueOfArray($this->page->getData(), "badiu_formconfig$fn");

        $formgroup = $this->getUtildata()->getVaueOfArray($formconfig, 'formgroup');
        $formfields = $this->getUtildata()->getVaueOfArray($formconfig, 'formfields');
        $cont=0;
        foreach ($formgroup as $fgroup) {
            $groupnameoriginal = $this->getUtildata()->getVaueOfArray($fgroup, 'name');
           
             if($cont==0){
               foreach ($formfields as $field) {
                $fgroup =  $this->getUtildata()->getVaueOfArray($field, 'group');
                if($fgroup == $groupnameoriginal){
                    $formfieldto->init($field);
                    $element = $formfactoryhtmllib->get($formfieldto);
                    array_push($elements,$element);
                 }
                }

            }
            $cont++;
           if( $cont > 0){break;}
         }
        return $elements;
    }

    public function getSchedulerFormElementsData(){
        $elements=array();
       
       $formfieldto = $this->getContainer()->get('badiu.system.core.lib.form.field');
       $formfactoryhtmllib=null;
       if($this->page->getViewsytemtype()=='bootstrap4'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap4');}
       else if($this->page->getViewsytemtype()=='bootstrap3'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap3');}
       else if($this->page->getViewsytemtype()=='bootstrap2'){ $formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap2');}
       $fn= $this->getUtildata()->getVaueOfArray($this->config, "formnumber");
       
      

       $formconfig =$this->getUtildata()->getVaueOfArray($this->page->getData(), "badiu_formconfig$fn");

       $formgroup = $this->getUtildata()->getVaueOfArray($formconfig, 'formgroup');
       $formfields = $this->getUtildata()->getVaueOfArray($formconfig, 'formfields');

       foreach ($formgroup as $fgroup) {
           $groupname = $this->getUtildata()->getVaueOfArray($fgroup, 'name');
           if($groupname=='_badiuschedulerformclone'){
              foreach ($formfields as $field) {
                   $fgroup =  $this->getUtildata()->getVaueOfArray($field, 'group');
                   if($fgroup == $groupname){
                       $formfieldto->init($field);
                       $element = $formfactoryhtmllib->get($formfieldto);
                       array_push($elements,$element);
                   }

               }
               break;
           }
         
       }
       return $elements;
   }
    public function formButtonLabel($type='add'){
		$fn= $this->getUtildata()->getVaueOfArray($this->config, "formnumber");
		$formconfig =$this->getUtildata()->getVaueOfArray($this->page->getData(), "badiu_formconfig$fn");
		$submitlabel=$this->getUtildata()->getVaueOfArray($formconfig, 'formconfig.submitlabel',true);
		if(!empty($submitlabel)){
			$label=$this->getTranslator()->trans($submitlabel);
			return $label;
		}
		
        $label=$this->getTranslator()->trans('add');
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $currentkey=$this->page->getKey();
        $isedit=$sysoperation->isEdit($currentkey);
        if($isedit){ $label=$this->getTranslator()->trans('edit');}
        return $label;
    }
   public function getPage(){
       return $this->page;
   }

   public function setPage($page){
        $this->page=$page;
    }

    public function getIsadvancedform(){
        return $this->isadvancedform;
    }
 
    public function setIsadvancedform($isadvancedform){
         $this->isadvancedform=$isadvancedform;
     }

     public function getConfig(){
        return $this->config;
    }
 
    public function setConfig($config){
         $this->config=$config;
     }
	
}
