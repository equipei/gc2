<div id="_badiu_theme_base_form_vuejs">

<div class="row">
	<div v-show="formcontrol.haserror" class="col alert alert-danger">
		<div v-html="formcontrol.message"></div>
	</div>
 </div> 
 
 <div class="row">
	<div v-show="formcontrol.status=='success'" class="col alert  alert-success">
		<div v-html="formcontrol.message"></div>
	</div>
 </div> 
 
<div class="row">
<div class="col" v-show="formcontrol.status=='open'">
	
	<?php  foreach ($fdata->othersgroups as $fgroup) { ?>
        <fieldset class="form-group">
            <legend>
                <a data-toggle="collapse" href="#<?php echo $fgroup->name; ?>" aria-controls="badiu-collapse" aria-expanded="false"><span class="glyphicon glyphicon-triangle-right"></span> <?php echo $fgroup->label; ?></a>
            </legend>
            <div class="collapse <?php echo $fgroup->status; ?>" id="<?php echo $fgroup->name; ?>">
                <?php  foreach ($fgroup->elements as $element) { ?>
                    <?php echo $element; ?>
                <?php }?>
            </div>
        </fieldset>
    <?php }?> <!-- foreach ($fdata->othersgroups as $fgroup) {-->

	</div>
	</div> <!-- end div row-->
	<div class="row" v-show="formcontrol.status=='open'">
		<div class="col">
            <button class="btn btn-primary" @click="processTabScheduleParamFilter()"><?php echo $fdata->proccessbuttonlabelsavefinish;?></button><?php echo $fdata->iconprocessing; ?>
            <button class="btn btn-primary"  @click="processTabScheduleBack()"><?php echo $fdata->proccessbuttonlabelgoback;?></button>
            <button class="btn btn-secondary" @click="processTabScheduleCancel()"><?php echo $fdata->proccessbuttonlabelcancel;?></button>
    	</div>
	</div>
	 

</div>