
<div id="badiu-report-search">
<div class="card">
   <?php if(!$fdata->config->onlyform){?> <div class="card-header">Filtro</div><?php }?>
    <div class="card-body">
        <!-- inicio filtro -->
		<?php if($fdata->config->showfirstitem){?>
        <div class="row" id="badiu-report-search-firtrow">
            <!-- star first column of filter -->

            <div class="col text-right">
                <div id="badiu-report-search-firtrow-elements">
                    <?php echo $fdata->firstelement; ?>
                </div>
                <button @click="search()"  type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.system.action.search');?></button>

               <a data-toggle="collapse" href="#badiu-report-search-othersrows"><?php echo $translator->trans('badiu.system.action.searchadvanced');?></a>
            </div>
        </div>
		<?php }?>
		<!-- end first column of filter -->
        <div class="row <?php if($fdata->config->showfirstitem){ echo "collapse";} ?>" id="badiu-report-search-othersrows">
            <!-- star first column of filter -->
            <div class="col-12">
            <?php  foreach ($fdata->othersgroups as $fgroup) {?> 
                <fieldset class="form-group" id="<?php echo $fgroup->name; ?>-fieldset">
                    <legend>
                        <a data-toggle="collapse" href="#<?php echo $fgroup->name; ?>" aria-controls="badiu-collapse" aria-expanded="false"><span class="glyphicon glyphicon-triangle-right"></span> <?php echo $fgroup->label; ?></a>
                    </legend>
                    <div class="collapse <?php echo $fgroup->status; ?>" id="<?php echo $fgroup->name; ?>">
                        <?php if(!empty($fdata->bootstrapcolumns)) {?><div class="row"><?php }?>
						<?php  foreach ($fgroup->elements  as $fkey => $element) { ?>
							<?php if(!empty($fdata->bootstrapcolumns)) {echo "<div class=\"col-sm-6 col-md-$fdata->bootstrapcolumns\" id=\"fgitem_$fkey\">";}?>                           
						   <?php echo $element; ?>
						   <?php if(!empty($fdata->bootstrapcolumns)) {echo "</div>";}?>
                        <?php }?>
                        <?php if(!empty($fdata->bootstrapcolumns)) {?></div><?php }?>
                    </div>
                </fieldset>
               <?php }?> <!-- foreach ($fdata->othersgroups as $fgroup) {-->
                
            </div> 
			<?php if(!$fdata->config->onlyform){?>
              <div class="col-12">
				<button  @click="search()" type="button" class="btn btn-primary">Pesquisar</button>
			  </div>
			<?php }?> 
        </div><!-- end first column of filter -->


    </div><!-- end card body -->
    <!-- fim do filtro -->
</div> 
</div> 
<hr />