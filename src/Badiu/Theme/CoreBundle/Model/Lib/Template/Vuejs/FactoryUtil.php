<?php
namespace Badiu\Theme\CoreBundle\Model\Lib\Template\Vuejs;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryUtil extends BadiuModelLib {

    public function __construct(Container $container) {
        parent::__construct($container);
    }
 
    function iconProcess($var="formcontrol.processing",$imgurlbudle=true,$imgurl='bundles/badiuthemecore/image/icons/processing.gif') {
        $html="";
        $urliconprocess="";
        if($imgurlbudle){
            $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
            $urliconprocess=$utilapp->getResourseUrl($imgurl);
        }else{$urliconprocess=$imgurl;}
        
        $html="<img src=\"$urliconprocess\" v-show=\"$var\">";
        return $html;
    }
    
}
