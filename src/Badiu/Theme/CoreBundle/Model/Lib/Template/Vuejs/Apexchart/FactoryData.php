<?php
namespace Badiu\Theme\CoreBundle\Model\Lib\Template\Vuejs\Apexchart;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryData extends BadiuModelLib {

    public function __construct(Container $container) {
        parent::__construct($container);
    }
 
	
    function makeDataRows($param) {
	
         $datarows=$this->getUtildata()->getVaueOfArray($param,'badiu_list_data_rows.data',true);
		 $dtyperows=$this->getUtildata()->getVaueOfArray($param,'badiu_list_data_rows.dtype',true);
		 
		 $jsdata="";
		 if(empty($datarows)){return "";}
		 if(!is_array($datarows)){return "";}
		 foreach ($datarows as $dk => $drow) {
			$dtype= $this->getUtildata()->getVaueOfArray($dtyperows,$dk);
			if($dtype=='bar'){$jsdata.=$this->makeDataBarStruture($dk,$drow);}
			else if($dtype=='pie'){$jsdata.=$this->makeDataPieStruture($dk,$drow);}
			
		 }
		 $outjsdata="apexchartdata:{
			 $jsdata
		 },";
        return  $outjsdata;
    }
   

	function makeDataBarStruture($index,$rows) {
		$tdata="";
		$tlabel="";
		$cont=0;
		$separator="";
		
		 if(!is_array($rows)){return "";}
		foreach ($rows as $item) {
            if ($cont == 0) {
                $separator = "";
            } else {
                $separator = ",";
            }
			$name =$this->getUtildata()->getVaueOfArray($item,'name');
			$value =$this->getUtildata()->getVaueOfArray($item,'value');
			$tdata.=$separator.$value;
			$tlabel.=$separator."'".$name."'";
			$cont++;
		}	
	$chart="
	   rowsdata$index:{
	
		series: [{
            data: [$tdata]
          }],
          chartOptions: {
            chart: {
              type: 'bar',
              height: 350
            },
            plotOptions: {
              bar: {
                borderRadius: 4,
                horizontal: true,
              }
            },
            dataLabels: {
              enabled: true,
            },
            xaxis: {
              categories: [$tlabel],
            }
          },
	   },";

	return $chart;
	}
	
	function makeSourceDataRows($param) {
         $datarows=$this->getUtildata()->getVaueOfArray($param,'data');
		 $dtyperows=$this->getUtildata()->getVaueOfArray($param,'dtype');
		 $jsdata="";
		 if(empty($datarows)){return null;}
		 if(!is_array($datarows)){return null;}
		 $newdata=array();
		 $names=array();
		 $values=array();
		  foreach ($datarows as $dk => $drow) {
			  $cont=0;
			  foreach ($drow as $ditem) {
				  $name =$this->getUtildata()->getVaueOfArray($ditem,'name');
				  $value =$this->getUtildata()->getVaueOfArray($ditem,'value');
				  if(is_numeric($value)){$value=$value+0;} 
				  $names[$dk][$cont]=$name;
				  $values[$dk][$cont]=$value;
				  $cont++;
			  }
			//$dtype= $this->getUtildata()->getVaueOfArray($dtyperows,$dk);
			//if($dtype=='bar'){$newdata[$dk]=$this->makeDataBarSourceStruture();}
			//else if($dtype=='pie'){$newdata[$dk]=$this->makeDataPieSourceStruture();}
			
		}
		 foreach ($datarows as $dk => $drow) {
			 $key="rowsdata$dk";
			
			 $dtype= $this->getUtildata()->getVaueOfArray($dtyperows,$dk);
			if($dtype=='bar'){
				$newdata[$key]=$this->makeDataBarSourceStruture();
				$newdata[$key]['series'][0]['data']=$this->getUtildata()->getVaueOfArray($values,$dk);
				$newdata[$key]['chartOptions']['xaxis']['categories']=$this->getUtildata()->getVaueOfArray($names,$dk);
			}
			else if($dtype=='pie'){
				$newdata[$key]=$this->makeDataPieSourceStruture();
				$lid=$this->getUtildata()->getVaueOfArray($values,$dk);
				$newdata[$key]['series']=$this->getUtildata()->getVaueOfArray($values,$dk);;
				$newdata[$key]['chartOptions']['labels']=$this->getUtildata()->getVaueOfArray($names,$dk);
				
			}
			
			
		}
		 return  $newdata;
    }
   
	function makeDataBarSourceStruture() {
		$param=array();
		$param['series'][0]['data']=array();
		$param['chartOptions']['chart']['type']='bar';
		$param['chartOptions']['chart']['height']=350;
		$param['chartOptions']['plotOptions']['bar']['borderRadius']=4;
		$param['chartOptions']['plotOptions']['bar']['horizontal']=true;
		$param['chartOptions']['dataLabels']['enabled']=true;
		$param['chartOptions']['xaxis']['categories']=array();
		return $param;
	}
	
	
	function makeDataPieStruture($index,$rows) {
		$tdata="";
		$tlabel="";
		$cont=0;
		$separator="";
		 if(!is_array($rows)){return "";}
		foreach ($rows as $item) {
            if ($cont == 0) {
                $separator = "";
            } else {
                $separator = ",";
            }
			$name =$this->getUtildata()->getVaueOfArray($item,'name');
			$value =$this->getUtildata()->getVaueOfArray($item,'value');
			$tdata.=$separator.$value;
			$tlabel.=$separator."'".$name."'";
			$cont++;
		}	
	$chart="
	   rowsdata$index:{
	
		series: [$tdata],
          chartOptions: {
            chart: {
              type: 'pie',
              width: 380
            },
			labels:[$tlabel],
            responsive: [{
              breakpoint: 480,
              options: {
                chart: {
                  width: 200
                },
                legend: {
                  position: 'bottom'
                }
              }
            }],
           
          },
	   },";

	return $chart;
	}
	
	function makeDataPieSourceStruture() {
		$param=array();
		$param['series']=array();
		$param['chartOptions']['chart']['type']='pie';
		$param['chartOptions']['chart']['width']=380;
		$param['chartOptions']['labels']=array();
		$param['chartOptions']['responsive'][0]['breakpoint']=480;
		$param['chartOptions']['responsive'][0]['options']['chart']['width']=200;
		$param['chartOptions']['responsive'][0]['options']['legend']['position']='bottom'; 
		return $param;
	}
}
