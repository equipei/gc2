<?php
namespace Badiu\Theme\CoreBundle\Model\Lib\Template\Vuejs;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryMakeData extends BadiuModelLib {

    public function __construct(Container $container) {
        parent::__construct($container);
    }
 
    
function makeDashboardRow($page, $utildata,$json) {
	
    //make row
        $jsrvar = "";
    
        $listrow = $utildata->getVaueOfArray($page->getData(), 'badiu_list_data_row');
        $listdata = $utildata->getVaueOfArray($listrow,'data');
        $listlabel = $utildata->getVaueOfArray($listrow,'label');
        $listtitle = $utildata->getVaueOfArray($listrow,'title');
        if(empty($listrow)){return $jsrvar ;}
        $cont = 0;
        $seq = 1;
        $separator = "";
        foreach ( $listdata as $row) {
            $jsrvar .= "rowdata$seq: {";
            $contsubitem = 0;
            foreach ($row as $k => $v) {
                if ($contsubitem == 0) {
                    $separator = "";
                } else {
                    $separator = ",";
                }
                if(is_a($v, 'DateTime')){$v="";}//revie when is object or date
                else if(is_array($v)){$v="";}//review
                $v=$json->escape($v);
                $jsrvar .= "$separator $k: "."'".$v."'";
                $contsubitem++;
            }
            $jsrvar .= "}";
            $cont++;
            $seq++;
        }
         if($cont){$jsrvar .= ","; }
         
     
         $jslabel=makeDashboardAddLabel($listlabel,$json,true);
          $jsrvar .=$jslabel;
         $jstitle=makeDashboardAddTitle($listtitle,$json,true);
         $jsrvar .=$jstitle;
     
         
        return $jsrvar;
    }
    
    
    function makeDashboardRows($page, $utildata,$json) {
    
        $jsrvar = "";
        $listrow = $utildata->getVaueOfArray($page->getData(), 'badiu_list_data_rows');
        $listdata = $utildata->getVaueOfArray($listrow,'data');
        $listlabel = $utildata->getVaueOfArray($listrow,'label');
        $listtitle = $utildata->getVaueOfArray($listrow,'title');
         if(empty($listrow)){return $jsrvar ;}
        $cont = 0;
         $seq=1;
        $separator = "";
        $separator1 = "";
        foreach ($listdata as $row) {
             if ($cont == 0) {
                    $separator = "";
                } else {
                    $separator = ",";
                }
                
            $jsrvar .= "$separator rowsdata$seq: [";
            $contsubitem = 0;
            if(is_array($row)){
              foreach ($row as $item) {
                if ($contsubitem == 0) {
                    $separator1 = "";
                } else {
                    $separator1 = ",";
                }
                
               $jsrvar .= "$separator1 {";
               $separator2 = ",";
               $contsubitem1 = 0;
                if(is_array($item)){
                 foreach ($item as $k => $v) {
                     if ($contsubitem1 == 0) {
                        $separator2 = "";
                        } else {
                            $separator2 = ",";
                        }
                        if(is_a($v, 'DateTime')){$v="";}//revie when is object or date
                        else if(is_array($v)){$v="";}//review
                        $v=$json->escape($v);
                        $jsrvar .= "$separator2 $k: "."'".$v."'";
                        $contsubitem1++;
                 }
                }
            
                $contsubitem++;
                 $jsrvar .= "}";
             }
            }
            $jsrvar .= "]";
            $cont++;
            $seq++;
        }
        if($cont){$jsrvar .= ","; }
        
        $jslabel=makeDashboardAddLabel($listlabel,$json,false);
        $jsrvar .=$jslabel;
        $jstitle=makeDashboardAddTitle($listtitle,$json,false);
         $jsrvar .=$jstitle;
        return $jsrvar;
    }
    
    function makeDashboardAddLabel($list,$json,$single=true) {
       $jsrvar = "";
        $varname="rowlabel";
        if(!$single){$varname="rowslabel";}
        if(empty($list)){return $jsrvar ;}
        $cont = 0;
        $seq = 1;
        $separator = "";
        foreach ($list as $row) {
            $jsrvar .= "$varname$seq: [";
            $contsubitem = 0;
            foreach ($row as $k => $v) {
                if ($contsubitem == 0) {
                    $separator = "";
                } else {
                    $separator = ",";
                }
                $v=$json->escape($v);
                $jsrvar .= "$separator {key: \"$k\", value: \"$v\"}";
                $contsubitem++;
            }
            $jsrvar .= "],"; 
            $cont++;
            $seq++;
        }
        // if($cont){$jsrvar .= ","; }
         
    
        return $jsrvar;
    }
    
    
    function makeDashboardAddTitle($list,$json,$single=true) {
        $jsrvar = "";
        $varname="rowtitle";
        if(!$single){$varname="rowstitle";}
        if(empty($list)){return $jsrvar ;}
     
        $seq = 1;
        $separator = "";
        foreach ($list as $row) {
            $v="\"$row\"";
            $jsrvar .= "$varname$seq: $v,";
               $seq++;
        }
    
         
    
        return $jsrvar;
    }

    function formarray($list,$var='customdata') {
        $html="";
      
        return $html;
    }
    
}
