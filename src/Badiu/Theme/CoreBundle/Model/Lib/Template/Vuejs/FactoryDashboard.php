<?php
namespace Badiu\Theme\CoreBundle\Model\Lib\Template\Vuejs;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryDashboard extends BadiuModelLib {

    public function __construct(Container $container) {
        parent::__construct($container);
    }

    function makeRowTable($index,$insidpanel=true,$additionalinfo=array()) {
        if (empty($index)) {
            return "";
        }
         $html =""; 
        
         //add head panel
         if($insidpanel){
              $html.="<div class=\"card\" id=\"row$index\">";
                $html.="<div class=\"card-header\" v-html=\"rowtitle$index\"> </div>";
                $html.="<div class=\"card-body\">";
         }
        $html .= "<table class=\"table table-condensed table-hover  bootgrid-table\" id=\"table$index\">";
       
        $html .= "<tr  v-for=\"row in rowlabel$index\"> ";
        $html .= "<td><div v-html=\"row.value\" ></div></td>";
        $html .= "<td><div v-html=\"rowdata".$index."[row.key]\" ></div></td>";
        $html .= "</tr>";

        $html .= "</table>";
        
        //add end panel
        if($insidpanel){
            $footer=$this->getUtildata()->getVaueOfArray($additionalinfo,'footer');
            $html.=$footer;
             $html.="</div>";
             $html.="</div>";
        }
       return $html;
    }
    function makeRowsTable($index,$insidpanel=true,$additionalinfo=array()) {
        if (empty($index)) {
            return "";
        }
         $html ="";
         //add head panel
         if($insidpanel){
              $html.="<div class=\"card\" id=\"cardrows$index\">";
                $html.="<div class=\"card-header\" v-html=\"rowstitle$index\"></div>";
                $html.="<div class=\"card-body\">";
         }
        $html .= "<table class=\"table table-condensed table-hover  bootgrid-table\" id=\"tables$index\">";
        $html .= "<thead>";
        $html .= "<tr> ";
        $html .= "<th  v-for=\"row in rowslabel$index\"> <div v-html=\"row.value\" ></div> </th>";
        $html .= "</tr>";

        $html .= "</thead>";
        $html .= "<tbody>";
        $html .= "<tr  v-for=\"row in rowsdata$index\"> ";
        $html .= "<td v-for=\"item in rowslabel$index\"> <div v-html=\"row[item.key]\"></div> </td>";
        $html .= "</tr>";
        $html .= "</tbody>";
        $html .= "</table>";
        
        //add end panel
        if($insidpanel){
            $footer=$this->getUtildata()->getVaueOfArray($additionalinfo,'footer');
            $html.=$footer;
             $html.="</div>";
             $html.="</div>";
        }
       return $html;
    }

    function makeRowListTable($size) {
        $html="";
      
        for ($i = 1; $i <= $size; $i++) {
              $html.= $this->makeRowTable($i,true);
         
        }
        return $html;
    }
    function makeRowsListTable($size) {
        $html="";
        for ($i = 1; $i <= $size; $i++) {
              $html.= $this->makeRowsTable($i,true);
         
        }
        return $html;
    }


    function makeRowTables($index,$lebelgrop) {
        if (empty($index)) {
            return "";
        }
         $html ="";
         $cont=0;
         foreach ($lebelgrop as $gk =>  $gvalue){
            $title=$this->getTranslator()->trans($gk);
            
            $tkey=$index."_".$cont;
            $html.="<div class=\"card\" id=\"cardrowtable$tkey\">";
            $html.="<div class=\"card-header\">$title</div>";
            $html.="<div class=\"card-body\">";
           
            $html .= "<table class=\"table table-condensed table-hover  bootgrid-table\" id=\"tables$tkey\">";
             foreach ($gvalue as $kl => $value){
               
                $html .= "<tr> ";
                $html .= "<td width=\"15%\">$value</td>";
                $html .= "<td> <div v-html=\"rowdata1.$kl\"></div> </td>";
                $html .= "</tr>";
                $html .= "</tbody>";
               
            }
            $cont++;
            $html .= "</table>";
            $html.="</div>";
            $html.="</div>";
            $html .= "<br /><br />";
         }
      
        
       return $html;
    }



    function makeAccordion($keytable,$index=1) {
        $indexsub=$index+1;
       // $data1=$this->getVaueOfArray($data, 'badiu_list_data_rows.data.'.$index,true);
       $html="<div id=\"badiu-accordion\">";
       
       $html.="<div  v-for=\"(row,index) in rowsdata$index\">";

       //head card
         $html.="<div class=\"card\" >";
         $html.="<div class=\"card-header\"   v-bind:id=\"'card-item-'+index\">";
         
         $html.=" <h5 class=\"mb-0\">";
         $html.="<button class=\"btn btn-link\" data-toggle=\"collapse\"  v-bind:data-target=\"'#card-subitem-'+index\"  aria-expanded=\"false\"  v-bind:aria-controls=\"'card-subitem-'+index\" v-html=\"row.offername\">";
       //  $html.=" {{row.offername}} - {{index}}";
          $html.="</button>";
          $html.="</h5>";
        
         $html.="</div>";
         //body card
        // $html.="<div  v-bind:id=\"'card-subitem-'+index\" v-if=\"index==0\" class=\"collapse show\" v-bind:aria-labelledby=\"'card-item-'+index\" data-parent=\"#badiu-accordion\">";
         $html.="<div  v-bind:id=\"'card-subitem-'+index\"  class=\"collapse\"  v-bind:class=\"{show: index==0 }\" v-bind:aria-labelledby=\"'card-item-'+index\" data-parent=\"#badiu-accordion\">";
         $html.="<div class=\"card-body\">";

         //content start
         $html .= "<table class=\"table table-condensed table-hover  bootgrid-table\"  v-bind:id=\"'subitem-table-'+index\" >";
         $html .= "<thead>";
         $html .= "<tr> ";
         $html .= "<th  v-for=\"subrow in rowslabel$indexsub\"> <div v-html=\"subrow.value\" ></div> </th>";
         $html .= "</tr>";
 
         $html .= "</thead>";
         $html .= "<tbody>";
         $html .= "<tr  v-for=\"subrow in rowsdata$indexsub\" v-if=\"subrow.offerid==row.".$keytable."\"> ";
         $html .= "<td v-for=\"subitem in rowslabel$indexsub\"> <div v-html=\"subrow[subitem.key]\"></div> </td>";
         $html .= "</tr>";
         $html .= "</tbody>";
         $html .= "</table>";
        //content end
         
         $html.="</div>";

         $html.="</div>";
        $html.="</div>";
       $html.="</div>";
       $html.="</div>";
       return $html;
    }
}
