<div id="badiu-report-search">
<div class="card">
    <div class="card-header">Filtro</div>
    <div class="card-body">
        <!-- inicio filtro -->
        <div class="row-fluid" id="badiu-report-search-firtrow  input-append">
            <!-- star first column of filter -->
 
            <div class="span8">
                <?php echo $fdata->firstelement; ?>
                
			</div>
			<div class="span2">
                <button @click="search()"  type="button" class="btn btn-primary btn">Pesquisar</button>
			</div>
			<div class="span2">
               <a data-toggle="collapse" href="#badiu-report-search-othersrows">Busca avancada</a>
            </div>
        </div><!-- end first column of filter -->
        <div class="row collapse" id="badiu-report-search-othersrows">
            <!-- star first column of filter -->
            <div class="span12">
            <?php  foreach ($fdata->othersgroups as $fgroup) { ?>
                <fieldset class="form-group">
                    <legend>
                        <a data-toggle="collapse" href="#<?php echo $fgroup->name; ?>" aria-controls="badiu-collapse" aria-expanded="false"><span class="glyphicon glyphicon-triangle-right"></span> <?php echo $fgroup->label; ?></a>
                    </legend>
                    <div class="collapse <?php echo $fgroup->status; ?>" id="<?php echo $fgroup->name; ?>">
                        <?php  foreach ($fgroup->elements as $element) { ?>
						
                           <?php echo $element; ?>
                        <?php }?>
                        
                    </div>
                </fieldset>
               <?php }?> <!-- foreach ($fdata->othersgroups as $fgroup) {-->
                
            </div>
            <div class="span12">
				<button  @click="search()" type="button" class="btn btn-primary">Pesquisar</button>
			</div>
        </div><!-- end first column of filter -->


    </div><!-- end card body -->
    <!-- fim do filtro -->
</div> 
</div> 
