<?php

namespace Badiu\Theme\CoreBundle\Model\Lib\Template\Vuejs;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FormFactoryHtmlBootstrap2 extends BadiuModelLib {
    private $field;

    function __construct(Container $container) {
        parent::__construct($container);
    }

    function get($field = null, $showlabel = true) {
        if (!empty($field)) {
            $this->field = $field;
        }


        if (empty($this->field)) {
            return null;
        }
        $field= $this->addDefaultClasse($field); 

        if ($field->getType() == 'text') {
            $html = $this->makeText($field, $showlabel);
            return $html;
        } else if ($field->getType() == 'email') {
            $html = $this->makeEmail($field, $showlabel);
            return $html;
        } else if ($field->getType() == 'password') {
            $html = $this->makePassword($field, $showlabel);
            return $html;
        } if ($field->getType() == 'textarea') {
            $html = $this->makeTextarea($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'choice') {
            $html = $this->makeChoice($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'checkbox') {
            $html = $this->makeCheckbox($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'entity') {
            $html = $this->makeChoice($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'hidden') {
            $html = $this->makeHidden($field);
            return $html;
        }else if ($field->getType() == 'date') {
            $html = $this->makeDate($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'datetime') {
            $html = $this->makeDatetime($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'datetime') {
            $html = $this->makeText($field, $showlabel);
            return $html;
        }
        else if ($field->getType() == 'badiufilterdate') {
            $html = $this->makeFilterdate($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'badiufilternumber') {
            $html = $this->makeFilternumber($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'badiutimeperiod') {
            $html = $this->makeTimePeriod($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'badiuhourtime') {
            $html = $this->makeHourtime($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'badiuhour') {
            $html = $this->makeHour($field, $showlabel);
            return $html;
        }else if ($field->getType() == 'badiuchoicetext') {
            $html = $this->makeChoicetext($field, $showlabel);
            return $html;
        }
       
        else if ($field->getType() == 'badiulmsmoodle') {
            $html = $this->makeLmsMoodle($field, false);
            return $html;
        } 
        else if ($field->getType() == 'badiuautocomplete') {
            $html = $this->makeAutocomplete($field, $showlabel);
            return $html;
        } 
    }

    function addDefaultClasse($field) {
        
           $csstxt=$field->getCssClasss();
           $csstxt.= "  span12 ";
           $field->setCssClasss($csstxt);
        return $field;
    }
    function makeText($field, $showlabel = true) {
        $html = "";
       $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required </label>";
         }
         $html .=$this->addRequiredMessage($field);
         $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "\" name=\"" . $field->getName() . "\">";
        


        return $html;
    } 
    
    
    function makeDate($field, $showlabel = true) {
        $html = "";
        //clean badiudate badiudatetime of css
        $css=$field->getCssClasss();
       $css= str_replace("badiudatetime","",$css);
       $css= str_replace("badiudate","",$css);
        $required=$this->addRequiredInfo($field);
         if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
       
             $html .=$this->addRequiredMessage($field);
            $html .= "<vuejs-datepicker class=\"" .$css . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiutdate\" name=\"" . $field->getName() . "_badiutdate\" :format=\"defaultDatepickerFormat\" :clear-button=\"true\" ></vuejs-datepicker>";
       

 
        return $html;
    }
    function makeDatetime($field, $showlabel = true) {
        $html = "";
        //clean badiudate badiudatetime of css
        $css=$field->getCssClasss();
       $css= str_replace("badiudatetime","",$css);
       $css= str_replace("badiudate","",$css);
        $required=$this->addRequiredInfo($field);
         if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
       
        $html .=$this->addRequiredMessage($field);
        $html .= "<badiudatetime class=\"" .$css . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiutdate\" name=\"" . $field->getName() . "_badiutdate\" format=\"d/m/Y H:i\" lang=\"pt\"></badiudatetime>";
      
        return $html;
    }
      
function makeEmail($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
    
         $html .=$this->addRequiredMessage($field);
        $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "\" name=\"" . $field->getName() . "\" >";
      return $html;
    }
    function makePassword($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
       
        $html .=$this->addRequiredMessage($field);
        $html .= "<input type=\"password\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "\" name=\"" . $field->getName() . "\" >";
        return $html;
    }
    function makeHidden($field) {
        $html = "";
         $html .= "<input type=\"hidden\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\" v-model=\"badiuform." . $field->getName() . "\"  name=\"" . $field->getName() . "\" >";
        return $html;
    }
    function makeTextarea($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . " $required </label>";
        }
        $html .=$this->addRequiredMessage($field);
        $html .= "<textarea   class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "\" name=\"" . $field->getName() . "\" ></textarea>";
        return $html;
    }
    function makeChoice($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
        
            $html .=$this->addRequiredMessage($field);
            $html .= "<select   class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "\"  options=\"badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "\" >";
            $html .= "<option value=\"\">---</option>";
            $html .= "<option v-for=\"item in badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
            $html .= "</select>"; 
          return $html;
    }
     function makeCheckbox($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label><br />";
        }

             $html .=$this->addRequiredMessage($field);
             $html .="<div class=\"row\">";
             $html .= "<div class=\"col\">";
             foreach ($field->getChoicelistvalues() as $key => $value) {
                  $id=$field->getName()."_".$key;
                 
                     $html .= "<input type=\"checkbox\" id=\"$id\" value=\"$key\"  v-model=\"badiuform." . $field->getName() . "\"> <label for=\"jack\">$value</label> &nbsp;&nbsp;";
                
             }
             $html .= "</div>";
             $html .= "</div>";
        
        return $html;
    }
    function makeFilterdate($field, $showlabel = true) {
        $html = "";
        
        $value=$field->getValue();
        $date1=$this->getUtildata()->getVaueOfArray($value,'date1');
        $date2=$this->getUtildata()->getVaueOfArray($value,'date2');
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "</label>";
        }
             $html .=$this->addRequiredMessage($field);
             $html .= "<div class=\"row-fluid\" id=\"" . $field->getName() . "\">";
               
                $html .= "<div class=\"span2\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiudateoperator\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiudateoperator\"  options=\"badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() ."_badiudateoperator\" >";
                    $html .= "<option v-for=\"item in badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
             
              $html .= "<div class=\"span3\"  style=\"position: unset; z-index: 118000\" >";
                   // $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiudate1\"  placeholder=\"".$field->getPlaceholder()."\" value=\"$date1\" name=\"" . $field->getName() . "_badiudate1\">";
                   $html .= "<vuejs-datepicker  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiudate1\"  placeholder=\"".$field->getPlaceholder()."\"  v-model=\"badiuform." . $field->getName() . "_badiudate1\" value=\"$date1\" name=\"" . $field->getName() . "_badiudate1\" :format=\"defaultDatepickerFormat\" :clear-button=\"true\" ></vuejs-datepicker>";
                $html .= "</div>";
                
				$html .= "<div class=\"span1\"  v-show=\"badiuform." . $field->getName() . "_badiudateoperator!='on'\">";
                $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuhour1\"   v-model=\"badiuform." . $field->getName() . "_badiuhour1\"  options=\"badiudefaultformoptions.hour\"  name=\"" . $field->getName() ."_badiuhour1\" >";
                $html .= "<option v-for=\"item in badiudefaultformoptions.hour\" :value=\"item.value\">{{item.text }}</option>";
                $html .= "</select>";
                $html .= "</div>";

                $html .= "<div class=\"span1\"  v-show=\"badiuform." . $field->getName() . "_badiudateoperator!='on'\">";
                $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuminute1\"   v-model=\"badiuform." . $field->getName() . "_badiuminute1\"  options=\"badiudefaultformoptions.minute\"  name=\"" . $field->getName() ."_badiuminute1\" >";
                $html .= "<option v-for=\"item in badiudefaultformoptions.minute\" :value=\"item.value\">{{item.text }}</option>";
                $html .= "</select>";
                $html .= "</div>";
                // v-model=\"badiuform." . $field->getName() . "_badiudate2\"
                $html .= "<div class=\"span3\"  style=\"position: relative\"  v-show=\"badiuform." . $field->getName() . "_badiudateoperator=='between'\">";
                   // $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiudate2\"  placeholder=\"".$field->getPlaceholder()."\"  value=\"$date2\"  name=\"" . $field->getName() . "_badiudate2\">";
                   $html .= "<vuejs-datepicker class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiudate2\"  placeholder=\"".$field->getPlaceholder()."\"   v-model=\"badiuform." . $field->getName() . "_badiudate2\"  name=\"" . $field->getName() . "_badiudate2\"  :format=\"defaultDatepickerFormat\" :clear-button=\"true\" ></vuejs-datepicker>";
                $html .= "</div>";
				
				 $html .= "<div class=\"span1\"  v-show=\"badiuform." . $field->getName() . "_badiudateoperator=='between'\">";
                $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuhour2\"   v-model=\"badiuform." . $field->getName() . "_badiuhour2\"  options=\"badiudefaultformoptions.hour\"  name=\"" . $field->getName() ."_badiuhour2\" >";
                $html .= "<option v-for=\"item in badiudefaultformoptions.hour\" :value=\"item.value\">{{item.text }}</option>";
                $html .= "</select>";
                $html .= "</div>";

                $html .= "<div class=\"span1\"  v-show=\"badiuform." . $field->getName() . "_badiudateoperator=='between'\">";
                $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuminute2\"   v-model=\"badiuform." . $field->getName() . "_badiuminute2\"  options=\"badiudefaultformoptions.minute\"  name=\"" . $field->getName() ."_badiuminute2\" >";
                $html .= "<option v-for=\"item in badiudefaultformoptions.minute\" :value=\"item.value\">{{item.text }}</option>";
                $html .= "</select>";
                $html .= "</div>";
        
             $html .= "</div>";
        


        return $html;
    }
 
function makeFilternumber($field, $showlabel = true) {
        $html = "";

        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "</label>";
        }
            $html .=$this->addRequiredMessage($field);
             $html .= "<div class=\"row\" id=\"" . $field->getName() . "\">";
               
                $html .= "<div class=\"span-4\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiunumberoperator\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiunumberoperator\"  options=\"badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiunumberoperator\" >";
                    $html .= "<option v-for=\"item in badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
               
                $html .= "<div class=\"span4\">";
                    $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiunumber1\"  placeholder=\"".$field->getPlaceholder()."\"   v-model=\"badiuform." . $field->getName() . "_badiunumber1\" name=\"" . $field->getName() . "_badiunumber1\">";
                $html .= "</div>";
                
                // v-model=\"badiuform." . $field->getName() . "_badiunumber2\"
                $html .= "<div class=\"span4\"  v-show=\"badiuform." . $field->getName() . "_badiunumberoperator=='between'\">";
                    $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiunumber2\"  placeholder=\"".$field->getPlaceholder()."\"  v-model=\"badiuform." . $field->getName() . "_badiunumber2\"  name=\"" . $field->getName() . "_badiunumber2\">";
                $html .= "</div>";
             $html .= "</div>";
       


        return $html;
    }
    function makeTimePeriod($field, $showlabel = true) {
        $html = "";//badiutimeperiod
        $config=$field->getConfig();
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "</label>";
        }
      
      //  $html .=$this->addRequiredMessage($field);
      if(empty($config)){
             $html .= "<div class=\"row-fluid\"  id=\"" . $field->getName() . "\">";
        
               
            
                $html .= "<div class=\"span8\">";
                $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiutimeperiodnumber\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiutimeperiodnumber\"  name=\"" . $field->getName() ."_badiutimeperiodnumber\"  >";
                $html .= "</div>";
                
               
                $html .= "<div class=\"span4\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiutimeperiodunittime\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiutimeperiodunittime\"  options=\"badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiutimeperiodunittime\" >";
                    $html .= "<option v-for=\"item in badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
               $html .= "</div>";  
       } else{
                $html .= "<div class=\"row\"  id=\"" . $field->getName() . "\">";
            
                $html .=$this->addRequiredMessage($field);
                   $html .= "<div class=\"span4\">";
                   $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiutimeperiodoperator\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiutimeperiodoperator\"  options=\"badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiutimeperiodoperator\" >";
                       $html .= "<option v-for=\"item in badiuformoptions." . $field->getName() . "_operator\" :value=\"item.value\">{{item.text }}</option>";
                       $html .= "</select>";
                   $html .= "</div>";
                   
                  $html .= "<div class=\"span4\">";
                   $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiutimeperiodnumber\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiutimeperiodnumber\"  name=\"" . $field->getName() ."_badiutimeperiodnumber\"  >";
                   $html .= "</div>";
                   $html .= "<div class=\"span4\">";
                       $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiutimeperiodunittime\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiutimeperiodunittime\"  options=\"badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiutimeperiodunittime\" >";
                       $html .= "<option v-for=\"item in badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                       $html .= "</select>";
                   $html .= "</div>";
                  $html .= "</div>";
            }   
              
        


        return $html;
    }
     function makeHour($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . " $required</label>";
        }
     
             $html .=$this->addRequiredMessage($field);
             $html .= "<div class=\"row-fluid\"  id=\"" . $field->getName() . "\">";
               
              $html .= "<div class=\"span2\">";
                    $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiutimehour\"  placeholder=\"Hora\" v-model=\"badiuform." . $field->getName() . "_badiutimehour\"  name=\"" . $field->getName() ."_badiutimehour\"  >";
                $html .= "</div>";
              
                $html .= "<div class=\"span10\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiutimeminute\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiutimeminute\"  options=\"badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiutimeminute\" >";
                    $html .= "<option v-for=\"item in badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
                $html .= "</div>";
       return $html;
    }
     function makeHourtime($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . " $required</label>";
        }
            $html .=$this->addRequiredMessage($field);
             $html .= "<div class=\"row-fluid\"  id=\"" . $field->getName() . "\">";
              
              $html .= "<div class=\"span2\">";
                     $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuhourtimehour\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiuhourtimehour\"  options=\"badiuformoptions." . $field->getName() . "_hour\"  name=\"" . $field->getName() . "_badiuhourtimehour\" >";
                    $html .= "<option v-for=\"item in badiuformoptions." . $field->getName() . "_hour\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
              
                $html .= "<div class=\"span10\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiuhourtimeminute\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiuhourtimeminute\"  options=\"badiuformoptions." . $field->getName() . "_minute\"  name=\"" . $field->getName() . "_badiuhourtimeminute\" >";
                    $html .= "<option v-for=\"item in badiuformoptions." . $field->getName() . "_minute\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
            $html .= "</div>";   
      
        return $html;
    }
      function makeChoicetext($field, $showlabel = true) {
        $html = "";
        $required=$this->addRequiredInfo($field);
        if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
        $html .=$this->addRequiredMessage($field);
             $html .= "<div class=\"row-fluid\"  id=\"" . $field->getName() . "\">";
            
                $html .= "<div class=\"span4\">";
                    $html .= "<select   class=\"form-control\" id=\"" . $field->getName() . "_badiutchoicetextfield1\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiuchoicetextfield1\"  options=\"badiuformoptions." . $field->getName() . "\"  name=\"" . $field->getName() . "_badiuchoicetextfield1\" >";
                    $html .= "<option v-for=\"item in badiuformoptions." . $field->getName() . "\" :value=\"item.value\">{{item.text }}</option>";
                    $html .= "</select>";
                $html .= "</div>";
                
                $html .= "<div class=\"span8\">";
                    $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "_badiuchoicetextfield2\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuform." . $field->getName() . "_badiuchoicetextfield2\"  name=\"" . $field->getName() ."_badiuchoicetextfield2\"  >";
                $html .= "</div>";
                $html .= "</div>";
       return $html;
    }
   
    function makeLmsMoodle($field, $showlabel = false) {
         $html = "";
         if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "</label>";
        }
          $html .= "<div> <a @click=\"showModalToSelectMoodleItem()\"> moodle </a> </div>";
             $html .= "<input type=\"hidden\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\" v-model=\"badiuform." . $field->getName() . "\"  name=\"" . $field->getName() . "\" >";
        
        return $html ;
     }
     
      function makeAutocomplete($field, $showlabel = false) {
         $html = "";
         $required=$this->addRequiredInfo($field);
         if ($showlabel) {
            $html .= "<label for=\"" . $field->getName() . "\">" . $field->getLabel() . "$required</label>";
        }
        
              $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\" @input=\"badiuautocomplete." . $field->getName() . "_istyping = true\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuautocomplete." . $field->getName() . "_query\" v-if=\"!badiuautocomplete." . $field->getName() . "_valueselected\"  @input=\"badiuautocomplete." . $field->getName() . "_istyping = true\" name=\"" . $field->getName() . "\" >";
             $html .= " <div v-if=\"badiuautocomplete." . $field->getName() . "_valueselected\" class=\"alert alert-info\"> ";
              $html .= "<span v-if=\"badiuautocomplete." . $field->getName() . "_valueselected\" v-html=\"badiuautocomplete." . $field->getName() . "_valueselected\" ></span>";
               $html .= " &nbsp;&nbsp; <a   v-if=\"badiuautocomplete." . $field->getName() . "_valueselected\" @click=\"" . $field->getName() ."_removeitemselected()\" >x<span class=\"glyphicon glyphicon-remove\"></span></a>";
             $html .= "  </div>";
            // $html .= "<input type=\"text\"  class=\"" . $field->getCssClasss() . "\" id=\"" . $field->getName() . "\" @input=\"badiuautocomplete." . $field->getName() . "_istyping  = true\"  placeholder=\"".$field->getPlaceholder()."\" v-model=\"badiuautocomplete." . $field->getName() . "_valueselected\" v-if=\"badiuautocomplete." . $field->getName() . "_valueselected\" @input=\"badiuautocomplete." . $field->getName() . "_istyping = true\" name=\"" . $field->getName() . "\" >";
              $html .= " <div align=\"center\" v-if=\"badiuautocomplete." . $field->getName() . "_isloading\">";
             $html .= "   <span>Procurando...</span>";
             $html .= "  </div>";

             $html .= " <div class=\"list-group\" v-if=\"badiuautocomplete." . $field->getName() . "_isshowlist\">";                                    
             $html .= "  <a v-for=\"sitem in badiuautocomplete." . $field->getName() . "_searchresult\"  @click=\"" . $field->getName() ."_itemselected(sitem)\"   href=\"#\" class=\"list-group-item\">{{sitem.id}} - {{sitem.name}}</a>  ";                                  
             $html .= " </div>";
        
         return $html ;
     }
     
      function addRequiredMessage($field) {
          $html="";
             $html .= "<div v-show=\"badiuaerrorcontrol." . $field->getName() . "\" class=\"alert alert-danger\">";
             $html .= " <div v-html=\"badiuaerrormessage." . $field->getName() . "\"></div>";
             $html .= "</div>";
             return $html;
           
      }
      
 function addRequiredInfo($field) {
          $html="";
          $required=$field->getRequired();
          if($required){
               $html="<i class=\"fa fa-asterisk text-danger\"></i>";
          }
           return $html;
           
      }  

  
    function getField() {
        return $this->field;
    }

    function setField($field) {
        $this->field = $field;
    }

   

}