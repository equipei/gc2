<?php
namespace Badiu\Theme\CoreBundle\Model\Lib\Template\Layout;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryList extends BadiuModelLib {

	private $param;
    public function __construct(Container $container) {
        parent::__construct($container);
    }
	
    function card($param){    
		$title=$this->getUtildata()->getVaueOfArray($param,'title');
		$url=$this->getUtildata()->getVaueOfArray($param,'url');
		$content=$this->getUtildata()->getVaueOfArray($param,'content');
		$id=$this->getUtildata()->getVaueOfArray($param,'id');
	 	$colclass=$this->getUtildata()->getVaueOfArray($this->param,'class.col',true);
	$out= "
		<div class=\"$colclass $id\" id=\"$id-id\">
		<div class=\"card\" >
			<div class=\"card-header\"><h5> <a href=\"$url\">$title</a></h5></div>
			<div class=\"card-body\">
                <p class=\"card-text\">  $content </p>
            </div>
		 </div>
		</div>
	";
	return $out;
	
	}
	
	
function makeGrid($list){
	if(!is_array($list)){return "";}
	if(sizeof($list)==0){return "";}
	
	$rowclass=$this->getUtildata()->getVaueOfArray($this->param,'class.row',true);
	

$html="";
$gridcont=1;
   $rowcont=1;
   $cont=0;
   $starrow="<div class=\"$rowclass badiu-row-$rowcont\" id=\"badiu-row-id-$rowcont\">";
   $endrow="</div> <hr />";
   $showrowcardnumber=3;
  foreach ($list as $item) {
        $url=$this->getUtildata()->getVaueOfArray($item,'link.url',true);
            $name=$this->getUtildata()->getVaueOfArray($item,'link.name',true);
            $description=$this->getUtildata()->getVaueOfArray($item,'link.description',true);
			$keyid=$this->getUtildata()->getVaueOfArray($item,'link.id',true);

            if($gridcont==1){$html.= $starrow;}
			$cparam=array('url'=>$url,'title'=>$name,'content'=>$description,'id'=>$keyid);
			$html.=$this->card($cparam);

            $gridcont++; 
            $cont++;
            if($gridcont > $showrowcardnumber){
                $gridcont=1;
				$rowcont++;
				$starrow="<div class=\"$rowclass badiu-row-$rowcont\" id=\"badiu-row-id-$rowcont\">";
                $html.= $endrow;
            } else if($cont==count($list)){$html.=   $endrow;}
            
      
        } //end foreach ($litem as $item) {

     return  $html;
  }

function makeAccordion($list){
	if(!is_array($list)){return "";}
	if(sizeof($list)==0){return "";}
	
	$rowclass=$this->getUtildata()->getVaueOfArray($this->param,'class.row',true);
	$colclass=$this->getUtildata()->getVaueOfArray($this->param,'class.col',true);
	
	$html="<div class=\"$rowclass\"><div class=\"$colclass\"><div id=\"adicional-itemmenu-accordion\">";
	$cont=0;
    foreach ($list as $item) {
			$subitems=$this->getUtildata()->getVaueOfArray($item,'items');
            $name=$this->getUtildata()->getVaueOfArray($item,'name');
			$content=$this->makeGrid($subitems);
			$aparam=array('title'=>$name,'seq'=>$cont,'content'=>$content);
            $html .=$this->accordion($aparam);
			
            $cont++;
      } //end foreach ($litem as $item) {
   
		
	 $html.=" </div> </div> </div>";
     return $html;
  } 
  
function accordion($param){   
	$title=$this->getUtildata()->getVaueOfArray($param,'title');
	$cont=$this->getUtildata()->getVaueOfArray($param,'seq');
	$content=$this->getUtildata()->getVaueOfArray($param,'content');
	$out="
	<div class=\"card\">
    <div class=\"card-header\" id=\"accordionitem-$cont\">
      <h5 class=\"mb-0\">
        <button class=\"btn btn-link\" data-toggle=\"collapse\" data-target=\"#collapseitem-$cont\" aria-expanded=\"false\" aria-controls=\"accordionitem-$cont\">
          $title
        </button>
      </h5>
    </div>

    <div id=\"collapseitem-$cont\" class=\"collapse\" aria-labelledby=\"accordionitem-$cont\" data-parent=\"#adicional-itemmenu-accordion\">
      <div class=\"card-body\">
        $content
      </div>
    </div>
  </div>
	";
	return $out;
  }  
  
    function getMenuItem($list,$position,$type) {
		$newlist=array();
		 foreach ($list as $litem) {
			foreach ($litem as $item) {
				$itype=$this->getUtildata()->getVaueOfArray($item,'type');
				$iposition=$this->getUtildata()->getVaueOfArray($item,'position');
				if($iposition==$position && $itype==$type){
					array_push($newlist,$item);
				}
		 }
	}
	return $newlist;
	}
   function initParam() {
	   $boostrap=$footer=$this->getUtildata()->getVaueOfArray($this->param,'boostrap');
	   $class=array();
	   if(empty($boostrap)){$boostrap="4";}
	   if($boostrap=="4"){$class=array('row'=>'row','col'=>'col');}
	   else if($boostrap=="3"){$class=array('row'=>'row','col'=>'col-md-4');}
	   else if($boostrap=="2"){$class=array('row'=>'row','col'=>'span4');}
	   $this->param['class']=$class;
  }
  
  function getParam() {
      return $this->param;
  }

  function setParam($param) {
      $this->param = $param;
  }

}
