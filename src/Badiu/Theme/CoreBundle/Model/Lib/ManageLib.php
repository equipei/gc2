<?php
namespace Badiu\Theme\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ManageLib extends BadiuModelLib {

	
    public function __construct(Container $container) {
        parent::__construct($container);
    }
	
    function stateLeftMenu($param=null){    
		if(empty($param)){$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();}
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		$ismobile=preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
		
		$value="";
		$key="badiu.theme.core.layout.menu.left.state";
		$exitkey=$badiuSession->existValue($key);
		
		if(!$exitkey){
			$value="";
			$value="";
			$badiuSession->addValue($key,$value);
			
		}
		else {
			
			$value=$badiuSession->getValue($key);
			
			if($value=="active"){$value="";}
			else {$value="active";}
			if($ismobile){$value="";}
			$badiuSession->addValue($key,$value);
			
		}
		return $value;
	}

}
