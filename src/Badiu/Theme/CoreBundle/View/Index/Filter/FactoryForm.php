
<?php
$formfieldto = $container->get('badiu.system.core.lib.form.field');
$formfactoryhtmllib = $container->get('badiu.theme.core.lib.template.vuejs.formfactoryhtml');
$utilstring=$this->getContainer()->get('badiu.system.core.lib.util.string');
$formconfig = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig');

$formgroup = $utildata->getVaueOfArray($formconfig, 'formgroup');
$formfields = $utildata->getVaueOfArray($formconfig, 'formfields');

//form with fieldset
function formWithFieldsetSearch($container, $page) {
    $viewsytemtype='bootstrap4'; 
    //$viewsytemtype=$page->getViewsytemtype();
    $formfieldto = $container->get('badiu.system.core.lib.form.field');
    $formfactoryhtmllib = $container->get('badiu.theme.core.lib.template.vuejs.formfactoryhtml');
    $formfactoryhtmllib->setViewsytemtype($viewsytemtype);
    $utildata = $container->get('badiu.system.core.lib.util.data');

    $formconfig = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig');

    $formgroup = $utildata->getVaueOfArray($formconfig, 'formgroup');
    $formfields = $utildata->getVaueOfArray($formconfig, 'formfields');
    $formcontent = "";

    $contfield = 0;
    $contgroup = 0;
    $csscontainer='';
    if($viewsytemtype=='bootstrap2'){$csscontainer='class="container-fluid"';}      
    $formcontent .= "<div  $csscontainer id=\"_badiu_theme_base_form_vuejs\">";
    $formcontent .= "<form>";
   
    if(empty($formgroup) || !is_array($formgroup)){return null;}
    foreach ($formgroup as $fgroup) {

        $groupnameoriginal = $utildata->getVaueOfArray($fgroup, 'name');
        $groupname = $utildata->getVaueOfArray($fgroup, 'name');
        $grouplabel = $utildata->getVaueOfArray($fgroup, 'label');
        $groupname =str_replace(".","-",$groupname);
        $openfieldset="";
        if($contgroup > 0){
              if($contgroup==1){
                  $formcontent .= "<div id=\"badiuadvancedsearch\" class=\"row collapse \">";
                 $formcontent .= " <div class=\"col-md-12\">"; //row 1
                  $openfieldset=" in ";
              }else{ $openfieldset="";}
              
              //star fieldset
             //   $formcontent .= "<div>"; // x0 start block
                $formcontent .= "<fieldset>"; // x1 start fieldset
                $formcontent .= "<legend>";
                $formcontent .= "<a data-toggle=\"collapse\" href=\"#$groupname\" ><span class=\"glyphicon glyphicon-triangle-right\"></span>$grouplabel</a>";
                $formcontent .= " </legend>";
                $formcontent .= "<div class=\"collapse $openfieldset\" id=\"$groupname\">";  //x2 start fieldset content
   
        }
        foreach ($formfields as $field) {
            $fgroup = $utildata->getVaueOfArray($field, 'group');
             
            if ($contfield == 0 && $contgroup==0) {
                $formfieldto->init($field);
                $element = $formfactoryhtmllib->get($formfieldto,false);
                $firstfield = <<< eof
                <div class="row pull-right">
                    <div class="col-md-9">
                        <div class="input-group">
                            $element
                            <span class="input-group-btn "><button @click="search()"  type="button" class="btn btn-primaryo">Pesquisar</button></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <a  data-toggle="collapse" href="#badiuadvancedsearch">Busca avancada</a>
                    </div>
                </div><hr />
   
eof;
         $firstfieldbootstrap2 = <<< eof
                <div class="row-fluid">
                    <div class="span8 text-right">
                        $element
                     </div>
                  <div class="spa2 text-left">
                         <button @click="search()"  type="button" class="btn" type="button">Pesquisar</button>
                    </div>
                    <div class="spa2 text-left"> 
                        <a  data-toggle="collapse" href="#badiuadvancedsearch">Busca avancada</a>
                    </div>
                </div><hr />
   
eof;
          if($viewsytemtype=='bootstrap2'){$firstfield=$firstfieldbootstrap2;}       
         $formcontent .= $firstfield;
            } else if ($contgroup > 0 && $fgroup == $groupnameoriginal) {
                 $formfieldto->init($field);
                $element = $formfactoryhtmllib->get($formfieldto);
                $cssclass='class=" row form-group"';
                 if($viewsytemtype=='bootstrap2'){$cssclass= ' ';}  
                $formcontent .= "<div $cssclass >";
                $formcontent .= $element;
                $formcontent .= "</div>";
            }
          $contfield++;
        }
         if($contgroup > 0){
                $formcontent .= "</div>"; //x2 send fieldset content
                $formcontent .= "</fieldset>";// x1 end fieldset
             //   $formcontent .= "</div>";  //x0 end block
         }
       

        $contgroup++;
    }
     //booton search
    
      $formcontent.=" <div class=\"col-md-12\">";
      $formcontent.=" <fieldset>";
      $formcontent.="<button  @click=\"search()\" type=\"submit\" class=\"btn btn-primary\" style=\"margin-top: 10px; margin-bottom:7px;\">Pesquisar</button>";
      $formcontent.="  </fieldset>";
      $formcontent.="</div>";
      $formcontent .= "</div>";  //end row 1
      $formcontent.="</div>"; //end of if($contgroup==1){$formcontent .= "<div class=\"badiu-item-other\">";}
     
    
       
      $formcontent.="</form>";
      $formcontent.="</div>";//end  "<div id=\"_badiu_theme_base_form_vuejs\">";
    return $formcontent;
}

/*echo "<pre>";
  print_r($formconfig);
  echo "</pre>";exit; */

echo formWithFieldsetSearch($container, $page); 


$currenturl = $utilapp->getCurrentUrl(true);
$externalservicequerykey = "";
if ($page->getIsexternalclient()) {
    $currenturl = "BADIU_CORE_SERVICE_CLIENTE_URLBASE";
    $externalservicequerykey = "_key=" . $page->getKey() . "&";
}
/*
 ob_start();
     $vuejsappffurl=$utilapp->getFilePath("BadiuThemeBaseBundle:Form/VuejsAppFactory.php",false);
    include_once($vuejsappffurl);
     $formappvuejs = ob_get_contents();
  ob_end_clean();
  
   ob_start();
   $vuejsdataurl=$utilapp->getFilePath($page->getConfig()->getFileprocessformfilterjsvariable(),false);
  include_once($vuejsdataurl);
  $datavuejs = ob_get_contents();
  ob_end_clean();
  
  ob_start();
    $vuejsmethodurl=$utilapp->getFilePath($page->getConfig()->getFileprocessformfilterjsmethod(),false);
    include_once($vuejsmethodurl);
    $methodvuejs = ob_get_contents();
  ob_end_clean();
  
 ob_start();
    $vuejswatchurl=$utilapp->getFilePath($page->getConfig()->getFileprocessformfilterjswatch(),false);
    include_once($vuejswatchurl);
    $watchvuejs = ob_get_contents();
  ob_end_clean();

$formappvuejs=$utilstring->replaceExpression("BADIU_THEME_BASE_APP_VUEJS_DATA",$datavuejs,$formappvuejs);
$formappvuejs=$utilstring->replaceExpression("BADIU_THEME_BASE_APP_VUEJS_METHOD",$methodvuejs,$formappvuejs);
$formappvuejs=$utilstring->replaceExpression("BADIU_THEME_BASE_APP_VUEJS_WATCH",$watchvuejs,$formappvuejs);
$formappvuejs=$utilstring->replaceExpression("BADIU_THEME_BASE_APP_JS_GENERAL","",$formappvuejs); //review add content general file

echo $formappvuejs; */ 

//include_once("FormFilterVuejs.php");
/*
echo "<pre>";
  print_r($formfields);
  echo "</pre>";exit; */


?>
 