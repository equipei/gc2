
<?php 
function makeTableHead($container,$table){
    $cont = 0;
    $separator = "";
    $out="";
	
    foreach ($table->getColumn()->getList() as $key =>  $item) {
        if ($cont > 0) {
            $separator = ",";
        }
		$type="";
		if(is_array($table->getColumntype()) && array_key_exists($key,$table->getColumntype())){$type=$table->getColumntype()[$key];}
        $out.=$separator;
        $out.="{name:\"$item\",key:\"$key\",type:\"$type\"}";
        $cont++; 
    }
     return $out;
}
//desabled
function makeReportCsv($container,$table){
	return "";
	 $utiljson = $container->get('badiu.system.core.lib.util.json');
	$cont = 0;
    $separator = "";
    $out="";
	$listcolumns=$table->getColumn()->getList();
	if(isset($listcolumns['_ctrl'])){unset($listcolumns['_ctrl']);}
	$listcolumnsvalues = array_values($listcolumns);
	$listcolumnskeys = array_keys($listcolumns);
	
	  foreach ($listcolumnsvalues as $item) {
        if ($cont > 0) {
            $separator = ";";
        }
		 $item = str_replace(';', '\;', $item);
		 $item = str_replace("'", "\'", $item);
		 $item = str_replace('"', '\"', $item);
		$out.=$separator;
		$out.=$item;
        //$out.='"'.$item.'"';
        $cont++; 
    }
	$out.="\n";
	
	$cont1=0;
	$data="";
	foreach ($table->getRow()->getList() as $row) {
		$item="";
		
        $contitem = 0;
        $separatoritem = "";
        foreach ($listcolumnskeys as $k) {
				$value="";
				if(isset($row[$k])){$value=$row[$k];}
				$value = str_replace(';', '\;',  $value);
				$value = str_replace("'", "\'", $value);
				$value = str_replace('"', '\"', $value);
				if ($contitem > 0) {
					$separatoritem = ";";
				}
				if(!empty($value)){
					$value=trim($value);
					$value=strip_tags($value);
				}
				if($value==""){$value="NULL";}
				$item.=$separatoritem;
				$item.=$value;
				//$item.='"'.$value.'"';
				$contitem++; 
			
         }  
		
		$out.=$item;
		$out.="\n";
    }	
	//echo $out;exit; 
	$out=$utiljson->escape($out);
     $result="csv: '".$out."',";
     return $result;
}
//desabled
function makeReportJson($container,$table){
	return "";
	 $utiljson = $container->get('badiu.system.core.lib.util.json');
	
	$listcolumns=$table->getColumn()->getList();
	if(isset($listcolumns['_ctrl'])){unset($listcolumns['_ctrl']);}
	$listcolumnskeys = array_keys($listcolumns);
	
	$out =array();;
	array_push($out,$listcolumns);
	
	foreach ($table->getRow()->getList() as $row) {
		 $sitem=array();
		 foreach ($listcolumnskeys as $k) {
				$value="";
				if(isset($row[$k])){$value=$row[$k];}
				if(!empty($value)){
					$value=trim($value);
					$value=strip_tags($value);
				}
				$sitem[$k]=$value;
		 }  
		 array_push($out,$sitem);
	}	
	
	  $result=$utiljson->encode($out);
	 $result = str_replace("'", "\'", $result);
     $result="json: '".$result."',";
     return $result;
}
?>