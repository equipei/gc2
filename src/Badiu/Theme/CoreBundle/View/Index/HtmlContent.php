<?php 

    $iconedit=$urliconprocess=$utilapp->getResourseUrl('bundles/badiuthemebase/image/icons/edit.gif');
    $imgiconedit="<img src=\"$iconedit\" />";
    
    $iconcopy=$urliconprocess=$utilapp->getResourseUrl('bundles/badiuthemebase/image/icons/copy.gif');
    $imgiconcopy="<img src=\"$iconcopy\" />";
    
    $icondelete=$urliconprocess=$utilapp->getResourseUrl('bundles/badiuthemebase/image/icons/delete.gif');
    $imgicondelete="<img src=\"$icondelete\" />";
    
    $iconrestore=$urliconprocess=$utilapp->getResourseUrl('bundles/badiuthemebase/image/icons/restore.gif');
    $imgiconrestore="<img src=\"$iconrestore\" />";
	
	$iconremove=$urliconprocess=$utilapp->getResourseUrl('bundles/badiuthemebase/image/icons/remove.gif');
    $imgiconremove="<img src=\"$iconremove\" />";
	
	$translator=$container->get('translator');
	$alticonedit=$translator->trans('badiu.system.action.edit.info');
	$alticoncopy=$translator->trans('badiu.system.action.copy.info');
	$alticondelete=$translator->trans('badiu.system.action.delete.info');
	$alticonrestore=$translator->trans('badiu.system.action.restore.info');
	
	$alticonremove=$translator->trans('badiu.system.action.remove.info');
	
	if(!$page->getIsexternalclient()){
		$imgiconedit='<i class="fa fa-edit"></i>';
		$imgiconcopy='<i class="fa fa-copy"></i>';
		$imgicondelete='<i class="fa fa-trash"></i>';
		$imgiconremove='<i class="fas fa-times-circle"></i>';
		$imgiconrestore='<i class="fas fa-trash-restore"></i>';
	}
	
	$permission=$container->get('badiu.system.access.permission');
	//echo $page->getKey();exit;
	$keybasesys=$container->get('badiu.system.core.lib.config.keyutil')->removeLastItem($page->getKey());
	$keyedit=$keybasesys.'.edit';
	$canedit=$permission->has_access($keyedit,$page->getSessionhashkey());
	$counrecord=$table->getCountrow();
	$badiuSession=$container->get('badiu.system.access.session');
	$badiuSession->setHashkey($page->getSessionhashkey());
    $dsession = $badiuSession->get();
	
	 $haspermissioncheduleradd= $badiuSession->hasParamPermission('badiu.system.core.param.permission.scheduler.manager','add');
	 $haspermissionchedulerview= $badiuSession->hasParamPermission('badiu.system.core.param.permission.scheduler.manager','view');
	 
    $countschedulertask=$table->getCountschedulertask(); 
	/*echo "<pre>";
 print_r($dsession);
 echo "</pre>";*/
    $ismreport=false;
	$pos=stripos($page->getKey(), "badiu.moodle.mreport.");
	$sclink="";
	
	if($pos!== false){$ismreport=true;}
	
    $linkchedulertask="";
    if(!empty($countschedulertask) && $haspermissionchedulerview){
		
        $parentid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		$serviceid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
		
        if($ismreport && !empty($parentid)){
			$paramlink=array('parentid'=>$parentid,'_serviceid'=>$serviceid,'functionalitykey'=>$page->getKey()); 
			$sclink=$utilapp->getUrlByRoute('badiu.moodle.mreport.schedulertasksite.index',$paramlink);
		
		}
		else{
			$paramlink=array('functionalitykey'=>$page->getKey()); 
			$sclink=$utilapp->getUrlByRoute('badiu.system.scheduler.taskreport.index',$paramlink);
		}
        
        $linkchedulertask="<div class=\"col\"><a  class=\"form-control btn btn-outline-primary\" href=\"$sclink\"> Relat&oacute;rios que j&aacute; foram agenados($countschedulertask) </a></div>";
    }
	
   $linkchedulertaskadd="";	
	if($haspermissioncheduleradd){
		$linkchedulertaskadd="<div class=\"col\"><a   class=\"form-control btn btn-outline-primary\" href=\"#\" @click=\"changeStatusScheduleformcontroladdnew(1)\">Agendar este relat&oacute;rio</a></div>";
	}
	
	$linkexport="";
	if($counrecord >0){
		$linkexport="<div class=\"col\"><a   class=\"form-control btn btn-outline-primary\"  href=\"$urltoextport\">Exportar relat&oacute;rio para excel</a></div>";
	}
	$space="";
    if(empty($linkchedulertask) && $linkchedulertaskadd){$space="<div class=\"col\"></div>";}
    
	$issuperuser=false;
	$rmoperation = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_operation');
	if($rmoperation=='remove'){$rmoperation=1;}
	else {$rmoperation=1;}
	
	$deletectr=1; // 1 - show delete optino | 2- Show restore option | 3 - Dont show any option
	$filterparam=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
	$filterdelete=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('deleted');
	
	if (array_key_exists('deleted',$filterparam)) {
		if($filterdelete=="0"){$deletectr=1;}
		else if($filterdelete==1){$deletectr=2;}
		else {$deletectr=3;}
	}
	
	
	
?> 
<div id="badiu-report-result">
<?php

$badiuSession = $container->get('badiu.system.access.session');
$headmessegainfo = $badiuSession->getValue($currentroute.'.headmessageinfo');
if(!empty($headmessegainfo)){$headmessegainfo="<div class=\"alert alert-info\" role=\"alert\">$headmessegainfo</div>";}

//echo $headmessegainfo;

?>
<div class="card">
    <div class="card-header">Relat&oacute;rio</div>
    <div class="card-body">
    <div class="row">
                        <div class="col"><h5>Resultado: <?php echo $infoResult; ?></h5> </div>
                         <?php echo $linkexport;?>
						<?php echo $linkchedulertaskadd;?>
                        <?php echo $linkchedulertask;?>
                     <?php echo $space;?>
                    </div> <br />
        <div class="report-container mb40"  id="_badiu_theme_base_index_table">
          
 
                
                    
                  
              
	            <!--	<div style="width: 600px; height: 400px;" id="chartTexst1">
                         </div>-->
            <div class="table-window" >

            <table class="table table-condensed table-hover  bootgrid-table">
                <thead>
                    <tr>
                        <th  v-for="(item, index) in tablehead" v-html="item.name"></th>
                    </tr>
                </thead>
                <tbody> 
                    <tr v-for="(itemdata, index) in tabledata">
                       <td v-for="(item, index1) in tablehead">
                            <div  v-if="item.key === '_ctrl'"> 
                         <?php if($canedit){?>
                                <a v-bind:href="urledit+itemdata.id.value+'<?php echo $editparentidparam; ?>'" title="<?php echo $alticonedit; ?>"><?php echo $imgiconedit;?> </a>
                                <a v-bind:href="urlcopy+itemdata.id.value" title="<?php echo $alticoncopy; ?>"><?php echo $imgiconcopy; ?></a>
                               <?php if($deletectr==1){?> <a href="#" @click="showModalToDelete(itemdata,'delete')" title="<?php echo $alticondelete; ?>"><?php echo $imgicondelete; ?></a> <?php }?>
							   <?php if($deletectr==2){?> <a href="#" @click="showModalToDelete(itemdata,'restore')" title="<?php echo $alticonrestore; ?>"><?php echo $imgiconrestore; ?></a> <?php }?>
								
								
                                 <?php  if ($rmoperation==1){?><a  href="#" @click="showModalToDelete(itemdata,'remove')" title="<?php echo $alticonremove; ?>"></i><?php echo $imgiconremove; ?></a> <?php }?>
						 <?php }?>
                            </div>
							<div  v-if="item.key === '_ctrpclm'"> 
								<div>
								<span v-html="itemdata[item.key].value" > </span>
										<a @click="showModalToDelete(itemdata,'delete')"><?php echo $imgicondelete; ?></a>
										<?php  if ($rmoperation==1){?><a @click="showModalToDelete(itemdata,'remove')"><?php echo $imgiconremove; ?></a> <?php }?>
								</div> 
							 </div>
                            <div  v-if="item.key !== '_ctrl' && item.key !== '_ctrpclm'"> 
								<div  v-if="item.type == ''" v-html="itemdata[item.key].value"></div>
								<div  v-if="item.type == 'choice'">
									<span v-if="!itemdata[item.key].crtedit" @click="tableColumnItemChoiceEnableEdit(item.key,itemdata)" v-html="itemdata[item.key].value"> </span>
									<a v-if="!itemdata[item.key].crtedit"  @click="tableColumnItemChoiceEnableEdit(item.key,itemdata)"><i class="fa fa-edit"></i></a>  
								
									<select  v-if="itemdata[item.key].crtedit"   class="form-control"    v-model="itemdata[item.key].value"  @change="tableColumnItemChoiceChange(item.key,itemdata)"   options="badiuformoptionstablecolumn[item.key]" >
										<option v-for="item in badiuformoptionstablecolumn[item.key]" :value="item.value">{{item.text }}</option> 
									</select>
								</div>
								
								<div  v-if="item.type == 'text'">
									<span v-if="!itemdata[item.key].crtedit" @click="tableColumnItemTextEnableEdit(item.key,itemdata)" v-html="itemdata[item.key].value"> </span>
									<a v-if="!itemdata[item.key].crtedit"  @click="tableColumnItemTextEnableEdit(item.key,itemdata)"><i class="fa fa-edit"></i></a>  
									 <input type="text" v-if="itemdata[item.key].crtedit"  v-model="itemdata[item.key].value" @change="tableColumnItemTextChange(item.key,itemdata)">
								</div>
								
                            </div>
							
                        </td> 
                    </tr>
                </tbody> 
            </table>

       </div> <!--and  table-window-->
        <?php echo $pagingout; ?><!-- defined in /Twig/BadiuDefaultReportExtension.php line 90 -->




  


    
    
    <!-- start modal schedule-->
     <?php 
        //$scheduleurl=$utilapp->getFilePath($page->getConfig()->getFileprocessindexscheduler());
      // if($page->getViewsytemtype()=='bootstrap2'){ include_once("Scheduler/HtmlContentBootstrap2.php"); }
	  // else if($page->getViewsytemtype()=='bootstrap4'){ include_once("Scheduler/HtmlContentBootstrap4.php"); }
    ?>
    <!--end modal schedule-->
    
</div> <!-- and _badiu_theme_base_index_table -->
</div> <!-- and card-body -->
</div> <!-- and card -->
</div> <!--  badiu-report-result -->

<?php 
$htmlcontentportionafterurl=$utilapp->getFilePath($page->getConfig()->getFileprocessindexhtmlcontentportionafter());
    if(file_exists($htmlcontentportionafterurl)){require_once($htmlcontentportionafterurl);}
?>

    <!-- start modal delete /remove-->
<div id="_badiu_theme_base_modal_delete_ctrmodal" style="visibility:hidden">
    <div id="_badiu_theme_base_modal_delete"  v-bind:class="[showmodalcrtclass]" v-bind:style="{ display: showmodalcrt }"  role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Remover item</h5>
                    <button type="button" class="close" v-on:click="showmodalcrt = 'none'" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
				{{this.operationmessage}}
                   
                   <div v-if="itemsected"> <br />Id: <span v-if="itemsected.id" v-html="itemsected.id.value"> </span><br /></div>
				   <div v-if="itemsected"> <br /> <span v-if="itemsected.name" v-html="itemsected.name.value"> </span><br /></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" @click="processDeleteService()">{{this.operationbuttonlabel}}</button> 
                    <button type="button" class="btn btn-secondary" v-on:click="showmodalcrt = 'none'"><?php echo $translator->trans('badiu.system.action.cancel')?></button>
                </div>
            </div>
        </div>
    </div>
</div>
    <!--end modal  delete /remove-->
	