
<?php
$formfieldto = $container->get('badiu.system.core.lib.form.field');
$formfactoryhtmllib = $container->get('badiu.theme.core.lib.template.vuejs.formfactoryhtml');
$utilstring=$this->getContainer()->get('badiu.system.core.lib.util.string');
$formconfig = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig');

$formgroup = $utildata->getVaueOfArray($formconfig, 'formgroup');
$formfields = $utildata->getVaueOfArray($formconfig, 'formfields');

//form with fieldset
function formWithFieldsetSearch($container, $page) {
    $viewsytemtype='bootstrap4'; 
    //$viewsytemtype=$page->getViewsytemtype();
    $formfieldto = $container->get('badiu.system.core.lib.form.field');
    $formfactoryhtmllib = $container->get('badiu.theme.core.lib.template.vuejs.formfactoryhtml');
    $formfactoryhtmllib->setViewsytemtype($viewsytemtype);
    $utildata = $container->get('badiu.system.core.lib.util.data');

    $formconfig = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig');

    $formgroup = $utildata->getVaueOfArray($formconfig, 'formgroup');
    $formfields = $utildata->getVaueOfArray($formconfig, 'formfields');
    $formcontent = "";
 
    $contfield = 0;
    $contgroup = 0;
    $csscontainer='';
    if($viewsytemtype=='bootstrap2'){$csscontainer='class="container-fluid"';}      
    $formcontent .= "<div  $csscontainer id=\"_badiu_theme_base_form_scheduler_vuejs\">";
  //  $formcontent .= "<form>";
    
    
                 
    $firstelement=null;
    
    if(empty($formgroup) || !is_array($formgroup)){return null;}
    foreach ($formgroup as $fgroup) {

        $groupnameoriginal = $utildata->getVaueOfArray($fgroup, 'name');
        $groupname = $utildata->getVaueOfArray($fgroup, 'name');
        $grouplabel = $utildata->getVaueOfArray($fgroup, 'label');
        $groupname =str_replace(".","-",$groupname);
        
        $openfieldset="";
        if($contgroup > 0){
              if($contgroup==1){
                 $openfieldset=" in ";
              }else{ $openfieldset="";}
               $formcontent .= "<fieldset>"; // x1 start fieldset
                $formcontent .= "<legend>";
                $formcontent .= "<a data-toggle=\"collapse\" href=\"#$groupname\" ><span class=\"glyphicon glyphicon-triangle-right\"></span>$grouplabel</a>";
                $formcontent .= " </legend>";
                $formcontent .= "<div class=\"collapse $openfieldset\" id=\"$groupname\">";  //x2 start fieldset content
   
        }
        foreach ($formfields as $field) {
            $fgroup = $utildata->getVaueOfArray($field, 'group');
            
            if ($contfield == 0) {
                $formfieldto->init($field);
                $firstelement=$formfactoryhtmllib->get($formfieldto);
           
            }else if ($contgroup > 0 && $fgroup == $groupnameoriginal) {
                $formfieldto->init($field);
                $element = $formfactoryhtmllib->get($formfieldto);
                $cssclass='class=" row form-group"';
                 if($viewsytemtype=='bootstrap2'){$cssclass= ' ';}  
                 if(!empty($firstelement)){
                      $formcontent .= "<div $cssclass >";
                      $formcontent .= $firstelement;
                      $formcontent .= "</div>";
                      $firstelement=null;
                 }
                 $formcontent .= "<div $cssclass >";
                 $formcontent .= $element;
                 $formcontent .= "</div>";
                
               
            }
          $contfield++;
        }
         if($contgroup > 0){
                $formcontent .= "</div>"; //x2 send fieldset content
                $formcontent .= "</fieldset>";// x1 end fieldset
             //   $formcontent .= "</div>";  //x0 end block
         }
       

        $contgroup++;
    }
     //booton search
    $translator = $container->get('translator');
     $buttonlabelsavefinish=$translator->trans('badiu.system.scheduler.report.savefinish');
     $buttonlabelback=$translator->trans('badiu.system.scheduler.report.back');
     $buttonlabelcancel=$translator->trans('badiu.system.scheduler.report.cancel');
     $iconprocess=iconProcess($container);
      $formcontent.=" <div class=\"col-md-12\">";
      $formcontent.=" <fieldset>";
      $formcontent.="<button @click=\"processTabScheduleParamFilter()\"  class=\"btn btn-primary\" style=\"margin-top: 10px; margin-bottom:7px;\">$buttonlabelsavefinish</button>$iconprocess";
      $formcontent.="<button @click=\"processTabScheduleBack()\"  class=\"btn btn-primary\" style=\"margin-top: 10px; margin-bottom:7px;\">$buttonlabelback</button>";
      $formcontent.="<button @click=\"processTabScheduleCancel()\"  class=\"btn btn-secondary\" style=\"margin-top: 10px; margin-bottom:7px;\">$buttonlabelcancel</button>";
      $formcontent.="  </fieldset>";
      $formcontent.="</div>";
     
    
       //@click=\"processTabScheduleParamFilter()\"
      //$formcontent.="</form>";
      $formcontent.="</div>";//end  "<div id=\"_badiu_theme_base_form_vuejs\">";
    return $formcontent;
}

/*echo "<pre>";
  print_r($formconfig);
  echo "</pre>";exit; */

echo formWithFieldsetSearch($container, $page); 



?>
 