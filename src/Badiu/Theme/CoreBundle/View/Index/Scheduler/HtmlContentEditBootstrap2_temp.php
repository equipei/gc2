<div>
       
          
                
				<div v-if="scheduleformcontrol.haserror"  v-html="scheduleformcontrol.message" class="alert alert-danger">
							
				</div>
                 <form role="form">
				 
				  <fieldset  class="form-group">
					<legend> <a data-toggle="collapse" href="#schedulegeneral" aria-expanded="false"><?php echo $translator->trans('badiu.system.scheduler.task.general'); ?></a></legend>
					
					<div class="collapse in" id="schedulegeneral" aria-expanded="true">
						 <div class="row-fluid">
                           <div class="span12">
                               <label  class="control-label" for="badiusystemschedule_name"><?php echo $translator->trans('badiu.system.scheduler.report.name'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="span12">
                               <input type="text" class="form-control span" id="badiusystemschedule_name"  placeholder="<?php echo $translator->trans('badiu.system.scheduler.report.name.placeholder'); ?>" v-model="scheduleform.name" name="badiusystemschedule_name">
                           </div>
                       </div>
					   
					    <div class="row-fluid">
                           <div class="span12">
                               <label  class="control-label" for="badiusystemschedule_status"><?php echo $translator->trans('badiu.system.scheduler.task.status'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="span12">
                               <select   class="form-control span12" id="badiusystemschedule_status"  v-model="scheduleform.status"  options="scheduleformoptions.status"  name="badiusystemschedule_status">
                                    <option v-for="item in scheduleformoptions.status" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>
                       </div>
					   
					   <div class="row-fluid">
                           <div class="span12">
                                <label for="badiusystemschedule_reportaction"><?php echo $translator->trans('badiu.system.scheduler.report.action'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="span12">
                               <select   class="form-control span12" id="badiusystemschedule_reportaction"  v-model="scheduleform.reportaction"  options="scheduleformoptions.reportaction"  name="badiusystemschedule_reportaction">
                                    <option v-for="item in scheduleformoptions.reportaction" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>  
                       </div>
					   
					    <div class="row-fluid">
                           <div class="span12">
                                <label for="badiusystemschedule_reportmessage_sendonlyreportwithrecord"><?php echo $translator->trans('badiu.system.scheduler.report.onlysendwithrecord'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="span12">
                               <select   class="form-control span12" id="badiusystemschedule_reportmessage_sendonlyreportwithrecord"  v-model="scheduleform.reportmessage.sendonlyreportwithrecord"  options="scheduleformoptions.defaulboolean"  name="badiusystemschedule_reportmessage_sendonlyreportwithrecord">
                                    <option v-for="item in scheduleformoptions.defaulboolean" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>  
                       </div>
					</div>
				  </fieldset>
				   
				 <fieldset  class="form-group">
				 <legend> <a data-toggle="collapse" href="#mailmessage" aria-expanded="false"><?php echo $translator->trans('badiu.system.scheduler.task.mailmsg'); ?></a></legend>
				<div class="collapse" id="mailmessage" aria-expanded="false">
                                     
                     
                     
                     <div class="row-fluid">
                           <div class="span12">
                              <label  v-if="scheduleform.reportaction=='send.report'"  for="badiusystemschedule_messagerecipientto"><?php echo $translator->trans('badiu.system.scheduler.report.msgto'); ?><i class="fa fa-asterisk text-danger"></i></label>
							  <label  v-if="scheduleform.reportaction=='send.menssage.touserinreport'"  for="badiusystemschedule_messagerecipientto"><?php echo $translator->trans('badiu.system.scheduler.report.msgtocopy'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="span12">
                               <input type="text" class="form-control span12" id="badiusystemschedule_messagerecipientto"  placeholder="<?php echo $translator->trans('badiu.system.scheduler.report.msgto.placeholder'); ?>" v-model="scheduleform.reportmessage.recipient.to" name="badiusystemschedule_messagerecipientto">
                           </div>
                       </div>
					
					   
                    <div class="row-fluid">
                           <div class="span12">
                              <label for="badiusystemschedule_messagesubject"><?php echo $translator->trans('badiu.system.scheduler.report.msgsubject'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="span12">
                               <input type="text" class="form-control span12" id="badiusystemschedule_messagesubject"   placeholder="<?php echo $translator->trans('badiu.system.scheduler.report.msgsubject.placeholder'); ?>"  v-model="scheduleform.reportmessage.message.subject" name="badiusystemschedule_messagesubject">
                           </div>
                       </div>
                   
                    <div class="form-group">
                        <label for="badiusystemschedule_messagebody"><?php echo $translator->trans('badiu.system.scheduler.report.msg'); ?></label> 
                         <div>
						  <vue-tinymce class="form-control"  id="badiusystemschedule_messagebody" v-model="scheduleform.reportmessage.message.body" toolbar=" fullscreen code | fontselect fontsizeselect formatselect | bold italic forecolor backcolor | link image | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |insertfile undo redo "></vue-tinymce>
						   <input type="file" ref="badiufileup"  style="display: none;" id="badiutextediorfileupload">
                         </div>
                        
                   </div> 
				   <?php 
						$badiuSession = $container->get('badiu.system.access.session');
						$formshowsmtpservice=$badiuSession->getValue('_badiu.moodle.mreport.scheduler.formshowsmtpservice');
				     if($formshowsmtpservice){?>
				   
				   <div class="row-fluid">
                           <div class="span12">
                              <label for="badiusystemschedule_smtpservice"><?php echo $translator->trans('badiu.system.scheduler.smtpservice'); ?></label>
                           </div>
                           <div class="span12">
                               <select   class="form-control" id="badiusystemschedule_reportmessage_smtpservice"  v-model="scheduleform.reportmessage.smtpservice"  options="scheduleformoptions.smtpservice"  name="badiusystemschedule_reportmessage_smtpservice">
                                    <option v-for="item in scheduleformoptions.smtpservice" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>
                       </div>
				   <?php }?>
				   </div>
                     </fieldset>
					 
					  <fieldset  class="form-group">
						<legend><legend> <a data-toggle="collapse" href="#scheduleexec" aria-expanded="false" ><?php echo $translator->trans('badiu.system.scheduler.task.routineexec'); ?></a></legend>
						 <div class="collapse" id="scheduleexec" aria-expanded="false">
						 
						<div class="form-group">
                              <label for="badiusystemschedule_timeroutine"><?php echo $translator->trans('badiu.system.scheduler.task.timeroutine'); ?><i class="fa fa-asterisk text-danger"></i></label>
                       
							 <select   class="form-control span12" id="badiusystemschedule_timeroutine_options"  v-model="scheduleform.timeroutine"  options="scheduleformoptions.timeroutine"  name="badiusystemschedule_timeroutine">
                                    <option v-for="item in scheduleformoptions.timeroutine" :value="item.value">{{item.text }}</option>
                                </select>
							 </div>
					 <!-- system_daily -->
					 
					  <div id="group_badiusystemschedule_timeroutine_system_daily">
                          <div class="row-fluid">
                             
							 <div class="span6"> 
							 <select   v-if="scheduleform.timeroutine=='system_daily'"  class="form-control span12" id="badiusystemschedule_timeroutineparamy_day_hour"  v-model="scheduleform.timeroutineparam.x"  options="scheduleformoptions.hour"  name="badiusystemschedule_timeroutineparamw_day_hour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="span6"> 
							 <select   v-if="scheduleform.timeroutine=='system_daily'"  class="form-control span12" id="badiusystemschedule_timeroutineparamy_day_minute"  v-model="scheduleform.timeroutineparam.y"  options="scheduleformoptions.minute"  name="badiusystemschedule_timeroutineparamw_day_minute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                         </div>
                   </div>	
                   <!-- system_timeinterval -->
                     <div   id="group_badiusystemschedule_timeroutine_system_timeinterval">
                         <div class="row-fluid">
                           
                             <div class="span6">
                                <input v-if="scheduleform.timeroutine=='system_timeinterval'"  type="text" class="form-control span12" id="badiusystemschedule_timeroutineparamx"  v-model="scheduleform.timeroutineparam.x" name="badiusystemschedule_timeroutineparamx">
                             </div>
                             <div class="span6">
                                <select  v-if="scheduleform.timeroutine=='system_timeinterval'" class="form-control span12" id="badiusystemschedule_timeroutineparamy"  v-model="scheduleform.timeroutineparam.y"  options="scheduleformoptions.unittime"  name="badiusystemschedule_timeroutineparamy">
                                    <option v-for="item in scheduleformoptions.unittime" :value="item.value">{{item.text }}</option>
                                </select>
                             </div>
                         </div>
                   </div>
                   
				    <!-- system_weekly -->
					 
					  <div id="group_badiusystemschedule_timeroutine_system_weekly">
                          <div class="row-fluid">
                             
							 
                             <div class="span4">
								<select   v-if="scheduleform.timeroutine=='system_weekly'"  class="form-control span12" id="badiusystemschedule_timeroutine_day_of_week"  v-model="scheduleform.timeroutineparam.x"  options="scheduleformoptions.daysofweek"  name="badiusystemschedule_timeroutine_day_of_week">
                                    <option v-for="item in scheduleformoptions.daysofweek" :value="item.value">{{item.text }}</option>
                                </select>
                             </div>
							  <div class="span4"> 
							 <select   v-if="scheduleform.timeroutine=='system_weekly'"  class="form-control span12" id="badiusystemschedule_timeroutineparamy_day_of_week_hour"  v-model="scheduleform.timeroutineparam.y"  options="scheduleformoptions.hour"  name="badiusystemschedule_timeroutineparamw_day_of_week_hour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="span4"> 
							 <select   v-if="scheduleform.timeroutine=='system_weekly'"  class="form-control span12" id="badiusystemschedule_timeroutineparamy_day_of_week_minute"  v-model="scheduleform.timeroutineparam.z"  options="scheduleformoptions.minute"  name="badiusystemschedule_timeroutineparamw_day_of_week_minute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                         </div>
                   </div>
				   
				   <!-- system_monthly -->
					 
					  <div id="group_badiusystemschedule_timeroutine_system_monthly">
                          <div class="row-fluid">
                             
							 
                             <div class="span4">
								<select   v-if="scheduleform.timeroutine=='system_monthly'"  class="form-control span12" id="badiusystemschedule_timeroutine_day_of_month"  v-model="scheduleform.timeroutineparam.x"  options="scheduleformoptions.daysofmonth"  name="badiusystemschedule_timeroutine_day_of_month">
                                    <option v-for="item in scheduleformoptions.daysofmonth" :value="item.value">{{item.text }}</option>
                                </select>
                             </div>
							  <div class="span4"> 
							 <select   v-if="scheduleform.timeroutine=='system_monthly'"  class="form-control span12" id="badiusystemschedule_timeroutineparamy_day_of_month_hour"  v-model="scheduleform.timeroutineparam.y"  options="scheduleformoptions.hour"  name="badiusystemschedule_timeroutineparamw_day_of_month_hour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="span4"> 
							 <select   v-if="scheduleform.timeroutine=='system_monthly'"  class="form-control span12" id="badiusystemschedule_timeroutineparamy_day_of_month_minute"  v-model="scheduleform.timeroutineparam.z"  options="scheduleformoptions.minute"  name="badiusystemschedule_timeroutineparamw_day_of_month_minute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                         </div>
                   </div>
				    <!-- once -->
					 
					  <div  id="group_badiusystemschedule_timeroutine_once">
                          <div class="row-fluid">
                             
							 
                             <div class="span4">
                                <vuejs-datepicker class="form-control" :format="defaultDatepickerFormat"  id="badiusystemschedule_timeexeconce"  v-model="scheduleform.timeexeconce" name="badiusystemschedule_timeexeconce" ></vuejs-datepicker>
                             </div>
                              
							   <div class="span4"> 
							 <select class="form-control" id="badiusystemschedule_timeexeconcehour"  v-model="scheduleform.timeexeconcehour"  options="scheduleformoptions.hour"  name="badiusystemschedule_timeexeconcehour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="span4"> 
							 <select   class="form-control" id="badiusystemschedule_timeexeconceminute"  v-model="scheduleform.timeexeconceminute"  options="scheduleformoptions.minute"  name="badiusystemschedule_timeexeconceminute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                         </div>
                   </div>
				   
                   <div  id="group_badiusystemschedule_timeroutine_periodexec" class="form-group">
                        <label for="badiusystemschedule_periodexec"><?php echo $translator->trans('badiu.system.scheduler.task.periodexec'); ?></label>
                         <div class="row-fluid">
                           
                             <div class="span4">
                                <vuejs-datepicker class="form-control span12" id="badiusystemschedule_execperiodstart" placeholder="<?php echo $translator->trans('badiu.system.scheduler.task.execperiodstart'); ?>"  :format="defaultDatepickerFormat" v-model="scheduleform.execperiodstart" name="badiusystemschedule_execperiodstart"></vuejs-datepicker>
                             </div>
                               <div class="span1"> 
							 <select class="form-control" id="badiusystemschedule_execperiodstarthour"  v-model="scheduleform.execperiodstarthour"  options="scheduleformoptions.hour"  name="badiusystemschedule_execperiodstarthour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="span1"> 
							 <select   class="form-control" id="badiusystemschedule_execperiodstartminute"  v-model="scheduleform.execperiodstartminute"  options="scheduleformoptions.minute"  name="badiusystemschedule_execperiodstartminute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div>
							  <div class="span4">
									<vuejs-datepicker class="form-control span12" :format="defaultDatepickerFormat" v-model="scheduleform.execperiodend" id="badiusystemschedule_execperiodend"  placeholder="<?php echo $translator->trans('badiu.system.scheduler.task.execperiodend'); ?>"  name="badiusystemschedule_execperiodend" ></vuejs-datepicker>
									
                             </div>
							  <div class="span1"> 
							 <select class="form-control" id="badiusystemschedule_execperiodendhour"  v-model="scheduleform.execperiodendhour"  options="scheduleformoptions.hour"  name="badiusystemschedule_execperiodendhour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="span1"> 
							 <select   class="form-control" id="badiusystemschedule_execperiodendminute"  v-model="scheduleform.execperiodendminute"  options="scheduleformoptions.minute"  name="badiusystemschedule_execperiodendminute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                         </div> 
                   </div> 
				   </div> 
				    </fieldset>
                 </form>
           
                <div>
                    <button type="button" class="btn btn-primary" @click="processTabSchedule()"><?php echo $translator->trans('badiu.system.scheduler.report.saveforword'); ?></button> <?php echo iconProcess($container);?>
					<button type="button" class="btn btn-primary" @click="processTabScheduleForword()"><?php echo $translator->trans('badiu.system.scheduler.report.forword'); ?></button> 
                    <button type="button" class="btn btn-secondary" @click="processTabScheduleCancel()"><?php echo $translator->trans('badiu.system.scheduler.report.cancel'); ?></button>
                </div>
          
        
    </div>

