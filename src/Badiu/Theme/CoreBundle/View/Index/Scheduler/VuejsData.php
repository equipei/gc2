	<?php  
	$sdto=getSchedulerData($container); 
	addSessionClientPluginAccess($container,$json,$utildata,$page);
	$djson = $container->get('badiu.system.core.lib.util.json');
	$badiuSession = $container->get('badiu.system.access.session');
	$badiuSession->setHashkey($page->getSessionhashkey());

	?>
	scheduleformcontrol: {status: "open", message: "", haserror: false,processing: false}, 
	scheduleformcontroladdnew: 0, 
	scheduletabcontrol: "schedulertab", // scheduletab | reportertab
	scheduleformeditfilter: {},
	scheduleform: {
                    _service: 'badiu.system.scheduler.task.lib.report',
                    _function: '<?php echo $sdto->function; ?>',
					_urlgoback: '<?php echo getUrlgoback($container,$page); ?>',
					<?php echo getTokenParam($container,$page);?>
					<?php echo getIdParam($sdto);?>
					reportfilter: {'config': 
											{
											'router': '<?php echo $currentroute;?>'},
											'filter': <?php echo $currentparamfilter;?> 
											<?php if(!empty($page->getConfig()->getSchedulermessageemailcolumn())){?> 
												,reportcolumnemail: '<?php echo $page->getConfig()->getSchedulermessageemailcolumn(); ?>'
											<?php }?>
								},
                    name: '<?php echo $djson->escape($sdto->name); ?>',
					status: '<?php echo $sdto->status; ?>',
					tcron: '<?php echo $sdto->tcron; ?>',
					lcron: '<?php echo $sdto->lcron; ?>',
                    reportaction: '<?php echo $sdto->reportaction; ?>',
					reportmessage: {
                        sendonlyreportwithrecord:  '<?php echo $sdto->sendonlyreportwithrecord; ?>',
						smtpservice: '<?php echo $sdto->smtpservice; ?>',
						recipient: {'to': '<?php echo $sdto->reportmessagerecipientto; ?>','sysuser':'<?php echo $sdto->reportmessagerecipientsysuser; ?>'},
						message: {
							subject: '<?php echo $djson->escape($sdto->reportmessagemessagesubject); ?>',
							body: '<?php echo $djson->escape($sdto->reportmessagemessagebody); ?>' 
						},
						delivery: {
							chanel: [<?php echo $sdto->reportmessagedeliverychanel; ?>],
							service: {
								email: '',
								moodlemessage: 'badiu.moodle.core.lib.message|sendFromReportScheduler',
								webservice: '<?php echo $sdto->reportmessagedeliverywebservicekey; ?>'
							}
						},
						
                    },
					exporttodatabase: {
						type: '<?php echo $sdto->exporttodbtype; ?>',//legacy | incremental
						targettype: '<?php echo $sdto->exporttodbtargettype; ?>',
						maxrowperconnection: '<?php echo $sdto->exporttodbmaxrowperconnection; ?>',
						maxrowperprocess: '<?php echo $sdto->exporttodbmaxrowperprocess; ?>',
						otherconfig: '<?php echo $sdto->exporttodbotherconfig; ?>',
						targetconfig: {
							method: '<?php echo $sdto->exporttodbtargetconfigmethod; ?>',
							service: '<?php echo $sdto->exporttodbtargetconfigservice; ?>',
							table: '<?php echo $sdto->exporttodbtargetconfigtable; ?>',
							synckey: '<?php echo $sdto->exporttodbtargetconfigsynckey; ?>',
							systeminstance: '<?php echo $sdto->exporttodbtargetsysteminstance; ?>',
						},
						
					},
					moodlebackuprestorecourse: {
						service: '<?php echo $sdto->moodlebackuprestorecourseservice; ?>',
						moodleidtarget: '<?php echo $sdto->moodlebackuprestorecoursemoodleidtarget; ?>',
						coursecategorytarget: '<?php echo $sdto->moodlebackuprestorecoursecoursecategorytarget; ?>',
						includeuserinteraction: '<?php echo $sdto->moodlebackuprestorecourseincludeuserinteraction; ?>',
						setcoursetimestart: '<?php echo $sdto->moodlebackuprestorecoursesetcoursetimestart; ?>',
							
					}, 
					timeroutine: '<?php echo $sdto->timeroutine; ?>', 
					timeroutineparam: {
						x: '<?php echo $sdto->timeroutineparamx; ?>', 
						y: '<?php echo $sdto->timeroutineparamy; ?>',
						z: '<?php echo $sdto->timeroutineparamz; ?>'
					},
					timeexeconce: '<?php echo $sdto->timeexeconce; ?>',
					timeexeconcehour: '<?php echo $sdto->timeexeconcehour; ?>',
					timeexeconceminute: '<?php echo $sdto->timeexeconceminute; ?>',
					execperiodstart: '<?php echo $sdto->execperiodstart; ?>', 
					execperiodstarthour: '<?php echo $sdto->execperiodstarthour; ?>', 
					execperiodstartminute: '<?php echo $sdto->execperiodstartminute; ?>', 
					execperiodend: '<?php echo $sdto->execperiodend; ?>',
					execperiodendhour: '<?php echo $sdto->execperiodendhour; ?>',
					execperiodendminute: '<?php echo $sdto->execperiodendminute; ?>',
					
					calendar: {
						key: '<?php echo $sdto->calendarkey; ?>',
						timetype: '<?php echo $sdto->calendartimetype; ?>',
					},
                   
                }, 
                scheduleformoptions: {
                    reportaction: [
						{value: 'send.report', text: '<?php echo $translator->trans('badiu.system.scheduler.report.sendreport'); ?>'}
					   <?php if(!empty($page->getConfig()->getSchedulermessageemailcolumn())){?>	
							,{value: 'send.menssage.touserinreport', text: '<?php echo $translator->trans('badiu.system.scheduler.report.sendmenssagetouserinreport'); ?>'} 
					   <?php }?>  
					
					   <?php if($badiuSession->getValue('badiu.system.core.param.config.scheduler.enabledatabaseexport')){?>	
							,{value: 'export.todatabase', text: '<?php echo $translator->trans('badiu.system.scheduler.action.export.todatabase'); ?>'} 
					   <?php }?> 

					    <?php if($badiuSession->getValue('badiu.moodle.mreport.param.config.scheduler.enablecoursebackuprestore')){?>	
							,{value: 'moodle.mreport.backuprestorecourse', text: '<?php echo $translator->trans('badiu.moodle.mreport.scheduler.action.enablecoursebackuprestore'); ?>'} 
					   <?php }?>   
					],
					status: [
						{value: 'active', text: '<?php echo $translator->trans('badiu.system.scheduler.task.status.active'); ?>'},
						{value: 'inactive', text: '<?php echo $translator->trans('badiu.system.scheduler.task.status.inactive'); ?>'}
						
					],
					defaulboolean: [
						{value: '0', text: '<?php echo $translator->trans('no'); ?>'},
						{value: '1', text: '<?php echo $translator->trans('yes'); ?>'}
						
					], 

					smtpservice: [
						{value: 'badiunetconfig', text: '<?php echo $translator->trans('badiu.system.scheduler.smtpservice.badiunetconfig'); ?>'},
						{value: 'moodleconfig', text: '<?php echo $translator->trans('badiu.system.scheduler.smtpservice.moodleconfig'); ?>'}
						
					], 
					<?php
						$systemrecipient=$container->get('badiu.system.scheduler.task.systemrecipient');
						$systemrecipient->setSessionhashkey($page->getSessionhashkey());
						$systemrecipient->setPconfig($page->getConfig()->getSchedulersystemrecipient());
						echo $systemrecipient->getFormOptions(false);
					 ?>
					exporttodatabasetype: [
						{value: 'incremental', text: '<?php echo $translator->trans('badiu.system.scheduler.action.export.todatabase.incremental'); ?>'},
						{value: 'legacy', text: '<?php echo $translator->trans('badiu.system.scheduler.action.export.todatabase.legacy'); ?>'}
						
					],
					<?php  
					$formfactorycomponent=$container->get('badiu.theme.core.lib.template.vuejs.formfactorycomponent');
					$formfactorycomponent->setSessionhashkey($page->getSessionhashkey());
					echo $formfactorycomponent->makeChoiceList('sitemoodle', 'badiu.moodle.core.formdataoptions','getMoodle',false);
					
					echo $formfactorycomponent->makeChoiceList('calendarkey', 'badiu.system.scheduler.task.form.dataoptions','getCalendar',false);
					echo $formfactorycomponent->makeChoiceList('tcron', 'badiu.system.scheduler.task.form.dataoptions','getTcron',false);
					echo $formfactorycomponent->makeChoiceList('lcron', 'badiu.system.scheduler.task.form.dataoptions','getLcron',false);
					echo $formfactorycomponent->makeChoiceList('menssagechaneldelivery', 'badiu.system.scheduler.task.form.dataoptions','getChanelDelivery',false);
					?>
					calendartimetype: [
						{value: '',text: '---'},
						{value: 'onlyworkingdays', text: '<?php echo $translator->trans('badiu.system.scheduler.calendartimetype.onlyworkingdays'); ?>'},
						{value: 'onlyworkingdaysandhours', text: '<?php echo $translator->trans('badiu.system.scheduler.calendartimetype.onlyworkingdaysandhours'); ?>'}
						
					],
					timeroutine: [
						{value: 'system_daily', text: '<?php echo $translator->trans('badiu.system.time.period.daily'); ?>'},
						{value: 'system_weekly', text: '<?php echo $translator->trans('badiu.system.time.period.weekly'); ?>'},
						{value: 'system_monthly', text: '<?php echo $translator->trans('badiu.system.time.period.monthly'); ?>'},
						{value: 'system_timeinterval', text: '<?php echo $translator->trans('badiu.system.time.period.interval'); ?>'},
						{value: 'once', text: '<?php echo $translator->trans('badiu.system.time.period.once'); ?>'}
					],
				    hour: [
						{value: '0', text: '00 h'},
						{value: '1', text: '01 h'},
						{value: '2', text: '02 h'},
						{value: '3', text: '03 h'},
						{value: '4', text: '04 h'},
						{value: '5', text: '05 h'},
						{value: '6', text: '06 h'},
						{value: '7', text: '07 h'},
						{value: '8', text: '08 h'},
						{value: '9', text: '09 h'},
						{value: '10', text: '10 h'},
						{value: '11', text: '11 h'},
						{value: '12', text: '12 h'},
						{value: '13', text: '13 h'},
						{value: '14', text: '14 h'},
						{value: '15', text: '15 h'},
						{value: '16', text: '16 h'},
						{value: '17', text: '17 h'},
						{value: '18', text: '18 h'},
						{value: '19', text: '19 h'},
						{value: '20', text: '20 h'},
						{value: '21', text: '21 h'},
						{value: '22', text: '22 h'},
						{value: '23', text: '23 h'}						
					],
					minute: [
						{value: '0', text: '00 min'},
						{value: '1', text: '01 min'},
						{value: '2', text: '02 min'},
						{value: '3', text: '03 min'},
						{value: '4', text: '04 min'},
						{value: '5', text: '05 min'},
						{value: '6', text: '06 min'},
						{value: '7', text: '07 min'},
						{value: '8', text: '08 min'},
						{value: '9', text: '09 min'},
						{value: '10', text: '10 min'},
						{value: '11', text: '11 min'},
						{value: '12', text: '12 min'},
						{value: '13', text: '13 min'},
						{value: '14', text: '14 min'},
						{value: '15', text: '15 min'},
						{value: '16', text: '16 min'},
						{value: '17', text: '17 min'},
						{value: '18', text: '18 min'},
						{value: '19', text: '19 min'},
						{value: '20', text: '20 min'},
						{value: '21', text: '21 min'},
						{value: '22', text: '22 min'},
						{value: '23', text: '23 min'},
						{value: '24', text: '24 min'},						
						{value: '25', text: '25 min'},
						{value: '26', text: '26 min'},
						{value: '27', text: '27 min'},
						{value: '28', text: '28 min'},
						{value: '29', text: '29 min'},
						{value: '30', text: '30 min'},
						{value: '41', text: '41 min'},
						{value: '32', text: '32 min'},
						{value: '33', text: '33 min'},
						{value: '34', text: '34 min'},						
						{value: '35', text: '35 min'},
						{value: '36', text: '36 min'},
						{value: '37', text: '37 min'},
						{value: '38', text: '38 min'},
						{value: '39', text: '39 min'},
						{value: '40', text: '40 min'},
						{value: '41', text: '41 min'},
						{value: '42', text: '42 min'},
						{value: '43', text: '43 min'},
						{value: '44', text: '44 min'},						
						{value: '45', text: '45 min'},
						{value: '46', text: '46 min'},
						{value: '47', text: '47 min'},
						{value: '48', text: '48 min'},
						{value: '49', text: '49 min'},
						{value: '50', text: '50 min'},
						{value: '51', text: '51 min'},
						{value: '52', text: '52 min'},
						{value: '53', text: '53 min'},
						{value: '54', text: '54 min'},						
						{value: '55', text: '55 min'},
						{value: '56', text: '56 min'},
						{value: '57', text: '57 min'},
						{value: '58', text: '58 min'},
						{value: '59', text: '59 min'}
					],
					unittime: [
						{value: 'system_time_unit_second', text: '<?php echo $translator->trans('badiu.system.time.second'); ?>'},
						{value: 'system_time_unit_minute', text: '<?php echo $translator->trans('badiu.system.time.minute'); ?>'},
						{value: 'system_time_unit_hour', text: '<?php echo $translator->trans('badiu.system.time.hour'); ?>'},
						{value: 'system_time_unit_day', text: '<?php echo $translator->trans('badiu.system.time.day'); ?>'},
						{value: 'system_time_unit_week', text: '<?php echo $translator->trans('badiu.system.time.week'); ?>'},
						{value: 'system_time_unit_month', text: '<?php echo $translator->trans('badiu.system.time.month'); ?>'},
						{value: 'system_time_unit_year', text: '<?php echo $translator->trans('badiu.system.time.year'); ?>'}
					],
					daysofweek: [
						{value: '0', text: '<?php echo $translator->trans('badiu.system.time.month.name.sunday'); ?>'},
						{value: '1', text: '<?php echo $translator->trans('badiu.system.time.month.name.monday'); ?>'},
						{value: '2', text: '<?php echo $translator->trans('badiu.system.time.month.name.tuesday'); ?>'},
						{value: '3', text: '<?php echo $translator->trans('badiu.system.time.month.name.wednesday'); ?>'},
						{value: '4', text: '<?php echo $translator->trans('badiu.system.time.month.name.thursday'); ?>'},
						{value: '5', text: '<?php echo $translator->trans('badiu.system.time.month.name.friday'); ?>'},
						{value: '6', text: '<?php echo $translator->trans('badiu.system.time.month.name.saturday'); ?>'}
					],
					daysofmonth: [
						{value: '1', text: '1'},
						{value: '2', text: '2'},
						{value: '3', text: '3'},
						{value: '4', text: '4'},
						{value: '5', text: '5'},
						{value: '6', text: '6'},
						{value: '7', text: '7'},
						{value: '8', text: '8'},
						{value: '9', text: '9'},
						{value: '10', text: '10'},
						{value: '11', text: '11'},
						{value: '12', text: '12'},
						{value: '13', text: '13'},
						{value: '14', text: '14'},
						{value: '15', text: '15'},
						{value: '16', text: '16'},
						{value: '17', text: '17'},
						{value: '18', text: '18'},
						{value: '19', text: '19'},
						{value: '20', text: '20'},
						{value: '21', text: '21'},
						{value: '22', text: '22'},
						{value: '23', text: '23'},
						{value: '24', text: '24'},						
						{value: '25', text: '25'},
						{value: '26', text: '26'},
						{value: '27', text: '27'},
						{value: '28', text: '28'},
						{value: '29', text: '29'},
						{value: '30', text: '30'},
						{value: '31', text: '31'}
						
					]
                },
<?php 
function getSchedulerData($container) {
	  $datecast=$container->get('badiu.system.core.lib.form.castdate');
	$sdto=new stdClass();
	$sdto->function="save"; 
	$sdto->id=""; 
	$sdto->name=""; 
	$sdto->status="active";
	$sdto->tcron="cron1";
	$sdto->lcron="entity";
	if($container->hasParameter('badiu.system.core.type.instalation')){
		$typeinstalation=$container->getParameter('badiu.system.core.type.instalation');
		if($typeinstalation=='multipleentity'){$sdto->lcron="global";}
	}
	
	$sdto->reportaction="send.report";
	$sdto->sendonlyreportwithrecord=1;
	$sdto->reportmessagerecipientto="";
	$sdto->reportmessagerecipientsysuser="";
	$sdto->reportmessagemessagesubject="";
	$sdto->reportmessagemessagebody="";
	$sdto->reportmessagedeliverychanel='"email"';
	$sdto->reportmessagedeliverywebservicekey="";
	//$sdto->timeroutine='system_timeinterval';
	$sdto->timeroutine='once';
	$sdto->timeroutineparamx=2;
	$sdto->timeroutineparamy='system_time_unit_day';
	$sdto->timeroutineparamz=""; 
	$sdto->timeexeconce=$datecast->convertToForm(new \DateTime());
	$datenow=new \DateTime();
	$sdto->timeexeconcehour=$datenow->format('H')+0;
	$sdto->timeexeconceminute=$datenow->format('i')+0;
	$sdto->execperiodstart="";
	$sdto->execperiodstarthour="";
	$sdto->execperiodstartminute="";
	$sdto->execperiodend="";
	$sdto->execperiodendhour="";
	$sdto->execperiodendminute="";
	$sdto->execperiodstarthour='';
	$sdto->execperiodstartminute='';
	$sdto->execperiodendhour='';
	$sdto->execperiodendminute='';
	
	$sdto->calendarkey='default';
	$sdto->calendartimetype='';
	
	$sdto->smtpservice="badiunetconfig";
	
	//exportodatabase
	$sdto->exporttodbtype='incremental';
	
	$sdto->exporttodbtargettype='moodle';
	$sdto->exporttodbmaxrowperconnection=2500;
	$sdto->exporttodbmaxrowperprocess=50000;
	$sdto->exporttodbotherconfig='';
	$sdto->exporttodbtargetconfigmethod='service';
	$sdto->exporttodbtargetconfigservice='badiu.moodle.mreport.lib.generalexportdbmoodlesummarize';
	$sdto->exporttodbtargetconfigtable='';
	$sdto->exporttodbtargetconfigsynckey='';
	$sdto->exporttodbtargetsysteminstance='';
	
	
	//moodlebackuprestorecourse
	$sdto->moodlebackuprestorecourseservice='badiu.moodle.mreport.lib.manageremotecoursebackuprestore';
	$sdto->moodlebackuprestorecoursemoodleidtarget='';
	$sdto->moodlebackuprestorecoursecoursecategorytarget='';
	$sdto->moodlebackuprestorecourseincludeuserinteraction='';
	$sdto->moodlebackuprestorecoursesetcoursetimestart='';
		
	
    $json = $container->get('badiu.system.core.lib.util.json');
    $utildata = $container->get('badiu.system.core.lib.util.data');
    
	 $schedulerid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_badiuschedulerid');
	 $foperation=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_foperation');
     if(empty($schedulerid)){ return $sdto;}
     $dto=$container->get('badiu.system.scheduler.task.data')->findById($schedulerid);
     
	 $sdto->function="edit"; 
	 if($foperation=='clone'){$sdto->function="addcopy"; }
	 $sdto->id=$schedulerid; 
	 if($foperation=='clone'){$sdto->id=-1; }
	 $sdto->name=$dto->getName();
	 $sdto->status=$dto->getStatus();
	 $sdto->tcron=$dto->getTcron();
	 $sdto->lcron=$dto->getLcron();
	 $sdto->reportaction=$dto->getReportaction();
     
     $reportmessage=$json->decode($dto->getReportmessage(),true);
     
	 $sendonlyreportwithrecord=$utildata->getVaueOfArray($reportmessage,'sendonlyreportwithrecord');
	 if($sendonlyreportwithrecord!=null || $sendonlyreportwithrecord!=''){$sdto->sendonlyreportwithrecord=$sendonlyreportwithrecord;}
	
	 $smtpservice=$utildata->getVaueOfArray($reportmessage,'smtpservice');
	 if(!empty($smtpservice)){$sdto->smtpservice=$smtpservice;}

	
     $to=$utildata->getVaueOfArray($reportmessage,'recipient.to',true);
	 $sdto->reportmessagerecipientto=$to;
	 $sysuser=$utildata->getVaueOfArray($reportmessage,'recipient.sysuser',true);
	 $sdto->reportmessagerecipientsysuser=$sysuser;
    
     $msgsubject=$utildata->getVaueOfArray($reportmessage,'message.subject',true);
     $sdto->reportmessagemessagesubject=$msgsubject;
     
     $msgbody=$utildata->getVaueOfArray($reportmessage,'message.body',true);
     if(!empty($msgbody)){$msgbody=preg_replace("/\r|\n/", "", $msgbody);}
     $sdto->reportmessagemessagebody= $msgbody;
    
	$reportmessagedeliverychanel=$utildata->getVaueOfArray($reportmessage,'delivery.chanel',true);
	$reportmessagedeliverychanel=$utildata->castArrayToString($reportmessagedeliverychanel,',','"');
	if(empty($reportmessagedeliverychanel)){$reportmessagedeliverychanel='"email"';}
	$sdto->reportmessagedeliverychanel=$reportmessagedeliverychanel;

	$sdto->reportmessagedeliverywebservicekey=$utildata->getVaueOfArray($reportmessage,'delivery.service.webservice',true);
    $timeroutine=$dto->getTimeroutine();
	$sdto->timeroutine=$timeroutine;
    $timeroutineparam=$json->decode($dto->getTimeroutineparam(),true);
    if($timeroutine=='system_daily'){
            $x=$utildata->getVaueOfArray($timeroutineparam, 'hour');
            $y=$utildata->getVaueOfArray($timeroutineparam, 'minute');
            $sdto->timeroutineparamx=$x;
		    $sdto->timeroutineparamy=$y;
			
        }else if($timeroutine=='system_timeinterval'){
            $x=$utildata->getVaueOfArray($timeroutineparam, 'value');
            $y=$utildata->getVaueOfArray($timeroutineparam, 'timeunit');
           // $y=$container()->get('badiu.system.core.lib.date.period.form.dataoptions')->getTimeUnitFullKey($y);
		    $y='system_time_unit_'.$y;
            $sdto->timeroutineparamx=$x;
		    $sdto->timeroutineparamy=$y;
			
        }else  if($timeroutine=='system_weekly'){
            $x=$utildata->getVaueOfArray($timeroutineparam, 'day');
            $y=$utildata->getVaueOfArray($timeroutineparam, 'hour');
            $z=$utildata->getVaueOfArray($timeroutineparam, 'minutes');
           $sdto->timeroutineparamx=$x;
		   $sdto->timeroutineparamy=$y;
		   $sdto->timeroutineparamz=$z;
		 
       
        }else  if($timeroutine=='system_monthly'){
            $x=$utildata->getVaueOfArray($timeroutineparam, 'day');
            $y=$utildata->getVaueOfArray($timeroutineparam, 'hour');
            $z=$utildata->getVaueOfArray($timeroutineparam, 'minutes');
           
            $sdto->timeroutineparamx=$x;
		   $sdto->timeroutineparamy=$y;
		   $sdto->timeroutineparamz=$z;
		
        }else if($timeroutine=='once'){
			  if(!empty($dto->getTimeexeconce())){
					$sdto->timeexeconce=$datecast->convertToForm($dto->getTimeexeconce());
					$sdto->timeexeconcehour=$dto->getTimeexeconce()->format('H')+0;
					$sdto->timeexeconceminute=$dto->getTimeexeconce()->format('i')+0;
			} 
		    
         }
		
		
		
        $execperiodstart=$datecast->convertToForm($dto->getExecperiodstart());
		$sdto->execperiodstart=$execperiodstart;
        if(!empty($execperiodstart)){
				$sdto->execperiodstarthour=$dto->getExecperiodstart()->format('H')+0;
				$sdto->execperiodstartminute=$dto->getExecperiodstart()->format('i')+0;
		} 
        
        $execperiodend=$datecast->convertToForm($dto->getExecperiodend());
		$sdto->execperiodend=$execperiodend;
		if(!empty($execperiodend)){
				$sdto->execperiodendhour=$dto->getExecperiodend()->format('H')+0;
				$sdto->execperiodendminute=$dto->getExecperiodend()->format('i')+0;
		} 
		
		//calendar
		$sdto->calendarkey=$utildata->getVaueOfArray($timeroutineparam,'calendar.key',true);
		$sdto->calendartimetype=$utildata->getVaueOfArray($timeroutineparam,'calendar.timetype',true);
		
		$dconfig=$json->decode($dto->getDconfig(),true);
		
		
		
		//exportodatabase
		$exporttodbtype=$utildata->getVaueOfArray($dconfig,'exporttodatabase.type',true);
		
		$exporttodbtargettype=$utildata->getVaueOfArray($dconfig,'exporttodatabase.targettype',true);
		$exporttodbmaxrowperconnection=$utildata->getVaueOfArray($dconfig,'exporttodatabase.maxrowperconnection',true);
		$exporttodbmaxrowperprocess=$utildata->getVaueOfArray($dconfig,'exporttodatabase.maxrowperprocess',true);
		$exporttodbotherconfig=$utildata->getVaueOfArray($dconfig,'exporttodatabase.otherconfig',true);
		$exporttodbtargetconfigmethod=$utildata->getVaueOfArray($dconfig,'exporttodatabase.targetconfig.method',true);
		$exporttodbtargetconfigservice=$utildata->getVaueOfArray($dconfig,'exporttodatabase.targetconfig.service',true);
		$exporttodbtargetconfigtable=$utildata->getVaueOfArray($dconfig,'exporttodatabase.targetconfig.table',true);
		$exporttodbtargetconfigsynckey=$utildata->getVaueOfArray($dconfig,'exporttodatabase.targetconfig.synckey',true);
		$exporttodbtargetsysteminstance=$utildata->getVaueOfArray($dconfig,'exporttodatabase.targetconfig.systeminstance',true);
		
		
		if(!empty($exporttodbtype)){$sdto->exporttodbtype=$exporttodbtype;}
		
		if(!empty($exporttodbtargettype)){$sdto->exporttodbtargettype=$exporttodbtargettype;}
		if(!empty($exporttodbmaxrowperconnection)){$sdto->exporttodbmaxrowperconnection=$exporttodbmaxrowperconnection;}
		if(!empty($exporttodbmaxrowperprocess)){$sdto->exporttodbmaxrowperprocess=$exporttodbmaxrowperprocess;}
		if(!empty($exporttodbotherconfig)){$sdto->exporttodbotherconfig=$exporttodbotherconfig;}
		if(!empty($exporttodbtargetconfigmethod)){$sdto->exporttodbtargetconfigmethod=$exporttodbtargetconfigmethod;}
		if(!empty($exporttodbtargetconfigservice)){$sdto->exporttodbtargetconfigservice=$exporttodbtargetconfigservice;}
		if(!empty($exporttodbtargetconfigtable)){$sdto->exporttodbtargetconfigtable=$exporttodbtargetconfigtable;}
		if(!empty($exporttodbtargetconfigsynckey)){$sdto->exporttodbtargetconfigsynckey=$exporttodbtargetconfigsynckey;}
		if(!empty($exporttodbtargetsysteminstance)){$sdto->exporttodbtargetsysteminstance=$exporttodbtargetsysteminstance;}
		
		
		//moodlebackuprestorecourse
		$moodlebackuprestorecourseservice=$utildata->getVaueOfArray($dconfig,'moodlebackuprestorecourse.service',true);
		$moodlebackuprestorecoursemoodleidtarget=$utildata->getVaueOfArray($dconfig,'moodlebackuprestorecourse.moodleidtarget',true);
		$moodlebackuprestorecoursecoursecategorytarget=$utildata->getVaueOfArray($dconfig,'moodlebackuprestorecourse.coursecategorytarget',true);
		$moodlebackuprestorecourseincludeuserinteraction=$utildata->getVaueOfArray($dconfig,'moodlebackuprestorecourse.includeuserinteraction',true);
		$moodlebackuprestorecoursesetcoursetimestart=$utildata->getVaueOfArray($dconfig,'moodlebackuprestorecourse.setcoursetimestart',true);

		if(!empty($moodlebackuprestorecourseservice)){$sdto->moodlebackuprestorecourseservice=$moodlebackuprestorecourseservice;}
		if(!empty($moodlebackuprestorecoursemoodleidtarget)){$sdto->moodlebackuprestorecoursemoodleidtarget=$moodlebackuprestorecoursemoodleidtarget;}
		if(!empty($moodlebackuprestorecoursecoursecategorytarget)){$sdto->moodlebackuprestorecoursecoursecategorytarget=$moodlebackuprestorecoursecoursecategorytarget;}
		if(!empty($moodlebackuprestorecourseincludeuserinteraction)){$sdto->moodlebackuprestorecourseincludeuserinteraction=$moodlebackuprestorecourseincludeuserinteraction;}
		if(!empty($moodlebackuprestorecoursesetcoursetimestart)){$sdto->moodlebackuprestorecoursesetcoursetimestart=$moodlebackuprestorecoursesetcoursetimestart;}
	

     return $sdto;  
}
function getTokenParam($container,$page) {
        $token=$container->get('badiu.auth.core.login.token')->getToken($page->getIsexternalclient());
	if(empty($token)){return '';}
	$js='';
	$js="_token: '".$token."',";
	return $js;
}

function getIdParam($dto) {
	$js='';
	if(empty($dto->id)){return '';}
	else {
		$js="id: '".$dto->id."',";
	}
	return $js;
}

function getUrlgoback($container,$page) {
	 $url= $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urlgoback'); 
	 $badiuSession=$container->get('badiu.system.access.session');
	 $badiuSession->setHashkey($page->getSessionhashkey());
     $clienttype=$badiuSession->get()->getType();
	 if($clienttype=='webservice'){
		  $cript=$container->get('badiu.system.core.lib.util.cript');
	      $cript->setKey('BADIUNET_EXTERNAL_CRITP_KKDDFFWW'); //this code seted too in Badiu\System\CoreBundle\Model\Functionality\BadiuFormat\TaskFormat  function linkManage
          $url=$cript->decode($url); 
	 }
	
	return $url;
}

function isRoutNavegation($modulekey,$rountekey) {

}
//temp
 function addSessionClientPluginAccess($container,$json,$utildata,$page){
	$parentid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
	if(empty($parentid)){return null;}

	$key="_badiu.moodle.mreport.scheduler.formshowsmtpservice";
	$badiuSession = $container->get('badiu.system.access.session');
	$badiuSession->setHashkey($page->getSessionhashkey());
	$exist=$badiuSession->existValue($key);
	if(!$exist){
	  if(!$container->has('badiu.admin.server.service.data')){return null;}
	  $servicedata = $container->get('badiu.admin.server.service.data');
	  $dconfig= $servicedata->getGlobalColumnValue('dconfig', array('id'=>$parentid));
	  if(empty($dconfig)){$badiuSession->addValue($key,0);return null;}
	  
	  $dconfig = $json->decode($dconfig, true);
	  $clientplugin=$utildata->getVaueOfArray($dconfig,'clientplugin');
	  if(empty($clientplugin)){$badiuSession->addValue($key,0);return null;}

	  $version=$utildata->getVaueOfArray($clientplugin,'version');
	  $version=$version+0;
	   if(empty($clientplugin)){$badiuSession->addValue($key,0);return null;}
	  if($version >=2019101000){$badiuSession->addValue($key,1);}
	  else{$badiuSession->addValue($key,0);}
	}


}
	
?>