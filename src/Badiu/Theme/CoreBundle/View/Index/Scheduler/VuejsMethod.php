			changeStatusScheduleformcontroladdnew: function (status) {
                this.scheduleformcontroladdnew=status;
            },
		
		
			processModalScheduleCheckFormError: function () {
				this.scheduleformcontrol.haserror=false;
				this.scheduleformcontrol.message='';
				
				if(this.isparamempty(this.scheduleform.name)){
					this.scheduleformcontrol.haserror=true;
				    this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.name.required'); ?>';
					return null;
				}
				 
				if(this.isparamempty(this.scheduleform.reportaction)){
					this.scheduleformcontrol.haserror=true;
				    this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.status.required'); ?>';
					return null;
				}
				
			
				//export.todatabase
				if(this.scheduleform.reportaction =='export.todatabase'){
					
					if(this.isparamempty(this.scheduleform.exporttodatabase.type)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.action.export.todatabase.type.required'); ?>';
						return null;
					}
					
					if(this.isparamempty(this.scheduleform.exporttodatabase.maxrowperprocess)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.action.export.maxrowperprocess.required'); ?>';
						return null;
					}
					
					if(this.isparamempty(this.scheduleform.exporttodatabase.maxrowperconnection)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.action.export.maxrowperconnection.required'); ?>';
						return null;
					}
					
					if(this.isparamempty(this.scheduleform.exporttodatabase.targetconfig.service)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.action.export.targetconfigservice.required'); ?>';
						return null;
					}
					
					if(this.isparamempty(this.scheduleform.exporttodatabase.targetconfig.systeminstance)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.action.export.targetsysteminstance.required'); ?>';
						return null;
					}
					
					if(this.isparamempty(this.scheduleform.exporttodatabase.targetconfig.table)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.action.export.targetconfigtable.required'); ?>';
						return null;
					}
					
					//check menssage send mail
					
					if(this.isparamempty(this.scheduleform.reportmessage.recipient.to) && !this.isparamempty(this.scheduleform.reportmessage.message.subject)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.msgto.required'); ?>';
						return null;
					}
				 
					if(this.isparamempty(this.scheduleform.reportmessage.message.subject) && !this.isparamempty(this.scheduleform.reportmessage.recipient.to)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.msgsubject.required'); ?>';
						return null;
					} 
								
				}
				//export.todatabase
				if(this.scheduleform.reportaction =='moodle.mreport.backuprestorecourse'){
					
					if(this.isparamempty(this.scheduleform.moodlebackuprestorecourse.service)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.service.required'); ?>';
						return null;
					}
					 
					if(this.isparamempty(this.scheduleform.moodlebackuprestorecourse.moodleidtarget)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.moodleidtarget.required'); ?>';
						return null;
					}
					if(this.isparamempty(this.scheduleform.moodlebackuprestorecourse.coursecategorytarget)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.coursecategorytarget.required'); ?>';
						return null;
					}
					
					//check menssage send mail
					
					if(this.isparamempty(this.scheduleform.reportmessage.recipient.to) && !this.isparamempty(this.scheduleform.reportmessage.message.subject)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.msgto.required'); ?>';
						return null;
					}
				 
					if(this.isparamempty(this.scheduleform.reportmessage.message.subject) && !this.isparamempty(this.scheduleform.reportmessage.recipient.to)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.msgsubject.required'); ?>';
						return null;
					} 
				}
				
				//export.todatabase
				if(this.scheduleform.reportaction =='send.report'){
					if(this.isparamempty(this.scheduleform.reportmessage.recipient.to) && this.isparamempty(this.scheduleform.reportmessage.recipient.sysuser)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.msgto.required'); ?>';
						return null;
					}
					
					if(this.isparamempty(this.scheduleform.reportmessage.message.subject)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.msgsubject.required'); ?>';
						return null;
					} 
				
				}
				
				
				if(this.scheduleform.reportaction =='send.menssage.touserinreport'){
					
					if(this.isparamempty(this.scheduleform.reportmessage.message.subject)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.msgsubject.required'); ?>';
						return null;
					} 
					
					if(this.isparamempty(this.scheduleform.reportmessage.message.body)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.msg.required'); ?>';
						return null;
					}
				} 
				
							
				//system_timeinterval
				if(!this.isparamempty(this.scheduleform.timeroutine) && this.scheduleform.timeroutine=='system_timeinterval' ){
					
					//x
					if(this.isparamempty(this.scheduleform.timeroutineparam.x)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.timeroutineparamx.required'); ?>';
					return null;
				}
					//y
					if(this.isparamempty(this.scheduleform.timeroutineparam.y)){ 
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.timeroutineparamy.required'); ?>';
					 return null;
				  }
					
				}  
				//once
				else if(!this.isparamempty(this.scheduleform.timeroutine) && this.scheduleform.timeroutine=='once' ){
					if(this.isparamempty(this.scheduleform.timeexeconce)){
						this.scheduleformcontrol.haserror=true;
						this.scheduleformcontrol.message='<?php echo $translator->trans('badiu.system.scheduler.report.timeexeconce.required'); ?>';
					return null;
				}
				
				}
			}, 
           processModalSchedule: function () {
			   this.processModalScheduleCheckFormError();
			   if(this.scheduleformcontrol.haserror){return null;}
			    var self = this;
			   //this.scheduleform.reportmessage.message.body=CKEDITOR.instances.badiusystemschedule_messagebody.getData();
			  // if(!this.isparamempty(this.scheduleform.execperiodstart)){this.scheduleform.execperiodstart=moment(this.scheduleform.execperiodstart).format('DD/MM/YYYY');}
			 //  if(!this.isparamempty(this.scheduleform.execperiodend)){this.scheduleform.execperiodend=moment(this.scheduleform.execperiodend).format('DD/MM/YYYY');}
			  
			 // console.log('DAta inicio: '+this.scheduleform.execperiodstart);return null;
			   //console.log(this.scheduleform);
			   
                 var url="<?php echo $utilapp->getServiceUrl();?>";
				 // console.log(this.scheduleform.execperiodstart);
                // console.log(url);
               //  console.log(this.scheduleform);
				  self.scheduleformcontrol.processing=true;
				 // this.scheduleform._clientsession=this.clientsession;
               axios.post(url, this.scheduleform).then(function (response) {
				      console.log(response.data);
					 if(response.data.status=='accept'){
						 self.scheduleformcontrol.status='success';
                         self.scheduleformcontrol.haserror=false;
						 self.scheduleformcontrol.message='';
						 self.scheduleformcontrol.processing=false;
						self.changeStatusScheduleformcontroladdnew(0);
						self.restFormFields();
						self.changeStatusScheduleformcontroladdnew(0);
					 }else if(response.data.status=='danied'){
							self.scheduleformcontrol.processing=false;
							self.scheduleformcontrol.status='open';
                            self.scheduleformcontrol.haserror=true;
                            self.scheduleformcontrol.message='Ocorreu um erro'; 
					 }
                   
              
               }).catch(function (error) {
				
				      console.log(error.response);
				   if(error.response!== undefined &&  error.response.data!== undefined){
					   var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   console.log(resperror);
					   self.scheduleformcontrol.processing=false;
					   self.scheduleformcontrol.status='open';
						self.scheduleformcontrol.haserror=true;
						self.scheduleformcontrol.message='Ocorreu um erro';
				   }
					 
                }); 
			
            }, 
			
			processTabSchedule: function () {
				
			   this.processModalScheduleCheckFormError();
			   if(this.scheduleformcontrol.haserror){return null;}
			    var self = this;
			  // this.scheduleform.reportmessage.message.body=CKEDITOR.instances.badiusystemschedule_messagebody.getData();
			     var url="<?php echo $utilapp->getServiceUrl();?>";
				self.scheduleformcontrol.processing=true;
				//this.scheduleform._clientsession=this.clientsession;
               axios.post(url, this.scheduleform).then(function (response) {
				      console.log(response.data); 
					 if(response.data.status=='accept'){
						 if(self.scheduleform.id==-1){self.scheduleform.id=response.data.message;}
						 self.scheduleformcontrol.status='success';
                         self.scheduleformcontrol.haserror=false;
						 self.scheduleformcontrol.message='';
						 self.scheduleformcontrol.processing=false;
						 self.scheduletabcontrol="reportertab";
						
					 }else if(response.data.status=='danied'){
							self.scheduleformcontrol.processing=false;
							self.scheduleformcontrol.status='open';
                            self.scheduleformcontrol.haserror=true;
                            self.scheduleformcontrol.message='Ocorreu um erro'; 
					 }
                   
              
               }).catch(function (error) {
				
				     // console.log(error.response);
				   if(error.response!== undefined &&  error.response.data!== undefined){
					   var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   //console.log(resperror);
					   self.scheduleformcontrol.processing=false;
					   self.scheduleformcontrol.status='open';
						self.scheduleformcontrol.haserror=true;
						self.scheduleformcontrol.message='Ocorreu um erro';
				   }
					 
                }); 
			
            },
		processTabScheduleForword: function () {
			 this.scheduletabcontrol="reportertab";
		},
		processTabScheduleBack: function () {
			 this.scheduletabcontrol="schedulertab";
		},		
		processTabScheduleCancel: function () {
			window.location.replace(this.scheduleform._urlgoback);
		},	
	  
		processTabScheduleParamFilter: function () {  
			 
			  this.scheduleformeditfilter.query=this.makesearchquerystring();
			  this.scheduleformeditfilter._function='editfilter';
			  this.scheduleformeditfilter._service=this.scheduleform._service;
			  this.scheduleformeditfilter.id=this.scheduleform.id;
			  this.scheduleformeditfilter.parentid='<?php echo $parentid;?>';
			  this.scheduleformeditfilter.urlgoback='<?php echo $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urlgoback'); ?>'
			
			    var self = this;
			   
			     var url="<?php echo $utilapp->getServiceUrl();?>";
				self.scheduleformcontrol.processing=true;
			   // this.scheduleform._clientsession=this.clientsession;
               axios.post(url,this.scheduleformeditfilter).then(function (response) {
				      console.log(response.data); 
					 if(response.data.status=='accept'){
						 self.scheduleformcontrol.status='success';
                         self.scheduleformcontrol.haserror=false;
						 self.scheduleformcontrol.message='';
						 self.scheduleformcontrol.processing=false;
						 if(!self.isparamempty(self.urlgoback)){window.location.replace(self.scheduleform._urlgoback);}
						
					 }else if(response.data.status=='danied'){
							self.scheduleformcontrol.processing=false;
							self.scheduleformcontrol.status='open';
                            self.scheduleformcontrol.haserror=true;
                            self.scheduleformcontrol.message='Ocorreu um erro'; 
					 }
                   
              
               }).catch(function (error) {
				
				     // console.log(error.response);
				   if(error.response!== undefined &&  error.response.data!== undefined){
					   var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   //console.log(resperror);
					   self.scheduleformcontrol.processing=false;
					   self.scheduleformcontrol.status='open';
						self.scheduleformcontrol.haserror=true;
						self.scheduleformcontrol.message='Ocorreu um erro';
				   }
					 
                });
			  
		},
		restFormFields: function () {
			this.scheduleform.name='';
			this.scheduleform.reportaction='send.report';
			this.scheduleform.reportmessage.recipient.to='';
			this.scheduleform.reportmessage.message.subject='';
			this.scheduleform.reportmessage.message.body='';
			this.scheduleform.timeroutine='system_timeinterval';
			this.scheduleform.timeroutineparam.x='2';
			this.scheduleform.timeroutineparam.y='system_time_unit_day';
			this.scheduleform.timeroutineparam.z='';
			this.scheduleform.execperiodstart='';
			this.scheduleform.execperiodend='';
		},
		changeScheduletab: function (tab) {
			this.scheduletabcontrol=tab;
		}, 

		