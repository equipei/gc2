<div>
   
                 
				<div v-show="scheduleformcontrol.haserror"  v-html="scheduleformcontrol.message" class="alert alert-danger">
							
				</div>
                
                <div>
				  <fieldset  class="form-group">
					<legend> <a data-toggle="collapse" href="#schedulegeneral" aria-expanded="false"><?php echo $translator->trans('badiu.system.scheduler.task.general'); ?></a></legend>
					
					<div class="collapse show" id="schedulegeneral" aria-expanded="true">
						 <div class="row">
                           <div class="col-md-12">
                               <label  class="control-label" for="badiusystemschedule_name"><?php echo $translator->trans('badiu.system.scheduler.report.name'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <input type="text" class="form-control" id="badiusystemschedule_name"  placeholder="<?php echo $translator->trans('badiu.system.scheduler.report.name.placeholder'); ?>" v-model="scheduleform.name" name="badiusystemschedule_name">
                           </div>
                       </div>
					   
					    <div class="row">
                           <div class="col-md-12">
                               <label  class="control-label" for="badiusystemschedule_status"><?php echo $translator->trans('badiu.system.scheduler.task.status'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <select   class="form-control" id="badiusystemschedule_status"  v-model="scheduleform.status"  options="scheduleformoptions.status"  name="badiusystemschedule_status">
                                    <option v-for="item in scheduleformoptions.status" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>
                       </div>
					   
					   <div class="row">
                           <div class="col-md-12">
                                <label for="badiusystemschedule_reportaction"><?php echo $translator->trans('badiu.system.scheduler.report.action'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <select   class="form-control" id="badiusystemschedule_reportaction"  v-model="scheduleform.reportaction"  options="scheduleformoptions.reportaction"  name="badiusystemschedule_reportaction">
                                    <option v-for="item in scheduleformoptions.reportaction" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>  
                       </div>
					   
					    <div class="row">
                           <div class="col-md-12">
                                <label for="badiusystemschedule_reportmessage_sendonlyreportwithrecord"><?php echo $translator->trans('badiu.system.scheduler.report.onlysendwithrecord'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <select   class="form-control" id="badiusystemschedule_reportmessage_sendonlyreportwithrecord"  v-model="scheduleform.reportmessage.sendonlyreportwithrecord"  options="scheduleformoptions.defaulboolean"  name="badiusystemschedule_reportmessage_sendonlyreportwithrecord">
                                    <option v-for="item in scheduleformoptions.defaulboolean" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>  
                       </div>
					</div>
				  </fieldset>
				   
				   
				   <fieldset v-show="scheduleform.reportaction=='export.todatabase'" class="form-group">
						<legend> <a data-toggle="collapse" href="#exportdata" aria-expanded="false"><?php echo $translator->trans('badiu.system.scheduler.action.export.data'); ?></a></legend>
						<div class="collapse" id="exportdata" aria-expanded="false">

						<div class="row">
                           <div class="col-md-12">
                                <label for="badiusystemschedule_exporttodatabasetype"><?php echo $translator->trans('badiu.system.scheduler.action.export.todatabase.type'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <select   class="form-control" id="badiusystemschedule_exporttodatabasetype"  v-model="scheduleform.exporttodatabase.type"  options="scheduleformoptions.exporttodatabasetype"  name="badiusystemschedule_exporttodatabasetype">
                                    <option v-for="item in scheduleformoptions.exporttodatabasetype" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>  
                       </div>
						
						<div class="row">
                           <div class="col-md-12">
                              <label for="badiusystemschedule_exporttodatabasemaxrowperprocess"><?php echo $translator->trans('badiu.system.scheduler.action.export.maxrowperprocess'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <input type="text" class="form-control" id="badiusystemschedule_exporttodatabasemaxrowperprocess"   placeholder="<?php echo $translator->trans('badiu.system.scheduler.action.export.maxrowperprocess.placeholder'); ?>"  v-model="scheduleform.exporttodatabase.maxrowperprocess" name="badiusystemschedule_exporttodatabasemaxrowperprocess">
                           </div>
                       </div>
					   
					   <div class="row">
                           <div class="col-md-12">
                              <label for="badiusystemschedule_exporttodatabasemaxrowperconnection"><?php echo $translator->trans('badiu.system.scheduler.action.export.maxrowperconnection'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <input type="text" class="form-control" id="badiusystemschedule_exporttodatabasemaxrowperconnection"   placeholder="<?php echo $translator->trans('badiu.system.scheduler.action.export.maxrowperconnection.placeholder'); ?>"  v-model="scheduleform.exporttodatabase.maxrowperconnection" name="badiusystemschedule_exporttodatabasemaxrowperconnection">
                           </div>
                       </div>
					   
					   <div class="row">
                           <div class="col-md-12">
                              <label for="badiusystemschedule_exporttodatabasetargetconfigservice"><?php echo $translator->trans('badiu.system.scheduler.action.export.targetconfigservice'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <input type="text" class="form-control" id="badiusystemschedule_exporttodatabasetargetconfigservice"   placeholder="<?php echo $translator->trans('badiu.system.scheduler.action.export.targetconfigservice.placeholder'); ?>"  v-model="scheduleform.exporttodatabase.targetconfig.service" name="badiusystemschedule_exporttodatabasetargetconfigservice">
                           </div>
                       </div>
					   
					   <div class="row">
                           <div class="col-md-12">
                                <label for="badiusystemschedule_exporttodatabasetargetconfigsysteminstance"><?php echo $translator->trans('badiu.system.scheduler.action.export.targetsysteminstance'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <select   class="form-control" id="badiusystemschedule_exporttodatabasetargetconfigsysteminstance"  v-model="scheduleform.exporttodatabase.targetconfig.systeminstance"  options="scheduleformoptions.sitemoodle"  name="badiusystemschedule_exporttodatabasetargetconfigsysteminstance">
                                    <option v-for="item in scheduleformoptions.sitemoodle" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>  
                       </div>
					   <div class="row"> 
                           <div class="col-md-12">
                              <label for="badiusystemschedule_exporttodatabasetargetconfigtable"><?php echo $translator->trans('badiu.system.scheduler.action.export.targetconfigtable'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <input type="text" class="form-control" id="badiusystemschedule_exporttodatabasetargetconfigtable"   placeholder="<?php echo $translator->trans('badiu.system.scheduler.action.export.targetconfigtable.placeholder'); ?>"  v-model="scheduleform.exporttodatabase.targetconfig.table" name="badiusystemschedule_exporttodatabasetargetconfigtable">
                           </div>
                       </div>
					    
					<div class="row"> 
                           <div class="col-md-12">
                              <label for="badiusystemschedule_exporttodatabasetargetconfigsynckey"><?php echo $translator->trans('badiu.system.scheduler.action.export.targetconfigsynckey'); ?></label>
                           </div>
                           <div class="col-md-12">
                               <input type="text" class="form-control" id="badiusystemschedule_exporttodatabasetargetconfigsynckey"   placeholder="<?php echo $translator->trans('badiu.system.scheduler.action.export.targetconfigsynckey.placeholder'); ?>"  v-model="scheduleform.exporttodatabase.targetconfig.synckey" name="badiusystemschedule_exporttodatabasetargetconfigsynckey">
                           </div>
                       </div>
					   
					    <div class="row"> 
                           <div class="col-12">
                              <label for="badiusystemschedule_exporttodatabaseotherconfig"><?php echo $translator->trans('badiu.system.scheduler.action.export.otherconfig'); ?></label>
                           </div>
                           <div class="col-12">
                               <textarea class="form-control" id="badiusystemschedule_exporttodatabaseotherconfig"   v-model="scheduleform.exporttodatabase.otherconfig" name="badiusystemschedule_exporttodatabaseotherconfig"></textarea>
                           </div>
                       </div>
						</div>
					</fieldset> 
				
				
				<fieldset v-show="scheduleform.reportaction=='moodle.mreport.backuprestorecourse'" class="form-group">
						<legend> <a data-toggle="collapse" href="#backuprestorecourse" aria-expanded="false"><?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.config'); ?></a></legend>
						<div class="collapse" id="backuprestorecourse" aria-expanded="false">

						  <div class="row">
                           <div class="col-md-12">
                              <label for="badiusystemschedule_moodlebackuprestorecourseservice"><?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.service'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <input type="text" class="form-control" id="badiusystemschedule_moodlebackuprestorecourseservice"   placeholder="<?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.service.placeholder'); ?>"  v-model="scheduleform.moodlebackuprestorecourse.service" name="badiusystemschedule_moodlebackuprestorecourseservice">
                           </div>
                       </div>
					   
					   <div class="row">
                           <div class="col-md-12">
                                <label for="badiusystemschedule_moodlebackuprestorecoursemoodleidtarget"><?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.moodleidtarget'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <select   class="form-control" id="badiusystemschedule_moodlebackuprestorecoursemoodleidtarget"  v-model="scheduleform.moodlebackuprestorecourse.moodleidtarget"  options="scheduleformoptions.sitemoodle"  name="badiusystemschedule_moodlebackuprestorecoursemoodleidtarget">
                                    <option v-for="item in scheduleformoptions.sitemoodle" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>  
                       </div>
					    <div class="row">
                           <div class="col-md-12">
                              <label for="badiusystemschedule_moodlebackuprestorecoursecoursecategorytarget"><?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.coursecategorytarget'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <input type="text" class="form-control" id="badiusystemschedule_moodlebackuprestorecoursecoursecategorytarget"   placeholder="<?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.coursecategorytarget.placeholder'); ?>"  v-model="scheduleform.moodlebackuprestorecourse.coursecategorytarget" name="badiusystemschedule_moodlebackuprestorecoursecoursecategorytarget">
                           </div>
                       </div>
					   
					   <div class="row">
                           <div class="col-md-12">
                                <label for="badiusystemschedule_moodlebackuprestorecourseincludeuserinteraction"><?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.includeuserinteraction'); ?></label>
                           </div>
                           <div class="col-md-12">
                               <select   class="form-control" id="badiusystemschedule_moodlebackuprestorecourseincludeuserinteraction"  v-model="scheduleform.moodlebackuprestorecourse.includeuserinteraction"  options="scheduleformoptions.defaulboolean"  name="badiusystemschedule_moodlebackuprestorecourseincludeuserinteraction">
                                     <option v-for="item in scheduleformoptions.defaulboolean" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>  
                       </div>
					   
					    <div class="row">
                           <div class="col-md-12">
                              <label for="badiusystemschedule_moodlebackuprestorecoursesetcoursetimestart"><?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.setcoursetimestart'); ?></label>
                           </div>
                           <div class="col-md-12">
                               <vuejs-datepicker class="form-control" :format="defaultDatepickerFormat" v-model="scheduleform.moodlebackuprestorecourse.setcoursetimestart" id="badiusystemschedule_moodlebackuprestorecoursesetcoursetimestart"  placeholder="<?php echo $translator->trans('badiu.moodle.mreport.scheduler.backuprestorecourse.setcoursetimestart.placeholder'); ?>"  name="badiusystemschedule_moodlebackuprestorecoursesetcoursetimestart"  :clear-button="true"></vuejs-datepicker>
                           </div>
                       </div>
					</fieldset>
					
				 <fieldset   class="form-group">
				 <legend> <a data-toggle="collapse" href="#mailmessage" aria-expanded="false"><?php echo $translator->trans('badiu.system.scheduler.task.mailmsg'); ?></a></legend>
				<div class="collapse" id="mailmessage" aria-expanded="false">
                                     
                     
                     
                     <div class="row">
                           <div class="col-md-12">
                              <label  v-show="scheduleform.reportaction=='send.report'"  for="badiusystemschedule_messagerecipientto"><?php echo $translator->trans('badiu.system.scheduler.report.msgto'); ?><i class="fa fa-asterisk text-danger"></i></label>
							  <label  v-show="scheduleform.reportaction=='send.menssage.touserinreport'"  for="badiusystemschedule_messagerecipientto"><?php echo $translator->trans('badiu.system.scheduler.report.msgtocopy'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <input type="text" class="form-control" id="badiusystemschedule_messagerecipientto"  placeholder="<?php echo $translator->trans('badiu.system.scheduler.report.msgto.placeholder'); ?>" v-model="scheduleform.reportmessage.recipient.to" name="badiusystemschedule_messagerecipientto">
                           </div>
                          
                           <?php 
                           if(!empty($page->getConfig()->getSchedulersystemrecipient())){
                              /* 
                               $systemrecipient=$container->get('badiu.system.scheduler.task.systemrecipient');
                                $systemrecipient->setPconfig($page->getConfig()->getSchedulersystemrecipient());
                                $listdefaultrecipient=$systemrecipient->getFormOptionsList();
                               $htmlfdr="";
                               if(!empty($listdefaultrecipient) && is_array($listdefaultrecipient)){
                                $htmlfdr .= "<div class=\"col-md-12\" id=\"badiusystemschedule_reportmessage_defaultrecipient\" >";
                                    foreach ($listdefaultrecipient as $key => $value) {
                                        $id="dafaultrecipient_".$key;
                                        $htmlfdr .= "<input type=\"checkbox\" id=\"$id\" value=\"$key\"  v-model=\"scheduleform.reportmessage.recipient.sysuser\"> <label>$value</label> &nbsp;&nbsp;";
                                    }
                                    $htmlfdr .= "</div>";
                               }
                               echo $htmlfdr;*/ 
                            }    
                          ?>
                            <?php  if(!empty($page->getConfig()->getSchedulersystemrecipient())){?>
                                <div class="col-md-12">
                               <select   class="form-control" id="badiusystemschedule_reportmessage_defaultrecipient"  v-model="scheduleform.reportmessage.recipient.sysuser"  options="scheduleformoptions.defaultrecipient"  name="badiusystemschedule_reportmessage_defaultrecipient">
                                    <option v-for="item in scheduleformoptions.defaultrecipient" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>  
                            <?php }?>
                       </div>
					
					   
                    <div class="row">
                           <div class="col-md-12">
                              <label for="badiusystemschedule_messagesubject"><?php echo $translator->trans('badiu.system.scheduler.report.msgsubject'); ?><i class="fa fa-asterisk text-danger"></i></label>
                           </div>
                           <div class="col-md-12">
                               <input type="text" class="form-control" id="badiusystemschedule_messagesubject"   placeholder="<?php echo $translator->trans('badiu.system.scheduler.report.msgsubject.placeholder'); ?>"  v-model="scheduleform.reportmessage.message.subject" name="badiusystemschedule_messagesubject">
                           </div>
                       </div>
                   
                  
					<div class="row">
                           <div class="col-md-12">
							<label for="badiusystemschedule_messagebody"><?php echo $translator->trans('badiu.system.scheduler.report.msg'); ?></label> 
						  </div>
						  <div class="col-md-12"> 
						  <div>
						  <vue-tinymce class="form-control"  id="badiusystemschedule_messagebody" v-model="scheduleform.reportmessage.message.body" toolbar=" fullscreen code | fontselect fontsizeselect formatselect | bold italic forecolor backcolor | link image | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |insertfile undo redo "></vue-tinymce>
						   <input type="file" ref="badiufileup"  style="display: none;" id="badiutextediorfileupload">
                         </div>
						 </div>
                   </div> 
				   
				    <?php 
						$badiuSession = $container->get('badiu.system.access.session');
						$badiuSession->setHashkey($page->getSessionhashkey());
						$formshowsmtpservice=$badiuSession->getValue('_badiu.moodle.mreport.scheduler.formshowsmtpservice');
				     if($formshowsmtpservice){?>
				   
				   <div class="row">
                           <div class="col-md-12">
                              <label for="badiusystemschedule_smtpservice"><?php echo $translator->trans('badiu.system.scheduler.smtpservice'); ?></label>
                           </div>
                           <div class="col-md-12">
                               <select   class="form-control" id="badiusystemschedule_reportmessage_smtpservice"  v-model="scheduleform.reportmessage.smtpservice"  options="scheduleformoptions.smtpservice"  name="badiusystemschedule_reportmessage_smtpservice">
                                    <option v-for="item in scheduleformoptions.smtpservice" :value="item.value">{{item.text }}</option>
                                </select>
                           </div>
                       </div>
				   <?php }?>
				     </div>
                     </fieldset>
					 
					
					
					
					  <fieldset  class="form-group">
						<legend><legend> <a data-toggle="collapse" href="#scheduleexec" aria-expanded="false" ><?php echo $translator->trans('badiu.system.scheduler.task.routineexec'); ?></a></legend>
						 <div class="collapse" id="scheduleexec" aria-expanded="false">
						 
						<div class="form-group">
                              <label for="badiusystemschedule_timeroutine"><?php echo $translator->trans('badiu.system.scheduler.task.timeroutine'); ?><i class="fa fa-asterisk text-danger"></i></label>
                       
							 <select   class="form-control" id="badiusystemschedule_timeroutine_options"  v-model="scheduleform.timeroutine"  options="scheduleformoptions.timeroutine"  name="badiusystemschedule_timeroutine">
                                    <option v-for="item in scheduleformoptions.timeroutine" :value="item.value">{{item.text }}</option>
                                </select>
							 </div>
					 <!-- system_daily -->
					 
					  <div id="group_badiusystemschedule_timeroutine_system_daily">
                          <div class="row">
                             
							 <div class="col-md-6"> 
							 <select   v-if="scheduleform.timeroutine=='system_daily'"  class="form-control" id="badiusystemschedule_timeroutineparamy_day_hour"  v-model="scheduleform.timeroutineparam.x"  options="scheduleformoptions.hour"  name="badiusystemschedule_timeroutineparamw_day_hour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="col-md-6"> 
							 <select   v-if="scheduleform.timeroutine=='system_daily'"  class="form-control" id="badiusystemschedule_timeroutineparamy_day_minute"  v-model="scheduleform.timeroutineparam.y"  options="scheduleformoptions.minute"  name="badiusystemschedule_timeroutineparamw_day_minute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                         </div>
                   </div>	
                   <!-- system_timeinterval -->
                     <div   id="group_badiusystemschedule_timeroutine_system_timeinterval">
                         <div class="row">
                           
                             <div class="col-md-6">
                                <input v-if="scheduleform.timeroutine=='system_timeinterval'"  type="text" class="form-control" id="badiusystemschedule_timeroutineparamx"  v-model="scheduleform.timeroutineparam.x" name="badiusystemschedule_timeroutineparamx">
                             </div>
                             <div class="col-md-6">
                                <select  v-if="scheduleform.timeroutine=='system_timeinterval'" class="form-control" id="badiusystemschedule_timeroutineparamy"  v-model="scheduleform.timeroutineparam.y"  options="scheduleformoptions.unittime"  name="badiusystemschedule_timeroutineparamy">
                                    <option v-for="item in scheduleformoptions.unittime" :value="item.value">{{item.text }}</option>
                                </select>
                             </div>
                         </div>
                   </div>
                   
				    <!-- system_weekly -->
					 
					  <div id="group_badiusystemschedule_timeroutine_system_weekly">
                          <div class="row">
                             
							 
                             <div class="col-md-4">
								<select   v-if="scheduleform.timeroutine=='system_weekly'"  class="form-control" id="badiusystemschedule_timeroutine_day_of_week"  v-model="scheduleform.timeroutineparam.x"  options="scheduleformoptions.daysofweek"  name="badiusystemschedule_timeroutine_day_of_week">
                                    <option v-for="item in scheduleformoptions.daysofweek" :value="item.value">{{item.text }}</option>
                                </select>
                             </div>
							  <div class="col-md-4"> 
							 <select   v-if="scheduleform.timeroutine=='system_weekly'"  class="form-control" id="badiusystemschedule_timeroutineparamy_day_of_week_hour"  v-model="scheduleform.timeroutineparam.y"  options="scheduleformoptions.hour"  name="badiusystemschedule_timeroutineparamw_day_of_week_hour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="col-md-4"> 
							 <select   v-if="scheduleform.timeroutine=='system_weekly'"  class="form-control" id="badiusystemschedule_timeroutineparamy_day_of_week_minute"  v-model="scheduleform.timeroutineparam.z"  options="scheduleformoptions.minute"  name="badiusystemschedule_timeroutineparamw_day_of_week_minute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                         </div>
                   </div>
				   
				   <!-- system_monthly -->
					 
					  <div id="group_badiusystemschedule_timeroutine_system_monthly">
                          <div class="row">
                             
							 
                             <div class="col-md-4">
								<select   v-if="scheduleform.timeroutine=='system_monthly'"  class="form-control" id="badiusystemschedule_timeroutine_day_of_month"  v-model="scheduleform.timeroutineparam.x"  options="scheduleformoptions.daysofmonth"  name="badiusystemschedule_timeroutine_day_of_month">
                                    <option v-for="item in scheduleformoptions.daysofmonth" :value="item.value">{{item.text }}</option>
                                </select>
                             </div>
							  <div class="col-md-4"> 
							 <select   v-if="scheduleform.timeroutine=='system_monthly'"  class="form-control" id="badiusystemschedule_timeroutineparamy_day_of_month_hour"  v-model="scheduleform.timeroutineparam.y"  options="scheduleformoptions.hour"  name="badiusystemschedule_timeroutineparamw_day_of_month_hour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="col-md-4"> 
							 <select   v-if="scheduleform.timeroutine=='system_monthly'"  class="form-control" id="badiusystemschedule_timeroutineparamy_day_of_month_minute"  v-model="scheduleform.timeroutineparam.z"  options="scheduleformoptions.minute"  name="badiusystemschedule_timeroutineparamw_day_of_month_minute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                         </div>
                   </div>
				    <!-- once -->
					 
					  <div  id="group_badiusystemschedule_timeroutine_once" v-show="scheduleform.timeroutine=='once'">
                          <div class="row">
                             
							 
                             <div class="col-md-4">
                                <vuejs-datepicker class="form-control" :format="defaultDatepickerFormat"  id="badiusystemschedule_timeexeconce"  v-model="scheduleform.timeexeconce" name="badiusystemschedule_timeexeconce"  :clear-button="true"></vuejs-datepicker>
                             </div>
                              
							 <div class="col-md-4"> 
							 <select class="form-control" id="badiusystemschedule_timeexeconcehour"  v-model="scheduleform.timeexeconcehour"  options="scheduleformoptions.hour"  name="badiusystemschedule_timeexeconcehour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="col-md-4"> 
							 <select   class="form-control" id="badiusystemschedule_timeexeconceminute"  v-model="scheduleform.timeexeconceminute"  options="scheduleformoptions.minute"  name="badiusystemschedule_timeexeconceminute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 

                         </div>
                   </div>
				   
                   <div  id="group_badiusystemschedule_timeroutine_periodexec" class="form-group">
                        <label for="badiusystemschedule_periodexec"><?php echo $translator->trans('badiu.system.scheduler.task.periodexec'); ?></label>
                         <div class="row">
                            
							<!-- <div class="col-md-6">
                                <badiudate class="form-control" id="badiusystemschedule_execperiodstart" placeholder="<?php echo $translator->trans('badiu.system.scheduler.task.execperiodstart'); ?>"  format="d/m/Y" lang="pt"  v-model="scheduleform.execperiodstart" name="badiusystemschedule_execperiodstart"></badiudate>
                             </div>-->
                             <div class="col-md-4">
                                <vuejs-datepicker class="form-control" id="badiusystemschedule_execperiodstart" placeholder="<?php echo $translator->trans('badiu.system.scheduler.task.execperiodstart'); ?>"  :format="defaultDatepickerFormat" v-model="scheduleform.execperiodstart" name="badiusystemschedule_execperiodstart"  :clear-button="true" ></vuejs-datepicker>
                             </div>
                               <div class="col-md-1"> 
							 <select class="form-control" id="badiusystemschedule_execperiodstarthour"  v-model="scheduleform.execperiodstarthour"  options="scheduleformoptions.hour"  name="badiusystemschedule_execperiodstarthour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="col-md-1"> 
							 <select   class="form-control" id="badiusystemschedule_execperiodstartminute"  v-model="scheduleform.execperiodstartminute"  options="scheduleformoptions.minute"  name="badiusystemschedule_execperiodstartminute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
							  
							  <div class="col-md-4">
									<vuejs-datepicker class="form-control" :format="defaultDatepickerFormat" v-model="scheduleform.execperiodend" id="badiusystemschedule_execperiodend"  placeholder="<?php echo $translator->trans('badiu.system.scheduler.task.execperiodend'); ?>"  name="badiusystemschedule_execperiodend"  :clear-button="true"></vuejs-datepicker>
									
                             </div>
							 
							  <div class="col-md-1"> 
							 <select class="form-control" id="badiusystemschedule_execperiodendhour"  v-model="scheduleform.execperiodendhour"  options="scheduleformoptions.hour"  name="badiusystemschedule_execperiodendhour">
                                    <option v-for="item in scheduleformoptions.hour" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                             <div class="col-md-1"> 
							 <select   class="form-control" id="badiusystemschedule_execperiodendminute"  v-model="scheduleform.execperiodendminute"  options="scheduleformoptions.minute"  name="badiusystemschedule_execperiodendminute">
                                    <option v-for="item in scheduleformoptions.minute" :value="item.value">{{item.text }}</option>
                                </select>
					        </div> 
                         </div> 
                   </div> 
				   
				   <div  id="group_badiusystemschedule_calendar" class="form-group">
                        <label for="badiusystemschedule_periodexec"><?php echo $translator->trans('badiu.system.scheduler.calendar'); ?></label>
                         <div class="row">
						  <div class="col-md-6">
							 <select   class="form-control" id="badiusystemschedule_calendarkey"  v-model="scheduleform.calendar.key"  options="scheduleformoptions.calendarkey"  name="badiusystemschedule_calendarkey">
                                    <option v-for="item in scheduleformoptions.calendarkey" :value="item.value">{{item.text }}</option>
                                </select>
						  </div>
						  <div class="col-md-6">
								<select   class="form-control" id="badiusystemschedule_calendartimetype"  v-model="scheduleform.calendar.timetype"  options="scheduleformoptions.calendartimetype"  name="badiusystemschedule_calendartimetype">
                                    <option v-for="item in scheduleformoptions.calendartimetype" :value="item.value">{{item.text }}</option>
                                </select>
						  </div>
						 </div>
					 </div>
				   </div> 
				    </fieldset>
                 </div>
                
                <div v-show="scheduleformcontroladdnew==0">
                    <button type="button" class="btn btn-primary" @click="processTabSchedule()"><?php echo $translator->trans('badiu.system.scheduler.report.saveforword'); ?></button> <?php echo iconProcess($container);?>
					<button type="button" class="btn btn-primary" @click="processTabScheduleForword()"><?php echo $translator->trans('badiu.system.scheduler.report.forword'); ?></button> 
                    <button type="button" class="btn btn-secondary" @click="processTabScheduleCancel()"><?php echo $translator->trans('badiu.system.scheduler.report.cancel'); ?></button>
                </div>
                <div v-show="scheduleformcontroladdnew==1">
                    <button type="button" class="btn btn-primary" @click="processModalSchedule()"><?php echo $translator->trans('badiu.system.scheduler.report.save'); ?></button> <?php echo iconProcess($container);?>
					<button type="button" class="btn btn-secondary" @click="changeStatusScheduleformcontroladdnew(0)"><?php echo $translator->trans('badiu.system.scheduler.report.cancel'); ?></button>
                </div>
			</div>
  	</div>      


