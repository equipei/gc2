<div id="_badiu_theme_base_modal_schedule" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?php echo $translator->trans('badiu.system.scheduler.report.index'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                 
                   <div class="form-group">
                        <label for="badiusystemschedule_name"><?php echo $translator->trans('badiu.system.scheduler.report.name'); ?></label>
                        <input type="text" class="form-control" id="badiusystemschedule_name"  v-model="scheduleform.name" name="badiusystemschedule_name">
                   </div>
                    <div class="form-group">
                        <label for="badiusystemschedule_reportaction"><?php echo $translator->trans('badiu.system.scheduler.report.action'); ?></label>
                        <select   class="form-control" id="badiusystemschedule_reportaction"  v-model="scheduleform.reportaction"  options="scheduleformoptions.reportaction"  name="badiusystemschedule_reportaction">
                            <option v-for="item in scheduleformoptions.reportaction" :value="item.value">{{item.text }}</option>
                        </select>
                   </div>
                   <div class="form-group">
                        <label for="badiusystemschedule_messagesubject"><?php echo $translator->trans('badiu.system.scheduler.report.msgsubject'); ?></label>
                        <input type="text" class="form-control" id="badiusystemschedule_messagesubject"  v-model="scheduleform.reportmessage.message.subject" name="badiusystemschedule_messagesubject">
                   </div>
                    <div class="form-group">
                        <label for="badiusystemschedule_messagebody"><?php echo $translator->trans('badiu.system.scheduler.report.msg'); ?></label> 
                        <textarea rows="5" class="form-control" id="badiusystemschedule_messagebody"  v-model="scheduleform.timeroutine" name="badiusystemschedule_messagebody"></textarea>
                         <!--<textarea rows="5" class="form-control ckeditor" id="badiusystemschedule_messagebody"  v-model="scheduleform.message.message.body" name="badiusystemschedule_messagebody"></textarea>-->
                   </div>
                    
                     <div class="form-group">
                        <label for="badiusystemschedule_timeroutine"><?php echo $translator->trans('badiu.system.scheduler.task.timeexeconce'); ?></label>
                        <select   class="form-control" id="badiusystemschedule_timeroutine"  v-model="scheduleform.timeroutine"  options="scheduleformoptions.timeroutine"  name="badiusystemschedule_timeroutine">
                            <option v-for="item in scheduleformoptions.timeroutine" :value="item.value">{{item.text }}</option>
                        </select>
                   </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" @click="processModalSchedule()"><?php echo $translator->trans('badiu.system.scheduler.report.save'); ?></button> 
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $translator->trans('badiu.system.scheduler.report.cancel'); ?></button>
                </div>
            </div>
        </div>
    </div>
