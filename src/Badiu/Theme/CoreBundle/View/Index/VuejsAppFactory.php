
<?php
$formconfig = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig');
$formfields = $utildata->getVaueOfArray($formconfig, 'formfields');
$currenturl = $utilapp->getCurrentUrl(true);
$externalservicequerykey = "";
$settkeysession="";
$gettkeysession="";

if ($page->getIsexternalclient()) {
    $currenturl = "BADIU_CORE_SERVICE_CLIENTE_URLBASE";
	$externalservicequerykey = "_key=" . $page->getKey() . "&";
	
	$clientsession=$container->get('badiu.auth.core.login.clientsession');
	$clientsession->setSessionhashkey($page->getSessionhashkey());
	$settkeysession=$clientsession->setSessionstorage();
	$gettkeysession=$clientsession->getSessionstorage();
}  

include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Form/VuejsMakeDataFactory.php"));
  
include_once("VuejsMakeDataFactory.php");

 ?> 
<script>
 <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Component/Tinymce.php"));?>   

var badiuthembaseindexapp =new Vue({
el: '#_badiu_theme_base_index_vuejs',
	components: {
		vuejsDatepicker,
		   <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessindexjscomponent()));?> 
	},
	created: function(){
				 <?php echo $settkeysession; ?> 
				 <?php echo $gettkeysession;?> 
                <?php include_once("Scheduler/VuejsCreated.php"); ?> 
				<?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessindexjscreated()));?> 
				<?php include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Interaction/Default/VuejsCreated.php"));?>
			
            },
        data: {
                urledit: "<?php echo $urledit; ?>",
                urlcopy: "<?php echo $urlcopy; ?>",
                itemsected: "",
                operation: "delete",
				operationmessage: "",
				operationmessages: {
					delete: "<?php echo $translator->trans('badiu.system.action.delete.confirm');?>",
					restore: "<?php echo $translator->trans('badiu.system.action.restore.confirm');?>", 
					remove: "<?php echo $translator->trans('badiu.system.action.remove.confirm');?>"
				},
				operationbuttonlabel: "",
				operationbuttonlabels: {
					delete: "<?php echo $translator->trans('badiu.system.action.delete');?>",
					restore: "<?php echo $translator->trans('badiu.system.action.restore');?>", 
					remove: "<?php echo $translator->trans('badiu.system.action.remove');?>"
				},
				showmodalcrt: "none",
				showmodalcrtclass: "modal fade",
				formcontrol: {status: "open", message: "", haserror: false,processing: false},
				serviceurl: "<?php echo $utilapp->getServiceUrl();?>",
			    currenturl: "<?php echo $utilapp->getCurrentUrl(true);?>", 
			    urlgoback: "<?php echo urlencode($utilapp->getCurrentUrl());?>",
				clientsession: "",
				clientsessionparam: "",
				<?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsDataLib.php"));?> 
                tablehead : [ <?php echo makeTableHead($container,$table);?>],
                tabledata : [
                            <?php
                            $cont = 0;
                            $separator = "";

                            foreach ($table->getRow()->getList() as $row) {
                                if ($cont > 0) {
                                    $separator = ",";
                                }
                                ?>

                                <?php echo $separator; ?>
                                        {
                                <?php
                                $contitem = 0;
                                $separatoritem = "";
                                foreach ($row as $key => $value) {
                                  
                                    if(is_array($value)){$value="";} //review it cast to json
                                    $v = $utiljson->escape($value);
                                   
                                    if ($contitem > 0) {
                                        $separatoritem = ",";
                                     }
                                    ?>

                                    <?php echo $separatoritem; ?>
                                    <?php echo $key; ?>:{value: "<?php echo $v; ?>",crtedit: false}
                                    <?php if ($contitem > 0 && $contitem==(sizeof($table->getColumn()->getList()))-1) {echo ",crtedit: false";}?>
                                    <?php $contitem++; ?>
                                <?php } ?>
                                       }
                                <?php $cont++; ?>
                            <?php } ?>
                ],
		badiuform: {
                      <?php echo makeDataFormField($container,$page,$formfields);?>    
                    },
                 badiudefaultformoptions: {
                      <?php echo makeDataDefaultFormOption($container,$formfields);?>    
                 },
				 badiuformoptions: {
                      <?php echo makeDataFormOption($container,$formfields);?>    
                 },
                 badiuautocomplete: {
                       <?php echo makeDataAutocomplete($container,$page,$formfields);?>
                 },
                 badiuaerrormessage: {
                       <?php echo makeErrorMessage($container,$formfields);?>
                 },
                 badiuaerrorcontrol: {
                       <?php echo makeErrorControl($container,$formfields);?>
                 },
                badiurequiredcontrol: {
                       <?php echo makeRequiredControl($container,$formfields);?>
                 },
				badiusharedata: {  
                       <?php /*echo makeReportCsv($container,$table);*/?>
					   <?php /* echo makeReportJson($container,$table);*/ ?>
                 }, 
                <?php include_once("Scheduler/VuejsData.php"); ?> 
              
                <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessindexjsvariable()));?> 
				<?php include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Interaction/Default/VuejsData.php"));?>
            },
             watch: {
                     <?php echo makeWatchAutocomplete($container,$formfields);?>
					<?php include_once("Scheduler/VuejsWatch.php"); ?> 
                    <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessindexjswatch()));?> 
					<?php include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Interaction/Default/VuejsWatch.php"));?>
                },
        methods: {
            <?php echo makeMethodAutocomplete($container,$formfields);?> 
            <?php echo makeMethodAutocompleteRemoveItemSelect($container,$page,$formfields);?> 
             <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsMethodLib.php"));?> 
            showModalToDelete: function (item,operation) {
                 this.itemsected=item;
                 this.operation=operation;
				 this.showmodalcrt="block";
				 document.getElementById("_badiu_theme_base_modal_delete_ctrmodal").style.visibility = "visible";
				 this.showmodalcrtclass	="modal fade show ";	
				 
				  if(this.operation=='delete'){this.operationmessage=this.operationmessages.delete;this.operationbuttonlabel=this.operationbuttonlabels.delete;}
				 else if(this.operation=='restore'){this.operationmessage=this.operationmessages.restore;this.operationbuttonlabel=this.operationbuttonlabels.restore;}
				 else if(this.operation=='remove'){this.operationmessage=this.operationmessages.remove;this.operationbuttonlabel=this.operationbuttonlabels.remove;}
			     },
            
            processDeleteService: function () {

            console.log(this.listDelete);

            var params = new URLSearchParams();
            params.append('_service','<?php echo $currentservicedata; ?>');
        
            if(this.operation=='delete'){
                params.append('_service','badiu.system.core.lib.util.cleandata');
				 params.append('key','<?php echo $page->getKey(); ?>');
				  params.append('_function','delete');
             
            }
			else if(this.operation=='restore'){
                params.append('_service','badiu.system.core.lib.util.cleandata');
				 params.append('key','<?php echo $page->getKey(); ?>');
				  params.append('_function','restore');
             
            }else if(this.operation=='remove'){
                  params.append('_function','removeNativeSqlService');
            }
             
            params.append('id', this.itemsected.id.value); 
            <?php  
                //$paramtoken=$container->get('badiu.auth.core.login.token')->getToken($page->getIsexternalclient());
                //if(!empty($paramtoken)){echo " params.append('_token', '".$paramtoken."'); ";}
				if($page->getIsexternalclient()){
					echo " params.append('_clientsession',this.clientsession)";
				}
            ?> 
            var self = this;
            console.log(this.operation);
            console.log(this.itemsected.id.value);
            axios.post('<?php echo $urservicebase?>', params, {headers: {
                    'Content-type': 'application/x-www-form-urlencoded',
                }}).then(function (response) {

                console.log(response.data);
				self.showmodalcrt="none";
                
                 location.reload(); 
           
               });

            },
			 makesearchquerystring: function () {
				 var query="";
                var contq=0;
               
               
          
                
              
				  Object.entries(this.badiuform).forEach(([k,v]) => {
				  var keyofautocomlete=k.indexOf("_badiuautocomplete");
				   if(keyofautocomlete==-1){
						if(contq==0){query=k+'='+v;}
						else{query+='&'+k+'='+v;}
						
						contq++;
				   }
                });  
              // console.log(query);
			  return query;
			 },
			 search: function () {
				
                var query=this.makesearchquerystring();
               var externalkey='<?php echo $externalservicequerykey;?>';
               var url=this.currenturl+"?"+externalkey+query;
           
               window.location.replace(url);
            
            },
           testChartInstance: function () {
                var chartx = new ApexCharts(document.querySelector("#chartTexst1"), this.chartText);
                 chartx.render();
            },
           <?php include_once("Scheduler/VuejsMethod.php"); ?> 
		  <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessindexjsmethod()));?> 
		  <?php include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Interaction/Default/VuejsMethod.php"));?>
        }
});

</script>
<?php include_once("Scheduler/JsGeneral.php");?> 

 <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessindexjsgeneral()));?> 

