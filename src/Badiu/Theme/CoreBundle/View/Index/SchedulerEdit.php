<div  id="_badiusystemreport">

   <ul class="nav nav-tabs">
        <li class="nav-item" ><a class="nav-link"  v-bind:class="{ active: scheduletabcontrol === 'schedulertab' }" v-on:click="changeScheduletab('schedulertab')"><b><?php echo $translator->trans('badiu.system.scheduler.task.schedulertab'); ?></b></a></li>
        <li class="nav-item" ><a class="nav-link" v-bind:class="{ active: scheduletabcontrol === 'reportertab' }" v-on:click="changeScheduletab('reportertab')"><b><?php echo $translator->trans('badiu.system.scheduler.task.reportertab'); ?></b></a></li>
  
   </ul>
 
    <div>
        <div v-if="scheduletabcontrol === 'schedulertab'" class="tabcontent">
            <?php 
             if($page->getViewsytemtype()=='bootstrap4'){include_once("Scheduler/HtmlContentEditBootstrap4.php");}
             else if($page->getViewsytemtype()=='bootstrap3'){include_once("Scheduler/HtmlContentEditBootstrap3.php");}
             else if($page->getViewsytemtype()=='bootstrap2'){include_once("Scheduler/HtmlContentEditBootstrap2.php");}
			 else {include_once("Scheduler/HtmlContentEditBootstrap4.php");}
           ?>
        </div> 
        <div  v-if="scheduletabcontrol === 'reportertab'" class="tabcontent">
          <?php  
         //$fileprocessformscheduler= $utilapp->getFilePath("BadiuThemeCoreBundle:View/Index/Scheduler/FactoryForm.php");
         // include_once($fileprocessformscheduler);
         $factoryformfilterschl=$container->get('badiu.theme.core.lib.template.vuejs.factoryform');
         $factoryformfilterschl->setPage($page);
         $foperation=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_foperation');
         $isclone=false;
         if($foperation=='clone'){$isclone=true;}
          echo $factoryformfilterschl->execScheduler($isclone);
          ?>
        </div>
        
    </div>
</div> 

