<?php

require_once("CoreLib.php");
require_once("ExternalClientJs.php");
$utilstring=$this->getContainer()->get('badiu.system.core.lib.util.string');
$utildata = $container->get('badiu.system.core.lib.util.data');
$formconfig = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig');
$formfields = $utildata->getVaueOfArray($formconfig, 'formfields');
$utilapp=$container->get('badiu.system.core.lib.util.app');
$serviceurl=$utilapp->getServiceUrl();
$servicekey="badiu.system.core.functionality.form.service";
$currenturl = $utilapp->getCurrentUrl(true);
$currentroute = $page->getKey();
$translator = $container->get('translator');
if ($page->getIsexternalclient()) {
    $currentroute= $page->getKey();
}


$urlgoback=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urlgoback');
if(!empty($urlgoback)){
    $urlgoback=urldecode($urlgoback);
    $urlindex= $urlgoback;
}






ob_start();
     $vuejsappffurl=$utilapp->getFilePath("BadiuThemeCoreBundle:View/Form/VuejsAppFactory.php");
    include_once($vuejsappffurl);
     $formappvuejs = ob_get_contents();
  ob_end_clean();
  
   ob_start();
   $vuejsdataurl=$utilapp->getFilePath($page->getConfig()->getFileprocessformaddjsvariable());
  include_once($vuejsdataurl);
  $datavuejs = ob_get_contents();
  ob_end_clean();
  
  ob_start();
    $vuejsmethodurl=$utilapp->getFilePath($page->getConfig()->getFileprocessformaddjsmethod());
    include_once($vuejsmethodurl);
    $methodvuejs = ob_get_contents();
  ob_end_clean();
  
 ob_start();
    $vuejswatchurl=$utilapp->getFilePath($page->getConfig()->getFileprocessformaddjswatch());
    include_once($vuejswatchurl);
    $watchvuejs = ob_get_contents();
  ob_end_clean();

  ob_start();
    $jsgeneralurl=$utilapp->getFilePath($page->getConfig()->getFileprocessformaddjsgeneral());
    include_once($jsgeneralurl);
    $jsgeneral = ob_get_contents();
  ob_end_clean();
  
  ob_start();
    $jscreatedurl=$utilapp->getFilePath($page->getConfig()->getFileprocessformaddjscreated());
    include_once($jscreatedurl);
    $jscreated = ob_get_contents();
  ob_end_clean();
  
$formappvuejs=$utilstring->replaceExpression("BADIU_THEME_BASE_APP_VUEJS_DATA",$datavuejs,$formappvuejs);
$formappvuejs=$utilstring->replaceExpression("BADIU_THEME_BASE_APP_VUEJS_METHOD",$methodvuejs,$formappvuejs);
$formappvuejs=$utilstring->replaceExpression("BADIU_THEME_BASE_APP_VUEJS_WATCH",$watchvuejs,$formappvuejs);
$formappvuejs=$utilstring->replaceExpression("BADIU_THEME_BASE_APP_VUEJS_CREATED",$jscreated,$formappvuejs);
$formappvuejs=$utilstring->replaceExpression("BADIU_THEME_BASE_APP_JS_GENERAL",$jsgeneral,$formappvuejs);

$factoryformfilter=$container->get('badiu.theme.core.lib.template.vuejs.factoryform');
$factoryformfilter->setPage($page);

include_once($utilapp->getFilePath($page->getConfig()->getFileprocessformaddhtmlcontent()));

echo $formappvuejs;  
require_once("ExternalClientCss.php");
?>
 