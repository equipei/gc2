
<?php
include_once($utilapp->getFilePath($page->getConfig()->getFileprocessviewhtmlcontent()));
include_once("VuejsMakeDataFactory.php");

include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Dashboard/VuejsMakeDataFactory.php"));

$settkeysession="";
$gettkeysession="";

if ($page->getIsexternalclient()) {
    $clientsession=$container->get('badiu.auth.core.login.clientsession');
	$clientsession->setSessionhashkey($page->getSessionhashkey());
	$settkeysession=$clientsession->setSessionstorage();
	$gettkeysession=$clientsession->getSessionstorage();
} 
 
?> 
<script>   
new Vue({
el: '#_badiu_theme_base_view_vuejs',
		components: {
			vuejsDatepicker,
         <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessviewjscomponent()));?> 
		},
        data: {
               formcontrol: {status: "open", message: "", haserror: false,processing: false},
			   serviceurl: '<?php echo $utilapp->getServiceUrl();?>',
			   currenturl: '<?php echo $utilapp->getCurrentUrl();?>',
			   urlgoback: '<?php echo urlencode($utilapp->getCurrentUrl());?>',
			   clientsession: "",
               <?php   echo makeTable($container,$table);?> 
               <?php   echo makeRow($container,$table);?> 
               <?php   echo makeDashboardRow($page, $utildata,$json);?> 
               <?php   echo makeDashboardRows($page, $utildata,$json);?>      
              //review   
               <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsDataLib.php"));?> 
               <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessviewjsvariable()));?> 

            }, watch: {
                   <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessviewjswatch()));?> 
                },
             methods: {
                <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsMethodLib.php"));?> 
                <?php  include_once("VuejsMethodCore.php");?> 
                <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessviewjsmethod()));?> 
           
            },
            created: function(){
				 <?php echo $settkeysession; ?> 
				 <?php echo $gettkeysession;?> 
                 <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessviewjscreated()));?> 
            } 
});

</script>
<?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessviewjsgeneral()));?> 