
<?php 
function makeTable($container,$table){
    $json = $container->get('badiu.system.core.lib.util.json');
    $cont = 0;
    $separator = "";
    $out="";
    if(!isset($table->getRow()->getList()[0])){return $out;}
    $out.= "tabledata : [";
        foreach ($table->getRow()->getList()[0]  as $key => $value) {
             $label="";
               if (array_key_exists($key,$table->getColumn()->getList())){$label=$table->getColumn()->getList()[$key];}
                 if ($cont > 0) {$separator = ",";}
                if(is_a($value, 'DateTime')){$v="";}//revie when is object or date
                else if(is_array($value)){$value="";}//review
                 $value= $json->escape($value);
                $out.=$separator;
               $out.="{key:\"$key\",value: \"$value\",label: \"$label\"}";
               $cont++;
         }
      $out.= "],";
     return $out;
}

function makeRow($container,$table){
    $json = $container->get('badiu.system.core.lib.util.json');
    $cont = 0;
    $separator = "";
    $out="";
    $out.= "rowdata : {";
    if(!isset($table->getRow()->getList()[0])){return $out;}
        foreach ($table->getRow()->getList()[0]  as $key => $value) {
               $label="";
               if (array_key_exists($key,$table->getColumn()->getList())){$label=$table->getColumn()->getList()[$key];}
                if ($cont > 0) {$separator = ",";}
                 if(is_a($value, 'DateTime')){$v="";}//revie when is object or date
                else if(is_array($value)){$value="";}//review
                $out.=$separator;
                  $value= $json->escape($value);
               $out.="$key:{value: \"$value\",label: \"$label\"}";
               $cont++;
         }
      $out.= "},";
     return $out;
}
?>