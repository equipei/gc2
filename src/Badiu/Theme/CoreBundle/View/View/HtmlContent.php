
<?php
$table1=$utildata->getVaueOfArray($page->getData(), 'badiu_table1');
$listdataformadmin=$utildata->getVaueOfArray($table1->getRow()->getList(), '0.adminformattemptdata',true);
 

$urledit=null;
$labeledit=$translator->trans('badiu.system.action.edit');;
if ($router->getRouteCollection()->get($page->getCrudroute()->getEdit()) !== null) {
	$id=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('id');
	$parentid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
	$currenturl=$utilapp->getCurrentUrl();
	if($id){
		$fparam=array('id'=>$id,'_urlgoback'=>$currenturl);
		if($parentid){$fparam['parentid']=$parentid;}
		$urledit=$utilapp->getUrlByRoute($page->getCrudroute()->getEdit(),$fparam);
	}
	
	
}

 
 ?>
<div id="_badiu_theme_base_view_vuejs">

	 <?php if($urledit){echo "<a href=\"$urledit\"  class=\"btn btn-outline-primary\">$labeledit</a><br />";}?>
    <!-- Simple list-->
    <?php if(empty($columnsgroup)) {?>
   
 
    <table class="table table-condensed table-hover  bootgrid-table">

        <tr v-for="(item, index) in tabledata">
            <td v-html="item.label"></td> 
            <td v-html="item.value"></td> 
        </tr>

    </table>
    <br /><br />

    <?php } ?><!-- end simple list-->

     <!-- group list-->
     <?php if(!empty($columnsgroup)) {
         $html="";
         foreach ($columnsgroup  as $kg => $group){
             $title= $translator->trans($kg);
            $html.= "<div class=\"card\">";
            $html.= "<div class=\"card-header\">$title</div>";
            $html.= "<div class=\"card-body\">";
            if(!empty($group)){
                $html.="<table class=\"table table-condensed table-hover  bootgrid-table\">";
                foreach ($group  as $column){
                    $html.= " <tr>";
                    $html.= "<td style=\"width: 15%\" v-html=\"rowdata.$column.label\" > </td> ";
                    $html.= "<td v-html=\"rowdata.$column.value\" > </td>";
                    $html.= " </tr>";
                }
                $html.= "</table>";
            }

                
            $html.= " </div>";
            $html.= "  </div>";

            $html.= "<hr />";
         }
     
     echo  $html;
     } ?><!-- end group list-->
    <?php echo $dashboardtemplate->makeRowsListTable($datarowscount);?>   
	
	<!-- print adminform -->
	<?php
	if(is_array($listdataformadmin)){
		 $htmladfd="";
		 foreach ($listdataformadmin  as $formgroup){
			 $title=$utildata->getVaueOfArray($formgroup, 'name');
			 $items=$utildata->getVaueOfArray($formgroup, 'items');
			
			 $htmladfd.= "<div class=\"card\">";
             $htmladfd.= "<div class=\"card-header\">$title</div>";
             $htmladfd.= "<div class=\"card-body\">";
			 if(!empty($items) && is_array($items)){
                $htmladfd.="<table class=\"table table-condensed table-hover  bootgrid-table\">";
                foreach ( $items  as  $fitem){
					
					 $name=$utildata->getVaueOfArray($fitem, 'name');
					 $value=$utildata->getVaueOfArray($fitem, 'value');
                    $htmladfd.= " <tr>";
                    $htmladfd.= "<td style=\"width: 15%\"> $name</td> ";
                    $htmladfd.= "<td>$value </td>";
                    $htmladfd.= " </tr>";
                }
                $htmladfd.= "</table>";
            }
			 
			$htmladfd.= " </div>";
            $htmladfd.= "  </div>";
		  }
		  
		echo $htmladfd;
	}
	
	?>   
</div>