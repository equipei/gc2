
 <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsDateComponent.php"));?> 
 <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Form/VuejsMakeDataFactory.php"));?> 
 
<script>
 <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Component/Tinymce.php"));?>    
 var badiuthembaseformapp =new Vue({
el: '#_badiu_theme_base_form_vuejs',
	components: {
		vuejsDatepicker
	},
        data: {
			//status: success | open 
			//typemessage: error | info
            formcontrol: {status: "open", message: "", haserror: false, typemessage: 'error', processing: false},
			serviceurl: "<?php echo $utilapp->getServiceUrl();?>",
			currenturl: "<?php echo $utilapp->getCurrentUrl();?>",
			servicekey: "<?php echo $servicekey;?>",
			currentroute: "<?php echo $currentroute;?>",
			urlgoback: "<?php echo urlencode($utilapp->getCurrentUrl());?>",
           <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsDataLib.php"));?> 
                
            badiuform: {
                      <?php echo makeDataFormField($container,$page,$formfields);?>    
                    },
                 badiuformoptions: {
                      <?php echo makeDataFormOption($container,$formfields);?>    
                 },
                 badiuautocomplete: {
                       <?php echo makeDataAutocomplete($container,$page,$formfields);?>
                 },
                 badiuaerrormessage: {
                       <?php echo makeErrorMessage($container,$formfields);?>
                 },
                 badiuaerrorcontrol: {
                       <?php echo makeErrorControl($container,$formfields);?>
                 },
                 badiurequiredcontrol: {
                       <?php echo makeRequiredControl($container,$formfields);?>
                 },
               BADIU_THEME_BASE_APP_VUEJS_DATA

				
            }, watch: {
                    <?php echo makeWatchAutocomplete($container,$formfields);?>
                     BADIU_THEME_BASE_APP_VUEJS_WATCH
                },
            methods: {  
		<?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsMethodLib.php"));?> 
		//review e change to isparamempty
             
             
                   
             <?php echo makeMethodAutocomplete($container,$formfields);?> 
            <?php echo makeMethodAutocompleteRemoveItemSelect($container,$page,$formfields);?> 
			<?php echo makeMethodFileImageUploadOnChange($container,$page,$formfields);?> 
			<?php echo makeMethodFileUploadOnChange($container,$page,$formfields);?> 
             
             <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Form/Add/VuejsMethodCore.php"));?> 
			 <?php echo makeMethodMask($container,$page,$formfields);?> 
			 
                 BADIU_THEME_BASE_APP_VUEJS_METHOD
				
            },
            created: function(){
				this.badiu_default_mask_fields();
				BADIU_THEME_BASE_APP_VUEJS_CREATED
               
            }
});


</script>

 BADIU_THEME_BASE_APP_JS_GENERAL
