
<?php 
function makeDataFormField($container,$page,$formfields){
 $houtjs="";
 $utildata = $container->get('badiu.system.core.lib.util.data');
  $djson = $container->get('badiu.system.core.lib.util.json');
 /*echo "<pre>";
 print_r($formfields);
 echo "</pre>";exit;*/
  if(!is_array($formfields)){return $houtjs;}
        foreach ($formfields as $field){
            $name= $utildata->getVaueOfArray($field, 'name');
            $type= $utildata->getVaueOfArray($field, 'type');
            $value=$utildata->getVaueOfArray($field, 'value');

            if($type=='badiufilterdate'){
               $defaultvalueoperator=$utildata->getVaueOfArray($value, 'operator');
               $defaultvaluedate1=$utildata->getVaueOfArray($value, 'date1');
               $defaultvaluedate1hour=$utildata->getVaueOfArray($value, 'hour1');
               $defaultvaluedate1minute=$utildata->getVaueOfArray($value, 'minute1');
               $defaultvaluedate2=$utildata->getVaueOfArray($value, 'date2');
               $defaultvaluedate2hour=$utildata->getVaueOfArray($value, 'hour2');
               $defaultvaluedate2minute=$utildata->getVaueOfArray($value, 'minute2');
               $houtjs.=$name."_badiudateoperator: '".$defaultvalueoperator."',";
               $houtjs.=$name."_badiudate1: '".$defaultvaluedate1."',";
               $houtjs.=$name."_badiuhour1: '".$defaultvaluedate1hour."',";
               $houtjs.=$name."_badiuminute1: '".$defaultvaluedate1minute."',";
               $houtjs.=$name."_badiudate2: '".$defaultvaluedate2."',";
               $houtjs.=$name."_badiuhour2: '".$defaultvaluedate2hour."',";
               $houtjs.=$name."_badiuminute2: '".$defaultvaluedate2minute."',";
             
            }else if($type=='badiufilternumber'){
               $defaultvalueoperator=$utildata->getVaueOfArray($value, 'operator');
               $defaultvaluenumber1=$utildata->getVaueOfArray($value, 'number1');
               $defaultvaluenumber2=$utildata->getVaueOfArray($value, 'number2');
               $houtjs.=$name."_badiunumberoperator: '".$defaultvalueoperator."',";
               $houtjs.=$name."_badiunumber1: '".$defaultvaluenumber1."',";
               $houtjs.=$name."_badiunumber2: '".$defaultvaluenumber2."',";
             
            }else if($type=='badiutimeperiod'){
               $defaultvalueoperator=$utildata->getVaueOfArray($value, 'operator');
			   $defaultvalueunittime=$utildata->getVaueOfArray($value, 'unittime');
               $defaultvaluenumber=$utildata->getVaueOfArray($value, 'number');
              $houtjs.=$name."_badiutimeperiodoperator: '".$defaultvalueoperator."',";
			  $houtjs.=$name."_badiutimeperiodunittime: '".$defaultvalueunittime."',";
              $houtjs.=$name."_badiutimeperiodnumber: '".$defaultvaluenumber."',";
              
            }else if($type=='badiuhourtime'){
               $defaultvaluehour=$utildata->getVaueOfArray($value, 'hour');
               $defaultvalueminute=$utildata->getVaueOfArray($value, 'minute');
              $houtjs.=$name."_badiuhourtimehour: '".$defaultvaluehour."',";
              $houtjs.=$name."_badiuhourtimeminute: '".$defaultvalueminute."',";
		
            }else if($type=='badiuchoicetext'){
               $defaultvaluefield1=$utildata->getVaueOfArray($value, 'field1');
               $defaultvaluefield2=$utildata->getVaueOfArray($value, 'field2');
              $houtjs.=$name."_badiuchoicetextfield1: '".$defaultvaluefield1."',";
              $houtjs.=$name."_badiuchoicetextfield2: '".$defaultvaluefield2."',";
              
            }
            else if($type=='badiuhour'){
               $defaultvaluehour=$utildata->getVaueOfArray($value, 'hour');
               $defaultvalueminute=$utildata->getVaueOfArray($value, 'minute');
              $houtjs.=$name."_badiutimehour: '".$defaultvaluehour."',";
              $houtjs.=$name."_badiutimeminute: '".$defaultvalueminute."',";
              
            }else if($type=='date'){
                 $houtjs.=$name."_badiutdate: '".$value."',";
              
            }else if($type=='datetime'){
                 $houtjs.=$name."_badiutdate: '".$value."',";
              
            }
            else if($type=='badiudatedefault'){
                $houtjs.=$name."_badiudatedefault: '".$value."',";
             
           }  else if($type=='badiudatetimedefault'){
            $houtjs.=$name."_badiutdatetimedefault: '".$value."',";
         
             }else if($type=='badiuautocomplete' || $type=='badiuautocompleteselectone' || $type=='badiuautocompleteforceselectone'  || $type=='badiuautocompleteforceselectmultiple' ){
                $defaultvaluefield1=$utildata->getVaueOfArray($value, 'field1');
                $defaultvaluefield2=$utildata->getVaueOfArray($value, 'field2');
				$defaultvaluefield2=$djson->escape($defaultvaluefield2);
                $houtjs.=$name.": '".$defaultvaluefield1."',";
                $houtjs.=$name."_badiuautocompleteitemname: '".$defaultvaluefield2."',";
				$itemchanged="";
					if(!empty($defaultvaluefield1) && !empty($defaultvaluefield2)){
						$houtjs.=$name."_badiuautocompleteitemchanged: true,";
					}else{
						$houtjs.=$name."_badiuautocompleteitemchanged: false,";
					}
				
              }else if($type=='badiuautocompleteselectmultiple'){
				   $defaultvaluefield1=$utildata->getVaueOfArray($value, 'field1');
				   $houtjs.=$name.":".$defaultvaluefield1.",";
			  }
			 else if($type=='badiufileimage'){
				  if(empty($value)){$value="";}
				  if($value=='null'){$value="";}
				  $houtjs.=$name.": '".$value."',";
					$fullurl="";
					$filename="";
					if(!empty($value)){
						$baseurl=$container->get('badiu.system.core.lib.util.app')->getBaseUrl();
						$fullurl= $baseurl."/system/file/get/$value";
						 $p=explode("/",$value);
						  $filename=$utildata->getVaueOfArray($p,1);
					}
					$houtjs.=$name."_badiufileimagefullurl: '".$fullurl."',";
					$houtjs.=$name."_badiufileimagename: '".$filename."',";
					$houtjs.=$name."_badiufileuploadprogress: 0,";
         
             }
			 	else if($type=='badiufile'){
					 if(empty($value)){$value="";}
					 if($value=='null'){$value="";}
				    $houtjs.=$name.": '".$value."',";
					$fullurl="";
					$filename="";
					if(!empty($value)){
						$baseurl=$container->get('badiu.system.core.lib.util.app')->getBaseUrl();
						$fullurl= $baseurl."/system/file/get/$value";
						 $p=explode("/",$value);
						  $filename=$utildata->getVaueOfArray($p,1);
					}
					$houtjs.=$name."_badiufilefullurl: '".$fullurl."',";
					$houtjs.=$name."_badiufilename: '".$filename."',";
					$houtjs.=$name."_badiufileuploadprogress: 0,";
         
             }
         else if($type=='checkbox'){
                 $dcbvalues=makeDefaultCheckboxValue($value,$djson);
                 $houtjs.=$name.": [$dcbvalues],";
                 if(is_array($value)){

                 }
            }
			else if($type=='choice'){
                 $houtjs.=$name.": '".$value."',";

            }else if($type=='badiuchoiceautocomplete'){
				 $v1=$utildata->getVaueOfArray($value, 'field3');
                 $houtjs.=$name."v1: '".$v1."',";

				$defaultvaluefield1=$utildata->getVaueOfArray($value, 'field1');
                $defaultvaluefield2=$utildata->getVaueOfArray($value, 'field2');
                $houtjs.=$name.": '".$defaultvaluefield1."',";
                $houtjs.=$name."_badiuautocompleteitemname: '".$defaultvaluefield2."',";
				$itemchanged="";
					if(!empty($defaultvaluefield1) && !empty($defaultvaluefield2)){
						$houtjs.=$name."_badiuautocompleteitemchanged: true,";
					}else{
						$houtjs.=$name."_badiuautocompleteitemchanged: false,";
					}
            }
			else{
				if(is_array($value)){ $houtjs.=$name.": '',";} //review it
                else{$houtjs.=$name.": '".$djson->escape($value)."',";}
            }
            //echo $houtjs;
        }
        
        //add parentid key / value
        $parentidkey=$page->getConfig()->getParenttablecolumn();
        $parentidvalue=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
        if(!empty($parentidkey) && !empty($parentidvalue)){
            $houtjs.= $parentidkey.": '". $parentidvalue."',";
        }
    return $houtjs;                             
}
function makeErrorMessage($container,$formfields){
 $houtjs="";
 if(empty($formfields)){return $houtjs; }
 if(!is_array($formfields)){return $houtjs; }
 $utildata = $container->get('badiu.system.core.lib.util.data');
        foreach ($formfields as $field){
            $name= $utildata->getVaueOfArray($field, 'name');
            $msg="";
            $required=$utildata->getVaueOfArray($field, 'required');
            //if($required){$msg="Preencha esta campo";}
            $houtjs.=$name.": '',";
            
        }
    return $houtjs;                             
}
function makeErrorControl($container,$formfields){
 $houtjs=""; 
 $utildata = $container->get('badiu.system.core.lib.util.data');
 if(!is_array($formfields)){return $houtjs;}
        foreach ($formfields as $field){
            $name= $utildata->getVaueOfArray($field, 'name');
            $houtjs.=$name.": false,";
       
        }
    return $houtjs;                             
}
function makeRequiredControl($container,$formfields){
 $houtjs=""; 

 $utildata = $container->get('badiu.system.core.lib.util.data');
 if(!is_array($formfields)){return $houtjs;}
        foreach ($formfields as $field){
            $name= $utildata->getVaueOfArray($field, 'name');
			$required= $utildata->getVaueOfArray($field, 'required');
			$vf=" false";
			if($required){$vf=" true";}
            $houtjs.=$name.": $vf,";
       
        }
    return $houtjs;                             
}

function makeDataFormOption($container,$formfields){
     $houtjs="";
	 $utildata = $container->get('badiu.system.core.lib.util.data');
     if(!is_array($formfields)){return $houtjs;}
foreach ($formfields as $field) {
        $choicelistvalues=$field['choicelistvalues'];
        $name=$field['name'];
        if($field['type']=='choice' || $field['type']=='radio' || $field['type']=='entity' || $field['type']=='badiufilterdate'  || $field['type']=='badiufilternumber'  ||  $field['type']=='badiuchoicetext'  ||  $field['type']=='badiuhour'  ||  $field['type']=='badiuchoiceautocomplete'){
           $houtjs.= $name.": ["; 
            $contcl=0;
            $sepatator=",";
            if(!empty($choicelistvalues) && is_array($choicelistvalues)){
                 foreach ($choicelistvalues as $clkey => $clvalue) {
                    $contcl++;  
					$vkey="";
					$text=$clvalue;
					if(is_array($clvalue)){
						$text= $utildata->getVaueOfArray($clvalue, 'name');
						$vkey= $utildata->getVaueOfArray($clvalue, 'shortname');
						$vk="'".$vkey."'";
						$vkey=", valuekey: $vk";
					}
                    $text="'".$text."'";
                    $v="'".$clkey."'";
                    if($contcl==sizeof($choicelistvalues)){$sepatator="";}
                    $houtjs.= "{ text: $text, value: $v $vkey }$sepatator";
                    
             }
            }
           
            $houtjs.= "],";
        }else if($field['type']=='badiutimeperiod'){
            $houtjs.= $name.": [";
            $contcl=0;
            $sepatator=",";
            if(!empty($choicelistvalues) && is_array($choicelistvalues)){
                 foreach ($choicelistvalues as $clkey => $clvalue) {
                    $contcl++;    
                    $text="'".$clvalue."'";
                    $v="'".$clkey."'";
                    if($contcl==sizeof($choicelistvalues)){$sepatator="";}
                    $houtjs.= "{ text: $text, value: $v }$sepatator";
                    
             }
            }
           
            $houtjs.= "],";

            $listoperator=array();
            $sdataoptions=$container->get('badiu.system.core.lib.date.period.form.dataoptions');
            $config=$field['config'];
            if($config=='full'){$listoperator=$sdataoptions->getOperatorFullPeriod();}
            else if($config=='past'){$listoperator=$sdataoptions->getOperatorPastPeriod();}
            else if($config=='future'){$listoperator=$sdataoptions->getOperatorFuturePeriod();}
           
            $houtjs.= $name."_operator: [";
            $contcl=0;
            $sepatator=",";
                foreach ($listoperator as $clkey => $clvalue) {
                     $contcl++;    
                    $text="'".$clvalue."'";
                    $v="'".$clkey."'";
                    if($contcl==sizeof($choicelistvalues)){$sepatator="";}
                    $houtjs.= "{ text: $text, value: $v }$sepatator";
                }
            
            $houtjs.= "],";

        }else if($field['type']=='badiuhourtime'){
            $sdataoptions=$container->get('badiu.system.core.lib.date.period.form.dataoptions');
            $listhour=$sdataoptions->getHours(true);
            $listminutes=$sdataoptions->getMinutes(true);
            
            //list hour
            $houtjs.= $name."_hour: [";
            $contcl=0;
            $sepatator=",";
                foreach ($listhour as $clkey => $clvalue) {
                     $contcl++;    
                    $text="'".$clvalue."'";
                    $v="'".$clkey."'";
                    if($contcl==sizeof($choicelistvalues)){$sepatator="";}
                    $houtjs.= "{ text: $text, value: $v }$sepatator";
                }
            
            $houtjs.= "],";
           
            //list minutes
            $houtjs.= $name."_minute: [";
            $contcl=0;
            $sepatator=",";
                foreach ($listminutes as $clkey => $clvalue) {
                     $contcl++;    
                    $text="'".$clvalue."'";
                    $v="'".$clkey."'";
                    if($contcl==sizeof($choicelistvalues)){$sepatator="";}
                    $houtjs.= "{ text: $text, value: $v }$sepatator";
                }
            
            $houtjs.= "],";
        } 
               
 }
return $houtjs;
}

function makeDataDefaultFormOption($container,$formfields){
    $houtjs="";
    if(!is_array($formfields)){return $houtjs;}
    $utildata = $container->get('badiu.system.core.lib.util.data');
    $showhourminutelist=false;
    foreach ($formfields as $field){
        $type= $utildata->getVaueOfArray($field, 'type');
        if($type=='badiufilterdate'  || $type=='badiuhour'){
            $showhourminutelist=true;
         break;
        }

    }
    if(!$showhourminutelist){return $houtjs;}

    $sdataoptions=$container->get('badiu.system.core.lib.date.period.form.dataoptions');
    $listhour=$sdataoptions->getHours(true);
    $listminutes=$sdataoptions->getMinutes(true);
    
    //list hour
    $houtjs.= "hour: [";
    $contcl=0;
    $sepatator=",";
        foreach ($listhour as $clkey => $clvalue) {
             $contcl++;    
            $text="'".$clvalue."'";
            $v="'".$clkey."'";
            if($contcl==sizeof($listhour)){$sepatator="";}
            $houtjs.= "{ text: $text, value: $v }$sepatator";
        }
    
    $houtjs.= "],";
   
    //list minutes
    $houtjs.= "minute: [";
    $contcl=0;
    $sepatator=",";
        foreach ($listminutes as $clkey => $clvalue) {
             $contcl++;    
            $text="'".$clvalue."'";
            $v="'".$clkey."'";
            if($contcl==sizeof($listminutes)){$sepatator="";}
            $houtjs.= "{ text: $text, value: $v }$sepatator";
        }
    
    $houtjs.= "],";
    return $houtjs;
}
function makeDataAutocomplete($container,$page,$formfields){
    $houtjs="";
    $utildata = $container->get('badiu.system.core.lib.util.data');
    $foperation=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
    $sysoperation=$container->get('badiu.system.core.lib.util.systemoperation');
	$djson = $container->get('badiu.system.core.lib.util.json');
    $currentkey=$page->getKey();
    $isedit=$sysoperation->isEdit($currentkey);
    if(!is_array($formfields)){return $houtjs;}
    foreach ($formfields as $field){
            $name= $utildata->getVaueOfArray($field, 'name');
            $type= $utildata->getVaueOfArray($field, 'type');
            $value=$utildata->getVaueOfArray($field, 'value');
                                    
            if($type=='badiuautocomplete' || $type=='badiuautocompleteselectmultiple' || $type=='badiuautocompleteselectone' || $type=='badiuchoiceautocomplete'){
                    
                 $defaultvaluefield1=$utildata->getVaueOfArray($value, 'field1');
                 $defaultvaluefield2=$utildata->getVaueOfArray($value, 'field2');
				 $defaultvaluefield2=$djson->escape($defaultvaluefield2);
                 $itemslectd=null;
                 if(!empty($defaultvaluefield1) && !empty($defaultvaluefield2)){
                     $itemslectd="$defaultvaluefield1 - $defaultvaluefield2";
                 }
                 
                  $houtjs.=$name.": '".$defaultvaluefield1."',";
                 $houtjs.=$name."_badiuautocompleteitemname: '".$defaultvaluefield2."',";
               
                    $houtjs.=$name."_query: '',";
                    //if($isedit ||  $foperation=='clone') {$houtjs.=$name."_valueselected: '".$itemslectd."',";}
                    if(!empty($itemslectd)) {$houtjs.=$name."_valueselected: '".$itemslectd."',";}
                    else {$houtjs.=$name."_valueselected: '',";}
                    $houtjs.=$name."_istyping: false,";
                    $houtjs.=$name."_isloading: false,";
                    $houtjs.=$name."_isshowlist: false,";
                    $houtjs.=$name."_searchresult: [],";
                    $houtjs.=$name.": '',";
                } 
                
    }
    return $houtjs;
}
function makeWatchAutocomplete($container,$formfields,$prefix=""){
    $houtjs="";
	 if(empty($formfields)){return $houtjs; }
	if(!is_array($formfields)){return $houtjs; }
 
    $utildata = $container->get('badiu.system.core.lib.util.data');
     $configchoice = $container->get('badiu.system.core.lib.form.configchoice');
     $utilapp=$container->get('badiu.system.core.lib.util.app');
    foreach ($formfields as $field){
        $name= $utildata->getVaueOfArray($field, 'name');
        $type= $utildata->getVaueOfArray($field, 'type');
        $value=$utildata->getVaueOfArray($field, 'value');
        
        //make service url
        $choicelist=$utildata->getVaueOfArray($field, 'choicelist');
        $choicelistparam=$configchoice->getConfig($choicelist);
        $kservice=$utildata->getVaueOfArray($choicelistparam, 'service');
        $kfunction=$utildata->getVaueOfArray($choicelistparam, 'function');
		$kparam=$utildata->getVaueOfArray($choicelistparam, 'param');
		
        $ursservicesearch=$utilapp->getServiceUrl();
        $parentid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
        if($parentid){$parentid="&parentid=$parentid";}
        
        //param form moodle
        $mdlparam="";
        $pos=stripos($kservice, "badiu.ams.core.lmsmoodle.");
        $posmdlcore=stripos($kservice, "badiu.moodle.core.formdataoptions");
        if($pos!== false){
            $mdlparam="&_serviceid=\"+this.badiuform.sserviceid+\"&_datasource=servicesql";
            if($parentid){$parentid="&parentid=\"+this.badiuform.sserviceid+\"";}
        }else if($posmdlcore!== false){

            $_serviceid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
            if(!empty($_serviceid)){
                $mdlparam="&_serviceid=$_serviceid&_datasource=servicesql";
                $parentid="";
            }
        }
        
		//dinamic param
		if(!empty($kparam) && is_array($kparam)){
			$cont=0;
			//$tlen
			 foreach($kparam as $kpi){
				 $mdlparam.="&$kpi=\"+this.".$prefix."badiuform.".$kpi."+\"";
				 $cont++;
			 }
			// echo   $mdlparam;exit;
		}
		
        $ursservicesearch="$ursservicesearch?_service=$kservice".$mdlparam."&_function=$kfunction".$parentid."&";
  
	   //for moodle filter
        $mdlperam=makeParamForMoodleAutocompleteFilter($container,$formfields);
         $ursservicesearch.= $mdlperam;
         $ursservicesearch.="gsearch=";
        if($type=='badiuautocomplete'  || $type=='badiuautocompleteselectmultiple'  || $type=='badiuautocompleteselectone'  || $type=='badiuchoiceautocomplete'){  
             
             $houtjs.=" '".$prefix."badiuautocomplete.".$name."_query': _.debounce(function () {\n";
             $houtjs.="this.".$prefix."badiuautocomplete.".$name."_istyping = false;\n";
             // $houtjs.="console.log(\"watch- $name\");\n";
			  
			  if($type=='badiuautocompleteselectone'){ $houtjs.="this.".$prefix."badiuform.".$name."=this.".$prefix."badiuautocomplete.".$name."_query;\n";}
			  
              $houtjs.="}, 1000),\n";
            
            $houtjs.="'".$prefix."badiuautocomplete.".$name."_istyping': function (value) {\n";
            $houtjs.="// if (!value) {\n";
            $houtjs.="this.".$prefix."badiuautocomplete.".$name."_isloading = true;\n";
            $houtjs.="self = this;\n";
            $houtjs.="var urls=\"".$ursservicesearch."\"+this.".$prefix."badiuautocomplete.".$name."_query;\n";
            
            $houtjs.="console.log(urls);\n";
            $houtjs.="axios.get(urls).then(response => {this.".$prefix."badiuautocomplete.".$name."_searchresult = response.data.message;\n";
            $houtjs.="// console.log(response.data);\n";
            $houtjs.="});\n";
            $houtjs.="this.".$prefix."badiuautocomplete.".$name."_isloading = false;\n";
            $houtjs.="this.".$prefix."badiuautocomplete.".$name."_isshowlist = true;\n";
            $houtjs.="//  }\n";
           $houtjs.="},\n ";
           
        }

    }
 return $houtjs;
}


function makeMethodAutocomplete($container,$formfields,$prefix=""){
    $houtjs="";
	if(empty($formfields)){return $houtjs; }
	if(!is_array($formfields)){return $houtjs; }
    $utildata = $container->get('badiu.system.core.lib.util.data');
    foreach ($formfields as $field){
        $name= $utildata->getVaueOfArray($field, 'name'); 
        $type= $utildata->getVaueOfArray($field, 'type');
        $value=$utildata->getVaueOfArray($field, 'value');
                                    
        if($type=='badiuautocomplete'  || $type=='badiuchoiceautocomplete'){  
            $houtjs.=" ".$name."_itemselected: function (itemchoose) { \n";
             $houtjs.="self = this;\n";
            $houtjs.="this.".$prefix."badiuautocomplete.".$name."_isshowlist = false;\n";
          //  $houtjs.="console.log(\"selected - $name\");";
			
            $houtjs.="this.".$prefix."badiuautocomplete.".$name."_valueselected = itemchoose.id +' - '+ itemchoose.name;\n";
            $houtjs.="this.".$prefix."badiuform.".$name."= itemchoose.id;\n";
            $houtjs.="this.".$prefix."badiuform.".$name."_badiuautocompleteitemname= itemchoose.name;\n";
           // $houtjs.="console.log(\"id selecionado: \"+ this.".$prefix."badiuform.".$name.");\n";
			$houtjs.=" if(typeof this.".$name."_exec_after_itemselected === \"function\"){this.".$name."_exec_after_itemselected();}\n";
            $houtjs.="},\n";
        }else if($type=='badiuautocompleteselectmultiple'){  
            $houtjs.=" ".$name."_itemselected: function (itemchoose) { \n";
             $houtjs.="self = this;\n";
            $houtjs.="this.".$prefix."badiuautocomplete.".$name."_isshowlist = false;\n";
            //$houtjs.="console.log(\"selected - $name\");\n";
            $houtjs.="if(!this.".$prefix."badiuform.".$name.".includes(itemchoose.name)){this.".$prefix."badiuform.".$name.".push(itemchoose.name);}\n";
           // $houtjs.="this.".$prefix."badiuform.".$name."= itemchoose.id;\n";
           // $houtjs.="this.".$prefix."badiuform.".$name."_badiuautocompleteitemname= itemchoose.name;\n";
           // $houtjs.="console.log(\"id selecionado: \"+ this.".$prefix."badiuform.".$name.");\n";
			//$houtjs.="console.log(\"list: \"+ this.".$prefix."badiuform.".$name.");\n";
			
            $houtjs.="},\n";
			
			//item typed
			$houtjs.=" ".$name."_itemtyped: function () { \n";
             $houtjs.="self = this;\n";
           // $houtjs.="console.log(\"typed -\"+ this.".$prefix."badiuautocomplete.".$name."_query);\n";
            $houtjs.="if(!this.isparamempty(this.".$prefix."badiuautocomplete.".$name."_query) && !this.".$prefix."badiuform.".$name.".includes( this.".$prefix."badiuautocomplete.".$name."_query)){this.".$prefix."badiuform.".$name.".push( this.".$prefix."badiuautocomplete.".$name."_query);}\n";
            $houtjs.="this.".$prefix."badiuautocomplete.".$name."_query=\"\";\n";
			$houtjs.="this.".$prefix."badiuautocomplete.".$name."_isshowlist = false;\n";
			//$houtjs.="console.log(\"list: \"+ this.".$prefix."badiuform.".$name.");\n";
            $houtjs.="},\n";
			
			//item removed
			 $houtjs.=" ".$name."_itemremoved: function (index) { \n";
			 $houtjs.="this.".$prefix."badiuform.".$name.".splice(index,1)\n";
            $houtjs.="},\n";
        } else if($type=='badiuautocompleteselectone'){  
            $houtjs.=" ".$name."_itemselected: function (itemchoose) { \n";
             $houtjs.="self = this;\n";
            $houtjs.="this.".$prefix."badiuautocomplete.".$name."_isshowlist = false;\n";
            //$houtjs.="console.log(\"selected - $name\");\n";
            $houtjs.="this.".$prefix."badiuform.".$name."=itemchoose.name;\n";
			$houtjs.="this.".$prefix."badiuautocomplete.".$name."_query=itemchoose.name;\n";
            $houtjs.="console.log(itemchoose.name);\n";
			$houtjs.=" if(typeof this.".$name."_exec_after_itemselected === \"function\"){this.".$name."_exec_after_itemselected();}\n";
			$houtjs.="},\n";
			
        } 
      
 }
 return $houtjs;
}

function makeMethodAutocompleteRemoveItemSelect($container,$page,$formfields,$prefix=""){
    $houtjs="";
	if(empty($formfields)){return $houtjs; }
	if(!is_array($formfields)){return $houtjs; }
    $utildata = $container->get('badiu.system.core.lib.util.data');
    $foperation=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
     $sysoperation=$container->get('badiu.system.core.lib.util.systemoperation');
    $currentkey=$page->getKey();
    $isedit=$sysoperation->isEdit($currentkey);
    
    foreach ($formfields as $field){
        $name= $utildata->getVaueOfArray($field, 'name'); 
        $type= $utildata->getVaueOfArray($field, 'type');
        $value=$utildata->getVaueOfArray($field, 'value');
                                    
        if($type=='badiuautocomplete'  || $type=='badiuautocompleteselectmultiple' || $type=='badiuautocompleteselectone'  || $type=='badiuchoiceautocomplete'){  
            $houtjs.=" ".$name."_removeitemselected: function () { \n";
            $houtjs.="self = this;\n";
           /* if($isedit ||  $foperation=='clone'){
                $houtjs.="this.badiuform.".$name."_badiuautocompleteitemchanged = true;\n";
             }*/
            $houtjs.="this.".$prefix."badiuautocomplete.".$name."_isshowlist = false;\n";
            $houtjs.="this.".$prefix."badiuautocomplete.".$name."_valueselected = '';\n";
            $houtjs.="this.".$prefix."badiuautocomplete.".$name."_query = '';\n";
            $houtjs.="this.".$prefix."badiuform.".$name."= '';\n";
            $houtjs.="this.".$prefix."badiuform.".$name."_badiuautocompleteitemname = true;\n";
           // $houtjs.="console.log('item unselected');\n";
            $houtjs.="},\n";
        } 
      
 }
 return $houtjs;
}

//delete
function initDateOnLoad($container,$formfields){
     $utildata = $container->get('badiu.system.core.lib.util.data');
      $houtjs="";
    foreach ($formfields as $field){
        $name= $utildata->getVaueOfArray($field, 'name');
        $type= $utildata->getVaueOfArray($field, 'type');
        if($type=='badiufilterdate'){
                 $houtjs.='this.badiuform.'.$name.'_badiudate1=document.querySelector("input[name='.$name.'_badiudate1]").value; ';
                 $houtjs.='this.badiuform.'.$name.'_badiudate2=document.querySelector("input[name='.$name.'_badiudate2]").value; ';
        }else if($type=='date'){
           $houtjs.='this.badiuform.'.$name.'_badiutdate=document.querySelector("input[name='.$name.'_badiutdate]").value; ';
        }else if($type=='datetime'){
           $houtjs.='this.badiuform.'.$name.'_badiutdate=document.querySelector("input[name='.$name.'_badiutdate]").value; ';
        }
    }
 return $houtjs;
}
function makeParamForMoodleAutocompleteFilter($container,$formfields){
    $utildata = $container->get('badiu.system.core.lib.util.data');
    $param="";
    $cont=0;
     foreach ($formfields as $field){
            $name= $utildata->getVaueOfArray($field, 'name');
             //,lmscoursecatparent,lmscoursecat,lmscourse
            if($name=='lmscoursecat'){
               $param.="lmscoursecat=\"+this.badiuform.lmscoursecat+\"&";
            }else if($name=='lmscourse'){
               $param.="lmscourse=\"+this.badiuform.lmscourse+\"&";
            }
     }
     return $param;
}

function makeDefaultCheckboxValue($value,$djson){
    $result="";
    if(empty($value)){return $result;}
    if(is_array($value)){
        $cont=0;
        $separator="";
        foreach ($value as $v){
            if($cont >0 ){$separator=",";}
            $v=$djson->escape($v);
            $result.=$separator."'".$v."'";
            $cont++;
        }
    }
    return $result;
}

function makeMethodFileImageUploadOnChange($container,$page,$formfields){
    $houtjs="";
	if(empty($formfields)){return $houtjs; }
	if(!is_array($formfields)){return $houtjs; }
    $utildata = $container->get('badiu.system.core.lib.util.data');
    $foperation=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
     $sysoperation=$container->get('badiu.system.core.lib.util.systemoperation');
    $currentkey=$page->getKey();
    $isedit=$sysoperation->isEdit($currentkey);
    
    foreach ($formfields as $field){
        $name= $utildata->getVaueOfArray($field, 'name'); 
        $type= $utildata->getVaueOfArray($field, 'type');
        $value=$utildata->getVaueOfArray($field, 'value');
                                    
        if($type=='badiufileimage'){  
            $houtjs.=" ".$name."_badiufileimageuploadonchange: function (e) { \n";
			$houtjs.="var datakey1=\"".$name."\";\n";
			$houtjs.="var datakey2=\"".$name."_badiufileimagefullurl\";\n";
			$houtjs.="var datakey3=\"".$name."_badiufileimagename\";\n";
			$houtjs.="var files = e.target.files || e.dataTransfer.files;\n";
			$houtjs.="if (!files.length){return null;}\n";
			$houtjs.="this.coreUploadFile(files[0],datakey1,datakey2,datakey3)\n";
			$houtjs.="},\n";
        } 
      
 }
 return $houtjs;
}

function makeMethodFileUploadOnChange($container,$page,$formfields){
    $houtjs="";
	if(empty($formfields)){return $houtjs; }
	if(!is_array($formfields)){return $houtjs; }
    $utildata = $container->get('badiu.system.core.lib.util.data');
    $foperation=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
     $sysoperation=$container->get('badiu.system.core.lib.util.systemoperation');
    $currentkey=$page->getKey();
    $isedit=$sysoperation->isEdit($currentkey);
    
    foreach ($formfields as $field){
        $name= $utildata->getVaueOfArray($field, 'name'); 
        $type= $utildata->getVaueOfArray($field, 'type');
        $value=$utildata->getVaueOfArray($field, 'value');
                                    
        if($type=='badiufile'){  
            $houtjs.=" ".$name."_badiufileuploadonchange: function (e) { \n";
			$houtjs.="var datakey1=\"".$name."\";\n";
			$houtjs.="var datakey2=\"".$name."_badiufilefullurl\";\n";
			$houtjs.="var datakey3=\"".$name."_badiufilename\";\n";
			$houtjs.="var files = e.target.files || e.dataTransfer.files;\n";
			$houtjs.="if (!files.length){return null;}\n";
			$houtjs.="this.coreUploadFile(files[0],datakey1,datakey2,datakey3)\n";
			$houtjs.="},\n";
        } 
      
 }
 return $houtjs;
}
function makeMethodMask($container,$page,$formfields){
    $houtjs="";
	if(empty($formfields)){return $houtjs; }
	if(!is_array($formfields)){return $houtjs; }
    $utildata = $container->get('badiu.system.core.lib.util.data');
    $foperation=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
     $sysoperation=$container->get('badiu.system.core.lib.util.systemoperation');
    $currentkey=$page->getKey();
    $isedit=$sysoperation->isEdit($currentkey);
    $jsmask="";
    foreach ($formfields as $field){
        $name= $utildata->getVaueOfArray($field, 'name'); 
        $type= $utildata->getVaueOfArray($field, 'type');
        $value=$utildata->getVaueOfArray($field, 'value');
                                    
        if($type=='badiudatedefault'){  
             $jsmask.=" $(\"#$name\").mask(\"99/99/9999\");\n";
		}else if($type=='numberint'){ 
             $jsmask.=" $(\"#$name\").mask(\"#.###.###.###.###.###.###.###.###.###\", {reverse: true}); \n";
		} else if($type=='numberdouble'){ 
             $jsmask.=" $(\"#$name\").mask(\"#.###.###.###.###.###.###.###.###.###,##\", {reverse: true}); \n";
		} 
      
 }
$houtjs=" badiu_default_mask_fields: function () {
    $(document).ready(function() {
		$jsmask
      });
 },";
 return $houtjs;
}
?>