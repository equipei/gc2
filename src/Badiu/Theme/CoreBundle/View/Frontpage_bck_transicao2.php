<?php
require_once("CoreLib.php");
require_once("ExternalClient.php");
   $adicionalcontents=$page->getAdditionalContents();
?>
<div class="card-columns">
<?php  
  foreach ($adicionalcontents as $litem) {
  
    foreach ($litem as $item) {
        if(isset($item['position']) && $item['position']=='content'){
        $url=$item['link']['url']; 
        $name=$item['link']['name']; 
        $description=$item['link']['description'];

      
   ?>
<?php if($page->getIsexternalclient()){?>
    <div class="card" >
    <div class="card-header"><h5><a href="<?php echo $url; ?>"><?php echo $name; ?></a></h5></div>
    <div class="card-body" style="height: 7rem;">

        <p class="card-text"> <?php echo $description; ?> </p>
        
       
    </div>
    <div class="card-footer"> <a href="<?php echo $url; ?>" class="btn btn-primary">Acessar</a></div>
    </div>
    <?php } ?>

    <?php if(!$page->getIsexternalclient()){?>
<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><a href="<?php echo $url; ?>" ><?php echo $name; ?></a></h4>
        </div>
        <div class="panel-body"style="height:80px;padding:5px;">
            <?php echo $description; ?> 
        </div>
    </div>
</div>
    <?php } ?>
<?php
    
 //  if($cont==count($litem) && $gridcont <4 ){ echo  $endrow;}
} // end  if(isset($item['position']) && $item['position']=='content'){
   } //end foreach ($litem as $item) {
    
  }//end  foreach ($adicionalcontents as $litem) {



    
?>

</div> <!-- end card-deck -->