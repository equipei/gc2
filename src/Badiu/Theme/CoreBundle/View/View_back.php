
<?php 
require_once("CoreLib.php");
require_once("ExternalClient.php");

$table=$page->getData()['badiu_table1'];
/*echo "<pre>";
print_r($page->getData());
echo "</pre>";*/
$utiljson=$container->get('badiu.system.core.lib.util.json');

?>
<br /><br />
     <div id="_badiu_theme_base_view_vuejs">
                    <table class="table table-condensed table-hover  bootgrid-table">
                       
                           <tr v-for="(item, index) in tabledata">
                             <td v-html="item.label"></td> 
                             <td v-html="item.value"></td> 
                           </tr>
                      
                    </table>
                      </div>

  <script>
                new Vue({
                    el: '#_badiu_theme_base_view_vuejs',
                    data: {
                                               
                            tabledata : [
                                 <?php
                                 $cont=0;
                                 $separator="";
                                 
                                 foreach ($table->getColumn()->getList()  as $key => $item) {
                                     $value=null;
                                     if(isset($table->getRow()->getList()[0][$key] )){
                                          $value=$table->getRow()->getList()[0][$key] ;
                                          $value= $utiljson->escape($value);
                                     }
                                    
                                    if($cont > 0){ $separator=",";}
                                   ?>
                                   <?php echo $separator; ?>
                                   {"key":"<?php echo $key; ?>","value": "<?php echo $value; ?>","label": "<?php echo $item; ?>"}
                                   <?php $cont++; ?>
                                <?php } ?>
                            ]
                        
                    },
                    methods: {
                        updateDate: function () {

                            console.log(this.list);
                           
                        }
                    }
                });   

        </script>