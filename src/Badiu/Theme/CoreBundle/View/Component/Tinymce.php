<?php

$filemanagerpluginurl=$utilapp->getResourseUrl('bundles/badiuthemecore/component/tinymce/filemanager/filemanager/plugin.min.js');
$filemanagerpath=$utilapp->getResourseUrl('bundles/badiuthemecore/component/tinymce/filemanager/filemanager/');
$baseurl=$container->get('badiu.system.core.lib.util.app')->getBaseUrl();

?>
Vue.component('vue-tinymce', {
			  template: '<textarea :id="id" :value="value"></textarea></textarea>',
			  props : {
			    value: {
			      type: String,
			      default: ''
			    },
			    id: {
			      type: String,
			      default: 'editor'
			    },
			    toolbar: {
			      default: 'fullscreen | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager media textpattern'
			    }
			  },
			  mounted () {
				self=this;
			    tinymce.init({
			      selector: '#' + this.id,
				  paste_data_images: true,
			      plugins : [
			        "advlist autolink lists link image imagetools charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking save table directionality emoticons template paste textpattern responsivefilemanager textpattern"
			      ],
				  textpattern_patterns: [
					{start: '*', end: '*', format: 'italic'},
					{start: '**', end: '**', format: 'bold'},
					{start: '#', format: 'h1'},
					{start: '##', format: 'h2'},
					{start: '###', format: 'h3'},
					{start: '####', format: 'h4'},
					{start: '#####', format: 'h5'},
					{start: '######', format: 'h6'},
					{start: '1. ', cmd: 'InsertOrderedList'},
					{start: '* ', cmd: 'InsertUnorderedList'},
					{start: '- ', cmd: 'InsertUnorderedList'},
					{start: '//brb', replacement: 'Be Right Back'}
				],
			      toolbar : this.toolbar,      
			      inline: false,
			      language: 'pt_BR',
			      resize : false,
			      statusbar : false,
			      branding : false,
			      min_height: 100,
			      code_dialog_height: 200,
			      init_instance_callback: (editor) => {
			        editor.on('KeyUp', (e) => {
			          this.$emit('input', editor.getContent());
			        });
			        editor.on('Change', (e) => { 
			          this.$emit('input', editor.getContent());
			        });
			      },
			      //external_filemanager_path:"<?php echo $filemanagerpath?>",
				  relative_urls : false,
				  remove_script_host : false,
			      //filemanager_title:"Gerenciador de fichereiros" ,
			      //external_plugins: { "filemanager" : "<?php echo $filemanagerpluginurl;?>"},
				  document_base_url: "<?php echo $baseurl;?>/system/file/get/",
			       file_picker_callback: function(callback, value, meta) {
    // File type
    if (meta.filetype =="media" || meta.filetype =="image" || meta.filetype =="file") {
		console.log('SITUATION 1');
		var input = document.getElementById('badiutextediorfileupload');
        input.click();
        input.onchange = function () {
            var file = input.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
			
			var fd = new FormData();
		fd.append("file",file);
		fd.append('filetype',meta.filetype);
		<?php 
                $paramtoken=$container->get('badiu.auth.core.login.token')->getToken($page->getIsexternalclient());
                if(!empty($paramtoken)){echo " fd.append('_token', '".$paramtoken."'); ";}
            ?>
		var url="<?php echo $baseurl;?>/system/file/set/single";
		 
			axios.post(url,fd).then(function (response) {
				console.log(response.data); 
				 if(response.data.status=='accept'){
					var filepath=response.data.message.code+"/"+response.data.message.name;
					callback(filepath);
				 }
				
               }).catch(function (error) { 
				console.log('---OCORREU ERRO----');
				   if(error.response!== undefined &&  error.response.data!== undefined){
					var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					console.log(resperror);
				   }
				 
                });
               
            };
            reader.readAsDataURL(file);
        };
		
	
    
	
     }
		        
   },
	 })
	}
});
