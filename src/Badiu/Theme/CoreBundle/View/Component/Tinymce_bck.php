<?php
$filemanagerpluginurl=$utilapp->getResourseUrl('bundles/badiuthemecore/component/tinymce/filemanager/filemanager/plugin.min.js');
$filemanagerpath=$utilapp->getResourseUrl('bundles/badiuthemecore/component/tinymce/filemanager/filemanager/');
?>
Vue.component('vue-tinymce', {
			  template: '<textarea :id="id" :value="value"></textarea>',
			  props : {
			    value: {
			      type: String,
			      default: ''
			    },
			    id: {
			      type: String,
			      default: 'editor'
			    },
			    toolbar: {
			      default: 'fullscreen | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager media'
			    }
			  },
			  mounted () {
			    tinymce.init({
			      selector: '#' + this.id,
			      plugins : [
			        "advlist autolink lists link image imagetools charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking save table directionality emoticons template paste textpattern responsivefilemanager"
			      ],
			      toolbar : this.toolbar,      
			      inline: false,
			      language: 'pt_BR',
			      resize : false,
			      statusbar : false,
			      branding : false,
			      min_height: 100,
			      code_dialog_height: 200,
			      init_instance_callback: (editor) => {
			        editor.on('KeyUp', (e) => {
			          this.$emit('input', editor.getContent());
			        });
			        editor.on('Change', (e) => { 
			          this.$emit('input', editor.getContent());
			        });
			      },
			      //external_filemanager_path:"<?php echo $filemanagerpath?>",
				  relative_urls : false,
				  remove_script_host : false,
			      //filemanager_title:"Gerenciador de fichereiros" ,
			      //external_plugins: { "filemanager" : "<?php echo $filemanagerpluginurl;?>"},
				  document_base_url: 'http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/core/file/download/',
			       file_picker_callback: function(callback, value, meta) {
    // File type
    if (meta.filetype =="media" || meta.filetype =="image" || meta.filetype =="file") {

      // Trigger click on file element
      jQuery("#fileupload").trigger("click");
      $("#fileupload").unbind('change');
      // File selection
      jQuery("#fileupload").on("change", function() {
         var file = this.files[0];
         var reader = new FileReader();
		                
         // FormData
         var fd = new FormData();
         var files = file;
         fd.append("file",files);
         fd.append('filetype',meta.filetype);

         var filename = "";
		                
         var url="http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/file/upload/exec/single";
		 var data=fd;
		 var self = this;
		 
			axios.post(url, data).then(function (response) {
				    self.filename = response.data;
				console.log(response.data); 
				 if(response.data.status=='accept'){
					var filepath=response.data.message.code+"/"+response.data.message.name;
					callback(filepath);
				 }
				
               }).catch(function (error) { 
				  console.log(error.response);
				 
                }); 			   
	
				reader.readAsDataURL(file);
       });
     }
		        
   },
			    })
			  }
			});