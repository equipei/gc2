<?php

require_once("CoreLib.php");
require_once("ExternalClientJs.php");
$utilapp=$container->get('badiu.system.core.lib.util.app');
$utilapp->setSessionhashkey($page->getSessionhashkey());
$utiljson=$container->get('badiu.system.core.lib.util.json');
$utildata = $container->get('badiu.system.core.lib.util.data');
$urservicebase=$utilapp->getServiceUrl();
$fileprocessformfilter=$utilapp->getFilePath($page->getConfig()->getFileprocessformfilter());

$currentroute = $page->getKey();
$currentroutebase=$container->get('badiu.system.core.lib.config.keyutil')->removeLastItem($currentroute);
$querystringsystem=$container->get('badiu.system.core.lib.http.querystringsystem');
$parentid=$querystringsystem->getParameter('parentid');
$mkey=$querystringsystem->getParameter('_mkey');
$editparentidparam="";
if($parentid){
	$editparentidparam="?parentid=$parentid";
	if(!empty($mkey)){$editparentidparam="$editparentidparam&_mkey=$mkey";}
}else{
	if(!empty($mkey)){$editparentidparam="?_mkey=$mkey";}
}	
$urledit = "";
$urlcopy = "";
$urladd = "";

$errorcode= $utildata->getVaueOfArray($page->getData(),'badiu_error_code');
$errormessage= $utildata->getVaueOfArray($page->getData(),'badiu_error_message');

$pagetype=$page->getConfig()->getType();
$currenturl=$utilapp->getCurrentUrl();
$schedulerthisreporturl=$currenturl;
$schedulerthisreporturl.="&_urlgoback=".urlencode($currenturl);
$schedulerthisreporturl.="&_badiuscheduleraction=_badiusystemreporschedulernew";
//params:  {_service: "badiu.system.core.lib.sql.factorysqlservice",bkey: "badiu.system.user.user", _function:"update",customchar3:$scope.user.firstName,customtext1:$scope.user.email,_token:$scope.token,id:$scope.userid},
$table = $utildata->getVaueOfArray($page->getData(),'badiu_table1');
if ($router->getRouteCollection()->get($page->getCrudroute()->getEdit()) !== null ) {
    if($pagetype=='scheduler'){
		if($page->getIsexternalclient()){
			$query="parentid=$parentid&_serviceid=$parentid&_datasource=servicesql&_service=badiu.system.scheduler.task.util&_function=redirectToEditLink&_urlgoback=$currenturl&id_badiu_empty_param_";
		 	 $urledit ="BADIU_CORE_SERVICE_CLIENTE_URLBASE?$query";
		}
         else{
			 
			 $urledit = $router->generate('badiu.system.core.service.process', array('_service'=>'badiu.system.scheduler.task.util','_function'=>'redirectToEditLink','_urlgoback'=>$currenturl,'id' => '_badiu_empty_param_'));
			$urledit = str_replace('_badiu_empty_param_', "", $urledit);
		 }
		 $paramurlcopy=array('_service'=>'badiu.system.scheduler.task.util','_function'=>'redirectToEditLink','_urlgoback'=>$currenturl,'_foperation'=>'clone','id' => '_badiu_empty_param_');
		 if(!empty($mkey)){$paramurlcopy['_mkey']=$mkey;}
         $urlcopy = $router->generate('badiu.system.core.service.process',$paramurlcopy );
         $urlcopy = str_replace('_badiu_empty_param_', "", $urlcopy); 
    }else{
        $urledit = $router->generate($page->getCrudroute()->getEdit(), array('id' => '_badiu_empty_param_'));
        $urledit = str_replace('_badiu_empty_param_', "", $urledit);
    }
    
}
if ($router->getRouteCollection()->get($page->getCrudroute()->getCopy()) !== null && $router->getRouteCollection()->get($page->getCrudroute()->getAdd()) !== null) {
    $paramlink=array();
    $paramlink['foperation']='clone';
    if($parentid){$paramlink['parentid']=$parentid;}
	 if(!empty($mkey)){$paramlink['_mkey']=$mkey;}
    $paramlink['id']='_badiu_empty_param_';
    if(!$pagetype=='scheduler'){ 
        $urlcopy = $router->generate($page->getCrudroute()->getAdd(),  $paramlink);
        $urlcopy = str_replace('_badiu_empty_param_', "", $urlcopy);
    }
 
}
$paramlink=array();
if($parentid){$paramlink['parentid']=$parentid;}
 if(!empty($mkey)){$paramlink['_mkey']=$mkey;}
 

if ($router->getRouteCollection()->get($page->getCrudroute()->getAdd()) !== null ) {
   
	$permission=$container->get('badiu.system.access.permission');
	$canadd= $permission->has_access($page->getCrudroute()->getAdd(),$page->getSessionhashkey());
	if($canadd){$urladd= $utilapp->getBaseUrl(false).$router->generate($page->getCrudroute()->getAdd(),$paramlink); }
}

//for  schedule
$currentparamfilter=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
if(empty($currentparamfilter)){$currentparamfilter=array();}
if(!empty($parentid)){$currentparamfilter['parentid']=$parentid;}
$currentparamfilter=$json->encode($currentparamfilter);
$currentroute=$page->getKey();
$currentservicedata=$container->get('badiu.system.core.lib.config.keyutil')->removeLastItem($currentroute);
$currentservicedata=$currentservicedata.".data";


?>
 
 <?php

ob_start();
   include_once("Index/VuejsAppFactory.php");
    $indexappvuejs = ob_get_contents();
  ob_end_clean();

  ob_start();
   $htmlcontenturl=$utilapp->getFilePath($page->getConfig()->getFileprocessindexhtmlcontent());
  include_once($htmlcontenturl);
  $htmlcontent = ob_get_contents();
  ob_end_clean();
  
  ob_start();
   include_once("Interaction/Default/HtmlContent.php");
  // $htmlcontent .= ob_get_contents();
  ob_end_clean();
 
  

  

 $scheduleredit=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_badiuscheduleraction');

$head=head($container,$page,$urladd);

$hiddenformfilter=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_hiddenformfilter');

if($scheduleredit=='_badiusystemreporscheduleredit'){
    echo "<div id=\"_badiu_theme_base_index_vuejs\">";
    include_once("Index/SchedulerEdit.php");
     echo "</div>"; 
    echo $indexappvuejs;  
}else{
    echo $head;

    if(!empty($errorcode)){
       echo "<div  class=\"alert alert-danger\"> $errormessage<br />  Codigo do erro:<br /> $errorcode </div>";
    }else{
        
        echo "<div id=\"_badiu_theme_base_index_vuejs\">";
      
        $htmlcontentportionbeforeurl=$utilapp->getFilePath($page->getConfig()->getFileprocessindexhtmlcontentportionbefore());
        if(file_exists($htmlcontentportionbeforeurl)){require_once($htmlcontentportionbeforeurl);}
 
          
         echo "<div v-if=\"scheduleformcontroladdnew==0\">";
        //include_once($fileprocessformfilter);
        $factoryformfilter=$container->get('badiu.theme.core.lib.template.vuejs.factoryform');
		$factoryformfilter->setSessionhashkey($page->getSessionhashkey());
        $factoryformfilter->setPage($page);
		
        if(!$hiddenformfilter){echo $factoryformfilter->exec();}
        echo  $htmlcontent; 
        echo "</div>"; // end scheduleformcontroladdnew==0

        echo "<div v-if=\"scheduleformcontroladdnew==1\">";
            echo "<h3 class=\"scheduletitle\"> Agendar Relatorio</h3>";
            echo "<a  class=\"btn btn-outline-primary\" @click=\"changeStatusScheduleformcontroladdnew(0)\">Voltar ao relatório</a>";
            if($page->getViewsytemtype()=='bootstrap4'){include_once("Index/Scheduler/HtmlContentEditBootstrap4.php");}
            else if($page->getViewsytemtype()=='bootstrap3'){include_once("Index/Scheduler/HtmlContentEditBootstrap3.php");} 
            else if($page->getViewsytemtype()=='bootstrap2'){include_once("Index/Scheduler/HtmlContentEditBootstrap2.php");}
			else {include_once("Index/Scheduler/HtmlContentEditBootstrap4.php");}
        echo "</div>"; 

        echo "</div>"; 
        echo $indexappvuejs;
    }   
}

require_once("ExternalClientCss.php");

function head($container,$page,$urladd){
    $operation=$page->getOperation();
	
	$translator=$container->get('translator');	
	$addlabel=$translator->trans($page->getCrudroute()->getAdd());
	
    $linkadd="";
    if(!empty($urladd)){ $linkadd="<a class=\"btn btn-primary\" href=\"$urladd\" role=\"button\">$addlabel</a>";};
    
    $html="<div class=\"row\">"; 
    $html.=" <div class=\"col-sm-6\"><h3 class=\"page-header\">$operation</h3></div>";
    $html.=" <div class=\"col-sm-6 text-right\">$linkadd</div>";
    $html.="</div>";
    return $html;
}


function iconProcess($container) {
    $html="";
    $utilapp=$container->get('badiu.system.core.lib.util.app');
    $urliconprocess=$utilapp->getResourseUrl('bundles/badiuthemecore/image/icons/processing.gif');
    $html="<img src=\"$urliconprocess\" v-show=\"scheduleformcontrol.processing\">";
    return $html;
}


 ?>
