<?php
include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardhtmlcontent()));
include_once("VuejsMakeDataFactory.php");
include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsDateComponent.php"));
include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Form/VuejsMakeDataFactory.php"));


$formconfig1 = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig1');
$formfields1 = $utildata->getVaueOfArray($formconfig1, 'formfields');

$formconfig2 = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig2');
$formfields2 = $utildata->getVaueOfArray($formconfig2, 'formfields');

$settkeysession="";
$gettkeysession="";

if ($page->getIsexternalclient()) {
    $clientsession=$container->get('badiu.auth.core.login.clientsession');
	$clientsession->setSessionhashkey($page->getSessionhashkey());
	$settkeysession=$clientsession->setSessionstorage();
	$gettkeysession=$clientsession->getSessionstorage();
} 

$servicekey="badiu.system.core.functionality.form.service";
$currenturl = $utilapp->getCurrentUrl(true);
$currentroute = $page->getKey();
?> 
<script>
<?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Component/Tinymce.php"));?>    
var badiuthembasedashboardapp =new Vue({ 
el: '#_badiu_theme_core_dashboard_vuejs',
     components: {
      <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjscomponent()));?> 
      },
        data: {
				formcontrol: {status: "open", message: "", haserror: false,processing: false},
				serviceurl: "<?php echo $utilapp->getServiceUrl();?>",
			    currenturl: "<?php echo $utilapp->getCurrentUrl();?>",
				servicekey: "<?php echo $servicekey;?>",
				currentroute: "<?php echo $currentroute;?>",
			    urlgoback: "<?php echo urlencode($utilapp->getCurrentUrl());?>",
				clientsession: "",
				 <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsDataLib.php"));?> 
				ffilter: {
					 badiuform: {
                      <?php echo makeDataFormField($container,$page,$formfields1);?>    
                    },
					badiuformoptions: {
                      <?php echo makeDataFormOption($container,$formfields1);?>    
					},
					badiuautocomplete: {
                       <?php echo makeDataAutocomplete($container,$page,$formfields1);?>
					},
					badiuaerrormessage: {
                       <?php echo makeErrorMessage($container,$formfields1);?>
					},
					badiuaerrorcontrol: {
                       <?php echo makeErrorControl($container,$formfields1);?>
					},
                 
				 },
				fdata: {
					  badiuform: {
                      <?php echo makeDataFormField($container,$page,$formfields2);?>    
                    },
					badiuformoptions: {
                      <?php echo makeDataFormOption($container,$formfields2);?>    
					},
					badiuautocomplete: {
                       <?php echo makeDataAutocomplete($container,$page,$formfields2);?>
					},
					badiuaerrormessage: {
                       <?php echo makeErrorMessage($container,$formfields2);?>
					},
					badiuaerrorcontrol: {
                       <?php echo makeErrorControl($container,$formfields2);?>
					},
				 },
               
                <?php   echo makeDashboardRow($page, $utildata,$json);?> 
                <?php   echo makeDashboardRows($page, $utildata,$json);?>     
               <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjsvariable()));?> 
					
            }, watch: {
                    <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjswatch()));?> 
                },
             methods: {
             <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsMethodLib.php"));?> 
			 <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Form/Add/VuejsMethodCore.php"));?> 
              <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjsmethod()));?> 
           
            },
            created: function(){
				  <?php echo $settkeysession; ?> 
				  <?php echo $gettkeysession;?> 
                  <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjscreated()));?> 
            } 
});

</script>
<?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjsgeneral()));?> 
