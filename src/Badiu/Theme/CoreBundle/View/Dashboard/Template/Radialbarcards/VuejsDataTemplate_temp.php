chartText<?php  echo $chart->index;?>: { 
  chart: {
    height: 280,
    type: "radialBar",
  },
  series: [<?php  echo $chart->percentage;?>],
  colors: ["#20E647"],
  plotOptions: {
    radialBar: {
      startAngle: -135,
      endAngle: 135,
      track: {
        background: '#333',
        startAngle: -135,
        endAngle: 135,
      },
      dataLabels: {
        name: {
          show: true,
        },
        value: {
          fontSize: "20px",
          show: true
        }
      }
    }
  },
  fill: {
    type: "gradient",
    gradient: {
      shade: "dark",
      type: "horizontal",
      gradientToColors: ["#87D4F9"],
      stops: [0, 100]
    }
  },
  stroke: {
    lineCap: "butt"
  },
  labels: ["<?php  echo $chart->title;?>"]
},