chartText<?php  echo $chart->index;?>: { 
  chart: {
    height: 280,
    type: "radialBar",
  },
  series: [<?php  echo $chart->percentage;?>],
  colors: ["#20E647"],
  plotOptions: {
    radialBar: {
     
      dataLabels: {
        name: {
          show: true,
        },
        value: {
          fontSize: "20px",
          show: true
        }
      }
    }
  },
  
  stroke: {
    lineCap: "butt"
  },
  labels: ["<?php  echo $chart->title;?>"]
},