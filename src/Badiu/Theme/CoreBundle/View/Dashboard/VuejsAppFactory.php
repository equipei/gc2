<?php
include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardhtmlcontent()));
include_once("VuejsMakeDataFactory.php");
include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsDateComponent.php"));
include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Form/VuejsMakeDataFactory.php"));

$formconfig1 = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig1');
$formfields1 = $utildata->getVaueOfArray($formconfig1, 'formfields');

$formconfig2 = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig2');
$formfields2 = $utildata->getVaueOfArray($formconfig2, 'formfields');

$apexchartfactorydata=$container->get('badiu.theme.core.lib.template.vuejs.apexchartfactorydata');
$settkeysession="";
$gettkeysession="";

if ($page->getIsexternalclient()) {
    $clientsession=$container->get('badiu.auth.core.login.clientsession');
	$clientsession->setSessionhashkey($page->getSessionhashkey());
	$settkeysession=$clientsession->setSessionstorage();
	$gettkeysession=$clientsession->getSessionstorage();
} 
$servicekey="badiu.system.core.functionality.form.service";
$currenturl = $utilapp->getCurrentUrl(true);
$currentroute = $page->getKey();

	$permission=$container->get('badiu.system.access.permission');
	$currentroutepkey=$container->get('badiu.system.core.lib.config.keyutil')->removeLastItem($currentroute);
	$permissionadd=$permission->has_access($currentroutepkey.'.add',$page->getSessionhashkey());
	$permissionedit=$permission->has_access($currentroutepkey.'.edit',$page->getSessionhashkey());
	$permissionremove=$permission->has_access($currentroutepkey.'.remove',$page->getSessionhashkey());
	$permissiondelete=$permission->has_access($currentroutepkey.'.delete',$page->getSessionhashkey());

	$permissionmanage=0;
	if($permissionadd==1 || $permissionedit==1 ||  $permissionremove==1 || $permissiondelete==1){$permissionmanage=1;}

	$badiuSession = $container->get('badiu.system.access.session');
	$badiuSession->setHashkey($page->getSessionhashkey());
	$isloggedin="1";
	if($badiuSession->get()->getUser()->getAnonymous()){$isloggedin="0";}
	
$parentid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
$freportid = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_freportid');
if($freportid){$parentid=$freportid;}
$prefixid="";
if(!empty($parentid)){$prefixid="-".$parentid;}

?> 

<script>
<?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Component/Tinymce.php"));?>  


var badiuthembasedashboardapp =new Vue({ 
el: '#_badiu_theme_core_dashboard_vuejs<?php echo $prefixid;?>',
     components: {
		 vuejsDatepicker,
      <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjscomponent()));?> 
	  apexchart: VueApexCharts,
      },
        data: {
				
				formcontrol: {
					ffilter: {status: "open", message: "", haserror: false,processing: false},
					fdata: { status: "open", message: "", haserror: false,processing: false}
				},
				 
				serviceurl: "<?php echo $utilapp->getServiceUrl();?>",
			    currenturl: "<?php echo $utilapp->getCurrentUrl();?>",
			    urlgoback: "<?php echo urlencode($utilapp->getCurrentUrl());?>",
				servicekey: "<?php echo $servicekey;?>",
				currentroute: "<?php echo $currentroute;?>",
				clientsession: "",
				isloggedin: "<?php echo $isloggedin;?>",
				
               <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsDataLib.php"));?> 
			   <?php   	if($page->getConfig()->getDashboarddatatype()=='unit'){?> dlist:{ <?php }?>
               <?php   echo makeDashboardRow($page, $utildata,$json);?> 
               <?php   echo makeDashboardRows($page, $utildata,$json);?>   
				<?php   	if($page->getConfig()->getDashboarddatatype()=='unit'){?> }, <?php }?>			   
               <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjsvariable()));?> 

			 <?php  echo $apexchartfactorydata->makeDataRows($page->getData());?> 

				badiudefaultformoptions: {
                      <?php echo makeDataDefaultFormOption($container,$formfields1);?>    
					},
				ffilter: {
					 badiuform: {
                      <?php echo makeDataFormField($container,$page,$formfields1);?>    
                    },
					
					badiuformoptions: {
                      <?php echo makeDataFormOption($container,$formfields1);?>    
					},
					badiuautocomplete: {
                       <?php echo makeDataAutocomplete($container,$page,$formfields1);?>
					},
					badiuaerrormessage: {
                       <?php echo makeErrorMessage($container,$formfields1);?>
					},
					badiuaerrorcontrol: {
                       <?php echo makeErrorControl($container,$formfields1);?>
					},
					badiurequiredcontrol: {
                       <?php echo makeRequiredControl($container,$formfields1);?>
					}
				 },
				fdata: {
					  badiuform: {
                      <?php echo makeDataFormField($container,$page,$formfields2);?>    
                    },
					badiuformoptions: {
                      <?php echo makeDataFormOption($container,$formfields2);?>    
					},
					badiuautocomplete: {
                       <?php echo makeDataAutocomplete($container,$page,$formfields2);?>
					},
					badiuaerrormessage: {
                       <?php echo makeErrorMessage($container,$formfields2);?>
					},
					badiuaerrorcontrol: {
                       <?php echo makeErrorControl($container,$formfields2);?>
					},
				 },

				 badiupermission: {
                       add: <?php echo $permissionadd;?>,
					   edit: <?php echo $permissionedit;?>,
					   remove: <?php echo $permissionremove;?>,
					   delete: <?php echo $permissiondelete;?>,
					   manage: <?php echo  $permissionmanage;?>,
					  
					},
            }, watch: {
					<?php echo makeWatchAutocomplete($container,$formfields1,"ffilter.");?>
                    <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjswatch()));?> 
                },
             methods: {
				<?php echo makeMethodAutocomplete($container,$formfields1,"ffilter.");?> 
				<?php echo makeMethodAutocompleteRemoveItemSelect($container,$page,$formfields1,"ffilter.");?> 
               <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Lib/VuejsMethodLib.php"));?> 
               <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjsmethod()));?> 
			   <?php  include_once($utilapp->getFilePath("BadiuThemeCoreBundle:View/Dashboard/VuejsMethodCore.php"));?> 
            },
            created: function(){
				  <?php echo $settkeysession; ?> 
				  <?php echo $gettkeysession;?> 
                  <?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjscreated()));?> 
            } 
});

</script>
<?php  include_once($utilapp->getFilePath($page->getConfig()->getFileprocessdashboardjsgeneral()));?> 
