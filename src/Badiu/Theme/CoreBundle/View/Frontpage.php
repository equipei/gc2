<?php
require_once("CoreLib.php");
require_once("ExternalClientJs.php");
   $adicionalcontents=$page->getAdditionalContents();

  // if($page->getViewsytemtype()=='bootstrap2'){echo cardBootstrap2($adicionalcontents); }
  //  else if($page->getViewsytemtype()=='bootstrap3'){echo cardBootstrap3($adicionalcontents); }
  //  else if($page->getViewsytemtype()=='bootstrap4'){echo cardBootstrap4($adicionalcontents,$utildata);echo accordionBootstrap4($adicionalcontents,$utildata); }
	
	$layoutfactorylist=$container->get('badiu.theme.core.lib.template.layout.factorylist');
	$lparam=array();
	if($page->getViewsytemtype()=='bootstrap2'){$lparam['boostrap']=2;}
	else if($page->getViewsytemtype()=='bootstrap3'){$lparam['boostrap']=3;}
	else if($page->getViewsytemtype()=='bootstrap4'){$lparam['boostrap']=4;}
	$layoutfactorylist->setParam($lparam);
	$layoutfactorylist->initParam();
	
	$listcards=$layoutfactorylist->getMenuItem($adicionalcontents,'content','link');
	$listcat=$layoutfactorylist->getMenuItem($adicionalcontents,'menu','category');
	 echo $layoutfactorylist->makeGrid($listcards);
	  echo $layoutfactorylist->makeAccordion($listcat);
    require_once("ExternalClientCss.php");

/*
function cardBootstrap4($adicionalcontents,$utildata){
//echo "<pre>";
//print_r($adicionalcontents);
//echo "</pre>";exit; 
	
  foreach ($adicionalcontents as $litem) {
   $gridcont=1;
   $rowcont=1;
   $cont=0;
   $starrow="<div class=\"row badiu-row-$rowcont\" id=\"badiu-row-id-$rowcont\">";
   $endrow="</div> <hr />";
   $showrowcardnumber=3;
   $html="";

    foreach ($litem as $item) {
		$type=$utildata->getVaueOfArray($item,'type');
		$position=$utildata->getVaueOfArray($item,'position');
        if($position=='content' && $type=='link'){
            $url=$item['link']['url']; 
            $name=$item['link']['name']; 
            $description=$item['link']['description'];
			$keyid=$item['link']['id'];

            if($gridcont==1){$html.= $starrow;}
            $html.="<div class=\"col $keyid\" id=\"$keyid-id\">";
             $html.="<div class=\"card\" >";
                $html.="<div class=\"card-header\"><h5> <a href=\"$url\">$name</a></h5></div> ";
                $html.="<div class=\"card-body\" style=\"height: 7rem;\">";
                    $html.=" <p class=\"card-text\">  $description </p>";
                $html.="</div>"; //end body
            $html.=" </div>"; //end card
            $html.=" </div>"; //end  col

            $gridcont++; 
            $cont++;
            if($gridcont > $showrowcardnumber){
                $gridcont=1;
				$rowcont++;
				$starrow="<div class=\"row badiu-row-$rowcont\" id=\"badiu-row-id-$rowcont\">";
                $html.= $endrow;
            } else if($cont==count($litem)){$html.=   $endrow;}
            
        } // end  if(isset($item['position']) && $item['position']=='content'){  
        } //end foreach ($litem as $item) {
    } //end  foreach ($adicionalcontents as $litem) {
     return  $html;
  }    
   
   function accordionBootstrap4($adicionalcontents,$utildata){

$html="</div><br /><br /><div class=\"row\"><div class=\"col\"><div id=\"adicional-itemmenu-accordion\">";
  foreach ($adicionalcontents as $litem) {
   $cont=0;
   
    foreach ($litem as $item) {
		$type=$utildata->getVaueOfArray($item,'type');
		$position=$utildata->getVaueOfArray($item,'position');
		
        if($position=='menu' && $type=='category'){
			
            $items=$utildata->getVaueOfArray($item,'items');
            $name=$utildata->getVaueOfArray($item,'name');
			$content="xxxxx dddd ssss $name";
            $html .=accordionItem($cont,$name,$content);
			
            $cont++;
            
        } // end  if($position=='menu' && $type=='category'){
      } //end foreach ($litem as $item) {
    } //end  foreach ($adicionalcontents as $litem) {
		
	 $html.=" </div> </div> </div>";
     return $html;
  } 


  function accordionItem($cont,$title,$content,$open=false){   
    $out="
	<div class=\"card\">
    <div class=\"card-header\" id=\"accordionitem-$cont\">
      <h5 class=\"mb-0\">
        <button class=\"btn btn-link\" data-toggle=\"collapse\" data-target=\"#collapseitem-$cont\" aria-expanded=\"false\" aria-controls=\"accordionitem-$cont\">
          $title
        </button>
      </h5>
    </div>

    <div id=\"collapseitem-$cont\" class=\"collapse\" aria-labelledby=\"accordionitem-$cont\" data-parent=\"#adicional-itemmenu-accordion\">
      <div class=\"card-body\">
        $content
      </div>
    </div>
  </div>
	";
	
	return $out;
  }
  function cardBootstrap3($adicionalcontents){        
    foreach ($adicionalcontents as $litem) {
     $gridcont=1;
     $cont=0;
	  $rowcont=1;
     $starrow="<div class=\"row badiu-row-$rowcont\" id=\"badiu-row-id-$rowcont\">";
     $endrow="</div> <hr />";
     $showrowcardnumber=3;
     $html="";
  
      foreach ($litem as $item) {
          if(isset($item['position']) && $item['position']=='content'){
               $url=$item['link']['url']; 
              $name=$item['link']['name']; 
              $description=$item['link']['description'];
			  $keyid=$item['link']['id'];
              if($gridcont==1){$html.= $starrow;}
              $html.="<div class=\"col-md-4  $keyid\" id=\"$keyid-id\">";
               $html.="<div class=\"card\" >";
                  $html.="<div class=\"card-header\"><h5> <a href=\"$url\">$name</a></h5></div> ";
                  $html.="<div class=\"card-body\" style=\"height: 7rem;\">";
                      $html.=" <p class=\"card-text\">  $description </p>";
                  $html.="</div>"; //end body
              $html.=" </div>"; //end card
              $html.=" </div>"; //end  col
  
              $gridcont++; 
              $cont++;
              if($gridcont > $showrowcardnumber){
                  $gridcont=1;
				  $rowcont++;
				   $starrow="<div class=\"row badiu-row-$rowcont\" id=\"badiu-row-id-$rowcont\">";
                  $html.= $endrow;
              } else if($cont==count($litem)){$html.=   $endrow;}
              
          } // end  if(isset($item['position']) && $item['position']=='content'){  
          } //end foreach ($litem as $item) {
      } //end  foreach ($adicionalcontents as $litem) {
       return  $html;
    }    
  function cardBootstrap2($adicionalcontents){        
    foreach ($adicionalcontents as $litem) {
     $gridcont=1;
     $cont=0;
	  $rowcont=1;
     $starrow="<div class=\"row badiu-row-$rowcont\" id=\"badiu-row-id-$rowcont\">";
     $endrow="</div> <hr />";
     $showrowcardnumber=3;
     $html="";
  
      foreach ($litem as $item) {
          if(isset($item['position']) && $item['position']=='content'){
               $url=$item['link']['url']; 
              $name=$item['link']['name']; 
              $description=$item['link']['description'];
			   $keyid=$item['link']['id'];
              if($gridcont==1){$html.= $starrow;}
              $html.="<div class=\"span4 $keyid\" id=\"$keyid-id\">";
               $html.="<div class=\"card\" >";
                  $html.="<div class=\"card-header\"><h5> <a href=\"$url\">$name</a></h5></div> ";
                  $html.="<div class=\"card-body\" style=\"height: 7rem;\">";
                      $html.=" <p class=\"card-text\">  $description </p>";
                  $html.="</div>"; //end body
              $html.=" </div>"; //end card
              $html.=" </div>"; //end  col
  
              $gridcont++; 
              $cont++;
              if($gridcont > $showrowcardnumber){
                  $gridcont=1;
				  $rowcont++;
				   $starrow="<div class=\"row badiu-row-$rowcont\" id=\"badiu-row-id-$rowcont\">";
                  $html.= $endrow;
              } 
              if($cont==count($litem) && $gridcont <$showrowcardnumber){ $html.=  $endrow;}
          } // end  if(isset($item['position']) && $item['position']=='content'){  
          } //end foreach ($litem as $item) {
      } //end  foreach ($adicionalcontents as $litem) {
       return  $html;
    }  
	*/
   ?>
