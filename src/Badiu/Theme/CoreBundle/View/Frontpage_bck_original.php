<?php
require_once("CoreLib.php");
require_once("ExternalClient.php");
   $adicionalcontents=$page->getAdditionalContents();
?>

<?php  
  foreach ($adicionalcontents as $litem) {
    foreach ($litem as $item) {
        if(isset($item['position']) && $item['position']=='content'){
        $url=$item['link']['url']; 
        $name=$item['link']['name']; 
        $description=$item['link']['description'];
   ?>


<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><a href="<?php echo $url; ?>" ><?php echo $name; ?></a></h4>
        </div>
        <div class="panel-body"style="height:80px;padding:5px;">
            <?php echo $description; ?> 
        </div>
    </div>
</div>

<?php
    } // end  if(isset($item['position']) && $item['position']=='content'){
   } //end foreach ($litem as $item) {
    
  }//end  foreach ($adicionalcontents as $litem) {
?>
