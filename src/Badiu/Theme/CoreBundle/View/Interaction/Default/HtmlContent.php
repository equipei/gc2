<hr>
<div class="badiu-chat-manager">
	

  <div class="container-interaction" v-for="(item, index) in systeminteration.ai.data">
   
   <div class="row chat-user" v-if="item.type=='user'">
    <div class="col">
		<i class="fa fa-user" aria-hidden="true"></i>
		<div class="text-user" v-html="item.message"></div>
		
		<div v-if="item.config.reportcountrowsexec" class="alert alert-secondary" role="alert">
		   <div class="labelreportrowsexec" v-html="item.config.labelreportrowsexec"></div>
		   <div> 
		     <a href="#" @click.prevent="systeminteration.ai.showdata = 1" v-if="!systeminteration.ai.showdata"><?php echo $translator->trans('badiu.system.openai.showdatasend'); ?></a>
		     <a href="#" @click.prevent="systeminteration.ai.showdata = 0" v-if="systeminteration.ai.showdata"> <?php echo $translator->trans('badiu.system.openai.hiddedatasend'); ?></a> 
		   </div>  
		   <textarea v-if="systeminteration.ai.showdata" class="form-control"  rows="3">{{item.config.content}}</textarea>
		  
		</div> 
	</div> 
  </div> 

<hr />
	<div class="row  chat-ai" v-if="item.type=='ai'">
    <div class="col">
		<i class="fa fa-desktop" aria-hidden="true"></i>
		<div  v-if="item.status=='normal'" class="text-ai" v-html="item.message"></div>
		<div v-if="item.status=='error'" class="alert alert-danger" role="alert" v-html="item.message"></div>
	</div>
   </div>
 
 
 </div> 
   
   <div class="container-chat-sender my-4 mr-3">
					<div class="form-group">
						<div class="input-group input-group-send">
							<input type="text" class="form-control" placeholder="Envie mensagem" id="caht-ai-message" @keyup.enter="systemInterationSendMessageAction()" v-model="systeminteration.ai.message" />
							<span class="input-group-addon ml-4" @click.prevent="systemInterationSendMessageAction()">
								<i class="fas fa-paper-plane"></i>
							</span>
						</div>
		</div>
  </div>
</div>

<style>
	.container-interaction .container-interaction-datasendtoanalyse {
		  color: #6c757d !important;
		  font-style: italic !important;
		  font-size: 9px; 
	}
	
  </style>