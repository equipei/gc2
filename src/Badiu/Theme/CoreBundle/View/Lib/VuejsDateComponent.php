<script>

Vue.component('badiudate', {

                template: '<input/>',

                props: [ 'format','lang','timepicker' ],

                mounted: function() {

                var self = this;

                $.datetimepicker.setLocale('pt');

                $(this.$el).datetimepicker({

                  format: this.format,
                  lang: this.lang,
                  timepicker: false,

                 /* onChangeDateTime: function(date) {

                    self.$emit('update-date', date);
                  }*/

                });
                },
                beforeDestroy: function() {

                  $(this.$el).datetimepicker('hide').datetimepicker('destroy');

                }

              }); 


//same component of badiudate difference is var timepicker: true,
Vue.component('badiudatetime', {

                template: '<input/>',

                props: [ 'format','lang','timepicker' ],

                mounted: function() {

                var self = this;

                $.datetimepicker.setLocale('pt');

                $(this.$el).datetimepicker({

                  format: this.format,
                  lang: this.lang,
                  timepicker: true,

                 /* onChangeDateTime: function(date) {

                    self.$emit('update-date', date);
                  }*/

                });
                },
                beforeDestroy: function() {

                  $(this.$el).datetimepicker('hide').datetimepicker('destroy');

                }

              }); //fim
              
</script>
