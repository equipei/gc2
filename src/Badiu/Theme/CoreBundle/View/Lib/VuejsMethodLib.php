  badiuserivicesync: function (url,param) {
	    return axios.post(url,param);
	  
  },
   isparamempty: function (param) {
	   if(param== undefined){return true;}
	   if(typeof param=== 'object'){return false;}
		 if(typeof param=== 'string'){param=param.trim();}
		if(param=== null){return true;}
		if(param=== ''){return true;}
		return false; 
	 
  },

	defaultDatepickerFormat(date) {
		return moment(date).format('DD/MM/YYYY');
	},

	formatCurrency(value) {
		if(this.isparamempty(value)){return null;}
		value=parseFloat(value);
		return value.toLocaleString(this.format.country, this.format.currency);
	},
	formatNumberdouble(value) {
		//value=parseInt(value);
		return value.toLocaleString(this.format.country, this.format.numberdouble);
	},
	
	coreUploadFile: function (file,datakey1,datakey2,datakey3) {
      var self=this;
	  var fd = new FormData();
		fd.append("file",file);
		fd.append('filetype','image');
				<?php
					$baseurl=$container->get('badiu.system.core.lib.util.app')->getBaseUrl();
					$fullurlget= $baseurl."/system/file/get/";
					$fullurlset= $baseurl."/system/file/set/single";
					?>
				var url="<?php echo $fullurlset;?>";
		 
			axios.post(url,fd,
								{
									headers: {
										'Content-Type': 'multipart/form-data'
									},
									onUploadProgress: function( progressEvent ) {
									var prog = parseInt( Math.round( ( progressEvent.loaded * 100 ) / progressEvent.total ) );
									self.badiuform[datakey1+'_badiufileuploadprogress']=prog;
									}.bind(this)
								}
						).then(function (response) {
				console.log(response.data); 
			
				 if(response.data.status=='accept'){
					var filepath=response.data.message.code+"/"+response.data.message.name;
					console.log(filepath);
					self.badiuform[datakey1]=filepath;
					
					self.badiuform[datakey2]='<?php echo $fullurlget;?>'+filepath;;
					self.badiuform[datakey3]=response.data.message.name;
				 } 
				
               }).catch(function (error) { 
				console.log('---OCORREU ERRO----');
				   if(error.response!== undefined &&  error.response.data!== undefined){
					var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					console.log(resperror);
				   }
				 
                });
               
       
    },
	removeFileUpload: function (datakey1,datakey2,datakey3) {
		this.badiuform[datakey1]=null;
		this.badiuform[datakey2]=null;
		this.badiuform[datakey3]=null;
		this.badiuform[datakey1+'_badiufileuploadprogress']=0;
	},
	
	badiudatevalidade: function (date,format) {
		if(format!= 'dd/mm/yyyy'){return false;}
		var x = date;
		var reg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
		if (x.match(reg)) {
			return true;
		}
		else {
			return false;
		} 
		
		
	},
	
	badiubrcpfvalidade: function (cpf) {
		cpf = cpf.replace(/[^\d]+/g,'');	
	if(cpf == '') return false;	
	// Elimina CPFs invalidos conhecidos	
	if (cpf.length != 11 || 
		cpf == "00000000000" || 
		cpf == "11111111111" || 
		cpf == "22222222222" || 
		cpf == "33333333333" || 
		cpf == "44444444444" || 
		cpf == "55555555555" || 
		cpf == "66666666666" || 
		cpf == "77777777777" || 
		cpf == "88888888888" || 
		cpf == "99999999999")
			return false;		
	// Valida 1o digito	
	add = 0;	
	for (i=0; i < 9; i ++)		
		add += parseInt(cpf.charAt(i)) * (10 - i);	
		rev = 11 - (add % 11);	
		if (rev == 10 || rev == 11)		
			rev = 0;	
		if (rev != parseInt(cpf.charAt(9)))		
			return false;		
	// Valida 2o digito	
	add = 0;	
	for (i = 0; i < 10; i ++)		
		add += parseInt(cpf.charAt(i)) * (11 - i);	
	rev = 11 - (add % 11);	
	if (rev == 10 || rev == 11)	
		rev = 0;	
	if (rev != parseInt(cpf.charAt(10)))
		return false;		
	return true; 
	},
	
getbadiuformoptionsvaluekey: function (fieldname,value) {
	    var list=this.badiuformoptions[fieldname];
		var valuekey="";
		for (i = 0; i < list.length; i++) {
			if(list[i].value==value){valuekey=list[i].valuekey;break;};
		}
		return valuekey;
  },