<?php

namespace Badiu\Theme\CoreBundle\Twig;
use Badiu\System\CoreBundle\Model\Lib\Http\Url;
use Badiu\System\CoreBundle\Model\Report\Data\Table\BadiuReportDataTable;

class BadiuReportViewExtension extends \Twig_Extension {
	/** @var  \Twig_Environment */
	protected $env;

	/**
	 * @param \Twig_Environment $environment
	 */
	public function initRuntime(\Twig_Environment $environment) {
		$this->env = $environment;
	}

	public function getName() {
		return 'badiu_report_data_view_extension';
	}

	public function getFunctions() {
		return array(
			'badiu_report_data_view' => new \Twig_Function_Method($this, 'defaultDataTable', array($this)),
		);
	}

	public function defaultDataTable(BadiuReportDataTable $table, $attibutes = array()) {
		$url = Url::getCurrentUrl();
		$url = Url::addParam($url, "format", "xls");
		$output = "";
               // print_r($table);exit;
		//  $output .="<div class=\"panel panel-default\">";
		//  $output .="<h5 class=\"title\">Resultado:". $table->getCountrow()."  </h5>";

		//  $output .="<div class=\"panel-body\">";
		//  $output .="<a href=\"$url\">Exportar para Excel</a>";
		$output .= "<br><div class=\"table-window\">";
		$output .= "<table class=\"table table-condensed table-hover  bootgrid-table\">";
		
		foreach ($table->getColumn()->getList() as $key => $item) {
			$output .= "<tr>";
			$output .= "<td style=\"width:15px;\">$item</td>";
			$output .= "<td style=\"width:155px;\">" . $table->getRow()->getList()[0][$key] . "</td>";
			$output .= "</tr>";
			
		}

		$output .= "</tbody> ";
		$output .= "</table> ";
		$output .= "</div>";

		return $output;
	}

}