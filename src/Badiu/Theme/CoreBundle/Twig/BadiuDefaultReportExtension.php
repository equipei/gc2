<?php

namespace Badiu\Theme\CoreBundle\Twig;
use Badiu\System\CoreBundle\Model\Lib\Http\HttpQueryString;
use Badiu\System\CoreBundle\Model\Lib\Http\Url;
use Badiu\System\CoreBundle\Model\Lib\Util\SpecialCharactere;
//use Badiu\System\CoreBundle\Model\Report\BadiuPagination;
use Badiu\System\CoreBundle\Model\Report\Data\Table\BadiuReportDataTable;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class BadiuDefaultReportExtension extends \Twig_Extension {

	/**
	 * @var Container
	 */
	private $container;

	private $router;
        
        /**
	 * @var object
	 */
	private $app;
	function __construct(Container $container) {
		$this->container = $container;
		$this->router = $this->container->get("router");
                $this->app=$this->container->get('badiu.system.core.lib.util.app'); 
	}

	/** @var  \Twig_Environment */
	protected $env;

	/**
	 * @param \Twig_Environment $environment
	 */
	public function initRuntime(\Twig_Environment $environment) {
		$this->env = $environment;
	}

	public function getName() {
		return 'badiu_report_data_table_extension';
	}

	public function getFunctions() {
		return array(
			'badiu_report_data_table' => new \Twig_Function_Method($this, 'defaultDataTable', array($this)),
                        'badiu_report_data_detail' => new \Twig_Function_Method($this, 'defaultDataDetail', array($this)),
                        'badiu_general_data_dashborad' => new \Twig_Function_Method($this, 'generalDataDashborad', array($this)),
                        'badiu_general_data_frontpage' => new \Twig_Function_Method($this, 'defaultFrontpage', array($this)),
                        'badiu_core_form_add' => new \Twig_Function_Method($this, 'defaultFormAdd', array($this)),
		);
	}
        
        public function defaultFrontpage($page, $attibutes = array()) {
           
            $container=$this->container;
            $router=$this->router;
            
            // echo $page->getData()['badiu_file_process_index'];exit;
            $fileprocessfrontpage=$page->getConfig()->getFileprocessfrontpage();
            $file=$this->app->getFilePath($fileprocessfrontpage);
            if(!file_exists($file)){return "file $fileprocessfrontpage | $file not found"; }
          
            
            ob_start();
             include_once($file);
             $contents = ob_get_contents();
            ob_end_clean();
         
        return $contents;
        }
	public function defaultDataTable($page, $attibutes = array()) {
            $table=$page->getData()['badiu_table1'];
            $container=$this->container;
            $router=$this->router;
            
            // echo $page->getData()['badiu_file_process_index'];exit;
            $fileprocessindex=$page->getConfig()->getFileprocessindex();
            $file=$this->app->getFilePath($fileprocessindex);
            if(!file_exists($file)){return "file $fileprocessindex | $file not found"; }
          
            
        
		//$pagination = new BadiuPagination($table->getCountrow(), $table->getPageindex());
                $pagination=$this->container->get('badiu.system.core.report.pagination');
                $pagination->start($table->getCountrow(), $table->getPageindex());
		$infoResult = $pagination->getInfo();
		//$urltoextport = Url::getCurrentUrl();
		$urltoextport =$container->get('badiu.system.core.lib.util.app')->getCurrentUrl();
		$urltoextport = Url::addParam($urltoextport, "_format", "xls");
                $pagingout=$this->pagination($table->getCountrow(), $table->getPageindex());
                 ob_start();
             include_once($file);
             $contents = ob_get_contents();
            ob_end_clean();
         
        return $contents;
		$output = "";
		//  $output .="<div class=\"panel panel-default\">";
		//   $output .="<h5 class=\"title\">Resultado: ". $infoResult."  </h5>";

		//  $output .="<div class=\"panel-body\">";
		// $output .="<a href=\"$url\" class=\"pull-left btn-export-excel\"></a>";
		$output .= "<div class=\"report-container mb40\">";
		$output .= "<div class=\"panel\">";
		//$output .= "    <div class=\"well\">";
		$output .= " <div class=\"row-fluid\">";
		$output .= "    <div class=\"col-xs-12\">";
		$output .= "        <h5 class=\"title pull-left\">Resultado: " . $infoResult . "</h5>";
		$output .= "            <a href=\"$url\" class=\"pull-left btn-export-excel\">Exportar</a>";
		$output .= "    </div>";
		$output .= "  </div>";
		$output .= "<div class=\"table-window\">";
		$output .= "<table class=\"table table-condensed table-hover  bootgrid-table\">";
		$output .= "<thead>";
		$output .= "<tr>";
		foreach ($table->getColumn()->getList() as $item) {
			$output .= "<th>$item</th>";
		}
		$output .= "</tr>";
		$output .= "</thead>";
		$output .= "<tbody> ";
		foreach ($table->getRow()->getList() as $row) {
                   
			$output .= "<tr>";
			//foreach ($row as $v) {
                        foreach ($table->getColumn()->getList() as $key=>$value) {
                            $v="";
                            if(isset($row[$key])){$v=$row[$key];}
                            $output .= "<td>$v</td>";
			}
			$output .= "</tr>";

		}
		$output .= "</tbody> ";
		$output .= "</table> ";
		$output .= "</div>";
		$output .=$pagingout;// $this->pagination($table->getCountrow(), $table->getPageindex());
		$output .= "</div>";
		$output .= "</div>";
		$output .= "</div><!-- report-container -->";
		// $output .=  $url;
		//$output .= "</div>";

		$output .= $this->ctrCheckbox();
		$output .= $this->ctrDelete();

		return $output;
	}

        public function generalDataDashborad($page, $attibutes = array()) {
             $container=$this->container;
            $router=$this->router;
            $fileprocessdashboard=$page->getConfig()->getFileprocessdashboard();
            $file=$this->app->getFilePath($fileprocessdashboard);
            if(!file_exists($file)){return "file $fileprocessdashboard | $file not found"; }
          
            ob_start(); 
             include_once($file);
             $contents = ob_get_contents();
            ob_end_clean();
         
        return $contents;
        }
         public function defaultDataDetail($page, $attibutes = array()) {
              $container=$this->container;
            $router=$this->router;
            $table=$page->getData()['badiu_table1'];
            $fileprocessview=$page->getConfig()->getFileprocessview();
            $file=$this->app->getFilePath($fileprocessview);
            if(!file_exists($file)){return "file $fileprocessview | $file not found"; }
          
            ob_start(); 
             include_once($file);
             $contents = ob_get_contents();
            ob_end_clean();
         
        return $contents;
        }
        
        public function defaultFormAdd($page, $attibutes = array()) {
              $container=$this->container;
              $router=$this->router;
            if(!method_exists($page->getConfig(),'getFileprocessformadd')){ return null;}
            $fileprocessformadd=$page->getConfig()->getFileprocessformadd();
           
            $file=$this->app->getFilePath($fileprocessformadd);
            if(!file_exists($file)){return "file $fileprocessformadd | $file not found"; }
           
            ob_start(); 
             include_once($file);
             $contents = ob_get_contents();
            ob_end_clean();
       
        return $contents;
        }
	public function defaultDataTable1(BadiuReportDataTable $table, $attibutes = array()) {
		/*  echo "<pre>";
			            print_r($table);
		*/
		//$pagination = new BadiuPagination($table->getCountrow(), $table->getPageindex());
                $pagination=$this->container->get('badiu.system.core.report.pagination');
                $pagination->start($table->getCountrow(), $table->getPageindex());
                
		$infoResult = $pagination->getInfo();
		$url = Url::getCurrentUrl();
		$url = Url::addParam($url, "_format", "xls");
		$output = "";
		$output .= $this->tableangularjs($table, $attibutes);

		$output .= "<div  ng-controller=\"badiubasedefaulttableController\">";
		$output .= "<div class=\"report-container mb40\">";
		$output .= "<div class=\"panel panel-default\">";
		$output .= "    <div class=\"well\">";
		$output .= " <div class=\"row-fluid\">";
		$output .= "    <div class=\"col-xs-12\">";
		$output .= "        <h5 class=\"title pull-left\">Resultado: " . $infoResult . "</h5>";
		$output .= "            <a href=\"$url\" class=\"pull-left btn-export-excel\">Exportar</a>";
		$output .= "    </div>";
		$output .= "  </div>";
		$output .= "<div class=\"table-window\">";
		$output .= "<table class=\"table table-condensed table-hover  bootgrid-table\">";

		$output .= "<thead>";
		$output .= '<tr ng-repeat = "hrow in badiubasedefaulttable "  ng-if="$index == 0" >';
		$output .= '<td ng-repeat = "hitem in hrow track by $index"><b><div ng-bind-html="hitem"></div></b></td>';
		$output .= "</tr>";
		$output .= "</thead>";

		$output .= "<tbody> ";
		$output .= '<tr ng-repeat = "row in badiubasedefaulttable "  ng-if="$index != 0">';
		$output .= '<td ng-repeat = "item in row track by $index"><div ng-bind-html="item"></div></td>';
		$output .= "</tr>";
		$output .= "</tbody> ";
		$output .= "</table> ";
		$output .= "</div>";
		$output .= $this->pagination($table->getCountrow(), $table->getPageindex());
		$output .= "</div>";
		$output .= "</div>";
		$output .= "</div>";
		// $output .=  $url;
		$output .= "</div>";

		$output .= "</div>";
		$output .= $this->ctrCheckbox();
		$output .= $this->ctrDelete();

		return $output;
	}
	public function pagination($countDbrows = 0, $pageIndex = 0) {
		// echo " pageIndex: $pageIndex";
		if ($countDbrows == 0) {
			return "";
		}

		//$pagination = new BadiuPagination($countDbrows, $pageIndex);
                $pagination=$this->container->get('badiu.system.core.report.pagination');
                $pagination->start($countDbrows, $pageIndex);

		// if($pageIndex==0){$pageIndex=1;}
		$output = "";
		$output .= "<nav aria-label=\"Page navigation\">";
		$output .= "<ul class=\"pagination\">";

		$url = Url::getCurrentUrl(); //review use Util/App as service
		$pos = strripos($url, "?");
		if ($pos === false) {$url .= "?";} else {
			$url .= "&";
			$qtring = $_SERVER["QUERY_STRING"];
			if (!empty($qtring)) {
				$urlWithoutParam = substr($url, 0, $pos);

				$queryString = new HttpQueryString($qtring);
				$queryString->makeQuery();
				$queryString->remove('_page');
				$queryString->makeQuery();
				$query = $queryString->getQuery();
				$url = $urlWithoutParam . "?" . $query . "&";
			}

		}

		$cont = 0;
		// print_r($pagination->getPages());
		foreach ($pagination->getPages() as $p) {
			$cont++;
			$pbd = $p - 1;
			$link = $url . "_page=" . $pbd;
			$active = "";
			if ($pageIndex == $pbd) {
				$active = " active";
			}

			$output .= "<li  class=\"page-item $active\" ><a href=\"$link\" class=\"page-link\">$p</a></li>";
		}
		$output .= "</ul>";
		$output .= "</nav>";
		if ($cont == 1) {$output = "";}
		return $output;
	}

	public function ctrCheckbox() {
		$output = "";

//modal
		//http://getbootstrap.com/javascript/#modals

		$output .= " <script type=\"text/javascript\" >";
		$output .= "    $(document).ready(function() { ";
		$output .= "       $(\"#chckHead\").click(function() {";
		$output .= "        $('.chcktbl').prop('checked', this.checked);";
		$output .= "      });";
		$output .= " $(\".chcktbl\").click(function() { ";
		$output .= "   if ($(\".chcktbl\").length == $(\".chcktbl:checked\").length) { ";
		$output .= "    $(\"#chckHead\").prop(\"checked\", \"checked\"); ";
		$output .= "   } ";
		$output .= "  else { ";
		$output .= "   $(\"#chckHead\").removeAttr(\"checked\"); ";
		$output .= "  }   ";
		$output .= " });  ";
		$output .= "   });   ";
		$output .= "</script>";
		return $output;
	}

	public function ctrDelete() {
		//http://bootsnipp.com/snippets/featured/form-wizard-and-validation
		// http://bootsnipp.com/snippets/featured/step-wizard-working
		$output = <<<EOF
       <!--start Modal -->
<div class="modal fade" id="badiu_modal_table_delete" tabindex="-1" role="dialog" aria-labelledby="badiu_modal_table_delete_label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="badiu_modal_table_delete_label">Confirmação de exclusão</h4>
      </div>
      <div class="modal-body">
          <div id="alert"> Desejas remover este registo?</div>
        <div id="loader-ajax-img"></div>
        <div id="msg-box"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-badiu-reporter-send">Confirmar</button>
      </div>
    </div>
  </div>
</div>


<!-- end  modal -->
EOF;

		//,array('_service'=>'badiu.system.core.functionality.report.service','bkey'=>$currentroute,'_function'=>'delete',id=>10)
		$url = $this->getRouter()->generate('badiu.system.core.service.process');
		$output .= '<script type="text/javascript">';
		$output .= " var badiuModalTableDeleteUrlService ='" . $url . "';";
		$output .= ' </script>';
		return $output;

	}
	function getRouter() {
		return $this->router;
	}

	function setRouter($router) {
		$this->router = $router;
	}

	function getContainer() {
		return $this->container;
	}

	function getEnv() {
		return $this->env;
	}

	function setContainer(Container $container) {
		$this->container = $container;
	}

	function setEnv(\Twig_Environment $env) {
		$this->env = $env;
	}

	function tableangularjs(BadiuReportDataTable $table, $attibutes = array()) {
		$scharactere = new SpecialCharactere();
		$out = "";
		//http://stackoverflow.com/questions/36678928/nested-ng-repeat-for-multidimensional-array
		/*
	                     * var myColumnDefs = [
	    {key:"label", sortable:true, resizeable:true},
	    {key:"notes", sortable:true,resizeable:true},......
*/
		$out .= '
                    <script>

                      badiusysbaseapp.controller("badiubasedefaulttableController", function($scope,$http,$filter) {
                       $scope.badiubasedefaulttablehead=[ ';
		$cont = 0;
		foreach ($table->getColumn()->getList() as $key => $item) {
			if ($cont == 0) {$out .= '"' . $key . '"';} else { $out .= ',"' . $key . '"';}
			$cont++;
		}
		$out .= '];';

		$out .= '   $scope.badiubasedefaulttable=[ ';
		// ["ngSanitize"]
		//head
		$cont = 0;
		$out .= '[';
		foreach ($table->getColumn()->getList() as $item) {
			$item = $scharactere->encodeForJavaScriptArray($item);
			if ($cont == 0) {$out .= '"' . $item . '"';} else { $out .= ',"' . $item . '"';}
			$cont++;
		}
		$out .= ']';
		//list data
		$contx = 0;
		foreach ($table->getRow()->getList() as $row) {
			if ($contx == 0) {
				if ($cont == 0) {$out .= '[';} else { $out .= ',[';}
			} else { $out .= ',[';}
			$conty = 0;
			foreach ($row as $v) {
				$v = $scharactere->encodeForJavaScriptArray($v);
				if ($conty == 0) {$out .= '"' . $v . '"';} else { $out .= ',"' . $v . '"';}
				$conty++;
			}
			$out .= ']';
			$contx++;

		}

		$out .= '];';
		$out .= ' });


                     </script>

                    ';

		return $out;
	}
        function getApp() {
            return $this->app;
        }

        function setApp($app) {
            $this->app = $app;
        }


        
}
