<?php

namespace Badiu\Theme\CoreBundle\Twig;
use Badiu\System\CoreBundle\Model\Lib\Http\HttpQueryString;
use Badiu\System\CoreBundle\Model\Lib\Http\Url;
use Badiu\System\CoreBundle\Model\Report\BadiuPagination;
use Badiu\System\CoreBundle\Model\Report\Data\Table\BadiuReportDataTable;
use Badiu\System\CoreBundle\Model\Lib\Util\SpecialCharactere;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class BadiuReportExtension extends \Twig_Extension {

	/**
	 * @var Container
	 */
	private $container;

	private $router;
	function __construct(Container $container) {
		$this->container = $container;
		$this->router = $this->container->get("router");
	}

	/** @var  \Twig_Environment */
	protected $env;

	/**
	 * @param \Twig_Environment $environment
	 */
	public function initRuntime(\Twig_Environment $environment) {
		$this->env = $environment;
	}

	public function getName() {
		return 'badiu_report_data_paging_extension';
	}

	public function getFunctions() {
		return array(
			'badiu_report_paging' => new \Twig_Function_Method($this, 'getPaging', array($this)),
                        'badiu_report_paginginfo' => new \Twig_Function_Method($this, 'getPagingInfo', array($this)),
                        'badiu_report_urlexport' => new \Twig_Function_Method($this, 'getExportUrl'),
                        'badiu_report_angulardata' => new \Twig_Function_Method($this, 'getAngularjsData', array($this)),
		);
	}

	

        public function getPaging(BadiuReportDataTable $table) {
                $output = $this->pagination($table->getCountrow(), $table->getPageindex());
		return $output;
	}
         public function getPagingInfo(BadiuReportDataTable $table) {
                $pagination = new BadiuPagination($table->getCountrow(), $table->getPageindex());
		$infoResult = $pagination->getInfo();
		return $infoResult;
	}
         public function getExportUrl() {
                $url = Url::getCurrentUrl();
		$url = Url::addParam($url, "_format", "xls");
		
		return $url;
	}
       
	public function pagination($countDbrows = 0, $pageIndex = 0) {
		// echo " pageIndex: $pageIndex";
		if ($countDbrows == 0) {
			return "";
		}

		$pagination = new BadiuPagination($countDbrows, $pageIndex);

		// if($pageIndex==0){$pageIndex=1;}
		$output = "";
		$output .= "<ul class=\"pagination\">";

		$url = Url::getCurrentUrl();
		$pos = strripos($url, "?");
		if ($pos === false) {$url .= "?";} else {
			$url .= "&";
			$qtring = $_SERVER["QUERY_STRING"];
			if (!empty($qtring)) {
				$urlWithoutParam = substr($url, 0, $pos);

				$queryString = new HttpQueryString($qtring);
				$queryString->makeQuery();
				$queryString->remove('_page');
				$queryString->makeQuery();
				$query = $queryString->getQuery();
				$url = $urlWithoutParam . "?" . $query . "&";
			}

		}

		$cont = 0;
		// print_r($pagination->getPages());
		foreach ($pagination->getPages() as $p) {
			$cont++;
			$pbd = $p - 1;
			$link = $url . "_page=" . $pbd;
			$active = "";
			if ($pageIndex == $pbd) {
				$active = "class=\"active\"";
			}

			$output .= "<li $active><a href=\"$link\">$p</a></li>";
		}
		$output .= "</ul>";
		if ($cont == 1) {$output = "";}
		return $output;
	}

 function getAngularjsData(BadiuReportDataTable $table, $attibutes = array()){
                  $scharactere=new SpecialCharactere();
                  $out ="";
                  $out .= '
                  
                       $scope.badiubasedefaulttablehead=[ ';
                        $cont=0;
                          foreach ($table->getColumn()->getList() as $key => $item) {
                          if($cont==0){ $out .= '"'.$key.'"';}
                           else { $out .= ',"'.$key.'"';}
                           $cont++;
                        }
                     $out .='];'; 
                     
                    $out .= '   $scope.badiubasedefaulttable=[ ';
                       // ["ngSanitize"]
                        //head
                        $cont=0;
                        $out .='['; 
                       foreach ($table->getColumn()->getList() as $item) {
                          $item= $scharactere->encodeForJavaScriptArray($item);
                           if($cont==0){ $out .= '"'.$item.'"';}
                           else { $out .= ',"'.$item.'"';}
                           $cont++;
                        }
                     $out .=']'; 
                    //list data
                      $contx=0;
                    foreach ($table->getRow()->getList() as $row) {
			if($contx==0){
                            if($cont == 0 ){$out .= '[';}
                            else {$out .= ',[';}
                        }
                        else { $out .= ',[';}
                        $conty=0;
			foreach ($row as $v) {
                            $v= $scharactere->encodeForJavaScriptArray($v);
                                if($conty==0){ $out .= '"'.$v.'"';}
                                else { $out .= ',"'.$v.'"';}
				$conty++;
			}
                        $out .= ']';
                        $contx++;
			
                        }
                   
                     $out .= '];';
                   
             
            return $out;
         }

	function getRouter() {
		return $this->router;
	}

	function setRouter($router) {
		$this->router = $router;
	}

	function getContainer() {
		return $this->container;
	}

	function getEnv() {
		return $this->env;
	}

	function setContainer(Container $container) {
		$this->container = $container;
	}

	function setEnv(\Twig_Environment $env) {
		$this->env = $env;
	}

      
        
}
