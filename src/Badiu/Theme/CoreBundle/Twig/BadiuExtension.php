<?php

namespace Badiu\Theme\CoreBundle\Twig;

class BadiuExtension extends \Twig_Extension
{
	/** @var  \Twig_Environment */
	protected $env;

	/**
     * @param \Twig_Environment $environment
     */
	public function initRuntime(\Twig_Environment $environment)
	{
		$this->env = $environment;
    }

    public function getName()
    {
        return 'badiu_extension';
    }

    public function getFunctions()
    {
        return array(
            'badiu_js_autocomplete'  => new \Twig_Function_Method($this, 'autocomplete', array($this)),
        );
    }

	public function autocomplete($id, $url, $dataArray = array(), $fieldMain = 'q', $fieldReturn = 'name', $dataType = 'json')
	{
		$data = '';
		foreach ($dataArray as $key => $value)
			$data = $key.':'.$value.',';

		$output = '
		<script type="text/javascript">
		    $(document).ready(function () {
		        $("#'.$id.'").autocomplete({
		            source: function(request, response) {
		                $.ajax({
		                    url: "'.$url.'",
		                    dataType: "'.$dataType.'",
		                    data: {
		                        '.$fieldMain.': request.term,
		                        '.$data.'
		                    },
		                    success: function(data) {
		                        response(data.map(function (value) {
		                            return {
		                                \'label\':value.'.$fieldReturn.'
		                            };
		                        }));
		                    },
		                });
		            },
		            close: function(event, ui){
		                $(this).change();
		            }
		        });
		    });
		</script>';

		return $output;
    }
}