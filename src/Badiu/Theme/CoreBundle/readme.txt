https://stackoverflow.com/questions/38603505/what-are-jquery-and-bootstrap-version-requirements-for-bootstrap-table

[jQuery 1.7.1 > bootstrap 2.0.3]
[jQuery 1.9.1 > bootstrap 2.3.0|2.3.2]
[jQuery 1.9.1 > bootstrap 3.0.0-rc1]
[jQuery 1.10.2 > bootstrap 3.0.0]
[jQuery 1.11.2 > bootstrap 3.3.4]
[jQuery 1.12.4 > bootstrap 3.3.7]
[jQuery 1.11.3 > bootstrap 4.0.0-alpha]

[jQuery 2.2.4 > bootstrap 3.3.6]
[jQuery 3.2.1 > bootstrap 3.3.7]

[jQuery 3.1.1 > bootstrap 4.0.0-alpha6