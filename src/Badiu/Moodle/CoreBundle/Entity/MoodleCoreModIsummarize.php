<?php

namespace Badiu\Moodle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MoodleCoreModIsummarize
 *
 * @ORM\Table(name="moodle_core_mod_isummarize", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="moodle_core_mod_isummarize_moduleinstance_uix", columns={"entity", "serviceid","modulekey","moduleinstance"})},
 *       indexes={@ORM\Index(name="moodle_core_mod_isummarize_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_serviceid_ix", columns={"serviceid"}),
  *              @ORM\Index(name="moodle_core_mod_isummarize_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_moduleinstance_ix", columns={"moduleinstance"}),  
 *              @ORM\Index(name="moodle_core_mod_isummarize_timecreated_ix", columns={"timecreated"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_timemodified_ix", columns={"timemodified"}), 
 *               @ORM\Index(name="moodle_core_mod_isummarize_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_userfirstname_ix", columns={"userfirstname"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_username_ix", columns={"username"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_userlastname_ix", columns={"userlastname"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_useremail_ix", columns={"useremail"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_useridnumber_ix", columns={"useridnumber"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_cohortid_ix", columns={"cohortid"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_cohortname_ix", columns={"cohortname"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_cohortidnumber_ix", columns={"cohortidnumber"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_coursecategoryid_ix", columns={"coursecategoryid"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_coursecategoryname_ix", columns={"coursecategoryname"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_coursecategorypath_ix", columns={"coursecategorypath"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_coursecategoryidnumber_ix", columns={"coursecategoryidnumber"}),
 *              @ORM\Index(name="moodle_core_mod_isummarize_courseid_ix", columns={"courseid"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_coursename_ix", columns={"coursename"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_courseshortname_ix", columns={"courseshortname"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_courseidnumber_ix", columns={"courseidnumber"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_coursetimestart_ix", columns={"coursetimestart"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_coursetimeend_ix", columns={"coursetimeend"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_groupid_ix", columns={"courseid"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_groupname_ix", columns={"courseid"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_groupidnumber_ix", columns={"groupidnumber"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_activityid_ix", columns={"activityid"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_activitymodule_ix", columns={"activitymodule"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_activityname_ix", columns={"activityname"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_activityshortname_ix", columns={"activityshortname"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_activityidnumber_ix", columns={"activityidnumber"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_activitytimestart_ix", columns={"activitytimestart"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_activityquestionid_ix", columns={"activityquestionid"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_activityquestionname_ix", columns={"activityquestionname"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_activityquestionidnumber_ix", columns={"activityquestionidnumber"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_activityquestiontype_ix", columns={"activityquestiontype"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_responsechar_ix", columns={"responsechar"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_responseint_ix", columns={"responseint"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_responseid_ix", columns={"responseid"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_responsetime_ix", columns={"responsetime"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_responsegrade_ix", columns={"responsegrade"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customint1_ix", columns={"customint1"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customint2_ix", columns={"customint2"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customint3_ix", columns={"customint3"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customint4_ix", columns={"customint4"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customint5_ix", columns={"customint5"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customchar1_ix", columns={"customchar1"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customchar2_ix", columns={"customchar2"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customchar3_ix", columns={"customchar3"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customchar4_ix", columns={"customchar4"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customchar5_ix", columns={"customchar5"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customdec1_ix", columns={"customdec1"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customdec2_ix", columns={"customdec2"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customdec3_ix", columns={"customdec3"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customdec4_ix", columns={"customdec4"}),
*              @ORM\Index(name="moodle_core_mod_isummarize_customdec5_ix", columns={"customdec5"})})

 *
 * @ORM\Entity
 */

class MoodleCoreModIsummarize {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

	
	/**
     * @var AdminServerService
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ServerBundle\Entity\AdminServerService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="serviceid", referencedColumnName="id", nullable=false)
     * })
     */
    private $serviceid;
	
	/**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=false)
     */
    private $modulekey;


    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=false)
     */
    private $moduleinstance; 
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="userid", type="bigint", nullable=true)
	 */
	private $userid;
	
	
		/**
	@var string
	 *
	 * @ORM\Column(name="username", type="string", length=255, nullable=true)
	 */
	private $username; 
	/**
	@var string
	 *
	 * @ORM\Column(name="userfirstname", type="string", length=255, nullable=true)
	 */
	private $userfirstname; 
	
		/**
	@var string
	 *
	 * @ORM\Column(name="userlastname", type="string", length=255, nullable=true)
	 */
	private $userlastname; 
	
	
	/**
	@var string
	 *
	 * @ORM\Column(name="useremail", type="string", length=255, nullable=true)
	 */
	private $useremail; 
	
	/**
	@var string
	 *
	 * @ORM\Column(name="useridnumber", type="string", length=255, nullable=true)
	 */
	private $useridnumber; 
	
	/**
	@var string
	 *
	 * @ORM\Column(name="userinfo", type="text", nullable=true)
	 */
	private $userinfo; 
	
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="cohortid", type="bigint", nullable=true)
	 */
	private $cohortid;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="cohortname", type="string", length=255, nullable=true)
	 */
	private $cohortname; 

		/**
	@var string
	 *
	 * @ORM\Column(name="cohortidnumber", type="string", length=255, nullable=true)
	 */
	private $cohortidnumber; 
	
	/**
	@var string
	 *
	 * @ORM\Column(name="cohortinfo", type="text", nullable=true)
	 */
	private $cohortinfo; 
	
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="coursecategoryid", type="bigint", nullable=true)
	 */
	private $coursecategoryid;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="coursecategoryname", type="string", length=255, nullable=true)
	 */
	private $coursecategoryname; 
	
	/**
	@var string
	 *
	 * @ORM\Column(name="coursecategorypath", type="string", length=255, nullable=true)
	 */
	private $coursecategoryshortpath; 

	/**
	@var string
	 *
	 * @ORM\Column(name="coursecategoryidnumber", type="string", length=255, nullable=true)
	 */
	private $coursecategoryidnumber; 
	
	
		/**
	@var string
	 *
	 * @ORM\Column(name="coursecategorydescription", type="text", nullable=true)
	 */
	private $coursecategorydescription; 
	
	/**
	@var string
	 *
	 * @ORM\Column(name="coursecategorysummary", type="text", nullable=true)
	 */
	private $coursecategorysummary;
	
	
	
	/**
	@var string
	 *
	 * @ORM\Column(name="coursecategoryinfo", type="text", nullable=true)
	 */
	private $coursecategoryinfo; 
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="courseid", type="bigint", nullable=true)
	 */
	private $courseid;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="coursename", type="string", length=255, nullable=true)
	 */
	private $coursename; 
	
	/**
	@var string
	 *
	 * @ORM\Column(name="courseshortname", type="string", length=255, nullable=true)
	 */
	private $courseshortname; 

		/**
	@var string
	 *
	 * @ORM\Column(name="courseidnumber", type="string", length=255, nullable=true)
	 */
	private $courseidnumber; 
	
		/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="coursetimestart", type="datetime", nullable=true)
	 */
	private $coursetimestart;
	
		/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="coursetimeend", type="datetime", nullable=true)
	 */
	private $coursetimeend;
	
		/**
	@var string
	 *
	 * @ORM\Column(name="coursedescription", type="text", nullable=true)
	 */
	private $coursedescription; 
	/**
	@var string
	 *
	 * @ORM\Column(name="coursesummary", type="text", nullable=true)
	 */
	private $coursesummary; 
	
	/**
	@var string
	 *
	 * @ORM\Column(name="courseinfo", type="text", nullable=true)
	 */
	private $courseinfo; 
	
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="groupid", type="bigint", nullable=true)
	 */
	private $groupid;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="groupname", type="string", length=255, nullable=true)
	 */
	private $groupname; 

		/**
	@var string
	 *
	 * @ORM\Column(name="groupidnumber", type="string", length=255, nullable=true)
	 */
	private $groupidnumber; 
	
	/**
	@var string
	 *
	 * @ORM\Column(name="groupinfo", type="text", nullable=true)
	 */
	private $groupinfo; 
	
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="activityid", type="bigint", nullable=true)
	 */
	private $activityid;
	
		/**
	@var string
	 *
	 * @ORM\Column(name="activitymodule", type="string", length=255, nullable=true)
	 */
	private $activitymodule;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="activityname", type="string", length=255, nullable=true)
	 */
	private $activityname; 
	
	/**
	@var string
	 *
	 * @ORM\Column(name="activityshortname", type="string", length=255, nullable=true)
	 */
	private $activityshortname; 

		/**
	@var string
	 *
	 * @ORM\Column(name="activityidnumber", type="string", length=255, nullable=true)
	 */
	private $activityidnumber; 
	
		/** 
	 * @var \DateTime
	 *
	 * @ORM\Column(name="activitytimestart", type="datetime", nullable=true)
	 */
	private $activitytimestart;
	
		/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="activitytimeend", type="datetime", nullable=true)
	 */
	private $activitytimeend;
	
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="activityquestionid", type="bigint", nullable=true)
	 */
	private $activityquestionid;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="activityquestionname",  type="string", length=255, nullable=true)
	 */
	private $activityquestionname;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="activityquestionidnumber",  type="string", length=255, nullable=true)
	 */
	private $activityquestionidnumber;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="activityquestioncontent", type="text", nullable=true)
	 */
	private $activityquestioncontent;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="activityquestiontype", type="string", length=255, nullable=true)
	 */
	private $activityquestiontype;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="activitydescription", type="text", nullable=true)
	 */
	private $activitydescription;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="activitysummary", type="text", nullable=true)
	 */
	private $activitysummary;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="activityinfo", type="text", nullable=true)
	 */
	private $activityinfo; 
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="responseid", type="bigint", nullable=true)
	 */
	private $responseid;
	
	/** 
	 * @var \DateTime
	 *
	 * @ORM\Column(name="responsetime", type="datetime", nullable=true)
	 */
	private $responsetime;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="responsechar", type="string", length=255, nullable=true)
	 */
	private $responsechar;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="responseint", type="bigint",  nullable=true)
	 */
	private $responseint;
		/**
	@var string
	 *
	 * @ORM\Column(name="responsetext", type="text", nullable=true)
	 */
	private $responsetext; 
	
	/**
	@var string
	 *
	 * @ORM\Column(name="responsecomplete", type="string", length=255, nullable=true)
	 */
	private $responsecomplete;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="responsegrade", type="float", precision=10, scale=0, nullable=true)
     */
    private $responsegrade;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="responseteachercomment", type="text", nullable=true)
	 */
	private $responseteachercomment; 
	
	     /**
     * @var integer
     *
     * @ORM\Column(name="customint1", type="bigint",  nullable=true)
     */
    private $customint1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint2", type="bigint",  nullable=true)
     */
    private $customint2;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint3", type="bigint",  nullable=true)
     */
    private $customint3;
	
	    /**
     * @var integer
     *
     * @ORM\Column(name="customint4", type="bigint",  nullable=true)
     */
    private $customint4;
	
		    /**
     * @var integer
     *
     * @ORM\Column(name="customint5", type="bigint",  nullable=true)
     */
    private $customint5;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="customchar1", type="string", length=255, nullable=true)
     */
    private $customchar1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="customchar2", type="string", length=255, nullable=true)
     */
    private $customchar2;
    
     
    
    /**
     * @var string
     *
     * @ORM\Column(name="customchar3", type="string", length=255, nullable=true)
     */
    private $customchar3;
    
	 /**
     * @var string
     *
     * @ORM\Column(name="customchar4", type="string", length=255, nullable=true)
     */
    private $customchar4;
	
		 /**
     * @var string
     *
     * @ORM\Column(name="customchar5", type="string", length=255, nullable=true)
     */
    private $customchar5;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="customtext1", type="text", nullable=true)
     */
    private $customtext1;
    
     /**
     * @var string
     *
     * @ORM\Column(name="customtext2", type="text", nullable=true)
     */
    private $customtext2;
    
     /**
     * @var string
     *
     * @ORM\Column(name="customtext3", type="text", nullable=true)
     */
    private $customtext3;
   
 /**
     * @var string
     *
     * @ORM\Column(name="customtext4", type="text", nullable=true)
     */
    private $customtext4;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="customtext5", type="text", nullable=true)
     */
    private $customtext5;
	
	/**
     * @var float
     *
     * @ORM\Column(name="customdec1", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec1;
	/**
     * @var float
     *
     * @ORM\Column(name="customdec2", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec2;
    /**
     * @var float
     *
     * @ORM\Column(name="customdec3", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec3;
	
	/**
     * @var float
     *
     * @ORM\Column(name="customdec4", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec4;
	
	/**
     * @var float
     *
     * @ORM\Column(name="customdec5", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec5;
   
	
    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
     

/**
 * @var string
 *
 * @ORM\Column(name="param", type="text", nullable=true)
 */
	private $param;
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;


}
