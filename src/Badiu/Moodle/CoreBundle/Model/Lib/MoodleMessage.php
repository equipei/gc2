<?php

namespace Badiu\Moodle\CoreBundle\Model\Lib;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class MoodleMessage extends BadiuModelLib{

   private  $sqlservice=null;

    function __construct(Container $container) {
        parent::__construct($container);
       
    }
     /*
	 param  array('_serviceid'=>0,'useridto'=>0,'useremailto'=>'xx','useridfrom'=>0,'subject'=>'xx','message'=>'xx')
	 */
	  public function send($param){
	
	   $param['_key']='local.badiunet.core.message.message.send';
      
        $result=$this->getSqlservice()->service($param);
		$result=$this->getSqlservice()->serviceResponse($result);
		$messagesid=$this->getUtildata()->getVaueOfArray($result,'messagesid');
		if($messagesid){return true;}
		return false;
  }
  
      
    public function sendFromReportScheduler($mconfg){
	   $param=array();
	   $serviceid=$this->getUtildata()->getVaueOfArray($mconfg,'_serviceid');
	   if(empty($serviceid)){return null;}
	   $param['_serviceid']=$this->getUtildata()->getVaueOfArray($mconfg,'_serviceid');
	   $param['useremailto']=$this->getUtildata()->getVaueOfArray($mconfg,'recipient.to',true);
	   $param['subject']=$this->getUtildata()->getVaueOfArray($mconfg,'message.subject',true);
	   $param['message']=$this->getUtildata()->getVaueOfArray($mconfg,'message.body',true);
	   $param['_serviceid']=$serviceid;
	   
	   $param['_key']='local.badiunet.core.message.message.send';
      
        $result=$this->getSqlservice()->service($param);
		$result=$this->getSqlservice()->serviceResponse($result);
		$messagesid=$this->getUtildata()->getVaueOfArray($result,'messagesid');
		if($messagesid){return true;}
		return false;
  }
   public function sendNotificationFromReportScheduler($mconfg){
	   $param=array();
	   $serviceid=$this->getUtildata()->getVaueOfArray($mconfg,'_serviceid');
	   if(empty($serviceid)){return null;}
	   $param['_serviceid']=$this->getUtildata()->getVaueOfArray($mconfg,'_serviceid');
	   $param['useremailto']=$this->getUtildata()->getVaueOfArray($mconfg,'recipient.to',true);
	   $param['subject']=$this->getUtildata()->getVaueOfArray($mconfg,'message.subject',true);
	   $param['message']=$this->getUtildata()->getVaueOfArray($mconfg,'message.body',true);
	   $param['_serviceid']=$serviceid;
	   
	   $param['_key']='local.badiunet.core.message.message.notification';
      
        $result=$this->getSqlservice()->service($param);
		$result=$this->getSqlservice()->serviceResponse($result);
		$messagesid=$this->getUtildata()->getVaueOfArray($result,'messagepopupnotificationid');
		if($messagesid){return true;}
		return false;
  }
	   function getSqlservice() {

            if(empty($this->sqlservice)){
                $this->sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
                $this->sqlservice->setSessionhashkey($this->getSessionhashkey());
            }
            return $this->sqlservice;
        }

        function setSqlservice($sqlservice) {
            $this->sqlservice = $sqlservice;
        }
}
