<?php

namespace Badiu\Moodle\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class MoodleCourseActivityItem extends BadiuModelLib{
    /**
     * @var object
     */
   private  $sqlservice;
    function __construct(Container $container) {
            parent::__construct($container);
      }


    
  function getModulesCourse($courseid){
        $sql="SELECT DISTINCT m.name FROM  {_pfx}course_modules cm INNER JOIN {_pfx}modules m ON m.id=cm.module  WHERE cm.course=$courseid ";
        $result=$this->getSqlservice()->search($sql);
        return $result;
    
}   
 
 function searchCourseInstanceModule($courseid,$modulename,$text){
        $text=strtolower($text);
		$modulenamesql="'".addslashes($modulename)."'";
		$textesql="'%".addslashes($text)."%'";
		$sql="SELECT cm.id,i.name FROM {_pfx}$modulename i INNER JOIN {_pfx}course_modules cm ON cm.instance=i.id INNER JOIN {_pfx}modules m ON m.id=cm.module WHERE cm.course=i.course AND cm.course=$courseid AND m.name=$modulenamesql AND LOWER(CONCAT(cm.id,i.name)) LIKE $textesql ";
		$result=$this->getSqlservice()->search($sql);
		return $result;
     
}  

 function searchCourseInstancesModules($courseid,$text){
	    if(empty($courseid)){return null;}
		if(!is_int((int)($courseid))){return null;}
		 if(empty($text)){return null;}
        $listmodules=$this->getModulesCourse($courseid);
		
		if(!is_array($listmodules)){return null;}
		$list=array();
		foreach($listmodules as $row) {
			$modulename=$this->getUtildata()->getVaueOfArray($row,'name');
			
			$instancemdlist=$this->searchCourseInstanceModule($courseid,$modulename,$text);
			
			if(is_array($instancemdlist)){
				foreach($instancemdlist as $irow) {
					$name=$this->getUtildata()->getVaueOfArray($irow,'name');
					$id=$this->getUtildata()->getVaueOfArray($irow,'id');
					array_push($list,array('id'=>$id,'name'=>$name));
				}
			}
			
		}
		
     return $list;
}


 function get_course_modules($courseid){
	    if(empty($courseid)){return null;}
		if(!is_int((int)$courseid)){return null;}
		$listmodules=$this->getModulesCourse($courseid);
		if(!is_array($listmodules)){return null;}
		$list=array();
		foreach($listmodules as $row) {
			$modulename=$this->getUtildata()->getVaueOfArray($row,'name');
			$instancemdlist=$this->getCourseInstanceModule($courseid,$modulename);
			if(is_array($instancemdlist)){
				foreach($instancemdlist as $irow) {
					$name=$this->getUtildata()->getVaueOfArray($irow,'name');
					$id=$this->getUtildata()->getVaueOfArray($irow,'id');
					array_push($list,array('id'=>$id,'name'=>$name));
				}
			}
			
		}
		
     return $list;
}

 function getCourseInstanceModule($courseid,$modulename){
        $modulenamesql="'".addslashes($modulename)."'";
		$sql="SELECT cm.id,i.name FROM {_pfx}$modulename i INNER JOIN {_pfx}course_modules cm ON cm.instance=i.id INNER JOIN {_pfx}modules m ON m.id=cm.module WHERE cm.course=i.course AND cm.course=$courseid AND m.name=$modulenamesql ";
		$result=$this->getSqlservice()->search($sql);
        return $result;
     
}  
function getInstanceName($cmid){
        if(empty($cmid)){return null;}
		if(!is_int((int)($cmid))){return null;}
		
		$modulename=$this->getModuleName($cmid);
		$modulenamesql="'".addslashes($modulename)."'";
		$sql="SELECT i.name FROM {_pfx}$modulename i INNER JOIN {_pfx}course_modules cm ON cm.instance=i.id INNER JOIN {_pfx}modules m ON m.id=cm.module WHERE cm.id=$cmid AND m.name=$modulenamesql";
		$result=$this->getSqlservice()->execSql($sql,"S");
        $value=$this->getUtildata()->getVaueOfArray($result,'name');
		return $value;
     
} 

 function getModuleName($cmid){
	  if(empty($cmid)){return null;}
		if(!is_int((int)($cmid))){return null;}
        $sql="SELECT m.name FROM  {_pfx}course_modules cm INNER JOIN {_pfx}modules m ON m.id=cm.module  WHERE cm.id=$cmid ";
        $result=$this->getSqlservice()->execSql($sql,"S");
		$value=$this->getUtildata()->getVaueOfArray($result,'name');
		 return $value;
     
}       
   
        function getSqlservice() {

            if(empty($this->sqlservice)){
                $this->sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
                $this->sqlservice->setSessionhashkey($this->getSessionhashkey());
            }
            return $this->sqlservice;
        }

        function setSqlservice($sqlservice) {
            $this->sqlservice = $sqlservice;
        }


}
