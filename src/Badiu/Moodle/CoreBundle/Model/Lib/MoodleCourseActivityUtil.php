<?php

namespace Badiu\Moodle\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class MoodleCourseActivityUtil extends BadiuModelLib{
    /**
     * @var object
     */
   private  $sqlservice;
    function __construct(Container $container) {
            parent::__construct($container);
      }

       function getMoedulesInfo($sserviceid,$courseid,$session=true) {
                $sessionkey='badiu.moodle.core.course.modules.info.'.$sserviceid.'.'.$courseid;
                
               if($session){
                
                    $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                    $badiuSession->setHashkey($this->getSessionhashkey());
                     if($badiuSession->existValue($sessionkey)){
                        $info=$badiuSession->getValue($sessionkey);   
                         return $info;
                       }  
                }
                //get list of course activity

                $listmodule=$this->getModuleList($sserviceid,$courseid);
                $listplugins=$this->getPluginList($listmodule);
                $listmodulekv=$this->changeModuleKeyNameIntanceValueId($listmodule);
                $listinstancename=$this->getPluginsInstanceName($listmodulekv,$listplugins,$sserviceid,$courseid);
                
                
                if($session){
                    $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                    $badiuSession->setHashkey($this->getSessionhashkey());
                     $badiuSession->addValue($sessionkey,$listinstancename);
                }
                
                 return $listinstancename;
        
    }

    public function getModuleList($sserviceid,$courseid){
        $sql="SELECT cm.id,m.name,cm.instance FROM {_pfx}course_modules cm INNER JOIN {_pfx}modules m ON m.id=cm.module WHERE  cm.course=$courseid";
        $this->getSqlservice()->setServiceid($sserviceid);
        $result=$this->getSqlservice()->search($sql);
        return $result;
    }
    public function changeModuleKeyNameIntanceValueId($modules){
        $list=array();
        foreach ($modules as $d) {
            $id=$this->getUtildata()->getVaueOfArray($d,'id');
            $instance=$this->getUtildata()->getVaueOfArray($d,'instance');
            $name=$this->getUtildata()->getVaueOfArray($d,'name');
            $key="$name/$instance";
            $list[$key]=  $id;
        }
        return $list;
    }
    public function getPluginList($modules){
        $list=array();
        if(!empty($modules)){
             foreach ($modules as $d) {
                $name=$this->getUtildata()->getVaueOfArray($d,'name');
              $list[$name]=  $name;
            }
        }
        return $list;
    } 
     public function getPluginsInstanceName($listmodulekv,$listplugins,$sserviceid,$courseid){
        $list=array();
        $this->getSqlservice()->setServiceid($sserviceid);
        foreach ($listplugins as $plugin) {
            $sql="SELECT id,name FROM {_pfx}$plugin WHERE course=$courseid";
            $result=$this->getSqlservice()->search($sql);
           if(is_array($result)) {
               foreach ($result as $prow) {
                 $id=$this->getUtildata()->getVaueOfArray($prow,'id');
                 $name=$this->getUtildata()->getVaueOfArray($prow,'name');
 
                 $pkey=$plugin."/".$id;
                 $cmid=$this->getUtildata()->getVaueOfArray($listmodulekv,$pkey);
 
                 $list[$cmid]=$name;
              }
            }
          
        }
        return $list;
    } 
   
        function getSqlservice() {

            if(empty($this->sqlservice)){
                $this->sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
                $this->sqlservice->setSessionhashkey($this->getSessionhashkey());
            }
            return $this->sqlservice;
        }

        function setSqlservice($sqlservice) {
            $this->sqlservice = $sqlservice;
        }


}
