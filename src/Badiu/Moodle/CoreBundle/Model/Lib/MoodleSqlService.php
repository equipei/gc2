<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class BadiuSqlService extends BadiuModelLib{

      /**
     * @var integer
     */
    private $serviceid=null;
    function __construct(Container $container) {
            parent::__construct($container);
            $querystringsystem=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem');
            $this->serviceid=$querystringsystem->getParameter('_serviceid');
        }
         function execSql($sql,$type) {
                $response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
                $param['query']=$sql;
                $param['type']= $type;
                $param['_serviceid']=$this->serviceid;
                $wsresponse=$this->getSearch()->execSqlService($param);
                $response->set($wsresponse);
                $result=$response->getMessage();
                return $result;
         }
             
        function searchSingle($sql) {
                  $result=$this->execSql($sql,"S");
                return $result;
           }
         function search($sql) {
                $result=$this->execSql($sql,"M");
                return $result;
           }
        
           
              function getServiceid() {
                  return $this->serviceid;
              }

              function setServiceid($serviceid) {
                  $this->serviceid = $serviceid;
              }


}
