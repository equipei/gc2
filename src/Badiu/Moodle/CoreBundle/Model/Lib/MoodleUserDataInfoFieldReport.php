<?php

namespace Badiu\Moodle\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class MoodleUserDataInfoFieldReport extends BadiuSqlFilter{
    /**
     * @var object
     */
   private  $sqlservice;
    private $dateformat='d/m/Y';
    private $data;
    private $usersdata;
    private $fields;
    function __construct(Container $container) {
            parent::__construct($container);

       }

         function makeReport($param=array()) {
          $this->usersdata=$this->getUsers($param);
          $this->data=$this->getUsersData($param);
        
         
          $this->fields=$this->getAllField();
          
          $firtrow=$this->makeFirstRow($this->usersdata,$this->fields);
         
          $result=array();
          array_push($result, $firtrow);
          $this->addDefultuserdataToData();
          foreach ($this->usersdata as $user) {
              $userid=$this->getUtildata()->getVaueOfArray($user,'id');
              $nrow=array();
              foreach ($firtrow  as $fkey => $fvalue) {
                    $dkey=$userid."/".$fkey;
                    $data=null;
                    if (array_key_exists($dkey,$this->data)){$data=$this->data[$dkey];}
                    $nrow[$fkey]=$data;
              } 
              array_push($result, $nrow);
          }
       
         return $result;
          
      }
        private function getUsersWsql($param) {
          $wsql="";
          $user=$this->getUtildata()->getVaueOfArray($param,'user');
          if(!empty($user)){$wsql.=" AND LOWER(CONCAT(u.id,u.firstname,u.lastname,u.email,u.username,u.idnumber)) LIKE '%".strtolower($user)."%' ";}
         
          
          $confirmed=$this->getUtildata()->getVaueOfArray($param,'confirmed');
          if($confirmed ===0 || !empty($confirmed)){$wsql.=" AND u.confirmed =$confirmed ";}
         // print_r($param);exit;
         $deleted=$this->getUtildata()->getVaueOfArray($param,'deleted');
          if($deleted ===0 || !empty($deleted)){$wsql.=" AND u.deleted =$deleted ";}
          
          $skype=$this->getUtildata()->getVaueOfArray($param,'skype');
          if(!empty($skype)){$wsql.=" AND u.skype ='".$skype."'";}
          return $wsql;
          
          
     }
     
     private function getUsersCourseWsql($param) {
          $wsql="";
          $course=$this->getUtildata()->getVaueOfArray($param,'course');
          if(!empty($course)){$wsql.=" AND LOWER(CONCAT(c.id,c.fullname,c.shortname)) LIKE '%".strtolower($course)."%' ";}
          
           $courseid=$this->getUtildata()->getVaueOfArray($param,'courseid');
          if(!empty($courseid)){$wsql.=" AND c.id =$courseid  ";}
         
          return $wsql;
          
          
     }
     
     function countUsers($param=array()) {
        
         $courseid=$this->getUtildata()->getVaueOfArray($param,'courseid');
         $course=$this->getUtildata()->getVaueOfArray($param,'course');
         
         $sql=null;
         if($courseid || $course){
              $wsql=$this->getUsersWsql($param);
              $wsql.=$this->getUsersCourseWsql($param);
             $sql="SELECT COUNT(DISTINCT u.id) AS countrecord FROM {_pfx}role_assignments rs INNER JOIN {_pfx}user u ON u.id=rs.userid INNER JOIN {_pfx}context e ON rs.contextid=e.id INNER JOIN mdl_course c ON c.id=e.instanceid LEFT JOIN  {_pfx}user_info_data d ON u.id=d.userid  WHERE  e.contextlevel=50 AND e.instanceid = c.id  $wsql ";
          }
         else{
             $wsql=$this->getUsersWsql($param);
             $sql="SELECT COUNT(DISTINCT u.id) AS countrecord  FROM {_pfx}user u LEFT JOIN  {_pfx}user_info_data d ON u.id=d.userid WHERE u.id > 0 $wsql";
             }
          $result=$this->getSqlservice()->searchSingle($sql);
          $result =$this->getUtildata()->getVaueOfArray($result,'countrecord');
        
         return  $result;
     }
     
       function getUsers($param=array()) {
         $offset=0;
         $limit=10;
         if(isset($param['course'])){$course=$param['course'];}
         if(isset($param['offset'])){ $offset=$param['offset'];}
         if(isset($param['limit'])){ $limit=$param['limit'];}
         $offset=$offset*$limit;
          $courseid=$this->getUtildata()->getVaueOfArray($param,'courseid');
           $course=$this->getUtildata()->getVaueOfArray($param,'course');
         $sql=null;
         if($courseid || $course){
              $wsql=$this->getUsersWsql($param);
              $wsql.=$this->getUsersCourseWsql($param);
              $sql="SELECT DISTINCT u.id,u.email,u.firstname,u.lastname,u.username FROM {_pfx}role_assignments rs INNER JOIN {_pfx}user u ON u.id=rs.userid INNER JOIN {_pfx}context e ON rs.contextid=e.id INNER JOIN mdl_course c ON c.id=e.instanceid  LEFT JOIN  {_pfx}user_info_data d ON u.id=d.userid  WHERE  e.contextlevel=50 AND e.instanceid =c.id  $wsql  ORDER BY u.firstname,u.lastname";
              
              }
         else{
              $wsql=$this->getUsersWsql($param);
             $sql="SELECT DISTINCT u.id,u.email,u.firstname,u.lastname,u.username FROM {_pfx}user u LEFT JOIN  {_pfx}user_info_data d ON u.id=d.userid WHERE u.id > 0 $wsql ORDER BY u.firstname,u.lastname";
         }
         $result=$this->getSqlservice()->search($sql,$offset,$limit);
              
         return  $result;
     }
     
     function getUsersData($param=array()) {
           $courseid=$this->getUtildata()->getVaueOfArray($param,'courseid');
            $course=$this->getUtildata()->getVaueOfArray($param,'course');
         $offset=0;
         $limit=10;
     
         if(isset($param['offset'])){ $offset=$param['offset'];}
         if(isset($param['limit'])){ $limit=$param['limit'];}
         
         $countfields=$this->countField();
         $limit=$limit*$countfields;
         
        
         $offset=$offset*$limit;
         $sql=null;
         if($courseid || $course){
              $wsql=$this->getUsersWsql($param);
              $wsql.=$this->getUsersCourseWsql($param);
             $sql="SELECT d.id,d.fieldid,d.userid,d.data,f.shortname,f.datatype FROM {_pfx}role_assignments rs INNER JOIN {_pfx}user u ON u.id=rs.userid INNER JOIN {_pfx}context e ON rs.contextid=e.id INNER JOIN mdl_course c ON c.id=e.instanceid  LEFT JOIN  {_pfx}user_info_data d  ON u.id=d.userid INNER JOIN {_pfx}user_info_field f ON d.fieldid=f.id  WHERE   e.contextlevel=50 AND e.instanceid =c.id  $wsql  ";
         }
         else{
             $wsql=$this->getUsersWsql($param);
             $sql="SELECT d.id,d.fieldid,d.userid,d.data,f.shortname,f.datatype FROM {_pfx}user u LEFT JOIN  {_pfx}user_info_data d  ON u.id=d.userid INNER JOIN {_pfx}user_info_field f ON d.fieldid=f.id  WHERE u.id > 0  $wsql ";
         }
         $result=$this->getSqlservice()->search($sql,$offset,$limit);
         $dnew=array();
        if(!empty( $result) && is_array($result)){
          foreach ($result as $row) {
             $userid=$this->getUtildata()->getVaueOfArray($row,'userid');
             $shortname=$this->getUtildata()->getVaueOfArray($row,'shortname');
             $data=$this->getUtildata()->getVaueOfArray($row,'data');
             $datatype=$this->getUtildata()->getVaueOfArray($row,'datatype');
            
             $key=$userid."/".$shortname;
            
             if($datatype=='datetime'){
                if(!empty($data)){$data= date($this->dateformat,$data);}
             }
             $dnew[$key]=$data;
          } 
        }
         return $dnew;
     }
     
      function makeFirstRow($users,$fields) {
           //first rows with column
          $firstrow=array();
          $cont=0;
          foreach ($users as $user) {
              $user=(array)$user;
              foreach ($user as $key => $value) {
                  $firstrow[$key]=$key;
              }
              if($cont==0){break;}
              $cont++;
          }
         foreach ($fields as $field) {
            $key=$this->getUtildata()->getVaueOfArray($field,'shortname');
            $name=$this->getUtildata()->getVaueOfArray($field,'name');
           
             $firstrow[$key]=$name;
         }
         
         return $firstrow;
      }
     
    function addDefultuserdataToData() {
     foreach ($this->usersdata as $user) {
      
               $user=(array)$user;
               $userid=$user['id'];
              foreach ($user as $key => $value) {
                  $key=$userid."/".$key ;
                  $this->data[$key]=$value;
              }
          }
    }
    
    
     function countField() {
            $sql ="SELECT COUNT(id) AS countrecord FROM {_pfx}user_info_field ";
            $result=$this->getSqlservice()->searchSingle($sql);
            $result =$this->getUtildata()->getVaueOfArray($result,'countrecord');
    }
     function getAllField() {
         global $CFG,$DB;
        $sql ="SELECT id,shortname,name,categoryid FROM {_pfx}user_info_field  ORDER BY categoryid,sortorder";
        $result=$this->getSqlservice()->search($sql,0,500);
        return $result;
    }
    function getSqlservice() {

        if(empty($this->sqlservice)){
            $this->sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
        }
        $this->sqlservice->setSessionhashkey($this->getSessionhashkey());
        return $this->sqlservice;
    }


        function setSqlservice($sqlservice) {
            $this->sqlservice = $sqlservice;
        }

        function getDateformat() {
            return $this->dateformat;
        }

        function setDateformat($dateformat) {
            $this->dateformat = $dateformat;
        }

        function getData() {
            return $this->data;
        }

        function setData($data) {
            $this->data = $data;
        }
        function setUsersdata($usersdata) {
            $this->usersdata = $usersdata;
        }


        function getFields() {
            return $this->fields;
        }

        function setFields($fields) {
            $this->fields = $fields;
        }



}
