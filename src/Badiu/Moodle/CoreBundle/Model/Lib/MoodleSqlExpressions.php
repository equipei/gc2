<?php

namespace Badiu\Moodle\CoreBundle\Model\Lib;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class MoodleSqlExpressions extends BadiuModelLib{

   private  $sqlservice=null;

    function __construct(Container $container) {
        parent::__construct($container);
       
    }
  
  /**
 * The `gradebook` function is responsible for modifying and executing an SQL query replacing expression __MOODLE_GRADEBOOK_ROLEIDS to id of role IDs
 * associated with a particular Moodle gradebook. This function takes an associative array `$param` as input,
 * which contains the keys `'sql'`, `'serviceid'`, and `'columns'`. It retrieves and processes these values
 * before executing the modified SQL query.
 *
 * @param array $param An associative array containing the following keys:
 *                     'sql' => A string representing the SQL query to be executed.
 *                     'serviceid' => A integer representing the unique  moodle identifier of a service.
 *                     'columns' => An array of column names associated with the Moodle gradebook.
 *
 * @return void The function returns the result of the modified SQL query execution.
 *
 * @example
 * $param = [
 *   'sql' => "SELECT * FROM users WHERE __MOODLE_GRADEBOOK_ROLEIDS",
 *   'serviceid' => 12345,
 *   'columns' => ['name', 'grade']
 * ];
 *
 * $result = gradebook($param);
 */
	public function gradebook($param){
		$param =$this->timeupdate($param);
		$sql=$this->getUtildata()->getVaueOfArray($param,'sql');
		$serviceid=$this->getUtildata()->getVaueOfArray($param,'serviceid');
		$columns=$this->getUtildata()->getVaueOfArray($param,'columns');
		$column=$this->getUtildata()->getVaueOfArray($columns,'roleid');
		if(empty($column)){$column="rs.roleid";}
		$mdldatautil=$this->getContainer()->get('badiu.moodle.core.lib.datautil');
		$mdldatautil->getSqlservice()->setServiceid($serviceid);
		$roles=$mdldatautil->getGradebookroles();
		$sqlrpl="";
		if (strpos($roles, ',') !== false) {
			$sqlrpl=" AND $column IN ($roles) ";
		}else{
			$sqlrpl=" AND $column = $roles";
		}
		$sql=str_replace("__MOODLE_GRADEBOOK_ROLEIDS",$sqlrpl,$sql);
		$param['sql']=$sql;

		return $param;
	}
  
  public function timeupdate($param){
		$timeupdate=$this->getUtildata()->getVaueOfArray($param,'param.source.timeupdate',true);
		if(empty($timeupdate)){return $param;}
		$sql=$this->getUtildata()->getVaueOfArray($param,'sql');
		$now=time()-$timeupdate;
		$sql=str_replace("__MOODLE_TIME",$now,$sql);
		$param['sql']=$sql;
	
		return $param;
	}
}
