<?php

namespace Badiu\Moodle\CoreBundle\Model\Lib;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class MoodleDefaultData extends BadiuModelLib{

    function __construct(Container $container) {
        parent::__construct($container);
    }
    
    
    public function getDefaultRoleName($shortname,$customname) {
                 if(!empty($customname)){return $customname;}
                if($shortname=='manager'){$name=$this->getTranslator()->trans('badiu.moodle.core.role.manager'); }
                else if($shortname=='coursecreator'){$name=$this->getTranslator()->trans('badiu.moodle.core.role.coursecreator'); }
                else if($shortname=='editingteacher'){$name=$this->getTranslator()->trans('badiu.moodle.core.role.editingteacher'); }
                else if($shortname=='teacher'){$name=$this->getTranslator()->trans('badiu.moodle.core.role.teacher'); }
                else if($shortname=='student'){$name=$this->getTranslator()->trans('badiu.moodle.core.role.student'); }
                else if($shortname=='guest'){$name=$this->getTranslator()->trans('badiu.moodle.core.role.guest'); }
                else if($shortname=='user'){$name=$this->getTranslator()->trans('badiu.moodle.core.role.user'); }
                else if($shortname=='frontpage'){$name=$this->getTranslator()->trans('badiu.moodle.core.role.frontpage'); }
               else {$name=$shortname;}
                return $name;
	}
    
                        /*
                 * 100 - Ativo
                   200 - Concluinte
                        completationconfig
                    300 - Inativo
                            methodenroldesabled
                            enroldesabled
                            startdatefuture
                            enddateexpired

                    400 - Evadido  
                    500 - Desistente
                */
    public function getBadiuStatus($row) {
              
            
              $mdlbstatus=100;
              $mdlbstatuinfo="";
              $methodstatus=$this->getUtildata()->getVaueOfArray($row, 'methodstatus');
              $enrolstatus=$this->getUtildata()->getVaueOfArray($row, 'enrolstatus');
              $timestart=$this->getUtildata()->getVaueOfArray($row, 'timestart');
              $timeend=$this->getUtildata()->getVaueOfArray($row, 'timeend');
             
              if($methodstatus ==1){
                  $mdlbstatus=300;
                  $mdlbstatuinfo='methodenroldesabled';
              }else if($enrolstatus ==1){
                   $mdlbstatus=300;
                   $mdlbstatuinfo='enroldesabled';
              }else{
                  $now =time();
                  if($timestart > $now){
                      $mdlbstatus=300;
                      $mdlbstatuinfo='startdatefuture';
                  }else if($timeend > 0 && $timeend < $now){
                       $mdlbstatus=300;
                       $mdlbstatuinfo='enddateexpired';
                  }
              }
              $status=new \stdClass();
              $status->code=$mdlbstatus;
              $status->info=$mdlbstatuinfo;
              return $status;
	}
}
