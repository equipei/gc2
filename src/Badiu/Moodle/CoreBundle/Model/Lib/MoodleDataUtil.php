<?php

namespace Badiu\Moodle\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class MoodleDataUtil extends BadiuSqlFilter{
    /**
     * @var object
     */
   private  $sqlservice;
    function __construct(Container $container) {
            parent::__construct($container);
            $this->sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
       }

       function getDbtype($serviceid) {
        $dbtype=null;
        if(!empty($serviceid)){
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $badiuSession->setHashkey($this->getSessionhashkey());
            $skey="badiu.moodle.mreport.dbtype.$serviceid";
            if($badiuSession->existValue($skey)){
                $dbtype=$badiuSession->getValue($skey); 
            }else{
                $data=$this->getContainer()->get('badiu.admin.server.service.data');
                $dbtype=$data->getGlobalColumnValue('dbtype',array('id'=>$serviceid)); 
                $badiuSession->addValue($skey,$dbtype); 
            }
            
            
        }

        
         return  $dbtype;

}
 function getVersionnumber($serviceid) {
        $versionnumber=null;
        if(!empty($serviceid)){
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $badiuSession->setHashkey($this->getSessionhashkey());
            $skey="badiu.moodle.mreport.versionnumber.$serviceid";
            if($badiuSession->existValue($skey)){
                $versionnumber=$badiuSession->getValue($skey); 
            }else{
                $data=$this->getContainer()->get('badiu.admin.server.service.data');
                $versionnumber=$data->getGlobalColumnValue('versionnumber',array('id'=>$serviceid)); 
                $badiuSession->addValue($skey,$versionnumber); 
            }
            
            
        }

        
         return  $versionnumber;

}
       function getSiteInfo($sserviceid,$session=true) {
                $sessionkey="badiu.moodle.mreport.site.info.$sserviceid";
                
               if($session){
                    $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                    $badiuSession->setHashkey($this->getSessionhashkey());
                     if($badiuSession->existValue($sessionkey)){
                        $info=$badiuSession->getValue($sessionkey);   
                         return $info;
                       }  
                }
                $data=$this->getContainer()->get('badiu.admin.server.service.data');
                $info=$data->getInfoById($sserviceid);
              
                if($session){
                    $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                    $badiuSession->setHashkey($this->getSessionhashkey());
                     $badiuSession->addValue($sessionkey,$info);
                }
                
                 return  $info;
        
    }
    function getCmsidinfo($sserviceid,$module,$instanceid,$session=true) {
        $sessionkey="badiu.moodle.mreport.site.$sserviceid.coursemoduleid.$module.$instanceid"; 
        
       if($session){
            $badiuSession=$this->getContainer()->get('badiu.system.access.session');
            $badiuSession->setHashkey($this->getSessionhashkey());
             if($badiuSession->existValue($sessionkey)){
                $info=$badiuSession->getValue($sessionkey);   
                 return $info;
               }  
        }
        $info=$this->getCmid($sserviceid,$module,$instanceid);
      
        if($session){
            $badiuSession=$this->getContainer()->get('badiu.system.access.session');
            $badiuSession->setHashkey($this->getSessionhashkey());
             $badiuSession->addValue($sessionkey,$info);
        }
        
         return  $info;

}



function getCmid($sserviceid,$module,$instanceid) {
    if(empty($sserviceid)){return null;}
    if(empty($module)){return null;}
    if(empty($instanceid)){return null;}
    $sql="SELECT cm.id FROM {_pfx}course_modules cm INNER JOIN {_pfx}modules m ON m.id=cm.module WHERE m.name='".$module."' AND  cm.instance=$instanceid";
      $this->getSqlservice()->setServiceid($sserviceid);
    $result=$this->getSqlservice()->searchSingle($sql);
    $result =$this->getUtildata()->getVaueOfArray($result,'id');
    return $result;
}


function getGradebookrolesSession() {
    $sserviceid=$this->getUtildata()->getVaueOfArray($this->getParam(),'_serviceid');
    $sessionkey="badiu.moodle.mreport.site.$sserviceid.gradebookroles"; 
    $rolesid=-1;
   $badiuSession=$this->getContainer()->get('badiu.system.access.session');
   $badiuSession->setHashkey($this->getSessionhashkey());
    if($badiuSession->existValue($sessionkey)){
        $rolesid=$badiuSession->getValue($sessionkey);   
        return $rolesid;
    }  
   
    $rolesid=$this->getGradebookroles();
    $badiuSession=$this->getContainer()->get('badiu.system.access.session');
    $badiuSession->setHashkey($this->getSessionhashkey());
    $badiuSession->addValue($sessionkey,$rolesid);
    
    return  $rolesid;

}


        function getSiteadmins() {
            $sql="SELECT value FROM {_pfx}config WHERE name='siteadmins'";
            $result=$this->getSqlservice()->searchSingle($sql);
            $result =$this->getUtildata()->getVaueOfArray($result,'value');
            if(empty($result)){$result=-1;}
            return $result;
        }
        function getGradebookroles() {
            $this->getSqlservice()->setSessionhashkey($this->getSessionhashkey());
            $sserviceid=$this->getUtildata()->getVaueOfArray($this->getParam(),'_serviceid');
			if(empty($sserviceid)){$sserviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');}
			if(!empty($sserviceid)){$this->getSqlservice()->setServiceid($sserviceid);}
            $sql="SELECT value FROM {_pfx}config WHERE name = 'gradebookroles'";
            $result=$this->getSqlservice()->searchSingle($sql);
             $result =$this->getUtildata()->getVaueOfArray($result,'value');
            if(empty($result)){$result=-1;}
            return $result;
        }

        function getCourse($sserviceid,$courseid) {
            $this->getSqlservice()->setSessionhashkey($this->getSessionhashkey());
            $this->getSqlservice()->setServiceid($sserviceid);
            $sql="SELECT fullname AS name FROM {_pfx}course WHERE  id= $courseid ";
            $result=$this->getSqlservice()->searchSingle($sql);
            $result =$this->getUtildata()->getVaueOfArray($result,'name');
            return $result;
        }

        function getCoursecatname($sserviceid,$coursecatid) {
            $this->getSqlservice()->setSessionhashkey($this->getSessionhashkey());
            $this->getSqlservice()->setServiceid($sserviceid);
            $sql="SELECT name  FROM {_pfx}course_categories  WHERE id= $coursecatid ";
            $result=$this->getSqlservice()->searchSingle($sql);
            $result =$this->getUtildata()->getVaueOfArray($result,'name');
            return $result;
        }
        function getRole($sserviceid,$roleid) {
            $this->getSqlservice()->setSessionhashkey($this->getSessionhashkey());
            $this->getSqlservice()->setServiceid($sserviceid);
            $sql="SELECT name,shortname FROM {_pfx}role WHERE  id= $roleid ";
            $result=$this->getSqlservice()->searchSingle($sql);
            $name =$this->getUtildata()->getVaueOfArray($result,'name');
            if(empty($name)){ $name =$this->getUtildata()->getVaueOfArray($result,'shortname');}
           
            return  $name;
        }
		
		 function getSessionDefualtlms() {
           
            $badiusession=$this->getContainer()->get('badiu.system.access.session');
     
            //get is session if exist
            if($badiusession->existValue('badiu.admin.servervice.defaultlms')){
                $defaultlms= $badiusession->getValue('badiu.admin.servervice.defaultlms');
				return $defaultlms;
            } 
           // echo "defaultlms $defaultlms";exit;
            //set is session if not exist
           $entity=$badiusession->get()->getEntity();
           $sserviceid=$this->getContainer()->get('badiu.admin.server.service.data')->getIdByShortname($entity,'defaultlms');
           if(empty($sserviceid)){$sserviceid=0;}
            $badiusession->addValue('badiu.admin.servervice.defaultlms',$sserviceid);
          
            return $sserviceid;
        }
		
		 function getSessionDefualtlmsUserid() {
           
            $badiusession=$this->getContainer()->get('badiu.system.access.session');
           
            //get is session if exist
            if($badiusession->existValue('badiu.admin.servervice.defaultlmsuserid')){
                $defaultlmsuserid= $badiusession->getValue('badiu.admin.servervice.defaultlmsuserid');
                return $defaultlmsuserid;
            } 
            
            //set is session if not exist
           $entity=$badiusession->get()->getEntity();
            $sserviceid=$this->getSessionDefualtlms();
            $username=$badiusession->get()->getUser()->getUsername();
            $this->getSqlservice()->setServiceid($sserviceid);
            $sql="SELECT id FROM {_pfx}user WHERE username= '".$username."'";
            $result=$this->getSqlservice()->searchSingle($sql);
            $mdluserid=$this->getUtildata()->getVaueOfArray($result,'id');
            if(empty($mdluserid)){$mdluserid=0;}
            $badiusession->addValue('badiu.admin.servervice.defaultlmsuserid',$mdluserid);
          
            return $mdluserid;
        }
		
        
       
       
       /* function getCoursegroup($groupid,$isshotname=false) {
            $wsql=" WHERE  g.id= $groupid ";
            if($isshotname){ $wsql=" WHERE g.idnumber = '".$groupid."'";}
            
            $sql="SELECT g.id,g.name,c.id AS courseid,c.fullname AS coursename,c.shortname AS courseshortname,ct.name AS coursecatname,ct.id AS coursecatid  FROM {_pfx}groups g INNER JOIN {_pfx}course c ON g.courseid=c.id INNER JOIN {_pfx}course_categories ct ON c.category=ct.id $wsql";
            $result=$this->getSqlservice()->searchSingle($sql);
            return $result;
        }
       
       
        
        function remoteAuth() {
            $urltarget=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urltarget');
            $urltarget=urldecode($urltarget);
            $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
            if(empty($serviceid)){echo 'param _serviceid is empty';exit;}
            
            $badiusession=$this->getContainer()->get('badiu.system.access.session');
            $entity=$badiusession->get()->getEntity();
            $userid=$badiusession->get()->getUser()->getId();
            $username=$badiusession->get()->getUser()->getUsername();
            $userfullname=$badiusession->get()->getUser()->getFullname();
            $moodleurl=$this->getSessionUrl($serviceid);
            $now=time();
            $basekey = "|$userid|$entity|$now";
            $lenth = strlen($basekey);
            $hash = $this->getContainer()->get('badiu.system.core.lib.util.hash');
            $lenth = 120 - $lenth;
            $authtokenkey = $hash->make($lenth);
            $authtoken = $authtokenkey . $basekey;
            $param = array('bkey' => 'badiu.moodle.user', 'modulekey' => 'badiu.system.user.authmoodleremote', 'moduleinstance' => $userid, 'name' => $userfullname, 'shortname' => $authtoken, 'customint1' => 1, 'customchar1' => $username,'customchar2' => $moodleurl,'customchar3' => $urltarget, 'entity' => $entity, 'timecreated' => new \DateTime(), 'deleted' => 0);
            $data = $this->getContainer()->get('badiu.system.module.data.data');
            $result = $data->insertNativeSql($param, false);
            
            $url="$moodleurl/report/badiunet/rauth.php?_tokenauth=$authtoken";
            
          //   echo "<pre>";
          //   print_r($param );
           //  echo "</pre>";
          //   echo "result: $result <br />";
           // echo $url;exit;
            header("Location: $url"); 
            exit;
          }
       function remoteAccessAuth() {
           $tokenauth=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_tokenauth');
           $moduledata=$this->getContainer()->get('badiu.system.module.data.data');
           $param=array('modulekey'=>'badiu.system.user.recoverpwd','customint1'=>1);
           $id=$moduledata->getIdByShortname($tokenauth,array('customint1'=>1));
           $response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
           if(empty($id)){return $response->denied('badiu.system.user.authmoodleremote.failed.tokenauthnotvalid', 'Token of remote auth not valid');}
           $resp=$moduledata->findById($id); 
           
           $resultdesable=$moduledata->updateNativeSql(array('id'=>$resp->getId(),'customint1'=>0),false);
             
           $result=array();
           $result['username']=$resp->getCustomchar1();
           $result['urltarget']=$resp->getCustomchar3();
           return $result;
       }   
          
       function getSessionUrl($serviceid=null) {   
          if(empty($serviceid)){
              $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
          }
          
          if(empty($serviceid)){return null;}
          $badiuSession=$this->getContainer()->get('badiu.system.access.session');
          $sessionkey="badiu.moodle.mreport.site_url_$serviceid";
          if($badiuSession->existValue($sessionkey)){
            $url=$badiuSession->getValue($sessionkey);   
             return $url;
           }  
           
         $sservicedata=$this->getContainer()->get('badiu.admin.server.service.data');
         $url=$sservicedata->getGlobalColumnValue('url',array('id'=>$serviceid));
         $badiuSession->addValue($sessionkey,$url);
       
         return $url;
     }
     */
     function getUserInfoFields() {
        $this->getSqlservice()->setSessionhashkey($this->getSessionhashkey());
		$sserviceid=$this->getUtildata()->getVaueOfArray($this->getParam(),'_serviceid');
		if(!empty($sserviceid)){$this->getSqlservice()->setServiceid($sserviceid);}
            $sql ="SELECT id,shortname,name,categoryid FROM {_pfx}user_info_field  ORDER BY categoryid,sortorder";
            //review limite
            $result=$this->getSqlservice()->search($sql,0,250);
            return $result;
        }
        
        function getCoursesOfCategory($categoryid,$param=array()) {
            $this->getSqlservice()->setSessionhashkey($this->getSessionhashkey());
            if(empty($categoryid)){return null;}
            $wsql="";
   
            $course= $this->getUtildata()->getVaueOfArray($param,'course');
            $coursevisible= $this->getUtildata()->getVaueOfArray($param,'coursevisible');
            $courseidnumber= $this->getUtildata()->getVaueOfArray($param,'courseidnumber');

            if(!empty($course)){ 
                $course=strtolower( $course);
                $wsql.=" AND LOWER(CONCAT(id,fullname,shortname))  LIKE '%".$course."%'";  
            }

            if($coursevisible!=null && $coursevisible!="" && ($coursevisible==0 || $coursevisible==1)){ 
                $wsql.=" AND visible = $coursevisible";  
            }

            if(!empty($courseidnumber)){ 
                $courseidnumber=strtolower($courseidnumber);
                $wsql.=" AND LOWER(dnumber)  LIKE '%".$courseidnumber."%'";  
            }
            $sql ="SELECT id,fullname,shortname FROM {_pfx}course  WHERE category = $categoryid $wsql  ORDER BY fullname";
          
            $result=$this->getSqlservice()->search($sql,0,20);
           
            return $result;
        }
        function getSqlservice() {
            if(!empty($this->sqlservice)){$this->sqlservice->setSessionhashkey($this->getSessionhashkey());}
            return $this->sqlservice;
        }

        function setSqlservice($sqlservice) {
            $this->sqlservice = $sqlservice;
        }


}
