<?php
namespace Badiu\Moodle\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuWebService;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class MoodleRemoteAccess    extends BadiuModelLib{
    /**
     * @var object
     */
   private  $webservice;
   
   private $container;
    function __construct(Container $container) {
                parent::__construct($container);
            $this->webservice=$this->getContainer()->get('badiu.system.core.lib.webservice');
       }
  
       
        function remoteAuth() {
            $urltarget=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urltarget');
            $urltarget=urldecode($urltarget);
            $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
            if(empty($serviceid)){echo 'param _serviceid is empty';exit;}
            
            $badiusession=$this->getContainer()->get('badiu.system.access.session');
			$badiusession->setHashkey($this->getSessionhashkey());
            $entity=$badiusession->get()->getEntity();
            $userid=$badiusession->get()->getUser()->getId();
            $username=$badiusession->get()->getUser()->getUsername();
            $userfullname=$badiusession->get()->getUser()->getFullname();
            $moodleurl=$this->getSessionUrl($serviceid);
			
			if(empty($moodleurl)){return null;}
			if(!empty($urltarget)){
				 $pos=strpos($urltarget,$moodleurl);
				 if($pos!== false){$urltarget= substr_replace($urltarget,'', $pos, strlen($moodleurl));}
				 $pos=stripos($urltarget,'/');
				 if($pos > 0){$urltarget='/'.$urltarget;}
			}
			$servicekey=$this->getSessionServicekey($serviceid);
			$managesyncmoodle=$this->getContainer()->get('badiu.ams.enrol.lib.managesyncmoodle');
			$sparam=array('userid'=>$userid,'sserviceid'=>$serviceid);
			$mdluserid=$managesyncmoodle->addUser($sparam);
            $now=time();
            $basekey = "|$userid|$entity|$now";
            $lenth = strlen($basekey);
            $hash = $this->getContainer()->get('badiu.system.core.lib.util.hash');
            $lenth = 120 - $lenth;
            $authtokenkey = $hash->make($lenth);
            $authtoken = $authtokenkey . $basekey;
			$param = array('bkey' => 'badiu.moodle.user', 'modulekey' => 'badiu.system.user.authmoodleremote', 'moduleinstance' => $userid, 'name' => $userfullname, 'shortname' => $authtoken, 'customint1' => 1,'customint2' => $mdluserid,'customint3'=>$serviceid, 'customchar1' => $username,'customchar2' => $moodleurl,'customchar3' => $urltarget,'customchar4'=>$moodleurl, 'entity' => $entity, 'timecreated' => new \DateTime(), 'deleted' => 0);
            $data = $this->getContainer()->get('badiu.system.module.data.data');
            $result = $data->insertNativeSql($param, false);
             $tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
          
            $authtoken=$tcript->encode('k2',$authtoken,$serviceid);
            $authtoken=base64_encode($authtoken);
          
            $url="$moodleurl/local/badiunet/rauth.php?_tokenauth=$authtoken&_appservicekeyinstance=$servicekey";
            
            header("Location: $url"); 
            exit;
          }
       function remoteAccessAuth() {
       
          
           $tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
		  
		   $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		   
		   $tokenauth=$this->getUtildata()->getVaueOfArray($param,'_tokenauth');
		 
		   $reqparam =$this->getUtildata()->getVaueOfArray($param,'_param');
           $tokenauth=$tcript->decode($tokenauth);
           $reqparam=$tcript->decode($reqparam);
		   $reqparam=$this->getJson()->decode($reqparam,true);
		   
		   $remotemoodleurl=$this->getUtildata()->getVaueOfArray($reqparam,'moodleurl');
		   
		   
           $moduledata=$this->getContainer()->get('badiu.system.module.data.data');
           $param=array('modulekey'=>'badiu.system.user.recoverpwd','customint1'=>1);
		  
          // $id=$moduledata->getIdByShortname($tokenauth,array('customint1'=>1));
		   $dparam=array('shortname'=>$tokenauth,'customint1'=>1);
		   $id=$moduledata->getGlobalColumnValue('id',$dparam);  
		  
           $response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
           if(empty($id)){return $response->denied('badiu.system.user.authmoodleremote.failed.tokenauthnotvalid', 'Token of remote auth not valid');}
          // $resp=$moduledata->findById($id); 
           
           $resultdesable=$moduledata->updateNativeSql(array('id'=>$id,'customint1'=>0),false);
           $resp= $moduledata->getGlobalColumnsValue('o.customchar1,o.customchar3,o.customchar4,o.customint2,o.customint3',array('id'=>$id));  
           $result=array();
           $result['userid']=$this->getUtildata()->getVaueOfArray($resp,'customint2');
		   $result['username']=$this->getUtildata()->getVaueOfArray($resp,'customchar1');
           $result['urltarget']=$this->getUtildata()->getVaueOfArray($resp,'customchar3');
		   $moodleurlss=$this->getUtildata()->getVaueOfArray($resp,'customchar4');
		   
		   if($remotemoodleurl!=$moodleurlss){ return $response->denied('badiu.system.user.authmoodleremote.failed.remoteurlnotequalserviceurl', 'Remote moodle url is differente moodle service url');}
           return $result;
       }   
  
/**
Moodle send request with token badiunet session request for user access badiunet by url redirect
Using Moodle session auth
*/  
 function remoteAccessRequest() {
       
          
           $tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
		   $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		  
		   $reqparam =$this->getUtildata()->getVaueOfArray($param,'_param');
		   $tokensession =$this->getUtildata()->getVaueOfArray($param,'_tokensession');
		   
           $reqparam=$tcript->decode($reqparam);
		   $tokensession=$tcript->decode($tokensession);
		   $reqparam=$this->getJson()->decode($reqparam,true);
		   
		   $remotemoodleurl=$this->getUtildata()->getVaueOfArray($reqparam,'moodleurl');
		   $remoterkey=$this->getUtildata()->getVaueOfArray($reqparam,'rkey');
		   
		   $response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
		   //check tkey
		   $dbsesseiondata = $this->getContainer()->get('badiu.system.access.dbsession.data');
		   $dbsesseiondto=$dbsesseiondata->findByTkey($tokensession);
			
			//session tkey not valid
			if(empty($dbsesseiondto)){
				return $response->denied('badiu.system.access.authwebservice.tokensessionnotvalid','token of session not found in database'); 
			}
       
			//check tkey expired
			$timetoexpire=$this->getUtildata()->getVaueOfArray($dbsesseiondto,'timetoexpire');
			if(!empty($timetoexpire)){$timetoexpire=$timetoexpire->getTimestamp();}
			else {$timetoexpire=0;}
        
			$timenow=new \DateTime();
			$timenow=$timenow->getTimestamp();
        
			if($timetoexpire==0 || $timetoexpire < $timenow ){
				return $response->denied('badiu.system.access.authwebservice.sessionexpired','session just expired'); 
			}
        
		
			//get config of sessiondb
			$ssdbdconfig=$this->getUtildata()->getVaueOfArray($dbsesseiondto,'dconfig');
			$ssdbdconfig=$this->getJson()->decode($ssdbdconfig, true);
			$sserviceid=$this->getUtildata()->getVaueOfArray($dbsesseiondto,'sclient');
			$entity=$this->getUtildata()->getVaueOfArray($dbsesseiondto,'entity');
			
			//tsession moodleurl
			$tsessionmoodleurl=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'client.moodleurl',true);
			$tsessionrkeys=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'rkeys');
			
			//check url
			if($remotemoodleurl!=$tsessionmoodleurl){return $response->denied('badiu.system.access.authwebservice.moodleurlnomatch','Moodle url remote request is not the same of Moodle url registres em tsession'); }
			
			//chek rkey
			if (!in_array($remoterkey, $tsessionrkeys)){return $response->denied('badiu.system.access.authwebservice.rkeynomatch','Moodle rkey remote request is not exist in tsession rkeys'); }
			
			//check rkey remote
			$crchoosekey = rand(0, 19);
			$chrkey=$this->getUtildata()->getVaueOfArray($tsessionrkeys,$crchoosekey);
			
			$sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
			$sparam=array();
			$sparam['_serviceid']=$sserviceid;
			$sparam['sessionid']=$tokensession;
			$sparam['rkey']=$chrkey;
			$sparam['_key']='local.badiunet.localbadiunet.general.existrkey';
      
			$sresult=$sqlservice->service($sparam);
			$fsresult=$sqlservice->serviceResponse($sresult);
			$sckrstatus=$this->getUtildata()->getVaueOfArray($sresult,"status");
			if($sckrstatus!="accept"){
				$rfmessage=$this->getUtildata()->getVaueOfArray($sresult,"message");
				$rfinfo=$this->getUtildata()->getVaueOfArray($sresult,"info");
				return $response->denied('badiu.system.access.authwebservice.checkrkeyinsidemoodlefailed','Remote key error: '.$rfinfo.' Remote message: '.$rfmessage);
			}
			if(!$fsresult){return $response->denied('badiu.system.access.authwebservice.checkrkeyinsidemoodlewithoutsucess','Moodle rkey check without success');}
		
			//sync user
			$suserresult=$this->syncUser($ssdbdconfig);
			$syncstatus=$this->getUtildata()->getVaueOfArray($suserresult,"status");
			$syncmessage=$this->getUtildata()->getVaueOfArray($suserresult,"message");
			$userid=$syncmessage;
			$syncinfo=$this->getUtildata()->getVaueOfArray($suserresult,"info");
			if($syncstatus!='acept'){
				return $response->denied('badiu.system.access.authwebservice.synuserfailure','Sync user from Moodle to badiu.net system failed. Message of system operation: key: '.$syncinfo.' message: '.$syncmessage);
			}
			
			//install entity
			$dinstall= $this->getContainer()->get('badiu.system.module.install');
            $resultinstall= $dinstall->setupentity($entity);
			
			//add permission admin
			$datarole = $this->getContainer()->get('badiu.system.access.role.data');
            $roleadminid=  $datarole->getIdByShortname($entity,'admin');
            $datauseraccess = $this->getContainer()->get('badiu.system.access.user.data');
            $param=array('entity'=>$entity,'roleid'=>$roleadminid,'userid'=>$userid,'deleted'=>0);
            $existroleuser=$datauseraccess->countNativeSql(array('entity'=>$entity,'roleid'=>$roleadminid,'userid'=>$userid),false);
			if(!$existroleuser){$datauseraccess->insertNativeSql($param, false);}
			
			//init session
			$firstname=$this->getUtildata()->getVaueOfArray($ssdbdconfig,"user.firstname",true);
			$lastname=$this->getUtildata()->getVaueOfArray($ssdbdconfig,"user.lastname",true);
			$email=$this->getUtildata()->getVaueOfArray($ssdbdconfig,"user.email",true);
			$username=$this->getUtildata()->getVaueOfArray($ssdbdconfig,"user.username",true);
			$mdluserid=$this->getUtildata()->getVaueOfArray($ssdbdconfig,"user.id",true);
			
			/*$ssparam=array('userid'=>$userid,'entity'=>$entity,'firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'username'=>$username);
            
			$this->getContainer()->get('badiu.system.access.session')->start($param);
			*/
			//build url
			$requestparam=$this->getUtildata()->getVaueOfArray($reqparam,"requestparam");
			
			$routkey=$this->getUtildata()->getVaueOfArray($requestparam,"_key");
			if(empty($routkey)){return $response->denied('badiu.system.access.authwebservice.paramrouterequired','Param _key is empty. This parameter is required');}
			unset($requestparam["_key"]);
			$app=$this->getContainer()->get('badiu.system.core.lib.util.app');
			$badiuneturl=$app->getUrlByRoute($routkey,$requestparam);
			
			//generate rekys
			$rkeys=array();
			$rkeycont=0;
			$hash = $this->getContainer()->get('badiu.system.core.lib.util.hash');
			while($rkeycont < 20){
				$rkey=$hash->make(30);
				array_push($rkeys,$rkey);
				$rkeycont++;
			}
			
			//generate authtoken
			$now=time();
            $basekey = "|$syncmessage|$entity|$now";
			$lenth = strlen($basekey);
			$lenth = 90 - $lenth;
			$authtokenkey = $hash->make($lenth);
            $authtoken = $authtokenkey . $basekey;
			$userfullname="$firstname $lastname";
			$moodleurl=$this->getUtildata()->getVaueOfArray($ssdbdconfig,"sserver.url",true);
			$badiunetremotlogin=$app->getUrlByRoute('badiu.auth.core.loginfromexternalsystem.link');
			
			$dataconfig=array('rkeys'=>$rkeys,'requestparam'=>$requestparam);
			$dataconfig=$this->getJson()->encode($dataconfig);
			$iatparam = array('bkey' => 'badiu.moodle.user', 'modulekey' => 'badiu.system.user.authbadiunetremote', 'moduleinstance' => $userid, 'name' => $userfullname, 'shortname' => $authtoken, 'customint1' => 1,'customint2' => $mdluserid,'customint3'=>$sserviceid, 'customchar1' => $username,'customchar2' => $badiunetremotlogin,'customchar3' => $badiuneturl,'customchar4'=>$moodleurl, 'entity' => $entity,'customtext1'=>$dataconfig,'timecreated' => new \DateTime(), 'deleted' => 0);
            $moduledata = $this->getContainer()->get('badiu.system.module.data.data');
            $cmiresult = $moduledata->insertNativeSql($iatparam, false);
            if(!$cmiresult){return $response->denied('badiu.system.access.authwebservice.registeremoterauhtindatabasefailed','Save data of remote auth in database on table system_module_data failed');}
			
			$resultdata=array('rkeys'=>$rkeys,'_tokenauth'=>$authtoken,'badiuneturluth'=>$badiunetremotlogin);
			$resultdata=$this->getJson()->encode($resultdata);
			$resultdata=$tcript->encode('k2',$resultdata,$sserviceid);
		    return $resultdata;
       }  	  
       function getSessionUrl($serviceid=null) {   
          if(empty($serviceid)){
              $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
          }
        
          if(empty($serviceid)){return null;}
          $badiuSession=$this->getContainer()->get('badiu.system.access.session');
		  $badiuSession->setHashkey($this->getSessionhashkey());
          $sessionkey="badiu.moodle.mreport.site_url_$serviceid";
          if($badiuSession->existValue($sessionkey)){
	
            $url=$badiuSession->getValue($sessionkey);  
			if(!empty($url)){return $url;}
			
           }  
           
         $sservicedata=$this->getContainer()->get('badiu.admin.server.service.data');
         $url=$sservicedata->getGlobalColumnValue('url',array('id'=>$serviceid));
         $badiuSession->addValue($sessionkey,$url);
       
         return $url;
     }
     
    function getSessionServicekey($serviceid=null) {   
          if(empty($serviceid)){
              $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
          }
        
          if(empty($serviceid)){return null;}
          $badiuSession=$this->getContainer()->get('badiu.system.access.session');
		  $badiuSession->setHashkey($this->getSessionhashkey());
          $sessionkey="badiu.moodle.mreport.site_servicekey_$serviceid";
          if($badiuSession->existValue($sessionkey)){
	
            $url=$badiuSession->getValue($sessionkey);  
			if(!empty($url)){return $url;}
			
           }  
           
         $sservicedata=$this->getContainer()->get('badiu.admin.server.service.data');
         $servicekey=$sservicedata->getGlobalColumnValue('servicekey',array('id'=>$serviceid));
         $badiuSession->addValue($sessionkey,$servicekey);
       
         return $servicekey;
     }
	 
		function getServiceidByUrl($param) {   
			$url=$this->getUtildata()->getVaueOfArray($param,'url');
			$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		
			$fparam=array('dtype'=>'site_moodle','url'=>$url,'entity'=>$entity);
			 $sservicedata=$this->getContainer()->get('badiu.admin.server.service.data');
			
			$count= $sservicedata->countNativeSql($fparam,false);
			
			$result=null; 
			if($count==1){
				$result=$sservicedata->getGlobalColumnValue('id',$fparam);
			}
			return $result;
		}
		
		private function syncUser($ssdbdconfig) { 
			
			//check  if user is syncronized
			$syncstatus=$this->getUtildata()->getVaueOfArray($ssdbdconfig,"user.serversync.status",true);
			$syncmessage=$this->getUtildata()->getVaueOfArray($ssdbdconfig,"user.serversync.message",true);
			if($syncstatus=='acept' && !empty($syncmessage)){
				$serversync=$this->getUtildata()->getVaueOfArray($ssdbdconfig,"user.serversync",true);
				return $serversync;
			}
			
			//syn user
			$tsuser=$this->getUtildata()->getVaueOfArray($ssdbdconfig,"user");
			$autosync=$this->getUtildata()->getVaueOfArray($tsuser,"autosync");
			$keysync=$this->getUtildata()->getVaueOfArray($tsuser,"keysync");
			if(!$autosync){$tsuser['autosync']=1;}
			if(empty($keysync)){$tsuser['keysync']="username";}
			
			$tsserver=$this->getUtildata()->getVaueOfArray($ssdbdconfig,"sserver");
			
			$paramdto=array("user"=>$tsuser,'sserver'=>$tsserver);
			$syncusermlib=$this->getContainer()->get('badiu.admin.server.syncuser.managelib');
			$synuser=$syncusermlib->process($paramdto);			
			
			return $synuser;
				
		}
        function getWebservice() {
            return $this->webservice;
        }

        function setWebservice($webservice) {
            $this->webservice = $webservice;
        }



}
