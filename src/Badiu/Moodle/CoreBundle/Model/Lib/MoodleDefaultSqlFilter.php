<?php

namespace Badiu\Moodle\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class MoodleDefaultSqlFilter extends BadiuSqlFilter{
    /**
     * @var object
     */
   private  $sqlservice;
    function __construct(Container $container) {
            parent::__construct($container);
            $this->sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
       }

        function withoutgrade() {
           
           $param=  $result =$this->getUtildata()->getVaueOfArray($this->getParam(),'withoutgrade');
           $sql=" ";
           
            if($param==1){$sql=" AND g.finalgrade IS NULL";}
            if($param=="0"){$sql=" AND g.finalgrade IS NOT NULL";}
            return $sql;
        }
        
          function enroltimevalidate($prefix="") {
           
           $param=  $this->getUtildata()->getVaueOfArray($this->getParam(),'enroltimevalidate');
           $sql=" ";
           $now=time();
            if($param==1){$sql=" AND (ue".$prefix.".timestart IS NULL OR ue".$prefix.".timestart = 0 OR ue".$prefix.".timestart <= $now ) AND (ue".$prefix.".timeend IS NULL OR ue".$prefix.".timeend = 0 OR ue".$prefix.".timeend >= $now )";}
            if($param==0 && $param !=''  && $param !=null){$sql=" AND ((ue".$prefix.".timestart > 0 AND ue".$prefix.".timestart > $now )  OR (ue".$prefix.".timeend > 0 AND ue".$prefix.".timeend < $now ) )";}
           // echo "$param | $sql<hr>";exit;
            return $sql;
        }
        
  
        function getSqlservice() {
            return $this->sqlservice;
        }

        function setSqlservice($sqlservice) {
            $this->sqlservice = $sqlservice;
        }


}
