<?php

namespace Badiu\Moodle\CoreBundle\Model\Lib;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ImportGeneralCastData extends BadiuModelLib{

    private $param=null;

    function __construct(Container $container) {
        parent::__construct($container);
       
    }
    
   
	function castdata($syncdbdata, $drows){
		if(!is_array($drows)){return $drows;}
		$newdata=array();
		if(empty($this->getParam())){return $drows;}
		
		
		foreach ($drows as $row) {
			 $row=$this->fieldCast($row);
			 $row=$this->addFields($row);
			 array_push($newdata,$row);
		}
		
		return $newdata;
	}
	
	function addFields($row){
		if(!is_array($row)){return $row;}
		
		$list=$this->getUtildata()->getVaueOfArray($this->getParam(),'addfields');
		foreach ($list as $fk=> $v) {
			$pos=stripos($v, "__");
              if($pos!== false){$v=$this->getUtildata()->getVaueOfArray($this->getParam(),$v);}
			  $row[$fk]=$v;
		}
		
		return $row;
	}
	
	function fieldCast($row){
		if(!is_array($row)){return $row;}
		
		$castpam=$this->getUtildata()->getVaueOfArray($this->getParam(),'param');
		foreach ($castpam as $ck=> $fields) {
			
			if(method_exists($this,$ck)){
				$row=$this->$ck($row,$fields);
			}
			
		}
		
		return $row;
	}
	
	private function casttimestamptodatetime($row,$fields){
		
		foreach ($fields as $field) {
			$fdata=$this->getUtildata()->getVaueOfArray($row,$field);
			if(is_numeric($fdata) && $fdata > 0){
				$d1=new \DateTime();
				$d1->setTimestamp($fdata);
				$row[$field]=$d1;
			}
		}
		
		return 	$row;	
	}
	
	
function getParam() {
    return $this->param;
}

function setParam($param) {
    $this->param = $param;
}
}
