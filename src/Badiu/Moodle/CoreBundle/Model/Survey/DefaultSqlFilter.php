<?php

namespace Badiu\Moodle\CoreBundle\Model\Survey;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class DefaultSqlFilter extends BadiuSqlFilter{
  
  
    function __construct(Container $container) {
            parent::__construct($container);
       }

        function exec() {
           
           return null;
        }
         function addcolumnsrankingamsdiscipline(){
			 $wsql="";
			 $fparam=array('pfx'=>1,'activityquestionalias'=>'mms.activityquestionid','amsdisciplinealias'=>'d.id','rankvalue'=>1,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>2,'activityquestionalias'=>'mms.activityquestionid','amsdisciplinealias'=>'d.id','rankvalue'=>2,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>3,'activityquestionalias'=>'mms.activityquestionid','amsdisciplinealias'=>'d.id','rankvalue'=>3,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>4,'activityquestionalias'=>'mms.activityquestionid','amsdisciplinealias'=>'d.id','rankvalue'=>4,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>5,'activityquestionalias'=>'mms.activityquestionid','amsdisciplinealias'=>'d.id','rankvalue'=>5,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>6,'activityquestionalias'=>'mms.activityquestionid','amsdisciplinealias'=>'d.id','operation'=>'sumall');
			 $wsql.=$this->addColumn($fparam);
			
			return  $wsql; 
		 }
	 function addcolumnsrankingamsodiscipline(){
			 $wsql="";
			 $fparam=array('pfx'=>1,'activityquestionalias'=>'mms.activityquestionid','amsodisciplinealias'=>'od.id','rankvalue'=>1,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>2,'activityquestionalias'=>'mms.activityquestionid','amsodisciplinealias'=>'od.id','rankvalue'=>2,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>3,'activityquestionalias'=>'mms.activityquestionid','amsodisciplinealias'=>'od.id','rankvalue'=>3,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>4,'activityquestionalias'=>'mms.activityquestionid','amsodisciplinealias'=>'od.id','rankvalue'=>4,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>5,'activityquestionalias'=>'mms.activityquestionid','amsodisciplinealias'=>'od.id','rankvalue'=>5,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>6,'activityquestionalias'=>'mms.activityquestionid','amsodisciplinealias'=>'od.id','operation'=>'sumall');
			 $wsql.=$this->addColumn($fparam);
			
			return  $wsql; 
		 }
    function addcolumnsrankingamsclasse(){
			 $wsql="";
			 $fparam=array('pfx'=>1,'activityquestionalias'=>'mms.activityquestionid','amsclassealias'=>'cl.id','rankvalue'=>1,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>2,'activityquestionalias'=>'mms.activityquestionid','amsclassealias'=>'cl.id','rankvalue'=>2,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>3,'activityquestionalias'=>'mms.activityquestionid','amsclassealias'=>'cl.id','rankvalue'=>3,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>4,'activityquestionalias'=>'mms.activityquestionid','amsclassealias'=>'cl.id','rankvalue'=>4,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>5,'activityquestionalias'=>'mms.activityquestionid','amsclassealias'=>'cl.id','rankvalue'=>5,'operation'=>'sumitem');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>6,'activityquestionalias'=>'mms.activityquestionid','amsclassealias'=>'cl.id','operation'=>'sumall');
			 $wsql.=$this->addColumn($fparam);
			 
			return  $wsql; 
		 }
 function addColumn($param) {
	 $pfx=$this->getUtildata()->getVaueOfArray($param,'pfx');
	 $activityquestionalias=$this->getUtildata()->getVaueOfArray($param,'activityquestionalias');
	 $amsdisciplinealias=$this->getUtildata()->getVaueOfArray($param,'amsdisciplinealias');
	 $amsodisciplinealias=$this->getUtildata()->getVaueOfArray($param,'amsodisciplinealias');
	 $amsclassealias=$this->getUtildata()->getVaueOfArray($param,'amsclassealias');
	 $rankvalue=$this->getUtildata()->getVaueOfArray($param,'rankvalue');
	 $operation=$this->getUtildata()->getVaueOfArray($param,'operation');
	
	
	
	 if(empty($pfx)){return ""; }
	 if(empty($activityquestionalias)){return ""; }
	 if($operation=='sumitem' && empty($rankvalue)){return ""; }
	 if(empty($operation)){return ""; }
	
     $itemfilter="";
	 if(!empty($amsdisciplinealias)){$itemfilter=" AND d$pfx.id=$amsdisciplinealias";}
	 else if(!empty($amsodisciplinealias)){$itemfilter=" AND od$pfx.id=$amsodisciplinealias";}
	 else if(!empty($amsclassealias)){$itemfilter=" AND cl$pfx.id=$amsclassealias";}
	 if($operation=='sumitem' ){
		 $wsql=" ,(SELECT COUNT(s$pfx.id) FROM BadiuMoodleCoreBundle:MoodleCoreModIsummarize s$pfx JOIN BadiuAdminServerBundle:AdminServerSyncData ssd$pfx WITH (s$pfx.serviceid=ssd$pfx.serviceid AND s$pfx.courseid=ssd$pfx.remotedataid) JOIN BadiuAmsOfferBundle:AmsOfferClasse cl$pfx  WITH (ssd$pfx.moduleinstance=cl$pfx.id AND ssd$pfx.modulekey='badiu.ams.offer.classe') JOIN cl$pfx.odisciplineid od$pfx JOIN od$pfx.disciplineid d$pfx  WHERE s$pfx.activityquestionid=$activityquestionalias $itemfilter AND s$pfx.responseint= $rankvalue ) AS rank$rankvalue ";
	 }else  if($operation=='sumall' ){
		  $wsql=" ,(SELECT COUNT(s$pfx.id) FROM BadiuMoodleCoreBundle:MoodleCoreModIsummarize s$pfx JOIN BadiuAdminServerBundle:AdminServerSyncData ssd$pfx WITH (s$pfx.serviceid=ssd$pfx.serviceid AND s$pfx.courseid=ssd$pfx.remotedataid) JOIN BadiuAmsOfferBundle:AmsOfferClasse cl$pfx  WITH (ssd$pfx.moduleinstance=cl$pfx.id AND ssd$pfx.modulekey='badiu.ams.offer.classe') JOIN cl$pfx.odisciplineid od$pfx JOIN od$pfx.disciplineid d$pfx  WHERE s$pfx.activityquestionid=$activityquestionalias $itemfilter AND s$pfx.responseint >=1 AND  s$pfx.responseint <=5 ) AS total ";
	 }
	
	 return $wsql;
  }

	
} 
