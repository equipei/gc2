<?php

namespace Badiu\Moodle\CoreBundle\Model\Site;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class SiteFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }

    public  function getVersion(){
        $list=array();
        $list['3.1']='3.1';
		$list['3.0']='3.0';
		$list['2.9']='2.9';
		$list['2.8']='2.8';
		$list['2.7']='2.7';
		$list['2.6']='2.6';
		$list['2.5']='2.5';
		$list['2.4']='2.4';
		$list['2.3']='2.3';
		$list['2.2']='2.2';	
		
        return $list;
    }

	public  function getDbType(){
        $list=array();
        $list['mysql']='MySQL';
		$list['pgsql']='PostgreSQL';
		
        return $list;
    }
	public  function getThemeBootstrap(){
        $list=array();
        $list['bootstrap4']='Bootstrap 4';
		$list['bootstrap3']='Bootstrap 3';
		$list['bootstrap2']='Bootstrap 2';
		
        return $list;
    }
}
