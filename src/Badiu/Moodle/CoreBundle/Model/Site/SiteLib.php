<?php
namespace Badiu\Moodle\CoreBundle\Model\Site;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class SiteLib extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }


  function generateToken($instanceid,$plugin="mreport") {
			$hash=$this->getContainer()->get('badiu.system.core.lib.util.hash');
			$token=$hash->make(30);
			$user=2;
			$entity=$this->getEntity();
			if(empty($plugin)){$plugin="mreport";}
			$stoken="$entity|$token|$user|$plugin|$instanceid";
			return $stoken;

	}
	 
	 function addField($id,$field,$value) {
			$data=$this->getContainer()->get('badiu.admin.server.service.data');
			
			$fieldvalue=$data->getGlobalColumnValue($field,array('id'=>$id));
			if(!empty($fieldvalue)){return $fieldvalue;}
			$fieldvalue=$value;
			if($field=='servicetoken'){$fieldvalue=$this->generateToken($id);}
			
			$param=array('id'=>$id,$field=>$fieldvalue);
			$result=$data->updateNativeSql($param,false);
        
		return $fieldvalue;
	}
}
