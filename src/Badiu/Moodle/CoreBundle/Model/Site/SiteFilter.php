<?php

namespace Badiu\Moodle\CoreBundle\Model\Site;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class SiteFilter extends BadiuFormFilter{

    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
     public function execAfterSubmit() {

          $id= $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
		  $entity= $this->getUtildata()->getVaueOfArray($this->getParam(), 'entity');
		  if(empty($id)){return null;}
		  $sitelib= $this->getContainer()->get('badiu.moodle.core.site.sitelib');
		  if(!empty($entity)){$sitelib->setEntity($entity);}
		  $sitelib->addField($id,'servicetoken',null);
		  $sitelib->addField($id,'servicerurl','/local/badiunet/synchttp/expdata.php');
		  $sitelib->addField($id,'servicerurl1','/local/badiuws/sync.php');
          
        }
      
}
