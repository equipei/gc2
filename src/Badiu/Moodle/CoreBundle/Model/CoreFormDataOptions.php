<?php

namespace Badiu\Moodle\CoreBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;

class CoreFormDataOptions extends BadiuFormDataOptions {
   private  $sqlservice;
   private  $serviceid;
    function __construct(Container $container, $baseKey = null) {
        parent::__construct($container, $baseKey);
       $this->sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');

    }

    public function getMoodle() {
      $badiuSession=$this->getContainer()->get('badiu.system.access.session');
      $badiuSession->setHashkey($this->getSessionhashkey());
      $entity=$badiuSession->get()->getEntity();
      $data=$this->getContainer()->get('badiu.admin.server.service.data');
      $list=$data->getGlobalColumnsValues('o.id,o.name,o.url',array('entity'=>$entity,'dtype'=>'site_moodle'));
   
      $newlist=array();
      foreach ($list as $row) {
        $id=$this->getUtildata()->getVaueOfArray($row,'id');
        $name=$this->getUtildata()->getVaueOfArray($row,'name');
        $url=$this->getUtildata()->getVaueOfArray($row,'url');
        $newlist[$id] ="$name - $url";
      }
      return $newlist;
  }

    public function getEnrolStatus() {
        $list = array();
        $list[0] = $this->getTranslator()->trans('badiu.moodle.core.enrol.statusactive');
        $list[1] = $this->getTranslator()->trans('badiu.moodle.core.enrol.statusinactive');
        return $list;
    }
    public function getEnrolStatusLabel($value) {
      $label="";
     if($value==0){$label=$this->getTranslator()->trans('badiu.moodle.core.enrol.statusactive');}
      else if($value==1){$label=$this->getTranslator()->trans('badiu.moodle.core.enrol.statusinactive');}
      return $label;
  }
     public function getRoles() { 
         $rolelib=$this->getContainer()->get('badiu.moodle.core.lib.defaultdata');
         $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
        $sqlservice->setSessionhashkey($this->getSessionhashkey());
        $sql="SELECT id,name,shortname FROM {_pfx}role ";
         $result=$sqlservice->search($sql);
        
           $list=array();
        if(empty($result)){return  $list;}
        foreach ($result as $row) {
             $id=$this->getUtildata()->getVaueOfArray($row,'id');
             $name=$this->getUtildata()->getVaueOfArray($row,'name');
             $shortname=$this->getUtildata()->getVaueOfArray($row,'shortname');
             $name=$rolelib->getDefaultRoleName($shortname,$name);
            
             $list[$id]=$name;
        }
       return $list;
     }
	 
	 public function getRolesShortname() { 
         $rolelib=$this->getContainer()->get('badiu.moodle.core.lib.defaultdata');
         $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
        $sqlservice->setSessionhashkey($this->getSessionhashkey());
        $sql="SELECT shortname AS id,name,shortname  FROM {_pfx}role ";
         $result=$sqlservice->search($sql);
        
           $list=array();
        if(empty($result)){return  $list;}
        foreach ($result as $row) {
             $id=$this->getUtildata()->getVaueOfArray($row,'id');
             $name=$this->getUtildata()->getVaueOfArray($row,'name');
             $shortname=$this->getUtildata()->getVaueOfArray($row,'shortname');
             $name=$rolelib->getDefaultRoleName($shortname,$name);
            
             $list[$id]=$name;
        }
       return $list;
     }
     public function getRolesLabel($value) { 
        if(empty($this->getServiceid())){return $value;}
        if(empty($value)){return $value;}
        $this->sqlservice->setServiceid($this->getServiceid());
        $this->sqlservice->setSessionhashkey($this->getSessionhashkey());
        $rolelib=$this->getContainer()->get('badiu.moodle.core.lib.defaultdata');
       
        $sql="SELECT id,name,shortname FROM {_pfx}role WHERE id=$value";
        $row=$this->sqlservice->searchSingle($sql);
       
        $name=$this->getUtildata()->getVaueOfArray($row,'name');
        $shortname=$this->getUtildata()->getVaueOfArray($row,'shortname');
        $name=$rolelib->getDefaultRoleName($shortname,$name);
        return $name;
     }
      public function getRolesGradebookData($fparam=array()) { 
		$cfkey= $this->getUtildata()->getVaueOfArray($fparam,'key');
		$notgradebook=$this->getUtildata()->getVaueOfArray($fparam,'notgradebook');
          $mdatautil=$this->getContainer()->get('badiu.moodle.core.lib.datautil');
          $rolelib=$this->getContainer()->get('badiu.moodle.core.lib.defaultdata');
          $gradebookroles=$mdatautil->getGradebookroles();
		 
        $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
        $sqlservice->setSessionhashkey($this->getSessionhashkey());
        
		$sql="";
		$sqlnotgradebook="";
		if($notgradebook){$sqlnotgradebook=" NOT ";}
		if($gradebookroles==-1){
			$sql="SELECT id,name,shortname FROM {_pfx}role WHERE id > 0 ";	
		}else{
			 $sql="SELECT id,name,shortname FROM {_pfx}role WHERE id $sqlnotgradebook IN ($gradebookroles) ";
		}
        
		// echo $sql;exit;
         $result=$sqlservice->search($sql);
        $list=array();
		
        foreach ($result as $row) {
             $id=$this->getUtildata()->getVaueOfArray($row,'id');
             $name=$this->getUtildata()->getVaueOfArray($row,'name');
             $shortname=$this->getUtildata()->getVaueOfArray($row,'shortname');
             $name=$rolelib->getDefaultRoleName($shortname,$name);
			 $add=true;
			if($notgradebook){
				 if($shortname=='frontpage' || $shortname=='user' || $shortname=='guest'){ $add=false;}
			}
			 if($cfkey=='shortname' && $add){ $list[$shortname]=$name;}
             else  if($add){$list[$id]=$name;}
        }
		
       return $list;
     }
	 public function getRolesGradebook() { 
          $list=$this->getRolesGradebookData();
       return $list;
     }
	 public function getRolesShortnameKeyGradebook() { 
          $list=$this->getRolesGradebookData(array('key'=>'shortname'));
       return $list;
     }
	 
	 public function getRolesNotGradebook() { 
          $list=$this->getRolesGradebookData(array('notgradebook'=>1));
       return $list;
     }
	 public function getRolesShortnameKeyNotGradebook() { 
          $list=$this->getRolesGradebookData(array('key'=>'shortname','notgradebook'=>1));
       return $list;
     }
    public function getEnableEnrolMethod() { 
        $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
        $sqlservice->setSessionhashkey($this->getSessionhashkey());
         $sql="SELECT DISTINCT enrol  FROM {_pfx}enrol";
         $result=$sqlservice->search($sql);
       $list=array();
       if(empty($result)){return  $list;}
       foreach ($result as $row) {
             $enrol=$this->getUtildata()->getVaueOfArray($row,'enrol');
             $list[$enrol]=$enrol;
        }
       return $list;
     }
     function getPluginsWithGrade() {
        $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
        $sqlservice->setSessionhashkey($this->getSessionhashkey());
         $sql="SELECT DISTINCT itemmodule FROM {_pfx}grade_items WHERE itemtype !='course' ORDER BY itemmodule";
         $result=$sqlservice->search($sql);
       $list=array();
       foreach ($result as $row) {
             $itemmodule=$this->getUtildata()->getVaueOfArray($row,'itemmodule');
             $list[$itemmodule]=$itemmodule;
        }
       return $list;
     }
     
      function getCourseGradeItem() {
          $list=array();
          $courseid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('courseid');
          if(empty($courseid)){return $list; }
          
          $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
          $sqlservice->setSessionhashkey($this->getSessionhashkey());
          $sql="SELECT id,itemname,itemtype FROM {_pfx}grade_items WHERE courseid =$courseid  ORDER BY itemname";
          $result=$sqlservice->search($sql);
       $list=array();
       $finalgradeid=null;
       foreach ($result as $row) {
             $id=$this->getUtildata()->getVaueOfArray($row,'id');
             $itemname=$this->getUtildata()->getVaueOfArray($row,'itemname');
              $itemtype=$this->getUtildata()->getVaueOfArray($row,'itemtype');
             if($itemtype=='course'){$finalgradeid=$id;}
             else{$list[$id]=$itemname;}
        }
        if($finalgradeid){
            $itemname=$this->getTranslator()->trans('badiu.moodle.mreport.grade.coursefinalgrade');
            $list[$finalgradeid]=$itemname;
            
        }
      
       return $list;
     }
     
     function getPlugins() {
      $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
      $sqlservice->setSessionhashkey($this->getSessionhashkey());
       $sql="SELECT DISTINCT id,name FROM {_pfx}modules WHERE id > 0 ORDER BY name ";
       $result=$sqlservice->search($sql);
     $list=array();
     foreach ($result as $row) {
           $id=$this->getUtildata()->getVaueOfArray($row,'id');
           $name=$this->getUtildata()->getVaueOfArray($row,'name');
           $list[$id]=$name;
      }
     return $list;
   }
   
   function getPluginsShortname() {
      $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
      $sqlservice->setSessionhashkey($this->getSessionhashkey());
       $sql="SELECT DISTINCT name FROM {_pfx}modules WHERE id > 0 ORDER BY name ";
       $result=$sqlservice->search($sql);
     $list=array();
     foreach ($result as $row) {
           $name=$this->getUtildata()->getVaueOfArray($row,'name');
           $list[$name]=$name;
      }
     return $list;
   }
    function getPluginsCourseShortname() {
		$courseid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('courseid');
		if(empty($courseid)){return null;}
      $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
      $sqlservice->setSessionhashkey($this->getSessionhashkey());
      $sql="SELECT DISTINCT m.name FROM {_pfx}course_modules cm INNER JOIN {_pfx}modules m ON m.id=cm.module WHERE  cm.course=$courseid";
       $result=$sqlservice->search($sql);
     $list=array();
     foreach ($result as $row) {
           $name=$this->getUtildata()->getVaueOfArray($row,'name');
           $list[$name]=$name;
      }
     return $list;
   }
   function getPluginsCourse() {
		$courseid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('courseid');
		if(empty($courseid)){return null;}
      $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
      $sqlservice->setSessionhashkey($this->getSessionhashkey());
      $sql="SELECT DISTINCT cm.id,m.name FROM {_pfx}course_modules cm INNER JOIN {_pfx}modules m ON m.id=cm.module WHERE  cm.course=$courseid";
       $result=$sqlservice->search($sql);
     $list=array();
     foreach ($result as $row) {
           $id=$this->getUtildata()->getVaueOfArray($row,'id');
		   $name=$this->getUtildata()->getVaueOfArray($row,'name');
           $list[$id]=$name;
      }
     return $list;
   }
   function getPluginsLog() {
    $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
    $sqlservice->setSessionhashkey($this->getSessionhashkey());
     $sql="SELECT DISTINCT id,name FROM {_pfx}modules WHERE id > 0 ORDER BY name ";
     $result=$sqlservice->search($sql);
   $list=array();
   foreach ($result as $row) {
         
         $name=$this->getUtildata()->getVaueOfArray($row,'name');
         $key="mod_".$name;
         $list[$key]=$name;
    }
   return $list;
 }
     function getCourseGradeItemtype() {
          $list=array(); 
         
         $list['mod']=$this->getTranslator()->trans('badiu.moodle.core.grede.itemtypemod');
         $list['manual']=$this->getTranslator()->trans('badiu.moodle.core.grede.itemtypemanaul');
         $list['course']=$this->getTranslator()->trans('badiu.moodle.core.grede.itemtypecourse');
      
       return $list;
     }
     function getCourseGradeItemtypemod() {
          $list=array(); 
         
         $list['mod']=$this->getTranslator()->trans('badiu.moodle.core.grede.itemtypemod');
           
       return $list;
     }
	 
     public function getUserMethodAuth() { 
        $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
        $sqlservice->setSessionhashkey($this->getSessionhashkey());
         $sql="SELECT DISTINCT auth  FROM {_pfx}user";
         $result=$sqlservice->search($sql);
         $list=array();
         foreach ($result as $row) {
             $auth=$this->getUtildata()->getVaueOfArray($row,'auth');
             $list[$auth]=$auth;
         }
       return $list;
     }
	 
	 function getActivityFilterIndicator() {
          $list=array(); 
         
         $list['completed']=$this->getTranslator()->trans('badiu.moodle.core.indicator.completed');
		 $list['notcompleted']=$this->getTranslator()->trans('badiu.moodle.core.indicator.notcompleted');
		 $list['accessed']=$this->getTranslator()->trans('badiu.moodle.core.indicator.accessed');
		 $list['notaccessed']=$this->getTranslator()->trans('badiu.moodle.core.indicator.notaccessed');
		 $list['withgrade']=$this->getTranslator()->trans('badiu.moodle.core.indicator.withgrade');
		 $list['withoutgrade']=$this->getTranslator()->trans('badiu.moodle.core.indicator.withoutgrade');
           
       return $list;
     }
	  public function getActivityForAutocomplete() { 
        $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$text=$this->getUtildata()->getVaueOfArray($param,'gsearch');
		$courseid=$this->getUtildata()->getVaueOfArray($param,'courseid');
		$serviceid=$this->getUtildata()->getVaueOfArray($param,'_serviceid');
		if(empty($serviceid)){return  null;}
		$libcourseactivityitem=$this->getContainer()->get('badiu.moodle.core.libcourseactivityitem');
		$libcourseactivityitem->setSessionhashkey($this->getSessionhashkey());
        $libcourseactivityitem->getSqlservice()->setServiceid($serviceid);
	   		
		$result=$libcourseactivityitem->searchCourseInstancesModules($courseid,$text);
        
       return $result; 
     }
     public function getActivityForAutocompleteOnEdit($id) { 
            $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
            if(empty($serviceid)){return  null;}
			$libcourseactivityitem=$this->getContainer()->get('badiu.moodle.core.libcourseactivityitem');
			$libcourseactivityitem->setSessionhashkey($this->getSessionhashkey());
			$libcourseactivityitem->getSqlservice()->setServiceid($serviceid);
	   		
			$result=$libcourseactivityitem->getInstanceName($id);
            return $result;
     }

     public function getCourseForAutocomplete() { 
        $gsearch=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch');
        $wsql="";
        if(!empty($gsearch)){ 
			$gsearch=addslashes($gsearch);
			$wsql.=" AND LOWER(CONCAT(id,fullname,shortname)) LIKE '%".strtolower($gsearch)."%' ";
		}
        $sql="SELECT id,fullname as name,shortname  FROM {_pfx}course WHERE id > 0  $wsql ";
        $this->sqlservice->setSessionhashkey($this->getSessionhashkey());
        $result=$this->sqlservice->search($sql);
      
       return $result;
     }
     public function getCourseForAutocompleteOnEdit($id) { 
            $this->sqlservice->setSessionhashkey($this->getSessionhashkey());
            if(empty($id)){$id=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('courseid');;}
            if(empty($id)){return null;}
            $sql="SELECT fullname as name  FROM {_pfx}course WHERE id= $id";
            $result=$this->sqlservice->searchSingle($sql);

            $result=$this->getUtildata()->getVaueOfArray($result,'name');
            return $result;
     }

  public function getCoursecatForAutocomplete() { 
        $gsearch=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch');
		$sserviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
        if(empty($gsearch)){return null;}
		$gsearch=addslashes($gsearch);
        $sparam=array();
        $sparam['_serviceid']=$sserviceid;
        $sparam['name']=$gsearch;
		$sparam['_key']='local.badiunet.course.category.getlisttree';
      
        $result=$this->sqlservice->service($sparam);
		$result=$this->sqlservice->serviceResponse($result);
	  
       return $result;
     }

	public function getCoursecatparentForAutocompleteOnEdit($param) { 
	
         $coursecatid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('coursecatid');
		 $sserviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
		 
         if(empty($coursecatid)){return null;}
        $sparam=array();
        $sparam['_serviceid']=$sserviceid;
        $sparam['id']=$coursecatid;
	    $sparam['name']='';
		$sparam['_key']='local.badiunet.course.category.getlisttree';
      
        $result=$this->sqlservice->service($sparam);
        $result=$this->sqlservice->serviceResponse($result);
		
	    $coursecatname=$this->getUtildata()->getVaueOfArray($result,'0.name',true);
		
       return  $coursecatname;
     }
	 
	 
	  public function getUserForAutocomplete() { 
        $gsearch=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch');
        $wsql="";
        if(!empty($gsearch)){ 
			$gsearch=addslashes($gsearch);
			$wsql.=" AND LOWER(CONCAT(id,username,firstname,lastname,email)) LIKE '%".strtolower($gsearch)."%' ";
		}
        $sql="SELECT id,firstname,lastname,email  FROM {_pfx}user WHERE  deleted=0 AND confirmed=1 $wsql ";
        $this->sqlservice->setSessionhashkey($this->getSessionhashkey());
        $result=$this->sqlservice->search($sql);
		
		$newlist=array();
         if(is_array($result)){
			 foreach($result as $iuser) {
				 $id=$this->getUtildata()->getVaueOfArray($iuser,'id');
				 $firstname=$this->getUtildata()->getVaueOfArray($iuser,'firstname');
				 $lastname=$this->getUtildata()->getVaueOfArray($iuser,'lastname');
				 $email=$this->getUtildata()->getVaueOfArray($iuser,'email');
				 $fname= "$firstname $lastname - $email";
				 $nitem=array('id'=>$id,'name'=>$fname);
				 array_push($newlist,$nitem);
			 }
		 }
       return $newlist;
     }
	 public function getUserCourseEnrolForAutocomplete() { 
        $gsearch=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch');
		$courseid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('courseid');
        $wsql="";
		
		if(empty($courseid)){ return null;}
        if(!empty($gsearch)){ 
			$gsearch=addslashes($gsearch);
			$wsql.=" AND LOWER(CONCAT(u.id,u.username,u.firstname,u.lastname,u.email)) LIKE '%".strtolower($gsearch)."%' ";
		}
        $sql="SELECT u.id,u.firstname,u.lastname,u.email  FROM {_pfx}role_assignments rs INNER JOIN {_pfx}user u ON rs.userid=u.id  INNER JOIN {_pfx}context e ON rs.contextid=e.id INNER JOIN {_pfx}enrol en ON e.instanceid=en.courseid INNER JOIN {_pfx}user_enrolments ue ON (en.id=ue.enrolid AND rs.userid=ue.userid)  WHERE e.contextlevel=50 AND e.instanceid=$courseid AND rs.userid=ue.userid  AND u.deleted=0 AND u.suspended =0  $wsql ";
        $this->sqlservice->setSessionhashkey($this->getSessionhashkey());
        $result=$this->sqlservice->search($sql);
		
		$newlist=array();
         if(is_array($result)){
			 foreach($result as $iuser) {
				 $id=$this->getUtildata()->getVaueOfArray($iuser,'id');
				 $firstname=$this->getUtildata()->getVaueOfArray($iuser,'firstname');
				 $lastname=$this->getUtildata()->getVaueOfArray($iuser,'lastname');
				 $email=$this->getUtildata()->getVaueOfArray($iuser,'email');
				 $fname= "$firstname $lastname - $email";
				 $nitem=array('id'=>$id,'name'=>$fname);
				 array_push($newlist,$nitem);
			 }
		 }
       return $newlist;
     }
     public function getUserForAutocompleteOnEdit($id) { 
            $this->sqlservice->setSessionhashkey($this->getSessionhashkey());
            if(empty($id)){$id=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('userid');}
            if(empty($id)){return null;}
            $sql="SELECT firstname,lastname,email  FROM {_pfx}user WHERE id= $id";
            $result=$this->sqlservice->searchSingle($sql);

            $firstname=$this->getUtildata()->getVaueOfArray($result,'firstname');
			$lastname=$this->getUtildata()->getVaueOfArray($result,'lastname');
			$email=$this->getUtildata()->getVaueOfArray($result,'email');
			$result= "$firstname $lastname - $email";
            return $result;
     }
	 /* review
	  public function getCoursecatForAutocomplete() { 
        $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
        $gsearch=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch');
        $wsql="";
        if(!empty($gsearch)){ $wsql=" AND LOWER(CONCAT(id,name)) LIKE '%".strtolower($gsearch)."%'";}
        $sql="SELECT id,name  FROM {_pfx}course_categories WHERE id > 0  $wsql ";
        
        $result=$this->sqlservice->search($sql);
      
       return $result;
     }
     
     public function getCoursecatForAutocompleteOnEdit($param) { 
            $dconfig = $this->getJson()->decode($param, true);
            $lms= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');
            $coursename=$this->getUtildata()->getVaueOfArray($lms, 'lmscoursecatname');
            return $coursename;
     }
    
     public function getCoursegroupForAutocomplete() { 
         $lmscourse=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('lmscourse');
        $gsearch=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch');
        $wsql="";
         if(!empty($lmscourse)){ $wsql.=" AND courseid=$lmscourse ";}
        if(!empty($gsearch)){ $wsql.=" AND LOWER(CONCAT(id,name)) LIKE '%".strtolower($gsearch)."%' ";}
        $sql="SELECT id,name  FROM {_pfx}groups WHERE id > 0  $wsql ";
         $result=$this->sqlservice->search($sql);
      
       return $result;
     }
     public function getCoursegroupForAutocompleteOnEdit($param) { 
         
            $dconfig = $this->getJson()->decode($param, true);
            $lms= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');
            $coursegroupname=$this->getUtildata()->getVaueOfArray($lms, 'lmscoursegroupname');
            return $coursegroupname;
     }*/

	public function getCoreSurveyQuestionType() {
        $list = array();
        $list['badiu.moodle.core.sync.questionnaire.response.boolean'] = $this->getTranslator()->trans('badiu.moodle.core.survey.questiontype.boolean');
		$list['badiu.moodle.core.sync.questionnaire.response.choiceone'] = $this->getTranslator()->trans('badiu.moodle.core.survey.questiontype.choiceone');
		$list['badiu.moodle.core.sync.questionnaire.response.text'] = $this->getTranslator()->trans('badiu.moodle.core.survey.questiontype.text');
        $list['badiu.moodle.core.sync.questionnaire.response.rank'] = $this->getTranslator()->trans('badiu.moodle.core.survey.questiontype.rank');
		
        return $list;
    }

     function getServiceid(){
      return $this->serviceid;
  }

  function setServiceid($serviceid) {
      $this->serviceid = $serviceid;
  }
}
