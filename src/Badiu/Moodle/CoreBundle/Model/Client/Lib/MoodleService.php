<?php

namespace Badiu\Moodle\CoreBundle\Model\Client\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class MoodleService extends BadiuModelLib {
    private $param;
    private $templetecript;
    private $response; 
    private $criptk1;
    private $criptk2;
    private $ipallowed;
    function __construct(Container $container) {
        parent::__construct($container);
        $this->templetecript=$this->getContainer()->get('badiu.system.core.lib.util.templetecript');
        $this->response= $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
        if(isset($_SERVER['SERVER_ADDR'])){$this->ipallowed=$_SERVER['SERVER_ADDR'];}
        else {$this->ipallowed=null;}
        
    }

    public function exec(){
        
        $this->param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
        $reqparam =$this->getUtildata()->getVaueOfArray($this->param,'_param');
		$servicekeyinstance=$this->getUtildata()->getVaueOfArray($this->param,'servicekeyinstance');
		
        $this->param=$this->templetecript->decode($reqparam);
        $this->param=$this->getJson()->decode($this->param,true);
		if(!empty($servicekeyinstance)){$this->param['servicekeyinstance']=$servicekeyinstance;}
        $validateparam=$this->validateParam();
        if(!empty($validateparam)){return $validateparam;}
        
        //validate token dna
        
        
        //validate moodleurl
      
        //check if exist
      
          $token=$this->existService();
        
          if($token){
              $result=array('token'=>$token,'criptk1'=>$this->criptk1,'criptk2'=>$this->criptk2,'ipallowed'=>$this->ipallowed);
             if(!empty($servicekeyinstance)){$result['servicekeyinstance']=$servicekeyinstance;}
              $result=$this->getJson()->encode($result);
              $result=$this->templetecript->encode('s1',$result);
              return $result; 
          }
          if(!$token  && !empty($servicekeyinstance)){
			  $result=array('servicekeyinstance'=>$servicekeyinstance,'errorservicekeyinstance'=>'badiu.admin.server.service.appclient.servicekeynotregistred');
			   $result=$this->getJson()->encode($result);
              $result=$this->templetecript->encode('s1',$result);
              return $result; 
		  }
        // add entity
        $entity=$this->addEntity();
       
        //add service
        $sserviceid=$this->addService($entity);
        
        //gerenate token
        $token=$this->generateToken($entity,$sserviceid);
        
        //add token
        $result=array('token'=>$token,'criptk1'=>$this->criptk1,'criptk2'=>$this->criptk2,'ipallowed'=>$this->ipallowed);
        $result=$this->getJson()->encode($result);
        $result=$this->templetecript->encode('s1',$result);
       
       return $result;
    }
     public function addtokendna(){
          $token=$this->getUtildata()->getVaueOfArray($this->param,'token');
          $tokendna=$this->getUtildata()->getVaueOfArray($this->param,'tokendna');
          $paranexist=array('servicetoken'=>$token);
          $data=$this->getContainer()->get('badiu.admin.server.service.data');
          $exist=$data->countGlobalRow($paranexist);
          if($exist){
               $id=$data->getGlobalColumnValue('id',$paranexist); 
               $param=array('id'=>$id,'servicetokendna'=>$tokendna);
               $result=$data->updateNativeSql($param,false);
               if($result){ $result=1;}
               else { $result=0;}
               return $result;
          }else{
              $response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
              $info='badiu.moodle.mreport.client.servicetokennotvalid';
              $message='Tonken not exist in database';
              $response->denied($info,$message);
              return $response;
          }
     }
     
      function enableService(){
       //  servicedisabled
        return true;
    }
    
    function validateTokenDna($token){
        return true;
    }
    
    function validateMoodleUrl($url){
        return true;
    }
    
   
    private function addEntity(){
         $data=$this->getContainer()->get('badiu.system.entity.entity.data');
        
        $name=$this->getUtildata()->getVaueOfArray($this->param,'moodlename');
        $name.= " - ".$this->getUtildata()->getVaueOfArray($this->param,'moodleurl');
        $tokendna=$this->getUtildata()->getVaueOfArray($this->param,'tokendna');
        
        $paranexist=array('idnumber'=>$tokendna);
        $exist=$data->countGlobalRow($paranexist);
        $result=null;
        if(!$exist){
            //statuid
            $statusdata=$this->getContainer()->get('badiu.system.entity.status.data');
            $statuid=$statusdata->getGlobalColumnValue('id',array('shortname'=>'active'));
            $param=array('name'=>$name,'deleted'=>0,'idnumber'=>$tokendna,'statusid'=>$statuid,'timecreated'=> new \DateTime());
            $result=$data->insertNativeSql($param,false);
        }else{
            $result=$data->getGlobalColumnValue('parentid',$paranexist);
            if(!empty($result)){ return  $result;}
           $result=$data->getGlobalColumnValue('id',$paranexist);
        }
        return  $result;
    }
    private function existService(){
		
        $tokendna=$this->getUtildata()->getVaueOfArray($this->param,'tokendna');
        $data=$this->getContainer()->get('badiu.admin.server.service.data');
        $paranexist=array('servicetokendna'=>$tokendna);
		$servicekeyinstance=$this->getUtildata()->getVaueOfArray($this->param,'servicekeyinstance');
		if(!empty($servicekeyinstance)){ $paranexist['servicekey']=$servicekeyinstance;}
		
        $exist=$data->countGlobalRow($paranexist);
		
		if(!$exist && !empty($servicekeyinstance)){$paranexist=array('servicekey'=>$servicekeyinstance);$exist=$data->countGlobalRow($paranexist);}
		
        if(!$exist){return false;}
        else {
           $result=$data->getGlobalColumnValue('servicetoken',$paranexist); 
           $dconfig=$data->getGlobalColumnValue('dconfig',$paranexist); 
           $dconfig= $this->getJson()->decode($dconfig,true);
           if(empty($dconfig)){$dconfig=array();}
           $this->criptk1=$this->getUtildata()->getVaueOfArray($dconfig,'cript.k1',true);
           $this->criptk2=$this->getUtildata()->getVaueOfArray($dconfig,'cript.k2',true);
           if(empty($this->criptk1) || empty($this->criptk2)){
                $this->criptk1=$this->templetecript->makeKey();
                $this->criptk2=$this->templetecript->makeKey();
                $dconfig['cript']=array('k1'=>$this->criptk1,'k2'=>$this->criptk2);
                 $dconfig=$this->getJson()->encode($dconfig);
                 
                $sserverid=$data->getGlobalColumnValue('id',$paranexist); 
                $param=array('id'=>$sserverid,'dconfig'=>$dconfig);
				if(!empty($servicekeyinstance)){$param['servicetokendna']=$tokendna;}
                $data=$this->getContainer()->get('badiu.admin.server.service.data');
                $data->updateNativeSql($param,false);
           }
           return $result;
        }
	return false;
    }
    private function addService($entity){
        $tokendna=$this->getUtildata()->getVaueOfArray($this->param,'tokendna');
        $name=$this->getUtildata()->getVaueOfArray($this->param,'moodlename');
        $url=$this->getUtildata()->getVaueOfArray($this->param,'moodleurl');
        $versionnumber=$this->getUtildata()->getVaueOfArray($this->param,'moodleversionnumber');
        $versiontxt=$this->getUtildata()->getVaueOfArray($this->param,'moodleversiontxt');
		$serviceversionumber=$this->getUtildata()->getVaueOfArray($this->param,'client.plugin.version',true);
		$serviceversiontext=$this->getUtildata()->getVaueOfArray($this->param,'client.plugin.release',true);
        $moodleadmins=$this->getUtildata()->getVaueOfArray($this->param,'moodleadmins');
        $this->criptk1=$this->templetecript->makeKey();
        $this->criptk2=$this->templetecript->makeKey();
        $dconfig=array('usersadministers'=>$moodleadmins,'cript'=>array('k1'=>$this->criptk1,'k2'=>$this->criptk2));
        $dconfig=$this->getJson()->encode($dconfig);
        
        $servicerurl='/local/badiunet/synchttp/expdata.php';
        $servicerurl1='/local/badiuws/sync.php';
        
        $param=array('entity'=>$entity,'name'=>$name,'url'=>$url,'servicerurl'=>$servicerurl,'servicerurl1'=>$servicerurl1,'dtype'=>'site_moodle','versiontxt'=>$versiontxt,'versionnumber'=>$versionnumber,'servicetokendna'=>$tokendna,'dconfig'=>$dconfig,'serviceversiontext'=>$serviceversiontext,'serviceversionumber'=>$serviceversionumber,'scheduleinherit'=>1,'statusinfo'=>'servicefree','deleted'=>0,'timecreated'=> new \DateTime());
        $data=$this->getContainer()->get('badiu.admin.server.service.data');
        $result=$data->insertNativeSql($param,false);
        return  $result;
    } 
  
    private function generateToken($entity,$instanceid) {
	$stoken="";
	$hash=$this->getContainer()->get('badiu.system.core.lib.util.hash');
	$token=$hash->make(30);
	$user=2;
	$plugin="mreport";
	$stoken="$entity|$token|$user|$plugin|$instanceid";
        $param=array('id'=>$instanceid,'servicetoken'=>$stoken);
        $data=$this->getContainer()->get('badiu.admin.server.service.data');
        $result=$data->updateNativeSql($param,false);
        
	return $stoken;
   }
   //review
   function getServiceid(){ 
        $this->param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
        $token=$this->getUtildata()->getVaueOfArray($this->param,'_token');
        $data=$this->getContainer()->get('badiu.admin.server.service.data');
        if(empty($token)){$token=$this->getUtildata()->getVaueOfArray($this->param,'token');}
        if(empty($token)){
           
            $response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
              $info='badiu.moodle.mreport.client.tokenisempty';
              $message='Tonken is required';
              $response->denied($info,$message);
              return $response;
         }
        $param=array('servicetoken'=>$token);
         $exist=$data->countGlobalRow($param);
        if(!$exist){
            $response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
              $info='badiu.moodle.mreport.client.servicetokennotvalid';
              $message='Tonken not exist in database';
              $response->denied($info,$message);
              return $response;
         }
        else {
           $id= $data->getGlobalColumnValue('id', $param); 
           return $id;
        }
        
    }
    
    private function validateParam(){
        $info=null;
        $message=null;
        
        $tokendna=$this->getUtildata()->getVaueOfArray($this->param,'tokendna');
        $name=$this->getUtildata()->getVaueOfArray($this->param,'moodlename');
        $url=$this->getUtildata()->getVaueOfArray($this->param,'moodleurl');
       
        if(empty($tokendna)){
            $info="badiu.moodle.mreport.client.lib.moodleservice.tokendnarequired";
            $message="Param tokendna is empty";
        }else if(empty($name)){
            $info="badiu.moodle.mreport.client.lib.moodleservice.moodlenamerequired";
            $message="Param moodlename is empty";
        }else if(empty($url)){
            $info="badiu.moodle.mreport.client.lib.moodleservice.moodleurlrequired";
            $message="Param moodleurl is empty";
        }
     
        if(!empty($info) && !empty($message)){
             return $this->response->denied($info,$message);
            
        }
       return null;
        
    }
    function getParam() { 
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }


}
