<?php

namespace Badiu\Moodle\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuMoodleCoreBundle:Default:index.html.twig', array('name' => $name));
    }
}
