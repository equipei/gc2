<?php

namespace Badiu\Util\HousingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UtilHousingEdifice
 *
 * @ORM\Table(name="util_housing_edifice", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="util_housing_edifice_name_uix", columns={"entity","enterpriseid",  "name"}), 
 *      @ORM\UniqueConstraint(name="util_housing_edifice_shortname_uix", columns={"entity", "shortname"}), 
 *      @ORM\UniqueConstraint(name="util_housing_edifice_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="util_housing_edifice_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="util_housing_edifice_name_ix", columns={"name"}), 
 *              @ORM\Index(name="util_housing_edifice_shortname_ix", columns={"shortname"}), 
 *              @ORM\Index(name="util_housing_edifice_categoryid_ix", columns={"categoryid"}),
 *              @ORM\Index(name="util_housing_edifice_addressdataid_ix", columns={"addressdataid"}),
 *              @ORM\Index(name="util_housing_edifice_enterpriseid_ix", columns={"enterpriseid"}),
 *              @ORM\Index(name="util_housing_edifice_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="util_housing_edifice_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="util_housing_edifice_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="util_housing_edifice_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class UtilHousingEdifice
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

 /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;
    /**
     * @var UtilHousingEdificeCategory
     *
     * @ORM\ManyToOne(targetEntity="UtilHousingEdificeCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;
    
	 /**
     * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="enterpriseid", referencedColumnName="id")
     * })
     */
    private $enterpriseid;
   
    
    /**
   * @var integer
   *
   * @ORM\Column(name="addressdataid", type="bigint", nullable=true)
     */
    private $addressdataid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    	 /**
		 * @var string
		 *
		 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
		 */
		private $shortname;

 /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;


    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;

    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getCategoryid() {
        return $this->categoryid;
    }

    public function setCategoryid(UtilHousingEdificeCategory $categoryid) {
        $this->categoryid = $categoryid;
    }

public function getShortname() {
        return $this->shortname;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

  

public function setEnterpriseid(\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise $enterpriseid)
    {
        $this->enterpriseid = $enterpriseid;

    }
    public function getEnterpriseid()
    {
        return $this->enterpriseid;
    }
	
	 
  /**
     * @return string
     */
    public function getModulekey()
    {
        return $this->modulekey;
    }

    /**
     * @param string $dtype
     */
    public function setModulekey($modulekey)
    {
        $this->modulekey = $modulekey;
    }


     /**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string $dtype
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }
    function getAddressdataid() {
        return $this->addressdataid;
    }

    function setAddressdataid($addressdataid) {
        $this->addressdataid = $addressdataid;
    }


}
