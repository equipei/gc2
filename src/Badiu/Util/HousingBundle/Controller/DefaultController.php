<?php

namespace Badiu\Util\HousingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuUtilHousingBundle:Default:index.html.twig', array('name' => $name));
    }
}
