<?php

namespace Badiu\Util\HousingBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class DisciplineFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
     public function get($key){ 
        
         if($key=='moduleinstance') return $this->getEnterprise();
           return array();
     }
    public  function getEnterprise(){
		$sformutil=$this->getContainer()->get('badiu.system.core.lib.form.util');
		$enterpriseData=$this->getContainer()->get('badiu.admin.enterprise.enterprise.data');
		$badiuSession=$this->container->get('badiu.system.access.session');
		
		$list=$enterpriseData->getFormChoice($badiuSession->get()->getEntity(),'name');
		$list=$sformutil->convertBdArrayToListOptions($list);
        return $list;
    }

}
