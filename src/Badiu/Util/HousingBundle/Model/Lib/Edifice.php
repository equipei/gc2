<?php

namespace Badiu\Util\HousingBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Edifice extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function getDataFormEdit($edificeid) {
			 $fdata=array();
			 $fdata=$this->getEdificeDb($edificeid,$fdata);
                         $addressdataid=$fdata['addressdataid'];
			 $fdata=$this->getAddressDb($addressdataid,$edificeid,$fdata);
			
			 return $fdata;
	}
	public function save($data) {
			$sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
			$isEdit=$sysoperation->isEdit();
			$edificeid=null;
			if($isEdit){
				$edificeid=$this->getContainer()->get('request')->get('id');
			}
			$dto=$data->getDto();
			
			//save user
			$data->setDto($this->getEdificeForm($edificeid,$dto));
			$result=$data->save();
			$edificeid=$data->getDto()->getId();
		
			//save address
			$addressDto=$this->getAddressForm($edificeid,$dto,$isEdit);
			$addressData=$this->getContainer()->get('badiu.util.address.data.data');
			$addressData->setDto($addressDto);
			$addressData->save();
                        $addressdataid=$addressData->getDto()->getid();
                      
                        $data->getDto()->setAddressdataid($addressdataid);
                        $data->save();
				
			return $data->getDto();
		}
	public function getEdificeDb($edificeid,$fdata) {
			 
			$data=$this->getContainer()->get('badiu.util.housing.edifice.data');
			$dto=$data->findById($edificeid);
			$fdata['name']= $dto->getName();
			$fdata['shortname']=$dto->getShortname();
                        if(!empty($dto->getEnterpriseid())){$fdata['enterpriseid']=$dto->getEnterpriseid()->getId();}
			 $fdata['idnumber']=$dto->getIdnumber();
			$fdata['param']=$dto->getParam();
			$fdata['description']=$dto->getDescription();
                        $fdata['addressdataid']=$dto->getAddressdataid();
			return $fdata;
		
	}
	
	public function getEdificeForm($edificeid,$fdata) {
			$dto=null;
			if(!empty($edificeid)){
				$data=$this->getContainer()->get('badiu.util.housing.edifice.data');
				$dto=$data->findById($edificeid);
			}else{
				$dto=$this->getContainer()->get('badiu.util.housing.edifice.entity');
				$dto=$this->initDefaultEntityData($dto);
			}
			
			
			if(isset($fdata['name'])) {$dto->setName($fdata['name']);}
			if(isset($fdata['shortname'])) {$dto->setEmail($fdata['shortname']);}
			if(isset($fdata['enterpriseid'])) {$dto->setEnterpriseid($this->getContainer()->get('badiu.admin.enterprise.enterprise.data')->findById($fdata['enterpriseid']));}
			if(isset($fdata['idnumber'])) {$dto->setIdnumber($fdata['idnumber']);}
			if(isset($fdata['param'])) {$dto->setParam($fdata['param']);}
			if(isset($fdata['description'])) {$dto->setDescription($fdata['description']);}
			
			return $dto;
		
	}
	
	
	public function getAddressDb($addressdataid,$edificeid,$fdata) {
		
			$data=$this->getContainer()->get('badiu.util.address.data.data');
                        $dto=null;
                        if(!empty($addressdataid)){$dto=$data->findById($addressdataid);}
                        else{$dto=$data->findLastByShortnameCategory('badiu.util.housing.edifice',$edificeid,'professional');}
			
			
			if($dto!=null){
				$stateid='';
				if(!empty($dto->getStateid())){$stateid=$dto->getStateid()->getId();}
				
                                if(!empty($dto->getCountryid())){$fdata['countryid']=$dto->getCountryid()->getId();}
                                $fdata['stateid']=$stateid;
				$fdata['state']=$dto->getState();
				$fdata['city']=$dto->getCity();
				$fdata['address']=$dto->getAddress();
				$fdata['cep']=$dto->getCep();
				$fdata['neighborhood']=$dto->getNeighborhood();
				$fdata['addressnumber']=$dto->getAddressnumber();
				$fdata['addressinfo']=$dto->getAddressinfo();
                                
				
			}
			return $fdata;
		}
	public function getAddressForm($edificeid,$fdata,$edit=FALSE) {
		
			$dto=null;
			if($edit){
				$data=$this->getContainer()->get('badiu.util.address.data.data');
				$dto=$data->findLastByShortnameCategory('badiu.util.housing.edifice',$edificeid,'professional');
			}
			if(empty($dto)){
				$dto=$this->getContainer()->get('badiu.util.address.data.entity');
				$dto=$this->initDefaultEntityData($dto);
				$dto->setModulekey('badiu.util.housing.edifice');
				$dto->setModuleinstance($edificeid);
				$dto->setCategoryid($this->getAddressCategory());
				
			}
		
		
			if(isset($fdata['stateid'])) {$dto->setStateid($this->getContainer()->get('badiu.util.address.state.data')->findById($fdata['stateid']));}
                        if(isset($fdata['countryid'])) {$dto->setCountryid($this->getContainer()->get('badiu.util.address.country.data')->findById($fdata['countryid']));}
			if(isset($fdata['state'])) {$dto->setState($fdata['state']);}
			if(isset($fdata['city'])) {$dto->setCity($fdata['city']);}
			if(isset($fdata['address'])) {$dto->setAddress($fdata['address']);}
			if (isset($fdata['cep'])) {$dto->setCep($fdata['cep']);}
			if(isset($fdata['neighborhood'])) {$dto->setNeighborhood($fdata['neighborhood']);}
			if(isset($fdata['addressnumber'])) {$dto->setAddressnumber($fdata['addressnumber']);}
			if(isset($fdata['addressinfo'])) {$dto->setAddressinfo($fdata['addressinfo']);}
		
			
			return $dto;
		
	}
		
	public function getAddressCategory($shortname='professional') {
		$data=$this->getContainer()->get('badiu.util.address.category.data');
		$dto=$data->findByShortname($this->getEntity(),$shortname);
		return $dto;
	}
	
	
}
