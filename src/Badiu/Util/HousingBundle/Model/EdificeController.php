<?php

namespace Badiu\Util\HousingBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EdificeController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
              

     public function addEdificeidToroom($dto,$dtoParent) {
        $dto->setEdificeid($dtoParent);
        return $dto;
        }


     public function findEdificeidInroom($dto) {
      
        return $dto->getEdificeid()->getId();
    }

     public function setDefaultDataOnOpenEditForm($dto) {
                  $lib=$this->getContainer()->get('badiu.util.housing.edifice.lib');
		  $id=$this->getContainer()->get('request')->get('id');
		  $dto=$lib->getDataFormEdit($id);
		  return $dto;
     }
    
	 public function save($data) {
		$dto=$data->getDto();
		$lib=$this->getContainer()->get('badiu.util.housing.edifice.lib');
		$result=$data->setDto($lib->save($data));
		return $result;
    }   
}
