<?php

namespace Badiu\Util\HousingBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class EdificeData  extends BadiuDataBase{
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

public function getMenuRoom($id) {

        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit=$sysoperation->isEdit();
      
        if($isEdit){
            $roomdata=$this->getContainer()->get('badiu.util.housing.room.data');
            $sql="SELECT c.id, c.name FROM ".$roomdata->getBundleEntity()." o JOIN o.edificeid c WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
            return  $result;
           
            
            }else{
                
                return  $this->getNameById($id);
            }
       
       
    }

}
