<?php

namespace Badiu\Util\AddressBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CityController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
              

     public function addCityidToneighborhood($dto,$dtoParent) {
        $dto->setCityid($dtoParent);
        return $dto;
        }


     public function findCityidInneighborhood($dto) {
      
        return $dto->getCityid()->getId();
    }

}
