<?php

namespace Badiu\Util\AddressBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Install extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
       
           $result= $this->initDbCountryBR();
		   $result+=$this->initPermissions();
    } 

     public function initDbCountryBR() {
         $cont=0;
         $datacountry = $this->getContainer()->get('badiu.util.address.country.data');
         $datastate = $this->getContainer()->get('badiu.util.address.state.data');
         $entity=$this->getEntity();
        
         //add country
         $param=array();
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.util.address.country.BR');
         $param['shortname']='BR';
         $param['fcode']=55;
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         if(!$datacountry->existByShortname($entity,'BR')){
              $result = $datacountry->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
      
         $liststate=array();
         $liststate['AC']=$this->getTranslator()->trans('badiu.util.address.state.BR.AC');
         $liststate['AL']=$this->getTranslator()->trans('badiu.util.address.state.BR.AL');
         $liststate['AM']=$this->getTranslator()->trans('badiu.util.address.state.BR.AM');
         $liststate['AP']=$this->getTranslator()->trans('badiu.util.address.state.BR.AP');
         $liststate['BA']=$this->getTranslator()->trans('badiu.util.address.state.BR.BA');
         $liststate['CE']=$this->getTranslator()->trans('badiu.util.address.state.BR.CE');
         $liststate['DF']=$this->getTranslator()->trans('badiu.util.address.state.BR.DF');
         $liststate['ES']=$this->getTranslator()->trans('badiu.util.address.state.BR.ES');
         $liststate['GO']=$this->getTranslator()->trans('badiu.util.address.state.BR.GO');
         $liststate['MA']=$this->getTranslator()->trans('badiu.util.address.state.BR.MA');
         $liststate['MG']=$this->getTranslator()->trans('badiu.util.address.state.BR.MG');
         $liststate['MS']=$this->getTranslator()->trans('badiu.util.address.state.BR.MS');
         $liststate['MT']=$this->getTranslator()->trans('badiu.util.address.state.BR.MT');
         $liststate['PA']=$this->getTranslator()->trans('badiu.util.address.state.BR.PA');
         $liststate['PB']=$this->getTranslator()->trans('badiu.util.address.state.BR.PB');
         $liststate['PE']=$this->getTranslator()->trans('badiu.util.address.state.BR.PE');
         $liststate['PI']=$this->getTranslator()->trans('badiu.util.address.state.BR.PI');
         $liststate['PR']=$this->getTranslator()->trans('badiu.util.address.state.BR.PR');
         $liststate['RJ']=$this->getTranslator()->trans('badiu.util.address.state.BR.RJ');
         $liststate['RN']=$this->getTranslator()->trans('badiu.util.address.state.BR.RN');
         $liststate['RO']=$this->getTranslator()->trans('badiu.util.address.state.BR.RO');
         $liststate['RR']=$this->getTranslator()->trans('badiu.util.address.state.BR.RR');
         $liststate['RS']=$this->getTranslator()->trans('badiu.util.address.state.BR.RS');
         $liststate['SC']=$this->getTranslator()->trans('badiu.util.address.state.BR.SC');
         $liststate['SE']=$this->getTranslator()->trans('badiu.util.address.state.BR.SE');
         $liststate['SP']=$this->getTranslator()->trans('badiu.util.address.state.BR.SP');
         $liststate['TO']=$this->getTranslator()->trans('badiu.util.address.state.BR.TO');

         $countryid=  $datacountry->getIdByShortname($entity,'BR');
         //add state
         foreach ($liststate as $statekey => $statename) {
             //type add CNPJ
            $param=array();
            $param['entity']=$entity;
            $param['countryid']=$countryid;
            $param['name']=$statename;
            $param['shortname']=$statekey;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
            if(!$datastate->existByShortname($entity,$statekey)){
                $result = $datastate->insertNativeSql($param,false); 
                if($result){$cont++;}
            }
         }
         
     }
	 
	  public function initPermissions() {
        $cont=0;
		
        $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');

		//guest
		$param=array(
            'roleshortname'=>'guest',
            'permissions'=>array('badiu.system.core.service.process/badiu.util.address.lib.brcepserviceviacep')
		);
      $cont+=$libpermission->add($param);
	  
        //authenticateduser
		$param=array(
            'roleshortname'=>'authenticateduser',
            'permissions'=>array('badiu.system.core.service.process/badiu.util.address.lib.brcepserviceviacep')
		);
      $cont+=$libpermission->add($param);

		return  $cont;
    }
}
