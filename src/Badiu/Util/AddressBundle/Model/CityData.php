<?php

namespace Badiu\Util\AddressBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class CityData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

public function getMenuNeighborhood($id) {

        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit=$sysoperation->isEdit();
      
        if($isEdit){
            $neighborhooddata=$this->getContainer()->get('badiu.util.address.neighborhood.data');
            $sql="SELECT c.id, c.name FROM ".$neighborhooddata->getBundleEntity()." o JOIN o.cityid c WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
            return  $result;
           
            
        }else{
            
            return  $this->getNameById($id);
        }
       
       
    }
	
	
	    public function getForAutocomplete() {
			$p=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		//	print_R($p);exit;
         $name=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
         $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        
        $wsql="";
        if(!empty($name)){
            $wsql=" AND LOWER(CONCAT(o.id,o.name))  LIKE :name ";
            $name=strtolower($name);
        }
         $sql="SELECT DISTINCT o.id,o.name FROM ".$this->getBundleEntity()." o WHERE  o.entity = :entity  $wsql ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(50);
        $result= $query->getResult();
        return  $result;
    }
  public function getNameByIdOnEditAutocomplete($id) {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT o.name FROM ".$this->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }

}
