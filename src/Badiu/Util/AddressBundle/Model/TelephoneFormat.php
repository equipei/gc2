<?php

namespace Badiu\Util\AddressBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class TelephoneFormat extends BadiuFormat{
     
     function __construct(Container $container) {
            parent::__construct($container);
       } 


public  function type($data){
            $value="";
			
			$fixe=$this->getTranslator()->trans('badiu.util.address.telephone.fixe');
			$mobile=$this->getTranslator()->trans('badiu.util.address.telephone.mobile');
             if (array_key_exists("dtype",$data)){
				$value=$data['dtype'];
				if($value=='fixe'){$value=$fixe;}
				else if($value=='mobile'){$value=$mobile;}
			 }
          return $value; 
    } 
}
