<?php

namespace Badiu\Util\AddressBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class StateData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

public function getMenuCity($id) {

        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit=$sysoperation->isEdit();
      
        if($isEdit){
            $citydata=$this->getContainer()->get('badiu.util.address.city.data');
            $sql="SELECT s.id, s.name FROM ".$citydata->getBundleEntity()." o JOIN o.stateid s WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
            return  $result;
         
            
        }else{
            
            return  $this->getNameById($id);
        }
       
       
    }

    
    public function getInfoById($id) {
        $sql = "SELECT o.name,o.shortname FROM " . $this->getBundleEntity() . " o  WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
        return $result;
    }
    
    
}
