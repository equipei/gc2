<?php

namespace Badiu\Util\AddressBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class TelephoneFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
  
  public  function getType(){
        $list=array();
        $list['fixe']=$this->getTranslator()->trans('badiu.util.address.telephone.fixe');
        $list['mobile']=$this->getTranslator()->trans('badiu.util.address.telephone.mobile');
        return $list;
    }

}
