<?php

namespace Badiu\Util\AddressBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class AddressData extends BadiuDataBase {

    function __construct(Container $container, $bundleEntity) {
        parent::__construct($container, $bundleEntity);
    }

//review
    public function findByModuleinstance($modulekey, $moduleinstance, $category) {

        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit = $sysoperation->isEdit();

        if ($isEdit) {
            $citydata = $this->getContainer()->get('badiu.util.address.city.data');
            $sql = "SELECT o FROM " . $citydata->getBundleEntity() . " o JOIN o.stateid s WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id', $id);
            $result = $query->getSingleResult();
            return $result;
        } else {

            return $this->getNameById($id);
        }
    }

    public function findLastByShortnameCategory($modulekey, $moduleinstance, $shortnameCategory) {

        //get categoryid
        $categorydata = $this->getContainer()->get('badiu.util.address.category.data');

        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $entity = $badiuSession->get()->getEntity();

        $categoryid = $categorydata->getIdByShortname($entity, $shortnameCategory);
        if (!empty($categoryid)) {
            $sql = "SELECT MAX(o.id) AS id FROM " . $this->getBundleEntity() . " o  WHERE o.modulekey=:modulekey AND o.moduleinstance=:moduleinstance AND o.categoryid = :categoryid ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('modulekey', $modulekey);
            $query->setParameter('moduleinstance', $moduleinstance);
            $query->setParameter('categoryid', $categoryid);

            $id = $query->getOneOrNullResult();
            if (!empty($id)) {
                $id = $id['id'];
            }

            if (!empty($id)) {
                $sql = "SELECT o FROM " . $this->getBundleEntity() . " o  WHERE o.id=:id";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('id', $id);
                $result = $query->getSingleResult();
                return $result;
            }
        }

        return null;
    }

    public function getModuleinstance($id) {
        $sql = "SELECT o.moduleinstance FROM " . $this->getBundleEntity() . " o  WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
        if (!empty($result)) {
            $result = $result['moduleinstance'];
        }
        return $result;
    }
     public function getCountryFcodeById($id) {
        $sql = "SELECT c.fcode FROM " . $this->getBundleEntity() . " o JOIN o.countryid c WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
        if (!empty($result)) {
            $result = $result['fcode'];
        }
        return $result;
    }

}
