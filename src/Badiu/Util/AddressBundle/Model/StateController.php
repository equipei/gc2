<?php

namespace Badiu\Util\AddressBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class StateController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
              

     public function addStateidToCity($dto,$dtoParent) {
        $dto->setStateid($dtoParent);
        return $dto;
    }


     public function findStateidInCity($dto) {
      
        return $dto->getStateid()->getId();
    }

}
