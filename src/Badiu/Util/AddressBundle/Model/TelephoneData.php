<?php

namespace Badiu\Util\AddressBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class TelephoneData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }


	public function findLastByShortnameCategory($modulekey,$moduleinstance,$shortnameCategory,$dtype,$sortorder=null) {
		//get categoryid
        $categorydata=$this->getContainer()->get('badiu.util.address.category.data');
		
		 $badiuSession=$this->getContainer()->get('badiu.system.access.session');
         $entity=$badiuSession->get()->getEntity();
				 
		$categoryid=$categorydata->getIdByShortname($entity,$shortnameCategory);
			if(!empty($categoryid)){
			$sortorderSql="";
			if($sortorder > 0){$sortorderSql=" AND o.sortorder = :sortorder ";}
		
			$sql="SELECT MAX(o.id) AS id FROM ".$this->getBundleEntity()." o  WHERE o.modulekey=:modulekey AND o.moduleinstance=:moduleinstance AND o.categoryid = :categoryid  AND o.dtype = :dtype $sortorderSql";
			$query = $this->getEm()->createQuery($sql);
            $query->setParameter('modulekey',$modulekey);
			$query->setParameter('moduleinstance',$moduleinstance);
			$query->setParameter('categoryid',$categoryid);
			$query->setParameter('dtype',$dtype);
			if($sortorder > 0){$query->setParameter('sortorder',$sortorder);}
			$id= $query->getOneOrNullResult();
			if(!empty($id)){$id=$id['id'];}
           
			if(!empty($id)){
				$sql="SELECT o FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
				$query = $this->getEm()->createQuery($sql);
				$query->setParameter('id',$id);
				$result= $query->getSingleResult();
				return  $result;
			}
		   
			
		}
       
       return null;
    }
    
    public function getModuleinstance($id) {
		$sql="SELECT o.moduleinstance FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
		$query = $this->getEm()->createQuery($sql);
                $query->setParameter('id',$id);
		$result= $query->getOneOrNullResult();
		if(!empty($result)){$result=$result['moduleinstance'];}
		return $result;
	}
}
