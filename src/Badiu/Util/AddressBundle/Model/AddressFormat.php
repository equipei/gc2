<?php

namespace Badiu\Util\AddressBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class AddressFormat extends BadiuFormat{
     
     function __construct(Container $container) {
            parent::__construct($container);
       } 


public  function defaultdd($data){
            $value="";
            $addresscomplement="";
            $addressnumber="";
            $address="";
            $state="";
            $country="";
            $stateshortname="";
            
            $cep="";
            $city="";
            $neighborhood="";
            $email=$this->getUtildata()->getVaueOfArray($data,'email');
            $phone=$this->getUtildata()->getVaueOfArray($data,'phone');
            $phonemobile=$this->getUtildata()->getVaueOfArray($data,'phonemobile');
           if(empty($data)){return null;}
             if (array_key_exists("addresscomplement",$data)){$addresscomplement=$data["addresscomplement"]; }              
             if (array_key_exists("addressnumber",$data)){$addressnumber=$data["addressnumber"]; }
             if (array_key_exists("address",$data)){$address=$data["address"]; }
             if (array_key_exists("state",$data)){$state=$data["state"]; }
             if (array_key_exists("country",$data)){$country=$data["country"]; }
             if (array_key_exists("stateshortname",$data)){$stateshortname=$data["stateshortname"]; }
             if (array_key_exists("cep",$data)){$cep=$data["cep"]; }
             else if (array_key_exists("postcode",$data)){$cep=$data["postcode"]; }
             if (array_key_exists("city",$data)){$city=$data["city"]; }
             if (array_key_exists("neighborhood",$data)){$neighborhood=$data["neighborhood"]; }
             
             
             $value.=$address;
             $value.=" ".$addressnumber;
             if(!is_array($addresscomplement)){ $value.=" ".$addresscomplement;}
              if(!empty($neighborhood) &&!empty($city) ){ $value.="<br>".$neighborhood ."      ".$city."    ".$state;}
             else {
                 if(!empty($neighborhood)){ $value.="<br>$neighborhood   $stateshortname";}
                 if(!empty($city) ){ $value.="<br>$city     $stateshortname";}
             }
            if(!empty($cep)){ $value.="<br>CEP: $cep";} 
            if(!empty($country)){ $value.="<br> $country";} 
            if(!empty($email)){$value.="<br>E-mail: $email";}
            if(!empty($phone)){$value.="<br>Tel fixo: $phone";}
            if(!empty($phonemobile)){$value.="<br>Tel cel: $phonemobile";}
            
          return $value; 
    } 
    
    
}
