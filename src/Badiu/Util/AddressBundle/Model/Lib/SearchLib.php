<?php

namespace Badiu\Util\AddressBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class SearchLib extends BadiuModelLib {

	//http://54.156.96.187/badiunet/web/app_dev.php/system/service/process?_service=badiu.admin.certificate.factorycreate
	//http://54.156.96.187/badiunet/web/app_dev.php/system/service/process?_service=badiu.admin.certificate.factorycreate&modulekey=badiu.ams.enrol.classe&moduleinstance=47&userid=1025
	
     function __construct(Container $container) {
        parent::__construct($container);
	} 
 
  	
	public function getCityForAutocomplete() {
		 $p=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		
         $name=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
		 $uf=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('banetsysform_dadosprofissionais_govbruf');
         if(empty($uf)){$uf=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('state');}
		 $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        
        $wsql="";
		 if(!empty($uf)){
			$wsql.=" AND e.shortname=:state ";
		 }
        if(!empty($name)){
            $wsql.=" AND LOWER(CONCAT(o.id,o.shortname,o.idnumber,o.name))  LIKE :name ";
            $name=strtolower($name);
        }
		$citydata=$this->getContainer()->get('badiu.util.address.city.data');
         $sql="SELECT DISTINCT o.shortname AS id,o.name FROM ".$citydata->getBundleEntity()." o JOIN  o.stateid e WHERE  o.entity = :entity  $wsql ";
       
        $query = $citydata->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        if(!empty($uf)){$query->setParameter('state',$uf);}
		if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(15);
        $result= $query->getResult();
        return  $result;
    }
  public function getCityNameByIdOnEditAutocomplete($id) {
		$data= $this->getContainer()->get('badiu.util.address.city.data');
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT o.name FROM ".$data->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }
  public function getCityNameByShortnameOnEditAutocomplete($id) {
		$data= $this->getContainer()->get('badiu.util.address.city.data');
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT o.name FROM ".$data->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.shortname=:id ";
        $query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }
  	public function getCountryForAutocomplete() {
		 $p=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		
         $name=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
		 
		 $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        
        $wsql="";
		 
        if(!empty($name)){
            $wsql.=" AND LOWER(CONCAT(o.id,o.shortname,o.name))  LIKE :name ";
            $name=strtolower($name);
        }
		$data=$this->getContainer()->get('badiu.util.address.country.data');
         $sql="SELECT DISTINCT o.shortname AS id,o.name FROM ".$data->getBundleEntity()." o   WHERE  o.entity = :entity  $wsql ";
       
        $query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(15);
        $result= $query->getResult();
        return  $result;
    }
  public function getCountryNameByIdOnEditAutocomplete($id) {
		$data= $this->getContainer()->get('badiu.util.address.country.data');
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT o.name FROM ".$data->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }
  
  public function getCountryNameByShortnameOnEditAutocomplete($id) {
		$data= $this->getContainer()->get('badiu.util.address.country.data');
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT o.name FROM ".$data->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.shortname=:id ";
        $query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }
}

