<?php

namespace Badiu\Util\AddressBundle\Model\Lib\Br;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ContactFormat extends BadiuFormat {
    private $json = null;
	function __construct(Container $container) {
		parent::__construct($container);
                $this->json = $this->getContainer()->get('badiu.system.core.lib.util.json');
	}

       
public  function personal($data){
               
                $contactdtype=$this->getContactDataByType($data,'personal');
                $address=$this->getUtildata()->getVaueOfArray($contactdtype,'address');
                $cep=$this->getUtildata()->getVaueOfArray($contactdtype,'postcode');
                $addressnumber=$this->getUtildata()->getVaueOfArray($contactdtype,'addressnumber');
                $addresscomplement=$this->getUtildata()->getVaueOfArray($contactdtype,'addresscomplement');
                $neighborhood=$this->getUtildata()->getVaueOfArray($contactdtype,'neighborhood');
                $city=$this->getUtildata()->getVaueOfArray($contactdtype,'city');
                $stateid=$this->getUtildata()->getVaueOfArray($contactdtype,'stateid');
                $state=$this->getUtildata()->getVaueOfArray($contactdtype,'state');
                $stateshortname=$this->getUtildata()->getVaueOfArray($contactdtype,'stateshortname');
                $country=$this->getUtildata()->getVaueOfArray($contactdtype,'country');
                $phone=$this->getUtildata()->getVaueOfArray($contactdtype,'phone');
                $phonemobile=$this->getUtildata()->getVaueOfArray($contactdtype,'phonemobile');
                $email=$this->getUtildata()->getVaueOfArray($contactdtype,'email');

               
              $tel=$phone;
              $labeltel=$this->getTranslator()->trans('badiu.util.address.telephone.name');
              if(!empty($tel)){
                  $tel="$labeltel: $tel";
                  if(!empty($phonemobile)){$tel.="  $phonemobile ";}
              }else{
                  if(!empty($phonemobile)){$tel="$labeltel: $phonemobile ";}
              }
              
              
             
             $value="";
             $value.=$address;
             $value.=" ".$addressnumber;
              $value.=" ".$addresscomplement;
              if(!empty($neighborhood) &&!empty($city) ){ $value.="<br>".$neighborhood ."      ".$city."    ".$state;}
              else {
                 if(!empty($neighborhood)){ $value.="<br>$neighborhood   $stateshortname";}
                 if(!empty($city) ){ $value.="<br>$city     $stateshortname";}
              }
            if(!empty($cep)){ $value.="<br>CEP: $cep";} 
            if(!empty($country)){ $value.="<br> $country";} 
            if(!empty($tel)){ $value.="<br> $tel";} 
       
          return $value; 
    } 
    public  function professional($data){
               
                $contactdtype=$this->getContactDataByType($data,'professional');
                $address=$this->getUtildata()->getVaueOfArray($contactdtype,'address');
                $cep=$this->getUtildata()->getVaueOfArray($contactdtype,'postcode');
                $addressnumber=$this->getUtildata()->getVaueOfArray($contactdtype,'addressnumber');
                $addresscomplement=$this->getUtildata()->getVaueOfArray($contactdtype,'addresscomplement');
                $neighborhood=$this->getUtildata()->getVaueOfArray($contactdtype,'neighborhood');
                $city=$this->getUtildata()->getVaueOfArray($contactdtype,'city');
                $stateid=$this->getUtildata()->getVaueOfArray($contactdtype,'stateid');
                $state=$this->getUtildata()->getVaueOfArray($contactdtype,'state');
                $stateshortname=$this->getUtildata()->getVaueOfArray($contactdtype,'stateshortname');
                $country=$this->getUtildata()->getVaueOfArray($contactdtype,'country');
                $phone=$this->getUtildata()->getVaueOfArray($contactdtype,'phone');
                $phonemobile=$this->getUtildata()->getVaueOfArray($contactdtype,'phonemobile');
                $email=$this->getUtildata()->getVaueOfArray($contactdtype,'email');

        //  professional
             
              $tel=$phone;
              $labeltel=$this->getTranslator()->trans('badiu.util.address.telephone.name');
              if(!empty($tel)){
                  $tel="$labeltel: $tel";
                  if(!empty($phonemobile)){$tel.="  $phonemobile ";}
              }else{
                  if(!empty($phonemobile)){$tel="$labeltel: $phonemobile ";}
              }
              
              
             
             $value="";
             $value.=$address;
             $value.=" ".$addressnumber;
              $value.=" ".$addresscomplement;
              if(!empty($neighborhood) &&!empty($city) ){ $value.="<br>".$neighborhood ."      ".$city."    ".$state;}
              else {
                 if(!empty($neighborhood)){ $value.="<br>$neighborhood   $stateshortname";}
                 if(!empty($city) ){ $value.="<br>$city     $stateshortname";}
              }
            if(!empty($cep)){ $value.="<br>CEP: $cep";} 
            if(!empty($country)){ $value.="<br> $country";} 
            if(!empty($tel)){ $value.="<br> $tel";} 
            if(!empty($email)){ $email.="<br> E-mail: $email";} 
          return $value; 
    } 
   private  function getContactDataByType($data,$type='personal'){
                $contactdata=$this->getUtildata()->getVaueOfArray($data,'contactdata');
                $contact=$this->json->decode($contactdata,true);
               
                $contactdata=$this->getUtildata()->getVaueOfArray($contact,$type);
                return $contactdata;
   }
}
