<?php

namespace Badiu\Util\AddressBundle\Model\Lib\Br;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ContactValidate {

    public function __construct(Container $container) {

        $this->container = $container;       
    }
    
    public function cep($cep){

        if(!preg_match('/^[0-9]{5,5}([- ]?[0-9]{3,3})?$/', $cep)) {

             return false;
        }        
        
        return true;
    }


    public function phoneFixe($number){


        if(!preg_match('^\(+[0-9]{2,3}\) [0-9]{4}-[0-9]{4}$^', $number)){

            return false;
        }

        return true;
    }

    
        public function phoneMobile($number){


        if(!preg_match('^\((10)|([1-9][1-9])\) [2-9][0-9]{3}-[0-9]{4}^', $number)){

            return false;
        }

        return true;
        
    }
   
}
