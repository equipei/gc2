<?php
namespace Badiu\Util\AddressBundle\Model\Lib\Br\Cep;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class LayoutDbFileTxtCorreio extends BadiuModelLib{
    
	
    function __construct(Container $container) {
            parent::__construct($container);
		
              }
    
	public function getData($line) {
		$nline=null;
                
		 $address= new \stdClass();
		 $address->cidade=trim(substr($line,17,77));
		 $address->bairro=trim(substr($line,102,77));
		 $address->logradouro=$this->getLogradouro($line);
		 $address->cep=trim(substr($line,518,8));
		return $address;
	}
	
	
	public function getLogradouro($line) {
           $value=substr($line,446,72);
           $value.= " ";
            $value.=substr($line,526,11);
            
             $value.= " "; 
            $value.=substr($line,538,36);
            
             $value.= " ";
            $value.=substr($line,574,11);
           return $value;
        }
        
      
}
