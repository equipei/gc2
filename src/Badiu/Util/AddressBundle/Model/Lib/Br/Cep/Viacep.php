<?php

namespace Badiu\Util\AddressBundle\Model\Lib\Br\Cep;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class Viacep extends BadiuModelLib{
    private $postcode;
    function __construct(Container $container) {
            parent::__construct($container);
           }
    
         /**
          * Undocumented function
          * error options postcodeempty | postcodeformatnotvalid |remoteservicepostcodenotavailable | remoteservicepostcodenotvalid | postcodeformatnotvalid
          * @return array  
          */
         public function exec() {
            $cep=null;
            if(empty($this->postcode)){
               $cep=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('cep');         
            }else{
               $cep=$this->postcode;
            }
             $valid=$this->validate($cep);
             if($valid!=null){return $valid;}
             $address=$this->searchRemote($cep);
           
            return $address;
         
         }
         public function searchRemote($cep) {
            $url = "https://viacep.com.br/ws/$cep/json";
            $result=null;
            try{
               $result= file_get_contents($url);
            } catch (\Exception $e) {
              
               $result=array('error'=>'remoteservicepostcodenotavailable');
               return $result;
            }
           
           
            $result = $this->getJson()->decode($result,true);

            $haserror=$this->getUtildata()->getVaueOfArray($result, 'erro');
            if($haserror){
               return array('error'=>'remoteservicepostcodenotvalid');
            }
            $newarray = array();
            $address=$this->getUtildata()->getVaueOfArray($result, 'logradouro');
            
            $postcode=$this->getUtildata()->getVaueOfArray($result, 'cep');
            if(!empty($postcode)){$postcode=trim($postcode);}
         
             $city=$this->getUtildata()->getVaueOfArray($result, 'localidade');
            if(!empty($city)){$city=trim($city);}
         
            $state=$this->getUtildata()->getVaueOfArray($result, 'uf');
            if(!empty($state)){$state=trim($state);}
         
            $stateid=null;
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $entity = $badiuSession->get()->getEntity(); 
            $stateid=$this->getContainer()->get('badiu.util.address.state.data')->getIdByShortname($entity,$state);
            
            $neighborhood=$this->getUtildata()->getVaueOfArray($result, 'bairro');
            if(!empty($neighborhood)){$neighborhood=trim($neighborhood);}
         
            $addresscomplement=$this->getUtildata()->getVaueOfArray($result, 'complemento');
            if(!empty($addresscomplementd)){$addresscomplement=trim($addresscomplement);}
            
            $ibge=$this->getUtildata()->getVaueOfArray($result, 'ibge');
            $newarray['address'] = $address;
            $newarray['addresscomplement'] = $addresscomplement; 
            $newarray['postcode'] = $postcode;
            $newarray['city'] = $city;
            $newarray['neighborhood'] = $neighborhood;
            $newarray['state'] = $state;
            $newarray['stateid'] = $stateid;
            $newarray['ibge'] = $ibge;
            return $newarray;
         }

         public function validate($cep) {
            $result=null;
            if(empty($cep)){
               $result=array('error'=>'postcodeempty');
            }
            $formatedcep = preg_replace("/[^0-9]/", "", $cep);
            if (!preg_match('/^[0-9]{8}?$/', $formatedcep)) {
               $result=array('error'=>'postcodeformatnotvalid');
            }
            return  $result;
         }
   
         public function getPostcode() {
            return $this->postcode;
        }
        
        public function setPostcode($postcode) {
            $this->postcode = $postcode;
        }
}
