<?php
namespace Badiu\Util\AddressBundle\Model\Lib\Br\Cep;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class LayoutDbFileTxtCorreio extends BadiuModelLib{
    
	
    function __construct(Container $container) {
            parent::__construct($container);
		
              }
    
	public function getData($line) {
		$nline=null;
		 if(!empty($line)){
			$line=explode("   ", $line);
			$line=$this->formatLine($line);
			$address=$this->splitData($line);
			return $address;
		 }
		return null;
	}
	
	public function formatLine($line) {
		$nline=$line; 
		//print_r($line);exit;
		foreach ($line as $k => $v) {
			// preg_split('/\s+/', $this_line);
			if (empty($v) || $v == "" || $v == "\t") {
                unset($nline[$k]);
            }
        }
		return $nline;
	}
	
	public function splitData($sline) {
		 $address= new \stdClass();
		 $address->cidade=null;
		 $address->bairro=null;
		 $address->logradouro=null;
		 $address->cep=null;
		  $sline= array_values($sline);
		 //print_r($sline);
		if(!array_key_exists (1,$sline) || !array_key_exists (2,$sline) || !array_key_exists (3,$sline) || !array_key_exists (4,$sline) || !array_key_exists (5,$sline) || !array_key_exists (6,$sline) || !array_key_exists (7,$sline)){return null;}
		
			$address->cidade = preg_replace('/[0-9]+/', "", $sline[1]);
			$address->bairro = preg_replace('/[0-9]+/', "", $sline[2]);
			$address->logradouro = preg_replace('/[0-9]+/', " ",$sline[4]) . ' ' . preg_replace('/[0-9]+/', "", $sline[5]);
			$address->cep = preg_replace("/\D/", "", $sline[7]);
			if(strlen($address->cep)>8){$address->cep=substr($address->cep, 0, 8);}
		return $address;

    }

	
}
