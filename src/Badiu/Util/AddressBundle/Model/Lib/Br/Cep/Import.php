<?php
namespace Badiu\Util\AddressBundle\Model\Lib\Br\Cep;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Import extends BadiuModelLib{
    //http://fabrica.badiu.com.br/~colaborador7/badiunet/web/app_dev.php/system/service/process?_service=badiu.util.address.lib.brcepimport&_function=updateDefaultAddressState&uf=DF
    /*
     * 
 AC - 3549
 AL - 9624
 AM - 15169
 AP - 2512
 BA - 47459
 CE - 25364
 DF - 33696
 ES - 33835
 GO - 52798
 MA - 13444
 MG - 79691
 MS - 13406
 MT - 15735
 PA - 23024
 PB - 12617
 PE - 45838
 PI - 10176
 PR - 52144
 RJ - 93990
 RN - 12641
 RO - 8767
 RR - 2400
 RS - 44478
 SC - 38333
 SE - 4605
 SP - 251469
 TO - 7317
     */
	private $pathfiles;
    function __construct(Container $container) {
            parent::__construct($container);
			 $this->pathfiles=$this->getContainer()->getParameter('badiu.util.addrress.br.cep.path');
              }
    
	public function exec() {
         
		$result=$this->pathfiles;
		//$rstate=$this->updateState();
		 $this->updateStatesAddress();
		 //$this->updateDefaultAddressState();
		//return $rstate; 
	}
	public function getStates() {
       
            $uf = array();
            $uf['AC']="Acre";
            $uf['AL']="Alagoas";
            $uf['AM']="Amazonas";
            $uf['AP']="Amap�";
            $uf['BA']="Bahia";
            $uf['CE']="Ceará";
            $uf['DF']="Distrito Federal";
            $uf['ES']="Espirito Santo";//ti li
            $uf['GO']="Goi�s";
            $uf['MA']="Maranhão";
            $uf['MG']="Minas Gerais";
            $uf['MS']="Mato Grosso do Sul";
            $uf['MT']="Mato Grosso";
            $uf['PA']="Par�";
            $uf['PB']="Para�ba";
            $uf['PE']="Pernambuco";
            $uf['PI']="Piau�";
            $uf['PR']="Paran�";
            $uf['RJ']="Rio de Janeiro";
            $uf['RN']="Rio Grande do Norte";
            $uf['RO']="Rond�nia";
            $uf['RR']="Roraima";
            $uf['RS']="Rio Grande do Sul";
            $uf['SC']="Santa Catarina";
            $uf['SE']="Sergipe";
            $uf['SP']="S�o Paulo";
            $uf['TO']="Tocantins";
            return $uf;
    }

    public function updateState() {
            $list=$this->getStates();
			 
			 $data=$this->getContainer()->get('badiu.util.address.state.data');
			 $cont=0;
       		 foreach ($list as $key => $value){
				 $dto=$this->getContainer()->get('badiu.util.address.state.entity');
				 $dto = $this->initDefaultEntityData($dto);
                 $dto->setName($value);
				 $dto->setShortname($key);
				 $dto->setIdnumber($key);
				 $dto->setCountry('br'); 
				 $data->setDto($dto);
				 //$exit=$data->existByParam($this->getEntity(),array('idnumber'=>$key,'shortname'=>$key));
				 if(!$data->existAddIdnumber()){
					$data->save();
					$cont++;
				 }
			 }
		return $cont;

    }
  
    public function updateDefaultAddressState($state='DF') {
                        //$start_memory = memory_get_usage(true);
                       //$start_memory1=$start_memory/1024/1024;
                       // echo   "<br>Memoria atual  $start_memory1";
			$uf=$this->getContainer()->get('request')->get('uf');
			if(!empty($uf)){$state=$uf;}
                        //	$filep='/home/colaborador8/public_html/Readfile/files/_DNE_GU_DF_LOGRADOUROS.TXT';
			$filep= $this->pathfiles."/DNE_GU_".$state."_LOGRADOUROS.TXT";
			//echo $filep."<br>";
			$fp = fopen($filep, "r")or die("Couldn't get handle");
			$cont=0;
			$process=0;
                       // echo "<br>state: $state";
			$stateid=$this->getContainer()->get('badiu.util.address.state.data')->getIdByShortname($this->getEntity(),$state);
                       //  echo "<br>stateid: $stateid";exit;
                        //echo "id estate $stateid";exit;
			$layout=$this->getContainer()->get('badiu.util.address.lib.brceplayoutdbfiletxtcorreio');
                        $listdbcep=$this->getCEP($state);
                        //echo sizeof($listdbcep);
                       
			$contprocess=0;
                        //$contmemory=0;
			if ($fp) {
                          //  $t1=time();
				 while (!feof($fp)) {
					$line = fgets($fp);
					
					 $data=$this->getContainer()->get('badiu.util.address.default.data');
					 //$data->getEm()->getConnection()->getConfiguration()->setSQLLogger(null);
					$address=null;
                                        if($cont> 0) {$address=$layout->getData($line);}
					if( $cont > 0 && $address!=null){
                                          //  echo "<br>". $line ." -- ".strlen($line);
                                              // echo "<br>". $address->cidade . " - ". $address->bairro ." -- ".$address->logradouro --." -- ".$address->cep;
						$address->estado=$state;
						//echo "------------------------------<br>";
						//print_r($address);
                                                $existcep=array_key_exists($address->cep,$listdbcep);
						if($address->cep!='00000000' && !$existcep){
                                                       
							$r=$this->updateDefaultAddress($address,$stateid,$data);
							$process++;
                                                      //   $contmemory++;
                                                        // $atualmemory= memory_get_usage(true) - $start_memory;
                                                     //    $atualmemory=$atualmemory/1024/1024;
                                                     //   echo "<br>  $atualmemory";
						}
						
						
					}
                                     /* if($contmemory=100){
                                          $contmemory=0;
                                         gc_collect_cycles();
                                      }*/
                                       /* if($process==2000){
                                             $t2=time();
                                            $tp=$t2-$t1;
                                             echo "<br> time: ".$tp;
                                            echo "<br>cont: $cont processed: $process";exit;
                                            
                                        }*/
					$cont++;
                                        //if($cont==10){exit;}
					//echo "<br> result: $r";
				}
				 fclose($fp);
                               //  $t2=time();
                              //   $tp=$t2-$t1;
                                //    echo "\n $state - $cont";
                                // echo "\n UF: ".$uf;
                                
                               //  echo "\n cont: ".$cont;
			}
                       echo "cont: $cont  process: $process";  
		return $process;

    }
	
   public function updateDefaultAddress($address,$stateid,$data) {
            //$list=$this->getStates();
			 
			 
			 //$cont=0; 
       		 
				 
				 $dto=array();
				 $dto['deleted'] =0;// $this->initDefaultEntityData($dto);
                                 $dto['stateid'] =$stateid;
                                 $dto['state']=$address->estado;
			
				 $dto['city']=$address->cidade;
				 $dto['neighborhood']=$address->bairro;
				 $dto['address']=$address->logradouro;
				 $dto['cep']=$address->cep;
				 
				 $dto['country']='br'; 
                                 //print_r($dto);exit;
				 $exist=$data->existByParam($this->getEntity(),array('cep'=>$dto['cep']));
				if(!$exist){
                                    // $t1=time();
                                    echo $data->insertNativeSql($dto);
                                   
                                    
                                   // $t2=time();
                                   // $tf=$t2-$t1;
                                 // echo "<br> id".$data->getDto()->getId() . " time save: $tf";
                                }
                               //  $data->getEm()->clear();
                              
				// }else{/* echo "<br>exist: ".$dto->getCep();*/}
                                 //$data=null;
			 
		return null; //$data->getDto()->getId();

    }
	public function updateStatesAddress(){
			$uf=$this->getContainer()->get('request')->get('uf');
			if(!empty($uf)){
				$result=$this->updateDefaultAddressState($uf);
					echo "$uf - endereço processado: $result <br>";
					return null;
			}
		  $list=$this->getStates();
		   foreach ($list as $key => $value){
				//echo "<br>Estado: $key ";
				$result=$this->updateDefaultAddressState($key);
				//echo "endereço processado: $result <br>";
		   }
	}
	public function getStatesid($state){
			$data=$this->getContainer()->get('badiu.util.address.state.data');
			$sql="SELECT o.id FROM ".$data->getBundleEntity()." o  WHERE  o.entity = :entity AND o.shortname = :shortname ";
                        $query = $data->getEm()->createQuery($sql);
                        $query->setParameter('entity',$this->getEntity());
			$query->setParameter('shortname',$state);
                        $result= $query->getSingleResult();
                        $result=$result['id'];
                        return $result;
	}
        
        public function getCEP($state){
			$data=$this->getContainer()->get('badiu.util.address.default.data');
			$sql="SELECT o.cep FROM ".$data->getBundleEntity()." o  WHERE  o.entity = :entity AND o.state = :state ";
                        $query = $data->getEm()->createQuery($sql);
                        $query->setParameter('entity',$this->getEntity());
			$query->setParameter('state',$state);
                        $result= $query->getResult();
                      
                        $lcep=array();
                        foreach ( $result as $r){
                          $lcep[$r['cep']]=null;
                        }
                        
                        return $lcep;
	}
}
