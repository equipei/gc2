<?php

namespace Badiu\Util\AddressBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ContactValidate {

    public function __construct(Container $container) {

        $this->container = $container;       
    }
    

    public function email($email){

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){

        return false;

        }

        return true;

    }


      
}
