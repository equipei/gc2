<?php
namespace Badiu\Util\AddressBundle\Model\Form\TypeData;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class TelephoneFormTypeData extends BadiuModelLib{
   function __construct(Container $container) {  
            parent::__construct($container);
	}
    
	public function updateDtoByFormData($dto,$formdata,$field) {
		if(isset($formdata[$field]) &&  is_array($formdata[$field])){
			$fdata=$formdata[$field];
			
			if(isset($fdata['field1']) && is_numeric($fdata['field1'])) {
				$operatorid=$this->getAddressTelephoneOperator($fdata['field1']);
				if($operatorid!=null)$dto->setOperatorid($operatorid);
			}
			
			if(isset($fdata['field2'])) {
				$field2=$fdata['field2'];
				$field2=preg_replace('/[^0-9]/', '', $field2);
				$dto->setLprefix($field2);
			}
			if(isset($fdata['field3'])) {
				$field3=$fdata['field3'];
				$field3=preg_replace('/[^0-9]/', '', $field3);
				$dto->setNumbertxt($field3);
			}
			
		}
		return  $dto;
	}
	
	public function updateFormDataByDto($dto,$formdata,$field) {
		 
		 $field1="";
		 $field2="";
		 $field3="";
		 
		 if(!empty($dto)){
			if(!empty($dto->getOperatorid()) && !empty($dto->getOperatorid()->getId())){$field1=$dto->getOperatorid()->getId();}
			$field2=$dto->getLprefix();
			$field3=$dto->getNumbertxt();
			
		 }
		$fdata=array();
		$fdata['field1']=$field1;
		$fdata['field2']=$field2;
		$fdata['field3']=$field3;
		$formdata[$field]=$fdata;
		return  $formdata;
	}
	public function getAddressTelephoneOperator($operatorid) {
		$dto=null;
		$data=$this->getContainer()->get('badiu.util.address.telephoneoperator.data');
		$exist=$data->exist($operatorid);
		if($exist){$dto=$data->findById($operatorid);}
		return $dto;
	}
}
