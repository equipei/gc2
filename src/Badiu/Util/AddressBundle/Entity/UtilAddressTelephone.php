<?php
namespace Badiu\Util\AddressBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  UtilAddressTelephone
 *
 * @ORM\Table(name="util_address_telephone", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="util_address_telephone_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="util_address_telephone_entity_ix", columns={"entity"}), 
  *  	@ORM\Index(name="util_address_telephone_iprefix_ix",columns={"iprefix"}),
 * 	    @ORM\Index(name="util_address_telephone_lprefix_ix",columns={"lprefix"}), 
 *      @ORM\Index(name="util_address_telephone_branch_ix",columns={"branch"}),
 *      @ORM\Index(name="util_address_telephone_number_ix",columns={"number"}),
 *      @ORM\Index(name="util_address_telephone_dtype_ix", columns={"dtype"}),
 *      @ORM\Index(name="util_address_telephone_operatorid_ix",columns={"operatorid"}), 
 *      @ORM\Index(name="util_address_telephone_categoryid_ix",columns={"categoryid"}),
 *      @ORM\Index(name="util_address_telephone_modulekey_ix",columns={"modulekey"}),
 *      @ORM\Index(name="util_address_telephone_moduleinstance_ix",columns={"moduleinstance"}),                          
 *              @ORM\Index(name="util_address_telephone_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="util_address_telephone_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class  UtilAddressTelephone
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
   private $id;
    
   
   /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
   private $entity;



    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

	
      /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * @var string
     *
     * @ORM\Column(name="iprefix", type="integer", nullable=true)
     */
    private $iprefix;


    /**
     * @var string
     *
     * @ORM\Column(name="lprefix", type="integer", nullable=true)
     */
    private $lprefix;


    /**
     * @var string
     *
     * @ORM\Column(name="branch", type="integer",  nullable=true)
     */
    private $branch;


    /**
     * @var string
     *
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $number;

	   /**
     * @var string
     *
     * @ORM\Column(name="numbertxt", type="string", length=255, nullable=true)
     */
    private $numbertxt;
/**
     * @var string
     *
     * @ORM\Column(name="sortorder", type="integer", nullable=true)
     */
    private $sortorder;
     /**
     * @var UtilAddressCategory
     *
     * @ORM\ManyToOne(targetEntity="UtilAddressCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;

 /**
     * @var UtilAddressTelephoneOperator
     *
     * @ORM\ManyToOne(targetEntity="UtilAddressTelephoneOperator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="operatorid", referencedColumnName="id")
     * })
     */
    private $operatorid;
	/**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='fixe'; // fixe | mobile
     /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;


    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }


    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

 

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }
   


    public function getCategoryid() {

        return $this->categoryid;
    }

    public function setCategoryid(UtilAddressCategory $categoryid) {

        $this->categoryid = $categoryid;
    }


    public function getIprefix() {

        return $this->iprefix;
    }

    public function setIprefix($iprefix) {

        $this->iprefix = $iprefix;
    }



    public function getLprefix() {

        return $this->lprefix;
    }

    public function setLprefix($lprefix) {

        $this->lprefix = $lprefix;
    }
   

    public function getBranch() {

        return $this->branch;
    }

    public function setBranch($branch) {

        $this->branch = $branch;
    }


    public function getNumber() {

        return $this->number;
    }
    public function getNumbertxt() {

        return $this->numbertxt;
    }
    public function setNumber($number) {

        $this->number = $number;
    }
    public function setNumbertxt($numbertxt) {

        $this->numbertxt = $numbertxt;
    }


    /**
     * @return string
     */
    public function getModulekey()
    {
        return $this->modulekey;
    }

    /**
     * @param string 
     */
    public function setModulekey($modulekey)
    {
        $this->modulekey = $modulekey;
    }


     /**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string 
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }

	 /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }
    
	public function getOperatorid() {

        return $this->operatorid;
    }

    public function setOperatorid(UtilAddressTelephoneOperator $operatorid) {

        $this->operatorid = $operatorid;
    } 

	
    public function getSortorder()
    {
        return $this->sortorder;
    }

  
    public function setSortorder($sortorder)
    {
        $this->sortorder = $sortorder;
    }
}
