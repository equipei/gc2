<?php

namespace Badiu\Util\AddressBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * UtilAddressData
 *
 * @ORM\Table(name="util_address_data", uniqueConstraints={         
 *      @ORM\UniqueConstraint(name="util_address_data_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="util_address_data_entity_ix", columns={"entity"}),             
 *              @ORM\Index(name="util_address_data_categoryid_ix", columns={"categoryid"}),             
 *              @ORM\Index(name="util_address_data_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="util_address_dat_idnumber_ix", columns={"idnumber"}),
 *              @ORM\Index(name="util_address_data_cityid_ix", columns={"cityid"}),
 *              @ORM\Index(name="util_address_data_neighborhoodid_ix", columns={"neighborhoodid"}),
 *              @ORM\Index(name="util_address_data_cep_ix", columns={"cep"}),
 *              @ORM\Index(name="util_address_data_state_ix", columns={"state"}),
 *              @ORM\Index(name="util_address_data_city_ix", columns={"city"}),
 *              @ORM\Index(name="util_address_data_neighborhood_ix", columns={"neighborhood"}),
 *              @ORM\Index(name="util_address_data_address_ix", columns={"address"}),
 *              @ORM\Index(name="util_address_data_addressinfo_ix", columns={"addressinfo"}),
 *              @ORM\Index(name="util_address_data_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="util_address_data_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="util_address_data_useridadd_ix", columns={"useridadd"}),
 *              @ORM\Index(name="util_address_data_useridadd_ix", columns={"useridadd"}),
 *              @ORM\Index(name="util_address_data_stateid_ix", columns={"stateid"}),           
 *              @ORM\Index(name="util_address_data_addressnumber_ix", columns={"addressnumber"})})
 *          @ORM\Entity 
 */


class UtilAddressData
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var UtilAddressCategory
     *
     * @ORM\ManyToOne(targetEntity="UtilAddressCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;
    
     /**
     * @var UtilAddressCountry
     *
     * @ORM\ManyToOne(targetEntity="UtilAddressCountry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="countryid", referencedColumnName="id")
     * })
     */
    private $countryid;
  
    /**
     * @var UtilAddressCity
     *
     * @ORM\ManyToOne(targetEntity="UtilAddressCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cityid", referencedColumnName="id")
     * })
     */
    private $cityid;
    
     /**
     * @var UtilAddressNeighborhood
     *
     * @ORM\ManyToOne(targetEntity="UtilAddressNeighborhood")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="neighborhoodid", referencedColumnName="id")
     * })
     */
    private $neighborhoodid;
    

     /**
     * @var UtilAddressState
     *
     * @ORM\ManyToOne(targetEntity="UtilAddressState")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stateid", referencedColumnName="id")
     * })
     */
    private $stateid;


    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

 
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

     /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;


    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;


    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;


 /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=255, nullable=true)
     */
    private $cep;


 /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;


 /**
     * @var string
     *
     * @ORM\Column(name="neighborhood", type="string", length=255, nullable=true)
     */
    private $neighborhood;


/**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;


/**
     * @var string
     *
     * @ORM\Column(name="addressinfo", type="string", length=255, nullable=true)
     */
    private $addressinfo;


/**
     * @var string
     *
     * @ORM\Column(name="addressnumber", type="string", length=255, nullable=true)
     */
    private $addressnumber;


    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

   
    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

 
    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

   
   
  /**
     * @return string
     */
    public function getModulekey()
    {
        return $this->modulekey;
    }

    /**
     * @param string $dtype
     */
    public function setModulekey($modulekey)
    {
        $this->modulekey = $modulekey;
    }


     /**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string $dtype
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }


  /**
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param string $dtype
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

  
    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $dtype
     */
    public function setState($state)
    {
        $this->state = $state;
    }
    


   /**
     * @return string
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param string $neighborhood
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;
    }


   /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $dtype
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
 

    /**
     * @return string
     */
    public function getAddressinfo()
    {
        return $this->addressinfo;
    }

    /**
     * @param string $dtype
     */
    public function setAddressinfo($addressinfo)
    {
        $this->addressinfo = $addressinfo;
    }

   /**
     * @return string
     */
    public function getAddressnumber()
    {
        return $this->addressnumber;
    }

    /**
     * @param string $dtype
     */
    public function setAddressnumber($addressnumber)
    {
        $this->addressnumber = $addressnumber;
    }


    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }


   
    /**
     * @return string
     */
    public function getCityid()
    {
        return $this->cityid;
    }

    /**
     * @param string $cityid
     */
    public function setCityid(UtilAddressCity $cityid)
    {
        $this->cityid = $cityid;
    }

     public function getCategoryid() {
        return $this->categoryid;
    }

    public function setCategoryid(UtilAddressCategory $categoryid) {
        $this->categoryid = $categoryid;
    }


    /**
     * @return string
     */
    public function getNeighborhoodid()
    {
        return $this->neighborhoodid;
    }

    public function setNeighborhoodid(UtilAddressNeighborhood $neighborhoodid)
    {
        $this->neighborhoodid = $neighborhoodid;
    }


    /**
     * @return string
     */
    public function getStateid()
    {
        return $this->stateid;
    }

    /**
     * @param string $stateid
     */
    public function setStateid(UtilAddressState $stateid)
    {
        $this->stateid = $stateid;
    }


    function getCountryid() {
        return $this->countryid;
    }

    function setCountryid(UtilAddressCountry $countryid) {
        $this->countryid = $countryid;
    }



}
