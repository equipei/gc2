<?php


namespace Badiu\Util\AddressBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UtilAddressCity
 *
 * @ORM\Table(name="util_address_city", uniqueConstraints={

 *      @ORM\UniqueConstraint(name="util_address_city_shortname_uix", columns={"entity","stateid", "shortname"}), 
 *      @ORM\UniqueConstraint(name="util_address_city_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="util_address_city_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="util_address_city_name_ix", columns={"name"}), 
 *              @ORM\Index(name="util_address_city_shortname_ix", columns={"shortname"}), 
 *              @ORM\Index(name="util_address_city_stateid_ix", columns={"stateid"}),
 *              @ORM\Index(name="util_address_city_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="util_address_city_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class UtilAddressCity
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var UtilAddressEstate
     *
     * @ORM\ManyToOne(targetEntity="UtilAddressState")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stateid", referencedColumnName="id")
     * })
     */
    private $stateid;

       
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

     /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=true)
     */
    private $latitude;
	
	 /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=0, nullable=true)
     */
    private $longitude;
     
	/**
     * @var integer
     *
     * @ORM\Column(name="telcode", type="integer", nullable=true)
     */
    private $telcode;
	
	/**
     * @var string
     *
     * @ORM\Column(name="dtimezone", type="string", length=255, nullable=true)
     */
    private $dtimezone;
	
	
	/**
     * @var integer
     *
     * @ORM\Column(name="capital", type="integer", nullable=true)
     */
    private $capital;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getStateid() {
        return $this->stateid;
    }

    public function setStateid(UtilAddressState $stateid) {
        $this->stateid = $stateid;
    }

    public function getShortname() {
        return $this->shortname;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

   

	public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }
	
	public function getLatitude() {
        return $this->latitude;
    }	
	
	public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }
	
	public function getLongitude() {
        return $this->longitude;
    }

	public function setTelcode($telcode) {
        $this->telcode = $telcode;
    }
	
	public function getTelcode() {
        return $this->telcode;
    }
	
	public function setDtimezone($dtimezone) {
        $this->dtimezone = $dtimezone;
    }
	
	public function getDtimezone() {
        return $this->dtimezone;
    }
	
	public function setCapital($capital) {
        $this->capital = $capital;
    }
	
	public function getCapital() {
        return $this->capital;
    }
}
