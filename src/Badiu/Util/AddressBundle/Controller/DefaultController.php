<?php

namespace Badiu\Util\AddressBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuUtilAddressBundle:Default:index.html.twig', array('name' => $name));
    }
}
