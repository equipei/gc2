<?php

namespace Badiu\Util\DocumentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UtilDocumentData
 *
 * @ORM\Table(name="util_document_data", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="util_document_data_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="util_document_data_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="util_document_data_categoryid_ix", columns={"categoryid"}),
 *              @ORM\Index(name="util_document_data_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="util_document_data_idnumber_ix", columns={"idnumber"}),
 *              @ORM\Index(name="util_document_data_typeid_ix", columns={"typeid"}),
 *              @ORM\Index(name="util_document_data_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="util_document_data_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="util_document_data_numbertext_ix", columns={"numbertext"}),
 *              @ORM\Index(name="util_document_data_useridadd_ix", columns={"useridadd"})})
 *          @ORM\Entity
 */

class UtilDocumentData
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var UtilDocumentCategory
     *
     * @ORM\ManyToOne(targetEntity="UtilDocumentCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;

    /**
     * @var 
     *
     * @ORM\ManyToOne(targetEntity="UtilDocumentType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typeid", referencedColumnName="id")
     * })
     */
    private $typeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;
 
	    /**
     * @var string
     *
     * @ORM\Column(name="numbertext",type="string", length=255, nullable=true)
     */
    private $numbertext;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="organissuer", type="string", length=255, nullable=true)
     */
    private $organissuer;
	
	 /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateofissue", type="datetime", nullable=true)
     */
    private $dateofissue;
	
		 /**
     * @var \DateTime
     *
     * @ORM\Column(name="expirationdate", type="datetime", nullable=true)
     */
    private $expirationdate;
	
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description",type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;

    /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;



    public function getId()
    {
        return $this->id;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function getIdnumber()
    {
        return $this->idnumber;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getParam()
    {
        return $this->param;
    }

    public function getTimecreated()
    {
        return $this->timecreated;
    }

    public function getTimemodified()
    {
        return $this->timemodified;
    }

    public function getUseridadd()
    {
        return $this->useridadd;
    }

    public function getUseridedit()
    {
        return $this->useridedit;
    }

    public function getDeleted()
    {
        return $this->deleted;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    public function setIdnumber($idnumber)
    {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setParam($param)
    {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated)
    {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified)
    {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd)
    {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit)
    {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return string
     */
    public function getModulekey()
    {
        return $this->modulekey;
    }

    /**
     * @param string $dtype
     */
    public function setModulekey($modulekey)
    {
        $this->modulekey = $modulekey;
    }

    /**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string $dtype
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }

    /**
     * @return string
     */
    public function getTypeid()
    {
        return $this->typeid;
    }

    /**
     * @param string $typeid
     */
    public function setTypeid(UtilDocumentType $typeid)
    {
        $this->typeid = $typeid;
    }

    public function getCategoryid()
    {
        return $this->categoryid;
    }

    public function setCategoryid(UtilDocumentCategory $categoryid)
    {
        $this->categoryid = $categoryid;
    }
 /**
     * @return string
     */
    public function getNumbertext()
    {
        return $this->numbertext;
    }

    /**
     * @param string $dtype
     */
    public function setNumbertext($numbertext)
    {
        $this->numbertext = $numbertext;
    }
	
		 public function getDateofissue()
    {
        return $this->dateofissue;
    }

	   public function setDateofissue($dateofissue)
    {
        $this->dateofissue = $dateofissue;
    }
	
		 public function getExpirationdate()
    {
        return $this->expirationdate;
    }

	   public function setExpirationdate(\DateTime $expirationdate)
    {
        $this->expirationdate = $expirationdate;
    }
	
		
	 public function getOrganissuer()
    {
        return $this->organissuer;
    }

	   public function setOrganissuer($organissuer)
    {
        $this->organissuer = $organissuer;
    }
}
