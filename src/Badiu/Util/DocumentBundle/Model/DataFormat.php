<?php

namespace Badiu\Util\DocumentBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class DataFormat extends BadiuFormat {

    public function __construct(Container $container) {

        parent::__construct($container);
    }

    public function typenumber($data) {
        $result=null;
		$value="";
        $docnumber = null;
        $doctype = null;

        if (isset($data["docnumber"])) {$docnumber = $data["docnumber"];}
        if (isset($data["doctype"])) {$doctype = $data["doctype"];}
        $value = $docnumber;
        if($doctype=='CNPJ'){
            $docservice = $this->getContainer()->get('badiu.util.document.role.cnpj');
            $value = $docservice->format($docnumber);
        }
         else if($doctype=='CPF'){
              $docservice = $this->getContainer()->get('badiu.util.document.role.cpf');
            $value = $docservice->format($docnumber);
         }
		 $result="$doctype: $value";
		 $badiuSession = $this->getContainer()->get('badiu.system.access.session');
		 $formathiddetype=$badiuSession->getValue('badiu.util.document.type.param.config.formathiddetype');
		 if($formathiddetype){$result=$value;}
        
		
		
		 
        return $value;
    }

}
