<?php

namespace Badiu\Util\DocumentBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class DocumentData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }



	public function findLastByShortnameCategory($modulekey,$moduleinstance,$shortnameCategory) {
		
		//get categoryid
        $catdata=$this->getContainer()->get('badiu.util.document.category.data');
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
         $entity=$badiuSession->get()->getEntity();
				 
		$categoryid=$catdata->getIdByShortname($entity,$shortnameCategory);
        if(!empty($categoryid)){
			$sql="SELECT MAX(o.id) AS id FROM ".$this->getBundleEntity()." o  WHERE o.modulekey=:modulekey AND o.moduleinstance=:moduleinstance AND o.categoryid = :categoryid ";
			$query = $this->getEm()->createQuery($sql);
            $query->setParameter('modulekey',$modulekey);
			$query->setParameter('moduleinstance',$moduleinstance);
			$query->setParameter('categoryid',$categoryid);
			
			$id= $query->getOneOrNullResult();
			if(!empty($id)){$id=$id['id'];}
           
			if(!empty($id)){
				$sql="SELECT o FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
				$query = $this->getEm()->createQuery($sql);
				$query->setParameter('id',$id);
				$result= $query->getSingleResult();
				return  $result;
			}
		   
			
		}
       
       return null;
    }
	
	public function findLastByShortnameType($modulekey,$moduleinstance,$shortnameType) {
		
		//get typeid
        $typedata=$this->getContainer()->get('badiu.util.document.type.data');
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
         $entity=$badiuSession->get()->getEntity();
				 
		$typeid=$typedata->getIdByShortname($entity,$shortnameType);
        if(!empty($typeid)){
			$sql="SELECT MAX(o.id) AS id FROM ".$this->getBundleEntity()." o  WHERE o.modulekey=:modulekey AND o.moduleinstance=:moduleinstance AND o.typeid = :typeid ";
			$query = $this->getEm()->createQuery($sql);
            $query->setParameter('modulekey',$modulekey);
			$query->setParameter('moduleinstance',$moduleinstance);
			$query->setParameter('typeid',$typeid);
			
			$id= $query->getOneOrNullResult();
			if(!empty($id)){$id=$id['id'];}
           
			if(!empty($id)){
				$sql="SELECT o FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
				$query = $this->getEm()->createQuery($sql);
				$query->setParameter('id',$id);
				$result= $query->getSingleResult();
				return  $result;
			}
		   
			
		}
       
       return null;
    }	
	
	
		public function getModuleinstance($id) {
		$sql="SELECT o.moduleinstance FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
		$query = $this->getEm()->createQuery($sql);
        $query->setParameter('id',$id);
		$result= $query->getOneOrNullResult();
		if(!empty($result)){$result=$result['moduleinstance'];}
		return $result;
	}

}
