<?php

namespace Badiu\Util\DocumentBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Badiu\Finac\SellBundle\Model\DOMAINTABLE;
class TypeFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
   
    public  function physicalPerson(){
        $list=$this->getContainer()->get('badiu.util.document.type.data')->getFormChoiceShortnamePhysicalPerson();
          return $list;
    }
    public  function legalPerson(){
            $list=$this->getContainer()->get('badiu.util.document.type.data')->getFormChoiceShortnameLegalPerson();
          return $list;
    }
}
