<?php
namespace Badiu\Util\DocumentBundle\Model\Role;

use Badiu\Util\DocumentBundle\Model\Role\CPF;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 *
 */

class DocumentGeneralRole
{

    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {

        $this->container = $container;

    }

     public function isValid($value)
     {

     	return true;

     }
     public function format($value)
     {

     	return $value;

     }

    public function getContainer()
    {
        return $this->container;

    }

    public function setContainer(Container $container)
    {
        $this->container = $container;

    }

}
