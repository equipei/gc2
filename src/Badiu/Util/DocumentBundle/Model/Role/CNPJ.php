<?php
namespace Badiu\Util\DocumentBundle\Model\Role;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CNPJ extends DocumentGeneralRole
{
    
    public $errorCNPJ = '';
    
    
    public $mask = '##.###.###/####-##';
    
    public function __construct(Container $container)
    {
        
        parent::__construct($container);
        
    }
    
    private $pArray_cnpj = array(5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
    private $sArray_cpj = array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
    
    public function clean($cpf)
    {
        
        return preg_replace('/[^0-9]/', '', $cpf);
        
    }
    //review
    public function isValid($cnpj)
    {
    
		$cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
	
	
		if (strlen($cnpj) != 14){return false;}

	
		if (preg_match('/(\d)\1{13}/', $cnpj)){return false;	}

		 $lcnpj = str_split($cnpj);
		if(!is_array($lcnpj)){return null;}
		if(sizeof($lcnpj)!=14){return null;}
		// Valida primeiro dígito verificador
		for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
		{
			$soma += $lcnpj[$i] * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}

		$resto = $soma % 11;

	 if ($lcnpj[12] != ($resto < 2 ? 0 : 11 - $resto)){return false;}

		// Valida segundo dígito verificador
		for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
		{
			$soma += $lcnpj[$i] * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}

		$resto = $soma % 11;

		return $lcnpj[13] == ($resto < 2 ? 0 : 11 - $resto);       
        
        
    }
    // Aply mask in filds withe cnpj
    public function format($value)
    {
        if (empty($value))
            return "";
        $k        = 0;
        $maskared = '';
        for ($i = 0; $i <= strlen($this->mask) - 1; $i++) {
            
            if ($this->mask[$i] == '#') {
                
                if (isset($value[$k])) {
                    $maskared .= $value[$k++];
                }
                
            } else {
                
                if (isset($this->mask[$i])) {
                    $maskared .= $this->mask[$i];
                }
                
            }
            
        }
        
        return $maskared;
        
    }
    
}