<?php

namespace Badiu\Util\DocumentBundle\Model\Role;

use Badiu\Util\DocumentBundle\Model\Role\DocumentGeneralRole;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CPF extends DocumentGeneralRole
{

    public function __construct(Container $container)
    {

        parent::__construct($container);

    }

   

    public function isValid($v)
    {
        $len = strlen($v);
        if ($len == 0) {return false;}
        $cpf_valid = $this->validate($v);
       
        return $cpf_valid;
    }

    public function clean($cpf)
    {

        return preg_replace('/[^0-9]/', '', $cpf);

    }

/**
 * Check if the provided CPF is valid
 *
 * @param &$cpf
 *   Number, the CPF number
 * @param $clean
 *   Boolean, FALSE to not clear the number
 * @return
 *   Boolean, true if its a valid CPF
 */

    public function validate($cpf, $clean = true)
    {
        if(empty($cpf)){return null;}
		// Clear the CPF
        if ($clean) {

            $cpf = $this->clean($cpf);
        }
        // Check if its not the forbidden combinations
        if (preg_match('/(\d)\1{10}/', $cpf)) {
			return false;
		}
        // Check the 11st and 12nd numbers
		$lcpf = str_split($cpf);
		if(!is_array($lcpf)){return null;}
		if(sizeof($lcpf)!=11){return null;}
         for ($t = 9; $t < 11; $t++) {
				for ($d = 0, $c = 0; $c < $t; $c++) {
					$d += $lcpf[$c] * (($t + 1) - $c);
				}
				$d = ((10 * $d) % 11) % 10;
				if ($lcpf[$c] != $d) {
				return false;
			}
		}

        return $cpf;
    }

    public function format($string)
    {
        if(!$this->validate($string)){return $string;}
        if(empty($string))return "";
		$output = preg_replace('/[^0-9]/', '', $string);
        $mask   = '###.###.###-##';
        $index  = -1;
        for ($i = 0; $i < strlen($mask); $i++):
            if ($mask[$i] == '#') {
                $mask[$i] = $output[++$index];
            }

        endfor;
        return $mask;
    }

    public function formatclean($string)
    {
        return preg_replace('/[^0-9]/', '', $string);

    }

}
