<?php

namespace Badiu\Util\DocumentBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Install extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
       
           $result= $this->initDbDocument();
    } 

     public function initDbDocument() {
         $cont=0;
          $datacategory = $this->getContainer()->get('badiu.util.document.category.data');
         $entity=$this->getEntity();
        
         $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
         $factorykeytree->init('badiu.util.document.category.data');
         
         $dtype=null;
         
         //category dd  legalperson
         $param=array();
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.util.document.category.legalperson');
         $param['shortname']='legalperson';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         $idpath=null;
          $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
          $level = $factorykeytree->getLevel($newidtree);
          $order = $factorykeytree->getOrder($newidtree);
          
          $parentinfo = $factorykeytree->getParentInfo($entity, $idpath, $dtype);
          $dtotree=array();
          $dtotree = $factorykeytree->makePath($parentinfo, $dtotree);
          
           $param['idpath']=$newidtree;
           $param['level']=$level;
           $param['orderidpath']=$order;
           $param['path']=$this->getUtildata()->getVaueOfArray($dtotree, 'path');
           $param['parent']=$this->getUtildata()->getVaueOfArray($dtotree, 'parent');
         if(! $datacategory->existByShortname($entity,'legalperson')){
              $result =  $datacategory->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         //category add physicalperson
         $param=array();
         $idpath=null;
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.util.document.category.physicalperson');
         $param['shortname']='physicalperson';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         
          $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
          $level = $factorykeytree->getLevel($newidtree);
          $order = $factorykeytree->getOrder($newidtree);
          
          $parentinfo = $factorykeytree->getParentInfo($entity, $idpath, $dtype);
          $dtotree=array();
          $dtotree = $factorykeytree->makePath($parentinfo, $dtotree);
       
          
           $param['idpath']=$newidtree;
           $param['level']=$level;
           $param['orderidpath']=$order;
           $param['path']=$this->getUtildata()->getVaueOfArray($dtotree, 'path');
           $param['parent']=$this->getUtildata()->getVaueOfArray($dtotree, 'parent');
         if(! $datacategory->existByShortname($entity,'physicalperson')){
              $result =  $datacategory->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         
         
          $datatype = $this->getContainer()->get('badiu.util.document.type.data');
         $legalpersonid=  $datacategory->getIdByShortname($entity,'legalperson');
         $physicalpersonid=  $datacategory->getIdByShortname($entity,'physicalperson');
         
         //type add CNPJ
         $param=array();
         $param['entity']=$entity;
         $param['categoryid']=$legalpersonid;
         $param['name']=$this->getTranslator()->trans('badiu.util.document.type.cnpj');
         $param['shortname']='CNPJ';
         $param['service']='badiu.util.document.role.cpnj';
		  $param['dtype']='indentify';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         if(!$datatype->existByShortname($entity,'CNPJ')){
              $result = $datatype->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         
         //type add BRPJIE
         $param=array();
         $param['entity']=$entity;
         $param['categoryid']=$legalpersonid;
         $param['name']=$this->getTranslator()->trans('badiu.util.document.type.brpjie');
         $param['shortname']='BRPJIE';
		 $param['dtype']='indentify';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         if(!$datatype->existByShortname($entity,'BRPJIE')){
              $result = $datatype->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         //type add BRPJIM
         $param=array();
         $param['entity']=$entity;
         $param['categoryid']=$legalpersonid;
         $param['name']=$this->getTranslator()->trans('badiu.util.document.type.brpjim');
         $param['shortname']='BRPJIM';
		 $param['dtype']='indentify';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         if(!$datatype->existByShortname($entity,'BRPJIM')){
              $result = $datatype->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         
         //type add CPF
         $param=array();
         $param['entity']=$entity;
         $param['categoryid']=$physicalpersonid;
         $param['name']=$this->getTranslator()->trans('badiu.util.document.type.cpf');
         $param['shortname']='CPF';
         $param['service']='badiu.util.document.role.cpf';
		 $param['dtype']='indentify';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         if(!$datatype->existByShortname($entity,'CPF')){
              $result = $datatype->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
          //type add EMAIL
         $param=array();
         $param['entity']=$entity;
         $param['categoryid']=$physicalpersonid;
         $param['name']=$this->getTranslator()->trans('badiu.util.document.type.email');
         $param['shortname']='EMAIL';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         if(!$datatype->existByShortname($entity,'EMAIL')){
              $result = $datatype->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         
          //type add EMAIL
         $param=array();
         $param['entity']=$entity;
         $param['categoryid']=$physicalpersonid;
         $param['name']=$this->getTranslator()->trans('badiu.util.document.type.rg');
         $param['shortname']='RG';
		 $param['dtype']='indentify';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         if(!$datatype->existByShortname($entity,'RG')){
              $result = $datatype->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         
           //type add EMAIL
         $param=array();
         $param['entity']=$entity;
         $param['categoryid']=$physicalpersonid;
         $param['name']=$this->getTranslator()->trans('badiu.util.document.type.rne');
         $param['shortname']='RNE';
		 $param['dtype']='indentify';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         if(!$datatype->existByShortname($entity,'RNE')){
              $result = $datatype->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
        return $cont; 
     }
}
