<?php

namespace Badiu\Util\DocumentBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class TypeData extends BadiuDataBase {

    function __construct(Container $container, $bundleEntity) {
        parent::__construct($container, $bundleEntity);
    }

    public function getFormChoiceShortnamePhysicalPerson() {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
        $sql = "SELECT o.shortname  AS id ,o.shortname AS name FROM " . $this->getBundleEntity() . " o JOIN o.categoryid c WHERE o.entity=:entity AND c.shortname=:categorysohortname";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
        $query->setParameter('categorysohortname','physicalperson');
        $result = $query->getResult();
        return $result;
    }
 public function getFormChoiceShortnameLegalPerson() {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
        $sql = "SELECT o.shortname  AS id,o.shortname AS name FROM " . $this->getBundleEntity() . " o JOIN o.categoryid c WHERE o.entity=:entity AND c.shortname=:categorysohortname";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
        $query->setParameter('categorysohortname','legalperson');
        $result = $query->getResult();
        return $result;
    }
}
