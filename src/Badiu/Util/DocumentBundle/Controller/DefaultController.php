<?php

namespace Badiu\Util\DocumentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuUtilDocumentBundle:Default:index.html.twig', array('name' => $name));
    }
}
