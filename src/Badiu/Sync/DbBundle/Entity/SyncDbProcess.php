<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\Sync\DbBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * SyncTable
 *
 * @ORM\Table(name="sync_db_process", 
 *       indexes={
 *                  @ORM\Index(name="sync_db_process_entity_ix", columns={"entity"}), 
 *                  @ORM\Index(name="sync_db_process_dataid_ix", columns={"dataid"}),
 *                  @ORM\Index(name="sync_db_process_timestart_ix", columns={"timestart"}),
 *                  @ORM\Index(name="sync_db_process_timeend_ix", columns={"timeend"})})
 * @ORM\Entity
 */
class SyncDbProcess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var SyncDbData
     *
     * @ORM\ManyToOne(targetEntity="SyncDbData")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dataid", referencedColumnName="id")
     * })
     */
    private $dataid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="newdatashouldexec", type="bigint", nullable=true)
     */
    private $newdatashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="newdataexec", type="bigint", nullable=true)
     */
    private $newdataexec;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="updatedatashouldexe", type="bigint", nullable=true)
     */
    private $updatedatashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="updatedataexec", type="bigint", nullable=true)
     */
    private $updatedataexec;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="deletedatashouldexec", type="bigint", nullable=true)
     */
    private $deletedatashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="deletedataexec", type="bigint", nullable=true)
     */
    private $deletedataexec;
       
     /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=true)
     */
    private $timecreated;
    
    /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    
    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getDataid() {
        return $this->dataid;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getTimestart() {
        return $this->timestart;
    }

    public function getTimeend() {
        return $this->timeend;
    }

    public function getTimeexec() {
        return $this->timeexec;
    }

    public function getDatashouldexec() {
        return $this->datashouldexec;
    }

    public function getDataexec() {
        return $this->dataexec;
    }

    public function getNewdatashouldexec() {
        return $this->newdatashouldexec;
    }

    public function getNewdataexec() {
        return $this->newdataexec;
    }

    public function getUpdatedatashouldexec() {
        return $this->updatedatashouldexec;
    }

    public function getUpdatedataexec() {
        return $this->updatedataexec;
    }

    public function getDeletedatashouldexec() {
        return $this->deletedatashouldexec;
    }

    public function getDeletedataexec() {
        return $this->deletedataexec;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getParam() {
        return $this->param;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setDataid(SyncDbData $dataid) {
        $this->dataid = $dataid;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    public function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    public function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }

    public function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    public function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }

    public function setNewdatashouldexec($newdatashouldexec) {
        $this->newdatashouldexec = $newdatashouldexec;
    }

    public function setNewdataexec($newdataexec) {
        $this->newdataexec = $newdataexec;
    }

    public function setUpdatedatashouldexec($updatedatashouldexec) {
        $this->updatedatashouldexec = $updatedatashouldexec;
    }

    public function setUpdatedataexec($updatedataexec) {
        $this->updatedataexec = $updatedataexec;
    }

    public function setDeletedatashouldexec($deletedatashouldexec) {
        $this->deletedatashouldexec = $deletedatashouldexec;
    }

    public function setDeletedataexec($deletedataexec) {
        $this->deletedataexec = $deletedataexec;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }



}
