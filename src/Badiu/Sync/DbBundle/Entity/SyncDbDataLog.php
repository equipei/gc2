<?php

namespace Badiu\Sync\DbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *SyncDbData
 *
 * @ORM\Table(name="sync_db_data_log", 
 *                                       indexes={@ORM\Index(name="sync_db_data_log_entityd_ix", columns={"entity"}), 
 *                                                @ORM\Index(name="sync_db_data_log_idnumber_ix", columns={"idnumber"})})
 * * @ORM\Entity
 */
class SyncDbDataLog
{
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
 
   /**
     * @var SyncDbData
     *
     * @ORM\ManyToOne(targetEntity="SyncDbData")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dataid", referencedColumnName="id")
     * })
     */
    private $dataid;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

  /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status; //success | failed | processing
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;
    
	/**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;

    
     /**
     * @var string
     *
     * @ORM\Column(name="resultinfo", type="text", nullable=true)
     */
    private $resultinfo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='automatic'; //automatic | manual
	
	    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;

    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
   
    function getId() {
        return $this->id;
    }

    function getDataid() {
        return $this->dataid;
    }

    function getEntity() {
        return $this->entity;
    }

    function getStatus() {
        return $this->status;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getTimeexec() {
        return $this->timeexec;
    }

    function getDatashouldexec() {
        return $this->datashouldexec;
    }

    function getDataexec() {
        return $this->dataexec;
    }

    function getDtype() {
        return $this->dtype;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDataid(SyncDbData $dataid) {
        $this->dataid = $dataid;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }

    function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function getResultinfo() {
        return $this->resultinfo;
    }

    function setResultinfo($resultinfo) {
        $this->resultinfo = $resultinfo;
    }



}
