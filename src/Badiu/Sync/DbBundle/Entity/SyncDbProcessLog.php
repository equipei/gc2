<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\Sync\DbBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * SyncTable
 *
 * @ORM\Table(name="sync_db_process_log",
 *       indexes={
 *                  @ORM\Index(name="sync_db_process_log_entity_ix", columns={"entity"}), 
 *                  @ORM\Index(name="sync_db_process_log_processid_ix", columns={"processid"}),
 *                  @ORM\Index(name="sync_db_process_log_operation_ix", columns={"operation"})})
 *                  
 * @ORM\Entity
 */
class SyncDbProcessLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var SyncDbProcess
     *
     * @ORM\ManyToOne(targetEntity="SyncDbProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processid", referencedColumnName="id")
     * })
     */
    private $processid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="operation", type="string", length=50, nullable=false)
     */
    private $operation;
     
    /**
     * @var string
     *
     * @ORM\Column(name="datakey", type="string", length=255, nullable=true)
     */
    private $datakey;
    
    /**
     * @var string
     *
     * @ORM\Column(name="datainfo", type="string", length=255, nullable=true)
     */
    private $datainfo;
    
   
    
     /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
   
    /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=true)
     */
    private $timecreated;
    
    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getProcessid() {
        return $this->processid;
    }

    public function getOperation() {
        return $this->operation;
    }

    public function getDatakey() {
        return $this->datakey;
    }

    public function getDatainfo() {
        return $this->datainfo;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setProcessid(SyncDbProcess $processid) {
        $this->processid = $processid;
    }

    public function setOperation($operation) {
        $this->operation = $operation;
    }

    public function setDatakey($datakey) {
        $this->datakey = $datakey;
    }

    public function setDatainfo($datainfo) {
        $this->datainfo = $datainfo;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }


}
