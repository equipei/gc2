<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\Sync\DbBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * SyncDbData
 *
 * @ORM\Table(name="sync_db_data", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="sync_db_data_name_uix", columns={"entity", "name"}),
 *      @ORM\UniqueConstraint(name="sync_db_data_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="sync_db_data_idnumber_uix", columns={"entity", "idnumber"})},
 *      indexes={
 *                  @ORM\Index(name="sync_db_data_entity_ix", columns={"entity"}),
 *                  @ORM\Index(name="sync_db_data_shortname_ix", columns={"shortname"}),
 *                  @ORM\Index(name="sync_db_data_dbsource_ix", columns={"dbsource"}),
 *                  @ORM\Index(name="sync_db_data_dbtarget_ix", columns={"dbtarget"}),
 *                  @ORM\Index(name="sync_db_data_name_ix", columns={"name"})})
 * @ORM\Entity
 */
class SyncDbData
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var SyncDbCategory
     *
     * @ORM\ManyToOne(targetEntity="SyncDbCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    
      /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
    /**
     * @var string
     *
     * @ORM\Column(name="typesourceconn", type="string", length=50, nullable=false)
     */
    private $typesourceconn;
    
    /**
     * @var \Badiu\Admin\ServerBundle\Entity\AdminServerService
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ServerBundle\Entity\AdminServerService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dbsource", referencedColumnName="id")
     * })
     */
    private $dbsource;
    
     /**
     * @var string
     *
     * @ORM\Column(name="dbsourceconfig", type="text", nullable=true)
     */
    private $dbsourceconfig; //{"template": {"dtype": "site_moodle"}}
    
    /**
     * @var string
     *
     * @ORM\Column(name="typetargetconn", type="string", length=50, nullable=false)
     */
    private $typetargetconn;
   
    
    
    /**
     * @var \Badiu\Admin\ServerBundle\Entity\AdminServerService
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ServerBundle\Entity\AdminServerService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dbtarget", referencedColumnName="id")
     * })
     */
    private $dbtarget;
   
    /**
     * @var string
     *
     * @ORM\Column(name="dbtargetconfig", type="text", nullable=true)
     */
    private $dbtargetconfig;
    
     /**
     * @var string
     * @ORM\Column(name="syncoperation", type="string", length=255, nullable=false)
      *  default values: _import| _update |_delete 
     */
    private $syncoperation;
     
    /**
     * @var string
     *
     * @ORM\Column(name="dbsourcesqlcount", type="text", nullable=true)
     */
    private $dbsourcesqlcount;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dbsourcesqlselecttoinsert", type="text", nullable=true)
     */
    private $dbsourcesqlselecttoinsert;
    
     /**
     * @var string
     *
     * @ORM\Column(name="dbsourcesqlselecttoupdate", type="text", nullable=true)
     */
    private $dbsourcesqlselecttoupdate;
    
     /**
     * @var string
     *
     * @ORM\Column(name="dbsourcesqlselecttodelete", type="text", nullable=true)
     */
    private $dbsourcesqlselecttodelete;
    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="dbtargetsqlcount", type="text", nullable=true)
     */
        private $dbtargetsqlcount;
    
     /**
     * @var string
     *
     * @ORM\Column(name="dbtargetsqllastid", type="text", nullable=true)
     */
    private $dbtargetsqllastid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dbtargetsqlinsert", type="text", nullable=true)
     */
    private $dbtargetsqlinsert;
    
     /**
     * @var string
     *
     * @ORM\Column(name="dbtargetsqlupdate", type="text", nullable=true)
     */
    private $dbtargetsqlupdate;
    
     /**
     * @var string
     *
     * @ORM\Column(name="dbtargetsqldelete", type="text", nullable=true)
     */
    private $dbtargetsqldelete;
    
    /**
     * @var string
          * @ORM\Column(name="lastid", type="text", nullable=true)
     */
    private $lastid;
    
   
     /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
    	/**
     * @var string
     *
     * @ORM\Column(name="simultaneousexec", type="text", nullable=true)
     */
    private $simultaneousexec;
	
	/**
     * @var string
     *
     * @ORM\Column(name="timeroutine", type="text", nullable=true)
     */
    private $timeroutine;  //timeinterval|daily|monthly|weekly|once
	
	 /**
     * @var string
     *
     * @ORM\Column(name="timeroutineparam", type="text", nullable=true)
     */
    private $timeroutineparam; //timeinterval -> timeunit: minute|hour|day|week|month  / value: xxx
                               //daily -> hour: xxx |minute: xxx
                               //monthly -> day: xxx | hour: xxx
                               //weekly -> day: 0-6 | hour: xxx
                               //once  -> datey: xxx

    	/**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status='active'; //active|inactive
	
		/**
     * @var string
     *
     * @ORM\Column(name="laststatus", type="text", nullable=true)
     */
    private $laststatus; //anable|desable|processing
    
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;
    
   	/**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;
    
    /**
     * @var string
     *
     * @ORM\Column(name="resultinfo", type="text", nullable=true)
     */
    private $resultinfo;
   
    /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="bigint", nullable=true)
     */
    private $sortorder;
    
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=true)
     */
    
     private $deleted;
     
    function getId() {
        return $this->id;
    }

    function getCategoryid() {
        return $this->categoryid;
    }

    function getEntity() {
        return $this->entity;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function getTypesourceconn() {
        return $this->typesourceconn;
    }

    function getDbsource() {
        return $this->dbsource;
    }

    function getDbsourceconfig() {
        return $this->dbsourceconfig;
    }

    function getTypetargetconn() {
        return $this->typetargetconn;
    }

    function getDbtarget() {
        return $this->dbtarget;
    }

    function getDbtargetconfig() {
        return $this->dbtargetconfig;
    }

    function getSyncoperation() {
        return $this->syncoperation;
    }

   
    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCategoryid(SyncDbCategory $categoryid) {
        $this->categoryid = $categoryid;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    function setTypesourceconn($typesourceconn) {
        $this->typesourceconn = $typesourceconn;
    }

    function setDbsource(\Badiu\Admin\ServerBundle\Entity\AdminServerService $dbsource) {
        $this->dbsource = $dbsource;
    }

    function setDbsourceconfig($dbsourceconfig) {
        $this->dbsourceconfig = $dbsourceconfig;
    }

    function setTypetargetconn($typetargetconn) {
        $this->typetargetconn = $typetargetconn;
    }

    function setDbtarget(\Badiu\Admin\ServerBundle\Entity\AdminServerService $dbtarget) {
        $this->dbtarget = $dbtarget;
    }

    function setDbtargetconfig($dbtargetconfig) {
        $this->dbtargetconfig = $dbtargetconfig;
    }

    function setSyncoperation($syncoperation) {
        $this->syncoperation = $syncoperation;
    }

   

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }
    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function getDbsourcesqlcount() {
        return $this->dbsourcesqlcount;
    }

    function getDbsourcesqlselecttoinsert() {
        return $this->dbsourcesqlselecttoinsert;
    }

    function getDbsourcesqlselecttoupdate() {
        return $this->dbsourcesqlselecttoupdate;
    }

    function getDbsourcesqlselecttodelete() {
        return $this->dbsourcesqlselecttodelete;
    }

    function getDbtargetsqlcount() {
        return $this->dbtargetsqlcount;
    }

    function getDbtargetsqllastid() {
        return $this->dbtargetsqllastid;
    }

    function getDbtargetsqlinsert() {
        return $this->dbtargetsqlinsert;
    }

    function getDbtargetsqlupdate() {
        return $this->dbtargetsqlupdate;
    }

    function getDbtargetsqldelete() {
        return $this->dbtargetsqldelete;
    }

    function setDbsourcesqlcount($dbsourcesqlcount) {
        $this->dbsourcesqlcount = $dbsourcesqlcount;
    }

    function setDbsourcesqlselecttoinsert($dbsourcesqlselecttoinsert) {
        $this->dbsourcesqlselecttoinsert = $dbsourcesqlselecttoinsert;
    }

    function setDbsourcesqlselecttoupdate($dbsourcesqlselecttoupdate) {
        $this->dbsourcesqlselecttoupdate = $dbsourcesqlselecttoupdate;
    }

    function setDbsourcesqlselecttodelete($dbsourcesqlselecttodelete) {
        $this->dbsourcesqlselecttodelete = $dbsourcesqlselecttodelete;
    }

    function setDbtargetsqlcount($dbtargetsqlcount) {
        $this->dbtargetsqlcount = $dbtargetsqlcount;
    }

    function setDbtargetsqllastid($dbtargetsqllastid) {
        $this->dbtargetsqllastid = $dbtargetsqllastid;
    }

    function setDbtargetsqlinsert($dbtargetsqlinsert) {
        $this->dbtargetsqlinsert = $dbtargetsqlinsert;
    }

    function setDbtargetsqlupdate($dbtargetsqlupdate) {
        $this->dbtargetsqlupdate = $dbtargetsqlupdate;
    }

    function setDbtargetsqldelete($dbtargetsqldelete) {
        $this->dbtargetsqldelete = $dbtargetsqldelete;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getLastid() {
        return $this->lastid;
    }

    function getSimultaneousexec() {
        return $this->simultaneousexec;
    }

    function getTimeroutine() {
        return $this->timeroutine;
    }

    function getTimeroutineparam() {
        return $this->timeroutineparam;
    }

    function getStatus() {
        return $this->status;
    }

    function getLaststatus() {
        return $this->laststatus;
    }

    function getResultinfo() {
        return $this->resultinfo;
    }

    function getSortorder() {
        return $this->sortorder;
    }

    function setLastid($lastid) {
        $this->lastid = $lastid;
    }

    function setSimultaneousexec($simultaneousexec) {
        $this->simultaneousexec = $simultaneousexec;
    }

    function setTimeroutine($timeroutine) {
        $this->timeroutine = $timeroutine;
    }

    function setTimeroutineparam($timeroutineparam) {
        $this->timeroutineparam = $timeroutineparam;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setLaststatus($laststatus) {
        $this->laststatus = $laststatus;
    }

    function setResultinfo($resultinfo) {
        $this->resultinfo = $resultinfo;
    }

    function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }


    function getTimestart(){
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getTimeexec() {
        return $this->timeexec;
    }

    function getDatashouldexec() {
        return $this->datashouldexec;
    }

    function getDataexec() {
        return $this->dataexec;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }

    function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }


}
