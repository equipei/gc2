<?php

namespace Badiu\Sync\DbBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class DataLogData extends BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
        parent::__construct($container,$bundleEntity);
    }

     public function start($param) {
         $dataid=null;
         $entity=null;
         $timestart=null;
         $dtype='automatic';
         
          if(isset($param['dataid'])) {$dataid=$param['dataid'];}
          if(isset($param['entity'])) {$entity=$param['entity'];}
          if(isset($param['timestart'])) {$timestart=$param['timestart'];}
          if(isset($param['dtype'])) {$dtype=$param['dtype'];}
          
          if(empty($entity)){
              $badiuSession=$this->getContainer()->get('badiu.system.access.session');
              $entity=$badiuSession->get()->getEntity();
          }
         
         $param=array('dataid'=>$dataid,'entity'=>$entity,'status'=>'processing','timestart'=>$timestart,'entity'=>$entity,'dtype'=>$dtype);
         $result=$this->insertNativeSql($param,false);
         return $result;
    }
    
     public function end($param) {  
         $id=0;
         $timestart=null;
         $timeend=new \Datetime();
         $datashouldexec=null;
         $dataexec=null;
         $resultinfo=null;
         
          if(isset($param['tasklogid'])) {$id=$param['tasklogid'];}
          if(isset($param['timestart'])) {$timestart=$param['timestart'];}
          
           if(isset($param['resultinfo']['datashouldexec'])) {$datashouldexec=$param['resultinfo']['datashouldexec'];}
           if(isset($param['resultinfo']['dataexec'])) {$dataexec=$param['resultinfo']['dataexec'];}
           if(isset($param['resultinfo'])) {$resultinfo=$param['resultinfo'];}
           
           $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
           $resultinfo= $json->encode($resultinfo);
           
         $start=$timestart->getTimestamp();
         $end=$timeend->getTimestamp();
         $timeexec=$end-$start; 
         $param=array('id'=>$id,'status'=>'success','timeend'=>new \Datetime(),'timeexec'=>$timeexec,'datashouldexec'=>$datashouldexec,'dataexec'=>$dataexec,'resultinfo'=>$resultinfo);
         $result=$this->updateNativeSql($param,false);
         return $result;
    }
}
