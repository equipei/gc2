<?php

namespace Badiu\Sync\DbBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class DataFormat extends BadiuFormat{
     private $configformat;
     private $router;
    
     function __construct(Container $container) {
            parent::__construct($container);
             $this->configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
             $this->router=$this->getContainer()->get("router");
       } 

   public  function task($data){
         $value="";
         $separator="<br />";
         $name=$this->getUtildata()->getVaueOfArray($data,'name');
         $typesourceconn=$this->getUtildata()->getVaueOfArray($data,'typesourceconn');
         $shortname=$this->getUtildata()->getVaueOfArray($data,'shortname');
        
         
       //  $shortnamelabel=$this->getTranslator()->trans('badiu.sync.db.data.shortname');
        // $sortorderlabel=$this->getTranslator()->trans('badiu.sync.db.data.sortorder');
       //  $syncoperationlabel=$this->getTranslator()->trans('badiu.sync.db.data.syncoperation');
   
         
         $value.=$name;
         $value.="<i>";
         if(!empty($shortname)){$value.=$separator.$shortname;}
         $value.=$separator.$typesourceconn;
        $value.="</i>";
        
        return $value; 
     }

        public  function datasource($data){
         $value="";
         $dbsourcename=$this->getUtildata()->getVaueOfArray($data,'dbsourcename');
         $dbtargetname=$this->getUtildata()->getVaueOfArray($data,'dbtargetname');
        
        
         
       //  $shortnamelabel=$this->getTranslator()->trans('badiu.sync.db.data.shortname');
        // $sortorderlabel=$this->getTranslator()->trans('badiu.sync.db.data.sortorder');
       //  $syncoperationlabel=$this->getTranslator()->trans('badiu.sync.db.data.syncoperation');
   
         
         $value.=$dbsourcename;
         $value.=" -> ";
        
         $value.=$dbtargetname;
            
        return $value; 
     }
  public  function lastexec($data){
  
         $value="";
         $id=$this->getUtildata()->getVaueOfArray($data,'id');
        
         $html="";
            if($this->router->getRouteCollection()->get('badiu.system.core.service.process')!==null ){
                 $html.="<a target=\"_blank\" href='".$this->router->generate('badiu.system.core.service.process',array('_service'=>'badiu.sync.db.data.cron','id'=>$id,'operation'=>'import'))."'>importar</a>  ";
            }
            $value.="$html<br />";
        return $value; 
     }
  
}
