<?php

namespace Badiu\Sync\DbBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Sync\DbBundle\Entity\SyncDbData;
use Badiu\Sync\DbBundle\Model\Lib\DOMAINTABLE;
class DataExec  {
    
    /**
     * @var Container
     */
    private $container;
    
    /**
     * @var integer
     */
    private $maxRowImport;
    
     /**
     * @var integer
     */
    private $maxRowImportPerConnection;
    
     /**
     * @var integer
     */
    private $maxIdImport;
    
   /**
     * @var integer
     */
    private $maxIdImportPerConnection;
    
    /**
     * @var SyncDbData
     */
     private  $data;
     
     /**
     * @var array
     */
     private  $columnssource;
     
     /**
     * @var array
     */
     private  $columnstarget;
    function __construct(Container $container) {
                $this->container=$container;
                $this->maxRowImport=16600;
                $this->maxRowImportPerConnection=4000;
                $this->maxIdImport=20;
                $this->maxIdImportPerConnection=10;
                //$this->clear();
      }
     public function clear(){
          $this->columnssource=null;
          $this->columnstarget=null;
          $this->data=null;
      }
      
   public function initData(SyncDbData $data){
             $this->data=$data;
             $coldata=$this->getContainer()->get('badiu.sync.db.column.data');
             $this->setColumnssource($coldata->findByTableid($this->getData()->getTblidsource()->getId()));
             $this->setColumnstarget($coldata->findByTableid($this->getData()->getTblidtarget()->getId()));
        }
   public function process($operation='_import_update_delete'){
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
         $listdata=$this->getContainer()->get('badiu.sync.db.data.data')->findByEntity($badiuSession->get()->getEntity());
        
		 foreach ($listdata  as $data) {
               echo "************----------".$data->getName()."----------***********<br>";
              if(strpos($data->getSyncoperation(),'import')>0 && strpos($operation,'import')>0 ){
                   $result= $this->import($data);
                   echo "----------start import----------<br>";
                   print_r($result);
                   echo "<br>----------end import----------<br><br><br><br>";
              }
              if(strpos($data->getSyncoperation(),'update')>0 && strpos($operation,'update')>0 ){
                   $result= $this->update($data);
                   echo "----------start update----------<br>";
                   print_r($result);
                   echo "<br>----------end update----------<br><br><br><br>";
              } 
              if(strpos($data->getSyncoperation(),'delete')>0 && strpos($operation,'delete')>0 ){
                   $result= $this->delete($data);
                   echo "----------start delete----------<br>";
                   print_r($result);
                   echo "<br>----------end delete----------<br><br><br><br>";
              }
          }
   }
    public function import(SyncDbData $data){
     
        $result=array();
        $result['process']=0;
        $result['exec']=0; 
        $result['dependence']=0;
        //start data
        $this->initData($data);
        //get last id  insert
		
        $ddata=$this->getContainer()->get('badiu.sync.db.data.data');
       // $lastidTarget=$this->getLastidTarget();
        $lastidinsert=$ddata->getLastidinsert($data->getId());
       
        //count record on source table
        $coutRecordSource=$this->getCoutRecordSource($lastidinsert,$data->getParam());
		
        if($coutRecordSource==0){
            return  $result;
        }
        //loop pagination
       $pagination=$this->pagination($coutRecordSource, $this->getMaxRowImportPerConnection());

       $x=0;
       for($i=0;$i< $pagination;$i++){
           $y=$this->getMaxRowImportPerConnection();
           if($i>0){$x=$x+$y;}
           //import
		   
           $rows=$this->selectSourceData($lastidinsert,$x,$y,$data->getParam());
		   $rows=$this->execServiceBeforeProcessAllRows('insert',$data,$rows);
             //insert
            $iresult=null;
           $symfonyservice=$data->getSfservicetarget();
		   
            if(!empty($symfonyservice)){
                $iresult=$this->insertDataSymfonyService($rows,$data,$data->getParam());
           }else{
               $iresult=$this->insertData($rows,$data);
           }
		    $rows=$this->execServiceAfterProcessAllRows('insert',$data,$rows);
			
           $result['process']+=$iresult['process'];
           $result['exec']+=$iresult['exec'];
           $result['dependence']+=$iresult['dependence'];
           
            if($result['process']>=$this->getMaxRowImport())break;
         }
           return $result;   
        
    }
    
    public function update(SyncDbData $data){
        
        $result=array();
        $result['process']=0;
        $result['exec']=0; 
        $result['dependence']=0;
      
        //start data
        $this->initData($data);
        
        //get last id updated
        $ddata=$this->getContainer()->get('badiu.sync.db.data.data');
        $lastidupdate=$ddata->getLastidupdate($data->getId());
     
       //count record on source table
        $coutRecordSource=$this->getCoutRecordSource($lastidupdate,$data->getParam());
       
        if(!empty($lastidupdate) && $coutRecordSource==0){
            $lastidupdate=null;
            $coutRecordSource=$this->getCoutRecordSource($lastidupdate,$data->getParam());
           
        }
        //loop pagination
       $pagination=$this->pagination($coutRecordSource, $this->getMaxRowImportPerConnection());
       $x=0;
      for($i=0;$i< $pagination;$i++){
           $y=$this->getMaxRowImportPerConnection();
           if($i>0){$x=$x+$y;}
           
           //import
           $rows=$this->selectSourceData($lastidupdate,$x,$y,$data->getParam());
           $rows=$this->execServiceBeforeProcessAllRows('update',$data,$rows);

         // print_r($rows);
        //  exit;
           $uprsult=null;
           $symfonyservice=$data->getSfservicetarget();
           if(!empty($symfonyservice)){
                $uprsult=$this->updateDataSymfonyService($rows,$data);
           }else{
                $uprsult=$this->updateData($rows,$data);
           }
            $rows=$this->execServiceAfterProcessAllRows('update',$data,$rows);
           //update
           $result['process']+=$uprsult['process'];
           $result['exec']+=$uprsult['exec'];
           $result['dependence']+=$uprsult['dependence'];
           if($result['process']>=$this->getMaxRowImport())break;
           
       }
        return $result;   
        
        //
        
    }
    
    public function delete(SyncDbData $data){
        
       
        $result=array();
        $result['process']=0;
        $result['exec']=0;
        $result['dependence']=0;
        //start data
        $this->initData($data);
       
        // check if source and target has same count data only if target is 100% slave
     /*   $coutRecordSource=$this->getCoutRecordSource();
        $coutRecordTarget=$this->getCoutRecordTarget();
        if($coutRecordSource==$coutRecordTarget){return $result;}
        */
        
        
       //get last id deleted
        $ddata=$this->getContainer()->get('badiu.sync.db.data.data');
        $lastiddelete=$ddata->getLastiddelete($data->getId());
       
        if(empty($lastiddelete)){$lastiddelete=null;}
       
      
       //count record on target table
       $coutRecordTarget=$this->getCoutRecordTarget($lastiddelete,$data->getParam());
      if($coutRecordTarget==0 && $lastiddelete!=null){
           $lastiddelete=null;
           $coutRecordTarget=$this->getCoutRecordTarget($lastiddelete,$data->getParam());
       }
       
       //count record on source table
       if($coutRecordTarget>0){
           $coutRecordSource=$this->getCoutRecordSource($lastiddelete,$data->getParam());
           if($coutRecordTarget <=$coutRecordSource){
              return $result;
           }
       }
       
        
        
       //loop pagination
       $pagination=$this->pagination($coutRecordTarget, $this->getMaxIdImportPerConnection());
      
       $x=0;
       for($i=0;$i< $pagination;$i++){
           $y=$this->getMaxIdImportPerConnection();
           if($i>0){$x=$x+$y;}
           
           //data
           $rowsSource=$this->selectSourcePK($lastiddelete,$x,$y,$data->getParam());
           
           $rowsTarget=$this->selectTargetPK($lastiddelete,$x,$y);
            
            //id to delete
           $listIdToDelete=$this->checkDeletedKey($data,$rowsSource,$rowsTarget);
           
           //delete/update
           $uprsult=null;
           $symfonyservice=$data->getSfservicetarget();
           if(!empty($symfonyservice)){
                $uprsult=$this->deleteDataSymfonyService($rowsTarget,$listIdToDelete,$data);
           }else{
                $uprsult=$this->deleteData($rowsTarget,$listIdToDelete,$data);
           }
           
           $result['process']+=$uprsult['process'];
           $result['exec']+=$uprsult['exec'];
           $result['dependence']+=$uprsult['dependence'];
           if($result['process']>=$this->getMaxIdImport())break;
           
       }
        
        return $result; 
    }
   public function getLastidTarget(){
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
       // $connection=$this->getContainer()->get('badiu.sync.db.lib.sql.factoryconnection')->get($this->getData()->getTblidtarget()->getDbid());
        
        $factorysql->setTable($this->getData()->getTblidtarget());
        $factorysql->setColumns($this->getColumnstarget());
        $sql= $factorysql->getSelectLastId();
		
		$factorydb=$this->getFactoryDb($this->getData()->getTblidtarget()->getDbid());
        $result = $factorydb->getRecord($sql);
        
        $result =$result['lastid'];
        if(empty($result)){$result=0;}
       return $result;
   }
   
      public function getLastidSource(){
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
        
        $factorysql->setTable($this->getData()->getTblidsource());
        $factorysql->setColumns($this->getColumnssource());
        $sql= $factorysql->getSelectLastId();
		
		$factorydb=$this->getFactoryDb($this->getData()->getTblidsource()->getDbid());
        $result = $factorydb->getRecord($sql);
        
        $result =$result['lastid'];
        return $result;
   }
   
   public function getCoutRecordSource($lastid,$dconfig){
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
		$factorysql->setTable($this->getData()->getTblidsource());
        $factorysql->setColumns($this->getColumnssource());
        $factorysql->setDboperation(DOMAINTABLE::$DB_SELECT_FROM_SOURCE);
        $factorysql->setDconfig($dconfig);
        $sql= $factorysql->getSelectCount($lastid);
		
		$factorydb=$this->getFactoryDb($this->getData()->getTblidsource()->getDbid());
		$result = $factorydb->getRecord($sql);
        
       return $result['countrecord'];
   }
     public function getCoutRecordTarget($lastid,$dconfig){
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
          
        $factorysql->setTable($this->getData()->getTblidtarget());
        $factorysql->setColumns($this->getColumnstarget());
        $factorysql->setDboperation(DOMAINTABLE::$DB_SELECT_FROM_TARGET);
        $factorysql->setDconfig($dconfig);
        $sql= $factorysql->getSelectCount($lastid);
		$factorydb=$this->getFactoryDb($this->getData()->getTblidtarget()->getDbid());
        $result = $factorydb->getRecord($sql);
       return $result['countrecord'];
   }
   
   public function existRecordTarget($data,$dconfig){
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
      
        $factorysql->setTable($this->getData()->getTblidtarget());
        $factorysql->setColumns($this->getColumnstarget());
        $factorysql->setDboperation(DOMAINTABLE::$DB_SELECT_FROM_TARGET);
        $factorysql->setDconfig($dconfig);
        $sql= $factorysql->existRecord($data);
	
		$factorydb=$this->getFactoryDb($this->getData()->getTblidtarget()->getDbid());
        $result = $factorydb->getRecord($sql);
      
       return $result['countrecord'];
   }
    public function selectSourceData($lastid,$x,$y,$dconfig){
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
       
        $factorysql->setTable($this->getData()->getTblidsource());
        $factorysql->setColumns($this->getColumnssource());
        $factorysql->setDboperation(DOMAINTABLE::$DB_SELECT_FROM_SOURCE);
        $factorysql->setDconfig($dconfig);
        $sql= $factorysql->getSelect($lastid);
        $factorydb=$this->getFactoryDb($this->getData()->getTblidsource()->getDbid());
        $result = $factorydb->getRecords($sql,$x,$y);
        
        return $result;
   }
   
    public function selectSourcePK($lastid,$x,$y,$dconfig){
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
         
        $factorysql->setTable($this->getData()->getTblidsource());
        $factorysql->setColumns($this->getColumnssource());
        $factorysql->setDboperation(DOMAINTABLE::$DB_SELECT_FROM_SOURCE);
        $factorysql->setDconfig($dconfig);
        $sql= $factorysql->getSelectPK($lastid);
		$factorydb=$this->getFactoryDb($this->getData()->getTblidsource()->getDbid());
       $result = $factorydb->getRecords($sql,$x,$y);
        
       return $result;
   }
   public function selectTargetPK($lastid,$x,$y){
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
        $factorysql->setTable($this->getData()->getTblidtarget());
        $factorysql->setColumns($this->getColumnstarget());
        $sql= $factorysql->getSelectPK($lastid);
		$factorydb=$this->getFactoryDb($this->getData()->getTblidtarget()->getDbid());
        $result = $factorydb->getRecords($sql,$x,$y);
        
       return $result;
   }
    public function insertData($rows,$data){
        $dataid=$data->getId();
        $dconfig=$data->getParam();
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
        $factorydb=$this->getFactoryDb($this->getData()->getTblidtarget()->getDbid());
        $factorysql->setTable($this->getData()->getTblidtarget());
        $factorysql->setColumns($this->getColumnstarget());
     
        $contProcess=0;
        $contExec=0;
        $resultExecServiceAfter=0;
		$resultExecServiceBefore=0;
		
        foreach ($rows  as $row) {
            //check if exist
			$resultExecServiceBefore+= $this->execServiceBeforeProcessEachRow('insert',$data,$row);
			
            if(!$this->existRecordTarget($row,$dconfig)){
               $sql=$factorysql->getInsert($row);
               echo $sql."<br>";
                $contExec+= $factorydb->insert($sql);
                 $valuePK=$factorysql->getValuesPK($row); 
                 $this->getContainer()->get('badiu.sync.db.data.data')->updateLastidinsert($valuePK,$dataid);
                $contProcess++;
             }
            $resultExecServiceAfter+= $this->execServiceAfterProcessEachRow('insert',$data,$row);
           }
             
         $result=array();
         $result['process']=$contProcess;
        $result['exec']=$contExec;
        $result['dependence']=$resultExecServiceAfter;
        
       return $result;
   }
    public function insertDataSymfonyService($rows,$data){
         $factoryconnection=$this->getContainer()->get('badiu.sync.db.lib.sql.factoryconnection');
         $symfonyservice=$this->getContainer()->get($data->getSfservicetarget());
         $symfonyservice->setDbconfig($factoryconnection->getConfig($data->getTblidtarget()->getDbid()));
         $symfonyservice->setData($rows);
         $result=$symfonyservice->insert();
         return $result;
    }
        
   public function updateData($rows,$data){
       $dataid=$data->getId();
       $dconfig=$data->getParam();

       $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
        $factorydb=$this->getFactoryDb($this->getData()->getTblidtarget()->getDbid());
        $factorysql->setTable($this->getData()->getTblidtarget());
        $factorysql->setColumns($this->getColumnstarget());
        $factorysql->setDboperation(DOMAINTABLE::$DB_UPDATE_TARGET);
        $factorysql->setDconfig($dconfig);
        $result=array();
        $contProcess=0;
        $contExec=0;
        $countrows=sizeof($rows)-1;
        $resultExecServiceAfter=0;
        foreach ($rows  as $row) {
            $resultExecServiceBefore= $this->execServiceBeforeProcessEachRow('update',$data,$row);
             $sql=$factorysql->getUpdate($row);
            echo "$sql<br>";
             $contExec += $factorydb->update($sql);
              if($countrows==$contProcess){
                $valuePK=$factorysql->getValuesPK($row); 
                $this->getContainer()->get('badiu.sync.db.data.data')->updateLastidupdate($valuePK,$dataid);
              }
           $resultExecServiceAfter= $this->execServiceAfterProcessEachRow('update',$data,$row);
            $contProcess++;
        }
        
        $result['process']=$contProcess;
        $result['exec']=$contExec;
        $result['dependence']=$resultExecServiceAfter;
       return $result;
   }
  public function updateDataSymfonyService($rows,$data){
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
        $factorysql->setColumns($this->getColumnstarget());
        $columnPk=$factorysql->geColumnPK();
        
        $factoryconnection=$this->getContainer()->get('badiu.sync.db.lib.sql.factoryconnection');
         $symfonyservice=$this->getContainer()->get($data->getSfservicetarget());
         $symfonyservice->setDbconfig($factoryconnection->getConfig($data->getTblidtarget()->getDbid()));
         $symfonyservice->setData($rows);
         $result=$symfonyservice->update();
         
         $countrows=sizeof($rows)-1;
         if($countrows>0){
             $row=$rows[$countrows];
             if(isset($row[$columnPk])){
                 $valuePK=$row[$columnPk]; 
                  $this->getContainer()->get('badiu.sync.db.data.data')->updateLastidupdate($valuePK,$data->getId());
             }
         }
         return $result;
  } 
   public function deleteData($rowsTarget,$rowsToDelete,$data){
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
        $connection=$this->getContainer()->get('badiu.sync.db.lib.sql.factoryconnection')->get($this->getData()->getTblidtarget()->getDbid());
        
        $factorysql->setTable($this->getData()->getTblidtarget());
        $factorysql->setColumns($this->getColumnstarget());
        $factorysql->setDboperation(DOMAINTABLE::$DB_DELETE_TARGET);
        $factorysql->setDconfig($dconfig);
        $result=array();
        $contProcess=0;
        $contExec=0;
        $dependence=0;
        $countrows=sizeof($rowsTarget)-1;
        $operation=$this->getOperationOnDelete($data);
        $symfonyservice=null;
        if($operation=='exec_symony_service'){ $symfonyservice=$this->getSymfonyServiceOnDelete($data);}
        foreach ($rowsTarget  as $row) {
            $valuePK=$row['colid'];
            if(in_array($valuePK, $rowsToDelete)) {
               $sql="";
              if($operation=='update_field'){
                $factorysql->setDboperation(DOMAINTABLE::$DB_UPDATE_TARGET);
                  $sql=$factorysql->getUpdateOnDelete($valuePK);
                  $contExec += $connection->exec($sql); 
              }
              else if($operation=='exec_symfony_service'){
                  $factoryconnection=$this->getContainer()->get('badiu.sync.db.lib.sql.factoryconnection');
                  $symfonyservice=$this->getContainer()->get($symfonyservice);
                  $symfonyservice->setDbconfig($factoryconnection->getConfig($data->getTblidtarget()->getDbid()));
                  $resultsfs=$symfonyservice->delete($valuePK);
                  $contExec +=$resultsfs['exec'];
                  $dependence+=$resultsfs['$dependence'];
              }
              else {
                  $sql=$factorysql->getDelete($valuePK);
                   $contExec += $connection->exec($sql); 
              }
              
            }
           if($countrows==$contProcess){
               $this->getContainer()->get('badiu.sync.db.data.data')->updateLastiddelete($valuePK,$data->getId());
              }
            $contProcess++;
        }
        $result['process']=$contProcess;
        $result['exec']=$contExec;
        $result['dependence']=$dependence;
       return $result;
   }
   
    public function deleteDataSymfonyService($rowsTarget,$rowsToDelete,$data){
        $factorysql=$this->getContainer()->get('badiu.sync.db.lib.sql.factorysql');
        $factorysql->setColumns($this->getColumnstarget());
        $columPk=$factorysql->geColumnPK();
        $result=array();
        $dataToDelte=array();
       $valuePK=null;
        foreach ($rowsTarget  as $row) {
            $valuePK=$row['colid'];
            if(in_array($valuePK, $rowsToDelete)) {
               array_push($dataToDelte,array($columPk=>$valuePK));
            }
           
        }
        
         $factoryconnection=$this->getContainer()->get('badiu.sync.db.lib.sql.factoryconnection');
         $symfonyservice=$this->getContainer()->get($data->getSfservicetarget());
         $symfonyservice->setDbconfig($factoryconnection->getConfig($data->getTblidtarget()->getDbid()));
         $symfonyservice->setData($dataToDelte);
         $result=$symfonyservice->delete();
         $this->getContainer()->get('badiu.sync.db.data.data')->updateLastiddelete($valuePK,$data->getId());
       
       return $result;
   }
    public function checkDeletedKey($data,$rowsSource,$rowsTarget){
        $listToDelete=array();
        $factorysqlutil= $this->getContainer()->get('badiu.sync.db.lib.sql.factorysqlutil');
        $listexception=$factorysqlutil->getPkListExceptionOnDelete($data);
        
        
         foreach ($rowsTarget  as $row) {
             $colid=$row['colid'];
             if (!in_array($row, $rowsSource) && !in_array($colid, $listexception)) {
               array_push($listToDelete,$colid);
             }
         }
       return  $listToDelete;
    }
    
     public function getOperationOnDelete($data){
        $param="";
        $httpQueryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
        $httpQueryString->setQuery($data->getParam());
        $httpQueryString->makeParam();
        if($httpQueryString->existKey(DOMAINTABLE::$PARAM_OPERATION_ON_DELETE)){
            $param=$httpQueryString->getValue(DOMAINTABLE::$PARAM_OPERATION_ON_DELETE);
        }
        return $param;
     }
     public function getSymfonyServiceOnDelete($data){
        $param="";
        $httpQueryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
        $httpQueryString->setQuery($data->getParam());
        $httpQueryString->makeParam();
        if($httpQueryString->existKey(DOMAINTABLE::$PARAM_SYMFONY_SERVICE_EXEC_ON_DELETE)){
            $param=$httpQueryString->getValue(DOMAINTABLE::$PARAM_SYMFONY_SERVICE_EXEC_ON_DELETE);
        }
        return $param;
     }

    public function execServiceAfterProcessAllRows($operation,$data,$rows){
			$serviceAddress=null;
        $paramKey=null;

        $httpQueryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
        $httpQueryString->setQuery($data->getParam());
        $httpQueryString->makeParam();

        if($operation=='insert'){$paramKey=DOMAINTABLE::$PARAM_SYMFONY_SERVICE_EXEC_AFTER_INSERT_ALL_ROWS;}
        else if($operation=='update'){$paramKey=DOMAINTABLE::$PARAM_SYMFONY_SERVICE_EXE_AFTER_UPDATE_ALL_ROWS;}
        else return $rows;
        if($httpQueryString->existKey($paramKey)){
            $serviceAddress=$httpQueryString->getValue($paramKey);
        }

        if(empty($serviceAddress)){return $rows;}
        $service=$httpQueryString=$this->getContainer()->get($serviceAddress);
        $rows= $service->exec($data,$rows);
        return $rows;
    }
    public function execServiceBeforeProcessAllRows($operation,$data,$rows){
        $serviceAddress=null;
        $paramKey=null;

        $httpQueryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
        $httpQueryString->setQuery($data->getParam());
        $httpQueryString->makeParam();

        if($operation=='insert'){$paramKey=DOMAINTABLE::$PARAM_SYMFONY_SERVICE_EXEC_BEFORE_INSERT_ALL_ROWS;}
        else if($operation=='update'){$paramKey=DOMAINTABLE::$PARAM_SYMFONY_SERVICE_EXEC_BEFORE_UPDATE_ALL_ROWS;}
        else return $rows;
        if($httpQueryString->existKey($paramKey)){
            $serviceAddress=$httpQueryString->getValue($paramKey);
        }

        if(empty($serviceAddress)){return $rows;}
        $service=$httpQueryString=$this->getContainer()->get($serviceAddress);
        $rows= $service->exec($data,$rows);
        return $rows;
    }
    public function execServiceBeforeProcessEachRow($operation,$data,$row){
		 $serviceAddress=null;
        $paramKey=null;
        
        $httpQueryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
        $httpQueryString->setQuery($data->getParam());
        $httpQueryString->makeParam();

        if($operation=='insert'){$paramKey=DOMAINTABLE::$PARAM_SYMFONY_SERVICE_EXEC_BEFORE_INSERT_EACH_ROW;}
        else if($operation=='update'){$paramKey=DOMAINTABLE::$PARAM_SYMFONY_SERVICE_EXEC_BEFORE_UPDATE_EACH_ROW;}
        else return 0;

        if($httpQueryString->existKey($paramKey)){
            $serviceAddress=$httpQueryString->getValue($paramKey);
        }

        if(empty($serviceAddress)){return 0;}
        $service=$httpQueryString=$this->getContainer()->get($serviceAddress);
        $result= $service->exec($data,$row);
        return $result;
    }
    public function execServiceAfterProcessEachRow($operation,$data,$row){
        $serviceAddress=null;
        $paramKey=null;
        
        $httpQueryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
        $httpQueryString->setQuery($data->getParam());
        $httpQueryString->makeParam();

        if($operation=='insert'){$paramKey=DOMAINTABLE::$PARAM_SYMFONY_SERVICE_EXEC_AFTER_INSERT_EACH_ROW;}
        else if($operation=='update'){$paramKey=DOMAINTABLE::$PARAM_SYMFONY_SERVICE_EXEC_AFTER_UPDATE_EACH_ROW;}
        else return 0;

        if($httpQueryString->existKey($paramKey)){
            $serviceAddress=$httpQueryString->getValue($paramKey);
        }

        if(empty($serviceAddress)){return 0;}
        $service=$httpQueryString=$this->getContainer()->get($serviceAddress);
        $result= $service->exec($data,$row);
        return $result;
    }
    public function pagination($totalRecord,$pagination){
      
        $paging=0;
        $paging=$totalRecord/$pagination;
        if($paging != round($paging)){
              $paging=floor($paging)+1;
         } 
         return $paging;
    } 
     
    public function getFactoryDb($db){
		$factorydb=null;
		if($db->getTypeConnection()==DOMAINTABLE::$TYPE_DB_CONNECTION){
			$dbconfig=$this->getContainer()->get('badiu.sync.db.lib.sql.factoryconnection')->getConfig($db);
			$factorydb=$this->getContainer()->get('badiu.system.core.lib.sql.factorydb');
			$factorydb->setConfig($dbconfig);
			
		}else if($db->getTypeConnection()==DOMAINTABLE::$TYPE_SQLSERVICE_CONNECTION){
			$dbconfig=$this->getContainer()->get('badiu.sync.db.lib.sqlservice.factoryconnection')->getConfig($db);
			$factorydb=$this->getContainer()->get('badiu.system.core.lib.sqlservice.factorydb');
			$factorydb->setConfig($dbconfig);
			
		}
		
		return $factorydb;
	}
    public function getContainer() {
        return $this->container;
    }

    public function getMaxRowImport() {
        return $this->maxRowImport;
    }

    public function getMaxRowImportPerConnection() {
        return $this->maxRowImportPerConnection;
    }

    public function getMaxIdImport() {
        return $this->maxIdImport;
    }

    public function getMaxIdImportPerConnection() {
        return $this->maxIdImportPerConnection;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }

    public function setMaxRowImport($maxRowImport) {
        $this->maxRowImport = $maxRowImport;
    }

    public function setMaxRowImportPerConnection($maxRowImportPerConnection) {
        $this->maxRowImportPerConnection = $maxRowImportPerConnection;
    }

    public function setMaxIdImport($maxIdImport) {
        $this->maxIdImport = $maxIdImport;
    }

    public function setMaxIdImportPerConnection($maxIdImportPerConnection) {
        $this->maxIdImportPerConnection = $maxIdImportPerConnection;
    }

    public function getData() {
        return $this->data;
    }

    public function setData(SyncDbData $data) {
        $this->data = $data;
    }

    public function getColumnssource() {
        return $this->columnssource;
    }

    public function getColumnstarget() {
        return $this->columnstarget;
    }

    public function setColumnssource($columnssource) {
        $this->columnssource = $columnssource;
    }

    public function setColumnstarget($columnstarget) {
        $this->columnstarget = $columnstarget;
    }



}
