<?php

namespace Badiu\Sync\DbBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class DataData extends BadiuDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
              
   
        public function getListToExec($param=array()) {
             $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.id > :id $wsql ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',0);
            $query=$this->makeSqlFilter($query, $param);
            $result= $query->getResult();
            return  $result;
        }
        
       public function updateLastidupdate($lastidupdate,$id) {
           $sql="UPDATE ".$this->getBundleEntity()." o SET o.lastidupdate=:lastidupdate WHERE o.id=:id";
           $query = $this->getEm()->createQuery($sql);
           $query->setParameter('lastidupdate', $lastidupdate);
           $query->setParameter('id', $id);
           $result=$query->execute();
           return $result;
      } 
     
      public function updateLastidinsert($lastidinsert,$id) {
           $sql="UPDATE ".$this->getBundleEntity()." o SET o.lastidinsert=:lastidinsert WHERE o.id=:id";
           $query = $this->getEm()->createQuery($sql);
           $query->setParameter('lastidinsert', $lastidinsert);
           $query->setParameter('id', $id);
           $result=$query->execute();
           return $result;
      } 
      public function updateLastiddelete($lastiddelete,$id) {
          $sql="UPDATE ".$this->getBundleEntity()." o SET o.lastiddelete=:lastiddelete WHERE o.id=:id";
           $query = $this->getEm()->createQuery($sql);
           $query->setParameter('lastiddelete', $lastiddelete);
           $query->setParameter('id', $id);
           $result=$query->execute();
           return $result;
      } 
     
      
        public function getLastidupdate($id) {
            $sql="SELECT  o.lastidupdate FROM ".$this->getBundleEntity()." o  WHERE o.id = :id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
            $result=$result['lastidupdate'];
            return  $result;
        }
       public function getLastidinsert($id) {
            $sql="SELECT  o.lastidinsert FROM ".$this->getBundleEntity()." o  WHERE o.id = :id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
            $result=$result['lastidinsert'];
            return  $result;
        }
        public function getLastiddelete($id) {
            $sql="SELECT  o.lastiddelete FROM ".$this->getBundleEntity()." o  WHERE o.id = :id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
            $result=$result['lastiddelete'];
            return  $result;
        }
      

        
        public function getEnabled($entity=null) {
                $wsql=" ";
              
                if(!empty($entity)){$wsql=" AND o.entity=:entity ";}
                $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE  o.status = :status $wsql";        
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('status','active');
                if(!empty($entity)){$query->setParameter('entity',$entity);}
                $result= $query->getResult();
             
		return $result;
    }

    
     
       public function chageStatus($id,$status,$timeend=false) {  
          $param=array('id'=>$id,'status'=>$status);
          if($timeend){$param=array('id'=>$id,'status'=>$status,'timelastexec'=>new \Datetime());}
           $result=$this->updateNativeSql($param);
           return $result;
       
     }
  
    public function start($param) {  
         $id=0;
         $timestart=null;
         if(isset($param['id'])) {$id=$param['id'];}
         if(isset($param['timestart'])) {$timestart=$param['timestart'];}
          
         $tparam=array('id'=>$id,'status'=>'processing','timestart'=>$timestart);
         $result=$this->updateNativeSql($tparam,false);
         return $result;
    }
    public function end($param) {  
         $id=0;
         $timestart=null;
         $timeend=new \Datetime();
         $datashouldexec=null;
         $dataexec=null;
         $resultinfo=null;
         
          if(isset($param['id'])) {$id=$param['id'];}
          if(isset($param['timestart'])) {$timestart=$param['timestart'];}
          
           if(isset($param['resultinfo']['datashouldexec'])) {$datashouldexec=$param['resultinfo']['datashouldexec'];}
           if(isset($param['resultinfo']['dataexec'])) {$dataexec=$param['resultinfo']['dataexec'];}
           if(isset($param['resultinfo'])) {$resultinfo=$param['resultinfo'];}
           
           $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
           $resultinfo= $json->encode($resultinfo);
           
         $start=$timestart->getTimestamp();
         $end=$timeend->getTimestamp();
         $timeexec=$end-$start; 
         $tparam=array('id'=>$id,'status'=>'active','laststatus'=>'success','timeend'=>new \Datetime(),'timeexec'=>$timeexec,'datashouldexec'=>$datashouldexec,'resultinfo'=>$resultinfo);
         $result=$this->updateNativeSql($tparam,false);
         return $result;
    }
}
