<?php

namespace Badiu\Sync\DbBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
/**
 * Description of DataExec
 *
 * @author lino
 */
class SqlServiceConnection {
    
  
      /**
     * @var Container
     */
    private $container;
    /**
     * @var string
     */
    private $whichdb; //source | target
    
    
    /**
     * @var object
     */
    private $syncdbdata;
    
     /**
	 * @var object
	 */
    private $search;
    
    /**
	 * @var integer
	 */
    private $sserviceid;
    function __construct(Container $container) {
            $this->container=$container;
            $this->search = $this->getContainer()->get('badiu.system.core.functionality.search');
     }
    function init($syncdbdata,$whichdb) {
         $this->syncdbdata=$syncdbdata;
         $this->whichdb=$whichdb;
          if($this->whichdb=='source'){
              if(!empty($this->getSyncdbdata()->getDbsource())){$this->sserviceid=$this->getSyncdbdata()->getDbsource()->getId();}
          }else if($this->whichdb=='target'){
              if(!empty($this->getSyncdbdata()->getDbtarget())){$this->sserviceid=$this->getSyncdbdata()->getDbtarget()->getId();}
          }
     }
     
     function select($param=null) {
         $sql=null;
         if($this->whichdb=='source'){$sql=$this->getSyncdbdata()->getDbsourcesqlselecttoinsert();}
          $sql=$this->changeExpression($sql,$param);
       
         $limit=100;
         $offset=0;
          if(isset($param['offset'])){$offset=$param['offset'];}
          if(isset($param['limit'])){$limit=$param['limit'];}
            //echo $sql;
          //  print_r($param);
         //   echo "\n <hr>";
         $param=array('_serviceid'=>$this->getSserviceid(),'type'=>'M','query'=>$sql,'offset'=>$offset,'limit'=>$limit);
         $result=  $this->getSearch()->execSqlService($param); 
         $result=$this->getResponse($result);
         return $result;
      }
      function maxid() {
          $sql=null;
          if($this->whichdb=='source'){$sql=$this->getSyncdbdata()->getDbsourcesqllastid();}
          else if($this->whichdb=='target'){$sql=$this->getSyncdbdata()->getDbtargetsqllastid();}
         $param=array('_serviceid'=>$this->getSserviceid(),'type'=>'S','query'=>$sql);
         $result=  $this->getSearch()->execSqlService($param);
         $result=$this->getResponseSingle($result);
         return $result;
     }
     
      function count($param) {
         $sql=null;
          if($this->whichdb=='source'){$sql=$this->getSyncdbdata()->getDbsourcesqlcount();}
          else if($this->whichdb=='target'){$sql=$this->getSyncdbdata()->getDbtargetsqlcount();}
         $sql=$this->changeExpression($sql,$param);
         $param=array('_serviceid'=>$this->getSserviceid(),'type'=>'S','query'=>$sql);
         $result=  $this->getSearch()->execSqlService($param); 
        $result=$this->getResponse($result,true);
         return $result;
      }
      
      function insert() {
        
         return null;
      }
      function getResponse($result,$single=false) {
          $value=null;
          if($result['status']=='accept'){
             if(!$single){
                 $value=$result['message'];
                 return $value;
             }
             
             if(is_array($result['message'])){
                foreach ($result['message'] as $key => $value) {
                    $value=$value;
                }
             }else{$value=$result['message'];}
         }else{echo "-------------------ERROR---------------------\n";print_r($result);} 
         return $value;
      } 
      
    function changeExpression($sql,$param) {
        
        if(empty($param)){return $sql;}
            foreach ($param as $key => $value) {
                $expkey=":".$key;
                $sql=str_replace($expkey,$value,$sql);
        }
         return $sql;
     }

     
     function getContainer() {
         return $this->container;
     }

     function getWhichdb() {
         return $this->whichdb;
     }

     function getSyncdbdata() {
         return $this->syncdbdata;
     }

     function getSearch() {
         return $this->search;
     }

     function setContainer(Container $container) {
         $this->container = $container;
     }

     function setWhichdb($whichdb) {
         $this->whichdb = $whichdb;
     }

     function setSyncdbdata($syncdbdata) {
         $this->syncdbdata = $syncdbdata;
     }

     function setSearch($search) {
         $this->search = $search;
     }

     function getSserviceid() {
         return $this->sserviceid;
     }

     function setSserviceid($sserviceid) {
         $this->sserviceid = $sserviceid;
     }


}
