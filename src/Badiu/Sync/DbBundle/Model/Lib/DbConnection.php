<?php

namespace Badiu\Sync\DbBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Description of DataExec
 *
 * @author lino
 */
class DbConnection {

    /**
     * @var Container
     */
    private $container;

    /**
     * @var string
     */
    private $whichdb; //source | target

    /**
     * @var string
     */
    private $dbtype;

    /**
     * @var object
     */
    private $syncdbdata;

    /**
     * @var object
     */
    private $factorydb;

    function __construct(Container $container) {
        $this->container = $container;
       
    }

    function init($syncdbdata, $whichdb) {
      
        $this->syncdbdata = $syncdbdata;
        $this->whichdb = $whichdb;

        if ($this->whichdb == 'source') {
            if (!empty($this->getSyncdbdata()->getDbsource())) {
                $this->dbtype = $this->getSyncdbdata()->getDbsource()->getDbtype();
                $this->initFactorydb();
            }
        } else if ($this->whichdb == 'target') {
             if (!empty($this->getSyncdbdata()->getDbtarget())) {
                 $this->sserviceid = $this->getSyncdbdata()->getDbtarget()->getId();
                $this->dbtype = $this->getSyncdbdata()->getDbtarget()->getDbtype();
                 $this->initFactorydb();
                
            }
        }
       
    }

    function insert($rows) {
        if ($this->whichdb == 'target') {
            $sql = $this->getSyncdbdata()->getDbtargetsqlinsert();
           if ($this->dbtype == 'mongodb') {
                $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
                $paramconf = $json->decode($sql, true);
                $mongdbconn = $this->getMongoDbConn($this->getSyncdbdata()->getDbtarget(), $paramconf);
                $mongoutil = $this->getContainer()->get('badiu.system.core.lib.mongodb.util');
                $rows = $mongoutil->castRowsToInsert($rows);
                $result = $mongdbconn->batchInsert($rows);

                return $result;
            } else {
               /* echo "xx15";
                echo "<pre>";
                print_r($rows);
                echo "</pre>";exit;*/
                //$result = $this->factorydb->batchInsert($rows);
                $result = $this->factorydb->insert($rows);
                return $result;
            }
        }
        return null;
    }

    function delete($paramfilter = null,$sql=null) {
        if ($this->whichdb == 'target') {
            if(empty($sql)){ $sql = $this->getSyncdbdata()->getDbtargetsqldelete();}
            
           if ($this->dbtype == 'mongodb') {//review
                  //$json = $this->getContainer()->get('badiu.system.core.lib.util.json');
                  //$paramconf = $json->decode($sql, true);
                  return null;
            } else {
                 $sql= $this->replaceParam($paramfilter,$sql);
                 $result = $this->factorydb->delete($sql);
                return $result;
            }
        }
        return null;
    }
    function select($param = null) {
       
        if ($this->whichdb == 'source') {
            
            $sql = $this->getSyncdbdata()->getDbsourcesqlselecttoinsert();
            
            $offset = 0;
            $limit = 100;
            if (isset($param['offset'])) {
                $offset = $param['offset'];
            }
            if (isset($param['limit'])) {
                $limit = $param['limit'];
            }
			 
            if ($this->dbtype == 'mongodb') {
                
                $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
                $paramconf = $json->decode($sql, true);
            
                $mongdbconn = $this->getMongoDbConn($this->getSyncdbdata()->getDbsource(), $paramconf,$param);
                $result = $mongdbconn->find($offset, $limit);
                return $result;
            } else {
				$lastid=0;
				 if (isset($param['id'])) {$lastid=$param['id'];}
				 if(empty($lastid)){$lastid=0;}
				 $sql=str_replace(":id",$lastid,$sql);
				 
				
                $result = $this->factorydb->getRecords($sql, $offset, $limit);
                return $result;
            }
        }


        return null;
    }

    function selectlastid($param = null, $paramfilter = null) {
     
        if ($this->whichdb == 'target') {
            $sql = $this->getSyncdbdata()->getDbtargetsqllastid();
             //print_r($sql);
            $offset = 0;
            $limit = 1;
            if (isset($param['offset'])) {
                $offset = $param['offset'];
            }
            if (isset($param['limit'])) {
                $limit = $param['limit'];
            }

            if ($this->dbtype == 'mongodb') {
                $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
                $paramconf = $json->decode($sql, true);
              
                $mongdbconn = $this->getMongoDbConn($this->getSyncdbdata()->getDbtarget(), $paramconf, $paramfilter);
                $result = $mongdbconn->find($offset, $limit);
                $cont = 0;
                $lastid = 0;
              foreach ($result as $row) {
                    if ($cont == 0) {
                        foreach ($row as $key => $value) {
                            if (is_int($value)) {$lastid = $value;break;}
                        }
                        break;
                    }
                    $cont++;
                }
                //echo "lastid: $lastid";exit;
                return $lastid;
            } else {
                $sql= $this->replaceParam($paramfilter,$sql);
               // echo $sql;exit;
                $result = $this->factorydb->getRecord($sql);
				//print_r($result);exit;
                $sresult=null;
                if (is_array($result )) {
                    foreach ($result as $r) {$sresult=$r;}
                }
                return $sresult;
            }
        }
        return null;
    }

    //review it
    function count($param = null, $paramfilter = null) {
            $sql = null;
            if ($this->whichdb == 'target') {$sql = $this->getSyncdbdata()->getDbtargetsqlcount();}
            else if ($this->whichdb == 'source') {$sql = $this->getSyncdbdata()->getDbsourcesqlcount();}
             //print_r($sql);
            $offset = 0;
            $limit = 1;
            if (isset($param['offset'])) {
                $offset = $param['offset'];
            }
            if (isset($param['limit'])) {
                $limit = $param['limit'];
            }

            if ($this->dbtype == 'mongodb') {
                $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
                $paramconf = $json->decode($sql, true);
                $mongdbconn = $this->getMongoDbConn($this->getSyncdbdata()->getDbtarget(), $paramconf, $paramfilter);
                $result = $mongdbconn->find($offset, $limit);
                $cont = 0;
                $lastid = 0;
              foreach ($result as $row) {
                    if ($cont == 0) {
                        foreach ($row as $key => $value) {
                            if (is_int($value)) {$lastid = $value;break;}
                        }
                        break;
                    }
                    $cont++;
                }
                //echo "lastid: $lastid";exit;
                return $lastid;
            } else {
                $result = $this->factorydb->getRecord($sql);
                $sresult=null;
                if (is_array($result )) {
                    foreach ($result as $r) {$sresult=$r;}
                }
                return $sresult;
            }
       
        return null;
    }
    function getMongoDbConn($sservice, $paramconf = null, $paramfilter = null) {
        $mongodbfactoryconn = $this->getContainer()->get('badiu.system.core.lib.mongodb.factoryconnection');
        $mongodbfactoryconn->initConfigBySservice($sservice, $paramconf, $paramfilter);
        return $mongodbfactoryconn;
    }

    private function initFactorydb() {
      
        if ($this->dbtype == 'mongodb') {
            return null;
        }
    
        $dbtype = $this->dbtype;
        $driveClass = null;
        $charset = "UTF8";
        $host = null;
        $port = null;
        $user = null;
        $password = null;
        $dbname = null;
       
        if ($this->whichdb == 'source') {
            if (!empty($this->getSyncdbdata()->getDbsource())) {
                $host = $this->getSyncdbdata()->getDbsource()->getDbhost();
                $port = $this->getSyncdbdata()->getDbsource()->getDbport();
                $user = $this->getSyncdbdata()->getDbsource()->getDbuser();
                $password = $this->getSyncdbdata()->getDbsource()->getDbpwd();
                $dbname = $this->getSyncdbdata()->getDbsource()->getDbname();
                $tcharset= $this->getSyncdbdata()->getDbsource()->getTcharset();
                if(!empty($tcharset)){$charset = $tcharset;}
                
			   $useapplicationconfig=false;
			   $dconfig=$this->getSyncdbdata()->getDbsource()->getDconfig();
			   if(!empty($dconfig)){
				   $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
				   $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
				   $dconfig= $json->decode($dconfig, true);
				   $useapplicationconfig=$utildata->getVaueOfArray($dconfig, 'connection.useapplicationconfig',true);
			   }
			  if($useapplicationconfig){
				    $host =  $this->getContainer()->getParameter('database_host');
					$port =  $this->getContainer()->getParameter('database_port');
					$user =  $this->getContainer()->getParameter('database_user');
					$password =  $this->getContainer()->getParameter('database_password');
					$dbname =  $this->getContainer()->getParameter('database_name');
			  }
			  				
            }
        } else if ($this->whichdb == 'target') {
              
            if (!empty($this->getSyncdbdata()->getDbtarget())) {
               
                $host = $this->getSyncdbdata()->getDbtarget()->getDbhost();
                $port = $this->getSyncdbdata()->getDbtarget()->getDbport();
                $user = $this->getSyncdbdata()->getDbtarget()->getDbuser();
               $password = $this->getSyncdbdata()->getDbtarget()->getDbpwd();
               $dbname = $this->getSyncdbdata()->getDbtarget()->getDbname();
               $tcharset= $this->getSyncdbdata()->getDbtarget()->getTcharset();
			   
			   $useapplicationconfig=false;
			   $dconfig=$this->getSyncdbdata()->getDbtarget()->getDconfig();
			   if(!empty($dconfig)){
				   $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
				   $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
				   $dconfig= $json->decode($dconfig, true);
				   $useapplicationconfig=$utildata->getVaueOfArray($dconfig, 'connection.useapplicationconfig',true);
			   }
			  if($useapplicationconfig){
				    $host =  $this->getContainer()->getParameter('database_host');
					$port =  $this->getContainer()->getParameter('database_port');
					$user =  $this->getContainer()->getParameter('database_user');
					$password =  $this->getContainer()->getParameter('database_password');
					$dbname =  $this->getContainer()->getParameter('database_name');
			  }
			  
			 

              if(!empty($tcharset)){$charset = $tcharset;}
            }
        }
        $conn = array();


        if (empty($driveClass)) {
            $conn['driver'] = $this->getDrive($dbtype);
        } else {
            $conn['driverClass'] = $driveClass;
        }
        $conn['host'] = $host;
        $conn['port'] = $port;
        $conn['user'] = $user;
        $conn['password'] = $password;
        $conn['dbname'] = $dbname;
        $conn['charset']= $charset;
        $conn['type'] = $dbtype;
       // print_r($conn);exit;
        $this->factorydb = $this->getContainer()->get('badiu.system.core.lib.sql.factorydb');
        $this->factorydb->setConfig($conn);
    }

    private function getDrive($drive) {
        $pdo = "";
        if ($drive == 'mysql') {
            $pdo = "pdo_mysql";
        } else if ($drive == 'pgsql') {
            $pdo = "pdo_pgsql";
        } else if ($drive == 'mssql') {
            $pdo = "pdo_sqlsrv";
        }
        return $pdo;
    }

     function replaceParam($param,$sql) {
         if(!is_array($param)){return $sql ;}
         foreach ($param as $key => $value) {
             $skey=":$key";
              $sql=str_replace($skey,$value,$sql);
         }
         return  $sql;
     }
    function getContainer() {
        return $this->container;
    }

    function getWhichdb() {
        return $this->whichdb;
    }

    function getSyncdbdata() {
        return $this->syncdbdata;
    }

    function getDbtype() {
        return $this->dbtype;
    }

    function setDbtype($dbtype) {
        $this->dbtype = $dbtype;
    }

    function getFactorydb() {
        return $this->factorydb;
    }

    function setFactorydb($factorydb) {
        $this->factorydb = $factorydb;
    }

}
