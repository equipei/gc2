<?php

namespace Badiu\Sync\DbBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class DataCron extends BadiuModelLib{
    // /system/service/process?_service=badiu.sync.db.data.cron&operation=import
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
              
	public function exec() {
                        
                        $dataexec=$this->getContainer()->get('badiu.sync.db.lib.dataexec');
                        $data=$this->getContainer()->get('badiu.sync.db.data.data');
                      //  $datalog=$this->getContainer()->get('badiu.sync.db.data.log');
                        $entity=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('entity');
                        $operation=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('operation');
                        $id=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('id');
                        $force=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_force');
                       $list=array();
                       if(!empty($id)){
                           $dto=$data->findById($id);
                           array_push($list,$dto);
                       }else{
                           $list=$data->getEnabled($entity);
                            $force=0;
                       }
                        
                        $listresult=array();
                        foreach ($list as $dsync) {
                       
                            //review
                            if($operation=='import'){
								
                                $exec=$this->isJustExec($dsync,$operation);
                                if($force){$exec=true;}
                               $resultmdl= $dataexec->import($dsync); 
							
							   array_push($listresult,$resultmdl);
                            }else if($operation=='update'){
                                 $exec=$this->isJustExec($dsync,$operation);
                            }
                            else if($operation=='delete'){
                                 $exec=$this->isJustExec($dsync,$operation);
                                 
                            }
                            
                            /*
                           $exec=$this->isJustExec($dsync,$operation);
                           if($force){$exec=true;}
                            if($exec){
                               $syncoperation=$dsync->geSyncoperation();
                               $syncid=$dsync->getId();
                               $confparam=$this->getJson()->decode($dsync->getParam(),true);
                               $result=null;
                           
                           //_import
                              
                                $timestart=new \Datetime();
                                $tparam=array('id'=>$syncid,'timestart'=>$timestart);
                                $data->start($tparam);
                                    
                                $param=array('taskid'=>$taskid,'entity'=>$this->getEntity(),'timestart'=>$timestart);
                                $tasklogid=$datalog->start($param);
                                $service->setOperation($operation); 
                                $service->setParam($confparam);
                                $result=array();
                                try {
                                     $result=$service->exec($tasklogid);
                                } catch (Exception $ex) {
                                    $result=array('error'=> $ex->getMessage()) ;  
                                }
                                    
                                $param=array('tasklogid'=>$tasklogid,'timestart'=>$timestart,'resultinfo'=>$result);
                                $datalog->end($param);
                                    
                                $tparam=array('id'=>$taskid,'timestart'=>$timestart,'resultinfo'=>$result);
                                $data->end($tparam);
                             }//end if($exec)
                            */
                            
                        }
		
			return $listresult;
	}
	

   
        public function isJustExec($dsync,$operation) {
             return true;
              $schedulerroutine=$this->getContainer()->get('badiu.system.core.lib.date.schedulerroutine');
              $param=array();
              $param['timeroutine']=$dsync->getTimeroutine();
              $param['timeroutineparam']=$this->castToJson($dsync->getTimeroutineparam());
              $param['timelastexec']=$dsync->getTimestart();
              $param['timeexeconce']=$dsync->getTimeexeconce();
              $param['execperiodstart']=$dsync->getExecperiodstart(); 
              $param['execperiodend']=$dsync->getExecperiodend();
            
              $schedulerroutine->initParam($param);
              $canexec= $schedulerroutine->canExec();
            
              return $canexec;
              
        }
    public function castToJson($text) {
            if(empty($text)){return null;}
            $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
            $jparam =$json->decode($text, false);
           return $jparam;
      }
      
}
