<?php

namespace Badiu\Sync\DbBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**
 * Description of DataExec
 *
 * @author lino
 */
class DataExec extends BadiuModelLib{


    /**
     * @var integer
     */
    private $maxRowImport;

    /**
     * @var integer
     */
    private $maxRowImportPerConnection;

    /**
     * @var integer
     */
    private $maxIdImport;

    /**
     * @var integer
     */
    private $maxIdImportPerConnection;

    /**
     * @var string
     */
    private $dataserivce;

    /**
     * @var object
     */
    private $dbsourcemconn;

    /**
     * @var object
     */
    private $dbtargetmconn;

    function __construct(Container $container) {
       parent::__construct($container);
      
        $this->maxRowImport = 50000;
        $this->maxRowImportPerConnection = 2500;
        $this->maxIdImport = 100000;
        $this->maxIdImportPerConnection = 10000;
        $this->dataserivce = $this->getContainer()->get('badiu.sync.db.data.data');

        $this->dbsourcemconn = $this->getContainer()->get('badiu.sync.db.lib.manageconnection');
        $this->dbtargetmconn = $this->getContainer()->get('badiu.sync.db.lib.manageconnection');
    }

    function import($list=null) {
           $badiuSession=$this->getContainer()->get('badiu.system.access.session');
           $entity=$badiuSession->get()->getEntity();
           if(empty($list)){
               $list = $this->getDataserivce()->getListToExec();
           }
           
           
            //list server service moodle
            $sdata = $this->getContainer()->get('badiu.admin.server.service.data');
            $param=  array('dtype'=>'site_moodle','syncdb'=>true);
            $listmdl=$sdata->findByParam($entity,$param);
            
          //$datasynclist = $this->getDataserivce()->getListToExec();
        
         foreach ($list as $dsync) {
           
             $istamplate=$this->istemplate($dsync);
             if(!$istamplate){$this->importexec($dsync);}
             else{
                 foreach ($listmdl as $mdl) {
                      $dsync->setDbsource($mdl);
                      $this->importexec($dsync);
                 }
             }
             
             echo "\n\n =========================================\n\n\n";
             
         }
         
    }

    function importexec($syncdbdata) {
        $this->getDbsourcemconn()->init($syncdbdata, 'source');
        $this->getDbtargetmconn()->init($syncdbdata, 'target');

        //target get lastid
        $paramfilter = $this->paramFilter($syncdbdata);
        $param = array('offset' => 0, 'limit' => 1);

        $lastid = $this->getDbtargetmconn()->selectlastid($param, $paramfilter);
       echo  $syncdbdata->getDbsource()->getName()."  - lastid: $lastid<br>";
        if (empty($lastid)) {
            $lastid = 0;
        }
        //echo "lastid: $lastid";
        //source count record
        $param = array();

        $param['id'] = $lastid;
        $param['limit'] = $this->maxRowImportPerConnection;

        $count = $this->getDbsourcemconn()->count($param);

        $paging = $this->countPaging($count, $this->maxRowImportPerConnection);
        $countimported = 0;
        $offset = 0;

        for ($i = 0; $i < $paging; $i++) {
            $offset = $i * $this->maxRowImportPerConnection;
            if ($offset > 1) {
                $offset = $offset - 1;
            }
            $param['offset'] = $offset;
            $drows = $this->getDbsourcemconn()->select($param);

            if (empty($drows) || sizeof($drows) == 0) {
                break;
            }
            $drows = $this->execServiceBeforeInsert($syncdbdata, $drows);

            $results = $this->getDbtargetmconn()->insert($drows);
            $countimported+=sizeof($drows);
           /* echo "\n count raws: ".sizeof($drows);
            echo "\n countimported: $countimported ";
            echo "\n pagin: $i / $paging";
            echo "\n maxrowimport: $countimported /".$this->maxRowImport;*/
            if ($countimported > $this->maxRowImport) {
               // echo "*********ATINGIU LIMITE***********";
                break;
            }
        }
        echo "\n total import | ".$syncdbdata->getDbsource()->getName()." - ".$syncdbdata->getId()." - ". $syncdbdata->getName().":". $countimported ." . <hr>------";
    }

   

    
    function update() {
        
    }

    function delete() {
        
    }

  

    public function istemplate($dsync,$templatetype='site_moodle') {
        $dbsourceconfig=$dsync->getDbsourceconfig();
        $dbsourceconfig=$this->getJson()->decode($dbsourceconfig, true);
        $template=$this->getUtildata()->getVaueOfArray($dbsourceconfig, 'template');
        $dtype     = $this->getUtildata()->getVaueOfArray($template,'dtype');
        if($dtype=='site_moodle') {return true;}
        return false;
        
    }
    public function countPaging($dbrecord, $limit) {
        $countpaging = $dbrecord / $limit;
        if ($countpaging != round($countpaging)) {
            $countpaging = floor($countpaging) + 1;
        }
        return $countpaging;
    }

    public function execServiceBeforeInsert($syndbdata, $rows) {
        $paramconf = $syndbdata->getDbtargetconfig();
        $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
        $paramconf = $json->decode($paramconf, true);
        $pservice = null;
        if (isset($paramconf['execbeforeinsert'])) {
            $pservice = $paramconf['execbeforeinsert'];
        }

        if (empty($pservice)) {
            return $rows;
        }
        foreach ($pservice as $dservice) {
            $service = null;
            $function = "exec";

            if (isset($dservice['address'])) {
                $service = $dservice['address'];
            }
            if (isset($dservice['function'])) {
                $function = $dservice['function'];
            }
            if (!empty($service) && !empty($function) && $this->getContainer()->has($service)) {
                $service = $this->getContainer()->get($service);
                if (method_exists($service, $function)) {
                    $rows = $service->$function($syndbdata, $rows);
                }
            }
        }

        return $rows;
    }

    public function paramFilter($syndbdata) {
        $paramfilter = array();

        if (empty($syndbdata->getDbsource())) {
            return $paramfilter;
        }
        $modulekey = $syndbdata->getDbsource()->getModulekey();
        $moduleinstance = $syndbdata->getDbsource()->getModuleinstance();

        if ($modulekey == 'badiu.gmoodle.mdl.site') {
            $paramfilter['moodleid'] = (int) $moduleinstance;
        }
        return $paramfilter;
    }

    function getMaxRowImport() {
        return $this->maxRowImport;
    }

    function getMaxRowImportPerConnection() {
        return $this->maxRowImportPerConnection;
    }

    function getMaxIdImport() {
        return $this->maxIdImport;
    }

    function getMaxIdImportPerConnection() {
        return $this->maxIdImportPerConnection;
    }


    function setMaxRowImport($maxRowImport) {
        $this->maxRowImport = $maxRowImport;
    }

    function setMaxRowImportPerConnection($maxRowImportPerConnection) {
        $this->maxRowImportPerConnection = $maxRowImportPerConnection;
    }

    function setMaxIdImport($maxIdImport) {
        $this->maxIdImport = $maxIdImport;
    }

    function setMaxIdImportPerConnection($maxIdImportPerConnection) {
        $this->maxIdImportPerConnection = $maxIdImportPerConnection;
    }

    function getDataserivce() {
        return $this->dataserivce;
    }

    function setDataserivce($dataserivce) {
        $this->dataserivce = $dataserivce;
    }

    function getDbsourcemconn() {
        return $this->dbsourcemconn;
    }

    function getDbtargetmconn() {
        return $this->dbtargetmconn;
    }

    function setDbsourcemconn($dbsourcemconn) {
        $this->dbsourcemconn = $dbsourcemconn;
    }

    function setDbtargetmconn($dbtargetmconn) {
        $this->dbtargetmconn = $dbtargetmconn;
    }

}
