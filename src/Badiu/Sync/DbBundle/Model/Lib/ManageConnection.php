<?php

namespace Badiu\Sync\DbBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
/**
 * Description of DataExec
 *
 * @author lino
 */
class ManageConnection {
    
  /**
     * @var Container
     */
    private $container;

    /**
     * @var string
     */
    private $whichdb; //source | target
    
    /**
     * @var object
     */
    private $syncdbdata;
    

    
    function __construct(Container $container) {
                $this->container=$container;
              
     }
   
     function init($syncdbdata,$whichdb) {
         $this->syncdbdata=$syncdbdata;
         $this->whichdb=$whichdb;
     }
    
   
     
     function maxid() {
         if($this->getSyncdbdata()->getTypesourceconn()=='sqlservice'){
             $sqlservicelib=$this->getContainer()->get('badiu.sync.db.lib.sqlserviceConnection');
             $sqlservicelib->init($this->getSyncdbdata(),$this->getWhichdb());
             $result=$sqlservicelib->maxid();
             return $result;
         }
         
         return null;
     }
     function count($param) {
        $conn=$this->getConnction();
         $result=$conn->count($param);
         return $result;
        
     }
     
     function select($param=array(),$paramfilter=array()) {
         $conn=$this->getConnction();
         $result=$conn->select($param,$paramfilter);
         return $result;
     }
     function  selectlastid($param=array(),$paramfilter=array()) {
		 $conn=$this->getConnction();
         $result=$conn->selectlastid($param,$paramfilter);
         return $result;
     }
       function insert($rows) {
            
             $conn=$this->getConnction();
             $result=$conn->insert($rows);
             
             return $result;
     }
     function delete($paramfilter = null,$sql=null) {
             $conn=$this->getConnction();
             $result=$conn->delete($paramfilter,$sql);
             
             return $result;
     }
     function update() {
         return null;
     }
     
     /* delete
      private function getConnction() {
          $conn=null;
          if($this->getWhichdb()=='source'){
               if($this->getSyncdbdata()->getTypesourceconn()=='sqlservice' || $this->getSyncdbdata()->getTypetargetconn()=='sqlservice'){
                   $sqlservicelib=$this->getContainer()->get('badiu.sync.db.lib.sqlserviceConnection');
                   $sqlservicelib->init($this->getSyncdbdata(),$this->getWhichdb());
                   $conn=$sqlservicelib;
               }
          }else if($this->getWhichdb()=='target'){
             if($this->getSyncdbdata()->getTypesourceconn()=='db' || $this->getSyncdbdata()->getTypetargetconn()=='db'){
                 $dblib=$this->getContainer()->get('badiu.sync.db.lib.dbconnection');
                $dblib->init($this->getSyncdbdata(),$this->getWhichdb());
                $conn=$dblib;
             }
          }
          
        
         return $conn;
      }*/
    /*
     private function getConnction() {
          $conn=null;
          if($this->getSyncdbdata()->getTypesourceconn()=='sqlservice' || $this->getSyncdbdata()->getTypetargetconn()=='sqlservice'){
                   $sqlservicelib=$this->getContainer()->get('badiu.sync.db.lib.sqlserviceConnection');
                   $sqlservicelib->init($this->getSyncdbdata(),$this->getWhichdb());
                   $conn=$sqlservicelib;
           }else if($this->getSyncdbdata()->getTypesourceconn()=='db' || $this->getSyncdbdata()->getTypetargetconn()=='db'){
                  $dblib=$this->getContainer()->get('badiu.sync.db.lib.dbconnection');
                  $dblib->init($this->getSyncdbdata(),$this->getWhichdb());
                  $conn=$dblib;
             }
               
          
        
         return $conn;
     }*/
    private function getConnction() {
            $conn=null;
			
            if($this->getWhichdb()=='source'){
                if($this->getSyncdbdata()->getTypesourceconn()=='sqlservice'){
                     $conn=$this->getContainer()->get('badiu.sync.db.lib.sqlserviceConnection');
                     $conn->init($this->getSyncdbdata(),$this->getWhichdb());
                }else if($this->getSyncdbdata()->getTypesourceconn()=='db'){
                    $conn=$this->getContainer()->get('badiu.sync.db.lib.dbconnection');
                    $conn->init($this->getSyncdbdata(),$this->getWhichdb());
                }
            }else if($this->getWhichdb()=='target'){
                if($this->getSyncdbdata()->getTypetargetconn()=='sqlservice'){
                     $conn=$this->getContainer()->get('badiu.sync.db.lib.sqlserviceConnection');
                     $conn->init($this->getSyncdbdata(),$this->getWhichdb());
                }else if ($this->getSyncdbdata()->getTypetargetconn()=='db'){
					$conn=$this->getContainer()->get('badiu.sync.db.lib.dbconnection');
                    $conn->init($this->getSyncdbdata(),$this->getWhichdb());
                }
            }
          return $conn;
      }
   
     function getContainer() {
         return $this->container;
     }

     function getWhichdb() {
         return $this->whichdb;
     }

     function getSyncdbdata() {
         return $this->syncdbdata;
     }

     function setContainer(Container $container) {
         $this->container = $container;
     }

     function setWhichdb($whichdb) {
         $this->whichdb = $whichdb;
     }

     function setSyncdbdata($syncdbdata) {
         $this->syncdbdata = $syncdbdata;
     }

  


}
