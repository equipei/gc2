<?php

namespace Badiu\Sync\DbBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

/**
 * Description of DataExec
 *
 * @author lino
 */
class DataExec extends BadiuModelLib {

    /**
     * @var integer
     */
    private $maxRowImport;

    /**
     * @var integer
     */
    private $maxRowImportPerConnection;

    /**
     * @var integer
     */
    private $maxIdImport;

    /**
     * @var integer
     */
    private $maxIdImportPerConnection;

    /**
     * @var object
     */
    private $dbsourcemconn;

    /**
     * @var object
     */
    private $dbtargetmconn;

    function __construct(Container $container) {
        parent::__construct($container);

        $this->maxRowImport = 5500000;
        $this->maxRowImportPerConnection = 500;
        $this->maxIdImport = 100000;
        $this->maxIdImportPerConnection = 10000;


        $this->dbsourcemconn = $this->getContainer()->get('badiu.sync.db.lib.manageconnection');
        $this->dbtargetmconn = $this->getContainer()->get('badiu.sync.db.lib.manageconnection');
    }

    function import($dsync) {
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $entity = $badiuSession->get()->getEntity();

		$presult=array();

        $istamplate = $this->isTemplate($dsync);
        if (!$istamplate) {
          $presult= $this->importexec($dsync);
        } else {

            //list server service moodle
            $sdata = $this->getContainer()->get('badiu.admin.server.service.data');
            $param = array('dtype' => 'site_moodle', 'syncdb' => true);
            $listmdl = $sdata->findByParam($entity, $param);
            foreach ($listmdl as $mdl) {
                $dsync->setDbsource($mdl);
                $this->importExec($dsync);
            }
        }
		return $presult;
        //echo "\n\n =========================================\n\n\n";
    }

    function importExec($dsync) {
		$presult=array();
        $isparent = $this->isParentList($dsync);

        if ($isparent) {
            $dbsourceconfig = $dsync->getDbsourceconfig();
            $dbsourceconfig = $this->getJson()->decode($dbsourceconfig, true);
            $parentlist = $this->getUtildata()->getVaueOfArray($dbsourceconfig, 'parentlist');
            $service = $this->getUtildata()->getVaueOfArray($parentlist, 'service');

            $cfunction = $this->getUtildata()->getVaueOfArray($parentlist, 'function');
            $cfieldkeytogetvalue = $this->getUtildata()->getVaueOfArray($parentlist, 'fieldkeytogetvalue');
            $cfieldkeytosetvalue = $this->getUtildata()->getVaueOfArray($parentlist, 'fieldkeytosetvalue');

            $function = 'exec';
            $fieldkeytogetvalue = 'id';
            $fieldkeytosetvalue = 'id';
            $listparent = null;
            if (!empty($cfunction)) {
                $function = $cfunction;
            }
            if (!empty($cfieldkeytogetvalue)) {
                $fieldkeytogetvalue = $cfieldkeytogetvalue;
            }
            if (!empty($cfieldkeytosetvalue)) {
                $fieldkeytosetvalue = $cfieldkeytosetvalue;
            }

            if (!empty($service) && !empty($function) && $this->getContainer()->has($service)) {
                $service = $this->getContainer()->get($service);
                if (method_exists($service, $function)) {
                    $listparent = $service->$function();
                }
            }
       
            if (!is_array($listparent)) {
                return null;
            } 
            foreach ($listparent as $plist) {
                $parentid = $this->getUtildata()->getVaueOfArray($plist, $fieldkeytogetvalue);
                //echo "$parentid | $fieldkeytosetvalue <br>";
                $this->importExecParentList($dsync, $parentid, $fieldkeytosetvalue); 
            }
        } else {
           $presult= $this->importExecDefault($dsync);
        }
		return $presult;
    }

    function importExecParentList($syncdbdata, $parentid, $fieldkeytosetvalue) {
        $this->getDbsourcemconn()->init($syncdbdata, 'source');
        $this->getDbtargetmconn()->init($syncdbdata, 'target');



        //process parent list
        //target get lastid
        $paramfilter = $this->paramFilter($syncdbdata);
        $paramfilter[$fieldkeytosetvalue]=$parentid;
       
         //echo "\n ".$syncdbdata->getName(). " - moodleid: $parentid | ";
        
        $param = array();
        $changelist = false;
        $replaynewlist = 0;
        $countdelete=$this->execSqlOnStartProcess($syncdbdata, $paramfilter);
       // echo "rows deleted: $countdelete | ";
        $lastid = $this->getDbtargetmconn()->selectlastid(array(), $paramfilter);

       

        if (empty($lastid)) {
            $lastid = 0;
        }
        //echo "lastid: $lastid";
        //source count record
        $param = array();

        $param['id'] = $lastid;
        $param['limit'] = $this->maxRowImportPerConnection;
        $param['offset'] = 0;
        //$count = $this->getDbsourcemconn()->count($param);
        // $count =$this->getMaxRowImport();
        // $paging = $this->countPaging($count, $this->maxRowImportPerConnection);
        $paging = $this->countPaging();

        $datashouldexec = 0;
        $dataexec = 0;
       
        $cont = 0;

        for ($i = 0; $i < $paging; $i++) {

           
            
            if ($cont > 0 && empty($lastid)) {
                $lastid = $this->getDbtargetmconn()->selectlastid(array(), $paramfilter);
              //  echo "***new db select***";
            }
           // echo "\n paging: $paging | i: $i | lastid: $lastid | replay: $replaynewlist";
         
            $param['id'] = $lastid+0;
            $param[$fieldkeytosetvalue]=$parentid;
            $drows = $this->getDbsourcemconn()->select($param);
        
         
           
            $drows = $this->execServiceBeforeInsert($syncdbdata, $drows);
           
           
            $sizeoflist = sizeof($drows);
           // echo "| total: $sizeoflist \n";
            //aupdate lastid
            if ($sizeoflist > 0) {
                 $lastid = $this->getLastidinlist($syncdbdata, $drows);
                $drowsinsert = $this->generateSqlBeforeInsert($syncdbdata, $drows);
                $result = $this->getDbtargetmconn()->insert($drowsinsert);
                $dataexec += $result;
                $replaynewlist = 0;
            } else {
                if ($changelist) {
                    $replaynewlist++;
                };
            }

            // echo "resutl insert : $results  lastid $lastid \n" ;

            $datashouldexec += $sizeoflist;
             if ($datashouldexec > $this->maxRowImport) {
                break;
            }
            if ($sizeoflist == 0 && !$changelist) {
                break;
            }
            if ($sizeoflist == 0 && $changelist && $replaynewlist == 20) {
                break;
            }

            $cont++;
        }
       // echo "datashouldexec :$datashouldexec | dataexec: $dataexec<hr>";
        //echo " oper. $syncdbdata->getName() . | " . $syncdbdata->getDbsource()->getName() . " - " . $syncdbdata->getId() . " - " . $syncdbdata->getName() . ":$datashouldexec de $dataexec<hr>------";
    }

     // need to teste
    function importExecDefault($syncdbdata) {
        $this->maxRowImport = 100000;
        $this->maxRowImportPerConnection = 2500;
        $this->getDbsourcemconn()->init($syncdbdata, 'source');
        $this->getDbtargetmconn()->init($syncdbdata, 'target');
        //target get lastid
        $paramfilter = $this->paramFilter($syncdbdata);

        $param = array();
       
        $countdelete=$this->execSqlOnStartProcess($syncdbdata, $paramfilter);
        $lastid = $this->getDbtargetmconn()->selectlastid(array(), $paramfilter);
       
       // echo $syncdbdata->getDbsource()->getName() . "  - lastid: $lastid<br>"; 

        if (empty($lastid)) {
            $lastid = 0;
        }

        $param['id'] = $lastid;
        $param['limit'] = $this->maxRowImportPerConnection;
        $param['offset'] = 0;
        $paging = $this->countPaging();
        $datashouldexec = 0;
        $dataexec = 0;
        $cont = 0;

        for ($i = 0; $i < $paging; $i++) {
             if ($cont > 0 && empty($lastid)) {
                $lastid = $this->getDbtargetmconn()->selectlastid(array(), $paramfilter);
            }
            $param['id'] = $lastid;
			
            $drows = $this->getDbsourcemconn()->select($param);
           
            $drows = $this->execServiceBeforeInsert($syncdbdata, $drows);
           /*echo "<pre>";
		   print_r($drows);
		   echo "</pre>";exit;*/
            $sizeoflist = sizeof($drows);

            //aupdate lastid
            if ($sizeoflist > 0) {
                 $lastid = $this->getLastidinlist($syncdbdata, $drows);
                $drowsinsert = $this->generateSqlBeforeInsert($syncdbdata, $drows);
                $result = $this->getDbtargetmconn()->insert($drowsinsert);
                $dataexec += $result;
               
            } 
            $datashouldexec += $sizeoflist;
            if ($datashouldexec > $this->maxRowImport) {
                break;
            }
            
            if ($sizeoflist == 0) {
                break;
            }

            $cont++;
        }
		$message=  $syncdbdata->getDbsource()->getName() . " - " . $syncdbdata->getId() . " - " . $syncdbdata->getName() ;
		$resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
		
       // echo "\n total import | " . $syncdbdata->getDbsource()->getName() . " - " . $syncdbdata->getId() . " - " . $syncdbdata->getName() . ":$datashouldexec de $dataexec<hr>------";
	  return $resultmdl; 
    }
  
     

    function update() {
        
    }

    function delete() {
        
    }

    public function isTemplate($dsync, $templatetype = 'site_moodle') {
        $dbsourceconfig = $dsync->getDbsourceconfig();
        $dbsourceconfig = $this->getJson()->decode($dbsourceconfig, true);
        $template = $this->getUtildata()->getVaueOfArray($dbsourceconfig, 'template');
        $dtype = $this->getUtildata()->getVaueOfArray($template, 'dtype');
        if ($dtype == 'site_moodle') {
            return true;
        }
        return false;
    }

    public function isParentList($dsync) {
        $dbsourceconfig = $dsync->getDbsourceconfig();
        $dbsourceconfig = $this->getJson()->decode($dbsourceconfig, true);
        $parentlist = $this->getUtildata()->getVaueOfArray($dbsourceconfig, 'parentlist');
        $service = $this->getUtildata()->getVaueOfArray($parentlist, 'service');
        if (!empty($service)) {
            return true;
        }
        return false;
    }

    public function countPaging($dbrecord = null, $limit = null) {
        if ($dbrecord == null) {
            $dbrecord = $this->maxRowImport;
        }
        if ($limit == null) {
            $limit = $this->maxRowImportPerConnection;
        }
        $countpaging = $dbrecord / $limit;
        if ($countpaging != round($countpaging)) {
            $countpaging = floor($countpaging) + 1;
        }
        return $countpaging;
    }

    public function execServiceBeforeInsert($syndbdata, $rows) {
        $paramconf = $syndbdata->getDbtargetconfig();
        $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
        $paramconf = $json->decode($paramconf, true);
        $pservice = null;
		
        if (isset($paramconf['execbeforeinsert'])) {
            $pservice = $paramconf['execbeforeinsert'];
        }

        if (empty($pservice)) {
			 return $rows;
        }
		
        foreach ($pservice as $dservice) {
            $service = null;
			$dservice['__dbsourseid']=$syndbdata->getDbsource()->getId();
			$dservice['__dbtargetid']=$syndbdata->getDbtarget()->getId();
			$dservice['__entity']=$syndbdata->getDbtarget()->getEntity();
		
            $function = "exec";
			$sparam=$this->getUtildata()->getVaueOfArray($dservice,'param');
			
            if (isset($dservice['address'])) {
                $service = $dservice['address'];
            }
            if (isset($dservice['function'])) {
                $function = $dservice['function'];
            }
            if (!empty($service) && !empty($function) && $this->getContainer()->has($service)) {
                $service = $this->getContainer()->get($service);
				if(!empty($sparam) && method_exists($service, 'setParam')){$service->setParam($dservice);}
                if (method_exists($service, $function)) {
                    $rows = $service->$function($syndbdata, $rows);
                }
            }
        } 

        return $rows;
    }

    public function execSqlOnStartProcess($syndbdata, $paramfilter) {
        $result=0;
        $paramconf = $syndbdata->getDbtargetconfig();
        $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
        $paramconf = $json->decode($paramconf, true);
        $sqlexeclist = null;
        if (isset($paramconf['execsqlonstartprocess'])) {
            $sqlexeclist = $paramconf['execsqlonstartprocess'];
        }

        if (empty($sqlexeclist)) {
            return null;
        }
        foreach ($sqlexeclist as $itemsql) {
            $sql = null;
           

            if (isset($itemsql['command'])) {
                $sql = $itemsql['command'];
               if(!empty($sql)){
                   $result=$this->getDbtargetmconn()->delete($paramfilter,$sql);
               }
                
            }
           
        }

        return $result;
    }
    
    public function generateSqlBeforeInsert($syndbdata, $rows) {
        $paramconf = $syndbdata->getDbtargetconfig();
        $paramconf = $this->getJson()->decode($paramconf, true);
        $createautomaticsql = $this->getUtildata()->getVaueOfArray($paramconf, 'createautomaticsql');
        $table = $this->getUtildata()->getVaueOfArray($paramconf, 'table');

        if ($createautomaticsql == 1 && !empty($table)) {
            $factorysqlcommand = $this->getContainer()->get('badiu.system.core.lib.sql.factorysqlcommand');
            $rows = $factorysqlcommand->batchInsert($table, $rows);
        }

        return $rows;
    }

    public function mappinddata($syndbdata, $rows, $witchdb) {
        $paramconf = null;
        if ($witchdb == 'target') {
            $paramconf = $syndbdata->getDbtargetconfig();
        } else if ($witchdb == 'source') {
            $paramconf = $syndbdata->getDbsourceconfig();
        }

        $paramconf = $this->getJson()->decode($paramconf, true);
        $mappingdataconf = $this->getUtildata()->getVaueOfArray($paramconf, 'mappingdata');


        if (!empty($mappingdataconf) && is_array($mappingdataconf) && sizeof($mappingdataconf) > 0) {

            $newlist = array();
            foreach ($rows as $row) {

                $rowmapped = array();
                foreach ($mappingdataconf as $key => $value) {

                    $rowmapped[$key] = $this->getUtildata()->getVaueOfArray($row, $value, true);
                }
                array_push($newlist, $rowmapped);
            }
            return $newlist;
        } else {
            return $rows;
        }


        return $rows;
    }

    public function getColumnsyncpaging($syndbdata) {
        $keypaging = 'id';
        $paramconf = null;
        $paramconf = $syndbdata->getDbtargetconfig();
        $paramconf = $this->getJson()->decode($paramconf, true);
        $confkey = $this->getUtildata()->getVaueOfArray($paramconf, 'columnsyncpaging');
        if (!empty($confkey)) {
            $keypaging = $confkey;
        }
        return $keypaging;
    }

    public function getLastidinlist($syndbdata, $rows) {
   
        $keypaging = $this->getColumnsyncpaging($syndbdata);
        $lastraw = end($rows);
        $lastid = $this->getUtildata()->getVaueOfArray($lastraw, $keypaging);
        if (empty($lastid)) {
            $lastid = 0;
        }
        return $lastid;
    }

    public function paramFilter($syndbdata) {
        $paramfilter = array();

        if (empty($syndbdata->getDbsource())) {
            return $paramfilter;
        }
        $modulekey = $syndbdata->getDbsource()->getModulekey();
        $moduleinstance = $syndbdata->getDbsource()->getModuleinstance();

        if ($modulekey == 'badiu.gmoodle.mdl.site') {
            $paramfilter['moodleid'] = (int) $moduleinstance;
        }
		$paramfilter['entity']=$syndbdata->getDbtarget()->getEntity();
		$paramfilter['dbtargetid']=$syndbdata->getDbtarget()->getId();
		$paramfilter['dbsourseid']=$syndbdata->getDbsource()->getId();
		
        return $paramfilter;
    }

    function getMaxRowImport() {
        return $this->maxRowImport;
    }

    function getMaxRowImportPerConnection() {
        return $this->maxRowImportPerConnection;
    }

    function getMaxIdImport() {
        return $this->maxIdImport;
    }

    function getMaxIdImportPerConnection() {
        return $this->maxIdImportPerConnection;
    }

    function setMaxRowImport($maxRowImport) {
        $this->maxRowImport = $maxRowImport;
    }

    function setMaxRowImportPerConnection($maxRowImportPerConnection) {
        $this->maxRowImportPerConnection = $maxRowImportPerConnection;
    }

    function setMaxIdImport($maxIdImport) {
        $this->maxIdImport = $maxIdImport;
    }

    function setMaxIdImportPerConnection($maxIdImportPerConnection) {
        $this->maxIdImportPerConnection = $maxIdImportPerConnection;
    }

    function getDbsourcemconn() {
        return $this->dbsourcemconn;
    }

    function getDbtargetmconn() {
        return $this->dbtargetmconn;
    }

    function setDbsourcemconn($dbsourcemconn) {
        $this->dbsourcemconn = $dbsourcemconn;
    }

    function setDbtargetmconn($dbtargetmconn) {
        $this->dbtargetmconn = $dbtargetmconn;
    }

}
