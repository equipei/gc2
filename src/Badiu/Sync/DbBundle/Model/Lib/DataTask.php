<?php

namespace Badiu\Sync\DbBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

class DataTask  extends  TaskGeneralExec{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
				$this->cresult=array('datashouldexec' => 0, 'dataexec' => 0);
        } 

  public function import() {
	  $this->process();
	   return $this->cresult;
  }
  public function update() {
    $this->process();
    return $this->cresult;
}

  public function process() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		$id=$this->getUtildata()->getVaueOfArray($this->getParam(),'id');
		$entity=$this->getUtildata()->getVaueOfArray($this->getParam(),'entity');
		$force=$this->getUtildata()->getVaueOfArray($this->getParam(),'force');
		if(empty($operation)){$operation='import';}
		if(empty($entity)){$entity=null;}
		
        $dataexec=$this->getContainer()->get('badiu.sync.db.lib.dataexec');
        $data=$this->getContainer()->get('badiu.sync.db.data.data');
					$list=array();
					$cont=0;
                       if(!empty($id)){
                           $dto=$data->findById($id);
                           array_push($list,$dto);
                       }else{
                           $list=$data->getEnabled($entity);
                            $force=0;
                       }
                        
                        
                        foreach ($list as $dsync) {
                       
                            //review
                            if($operation=='import'){
								$resultmdl= $dataexec->import($dsync);
								$this->cresult = $this->addResult($this->cresult, $resultmdl); 
								$cont++;
                            }
							
                             
                            
                        }
						
    
	
  return $cont;
}


public function updateRecentDataFromLms() {
	
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
	
		if($operation!='syncrecentdata'){return null;}
	
		
		$itemtypes=$this->getUtildata()->getVaueOfArray($this->getParam(),'itemtypes');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		$lasttime=$this->getUtildata()->getVaueOfArray($this->getParam(),'lasttime');
		
		$execafterservice=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.service',true);
		//$execafterfunction=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.function',true);
		$execafterdefaultroles=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.defaultroles',true);
		$execafterroles=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.roles',true);
		
		if(empty($itemtypes)){$itemtypes=array('courseaccess','coursefinalgrade','courseprogress','coursecompletation');}
		if(empty($maxrecord)){$maxrecord=50;}
		if(empty($lasttime)){$lasttime=300;}
		
		$listlms=$this->getLmsEnabled($this->getParam());
		
		 if(!is_array($listlms)){return  null;}
		 $lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesyncrecentdata');
		 $count=0;
		 
		 $listrecoredprocessed=array();
		  foreach ($listlms as $lrow) {
			  $sserviceid =$this->getUtildata()->getVaueOfArray($lrow,'sserviceid');
			  $fparam=array('lasttime'=>$lasttime,'sserviceid'=> $sserviceid);
			 
			  //courseaccess
			  if (in_array("courseaccess",$itemtypes)){
				  $lmsresult=$lmsmoodlesync->importLastaccessCourseFromMoodle($fparam);
				  $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
				  $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			      $message=null;
			      $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				  $this->cresult = $this->addResult($this->cresult, $resultmdl); 
				  
				  $rpd=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed');
				  $rpkey=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed.key',true);
				  $listrecoredprocessed[$rpkey]=$rpd;
				 
			 }
			  
			  if (in_array("coursefinalgrade",$itemtypes)){
				  $lmsresult=$lmsmoodlesync->importGradesFromMoodle($fparam);
				  $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
				  $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			      $message=null;
			      $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				  $this->cresult = $this->addResult($this->cresult, $resultmdl); 
				  
				  $rpd=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed');
				  $rpkey=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed.key',true);
				  $listrecoredprocessed[$rpkey]=$rpd;
			 }
			 if (in_array("coursecompletation",$itemtypes)){
				  $lmsresult=$lmsmoodlesync->importCourseEnrolCompletationFromMoodle($fparam);
				  $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
				  $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			      $message=null;
			      $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				  $this->cresult = $this->addResult($this->cresult, $resultmdl); 
				  
				  $rpd=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed');
				  $rpkey=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed.key',true);
				  $listrecoredprocessed[$rpkey]=$rpd;
			 }
			if (in_array("courseprogress",$itemtypes)){
				  $lmsresult=$lmsmoodlesync->importCourseEnrolProgressFromMoodle($fparam);
				  $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
				  $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			      $message=null;
			      $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				  $this->cresult = $this->addResult($this->cresult, $resultmdl);

				  $rpd=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed');
				  $rpkey=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed.key',true);
				  $listrecoredprocessed[$rpkey]=$rpd;				  
			 }
			
			  $count++;
		  }
		
		  $sparam=array('defaultroles'=>$execafterdefaultroles,'roles'=>$execafterroles);
		  //exec string service/function
		 
		   foreach ($listrecoredprocessed as $prow) {
			   if(!empty($prow) && sizeof($prow) >0){
			    $classeid=$this->getUtildata()->getVaueOfArray($prow,'classeid');
				$userid=$this->getUtildata()->getVaueOfArray($prow,'userid');
			    $sparam['classeid']=$classeid;
				$sparam['userid']=$userid;
			 
			     $spresult=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($execafterservice,$sparam);
				 if(is_array($spresult)){
					foreach ($spresult as $prrow) { $this->addResult($this->cresult,$prrow); }
				}
			  }
		   }
		 
		 
		  return $count;
	}

}
?>