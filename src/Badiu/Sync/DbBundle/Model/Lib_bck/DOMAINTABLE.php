<?php
namespace Badiu\Sync\DbBundle\Model\Lib;
class DOMAINTABLE {
	//TYPE CONNECTION
	
    public static  $TYPE_DB_CONNECTION="db";
	public static  $TYPE_SQLSERVICE_CONNECTION="sqlservice";
	
  // DATA DBOPERATION_
    public static  $DB_SELECT_FROM_SOURCE   ="db_select_from_soure";
    public static  $DB_SELECT_FROM_TARGET   ="db_select_from_target";
    public static  $DB_UPDATE_TARGET        ="db_update_target";
    public static  $DB_DELETE_TARGET        ="db_delete_target";

    // DATA TYPE
    public static  $DATA_TYPE_INTEGER	="integer";
    public static  $DATA_TYPE_DOUBLE	="double";
    public static  $DATA_TYPE_VARCHAR	="varchar";
    public static  $DATA_TYPE_TEXT	="text";
    public static  $DATA_TYPE_DATE      ="date";
    public static  $DATA_TYPE_DATE_TIME  ="date_time";
    
     // TABLE DATA TYPE
    public static  $TABLE_DATA_TIYPE_DEFAULT   ="default";
    public static  $TABLE_DATA_TIYPE_SQL   ="sql";
  
    //INSERT TYPE
    public static  $INSERT_TYPE_FULL_VALUE_FROM_SELECTED_COLUMN          ="insert_full_value_from_selected_column";
    public static  $INSERT_TYPE_PARTIAL_VALUE_FROM_SELECTED_COLUMN	="insert_partial_value_from_selected_column";
    public static  $INSERT_TYPE_COMMAND_VALUE                            ="insert_command_value";
    public static  $INSERT_TYPE_DEFULT_VALUE                             ="insert_default_value";
    
    //PARAM PARTIAL VALUE
    public static  $PARAM_PARTIAL_COLUM_SOURCE                     ="partial_value_column_source";
    public static  $PARAM_PARTIAL_VALUE_NUMBER_WORD_LEFT           ="partial_value_number_word_left";
    public static  $PARAM_PARTIAL_VALUE_WITHOUT_NUMBER_WORD_LEFT   ="partial_value_without_number_word_left";
    
    //PARAM COMMAND VALUE
    public static  $PARAM_COMMAND_VALUE                         ="command_value";
    
   //PARAM DEFAULT VALUE
    public static  $PARAM_OPERATION_ON_DELETE                   ="operation_on_delete"; //values: update_field | exec_symfony_service
    public static  $PARAM_OPERATION_ON_DELETE_PK_LIST_EXCEPTION  ="operation_on_delete_pk_list_exception"; //values: update_field
    
    public static  $PARAM_SYMFONY_SERVICE_EXEC_ON_DELETE        ="symfomy_service_exec_on_delete"; //values: especificar url do servico
    
    public static  $PARAM_DEFAULT_VALUES_ON_DELETE              ="default_value_on_delete";
    
    
    
    //PASSWORD CONF
    public static  $PARAM_PASSWORD_CRYPT                     ="password_crypt"; //values: md5 |

     //PARA SQL  FILTER
    public static  $PARAM_SOURCE_SQL_FILTER_ON_SELECT           ="param_source_sql_filter_on_select"; //
    public static  $PARAM_TARGET_SQL_FILTER_ON_SELECT           ="param_target_sql_filter_on_select"; //
    
    public static  $PARAM_TARGET_SQL_FILTER_ON_DELETE           ="param_target_sql_filter_on_delete"; //
    public static  $PARAM_TARGET_SQL_FILTER_ON_UPDATE           ="param_target_sql_filter_on_update";

   //sql filter expression
    public static  $EXPRESSION_SQL_FILTER_DATETIME_LAST_X_DAYS  ="{datetime_last_x_days}";


    //PROCESS SERVICE BEFORE INSERT ALL ROWS
    public static  $PARAM_SYMFONY_SERVICE_EXEC_BEFORE_INSERT_ALL_ROWS           ="symfomy_service_exec_before_insert_all_rows";
    public static  $PARAM_SYMFONY_SERVICE_EXEC_BEFORE_UPDATE_ALL_ROWS           ="symfomy_service_exec_before_update_all_rows";

    //PROCESS SERVICE AFTER INSERT ALL ROWS
    public static  $PARAM_SYMFONY_SERVICE_EXEC_AFTER_INSERT_ALL_ROWS           ="symfomy_service_exec_after_insert_all_rows";
    public static  $PARAM_SYMFONY_SERVICE_EXEC_AFTER_UPDATE_ALL_ROWS           ="symfomy_service_exec_after_update_all_rows";

    //PROCESS SERVICE BEFORE INSERT EACH ROW
    public static  $PARAM_SYMFONY_SERVICE_EXEC_BEFORE_INSERT_EACH_ROW           ="symfomy_service_exec_before_insert_each_row";
    public static  $PARAM_SYMFONY_SERVICE_EXEC_BEFORE_UPDATE_EACH_ROW           ="symfomy_service_exec_before_update_each_row";

    //PROCESS SERVICE AFTER INSERT EACH ROW
    public static  $PARAM_SYMFONY_SERVICE_EXEC_AFTER_INSERT_EACH_ROW           ="symfomy_service_exec_after_insert_each_row";
    public static  $PARAM_SYMFONY_SERVICE_EXEC_AFTER_UPDATE_EACH_ROW           ="symfomy_service_exec_after_update_each_row";

}