<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\Sync\DbBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;
/**
 * Description of MakeSelect
 *
 * @author lino
 */
class SyncDbUtil {
    
  /**
     * @var Container
     */
    private $container;

      /**
     * @var Translator
     */
    private $translator;
    
    function __construct(Container $container) {
                $this->container=$container;
                $this->translator=$this->container->get('translator');
      }
     public function getDataBaseOptions() {
         $list=array();
         $list['mysql']=$this->getTranslator()->trans('mysql');
         $list['pgsql']=$this->getTranslator()->trans('pgsql');
         $list['mssql']=$this->getTranslator()->trans('mssql');
         $list['oracle']=$this->getTranslator()->trans('oracle');
         return $list;
     } 
    
      public function getTypConnectionOptions() {
         $list=array();
         $list['db']=$this->getTranslator()->trans('db');
         $list['synchttp']=$this->getTranslator()->trans('synchttp');
         $list['symfonyservice']=$this->getTranslator()->trans('symfonyservice');
         return $list;
     } 
    
     public function getSyncOperationOptions() {
         $list=array();
         $list['_import']=$this->getTranslator()->trans('import');
         $list['_update']=$this->getTranslator()->trans('update');
         $list['_delete']=$this->getTranslator()->trans('delete');
         
         $list['_import_update']=$this->getTranslator()->trans('import.update');
         $list['_import_delete']=$this->getTranslator()->trans('import.delete');
         $list['_update_delete']=$this->getTranslator()->trans('update.delete');
         $list['_import_update_delete']=$this->getTranslator()->trans('import.update.delete');
        return $list;
     } 
     
      public function getDataTypeOptions() {
         $list=array();
         $list['insert_full_value_from_selected_column']=$this->getTranslator()->trans('insert_full_value_from_selected_column');
         $list['insert_partial_value_from_selected_column']=$this->getTranslator()->trans('insert_partial_value_from_selected_column');
         $list['insert_default_value']=$this->getTranslator()->trans('insert_default_value');
         $list['insert_command_value']=$this->getTranslator()->trans('insert_command_value');
         return $list;
     } 
     public function getInserType() {
         $list=array();
         $list['varchar']=$this->getTranslator()->trans('varchar');
         $list['text']=$this->getTranslator()->trans('text');
         $list['integer']=$this->getTranslator()->trans('integer');
         $list['datetime']=$this->getTranslator()->trans('datetime');
         return $list;
     } 
     public function getContainer() {
         return $this->container;
     }

     public function setContainer(Container $container) {
         $this->container = $container;
     }

     public function getTranslator() {
         return $this->translator;
     }

     public function setTranslator(Translator $translator) {
         $this->translator = $translator;
     }


         
}
