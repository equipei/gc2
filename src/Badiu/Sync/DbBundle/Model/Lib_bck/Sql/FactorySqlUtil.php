<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\Sync\DbBundle\Model\Lib\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Sync\DbBundle\Model\Lib\DOMAINTABLE;

/**
 * Description of FactorySqlUtil
 *
 * @author lino
 */
class FactorySqlUtil {
    
      /**
     * @var Container
     */
     private $container;
     
     
      function __construct(Container $container) {
                $this->container=$container;
     }
           
   public   function getColumn($row,$peration='select'){
        $columns="";
       if($peration=='select'){
           $columns=$row->getName();
           $alias=$row->getNamealias();
           if(!empty($alias)){
               $columns.=" AS ".$alias;
           }
          
       }else  if($peration=='insert'){
           $columns=$row->getName();
       }  
      else  if($peration=='update'){
           $columns=$row->getName();
      }
      return  $columns;
   }
   
    public   function getColumns($columns,$peration='select'){
        $txtcolumns="";
        if(!empty($columns)){
            $cont=0;
             foreach ($columns  as $col) {
                 if($peration=='select'){
                     if($cont==0){$txtcolumns.=$this->getColumn($col,$peration);}
                     else{$txtcolumns.=", ".$this->getColumn($col,$peration);} 
                    $cont++;
                 }else if($peration=='insert' && strpos($col->getSyncoperation(),'import')>0){
                     if($cont==0){$txtcolumns.=$this->getColumn($col,$peration);}
                     else{$txtcolumns.=", ".$this->getColumn($col,$peration);} 
                    $cont++;
                 }else if($peration=='update' && strpos($col->getSyncoperation(),'update')>0){
                      if($cont==0){$txtcolumns.=$this->getColumn($col,$peration);}
                     else{$txtcolumns.=", ".$this->getColumn($col,$peration);} 
                    $cont++;
                 }
                 
              
             }
        }
        return  $txtcolumns;
    }
    
    public   function getValueType($value,$type){
        $sqlvalue="";
        if($type==DOMAINTABLE::$DATA_TYPE_VARCHAR || $type==DOMAINTABLE::$DATA_TYPE_TEXT ){
            if($value!="" && $value!=null){
                $sqlvalue="'".addslashes($value)."'";
            }else{
                $sqlvalue="NULL";
            }
        }else  if($type==DOMAINTABLE::$DATA_TYPE_INTEGER || $type==DOMAINTABLE::$DATA_TYPE_DOUBLE ){
            if($value!="" && $value!=null){
                $sqlvalue=$value;
            }else{
                $sqlvalue="NULL";
            }
        }
        
        return $sqlvalue;
    }
     public   function getValue($data,$row,$peration='insert'){
          $sqlvalue="";
        if($peration=='insert'){
            //insert_full_value_from_selected_column
            if($row->getInsertype()==DOMAINTABLE::$INSERT_TYPE_FULL_VALUE_FROM_SELECTED_COLUMN){
                if(isset($data[$row->getName()])){
                    $value=$data[$row->getName()];
                   $sqlvalue= $this->getValueType($value,$row->getDatatype());
                }
            }
            //insert_partial_value_from_selected_column   
             if($row->getInsertype()==DOMAINTABLE::$INSERT_TYPE_PARTIAL_VALUE_FROM_SELECTED_COLUMN){
                 $sqlvalue=$this->getPartialValue($data,$row);
             }
                    
            //insert_default_value  
             if($row->getInsertype()==DOMAINTABLE::$INSERT_TYPE_DEFULT_VALUE){
                 $sqlvalue=$this->getValueType($row->getInsertdefaultvalue(),$row->getDatatype());
             }
             //insert_command_value
             if($row->getInsertype()==DOMAINTABLE::$INSERT_TYPE_COMMAND_VALUE){
                 $value=$this->getCommadValue($row);
                 $sqlvalue=$this->getValueType($value,$row->getDatatype());
             }
        }  
        else  if($peration=='update'){
            //insert_full_value_from_selected_column
             if($row->getInsertype()==DOMAINTABLE::$INSERT_TYPE_FULL_VALUE_FROM_SELECTED_COLUMN){
                 $value=$data[$row->getName()];
                 $value= $this->getValueType($value,$row->getDatatype());
                 $sqlvalue=  $row->getName()." = $value ";
             }
            //insert_partial_value_from_selected_column   
             if($row->getInsertype()==DOMAINTABLE::$INSERT_TYPE_PARTIAL_VALUE_FROM_SELECTED_COLUMN){
                 $value=$this->getPartialValue($data,$row);
                 $sqlvalue=  $row->getName()." = $value ";
             }
              //insert_default_value  
             if($row->getInsertype()==DOMAINTABLE::$INSERT_TYPE_DEFULT_VALUE){
                 $value=$this->getValueType($row->getInsertdefaultvalue(),$row->getDatatype());
                 $sqlvalue=  $row->getName()." = $value ";
             }
             
             //insert_command_value
             if($row->getInsertype()==DOMAINTABLE::$INSERT_TYPE_COMMAND_VALUE){
                 $value=$this->getCommadValue($row);
                 $value=$this->getValueType($value,$row->getDatatype());
                 $sqlvalue=  $row->getName()." = $value ";
             }
        }  
        return $sqlvalue;
     }
    public   function getValues($data,$columns,$peration='insert'){
        $values="";
         if(empty($columns)) {return  $values;}
        if($peration=='insert'){
            $cont=0;
             foreach ($columns  as $col) {
                if(strpos($col->getSyncoperation(),'import')>0){
                    if($cont==0){
                        $values=$this->getValue($data,$col);
                    } else{
                        $values.=", ".$this->getValue($data,$col);
                    } 
                    $cont++;
                }//end if(strpos($col->getSyncoperation(),'import')>0)
              }
        }
        else if($peration=='update'){
            $cont=0;
             foreach ($columns  as $col) {
                 if(strpos($col->getSyncoperation(),'update')>0){
                        if(!$col->getPrimarykey()){
                             if($cont==0){
                                    $values.=$this->getValue($data,$col,'update');
                              } else{
                                $values.=", ".$this->getValue($data,$col,'update');
                            } 
                      $cont++;
                    }//enf(!$col->getPrimarykey())
                 }//end if(strpos($col->getSyncoperation(),'update')>0)
                 
              
             }//end foreach 
        }// end  else if($peration=='update')
       
        else if($peration=='delete'){
            $cont=0;
             foreach ($columns  as $col) {
                 if(strpos($col->getSyncoperation(),'delete')>0){
                  
                   $defaulValueOndelete=$this->getDefaultValueOnDelete($col);
                   $defaulValueOndelete=trim($defaulValueOndelete);
                   
                   if(!$col->getPrimarykey()&& strlen($defaulValueOndelete)>0){
                      
                          if($cont==0){
                               $value=$this->getValueType($defaulValueOndelete,$col->getDatatype());
                               $values=  $col->getName()." = $value ";
                            } else{
                                $value=$this->getValueType($defaulValueOndelete,$col->getDatatype());
                               $values.=", ".$col->getName()." = $value ";
                        } 
                      $cont++;
                    }//enf(!$col->getPrimarykey())
                 }//end if(strpos($col->getSyncoperation(),'update')>0)
                 
             
             }//end foreach 
        } //end  else if($peration=='update')
        return  $values;
    }
   /*
   public   function getValuePK($data,$columns){
        $value=null;
         if(empty($columns)) {return  $value;}
          foreach ($columns  as $col) {
              if($col->getPrimarykey()){
                  if(isset($data[$col->getName()])){
                    $value=$data[$col->getName()];
                 }
                 
                  break;
              }
          }//end foreach 
        
        return  $value;
    }*/
    
     public   function getPKValues($data,$columns){
        $values="";
        $cont=0;
           foreach ($columns  as $col) {
                if($col->getPrimarykey()){
                    $value=$data[$col->getName()];
                  if($cont==0){$values="$value";}
                  else{$values .= ",$value";}
                  $cont++;
                }
           }
        
        return  $values;
    }
    public   function getFilterWithRowData($data,$columns){
        $values="";
         if(empty($columns)) {return  $values;}
         $cont=0;
          foreach ($columns  as $col) {
              if($col->getPrimarykey()){
                  if($cont==0){$values=" WHERE ".$this->getValue($data,$col,'update');}
                  else{$values .= " AND ".$this->getValue($data,$col,'update');}
                  $cont++;
                
              }
          }//end foreach 
        
        return  $values;
    }
     public   function getFilterWithLastid($lastid,$columns){
         $values="";
          if(empty($columns)) {return  $values;}
          
         $lidparam=array();
         if($lastid!=null){
               $pos=stripos($lastid, ",");
               if($pos=== false){
                    $lidparam[0]=$lastid;
               }else{
                $lidparam = explode(",", $lastid);
           }
        
         }
        
         $cont=0;
         foreach ($columns  as $col) {
              if($col->getPrimarykey()){
                  $type=$col->getDatatype();
                  $valuelastid=null;
                  if(isset($lidparam[$cont])){$valuelastid=$lidparam[$cont];}
                  $valuep="";
                  if($valuelastid!=null){ $valuep=$col->getName(). " > ".$this->getValueType($valuelastid,$type) ;}
                  else { $valuep=$col->getName(). " IS NOT NULL " ;}
                  if($cont==0){$values= " WHERE $valuep ";}
                  else{$values .= " AND ".$valuep;}
                  $cont++;
                
              }
          }//end foreach 
        
        return  $values;
     }
     public   function getPK($columns){
        $values="";
        if(!empty($columns)){
            $cont=0;
            
            foreach ($columns  as $col) {
                if($col->getPrimarykey()){
                    if($cont==0){$values= $col->getName();}
                    else{$values .= ",". $col->getName();}
                    $cont++;
                }
             }
        }
        return  $values;
    }
    public   function getPKList($columns){
        $list=array();
        if(!empty($columns)){
            $cont=0;
           foreach ($columns  as $col) {
                if($col->getPrimarykey()){
                    $list[$cont]=$col->getName();
                    $cont++;
                }
             }
        }
        return   $list;
    }
    public   function countPK($columns){
       $cont=0;
        if(!empty($columns)){
           
            foreach ($columns  as $col) {
                if($col->getPrimarykey()){
                     $cont++;
                }
             }
        }
        return  $cont;
    }
    
   /* public   function getPKType($columns){
       
        if(!empty($columns)){
            foreach ($columns  as $col) {
                if($col->getPrimarykey()){
                    return $col->getDatatype();
                    break;
                }
             }
        }
        return  null;
    }*/
    public   function getPartialValue($data,$row){
        $sqlvalue="";
       
         $httpQueryString= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
         $httpQueryString->setQuery($row->getParam()); 
         $httpQueryString->makeParam();
         
         //left
          if($httpQueryString->existKey(DOMAINTABLE::$PARAM_PARTIAL_VALUE_NUMBER_WORD_LEFT)){
              $left=$httpQueryString->getValue(DOMAINTABLE::$PARAM_PARTIAL_VALUE_NUMBER_WORD_LEFT);
              if(empty($left)){$left=1;}
              $left=$left+0;
              $columnsource=$httpQueryString->getValue(DOMAINTABLE::$PARAM_PARTIAL_COLUM_SOURCE);
              if(!empty($columnsource)){
                  $valuepartial=$data[$columnsource];
                  $separted=explode(" ",$valuepartial);
                  $cont=0;
                  $value=" ";
                  foreach ($separted  as $v) {
                      if($cont==0){$value=$v;}
                      else{$value.= " ".$v;}
                     $cont++;
                     if($left==$cont){break;}
                  }
                  $sqlvalue= $this->getValueType($value,$row->getDatatype());
              }
              
          }
          else  if($httpQueryString->existKey(DOMAINTABLE::$PARAM_PARTIAL_VALUE_WITHOUT_NUMBER_WORD_LEFT)){
              $left=$httpQueryString->getValue(DOMAINTABLE::$PARAM_PARTIAL_VALUE_WITHOUT_NUMBER_WORD_LEFT);
              if(empty($left)){$left=1;}
              $left=$left+0;
              $columnsource=$httpQueryString->getValue(DOMAINTABLE::$PARAM_PARTIAL_COLUM_SOURCE);
              if(!empty($columnsource)){
                  $valuepartial=$data[$columnsource];
                  $separted=explode(" ",$valuepartial);
                  $cont=0;
                  $contAux=0;
                  $value=" ";
                  foreach ($separted  as $v) {
                      if($contAux>=$left){
                           if($cont==0){$value=$v;}
                            else{$value.= " ".$v;}
                            $cont++;
                      }
                      $contAux++;
                  }
                  $sqlvalue= $this->getValueType($value,$row->getDatatype());
              }
          }
        return  $sqlvalue;
    }
    
     public   function getCommadValue($row){
         $value="";
          $httpQueryString= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
         $httpQueryString->setQuery($row->getParam());
          $httpQueryString->makeParam(); 
          if($httpQueryString->existKey(DOMAINTABLE::$PARAM_COMMAND_VALUE)){
              $command=$httpQueryString->getValue(DOMAINTABLE::$PARAM_COMMAND_VALUE);
              if($command=='timestamp'){$value=time();}
             
          }
          return $value;
     }
     
      public   function getDefaultValueOnDelete($row){
         $value=FactorySqlUtil::getPkListExceptionOnDelete($row);
          $httpQueryString= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
         $httpQueryString->setQuery($row->getParam());
          $httpQueryString->makeParam();
          if($httpQueryString->existKey(DOMAINTABLE::$PARAM_DEFAULT_VALUES_ON_DELETE)){
              $value=$httpQueryString->getValue(DOMAINTABLE::$PARAM_DEFAULT_VALUES_ON_DELETE);
          }
          return $value;
     }
     public   function getPkListExceptionOnDelete($row){
         $list=array();
         $value="";
          $httpQueryString= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
         $httpQueryString->setQuery($row->getParam());
          $httpQueryString->makeParam();
          if($httpQueryString->existKey(DOMAINTABLE::$PARAM_OPERATION_ON_DELETE_PK_LIST_EXCEPTION)){
              $value=$httpQueryString->getValue(DOMAINTABLE::$PARAM_OPERATION_ON_DELETE_PK_LIST_EXCEPTION);
          }
          if(!empty($value)){
            if(strpos($value,',')=== false){
                 $list=array($value);
            }else{
                $list=explode(",", $value);
            }
          }
          return $list;
     }
    
    public   function getTargetSqlFilterOnSelect($param){

         $value="";
         $httpQueryString= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
         $httpQueryString->setQuery($param);
          $httpQueryString->makeParam(); 
          if($httpQueryString->existKey(DOMAINTABLE::$PARAM_TARGET_SQL_FILTER_ON_SELECT)){
              $value=$httpQueryString->getValue(DOMAINTABLE::$PARAM_TARGET_SQL_FILTER_ON_SELECT);
        }
          return $value;
     } 


      public   function getSourceSqlFilterOnSelect($param){
         $value="";
          $httpQueryString= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
         $httpQueryString->setQuery($param);
          $httpQueryString->makeParam(); 
          if($httpQueryString->existKey(DOMAINTABLE::$PARAM_SOURCE_SQL_FILTER_ON_SELECT)){
              $value=$httpQueryString->getValue(DOMAINTABLE::$PARAM_SOURCE_SQL_FILTER_ON_SELECT);
        }
          return $value;
     } 

     public   function getTargetSqlFilterOnUpdate($param){
         $value="";
         $httpQueryString= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
         $httpQueryString->setQuery($param);
          $httpQueryString->makeParam(); 
          if($httpQueryString->existKey(DOMAINTABLE::$PARAM_TARGET_SQL_FILTER_ON_UPDATE)){
              $value=$httpQueryString->getValue(DOMAINTABLE::$PARAM_TARGET_SQL_FILTER_ON_UPDATE);
        }
          return $value;
     } 


      public   function getTargetSqlFilterOnDelete($param){
         $value="";
          $httpQueryString= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
         $httpQueryString->setQuery($param);
          $httpQueryString->makeParam(); 
          if($httpQueryString->existKey(DOMAINTABLE::$PARAM_TARGET_SQL_FILTER_ON_DELETE)){
              $value=$httpQueryString->getValue(DOMAINTABLE::$PARAM_TARGET_SQL_FILTER_ON_DELETE);
        }
          return $value;
     }

    public function getContainer() {
         return $this->container;
     }

     public function setContainer(Container $container) {
         $this->container = $container;
     }


}
