<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\Sync\DbBundle\Model\Lib\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Sync\DbBundle\Model\Lib\DOMAINTABLE;

/**
 * Description of FactorySqlFilterExpression
 *
 * @author lino
 */
class FactorySqlFilterExpression {

      /**
     * @var Container
     */
     private $container;

    /**
     * @var string
     */
    private $dbtype;

    /**
     * @var string
     */
    private $param;

      function __construct(Container $container) {
                $this->container=$container;
                $this->dbtype='mysql';
                $this->param=null;
     }


    public function exec($dbtype,$param){
        $this->dbtype=$dbtype;
        $this->param=$param;
        $param=$this->dateTimeLastXDeys();
        return $param;
    }

    // convert {datetime_last_x_days} do date
    public function dateTimeLastXDeys($param){

        $doReplace=TRUE;
        while ($doReplace){
            //check existe text {datetime_last_
            $expressionStart="{datetime_last_";
            $expressionEnd="_days}";
            $posStart = strpos($param, $expressionStart);
            $posEnd= strpos($param, $expressionEnd);
            if ($posStart === false) {
                $doReplace=FALSE;
            }else{

            }
        }




    }

    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param Container $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return string
     */
    public function getDbtype()
    {
        return $this->dbtype;
    }

    /**
     * @param string $dbtype
     */
    public function setDbtype($dbtype)
    {
        $this->dbtype = $dbtype;
    }

    /**
     * @return string
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * @param string $param
     */
    public function setParam($param)
    {
        $this->param = $param;
    }



}
