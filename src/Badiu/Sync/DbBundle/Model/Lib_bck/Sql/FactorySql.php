<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\Sync\DbBundle\Model\Lib\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Sync\DbBundle\Entity\SyncDbTable;
use Badiu\Sync\DbBundle\Model\Lib\DOMAINTABLE;


/**
 * Description of MakeSelect
 *
 * @author lino
 */
class FactorySql {
    
      /**
     * @var Container
     */
    private $container;
    
    /**
     * @var SyncDbTable
     *
    */
    private $table;
    
    /**
     * @var String
     *
    */
    private $dconfig;

    /**
     * @var string
     *
    */
    private $dboperation;

    /**
     * @var array
     *
    */
    private $columns;
    
    
       /**
        */
     private $factorysqlutil;
   
      function __construct(Container $container) {
                $this->container=$container;
                 $this->factorysqlutil= $this->getContainer()->get('badiu.sync.db.lib.sql.factorysqlutil');
                $this->dboperation=null;
                $this->dconfig=null;
     }
     
   
      public function clear(){
          $this->table=null;
          $this->columns=null;
          $this->dboperation=null;
      }
    public function getSelect($lastid=null){
         
        $sql="";
        if($this->getTable()->getDtype()==DOMAINTABLE::$TABLE_DATA_TIYPE_SQL){
             $factorycsqlcommand=$this->getContainer()->get('badiu.sync.db.lib.sql.command.factorycsqlcommand');
             $factorycsqlcommand->setTable($this->getTable());
             $sql=$factorycsqlcommand->replaceData($lastid,'select');
        }else{
            $table=$this->getTable()->getTblname();
            $columns=$this->getFactorysqlutil()->getColumns($this->getColumns());
            $distinct =$this->getDistinct();
            $columnpk=$this->geColumnPK();
            $wsql=$this->getFactorysqlutil()->getFilterWithLastid($lastid,$this->getColumns());
            $wsql.=$this->getSqlFilter();
            $sql="SELECT $distinct $columns FROM $table $wsql ";
        }
        
        
       
        return $sql;
    }
    //REVER  AS colid  quando temm2  ou mais colunas
     public function getSelectPK($lastid=null){
        $sql="";
        if($this->getTable()->getDtype()==DOMAINTABLE::$TABLE_DATA_TIYPE_SQL){
             $factorycsqlcommand=$this->getContainer()->get('badiu.sync.db.lib.sql.command.factorycsqlcommand');
             $factorycsqlcommand->setTable($this->getTable());
             $sql=$factorycsqlcommand->replaceData($lastid,'selectpk');
        }else{
             $table=$this->getTable()->getTblname();
            $columnpk=$this->geColumnPK();
            $distinct =$this->getDistinct();
            $wsql=$this->getFactorysqlutil()->getFilterWithLastid($lastid,$this->getColumns());
            $wsql.=$this->getSqlFilter();
            $wsql .= " ORDER BY  $columnpk ";
            $sql="SELECT  $distinct $columnpk  FROM $table $wsql ";
        }
       
       
        return $sql;
    } 
     public function getInsert($data){
        $sql=null;
          if($this->getTable()->getDtype()==DOMAINTABLE::$TABLE_DATA_TIYPE_SQL){
                $factorycsqlcommand=$this->getContainer()->get('badiu.sync.db.lib.sql.command.factorycsqlcommand');
                $factorycsqlcommand->setTable($this->getTable());
                $sql=$factorycsqlcommand->replaceData($data,'insert');
            
         }else{

            $table=$this->getTable()->getTblname();
            $columns=$this->getFactorysqlutil()->getColumns($this->getColumns(),'insert');
            $values=$this->getFactorysqlutil()->getValues($data, $this->getColumns(),'insert');
            $sql="INSERT INTO $table ($columns) VALUES ($values) ";
         }
       return $sql;
     }
     
     public function getDelete($data){
        $sql=null;
          if($this->getTable()->getDtype()==DOMAINTABLE::$TABLE_DATA_TIYPE_SQL){
                $factorycsqlcommand=$this->getContainer()->get('badiu.sync.db.lib.sql.command.factorycsqlcommand');
                $factorycsqlcommand->setTable($this->getTable());
                $pk=$this->getTable()->getSqlpk();
                $id=$data[$pk];
                $sql=$factorycsqlcommand->replaceData($id,'delete');
            
         }else{
            $table=$this->getTable()->getTblname();
            $filter=$this->getFactorysqlutil()->getFilterWithRowData($data, $this->getColumns());
            $filter.=$this->getSqlFilter();
            $sql="DELETE FROM $table  $filter ";
        }
        return $sql;
     }
      public function getUpdateOnDelete($data){
        $sql=null;

         if($this->getTable()->getDtype()==DOMAINTABLE::$TABLE_DATA_TIYPE_SQL){
                $factorycsqlcommand=$this->getContainer()->get('badiu.sync.db.lib.sql.command.factorycsqlcommand');
                $factorycsqlcommand->setTable($this->getTable());
                $sql=$factorycsqlcommand->replaceData($data,'update');
            
         }else{
            $table=$this->getTable()->getTblname();
            $values=$this->getFactorysqlutil()->getValues(null, $this->getColumns(),'delete');
            $filter=$this->getFactorysqlutil()->getFilterWithRowData($data, $this->getColumns());
            $filter.=$this->getSqlFilter();
            $sql="UPDATE $table SET  $values $filter";
          }
        return $sql;
     }
     public function getUpdate($data){
        $sql=null;
          if($this->getTable()->getDtype()==DOMAINTABLE::$TABLE_DATA_TIYPE_SQL){
                $factorycsqlcommand=$this->getContainer()->get('badiu.sync.db.lib.sql.command.factorycsqlcommand');
                $factorycsqlcommand->setTable($this->getTable());
                $sql=$factorycsqlcommand->replaceData($data,'update');
            
         }else{
                $table=$this->getTable()->getTblname();
                $values=$this->getFactorysqlutil()->getValues($data, $this->getColumns(),'update');
                $filter=$this->getFactorysqlutil()->getFilterWithRowData($data, $this->getColumns());
                 $filter.=$this->getSqlFilter();
                $sql="UPDATE $table SET $values  $filter ";
        }
       
        return $sql;
     }
    public function getSelectCount($lastid=null){
        
        $sql="";
        if($this->getTable()->getDtype()==DOMAINTABLE::$TABLE_DATA_TIYPE_SQL){
            $factorycsqlcommand=$this->getContainer()->get('badiu.sync.db.lib.sql.command.factorycsqlcommand');
            $factorycsqlcommand->setTable($this->getTable());
            $sql=$factorycsqlcommand->replaceData($lastid,'selectcount');
           
        }else{
            $table=$this->getTable()->getTblname();
            $columnpk=$this->geColumnPK();
            $distinct =$this->getDistinct();
            $wsql=$this->getFactorysqlutil()->getFilterWithLastid($lastid,$this->getColumns());
            $wsql.=$this->getSqlFilter();
            $sql="SELECT COUNT($distinct $columnpk)  AS countrecord  FROM $table  $wsql ";
      
        }
       return $sql;
    }
 
    public function existRecord($data){
         $sql="";
         if($this->getTable()->getDtype()==DOMAINTABLE::$TABLE_DATA_TIYPE_SQL){
             $factorycsqlcommand=$this->getContainer()->get('badiu.sync.db.lib.sql.command.factorycsqlcommand');
             $factorycsqlcommand->setTable($this->getTable());
             $pk=$this->getTable()->getSqlpk();
             $id=$data[$pk];
             //$sql=$factorycsqlcommand->replaceData($id,'selectexist');
			 $sql=$factorycsqlcommand->replaceData($data,'selectexist');
       
          }else{
             $table=$this->getTable()->getTblname();
             $columnpk=$this->geColumnPK();
             $wsql=$this->getFactorysqlutil()->getFilterWithRowData($data,$this->getColumns());
            $wsql.=$this->getSqlFilter();
            $sql="SELECT COUNT($columnpk)  AS countrecord  FROM $table  $wsql ";
          }
        //echo "xxx$sql";exit;
        return $sql;
       
    }

     public function geColumnPK(){
            $columnpk=$this->getFactorysqlutil()->getPK($this->getColumns());
           return $columnpk; 
     }
      public function geColumnPKList(){
            $columnlist=$this->getFactorysqlutil()->getPKList($this->getColumns());
           return $columnlist; 
     }
      public function countColumnPK(){
            $count=$this->getFactorysqlutil()->countPK($this->getColumns());
           return $count; 
     }
      public function getValuesPK($data){
            $valuespk=$this->getFactorysqlutil()->getPKValues($data,$this->getColumns());
           return $valuespk; 
     }
     public function getDistinct(){
         $distinct="";
         if($this->getTable()->getSelectdistinct()){
             $distinct=" DISTINCT ";
         }
           return $distinct; 
     }

     public function getSqlFilter(){
        $data=$this->getDconfig();
         $filter="";
         if($this->dboperation==DOMAINTABLE::$DB_SELECT_FROM_SOURCE){
            $filter=$this->getFactorysqlutil()->getSourceSqlFilterOnSelect($data);
         }else if($this->dboperation==DOMAINTABLE::$DB_SELECT_FROM_TARGET){
            $filter=$this->getFactorysqlutil()->getTargetSqlFilterOnSelect($data);
         }
        else if($this->dboperation==DOMAINTABLE::$DB_UPDATE_TARGET){
            $filter=$this->getFactorysqlutil()->getTargetSqlFilterOnUpdate($data);
         }else if($this->dboperation==DOMAINTABLE::$DB_DELETE_TARGET){
            $filter=$this->getFactorysqlutil()->getTargetSqlFilterOnDelete($data);
         }
           return $filter; 
     }
     public function getTable() {
         return $this->table;
     }

     public function getColumns() {
         return $this->columns;
     }

     public function setTable(SyncDbTable $table) {
         $this->table = $table;
     }

     public function setColumns($columns) {
         $this->columns = $columns;
     }

     public function getContainer() {
         return $this->container;
     }

     public function getFactorysqlutil() {
         return $this->factorysqlutil;
     }
    
    public function getDconfig() {
         return $this->dconfig;
     }


     public function setContainer(Container $container) {
         $this->container = $container;
     }

     public function setFactorysqlutil($factorysqlutil) {
         $this->factorysqlutil = $factorysqlutil;
     }


     public function setDboperation($dboperation) {
         $this->dboperation = $dboperation;
     }

    

public function setDconfig($dconfig) {
         $this->dconfig = $dconfig;
     }


}
