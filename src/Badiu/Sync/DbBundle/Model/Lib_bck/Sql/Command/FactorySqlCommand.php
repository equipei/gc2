<?php



namespace Badiu\Sync\DbBundle\Model\Lib\Sql\Command;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Sync\DbBundle\Model\Lib\DOMAINTABLE;
use Badiu\Sync\DbBundle\Entity\SyncDbTable;

/**
 * Description of FactorySqlCommand
 *
 * @author lino
 */
class FactorySqlCommand{
    
      /**
     * @var Container
     */
     private $container;
     
     /**
     * @var SyncDbTable
     *
    */
    private $table;
     
      function __construct(Container $container) {
                $this->container=$container;
     

     }
   
  /*    
function getCount(){
          $tsql=$this->getTable()->getSqlselect();
          $pk=$this->getTable()->getSqlpk();
          $distinct="";
          if($this->getTable()->getSelectdistinct()){
             $distinct=" DISTINCT ";
           }
          //get command after FROM
          $sql=$this->getAfterFrom($tsql);
          $sql="SELECT COUNT($distinct $pk) AS countrecord FROM $sql ";

         return $sql;

      }
*/
   
   /* function getAfterFrom($tsql){
       $sql="";
        $pos=stripos($tsql, " from ");
        if($pos=== false){
               return null;
        }else{
           $lsql=  preg_split("/from/i", $tsql);   
           $sql= $lsql[1];
        }
        return $sql;
    }
*/

/*
function replaceData($data,$operation='select'){
      $sql=null;
	echo "----------------------------<br>";
	print_r($data);
      //set $data to  0 if not array and empty
      if(!is_array($data) && empty($data)){$data=0;}
      if($operation=='select'){$sql=$this->getTable()->getSqlselect();}
      else if($operation=='insert'){$sql=$this->getTable()->getSqlinsert();}
      else if($operation=='update'){$sql=$this->getTable()->getSqlupdate();}
      else if($operation=='delete'){$sql=$this->getTable()->getSqldelete();}
      else if($operation=='selectcount'){$sql=$this->getTable()->getSqlcount();}
      else if($operation=='selectpk'){$sql=$this->getTable()->getSqlselectpk();}
      else if($operation=='selectexist'){$sql=$this->getTable()->getSqlexist();}

	
      if($operation=='insert' || $operation=='update'){
          foreach ($data as $key => $value){
           $sqlkey="{".$key."}";
           $sql=str_replace($sqlkey,$value, $sql);
         }
      }else{
            $value=$data;
           $pk=$this->getTable()->getSqlpk();
           $sqlkey="{".$pk."}";

            $sql=str_replace($sqlkey,$value, $sql);
      }
     
    
      return $sql;

 }
*/

 function replaceData($data,$operation='select'){
      $sql=null;
	  //set $data to  0 if not array and empty
      if(!is_array($data) && empty($data)){$data=0;}
      if($operation=='select'){$sql=$this->getTable()->getSqlselect();}
      else if($operation=='insert'){$sql=$this->getTable()->getSqlinsert();}
      else if($operation=='update'){$sql=$this->getTable()->getSqlupdate();}
      else if($operation=='delete'){$sql=$this->getTable()->getSqldelete();}
      else if($operation=='selectcount'){$sql=$this->getTable()->getSqlcount();}
      else if($operation=='selectpk'){$sql=$this->getTable()->getSqlselectpk();}
      else if($operation=='selectexist'){$sql=$this->getTable()->getSqlexist();}

	 $sql=$this->replaceExpression($data,$sql);
      return $sql;

 }
	public function replaceExpression($data,$txt) {
			$sql=null;
			if(is_array($data)){
				 foreach ($data as $key => $value){
					$sqlkey="{".$key."}";
					$txt=str_replace($sqlkey,$value, $txt);
				}
			}else{
				 $value=$data;
				$pk=$this->getTable()->getSqlpk();
				$sqlkey="{".$pk."}";
				$txt=str_replace($sqlkey,$value, $txt);
			}
			$sql=$txt;
			return $sql;
	}
      public function setTable(SyncDbTable $table) {
         $this->table = $table;
     }

      public function getTable() {
         return $this->table;
     }

}
