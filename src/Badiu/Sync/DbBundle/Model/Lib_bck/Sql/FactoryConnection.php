<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\Sync\DbBundle\Model\Lib\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Sync\DbBundle\Entity\SyncDb;
/**
 * Description of MakeSelect
 *
 * @author lino
 */
class FactoryConnection {
    
   /**
     * @var Container
     */
    private $container;
   
     /**
     * @var EntityManager
     */
    private $em; 
    
    
    
     function __construct(Container $container) {
                $this->container=$container;
                $db=$this->container->get('badiu.system.access.session')->get()->getDbapp(); 
                $this->em=$this->container->get('doctrine')->getEntityManager($db); 
      }
    
    public function get(SyncDb $db,$dbapp=null) {
        $config = $this->getConfig($db);
        $factoryconnection=$this->container->get('badiu.system.core.lib.sql.factoryconnection');
        $connection=$factoryconnection->get($config,$dbapp);
        return $connection;
    }
      
        public function getConfig(SyncDb $db) {
            
                $driveClass=$this->getDriverClass($db->getParam());
                $conn = array();
                if(empty($driveClass)){$conn['driver']= $this->getDrive($db->getDbtype());}
                else{$conn['driverClass']=$driveClass;}
                $conn['host']= $db->getHost();
                $conn['port']= $db->getPort();
                $conn['user']= $db->getUser();
                $conn['password']= $db->getPassword();
                $conn['dbname']= $db->getDbname();
                $charset=$db->getBdcharset();
               if(!empty($charset)){  $conn['charset']= $db->getBdcharset();}
                $conn['type']= $db->getDbtype();
                                           
            return $conn;
        }
     public function getDrive($drive) {
         $pdo="";
         if($drive=='mysql'){$pdo="pdo_mysql";}
         else if($drive=='pgsql'){$pdo="pdo_pgsql";}
         else if($drive=='mssql'){$pdo="pdo_sqlsrv";}
         return $pdo;
     }
     public function getDriverClass($param) {
         $class=null;
          $httpQueryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
          $httpQueryString->setQuery($param);
          $httpQueryString->makeParam();
          if($httpQueryString->existKey('doctrine_driver_class')){
            $class=$httpQueryString->getValue('doctrine_driver_class');
         }
         return $class;
     }
       public function getContainer() {
           return $this->container;
       }

       public function setContainer(Container $container) {
           $this->container = $container;
       }
       
       public function getEm() {
           return $this->em;
       }

       public function setEm(EntityManager $em) {
           $this->em = $em;
       }


}
