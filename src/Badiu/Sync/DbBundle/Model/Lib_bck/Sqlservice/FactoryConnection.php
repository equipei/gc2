<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\Sync\DbBundle\Model\Lib\Sqlservice;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Sync\DbBundle\Entity\SyncDb;
use Badiu\Sync\DbBundle\Model\Lib\DOMAINTABLE;
/**
 * Description of MakeSelect
 *
 * @author lino
 */
class FactoryConnection {
    
   /**
     * @var Container
     */
    private $container;
   
     /**
     * @var EntityManager
     */
    private $em; 
    
    
    
     function __construct(Container $container) {
                $this->container=$container;
               
      }
    
     public function getConfig(SyncDb $db) {
				$param=array();
				
					$localServiceUrl=$db->getServiceurl();
					$localServiceToken=$db->getServicetoken();
					$moduleinstance=$db->getModuleinstance();
					$modulekey=$db->getModulekey();
					if(!empty($localServiceUrl)){
						$param['url']=$localServiceUrl;
						$param['token']=$localServiceToken;
						$param['dbtblprefix']=$db->getDblprefix();
						$param['dbtype']=$db->getDbtype();
					}else if (!empty($modulekey) && !empty($moduleinstance) ){
						if($modulekey=='badiu.admin.server.service'){
							$sconfig=$this->getAdminServerService($moduleinstance);
							if($sconfig!=null){
								$param['url']=$sconfig['url'];
								$param['servicerurl']=$sconfig['servicerurl'];
								$param['token']=$sconfig['servicetoken'];
								$param['dbtblprefix']=$sconfig['dbtblprefix'];
								$param['dbtype']=$sconfig['dbtype'];
								
							}
							
						}
						
					
					}
					
				
                
            return $param;
        }
    
	public function getAdminServerService($intanceid) {
		 //get service data 
		  $data=$this->getContainer()->get('badiu.admin.server.service.data');
		  $result=$data->getInfoOfServiceForDs($intanceid);
		  return $result;
		  
	}
       public function getContainer() {
           return $this->container;
       }

       public function setContainer(Container $container) {
           $this->container = $container;
       }
       
       public function getEm() {
           return $this->em;
       }

       public function setEm(EntityManager $em) {
           $this->em = $em;
       }


}
