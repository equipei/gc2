<?php

namespace Badiu\Sync\DbBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class FormDataOptions extends BadiuFormDataOptions{
    
     
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
     
    
     public function getDataBase() {
         $list=array();
         $list['mysql']=$this->getTranslator()->trans('badiu.sync.db.data.mysql');
         $list['pgsql']=$this->getTranslator()->trans('badiu.sync.db.data.pgsql');
         $list['mssql']=$this->getTranslator()->trans('badiu.sync.db.data.mssql');
         $list['oracle']=$this->getTranslator()->trans('badiu.sync.db.data.oracle');
         $list['mongodb']=$this->getTranslator()->trans('badiu.sync.db.data.mongodb');
         return $list;
     } 
    
      public function getTypConnection() {
         $list=array();
         $list['db']=$this->getTranslator()->trans('badiu.sync.db.data.typeconn.db');
         $list['sqlservice']=$this->getTranslator()->trans('badiu.sync.db.data.typeconn.sqlservice');
         $list['service']=$this->getTranslator()->trans('badiu.sync.db.data.typeconn.service');
         return $list;
     } 
    
     public function getSyncOperation() {
         $list=array();
         $list['_import']=$this->getTranslator()->trans('badiu.sync.db.data.operation.import');
         $list['_update']=$this->getTranslator()->trans('badiu.sync.db.data.operation.update');
         $list['_delete']=$this->getTranslator()->trans('badiu.sync.db.data.operation.delete');
         
         $list['_import_update']=$this->getTranslator()->trans('badiu.sync.db.data.operation.importupdatee');
         $list['_import_delete']=$this->getTranslator()->trans('badiu.sync.db.data.operation.importdelete');
         $list['_update_delete']=$this->getTranslator()->trans('badiu.sync.db.data.operation.updatedelete');
         $list['_import_update_delete']=$this->getTranslator()->trans('badiu.sync.db.data.operation.importupdatedelete');
        return $list;
     } 
     
      public function getDataInserType() {
         $list=array();
         $list['insert_full_value_from_selected_column']=$this->getTranslator()->trans('insert_full_value_from_selected_column');
         $list['insert_partial_value_from_selected_column']=$this->getTranslator()->trans('insert_partial_value_from_selected_column');
         $list['insert_default_value']=$this->getTranslator()->trans('insert_default_value');
         $list['insert_command_value']=$this->getTranslator()->trans('insert_command_value');
         return $list;
     } 
     public function getDataType() {
         $list=array();
         $list['varchar']=$this->getTranslator()->trans('varchar');
         $list['text']=$this->getTranslator()->trans('text');
         $list['integer']=$this->getTranslator()->trans('integer');
         $list['datetime']=$this->getTranslator()->trans('datetime');
         return $list;
     } 
    
      public  function getDb(){
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
        $data=$this->getContainer()->get('badiu.sync.db.db.data');
        $list=$data->getIdNameByEntity($entity);
        $sformutil=$this->getContainer()->get('badiu.system.core.lib.form.util');
        $list=$sformutil->convertBdArrayToListOptions($list);
        return $list;
    }
    
    public  function getTable(){
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
        $data=$this->getContainer()->get('badiu.sync.db.table.data');
        $list=$data->getIdNameByEntity($entity);
        $sformutil=$this->getContainer()->get('badiu.system.core.lib.form.util');
        $list=$sformutil->convertBdArrayToListOptions($list);
        return $list;
    }
}
