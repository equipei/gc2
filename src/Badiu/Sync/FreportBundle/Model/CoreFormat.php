<?php

namespace Badiu\Sync\FreportBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;

class CoreFormat extends BadiuFormat{
     private $formdataoptions=null;
	 private $permission;
	 private  $filelib;
	 
     function __construct(Container $container) {
            parent::__construct($container);
			$this->formdataoptions=$this->getContainer()->get('badiu.sync.freport.core.form.dataoptions');
			$this->permission=$this->getContainer()->get('badiu.system.access.permission');
			$this->filelib=$this->getContainer()->get('badiu.system.file.file.lib');
			
       } 

public  function defaultimageurl($data){
	
		$defaultimage=$this->getUtildata()->getVaueOfArray($data,'defaultimage');
		$url=null;
		if(empty($defaultimage)){$url=$this->getUtilapp()->getResourseUrl("bundles/badiuthemecore/image/default/sync-freport-dafaultreport.jpg"); }
		else{$url= $this->getUtilapp()->getFileUrl($defaultimage);}
	
		return $url;
}
public  function type($data){
	//print_r($data);exit;
            $value="";
            $type=null;
             $type=$this->getUtildata()->getVaueOfArray($data, 'dtype');
			 $value=$this->formdataoptions->getTypeLabel($type);
            return $value; 
     } 
	
public  function formattype($data){
            $value="";
            $type=null;
             $type=$this->getUtildata()->getVaueOfArray($data, 'ftype');
			 $value=$this->formdataoptions->getFormatTypeLabel($type);
            return $value; 
     } 	

public  function sqltype($data){
            $value="";
            $type=null;
             $type=$this->getUtildata()->getVaueOfArray($data, 'sqltype');
			 $value=$this->formdataoptions->getSqlTypeLabel($type);
            return $value; 
     } 	
public  function config($data){
            $value="";
            $dkey=$this->getUtildata()->getVaueOfArray($data, 'dkey');
			$serviceaddress=$this->getUtildata()->getVaueOfArray($data, 'serviceaddress');
			
			$expressaion=$this->expression($data);
			$sqlcommand=$this->getUtildata()->getVaueOfArray($data, 'sqlcommand');
			$dconfig=$this->getUtildata()->getVaueOfArray($data, 'dconfig');
			
			$valueaux="";
			if(!empty($serviceaddress)){$valueaux.="$serviceaddress<br />";}
			if(!empty($dkey)){$valueaux.="$dkey<br />";}
			if(!empty($expressaion)){$valueaux.="<br />";}
			$value.=$sqlcommand;	
		if(!empty($sqlcommand) && !empty($dconfig)){
			$value.=$sqlcommand;	
		
			$value.=$dconfig;
		}else {
			$value.=$sqlcommand;	
			$value.=$dconfig;
		}
		if(!empty($value)){
			$value="$valueaux <textarea id=\"freportcommand\" name=\"freportcommandcode\" rows=\"3\" cols=\"60\">$value</textarea>";
		}else {$value=$valueaux;}
            return $value; 
     }
public  function linkoptions($data){
             $dtype=$this->getUtildata()->getVaueOfArray($data, 'dtype');
			 $id=$this->getUtildata()->getVaueOfArray($data, 'id');
			 $dkey=$this->getUtildata()->getVaueOfArray($data, 'dkey');
			 $sserviceid=$this->getUtildata()->getVaueOfArray($data, 'sserviceid');
			 $entity=$this->getUtildata()->getVaueOfArray($data, 'entity');
			 $param=$this->getUtildata()->getVaueOfArray($data, 'param');
			 $param=$this->getJson()->decode($param,true);
			 if(empty($id)){return null;}
			$links="";
			//if($dtype=='dashboarddataview'){
				//$fparam=array('parentid'=>$id);
				$fparam=array('parentid'=>$id,'_freportid'=>$id,'_fdkey'=>1,'_dkey'=>'badiu.sync.freport.monitorfviewdefault.dashboard','_force'=>1);
				$permissionvr=$this->permission->has_access('badiu.sync.freport.monitorfviewdefault.dashboard',$this->getSessionhashkey());
				if($permissionvr){
					//$dvurl=$this->getUtilapp()->getUrlByRoute('badiu.sync.freport.monitorfviewdefault.dashboard',$fparam);
					$dvurl=$this->getUtilapp()->getUrlByRoute('badiu.system.core.report.dynamicdp.dashboard',$fparam);
					$label=$this->getTranslator()->trans('badiu.sync.freport.showreport');
					$links.="<a href=\"$dvurl\"  target=\"_blank\">$label</a>";
				}
				
			/*} else if($dtype=='datalistcron'){
				$fparam=array('parentid'=>$id);
				$permissionvr=$this->permission->has_access('badiu.sync.freport.itemconsolidateperiod.index',$this->getSessionhashkey());
				if($permissionvr){
					$dvurl=$this->getUtilapp()->getUrlByRoute('badiu.sync.freport.itemconsolidateperiod.index',$fparam);
					$label=$this->getTranslator()->trans('badiu.sync.freport.showreport');
					$links.="<a href=\"$dvurl\">$label</a>";
				}
			}*/
	
			if($dtype=='datakey'){
				
				
				$fparam=array('_freportid'=>$id,'_dkey'=>$dkey,'_force'=>1);
				$permissionvr=$this->permission->has_access($dkey,$this->getSessionhashkey());
				$dkey='badiu.system.core.report.dynamic.index';
				if(!empty($sserviceid)){
					$fparam['parentid']=$sserviceid;
					$fparam['_serviceid']=$sserviceid;
					$fparam['_datasource']='servicesql';
					$dkey='badiu.system.core.report.dynamicp.index';
				}
				$url=$this->getUtilapp()->getUrlByRoute($dkey,$fparam);
				$urlfoward=null;
				$accessby=$this->getUtildata()->getVaueOfArray($param, 'accessby');
				$paramadd=$this->getUtildata()->getVaueOfArray($param, 'paramadd');
				
				if(!empty($accessby) && !empty($paramadd)){
					//echo $accessby;exit;
					$idaccessby=$this->getContainer()->get('badiu.sync.freport.freport.data')->getGlobalColumnValue('id',array('entity'=> $entity,'dkey'=>$accessby));
					if(empty($idaccessby)){return null;}
					$urlfoward="$url&$paramadd";
					$urlfoward=base64_encode($urlfoward);
					
					$fparam['_freportid']=$idaccessby;
					$fparam['_dkey']=$accessby;
					$fparam['_urlfoward']=$urlfoward;
					
					$url=$this->getUtilapp()->getUrlByRoute($dkey,$fparam);
					
				}
				
				if($permissionvr){
					$label=$this->getTranslator()->trans('badiu.sync.freport.showreport');
					$links="<a href=\"$url\"  target=\"_blank\">$label</a>";
				}
			}else if($dtype=='datalink'){
				$url=$this->getUtildata()->getVaueOfArray($data, 'param');
				$baseurl=$this->getUtilapp()->getBaseUrl();
				$url=str_replace("_BADIU_BASE_URL",$baseurl,$url);
				$url=str_replace("_BADIU_PARAM_PARENTID",$sserviceid,$url);
				$url=str_replace("_BADIU_PARAM_SERVICEID",$sserviceid,$url);
				
				$links="<a href=\"$url\"  target=\"_blank\">$label</a>";
				
			}
            return $links; 
     } 	 
public  function linksubitem($data){
             $dtype=$this->getUtildata()->getVaueOfArray($data, 'dtype');
			 $id=$this->getUtildata()->getVaueOfArray($data, 'id');
			 $name=$this->getUtildata()->getVaueOfArray($data, 'name');
			 $fparam=array('parentid'=>$id);
			 $key='badiu.sync.freport.data.index';
			 if($dtype=='dashboarditemview'){
				  $key='badiu.sync.freport.item.index';
			}
			 $url=$this->getUtilapp()->getUrlByRoute($key,$fparam);
			 $link="<a href=\"$url\">$name</a>";
			 return $link;
}
public  function accessurl($data){
             $dtype=$this->getUtildata()->getVaueOfArray($data, 'dtype');
			 $id=$this->getUtildata()->getVaueOfArray($data, 'id');
			 $dkey=$this->getUtildata()->getVaueOfArray($data, 'dkey');
			 $sserviceid=$this->getUtildata()->getVaueOfArray($data, 'sserviceid');
			 $modulekey=$this->getUtildata()->getVaueOfArray($data, 'modulekey');
			 $qparams=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
			 $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
			
			 if(empty($id)){return null;}
			 
			
			 $entity=$this->getUtildata()->getVaueOfArray($data, 'entity');
			 $param=$this->getUtildata()->getVaueOfArray($data, 'param');
			 $param=$this->getJson()->decode($param,true);
			 
			 
			$url=null;
			$canaaccessby=true;
			$fparam=array('_freportid'=>$id,'_fdkey'=>1,'_dkey'=>$dkey,'_force'=>1);
			if($modulekey=='badiu.moodle.mreport'){
					if(empty($sserviceid)){$sserviceid=$parentid;}
					if(empty($parentid)){$parentid=$sserviceid;}
					$fparam['parentid']=$parentid;
					$fparam['_serviceid']=$parentid;
					$fparam['_datasource']='servicesql';
					if(empty($parentid)){return null;}
					
					//check for mreport situation
					$qdkey=$this->getUtildata()->getVaueOfArray($qparams, '_dkey');
					$qcourseid=$this->getUtildata()->getVaueOfArray($qparams, 'courseid');
					if($qdkey=='badiu.moodle.mreport.courseview.freport.index' && !empty($qcourseid)){
						$canaaccessby=false;
						$fparam['courseid']=$qcourseid;
					}
			}
			
			$urlfoward=null;
			$accessby=$this->getUtildata()->getVaueOfArray($param, 'accessby');
			$paramadd=$this->getUtildata()->getVaueOfArray($param, 'paramadd');
				
				
			if($dtype=='datakey'){
				$key='badiu.system.core.report.dynamic.index';
				if($modulekey=='badiu.moodle.mreport'){$key='badiu.system.core.report.dynamicp.index';}
				if(isset($fparam['_fdkey'])){unset($fparam['_fdkey']);}
				$url=$this->getUtilapp()->getUrlByRoute($key,$fparam);
				
				
				
				if(!empty($accessby) && !empty($paramadd) && $canaaccessby){
					//echo $accessby;exit;
					$idaccessby=$this->getContainer()->get('badiu.sync.freport.freport.data')->getGlobalColumnValue('id',array('entity'=> $entity,'dkey'=>$accessby));
					if(empty($idaccessby)){return null;}
					$urlfoward="$url&$paramadd";
					$urlfoward=base64_encode($urlfoward);
					
					$fparam['_freportid']=$idaccessby;
					$fparam['_dkey']=$accessby;
					$fparam['_urlfoward']=$urlfoward;
					
					$url=$this->getUtilapp()->getUrlByRoute($key,$fparam);
					
					
				}
				
			}else if($dtype=='dashboarddataview' || $dtype=='dashboarditemview'  || $dtype=='datasql'){
				$fparam['_dkey']='badiu.sync.freport.monitorfviewdefault.dashboard';
				$key='badiu.system.core.report.dynamicd.dashboard';
				//if($modulekey=='badiu.moodle.mreport'){$key='badiu.system.core.report.dynamicdp.dashboard';}
				if($modulekey=='badiu.moodle.mreport'){$fparam['_mkey']='badiu.moodle.mreport.freport.index';}
				$url=$this->getUtilapp()->getUrlByRoute($key,$fparam);
				
				if(!empty($accessby) && !empty($paramadd) && $canaaccessby){
					//echo $accessby;exit;
					$idaccessby=$this->getContainer()->get('badiu.sync.freport.freport.data')->getGlobalColumnValue('id',array('entity'=> $entity,'dkey'=>$accessby));
					if(empty($idaccessby)){return null;}
					$urlfoward="$url&$paramadd";
					$urlfoward=base64_encode($urlfoward);
					
					$fparam['_freportid']=$idaccessby;
					$fparam['_dkey']=$accessby;
					$fparam['_urlfoward']=$urlfoward;
					if(isset($fparam['_fdkey'])){unset($fparam['_fdkey']);}
					$key='badiu.system.core.report.dynamicp.index';
					$url=$this->getUtilapp()->getUrlByRoute($key,$fparam);
					
					
				}
			}else if($dtype=='datalink'){
				$url=$this->buildlinkurl($data,$fparam);
			}
            return $url; 
     }
public  function linkoptionsitem($data){
             $freportid=$this->getUtildata()->getVaueOfArray($data, 'freportid');
			 $id=$this->getUtildata()->getVaueOfArray($data, 'id');
			 if(empty($id)){return null;}
			$links="";
	
				$fparam=array('parentid'=>$freportid,'itemid'=>$id,'stype'=>'freportitem');
				$permissionvr=$this->permission->has_access('badiu.sync.freport.monitorfviewdefault.dashboard',$this->getSessionhashkey());
				if($permissionvr){
					$dvurl=$this->getUtilapp()->getUrlByRoute('badiu.sync.freport.monitorfviewdefault.dashboard',$fparam);
					$label=$this->getTranslator()->trans('badiu.sync.freport.showreport');
					$links.="<a href=\"$dvurl\"  target=\"_blank\">$label</a>";
				}
				
			
			
            return $links; 
     } 	
public  function expression($data){
             $id= $this->getUtildata()->getVaueOfArray($data, 'freportid');
			 $shortname=$this->getUtildata()->getVaueOfArray($data, 'shortname');
			 
			 if(empty($shortname)){$shortname=$id;}
			 $value="{badiu.sync.freport.lib.factoryexpressioncontent|report|$shortname}";
			
            return $value; 
     }
public  function expressionitem($data){
             $id=$this->getUtildata()->getVaueOfArray($data, 'id');
			 $shortname=$this->getUtildata()->getVaueOfArray($data, 'shortname');
			 
			 if(empty($shortname)){$shortname=$id;}
			 $value="{badiu.sync.freport.lib.factoryexpressioncontent|reportitem|$shortname}";
			
            return $value; 
     }
public  function ctype($data){
             $ctype=$this->getUtildata()->getVaueOfArray($data, 'ctype');
			$value=$this->formdataoptions->getCtypeLabel($ctype);
			return $value; 
     }

public  function levelaccess($data){
             $levelaccess=$this->getUtildata()->getVaueOfArray($data, 'levelaccess');
			$levelaccess=$this->formdataoptions->getLevelaccessLabel($levelaccess);
			return $levelaccess; 
     }	

private  function buildlinkurl($data,$fparam){

			 $dconfig=$this->getUtildata()->getVaueOfArray($data, 'dconfig');
			
			 if(empty($dconfig)){return null;}
			 $url=null;
			 $qparams=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
			 $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
			 $serviceid=$this->getUtildata()->getVaueOfArray($qparams, 'serviceid');
			 $courseid=$this->getUtildata()->getVaueOfArray($qparams, 'courseid');
			 $userid=$this->getUtildata()->getVaueOfArray($qparams, 'userid');
			 
			 //replace
			  $dconfig=str_replace("{parentid}",$parentid,$dconfig);
			  $dconfig=str_replace("{_serviceid}",$serviceid,$dconfig);
			  $dconfig=str_replace("{userid}",$userid,$dconfig);
			 
			  $dconfig=$this->getJson()->decode($dconfig,true);
			
              $lroute=$this->getUtildata()->getVaueOfArray($dconfig, 'route');
			  $lurl=$this->getUtildata()->getVaueOfArray($dconfig, 'url');
			  $lparam=$this->getUtildata()->getVaueOfArray($dconfig, 'param');
			  $lquery=$this->getUtildata()->getVaueOfArray($dconfig, 'query');
			 
			 if(!empty($lurl)){
				 if (filter_var($lurl, FILTER_VALIDATE_URL)) {return $lurl;}
				 $url=$this->getUtilapp()->getBaseUrl().$lurl;
				 return $url; 
			 }
			
			 if(empty($lroute)){return null;}
			 
			 if(isset($fparam['_freportid'])){unset($fparam['_freportid']);}
			 if(isset($fparam['_fdkey'])){unset($fparam['_fdkey']);}
			 if(isset($fparam['_force'])){unset($fparam['_force']);}
			
			 if(!empty($lparam) && is_array($lparam)){
				 foreach ($lparam as $k => $v) {
					 $fparam[$k]=$v;
				 }
				 
			 }else if(!empty($lquery)){
				$querystring=$this->getContainer()->get('badiu.system.core.lib.http.querystring'); 
				$querystring->setQuery($lquery);
				$querystring->makeParam();
				$qtparam=$querystring->getParam();
				foreach ($qtparam as $k => $v) {
					 $fparam[$k]=$v;
				 }
				 
			 }
			$url=$this->getUtilapp()->getUrlByRoute($lroute,$fparam);
			 
			return $url; 
     }	
	 
	 public  function urleditlang($data){
             $id=$this->getUtildata()->getVaueOfArray($data, 'id');
			$fparam=array('parentid'=>$id,'_urlgoback'=>urlencode($this->getUtilapp()->getCurrentUrl()));
		
			$url=$this->getUtilapp()->getUrlByRoute('badiu.sync.freport.translator.add',$fparam);
			return $url; 
     }	
}
