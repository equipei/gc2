<?php
namespace Badiu\Sync\FreportBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class CoreFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
    
  
  public  function getLine(){
    $list=array();
    $list[1]=1;
	$list[2]=2;
	$list[3]=3;
	$list[4]=4;
	$list[5]=5;
	$list[6]=6;
	$list[7]=7;
	$list[8]=8;
	$list[9]=9;
	$list[10]=10;
   
    return $list;
}
  
  public  function getType(){
    $list=array();
    $list['dashboarddataview']=$this->getTypeLabel('dashboarddataview');
	$list['dashboarditemview']=$this->getTypeLabel('dashboarditemview');
	$list['datalistcron']=$this->getTypeLabel('datalistcron');
	$list['datalistsql']=$this->getTypeLabel('datalistsql');
	$list['datasql']=$this->getTypeLabel('datasql');
	$list['datakey']=$this->getTypeLabel('datakey');
	$list['datalink']=$this->getTypeLabel('datalink');
	$list['datacron']=$this->getTypeLabel('datacron');
   
    return $list;
}

public  function getTypeLabel($key){
    $list=array();
	if(empty($key)){return null;}
    $label=$this->getTranslator()->trans('badiu.sync.freport.type.'.$key);
    return $label;
}

 public  function getFormatType(){
    $list=array();
    $list['table']=$this->getFormatTypeLabel('table');
	$list['chartbar']=$this->getFormatTypeLabel('chartbar');
	$list['chartpie']=$this->getFormatTypeLabel('chartpie');
	$list['chartareabasic']=$this->getFormatTypeLabel('chartareabasic');
	$list['chartlinebasic']=$this->getFormatTypeLabel('chartlinebasic');
	
	$list['card']=$this->getFormatTypeLabel('card');
   
    return $list;
}

public  function getFormatTypeLabel($key){
    $list=array();
	if(empty($key)){return null;}
    $label=$this->getTranslator()->trans('badiu.sync.freport.ftype.'.$key);
    return $label;
}

public  function getSqlType(){
    $list=array();
   // $list['nativesql']=$this->getSqlTypeLabel('nativesql');
	$list['nativesqlservice']=$this->getSqlTypeLabel('nativesqlservice');
	$list['nativesqlservicelist']=$this->getSqlTypeLabel('nativesqlservicelist');
	$list['doctrinesql']=$this->getSqlTypeLabel('doctrinesql');
	$list['doctrinesqllist']=$this->getSqlTypeLabel('doctrinesqllist');
	
    return $list;
}

public  function getSqlTypeLabel($key){
    $list=array();
	if(empty($key)){return null;}
    $label=$this->getTranslator()->trans('badiu.sync.freport.sqltype.'.$key);
    return $label;
}

public  function getCtype(){
    $list=array();
    $list['free']=$this->getCtypeLabel('free');
	$list['commercial']=$this->getCtypeLabel('commercial');
    return $list;
}
public  function getCtypeLabel($key){
    $list=array();
	if(empty($key)){return null;}
    $label=$this->getTranslator()->trans('badiu.sync.freport.ctype.'.$key);
    return $label;
}

public  function getLevelaccess(){
    $list=array();
    $list[100]=$this->getLevelaccessLabel(100);
	$list[200]=$this->getLevelaccessLabel(200);
	$list[300]=$this->getLevelaccessLabel(300);
    return $list;
}
public  function getLevelaccessLabel($code){
    $list=array();
	$key=null;
	if($code==100){$key='global';}
	else if($code==200){$key='entity';}
	else if($code==300){$key='moduleinstance';}
	if(empty($key)){return null;}
    $label=$this->getTranslator()->trans('badiu.sync.freport.levelaccess.'.$key);
    return $label;
}

public  function getSourcedataText(){
    $list=array();
    $list['report']=$this->getSourcedataTextLabel('report');
	 $list['item']=$this->getSourcedataTextLabel('item');
	
    return $list;
}
public  function getSourcedataTextLabel($key){
    
    $label=$this->getTranslator()->trans('badiu.sync.freport.exporttotranslator.sourcedata.'.$key);
    return $label;
}
}
