<?php
namespace Badiu\Sync\FreportBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\UserBundle\Model\UserFormController;

class TranslatorFormController extends UserFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
          }
     
     public function changeParamOnOpen() {
        $param=$this->getParam();
		
		$initfrompackage=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_initfrompackage');
		$parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		
		$locale=$this->getLang(); 
		$entity=$this->getEntity();
		
		$tidata=$this->getContainer()->get('badiu.system.module.translatorinstance.data');
		
		$fparam=array('locale'=>$locale,'modulekey'=>'badiu.sync.freport.freport','entity'=>$entity,'moduleinstance'=>$parentid,'mkey'=>'name');
		$name=$tidata->getGlobalColumnValue("message",$fparam);
		
		$fparam['mkey']='description';
		$description=$tidata->getGlobalColumnValue("message",$fparam);
		
		
		$fparam['mkey']='defaultimage';
		$defaultimage=$tidata->getGlobalColumnValue("message",$fparam);
		
		
		$param['name']=$name;
		$param['description']=$description;
		$param['locale']=$locale;
		$param['mstatus']='inreview';
		$this->setParam($param);
     } 

	public function saveExec() {
		$urlgoback=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urlgoback');
		$this->setUrlgoback($urlgoback);
		$name=$this->getParamItem('name');
		$description=$this->getParamItem('description');
		$defaultimage=$this->getParamItem('defaultimage');
		
		$cont=0;
		$r=$this->add('name',$name);
		if($r){$cont++;}
		$r=$this->add('description',$description);
		if($r){$cont++;}
		$r=$this->add('defaultimage',$defaultimage);
		if($r){$cont++;}
		
		$translatormanage=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
		$fparam=array('level'=>'entity','entity'=>$this->getEntity()); 
		$result=$translatormanage->makeCache($fparam);
				
		return $cont;
	}
	
	 private function add($mkey,$message) {
		 $tlmanage=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
		 $messageidx=$tlmanage->getMessageidx($message);
		 $parentid=$this->getParamItem('freportid');
		 $modulekey=$this->getParamItem('modulekey');
		 $locale=$this->getParamItem('locale');
		 $mstatus=$this->getParamItem('mstatus');
		$entity=$this->getEntity();
		
		
		 $session=$this->getContainer()->get('badiu.system.access.session');
		 $sessiondata=$session->get();
		 $useridadd=$sessiondata->getUser()->getId();
		 
		$tdata=$this->getContainer()->get('badiu.system.module.translatorinstance.data');
        $paramadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$parentid,'mstatus'=>$mstatus,'locale'=>$locale,'mkey'=>$mkey,'message'=>$messageidx,'messageidx'=>$messageidx,'useridadd'=>$useridadd,'deleted'=>0,'timecreated'=>new \DateTime());
		$paramedit=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$parentid,'mstatus'=>$mstatus,'locale'=>$locale,'mkey'=>$mkey,'message'=>$messageidx,'messageidx'=>$messageidx,'useridadd'=>$useridadd,'deleted'=>0,'timemodified'=>new \DateTime());
		
		$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$parentid,'locale'=>$locale,'mkey'=>$mkey);
        $result=$tdata->addNativeSql($paramcheckexist,$paramadd,$paramedit,true);
        $r=$this->getUtildata()->getVaueOfArray($result,'id');
		
		
		return $r;
    }
	
   
    
}
