<?php

namespace Badiu\Sync\FreportBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class FreportDefaultSqlFilter extends BadiuSqlFilter{
    /**
     * @var object
     */
  
   

    function __construct(Container $container) {
            parent::__construct($container);
     }

        function additemperiodfields() {
       
           $freportid= $this->getUtildata()->getVaueOfArray($this->getParam(),'parentid');
		   $itemid= $this->getUtildata()->getVaueOfArray($this->getParam(),'itemid');
		   $periodscale=$this->getPeriodScale($freportid);
		
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			$badiuSession->addValue('_addcolumndynamicallysyncfreportitem',$periodscale);
	
            return " ";
        }
        
       

        function getPeriodScale($freportid,$type='year'){
			
			 $data =$this->getContainer()->get('badiu.sync.freport.data.data');
			  $itemid= $this->getUtildata()->getVaueOfArray($this->getParam(),'itemid');
			 $fparam=array('freportid'=>$freportid,'enable'=>1,'deleted'=>0);
			 if(!empty($itemid)){$fparam['id']=$itemid;}
			 $list=$data->getGlobalColumnValues('content',$fparam);
			
			 $result=array();
			 if(empty($list)){return  null;}
			 if(!is_array($list)){return  null;}
			 
			 $min=0;
			 $max=0;
			
			 
			 foreach ($list as $row) {
				 $content=$this->getUtildata()->getVaueOfArray($row,'content');
				 $content= $this->getJson()->decode($content, true);
				 $periods= $this->getUtildata()->getVaueOfArray($content,'period');
				
				
				 if(!empty($periods) && is_array($periods)){
					  foreach ($periods as $k => $v) {
						 
						  if($min ==0){$min=$k;}
						  else  if($k < $min){$min=$k;}
						
						 if($k > $max){$max=$k;}
					  }
				 }
			}	
			$contlimit=0;
			while ($min <= $max){
				
				$result[$min]=$min;
				$min++;
				$contlimit++;
				if($min > $max){break;}
				if($contlimit > 50 ){break;}
				
			}
			$result['total']=$this->getTranslator()->trans('badiu.sync.freport.itemconsolidateperiod.total');
			return $result;	
		

}

}
