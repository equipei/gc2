<?php

namespace Badiu\Sync\FreportBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class AccessLib extends  BadiuModelLib{
  
   function __construct(Container $container) {
                parent::__construct($container);
				
        } 

  public function checkAutorization($param) {
		$resp = new \stdClass();
		$resp->autoryzed=true;
		$resp->message='success';
		
		$freportid=$this->getUtildata()->getVaueOfArray($param,'freportid');
		if(empty($freportid)){return $resp;}
		$freportdata=$this->getContainer()->get('badiu.sync.freport.freport.data');
		$frrow=$freportdata->getGlobalColumnsValue('o.levelaccess,o.moduleinstance,o.entity',array('id'=>$freportid));

		$defaultentity =1;
			if($this->getContainer()->hasParameter('badiu.system.access.defaultentity')){
				$defaultentity =$this->getContainer()->getParameter('badiu.system.access.defaultentity');
			}
		if(empty($defaultentity)){$defaultentity=1;}
		
		$levelaccess = $this->getUtildata()->getVaueOfArray($frrow,'levelaccess');
		$moduleinstance = $this->getUtildata()->getVaueOfArray($frrow,'moduleinstance');
		$entity = $this->getUtildata()->getVaueOfArray($frrow,'entity');

		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
		$badiuSession->setHashkey($this->getSessionhashkey());
		$dsession = $badiuSession->get();
			
		//check server service is from current entity
		$serviceid = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');		
		if(!empty($serviceid)){
			
			$sservicedata=$this->getContainer()->get('badiu.admin.server.service.data');
			$fparam=array('id'=>$serviceid);
			$centity=$sservicedata->getGlobalColumnValue('entity',$fparam);
			if($centity!=$dsession->getEntity()){
				$resp->autoryzed=false;
				$resp->message=$this->getTranslator()->trans('badiu.sync.freport.otherentity.messageaccessdenied');
				return $resp;
			}
		} 
	
		//check for entity level
		if($levelaccess==200){
			if($entity!=$dsession->getEntity()){
				$resp->autoryzed=false;
				$resp->message=$this->getTranslator()->trans('badiu.sync.freport.levelaccess.global.messageaccessdenied');
				return $resp;
			}
		}	
			
		//check for instance level
		if($levelaccess==300){
			if($serviceid!=$moduleinstance){
				$resp->autoryzed=false;
				$resp->message=$this->getTranslator()->trans('badiu.sync.freport.levelaccess.entity.messageaccessdenied');
				return $resp;
			}
		}	
	return $resp;
	}

	
}
?>