<?php

namespace Badiu\Sync\FreportBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

class SummarizeItemsTask extends  TaskGeneralExec{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
				$this->cresult=array('datashouldexec' => 0, 'dataexec' => 0);
        } 

  public function import() {
	$this->yearData();
   return $this->cresult;

}

public function update() {
   $this->yearData();
   return $this->cresult;
}

public function yearData(){
	$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
	if($operation!='summarizebyyear'){return null;}
	$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
	$period=$this->getUtildata()->getVaueOfArray($this->getParam(),'period');
	$reportshortname=$this->getUtildata()->getVaueOfArray($this->getParam(),'reportshortname');
	$reporttype=$this->getUtildata()->getVaueOfArray($this->getParam(),'reporttype');
	
	//get lista of enrol not completed
	$fparam=array('maxrecord'=>$maxrecord,'reportshortname'=>$reportshortname);
	$lisreportitem=$this->getReportItems($fparam);
	
	 if(!is_array($lisreportitem)){return  null;}
	$econtexec=0;
	
	
	
	foreach ($lisreportitem as $row) {
		$id= $this->getUtildata()->getVaueOfArray($row,'id');
		$name= $this->getUtildata()->getVaueOfArray($row,'name');
		$serviceaddress= $this->getUtildata()->getVaueOfArray($row,'serviceaddress');
		
		$dconfig= $this->getUtildata()->getVaueOfArray($row,'dconfig');
	    $dconfig = $this->getJson()->decode($dconfig, true);
	
		$filter = $this->getUtildata()->getVaueOfArray($dconfig,'filter');
		if(!is_array($filter)){$filter=array();}
		$filter['_period']=$period;
		
		$servicekey=null;
		$function=null;
		$service=null;
		$eresult=null;
		
		if(!empty($serviceaddress)){
			$pos=stripos($serviceaddress, "|");
            if($pos!== false){
				$p=explode("|",$serviceaddress);
				$servicekey=$this->getUtildata()->getVaueOfArray($p,0);
				$function=$this->getUtildata()->getVaueOfArray($p,1);
			}
			if(empty($function)){
				$pos=stripos($serviceaddress, "/");
				if($pos!== false){
					$p=explode("/",$serviceaddress);
					$servicekey=$this->getUtildata()->getVaueOfArray($p,0);
					$function=$this->getUtildata()->getVaueOfArray($p,1);
				}
			}
			if(empty($servicekey)){$servicekey=$serviceaddress;}
			if(empty($function)){$function='exec';}
		}
		if(! empty($servicekey) && $this->getContainer()->has($servicekey)){
			$service=$this->getContainer()->get($servicekey);
			if(method_exists($service,$function)){
				$result=$service->$function($filter);
				$eresult=$this->updateContent($id,$result,$period);
			}
		}
		
		
		  
		$datashouldexec=1;
		$dataexec=0;
		if($eresult){$dataexec=1;$econtexec++;}
		 $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => null);
		$this->cresult = $this->addResult($this->cresult, $resultmdl);  
	}
	return $econtexec;	 
}

 public function updateContent($id,$content,$period) {
	
	 if(empty($id)){return null;}
	
	  $data =$this->getContainer()->get('badiu.sync.freport.data.data');
	  $dbcontent=$data->getGlobalColumnValue('content',array('id'=>$id));
	  $dbcontent = $this->getJson()->decode($dbcontent, true);
	  if(empty($dbcontent)){$dbcontent=array();}
	  if($period=="allyears"){$dbcontent=array();}
	  if(!is_array($dbcontent)){$dbcontent=array();}
	  
	  $dbcontentperiod= $this->getUtildata()->getVaueOfArray($dbcontent,'period');
	  if(empty($dbcontentperiod)){$dbcontentperiod=array();}
	  if(!is_array($dbcontentperiod)){$dbcontentperiod=array();}
	  
	  foreach ($content as $key => $value) {
		  $dbcontentperiod[$key]=$value;
	  }
	 $dbcontent['period']=$dbcontentperiod;
	 $dbcontent = $this->getJson()->encode($dbcontent);
	 $aparam=array('content'=>$dbcontent,'id'=>$id,'timemodified'=>new \DateTime());
	 $dresult=$data->updateNativeSql($aparam,false);
	 return $dresult;
 }
  public function getReportItems($param) {
	    $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		$reportshortname=$this->getUtildata()->getVaueOfArray($param,'reportshortname');
		$reporttype=$this->getUtildata()->getVaueOfArray($param,'reporttype');
		$wsql="";
		 if(!empty($reportshortname)){
			$wsql.=" AND r.shortname = :reportshortname ";
		 }
		if(!empty($reporttype)){
			$wsql.=" AND r.dtype = :reporttype ";
		 }
		$sql="SELECT o.id,o.entity, o.name,o.serviceaddress,o.dconfig FROM BadiuSyncFreportBundle:SyncFreportData o JOIN o.freportid r WHERE o.enable=:enable AND o.deleted=:deleted $wsql ORDER BY o.sortorder ";
        
		$data=$this->getContainer()->get('badiu.sync.freport.data.data');
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('enable', 1);
		if(!empty($reportshortname)){$query->setParameter('reportshortname', $reportshortname);}
		if(!empty($reporttype)){$query->setParameter('reporttype', $reporttype);}
		$query->setParameter('deleted', 0);
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		
		
        return $result;
    } 
	
}
?>