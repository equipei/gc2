<?php

namespace Badiu\Sync\FreportBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ManageTranslator extends BadiuModelLib {

	function __construct(Container $container) {
                parent::__construct($container);
           
       }
	
	public function  exportText($param){
		$cont=0;
	

		$package=$this->getUtildata()->getVaueOfArray($param,'package');
		$sourcedata=$this->getUtildata()->getVaueOfArray($param,'sourcedata');
		$operation=$this->getUtildata()->getVaueOfArray($param,'operation');
		$status=$this->getUtildata()->getVaueOfArray($param,'status');
		 $entity=$this->getEntity();
		 $listreport=null;
		 if($sourcedata=='report'){$listreport=$this->getListReport($param);}
		 else if($sourcedata=='item'){$listreport=$this->getListReportItem($param);}
		
		
		if(!is_array($listreport)){return $cont;}
		$mparam=array('modulekey'=>'badiu.sync.freport.freport','locale'=>$package,'mstatus'=>$status,'entity'=>$entity,'operation'=>$operation);
		if($sourcedata=='item'){$mparam['modulekey']='badiu.sync.freport.item';}
		foreach($listreport as $row) {
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			$description=$this->getUtildata()->getVaueOfArray($row,'description');
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			
			$mparam['mkey']='name';
			$mparam['message']=$name;
			$mparam['freportid']=$id;
			$r=$this->add($mparam);
			if($r){$cont++;}
			if($sourcedata=='report'){
				$mparam['mkey']='description';
				$mparam['message']=$description;
				$mparam['freportid']=$id;
				$r=$this->add($mparam);
				if($r){$cont++;}
			}
		}
		
		return $cont;
	}
	
	  private function getListReport($param) {
		 $operation=$this->getUtildata()->getVaueOfArray($param,'operation');
		 $package=$this->getUtildata()->getVaueOfArray($param,'package');
		 $entity=$this->getEntity();
        $freportdata = $this->getContainer()->get('badiu.sync.freport.freport.data');
        
		$wsql="";
		if($operation=='insert'){$wsql=" AND (SELECT COUNT(ti.id) FROM BadiuSystemModuleBundle:SystemModuleTranslatorInstance ti WHERE ti.moduleinstance=o.id AND ti.entity=o.entity AND ti.modulekey='badiu.sync.freport.freport' AND ti.locale='".$package."' AND (ti.mkey ='name' OR ti.mkey='description')) =0 "; }
		else if ($operation=='update'){$wsql=" AND (SELECT COUNT(ti.id) FROM BadiuSystemModuleBundle:SystemModuleTranslatorInstance ti WHERE ti.moduleinstance=o.id AND ti.entity=o.entity AND ti.modulekey='badiu.sync.freport.freport' AND ti.locale='".$package."' AND (ti.mkey ='name' OR ti.mkey='description')) > 0 "; }
        $sql="SELECT o.id,o.name, o.description  FROM BadiuSyncFreportBundle:SyncFreport o  WHERE o.entity= :entity $wsql";
		$query =$freportdata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
       
        $result = $query->getResult();
      
        return $result;
    }
	
	  private function getListReportItem($param) {
		 $operation=$this->getUtildata()->getVaueOfArray($param,'operation');
		 $package=$this->getUtildata()->getVaueOfArray($param,'package');
		 $entity=$this->getEntity();
        $freportdata = $this->getContainer()->get('badiu.sync.freport.item.data');
        
		$wsql="";
		if($operation=='insert'){$wsql=" AND (SELECT COUNT(ti.id) FROM BadiuSystemModuleBundle:SystemModuleTranslatorInstance ti WHERE ti.moduleinstance=o.id AND ti.entity=o.entity AND ti.modulekey='badiu.sync.freport.item' AND ti.locale='".$package."' AND (ti.mkey ='name')) =0 "; }
		else if ($operation=='update'){$wsql=" AND (SELECT COUNT(ti.id) FROM BadiuSystemModuleBundle:SystemModuleTranslatorInstance ti WHERE ti.moduleinstance=o.id AND ti.entity=o.entity AND ti.modulekey='badiu.sync.freport.item' AND ti.locale='".$package."' AND (ti.mkey ='name')) > 0 "; }
        $sql="SELECT o.id,o.name FROM BadiuSyncFreportBundle:SyncFreport o  WHERE o.entity= :entity AND o.name IS NOT NULL $wsql";
		$query =$freportdata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
       
        $result = $query->getResult();
      
        return $result;
    }
	 private function add($param) {
		 $tlmanage=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
		 
		 $mkey=$this->getUtildata()->getVaueOfArray($param,'mkey');
		 $message=$this->getUtildata()->getVaueOfArray($param,'message');
		 $messageidx=$tlmanage->getMessageidx($message);
		 $parentid=$this->getUtildata()->getVaueOfArray($param,'freportid');
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $locale=$this->getUtildata()->getVaueOfArray($param,'locale');
		 $mstatus=$this->getUtildata()->getVaueOfArray($param,'mstatus');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		
		 $operation=$this->getUtildata()->getVaueOfArray($param,'operation');
		 
		 $session=$this->getContainer()->get('badiu.system.access.session');
		 $sessiondata=$session->get();
		 $useridadd=$sessiondata->getUser()->getId();
		 
		$tdata=$this->getContainer()->get('badiu.system.module.translatorinstance.data');
        $paramadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$parentid,'mstatus'=>$mstatus,'locale'=>$locale,'mkey'=>$mkey,'message'=>$message,'messageidx'=>$messageidx,'useridadd'=>$useridadd,'deleted'=>0,'timecreated'=>new \DateTime());
		$paramedit=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$parentid,'mstatus'=>$mstatus,'locale'=>$locale,'mkey'=>$mkey,'message'=>$message,'messageidx'=>$messageidx,'useridadd'=>$useridadd,'deleted'=>0,'timemodified'=>new \DateTime());
		if($operation=='insert'){$paramedit=null;}
		$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$parentid,'locale'=>$locale,'mkey'=>$mkey);
        $result=$tdata->addNativeSql($paramcheckexist,$paramadd,$paramedit,true);
        $r=$this->getUtildata()->getVaueOfArray($result,'id');
		return $r;
    }
	

}
?>