<?php

namespace Badiu\Sync\FreportBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\Yaml\Yaml;
class FactoryServiceReportDynamicKey extends BadiuModelLib{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
		  } 


	public function initSession($fparam) {
		  
		 $freportid=$this->getUtildata()->getVaueOfArray($fparam, '_freportid');
		 $force = $this->getUtildata()->getVaueOfArray($fparam, '_force');
		 $dkey = $this->getUtildata()->getVaueOfArray($fparam, '_dkey');
		 $filter = $this->getUtildata()->getVaueOfArray($fparam, 'filter');
	
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
		$sessionkey=$dkey.'.'.$freportid;
		$hassession=$badiuSession->getValue($sessionkey);
		if($hassession && !$force){return null;} 
		
		$faparam=array('freportid'=>$freportid,'dkey'=>$dkey,'filter'=>$filter);
		$this->addSession($faparam);
    }

	public function initSessionOfDynmickey($fparam) {
		
		 $freportid=$this->getUtildata()->getVaueOfArray($fparam, '_freportid');
		 $dkey = $this->getUtildata()->getVaueOfArray($fparam, '_dkey');
		 $fdkey = $this->getUtildata()->getVaueOfArray($fparam, '_fdkey');
		 $force = $this->getUtildata()->getVaueOfArray($fparam, '_force');
		 
		 if(!$fdkey){return $dkey;}
		 if(empty($dkey)){return $dkey;}
		 $p=explode(".",$dkey);
		 if(!is_array($p)){return null;}
		 $tsize=sizeof($p);
		
		 $nk="";
		 $cont=0;
		 foreach ($p as $i) {
			 $cont++;
			
			 $item=$i;
			if ($cont == $tsize-1) {$item=$i."".$freportid;}
			if ($cont == 1){$nk=$item;}
			else{$nk=$nk.'.'.$item;}
			
		 }
		$fparam=array('filter'=>1,'_freportid'=> $freportid,'_dkey'=>$nk,'_force'=>$force);
		
		$this->initSession($fparam);
		
		return  $nk;
	
		
    }	
 public function addSession($param){
	
	$freportid=$this->getUtildata()->getVaueOfArray($param,'freportid');
	$dkey=$this->getUtildata()->getVaueOfArray($param,'dkey');
	$freportdata=$this->getContainer()->get('badiu.sync.freport.freport.data');
	$filter= $this->getUtildata()->getVaueOfArray($param,'filter');
	$row=$freportdata->getGlobalColumnsValue('o.dkey,o.dconfig,o.customtext1',array('id'=>$freportid));
	$dconfig= $this->getUtildata()->getVaueOfArray($row,'dconfig');
	$filterconfig= $this->getUtildata()->getVaueOfArray($row,'customtext1');
	$cbkey= $this->getUtildata()->getVaueOfArray($row,'dkey');
	
	
	if($filter){$cbkey=$dkey;}
	
	if($cbkey!=$dkey){return null;}
	$cbkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($cbkey);
	
	$confyaml=null;
	if($filter){$confyaml = Yaml::parse($filterconfig);}
	else {$confyaml = Yaml::parse($dconfig);}
	
	$badiuSession = $this->getContainer()->get('badiu.system.access.session');
    $badiuSession->setHashkey($this->getSessionhashkey());
	if(is_array($confyaml)){
		foreach ($confyaml as $k => $v) {
			$key=$cbkey.'.'.$k;
			$badiuSession->addValue($key,$v);
			
		}
	}
	
	$sessionkey=$dkey.'.'.$freportid;
	$badiuSession->addValue($sessionkey,1);
	
 }
 	
}
?>