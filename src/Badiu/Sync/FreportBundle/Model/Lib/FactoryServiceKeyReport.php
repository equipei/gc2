<?php

namespace Badiu\Sync\FreportBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryServiceKeyReport extends BadiuModelLib{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
		  } 

 public function teste() {
	$param=array('freportid'=>1);
	$result=$this->make($param);
	print_r($result);exit;
 }
  public function make($param) {
		$freportid=$this->getUtildata()->getVaueOfArray($param,'freportid');
		$itemid=$this->getUtildata()->getVaueOfArray($param,'itemid');
		$fparam=array('parentid'=>$freportid,'entity'=>$this->getEntity(),'_page'=>0,'_addcolumndynamicallysyncfreportitem'=>1,'itemid'=>$itemid);

	 
	  $report= $this->getContainer()->get("badiu.sync.freport.itemconsolidateperiod.report");
	  $report->getKeymanger()->setBaseKey("badiu.sync.freport.itemconsolidateperiod");
      $report->setSessionhashkey($this->getSessionhashkey());
	  $report->setKey("badiu.sync.freport.itemconsolidateperiod.index");
	 $report->extractData($fparam,'indexCrud','dbsearch.fields.table.view');
	 
	 $report->makeTable('indexCrud');
	 $result = $report->getTable()->castToArray();
	
	$dataservice=$this->getContainer()->get('badiu.sync.freport.freport.data');
	$rparam=$dataservice->getGlobalColumnsValue('o.id AS freportid,o.name,o.description',array('id'=>$freportid));	
	$result=$this->makeHtmlTable($result,$rparam);
	
	return $result;
 }


	public function makeHtmlTable($data,$param) {
		$columns=$this->getUtildata()->getVaueOfArray($data,'columns');
		$list =$this->getUtildata()->getVaueOfArray($data,'rows');
		$name = $this->getUtildata()->getVaueOfArray($param,'name');
		$html="";
		// $html.='<div class="col-sm-6 col-md-6 mb-2" >';
		//  $html.="<h5>$name</h5> "; 
		  
		  $headtable= $this->makeHtmlTableHead($columns);
		   $html.="<table class=\"table table-striped\">$headtable";
		 
		   foreach ($list as $item) {
			    $html.="<tr>";
			    foreach ($columns as $k =>$v) {
					$value=$this->getUtildata()->getVaueOfArray($item,$k);
					  $html.="<td>$value</td>"; 
				}
			   $html.="</tr>"; 
		   }
		  
		   $html.="</table>";
		  //   $html.="</div>"; 
			 $param['codecontent']=$html;
		$html=$this->defaultCard($param);
		   return $html;
	}
	
	
	public function makeHtmlTableHead($list) {
		  $headtable="<thead><tr>";
		 if(!empty($list) && is_array($list)){
			foreach ($list as $k =>$v) {
				$headtable.=" <th>$v</th>";
			}
			}
		$headtable.="</thead></tr>";
		return $headtable;
	}

	public function defaultCard($param) {
		$title = $this->getUtildata()->getVaueOfArray($param,'name');
		$id = $this->getUtildata()->getVaueOfArray($param,'id');
		$shortname = $this->getUtildata()->getVaueOfArray($param,'shortname');
		$content = $this->getUtildata()->getVaueOfArray($param,'codecontent');
				 $dconfig= $this->getUtildata()->getVaueOfArray($param,'dconfig');
		 if(!is_array( $dconfig)){ $dconfig = $this->getJson()->decode($dconfig, true);}
		 $css=$this->getUtildata()->getVaueOfArray($dconfig,'classcss');
		 if(empty($css)){$css=" col ";}
		$html="<div class=\"$css $shortname \">
				<div class=\"card \">
					<div class=\"card-header text-center\">$title</div>
					<div class=\"card-body\"><p class=\"card-text text-center\">$content</p></div>
				 </div>
	      </div>";
	   
		return $html;
	}	
}
?>