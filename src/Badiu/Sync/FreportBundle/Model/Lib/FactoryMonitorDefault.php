<?php

namespace Badiu\Sync\FreportBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryMonitorDefault extends BadiuModelLib{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
		  } 


  public function makeList($param) {
	
	$items=$this->getUtildata()->getVaueOfArray($param,'dconfig.items',true);
	if(empty($items)){return null;}
	if(!is_array($items)){return null;}
	$dataservice=$this->getContainer()->get('badiu.sync.freport.freport.data');
	$result="";
	 $result.="<div class=\"badiu-sync-freport-monitor\" id=\"badiu-sync-freport-monitor-id\">";
	 
	 $formcofig= $this->getUtildata()->getVaueOfArray($param,'_formcofig');
	 
	 foreach ($items as $item) {
		  $row=$dataservice->getGlobalColumnsValue('o.id AS freportid,o.serviceaddress,o.dashboarditem',array('shortname'=>$item));
		   $row['_formcofig']=$row;
		 $id = $this->getUtildata()->getVaueOfArray($row,'id');
		  $serviceaddress=$this->getUtildata()->getVaueOfArray($row,'serviceaddress');
		  if(empty($serviceaddress)){$serviceaddress="badiu.sync.freport.freport.lib.factorysqlreport/make";}
		  $result.="<div class=\"row $item badiu-sync-freport-monitor-item$id\" id=\"badiu-sync-freport-monitor-id$item\">";
		  $result.=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($serviceaddress,$row,$this->getSystemdata());
		  $result.="</div> <hr />";  
	 }
	 $result.="</div>";  
	return $result;
 }

 public function makeItemList($param) {
	
	$reportshortname=$this->getUtildata()->getVaueOfArray($param,'reportshortname'); //review get freportid by shortname
    $freportid=$this->getUtildata()->getVaueOfArray($param,'freportid');
	$formcofig= $this->getUtildata()->getVaueOfArray($param,'_formcofig');
	if(empty($freportid)){return null;}
	$itemservice=$this->getContainer()->get('badiu.sync.freport.item.data');
	$fparam=array('dfreportid'=>$freportid);
	$maxline=$itemservice->getMaxGlobalColumnValue('line',$fparam);
	$serviceaddress='badiu.sync.freport.freport.lib.factorysqlreport/make';
	
	 
	if(empty($maxline)){return null;}
	$dataservice=$this->getContainer()->get('badiu.sync.freport.freport.data');
	$result="";
	 $result.="<div class=\"badiu-sync-freport-monitor\" id=\"badiu-sync-freport-monitor-id\">";
	 
	 $cont=1;
	 while ($cont <= $maxline) {
		  $row=array('line'=>$cont,'freportid'=>$freportid,'dashboardtype'=>'bditem','_formcofig'=>$formcofig);
		  $result.="<div class=\"row badiu-sync-freport-monitor-item$cont\" id=\"badiu-sync-freport-monitor-id$cont\">"; 
		  $result.=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($serviceaddress,$row,$this->getSystemdata());
		  $result.="</div> <hr />";  
		  $cont++;
	 }
	 $result.="</div>";  
	return $result;
 }
}
?>