<?php

namespace Badiu\Sync\FreportBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactorySqlReport extends BadiuModelLib{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
		  } 

 public function teste() {
	$param=array('freportid'=>2);
	$result=$this->make($param);
	print_r($result);exit;
 }
  public function make($param) {
	$lisreportitem=array();
	
	$formcofig= $this->getUtildata()->getVaueOfArray($param,'_formcofig');
	$freportid=$this->getUtildata()->getVaueOfArray($param,'freportid');
	$dashboarditem= $this->getUtildata()->getVaueOfArray($param,'dashboarditem');
	if($dashboarditem){$lisreportitem=$this->getReportItem($param);}
	
	else{$lisreportitem=$this->getReportItems($param);}
	
	 if(!is_array($lisreportitem)){return  null;}
	$econtexec=0;
	$htmlout="";
 
	$lisreportitem=$this->changeAlternativenameWithTranslator($lisreportitem,$freportid) ;
	foreach ($lisreportitem as $row) {
		
		$id= $this->getUtildata()->getVaueOfArray($row,'id');
		$name= $this->getUtildata()->getVaueOfArray($row,'name');
		$alternativename= $this->getUtildata()->getVaueOfArray($row,'alternativename');
		if(!empty($alternativename)){
			$name=$alternativename;
			$row['name']=$alternativename;
		}
		$ftype= $this->getUtildata()->getVaueOfArray($row,'ftype');
	
		$rows=$this->getDbData($row,$formcofig);
		if(empty($rows)){return $htmlout;}
		
		if($ftype=='table'){$htmlout.=$this->makeHtmlTable($rows,$row);}
		else if($ftype=='chartpie'){$htmlout.=$this->makeChartPieSimple($rows,$row);}
		else if($ftype=='chartbar'){$htmlout.=$this->makeChartBarSimple($rows,$row);}
		else if($ftype=='card'){$htmlout.=$this->makeCard($rows,$row);}
		else if($ftype=='chartareabasic'){$htmlout.=$this->makeChartAreaBasic($rows,$row);}
		else if($ftype=='chartlinebasic'){$htmlout.=$this->makeChartLineBasic($rows,$row);}
		
		else {$htmlout.=$this->makeHtmlTable($rows,$row);}
	}
	
	return $htmlout;
 }

	public function getReportItem($param) {
	    $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		$reportshortname=$this->getUtildata()->getVaueOfArray($param,'reportshortname');
		$reporttype=$this->getUtildata()->getVaueOfArray($param,'reporttype');
		$freportid=$this->getUtildata()->getVaueOfArray($param,'freportid');
		
		if(empty($maxrecord)){$maxrecord=50;}
		$wsql="";
		if(!empty($reportshortname)){
			$wsql.=" AND o.shortname = :reportshortname ";
		 }
		 if(!empty($freportid)){
			$wsql.=" AND o.id = :freportid ";
		 }
		  
		$sql="SELECT o.id,o.entity, o.name,o.serviceaddress,o.content,o.ftype,o.param,o.sqltype,o.sqlcommand,o.dconfig,s.id AS sserviceid FROM BadiuSyncFreportBundle:SyncFreport o LEFT JOIN o.sserviceid s WHERE o.id > 0  $wsql ";
        
		$data=$this->getContainer()->get('badiu.sync.freport.data.data');
		$query = $data->getEm()->createQuery($sql);
        if(!empty($reportshortname)){$query->setParameter('reportshortname', $reportshortname);}
		if(!empty($freportid)){$query->setParameter('freportid', $freportid);}
		$result = $query->getResult();
		
		
        return $result;
    } 
	
	public function getReportItems($param) {
	    $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		$reportshortname=$this->getUtildata()->getVaueOfArray($param,'reportshortname');
		$reporttype=$this->getUtildata()->getVaueOfArray($param,'reporttype');
		$freportid=$this->getUtildata()->getVaueOfArray($param,'freportid');
		$itemid=$this->getUtildata()->getVaueOfArray($param,'itemid');
		$dashboardtype=$this->getUtildata()->getVaueOfArray($param,'dashboardtype');
		$line=$this->getUtildata()->getVaueOfArray($param,'line');


		
	
		if(empty($maxrecord)){$maxrecord=50;}
		$wsql="";
		if(!empty($reportshortname)){
			$wsql.=" AND r.shortname = :reportshortname ";
		 }
		if(!empty($reporttype)){
			$wsql.=" AND r.dtype = :reporttype ";
		 }
		 if(!empty($freportid)){
			 $wsql.=" AND r.id = :freportid ";
		 }
		  if(!empty($itemid)){
			$wsql.=" AND o.id = :itemid ";
		 }
		 
		
	
		$sql="SELECT o.id,o.entity, o.name,o.serviceaddress,o.content,o.ftype,o.param,o.sqltype,o.sqlcommand,o.dconfig,s.id AS sserviceid,rs.id AS rsserviceid FROM BadiuSyncFreportBundle:SyncFreportData o JOIN o.freportid r LEFT JOIN o.sserviceid s LEFT JOIN r.sserviceid rs WHERE o.enable=:enable AND o.deleted=:deleted $wsql ORDER BY o.sortorder ";
        if($dashboardtype=='bditem'){
			$sql="SELECT r.id,r.entity, r.name,o.name AS alternativename,r.serviceaddress,r.content,r.ftype,r.param,r.sqltype,r.sqlcommand,r.dconfig,s.id AS sserviceid FROM BadiuSyncFreportBundle:SyncFreportItem o JOIN o.freportid r LEFT JOIN r.sserviceid s WHERE o.enable=:enable AND o.deleted=:deleted AND o.line=:line AND o.dfreportid = :freportid  ORDER BY o.sortorder ";
		}
		
			 
		$data=$this->getContainer()->get('badiu.sync.freport.data.data');
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('enable', 1);
		if(!empty($reportshortname)){$query->setParameter('reportshortname', $reportshortname);}
		if(!empty($reporttype)){$query->setParameter('reporttype', $reporttype);}
		if(!empty($freportid)){$query->setParameter('freportid', $freportid);}
		if(!empty($itemid)){$query->setParameter('itemid', $itemid);}
		if(!empty($line)){$query->setParameter('line', $line);}
		
		$query->setParameter('deleted', 0);
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		return $result;
    } 
	public function replaceSqlExpression($sql,$filter) {
		$entity= $this->getEntity();
		$sql=str_replace("{entity}",$entity,$sql);
		if(!is_array($filter)){return $sql;}
		foreach ($filter as $key => $value){
			if(!is_numeric($value)){$value="'".$value."'";}
			$find='{'.$key.'}';
			$sql=str_replace($find,$value,$sql);
		}
		return $sql;
	}
	public function getDbData($row,$formcofig) {
		$result=null;
		$sqltype= $this->getUtildata()->getVaueOfArray($row,'sqltype');
		
		if($sqltype=='doctrinesqllist' || $sqltype=='nativesqlservicelist'){$result=$this->getDbDataSqlList($row,$formcofig);}
		else {$result=$this->getDbDataNormal($row,$formcofig);}
		
		return $result;
		
	}
	public function getDbDataNormal($row,$formcofig) {
		
		$sqltype= $this->getUtildata()->getVaueOfArray($row,'sqltype');
		$sserviceid= $this->getUtildata()->getVaueOfArray($row,'sserviceid');
		$datasource='dbdoctrine';
		$typedata='multiple'; //single | multiple
		//$sql= $this->getUtildata()->getVaueOfArray($row,'content');
		
		if($sqltype=='nativesqlservice'){$datasource='servicesql';}
		else if($sqltype=='doctrinesql'){$datasource='dbdoctrine';}
				
		$sql= $this->getUtildata()->getVaueOfArray($row,'sqlcommand');
		$dconfig= $this->getUtildata()->getVaueOfArray($row,'dconfig');
		$entity= $this->getUtildata()->getVaueOfArray($row,'entity');
	    $dconfig = $this->getJson()->decode($dconfig, true);
	
		
		$filter = $this->getUtildata()->getVaueOfArray($dconfig,'filter');
		$sql=$this->replaceSqlExpression($sql,$filter);
		$filter=$this->addDefultfilter($filter,$formcofig);
		
		$checkparamfrequired=$this->checkRequiredFilter($filter,$dconfig);
		if(!$checkparamfrequired){return null;}
			
		if(!is_array($filter)){$filter=array();}
		$filter['entity']=$entity;
		/*if($sqltype=='nativesqlservice'){
			$filter['_datasource']='servicesql';
			$filter['_serviceid']=$sserviceid;
			
		}*/
		
		$qparam=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		if(is_array($qparam)){
			foreach ($qparam as $key => $value){
				if(!array_key_exists($key,$filter)){$filter[$key]=$value;}
			}
		}
		$overrideserviceid=$this->getUtildata()->getVaueOfArray($qparam,'_serviceid');
		
		if(!empty($overrideserviceid)){$sserviceid=$overrideserviceid;}
		
		$maxrecord=$this->getUtildata()->getVaueOfArray($filter,'maxrecord');
		if(empty($maxrecord)){$maxrecord=50;}
		$offset=0;
		$limit=$maxrecord;
		//$this->getSearch()->setDatasource($datasource);
		
		$dataservice=$this->getContainer()->get('badiu.sync.freport.freport.data');
		$factoryquery=$this->getContainer()->get('badiu.system.core.lib.sql.factoryquery');
        $factoryquery->setSessionhashkey($this->getSessionhashkey());
		
		$factorysqlexpressions=$this->getContainer()->get('badiu.system.core.lib.sql.factorysqlexpressions');
		$filter=$factorysqlexpressions->filter($filter);
					 
        $sql=$factoryquery->makeSqlSearch($sql,$filter,$datasource);
		
		$this->getSearch()->setDatasource($datasource);
		$result=null;
		 if($datasource=='dbdoctrine'){
			 $query = $dataservice->getEm()->createQuery($sql);
			 $query=$factoryquery->makeParamFilter($query,$sql,$filter);
			  $query->setMaxResults($maxrecord);
              $result= $query->getResult();
		 }else if($datasource=='servicesql'  && !empty($sserviceid)){
			 $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
			 $sqlservice->setServiceid($sserviceid);
			 $filter['_datasource']='servicesql';
			 
			 $result=$sqlservice->search($sql,$offset,$limit);
			
		 }else {return null;}
		return $result;
	}
	public function getDbDataSqlList($row,$formcofig) {
		
		$sqltype= $this->getUtildata()->getVaueOfArray($row,'sqltype');
		$sserviceid= $this->getUtildata()->getVaueOfArray($row,'sserviceid');
		$datasource='dbdoctrine';
		$typedata='multiple'; //single | multiple
		//$sql= $this->getUtildata()->getVaueOfArray($row,'content');
		
		if($sqltype=='nativesqlservicelist'){$datasource='servicesql';}
		else if($sqltype=='doctrinesqllist'){$datasource='dbdoctrine';}
				
		$sql= $this->getUtildata()->getVaueOfArray($row,'sqlcommand');
		$dconfig= $this->getUtildata()->getVaueOfArray($row,'dconfig');
		$entity= $this->getUtildata()->getVaueOfArray($row,'entity');
	    $dconfig = $this->getJson()->decode($dconfig, true);
	
		$type = $this->getUtildata()->getVaueOfArray($dconfig,'type');
		$size = $this->getUtildata()->getVaueOfArray($dconfig,'size');
		$scale = $this->getUtildata()->getVaueOfArray($dconfig,'scale');
		$filter = $this->getUtildata()->getVaueOfArray($dconfig,'filter');
		
		if(empty($scale)){$scale=1;}		
		if($scale > 1){$size=$size/$scale;}
		
		if($type=='dynamicdate' && !is_array($filter)){return null;}
		$filter['entity']=$entity;
		
		$qparam=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		if(is_array($qparam)){
			foreach ($qparam as $key => $value){
				if(!array_key_exists($key,$filter)){$filter[$key]=$value;}
			}
		}
		$overrideserviceid=$this->getUtildata()->getVaueOfArray($qparam,'_serviceid');
		
		if(!empty($overrideserviceid)){$sserviceid=$overrideserviceid;}
		
		$dataservice=$this->getContainer()->get('badiu.sync.freport.freport.data');
		$factoryquery=$this->getContainer()->get('badiu.system.core.lib.sql.factoryquery');
        $factoryquery->setSessionhashkey($this->getSessionhashkey());
		$factorysqlexpressions=$this->getContainer()->get('badiu.system.core.lib.sql.factorysqlexpressions');
		$sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
		$this->getSearch()->setDatasource($datasource);
		$result=array();
		
		if($type=='dynamicdate' && $size > 0 && $size < 90){
			
			$sql=$this->replaceSqlExpression($sql,$filter);
			$filter=$this->addDefultfilter($filter,$formcofig);
		
			$checkparamfrequired=$this->checkRequiredFilter($filter,$dconfig);
			//if(!$checkparamfrequired){return null;}
				
			$unittime=$type = $this->getUtildata()->getVaueOfArray($dconfig,'unittime');
			$dtime1 = $this->getUtildata()->getVaueOfArray($filter,'dtime1');
			$dtime2 = $this->getUtildata()->getVaueOfArray($filter,'dtime2');
			$currentmonth=0;
			$exec=true;
			$cont=0;
			$x=0;
			$y=0;
			while ($exec){
				if($scale <= 1){
					$x=$cont;
					$y=$cont;
				}else if($scale >= 1){
					if($cont==0){
						$x=$scale-1;
						$y=0;
					}else{
						$y=$x+1;
						$x=$x+$scale;
						
					}
				}
				
				$dt1=str_replace("{p}",$x,$dtime1);
				$dt2=str_replace("{p}",$y,$dtime2);
				$filter['dtime1']=$dt1;
				$filter['dtime2']=$dt2;
				
				$cfilter=$factorysqlexpressions->filter($filter);
				
				$csql=$factoryquery->makeSqlSearch($sql,$cfilter,$datasource);
				$itemresult=null;
				$label=null;
				$now=new \DateTime();
				if($unittime=='minutes'){
					$now->modify("-$y minutes");
					$label= $now->format('H:i');
				}else if($unittime=='hour'){
					$now->modify("-$y hour");
					$cthour = $now->format('H');
					$label=$this->getTranslator()->trans('badiu.system.time.hour.shortp',array('%x%'=>$cthour));
				}else if($unittime=='day'){
					$currentyear=$now->format('Y');
					$now->modify("-$y day");
					$ctday = $now->format('d');
					$nmonth=  $now->format('n');
					if($cont==0){$currentmonth= $now->format('n');}
					$label=$this->getTranslator()->trans('badiu.system.time.month.name.'.$nmonth);
					$label= $ctday."/".$label."/".$now->format('Y');;
					/*if($currentyear != $now->format('Y')){
						$label= $label."/".$now->format('Y');
					}*/
				}else if($unittime=='month'){
					if($cont==0){$currentmonth= $now->format('n');}
					$now->modify("-$y month");
					$nmonth=  $now->format('n');
					$label=$this->getTranslator()->trans('badiu.system.time.month.name.'.$nmonth);
					if($currentmonth < ($size +1) ){
						$label= $label."/".$now->format('Y');
					}
					//$label=$this->getTranslator()->trans('badiu.system.time.hour.shortp',array('%x%'=>$cthour));
				}
				
				if($datasource=='dbdoctrine'){
					$query = $dataservice->getEm()->createQuery($csql);
					$query=$factoryquery->makeParamFilter($query,$csql,$cfilter);
					$itemresult= $query->getSingleResult();
				}else if($datasource=='servicesql'  && !empty($sserviceid)){
					$sqlservice->setServiceid($sserviceid);
					$itemresult=$sqlservice->searchSingle($csql);
				}
				$itemdata=null;
				if(is_array($itemresult)){foreach ($itemresult as $v) {$itemdata= $v;}}
				$result[$cont]=array('name'=>$label,'value'=>$itemdata); 
				$cont++;
				if($cont > $size){$exec=false;}
			}
			krsort($result); 
		}else if($type=='listsql'){
			
			
			$listsql = $this->getJson()->decode($sql, true);
			
			$result=array();
			if(!is_array($listsql)){return $result;}
			$cont=0;
			
			
			foreach($listsql as $key=>$isql) {
				$isql=$this->replaceSqlExpression($isql,$filter);
			    $filter=$this->addDefultfilter($filter,$formcofig);
				
				$checkparamfrequired=$this->checkRequiredFilter($filter,$dconfig);
				if(!$checkparamfrequired){return null;}
				
				$cfilter=$factorysqlexpressions->filter($filter);
				$csql=$factoryquery->makeSqlSearch($isql,$cfilter,$datasource);
				
				$itemresult=null;
				$label=$this->getUtildata()->getVaueOfArray($dconfig,'label.listsql.'.$key,true);
				if($datasource=='dbdoctrine'){
					$query = $dataservice->getEm()->createQuery($csql);
					$query=$factoryquery->makeParamFilter($query,$csql,$filter);
					$itemresult= $query->getSingleResult();
				}else if($datasource=='servicesql'  && !empty($sserviceid)){
					$sqlservice->setServiceid($sserviceid);
					$itemresult=$sqlservice->searchSingle($csql);
				}
				if(is_array($itemresult)){foreach ($itemresult as $v) {$itemdata= $v;}}
				$result[$cont]=array('name'=>$label,'value'=>$itemdata); 
				$cont++;
			}
		}
		
		return $result;
		
	}
	
	public function addDefultfilter($fparam,$formcofig) {
		$sessionhashkey=$type = $this->getUtildata()->getVaueOfArray($formcofig,'sessionhashkey');
		$defaultdata=$type = $this->getUtildata()->getVaueOfArray($formcofig,'defaultdata');
		$defaultdataonopenform=$type = $this->getUtildata()->getVaueOfArray($formcofig,'defaultdataonopenform');
		
		$formdefaultdata = $this->getContainer()->get('badiu.system.core.lib.form.defaultdata');
        $formdefaultdata->setSessionhashkey($sessionhashkey);
		
        $formdefaultdata->setParam($defaultdata);
        $fparam = $formdefaultdata->setDefaultParam($fparam);
    	$formdefaultdata->setParam($defaultdataonopenform);
        $fparam = $formdefaultdata->setDefaultParamOnOpenForm($fparam);
		
        return $fparam;
	}
	public function checkRequiredFilter($fparam,$dconfig) {
		$requiredparam=$this->getUtildata()->getVaueOfArray($dconfig,'requiredparam');
		
		if(!is_array($requiredparam)){return true;}
		foreach($requiredparam as $kparam) {
			$value=$this->getUtildata()->getVaueOfArray($fparam,$kparam);
			if($value!=null){$value=trim($value);}
			if($value=="0"){}
			else if(empty($value)){return false;}
		} 
			
        return true;
	}
	public function makeHtmlTable($list,$param) {
		$name = $this->getUtildata()->getVaueOfArray($param,'name');
		$html="";
		
		  $lhead=$this->getTableHead($list,$param);
		  $headtable= $this->makeHtmlTableHead($lhead);
		   $html.="<table class=\"table table-striped\">$headtable";
		 if(is_array($list)){
		   foreach ($list as $item) {
			  
			    $html.="<tr>";
			    foreach ($lhead as $k =>$v) {
					$value=$this->getUtildata()->getVaueOfArray($item,$k);
					if($value!=null && is_numeric( $value) && strpos( $value, '.') !== false){$value=number_format($value, 2, ',', '.');}
					else if($value!=null && is_numeric( $value) && strpos( $value, '.') === false){$value=number_format($value, 0, ',', '.');}
					  $html.="<td>$value</td>"; 
				}
			   $html.="</tr>"; 
		   }
		 }
		   $html.="</table>";
		 
		   
		   $param['codecontent']=$html;
		$html=$this->defaultCard($param);
		   return $html;
	}
	
	public function getTableHead($list,$param) {
		 $head=array();
		 $dconfig= $this->getUtildata()->getVaueOfArray($param,'dconfig');
		 if(!is_array( $dconfig)){ $dconfig = $this->getJson()->decode($dconfig, true);}
	   $firstrow=null;
		if(is_array($list)){foreach ($list as $r) {$firstrow=$r;break;}}
		  	
		 if(!empty($firstrow) && is_array($firstrow)){
			foreach ($firstrow as $k =>$v) {
				$value=$this->getUtildata()->getVaueOfArray($dconfig,'label.'.$k,true);
				if(empty($value)){$value=$this->getTranslator()->trans($k);}
				$head[$k]=$value;
			}
		}
		
		return $head;
	}
	
	public function makeHtmlTableHead($list) {
		  $headtable="<thead><tr>";
		 if(!empty($list) && is_array($list)){
			foreach ($list as $k =>$v) {
				$headtable.=" <th>$v</th>";
			}
			}
		$headtable.="</thead></tr>";
		return $headtable;
	}
	
	
	public function makeChartPieSimple($list,$param) {
		$title = $this->getUtildata()->getVaueOfArray($param,'name');
		$id = $this->getUtildata()->getVaueOfArray($param,'id');
	
		$labels="";
		$series="";
		//if(empty($list)){return "";}
		$cont=0;
		
		 foreach ($list as $item) {
			 $name=$this->getUtildata()->getVaueOfArray($item,'name');
			 $value=$this->getUtildata()->getVaueOfArray($item,'value');
			 $separator="";
			 $name=$this->getJson()->escape($name);
			 if($cont > 0){$separator=", ";}
			 $series.="$separator$value";
			 $labels.=$separator ."'".$name."'";
			 $cont++;
		 }
		 
		$html="
		
		<div id=\"chart$id\"></div>
		
		<script> 
		var options$id = {
          series: [$series],
          chart: {
		  width: '100%',
          type: 'pie',
        },
		legend: {
              position: 'bottom',
			  floating: false,
			  horizontalAlign: 'left',
			 
		  },
        labels: [$labels],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom',
			  floating: true
		  }
          }
        }]
        };

		var chart = null;

		setTimeout(function(){
			var chart = new ApexCharts(document.querySelector(\"#chart$id\"), options$id);
        	chart.render();
		}, 100);

		</script>
		";
		$param['codecontent']=$html;
		$html=$this->defaultCard($param);
		return $html;
	}
	
	public function makeChartBarSimple($list,$param) {
		$title = $this->getUtildata()->getVaueOfArray($param,'name');
		$id = $this->getUtildata()->getVaueOfArray($param,'id');
	
		$labels="";
		$series="";
		//if(empty($list)){return "";}
		$cont=0;
		
		 foreach ($list as $item) {
			 $name=$this->getUtildata()->getVaueOfArray($item,'name');
			 $value=$this->getUtildata()->getVaueOfArray($item,'value');
			 $separator="";
			 $name=$this->getJson()->escape($name);
			 if($cont > 0){$separator=", ";}
			 $series.="$separator$value";
			 $labels.=$separator ."'".$name."'";
			 $cont++;
		 }
		 
		$html="
		
		<div id=\"chart$id\"></div>
		
		<script> 
		var options$id = {
          series: [{
          data: [$series]
        }],
          chart: {
          type: 'bar',
		  height: 350
        },
        plotOptions: {
          bar: {
            borderRadius: 8,
            horizontal: true,
			
          }
        },
        dataLabels: {
          enabled: true
        },
        xaxis: {
          categories: [$labels],
        }
        };

		var chart = null;

		setTimeout(function(){
			var chart = new ApexCharts(document.querySelector(\"#chart$id\"), options$id);
        	chart.render();
		}, 100);

		</script>
		";
		
		$param['codecontent']=$html;
		$html=$this->defaultCard($param);
		
		return $html;
	}
	
	public function makeChartAreaBasic($list,$param) {
		$title = $this->getUtildata()->getVaueOfArray($param,'name');
		$id = $this->getUtildata()->getVaueOfArray($param,'id');
	
		$labels="";
		$series="";
		//if(empty($list)){return "";}
		$cont=0;
		
		 foreach ($list as $item) {
			 $name=$this->getUtildata()->getVaueOfArray($item,'name');
			 $value=$this->getUtildata()->getVaueOfArray($item,'value');
			 $separator="";
			 $name=$this->getJson()->escape($name);
			 if($cont > 0){$separator=", ";}
			 $series.="$separator$value";
			 $labels.=$separator ."'".$name."'";
			 $cont++;
		 }
		 
		$html="
		
		<div id=\"chart$id\"></div>
		
		<script> 
		var options$id = {
          series: [{
			  name: '',
			  data: [$series]
          }],
          chart: {
          type: 'area',
		  height: 350,
		  zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
		stroke: {
          curve: 'straight'
        },
		 title: {
          text: '',
          align: 'left'
        },
        subtitle: {
          text: '',
          align: 'left'
        },
		labels: [$labels],
        
		yaxis: {
          opposite: true
        },
        legend: {
          horizontalAlign: 'left'
        }
        };

		var chart = null;

		setTimeout(function(){
			var chart = new ApexCharts(document.querySelector(\"#chart$id\"), options$id);
        	chart.render();
		}, 100);

		</script>
		";
		
		$param['codecontent']=$html;
		$html=$this->defaultCard($param);
		
		return $html;
	}
	
	public function makeChartLineBasic($list,$param) {
		$title = $this->getUtildata()->getVaueOfArray($param,'name');
		$id = $this->getUtildata()->getVaueOfArray($param,'id');
	
		$labels="";
		$series="";
		//if(empty($list)){return "";}
		$cont=0;
		
		 foreach ($list as $item) {
			 $name=$this->getUtildata()->getVaueOfArray($item,'name');
			 $value=$this->getUtildata()->getVaueOfArray($item,'value');
			 $separator="";
			 $name=$this->getJson()->escape($name);
			 if($cont > 0){$separator=", ";}
			 $series.="$separator$value";
			 $labels.=$separator ."'".$name."'";
			 $cont++;
		 }
		 
		$html="
		
		<div id=\"chart$id\"></div>
		
		<script> 
		var options$id = {
          series: [{
			  name: '',
			  data: [$series]
          }],
          chart: {
          type: 'line',
		  height: 350,
		  zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
		stroke: {
          curve: 'straight'
        },
		 title: {
          text: '',
          align: 'left'
        },
        subtitle: {
          text: '',
          align: 'left'
        },
		labels: [$labels],
        
	
        xaxis: {
          categories: [$labels],
        }
        };

		var chart = null;

		setTimeout(function(){
			var chart = new ApexCharts(document.querySelector(\"#chart$id\"), options$id);
        	chart.render();
		}, 100);

		</script>
		";
		
		$param['codecontent']=$html;
		$html=$this->defaultCard($param);
		
		return $html;
	}
	public function makeCard($list,$param) {
		$value=$this->getSingleValue($list,$param);
		$param['codecontent']=$value;
		$html=$this->defaultCard($param);
		
		return $html;
	}
	
	
	public function getSingleValue($list,$param) {
		 $dconfig= $this->getUtildata()->getVaueOfArray($param,'dconfig');
		 if(!is_array( $dconfig)){ $dconfig = $this->getJson()->decode($dconfig, true);}
		 
		 $datatype=$this->getUtildata()->getVaueOfArray($dconfig,'datatype');
	     if($datatype=='percentage'){
			 $item=array();
			 foreach ($list as $r) {$item=$r;break;}
			 $x=$this->getUtildata()->getVaueOfArray($item,'partialitem');
			 $y=$this->getUtildata()->getVaueOfArray($item,'totalitem');
			 
			 $percn=null;
			 if($x!=null && $y!=null){
             if($y >=0 && $x >=0 && $y>= $x){
                if($y==0){$percn=0;}
                else{
                    $percn= $x*100/$y;
                    $percn=number_format($percn, 2, ',', '.');
                }
				$percn.=" %";
             }
			 ;
			}
			return $percn;
		}else {
			$item=null;
			$value=null;
			
			 foreach ($list as $r) {$item=$r;break;}
			 if(is_array($item)){ foreach ($item as $r) {$value=$r;break;}}
			
			 if($value!=null && is_numeric( $value) && strpos( $value, '.') !== false){$value=number_format($value, 2, ',', '.');}
			 else if($value!=null && is_numeric( $value) && strpos( $value, '.') === false){$value=number_format($value, 0, ',', '.');}
			 return  $value;
			 //echo $value;exit;
		}
		return null;
	}
	
	
	public function defaultCard($param) {
		$title = $this->getUtildata()->getVaueOfArray($param,'name');
		$id = $this->getUtildata()->getVaueOfArray($param,'id');
		$shortname = $this->getUtildata()->getVaueOfArray($param,'shortname');
		$content = $this->getUtildata()->getVaueOfArray($param,'codecontent');
		 $dconfig= $this->getUtildata()->getVaueOfArray($param,'dconfig');
		 if(!is_array( $dconfig)){ $dconfig = $this->getJson()->decode($dconfig, true);}
		 $css=$this->getUtildata()->getVaueOfArray($dconfig,'classcss');
		 if(empty($css)){$css=" col ";}
		// text-center
		$html="<div class=\"$css  $shortname \">
				<div class=\"card\">
					<div class=\"card-header  text-center\">$title</div>
					<div class=\"card-body\"><p class=\"card-text text-center\">$content</p></div>
				 </div>
			 </div>
	      ";
	   
		return $html;
	}
	
	
	   public function changeAlternativenameWithTranslator($lisreportitem,$freportid){
       $list=array();
       $this->getTranslator()->setSystemdata($this->getSystemdata());
       if(!empty($lisreportitem)){
           if(!is_array($lisreportitem)){return $lisreportitem;}
		   if(sizeof($lisreportitem)==0){return $lisreportitem;}
          foreach ($lisreportitem as $row) {
               $id=$this->getUtildata()->getVaueOfArray($row,'id');
			   $name=$this->getUtildata()->getVaueOfArray($row,'name');
			   $alternativename= $this->getUtildata()->getVaueOfArray($row,'alternativename');
			   
			   if(!empty($alternativename)){
					$mkeyname='badiu.sync.freport.item.'.$id.'.name';
					$tname=$this->getTranslator()->trans($mkeyname);
				    if(!empty($tname) && $tname!=$mkeyname ){$row['alternativename']=$tname;}
				}
		
			
			   $mkeyname='badiu.sync.freport.freport.'.$freportid.'.name';
			   
			   $tname=$this->getTranslator()->trans($mkeyname);
			    
			    if(!empty($tname) && $tname!=$mkeyname ){$row['name']=$tname;}
			    
			   array_push($list,$row);
            
           }
       }
	   
       return $list;
   } 
}
