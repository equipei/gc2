<?php

namespace Badiu\Sync\FreportBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ManageSyncTag extends BadiuModelLib {

	function __construct(Container $container) {
                parent::__construct($container);
           
       }
	// system/service/process?_service=badiu.sync.freport.lib.managesynctag&_function=addTagFreport
	public function  addTagFreport(){
		//list discipline
		$fdata=$this->getContainer()->get('badiu.sync.freport.freport.data');
		$fparam=array('entity'=>1);
		$listd=$fdata->getGlobalColumnsValues('o.id,o.name',$fparam);
		if(empty($listd)){return null;}
		if(!is_array($listd)){return null;}
		$modulekey='badiu.sync.freport.freport';
		
		$tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
		$entity=1;
		$cont=0;
		foreach ($listd as $row) {
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			if(!empty($id ) && !empty($name )&& !empty($modulekey )){
				$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'dtype'=>'name');
				$paramadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','timecreated'=>new \DateTime());
				$paramedit=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','timemodified'=>new \DateTime());
				$tagdata->addNativeSql($paramcheckexist,$paramadd,$paramedit);
				$cont++;
			}
		}
		
		return $cont;
	}
	
}
?>