<?php

namespace Badiu\Sync\FreportBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class CoreConfig extends BadiuModelLib {

	  function __construct(Container $container) {
        parent::__construct($container);
     
    } 


	function getSiteTitle() {
		$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$freportid=$this->getUtildata()->getVaueOfArray($param,'_freportid');
		
		if(empty($freportid)){return null;}

		$freportdata=$this->getContainer()->get('badiu.sync.freport.freport.data');
		$fcparam=array('id'=>$freportid);
		
		
		$title=$freportdata->getGlobalColumnValue('name',$fcparam);
		
		$tmkey='badiu.sync.freport.freport.'.$freportid.'.name';  
		$ttitle=$this->getTranslator()->trans($tmkey);  
		if(!empty($ttitle) && $ttitle!=$tmkey ){$title=$ttitle;}
 
		return $title;
		
	}

}