<?php

namespace Badiu\Sync\FreportBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportChangeData;
class FreportItemConsolidatePeriodChangeData extends BadiuReportChangeData
{

    function __construct(Container $container) {
            parent::__construct($container);
              }
  public function all($data){
	  $table=$this->getUtildata()->getVaueOfArray($data, 'badiu_table1_rows');
	  $newtable=array();
	  if(is_array($table)){
		   foreach ($table as $row) {
			$row=$this->castContent($row);
			array_push($newtable,$row);
		}
	  }
	 
	   $data['badiu_table1_rows']=$newtable;
	/*echo "<pre>";
	print_r($newtable);echo "</pre>";exit;*/
		return $data;
	}  
	public function castContent($row){
		 $content=$this->getUtildata()->getVaueOfArray($row,'content');
		 $content= $this->getJson()->decode($content, true);
		 $periods= $this->getUtildata()->getVaueOfArray($content,'period');
		 $total=0;
		 if(!is_array($periods)){return $row;}
		  foreach ($periods as $k => $v) {
			  $row[$k]=$v;
			  $total+=$v;
		  }
		  $row['total']=$total;
     return $row;
      
  }   

}
