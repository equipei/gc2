<?php

namespace Badiu\Sync\FreportBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ItemData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

  public function getItemForAutocomplete() {
         $dfreportid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
         $name=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
         
        if(empty($dfreportid)) return null;

       
        $wsql="";
        if(!empty($name)){
            $wsql=" AND LOWER(CONCAT(r.id,r.name))  LIKE :name ";
            $name=strtolower($name);
        } 
		$sql="SELECT r.id,r.name FROM BadiuSyncFreportBundle:SyncFreport r WHERE r.entity=:entity AND r.deleted=:deleted AND r.dashboarditem=:dashboarditem $wsql AND (SELECT COUNT(i.id) FROM BadiuSyncFreportBundle:SyncFreportItem i WHERE i.freportid=r.id AND i.dfreportid=$dfreportid)=0 ORDER BY r.name ";
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('deleted',0);
		$query->setParameter('dashboarditem',1);
		
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(50);
        $result= $query->getResult();
        return  $result;
        }

public function getItemNameByIdOnEditAutocomplete($id) {
     
		$name=$this->getContainer()->get('badiu.sync.freport.freport.data')->getGlobalColumnValue('name',array('id'=>$id));;
        
        return $name;
  }
  
}
