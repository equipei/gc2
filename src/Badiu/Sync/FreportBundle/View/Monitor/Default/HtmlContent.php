<?php


/*echo "<pre>";
 print_r($page->getData());
 echo "</pre>";*/
 $instanceneme="";
$parentid = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
$freportid = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_freportid');
if($freportid){$parentid=$freportid;}

$accesslib=$container->get('badiu.sync.freport.freport.lib.accesslib');
$accesslib->setSessionhashkey($page->getSessionhashkey());
$fparam=array('freportid'=>$freportid);
if(!$freportid){$fparam=array('freportid'=>$parentid);}
$accessck=$accesslib->checkAutorization($fparam);
if(!$accessck->autoryzed){echo $accessck->message;exit;}

$stype = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('stype');
$itemid = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('itemid');

if(empty($stype)){$stype="freport";}
 
$sdata=$container->get('badiu.sync.freport.freport.data');

$datarow=null;
if($stype=='freport'){
	$datarow=$sdata->getGlobalColumnsValue('o.id AS freportid,o.serviceaddress,o.dtype,o.ftype,o.dconfig,o.name,o.dashboarditem,o.customtext1,o.customcss,o.customjs,o.param',array('id'=>$parentid));
}else if($stype=='freportitem'){
	$datarow=$sdata->getGlobalColumnsValue('o.id,o.serviceaddress,o.dtype,o.ftype,o.dconfig,o.name,o.customtext1',array('id'=>$itemid));
	$serviceaddress=$utildata->getVaueOfArray($datarow,'serviceaddress');
	
		$datarow1=$sdata->getGlobalColumnsValue('o.id,o.serviceaddress,o.dtype,o.ftype,o.dconfig',array('id'=>$parentid));
		$datarow['serviceaddress']=$utildata->getVaueOfArray($datarow1,'serviceaddress');
	
	$datarow['freportid']=$parentid;
	$datarow['itemid']=$itemid;
}

$dconfig =$utildata->getVaueOfArray($datarow,'dconfig');
$dconfig=$json->decode($dconfig, true);
$datarow['dconfig']=$dconfig;
$datarow['stype']=$stype;
$title=$utildata->getVaueOfArray($datarow,'name');
$customtext1=$utildata->getVaueOfArray($datarow,'customtext1');

$serviceaddress=$utildata->getVaueOfArray($datarow,'serviceaddress');
$dtype=$utildata->getVaueOfArray($datarow,'dtype');
if(empty($serviceaddress) && $dtype=='datasql'){$serviceaddress='badiu.sync.freport.freport.lib.factorysqlreport/make';}
if(empty($serviceaddress) && $dtype=='dashboarditemview'){$serviceaddress='badiu.sync.freport.freport.lib.factorymonitordefault/makeItemList';}

//get moduleinstance name
$serviceid = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
$courseid = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('courseid');
$itemparam = $utildata->getVaueOfArray($datarow,'param');
if(!empty($serviceid) && !empty($itemparam) && !empty($courseid)){
	$itemparam=$json->decode($itemparam, true);
	$accessby=$utildata->getVaueOfArray($itemparam,'accessby');
	if($accessby=="badiu.moodle.mreport.course.frontpage.index"){
		if($container->has('badiu.moodle.core.lib.datautil')){
			$moodledatautil=$container->get('badiu.moodle.core.lib.datautil');
			$moodledatautil->setSessionhashkey($page->getSessionhashkey());
			$instanceneme=$moodledatautil->getCourse($serviceid,$courseid);
		}
	}
}



//defult filter
$defaultdatafilterconfig=array();
$defaultdatafilterconfigonopenform=array();
if(isset($page->getData()['badiu_formconfig1_defaultdata'])){$defaultdatafilterconfig=$page->getData()['badiu_formconfig1_defaultdata'];}
if(isset($page->getData()['badiu_formconfig1_defaultdataonpenform'])){$defaultdatafilterconfigonopenform=$page->getData()['badiu_formconfig1_defaultdataonpenform'];}
$datarow['_formcofig']=array('defaultdata'=>$defaultdatafilterconfig,'defaultdataonopenform'=>$defaultdatafilterconfigonopenform,'sessionhashkey'=>$page->getSessionhashkey());	

$result=$container->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($serviceaddress,$datarow,$page->getSystemdata());

$prefixid = "";
if (!empty($freportid)) {
	$prefixid = "-" . $freportid;
}
 
//echo 	$customtext1;
$formfactoryhtmllib = $container->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap4');
$formfactoryhtmllib->setDprefix('ffilter.');
$formfieldto = $container->get('badiu.system.core.lib.form.field'); 

$formconfig1 = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig1');
$formfields1 = $utildata->getVaueOfArray($formconfig1, 'formfields');
  
$factoryformfilter=$container->get('badiu.theme.core.lib.template.vuejs.factoryform');
$factoryformfilter->setSessionhashkey($page->getSessionhashkey());
$factoryformfilter->setPage($page);
$formconfig=new stdClass();
$formconfig->showfirstitem=true;
$formconfig->onlyform=false;
$formconfig->formnumber=1;
$factoryformfilter->setConfig($formconfig);

$tmkey='badiu.sync.freport.freport.'.$freportid.'.name';  

 $translator->setSystemdata($page->getSystemdata());
$ttitle=$translator->trans($tmkey);  

 if(!empty($ttitle) && $ttitle!=$tmkey ){$title=$ttitle;}
?>
<div id="_badiu_theme_core_dashboard_vuejs<?php echo $prefixid; ?>" class="report">


 <?php if(!empty($instanceneme)){echo "<h2 class=\"report-intance-title\">$instanceneme</h2>";}; ?>
<h3 class="report-title"><?php echo $title;?></h3>
<div class="badiu-sync-freport-monitor" id="badiu-sync-freport-monitor-id">	
 <?php echo $factoryformfilter->exec(); ?>
 <?php echo $result; ?>

</div> 
</div>	
<?php 
if(isset($datarow['customcss'])){echo $datarow['customcss'];}
if(isset($datarow['customjs'])){echo $datarow['customjs'];}
?>