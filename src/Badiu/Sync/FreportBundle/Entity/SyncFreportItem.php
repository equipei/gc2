<?php

namespace Badiu\Sync\FreportBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * SyncFreportItem
 *
 * @ORM\Table(name="sync_freport_item", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="sync_freport_item_freportid_uix", columns={"entity", "freportid","dfreportid"}), 
 *      @ORM\UniqueConstraint(name="sync_freport_item_shortname_uix", columns={"entity", "shortname"}), 
 *      @ORM\UniqueConstraint(name="sync_freport_item_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="sync_freport_item_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="sync_freport_item_line_ix", columns={"line"}), 
 *              @ORM\Index(name="sync_freport_item_dfreportid_ix", columns={"dfreportid"}), 
 *              @ORM\Index(name="sync_freport_item_freportid_ix", columns={"freportid"}),
 *              @ORM\Index(name="sync_freport_item_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="sync_freport_item_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="sync_freport_item_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class SyncFreportItem
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var SyncFreport
     *
     * @ORM\ManyToOne(targetEntity="SyncFreport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="freportid", referencedColumnName="id", nullable=false)
     * })
     */
    private $freportid;
	
	    /**
     * @var SyncFreport
     *
     * @ORM\ManyToOne(targetEntity="SyncFreport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dfreportid", referencedColumnName="id", nullable=false)
     * })
     */
    private $dfreportid;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

  /**
     * @var integer
     *
     * @ORM\Column(name="line", type="bigint", nullable=false)
     */
    private $line;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
		 * @var string
		 *
		 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
		 */
  private $shortname;

    
  		      /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="float", precision=10, scale=0, nullable=true)
     */
    private $sortorder; 
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;
    
   
	
	/**
     * @var integer
     *
     * @ORM\Column(name="enable", type="integer", nullable=true)
     */
    private $enable;
	
	
	
     /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
	
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

 
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
	
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    
    function getId() {
        return $this->id;
    }

    function getFreportid() {
        return $this->freportid;
    }

    function getEntity() {
        return $this->entity;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function getDtype() {
        return $this->dtype;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setFreportid(SyncFreport $freportid) {
        $this->freportid = $freportid;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


function getDfreportid() {
        return $this->dfreportid;
    }

function setDfreportid(SyncFreport $dfreportid) {
        $this->dfreportid = $dfreportid;
    }
	
function getEnable() {
        return $this->enable;
    }
	
	 function setEnable($enable) {
        $this->enable = $enable;
    }
	
	 public function getSortorder() {
        return $this->sortorder;
    }

    public function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }
	
	 function setLine($line) {
            $this->line = $line;
        }
	function getLine() {
            return $this->line;
        }

}
