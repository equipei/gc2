<?php

namespace Badiu\Sync\FreportBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * SyncFreport
 *
 * @ORM\Table(name="sync_freport", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="sync_freport_name_dkey", columns={"entity", "dkey"}), 
 *      @ORM\UniqueConstraint(name="sync_freport_shortname_uix", columns={"entity", "shortname"}), 
 *      @ORM\UniqueConstraint(name="sync_freport_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="sync_freport_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="sync_freport_name_ix", columns={"name"}), 
 *              @ORM\Index(name="sync_freport_shortname_ix", columns={"shortname"}), 
 *              @ORM\Index(name="sync_freport_dkey_ix", columns={"dkey"}), 
 *              @ORM\Index(name="sync_freport_pkey_ix", columns={"pkey"}), 
 *              @ORM\Index(name="sync_freport_categoryid_ix", columns={"categoryid"}),
 *              @ORM\Index(name="sync_freport_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="sync_freport_sserviceid_ix", columns={"sserviceid"}),
 *              @ORM\Index(name="sync_freport_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="sync_freport_moduleinstance_ix", columns={"moduleinstance"}), 
 *              @ORM\Index(name="sync_freport_dashboarditem_ix", columns={"dashboarditem"}),
*              @ORM\Index(name="sync_freport_ctype_ix", columns={"ctype"}),
*              @ORM\Index(name="sync_freport_levelaccess_ix", columns={"levelaccess"}), 
 *              @ORM\Index(name="sync_freport_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="sync_freport_customint1_ix", columns={"customint1"}),
 *              @ORM\Index(name="sync_freport_customint2_ix", columns={"customint2"}),
 *              @ORM\Index(name="sync_freport_customint3_ix", columns={"customint3"}),
 *              @ORM\Index(name="sync_freport_customchar1_ix", columns={"customchar1"}),
 *              @ORM\Index(name="sync_freport_customchar2_ix", columns={"customchar2"}),
 *              @ORM\Index(name="sync_freport_customchar3_ix", columns={"customchar3"}) ,
 *              @ORM\Index(name="sync_freport_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class SyncFreport
{ 
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var SyncFreportCategory
     *
     * @ORM\ManyToOne(targetEntity="SyncFreportCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;
    
         /**
     * @var \Badiu\Admin\ServerBundle\Entity\AdminServerService
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ServerBundle\Entity\AdminServerService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sserviceid", referencedColumnName="id")
     * })
     */
    private $sserviceid;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
		 * @var string
		 *
		 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
		 */
  private $shortname;

    
   
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype='chartbarsimple'; //table | chartbar | chartpie | card 
						//chartlinesimple | chartbarsimple | chartpiesimple |inforsimple | tablesimple | tableitemconsolidate
    
	    /**
     * @var string
     *
     * @ORM\Column(name="ftype", type="string", length=50, nullable=true)
     */
    private $ftype;  //dashboarddataview | datalistcron | datalistsql | datasql | datakey | datacron
   
	    /**
     * @var string
     *
     * @ORM\Column(name="sqltype", type="string", length=50, nullable=true)
     */
    private $sqltype; // nativesql | doctrinesql
	
	/**
     * @var string
     *
     * @ORM\Column(name="ctype", type="string", length=50, nullable=true)
     */
    private $ctype; // free | commercial
	
	    /**
     * @var string
     *
     * @ORM\Column(name="serviceaddress", type="string", length=255, nullable=true)
     */
    private $serviceaddress;
	
	    /**
     * @var string
     *
     * @ORM\Column(name="dkey", type="string", length=255, nullable=true)
     */
    private $dkey;
	
		    /**
	  * parent key		
     * @var string
     *
     * @ORM\Column(name="pkey", type="string", length=255, nullable=true)
     */
    private $pkey;
	/**
     * @var integer
     *
     * @ORM\Column(name="enable", type="integer", nullable=true)
     */
    private $enable;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="levelaccess", type="integer", nullable=true)
     */
    private $levelaccess; // 100 - global | 200 - entity | 300 - moduleinstance
	
	/**
     * @var integer
     *
     * @ORM\Column(name="dashboarditem", type="integer", nullable=true)
     */
    private $dashboarditem;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="text", nullable=true)
	 */
	private $content;
	
		      /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="float", precision=10, scale=0, nullable=true)
     */
    private $sortorder;
	
		/**
     * @var string
     *
     * @ORM\Column(name="customcss", type="text", nullable=true)
     */
    private $customcss;
	
	/**
     * @var string
     *
     * @ORM\Column(name="customjs", type="text", nullable=true)
     */
    private $customjs;
	
     /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
	
	
 /**
     * @var string
     *
     * @ORM\Column(name="sqlcommand", type="text", nullable=true)
     */
    private $sqlcommand;
	
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
	
	/**
     * @var string
     *
     * @ORM\Column(name="defaultimage", type="string", length=255, nullable=true)
     */
    private $defaultimage;
	
	
       /**
     * @var integer
     *
     * @ORM\Column(name="customint1", type="bigint",  nullable=true)
     */
    private $customint1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint2", type="bigint",  nullable=true)
     */
    private $customint2;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint3", type="bigint",  nullable=true)
     */
    private $customint3;
	
	/**
     * @var string
     *
     * @ORM\Column(name="customchar1", type="string", length=255, nullable=true)
     */
    private $customchar1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="customchar2", type="string", length=255, nullable=true)
     */
    private $customchar2;
    
     
    
    /**
     * @var string
     *
     * @ORM\Column(name="customchar3", type="string", length=255, nullable=true)
     */
    private $customchar3;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="customtext1", type="text", nullable=true)
     */
    private $customtext1;
    
     /**
     * @var string
     *
     * @ORM\Column(name="customtext2", type="text", nullable=true)
     */
    private $customtext2;
    
     /**
     * @var string
     *
     * @ORM\Column(name="customtext3", type="text", nullable=true)
     */
    private $customtext3;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    function getId() {
        return $this->id;
    }

    function getCategoryid() {
        return $this->categoryid;
    }

    function getEntity() {
        return $this->entity;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function getDtype() {
        return $this->dtype;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCategoryid(SyncFreportCategory $categoryid) {
        $this->categoryid = $categoryid;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getSserviceid() {
        return $this->sserviceid;
    }

    function setSserviceid($sserviceid) {
        $this->sserviceid = $sserviceid;
    }


function getEnable() {
        return $this->enable;
    }
	
	 function setEnable($enable) {
        $this->enable = $enable;
    }
	
	 public function getSortorder() {
        return $this->sortorder;
    }

    public function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }
	
	 function setContent($content) {
            $this->content = $content;
        }
	function getContent() {
            return $this->content;
        }
		
	 function setServiceaddress($serviceaddress) {
            $this->serviceaddress = $serviceaddress;
        }
	function getServiceaddress() {
            return $this->serviceaddress;
        }	


    function setFtype($ftype) {
        $this->ftype = $ftype;
    }

 function getFtype() {
        return $this->ftype;
    }

 function setSqltype($sqltype) {
        $this->sqltype = $sqltype;
    }

 function getSqltype() {
        return $this->sqltype;
    }
	
	function setSqlcommand($sqlcommand) {
        $this->sqlcommand = $sqlcommand;
    }

 function getSqlcommand() {
        return $this->sqlcommand;
    }
	function setDkey($dkey) {
        $this->dkey = $dkey;
    }

 function getDkey() {
        return $this->dkey;
    }

	function setPkey($pkey) {
        $this->pkey = $pkey;
    }

 function getPkey() {
        return $this->pkey;
    }
 function getDefaultimage() {
        return $this->defaultimage;
    }

    function setDefaultimage($defaultimage) {
        $this->defaultimage = $defaultimage;
    }
	
	
	 function getModulekey() {
            return $this->modulekey;
        }

        function getModuleinstance() {
            return $this->moduleinstance;
        }
		
		 function setModulekey($modulekey) {
            $this->modulekey = $modulekey;
        }

        function setModuleinstance($moduleinstance) {
            $this->moduleinstance = $moduleinstance;
        }
		
		function setDashboarditem($dashboarditem) {
        $this->dashboarditem = $dashboarditem;
    }

 function getDashboarditem() {
        return $this->dashboarditem;
    }

/**
     * @return string
     */
    public function getCustomcss() {
        return $this->customcss;
    }

    /**
     * @param string $customcss
     */
    public function setCustomcss($customcss) {
        $this->customcss = $customcss;
    }
	
	/**
     * @return string
     */
    public function getCustomjs() {
        return $this->customjs;
    }

    /**
     * @param string $customjs
     */
    public function setCustomjs($customjs) {
        $this->customjs = $customjs;
    }
	
		function setCtype($ctype) {
        $this->ctype = $ctype;
    }

 function getCtype() {
        return $this->ctype;
    }

		function setLevelaccess($levelaccess) {
        $this->levelaccess = $levelaccess;
    }

 function getLevelaccess() {
        return $this->levelaccess;
    }

 function getCustomint1() {
        return $this->customint1;
    }

    function getCustomint2() {
        return $this->customint2;
    }

    function getCustomint3() {
        return $this->customint3;
    }

  function getCustomchar1() {
        return $this->customchar1;
    }

    function getCustomchar2() {
        return $this->customchar2;
    }

    function getCustomchar3() {
        return $this->customchar3;
    }

 function getCustomtext1() {
        return $this->customtext1;
    }

    function getCustomtext2() {
        return $this->customtext2;
    }

    function getCustomtext3() {
        return $this->customtext3;
    }

function setCustomint1($customint1) {
        $this->customint1 = $customint1;
    }

    function setCustomint2($customint2) {
        $this->customint2 = $customint2;
    }

    function setCustomint3($customint3) {
        $this->customint3 = $customint3;
    }

  function setCustomchar1($customchar1) {
        $this->customchar1 = $customchar1;
    }

    function setCustomchar2($customchar2) {
        $this->customchar2 = $customchar2;
    }

    function setCustomchar3($customchar3) {
        $this->customchar3 = $customchar3;
    }

 function setCustomtext1($customtext1) {
        $this->customtext1 = $customtext1;
    }

    function setCustomtext2($customtext2) {
        $this->customtext2 = $customtext2;
    }

    function setCustomtext3($customtext3) {
        $this->customtext3 = $customtext3;
    }	
}
