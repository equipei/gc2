<?php

namespace Badiu\Sync\FreportBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * SyncFreport
 *
 * @ORM\Table(name="sync_freport_data", uniqueConstraints={
*      @ORM\UniqueConstraint(name="sync_freport_data_shortname_uix", columns={"entity", "freportid", "shortname"}), 
 *      @ORM\UniqueConstraint(name="sync_freport_data_idnumber_uix", columns={"entity", "freportid", "idnumber"})},
 *       indexes={@ORM\Index(name="sync_freport_data_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="sync_freport_data_name_ix", columns={"name"}), 
 *              @ORM\Index(name="sync_freport_data_shortname_ix", columns={"shortname"}), 
 *              @ORM\Index(name="sync_freport_data_freportid_ix", columns={"freportid"}),
 *              @ORM\Index(name="sync_freport_data_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="sync_freport_data_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="sync_freport_data_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class SyncFreportData
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var SyncFreport
     *
     * @ORM\ManyToOne(targetEntity="SyncFreport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="freportid", referencedColumnName="id")
     * })
     */
    private $freportid;
	
	      /**
     * @var \Badiu\Admin\ServerBundle\Entity\AdminServerService
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ServerBundle\Entity\AdminServerService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sserviceid", referencedColumnName="id")
     * })
     */
    private $sserviceid;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
		 * @var string
		 *
		 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
		 */
  private $shortname;

    
  		      /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="float", precision=10, scale=0, nullable=true)
     */
    private $sortorder; 
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ftype", type="string", length=50, nullable=true)
     */
    private $ftype;  //dashboarddataview | datalistcron | datalistsql | datasql | datacron
   
	    /**
     * @var string
     *
     * @ORM\Column(name="sqltype", type="string", length=50, nullable=true)
     */
    private $sqltype; // nativesql | doctrinesql
	
    
	    /**
     * @var string
     *
     * @ORM\Column(name="serviceaddress", type="string", length=255, nullable=true)
     */
    private $serviceaddress;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="enable", type="integer", nullable=true)
     */
    private $enable;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="text", nullable=true)
	 */
	private $content;
	

	
     /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
	
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

 /**
     * @var string
     *
     * @ORM\Column(name="sqlcommand", type="text", nullable=true)
     */
    private $sqlcommand;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
	
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    
    function getId() {
        return $this->id;
    }

    function getFreportid() {
        return $this->freportid;
    }

    function getEntity() {
        return $this->entity;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function getDtype() {
        return $this->dtype;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setFreportid(SyncFreport $freportid) {
        $this->freportid = $freportid;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }



function getEnable() {
        return $this->enable;
    }
	
	 function setEnable($enable) {
        $this->enable = $enable;
    }
	
	 public function getSortorder() {
        return $this->sortorder;
    }

    public function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }
	
	 function setContent($content) {
            $this->content = $content;
        }
	function getContent() {
            return $this->content;
        }
		
	 function setServiceaddress($serviceaddress) {
            $this->serviceaddress = $serviceaddress;
        }
	function getServiceaddress() {
            return $this->serviceaddress;
        }	

 function setFtype($ftype) {
        $this->ftype = $ftype;
    }

 function getFtype() {
        return $this->ftype;
    }

 function setSqltype($sqltype) {
        $this->sqltype = $sqltype;
    }

 function getSqltype() {
        return $this->sqltype;
    }	
	
	function setSqlcommand($sqlcommand) {
        $this->sqlcommand = $sqlcommand;
    }

 function getSqlcommand() {
        return $this->sqlcommand;
    }	
	
	  function getSserviceid() {
        return $this->sserviceid;
    }

    function setSserviceid($sserviceid) {
        $this->sserviceid = $sserviceid;
    }

}
