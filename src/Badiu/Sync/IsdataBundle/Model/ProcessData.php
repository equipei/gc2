<?php

namespace Badiu\Sync\IsdataBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class ProcessData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
     public function existByKey($entity,$execmodulekey,$modulekey,$moduleinstance) {
           $r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.execmodulekey= :execmodulekey  AND o.modulekey= :modulekey AND o.moduleinstance= :moduleinstance ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
	    $query->setParameter('execmodulekey',$execmodulekey);
            $query->setParameter('modulekey',$modulekey);
	    $query->setParameter('moduleinstance',$moduleinstance);
            $result= $query->getSingleResult();
            if($result['countrecord']>0){$r=TRUE;}
            return $r;
     } 
     
          public function findByKey($entity,$execmodulekey,$modulekey,$moduleinstance) {
            $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.execmodulekey= :execmodulekey  AND o.modulekey= :modulekey AND o.moduleinstance= :moduleinstance ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
	   $query->setParameter('execmodulekey',$execmodulekey);
            $query->setParameter('modulekey',$modulekey);
	    $query->setParameter('moduleinstance',$moduleinstance);
            $result= $query->getSingleResult();
            return $result;
     }
      public function getIdByKey($entity,$execmodulekey,$modulekey,$moduleinstance) {
            $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.execmodulekey= :execmodulekey  AND o.modulekey= :modulekey AND o.moduleinstance= :moduleinstance ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
	   $query->setParameter('execmodulekey',$execmodulekey);
            $query->setParameter('modulekey',$modulekey);
	    $query->setParameter('moduleinstance',$moduleinstance);
             $result= $query->getSingleResult();
            $result=$result['id'];
             return $result;
     }
     public function start($param) {
           
            $entity=null;
            $execmodulekey=null;
            $modulekey=null;
            $moduleinstance=null;
            $moduleinstancename=null;
            $timestart=null;
            $id=null;
            if(isset($param['entity'])) {$entity=$param['entity'];}
            if(isset($param['execmodulekey'])){$execmodulekey=$param['execmodulekey'];}
            if(isset($param['modulekey'])){$modulekey=$param['modulekey'];}
            if(isset($param['moduleinstance'])){$moduleinstance=$param['moduleinstance'];}
            if(isset($param['moduleinstancename'])){$moduleinstancename=$param['moduleinstancename'];}
            if(isset($param['processtimestart'])){$timestart=$param['processtimestart'];}
          
            $exist=$this->existByKey($entity,$execmodulekey,$modulekey,$moduleinstance);
             
            if($exist){
                $id=$this->getIdByKey($entity,$execmodulekey,$modulekey,$moduleinstance);
                $param=array('id'=>$id,'status'=>'processing','timestart'=>$timestart);
                $result=$this->updateNativeSql($param,false);
            }
            else {
                $param=array('entity'=>$entity,'execmodulekey'=>$execmodulekey,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'moduleinstancename'=>$moduleinstancename,'status'=>'processing','timestart'=> $timestart);
                $id=$this->insertNativeSql($param,false);
            }
           return $id;
        }
      /* public function start($param) {
           
            $entity=null;
            $execmodulekey=null;
            $modulekey=null;
            $moduleinstance=null;
            
            $dto=null;
            if(isset($param['entity'])) {$entity=$param['entity'];}
            if(isset($param['execmodulekey'])){$execmodulekey=$param['execmodulekey'];}
            if(isset($param['modulekey'])){$modulekey=$param['modulekey'];}
            if(isset($param['moduleinstance'])){$moduleinstance=$param['moduleinstance'];}
          
            $exist=$this->existByKey($entity,$execmodulekey,$modulekey,$moduleinstance);
            if($exist){$dto=$this->findByKey($entity,$execmodulekey,$modulekey,$moduleinstance);}
            else {
                $dto=$this->getContainer()->get('badiu.sync.isdata.process.entity');
                $dto->setEntity($entity);
                $dto->setExecmodulekey($execmodulekey);
                $dto->setModulekey($modulekey);
                $dto->setModuleinstance($moduleinstance);
             }
           
            if(isset($param['moduleinstancename'])){ $dto->setModuleinstancename($param['moduleinstancename']);}
            $dto->setStatus('processing');
            $dto->setTimestart(new \Datetime());
            $this->setDto($dto);
            $this->save();
            return  $this->getDto();
        }*/
    public function end($param) {
              $processid=null;
              $timestart=null;
              $timeend=new \Datetime();
              $datashouldexec=0;
              $dataexec=0;
              if(isset($param['processid'])){$processid=$param['processid'];}
              if(isset($param['processtimestart'])){$timestart=$param['processtimestart'];}
              
              if(isset($param['datashouldexec'])){$datashouldexec=$param['datashouldexec'];}
              if(isset($param['dataexec'])){$dataexec=$param['dataexec'];}
              
            
              $tstart=$timestart->getTimestamp();
              $tend=$timeend->getTimestamp();
              $timeexec=$tend-$tstart; 
              $param=array('id'=>$processid,'status'=>'success','timeend'=>$timeend,'timeexec'=>$timeexec,'datashouldexec'=>$datashouldexec,'dataexec'=>$dataexec);
              $result=$this->updateNativeSql($param,false);
             return  $result;
        }
}
