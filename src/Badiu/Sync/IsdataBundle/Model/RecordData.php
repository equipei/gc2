<?php

namespace Badiu\Sync\IsdataBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class RecordData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
        
   public function countByProcessid($processid,$lastid=null) {
            $wsqllastid="";
            if(!empty($lastid)){$wsqllastid=" AND o.moduleinstance > :moduleinstance";}
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE  o.processid= :processid  $wsqllastid ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('processid',$processid);
             if(!empty($lastid)){$query->setParameter('moduleinstance',$lastid);}
            $result= $query->getSingleResult();
            return $result['countrecord'];
     }
    public function getLastModuleinstanceByProcessid($processid,$modulekey) {
            $sql="SELECT  MAX(o.moduleinstance) AS lastrecord FROM ".$this->getBundleEntity()." o  WHERE  o.processid= :processid  AND o.modulekey= :modulekey ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('processid',$processid);
            $query->setParameter('modulekey',$modulekey);
            $result= $query->getSingleResult();
            return $result['lastrecord'];
     }
    public function existByKey($processid,$modulekey,$moduleinstance) {
           $r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE  o.processid= :processid  AND o.modulekey= :modulekey AND o.moduleinstance= :moduleinstance ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('processid',$processid);
            $query->setParameter('modulekey',$modulekey);
	    $query->setParameter('moduleinstance',$moduleinstance);
            $result= $query->getSingleResult();
            if($result['countrecord']>0){$r=TRUE;}
            return $r;
     }
       public function add($param) {
            $modulekey=null;
            $moduleinstance=null;
            $moduleinstancename=null;
            $processid=null;
            $entity=null;
            $idnumber=null;
            $result=0;
            if(isset($param['processid'])) {$processid=$param['processid'];}
            if(isset($param['modulekey'])){$modulekey=$param['modulekey'];}
            if(isset($param['moduleinstance'])){$moduleinstance=$param['moduleinstance'];}
            if(isset($param['moduleinstancename'])){$moduleinstancename=$param['moduleinstancename'];}
            if(isset($param['entity'])) {$entity=$param['entity'];}
            if(isset($param['idnumber'])) {$idnumber=$param['idnumber'];}
         
            $exitparam=array('processid'=>$processid,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance);
            $countrecord=$this->countNativeSql($exitparam,false);
           
            if(!$countrecord){
                $addparam=array('processid'=>$processid,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'moduleinstancename'=>$moduleinstancename,'idnumber'=>$idnumber,'entity'=>$entity,'status'=>'unlock','lastidprecessed'=>0,'failed'=>0,'timecreated'=>new \Datetime(),'timemodified'=>new \Datetime() );
                $r=$this->insertNativeSql($addparam,false);
            if($r > 0){$result++;}
            }
            return $result;
       }
      
   
       public function start($param) {
           /* $dto->setStatus('processing');
            $dto->setTimestart(new \Datetime());
            $this->setDto($dto);
            $this->save();
            return $this->getDto();*/
        }
    public function end($param) {
             
            $id=null;
            $timestart=null;
            $timeend=new \Datetime();
            $result=0;
            if(isset($param['recordid'])) {$id=$param['recordid'];}
            if(isset($param['timestart'])) {$timestart=$param['timestart'];}
           
             
             $timeexec=$timeend->getTimestamp()-$timestart->getTimestamp();
             
             $paramup=array('id'=>$id,'timeend'=>$timestart,'status'=>'unlock','timeexec'=>$timeexec,'timemodified'=>new \Datetime());
           
            $result=$this->updateNativeSql($paramup,false);
           
            return  $result;
        }
   
       public function countByModuleKey($processid,$modulekey,$moduleinstance) {
          	$r=FALSE;
                $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.processid =:processid AND o.modulekey = :modulekey  AND o.moduleinstance = :moduleinstance";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('processid',$processid);
                $query->setParameter('modulekey',$modulekey);
                 $query->setParameter('moduleinstance',$moduleinstance);
                $result= $query->getSingleResult();
                if($result['countrecord']>0){$r=TRUE;}
                return $r;
        }
   
         public function getListToProcess($processid,$modulekey,$limit=200,$lastid=null) {
          	$wsqllastid="";
                if(!empty($lastid)){$wsqllastid=" AND o.moduleinstance > :moduleinstance";}
                $sql="SELECT  o.id,o.moduleinstance,o.idnumber FROM ".$this->getBundleEntity()." o  WHERE  o.processid =:processid AND o.modulekey = :modulekey  AND o.status=:status $wsqllastid ORDER BY o.timemodified ";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('processid',$processid);
                $query->setParameter('modulekey',$modulekey);
                $query->setParameter('status','unlock');
                if(!empty($lastid)){$query->setParameter('moduleinstance',$lastid);}
                $query->setFirstResult(0);
		$query->setMaxResults($limit);
                $result= $query->getResult();
                return $result;
        }
       public function lockList($list,$timestart) {
           $cont=0;    
           foreach ($list as $l) {
                    $id=$l['id'];
                    $param=array('id'=>$id,'status'=>'lock','timemodified'=>$timestart,'timelock'=>new \Datetime());
                    $result=$this->updateNativeSql($param,false);
                    if($result){ $cont++;}
               }
               return $cont;
          } 
     /* public function unlock($dto) {
            $dto->setStatus('unlock');
            $this->setDto($dto);
            $dto->setTimemodified(new \Datetime());
            $dto->setTimeupdate(new \Datetime());
            $this->save();
            return $this->getDto()->getId();
          } */
}

