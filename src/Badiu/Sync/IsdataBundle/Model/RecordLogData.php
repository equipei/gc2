<?php

namespace Badiu\Sync\IsdataBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class RecordLogData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
     
       public function start($param) {
          
            $recordid=null;
            $entity=null;
          
            $result=0;
            if(isset($param['recordid'])) {$recordid=$param['recordid'];}
            if(isset($param['entity'])) {$entity=$param['entity'];}
            $timestart=new \Datetime();
           $addparam=array('recordid'=>$recordid,'entity'=>$entity,'status'=>'processing','failed'=>0,'timestart'=> $timestart );
           $id=$this->insertNativeSql($addparam,false);
           $result=array('id'=>$id,'timestart'=>$timestart);
           return $result;
        }
        public function startList($param,$list) {
            
            $result=array();
            foreach ($list as $l) {
                $recordid=$l['id'];
                $param['recordid']=$l['id'];
               $result[$recordid]=$this->start($param);
            }
           return $result;
        }
    public function end($param) {
        
            $id=null;
            $timestart=null;
            $timeend=new \Datetime();
            $result=0;
            if(isset($param['id'])) {$id=$param['id'];}
            if(isset($param['timestart'])) {$timestart=$param['timestart'];}
           
             
             $timeexec=$timeend->getTimestamp()-$timestart->getTimestamp();
             $paramup=array('id'=>$id,'timeend'=>$timestart,'status'=>'success','timeexec'=>$timeexec);
            $result=$this->updateNativeSql($paramup,false);
            return  $result;
        }
}
