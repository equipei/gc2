<?php

namespace Badiu\Sync\IsdataBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class SyncBaseLib    extends BadiuModelLib{
   
    private $execmodulekey;
    private $modulekey;
    private $moduleinstance;   
    
   private $datatargetrecord;
   
   private $datasourcerecord;
     private $modulekeyrecord;
  /**
     * @var integer
     */
  private $maxrecordtosync;
    /**
     * @var integer
     */
  private $maxrecordtosyncperconn;
  
    /**
     * @var integer
     */
  private $maxrecordtoprocess;
    /**
     * @var integer
     */
  private $maxrecordtoprocessperconn;
  
      /**
     * @var object
     */
  private $mongoutil;

         /**
     * @var object
     */
  private $mongocollection; 
  
    function __construct(Container $container) {
                parent::__construct($container);
                    $this->maxrecordtosync=50000;
                    $this->maxrecordtosyncperconn=2500;
                    $this->maxrecordtoprocess=50000; 
                    $this->maxrecordtoprocessperconn=2500;
                   // $this->mongoutil=$this->getContainer()->get('badiu.system.core.lib.mongodb.util');
              }
              
              
         public  function countPaging($dbrecord,$limit) {
            $countpaging=$dbrecord/$limit;
            if($countpaging != round($countpaging)){
                $countpaging=floor($countpaging)+1;
            }
            return $countpaging;
        }
       
          public function run($tasklog=10) {
        $t1=time();
         //get list of moodle
       $this->getSearch()->getKeymanger()->setBaseKey('badiu.gmoodle.mdl.sitelistforsync');
       $param=array();
       $param['execcron']=1;
       $param['deleted']=0;
       $moodlelist = $this->getSearch()->searchList($param);
       if(!isset($moodlelist[1])) return null;
       $proccessdata=$this->getContainer()->get('badiu.sync.isdata.process.data');
       $proccesslogdata=$this->getContainer()->get('badiu.sync.isdata.processlog.data');
       $cont=0;
        foreach ($moodlelist[1] as $moodle) {
            $entity=$moodle['entity'];
           $modulekey=$this->getModulekey();
           $moduleinstance=$moodle['id'];
           $moduleinstancename=$moodle['name'];
              
           $param=array();
           $param['entity']=$entity;
           $param['tasklog']=$tasklog;
           $param['execmodulekey']=$this->getExecmodulekey();
           $param['modulekey']=$modulekey;
           $param['moduleinstance']=$moduleinstance;
           $param['moduleinstancename']=$moduleinstancename;
            $param['processtimestart']=new \Datetime();
            //add proccess
           $processid=$proccessdata->start($param);
           $param['processid']=$processid;
           $param['processlogtimestart']=new \Datetime();
           $processlogid=$proccesslogdata->start($param);
           $param['processlogid']=$processlogid;
           //control new record
           $resultrecupdate=null;
          
           $resultrecupdate=$this->updateRecord($param);
         
          $param['datashouldexec']=$resultrecupdate['datashouldexec'];
          $param['dataexec']=$resultrecupdate['dataexec'];
          
          $this->process($param);
            //sync record  
           echo "------------------------- \n\n id $moduleinstance Moodle x: $moduleinstancename    ";
           
           //update record
            
           // end process
           $proccesslogdata->end($param);
           $proccessdata->end($param);
          
           $cont++;
          //  if($cont==1){break;}    
       } 
        //inser into mongodb
        
        //sync data
        $t2=time();     
        echo "<hr> \n exec:".$resultrecupdate['dataexec']."    timestar: ". date('Y-m-d h:i:s', $t1). " timeend: ". date('Y-m-d h:i:s', $t2)  ;  
    }
    
     public function updateRecord($param) {
             $processid=null;
             $modulekey=null;
             $moduleinstance=null;
             $result=0;
            if(isset($param['processid'])){$processid=$param['processid'];}
            if(isset($param['modulekey'])){$modulekey=$param['modulekey'];}
            if(isset($param['moduleinstance'])){$moduleinstance=$param['moduleinstance'];}
            
              
             //count record
             $counttargetrecord=$this->datatargetrecord->countByProcessid($processid);
             $countsourcerecord=$this->datasourcerecord->countByMoodleid($moduleinstance);
             echo "counttargetrecord: $counttargetrecord | countsourcerecord: $countsourcerecord";
             $lastid=0;
             if($countsourcerecord==0) {return $result;}
             if($counttargetrecord==$countsourcerecord){return $result;}
             if($counttargetrecord > $countsourcerecord){/*delete all record of target*/;}
             
              if($counttargetrecord > 0){
                  $lastid=$this->datatargetrecord->getLastModuleinstanceByProcessid($processid,$this->getModulekeyrecord());
                  $countsourcerecord=$this->datasourcerecord->countByMoodleid($moduleinstance,$lastid);
              }
              
             $paging=$this->countPaging($countsourcerecord,$this->getMaxrecordtosyncperconn());
             
             echo "\n\n LAST DATA: paging: $paging lastid:$lastid  |counttargetrecord: $counttargetrecord | countsourcerecord: $countsourcerecord";
           
             $nparam=$param;
             $processed=0;
            
             for ($i = 0; $i < $paging; $i++) {
                  if($i>0){$lastid=$this->datatargetrecord->getLastModuleinstanceByProcessid($processid,$this->getModulekeyrecord());}
                  $rows=$this->datasourcerecord->getSyncList($moduleinstance,$lastid,$this->getMaxrecordtosyncperconn());
                  $controw=0;
                
                  foreach ($rows as $row) {
                     
                    $nparam['modulekey']=$this->getModulekeyrecord();
                    $nparam['moduleinstance']=$row['id'];
                    $nparam['moduleinstancename']=$row['name'];
                    $result+=$this->datatargetrecord->add($nparam);  
                    $processed++;
                    $controw++;
                }
                if($controw==0){break;}
                 if($processed>=$this->getMaxrecordtosync()){break;}
             }  
            
             $rdata=  array('datashouldexec'=>$processed,'dataexec'=>$result);
             return  $rdata;
        }
     public function process($param) {
         
         $recordlogdata=$this->getContainer()->get('badiu.sync.isdata.recordlog.data');
         $result=0;
         $modulekey= $this->getModulekeyrecord();
         $processid=null;
         if(isset($param['processid'])) {$processid=$param['processid'];}
          
         $counttargetrecord=$this->datatargetrecord->countByProcessid($processid);
         
         $paging=$this->countPaging($counttargetrecord,$this->getMaxrecordtoprocessperconn()); 
        //get last record modified that is not locked
        
        echo "\n upate mongo course paging: $paging ";
         $processed=0;
        
        for ($i = 0; $i < $paging; $i++) {
            $listtoprocess=$this->datatargetrecord->getListToProcess($processid,$modulekey,$this->getMaxrecordtoprocessperconn());
            $processed+=sizeof($listtoprocess);
            $controw=sizeof($listtoprocess);
             if($controw==0){break;}
            //lock record select
            $lockrecordtimestart=new \Datetime();
            $contlock=$this->datatargetrecord->lockList($listtoprocess,$lockrecordtimestart);
      
            //add log start
            $listlogprocess=$recordlogdata->startList($param,$listtoprocess);
       
            // update status for each recod  and unlock log record and record
            $result+=$this->sync($listtoprocess,$listlogprocess,$lockrecordtimestart);
            
            if($processed>=$this->getMaxrecordtoprocess()){break;}
           // echo "\n upate record paging: $i | result: $result |   processed: $processed ";
        }
      // echo "<hr>";
       
    
      return $result;
   }
    
    public function sync($listtoprocess,$listlogprocess,$lockrecordtimestart){
             if (empty($listtoprocess)) {return null;}
          
                $cont=0;
                $recordlogdata=$this->getContainer()->get('badiu.sync.isdata.recordlog.data');
                $moduleservice=$this->getContainer()->get($this->getExecmodulekey());
                foreach ($listtoprocess as $row){
                    
                    $recordid=$row['id'];
                   
                    $process=$moduleservice->processSync($row);
                   
                    $recordlogid=null;
                    $timestart=null;
                    if (array_key_exists($recordid,$listlogprocess)){
                            $recordlogid=$listlogprocess[$recordid]['id'];
                            $timestart=$listlogprocess[$recordid]['timestart'];
                    }
                    //end record log
                    $param=array('id'=>$recordlogid,'timestart'=>$timestart);
                    $result=$recordlogdata->end($param);
                         
                    //unlock record
                    $param=array('recordid'=>$recordid,'timestart'=>$lockrecordtimestart);
                    $result=$this->datatargetrecord->end($param);
                    if($result){$cont++;}
                      

                }
            
        return $cont;
    }   
    
   
        function getExecmodulekey() {
                  return $this->execmodulekey;
              }

              function getModulekey() {
                  return $this->modulekey;
              }

              function getModuleinstance() {
                  return $this->moduleinstance;
              }

              function setExecmodulekey($execmodulekey) {
                  $this->execmodulekey = $execmodulekey;
              }

              function setModulekey($modulekey) {
                  $this->modulekey = $modulekey;
              }

              function setModuleinstance($moduleinstance) {
                  $this->moduleinstance = $moduleinstance;
              }

              function getMaxrecordtosync() {
                  return $this->maxrecordtosync;
              }

              function getMaxrecordtoprocess() {
                  return $this->maxrecordtoprocess;
              }

              function setMaxrecordtosync($maxrecordtosync) {
                  $this->maxrecordtosync = $maxrecordtosync;
              }

              function setMaxrecordtoprocess($maxrecordtoprocess) {
                  $this->maxrecordtoprocess = $maxrecordtoprocess;
              }

              function getMaxrecordtosyncperconn() {
                  return $this->maxrecordtosyncperconn;
              }

              function getMaxrecordtoprocessperconn() {
                  return $this->maxrecordtoprocessperconn;
              }

              function setMaxrecordtosyncperconn($maxrecordtosyncperconn) {
                  $this->maxrecordtosyncperconn = $maxrecordtosyncperconn;
              }

              function setMaxrecordtoprocessperconn($maxrecordtoprocessperconn) {
                  $this->maxrecordtoprocessperconn = $maxrecordtoprocessperconn;
              }


              function getModulekeyrecord() {
                  return $this->modulekeyrecord;
              }

              function setModulekeyrecord($modulekeyrecord) {
                  $this->modulekeyrecord = $modulekeyrecord;
              }

              function getMongoutil() {
                  return $this->mongoutil;
              }

              function getMongocollection() {
                  return $this->mongocollection;
              }

              function setMongoutil($mongoutil) {
                  $this->mongoutil = $mongoutil;
              }

              function setMongocollection($mongocollection) {
                  $this->mongocollection = $mongocollection;
              }


              function getDatatargetrecord() {
                  return $this->datatargetrecord;
              }

              function getDatasourcerecord() {
                  return $this->datasourcerecord;
              }

              function setDatatargetrecord($datatargetrecord) {
                  $this->datatargetrecord = $datatargetrecord;
              }

              function setDatasourcerecord($datasourcerecord) {
                  $this->datasourcerecord = $datasourcerecord;
              }




}
