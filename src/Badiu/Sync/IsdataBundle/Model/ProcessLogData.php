<?php

namespace Badiu\Sync\IsdataBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class ProcessLogData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
    
     public function start($param) {
             $entity=null;
             $processid=null;
             $tasklogid=null;
             $timestart=null;
             $id=null;
            if(isset($param['entity'])) {$entity=$param['entity'];}
            if(isset($param['processid'])){$processid=$param['processid'];}
            if(isset($param['tasklogid'])){$tasklogid=$param['tasklogid'];}
            if(isset($param['processlogtimestart'])){$timestart=$param['processlogtimestart'];}
           
            $param=array('entity'=>$entity,'processid'=>$processid,'tasklogid'=>$tasklogid,'status'=>'processing','timestart'=>$timestart);
            $id=$this->insertNativeSql($param,false);
            return  $id;
        }
    public function end($param) {
            
              $logid=null;
              $timestart=null;
              $timeend=new \Datetime();
              $datashouldexec=0;
              $dataexec=0;
              
              if(isset($param['processlogid'])){$logid=$param['processlogid'];}
              if(isset($param['processlogtimestart'])){$timestart=$param['processlogtimestart'];}
               if(isset($param['datashouldexec'])){$datashouldexec=$param['datashouldexec'];}
              if(isset($param['dataexec'])){$dataexec=$param['dataexec'];}
              
              
              $tstart=$timestart->getTimestamp();
              $tend=$timeend->getTimestamp();
              $timeexec=$tend-$tstart; 
              $param=array('id'=>$logid,'status'=>'success','timeend'=>$timeend,'timeexec'=>$timeexec,'datashouldexec'=>$datashouldexec,'dataexec'=>$dataexec);
              $result=$this->updateNativeSql($param,false);
            return  $result;
        }
}
