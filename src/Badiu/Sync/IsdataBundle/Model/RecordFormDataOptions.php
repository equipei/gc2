<?php

namespace Badiu\System\SchedulerBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class RoleFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
    
    public  function getAction(){
        $list=array();
		// comentado só para atender sistuaçao do titanio temp
       // $list[DOMAINTABLE::$ROLE_ACTION_SEND_REPORT]=$this->getTranslator()->trans('badiu.system.scheduler.action.send.report');
        $list[DOMAINTABLE::$ROLE_ACTION_SEND_MESSAGE_TOUSERINREPORT]=$this->getTranslator()->trans('badiu.system.scheduler.action.send.menssage.touserinreport');
		//$list[DOMAINTABLE::$ROLE_ACTION_SEND_MESSAGE_GENERAL]=$this->getTranslator()->trans('badiu.system.scheduler.actionsend.menssage.general');
        return $list;
    }

	 public  function getStatus(){
        $list=array();
        $list[DOMAINTABLE::$TASK_STATUS_ACTIVE]=$this->getTranslator()->trans('badiu.system.scheduler.task.status.active');
        $list[DOMAINTABLE::$TASK_STATUS_INACTIVE]=$this->getTranslator()->trans('badiu.system.scheduler.task.status.inactive');
		return $list;
    }
}
