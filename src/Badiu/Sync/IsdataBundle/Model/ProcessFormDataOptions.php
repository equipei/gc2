<?php

namespace Badiu\Sync\IsdataBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Badiu\Sync\IsdataBundle\Model\DOMAINTABLE;
class ProcessFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
    
  
    public  function getStatus(){
        $list=array();
        $list[DOMAINTABLE::$STATUS_SUCCESS]=$this->getTranslator()->trans('badiu.sync.isdata.status.success');
        $list[DOMAINTABLE::$STATUS_PROCESSING]=$this->getTranslator()->trans('badiu.sync.isdata.status.processing');
	return $list;
    }
}
