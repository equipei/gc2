<?php

namespace Badiu\Sync\IsdataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * SyncIsdataProcess
 *
 * @ORM\Table(name="sync_isdata_process",uniqueConstraints={
 *      @ORM\UniqueConstraint(name="sync_isdata_process_uix", columns={"entity", "execmodulekey","modulekey","moduleinstance"})},
 *       indexes={@ORM\Index(name="sync_isdata_process_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="sync_isdata_process_execmodulekey_ix", columns={"execmodulekey"}), 
 *              @ORM\Index(name="sync_isdata_process_modulekey_ix", columns={"modulekey"}), 
 *              @ORM\Index(name="sync_isdata_process_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="sync_isdata_process_moduleinstancename_ix", columns={"moduleinstancename"}),
 *              @ORM\Index(name="sync_isdata_process_timestart_ix", columns={"timestart"}), 
 *              @ORM\Index(name="sync_isdata_process_timeend_ix", columns={"timeend"}),
 *              @ORM\Index(name="sync_isdata_process_status_ix", columns={"status"})})
 * @ORM\Entity
 */

class SyncIsdataProcess
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

     /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
      /**
     * @var string
     *
     * @ORM\Column(name="execmodulekey", type="string", length=255, nullable=false)
     */
    private $execmodulekey; //module/operation that exec sync control. Exemple gmoodle monitor login duplication
    
       /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=false)
     */
    private $modulekey; //module/operation  recorda that is executed Example: badiu.gmoodle.mdl 
        /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=false)
     */
    private $moduleinstance; //instance of module/operation . Exemple: id of website moodle 
    
      /**
     * @var string
     *
     * @ORM\Column(name="moduleinstancename", type="string", length=255, nullable=true)
     */
    private $moduleinstancename; //nome of instance module/operation  Exemple:   nome of website moodle 
    
  /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=false)
     */
    private $status; //success | failed | processing
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;
    
	/**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;

   
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="errormsg", type="text", nullable=true)
     */
    private $errormsg;
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getExecmodulekey() {
        return $this->execmodulekey;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }

    function getModuleinstancename() {
        return $this->moduleinstancename;
    }

    function getStatus() {
        return $this->status;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getTimeexec() {
        return $this->timeexec;
    }

    function getDatashouldexec() {
        return $this->datashouldexec;
    }

    function getDataexec() {
        return $this->dataexec;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getErrormsg() {
        return $this->errormsg;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setExecmodulekey($execmodulekey) {
        $this->execmodulekey = $execmodulekey;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }

    function setModuleinstancename($moduleinstancename) {
        $this->moduleinstancename = $moduleinstancename;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }

    function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setErrormsg($errormsg) {
        $this->errormsg = $errormsg;
    }
    function getShortname() {
        return $this->shortname;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }



}