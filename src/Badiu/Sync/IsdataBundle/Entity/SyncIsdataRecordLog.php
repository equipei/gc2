<?php

namespace Badiu\Sync\IsdataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SyncIsdataRecord
 *
 * @ORM\Table(name="sync_isdata_record_log",indexes={
 *              @ORM\Index(name="sync_isdata_record_log_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="sync_isdata_record_log_recordid_ix", columns={"recordid"}), 
 *              @ORM\Index(name="sync_isdata_record_log_status_ix", columns={"status"}),
 *              @ORM\Index(name="sync_isdata_record_log_status_ix", columns={"timestart"})})
 * @ORM\Entity
 */
class SyncIsdataRecordLog
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

     
     /**
     * @var SyncIsdataRecord
     *
     * @ORM\ManyToOne(targetEntity="SyncIsdataRecord")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recordid", referencedColumnName="id")
     * })
     */
    private $recordid;

  /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status; // processing | failed |success
   

   /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;
    
   	/**
     * @var integer
     *
     * @ORM\Column(name="filteridstart", type="bigint", nullable=true)
     */
    private $filteridstart;
    
    	/**
     * @var integer
     *
     * @ORM\Column(name="filteridend", type="bigint", nullable=true)
     */
    private $filteridend;

   /**
     * @var \DateTime
     *
     * @ORM\Column(name="filtertimestart", type="datetime", nullable=true)
     */
    private $filtertimestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="filtertimeend", type="datetime", nullable=true)
     */
    private $filtertimeend;
    	/**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;

    
     	/**
     * @var integer
     *
     * @ORM\Column(name="resultprocess", type="bigint", nullable=true)
     */
    private $resultprocess;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var boolean
     *
     * @ORM\Column(name="failed", type="integer", nullable=true)
     */
    private $failed;
     /**
     * @var string
     *
     * @ORM\Column(name="errormsg", type="text", nullable=true)
     */
    private $errormsg;
   
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getRecordid() {
        return $this->recordid;
    }



    function getStatus() {
        return $this->status;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getTimeexec() {
        return $this->timeexec;
    }

    function getFilteridstart() {
        return $this->filteridstart;
    }

    function getFilteridend() {
        return $this->filteridend;
    }

    function getFiltertimestart() {
        return $this->filtertimestart;
    }

    function getFiltertimeend() {
        return $this->filtertimeend;
    }

    function getDatashouldexec() {
        return $this->datashouldexec;
    }

    function getDataexec() {
        return $this->dataexec;
    }

    function getResultprocess() {
        return $this->resultprocess;
    }

    function getDescription() {
        return $this->description;
    }

    function getFailed() {
        return $this->failed;
    }

    function getErrormsg() {
        return $this->errormsg;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setRecordid(SyncIsdataRecord $recordid) {
        $this->recordid = $recordid;
    }


    function setStatus($status) {
        $this->status = $status;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }

    function setFilteridstart($filteridstart) {
        $this->filteridstart = $filteridstart;
    }

    function setFilteridend($filteridend) {
        $this->filteridend = $filteridend;
    }

    function setFiltertimestart(\DateTime $filtertimestart) {
        $this->filtertimestart = $filtertimestart;
    }

    function setFiltertimeend(\DateTime $filtertimeend) {
        $this->filtertimeend = $filtertimeend;
    }

    function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }

    function setResultprocess($resultprocess) {
        $this->resultprocess = $resultprocess;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setFailed($failed) {
        $this->failed = $failed;
    }

    function setErrormsg($errormsg) {
        $this->errormsg = $errormsg;
    }



}