<?php

namespace Badiu\Sync\IsdataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SyncIsdataRecord
 *
 * @ORM\Table(name="sync_isdata_record",uniqueConstraints={
 *      @ORM\UniqueConstraint(name="sync_isdata_record_uix", columns={"processid", "modulekey","moduleinstance"})},
 *       indexes={@ORM\Index(name="sync_isdata_record_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="sync_isdata_record_modulekey_ix", columns={"modulekey"}), 
 *              @ORM\Index(name="sync_isdata_record_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="sync_isdata_record_moduleinstancename_ix", columns={"moduleinstancename"}),
 *              @ORM\Index(name="sync_isdata_record_processid_ix", columns={"processid"}), 
 *              @ORM\Index(name="sync_isdata_record_timestart_ix", columns={"timestart"}), 
 *              @ORM\Index(name="sync_isdata_record_timeend_ix", columns={"timeend"}), 
 *              @ORM\Index(name="sync_isdata_record_lastidprecessed_ix", columns={"lastidprecessed"}),
 *              @ORM\Index(name="sync_isdata_record_status_ix", columns={"status"})})
 * @ORM\Entity
 */
class SyncIsdataRecord
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

       /**
     * @var SyncIsdataProcess
     *
     * @ORM\ManyToOne(targetEntity="SyncIsdataProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processid", referencedColumnName="id")
     * })
     */
    private $processid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=false)
     */
    private $modulekey; //module/operation is executed/computed Exemple: badiu.gmoodle.mdluser
   
        /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=false)
     */
    private $moduleinstance;  //instance of record. Exemple:  instance of moodle in key badiu.gmoodle.mdluser
    
      /**
     * @var string
     *
     * @ORM\Column(name="moduleinstancename", type="string", length=255, nullable=true)
     */
    private $moduleinstancename; //name  of instance  record. Exemple: nome of user in key record  badiu.gmoodle.mdluser
    
    
  /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status; //unlock | lock | review |processing
   
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart; //last process
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend; //last process
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec; //last process
    
    /**
   * @var integer
   *
   * @ORM\Column(name="lastidprecessed", type="bigint", nullable=false)
     */
    private $lastidprecessed=0;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=false)
     */
    private $timemodified;
  
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timelock", type="datetime", nullable=true)
     */
    private $timelock;
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timelockcancel", type="datetime", nullable=true)
     */
    private $timelockcancel;
	
        	/**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;
    
     	/**
     * @var integer
     *
     * @ORM\Column(name="countrowcess", type="bigint", nullable=true)
     */
    private $countrowprocess;
    
     	/**
     * @var integer
     *
     * @ORM\Column(name="resultprocess", type="bigint", nullable=true)
     */
    private $resultprocess;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var boolean
     *
     * @ORM\Column(name="failed", type="integer", nullable=false)
     */
    private $failed;
     /**
     * @var string
     *
     * @ORM\Column(name="errormsg", type="text", nullable=true)
     */
    private $errormsg;

    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getProcessid() {
        return $this->processid;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }

    function getModuleinstancename() {
        return $this->moduleinstancename;
    }

    function getStatus() {
        return $this->status;
    }

    function getLastidprecessed() {
        return $this->lastidprecessed;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getTimeupdate() {
        return $this->timeupdate;
    }

    function getTimelock() {
        return $this->timelock;
    }

    function getTimelockcancel() {
        return $this->timelockcancel;
    }

    function getDatashouldexec() {
        return $this->datashouldexec;
    }

    function getDataexec() {
        return $this->dataexec;
    }

    function getCountrowprocess() {
        return $this->countrowprocess;
    }

    function getResultprocess() {
        return $this->resultprocess;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getFailed() {
        return $this->failed;
    }

    function getErrormsg() {
        return $this->errormsg;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setProcessid(SyncIsdataProcess $processid) {
        $this->processid = $processid;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }

    function setModuleinstancename($moduleinstancename) {
        $this->moduleinstancename = $moduleinstancename;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setLastidprecessed($lastidprecessed) {
        $this->lastidprecessed = $lastidprecessed;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setTimeupdate(\DateTime $timeupdate) {
        $this->timeupdate = $timeupdate;
    }

    function setTimelock(\DateTime $timelock) {
        $this->timelock = $timelock;
    }

    function setTimelockcancel(\DateTime $timelockcancel) {
        $this->timelockcancel = $timelockcancel;
    }

    function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }

    function setCountrowprocess($countrowprocess) {
        $this->countrowprocess = $countrowprocess;
    }

    function setResultprocess($resultprocess) {
        $this->resultprocess = $resultprocess;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setFailed($failed) {
        $this->failed = $failed;
    }

    function setErrormsg($errormsg) {
        $this->errormsg = $errormsg;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getTimeexec() {
        return $this->timeexec;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }



}