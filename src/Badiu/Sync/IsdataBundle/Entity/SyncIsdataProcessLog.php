<?php
namespace Badiu\Sync\IsdataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SyncIsdataProcessLog
 *
 * @ORM\Table(name="sync_isdata_process_log",
 *       indexes={@ORM\Index(name="sync_isdata_process_log_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="sync_isdata_process_log_tasklogid_ix", columns={"tasklogid"}), 
 *              @ORM\Index(name="sync_isdata_process_log_processid_ix", columns={"processid"}),
 *              @ORM\Index(name="sync_isdata_process_log_timestart_ix", columns={"timestart"}), 
 *              @ORM\Index(name="sync_isdata_process_log_timeend_ix", columns={"timeend"}),
 *              @ORM\Index(name="sync_isdata_process_log_status_ix", columns={"status"})})
 * @ORM\Entity
 */
class SyncIsdataProcessLog
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var SyncIsdataProcess
     *
     * @ORM\ManyToOne(targetEntity="SyncIsdataProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processid", referencedColumnName="id")
     * })
     */
    private $processid;
    /**
     * @var \Badiu\System\SchedulerBundle\Entity\SystemSchedulerTaskLog
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\SchedulerBundle\Entity\SystemSchedulerTaskLog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasklogid", referencedColumnName="id")
     * })
     */
    private $tasklogid;
    
    
  /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status; //success | failed | processing
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;
    
	/**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;

   
     /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="errormsg", type="text", nullable=true)
     */
    private $errormsg;

    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getProcessid() {
        return $this->processid;
    }

    function getTasklogid() {
        return $this->tasklogid;
    }

    function getStatus() {
        return $this->status;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getTimeexec() {
        return $this->timeexec;
    }

    function getDatashouldexec() {
        return $this->datashouldexec;
    }

    function getDataexec() {
        return $this->dataexec;
    }

    function getDescription() {
        return $this->description;
    }

    function getErrormsg() {
        return $this->errormsg;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setProcessid(SyncIsdataProcess $processid) {
        $this->processid = $processid;
    }

    function setTasklogid(\Badiu\System\SchedulerBundle\Entity\SystemSchedulerTaskLog $tasklogid) {
        $this->tasklogid = $tasklogid;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }

    function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setErrormsg($errormsg) {
        $this->errormsg = $errormsg;
    }


}