<?php

namespace Badiu\Auth\ManualBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class Login {
    
     /**
     * @var Container
     */
    private $container;
   
 function __construct(Container $container) {
                $this->container=$container;
         }
         
        public function exec($login,$password) {
            if(!$this->checkLogin($login,$password)){
               return FALSE;
            }else{
                 $user=$this->getUser($login,$password);
                 $param=array('userid'=>$user->getId(),'entity'=>$user->getEntity(),'firstname'=>$user->getFirstname(),'lastname'=>$user->getLastname(),'email'=>$user->getEmail(),'username'=>$user->getUsername());
                 $this->getContainer()->get('badiu.system.access.session')->start($param);
                  //$this->startSession($user);
                  return true;
            } 
           /* if($login=="admin" && $password=="badiu"){
                return true;
            }else{
                return false;
            }*/
         }
         public function checkLogin($login,$password) {
                $r=FALSE;
              $duser=$this->getContainer()->get('badiu.system.user.user.data');
              $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$duser->getBundleEntity()." o  WHERE o.username = :username AND o.password = :password ";
             
              $query = $duser->getEm()->createQuery($sql);
              $query->setParameter('username',$login);
              $query->setParameter('password',md5($password));
              $result= $query->getSingleResult();
             
              if($result['countrecord']==1){$r=TRUE;}
             
             return $r;
         }
         public function getUser($login,$password) {
              
              $duser=$this->getContainer()->get('badiu.system.user.user.data');
              $sql="SELECT  o FROM ".$duser->getBundleEntity()." o  WHERE o.username = :username AND o.password = :password ";
              $query = $duser->getEm()->createQuery($sql);
              $query->setParameter('username',$login);
              $query->setParameter('password',md5($password));
              $result= $query->getSingleResult();
              return $result;
         }
        /*  public function startSession($user) {
                //get sessionn data
                 $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                 $dsession=$this->getContainer()->get('badiu.system.access.session.data');
                 
                 $permd=$this->getContainer()->get('badiu.system.access.permission.data');
                $keyperms=$permd->getKeysByUserid($user->getEntity(),$user->getId());
                $keyperms=$this->getContainer()->get('badiu.system.access.permission')->getKeys( $keyperms);
                
                $keypermsautuser=$permd->getKeysByRole($user->getEntity(),'authenticateduser');
                $keyperms=$this->getContainer()->get('badiu.system.access.permission')->addKeys( $keyperms,$keypermsautuser);
                
                $dsession->setPermissions($keyperms);
                
                //record permission
                 $listrecord=$this->getContainer()->get('badiu.system.access.userrecord.data')->getModuleinstancesByUserid($user->getEntity(),$user->getId());
               
                 $dsession->setRecordpermissions($listrecord);
                 
                $dsession->setEntity($user->getEntity());
                $dsession->getUser()->setId($user->getId());
                $dsession->getUser()->setFirstname($user->getFirstname());
                $dsession->getUser()->setLastname($user->getLastname());
                $dsession->getUser()->setFullname($user->getFirstname().' '.$user->getLastname());
                $dsession->getUser()->setEmail($user->getEmail());
                $dsession->getUser()->setUsername($user->getUsername());
                $badiuSession->save($dsession);
                
                //$badiuSession->initConfigCustom();
            
         }*/
         
         public function getContainer() {
             return $this->container;
         }

         public function setContainer(Container $container) {
             $this->container = $container;
         }



}
