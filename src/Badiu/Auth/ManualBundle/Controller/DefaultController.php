<?php

namespace Badiu\Auth\ManualBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuAuthManualBundle:Default:index.html.twig', array('name' => $name));
    }
}
