<?php
$utilapp=$container->get('badiu.system.core.lib.util.app');
$router = $container->get("router"); 
$formconfig=new stdClass();
$formconfig->showsubmitrow=false;
$factoryformfilter->setConfig($formconfig);
$iconProcess=$container->get('badiu.theme.core.lib.template.vuejs.factoryutil')->iconProcess();

$badiuSession = $this->container->get('badiu.system.access.session');
//$badiuSession->setHashkey($this->getSessionhashkey());
$ssogovbrenable = $badiuSession->getValue('badiu.auth.ssogovbr.formlogin.param.config.enable');
$privatepolice = $badiuSession->getValue('badiu.system.core.param.config.policeprivatetermonlogin');
$usernewsinginroute = $badiuSession->getValue('badiu.system.core.param.config.usernewsinginroute');

$querystringsystem=$container->get('badiu.system.core.lib.http.querystringsystem');
$exturlgoback=$querystringsystem->getParameter('_exturlgoback');
$extbaseurl=$querystringsystem->getParameter('_extbaseurl');

if(!empty($exturlgoback) && !empty($extbaseurl)){
	$loginexternalsession=array('_exturlgoback'=>$exturlgoback,'_extbaseurl'=>$extbaseurl);
	$badiuSession->addValueOff('badiu.auth.core.externalrequestlogin',$loginexternalsession);
}


?>

<br /><br /><br />
<div id="_badiu_theme_base_form_vuejs">
 
 <div id="formlogin"> 
	<div class="row card-login justify-content-center" v-if="formcontrol.status == 'open'" >
		
		<div class="col card" ><br /> 
			<?php if(!empty($privatepolice)){?><div class="alert alert-info" role="alert"><?php echo $privatepolice; ?></div><?php }?>
			<div class="card-header"><?php echo $translator->trans('badiu.auth.core.login.title'); ?></div>
			<div class="card-body"><?php echo $factoryformfilter->exec('add');?></div> 
			<div class="card-footer">
 
			   	<button  @click="send()"  type="button" class="btn btn-primary btn-block"><i class="fas fa-lock icon-lock-login"></i><?php echo $translator->trans('badiu.auth.core.login.sumit'); ?>	</button><?php echo $iconProcess; ?>
			
			<?php if ($ssogovbrenable) {
							$ssogovbrurlrequest = $utilapp->getUrlByRoute('badiu.auth.ssogovbr.request.link'); ?>
							<br />
							<a class="btn btn-primary btn-block" href="<?php echo $ssogovbrurlrequest; ?>">
								<!--<i class="fas fa-sign-in-alt icon-lock-login"></i>-->
								<?php echo $translator->trans('badiu.auth.ssogovbr.formlogin.button'); ?>
							</a>
						<?php } ?>
						
						<?php if ($usernewsinginroute && $router->getRouteCollection()->get($usernewsinginroute) !== null) {
							$newuserurl = $utilapp->getUrlByRoute($usernewsinginroute); ?>
							<br />
							<a class="btn btn-primary btn-block" href="<?php echo $newuserurl; ?>">
								<i class="fas fa-sign-in-alt icon-lock-login"></i>
								<?php echo $translator->trans('badiu.auth.core.login.newuser'); ?>
							</a>
						<?php } ?>
						
						<?php 
							$recoverpwdurl = $utilapp->getUrlByRoute('badiu.system.user.recoverpwd.add',array('parentid'=>1)); ?>
							<br />
							<a class="btn btn-primary  btn-block" href="<?php echo $recoverpwdurl; ?>">
								Recuperar senha
							</a>
						
						
			</div>
		</div>

	
	</div>
	</div>
</div>

