<?php

namespace Badiu\Auth\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AuthSync
 *  
 * @ORM\Table(name="auth_core_sync", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="auth_core_sync_name_uix", columns={"entity","name"}),
 *      @ORM\UniqueConstraint(name="auth_core_sync_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="auth_core_sync_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="auth_core_sync_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="auth_core_sync_name_ix", columns={"name"}), 
 *              @ORM\Index(name="auth_core_sync_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="auth_core_sync_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="auth_core_sync_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="auth_core_sync_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AuthSync {
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
  
    /**
     * @var string
     *
     * @ORM\Column(name="addressurl", type="string", length=255, nullable=false)
     */
    private $addressurl;  

	/**
     * @var integer
     *
     * @ORM\Column(name="enable", type="integer", nullable=true)
     */
    private $enable;
	
	/** 
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;
   
   	      /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="float", precision=10, scale=0, nullable=true)
     */
    private $sortorder;

 /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
	
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getShortname() {
        return $this->shortname;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }



     /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }


	  /**
     * @return string
     */
    public function getAddressurl()
    {
        return $this->addressurl;
    }

    /**
     * @param string $addressurl
     */
    public function setAddressurl($addressurl)
    {
        $this->addressurl = $addressurl;
    }

function getDconfig() {
        return $this->dconfig;
    }
	
	 function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

function getEnable() {
        return $this->enable;
    }
	
	 function setEnable($enable) {
        $this->enable = $enable;
    }
	
	 public function getSortorder() {
        return $this->sortorder;
    }

    public function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }
}
