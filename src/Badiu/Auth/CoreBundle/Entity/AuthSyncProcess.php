<?php

namespace Badiu\Auth\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AuthSyncProcess
 *  
 * @ORM\Table(name="auth_core_sync_process",
 *       indexes={@ORM\Index(name="auth_core_sync_process_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="auth_core_sync_process_action_ix", columns={"action"}), 
 *              @ORM\Index(name="auth_core_sync_process_status_ix", columns={"status"}),
 *              @ORM\Index(name="auth_core_sync_process_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="auth_core_sync_process_skey_ix", columns={"skey"})})
 * @ORM\Entity
 */
class AuthSyncProcess {
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=255, nullable=false)
     */
    private $action; //login | logout

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status; //inque | inprocess | completed
  
    /**
     * @var string
     *
     * @ORM\Column(name="addressurl", type="string", length=255, nullable=false)
     */
    private $addressurl;  
	
		/**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id", nullable=false)
     * })
     */
    private $userid; 

	/**
     * @var string
     *
     * @ORM\Column(name="skey", type="string", length=255, nullable=false)
     */
    private $skey;
	 /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
	
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    

    public function getStatus() {
        return $this->status;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }


    public function setStatus($status) {
        $this->status = $status;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }



    
	  /**
     * @return string
     */
    public function getAddressurl()
    {
        return $this->addressurl;
    }

    /**
     * @param string $addressurl
     */
    public function setAddressurl($addressurl)
    {
        $this->addressurl = $addressurl;
    }

  /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }
function getDconfig() {
        return $this->dconfig;
    }
	
	 function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }
	
	 /**
     * @return string
     */
    public function getSkey() {
        return $this->skey;
    }

    /**
     * @param string $skey
     */
    public function setSkey($skey) {
        $this->skey = $skey;
    }
	
	 function getUserid() {
        return $this->userid;
    }
function setUserid($userid) {
        $this->userid = $userid;
    }
}
