<?php

namespace Badiu\Auth\CoreBundle\Entity;

class Auth
   {
   
    /**
     * @var string
      */
    private $login;
    
    /**
     * @var string
      */
    private $password;

    public function getLogin() {
        return $this->login;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setLogin($login) {
        $this->login = $login;
    }

    public function setPassword($password) {
        $this->password = $password;
    }



}
