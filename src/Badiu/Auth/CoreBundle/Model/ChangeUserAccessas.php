<?php

namespace Badiu\Auth\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class ChangeUserAccessas {
    
     /**
     * @var Container
     */
    private $container;
   
 function __construct(Container $container) {
                $this->container=$container;
         }

          public function exec() {
             
              $userid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
           
              //get user current data
              $badiuSession=$this->getContainer()->get('badiu.system.access.session');
              $dsession = $badiuSession->get();
              //check user exist in entity
              $existuser=$this->getContainer()->get('badiu.system.user.user.data')->countRow($dsession->getEntity(),array('id'=>$userid));
              if(!$existuser){echo "User not found ";exit;}
              //check permission
              $haspermission= $badiuSession->hasParamPermission('badiu.system.core.param.permission.access.as','exec');
              if(!$haspermission){echo "You have no permission to access user as ";exit;}

              
              $currentuser=new \stdClass();
              $currentuser->userfullname=$dsession->getUser()->getFullname();
              $currentuser->userid=$dsession->getUser()->getId();
              $currentuser->useremail=$dsession->getUser()->getEmail();
              //restart session user
              $userdata=$this->getContainer()->get('badiu.system.user.user.data');
              $param=$userdata->getGlobalColumnsValue('o.id AS userid,o.entity,o.firstname,o.lastname,o.email,o.username ',array('id'=>$userid));
              $param['accessedby']=$currentuser;
             // $param=array('userid'=>$user->getId(),'entity'=>$user->getEntity(),'firstname'=>$user->getFirstname(),'lastname'=>$user->getLastname(),'email'=>$user->getEmail(),'username'=>$user->getUsername());
              $this->getContainer()->get('badiu.system.access.session')->start($param);
              //$lroute=$this->router->generate('badiu.system.core.core.frontpages');
               $url=$this->getContainer()->get('badiu.system.core.lib.util.app')->getUrlByRoute('badiu.admin.client.client.frontpage');
            
              header("Location: $url"); 
              exit;
          }
          public function goback() {
           
            $userid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
         
            //get user current data
            $badiuSession=$this->getContainer()->get('badiu.system.access.session');
            $dsession = $badiuSession->get();
            //check if user is in session accesedby
            $useridaccessedby=$dsession->getAccessby()->userid;
            if(empty($useridaccessedby)){echo "No user in session accessed by";exit;}
            if($useridaccessedby!=$userid){echo "The param parentid does not exist in session accessedby";exit;}
            $existuser=$this->getContainer()->get('badiu.system.user.user.data')->countRow($dsession->getEntity(),array('id'=>$userid));
            if(!$existuser){echo "User not found ";exit;}
                       
            $currentuser=null;
           
            //restart session user
            $userdata=$this->getContainer()->get('badiu.system.user.user.data');
            $param=$userdata->getGlobalColumnsValue('o.id AS userid,o.entity,o.firstname,o.lastname,o.email,o.username ',array('id'=>$userid));
            $param['accessedby']=$currentuser;
           // $param=array('userid'=>$user->getId(),'entity'=>$user->getEntity(),'firstname'=>$user->getFirstname(),'lastname'=>$user->getLastname(),'email'=>$user->getEmail(),'username'=>$user->getUsername());
            $this->getContainer()->get('badiu.system.access.session')->start($param);
            //$lroute=$this->router->generate('badiu.system.core.core.frontpages');
             $url=$this->getContainer()->get('badiu.system.core.lib.util.app')->getUrlByRoute('badiu.system.core.core.frontpage');
          
            header("Location: $url"); 
            exit;
        }
         
         public function getContainer() {
             return $this->container;
         }

         public function setContainer(Container $container) {
             $this->container = $container;
         }



}
