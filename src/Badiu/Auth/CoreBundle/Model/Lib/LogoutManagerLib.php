<?php

namespace Badiu\Auth\CoreBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class LogoutManagerLib extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

     public function exec() {
		 
		 //list of site
		 $syncdata=$this->getContainer()->get('badiu.auth.core.sync.data');
		 $fparam=array('entity'=>$entity=$this->getEntity(),'enable'=>1);
		 $list=$syncdata->getGlobalColumnValues('addressurl',$fparam);
		 if(empty($list)){return null;}
		 if(!is_array($list)){return null;}
		 $cont=0;
		 foreach ($list as $row) {
			 $host=$this->getUtildata()->getVaueOfArray($row,'addressurl');
			 $param="";
			 
			 if(!empty($host)){
				$pos=stripos($host, "?");
				if($pos!== false){
					$p= explode("?", $host);
					$host=$this->getUtildata()->getVaueOfArray($p,0);
					$param=$this->getUtildata()->getVaueOfArray($p,1);
                }
			  }
			 $this->request($host,$param);
			 //file_get_contents($host);
			 $cont++;
		 }
		 return $cont;
	 }
	 
	 function request($host,$param) {
        $ch = curl_init();
		
		 curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$param);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
		
        curl_close($ch);
      
      return $result;
    }
}
