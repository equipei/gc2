<?php

namespace Badiu\Auth\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class LoginToken {
    
     /**
     * @var Container
     */
    private $container;
   
 function __construct(Container $container) {
                $this->container=$container;
         }

          public function startSession() {
                $token=null;
                $querystringsystem= $this->container->get('badiu.system.core.lib.http.querystringsystem');
                $token=$querystringsystem->getParameter('_token');
                if(!empty($token)){
                    $p = explode("|",$token);
                    $key=null;
                    if(isset($p[0])) {$key=$p[0];}
                    if($key=='s1'){
                        $tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
                        $token=$tcript->decode($token);
                    }
                   
                }
                 
				if(empty($token)){return false;}
				$entity=null;
				$userid=null;
				$tokencode=null;
			
				$p = explode("|",$token);
				
                   if(isset($p[0])) {$entity=$p[0];}
                   if(isset($p[1])) {$tokencode=$p[1];}
                   if(isset($p[2])) {$userid=$p[2];}
				 
				 if(empty($entity) || empty($tokencode)){return false;}
				
				//check mreport
				$servicedata=$this->container->get('badiu.admin.server.service.data');
				$checkToken=$servicedata->exitToken($token);
				if(!$checkToken)return false;
				
                $entity=$servicedata->getGlobalColumnValue('entity',array('servicetoken'=>$token)); 
                if(empty($entity)){return false;}
                //get sessionn data
                 $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                 $dsession=$this->getContainer()->get('badiu.system.access.session.data');
                
                $dsession->setEntity($entity);
                $dsession->setType('webservice_token_access');
                $dsession->getUser()->setId($userid);
                $dsession->getUser()->setFullname("json service");
                $dsession->setPermissions(array('badiu.system.core.service.process')); //default permission reviw it. read from database
                $badiuSession->save($dsession);
				
				return true;
               
         }
         
         function getToken($isexternalclient) {
            if(!$isexternalclient){return '';}
              $js='';
            $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
            if(empty($serviceid)){return '';}
            $sdata=$this->getContainer()->get('badiu.admin.server.service.data');
            $param=array('id'=>$serviceid);
            $token=$sdata->getGlobalColumnValue('servicetoken', $param); 
            $tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
            $token=$tcript->encode('s1',$token);
            return $token;
        }
         public function getContainer() {
             return $this->container;
         }

         public function setContainer(Container $container) {
             $this->container = $container;
         }



}
