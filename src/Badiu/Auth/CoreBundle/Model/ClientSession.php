<?php

namespace Badiu\Auth\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ClientSession extends BadiuModelLib{
      
 function __construct(Container $container) {
    parent::__construct($container);
   }

   public function exec() {
        //get session token
        
        //get urlreferer

        //get php session 

        //init session
        
   }
    public function setSessionstorage() {
        $result="";
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $dsession=$badiuSession->get();
        $wsinfo=$dsession->getWebserviceinfo();
       
        if(empty($wsinfo)){return $result;}
        $clientsessionstart=$this->getUtildata()->getVaueOfArray($wsinfo,'clientsessionstart');
        if($clientsessionstart) {return $result;} 
       
        $tkey=$this->getUtildata()->getVaueOfArray($wsinfo,'tkey');
       
        $wsinfo['clientsessionstart']=1;
        $dsession->setWebserviceinfo($wsinfo);
        $badiuSession->save($dsession);
        //unset($wsinfo['tkey']);
        $upsdb=$this->updateDbSession($tkey,$wsinfo);
        $tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
        $tkey=$tcript->encode('s1',$tkey);
		
        $out="localStorage.setItem('tkeycsbnet','".$tkey."');"; 
		
        return $out;
    }
    public function getSessionstorage() {
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $dsession=$badiuSession->get();
        $wsinfo=$dsession->getWebserviceinfo();
        if(empty($wsinfo)){return "";}
        $out="this.clientsession=localStorage.getItem('tkeycsbnet');"; 
        return  $out;
   
 }
 public function updateDbSession($tkey,$dconfig) {
        $dbsesseiondata = $this->getContainer()->get('badiu.system.access.dbsession.data');
        $dbsesseiondto=$dbsesseiondata->findByTkey($tkey);
        $id=$this->getUtildata()->getVaueOfArray($dbsesseiondto,'id');
        $dconfig=$this->getJson()->encode($dconfig);
        $sparam=array('id'=>$id,'dconfig'=>$dconfig);
        $result=$dbsesseiondata->updateNativeSql($sparam,false);
        return $result;
 } 
 // review it is invoced inside BadiuLinkController and  BadiuFormController
 public function validateSession($sessionkey) {
       $result=array();
        $tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
        $tkey=$tcript->decode($sessionkey);

        $dbsesseiondata = $this->getContainer()->get('badiu.system.access.dbsession.data');
        $dbsesseiondto=$dbsesseiondata->findByTkey($tkey);
       
        //session not valid
        if(empty($dbsesseiondto)){
             return $this->response->denied('badiu.system.access.authwebservice.tokensessionnotvalid','token of session not found in database'); 
        }
       
        //check expired
        $timetoexpire=$this->getUtildata()->getVaueOfArray($dbsesseiondto,'timetoexpire');
        if(!empty($timetoexpire)){
           $timetoexpire=$timetoexpire->getTimestamp();
           //add two hour of telerance
           $timetoexpire+=3600;
         }
        else {$timetoexpire=0;}
        
        $timenow=new \DateTime();
        $timenow=$timenow->getTimestamp();
        
        if($timetoexpire==0 || $timetoexpire < $timenow ){
            $result['status']='danied';
            $result['info']='badiu.auth.cliensession.sessionexpired';
            $result['message']='Session has expired';
           return  $result; 
        } 
        
        //get config of sessiondb
         $ssdbdconfig=$this->getUtildata()->getVaueOfArray($dbsesseiondto,'dconfig');
         $ssdbdconfig=$this->getJson()->decode($ssdbdconfig, true);
         $this->initSession($sessionkey,$dbsesseiondto,$ssdbdconfig);

         $result['status']='acept';
         $result['info']=null;
         $result['message']=$ssdbdconfig;

         return $result;
 }
 private function initSession($sessionkey,$dbsesseiondto,$ssdbdconfig) {
       
    $entity=$this->getUtildata()->getVaueOfArray($dbsesseiondto,'entity');
    $sclient=$this->getUtildata()->getVaueOfArray($dbsesseiondto,'sclient');

    $clientuserid=$this->getUtildata()->getVaueOfArray($dbsesseiondto,'user.id',true);
    $clientusername=$this->getUtildata()->getVaueOfArray($dbsesseiondto,'user.firstname',true);
    $clientusername.= " ".$this->getUtildata()->getVaueOfArray($dbsesseiondto,'user.lastname',true);

    $badiuSession = $this->getContainer()->get('badiu.system.access.session');
    $badiuSession->setHashkey($sessionkey);
    $dsession = $this->getContainer()->get('badiu.system.access.session.data');
 
    
    $dsession->setEntity($entity);
    $dsession->setType('clientuserwebservice');
   // $dsession->getUser()->setId($sclient);
    $dsession->getUser()->setId($clientuserid);
    $dsession->getUser()->setFullname($clientusername);
    $dsession->setWebserviceinfo($ssdbdconfig);
    //$sessionroles=$this->getRolesForSession($ssdbdconfig);
   // $dsession->setRoles( $sessionroles);
   // $dsession->setPermissions(array('badiu.moodle.mreport.')); 
  //  $listclientperms=$this->getPermissions($ssdbdconfig);
  //  $dsession->setPermissions($listclientperms);  
    //$dsession->setPermissions(array('badiu.')); 
     $badiuSession->save($dsession);
     $badiuSession->initConfigEntity(); 
     //$this->log['entity']=$entity;
    
 }

 
 //delete this item. Same function in service badiu.admin.server.syncuser.managelib
/* public function syncUser($tkey,$ssdbdconfig) {

    $result=array();
    $sync=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.sync',true);
    $syncid=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.syncid',true);
    if($sync && $syncid){
        $result['status']='acept';
        $result['info']=null;
        $result['message']=$syncid;
        return $result;
    }

    $clientuserid=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.id',true);
    $clientuserfirstname=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.firstname',true);
    $clientuserlastname=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.lastname',true);
    $clientuseremail=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.email',true);
    $clientuserusername=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.username',true);
    $clientuseruseridnumber=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.idnumber',true);
    $entity=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'sserver.entity',true);

    $userdata=$this->getContainer()->get('badiu.system.user.user.data');
    $paramexist=array('entity'=>$entity,'username'=>$clientuserusername);
    $existuser=$userdata->countGlobalRow($paramexist);
    if($existuser){
        $syncid=$userdata->getGlobalColumnValue('id',$paramexist);
        if($syncid){
            $result['status']='acept';
            $result['info']='badiu.auth.cliensession.userjustsynced';
            $result['message']=$syncid;

            $ssdbdconfig['user']['sync']=1;
            $ssdbdconfig['user']['syncid']=$syncid;
            $this->updateDbSession($tkey,$ssdbdconfig);
            
            return $result;
        }else{ 
            $result['status']='danied';
            $result['info']='badiu.auth.cliensession.checkexistsyncfailure';
            $result['message']='Failure on find id of user synced';
            
            $ssdbdconfig['user']['sync']=0;
            $ssdbdconfig['user']['syncid']=null;
            $this->updateDbSession($tkey,$ssdbdconfig);

            return $result;
        }

    }
    $server=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'sserver');
    $clientuser=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user');
    $syncserver=array('sync'=>array('server'=>$server,'user'=>$clientuser));
    $dconfig=$this->getJson()->encode($syncserver);
    $parami=array('entity'=>$entity,'username'=>$clientuserusername,'doctype'=>'EMAIL','docnumber'=>$clientuseremail,'firstname'=>$clientuserfirstname,'lastname'=>$clientuserlastname,'email'=>$clientuseremail,'dconfig'=>$dconfig,'auth'=>'manual','deleted'=>0,'timecreated'=>new \DateTime());
    $newuserid=$userdata->insertNativeSql($parami,false);
    $result['status']='acept';
    $result['info']='badiu.auth.cliensession.newsyncedusersuccess';
    $result['message']=$newuserid;

    $ssdbdconfig['user']['sync']=1;
    $ssdbdconfig['user']['syncid']=$syncid;
    $this->updateDbSession($tkey,$ssdbdconfig);

    return $result;
 }*/
}