<?php

namespace Badiu\Auth\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class LoginFromExternalSystemLink extends BadiuModelLib{
    
   
 function __construct(Container $container) {
                parent::__construct($container);
         }
		
		public function exec() {
				
			$tokenauth=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_tokenauth');
			$moduledata=$this->getContainer()->get('badiu.system.module.data.data');
			$response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
			
			$dparam=array('shortname'=>$tokenauth,'modulekey' => 'badiu.system.user.authbadiunetremote','customint1'=>1);
		    $id=$moduledata->getGlobalColumnValue('id',$dparam);  
		    if(empty($id)){echo "Auth token not valid";exit;}
		    $resultdesable=$moduledata->updateNativeSql(array('id'=>$id,'customint1'=>0),false);
            
			$respdto= $moduledata->getGlobalColumnsValue('o.entity,o.moduleinstance AS userid,o.customint3 AS sserviceid,o.customchar3 AS urltogo,o.customchar3,o.customtext1 AS dconfig',array('id'=>$id));  
			$dconfig=$this->getUtildata()->getVaueOfArray($respdto,"dconfig");
			$sserviceid=$this->getUtildata()->getVaueOfArray($respdto,"sserviceid");
			$dconfig=$this->getJson()->decode($dconfig,true);
			$listkeys=$this->getUtildata()->getVaueOfArray($dconfig,"rkeys");
			
			//check rkey remote
			$crchoosekey = rand(0, 19);
			$chrkey=$this->getUtildata()->getVaueOfArray($listkeys,$crchoosekey);
			
			$sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
			$sparam=array();
			$sparam['_serviceid']=$sserviceid;
			$sparam['sessionid']=$tokenauth;
			$sparam['rkey']=$chrkey;
			$sparam['_key']='local.badiunet.localbadiunet.general.existrkey';
      
			$sresult=$sqlservice->service($sparam);
			$fsresult=$sqlservice->serviceResponse($sresult);
			$sckrstatus=$this->getUtildata()->getVaueOfArray($sresult,"status");
			
			if($sckrstatus!="accept"){
				$rfmessage=$this->getUtildata()->getVaueOfArray($sresult,"message");
				$rfinfo=$this->getUtildata()->getVaueOfArray($sresult,"info");
				$resperror= $response->denied('badiu.system.access.authwebservice.checkrkeyinsidemoodlefailed','Remote key error: '.$rfinfo.' Remote message: '.$rfmessage);
				print_r($resperror);exit; 
			}
			if(!$fsresult){
				$resperror=$response->denied('badiu.system.access.authwebservice.checkrkeyinsidemoodlewithoutsucess','Moodle rkey check without success');
				print_r($resperror);exit; 
			}
			$userid=$this->getUtildata()->getVaueOfArray($respdto,"userid");
			$urltogo=$this->getUtildata()->getVaueOfArray($respdto,"urltogo");
			$entity=$this->getUtildata()->getVaueOfArray($respdto,"entity");
			
			//check send tokenauth in urldecode
			 $addtokenauth=$this->getUtildata()->getVaueOfArray($dconfig,"requestparam.addtokenauth",true);
			 if($addtokenauth==1){
				 if (strpos($urltogo, '?') !== false) {$urltogo.="&_tokenauth=$tokenauth";}
				 else {$urltogo.="?_tokenauth=$tokenauth";}
			 }
			 $userdata=$this->getContainer()->get('badiu.system.user.user.data');
			 $lparam=$userdata-> getInfoById($userid);
			 $lparam['entity']=$entity;
			 $this->getContainer()->get('badiu.system.access.session')->start($lparam);
			 header('Location: '.$urltogo);
			 exit;
		}
		
   

}
