<?php

namespace Badiu\Auth\CoreBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;

class LoginFormController extends BadiuFormController {

   
    private $router;
    function __construct(Container $container) {
        parent::__construct($container);
        $this->router=null;
    }

    public function save() {
        // try {
      
         $this->addDefaultData();
         $this->castData();
         $check=$this->validation();
         if($check != null){return $check;}
         
		

		 
         $this->execBefore();
         $this->changeParam();
         
         $pconfig=$this->getContainer()->get('badiu.auth.core.config')->getDefaulFormConfig();
         $servicekey=$pconfig['badiu.auth.core.login.service'];
         $nextrouter=$pconfig['badiu.auth.core.login.router.go.after.login'];
		 
		 $badiuSession = $this->getContainer()->get('badiu.system.access.session');
	     $execfuncionservice=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice');
		 $serviceexecbefore=$badiuSession->getValue('badiu.auth.core.login.param.config.serviceexecbefore');
		 if(!empty($serviceexecbefore)){
			$sebresult=$execfuncionservice->exec($serviceexecbefore,'exec',$this->getParam());
			if(!empty($sebresult)){return $sebresult;}	 		
		 }
         $result=$this->processLogin($servicekey);
         $this->param['result']=$result;
         $this->execAfter();

         //review to use $urlgoback= $this->getContainer()->get('badiu.system.access.browselib')->afterLoggingin();
         if($result){ 
            
			$extredirect=$this->externalRedirect();
			if(!empty($extredirect)){return $extredirect;}
			$serviceexecafter=$badiuSession->getValue('badiu.auth.core.login.param.config.serviceexecafter');
			if(!empty($serviceexecafter)){
				$sebresult=$execfuncionservice->exec($serviceexecafter,'exec',$this->getParam());
				if(!empty($sebresult)){return $sebresult;}			
			}
			
			
            $urlreferer=$badiuSession->getUrlReferer(); 
            $urlgoback=null;
            $defaultroleroute=$badiuSession->get()->getDefaultroute();
            $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
            if(!empty($urlreferer)){$urlgoback=$urlreferer;}
            else{
                if(!empty($defaultroleroute)){$nextrouter=$defaultroleroute;}
                $urlgoback= $utilapp->getUrlByRoute($nextrouter);
           }
           if(!empty($defaultroleroute)){
                $urlfrontpage= $utilapp->getUrlByRoute('badiu.system.core.core.frontpage');
                if($urlfrontpage==$urlreferer){$urlgoback= $utilapp->getUrlByRoute($defaultroleroute);}
                else  if($urlfrontpage."/"==$urlreferer){$urlgoback= $utilapp->getUrlByRoute($defaultroleroute);}
           }
		  /* $lparam=array('urlgoback'=>$urlgoback,'outurl'=>1);
		     $controlurlgoback=$this->getContainer()->get('badiu.system.user.user.lib.controlupdateprofile')->requireUpdate($lparam);
		     if(!empty($controlurlgoback)){$urlgoback=$controlurlgoback;}*/
            $this->setSuccessmessage($this->getTranslator()->trans('badiu.auth.core.login.sucess'));
            $outrsult=array('result'=>$result,'message'=>$this->getSuccessmessage(),'urlgoback'=>$urlgoback);
            $this->getResponse()->setStatus("accept");
            $this->getResponse()->setMessage($outrsult);
            return $this->getResponse()->get();  
         }else{
            $failuremessage=$this->getTranslator()->trans('badiu.auth.core.login.failure');
            $outrsult=array('result'=>$result,'generalerror'=>$failuremessage,'urlgoback'=>null);
            $this->getResponse()->setStatus("danied");
            $this->getResponse()->setMessage($outrsult);
            $this->getResponse()->setInfo('badiu.auth.core.login.failure');
			
            return $this->getResponse()->get();
         }
        return null;
     } 
    public function externalRedirect() {
        $result=null;
	
         $badiuSession = $this->getContainer()->get('badiu.system.access.session');
		 $extvalue=$badiuSession->getValueOff('badiu.auth.core.externalrequestlogin');
		
		 if(!is_array($extvalue)){return null;}
		 $exturlgoback=$this->getUtildata()->getVaueOfArray($extvalue,'_exturlgoback');
		 $extbaseurl=$this->getUtildata()->getVaueOfArray($extvalue,'_extbaseurl');
		 if(!empty($extbaseurl)){
			$badiuSession->addValueOff('badiu.auth.core.externalrequestlogin',null);
			 $moodleremoteaccess=$this->getContainer()->get('badiu.moodle.core.lib.remoteaccess');
			 $fparam=array('url'=>$extbaseurl,'entity'=>$this->getEntity());
			 $sserviceid=$moodleremoteaccess->getServiceidByUrl($fparam);
			
			 if(empty($sserviceid)){return null;}
			 $utilapp =$this->getContainer()->get('badiu.system.core.lib.util.app');
			 $rparam=array('_service'=>'badiu.moodle.core.lib.remoteaccess','_function'=>'remoteAuth','_serviceid'=>$sserviceid,'_urltarget'=>$exturlgoback);
			$result= $utilapp->getUrlByRoute('badiu.system.core.service.process',$rparam);
			
			$lparam=array('urlgoback'=>$result,'outurl'=>1);
		    $upresult=$this->getContainer()->get('badiu.system.user.user.lib.controlupdateprofile')->requireUpdate($lparam);
		    if(!empty($upresult)){$result=$upresult;}
			 $this->setSuccessmessage($this->getTranslator()->trans('badiu.auth.core.login.sucess'));
            $outrsult=array('result'=>$result,'message'=>$this->getSuccessmessage(),'urlgoback'=>$result);
            $this->getResponse()->setStatus("accept");
            $this->getResponse()->setMessage($outrsult);
            return $this->getResponse()->get();  
		 }
        return $result;
    }

   
    public function processLogin($servicekey) {
        if(empty($servicekey)){$servicekey='badiu.auth.core.login.manual';}
        $param = $this->getParam();
        $login=$this->getUtildata()->getVaueOfArray($param, 'login');
        $password=$this->getUtildata()->getVaueOfArray($param, 'password');
        $loginapi=$this->getContainer()->get($servicekey);
        $result=$loginapi->exec($login,$password);
        return $result;
    }
    

    private function addlog($action,$subaction) {
       // $action=,$subaction
         //$parentid =$this->parentid;
        $parentid = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('sellid');
        $dloglib = $this->getContainer()->get('badiu.system.log.core.lib');
        $logtimeend = new \DateTime();
        if (!empty($this->logtimestart)) {
            $start = $this->logtimestart->getTimestamp();
            $end = $logtimeend->getTimestamp();
            $timeexec = $end - $start;
            $this->log['timeexec'] = $timeexec;
        }
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $sessiontype = $badiuSession->get()->getType();
        if ($sessiontype == 'webservice') {
            $logparam = $badiuSession->get()->getWebserviceinfo();
        } else if ($sessiontype == 'web') {
            $key=$this->router;
            $bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
            if (!isset($this->log['dtype'])) {
                $this->log['dtype'] = 'web';
            }
            if (!isset($this->log['clientdevice'])) {
                $this->log['clientdevice'] = 'server';
            }
            if (!isset($this->log['modulekey'])) {
                $this->log['modulekey'] =  $bkey;
            }
            if (!isset($this->log['moduleinstance'])) {
                $this->log['moduleinstance'] = $parentid;
            }
            if (!isset($this->log['functionalitykey'])) {
                $this->log['functionalitykey'] =$key;
            }

            if (!isset($this->log['action'])) {
                $this->log['action'] = $action;
            }
            if (!isset($this->log['rkey'])) {
                $this->log['rkey'] = null;
            }
            if (!isset($this->log['ip'])) {
                $this->log['ip'] = $_SERVER["REMOTE_ADDR"];
            }


            if (!isset($this->log['param'])) {
                $param = array();
                $client = array('browser' => $_SERVER['HTTP_USER_AGENT'], 'ip' => $_SERVER["REMOTE_ADDR"]);
                $param['client'] = $client;

                $user = array('firstname' => $badiuSession->get()->getUser()->getFirstname(),
                    'lastname' => $badiuSession->get()->getUser()->getLastname(),
                    'id' => $badiuSession->get()->getUser()->getId(),
                    'email' => $badiuSession->get()->getUser()->getEmail(),
                    'roles' => null);
                $param['user'] = $user;
                $param['scheduler']=$subaction;
                $sserver = array();

                
                $pos = stripos($key, "badiu.moodle.mreport");

                $serviceid =$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
                if ($pos !== false && $serviceid == $parentid && !empty($parentid)) {
                    $servicedata = $this->getContainer()->get('badiu.admin.server.service.data');
                    $info = $servicedata->getInfoById($serviceid);
                    $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
                    $sserver['id'] = $serviceid;
                    $sserver['url'] = $utildata->getVaueOfArray($info, 'url');
                    $sserver['name'] = $utildata->getVaueOfArray($info, 'name');
                    $sserver['entity'] = $utildata->getVaueOfArray($info, 'entity');
                    $param['sserver'] = $sserver;
                }


                $query = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
                if (!isset($query['parentid']) && !empty($parentid)) {
                    $query['parentid'] = $parentid;
                }
                $param['query'] = $query;
                $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
                $logparam = $json->encode($param);
                $this->log['param'] = $logparam;
               
            }
        }



        
        $this->log['entity'] = $badiuSession->get()->getEntity();
        $this->log['userid'] = $badiuSession->get()->getUser()->getId();
        

        $dloglib->add($this->log);
    }



}
