<?php

namespace Badiu\Auth\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class ChangeEntity {
    
     /**
     * @var Container
     */
    private $container;
   
 function __construct(Container $container) {
                $this->container=$container;
         }

          public function exec() {

             $usermanageentity = $this->getContainer()->getParameter('badiu.system.access.usermanageentity');

             $badiuSession=$this->getContainer()->get('badiu.system.access.session');
             $dsession = $badiuSession->get();
             $currentuser=$dsession->getUser()->getId();
             if($usermanageentity!=$currentuser){
                 echo 'Voce nao tem permissao para acessar essa area do sistema';
                 $filter=false;
                 echo exit; 
             }
             
              $tcript= $this->container->get('badiu.system.core.lib.util.templetecript');
              $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
              $entitytoken=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_entitytoken');
              
              $entitytoken=$tcript->decode($entitytoken);
              
              $p=explode('______',$entitytoken);
              $id=$utildata->getVaueOfArray($p,0);
              $name=$utildata->getVaueOfArray($p,1);
              
              // validate entity
              $entitydata=$this->getContainer()->get('badiu.system.entity.entity.data');
              $nameindb= $entitydata->getGlobalColumnValue('name',array('id'=>$id));
              if($nameindb!=$name){echo "token not valid";exit;}
              
              //check permission
              
              //get user current data
              $badiuSession=$this->getContainer()->get('badiu.system.access.session');
              $dsession = $badiuSession->get();
              
              //restart session entity
              
               $dsession->setEntity($id);
               $badiuSession->save($dsession);
              // $badiuSession->initConfigEntity();
			  $badiuSession->initCache();
              //$lroute=$this->router->generate('badiu.system.core.core.frontpages');
               $url=$this->getContainer()->get('badiu.system.core.lib.util.app')->getUrlByRoute('badiu.moodle.mreport.site.index');
            
              header("Location: $url"); 
              exit;
          }
         
         
         public function getContainer() {
             return $this->container;
         }

         public function setContainer(Container $container) {
             $this->container = $container;
         }



}
