<?php

namespace Badiu\Auth\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class LogoutSyncLink extends BadiuModelLib{
private $param;
    function __construct(Container $container) {
            parent::__construct($container);
          $this->param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
        }
    
		public function exec() {
				$this->externalRequest();
				$this->sisdefault();
				exit;
		}

	function sisdefault() {
		$operation=$this->getUtildata()->getVaueOfArray($this->param, '_operation');
		$status=$this->getUtildata()->getVaueOfArray($this->param, '_status');
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
		$routerafterlogout=$badiuSession->getValue('badiu.auth.core.login.router.go.after.logout');
		$urlafterlogout=$this->getUtilapp()->getUrlByRoute($routerafterlogout);
		$badiuSession->delete();
		
		if(!empty($operation)){return null;}
		$list=null;
		if($status=='start'){
			$list=$this->getList();
			$newlist=array();
			foreach ($list as $row) {
				$id=$this->getUtildata()->getVaueOfArray($row, 'id');
				$newlist[$id]=$row;
			}
			$list=$newlist;
			$badiuSession->addValueOff('badiu.auth.core.synclogout.currentapp',null);
			$badiuSession->addValueOff('badiu.auth.core.synclogout.listapp',$newlist);
		}else{
			$list=$badiuSession->getValueOff('badiu.auth.core.synclogout.listapp');
		}
		
		if(empty($list)){header('Location: '.$urlafterlogout);exit;}
		if(!is_array($list)){header('Location: '.$urlafterlogout);exit;}
		
		foreach ($list as $row) {
			$id=$this->getUtildata()->getVaueOfArray($row, 'id');
			unset($list[$id]);
			$badiuSession->addValueOff('badiu.auth.core.synclogout.listapp',$list);
			$badiuSession->addValueOff('badiu.auth.core.synclogout.currentapp',$id);
			$requrl=$this->sisdefaultMakeUrlParam($row);
			header('Location: '.$requrl);exit;
		  	
		}
		header('Location: '.$urlafterlogout);exit;
	}
	function externalRequest() {
		$operation=$this->getUtildata()->getVaueOfArray($this->param, '_operation');
		if($operation!='requestlogout'){return null;}
		
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
		$routerafterlogout=$badiuSession->getValue('badiu.auth.core.login.router.go.after.logout');
		$urlafterlogout=$this->getUtilapp()->getUrlByRoute($routerafterlogout);
		$badiuSession->delete();
		header('Location: '.$urlafterlogout);exit;
	}
	public function getList() {
		
		$entity=$this->getUtildata()->getVaueOfArray($this->param, 'entity');
		if(empty($entity)){$entity=$this->getEntity();}
		
		$syncdata=$this->getContainer()->get('badiu.auth.core.sync.data');
		$fparam=array('entity'=>$entity,'enable'=>1,'_orderby'=>' ORDER BY o.sortorder');
		$list=$syncdata->getGlobalColumnsValues('o.id,o.addressurl,o.param',$fparam);
		
		return $list;
		
	}
	function sisdefaultMakeUrlParam($param) {
		$addressurl=$this->getUtildata()->getVaueOfArray($param, 'addressurl');
		$id=$this->getUtildata()->getVaueOfArray($param, 'id');
		$cparam=$this->getUtildata()->getVaueOfArray($param, 'param');
		
		$defaulturl=$this->getUtilapp()->getUrlByRoute('badiu.auth.core.synclogout.link');
		$requestparam="";
		if(!empty($cparam)){
			 $requestparam=str_replace("{URL_TO_REDIRECT}",$defaulturl,$cparam);
		}else{
			$requestparam="_urlgoback=$defaulturl&_appid=$id";
		}
		$addressurl.="?$requestparam";
		
		return $addressurl;
	}

   function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }
	
}