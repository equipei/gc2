<?php

namespace Badiu\Auth\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class LoginSystemModuleToken extends BadiuModelLib{
    
   
 function __construct(Container $container) {
                parent::__construct($container);
         }
		
		  public function authRequest() {
			  $stoken = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_stoken','ajaxws');
			  $response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
			  
			  //token required
			  if(empty($stoken)){
				  return $response->denied('badiu.system.access.authsystemmodulewebservice.tokenrequired','token is empty');
			  }
			  
			  $mdata= $this->getContainer()->get('badiu.system.module.service.data');
			  $tdata=$mdata->getGlobalColumnsValue('o.id,o.name,o.entity,o.enable,o.timestart,o.timeend,o.userid,o.ipallowed', array('stoken'=>$stoken));
			  
			  //token not exist
			  if(empty($tdata) || (is_array($tdata) && sizeof($tdata)==0)){
				  return $response->denied('badiu.system.access.authsystemmodulewebservice.tokennoexistindatabase','token not exist in database');
			  }
			  
			  //token miss configurable
			  $entity=$this->getUtildata()->getVaueOfArray($tdata,'entity');
			  $userid=$this->getUtildata()->getVaueOfArray($tdata,'userid');
			  if(empty($entity) || empty($userid)){
				  return $response->denied('badiu.system.access.authsystemmodulewebservice.tokenmissconfigurable','token miss configurable');
			  }
			  //token desabled
			  $enable=$this->getUtildata()->getVaueOfArray($tdata,'enable');
			  if(!$enable){
				  return $response->denied('badiu.system.access.authsystemmodulewebservice.tokendesabled','token disabled');
			  }
			  //token expired
			  $now=new \DateTime();
			  $timestart=$this->getUtildata()->getVaueOfArray($tdata,'timestart');
			  if(!empty($timestart)){
				  if (!is_a($timestart, 'DateTime') && is_string($timestart)) {
					$timestart = \DateTime::createFromFormat('Y-m-d H:i:s', $timestart);
				}
				if($timestart->getTimestamp() > $now->getTimestamp()){
					return $response->denied('badiu.system.access.authsystemmodulewebservice.tokentimestartnotavailable','token time start not available');
				}
			  }
			  $timeend=$this->getUtildata()->getVaueOfArray($tdata,'timeend');
			  if(!empty($timeend)){
				  if (!is_a($timeend, 'DateTime') && is_string($timeend)) {
					$timeend = \DateTime::createFromFormat('Y-m-d H:i:s', $timeend);
				}
				if($timeend->getTimestamp() > $now->getTimestamp()){
					return $response->denied('badiu.system.access.authsystemmodulewebservice.tokentimeendnotavailable','token time end not available');
				}
			  }
			  //request from host not allowed
			  
			  //server service outside entity
			 
			  $serviceid = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');		
			  if(!empty($serviceid)){
				   $sservicedata=$this->getContainer()->get('badiu.admin.server.service.data');
					$fparam=array('id'=>$serviceid);
					$centity=$sservicedata->getGlobalColumnValue('entity',$fparam);
					if($centity!=$entity){
						return $response->denied('badiu.system.access.authsystemmodulewebservice.tokennotallowedtoaccessentiy','token not allowed to access entiy');
					}
			  }
			  //validate parameter
			  
			  
			 $this->initSession($userid);
			  
			  //check pemission
			  $key = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_key');
			  $permission=$this->getContainer()->get('badiu.system.access.permission');
			 $perm= $permission->has_access($key);
			 if(!$perm){
					return $response->denied('badiu.system.access.authsystemmodulewebservice.tokenwithoutpermission','token has no permission to access report key '.$key);
				}
			 return null;
			 
		  }
          public function initSession($userid) {
				 //get sessionn data
				 $udata= $this->getContainer()->get('badiu.system.user.user.data');
				 $dtouser=$udata->getGlobalColumnsValue('o.id,o.entity,o.firstname,o.lastname,o.alternatename,o.enablealternatename,o.email,o.auth,o.username,o.idnumber,o.shortname,o.doctype,o.docnumber,o.ukey', array('id'=>$userid));
			 
                 $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                 $badiuSession->start($dtouser);  
                
               
         }
         
   

}
