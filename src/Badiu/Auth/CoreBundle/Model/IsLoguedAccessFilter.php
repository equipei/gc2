<?php

namespace Badiu\Auth\CoreBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuAccessFilter;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class IsLoguedAccessFilter extends  BadiuAccessFilter {

	function __construct(Container $container) {
		parent::__construct($container);
	}

       public function exec(){
		  
		  //check session
		   $badiuSession = $this->getContainer()->get('badiu.system.access.session');
           if (!$badiuSession->exist()) {return true;}
		   $routfp=$badiuSession->getValue('badiu.system.core.param.config.site.route.frontpage');
		 
		  $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
		 
		  $url=$utilapp->getUrlByRoute($routfp);
		
		   header('Location: '.$url);exit;
        return true;
         } 
    
}
