<?php
 
namespace Badiu\Auth\CoreBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }
 

    public function exec() {
       $result=$this->initPermissions();
  		return $result;
	} 
	
	
	  public function initPermissions() {
        $cont=0;
		
        $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');

        //guest  
        $param=array(
          'roleshortname'=>'guest',
          'permissions'=>array('badiu.financ.ecommerce.login.add',
          'badiu.auth.core.logaut.index',
		  'badiu.auth.core.synclogout.link',
		  'badiu.auth.core.loginfromexternalsystem.link')
        );
        $cont+=$libpermission->add($param);

		return  $cont;
    }
 
}
