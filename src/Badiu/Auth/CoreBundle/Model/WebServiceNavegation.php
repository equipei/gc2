<?php

namespace Badiu\Auth\CoreBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
//use Symfony\Component\HttpFoundation\Session\Session;

class WebServiceNavegation {

    /**
     * @var Container
     */
    private $container;
    private $utildata;
    private $response;
    private $tcript;
    private $json;
    private $log;
    private $logtimestart;
    private $typecontent='formated';
    private $syssession;
	private $typerequest=null;
    function __construct(Container $container) {
        $this->container = $container;
        $this->utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
        $this->response= $this->container->get('badiu.system.core.lib.util.jsonsyncresponse');
        $this->tcript= $this->container->get('badiu.system.core.lib.util.templetecript');
        $this->json = $this->getContainer()->get('badiu.system.core.lib.util.json');
        $this->log=array();
        $this->logtimestart=null;
    }

    public function exec() {
      
        $this->syssession=$this->getContainer()->get('badiu.system.access.session');
        $this->syssession->initHashkey('failurewebserviceloggedin_'.time());
        $this->tcript->setSessionhashkey($this->getSyssession()->getHashkey());
        
        $this->logtimestart=new \DateTime();
        $token = null;
        $jparam = file_get_contents('php://input');
        
        $param = $this->json->decode($jparam, true);
        
       
        $reqparam =$this->utildata->getVaueOfArray($param,'_param');
        $reqparam=$this->tcript->decode($reqparam);
      
        $reqparam=$this->json->decode($reqparam, true);
        $token =$this->utildata->getVaueOfArray($reqparam,'_token');
     
        if (empty($token)) {
             $this->log['action']='failurewebserviceloggedin';
            $logparam=$reqparam;
            $logparam['response']['status']='denied';
            $logparam['response']['info']='badiu.system.access.authwebservice.tokenrequired';
            $logparam['response']['message']='token is empty';
            $logparam=$this->json->encode($logparam);
            $this->log['param']= $logparam;
            $this->log['_skipsession']=1;
            $this->addlog();
           return $this->response->denied('badiu.system.access.authwebservice.tokenrequired','token is empty');
        } 
       
        $servicedata = $this->container->get('badiu.admin.server.service.data');
        $checkToken = $servicedata->exitToken($token);
        if (!$checkToken) {
             $this->log['action']='failurewebserviceloggedin';
            $logparam=$reqparam;
            $logparam['response']['status']='denied';
            $logparam['response']['info']='badiu.system.access.authwebservice.tokennotvalid';
            $logparam['response']['message']='token is not valid';
            $logparam=$this->json->encode($logparam);
            $this->log['param']= $logparam;
            $this->log['_skipsession']=1;
            $this->addlog();
           return $this->response->denied('badiu.system.access.authwebservice.tokennotvalid','token is not valid');
        }
       
        
        $sserverinfo=$servicedata->getInfoByToken($token);
       
		//syncuser
		
		$syncusermlib=$this->container->get('badiu.admin.server.syncuser.managelib');
		
		$synparam=$reqparam;
		$synparam['sserver']=$sserverinfo;
		if(isset($synparam['_token'])){unset($synparam['_token']);}
		$synuser=$syncusermlib->process($synparam);
		$reqparam['user']['serversync']=$synuser;
		//print_r($synuser);exit;
        $rsession=$this->startsession($sserverinfo,$reqparam);
       
        if(empty($rsession)){ return $this->response->denied('badiu.system.access.authwebservice.failed','general error');}
        $rsession=$this->json->encode($rsession);
        $sid = $this->utildata->getVaueOfArray($sserverinfo,'id');
        $entity=$this->utildata->getVaueOfArray($sserverinfo,'entity');
		
        $rsession=$this->tcript->encode('k1',$rsession, $sid);
          // echo $rsession;exit; 
       // $this->addlog();
      
        $logparam=$reqparam;
        
        $logparam['response']['status']='accept';
        $logparam['response']['info']='badiu.system.access.authwebservice.success';
        $logparam['response']['message']='loggin success';
        $logparam['sservice']['id']=$sid;
        $logparam['sservice']['url']=$this->utildata->getVaueOfArray($sserverinfo,'url');
        $logparam['sservice']['name']=$this->utildata->getVaueOfArray($sserverinfo,'name');
     
        $logparam=$this->json->encode($logparam);
        $this->log['param']= $logparam;
        $this->log['entity']=$entity;
        $this->log['action']='webserviceloggedin';
        $this->log['_skipsession']=1; 
         $this->addlog();
     
        $this->updateServerService($entity,$sid,$reqparam);
        return $this->response->accept($rsession,'badiu.system.access.authwebservice.success');
   
    } 

    private function startsession($sserverinfo,$reqparam) {
        
         $entity = $this->utildata->getVaueOfArray($sserverinfo,'entity');
        $sid = $this->utildata->getVaueOfArray($sserverinfo,'id');
        $sname = $this->utildata->getVaueOfArray($sserverinfo,'name');
        
        $client= $this->utildata->getVaueOfArray($reqparam,'client');
        $cleintuser= $this->utildata->getVaueOfArray($reqparam,'user');
        $cleintuserid= $this->utildata->getVaueOfArray($cleintuser,'id');
        
		//add client  defaulturlservice
		$moodleurl=$this->utildata->getVaueOfArray($client,'moodleurl');
		$defaulturlservice=$moodleurl."/local/badiunet/fcservice/index.php";
		$lastcharmdlurl=substr($moodleurl, -1);
		if($lastcharmdlurl=='/'){$defaulturlservice=$moodleurl."local/badiunet/fcservice/index.php";}
		
		
		$client['defaulturlservice']=$defaulturlservice;
		
        $dbsessiondata=$this->getContainer()->get('badiu.system.access.dbsession.data');
        $hash=$this->getContainer()->get('badiu.system.core.lib.util.hash');
        $tkey=$hash->make(50)."-".time()."-".$sid."-".$cleintuserid;
        
		$rkeys=array();
		$rkeycont=0;
		while($rkeycont < 20){
			$rkey=$hash->make(30);
			array_push($rkeys,$rkey);
			$rkeycont++;
		}
        
        $criptkey=$this->tcript->makeKey();
        $sessiondna=$hash->make(1024); 
        $dconfigsys=array('cripty'=>100,'criptykey'=>$criptkey,'sessiondna'=>$sessiondna,'client'=>$client,'user'=>$cleintuser,'sserver'=>$sserverinfo,'rkeys'=>$rkeys);
        $dconfig=$this->json->encode($dconfigsys);
        
       $timetoexpire= new \DateTime();
       $sessiontimeduration=300;
       $timetoexpire->modify('+'.$sessiontimeduration.' minutes');
       $sessionparam=array('entity'=>$entity,'sclient'=>$sid,'dtype'=>'sservice','tkey'=>$tkey,'status'=>'active','dconfig'=>$dconfig,'timecreated'=> new \DateTime(),'lastaccess'=>new \DateTime(),'timetoexpire'=>$timetoexpire);
        
       $result=$dbsessiondata->insertNativeSql($sessionparam,false); 
      
        if($result){
            $routefrontpage='';
            $isteacher=$this->isRoleTeacher($reqparam);
            if($isteacher){ $routefrontpage=array('_key'=>'badiu.moodle.mreport.course.course.index','parentid'=>$sid,'_datasource'=>'servicesql','_service'=>'badiu.system.core.functionality.content.service');}
            $result=array('tkey'=>$tkey,'cripty'=>100,'criptykey'=>$criptkey,'sessiondna'=>$sessiondna,'routefrontpage'=>$routefrontpage,'rkeys'=>$rkeys);
            return $result; 
        }else{
            return null;
        }
    }
    
    public function authRequest($param=null) {
       
         if(empty($param)){$param = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();}
      
        $tkey =$this->utildata->getVaueOfArray($param,'_tokensession');
       if($this->getTyperequest()=='ajaxws'){$tkey =$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_clientsession','ajaxws');}
        if (empty($tkey)) {
           return $this->response->denied('badiu.system.access.authwebservice.tokensession','token is empty');
        }
       
        //start for skip session in tscrip->decode 
        $this->syssession=$this->getContainer()->get('badiu.system.access.session');
        $this->syssession->initHashkey('requestwebserviceloggedin_'.time());
		 if($this->getTyperequest()=='ajaxws'){$this->syssession->setHashkey($tkey);}
        $this->tcript->setSessionhashkey($this->getSyssession()->getHashkey());
        $tkey=$this->tcript->decode($tkey);
      
        $dbsesseiondata = $this->getContainer()->get('badiu.system.access.dbsession.data');
        $dbsesseiondto=$dbsesseiondata->findByTkey($tkey);
       
        //session not valid
        if(empty($dbsesseiondto)){
             return $this->response->denied('badiu.system.access.authwebservice.tokensessionnotvalid','token of session not found in database'); 
        }
       
        //check expired
        $timetoexpire=$this->utildata->getVaueOfArray($dbsesseiondto,'timetoexpire');
        if(!empty($timetoexpire)){$timetoexpire=$timetoexpire->getTimestamp();}
        else {$timetoexpire=0;}
        
        $timenow=new \DateTime();
        $timenow=$timenow->getTimestamp();
        
        if($timetoexpire==0 || $timetoexpire < $timenow ){
             return $this->response->denied('badiu.system.access.authwebservice.sessionexpired','session just expired'); 
        }
        
        //get config of sessiondb
         $ssdbdconfig=$this->utildata->getVaueOfArray($dbsesseiondto,'dconfig');
         $ssdbdconfig=$this->json->decode($ssdbdconfig, true);
         $ssdbdconfig['tkey']=$tkey;
        
         $this->initSession($dbsesseiondto,$ssdbdconfig);

         //get param send by client
         $cparamdto =$this->utildata->getVaueOfArray($param,'_param');
         
         $cparamdto=$this->tcript->decode($cparamdto);
         
         $cparamdto=$this->json->decode($cparamdto, true);
        
         $this->typecontent=$this->utildata->getVaueOfArray($cparamdto,'client.typecontent',true);
		 
		 $lang=$this->utildata->getVaueOfArray($cparamdto,'client.lang',true);
         if(!empty($lang)){
			 $dsession=$this->syssession->get();
			 $dsession->setLang($lang);
			 $this->syssession->save($dsession);
		 }
		 
         //check token dv
         $sessiondna=$this->utildata->getVaueOfArray($ssdbdconfig,'sessiondna');
         if(!$this->getTyperequest()=='ajaxws'){
			 $chektokendv=$this->checkTokendv($cparamdto,$sessiondna);
			if(!$chektokendv){
				return $this->response->denied('badiu.system.access.authwebservice.tokendvnotvalid','token of digit verify is not valid'); 
			}
		 }
         
        
          //check client browser
        $checkClientIdentify=$this->checkClientIdentify($cparamdto,$ssdbdconfig);
        
         if(!$checkClientIdentify){
            return $this->response->denied('badiu.system.access.authwebservice.clientbrowsernotvalidtokensession','the browser of request  does not match with session token'); 
         }
         
         $checkhttpreferer=$this->checkHttpReferer($ssdbdconfig);
          if(!$checkhttpreferer){
            return $this->response->denied('badiu.system.access.authwebservice.connectioncomefromnotallowedurl','The connection came from a URL not allowed'); 
         }
         
        $sessionid=$this->utildata->getVaueOfArray($dbsesseiondto,'id');
        $this->updatesession($sessionid);
        
	    if($this->getTyperequest()=='ajaxws'){return $this->response->accept('Session autentication sucess','badiu.system.access.authwebservice.sessionautentication.success'); }
        //get param for service
        // $fparam=$this->decriptQuery($param,$ssdbdconfig);
         $fparam=$param;
		 
         unset($fparam['_tokendv']);
         unset($fparam['_tokensession']);
         unset($fparam['_param']);
      
        return $fparam;
    }
   
     private function checkTokendv($cparamdto,$sessiondna) {
    
        $tokendvposition=$this->utildata->getVaueOfArray($cparamdto,'_tokendv.postion',true);
        $tokendvcode=$this->utildata->getVaueOfArray($cparamdto,'_tokendv.dv',true);
        $syskey= $this->container->get('badiu.system.core.lib.util.syskey');
        $psessiondna=substr($sessiondna,$tokendvposition,25);
        $dv=$syskey->makeVerifyingDigit($psessiondna,100);
        if($tokendvcode==$dv){return true;}
         return false;
         
     }
     private function checkClientIdentify($cparamdto,$ssdbdconfig) {
         $format=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_format');
         $jparam=file_get_contents('php://input');
         $client=$this->utildata->getVaueOfArray($_SERVER,'HTTP_USER_AGENT');
         $query=$this->utildata->getVaueOfArray($_SERVER,'QUERY_STRING');
		 
		
         if((empty($jparam) && !empty($query) && $format=='xls') || ($this->getTyperequest()=='ajaxws') ){
            $clientbrowserrequest=$client;
          }else{
             $clientbrowserrequest=$this->utildata->getVaueOfArray($cparamdto,'client.browser',true); 
         }
          $clientbrowsersession=$this->utildata->getVaueOfArray($ssdbdconfig,'client.browser',true);
         
          if($clientbrowserrequest==$clientbrowsersession){return true;}
          
       return false;
         
     }
     
       private function checkHttpReferer($ssdbdconfig) {
         $format=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_format');
         $jparam=file_get_contents('php://input');
         $query=$this->utildata->getVaueOfArray($_SERVER,'QUERY_STRING');
         if(empty($jparam) && !empty($query) && $format=='xls'){
              $httprefer=null;
              if(isset($_SERVER['HTTP_REFERER'])){$httprefer=$_SERVER['HTTP_REFERER'];}
              else{return false;}
             
               $siteshoudrequest=$this->utildata->getVaueOfArray($ssdbdconfig,'client.moodleurl',true);
				if(!empty($siteshoudrequest)) {
					$lurl= parse_url($siteshoudrequest);
					$siteshoudrequest=$lurl['scheme'].'://'.$lurl['host'];
			  }
              $pos=strpos($httprefer,$siteshoudrequest);
			  
			 // if($this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid')==2){echo "$httprefer,$pos, $siteshoudrequest";exit;}
              if ($pos !== 0) {return false;} 
         }
        
         return true;
         
     }
     private function initSession($dbsesseiondto,$ssdbdconfig) {
       
        $entity=$this->utildata->getVaueOfArray($dbsesseiondto,'entity');
        $sclient=$this->utildata->getVaueOfArray($dbsesseiondto,'sclient');
      
        $this->syssession=$this->getContainer()->get('badiu.system.access.session');
        $this->syssession->initHashkey("webservicenavegation_".$entity."-".$sclient);
		if($this->getTyperequest()=='ajaxws'){
			$tkey =$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_clientsession','ajaxws');
			$this->syssession->setHashkey($tkey);
			}
        $this->tcript->setSessionhashkey($this->getSyssession()->getHashkey());
      
	   $isserversync=false;
	   $sysuserid=null;
		$serversyncstatus=$this->utildata->getVaueOfArray($ssdbdconfig,'user.serversync.status',true);
		$serversyncmessage=$this->utildata->getVaueOfArray($ssdbdconfig,'user.serversync.message',true);
        
		$lang=$this->utildata->getVaueOfArray($ssdbdconfig,'client.lang',true);
        
		if($serversyncstatus=='acept' && !empty($serversyncmessage) && is_numeric($serversyncmessage)){ $isserversync=true;$sysuserid=$serversyncmessage;}
		
		$dsession = $this->getContainer()->get('badiu.system.access.session.data');
		 if(!empty($lang)){$dsession->setLang($lang);}
        $dsession->setEntity($entity);
        $dsession->setType('webservice');
		$dsession->getUser()->setAnonymous(true);
        $dsession->getUser()->setId($sclient);
        $dsession->getUser()->setFullname('Web service'); //review set real name
        $dsession->setWebserviceinfo($ssdbdconfig);
        $sessionroles=$this->getRolesForSession($ssdbdconfig);
		$dsession->setRoles($sessionroles);
       // $dsession->setPermissions(array('badiu.moodle.mreport.')); 
        $listclientperms=$this->getPermissions($ssdbdconfig,$entity);
       
        //$dsession->setPermissions(array('badiu.')); 
        
		if($isserversync){
			
			$dsession->setType('webservicesynceduser');
			 $dsession->getUser()->setId($sysuserid);
			
			 $userfullname=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.firstname',true);
			 $userfullname.= " ".$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.lastname',true);
			 $dsession->getUser()->setFullname($userfullname);
			 $dsession->getUser()->setAnonymous(false);
			 $uroles=$this->syssession->getRoles($entity,$sysuserid);
			// $dsession->setRoles($uroles);
             $dsession->addRoles($uroles);
			 $permd=$this->getContainer()->get('badiu.system.access.permission.data');
			 $keyperms=$permd->getKeysByUserid($entity,$sysuserid);
			 
			 $keyperms=$this->getContainer()->get('badiu.system.access.permission')->getKeys( $keyperms);
             
			$keypermsautuser=$permd->getKeysByRole($entity,'authenticateduser');
			$accessperm=$this->getContainer()->get('badiu.system.access.permission');
			
			$keyperms=$accessperm->addKeys($keyperms,$keypermsautuser);
			
		
			if(is_array($keyperms) && is_array($keyperms)){$listclientperms = array_merge($listclientperms, $keyperms);}
			else if (!is_array($listclientperms)){$listclientperms=$keyperms;}
			
			
			
		}
		
		$dsession->setPermissions($listclientperms);  
		// $dsession->setPermissions(array('badiu.'));//REVIE DELETE
        $this->syssession->save($dsession);
        //$this->syssession->initConfigEntity(); 
		$this->syssession->initCache(array('entity'=>$entity));
		
        $this->log['entity']=$entity;
        $this->addSessionPermissiondanedServerService($ssdbdconfig);
     }
     //check permission dained for moodle with dbtype is not mysql
     private function addSessionPermissiondanedServerService($ssdbdconfig) {
            $dbtype=$this->utildata->getVaueOfArray($ssdbdconfig,'client.moodledbtype',true);
            $sserviceid=$this->utildata->getVaueOfArray($ssdbdconfig,'sserver.id',true);
            if(empty($sserviceid)){return null;}
           
            if(empty($dbtype)){
                $servicedata = $this->container->get('badiu.admin.server.service.data');
                $dbtype= $servicedata->getGlobalColumnValue('dbtype', array('id'=>$sserviceid));
            }
            $process=false;
           if($dbtype=='pgsql'){$process=true;}
           if($dbtype=='oci'){$process=true;}
           if($dbtype=='sqlsrv'){$process=true;}
            if(!$process){return null;}
           $comfpermdainedpsql=$this->getContainer()->getParameter('badiu.system.access.role.permission.dained.mreport.pgsql');
          
           if(!empty($comfpermdainedpsql)){
                $listpermdanedpsql=array();
                $pos=stripos($comfpermdainedpsql, ",");
              if($pos=== false){
                  $listpermdanedpsql=array($comfpermdainedpsql);
              }else{
                  $listpermdanedpsql= explode(",",$comfpermdainedpsql);
              }
            }
            
           $kpd="badiu.admin.server.service.". $sserviceid.".permissions.denied";
           $this->getSyssession()->addValue($kpd,$listpermdanedpsql);
           
     }
      private function updateServerService($entity,$sserviceid,$reqparam) {
          //lastaccess and plugin access
           $servicedata = $this->container->get('badiu.admin.server.service.data');
 
           $clientplugin= $this->utildata->getVaueOfArray($reqparam,'client.plugin',true);
		   $serviceversionumber=$this->getUtildata()->getVaueOfArray($reqparam,'client.plugin.version',true);
		   $serviceversiontext=$this->getUtildata()->getVaueOfArray($reqparam,'client.plugin.release',true);
		
           $sdatass= $servicedata->getGlobalColumnsValue('o.dbtype,o.dconfig', array('id'=>$sserviceid));
           $dconfig= $this->utildata->getVaueOfArray($sdatass,'dconfig');
           $dbtype= $this->utildata->getVaueOfArray($sdatass,'dbtype');
          // $dconfig= $servicedata->getGlobalColumnValue('dconfig', array('id'=>$sserviceid));
           if(!empty($dconfig)){$dconfig = $this->getJson()->decode($dconfig, true);}
           else {$dconfig=array();}
           $dconfig['clientplugin']=$clientplugin;
           $dconfig=$this->getJson()->encode($dconfig);

 
           $ssparam=array('id'=>$sserviceid,'dconfig'=>$dconfig,'serviceversiontext'=>$serviceversiontext,'serviceversionumber'=>$serviceversionumber,'lastaccess'=>new \DateTime());
           $ssparamentity=array('id'=>$sserviceid,'dconfig'=>$dconfig,'lastaccess'=>new \DateTime());
           $mdldbtype=$this->utildata->getVaueOfArray($reqparam,'client.moodledbtype',true);
          
           if(!empty($mdldbtype) &&  empty($dbtype) ){$ssparam['dbtype']=$mdldbtype;}
           else if(!empty($mdldbtype) &&  !empty($dbtype) && ($mdldbtype!=$dbtype)){$ssparam['dbtype']=$mdldbtype;}
         
           $servicedata->updateNativeSql($ssparam,false);
           
           $entitydata = $this->container->get('badiu.system.entity.entity.data');
           $eparam=array('id'=>$entity,'lastaccess'=>new \DateTime());
           $entitydata->updateNativeSql($ssparamentity,false);
       }
    
     private function updatesession($id) {
          $dbsessiondata=$this->getContainer()->get('badiu.system.access.dbsession.data');
          $timetoexpire= new \DateTime();
         $sessiontimeduration=240;
         $timetoexpire->modify('+'.$sessiontimeduration.' minutes');
         $sessionparam=array('id'=>$id,'lastaccess'=>new \DateTime(),'timetoexpire'=>$timetoexpire);
        $result=$dbsessiondata->updateNativeSql($sessionparam,false); 
    }
    function decriptQuery($query,$ssdbdconfig) {
        if(!is_array($query)){return $query;}
         
         $format=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_format');
         $jparam=file_get_contents('php://input');
         $queryst=$this->utildata->getVaueOfArray($_SERVER,'QUERY_STRING');
      
         if(empty($jparam) && !empty($queryst) && $format=='xls'){return $query;}
        $newdata=array();
       
        foreach ($query as $key => $value) {
             $endwithid=$this->stringEndsWith($key,'id');
               if($key=="_service" || $key=="_key" || $key=="_datasource" || $key=="_tokendv"  || $key=="_tokensession" || $key=="_param"  || $endwithid==true){
                    $newdata[$key]=$value;
               }else{
                     $key=$this->tcript->decode($key);
                    $value=$this->tcript->decode($value);
                    $newdata[$key]=$value;
               }
               
        }
       
       return $newdata; 
    }
 function stringEndsWith($text, $tend)
    {
        $length = strlen($tend);
        if ($length == 0) {
        return false;
    }
   return (substr($text, -$length) === $tend);
}
    private function addlog() {
           if(!$this->log){return null;}
           
           $dloglib=$this->getContainer()->get('badiu.system.log.core.lib');
           $dloglib->getSessionhashkey($this->getSyssession()->getHashkey());
           $logtimeend=new \DateTime();
           if(!empty($this->logtimestart)){
               $start=$this->logtimestart->getTimestamp();
               $end=$logtimeend->getTimestamp();
               $timeexec=$end-$start; 
               $this->log['timeexec']=$timeexec;
           }
       
           
           $this->log['dtype']='webservice';
           $this->log['clientdevice']='server';
           $this->log['modulekey']='badiu.auth.core.webservice';
          
           $dloglib->add($this->log);
           
           
       }
       function getRoles($sserveraparam) {
        $clientroles=$this->getUtildata()->getVaueOfArray($sserveraparam,'user.roles',true);
        
        $roles=array();
        if(!empty($clientroles)){
            $pos=stripos($clientroles, ",");
            if($pos=== false){
                $roles=array($clientroles);
            }else{
                $roles= explode(",", $clientroles);
           }
        }
        return $roles;
    }
    function getRolesForSession($sserveraparam) {
        $roles=$this->getRoles($sserveraparam);
        $newroles=array();
        //if version is not recent
        if(empty($roles)){
            array_push($newroles,"moodle_admin");
            return $newroles; 
         }
        foreach ($roles as $role) {
            $moodlerole="moodle_$role";
            array_push($newroles,$moodlerole);
        }
        return $newroles;
    }
   
    function isRoleTeacher($sserveraparam) {
     
       $roles= $this->getRoles($sserveraparam);
        $iseditingteacher=in_array('editingteacher', $roles);
        $isteachear=in_array('teacher', $roles);
        $ismanager=in_array('manager', $roles);
        $isadmin=in_array('admin', $roles);
		$ismreport=in_array('mreport', $roles);
        if($ismreport) {return false;}
		
        if(!$isadmin && $iseditingteacher) return true;
        if(!$isadmin && $isteachear) return true;
        if(!$isadmin && $ismanager) return true;
        return false;
    }
    function getPermissions($sserveraparam,$entity) {
       
        $clientversion=$this->getUtildata()->getVaueOfArray($sserveraparam,'client.plugin.version',true);
       if(empty($clientversion)){return array('badiu.');}
       
        $roles=$this->getRoles($sserveraparam);
        
        
        $permissiondata=$this->getContainer()->get('badiu.system.access.permission.data');
        $permlib=$this->getContainer()->get('badiu.system.access.permission');
        $defaultrolepermlib=$this->getContainer()->get('badiu.system.access.lib.defaultpermissionrole');
        $allperm=array();
        foreach ($roles as $role) {
            $moodlerole="moodle_$role";
             $listsysperm=$permissiondata->getKeysByRole($entity,$moodlerole);
             if(empty($listsysperm)){
                $listsysperm=$defaultrolepermlib->get($moodlerole);
             }
              $allperm=$permlib->addKeys($allperm,$listsysperm);
             
        }
        //print_r($allperm);exit;
        return $allperm;
      

}

    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }

    function getUtildata() {
        return $this->utildata;
    }

    function getResponse() {
        return $this->response;
    }

    function getCript() {
        return $this->cript;
    }

    function getJson() {
        return $this->json;
    }

    function setUtildata($utildata) {
        $this->utildata = $utildata;
    }

    function setResponse($response) {
        $this->response = $response;
    }

    function setCript($cript) {
        $this->cript = $cript;
    }

    function setJson($json) {
        $this->json = $json;
    }
    function getTypecontent() {
        return $this->typecontent;
    }

    function setTypecontent($typecontent) {
        $this->typecontent = $typecontent;
    }

    function getSyssession() {
        return $this->syssession;
    }

    function setSyssession($syssession) {
        $this->syssession = $syssession;
    }
	
	function getTyperequest() {
        return $this->typerequest;
    }

    function setTyperequest($typerequest) {
        $this->typerequest = $typerequest;
    } 

}

