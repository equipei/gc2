<?php

namespace Badiu\Auth\CoreBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger; 
use  Badiu\System\CoreBundle\Model\Lib\Config\KeyUtil;
class DefaultController extends Controller
{
     
  private $keymanger;
  function __construct($key=null) { 
              $this->keymanger=new BadiuKeyManger();
              if($key!=null){$this->keymanger->setBaseKey($key);}
           
      }
     
      
     public function initKeyManger(Request $request){
          $key=$request->attributes->get('_route');
		   $keyutil=new KeyUtil();
           $key=$keyutil->removeLastItem($key);
           $this->keymanger->setBaseKey($key);
      }
    
    public function loginAction(Request $request) {
         $this->initKeyManger($request);
        
        //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
       //translator
        $translator=$this->get('translator');
       
         $page=$this->get('badiu.system.core.page');
       
        $formOption=array();
        $formOption['action']=$this->generateUrl($this->getKeymanger()->routeIndex());
        $formOption['method']='POST';
        $formOption['label']=$translator->trans($this->getKeymanger()->routeIndex());
        $formOption['attr']=array( 'role'=>'form' );
      
       
        $dto = $this->get($this->getKeymanger()->entity());
        $type=$this->get($this->getKeymanger()->formType());
        $form = $this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans('login'),'attr' => array('class' => 'btn btn-default')));
        
        
         
        $form->handleRequest($request); 
        
        if ($form->isValid()) {
            $pconfig=$this->get('badiu.auth.core.config')->getDefaulFormConfig();
            $servicekey=$pconfig['badiu.auth.core.login.service'];
            $nextrouter=$pconfig['badiu.auth.core.login.router.go.after.login'];
            

           $loginservice=$this->get($servicekey);
          
             $process=$this->get($servicekey)->exec($dto->getLogin(),$dto->getPassword());
            
            $error=false;  
            if(!$process){
                
                $form->get('login')->addError(new FormError($translator->trans('login.password.invalid')));
                $error=true;
            }
            
            if($error){
                 
                $page->setForm($form->createView());
               return $this->render($badiuSession->get()->getTheme().':Temp:login.html.twig', array('page' =>$page));
            }
           		//return $this->redirect($this->generateUrl('badiu.extra.erede.support.ticket.tchannel.index',array('channelid'=>16)));
            $urlreferer=$badiuSession->getUrlReferer(); 
            //echo "urlreferer $urlreferer";exit;
            
           if(!empty($urlreferer)) return $this->redirect($urlreferer);
            //redirect to entity custom

            $entityroutefrontpage=$badiuSession->get()->getConfigcustom()->getValue('badiu.system.entity.config.route.frontpage');
            if(!empty($entityroutefrontpage)){$nextrouter=$entityroutefrontpage;}
             return $this->redirect($this->generateUrl($nextrouter));
         }
         $page->setForm($form->createView());
         return $this->render($badiuSession->get()->getTheme().':Temp:login.html.twig', array('page' =>$page));
  
    }
     public function logoutAction(Request $request) {
			$badiuSession=$this->container->get('badiu.system.access.session');
			$routernext=$badiuSession->getValue('badiu.auth.core.login.router.go.after.logout');
			if(empty($routernext)){$routernext='badiu.auth.core.login.add';} 
			$this->container->get('badiu.auth.core.lib.logoutmanager')->exec();
			$badiuSession->delete();
			return $this->redirect($this->generateUrl($routernext));
			
	 }
    public function getKeymanger() {
        return $this->keymanger;
    }

    public function setKeymanger(BadiuKeyManger $keymanger) {
        $this->keymanger = $keymanger;
    }

}
