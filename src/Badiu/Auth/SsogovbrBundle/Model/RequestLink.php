<?php

namespace Badiu\Auth\SsogovbrBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class RequestLink extends BadiuModelLib{

    function __construct(Container $container) {
            parent::__construct($container);
          
        }
    
		function exec() {
            $state=time(); 
            $nonce=time();
            $routegoback=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_routeparentidgoback');
            if(!empty($routegoback)){ $state="badiurouteparentid_$routegoback";}
            $URL_PROVIDER=$this->getContainer()->getParameter('badiu.auth.ssogovbr.config.providerurl');
            $CLIENT_ID=$this->getContainer()->getParameter('badiu.auth.ssogovbr.config.clientid');
            $REDIRECT_URI=$this->getContainer()->getParameter('badiu.auth.ssogovbr.config.redirecturl');
			$REDIRECT_URI=urlencode($REDIRECT_URI);
            $url="$URL_PROVIDER/authorize?response_type=code&client_id=$CLIENT_ID&scope=openid profile phone email govbr_empresa&redirect_uri=$REDIRECT_URI&nonce=$nonce&state=$state";
         
		   header('Location: '.$url);
            exit;
            return null;
		}

}