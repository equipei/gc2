<?php

namespace Badiu\Auth\SsogovbrBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Firebase\JWT\JWT;
class ReturnLink extends BadiuModelLib{

    function __construct(Container $container) {
            parent::__construct($container);
          
        }
    
		function exec() {
          
           $param=$this->getRemoteAccess();
		
			$badiuSession=$this->getContainer()->get('badiu.system.access.session');
			$routerafterlogout=$badiuSession->getValue('badiu.auth.core.login.router.go.after.logout');
			$urlafterlogout=$this->getUtilapp()->getUrlByRoute($routerafterlogout);
			header('Location: '.$urlafterlogout);exit;
			
			return null;
		}

        function getRemoteAccess() {
            $URL_PROVIDER=$this->getContainer()->getParameter('badiu.auth.ssogovbr.config.providerurl');
            $CLIENT_ID=$this->getContainer()->getParameter('badiu.auth.ssogovbr.config.clientid');
            $SECRET=$this->getContainer()->getParameter('badiu.auth.ssogovbr.config.secret');
            $REDIRECT_URI=$this->getContainer()->getParameter('badiu.auth.ssogovbr.config.redirecturl');

            $parameters=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
            $code=$this->getUtildata()->getVaueOfArray( $parameters,'code');
            $state=$this->getUtildata()->getVaueOfArray( $parameters,'state');

            if(empty($code) || empty($state)) {return null;}
           
            $fields_string="";
            $campos = array(
                'grant_type' => urlencode('authorization_code'),
                'code' => urlencode($code),
                'redirect_uri' => urlencode($REDIRECT_URI)
            );
            
            foreach ($campos as $key => $value)
            {
                $fields_string .= $key . '=' . $value . '&';
            }
        
            rtrim($fields_string, '&');
        
            $ch_token = curl_init();
        
            curl_setopt($ch_token, CURLOPT_URL, $URL_PROVIDER . "/token");
            curl_setopt($ch_token, CURLOPT_POST, 2);
            curl_setopt($ch_token, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch_token, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch_token, CURLOPT_SSL_VERIFYPEER, true);
        
            $headers = array(
                'Content-Type:application/x-www-form-urlencoded',
                'Authorization: Basic ' . base64_encode($CLIENT_ID . ":" . $SECRET)
            );
        
            curl_setopt($ch_token, CURLOPT_HTTPHEADER, $headers);
            $json_output_tokens = json_decode(curl_exec($ch_token), true);
            curl_close($ch_token);

           // print_r($json_output_tokens);

           //Etapa 3: De posse do access token, podemos extrair algumas informações acerca do usuário
            $url    = $URL_PROVIDER . "/jwk";

            $ch_jwk = curl_init();

            curl_setopt($ch_jwk, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch_jwk, CURLOPT_URL, $url);
            curl_setopt($ch_jwk, CURLOPT_RETURNTRANSFER, TRUE);
            $json_output_jwk = json_decode(curl_exec($ch_jwk), true);

            curl_close($ch_jwk);

            $access_token = $json_output_tokens['access_token'];
            $json_output_payload_id_token=null;
            try 
                 {
                    $json_output_payload_access_token = $this->processToClaims($access_token, $json_output_jwk);
                }
            catch (Exception $e) 
                {
                    $detalhamentoErro = $e;
                }

         //Etapa 4: De posse do id token, podemos extrair algumas informações acerca do usuário.

            $id_token = $json_output_tokens['id_token'];
            try 
                {
                    $json_output_payload_id_token = $this->processToClaims($id_token, $json_output_jwk);
                }
             catch (Exception $e) 
                {
                    $detalhamentoErro = $e;
                }
          
                
               //Serviço de obtenção da foto do usuário
                $url = $json_output_payload_id_token['picture'];
                $ch_user_picture = curl_init();
                curl_setopt($ch_user_picture, CURLOPT_SSL_VERIFYPEER, true);
                curl_setopt($ch_user_picture, CURLOPT_URL, $url);
                curl_setopt($ch_user_picture, CURLOPT_RETURNTRANSFER, TRUE);
                $headers = array(
                    'Authorization: Bearer ' . $access_token
                );
                curl_setopt($ch_user_picture, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch_user_picture, CURLOPT_VERBOSE, true);
                curl_setopt($ch_user_picture, CURLOPT_FAILONERROR, true);
                $json_output_user_picture = curl_exec($ch_user_picture);
                if (curl_error($ch_user_picture)) {
                    $msg_error = curl_error($ch_user_picture);
                }
                curl_close($ch_user_picture);
                $this->addUser($json_output_payload_id_token);

                $param=array();
         }


         /**
 * Função que valida o token (access_token ou id_token) (Valida o tempo de expiração ˓→e a assinatura)
 *
 */
function processToClaims($token, $jwk)
{
    $modulus        = JWT::urlsafeB64Decode($jwk['keys'][0]['n']);
    $publicExponent = JWT::urlsafeB64Decode($jwk['keys'][0]['e']);
    $components     = array(
        'modulus' => pack('Ca*a*', 2, $this->encodeLength(strlen($modulus)), $modulus),
        'publicExponent' => pack('Ca*a*', 2, $this->encodeLength(strlen($publicExponent)), $publicExponent)
    );
    $RSAPublicKey   = pack('Ca*a*a*', 48, $this->encodeLength(strlen($components['modulus']) + strlen($components['publicExponent'])), $components['modulus'], $components['publicExponent']);
    $rsaOID         = pack('H*', '300d06092a864886f70d0101010500'); // hex version of MA0GCSqGSIb3DQEBAQUA
    $RSAPublicKey   = chr(0) . $RSAPublicKey;
    $RSAPublicKey   = chr(3) . $this->encodeLength(strlen($RSAPublicKey)) . $RSAPublicKey;
    $RSAPublicKey   = pack('Ca*a*', 48, $this->encodeLength(strlen($rsaOID . $RSAPublicKey)), $rsaOID . $RSAPublicKey);
    $RSAPublicKey   = "-----BEGIN PUBLIC KEY-----\r\n" . chunk_split(base64_encode($RSAPublicKey), 64) . '-----END PUBLIC KEY-----';
    JWT::$leeway    = 30 * 60; //em segundos
    $decoded        = JWT::decode($token, $RSAPublicKey, array(
        'RS256'
    ));
    return (array) $decoded;
}


function encodeLength($length)
{
    if ($length <= 0x7F)
     {
        return chr($length);
    }
    $temp = ltrim(pack('N', $length), chr(0));
    return pack('Ca*', 0x80 | strlen($temp), $temp);
}


function addUser($param){
    $cpf=$this->getUtildata()->getVaueOfArray($param, 'sub');
    $email_verified=$this->getUtildata()->getVaueOfArray($param, 'email_verified');
    $cpf=$this->getUtildata()->getVaueOfArray($param, 'sub');
    $phone_number=$this->getUtildata()->getVaueOfArray($param, 'phone_number');
    $email=$this->getUtildata()->getVaueOfArray($param, 'email');
    $name=$this->getUtildata()->getVaueOfArray($param, 'name');
    $jti=$this->getUtildata()->getVaueOfArray($param, 'jti');

    
    //$param=array('username'=>$cpf,'entity'=>$this->getEntity());
    $param=array('username'=>$cpf);
    $userdb=$this->getContainer()->get('badiu.system.user.user.data');
    $exit=$userdb->countGlobalRow($param);
    $userid=null;
	$updateprofile=false;
    if(!$exit){
        $peple=$this->getContainer()->get('badiu.system.user.user.util')->splitFirtnameLastname($name);
        if(empty($peple->lastname)){$peple->lastname=" ";}
        $paramtoinsert=array('firstname'=>$peple->firtname,'lastname'=>$peple->lastname,'email'=> $email,'username'=>$cpf,'timecreated'=>new \DateTime(),'deleted'=>FALSE,'entity'=>$this->getEntity(),'confirmed'=>1,'doctype'=>'CPF','docnumber'=>$cpf);
        $userid=$userdb->insertNativeSql( $paramtoinsert,false);
		$updateprofile=true;
    }else{
        $exit=$userdb->countGlobalRow($param);
        $userdto=$userdb->getGlobalColumnsValue('o.id,o.statusregister',array('username'=>$cpf));
		$userid=$this->getUtildata()->getVaueOfArray($userdto, 'id');
		$statusregister=$this->getUtildata()->getVaueOfArray($userdto, 'statusregister');
		if($statusregister!='complete'){$updateprofile=true;}
    }
    
    $fparam=$userdb->getGlobalColumnsValue('o.id AS userid,o.entity,o.firstname,o.lastname,o.email,o.username ',array('id'=>$userid));
    $this->getContainer()->get('badiu.system.access.session')->start($fparam);

    $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
    $routegoback=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('state');
 
    $pos = strpos($routegoback,'badiurouteparentid_');

    if ($pos !== false) {
        $p=array();
        $pos=stripos($routegoback, "_");
        if($pos!== false){
            $p = explode("_",$routegoback);
        }
      
        $route=$this->getUtildata()->getVaueOfArray($p,1);
        $parentid=$this->getUtildata()->getVaueOfArray($p,2);
        $param=array('parentid'=>$parentid);
		
		//review it to set role dynamic
		if($updateprofile){
			$route='badiu.admin.selection.loginsingin.edit';
			$param=array('id'=>$userid,'requestid'=>$parentid); //send to process selection edit user
		}
        $urlgoback= $utilapp->getUrlByRoute($route,$param);
        header('Location: '.$urlgoback);
        exit;
    }
    //$urlgoback= $this->getContainer()->get('badiu.system.access.browselib')->afterLoggingin();
   // header('Location: '.$urlgoback);

    
    $route= 'badiu.tms.my.studentfviewdefault.dashboard';
    $url=$utilapp->getUrlByRoute($route);
	
	$lparam=array('urlgoback'=>$url,'outurl'=>0);
	$this->getContainer()->get('badiu.system.user.user.lib.controlupdateprofile')->requireUpdate($lparam);
			 
    header('Location: '.$url);
    exit;


}

}