<?php

namespace Badiu\System\SchedulerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *SystemSchedulerTask
 *
 * @ORM\Table(name="system_scheduler_task", 
 *                  uniqueConstraints={@ORM\UniqueConstraint(name="system_scheduler_task_shortname_uix", columns={"entity", "shortname"}), 
 *                                      @ORM\UniqueConstraint(name="system_scheduler_task_idnumber_uix", columns={"entity", "idnumber"})},
 *                                       indexes={@ORM\Index(name="system_scheduler_task_entityd_ix", columns={"entity"}), 
 *                                                @ORM\Index(name="system_scheduler_task_name_ix", columns={"name"}), 
 *                                                @ORM\Index(name="system_scheduler_task_categoryid_ix", columns={"categoryid"}), 
 *                                                @ORM\Index(name="system_scheduler_task_timeroutine_ix", columns={"timeroutine"}),
 *                                                @ORM\Index(name="system_scheduler_task_timeroutine_ix", columns={"timeroutine"}),
*                                                @ORM\Index(name="system_scheduler_task_deleted_ix", columns={"deleted"}), 
*                                                @ORM\Index(name="system_scheduler_task_tcron_ix", columns={"tcron"}), 
*                                                @ORM\Index(name="system_scheduler_task_lcron_ix", columns={"lcron"}), 
*                                                @ORM\Index(name="system_scheduler_task_lmodel_ix", columns={"lmodel"}),
*                                                @ORM\Index(name="system_scheduler_task_functionalitykey", columns={"functionalitykey"}),
*                                                @ORM\Index(name="system_scheduler_task_subprocessstatus", columns={"subprocessstatus"}),
*                                                @ORM\Index(name="system_scheduler_task_queuetimestart", columns={"queuetimestart"}),
*                                                @ORM\Index(name="system_scheduler_task_queuetimeend", columns={"queuetimeend"}),
*                                                @ORM\Index(name="system_scheduler_task_queuetimewait", columns={"queuetimewait"}),
 *                                                @ORM\Index(name="system_scheduler_task_idnumber_ix", columns={"idnumber"})})
 * * @ORM\Entity
 */
class SystemSchedulerTask
{
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
 
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

      /**
     * @var SystemSchedulerTaskCategory
     *
     * @ORM\ManyToOne(targetEntity="SystemSchedulerTaskCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

	/**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;

    
	 /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='service'; //service |url_absolute | url_relative
    
	 /**
     * @var string
     *
     * @ORM\Column(name="tcron", type="string", length=50, nullable=true)
     */
    private $tcron;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="lcron", type="string", length=50, nullable=true)
     */
    private $lcron; //level of cron global | entity
    
	/**
	 * 100 - none | 200 - model
     * @var integer
     *
     * @ORM\Column(name="lmodel", type="integer", nullable=true)
     */
    private $lmodel;
	 /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey; 

  /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;

    /**
	 * @var string
	 *
	 * @ORM\Column(name="functionalitykey", type="string", length=255, nullable=true)
	 */
	private $functionalitykey;
      /**
     * @var string
     *
     * @ORM\Column(name="doperation", type="string", length=50, nullable=true)
     */
    private $doperation; //import | update | delete | report | other
    
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;
    
    
    	/**
     * @var string
     *
     * @ORM\Column(name="reportaction", type="string", length=255, nullable=true)
     */
    private $reportaction;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="reporttype", type="string", length=255, nullable=true)
     */
    private $reporttype;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="reportfilter", type="text", nullable=true)
     */
    private $reportfilter;
    
    	/**
     * @var string
     *
     * @ORM\Column(name="reportmessage", type="text", nullable=true)
     */
    
    private $reportmessage;
        
	/**
     * @var boolean
     *
     * @ORM\Column(name="simultaneousexec", type="integer", nullable=true)
     */
    private $simultaneousexec;
	
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="timeroutine", type="string", length=255, nullable=false)
     */
    private $timeroutine;  //timeinterval|daily|monthly|weekly|once
	
	 /**
     * @var string
     *
     * @ORM\Column(name="timeroutineparam", type="text", nullable=true)
     */
    private $timeroutineparam; //timeinterval -> timeunit: minute|hour|day|week|month  / value: xxx
                               //daily -> hour: xxx |minute: xxx
                               //monthly -> day: xxx | hour: xxx
                               //weekly -> day: 0-6 | hour: xxx
                               //once  -> datey: xxx
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="execperiodstart", type="datetime", nullable=true)
     */
    private $execperiodstart;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="execperiodend", type="datetime", nullable=true)
     */
    private $execperiodend;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="timeexeconce", type="datetime", nullable=true)
     */
    private $timeexeconce;
	/**
     * @var string
     * 
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status='active'; //active|inactive|processing|queueanalysis|failed
	
		/**
     * @var string
     *
     * @ORM\Column(name="laststatus", type="string", length=50, nullable=true)
     */
    private $laststatus; //anable|desable|processing
   
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="queuetimestart", type="datetime", nullable=true)
     */
    private $queuetimestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="queuetimeend", type="datetime", nullable=true)
     */
    private $queuetimeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="queuetimewait", type="bigint", nullable=true)
     */
    private $queuetimewait;

        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;
    
   	/**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;

	/**
	 * record last id to control next  process ou data to control process
     * @var string
     *
     * @ORM\Column(name="reference",type="text", nullable=true)
     */
    private $reference; 
    
     /**
     * @var string
     *
     * @ORM\Column(name="resultinfo", type="text", nullable=true)
     */
    private $resultinfo;
   
    /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="bigint", nullable=true)
     */
    private $sortorder;
   

    /**
     * @var string
     * 
     * @ORM\Column(name="subprocessstatus", type="string", length=50, nullable=true)
     */
    private $subprocessstatus='none'; //none|processing|incomplete|completed

      /**
     * @var float
     *
     * @ORM\Column(name="subprocessprogress", type="float", precision=10, scale=0, nullable=true)
     */
    private $subprocessprogress;

   /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig; 
      
     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;

      /**
     * @var string
     *
     * @ORM\Column(name="msgerror", type="text", nullable=true)
     */
    private $msgerror;
    /**
     * @var \DateTime 
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    

    function getDoperation() {
        return $this->doperation;
    }

    function getAddress() {
        return $this->address;
    }

    function getSimultaneousexec() {
        return $this->simultaneousexec;
    }

    function getTimeroutine() {
        return $this->timeroutine;
    }

    function getTimeroutineparam() {
        return $this->timeroutineparam;
    }

    function getExecperiodstart() {
        return $this->execperiodstart;
    }

    function getExecperiodend() {
        return $this->execperiodend;
    }

    function getTimeexeconce() {
        return $this->timeexeconce;
    }

    function getStatus() {
        return $this->status;
    }

    function getLaststatus() {
        return $this->laststatus;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getTimeexec() {
        return $this->timeexec;
    }

    function getDatashouldexec() {
        return $this->datashouldexec;
    }

    function getDataexec() {
        return $this->dataexec;
    }

    function getResultinfo() {
        return $this->resultinfo;
    }

    function getDtype() {
        return $this->dtype;
    }

    function getSortorder() {
        return $this->sortorder;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }


    function setDoperation($doperation) {
        $this->doperation = $doperation;
    }

    function setAddress($address) {
        $this->address = $address;
    }

    function setSimultaneousexec($simultaneousexec) {
        $this->simultaneousexec = $simultaneousexec;
    }

    function setTimeroutine($timeroutine) {
        $this->timeroutine = $timeroutine;
    }

    function setTimeroutineparam($timeroutineparam) {
        $this->timeroutineparam = $timeroutineparam;
    }

    function setExecperiodstart($execperiodstart) {
        $this->execperiodstart = $execperiodstart;
    }

    function setExecperiodend($execperiodend) {
        $this->execperiodend = $execperiodend;
    }

    function setTimeexeconce($timeexeconce) {
        $this->timeexeconce = $timeexeconce;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setLaststatus($laststatus) {
        $this->laststatus = $laststatus;
    }

    function setTimestart($timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend($timeend) {
        $this->timeend = $timeend;
    }

    function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }

    function setQueuetimestart($queuetimestart) {
        $this->queuetimestart = $queuetimestart;
    }

    function setQueuetimeend($queuetimeend) {
        $this->queuetimeend = $queuetimeend;
    }

    function setQueuetimewait($queuetimewait) {
        $this->queuetimewait = $queuetimewait;
    }
    function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }

    function setResultinfo($resultinfo) {
        $this->resultinfo = $resultinfo;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated($timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified($timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }


    function getCategoryid(){
        return $this->categoryid;
    }

    function setCategoryid(SystemSchedulerTaskCategory $categoryid) {
        $this->categoryid = $categoryid;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }

    function getReportaction() {
        return $this->reportaction;
    }

    function getReporttype() {
        return $this->reporttype;
    }

    function getReportfilter() {
        return $this->reportfilter;
    }

    function getReportmessage() {
        return $this->reportmessage;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }

    function setReportaction($reportaction) {
        $this->reportaction = $reportaction;
    }

    function setReporttype($reporttype) {
        $this->reporttype = $reporttype;
    }

    function setReportfilter($reportfilter) {
        $this->reportfilter = $reportfilter;
    }

    function setReportmessage($reportmessage) {
        $this->reportmessage = $reportmessage;
    }


    function getFunctionalitykey() {
        return $this->functionalitykey;
    }

    function setFunctionalitykey($functionalitykey) {
        $this->functionalitykey = $functionalitykey;
    }

    function getSubprocessstatus() {
        return $this->subprocessstatus;
    }

    function setSubprocessstatus($subprocessstatus) {
        $this->subprocessstatus = $subprocessstatus;
    }

    function getSubprocessprogress() {
        return $this->subprocessprogress;
    }

    function setSubprocessprogress($subprocessprogress) {
        $this->subprocessprogress = $subprocessprogress;
    }

    function getMsgerror() {
        return $this->msgerror;
    }

    function setMsgerror($msgerror) {
        $this->msgerror = $msgerror;
    }

    function getTcron() {
        return $this->tcron;
    }

    function setTcron($tcron) {
        $this->tcron = $tcron;
    }	
	
	 function getLcron() {
        return $this->lcron;
    }

    function setLcron($lcron) {
        $this->lcron = $lcron;
    }	
	
	
    /**
     * @return integer
     */ 
    public function getLmodel() {
        return $this->lmodel;
    }

    /**
     * @param integer $lmodel
     */
    public function setLmodel($lmodel) {
        $this->lmodel = $lmodel;
    }
	
	  /**
     * @return integer
     */ 
    public function getReference() {
        return $this->reference;
    }

    /**
     * @param integer $reference
     */
    public function setReference($reference) {
        $this->Reference = $reference;
    }
}
