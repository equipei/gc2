<?php

namespace Badiu\System\SchedulerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *SystemSchedulerTask
 *
 * @ORM\Table(name="system_scheduler_task_block_list", 
 *                                       indexes={@ORM\Index(name="system_scheduler_task_log_message_entity_ix", columns={"entity"}), 
 *                                        @ORM\Index(name="system_scheduler_task_block_list_contact_ix", columns={"contact"}), 
 *                                        @ORM\Index(name="system_scheduler_task_block_list_reason_ix", columns={"reason"}),
 *                                        @ORM\Index(name="system_scheduler_task_block_list_timecreated_ix", columns={"timecreated"}),
*                                        @ORM\Index(name="system_scheduler_task_block_list_dtype_ix", columns={"dtype"}),
*                                        @ORM\Index(name="system_scheduler_task_block_list_moduleinstance_ix", columns={"moduleinstance"}),
 *                                        @ORM\Index(name="system_scheduler_task_block_list_modulekey_ix", columns={"modulekey"})})
 * * @ORM\Entity
 */
class SystemSchedulerBlockList
{
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
   	
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;
 
      /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=false)
     */
    private $contact;

    
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype; //email
	
	/**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=50, nullable=false)
     */
    private $reason; //notexist | privateprotected
	
    	 /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey; 

  /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;
    
   
	
   /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string 
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig; 
      
     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime 
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
   
    function getId() {
        return $this->id;
    }

   

    function getEntity() {
        return $this->entity;
    }

    

   
    function getReason() {
        return $this->reason;
    }


    function getDtype() {
        return $this->dtype;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }

    
    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated(){
        return $this->timecreated;
    }

    function setId($id) {
        $this->id = $id;
    }

  

    function setEntity($entity) {
        $this->entity = $entity;
    }

    

    function setReason($reason) {
        $this->reason = $reason;
    }

    

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }

   

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }
    function getContact() {
        return $this->contact;
    }

    function setContact($contact) {
        $this->contact = $contact;
    }

}
