<?php

namespace Badiu\System\SchedulerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *SystemSchedulerCronLog
 *
 * @ORM\Table(name="system_scheduler_cron_log", 
 *                                       indexes={
 *                                                @ORM\Index(name="system_scheduler_cron_log_modulekey_ix", columns={"modulekey"}), 
 *                                                @ORM\Index(name="system_scheduler_cron_log_status_ix", columns={"status"}), 
 *                                                @ORM\Index(name="system_scheduler_cron_log_timestart_ix", columns={"timestart"}), 
 *                                                @ORM\Index(name="system_scheduler_cron_log_timeend_ix", columns={"modulekey"}),
 *                                                @ORM\Index(name="system_scheduler_cron_log_taskqueue_ix", columns={"taskqueue"}),  
 *                                                @ORM\Index(name="system_scheduler_cron_log_timeexec_ix", columns={"timeexec"})})
 * * @ORM\Entity
 */
class SystemSchedulerCronLog
{
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
 
	    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;
    
    	    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
  /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status; //success | failed | processing
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype='automatic'; //automatic | manual


    /**
     * @var integer
     *
     * @ORM\Column(name="taskqueue", type="bigint", nullable=true)
     */
    private $taskqueue;
    
	/**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;

    
     /**
     * @var string
     *
     * @ORM\Column(name="resultinfo", type="text", nullable=true)
     */
    private $resultinfo;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
   
        /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig; 
      

    function getId() {
        return $this->id;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getStatus() {
        return $this->status;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getTimeexec() {
        return $this->timeexec;
    }

    function getDtype() {
        return $this->dtype;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function getName() {
        return $this->name;
    }

    function setName($name) {
        $this->name = $name;
    }

    function getTaskqueue() {
        return $this->taskqueue;
    }

    function setTaskqueue($taskqueue) {
        $this->taskqueue = $taskqueue;
    }

    
    function getDatashouldexec() {
        return $this->datashouldexec;
    }

    function getDataexec() {
        return $this->dataexec;
    }

    function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }

    function getResultinfo() {
        return $this->resultinfo;
    }
	
	  function setResultinfo($resultinfo) {
        $this->resultinfo = $resultinfo;
    }

	
   function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }	
}
