<?php

namespace Badiu\System\SchedulerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * SystemSchedulerRoleLog
 *
 * @ORM\Table(name="system_cheduler_role_log", 
 *       indexes={@ORM\Index(name="system_cheduler_role_log_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="system_cheduler_role_log_processid_ix", columns={"processid"}), 
 *              @ORM\Index(name="system_cheduler_role_log_modulekey_ix", columns={"modulekey"}), 
 *              @ORM\Index(name="system_cheduler_role_log_moduleinstance_ix", columns={"moduleinstance"})})
 * @ORM\Entity
 */
class SystemSchedulerRoleLog
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var SystemSchedulerRoleProcess
     *
     * @ORM\ManyToOne(targetEntity="SystemSchedulerRoleProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processid", referencedColumnName="id")
     * })
     */
    private $processid;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

   
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey; 

  /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;
	/**
     * @var string
     *
     * @ORM\Column(name="paramchar1", type="string", length=255, nullable=true)
     */
    private $paramchar1;
	
	/**
     * @var string
     *
     * @ORM\Column(name="paramchar2", type="string", length=255, nullable=true)
     */
    private $paramchar2;
	
	/**
     * @var string
     *
     * @ORM\Column(name="paramchar3", type="string", length=255, nullable=true)
     */
    private $paramchar3;
	
	  /**
     * @var integer
     *
     * @ORM\Column(name="paramint1", type="bigint", nullable=true)
     */
    private $paramint1;
	  /**
     * @var integer
     *
     * @ORM\Column(name="paramint2", type="bigint", nullable=true)
     */
    private $paramint2;
	  /**
     * @var integer
     *
     * @ORM\Column(name="paramint3", type="bigint", nullable=true)
     */
    private $paramint3;
	
	    /**
     * @var string
     *
     * @ORM\Column(name="paramtext1", type="text", nullable=true)
     */
    private $paramtext1;
	    /**
     * @var string
     *
     * @ORM\Column(name="paramtext2", type="text", nullable=true)
     */
    private $paramtext2;
	    /**
     * @var string
     *
     * @ORM\Column(name="paramtext3", type="text", nullable=true)
     */
    private $paramtext3;
	/**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    
    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

  
    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getProcessid() {
        return $this->processid;
    }

    public function setProcessid(SystemSchedulerRoleProcess $processid) {
        $this->processid = $processid;
    }

    /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }

	
	     /**
     * @return string
     */
    public function getModulekey()
    {
        return $this->modulekey;
    }

    /**
     * @param string $modulekey
     */
    public function setModulekey($modulekey)
    {
        $this->modulekey = $modulekey;
    }
	
	    public function getDaction()
    {
        return $this->daction;
    }

	
	 public function getParamchar1() {
        return $this->paramchar1;
    }

	
	 public function getParamchar2() {
        return $this->paramchar2;
    }
	

	 public function getParamchar3() {
        return $this->paramchar3;
    }
 
 public function getParamint1() {
        return $this->paramint1;
    }
	

	
	 public function getParamint2() {
        return $this->paramint2;
    }
	

	 public function getParamint3() {
        return $this->paramint3;
    }
	

	
	 public function getParamtext1() {
        return $this->paramtext1;
    }

	 public function getParamtext2() {
        return $this->paramtext2;
    }
	

	
	 public function getParamtext3() {
        return $this->paramtext3;
    }

}
