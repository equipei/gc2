<?php

namespace Badiu\System\SchedulerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * SystemSchedulerRoleProcess
 *
 * @ORM\Table(name="system_scheduler_role_process", 
 *       indexes={@ORM\Index(name="system_scheduler_role_process_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="system_scheduler_role_process_categoryid_ix", columns={"roleid"}),
 *              @ORM\Index(name="system_scheduler_role_process_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="system_scheduler_role_process_timestart_ix", columns={"timestart"}),
 *              @ORM\Index(name="system_scheduler_role_process_timeend_ix", columns={"timeend"}),
 *              @ORM\Index(name="system_scheduler_role_process_status_ix", columns={"status"})})
 * @ORM\Entity
 */
class SystemSchedulerRoleProcess
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var SystemSchedulerRole
     *
     * @ORM\ManyToOne(targetEntity="SystemSchedulerRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roleid", referencedColumnName="id")
     * })
     */
    private $roleid;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

	/**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status; //success | failed
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;
    
	/**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;

    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='automatic'; //automatic | manual
	

    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getRoleid() {
        return $this->roleid;
    }

    public function setRoleid(SystemSchedulerRole $roleid) {
        $this->roleid = $roleid;
    }


    /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }

	
	
	 public function getTimestart() {
        return $this->timestart;
    }

    public function getTimeend() {
        return $this->timeend;
    }

    public function getTimeexec() {
        return $this->timeexec;
    }
	
	
    public function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    public function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    public function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }

    public function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    public function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }

	   public function getStatus() {
        return $this->status;
    }
	public function setStatus($status) {
        $this->status = $status;
    }
	  public function getDatashouldexec() {
        return $this->datashouldexec;
    }

    public function getDataexec() {
        return $this->dataexec;
    }

	
	
}
