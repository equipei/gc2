<?php

namespace Badiu\System\SchedulerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *SystemSchedulerTask
 *
 * @ORM\Table(name="system_scheduler_task_log", 
 *                                       indexes={@ORM\Index(name="system_scheduler_task_log_entityd_ix", columns={"entity"}), 
  *                                      @ORM\Index(name="system_scheduler_task_log_subprocessprogress_ix", columns={"subprocessprogress"}),
 *                                       @ORM\Index(name="system_scheduler_task_log_subprocessstatus_ix", columns={"subprocessstatus"}),
 *                                      @ORM\Index(name="system_scheduler_task_log_timestart", columns={"timestart"}),
*                                       @ORM\Index(name="system_scheduler_task_log_timeend", columns={"timeend"}),
*                                       @ORM\Index(name="system_scheduler_task_log_timeexec", columns={"timeexec"}), 
 *                                      @ORM\Index(name="system_scheduler_task_log_queuetimestart", columns={"queuetimestart"}),
*                                       @ORM\Index(name="system_scheduler_task_log_queuetimeend", columns={"queuetimeend"}),
*                                       @ORM\Index(name="system_scheduler_task_log_queuetimewait", columns={"queuetimewait"}),
 *                                       @ORM\Index(name="system_scheduler_task_log_idnumber_ix", columns={"idnumber"})})
 * * @ORM\Entity
 */
class SystemSchedulerTaskLog
{
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
 
    /**
     * @var SystemSchedulerTask
     *
     * @ORM\ManyToOne(targetEntity="SystemSchedulerTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taskid", referencedColumnName="id")
     * })
     */
    private $taskid;
    
   /**
     * @var SystemSchedulerCronLog
     *
     * @ORM\ManyToOne(targetEntity="SystemSchedulerCronLog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cronlogid", referencedColumnName="id")
     * })
     */
    private $cronlogid;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

  /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status; //success | failed | processing

    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="queuetimestart", type="datetime", nullable=true)
     */
    private $queuetimestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="queuetimeend", type="datetime", nullable=true)
     */
    private $queuetimeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="queuetimewait", type="bigint", nullable=true)
     */
    private $queuetimewait;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;
    
    
	/**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;

    
     /**
     * @var string
     *
     * @ORM\Column(name="resultinfo", type="text", nullable=true)
     */
    private $resultinfo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype='automatic'; //automatic | manual
	
	    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;

    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     * 
     * @ORM\Column(name="subprocessstatus", type="string", length=50, nullable=true)
     */
    private $subprocessstatus='none'; //none|waiting|processing|completed

      /**
     * @var float
     *
     * @ORM\Column(name="subprocessprogress", type="float", precision=10, scale=0, nullable=true)
     */
    private $subprocessprogress;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

	/**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig; 
     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
   
          /**
     * @var string
     *
     * @ORM\Column(name="msgerror", type="text", nullable=true)
     */
    private $msgerror;
    function getId() {
        return $this->id;
    }

    function getTaskid() {
        return $this->taskid;
    }

    function getEntity() {
        return $this->entity;
    }

    function getStatus() {
        return $this->status;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getTimeexec() {
        return $this->timeexec;
    }

    function getDatashouldexec() {
        return $this->datashouldexec;
    }

    function getDataexec() {
        return $this->dataexec;
    }

    function getDtype() {
        return $this->dtype;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTaskid(SystemSchedulerTask $taskid) {
        $this->taskid = $taskid;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }

    function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function getResultinfo() {
        return $this->resultinfo;
    }

    function setResultinfo($resultinfo) {
        $this->resultinfo = $resultinfo;
    }

    function getCronlogid(){
        return $this->cronlogid;
    }

    function setCronlogid(SystemSchedulerCronLog $cronlogid) {
        $this->cronlogid = $cronlogid;
    }
    function getSubprocessstatus() {
        return $this->subprocessstatus;
    }

    function setSubprocessstatus($subprocessstatus) {
        $this->subprocessstatus = $subprocessstatus;
    }

    function getSubprocessprogress() {
        return $this->subprocessprogress;
    }

    function setSubprocessprogress($subprocessprogress) {
        $this->subprocessprogress = $subprocessprogress;
    }

    function getMsgerror() {
        return $this->msgerror;
    }

    function setMsgerror($msgerror) {
        $this->msgerror = $msgerror;
    }


    function getQueuetimestart() {
        return $this->queuetimestart;
    }

    function getQueuetimeend() {
        return $this->queuetimeend;
    }

    function getQueuetimewait() {
        return $this->queuetimewait;
    }
	
	
	  function setQueuetimestart(\DateTime $queuetimestart) {
        $this->queuetimestart = $queuetimestart;
    }

    function setQueuetimeend(\DateTime $queuetimeend) {
        $this->queuetimeend = $queuetimeend;
    }

    function setQueuetimewait($queuetimewait) {
        $this->queuetimewait = $queuetimewait;
    }
	
	 function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }
}
