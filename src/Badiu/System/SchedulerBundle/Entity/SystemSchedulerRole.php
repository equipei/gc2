<?php

namespace Badiu\System\SchedulerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * SystemSchedulerRole
 *
 * @ORM\Table(name="system_scheduler_role", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_scheduler_role_name_uix", columns={"entity", "name"}), 
 *      @ORM\UniqueConstraint(name="system_scheduler_role_shortname_uix", columns={"entity", "shortname"}), 
 *      @ORM\UniqueConstraint(name="system_scheduler_role_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="system_scheduler_role_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="system_scheduler_role_name_ix", columns={"name"}), 
 *              @ORM\Index(name="system_scheduler_role_shortname_ix", columns={"shortname"}), 

 *              @ORM\Index(name="system_scheduler_role_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="system_scheduler_role_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="system_scheduler_role_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class SystemSchedulerRole
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    	 /**
		 * @var string
		 *
		 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
		 */
	private $shortname;

    
    


    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey; 

  /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;
	/**
     * @var string
     *
     * @ORM\Column(name="daction", type="string", length=255, nullable=false)
     */
    private $daction;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="reporttype", type="string", length=255, nullable=true)
     */
    private $reporttype;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="reportfilter", type="text", nullable=true)
     */
    private $reportfilter;
	
	/**
     * @var string
     *
     * @ORM\Column(name="dateexectype", type="string", length=255, nullable=true)
     */
    private $dateexectype;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="dateexecfilter", type="text", nullable=true)
     */
    
	private $dateexecfilter;
	
	/**
     * @var string
     *
     * @ORM\Column(name="sendtype", type="string", length=255, nullable=true)
     */
    private $sendtype; //mail|sms
	
	 /**
     * @var string
     *
     * @ORM\Column(name="sendsubject", type="string", length=255, nullable=true)
     */
    private $sendsubject;
	
	/**
     * @var string
     *
     * @ORM\Column(name="sendoption", type="text", nullable=true)
     */
    private $sendoption;
	
		/**
     * @var string
     *
     * @ORM\Column(name="sendmessage", type="text", nullable=true)
     */
    
	private $sendmessage;

	/**
     * @var string
     *
     * @ORM\Column(name="sendmailto", type="text", nullable=true)
     */
    
	private $sendmailto;
	
	   /**
     * @var \DateTime
     *
     * @ORM\Column(name="timelastexec", type="datetime", nullable=true)
     */
    private $timelastexec;
    
	    /**
     * @var integer
     *
     * @ORM\Column(name="customint1", type="bigint",  nullable=true)
     */
    private $customint1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint2", type="bigint",  nullable=true)
     */
    private $customint2;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint3", type="bigint",  nullable=true)
     */
    private $customint3;
	
	    /**
     * @var integer
     *
     * @ORM\Column(name="customint4", type="bigint",  nullable=true)
     */
    private $customint4;
     /**
     * @var float
     *
     * @ORM\Column(name="customdec1", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec1;
    
    /**
     * @var float
     *
     * @ORM\Column(name="customdec2", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec2;
    /**
     * @var float
     *
     * @ORM\Column(name="customdec3", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec3;
     /**
     * @var string
     *
     * @ORM\Column(name="customchar1", type="string", length=255, nullable=true)
     */
    private $customchar1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="customchar2", type="string", length=255, nullable=true)
     */
    private $customchar2;
    
     
    
    /**
     * @var string
     *
     * @ORM\Column(name="customchar3", type="string", length=255, nullable=true)
     */
    private $customchar3;
    
	 /**
     * @var string
     *
     * @ORM\Column(name="customchar4", type="string", length=255, nullable=true)
     */
    private $customchar4;
    
    /**
     * @var string
     *
     * @ORM\Column(name="customtext1", type="text", nullable=true)
     */
    private $customtext1;
    
     /**
     * @var string
     *
     * @ORM\Column(name="customtext2", type="text", nullable=true)
     */
    private $customtext2;
    
     /**
     * @var string
     *
     * @ORM\Column(name="customtext3", type="text", nullable=true)
     */
    private $customtext3;
	/**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }



public function getShortname() {
        return $this->shortname;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }

	
	     /**
     * @return string
     */
    public function getModulekey()
    {
        return $this->modulekey;
    }

    /**
     * @param string $modulekey
     */
    public function setModulekey($modulekey)
    {
        $this->modulekey = $modulekey;
    }
	
	/**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string $dtype
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }
	    public function getDaction()
    {
        return $this->daction;
    }

  
    public function setDaction($daction)
    {
        $this->daction = $daction;
    }
	
	  public function getReporttype()
    {
        return $this->reporttype;
    }

  
    public function setReporttype($reporttype)
    {
        $this->reporttype = $reporttype;
    }
	
		
    public function getReportfilter()
    {
        return $this->reportfilter;
    }

  
    public function setReportfilter($reportfilter)
    {
        $this->reportfilter = $reportfilter;
    }
	
	    public function getDateexectype()
    {
        return $this->dateexectype;
    }

  
    public function setDateexectype($dateexectype)
    {
        $this->dateexectype = $dateexectype;
    }
	
	    public function getDateexecfilter()
    {
        return $this->dateexecfilter;
    }

  
    public function setDateexecfilter($dateexecfilter)
    {
        $this->dateexecfilter = $dateexecfilter;
    }
	
		
    public function getSendtype()
    {
        return $this->sendtype;
    }

  
    public function setSendtype($sendtype)
    {
        $this->sendtype = $sendtype;
    }
	
		
    public function getSendsubject()
    {
        return $this->sendsubject;
    }

  
    public function setSendsubject($sendsubject)
    {
        $this->sendsubject = $sendsubject;
    }
	
	    public function getSendoption()
    {
        return $this->sendoption;
    }

  
    public function setSendoption($sendoption)
    {
        $this->sendoption = $sendoption;
    }
	
	    public function getSendmailto()
    {
        return $this->sendmailto;
    }

  
    public function setSendmailto($sendmailto)
    {
        $this->sendmailto = $sendmailto;
    }
	
	  public function setTimelastexec(\DateTime $timelastexec) {
        $this->timelastexec = $timelastexec;
    }

  public function setSendmessage($sendmessage) {
        $this->sendmessage = $sendmessage;
    }
    public function getSendmessage() {
        return $this->sendmessage;
    }

    public function getCustomint1() {
        return $this->customint1;
    }

    public function getCustomint2() {
        return $this->customint2;
    }

    public function getCustomint3() {
        return $this->customint3;
    }

    public function getCustomdec1() {
        return $this->customdec1;
    }

    public function getCustomdec2() {
        return $this->customdec2;
    }

    public function getCustomdec3() {
        return $this->customdec3;
    }

    public function getCustomchar1() {
        return $this->customchar1;
    }

    public function getCustomchar2() {
        return $this->customchar2;
    }

    public function getCustomchar3() {
        return $this->customchar3;
    }

    public function getCustomtext1() {
        return $this->customtext1;
    }

    public function getCustomtext2() {
        return $this->customtext2;
    }

    public function getCustomtext3() {
        return $this->customtext3;
    }
	
	
    public function setCustomint1($customint1) {
        $this->customint1 = $customint1;
    }

    public function setCustomint2($customint2) {
        $this->customint2 = $customint2;
    }

    public function setCustomint3($customint3) {
        $this->customint3 = $customint3;
    }

	
    public function setCustomdec1($customdec1) {
        $this->customdec1 = $customdec1;
    }

    public function setCustomdec2($customdec2) {
        $this->customdec2 = $customdec2;
    }

    public function setCustomdec3($customdec3) {
        $this->customdec3 = $customdec3;
    }

    public function setCustomchar1($customchar1) {
        $this->customchar1 = $customchar1;
    }

    public function setCustomchar2($customchar2) {
        $this->customchar2 = $customchar2;
    }

    public function setCustomchar3($customchar3) {
        $this->customchar3 = $customchar3;
    }

    public function setCustomtext1($customtext1) {
        $this->customtext1 = $customtext1;
    }

    public function setCustomtext2($customtext2) {
        $this->customtext2 = $customtext2;
    }

    public function setCustomtext3($customtext3) {
        $this->customtext3 = $customtext3;
    }
}
