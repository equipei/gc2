<?php

namespace Badiu\System\SchedulerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
 *SystemSchedulerTaskSubProcess
 *
 * @ORM\Table(name="system_scheduler_task_subprocess",
 *                                      uniqueConstraints={@ORM\UniqueConstraint(name="system_scheduler_task_subprocess_taskpagingindex_uix", columns={"taskid", "tasklogid", "pagingindex"})}, 
 *                                       indexes={@ORM\Index(name="system_scheduler_task_subprocess_entity_ix", columns={"entity"}), 
 *                                      @ORM\Index(name="system_scheduler_task_subprocess_taskid_ix", columns={"taskid"}),
 *                                      @ORM\Index(name="system_scheduler_task_subprocess_tasklogid_ix", columns={"tasklogid"}),
 *                                       @ORM\Index(name="system_scheduler_task_subprocess_timeexec_ix", columns={"timeexec"}),
 *                                       @ORM\Index(name="system_scheduler_task_subprocess_pagingindex_ix", columns={"pagingindex"}),
 *                                       @ORM\Index(name="system_scheduler_task_subprocess_idnumber_ix", columns={"idnumber"})})
 * * @ORM\Entity
 */ 
class SystemSchedulerTaskSubProcess
{
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
 
    /**
     * @var SystemSchedulerTask
     *
     * @ORM\ManyToOne(targetEntity="SystemSchedulerTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taskid", referencedColumnName="id")
     * })
     */
    private $taskid;
 /**
     * @var SystemSchedulerTask
     *
     * @ORM\ManyToOne(targetEntity="SystemSchedulerTaskLog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasklogid", referencedColumnName="id")
     * })
     */
    private $tasklogid;
    
   /**
     * @var SystemSchedulerCronLog
     *
     * @ORM\ManyToOne(targetEntity="SystemSchedulerCronLog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cronlogid", referencedColumnName="id")
     * })
     */
    private $cronlogid;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

  /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status; //success | failed | processing  | programmed
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;

    /**
     * @var integer
     *
     * @ORM\Column(name="pagingindex", type="bigint", nullable=true)
     */
    private $pagingindex;
    
	/**
     * @var integer
     *
     * @ORM\Column(name="datashouldexec", type="bigint", nullable=true)
     */
    private $datashouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dataexec", type="bigint", nullable=true)
     */
    private $dataexec;

    
     /**
     * @var string
     *
     * @ORM\Column(name="resultinfo", type="text", nullable=true)
     */
    private $resultinfo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='automatic'; //automatic | manual
	
	    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;

    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;

    /**
     * @var \DateTime 
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;
   
    function getId() {
        return $this->id;
    }

    function getTaskid() {
        return $this->taskid;
    }

    function getEntity() {
        return $this->entity;
    }

    function getStatus() {
        return $this->status;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getTimeexec() {
        return $this->timeexec;
    }

    function getDatashouldexec() {
        return $this->datashouldexec;
    }

    function getDataexec() {
        return $this->dataexec;
    }

    function getDtype() {
        return $this->dtype;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTasklogid(SystemSchedulerTask $tasklogid) {
        $this->tasklogid = $tasklogid;
    }

    function getTasklogid() {
        return $this->tasklogid;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setTimeexec($timeexec) {
        $this->timeexec = $timeexec;
    }

    function setDatashouldexec($datashouldexec) {
        $this->datashouldexec = $datashouldexec;
    }

    function setDataexec($dataexec) {
        $this->dataexec = $dataexec;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function getResultinfo() {
        return $this->resultinfo;
    }

    function setResultinfo($resultinfo) {
        $this->resultinfo = $resultinfo;
    }

    function getCronlogid(){
        return $this->cronlogid;
    }

    function setCronlogid(SystemSchedulerCronLog $cronlogid) {
        $this->cronlogid = $cronlogid;
    }
   
    function getPagingindex(){
        return $this->pagingindex;
    }

    function setPagingindex($pagingindex) {
        $this->pagingindex = $pagingindex;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }
}
