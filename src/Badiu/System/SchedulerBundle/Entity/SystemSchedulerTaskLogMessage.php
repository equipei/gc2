<?php

namespace Badiu\System\SchedulerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *SystemSchedulerTask
 *
 * @ORM\Table(name="system_scheduler_task_log_message", 
 *                                       indexes={@ORM\Index(name="system_scheduler_task_log_message_entity_ix", columns={"entity"}), 
 *                                        @ORM\Index(name="system_scheduler_task_log_message_userrecipient_ix", columns={"userrecipient"}), 
 *                                        @ORM\Index(name="system_scheduler_task_log_message_usercontact_ix", columns={"usercontact"}),  
 *                                        @ORM\Index(name="system_scheduler_task_log_message_msgsubject_ix", columns={"msgsubject"}),
 *                                        @ORM\Index(name="system_scheduler_task_log_message_timecreated_ix", columns={"timecreated"}),
*                                        @ORM\Index(name="system_scheduler_task_log_message_tasklogid_ix", columns={"tasklogid"}),
*                                        @ORM\Index(name="system_scheduler_task_log_message_channelshouldexec_ix", columns={"channelshouldexec"}),
*                                        @ORM\Index(name="system_scheduler_task_log_message_channelexec_ix", columns={"channelexec"}),
*                                        @ORM\Index(name="system_scheduler_task_log_message_channeldelivered_ix", columns={"channeldelivered"}),
 *                                        @ORM\Index(name="system_scheduler_task_log_message_name_ix", columns={"name"})})
 * * @ORM\Entity
 */
class SystemSchedulerTaskLogMessage
{
     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
 
    /**
     * @var SystemSchedulerTask
     *
     * @ORM\ManyToOne(targetEntity="SystemSchedulerTaskLog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasklogid", referencedColumnName="id")
     * })
     */
    private $tasklogid;
   
	
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;
 
      /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
            
     /**
     * @var string
     *
     * @ORM\Column(name="userrecipient", type="string", length=255, nullable=true)
     */
    private $userrecipient;
    
    /**
     * @var string
     *
     * @ORM\Column(name="usercontact", type="string", length=255, nullable=true)
     */
    private $usercontact;
    
    /**
     * @var string
     *
     * @ORM\Column(name="msgsubject", type="string", length=255, nullable=true)
     */
    private $msgsubject;
    
     /**
     * @var string
     *
     * @ORM\Column(name="msgcontent", type="text", nullable=true)
     */
    private $msgcontent;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='mailsendtouserreporterfilter'; //mailsendtouserreporterfilter
	
    	 /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey; 

  /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstanceref", type="bigint", nullable=true)
     */
    private $moduleinstanceref;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="channelshouldexec", type="bigint", nullable=true)
     */
    private $channelshouldexec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="channelexec", type="bigint", nullable=true)
     */
    private $channelexec;
	
	  /**
     * @var string
     *
     * @ORM\Column(name="channelinfo", type="text", nullable=true)
     */
    private $channelinfo;
	
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="channeldelivered", type="string", length=255, nullable=true)
     */
    private $channeldelivered;
	
   /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string 
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig; 
      
     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime 
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
   
    function getId() {
        return $this->id;
    }

    function getTasklogid() {
        return $this->tasklogid;
    }

    function getEntity() {
        return $this->entity;
    }

    function getUserrecipient() {
        return $this->userrecipient;
    }

    function getUsercontact() {
        return $this->usercontact;
    }

    function getMsgsubject() {
        return $this->msgsubject;
    }

    function getMsgcontent() {
        return $this->msgcontent;
    }

    function getDtype() {
        return $this->dtype;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }

    function getModuleinstanceref() {
        return $this->moduleinstanceref;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated(){
        return $this->timecreated;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTasklogid(SystemSchedulerTask $tasklogid) {
        $this->tasklogid = $tasklogid;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setUserrecipient($userrecipient) {
        $this->userrecipient = $userrecipient;
    }

    function setUsercontact($usercontact) {
        $this->usercontact = $usercontact;
    }

    function setMsgsubject($msgsubject) {
        $this->msgsubject = $msgsubject;
    }

    function setMsgcontent($msgcontent) {
        $this->msgcontent = $msgcontent;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }

    function setModuleinstanceref($moduleinstanceref) {
        $this->moduleinstanceref = $moduleinstanceref;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }
    function getName() {
        return $this->name;
    }

    function setName($name) {
        $this->name = $name;
    }


 function getChannelshouldexec() {
        return $this->channelshouldexec;
    }

    function setChannelshouldexec($channelshouldexec) {
        $this->channelshouldexec = $channelshouldexec;
    }
	
	function getChannelexec() {
        return $this->channelexec;
    }

    function setChannelexec($channelexec) {
        $this->channelexec = $channelexec;
    }

   function getChannelinfo() {
        return $this->channelinfo;
    }

    function setChannelinfo($channelinfo) {
        $this->channelinfo = $channelinfo;
    }
	
	 function getChanneldelivered() {
        return $this->channeldelivered;
    }

    function setChanneldelivered($channeldelivered) {
        $this->channeldelivered = $channeldelivered;
    }
}
