<?php

namespace Badiu\System\SchedulerBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormError;
class TaskController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
              
    public function addCheckForm($form,$data) {
           $check=true;
		 
         // if ($data->existEdit()) {
               // $form->get('cpf')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.cpfnotvalid')));
//                $check= false; 
          //  }
           return  $check;
       }
	   

	  public function changeParamOnOpen() {
		   $param = $this->getParam();
		   if(!$this->isEdit()){
			  if($this->getContainer()->hasParameter('badiu.system.core.type.instalation')){
				$typeinstalation=$this->getContainer()->getParameter('badiu.system.core.type.instalation');
				if($typeinstalation=='multipleentity'){$param['lcron']="global";}
			 }
			   $this->setParam($param); 
		   }
		   
		
	   }
	 public function setDefaultDataOnOpenEditForm($dto) {
          $lib=$this->getContainer()->get('badiu.system.scheduler.task.lib');
		  $id=$this->getContainer()->get('request')->get('id');
		  $dto=$lib->getDataFormEdit($id);
		  return $dto;
     }
    
	 public function save($data) {
	 
		$dto=$data->getDto();
		$lib=$this->getContainer()->get('badiu.system.scheduler.task.lib');
		$result=$data->setDto($lib->save($data));
		return $result;
    }   
	
	
}
