<?php

namespace Badiu\System\SchedulerBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class TaskData extends BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
        parent::__construct($container,$bundleEntity);
    }

	 public function getListForCron($param) {
				$maxrecord=null;
				if(isset($param['maxrecord'])){
					$maxrecord=$param['maxrecord'];
					unset($param['maxrecord']);
				}
				$sortorder="";
				if(isset($param['sortorder'])){
					$sortorder=$param['sortorder'];
					unset($param['sortorder']);
				}
				$wosql="";
				if($sortorder=='old'){$wosql=" AND o.queuetimestart IS NOT NULL ORDER BY o.queuetimestart ";}
				else if($sortorder=='new'){$wosql=" AND o.queuetimestart IS NULL  ";}
				
				$wsql=$this->makeSqlWhere($param);
				$sql="SELECT  o.id,o.entity,o.name,o.shortname,o.dtype,o.tcron,o.modulekey,o.moduleinstance,o.functionalitykey,o.doperation,o.address,o.reportaction,o.reporttype,o.reportfilter,o.reportmessage,o.simultaneousexec,o.timeroutine,o.timeroutineparam,o.execperiodstart,o.execperiodend,o.timeexeconce,o.status,o.laststatus,o.queuetimestart,o.queuetimeend,o.queuetimewait,o.timestart,o.timeend,o.timeexec,o.subprocessstatus,o.subprocessprogress,o.dconfig,o.param,o.reference FROM ".$this->getBundleEntity()." o  WHERE  o.id > :bnetctrid $wsql $wosql ";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('bnetctrid',0);
                $query=$this->makeSqlFilter($query, $param);
				if(!empty($maxrecord)){$query->setMaxResults($maxrecord);}
                $result= $query->getResult();
             
			return $result;
    }
	
	public function getListMonitorForCron($param) {
                $wsql=$this->makeSqlWhere($param);
				$sql="SELECT  o.id,o.entity,o.name,o.shortname,o.dtype,o.tcron,o.modulekey,o.moduleinstance,o.functionalitykey,o.doperation,o.address,o.reportaction,o.reporttype,o.reportfilter,o.reportmessage,o.simultaneousexec,o.timeroutine,o.timeroutineparam,o.execperiodstart,o.execperiodend,o.timeexeconce,o.status,o.laststatus,o.queuetimestart,o.queuetimeend,o.queuetimewait,o.timestart,o.timeend,o.timeexec,o.subprocessstatus,o.subprocessprogress,o.dconfig,o.param,o.reference FROM ".$this->getBundleEntity()." o  WHERE  o.id > :bnetctrid AND (o.timestart IS NULL OR o.timestart <= :timestart) AND o.shortname LIKE :shortname $wsql";
                
				$now=new \Datetime();
				$now->modify('-20 minutes');
				$query = $this->getEm()->createQuery($sql);
                $query->setParameter('bnetctrid',0);
				$query->setParameter('timestart',$now);
				$query->setParameter('shortname','monitor_%');
                $query=$this->makeSqlFilter($query, $param);
                $result= $query->getResult();
             
			return $result;
    }
    public function getEnabled($entity=null) {
                $wsql=" ";
              
                if(!empty($entity)){$wsql=" AND o.entity=:entity ";}
                $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE  o.status = :status AND o.deleted=:deleted $wsql ORDER BY o.sortorder ";        
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('status','active');
				 $query->setParameter('deleted',0); 
                if(!empty($entity)){$query->setParameter('entity',$entity);}
                $result= $query->getResult();
             
		return $result;
    }
    public function getProcessing($entity=null) {
     $wsql=" ";
   
     if(!empty($entity)){$wsql=" AND o.entity=:entity ";}
     $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE  o.status = :status $wsql ORDER BY o.sortorder ";        
     $query = $this->getEm()->createQuery($sql);
     $query->setParameter('status','processing');
     if(!empty($entity)){$query->setParameter('entity',$entity);}
     $result= $query->getResult();
  
     return $result;
}
    
     
       public function chageStatus($id,$status,$timeend=false) {  
          $param=array('id'=>$id,'status'=>$status);
          if($timeend){$param=array('id'=>$id,'status'=>$status,'timelastexec'=>new \Datetime());}
           $result=$this->updateNativeSql($param);
           return $result;
       
     }
  
    public function start($param) {  
         $id=0;
         $timestart=null;
         if(isset($param['id'])) {$id=$param['id'];}
         if(isset($param['timestart'])) {$timestart=$param['timestart'];}
          
         $tparam=array('id'=>$id,'status'=>'processing','timestart'=>$timestart);
         $result=$this->updateNativeSql($tparam,false);
         return $result;
    }
    public function end($param) {  
         $id=0;
         $timestart=null;
         $timeend=new \Datetime();
         $datashouldexec=null;
         $dataexec=null;
         $resultinfo=null;
         
          if(isset($param['id'])) {$id=$param['id'];}
          if(isset($param['timestart'])) {$timestart=$param['timestart'];}
          
           if(isset($param['resultinfo']['datashouldexec'])) {$datashouldexec=$param['resultinfo']['datashouldexec'];}
           if(isset($param['resultinfo']['dataexec'])) {$dataexec=$param['resultinfo']['dataexec'];}
           if(isset($param['resultinfo'])) {$resultinfo=$param['resultinfo'];}
           
           $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
           $resultinfo= $json->encode($resultinfo);
           
         $start=$timestart->getTimestamp();
         $end=$timeend->getTimestamp();
         $timeexec=$end-$start; 
         $tparam=array('id'=>$id,'status'=>'active','laststatus'=>'success','timeend'=>new \Datetime(),'timeexec'=>$timeexec,'datashouldexec'=>$datashouldexec,'dataexec'=>$dataexec,'resultinfo'=>$resultinfo);
         $result=$this->updateNativeSql($tparam,false);
         return $result;
    }
    
    public function removeNativeSqlService() {
             $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
           
            unset($param['_service']);
            unset($param['_function']);
            unset($param['_token']);
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $entity=$badiuSession->get()->getEntity();
             $param['entity']=$entity;
             $id=$param['id'];
             
           
            $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
            
            $datatasklog=$this->getContainer()->get('badiu.system.scheduler.tasklog.data');
            $datatasklogmsg=$this->getContainer()->get('badiu.system.scheduler.tasklogmessage.data');

             //list of subprocess
             $tasksubprocessdata=$this->getContainer()->get('badiu.system.scheduler.tasksubprocess.data');
             $lissubprocess=$tasksubprocessdata->getColumnValues($entity,'id',array('taskid'=>$id));
             foreach ($lissubprocess as $row) {
                  $supid=$utildata->getVaueOfArray($row,'id');
                  $spparam['id']=$supid;
                  $tasksubprocessdata->removeNativeSql($spparam,false);
             }

            //list of log
            $listlog=$datatasklog->getColumnValues($entity,'id',array('taskid'=>$id));
           
         

            foreach ($listlog as $row) {
               $logid=$utildata->getVaueOfArray($row,'id');
               //remove message
               $datatasklogmsg->removeByParam($entity,array('tasklogid'=>$logid));
               
               //remove log
               $param['id']=$logid;
              
               $datatasklog->removeNativeSql($param,false);
            }

            
            $param['id']=$id;
          
            $result= parent::removeNativeSql($param,false);
            return $result;
     }
	 
	 
	 public function getListByStringShortname($param) {
				$startshortname=null;
				if(isset($param['startshortname'])){
					$startshortname=$param['startshortname'];
					unset($param['startshortname']);
				}
				
				$wsqlst="";
				if(!empty($startshortname)){
					$ltext="'".$startshortname."%'";
					$wsqlst=" AND o.shortname LIKE $ltext";
				}
				
				
				$wsql=$this->makeSqlWhere($param);
				$sql="SELECT o FROM ".$this->getBundleEntity()." o  WHERE  o.id > :bnetctrid $wsql $wsqlst ";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('bnetctrid',0);
                $query=$this->makeSqlFilter($query, $param);
				
               $result= $query->getArrayResult();
             
			return $result;
    }
}
