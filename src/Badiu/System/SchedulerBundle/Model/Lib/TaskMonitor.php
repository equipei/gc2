<?php
namespace Badiu\System\SchedulerBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

class TaskMonitor extends TaskGeneralExec {

    //http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_service=badiu.financ.sell.sell.lib.syncfromwoocommerce.sellsnrolmoodle&operation=view&productcode=100.0020.0070.0110.0000.001.009&status=pending&sserviceid=18
    private $taskdb = null;

    function __construct(Container $container) {
        parent::__construct($container);
        $this->taskdb = $this->getContainer()->get('badiu.system.scheduler.task.data');
    }
    
    public function update() {
        $entity=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('entity');
       // $list=$this->taskdb->getProcessing($entity);
        $status=$this->getUtildata()->getVaueOfArray($this->getParam(),'status');
        
        $result = array('datashouldexec' => 0, 'dataexec' => 0);
        if(empty($status)){$result['info'] ='no status defined';return  $result;}
        $paramfilter=array('status'=>$status);

        $fields=" o.id,o.timestart AS lastdateexec ";
        if($status=='queueanalysis'){$fields=" o.id,o.queuetimestart AS lastdateexec ";}
        $list=$this->taskdb->getGlobalColumnsValues($fields,$paramfilter);
       
        if(!is_array($list)){return $result;}
        $datashouldexec=0;
        $dataexec=0;
      
        foreach ($list as $row) {
            $datashouldexec = 0;
            $id=$this->getUtildata()->getVaueOfArray($row,'id');
            $lastdateexec=$this->getUtildata()->getVaueOfArray($row,'lastdateexec');   
            $process=$this->cancelProcessingStatus($lastdateexec);
            if($process){
                $datashouldexec++;
                $paramupdate=array('id'=>$id,'status'=>'active','timemodified'=>new \DateTime());
                $updresult= $this->taskdb->updateNativeSql($paramupdate,false);
                if($updresult){$dataexec++;}
                
            }
           
        }
        $result = array('datashouldexec' =>$datashouldexec, 'dataexec' => $dataexec);
        return $result;
    }

    public function cancelProcessingStatus($lastdateexec) {
        $now = new \DateTime(); 
        $now=$now->getTimestamp();

        if(empty($lastdateexec)){return false;}
        $lastdateexec=$lastdateexec->getTimestamp();
       
       
        $diff=$now-$lastdateexec;
       
        if($diff >=3600){return true;}

        return false;
    }

}
