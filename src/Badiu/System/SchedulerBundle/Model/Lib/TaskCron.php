<?php

namespace Badiu\System\SchedulerBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Badiu\System\SchedulerBundle\Entity\SystemSchedulerTask;
class TaskCron extends BadiuModelLib{
  
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
              
	public function exec() {
		$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$taskexec=$this->getContainer()->get('badiu.system.scheduler.task.exec');
		$result=$taskexec->exec($param);
		return $result; 
	}
	
}
