<?php

namespace Badiu\System\SchedulerBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class TaskReport extends BadiuModelLib {

    private $logtimestart;
    private $router;
    function __construct(Container $container) {
        parent::__construct($container);
        $this->router=null;
    }

    public function save() {
        $this->logtimestart = new \DateTime();
        $param = $this->getFormParam();
       
        $data = $this->getContainer()->get('badiu.system.scheduler.task.data');
        $result = $data->insertNativeSql($param,false);
        $this->addlog('create','badiu.system.scheduler.task.resport.add');
        $this->logOffAccessRemoteByToken();
        return $result;
    } 

    public function addcopy() {
        $this->logtimestart = new \DateTime();
        $param = $this->getFormParam();
        $data = $this->getContainer()->get('badiu.system.scheduler.task.data');
        $result = $data->insertNativeSql($param,false);
        $this->addlog('add','badiu.system.scheduler.task.resport.addcopy');
        $this->logOffAccessRemoteByToken();
        return $result;
    }

    public function editfilter() {
        $this->logtimestart = new \DateTime();
        $param = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
  
        
        $query = $this->getUtildata()->getVaueOfArray($param, 'query');

        $id = $this->getUtildata()->getVaueOfArray($param, 'id');
        $parentid = $this->getUtildata()->getVaueOfArray($param, 'parentid');
        $urlgoback = $this->getUtildata()->getVaueOfArray($param, 'urlgoback');
        if (empty($id)) {
            return null;
        }
        $queystring = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
        $queystring->setQuery($query);
        $queystring->makeParam();
        if (!empty($parentid)) {
            $this->parentid=$parentid;
            $queystring->add('parentid', $parentid); 
            if($queystring->existKey('_datasource') && empty($queystring->getValue('_datasource'))){$queystring->add('_datasource','servicesql');}
           
        }
       
        $pquery = $queystring->getParam();
        $data = $this->getContainer()->get('badiu.system.scheduler.task.data');
      
        // $rconfig = $data->getColumnValue($this->getEntity(), 'reportfilter', array('id' => $id));
       $rconfig = $data->getGlobalColumnValue('reportfilter', array('id' => $id)); //review for access remote
        
       
        $rconfig = $this->getJson()->decode($rconfig, true);
        $pquery=$this->cleanParamSchedulerformcloneAutocompelte($pquery);
        
        $rconfig['filter'] = $pquery;

        $rconfig = $this->getJson()->encode($rconfig);

        $paramupdate = array('reportfilter' => $rconfig, 'id' => $id);

        $result = $data->updateNativeSql($paramupdate,false);

        $response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
        $successmessage = $this->getTranslator()->trans('badiu.system.crud.message.edit.sucess');
        //remove , 'urlgoback' => $urlgoback
        $outrsult = array('result' => $result, 'message' => $successmessage);

        $response->setStatus("accept");
        $response->setMessage($outrsult);
         $this->addlog('edit','badiu.system.scheduler.task.resport.updatefilter');
         $this->logOffAccessRemoteByToken();
        return $response->get();
    }

    public function edit() {
        $this->logtimestart = new \DateTime();
        $param = $this->getFormParam(false);
        $data = $this->getContainer()->get('badiu.system.scheduler.task.data');
        $result = $data->updateNativeSql($param,false);
        $this->addlog('edit','badiu.system.scheduler.task.resport.updateconfig');
        $this->logOffAccessRemoteByToken();
        return $result; 
    }
    
    public function makeJsonTimeroutineparam($param) {
        $timeroutine = $this->getUtildata()->getVaueOfArray($param, 'timeroutine');
        $newtimeroutineparam = array();
        if ($timeroutine == 'system_daily') {
            $hour = $this->getUtildata()->getVaueOfArray($param, 'timeroutineparam.x', true);
            $minutes = $this->getUtildata()->getVaueOfArray($param, 'timeroutineparam.y', true);
            $newtimeroutineparam['hour'] = $hour;
            $newtimeroutineparam['minute'] = $minutes;
            $param['timeexeconce'] =null;
        } else if ($timeroutine == 'system_timeinterval') {
            $value = $this->getUtildata()->getVaueOfArray($param, 'timeroutineparam.x', true);
            $timeunit = $this->getUtildata()->getVaueOfArray($param, 'timeroutineparam.y', true);
            $timeunit = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions')->getTimeUnitShortKey($timeunit);
            $newtimeroutineparam['value'] = $value;
            $newtimeroutineparam['timeunit'] = $timeunit;
            $param['timeexeconce'] =null;
        } else if ($timeroutine == 'system_weekly') {
            $dayofweek = $this->getUtildata()->getVaueOfArray($param, 'timeroutineparam.x', true);
            $hour = $this->getUtildata()->getVaueOfArray($param, 'timeroutineparam.y', true);
            $minutes = $this->getUtildata()->getVaueOfArray($param, 'timeroutineparam.z', true);
            $newtimeroutineparam['day'] = $dayofweek;
            $newtimeroutineparam['hour'] = $hour;
            $newtimeroutineparam['minute'] = $minutes;
            $param['timeexeconce'] =null;
        } else if ($timeroutine == 'system_monthly') {
            $day = $this->getUtildata()->getVaueOfArray($param, 'timeroutineparam.x', true);
            $hour = $this->getUtildata()->getVaueOfArray($param, 'timeroutineparam.y', true);
            $minutes = $this->getUtildata()->getVaueOfArray($param, 'timeroutineparam.z', true);
            $newtimeroutineparam['day'] = $day;
            $newtimeroutineparam['hour'] = $hour;
            $newtimeroutineparam['minute'] = $minutes;
            $param['timeexeconce'] =null;
        } else if ($timeroutine == 'once') {
            $value = $this->getUtildata()->getVaueOfArray($param, 'timeexeconce');
            if (!empty($value)) {
                $value = new \DateTime($value);
                $now =new \Datetime();
                $nowhour =$this->getUtildata()->getVaueOfArray($param, 'timeexeconcehour');
                if(empty($nowhour)){$nowhour=0;}
                $nowminutes =$this->getUtildata()->getVaueOfArray($param, 'timeexeconceminute');
                if(empty($nowminutes)){$nowminutes=0;}
                $value->setTime ($nowhour,$nowminutes,0);
                $param['timeexeconce'] = $value;
                
                
            } 
        }
		
		//calendar
		$newtimeroutineparam['calendar']=$this->getUtildata()->getVaueOfArray($param, 'calendar');
		
        $newtimeroutineparam = $this->getJson()->encode($newtimeroutineparam);
        $param['timeroutineparam'] = $newtimeroutineparam;
        unset($param['_urlgoback']);

        unset($param['timeexeconcehour']);
        unset($param['timeexeconceminute']);
		unset($param['calendar']);
        if(isset($param['_clientsession'])){unset($param['_clientsession']);}
        return $param;
    }

    public function getFormParam($add = true) {
        $param = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();

        unset($param['_service']);
        unset($param['_function']);

        $dconfig=array();
        $dconfig['exporttodatabase']= $this->getUtildata()->getVaueOfArray($param, 'exporttodatabase');
        $dconfig['moodlebackuprestorecourse']= $this->getUtildata()->getVaueOfArray($param, 'moodlebackuprestorecourse');
        $taksid=$this->getUtildata()->getVaueOfArray($param, 'id');
        unset($param['exporttodatabase']);
        unset($param['moodlebackuprestorecourse']);
        
        if ($add) {
            unset($param['id']);
        }
        $param['status'] = $this->getUtildata()->getVaueOfArray($param, 'status');
        $param['subprocessstatus']='none';
        $param['dtype'] = 'service';
        $param['address'] = 'badiu.system.core.lib.report.factoryscheduler';
        $param['deleted'] = 0;
        $param['doperation'] = 'report';
		$param['lmodel'] = 100;
        
       
        //serverservice
        $serviceid = $this->getUtildata()->getVaueOfArray($param, 'reportfilter.filter._serviceid', true);
        $route = $this->getUtildata()->getVaueOfArray($param, 'reportfilter.config.router', true);
        $this->router=$route;
        $key = $this->getUtildata()->getVaueOfArray($param, 'reportfilter.filter._key', true);
        $dkey = $this->getUtildata()->getVaueOfArray($param, 'reportfilter.filter._dkey', true);
        
        $this->parentid=$this->getUtildata()->getVaueOfArray($param, 'reportfilter.filter.parentid', true);
      
        if(empty($this->parentid) && !empty($serviceid)){$this->parentid=$serviceid;}

        if(!empty($dkey)){
            $key='badiu.system.core.report.dynamic.index';
            if(!empty($this->parentid)){$key='badiu.system.core.report.dynamicp.index';}
            $param['reportfilter']['config']['router'] = $key; 
        }

        //default modulekey
        if (!empty($route)) {
            $param['modulekey'] = $this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($route);
			  $param['moduleinstance'] = $this->parentid;
        }

        $posmdl = stripos($route, "badiu.moodle.mreport");
        if (!empty($serviceid) && $posmdl !== false) {
            $param['modulekey'] = 'badiu.moodle.mreport.site';
            $param['moduleinstance'] = $serviceid;
        }
		//for extra functionality
		$posmdl = stripos($route, "badiu.extra.");
        if (!empty($serviceid) && $posmdl !== false) {
            $param['modulekey'] = 'badiu.moodle.mreport.site';
            $param['moduleinstance'] = $serviceid;
        }
		
        // come from client
        if ($route == 'badiu.system.core.service.process') {
            $param['modulekey'] = 'badiu.moodle.mreport.site';
            $param['moduleinstance'] = $serviceid;
            $param['reportfilter']['config']['router'] = $key;
           // $param['_datasource']='servicesql';
            $this->router= $key;
        }
        
        $param['functionalitykey'] =$this->router;


        $reportfilter = $this->getUtildata()->getVaueOfArray($param, 'reportfilter');
        $reportfilter = $this->getJson()->encode($reportfilter);
        if ($add) {
            $param['reportfilter'] = $reportfilter;
        } else {
            unset($param['reportfilter']);
        }
        if ($add) {
             $dconfig=$this->getJson()->encode($dconfig);
        } else { 
            $tdata = $this->getContainer()->get('badiu.system.scheduler.task.data');
            $rdconfig = $tdata->getGlobalColumnValue('dconfig', array('id' => $taksid));
            $rdconfig = $this->getJson()->decode($rdconfig,true);
            $rdconfig['exporttodatabase']=$this->getUtildata()->getVaueOfArray($dconfig, 'exporttodatabase');
            $rdconfig['moodlebackuprestorecourse']=$this->getUtildata()->getVaueOfArray($dconfig, 'moodlebackuprestorecourse');
            $dconfig=$this->getJson()->encode($rdconfig);
        }
        $param['dconfig'] =  $dconfig;
 
        $param = $this->makeJsonTimeroutineparam($param);
        $param = $this->castDate($param);
        $reportmessage = $this->getUtildata()->getVaueOfArray($param, 'reportmessage');
        $reportmessage = $this->getJson()->encode($reportmessage);
        $param['reportmessage'] = $reportmessage;
        $param['entity'] = $this->getEntity();
        if ($add) {
            $param['timecreated'] = new \DateTime();
        } else { 
            $param['timemodified'] = new \DateTime();
        }
       
	   //entity for remote service
	   $modulekey=$this->getUtildata()->getVaueOfArray($param, 'modulekey');
	  if($modulekey=='badiu.moodle.mreport.site' && ($serviceid == $this->parentid) && !empty($this->parentid) ){
		  $entity = $this->getContainer()->get('badiu.admin.server.service.data')->getGlobalColumnValue('entity',array('id'=>$serviceid));
         if(!empty($entity)){$param['entity'] =$entity ;}
	  }
        return $param;
    }

    public function cleanParamSchedulerformcloneAutocompelte($param) {
         //remove autocomplete
         $newparam=array();
         $autocmpparam=array();
        foreach ($param as $key => $value) {
                $pos = stripos($key, "_badiuautocomplete");
                if ($pos === false){
                    $newparam[$key]=$value;
                    $pos1 = stripos($key, "_badiuschlclone_");
                    if ($pos1 !== false){
                        $nkey=str_replace("_badiuschlclone_","",$key);
                        $autocmpparam[$nkey]=$value; 
                        unset($newparam[$key]);
                    } 
                }
               
             }
             //replace value of clone autocomplete
             foreach ($autocmpparam as $key => $value) {
                 if(!empty( $value)){ $newparam[$key]=$value;}
              }

        return $newparam;
    }
    public function castDate($param) {

        $execperiodstart = $this->getUtildata()->getVaueOfArray($param, 'execperiodstart');
        $execperiodend = $this->getUtildata()->getVaueOfArray($param, 'execperiodend');
        if (!empty($execperiodstart)) {
            $execperiodstart = new \DateTime($execperiodstart);

            $nowhour =$this->getUtildata()->getVaueOfArray($param, 'execperiodstarthour');
            if(empty($nowhour)){$nowhour=0;}
            $nowminutes =$this->getUtildata()->getVaueOfArray($param, 'execperiodstartminute');
            if(empty($nowminutes)){$nowminutes=0;}

            $execperiodstart->setTime ($nowhour,$nowminutes,0);
        }
        if (!empty($execperiodend)) {
            $execperiodend = new \DateTime($execperiodend);
            $nowhour =$this->getUtildata()->getVaueOfArray($param, 'execperiodendhour');
            if(empty($nowhour)){$nowhour=23;}
            $nowminutes =$this->getUtildata()->getVaueOfArray($param, 'execperiodendminute');
            if(empty($nowminutes)){$nowminutes=59;}
            $execperiodend->setTime ($nowhour,$nowminutes,0);
        }
        $param['execperiodstart'] = $execperiodstart;
        $param['execperiodend'] = $execperiodend;


        unset($param['execperiodstarthour']);
        unset($param['execperiodstartminute']);

        unset($param['execperiodendhour']);
        unset($param['execperiodendminute']);

        return $param;
    }

    private function addlog($action,$subaction) {
         //$parentid =$this->parentid;
        $parentid = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
        $dloglib = $this->getContainer()->get('badiu.system.log.core.lib');
        $logtimeend = new \DateTime();
        if (!empty($this->logtimestart)) {
            $start = $this->logtimestart->getTimestamp();
            $end = $logtimeend->getTimestamp();
            $timeexec = $end - $start;
            $this->log['timeexec'] = $timeexec;
        }
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $sessiontype = $badiuSession->get()->getType();
        if ($sessiontype == 'webservice') {
            $logparam = $badiuSession->get()->getWebserviceinfo();
        } else if ($sessiontype == 'web') {
            $key=$this->router;
            $bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
            if (!isset($this->log['dtype'])) {
                $this->log['dtype'] = 'web';
            }
            if (!isset($this->log['clientdevice'])) {
                $this->log['clientdevice'] = 'server';
            }
            if (!isset($this->log['modulekey'])) {
                $this->log['modulekey'] =  $bkey;
            }
            if (!isset($this->log['moduleinstance'])) {
                $this->log['moduleinstance'] = $parentid;
            }
            if (!isset($this->log['functionalitykey'])) {
                $this->log['functionalitykey'] =$key;
            }

            if (!isset($this->log['action'])) {
                $this->log['action'] = $action;
            }
            if (!isset($this->log['rkey'])) {
                $this->log['rkey'] = null;
            }
            if (!isset($this->log['ip'])) {
                $this->log['ip'] = $_SERVER["REMOTE_ADDR"];
            }


            if (!isset($this->log['param'])) {
                $param = array();
                $client = array('browser' => $_SERVER['HTTP_USER_AGENT'], 'ip' => $_SERVER["REMOTE_ADDR"]);
                $param['client'] = $client;

                $user = array('firstname' => $badiuSession->get()->getUser()->getFirstname(),
                    'lastname' => $badiuSession->get()->getUser()->getLastname(),
                    'id' => $badiuSession->get()->getUser()->getId(),
                    'email' => $badiuSession->get()->getUser()->getEmail(),
                    'roles' => null);
                $param['user'] = $user;
                $param['scheduler']=$subaction;
                $sserver = array();

                
                $pos = stripos($key, "badiu.moodle.mreport");

                $serviceid =$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
                if ($pos !== false && $serviceid == $parentid && !empty($parentid)) {
                    $servicedata = $this->getContainer()->get('badiu.admin.server.service.data');
                    $info = $servicedata->getInfoById($serviceid);
                    $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
                    $sserver['id'] = $serviceid;
                    $sserver['url'] = $utildata->getVaueOfArray($info, 'url');
                    $sserver['name'] = $utildata->getVaueOfArray($info, 'name');
                    $sserver['entity'] = $utildata->getVaueOfArray($info, 'entity');
                    $param['sserver'] = $sserver;
                }


                $query = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
                if (!isset($query['parentid']) && !empty($parentid)) {
                    $query['parentid'] = $parentid;
                }
                $param['query'] = $query;
                $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
                $logparam = $json->encode($param);
                $this->log['param'] = $logparam;
               
            }
        }



        
        $this->log['entity'] = $badiuSession->get()->getEntity();
		if ($sessiontype == 'webservice') {
			 $ssdbdconfig = $badiuSession->get()->getWebserviceinfo();
			 $serversyncstatus=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.serversync.status',true);
			 $serversyncmessage=$this->getUtildata()->getVaueOfArray($ssdbdconfig,'user.serversync.message',true); 
			 if($serversyncstatus=='acept' && !empty($serversyncmessage) && is_numeric($serversyncmessage)){$this->log['userid'] =$serversyncmessage;}
		}

        

        $dloglib->add($this->log);
    }

    function logOffAccessRemoteByToken() {
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $dsession=$badiuSession->get();
         if($dsession->getType()=='webservice_token_access'){
            $badiuSession->delete();
         }
    }
    function getLogtimestart() {
        return $this->logtimestart;
    }

    function setLogtimestart($logtimestart) {
        $this->logtimestart = $logtimestart;
    }
   


}
