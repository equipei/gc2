<?php

namespace Badiu\System\SchedulerBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class TaskGeneralExec extends BadiuModelLib {
      /**
     * @var integer
     */
    private $maxrecordtosync;
    /**
     * @var integer
     */
    private $maxrecordtosyncperconn;
    private $operation = null;
    private $param = null;
    private $taskdto = null;
    private $tasklogid = null;

         /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
    function __construct(Container $container) {
        parent::__construct($container);
        $this->maxrecordtosync=5000;
        $this->maxrecordtosyncperconn=200;
    }
    public function exec() {
            $resultoriginal=array();
            $result=array();
            if($this->getOperation()=='import'){$result=$this->import();}
            else if($this->getOperation()=='update'){$result=$this->update();}
            else if($this->getOperation()=='report'){$result=$this->report();}
            else {$result=$this->update();}
          return $result;
    }
    
    public function import() {
        $result = array('datashouldexec' => 0, 'dataexec' => 0, 'message' => null);
        return $result;
    } 

    public function delete() {
        $result = array('datashouldexec' => 0, 'dataexec' => 0, 'message' => null);
        return $result;
    }

    public function update() {
        $result = array('datashouldexec' => 0, 'dataexec' => 0, 'message' => null);
        return $result;
    }
    public function report() {
        $result = array('datashouldexec' => 0, 'dataexec' => 0, 'message' => null);
        return $result;
    }
    public function addResult($resultx, $resulty) {
        $datashouldexecx = 0;
        $dataexecx = 0;
        $messagex = "";


        $datashouldexecy = 0;
        $dataexecy = 0;
        $messagey = "";

        $log = array();
       
        if (isset($resultx['datashouldexec'])) {
            $datashouldexecx = $resultx['datashouldexec'];
        }
        if (isset($resultx['dataexec'])) {
            $dataexecx = $resultx['dataexec'];
        }
        
        if (isset($resultx['log'])) {
            $log = $resultx['log'];
         }


        if (isset($resulty['datashouldexec'])) {
            $datashouldexecy = $resulty['datashouldexec'];
        }
        if (isset($resulty['dataexec'])) {
            $dataexecy = $resulty['dataexec'];
        }
       
        array_push($log, $resulty);
        
        $result = array('datashouldexec' => $datashouldexecx + $datashouldexecy, 'dataexec' => $dataexecx + $dataexecy, 'log' => $log);
        return $result;
    }

    public function addParamFilter($filter) {

        if (!is_array($filter)) {
            return $filter;
        }
        if (!is_array($this->param)) {
            return $filter;
        }

        foreach ($this->param as $key => $value) {
            if (ctype_digit($value)) {
                $value = $value + 0;
            }
            $filter[$key] = $value;
        }
        return $filter;
    }

    function getOperation() {
        return $this->operation;
    }

    function getParam() {
        return $this->param;
    }

    function setOperation($operation) {
        $this->operation = $operation;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function getMaxrecordtosync() {
        return $this->maxrecordtosync;
    }

    function getMaxrecordtosyncperconn() {
        return $this->maxrecordtosyncperconn;
    }

    function setMaxrecordtosync($maxrecordtosync) {
        $this->maxrecordtosync = $maxrecordtosync;
    }

    function setMaxrecordtosyncperconn($maxrecordtosyncperconn) {
        $this->maxrecordtosyncperconn = $maxrecordtosyncperconn;
    }
    function getTaskdto() {
        return $this->taskdto;
    }

    function setTaskdto($taskdto) {
        $this->taskdto = $taskdto;
    }

    function getTasklogid() {
        return $this->tasklogid;
    }

    function setTasklogid($tasklogid) {
        $this->tasklogid = $tasklogid;
    }

	public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }

}
