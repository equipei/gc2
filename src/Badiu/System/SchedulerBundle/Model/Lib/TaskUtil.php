<?php

namespace Badiu\System\SchedulerBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class TaskUtil extends BadiuModelLib{
    //http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_service=badiu.system.scheduler.task.util&_function=redirectToEditLink&id=61
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
	public function redirectToEditLink() {
			$id=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('id');
                        $_urlgoback=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urlgoback');
                        $_foperation=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_foperation');
                        $reportfilter=$this->getContainer()->get('badiu.system.scheduler.task.data')->getGlobalColumnValue('reportfilter',array('id'=>$id));
			$reportfilter=$this->getJson()->decode($reportfilter,true);
                        $router=$this->getUtildata()->getVaueOfArray($reportfilter,'config.router',true);
                        $filter=$this->getUtildata()->getVaueOfArray($reportfilter,'filter');
                        $filter['_badiuscheduleraction']='_badiusystemreporscheduleredit';
                        $filter['_urlgoback']=$_urlgoback;
                        $filter['_foperation']=$_foperation;
                        $filter['_badiuschedulerid']=$id;
                        $url=$this->getContainer()->get('badiu.system.core.lib.util.app')->getUrlByRoute($router,$filter);
                        header('Location: '.$url);
                        exit;
                        //print_r($reportfilter);echo "<hr>";
                      //  print_r($url);exit;
                        return null;
		}
	
	

}
