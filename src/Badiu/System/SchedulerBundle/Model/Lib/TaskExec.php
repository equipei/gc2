<?php

namespace Badiu\System\SchedulerBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Badiu\System\SchedulerBundle\Entity\SystemSchedulerTask;
class TaskExec extends BadiuModelLib{
    private $logtimestart;
   // private $type;
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
      //  tcron,typeexec      
	public function exec($param) {
      
                $this->logtimestart=new \DateTime();
                $badiuSession=$this->getContainer()->get('badiu.system.access.session');
				
				$tcron=$this->getUtildata()->getVaueOfArray($param,'tcron');
				if(empty($tcron)){
					$tcron=$badiuSession->getValue('badiu.system.scheduler.task.param.config.crondefaulttype');
					if(empty($tcron)){$tcron='cron1';}
				}
				
				$lcron=$this->getUtildata()->getVaueOfArray($param,'lcron');
				if(empty($lcron)){
					$lcron=$badiuSession->getValue('badiu.system.scheduler.task.param.config.crondefaultlevel');
					if(empty($lcron)){$lcron='entity';}
					if($this->getContainer()->hasParameter('badiu.system.core.type.instalation')){
						$typeinstalation=$this->getContainer()->getParameter('badiu.system.core.type.instalation');
						if($typeinstalation=='multipleentity'){$lcron="global";}
					
					}
				}
				
				$typeexec=$this->getUtildata()->getVaueOfArray($param,'typeexec');
				if(empty($typeexec)){$typeexec='automatic';}
				
				$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
				$id=$this->getUtildata()->getVaueOfArray($param,'id');
				$force=$this->getUtildata()->getVaueOfArray($param,'_force');
				
				$statusexec=$this->getUtildata()->getVaueOfArray($param,'statusexec');
				if(empty($statusexec)){$statusexec='active';}
				
				$typeexec=$this->getUtildata()->getVaueOfArray($param,'typeexec');
				if(empty($typeexec)){$typeexec='automatic';}
				
				$maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
				if(empty($maxrecord)){$maxrecord=$badiuSession->getValue('badiu.system.scheduler.task.param.config.crondefaultmaxcord');}
				if(empty($maxrecord)){$maxrecord=20;}
				
                $cronlogdata=$this->getContainer()->get('badiu.system.scheduler.cronlog.data');
                $cromlogparami=array('name'=>$typeexec,'timestart'=>$this->logtimestart,'status'=>'processing','dtype'=>$typeexec);
                $cronlogid=$cronlogdata->insertNativeSql($cromlogparami,false);
              
                $data=$this->getContainer()->get('badiu.system.scheduler.task.data');
                $datalog=$this->getContainer()->get('badiu.system.scheduler.tasklog.data');
                
				$ftparam=array();
				$ftparam['status']=$statusexec;
				$ftparam['tcron']=$tcron;
				$ftparam['lcron']=$lcron;
				$ftparam['lmodel']=100;
				$ftparam['deleted']=0;
				$ftparam['maxrecord']=$maxrecord;
				$ftparam['sortorder']='old';
				if(!empty($entity)){$ftparam['entity']=$entity;}
				if(!empty($id)){
					$ftparam['id']=$id;
					unset($ftparam['status']);
					unset($ftparam['tcron']);
					unset($ftparam['lcron']);
					unset($ftparam['sortorder']);
				} 
				
                $list=$data->getListForCron($ftparam);
			
				//monitor itms
				$monitorftparam=array();
				$monitorftparam['deleted']=0;
				$monitorftparam['tcron']=$tcron;
				$monitorftparam['lcron']=$lcron;
				$monitorftparam['lmodel']=100;
				$monitorlist=$data->getListMonitorForCron($monitorftparam);
				if(is_array($monitorlist)){
					if(!is_array($list)){$list=array();}
					foreach ($monitorlist as $mitem) {array_push($list, $mitem);}
				}
				
				
				//new itens
				if(empty($id)){
					$ftparam['sortorder']='new';
					$listnewitem=$data->getListForCron($ftparam);
					if(is_array($listnewitem)){
						if(!is_array($list)){$list=array();}
						foreach ($listnewitem as $nitem) {array_push($list, $nitem);}
					}
				}
			
                $this->addStatusQueueAnalysis($list);
              
                //update cron with countqueue
                $counttaskqueue=sizeof($list);
                $paramcroncoutqueue=array('id'=>$cronlogid,'taskqueue'=>$counttaskqueue);
                $cronlogdata->updateNativeSql($paramcroncoutqueue,false);
                
                $crondatashouldexec=0;
                $crondataexec=0;
                $timestart=new \Datetime();
                $tasklogid=0;
                
                foreach ($list as $taskarray) {
                      $task=$this->castTaskArrayToObject($taskarray);
					   $taskid=$task->getId();
                        try{ 
							$badiuSession=$this->initSession($task);
                            $exec=$this->isJustExec($task,$badiuSession->getHashkey());
                            $tstatus=$task->getStatus();
							
							$shortname=$this->getUtildata()->getVaueOfArray($task,'shortname');
							$pos=stripos($shortname, "monitor_");
							if($pos!== false  && $tstatus !='active'){$tstatus='active';}
							
                           if($force && $tstatus=='active'){$exec=true;}
                           if($force){$exec=true;}
                            if($exec){ 
                                $crondatashouldexec++;

                                $dtype=$task->getDtype();
                              
                             
                               $address=$task->getAddress();
                               $operation=$task->getDoperation();
                               $confparam=$this->getJson()->decode($task->getParam(),true);
                               $result=null;
                               
                               
                              
                               if($dtype=='service'){
                                  if(!empty($address) && $this->getContainer()->has($address)){
                                    
                                     $service=$this->getContainer()->get($address);
                                    //start task 
                                    $timestart=new \Datetime();
                                    $tparam=array('id'=>$taskid,'timestart'=>$timestart);
                                    $data->start($tparam);
                                    
                                    $param=array('taskid'=>$taskid,'entity'=>$task->getEntity(),'timestart'=>$timestart,'cronlogid'=>$cronlogid);
                                    $tasklogid=$datalog->start($param); 
                                    
                                    //set datetimeend of StatusQueueAnalysis
                                    if(!$force){$this->endStatusQueueAnalysis($taskid);}

                                    $service->setTasklogid($tasklogid);
                                    $service->setOperation($operation); 
                                    $service->setParam($confparam);
                                    $service->setTaskdto($task);
                                    $service->setSessionhashkey($badiuSession->getHashkey());
									$this->getSystemdata()->setEntity($task->getEntity());
									$this->getSystemdata()->init();
									$service->setSystemdata($this->getSystemdata());
                                    $result=array();
                                    
                                    $result=$service->exec();
                                    
                                    $param=array('tasklogid'=>$tasklogid,'timestart'=>$timestart,'resultinfo'=>$result);
                                    $datalog->end($param);
                                    
                                    $tparam=array('id'=>$taskid,'timestart'=>$timestart,'resultinfo'=>$result);
                                    $data->end($tparam);
                                    
                                    $crondataexec++;
                                    
                                    
                                    $badiuSession->delete(); 
                                   
                                 }
                               }
                               $badiuSession->delete(); 
                               
                             }
                            //$exec is false
                             else{
                             
                               //chante status from queueanalysis to anable
                              
                                $etparam=array('id'=>$task->getId(),'status'=>'active'); 
                                $data->updateNativeSql($etparam,false);
                                if(!$force){$this->endStatusQueueAnalysis($task->getId());}
                                 $badiuSession->delete(); 
                             }
                           }catch (\Exception $e) { 
                               $msgerror=$e->getMessage();
                              
                               //$msgerror=addslashes($msgerror);
                                $timeend=new \DateTime();
                                $timeexec=$timeend->getTimestamp()-$timestart->getTimestamp();
                                $tglogparam=array('id'=>$tasklogid,'timestart'=>$timestart,'timeend'=>$timeend,'timeexec'=>$timeexec,'status'=>'failed','msgerror'=>$msgerror);
                            
                                $datalog->updateNativeSql($tglogparam,false);
 
                                $tparam=array('id'=>$taskid,'timestart'=>$timestart,'timeend'=>$timeend,'timeexec'=>$timeexec,'status'=>'failed','msgerror'=>$msgerror);
                                $data->updateNativeSql($tparam,false);
                                if($force){echo  $msgerror;}

                               if(isset($badiuSession)){ $badiuSession->delete();}
                            } 
                            
                           
                           $this->desable($task);
                           if($force && $id){echo "crondatashouldexec:  $crondatashouldexec | crondataexec: $crondataexec";}
                }
                //and cron log
                $logtimeend=new \DateTime();
                $start=$this->logtimestart->getTimestamp();
                $end=$logtimeend->getTimestamp();
                $timeexec=$end-$start; 
                $paramcroncoutqueue=array('id'=>$cronlogid,'timeend'=>$logtimeend,'timeexec'=>$timeexec,'datashouldexec'=>$crondatashouldexec,'dataexec'=>$crondataexec);
                $cronlogdata->updateNativeSql($paramcroncoutqueue,false);
                $list=null;
			return null;
	}
	
 
   
        public function isJustExec($task,$sessionhashkey) {
            $subprocessstatus=$task->getSubprocessstatus();
            if($subprocessstatus=='completed'){return false;}

              $schedulerroutine=$this->getContainer()->get('badiu.system.core.lib.date.schedulerroutine');
              $schedulerroutine->setSessionhashkey($sessionhashkey);
			  $schedulerroutine->setSystemdata($this->getSystemdata());
              $param=array();
              $param['timeroutine']=$task->getTimeroutine();
              $param['timeroutineparam']=$this->castToJson($task->getTimeroutineparam());
              $param['timelastexec']=$task->getTimestart();
              $param['timeexeconce']=$task->getTimeexeconce();
              $param['execperiodstart']=$task->getExecperiodstart(); 
              $param['execperiodend']=$task->getExecperiodend();
            
              $schedulerroutine->initParam($param);
              $canexec= $schedulerroutine->canExec();
            
              return $canexec;
              
        }
	
    public function castToJson($text) {
            if(empty($text)){return null;}
            $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
            $jparam =$json->decode($text, false);
           return $jparam;
      }
      public function initSession($task) {
		$systemdata=$this->getContainer()->get('badiu.system.core.functionality.systemdata');
		$systemdata->setEntity($task->getEntity());
		$systemdata->init();
		$this->setSystemdata($systemdata);
				
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $badiuSession->initHashkey($task->getId());
		$badiuSession->setSystemdata($systemdata);
        $dsession = $this->getContainer()->get('badiu.system.access.session.data');

        $iscronlogscheduler=true;
        if($iscronlogscheduler){
            $logcron= $this->getContainer()->get('badiu.system.log.core.lib.cron');
            $logcron->add("started cron taskid: ". $task->getId(),array('self'=>1));
         }
        $dsession->setIscronlogscheduler($iscronlogscheduler);
        $dsession->setEntity($task->getEntity());
        $dsession->setType('cron');
        $dsession->getUser()->setId(1);
        $dsession->getUser()->setFullname('Cron exec');
       
        $dsession->setPermissions(array('badiu.')); 
        
        $badiuSession->save($dsession);
        
        //$badiuSession->initConfigEntity(); 
		
		//just started by $badiuSession->setSystemdata($systemdata);
		//$badiuSession->initCache(array('entity'=>$task->getEntity()));
        return $badiuSession;
      }

      public function desable($task) {
            $processupdate=false;
            //exec once
            if($task->getTimeroutine()=='once' ){
                $timeexeconce=$task->getTimeexeconce();
                $timelastexec=$task->getTimestart();
                if(!empty($timelastexec)){$processupdate=true;}
                else {
                    $now =new \Datetime();
                    $stdatenow=$now->format('Y-m-d');
                    $stdateconf=$timeexeconce->format('Y-m-d');
                    if($stdatenow!=$stdateconf && $timeexeconce->getTimestamp() < $now->getTimestamp() ){
                        $processupdate=true;
                    }
                }
            }
            
            //exec period
            if(!$processupdate){
                $now =new \Datetime();
                $execperiodstart=$task->getExecperiodstart(); 
                $execperiodend=$task->getExecperiodend();

                //only timestart
                if(!empty($execperiodstart) && empty($execperiodend)){
                    $processupdate=false;
                }

                //only timeend
                if(!empty($execperiodend) && empty($execperiodstart)){
                    if($execperiodend->getTimestamp() < $now->getTimestamp()){$processupdate=true;}
                }

                //timestart and timeend
                if(!empty($execperiodstart) && !empty($execperiodend)){
                    if($execperiodstart->getTimestamp() > $execperiodend->getTimestamp()){$processupdate=true;}
                    else if($execperiodstart->getTimestamp() < $now->getTimestamp() && $execperiodend->getTimestamp() < $now->getTimestamp()){$processupdate=true;}
                 }  
         }

         //subprocess completed
         if(!$processupdate){
            $subprocessstatus=$task->getSubprocessstatus();
            if($subprocessstatus=='completed'){$processupdate=true;}
         }
        
         if($processupdate){
            $tdata=$this->getContainer()->get('badiu.system.scheduler.task.data');
            $id=$task->getId();
            $tparam=array('id'=>$id,'status'=>'inactive');
            $tdata->updateNativeSql($tparam,false);
         }
      }

      public function addStatusQueueAnalysis($list) {
          if(!is_array($list)){return null;}
          $tdata=$this->getContainer()->get('badiu.system.scheduler.task.data');
            foreach ($list as $task) {
                $id=$this->getUtildata()->getVaueOfArray($task,'id');
				//$shortname=$this->getUtildata()->getVaueOfArray($task,'shortname');
				//$pos=stripos($shortname, "monitor_");
				//if($pos=== false){
					$tparam=array('id'=>$id,'status'=>'queueanalysis','queuetimestart'=>new \DateTime());
					$tdata->updateNativeSql($tparam,false);
				//}
            }
      }

      /**
       * change only queuetimeend
       *
       * @return void
       */
      public function endStatusQueueAnalysis($taskid) {
             $tdata=$this->getContainer()->get('badiu.system.scheduler.task.data');
            //get queuetimestart
            $timestart=$tdata->getGlobalColumnValue('queuetimestart',array('id'=>$taskid));
            if(empty($timestart)){return null;}
             $timeend=new \DateTime();
             $start=$timestart->getTimestamp();
             $end=$timeend->getTimestamp();
             $timeexec=$end-$start; 
            
             //set queuetimeend
              $tparam=array('id'=>$taskid,'queuetimeend'=>$timeend,'queuetimewait'=> $timeexec);
              $tdata->updateNativeSql($tparam,false);
        
    }
      public function testeMonitorSession($operation,$crontimestart,$text,$param=null) {
            $controksession="";
            $sessionstart=$this->getUtildata()->getVaueOfArray($param,'sessionstart');
            $currentsession=$crontimestart->getTimestamp();
            $badiuSession=$this->getContainer()->get('badiu.system.access.session');
            $tstart=new \DateTime();
            if($sessionstart){
                $hash = $this->getContainer()->get('badiu.system.core.lib.util.hash');
                $skey=$hash->make(50);
                $controksession=$tstart->format('d/m/Y H:i:s.u').'__'.$skey;
                $badiuSession->addValue('crontabsk',$controksession);
                
            }
            $controksessionget=$badiuSession->getValue('crontabsk');
            if(empty($controksessionget)){$controksessionget='sessionkeynotstarted';}
            $now=new \DateTime();
             $now=$now->format('d/m/Y H:i:s.u');
             if($text=='13/277'){
                $now.=" SQL: ". $badiuSession->getValue('badiu.moodle.mreport.enrol.enrol.dbsearch.sql.count');
                $badiuSession->addValue('error125','13/277');
            }
            $tlog="$currentsession | $operation| $controksessionget | $text | $now";
            $txt="";

           
            if(is_string($tlog)){ $txt=$tlog."\n"; }
            else if(is_array($tlog)){
                 $txt= json_encode($tlog);
                $txt.="\n";
            }
            $defaultpath =  $this->getContainer()->getParameter('badiu.system.file.defaultpath');
            $logfile="$defaultpath/log.txt";
             $myfile = fopen($logfile, "a") or die("Unable to open file!");
            
             fwrite($myfile, $txt);
             fclose($myfile);  
            
    }

	function castTaskArrayToObject($param) {
		$dto=new SystemSchedulerTask();
		
		$id=$this->getUtildata()->getVaueOfArray($param,'id');
		$dto->setId($id);
		
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		$dto->setEntity($entity);
		
		$name=$this->getUtildata()->getVaueOfArray($param,'name');
		$dto->setName($name);
		
		$shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
		$dto->setShortname($shortname);
		
		$dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
		$dto->setDtype($dtype);
		
		$tcron=$this->getUtildata()->getVaueOfArray($param,'tcron');
		$dto->setTcron($tcron);
		
		$modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		$dto->setModulekey($modulekey);
		
		$moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		$dto->setModuleinstance($moduleinstance);
		
		$functionalitykey=$this->getUtildata()->getVaueOfArray($param,'functionalitykey');
		$dto->setFunctionalitykey($functionalitykey);
		
		$doperation=$this->getUtildata()->getVaueOfArray($param,'doperation');
		$dto->setDoperation($doperation);
		
		$address=$this->getUtildata()->getVaueOfArray($param,'address');
		$dto->setAddress($address);
		
		$reportaction=$this->getUtildata()->getVaueOfArray($param,'reportaction');
		$dto->setReportaction($reportaction);
		
		$reporttype=$this->getUtildata()->getVaueOfArray($param,'reporttype');
		$dto->setReporttype($reporttype);
		
		$reportfilter=$this->getUtildata()->getVaueOfArray($param,'reportfilter');
		$dto->setReportfilter($reportfilter);
		
		$reportmessage=$this->getUtildata()->getVaueOfArray($param,'reportmessage');
		$dto->setReportmessage($reportmessage);
		
		$simultaneousexec=$this->getUtildata()->getVaueOfArray($param,'simultaneousexec');
		$dto->setSimultaneousexec($simultaneousexec);
		
		$timeroutine=$this->getUtildata()->getVaueOfArray($param,'timeroutine');
		$dto->setTimeroutine($timeroutine);
		
		$timeroutineparam=$this->getUtildata()->getVaueOfArray($param,'timeroutineparam');
		$dto->setTimeroutineparam($timeroutineparam);
		
		$execperiodstart=$this->getUtildata()->getVaueOfArray($param,'execperiodstart');
		$dto->setExecperiodstart($execperiodstart);
		
		$execperiodend=$this->getUtildata()->getVaueOfArray($param,'execperiodend');
		$dto->setExecperiodend($execperiodend);
		
		$timeexeconce=$this->getUtildata()->getVaueOfArray($param,'timeexeconce');
		$dto->setTimeexeconce($timeexeconce);
		
		$status=$this->getUtildata()->getVaueOfArray($param,'status');
		$dto->setStatus($status);
		
		$laststatus=$this->getUtildata()->getVaueOfArray($param,'laststatus');
		$dto->setLaststatus($laststatus);
		
		$timeexeconce=$this->getUtildata()->getVaueOfArray($param,'timeexeconce');
		$dto->setTimeexeconce($timeexeconce);
				
		$queuetimestart=$this->getUtildata()->getVaueOfArray($param,'queuetimestart');
		$dto->setQueuetimestart($queuetimestart);
		
		$queuetimeend=$this->getUtildata()->getVaueOfArray($param,'queuetimeend');
		$dto->setQueuetimeend($queuetimeend);
		
		$queuetimewait=$this->getUtildata()->getVaueOfArray($param,'queuetimewait');
		$dto->setQueuetimewait($queuetimewait);
				
		$timestart=$this->getUtildata()->getVaueOfArray($param,'timestart');
		$dto->setTimestart($timestart);
		
		$timeend=$this->getUtildata()->getVaueOfArray($param,'timeend');
		$dto->setTimeend($timeend);
		
		$timeexec=$this->getUtildata()->getVaueOfArray($param,'timeexec');
		$dto->setTimeexec($timeexec);

		
		$subprocessstatus=$this->getUtildata()->getVaueOfArray($param,'subprocessstatus');
		$dto->setSubprocessstatus($subprocessstatus);
		
		$subprocessprogress=$this->getUtildata()->getVaueOfArray($param,'subprocessprogress');
		$dto->setSubprocessprogress($subprocessprogress);
		
		$dconfig=$this->getUtildata()->getVaueOfArray($param,'dconfig');
		$dto->setDconfig($dconfig);
		
		$param=$this->getUtildata()->getVaueOfArray($param,'param');
		$dto->setParam($param);
		 
		 $reference=$this->getUtildata()->getVaueOfArray($param,'reference');
		$dto->setReference($reference);
		 
		//o.id,o.entity,o.name,o.,o.dtype,o.tcron,o.modulekey,o.moduleinstance,o.functionalitykey,o.doperation,o.address,o.reportaction,o.reporttype,,o.reportfilter,o.reportmessage,o.simultaneousexec,o.timeroutine,o.timeroutineparam,o.execperiodstart,o.execperiodend,o.timeexeconce,o.status,o.laststatus
		//,o.queuetimestart,o.queuetimeend,o.queuetimewait,o.timestart,o.timeend,o.timeexec,o.subprocessstatus,o.subprocessprogress,o.dconfig,o.param
        return $dto;
    }
    
    function getLogtimestart() {
        return $this->logtimestart;
    }

    function setLogtimestart($logtimestart) {
        $this->logtimestart = $logtimestart;
    }

   /* function getType() {
        return $this->type;
    }

    function setType($type) {
        $this->type = $type;
    }*/
}
