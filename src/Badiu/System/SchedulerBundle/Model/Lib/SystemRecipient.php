<?php

namespace Badiu\System\SchedulerBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class SystemRecipient extends BadiuModelLib{
	/*
	 @string
	*/
		private $pconfig;
		
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function getFormOptions($converlist = true){
		
			$sconf=$this->getParamofconig();
			
			if(empty($sconf)){return "";}
			$fvcomponet= $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactorycomponent');
			$fvcomponet->setSessionhashkey($this->getSessionhashkey());
			$optionsjs=$fvcomponet->makeChoiceList('defaultrecipient', $sconf->service,'getFormoptions',$converlist);
			
			return $optionsjs;
			
	}
	public function getFormOptionsList(){
		$sconf=$this->getParamofconig();
		if(empty($sconf)){return "";}
		if(!$this->getContainer()->has($sconf->service)){return null;}
		$servicesconfig=$this->getContainer()->get($sconf->service);
		$servicesconfig->setSessionhashkey($this->getSessionhashkey());
		$list=$servicesconfig->getFormoptions(); 
	return $list;
		
}
	public function getParamofconig(){
		        if(empty($this->pconfig)){return null;}
				$result=new \stdClass();
				$result->service=null;
				$result->columnkey=null;
			

				$subQueryString = $this->getContainer()->get('badiu.system.core.lib.http.subquerystring');
				$subQueryString->setQuery($this->getPconfig());
				$subQueryString->makeParam();

				$result->service=$subQueryString->getValue('service');
				$result->columnkey=$subQueryString->getValue('columnkey');
				return $result;

	}
	public function getPconfig() {
		return $this->pconfig;
}

public function setPconfig($pconfig) {
		$this->pconfig = $pconfig;
}
}
