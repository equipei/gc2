<?php

namespace Badiu\System\SchedulerBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Task extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function getDataFormEdit($taskid) {
			 $fdata=array();
			 $fdata=$this->getTaskDb($taskid,$fdata);
			return $fdata;
	}
	public function save($data) {
			$sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
			$isEdit=$sysoperation->isEdit();
			$taskid=null;
			if($isEdit){
				$taskid=$this->getContainer()->get('request')->get('id');
			}
			$dto=$data->getDto();
			
			//save user
			$data->setDto($this->getTaskForm($taskid,$dto));
			$result=$data->save();
							
			return $data->getDto();
		}
	public function getTaskDb($taskid,$fdata) {
			 
			$data=$this->getContainer()->get('badiu.system.scheduler.task.data');
			$dto=$data->findById($taskid);
			$fdata['name']= $dto->getName();
			$fdata['shortname']=$dto->getShortname();
			$fdata['dtype']=$dto->getDtype();
			$fdata['address']=$dto->getAddress();
			$fdata['simultaneousexec']=$dto->getSimultaneousexec();
			
			$fdata['timeroutine']=$dto->getTimeroutine();
			$fdata['timeroutineparam']=$dto->getTimeroutineparam();//review
			$fdata['execperiodstart']=$dto->getExecperiodstart();
			$fdata['execperiodend']=$dto->getExecperiodend();
			$fdata['timeexeconce']=$dto->getTimeexeconce();
			$fdata['status']=$dto->getStatus();
			
			$fdata['idnumber']=$dto->getIdnumber();
			$fdata['param']=$dto->getParam();
			$fdata['description']=$dto->getDescription();
                        
                        $fdata['doperation']=$dto->getDoperation();
                        $fdata['sortorder']=$dto->getSortorder();
			return $fdata;
		
	}
	
	public function getTaskForm($taskid,$fdata) {
			$dto=null;
			if(!empty($taskid)){
				$data=$this->getContainer()->get('badiu.system.scheduler.task.data');
				$dto=$data->findById($taskid);
			}
			if(empty($dto)){
				$dto=$this->getContainer()->get('badiu.system.scheduler.task.entity');
				$dto=$this->initDefaultEntityData($dto);
			}
			
			if(isset($fdata['name'])) {$dto->setName($fdata['name']);}
			if(isset($fdata['shortname'])) {$dto->setShortname($fdata['shortname']);}
			if(isset($fdata['address'])) {$dto->setAddress($fdata['address']);}
			if(isset($fdata['simultaneousexec'])) {$dto->setSimultaneousexec($fdata['simultaneousexec']);}
			if(isset($fdata['timeroutine'])) {$dto->setTimeroutine($fdata['timeroutine']);}
			if(isset($fdata['timeroutineparam'])) {$dto->setTimeroutineparam($fdata['timeroutineparam']);}
			if(isset($fdata['execperiodstart'])) {$dto->setExecperiodstart($fdata['execperiodstart']);}
			if(isset($fdata['execperiodend'])) {$dto->setExecperiodend($fdata['execperiodend']);}
			if(isset($fdata['timeexeconce'])) {$dto->setTimeexeconce($fdata['timeexeconce']);}
			if(isset($fdata['status'])) {$dto->setStatus($fdata['status']);}
			
			if(isset($fdata['idnumber'])) {$dto->setIdnumber($fdata['idnumber']);}
			if(isset($fdata['param'])) {$dto->setParam($fdata['param']);}
			if(isset($fdata['description'])) {$dto->setDescription($fdata['description']);}
                        
                        if(isset($fdata['doperation'])) {$dto->setDoperation($fdata['doperation']);}
                        if(isset($fdata['sortorder'])) {$dto->setSortorder($fdata['sortorder']);}
			
						$dto->setTcron('cron1');
			return $dto;
		
	}
	
	

}
