<?php

namespace Badiu\System\SchedulerBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class TaskFormat extends BadiuFormat{
     private $configformat;
     private $router;
     private  $utilapp;
     private  $cript;
     
     function __construct(Container $container) {
            parent::__construct($container);
             $this->configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
             $this->router=$this->getContainer()->get("router");
             $this->utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
             $this->cript=$this->getContainer()->get('badiu.system.core.lib.util.cript');
       } 

        public  function doperation($data){
            $value="";
          $value=$this->getUtildata()->getVaueOfArray($data,'doperation');
      
        if($value==DOMAINTABLE::$TASK_OPERATION_IMPORT){$value= $this->getTranslator()->trans('badiu.system.scheduler.operation.import');}
        if($value==DOMAINTABLE::$TASK_OPERATION_UPDATE){$value=  $this->getTranslator()->trans('badiu.system.scheduler.operation.update');}
        if($value==DOMAINTABLE::$TASK_OPERATION_DELETE){$value=  $this->getTranslator()->trans('badiu.system.scheduler.operation.delete');}
        if($value==DOMAINTABLE::$TASK_OPERATION_REPORT){$value= $this->getTranslator()->trans('badiu.system.scheduler.operation.report');}
        if($value==DOMAINTABLE::$TASK_OPERATION_SUMMARIZE){$value=  $this->getTranslator()->trans('badiu.system.scheduler.operation.other');}
        if($value==DOMAINTABLE::$TASK_OPERATION_OTHER){$value=  $this->getTranslator()->trans('badiu.system.scheduler.operation.summarize');}
      
        return $value; 
    }
     public  function routineexec($data){
         $value="";
         $simultaneousexec=$this->getUtildata()->getVaueOfArray($data,'simultaneousexec');
         $timeroutine=$this->getUtildata()->getVaueOfArray($data,'timeroutine');
         $timeroutineparam=$this->getUtildata()->getVaueOfArray($data,'timeroutineparam');
         $param=$this->getUtildata()->getVaueOfArray($data,'param');
         
       //  $value.="$simultaneousexec<br />";
         $value.="$timeroutine<br />";
         $value.="$timeroutineparam<br />";
         $value.="$param<br />";
         
          $resultinfo=$this->getUtildata()->getVaueOfArray($data,'resultinfo');
         
         $id=$this->getUtildata()->getVaueOfArray($data,'id');
         
         $dresultinfo=$this->getJson()->decode($resultinfo,true);
         $datashouldexec=$this->getUtildata()->getVaueOfArray($dresultinfo,'datashouldexec');
         $dataexec=$this->getUtildata()->getVaueOfArray($dresultinfo,'dataexec');
          if(is_array($dataexec) || is_array($datashouldexec)){return $value;}
          $value.="$dataexec de $datashouldexec <br />";
         
         $html="";
            if($this->router->getRouteCollection()->get('badiu.system.core.service.process')!==null ){
                 $html.="<a   data-toggle=\"tooltip\" data-placement=\"left\" title='".$resultinfo."' target=\"_blank\" href='".$this->router->generate('badiu.system.core.service.process',array('_service'=>'badiu.system.scheduler.task.cron','id'=>$id,'_force'=>1))."'>Executar</a>  ";
            }
             $value.="$html";
       
          $value="<font size=\"2\">$value</font>";   
         
        return $value; 
     }
  public  function lastexec($data){
  /*    echo "<pre>";
      print_r($data);
      echo "</pre>";exit;*/
         $value="";
         $timeexec=$this->getUtildata()->getVaueOfArray($data,'timeexec');
         $timeexec=$this->configformat->defaulfTimeDuration($timeexec);
        
          
         //$timeend=$this->getUtildata()->getVaueOfArray($data,'timeend');
        // $timestart=$this->getUtildata()->getVaueOfArray($data,'timestart');
         
         $timeend=$this->getUtildata()->getVaueOfArray($data,'timeend');
         $timestart=$this->getUtildata()->getVaueOfArray($data,'timestart');
         
         $timestart=$this->configformat->defaultDateTime($timestart);
         $timeend=$this->configformat->defaultDateTime($timeend);
        
         $resultinfo=$this->getUtildata()->getVaueOfArray($data,'resultinfo');
         if(empty($resultinfo)){return null;}
         $id=$this->getUtildata()->getVaueOfArray($data,'id');
         
         $dresultinfo=$this->getJson()->decode($resultinfo,true);
         $datashouldexec=$this->getUtildata()->getVaueOfArray($dresultinfo,'datashouldexec');
         $dataexec=$this->getUtildata()->getVaueOfArray($dresultinfo,'dataexec');
       
         //$value.="$timeexec<br />";
       //  $value.="$timestart<br />";
      //   $value.="$timeend<br />";
        
        
         $value.="$dataexec de $datashouldexec <br />";
         
         $html="";
            if($this->router->getRouteCollection()->get('badiu.system.core.service.process')!==null ){
                 $html.="<a   data-toggle=\"tooltip\" data-placement=\"left\" title='".$resultinfo."' target=\"_blank\" href='".$this->router->generate('badiu.system.core.service.process',array('_service'=>'badiu.system.scheduler.task.cron','id'=>$id))."'>Executar</a>  ";
            }
            $value.="$html<br />";
            $value="<font size=\"2\">$value</font>";
        return $value; 
     }
   
     
   public  function manualexec($data){
         $id=$this->getUtildata()->getVaueOfArray($data,'id');
          $html="";
            if($this->router->getRouteCollection()->get('badiu.system.scheduler.task.cron')!==null ){
                 $html.="<a  target=\"_blank\" href='".$this->router->generate('badiu.system.scheduler.task.cron',array('id'=>$id,'_force'=>1))."'>Executar</a>  ";
            }
        return $html; 
     }
     public  function showreport($data){
         $reportfilter=$this->getUtildata()->getVaueOfArray($data,'reportfilter');
         
         $reportfilter=$this->getJson()->decode($reportfilter,true);
         $router=$this->getUtildata()->getVaueOfArray($reportfilter,'config.router',true);
         
         if(empty($router)){return null;}
         $filter=$this->getUtildata()->getVaueOfArray($reportfilter,'filter');
         //if($this->getUtildata()->getVaueOfArray($data,'id')==219){print_r($router);echo "<hr>";print_r($reportfilter);exit;}
         $apputil=$this->getContainer()->get('badiu.system.core.lib.util.app');
         $apputil->setSessionhashkey($this->getSessionhashkey());
         $url=$apputil->getUrlByRoute($router,$filter);
         $label=$this->getTranslator()->trans('badiu.system.scheduler.task.showreport');
         $link="<a href=\"$url\" target=\"_blank\">$label</a>";
         return $link;
     }
   /*    public  function timeexec($lastdataexec){
            $value=$lastdataexec;
            $labelmin=$this->getTranslator()->trans('badiu.system.time.minute.short');
	    $labelhour=$this->getTranslator()->trans('badiu.system.time.hour.short');
           
                if($value > 0 && $value < 60) {$value=$value." $labelmin";}  
				else if($value >= 60) {
					$value=$value/60;
					$value=$value." $labelhour";
				}  
			  
          return $value; 
    }
    */
    
     public  function status($data){
         $status=$this->getUtildata()->getVaueOfArray($data,'status');
         $result=$status;
         if($status=='active'){$result=$this->getTranslator()->trans('badiu.system.scheduler.task.status.active');}
        else if($status=='inactive'){$result=$this->getTranslator()->trans('badiu.system.scheduler.task.status.inactive');}
        else if($status=='processing'){$result=$this->getTranslator()->trans('badiu.system.scheduler.task.status.processing');}
        else if($status=='success'){$result=$this->getTranslator()->trans('badiu.system.scheduler.task.status.success');}
        else if($status=='failed'){$result=$this->getTranslator()->trans('badiu.system.scheduler.task.status.failed');}
        else if($status=='queueanalysis'){$result=$this->getTranslator()->trans('badiu.system.scheduler.task.status.queueanalysis');}
        return $result;
     }
     
      public  function reportaction($data){
         $reportaction=$this->getUtildata()->getVaueOfArray($data,'reportaction');
         $result=$reportaction;
         if($reportaction=='send.report'){$result=$this->getTranslator()->trans('badiu.system.scheduler.report.sendreport');}
        else if($reportaction=='send.menssage.touserinreport'){$result=$this->getTranslator()->trans('badiu.system.scheduler.report.sendmenssagetouserinreport');}
       
        return $result;
     }
    public  function resultinfo($data){
         $resultinfo=$this->getUtildata()->getVaueOfArray($data,'resultinfo');
    
         $dresultinfo=$this->getJson()->decode($resultinfo,true);
          
         $datashouldexec=$this->getUtildata()->getVaueOfArray($dresultinfo,'resultinfo.datashouldexec',true);
         $dataexec=$this->getUtildata()->getVaueOfArray($dresultinfo,'resultinfo.dataexec',true);
         $reportrowsize=$this->getUtildata()->getVaueOfArray($dresultinfo,'resultinfo.reportrowsize',true);
      
         $sysuser=$this->getUtildata()->getVaueOfArray($dresultinfo,'resultinfo.sysuser',true);
         $sysusrinfo="";
         if(is_array($sysuser)){
            $countsysuser=count($sysuser);
            if($countsysuser){
               $role=$this->getUtildata()->getVaueOfArray($sysuser,'0.role',true);
               $sysusrinfo=$this->getTranslator()->trans('badiu.system.scheduler.task.sysuserinfo',array('%record%'=>$countsysuser,'%sysuser%'=>$role));
            }
            
         }
        // if($datashouldexec==null){return null;}
         $result=null;
         if(empty($reportrowsize)){$reportrowsize=$dataexec;}
         if($reportrowsize >=0){
            $result=$this->getTranslator()->trans('badiu.system.scheduler.task.resultsuccess',array('%record%'=>$reportrowsize));
            if(!empty($sysusrinfo)){$result.="<br />".$sysusrinfo;}
         }

       /*  if($datashouldexec==$dataexec){
             if($reportrowsize >=0 && $datashouldexec==1){$dataexec=$reportrowsize; }
             $result=$this->getTranslator()->trans('badiu.system.scheduler.task.resultsuccess',array('%record%'=>$dataexec));
         }else{
            $result=$this->getTranslator()->trans('badiu.system.scheduler.task.resultsuccess',array('%recordx%'=>$dataexec,'%recordy%'=>$datashouldexec)); 
         } */
        return $result;
     }
     
   public  function sendonlyreportwithrecord($data){
        $sendonlyreportwithrecord="";
         $sendonlyreportwithrecord=$this->getUtildata()->getVaueOfArray($data,'reportmessage');
         $sendonlyreportwithrecord=$this->getJson()->decode($sendonlyreportwithrecord,true);
         $sendonlyreportwithrecord=$this->getUtildata()->getVaueOfArray($sendonlyreportwithrecord,'sendonlyreportwithrecord');
         if($sendonlyreportwithrecord==null || $sendonlyreportwithrecord==''){$sendonlyreportwithrecord=1;}
         $value="";
         if($sendonlyreportwithrecord){$value=$this->getTranslator()->trans('yes');}
         else{$value=$this->getTranslator()->trans('no');}
        
      return $value; 
  }
  
  public  function messagerecipientto($data){
        $vout="";
         $value=$this->getUtildata()->getVaueOfArray($data,'reportmessage');
      
         $value=$this->getJson()->decode($value,true);
         $to=$this->getUtildata()->getVaueOfArray($value,'recipient.to',true);
         $vout=$to;
         $sysuser=$this->getUtildata()->getVaueOfArray($value,'recipient.sysuser',true);
         if(!empty($sysuser)){
            if(empty($vout)){$vout=$sysuser;}
            else {$vout.="<br />".$sysuser;}
         }
        
        return $vout; 
  }
  
   public  function messagesubject($data){
        $value="";
        $value=$this->getUtildata()->getVaueOfArray($data,'msgsubject');
        if(!empty($value)){return $value;}
         $value=$this->getUtildata()->getVaueOfArray($data,'reportmessage');
         $value=$this->getJson()->decode($value,true);
         $value=$this->getUtildata()->getVaueOfArray($value,'message.subject',true);
        return $value; 
  }
   
  public  function messagebody($data){
        $value="";
        $value=$this->getUtildata()->getVaueOfArray($data,'msgcontent');
         if(!empty($value)){return $value;}
         $value=$this->getUtildata()->getVaueOfArray($data,'reportmessage');
         $value=$this->getJson()->decode($value,true);
         $value=$this->getUtildata()->getVaueOfArray($value,'message.body',true);
        return $value; 
  }

  
  public  function smtpservice($data){
   $value="";
    $value=$this->getUtildata()->getVaueOfArray($data,'reportmessage');

    $value=$this->getJson()->decode($value,true);
    $value=$this->getUtildata()->getVaueOfArray($value,'smtpservice');
    if($value=='badiunetconfig'){$value=$this->getTranslator()->trans('badiu.system.scheduler.smtpservice.badiunetconfig.short');}
    else if($value=='moodleconfig'){$value=$this->getTranslator()->trans('badiu.system.scheduler.smtpservice.moodleconfig.short');}
    else {$value=$this->getTranslator()->trans('badiu.system.scheduler.smtpservice.badiunetconfig.short');}
   
   return $value; 
}

public  function reporttype($data){
  
   $reportfilter=$this->getUtildata()->getVaueOfArray($data,'reportfilter');
   $reportfilter=$this->getJson()->decode($reportfilter,true);
   $router=$this->getUtildata()->getVaueOfArray($reportfilter,'config.router',true);
   $value= $this->getTranslator()->trans($router);
   return $value; 
}

  public  function linkManage($data){
	 
   $this->utilapp->setSessionhashkey($this->getSessionhashkey());
   $id=$this->getUtildata()->getVaueOfArray($data,'id');
   $reportfilter=$this->getUtildata()->getVaueOfArray($data,'reportfilter');
   $reportfilter=$this->getJson()->decode($reportfilter,true);
   $router=$this->getUtildata()->getVaueOfArray($reportfilter,'config.router',true);
   $filter=$this->getUtildata()->getVaueOfArray($reportfilter,'filter');
   $urlgoback=$this->utilapp->getCurrentUrl();;
   $filter['_badiuscheduleraction']='_badiusystemreporscheduleredit';
   $filter['_urlgoback']=$urlgoback;
   $filter['_foperation']='';
   $filter['_badiuschedulerid']=$id;
   if(empty($router)){return null;}
   $badiuSession=$this->getContainer()->get('badiu.system.access.session');
   $badiuSession->setHashkey($this->getSessionhashkey());
   $clienttype=$badiuSession->get()->getType();
   $urledit="";
   
 
   if($clienttype=='webservice' || $clienttype=='webservicesynceduser' ){
      $this->cript->setKey('BADIUNET_EXTERNAL_CRITP_KKDDFFWW');
      $filter['_urlgoback']=urlencode($this->cript->encode($urlgoback)); 
      $filter['_key']=$router;
      $queryString = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
      $queryString->setParam($filter);
      $queryString->makeQuery();
      $pquery=$queryString->getQuery();
      $urledit="BADIU_CORE_SERVICE_CLIENTE_URLBASE?$pquery";

   }else{$urledit=$this->getContainer()->get('badiu.system.core.lib.util.app')->getUrlByRoute($router,$filter);}

   $iconedit=$urliconprocess=$this->utilapp->getResourseUrl('bundles/badiuthemecore/image/icons/edit.gif');
   $imgiconedit="<img src=\"$iconedit\" />";
   
   $iconcopy=$urliconprocess=$this->utilapp->getResourseUrl('bundles/badiuthemecore/image/icons/copy.gif');
   $imgiconcopy="<img src=\"$iconcopy\" />";
   
   $icondelete=$urliconprocess=$this->utilapp->getResourseUrl('bundles/badiuthemecore/image/icons/delete.gif');
   $imgicondelete="<img src=\"$icondelete\" />";
   
   $iconremove=$urliconprocess=$this->utilapp->getResourseUrl('bundles/badiuthemecore/image/icons/remove.gif');
   $imgiconremove="<img src=\"$iconremove\" />";

   $out="<a href=\"$urledit\">$imgiconedit</a>";
   $out.="&nbsp;&nbsp;<a href=\"$urledit&_foperation=clone\">$imgiconcopy</a>";
 //  $out.="&nbsp; <a @click=\"showModalToDelete(itemdata,'delete')\"> $imgicondelete</a>"; 
//   $out.="&nbsp; <a  @click=\"showModalToDelete(itemdata,'remove')\">$imgiconremove</a>";

  return $out; 
} 

public  function linkview($data){
		$url=null;
         $name=$this->getUtildata()->getVaueOfArray($data,'name');
		 $id=$this->getUtildata()->getVaueOfArray($data,'id');
		 $reportfilter=$this->getUtildata()->getVaueOfArray($data,'reportfilter');
		 $reportfilter=$this->getJson()->decode($reportfilter,true);
		 $router=$this->getUtildata()->getVaueOfArray($reportfilter,'config.router',true);
		 if($router=='badiu.system.core.report.dynamic.index' || 
			$router=='badiu.system.core.report.dynamicp.index' || 
			$router=='badiu.system.core.report.dynamicd.dashboard' || 
			$router=='badiu.system.core.report.dynamicdp.dashboard'){
				$router=$this->getUtildata()->getVaueOfArray($reportfilter,'filter._dkey',true);
		}
		if(empty($router)){return $name;}
		 $pos=stripos($router, "badiu.moodle.mreport.");
		 if($pos=== false){
			 $fparam=array('id'=>$id,'parentid'=>$id);
			 $url=$this->getUtilapp()->getUrlByRoute('badiu.system.scheduler.taskreport.view',$fparam);
		 }else{
			 $fparam=array('id'=>$id);
			 $url=$this->getUtilapp()->getUrlByRoute('badiu.moodle.mreport.schedulertask.view',$fparam);
		 }
		 if(empty($url)){return $name;}
		 $link="<a href=\"$url\">$name</a>";
         return $link;
         
     }
	 
}
