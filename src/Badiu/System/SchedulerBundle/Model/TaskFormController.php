<?php

namespace Badiu\System\SchedulerBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class TaskFormController extends BadiuFormController
{  
	private $replication=array();
    function __construct(Container $container) {
            parent::__construct($container);
              }
     
  public function changeParamOnOpen() {
		   $param = $this->getParam();
		   if(!$this->isEdit()){
			  if($this->getContainer()->hasParameter('badiu.system.core.type.instalation')){
				$typeinstalation=$this->getContainer()->getParameter('badiu.system.core.type.instalation');
				if($typeinstalation=='multipleentity'){$param['lcron']="global";}
				else{$param['lcron']="entity";}
			 }else{
				 $param['lcron']="entity";
			 }
			   $this->setParam($param); 
		   }
		   
		
	   }
      
     
    
     public function execAfter(){
		  $enrolid=$this->getParamItem('id');
		  $classeid=$this->getParamItem('classeid');
		  $userid=$this->getParamItem('userid');
		  $roleid=$this->getParamItem('roleid');
		  $statusid=$this->getParamItem('statusid');
		  $enrolreplicate=$this->getParamItem('enrolreplicate');
		  $enrolreplicateupadate=$this->getParamItem('enrolreplicateupadate');
		  $timestart=$this->getParamItem('timestart');
		  $timeend=$this->getParamItem('timeend');
		 
		  $isedit=$this->isEdit();
		  $fparam=array('id'=>$enrolid,'classeid'=>$classeid,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid,'isedit'=>$isedit,'enrolreplicate'=>$enrolreplicate,'enrolreplicateupadate'=>$enrolreplicateupadate,'timestart'=>$timestart,'timeend'=>$timeend);
		 
		  $this->getContainer()->get('badiu.ams.enrol.classe.lib')->replicate($fparam);
		  parent::execAfter();
	 }
    
}
