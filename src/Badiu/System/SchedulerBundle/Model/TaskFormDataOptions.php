<?php

namespace Badiu\System\SchedulerBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;

class TaskFormDataOptions extends BadiuFormDataOptions {

    function __construct(Container $container, $baseKey = null) {
        parent::__construct($container, $baseKey);
    }

    public function getDtype() {
        $list = array();
        $list[DOMAINTABLE::$TASK_SERVICE] = $this->getTranslator()->trans('badiu.system.scheduler.task.dtype.service');
        $list[DOMAINTABLE::$TAKS_URL_RELATIVE] = $this->getTranslator()->trans('badiu.system.scheduler.task.dtype.urlrelative');
        $list[DOMAINTABLE::$TAKS_URL_FULL] = $this->getTranslator()->trans('badiu.system.scheduler.task.dtype.urlfull');
        return $list;
    }

     public function getStatus() {
        $list = array();
        $list[DOMAINTABLE::$TASK_STATUS_ACTIVE] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.active');
        $list[DOMAINTABLE::$TASK_STATUS_INACTIVE] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.inactive');
        return $list;
    }
    public function getStatusFull() {
        $list = array();
        $list[DOMAINTABLE::$TASK_STATUS_ACTIVE] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.active');
        $list[DOMAINTABLE::$TASK_STATUS_INACTIVE] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.inactive');
        $list[DOMAINTABLE::$TASK_STATUS_QUEUEANALYSIS] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.queueanalysis');
        $list[DOMAINTABLE::$TASK_STATUS_PROCESSING] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.processing');
        $list[DOMAINTABLE::$TASK_STATUS_FAILED] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.failed');
        return $list;
    }
    public function getStatusFullLog() {
        $list = array();
        $list[DOMAINTABLE::$TASK_STATUS_ACTIVE] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.active');
        $list[DOMAINTABLE::$TASK_STATUS_INACTIVE] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.inactive');
        $list[DOMAINTABLE::$TASK_STATUS_QUEUEANALYSIS] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.queueanalysis');
        $list[DOMAINTABLE::$TASK_STATUS_PROCESSING] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.processing');
        $list[DOMAINTABLE::$TASK_STATUS_SUCCESS] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.success');
        $list[DOMAINTABLE::$TASK_STATUS_FAILED] = $this->getTranslator()->trans('badiu.system.scheduler.task.status.failed');
        return $list;
    }
     public function getOperation() {
       $list = array();
        $list[DOMAINTABLE::$TASK_OPERATION_IMPORT] = $this->getTranslator()->trans('badiu.system.scheduler.operation.import');
        $list[DOMAINTABLE::$TASK_OPERATION_UPDATE] = $this->getTranslator()->trans('badiu.system.scheduler.operation.update');
        $list[DOMAINTABLE::$TASK_OPERATION_DELETE] = $this->getTranslator()->trans('badiu.system.scheduler.operation.delete');
        $list[DOMAINTABLE::$TASK_OPERATION_REPORT] = $this->getTranslator()->trans('badiu.system.scheduler.operation.report');
        $list[DOMAINTABLE::$TASK_OPERATION_SUMMARIZE] = $this->getTranslator()->trans('badiu.system.scheduler.operation.other');
        $list[DOMAINTABLE::$TASK_OPERATION_OTHER] = $this->getTranslator()->trans('badiu.system.scheduler.operation.summarize');
       return $list;
    }
     public function getOperationSendReport() {
       $list = array();
        $list[DOMAINTABLE::$ROLE_ACTION_SEND_MESSAGE_TOUSERINREPORT] = $this->getTranslator()->trans('badiu.system.scheduler.action.send.menssage.touserinreport');
        $list[DOMAINTABLE::$ROLE_ACTION_SEND_REPORT] = $this->getTranslator()->trans('badiu.system.scheduler.action.send.report');

        $badiuSession= $this->getContainer()->get('badiu.system.access.session');
               
       $enabledatabaseexport =  $badiuSession->get()->getConfig()->getValue('badiu.system.core.param.config.scheduler.enabledatabaseexport');
       if($enabledatabaseexport)  {$list[DOMAINTABLE::$ROLE_ACTION_EXPORT_TODATABASE] = $this->getTranslator()->trans('badiu.system.scheduler.action.export.todatabase');}
        return $list;
    } 

    public function getExporttodatabasetype() {
        $list = array();
         $list['incremental'] = $this->getTranslator()->trans('badiu.system.scheduler.action.export.todatabase.incremental');
         $list['legacy'] = $this->getTranslator()->trans('badiu.system.scheduler.action.export.todatabase.legacy');
        return $list;
     }

     public function getExporttodatabasetypeLabel($type) {
        $label="";
         if($type=='incremental'){$label= $this->getTranslator()->trans('badiu.system.scheduler.action.export.todatabase.incremental');}
        else  if($type=='legacy'){$label= $this->getTranslator()->trans('badiu.system.scheduler.action.export.todatabase.legacy');}
        return $label;
     }
	 
	   public function getCalendar() {
        $list = array();
         $list['default'] = $this->getTranslator()->trans('badiu.system.scheduler.calendar.default');
        
        return $list;
     }
	 
	

	  public function getLcron() {
        $list = array();
         $list['entity'] = $this->getLcronLabel('entity');
         $list['global'] = $this->getLcronLabel('global');
		 
		  if(!$this->getContainer()->hasParameter('badiu.system.core.type.instalation')){unset($list['global']);}
		  else{
			  $typeinstalation=$this->getContainer()->getParameter('badiu.system.core.type.instalation');
				if($typeinstalation!='multipleentity'){unset($list['global']);}
		}
        return $list;
     }

     public function getLcronLabel($type) {
        $label="";
        $label= $this->getTranslator()->trans('badiu.system.scheduler.task.lcron.'.$type);
        return $label;
     }
	 
	  public function getTcron() {
        $list = array();
         $list['cron1'] = $this->getTcronLabel('cron1');
         $list['cron2'] = $this->getTcronLabel('cron2');
		 $list['cron3'] = $this->getTcronLabel('cron3');
		 $list['cron4'] = $this->getTcronLabel('cron4');
		 $list['cron5'] = $this->getTcronLabel('cron5');
		 $list['cron6'] = $this->getTcronLabel('cron6');
		 $list['cron7'] = $this->getTcronLabel('cron7');
		 $list['cron8'] = $this->getTcronLabel('cron8');
		 $list['cron9'] = $this->getTcronLabel('cron9');
		 $list['cron10'] = $this->getTcronLabel('cron10');
        return $list;
     }
	 
	  public function getTcronLabel($type) {
        $label="";
        $label= $this->getTranslator()->trans('badiu.system.scheduler.task.tcron.'.$type);
        return $label; 
     }
	 
	     public function getLmodel() {
        $list = array();
         $list[100] = $this->getTranslator()->trans('badiu.system.scheduler.task.lmodel.none');
         $list[200] = $this->getTranslator()->trans('badiu.system.scheduler.task.lmodel.template');
        return $list;
     }

     public function getLmodelLabel($type) {
        $label="";
         if($type==100){$label= $this->getTranslator()->trans('badiu.system.scheduler.task.lmodel.none');}
        else  if($type==200){$label= $this->getTranslator()->trans('badiu.system.scheduler.task.lmodel.template');}
        return $label;
     }
	 
	 public function getChanelDelivery() {
        $list = array();
         $list['email'] = $this->getChanelDeliveryLabel('email');
         //$list['moodlemessage'] = $this->getChanelDeliveryLabel('moodlemessage');
		 //$list['webservice'] = $this->getChanelDeliveryLabel('webservice');
		 
		//check version plugin for moodlemessage
		
		
		$serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
		if(!empty($serviceid) && $this->getContainer()->has('badiu.admin.server.service.data')){
			$servicedata = $this->getContainer()->get('badiu.admin.server.service.data');
			$badiunetmoodlepluginversion=$servicedata->getGlobalColumnValue('serviceversionumber',array('id'=>$serviceid));
			if($badiunetmoodlepluginversion >=2022102700){$list['moodlemessage'] = $this->getChanelDeliveryLabel('moodlemessage');}
			if($badiunetmoodlepluginversion >=2022120100){$list['moodlenotification'] = $this->getChanelDeliveryLabel('moodlenotification');}
		}
		
		//client webservice recipient
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $entity = $badiuSession->get()->getEntity();
		
		$clientwsdata = $this->getContainer()->get('badiu.system.module.clientwsschedulerdelivery.data');
		$listws=$clientwsdata->getGlobalColumnsValues('o.id,o.name',array('entity'=>$entity,'dtype'=>'clientwsschedulerdelivery','modulekey'=>'badiu.system.module.clientwebserverschedulerdelivery','bundlekey'=>'badiu.system.module'));
		if(is_array($listws)){
			foreach ($listws as $item) {
				$id=$this->getUtildata()->getVaueOfArray($item, 'id');
				$name=$this->getUtildata()->getVaueOfArray($item, 'name');
				$list[$id]=$name; 
			}
		}
		
        return $list;
     }
	  
	 public function getChanelDeliveryLabel($type) {
        $label="";
        $label= $this->getTranslator()->trans('badiu.system.scheduler.chaneldelivery.'.$type);
        return $label; 
     }
}
