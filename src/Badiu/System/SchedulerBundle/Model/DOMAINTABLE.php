<?php

namespace Badiu\System\SchedulerBundle\Model;

class DOMAINTABLE {

    //task  type
    public static $TASK_SERVICE = "service";
    public static $TAKS_URL_FULL = "urlfull";
    public static $TAKS_URL_RELATIVE = "urlrelative";
    //task  type
    public static $TASK_STATUS_ACTIVE = "active";
    public static $TASK_STATUS_INACTIVE = "inactive";
    public static $TASK_STATUS_PROCESSING = "processing";
    public static $TASK_STATUS_FAILED = "failed";
    public static $TASK_STATUS_SUCCESS = "success";
    public static $TASK_STATUS_QUEUEANALYSIS = "queueanalysis";
    
    //task  operation
    public static $TASK_OPERATION_IMPORT = "import";
    public static $TASK_OPERATION_UPDATE = "update";
    public static $TASK_OPERATION_DELETE = "delete";
    public static $TASK_OPERATION_REPORT = "report";
    public static $TASK_OPERATION_SUMMARIZE = "summarize";
    public static $TASK_OPERATION_OTHER = "other";
    
    //role   action
    public static $ROLE_ACTION_SEND_REPORT = "send.report";
    public static $ROLE_ACTION_SEND_MESSAGE_TOUSERINREPORT = "send.menssage.touserinreport";
    public static $ROLE_ACTION_EXPORT_TODATABASE = "export.todatabase";
    public static $ROLE_ACTION_SEND_MESSAGE_GENERAL = "send.menssage.general";

}

?>