<?php

namespace Badiu\System\SchedulerBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class TaskLogMessageData extends BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
        parent::__construct($container,$bundleEntity);
    }

}
