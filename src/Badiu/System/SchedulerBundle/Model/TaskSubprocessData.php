<?php

namespace Badiu\System\SchedulerBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class TaskSubprocessData extends BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
        parent::__construct($container,$bundleEntity);
    }

    public function getIdByTaskPaging($taskid,$pagingindex) {
                
                $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE  o.taskid = :taskid AND o.pagingindex=:pagingindex";        
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('taskid',$taskid);
                $query->setParameter('pagingindex',$pagingindex);
                $result= $query->getOneOrNullResult();
                if(isset($result['id'])){$result=$result['id'];}
             
		return $result;
    }
    
    public function getListToExec($taskid,$status='programmed') {
          $sql="SELECT  o.id,o.pagingindex,o.datashouldexec FROM ".$this->getBundleEntity()." o  WHERE  o.taskid = :taskid AND o.status=:status ORDER BY o.pagingindex";        
          $query = $this->getEm()->createQuery($sql);
          $query->setParameter('taskid',$taskid);
          $query->setParameter('status',$status);
          $query->setMaxResults(300);
          $result= $query->getResult();
          return $result;
     }
    public function count($taskid,$status=null) {
         $wsql=" ";
         if(!empty($status)){$wsql=" AND o.status=:status ";}
          $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE  o.taskid = :taskid $wsql ORDER BY o.pagingindex";        
          $query = $this->getEm()->createQuery($sql);
          $query->setParameter('taskid',$taskid);
          if(!empty($status)){ $query->setParameter('status',$status);}
          $result= $query->getOneOrNullResult();
          if(isset($result['countrecord'])){$result=$result['countrecord'];}
          return $result;
     }

     
}
