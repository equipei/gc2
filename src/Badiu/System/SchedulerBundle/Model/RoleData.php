<?php

namespace Badiu\System\SchedulerBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class RoleData extends BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
        parent::__construct($container,$bundleEntity);
    }

    public function getByReporttype($entity,$reporttype) {
		$sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.reporttype=:reporttype ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('reporttype',$reporttype);
        $result= $query->getResult();
		return $result;
    }

    
     
}
