<?php

namespace Badiu\System\SchedulerBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class TaskLog extends BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
        parent::__construct($container,$bundleEntity);
    }

     public function start($task,$entity) {  
         $dto=$this->getContainer()->get('badiu.system.scheduler.tasklog.entity');
         $dto->setTaskid($task);
         $dto->setEntity($entity);
         $dto->setStatus('processing');
         $dto->setTimestart(new \Datetime());
         $this->setDto($dto); 
         $this->save();
         return $this->getDto();
    }
    
     public function end($dto) {  
         $dto->setStatus('success');
         $dto->setTimeend(new \Datetime());
         $timeexec=$dto->getTimestart()->getTimestamp()-$dto->getTimeend()->getTimestamp();
         $dto->setTimeexec($timeexec);
         $this->setDto($dto); 
         $this->save();
         return $this->getDto();
    }
}
