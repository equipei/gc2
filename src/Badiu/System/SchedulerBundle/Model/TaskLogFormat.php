<?php

namespace Badiu\System\SchedulerBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class TaskLogFormat extends BadiuFormat{
     private $configformat;
  
     function __construct(Container $container) {
            parent::__construct($container);
             $this->configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
    
       } 
      public  function typeexec($data){
         $dtype=$this->getUtildata()->getVaueOfArray($data,'dtype');
         $result=$dtype;
         if($dtype=='automatic'){$result=$this->getTranslator()->trans('badiu.system.scheduler.tasklog.automatic');}
        else if($dtype=='manual'){$result=$this->getTranslator()->trans('badiu.system.scheduler.tasklog.manual');}
        return $result;
     }
}
