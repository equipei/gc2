<?php

namespace Badiu\System\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuSystemUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
