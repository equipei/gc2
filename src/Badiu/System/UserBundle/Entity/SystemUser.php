<?php

namespace Badiu\System\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemUser
 *
 * @ORM\Table(name="system_user", uniqueConstraints={
 *
 *      @ORM\UniqueConstraint(name="system_user_username_uix", columns={"username","entity"}),
 *      @ORM\UniqueConstraint(name="system_user_idnumber_uix", columns={"entity", "idnumber"}),
 *      @ORM\UniqueConstraint(name="system_user_ukey_uix", columns={"entity", "ukey"}),
 *      @ORM\UniqueConstraint(name="system_user_shortname_uix", columns={"entity", "shortname"}),
 *     @ORM\UniqueConstraint(name="system_user_idnumber_uix", columns={"entity", "moduleinstanceextid","modulekey","moduleinstance"}),
 *     @ORM\UniqueConstraint(name="system_user_doc_uix", columns={"entity", "doctype", "docnumber"})},
 *       indexes={@ORM\Index(name="system_user_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="system_user_idnumber_ix", columns={"idnumber"}),
 *              @ORM\Index(name="system_user_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="system_user_deleted_ix", columns={"deleted"}),
 *               @ORM\Index(name="system_user_confirmed_ix", columns={"confirmed"}),
 *              @ORM\Index(name="system_user_firstname_ix", columns={"firstname"}),
 *              @ORM\Index(name="system_user_lastname_ix", columns={"lastname"}),
 *              @ORM\Index(name="system_user_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="system_user_sourcemodule_ix", columns={"sourcemodule"}),
 *              @ORM\Index(name="system_user_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="system_user_adminformattemptid_ix", columns={"adminformattemptid"}),
 *              @ORM\Index(name="system_user_moduleinstanceextid_ix", columns={"moduleinstanceextid"}), 
 *              @ORM\Index(name="system_user_city_ix", columns={"city"}),
 *              @ORM\Index(name="system_user_status_ix", columns={"statusid"}),
 *              @ORM\Index(name="system_user_firstaccess_ix", columns={"firstaccess"}),
 *              @ORM\Index(name="system_user_lastaccess_ix", columns={"lastaccess"}),
 *              @ORM\Index(name="system_user_email_ix", columns={"email"}),
*              @ORM\Index(name="system_user_marker_ix", columns={"marker"}),
 *              @ORM\Index(name="system_user_statusregister_ix", columns={"statusregister"}),
  *              @ORM\Index(name="system_user_registerupadtetime_ix", columns={"registerupadtetime"}), 
*              @ORM\Index(name="system_user_registernextupadtetime_ix", columns={"registernextupadtetime"}), 
 *              @ORM\Index(name="system_user_lastaccess_ix", columns={"lastaccess"})})
 *
 * @ORM\Entity
 */

class SystemUser {
	     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="firstname", type="string", length=255, nullable=false)
	 */
	private $firstname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lastname", type="string", length=255, nullable=false)
	 */
	private $lastname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="alternatename", type="string", length=255, nullable=true)
	 */
	private $alternatename;

	/**
     * @var integer
     *
     * @ORM\Column(name="enablealternatename", type="integer", nullable=true)
     */
    private $enablealternatename;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255, nullable=true)
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="auth", type="string", length=100, nullable=true)
	 */
	private $auth = 'manual';

	/**
	 * @var string
	 *
	 * @ORM\Column(name="username", type="string", length=100, nullable=false)
	 */
	private $username;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="password", type="string", length=255, nullable=true)
	 */
	private $password;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;

        /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="doctype", type="string", length=255, nullable=true)
	 */

	private $doctype;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="docnumber", type="string", length=255, nullable=true)
	 */
	private $docnumber;
        
         /**
	 * @var string
	 *
	 * @ORM\Column(name="ukey", type="string", length=255, nullable=true)
	 */
    private $ukey;
    
         /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
      /**
     * @var string
     *
     * @ORM\Column(name="marker", type="string", length=255, nullable=true)
     */
    private $marker;
        
           /**
	 * @var string
	 *
	 * @ORM\Column(name="nationalitystatus", type="string", length=255, nullable=true)
	 */
	private $nationalitystatus; //native | resident | foreign 

    /**
	 * @var string
	 *
	 * @ORM\Column(name="statusregister", type="string", length=255, nullable=true)
	 */
    private $statusregister; //complete | incomplete 
    
    /**
	 * @var string
	 *
	 * @ORM\Column(name="statusregisterinfo", type="text", nullable=true)
	 */
	private $statusregisterinfo; 
	
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="registerupadtetime", type="datetime", nullable=true)
	 */
	private $registerupadtetime;
	
	/**
	 * @var \DateTime
	 * 
	 * @ORM\Column(name="registernextupadtetime", type="datetime", nullable=true)
	 */
	private $registernextupadtetime;
	
	
            /**
	 * @var string
	 *
	 * @ORM\Column(name="sex", type="string", length=255, nullable=true)
	 */
	private $sex; //male | female | other
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="sexother", type="string", length=255, nullable=true)
	 */
	private $sexother;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="sourcemodule", type="string", length=255, nullable=true)
	 */
	private $sourcemodule;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="adminformattemptid", type="bigint", nullable=true)
	 */
	private $adminformattemptid;
	

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstanceextid", type="bigint", nullable=true)
	 */
	private $moduleinstanceextid;

	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="synclog", type="text", nullable=true)
	 */
	private $synclog;

        
     /**
	 * @var string
	 *
	 * @ORM\Column(name="contactdata", type="text", nullable=true)
	 */
    private $contactdata;
    
    
     
    /**
    * @var string
    *
    * @ORM\Column(name="city", type="string", length=255, nullable=true)
    */
    private $city;


    
    /**
     * @var SystemUserStatus
     *
     * @ORM\ManyToOne(targetEntity="SystemUserStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
     private $statusid;

/**
	 * @var string
	 *
	 * @ORM\Column(name="postcode", type="string", length=255, nullable=true)
	 */
	private $postcode;
	

/**
	 * @var string
	 *
	 * @ORM\Column(name="addressnumber", type="string", length=255, nullable=true)
	 */
	private $addressnumber;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="address", type="string", length=255, nullable=true)
	 */
	private $address;
	
		/**
	 * @var string
	 *
	 * @ORM\Column(name="addresscomplement", type="string", length=255, nullable=true)
	 */
	private $addresscomplement;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="neighborhood", type="string", length=255, nullable=true)
	 */
	private $neighborhood;
	
	
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="state", type="string", length=255, nullable=true)
	 */
	private $state;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="country", type="string", length=255, nullable=true)
	 */
	private $country;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="phone", type="string", length=255, nullable=true)
	 */
	private $phone;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="phonemobile", type="string", length=255, nullable=true)
	 */
	private $phonemobile;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="profession", type="string", length=255, nullable=true)
	 */
	private $profession;
	
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="schoollevel", type="string", length=255, nullable=true)
	 */
	private $schoollevel;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="highereducationarea", type="string", length=255, nullable=true)
	 */
	private $highereducationarea;
	
         /**
	 * @var string
	 *
	 * @ORM\Column(name="documentdata", type="text", nullable=true)
	 */
    private $documentdata;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="securitypolicyaccept", type="integer", nullable=true)
     */
    private $securitypolicyaccept;
    
	/**
	 * @var string
	 *
	 * @ORM\Column(name="lang", type="string", length=30, nullable=true)
	 */
        
	private $lang;

        /**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dateofbirth", type="datetime", nullable=true)
	 */
	private $dateofbirth;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="theme", type="string", length=100, nullable=true)
	 */
	private $theme;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="firstaccess", type="datetime", nullable=true)
	 */
	private $firstaccess;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="lastaccess", type="datetime", nullable=true)
	 */
	private $lastaccess;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lastip", type="string", length=45, nullable=true)
	 */
	private $lastip;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="confirmed", type="integer", nullable=true)
	 */
	private $confirmed = 1;

      
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customint1", type="bigint",  nullable=true)
	 */
	private $customint1;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customint2", type="bigint",  nullable=true)
	 */
	private $customint2;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customint3", type="bigint",  nullable=true)
	 */
	private $customint3;
	/**
	 * @var float
	 *
	 * @ORM\Column(name="customdec1", type="float",  precision=10, scale=0, nullable=true)
	 */
	private $customdec1;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="customdec2", type="float",  precision=10, scale=0, nullable=true)
	 */
	private $customdec2;
	/**
	 * @var float
	 *
	 * @ORM\Column(name="customdec3", type="float",  precision=10, scale=0, nullable=true)
	 */
	private $customdec3;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="customchar1", type="string", length=255, nullable=true)
	 */
	private $customchar1;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customchar2", type="string", length=255, nullable=true)
	 */
	private $customchar2;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customchar3", type="string", length=255, nullable=true)
	 */
	private $customchar3;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customtext1", type="text", nullable=true)
	 */
	private $customtext1;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customtext2", type="text", nullable=true)
	 */
	private $customtext2;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customtext3", type="text", nullable=true)
	 */
	private $customtext3;

/**
 * @var string
 *
 * @ORM\Column(name="param", type="text", nullable=true)
 */
	private $param;
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted;

	
        function getId() {
            return $this->id;
        }

        function getEntity() {
            return $this->entity;
        }

        function getFirstname() {
            return $this->firstname;
        }

        function getLastname() {
            return $this->lastname;
        }

        function getEmail() {
            return $this->email;
        }

        function getAuth() {
            return $this->auth;
        }

        function getUsername() {
            return $this->username;
        }

        function getPassword() {
            return $this->password;
        }

        function getIdnumber() {
            return $this->idnumber;
        }

        function getDoctype() {
            return $this->doctype;
        }

        function getDocnumber() {
            return $this->docnumber;
        }

        function getUkey() {
            return $this->ukey;
        }

        function getNationalitystatus() {
            return $this->nationalitystatus;
        }

        function getSex() {
            return $this->sex;
        }

        function getSourcemodule() {
            return $this->sourcemodule;
        }

        function getModulekey() {
            return $this->modulekey;
        }

        function getModuleinstance() {
            return $this->moduleinstance;
        }

        function getModuleinstanceextid() {
            return $this->moduleinstanceextid;
        }

        function getSynclog() {
            return $this->synclog;
        }

       

        function getCity() {
            return $this->city;
        }

        function getState() {
            return $this->state;
        }

        function getCountry() {
            return $this->country;
        }

        function getLang() {
            return $this->lang;
        }

        function getTheme() {
            return $this->theme;
        }

        function getFirstaccess(){
            return $this->firstaccess;
        }

        function getLastaccess(){
            return $this->lastaccess;
        }

        function getLastip() {
            return $this->lastip;
        }

        function getDescription() {
            return $this->description;
        }

        function getConfirmed() {
            return $this->confirmed;
        }

        function getCustomint1() {
            return $this->customint1;
        }

        function getCustomint2() {
            return $this->customint2;
        }

        function getCustomint3() {
            return $this->customint3;
        }

        function getCustomdec1() {
            return $this->customdec1;
        }

        function getCustomdec2() {
            return $this->customdec2;
        }

        function getCustomdec3() {
            return $this->customdec3;
        }

        function getCustomchar1() {
            return $this->customchar1;
        }

        function getCustomchar2() {
            return $this->customchar2;
        }

        function getCustomchar3() {
            return $this->customchar3;
        }

        function getCustomtext1() {
            return $this->customtext1;
        }

        function getCustomtext2() {
            return $this->customtext2;
        }

        function getCustomtext3() {
            return $this->customtext3;
        }

        function getParam() {
            return $this->param;
        }

        function getTimecreated(){
            return $this->timecreated;
        }

        function getTimemodified(){
            return $this->timemodified;
        }

        function getUseridadd() {
            return $this->useridadd;
        }

        function getUseridedit() {
            return $this->useridedit;
        }

        function getDeleted() {
            return $this->deleted;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setEntity($entity) {
            $this->entity = $entity;
        }

        function setFirstname($firstname) {
            $this->firstname = $firstname;
        }

        function setLastname($lastname) {
            $this->lastname = $lastname;
        }

        function setEmail($email) {
            $this->email = $email;
        }

        function setAuth($auth) {
            $this->auth = $auth;
        }

        function setUsername($username) {
            $this->username = $username;
        }

        function setPassword($password) {
            $this->password = $password;
        }

        function setIdnumber($idnumber) {
            $this->idnumber = $idnumber;
        }

        function setDoctype($doctype) {
            $this->doctype = $doctype;
        }

        function setDocnumber($docnumber) {
            $this->docnumber = $docnumber;
        }

        function setUkey($ukey) {
            $this->ukey = $ukey;
        }

        function setNationalitystatus($nationalitystatus) {
            $this->nationalitystatus = $nationalitystatus;
        }

        function setSex($sex) {
            $this->sex = $sex;
        }

		function getSexother() {
            return $this->sexother;
        }
		function setSexother($sexother) {
            $this->sexother = $sexother;
        }
        function setSourcemodule($sourcemodule) {
            $this->sourcemodule = $sourcemodule;
        }

        function setModulekey($modulekey) {
            $this->modulekey = $modulekey;
        }

        function setModuleinstance($moduleinstance) {
            $this->moduleinstance = $moduleinstance;
        }

        function setModuleinstanceextid($moduleinstanceextid) {
            $this->moduleinstanceextid = $moduleinstanceextid;
        }

        function setSynclog($synclog) {
            $this->synclog = $synclog;
        }

      

        function setCity($city) {
            $this->city = $city;
        }

        function setState($state) {
            $this->state = $state;
        }

        function setCountry($country) {
            $this->country = $country;
        }

        function setLang($lang) {
            $this->lang = $lang;
        }

        function setTheme($theme) {
            $this->theme = $theme;
        }

        function setFirstaccess(\DateTime $firstaccess) {
            $this->firstaccess = $firstaccess;
        }

        function setLastaccess(\DateTime $lastaccess) {
            $this->lastaccess = $lastaccess;
        }

        function setLastip($lastip) {
            $this->lastip = $lastip;
        }

        function setDescription($description) {
            $this->description = $description;
        }

        function setConfirmed($confirmed) {
            $this->confirmed = $confirmed;
        }

        function setCustomint1($customint1) {
            $this->customint1 = $customint1;
        }

        function setCustomint2($customint2) {
            $this->customint2 = $customint2;
        }

        function setCustomint3($customint3) {
            $this->customint3 = $customint3;
        }

        function setCustomdec1($customdec1) {
            $this->customdec1 = $customdec1;
        }

        function setCustomdec2($customdec2) {
            $this->customdec2 = $customdec2;
        }

        function setCustomdec3($customdec3) {
            $this->customdec3 = $customdec3;
        }

        function setCustomchar1($customchar1) {
            $this->customchar1 = $customchar1;
        }

        function setCustomchar2($customchar2) {
            $this->customchar2 = $customchar2;
        }

        function setCustomchar3($customchar3) {
            $this->customchar3 = $customchar3;
        }

        function setCustomtext1($customtext1) {
            $this->customtext1 = $customtext1;
        }

        function setCustomtext2($customtext2) {
            $this->customtext2 = $customtext2;
        }

        function setCustomtext3($customtext3) {
            $this->customtext3 = $customtext3;
        }

        function setParam($param) {
            $this->param = $param;
        }

        function setTimecreated(\DateTime $timecreated) {
            $this->timecreated = $timecreated;
        }

        function setTimemodified(\DateTime $timemodified) {
            $this->timemodified = $timemodified;
        }

        function setUseridadd($useridadd) {
            $this->useridadd = $useridadd;
        }

        function setUseridedit($useridedit) {
            $this->useridedit = $useridedit;
        }

        function setDeleted($deleted) {
            $this->deleted = $deleted;
        }

       




        function getDateofbirth(){
            return $this->dateofbirth;
        }

        function setDateofbirth(\DateTime $dateofbirth) {
            $this->dateofbirth = $dateofbirth;
        }

        function getContactdata() {
            return $this->contactdata;
        }

        function getDocumentdata() {
            return $this->documentdata;
        }

        function setContactdata($contactdata) {
            $this->contactdata = $contactdata;
        }

        function setDocumentdata($documentdata) {
            $this->documentdata = $documentdata;
        }

        function setStatusid(SystemUserStatus $statusid) {
            $this->statusid = $statusid;
        }



        function getDconfig() {
            return $this->dconfig;
        }
    
        function setDconfig($dconfig) {
            $this->dconfig = $dconfig;
        }
    
        function getMarker() {
            return $this->marker;
        }
        function setMarker($marker) {
            $this->marker = $marker;
        }
    
        public function getShortname() {
            return $this->shortname;
        }
        public function setShortname($shortname) {
            $this->shortname = $shortname;
        }    
    
        function getStatusregister() {
            return $this->statusregister;
        }
        function setStatusregister($statusregister) {
            $this->statusregister = $statusregister;
        }

        function getStatusregisterinfo() {
            return $this->statusregisterinfo;
        }
        function setStatusregisterinfo($statusregisterinfo) {
            $this->statusregisterinfo = $statusregisterinfo;
        }
		
		function getAdminformattemptid() {
            return $this->adminformattemptid;
        }
        function setAdminformattemptid($adminformattemptid) {
            $this->adminformattemptid = $adminformattemptid;
        }
		
		public function  setEnablealternatename($enablealternatename) {
        $this->enablealternatename=$enablealternatename;
    }
	
	 public function getEnablealternatename() {
        return $this->enablealternatename;
    }
	
	public function  setAlternatename($alternatename) {
        $this->alternatename=$alternatename;
    }
	
	 public function getAlternatename() {
        return $this->alternatename;
    }
	
	public function  setPostcode($postcode) {
        $this->postcode=$postcode;
    }
	
	 public function getPostcode() {
        return $this->postcode;
    }
	
	public function  setAddressnumber($addressnumber) {
        $this->addressnumber=$addressnumber;
    }
	
	 public function getAddressnumber() {
        return $this->addressnumber;
    }
	
	public function  setAddress($address) {
        $this->address=$address;
    }
	
	 public function getAddress() {
        return $this->address;
    }
	
	public function  setAddresscomplement($addresscomplement) {
        $this->addresscomplement=$addresscomplement;
    }
	
	 public function getAddresscomplement() {
        return $this->addresscomplement;
    }
	
	public function  setNeighborhood($neighborhood) {
        $this->neighborhood=$neighborhood;
    }
	
	 public function getNeighborhood() {
        return $this->neighborhood;
    }
	
	public function  setPhonemobile($phonemobile) {
        $this->phonemobile=$phonemobile;
    }
	
	 public function getPhonemobile() {
        return $this->phonemobile;
    }
	
	public function  setProfession($profession) {
        $this->profession=$profession;
    }
	
	 public function getProfession() {
        return $this->profession;
    }
	
	public function  setSchoollevel($schoollevel) {
        $this->schoollevel=$schoollevel;
    }
	
	 public function getSchoollevel() {
        return $this->schoollevel;
    }
	
	
	public function  setHighereducationarea($highereducationarea) {
        $this->highereducationarea=$highereducationarea;
    }
	
	 public function getHighereducationarea() {
        return $this->highereducationarea;
    }
	
	public function  setRegisterupadtetime($registerupadtetime) {
        $this->registerupadtetime=$registerupadtetime;
    }
	 
	 public function getRegisterupadtetime() {
        return $this->registerupadtetime;
    }
	
	public function  setRegisternextupadtetime($registernextupadtetime) {
        $this->registernextupadtetime=$registernextupadtetime;
    }
	
	 public function getRegisternextupadtetime() {
        return $this->registernextupadtetime;
    }
	
	function setSecuritypolicyaccept($securitypolicyaccept) {
            $this->securitypolicyaccept = $securitypolicyaccept;
        }

        function getSecuritypolicyaccept() {
            return $this->securitypolicyaccept;
        }
		
}
