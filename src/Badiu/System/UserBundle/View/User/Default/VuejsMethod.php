<?php
$sysoperation=$container->get('badiu.system.core.lib.util.systemoperation');
$isedit=$sysoperation->isEdit();

?> 
systemuserdefaultform_statusregister: function () {
	<?php if(!$isedit){?>$("#fgitem_statusregister").hide(); <?php }?>	
	 
},

systemuserdefaultform_alternatename: function () {
	if(this.badiuform.enablealternatename==1){
		$("#fgitem_alternatename").show(); 		
	}else  { 
	    $("#fgitem_alternatename").hide(); 	
	}
},

systemuserdefaultform_sex: function () {
	if(this.badiuform.sex=='other'){
		$("#fgitem_sexother").show(); 		
	}else  { 
	    $("#fgitem_sexother").hide(); 	
	}
},  
 
systemuserdefaultform_schoollevel: function () {
	if(this.badiuform.schoollevel=='universitygraduateincomplete' || 
		this.badiuform.schoollevel=='universitygraduatecompleted' || 
		this.badiuform.schoollevel=='universityspecialization' || 
		this.badiuform.schoollevel=='universitymasters' || 
		this.badiuform.schoollevel=='universitydoctorate'){
			$("#fgitem_highereducationarea").show(); 
			this.badiurequiredcontrol.highereducationarea=true;		
	}else  { 
			$("#fgitem_highereducationarea").hide(); 
			this.badiurequiredcontrol.highereducationarea=false;					
	}
},  

systemuserdefaultform_dadosprofissionais_govbrocupacaoprincipal: function () {
	if(this.badiuform.banetsysform_dadosprofissionais_govbrocupacaoprincipal=='setorpublico'){
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_govbresferaatuacao=true;
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_govbrareadeatuacao=true;
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_govbrtipodevinculo=true;
	}else  {  
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_govbresferaatuacao=false;
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_govbrareadeatuacao=false;
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_govbrtipodevinculo=false;
	} 
},

systemuserdefaultform_dadosprofissionais_govbrpais: function () {
	if(this.badiuform.banetsysform_dadosprofissionais_govbrpais=='BR'){
		
		$("#fgitem_banetsysform_dadosprofissionais_govbruf").show();
		$("#fgitem_banetsysform_dadosprofissionais_govbrcidade").show();
		
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_govbruf=true;
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_govbrcidade=true;
		
	}else  {  
		$("#fgitem_banetsysform_dadosprofissionais_govbruf").hide();
		$("#fgitem_banetsysform_dadosprofissionais_govbrcidade").hide();
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_govbruf=false;
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_govbrcidade=false;
	} 
},

//change state
systemuserdefaultform_dadosprofissionais_govbruf_change: function () {
	
	//review
	/*this.badiuform.banetsysform_dadosprofissionais_govbrcidade_badiuautocompleteitemname="";
	this.badiuform.banetsysform_dadosprofissionais_govbrcidade="BR";
	this.badiuform.banetsysform_dadosprofissionais_govbrcidade_badiuautocompleteitemchanged=false;
	this.badiuautocomplete.banetsysform_dadosprofissionais_govbrcidade_valueselected="";
	*/
},

systemuserdefaultform_dadosprofissionais_govbrareadeatuacao: function () {
	if(this.badiuform.banetsysform_dadosprofissionais_govbrareadeatuacao=='outraarea'){
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_areadeatuacaooutra=true;
		$("#fgitem_banetsysform_dadosprofissionais_areadeatuacaooutra").show(); 		
	}else  { 
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_areadeatuacaooutra=false;
	    $("#fgitem_banetsysform_dadosprofissionais_areadeatuacaooutra").hide(); 	
		
	}
}, 

systemuserdefaultform_dadosprofissionais_govbrtipodevinculo: function () {
	if(this.badiuform.banetsysform_dadosprofissionais_govbrtipodevinculo=='tipovinculooutraarea'){
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_tipodevinculooutro=true;
		$("#fgitem_banetsysform_dadosprofissionais_tipodevinculooutro").show(); 		
	}else  { 
		this.badiurequiredcontrol.banetsysform_dadosprofissionais_areadeatuacaooutra=false;
	    $("#fgitem_banetsysform_dadosprofissionais_tipodevinculooutro").hide(); 	
		
	}
}, 
