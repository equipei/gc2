<?php

namespace  Badiu\System\UserBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class UserRegisterUpadtetimebatchFormController extends BadiuFormController
{
    function __construct(Container $container) {
           parent::__construct($container);
              }

	

	 public function saveExec() {
		 $registernextupadtetime=$this->getParamItem('registernextupadtetime'); 
		 $entity=$this->getEntity();
		 $userdata=$this->getContainer()->get('badiu.system.user.user.data');
		 $result=$userdata->changeRegisterUpadtetimeForAll($entity,$registernextupadtetime);
		 $this->setResultexec($result);
		 return $result;
	  }

	public function execResponse() {
		$this->setSuccessmessage($this->getTranslator()->trans('badiu.system.user.registerupadtetimebatch.result',array('%countuser%'=>$this->getResultexec())));
		$outrsult=array('result'=>$this->getResultexec(),'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
		$this->getResponse()->setStatus("accept");
		$this->getResponse()->setMessage($outrsult);
		return $this->getResponse()->get();
	}
}
