<?php

namespace  Badiu\System\UserBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class UserFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
              
              
      public function changeParamOnOpen() {
          if ($this->isEdit() || $this->isClone()) {
             $this->setFormAddress();
            $this->setForm();
            $this->setFormDconfig();
            $this->setFormDocumentdata();
        }
     
     }      
    public function changeParam() {
         $this->getFormAddress();
         $this->getForm();
         $this->getFormDconfig();
         $this->getFormDocumentdata();
     }
    
    public function validation() {
         
        $isdocumentvalid = $this->isDocumentValid();
        if ($isdocumentvalid != null) {
            return $isdocumentvalid;
        }
        
        $isemailvalid = $this->isEmailValid();
        if ($isemailvalid != null) {
            return $isemailvalid;
        }

        $isemailduplicate = $this->isEmailDuplicate();
        if ($isemailduplicate != null) {
            return $isemailduplicate;
        }
       
        
        return null;
    }
    
    public function execAfter(){
        parent::execAfter();
        $userid=$this->getParamItem('id');
		//review 
		/*if($this->getContainer()->has('badiu.financ.ecommerce.lib.checkuserprofile')){
			$checkuserprofile = $this->getContainer()->get('badiu.financ.ecommerce.lib.checkuserprofile');
			$ckparam=array('userid'=>$userid);
			$pstatus=$checkuserprofile->moniotorStatus($ckparam);
		}*/
        
       
   }
     public function isDocumentValid() {
    
        $doctype = $this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield1');
        $docnumber = $this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield2');
        $entity =$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
         $id=$this->getParamItem('id');
       $data = $this->getContainer()->get($this->getKminherit()->data());
        if ($doctype == 'CPF') {
            
            $cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
            $valid = $cpfdata->isValid($docnumber);
            if (!$valid) {
               
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['document']= $this->getTranslator()->trans('badiu.system.user.user.message.cpfnotvalid');
                $info = 'badiu.system.user.user.message.cpfnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
            
          $number = $cpfdata->clean($docnumber);
          
           
           $duplicatecpf=null;
           if($this->isedit()){$duplicatecpf = $data->existColumnEdit($id,$entity, 'docnumber', $number);}
           else{$duplicatecpf = $data->existColumnAdd($entity, 'docnumber', $number);}
          
           
          if ($duplicatecpf) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['document']= $this->getTranslator()->trans('badiu.system.user.user.message.cpfexist');
                $info = 'badiu.system.user.user.message.cpfexis';
                return $this->getResponse()->denied($info, $message);
      
            }
      }else  if ($doctype == 'EMAIL') {
          //set key message for document
         $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');
         $email = $docnumber;
        
        if ($email) {
            $validate = $validatemail->email($email);
            if (!$validate) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['document']= $this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid');
                $info = 'badiu.system.user.user.message.emailnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
            $duplicateemail=null;
            if($this->isedit()){$duplicateemail = $data->existColumnEdit($id,$entity, 'docnumber', $email);}
            else{$duplicateemail = $data->existColumnAdd($entity, 'docnumber', $email);}
          
            if ($duplicateemail) {
                $message=array();
                 $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['document']= $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                $info = 'badiu.system.user.user.message.duplicationemail';
                return $this->getResponse()->denied($info, $message);
            }  
        }
      }
       
      return null;
    }
    public function isEmailValid() {
        $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');
        $email = $this->getUtildata()->getVaueOfArray($this->getParam(), 'email');
        $email=trim($email);
        if ($email) {
            $validate = $validatemail->email($email);
            if (!$validate) {
                $message=array();
                 $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['email']=$this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid');
                $info = 'badiu.system.user.user.message.emailnotvalid';
                return $this->getResponse()->denied($info, $message);
            }

        }
        $this->addParamItem('email',$email);
        return null;
    }

    public function isEmailDuplicate() {
        $data = $this->getContainer()->get($this->getKminherit()->data());
        $email = $this->getUtildata()->getVaueOfArray($this->getParam(), 'email');
         $id=$this->getParamItem('id');
         $entity =$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
        if ($email) {
            $duplicateemail = null;
             if($this->isedit()){$duplicateemail = $data->existColumnEdit($id,$entity, 'email', $email);}
            else{$duplicateemail = $data->existColumnAdd($entity, 'email', $email);}
          
            if ($duplicateemail) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.duplication.message');
                $message['email']= $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                $info = 'badiu.system.user.user.message.duplicationemail';
                return $this->getResponse()->denied($info, $message);
            }
        }
        
    }
    
   function  setFormAddress(){
        $param = $this->getParam();
        $contactdata     = $this->getUtildata()->getVaueOfArray($this->getParam(), 'contactdata');
         $contactdata=$this->getJson()->decode($contactdata,true); 
        $personalcontact     = $this->getUtildata()->getVaueOfArray($contactdata, 'personal');
        $param['personalpostcode']          = $this->getUtildata()->getVaueOfArray($personalcontact, 'postcode');
        $param['personaladdress']           = $this->getUtildata()->getVaueOfArray($personalcontact, 'address');
        $param['personaladdressnumber']     = $this->getUtildata()->getVaueOfArray($personalcontact, 'addressnumber');
        $param['personaladdresscomplement'] = $this->getUtildata()->getVaueOfArray($personalcontact, 'addresscomplement');
        $param['personalneighborhood']      = $this->getUtildata()->getVaueOfArray($personalcontact, 'neighborhood');
        $param['personalcity']              = $this->getUtildata()->getVaueOfArray($personalcontact, 'city');
        $param['personalstateid']             = $this->getUtildata()->getVaueOfArray($personalcontact, 'stateid');
        $param['personalcountry']           = $this->getUtildata()->getVaueOfArray($personalcontact, 'country');
        $param['personalphone']             = $this->getUtildata()->getVaueOfArray($personalcontact, 'phone');
        $param['personalphonemobile']       = $this->getUtildata()->getVaueOfArray($personalcontact, 'phonemobile');
       
        
         $professionalcontact     = $this->getUtildata()->getVaueOfArray($contactdata, 'professional');
        $param['professionalpostcode']          = $this->getUtildata()->getVaueOfArray($professionalcontact, 'postcode');
        $param['professionaladdress']           = $this->getUtildata()->getVaueOfArray($professionalcontact, 'address');
        $param['professionaladdressnumber']     = $this->getUtildata()->getVaueOfArray($professionalcontact, 'addressnumber');
        $param['professionaladdresscomplement'] = $this->getUtildata()->getVaueOfArray($professionalcontact, 'addresscomplement');
        $param['professionalneighborhood']      = $this->getUtildata()->getVaueOfArray($professionalcontact, 'neighborhood');
        $param['professionalcity']              = $this->getUtildata()->getVaueOfArray($professionalcontact, 'city');
        $param['professionalstateid']             = $this->getUtildata()->getVaueOfArray($professionalcontact, 'stateid');
        $param['professionalcountry']           = $this->getUtildata()->getVaueOfArray($professionalcontact, 'country');
        $param['professionalphone']             = $this->getUtildata()->getVaueOfArray($professionalcontact, 'phone');
        $param['professionalphonemobile']       = $this->getUtildata()->getVaueOfArray($professionalcontact, 'phonemobile');
        $param['professionalemail']             =$this->getUtildata()->getVaueOfArray($professionalcontact, 'email');
        $this->setParam($param);
   }
   
  
   function  getFormAddress(){
       //contact
        $personalcontact                      = array();
        $personalcontact['postcode']          = $this->getUtildata()->getVaueOfArray($this->getParam(), 'personalpostcode');
        $personalcontact['address']           = $this->getUtildata()->getVaueOfArray($this->getParam(), 'personaladdress');
        $personalcontact['addressnumber']     = $this->getUtildata()->getVaueOfArray($this->getParam(), 'personaladdressnumber');
        $personalcontact['addresscomplement'] = $this->getUtildata()->getVaueOfArray($this->getParam(), 'personaladdresscomplement');
        $personalcontact['neighborhood']      = $this->getUtildata()->getVaueOfArray($this->getParam(), 'personalneighborhood');
        $personalcontact['city']              = $this->getUtildata()->getVaueOfArray($this->getParam(), 'personalcity');
        $personalcontact['stateid']           = $this->getUtildata()->getVaueOfArray($this->getParam(), 'personalstateid');
        $personalcontact['state']             = "";
        $personalcontact['stateshortname']    = "";

        if (!empty($personalcontact['stateid'])) {
            $stateinfo                         = $this->getContainer()->get('badiu.util.address.state.data')->getInfoById($personalcontact['stateid']);
            $personalcontact['state']          = $this->getUtildata()->getVaueOfArray($stateinfo, 'name');
            $personalcontact['stateshortname'] = $this->getUtildata()->getVaueOfArray($stateinfo, 'shortname');
        }
        
        $personalcontact['country']  = $this->getUtildata()->getVaueOfArray($this->getParam(), 'personalcountry');

        $personalcontact['phone']       = $this->getUtildata()->getVaueOfArray($this->getParam(), 'personalphone');
        $personalcontact['phonemobile'] = $this->getUtildata()->getVaueOfArray($this->getParam(), 'personalphonemobile');
        $personalcontact['email']       = $this->getUtildata()->getVaueOfArray($this->getParam(), 'email');
        
        
        $professionalcontact                      = array();
        $professionalcontact['postcode']          = $this->getUtildata()->getVaueOfArray($this->getParam(), 'professionalpostcode');
        $professionalcontact['address']           = $this->getUtildata()->getVaueOfArray($this->getParam(), 'professionaladdress');
        $professionalcontact['addressnumber']     = $this->getUtildata()->getVaueOfArray($this->getParam(), 'professionaladdressnumber');
        $professionalcontact['addresscomplement'] = $this->getUtildata()->getVaueOfArray($this->getParam(), 'professionaladdresscomplement');
        $professionalcontact['neighborhood']      = $this->getUtildata()->getVaueOfArray($this->getParam(), 'professionalneighborhood');
        $professionalcontact['city']              = $this->getUtildata()->getVaueOfArray($this->getParam(), 'professionalcity');
        $professionalcontact['stateid']             = $this->getUtildata()->getVaueOfArray($this->getParam(), 'professionalstateid');
        $professionalcontact['state']             = "";
        $professionalcontact['stateshortname']    = ""; 
        
        $professionalcontact['country']           = $this->getUtildata()->getVaueOfArray($this->getParam(), 'professionalcountry');
        $professionalcontact['phone']       = $this->getUtildata()->getVaueOfArray($this->getParam(), 'professionalphone');
        $professionalcontact['phonemobile'] = $this->getUtildata()->getVaueOfArray($this->getParam(), 'professionalphonemobile');
        $professionalcontact['email']       = $this->getUtildata()->getVaueOfArray($this->getParam(), 'professionalemail');
        
        if (!empty($professionalcontact['stateid'])) {
            $stateinfo                         = $this->getContainer()->get('badiu.util.address.state.data')->getInfoById($professionalcontact['stateid']);
            $professionalcontact['state']          = $this->getUtildata()->getVaueOfArray($stateinfo, 'name');
            $professionalcontact['stateshortname'] = $this->getUtildata()->getVaueOfArray($stateinfo, 'shortname');
        }
        
        
        $contact                 = array();
        $contact['personal']     = $personalcontact;
        $contact['professional'] = $professionalcontact;
        $contactdatajsont        = $this->getJson()->encode($contact);
        
        $newparam=$this->getParam();
        $newparam['contactdata']=$contactdatajsont;
        
        //clean conctat field
        unset($newparam['personalpostcode']);
         unset($newparam['personaladdress']);
        unset($newparam['personaladdressnumber']);
        unset($newparam['personaladdresscomplement']);
        unset($newparam['personalneighborhood']);
        unset($newparam['personalcity']);
        unset($newparam['personalstateid']);
        unset($newparam['personalphone']);
        unset($newparam['personalphonemobile']);
        unset($newparam['personalphonemobile']);
        unset($newparam['personalcountry']);
        
        unset($newparam['professionalpostcode']);        
        unset($newparam['professionaladdress']);
        unset($newparam['professionaladdressnumber']);        
        unset($newparam['professionaladdresscomplement']);
        unset($newparam['professionalneighborhood']);        
        unset($newparam['professionalcity']);  
        unset($newparam['professionalstateid']);  
        unset($newparam['professionalphone']);
        unset($newparam['professionalphonemobile']);
        unset($newparam['professionalemail']);
        unset($newparam['professionalcountry']);
        
        
       
        $this->setParam($newparam);
   }
  
   function  getForm(){
       $newparam=$this->getParam();
       //document
        $newparam['doctype']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield1');
        $newparam['docnumber']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield2');
       
        $newparam['entity']=$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
        $newparam['deleted']=FALSE; 
        $newparam['confirmed']=TRUE;
        
         unset($newparam['document']);
         unset($newparam['document_badiuchoicetextfield1']);
         unset($newparam['document_badiuchoicetextfield2']);
         
         //password
         $password=$this->getUtildata()->getVaueOfArray($this->getParam(), 'password');
        if(!empty($password)){
            $password=md5($password);
            $newparam['password']=$password;
        }else{unset($newparam['password']);}
         
         
        $this->setParam($newparam);
      
   }
   
    function  setForm(){
       
            $param = $this->getParam();
             //doc
            $docconf=array();
            $docconf['field1'] = $this->getUtildata()->getVaueOfArray($this->getParam(), 'doctype');
            $docconf['field2'] =  $this->getUtildata()->getVaueOfArray($this->getParam(), 'docnumber');
            $param['document'] =  $docconf;
            
            //password
            $param['password'] = "";
            $this->setParam($param);
   }

   function  getFormDconfig(){
        $param = $this->getParam();
        $profiledata=array();
        $profiledata['maritalstatus']=$this->getParamItem('maritalstatus');
        unset($param['maritalstatus']);

        $profiledata['dadname']=$this->getParamItem('dadname');
        unset($param['dadname']);

        $profiledata['mothername']=$this->getParamItem('mothername');
        unset($param['mothername']);

        $profiledata['profession']=$this->getParamItem('profession');
        unset($param['profession']);

        $profiledata['schoollevel']=$this->getParamItem('schoollevel');
        //unset($param['schoollevel']); 

        $profiledata['placeofbirth']=$this->getParamItem('placeofbirth');
        unset($param['placeofbirth']);

        $cdbdconfig=array();
        if($this->isEdit()){
            $id=$this->getParamItem('id');
            $data=$this->getContainer()->get($this->getKminherit()->data());
            $cdbdconfig=$data->getColumnValue($this->getEntity(),'dconfig',$paramf=array('id'=>$id));
            $cdbdconfig = $this->getJson()->decode($cdbdconfig, true);
            if(empty($cdbdconfig)){$cdbdconfig=array();}
        }
        $cdbdconfig['profile']=$profiledata;
        $cdbdconfig = $this->getJson()->encode($cdbdconfig);
        $param['dconfig']=$cdbdconfig;
       
        $this->setParam($param);
    }

    function  setFormDconfig(){
        $param = $this->getParam();
        $dconfig=$this->getParamItem('dconfig');
        $dconfig = $this->getJson()->decode($dconfig, true);
        $paramdconf=$this->getUtildata()->getVaueOfArray( $dconfig, 'profile');
       if(is_array($paramdconf)){ $param =array_merge($param,$paramdconf);}
        $this->setParam($param);
    }

    function  getFormDocumentdata(){
        $param = $this->getParam();
        $docdata=array();
        $docdata['rg']['number']=$this->getParamItem('rg');
        unset($param['rg']);

      
        $cdbdocumentdata=array();
        if($this->isEdit()){
            $id=$this->getParamItem('id');
            $data=$this->getContainer()->get($this->getKminherit()->data());
            $cdbdocumentdata=$data->getColumnValue($this->getEntity(),'documentdata',$paramf=array('id'=>$id));
            $cdbdocumentdata = $this->getJson()->decode($cdbdocumentdata, true);
            if(empty($cdbdocumentdata)){$cdbdocumentdata=array();}
        }
        foreach ($docdata as $key => $value){$cdbdocumentdata[$key]=$value;}
      
        $cdbdocumentdata = $this->getJson()->encode($cdbdocumentdata);
        $param['documentdata']=$cdbdocumentdata;
       
        $this->setParam($param);
    }
    function  setFormDocumentdata(){
        $param = $this->getParam();
        $documentdata=$this->getParamItem('documentdata');
        $documentdata = $this->getJson()->decode($documentdata, true);
        $rg=$this->getUtildata()->getVaueOfArray( $documentdata, 'rg.number',true);
        $param['rg']=$rg;
        $this->setParam($param);
    }
    
}
