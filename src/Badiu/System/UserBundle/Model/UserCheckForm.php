<?php

namespace Badiu\System\UserBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuCheckForm;

class UserCheckForm extends BadiuCheckForm {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function validation() {
        
        $isdocumentvalid = $this->isDocumentValid();
        if ($isdocumentvalid != null) {
            return $isdocumentvalid;
        }
        
        $isemailvalid = $this->isEmailValid();
        if ($isemailvalid != null) {
            return $isemailvalid;
        }

        $isemailduplicate = $this->isEmailDuplicate();
        if ($isemailduplicate != null) {
            return $isemailduplicate;
        }
       
        
        return null;
    }

    public function isDocumentValid() {
   
        $doctype = $this->getUtildata()->getVaueOfArray($this->getParam(), 'doctype');
        $docnumber = $this->getUtildata()->getVaueOfArray($this->getParam(), 'docnumber');
        $entity = $this->getUtildata()->getVaueOfArray($this->getParam(), 'entity');
        if ($doctype == 'CPF') {
            echo "cpf: $docnumber";exit;
            $cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
            $valid = $cpfdata->isValid($docnumber);
            if (!$valid) {
                $message=array();
                $message['generalerror']= $this->getTranslator()->trans('badiu.system.user.user.message.cpfnotvalid');
                $info = 'badiu.system.user.user.message.cpfnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
            
          $number = $cpfdata->clean($docnumber);
          $data = $this->getContainer()->get($this->getKminherit()->data());
          $duplicatecpf = $data->existColumnAdd($entity, 'docnumber', $number);

          if ($duplicatecpf) {
                $message=array();
                $message['generalerror']= $this->getTranslator()->trans('badiu.system.user.user.message.cpfexis');
                $info = 'badiu.system.user.user.message.cpfexis';
                return $this->getResponse()->denied($info, $message);
      
            }
      }else  if ($doctype == 'EMAIL') {
          //set key message for document
         $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');
         $email = $docnumber;
        
        if ($email) {
            $validate = $validatemail->email($email);
            if (!$validate) {
                $message=array();
                $message['generalerror']= $this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid');
                $info = 'badiu.system.user.user.message.emailnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
          $duplicateemail = $data->existColumnAdd($this->getEntity(), 'email', $email);
            if ($duplicateemail) {
                $message=array();
                $message['generalerror']= $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                $info = 'badiu.system.user.user.message.duplicationemail';
                return $this->getResponse()->denied($info, $message);
            }  
        }
      }
       
      return null;
    }
    public function isEmailValid() {
        $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');
        $email = $this->getUtildata()->getVaueOfArray($this->getParam(), 'email');
        
        if ($email) {
            $validate = $validatemail->email($email);
            if (!$validate) {
                $message=array();
                $message['generalerror']=$this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid');
                $info = 'badiu.system.user.user.message.emailnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
        }
        return null;
    }

    public function isEmailDuplicate() {
        $data = $this->getContainer()->get($this->getKminherit()->data());
        $email = $this->getUtildata()->getVaueOfArray($this->getParam(), 'email');
        if ($email) {
            $duplicateemail = $data->existColumnAdd($this->getEntity(), 'email', $email);
            if ($duplicateemail) {
                $message=array();
                $message['generalerror']= $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                $info = 'badiu.system.user.user.message.duplicationemail';
                return $this->getResponse()->denied($info, $message);
            }
        }
        
    }

    /*
      public function addCheckForm($form, $data) {

      $check = true;

      // check cpf
      

     

      //validate email
      $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');

      $email = $form->get('email')->getData();

      if ($email) {

      $validate = $validatemail->email($email);
      if (!$validate) {
      $form->get('email')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid')));
      $check = false;
      }
      }

      //check email
      $email = $form->get('email')->getData();
      $cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
      $duplicateemail = $data->existColumnAdd($this->getEntity(), 'email', $email);
      if ($duplicateemail) {
      $form->get('email')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array(
      '%record%' => $email
      ))));
      $check = false;
      }

      //validate cep
      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');

      $cep = $form->get('personalpostcode')->getData();

      if ($cep) {
      $validate = $validatecontact->cep($cep);
      if (!$validate) {
      $form->get('personalpostcode')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.cepnotvalid')));
      $check = false;
      }
      }

      //validate username
      $username = $form->get('username')->getData();
      $duplicateusername = $data->existColumnAdd($this->getEntity(), 'username', $username);
      if ($duplicateusername) {
      $form->get('username')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.duplicationusername', array(
      '%record%' => $username
      ))));
      $check = false;
      }

      //validate telefone fixo

      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');

      $number_fixed = $form->get('personalphone')->getData();

      if ($number_fixed) {
      $validate = $validatecontact->phoneFixe($number_fixed);
      if (!$validate) {
      $form->get('personalphone')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonenotvalid')));
      $check = false;
      }
      }

      //validate telefone mobile

      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');

      $number_mobile = $form->get('personalphonemobile')->getData();

      if ($number_mobile) {
      $validate = $validatecontact->phoneMobile($number_mobile);
      if (!$validate) {
      $form->get('personalphonemobile')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonemobilenotvalid')));
      $check = false;
      }
      }


      //Professional teste
      //validate cep

      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');

      $cep = $form->get('professionalpostcode')->getData();

      if ($cep) {
      $validate = $validatecontact->cep($cep);
      if (!$validate) {
      $form->get('professionalpostcode')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.cepnotvalid')));
      $check = false;
      }
      }



      //validate telefone fixo
      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');
      $number = $form->get('professionalphone')->getData();
      if ($number) {
      $validate = $validatecontact->phoneFixe($number);
      if (!$validate) {
      $form->get('professionalphone')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonenotvalid')));
      $check = false;
      }
      }


      //validate telefone mobile

      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');

      $number = $form->get('professionalphonemobile')->getData();

      if ($number) {
      $validate = $validatecontact->phoneMobile($number);
      if (!$validate) {
      $form->get('professionalphonemobile')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonemobilenotvalid')));
      $check = false;
      }
      }

      //validate email

      $email = $form->get('professionalemail')->getData();

      if ($email) {
      $validate = $validatemail->Email($email);
      if (!$validate) {
      $form->get('professionalemail')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid')));
      $check = false;
      }
      }

      return $check;
      }

      public function editCheckForm($form, $data) {
      $check = true;

      // check CPF
      $doc = $form->get('document')->getData();
      $doctype = $this->getUtildata()->getVaueOfArray($doc, 'field1');
      $docnumber = $this->getUtildata()->getVaueOfArray($doc, 'field2');

      if ($doctype == 'CPF') {
      $cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
      $valid = $cpfdata->isValid($docnumber);
      if (!$valid) {
      $form->get('document')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.cpfnotvalid')));
      $check = false;
      }


      $number = $cpfdata->clean($docnumber);

      $duplicatecpf = $data->existColumnEdit($this->getId(), $this->getEntity(), 'docnumber', $number);

      if ($duplicatecpf) {

      $form->get('document')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.cpfexist')));
      $check = false;
      }
      }

      //validate email

      $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');

      $email = $form->get('email')->getData();

      if ($email) {

      $validate = $validatemail->email($email);
      if (!$validate) {
      $form->get('email')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid')));
      $check = false;
      }
      }

      //check email
      $email = $form->get('email')->getData();
      $duplicateemail = $data->existColumnEdit($this->getId(), $this->getEntity(), 'email', $email);
      if ($duplicateemail) {
      $form->get('email')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array(
      '%record%' => $email
      ))));
      $check = false;
      }


      //validate cep

      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');

      $cep = $form->get('personalpostcode')->getData();

      if ($cep) {
      $validate = $validatecontact->cep($cep);
      if (!$validate) {
      $form->get('personalpostcode')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.cepnotvalid')));
      $check = false;
      }
      }

      //check username
      $username = $form->get('username')->getData();
      $duplicateusername = $data->existColumnEdit($this->getId(), $this->getEntity(), 'username', $username);
      if ($duplicateusername) {
      $form->get('username')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.duplicationusername', array(
      '%record%' => $username
      ))));
      $check = false;
      }

      //validate telefone fixo
      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');
      $number = $form->get('personalphone')->getData();
      if ($number) {
      $validate = $validatecontact->phoneFixe($number);
      if (!$validate) {
      $form->get('personalphone')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonenotvalid')));
      $check = false;
      }
      }


      //validate telefone mobile

      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');

      $number = $form->get('personalphonemobile')->getData();

      if ($number) {

      $validate = $validatecontact->phoneMobile($number);
      if (!$validate) {
      $form->get('personalphonemobile')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonemobilenotvalid')));
      $check = false;
      }
      }


      //Professional
      //validate cep

      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');

      $cep = $form->get('professionalpostcode')->getData();

      if ($cep) {
      $validate = $validatecontact->cep($cep);
      if (!$validate) {
      $form->get('professionalpostcode')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.cepnotvalid')));
      $check = false;
      }
      }


      //validate telefone fixo
      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');
      $number = $form->get('professionalphone')->getData();
      if ($number) {
      $validate = $validatecontact->phoneFixe($number);
      if (!$validate) {
      $form->get('professionalphone')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonenotvalid')));
      $check = false;
      }
      }


      //validate telefone mobile

      $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');

      $number = $form->get('professionalphonemobile')->getData();

      if ($number) {
      $validate = $validatecontact->PhoneMobile($number);
      if (!$validate) {
      $form->get('professionalphonemobile')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonemobilenotvalid')));
      $check = false;
      }
      }

      //validate email

      $email = $form->get('professionalemail')->getData();

      if ($email) {
      $validate = $validatemail->Email($email);
      if (!$validate) {
      $form->get('professionalemail')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid')));
      $check = false;
      }
      }

      return $check;
      }
     */
}
