<?php
 
namespace  Badiu\System\UserBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }
 

    public function exec() {
       $result=$this->initPermissions();
  		return $result;
	} 
	
	
	  public function initPermissions() {
        $cont=0;
		
        $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');

        //guest  
        $param=array(
		  'entity'=>$this->getEntity(),
          'roleshortname'=>'guest',
          'permissions'=>array('badiu.system.user.recoverpwd.add',
		  'badiu.admin.client.newdefaultprofile.add', //temp
          'badiu.system.user.recoverpwdchange.add',
		  'badiu.system.user.recoverpwd.add')
        );
        $cont+=$libpermission->add($param);

		return  $cont;
    }
 
}
