<?php

namespace  Badiu\System\UserBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class RecoverpwdFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
    

	 public function save() {
		 $valid=$this->validation();
		 if(!empty($valid)){return $valid;}
		 
        $recoverykey = $this->resetPassword();
		$email=$this->getParamItem('emailtorecover'); 
        $mailsend = false;
        try {
            $mailsend = $this->sendMail($email, $recoverykey);
        } catch (\Swift_TransportException $ex) {
            $mailsend = false;
        }
		if($mailsend){
			$email=$this->partialEmailIdentify($email);
			
			$msg=$this->getTranslator()->trans('badiu.system.user.recoverpwd.messagemailsendsuccess',array('%email%'=>$email));
			
			$info='badiu.system.user.recoverpwd.messagemailsendsuccess';
			$outrsult=array('result'=>$mailsend,'message'=>$msg,'urlgoback'=>null);
			return $this->getResponse()->accept($outrsult,$info);
		}
		$message=array();
        $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.recoverpwd.messagemailsendfailed');
        $info = 'badiu.system.user.recoverpwd.messagemailsendfailed';
        return $this->getResponse()->denied($info, $message);
		
    }
	
    public function validation() {
       
        $isemailvalid = $this->isEmailValid();
        if ($isemailvalid != null) {
            return $isemailvalid;
        }

       $isloginvalid = $this->isLoginValid();
        if ($isloginvalid != null) {
            return $isloginvalid;
        }
        
        return null;
    }
   
    public function isEmailValid() {
		 $typeidentify=$this->getParamItem('typeidentify'); 
		 if($typeidentify!='email'){return null;}
		 
		 $email=$this->getParamItem('email'); 
		 $sysentity=$this->getParamItem('sysentity'); 
		
		
         $email=trim($email);
		 if(empty($email)){
			 $message=array();
             $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.recoverpwd.emailrequired');
             $info = 'badiu.system.user.recoverpwd.emailrequired';
             return $this->getResponse()->denied($info, $message);
		 }
        
		 $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');
         $validate = $validatemail->email($email);
         if (!$validate) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid');
                $info = 'badiu.system.user.user.message.emailnotvalid';
                return $this->getResponse()->denied($info, $message);
         }
		
		
		$param = array('email' => $email, 'entity' => $sysentity);
        $data = $this->getContainer()->get('badiu.system.user.user.data');
        $exist = $data->countNativeSql($param, false);
        if (!$exist) {
			$message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.recoverpwd.emailnotfound');
            $info = 'badiu.system.user.recoverpwd.emailnotfound';
            return $this->getResponse()->denied($info, $message);
		}
       $this->addParamItem('email',$email);  
        return null;
    }

   public function isLoginValid() {
		 $typeidentify=$this->getParamItem('typeidentify'); 
		 if($typeidentify!='username'){return null;}
		  $username=$this->getParamItem('username'); 
		$sysentity=$this->getParamItem('sysentity'); 
		$entity=$sysentity;
		$username=trim($username);
         if(empty($username)){
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.recoverpwd.usernamerequired');
                $info = 'badiu.system.user.recoverpwd.usernamerequired';
                return $this->getResponse()->denied($info, $message);
         }
		
		$param = array('username' => $username, 'entity' => $entity);
        $data = $this->getContainer()->get('badiu.system.user.user.data');
        $exist = $data->countNativeSql($param, false);
        if (!$exist) {
			$message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.recoverpwd.usernamelnotfound');
            $info = 'badiu.system.user.recoverpwd.usernamelnotfound';
            return $this->getResponse()->denied($info, $message);
		}
        $this->addParamItem('username',$username);  
        return null;
    }
    
	

    public function resetPassword() {
        $typeidentify=$this->getParamItem('typeidentify'); 
		$username=$this->getParamItem('username'); 
		$email=$this->getParamItem('email'); 
		$entity=$this->getParamItem('sysentity'); 
        
        if ($typeidentify == 'email' && !empty($email)) {
            $param = array('email' => $email, 'entity' => $entity);
            $data = $this->getContainer()->get('badiu.system.user.user.data');
			
            $user = $data->getInfoByEmailForLogin($entity, $email);
            if (!empty($user)) {
                $userid = $this->getUtildata()->getVaueOfArray($user,'id');
                $name =$this->getUtildata()->getVaueOfArray($user,'name');
                $reciverkey = $this->generateToken($entity, $userid, $name, $email);
				$this->addParamItem('emailtorecover',$email); 
                return $reciverkey;
            }
        } else if ($typeidentify == 'username' && !empty($username)) {
            $data = $this->getContainer()->get('badiu.system.user.user.data');
            $user = $data->getInfoByUsernameForLogin($username);
            if (!empty($user)) {
                $userid = $this->getUtildata()->getVaueOfArray($user,'id');
                $name =$this->getUtildata()->getVaueOfArray($user,'name');
                $email =$this->getUtildata()->getVaueOfArray($user,'email');
                $reciverkey = $this->generateToken($entity, $userid, $name, $email);
				$this->addParamItem('emailtorecover',$email); 
                return $reciverkey;
            }
        }
        return null;
    }

    public function generateToken($entity, $userid, $name, $email) {
        //genarate ukey
        $now = time();
        $basekey = "|$userid|$entity|$now";
        $lenth = strlen($basekey);
        $hash = $this->getContainer()->get('badiu.system.core.lib.util.hash');
        $lenth = 60 - $lenth;
        $recoverkey = $hash->make($lenth);
        $recover = $recoverkey . $basekey;
        $param = array('bkey' => 'badiu.system.user', 'modulekey' => 'badiu.system.user.recoverpwd', 'moduleinstance' => $userid, 'name' => $name, 'shortname' => $recover, 'customint1' => 1, 'customchar1' => $email, 'entity' => $entity, 'timecreated' => new \DateTime(), 'deleted' => 0);
        $data = $this->getContainer()->get('badiu.system.module.data.data');
        $result = $data->insertNativeSql($param, false);
        if ($result) {
            return $recover;
        }
    }

    function sendMail($email, $recoverykey) {
        
        $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
        $urlpwd=$utilapp->getUrlByRoute('badiu.system.user.recoverpwdchange.add',array('cpwtoken'=>$recoverykey));
        $link="<a href=\"$urlpwd\" target = \"_blank\" >$urlpwd</a>";
        $msg=$this->getTranslator()->trans('badiu.system.user.recoverpwd.messagemailcontent',array('%link%'=>$link,'%sitename%'=>'Badiu.Net'));
        $mconfig=array();
      
        $mconfig['useridto']=null;
        $mconfig['userto']=null;
        $mconfig['recipient']['to']=$email;
        $mconfig['recipient']['bcc']= null;
        $mconfig['message']['subject']=$this->getTranslator()->trans('badiu.system.user.recoverpwd.messagemailsubject');
        $mconfig['message']['body']=$msg;
        $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
		
		$systemdata=$this->getContainer()->get('badiu.system.core.functionality.systemdata');
		$systemdata->setEntity($this->getEntity());
		$systemdata->init();
		$smtp->setSystemdata($systemdata);
		
        //$smtp->setUsersender($this->getUsersender());
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        //$badiuSession->setHashkey($this->getSessionhashkey());
        $entity= $this->getParamItem('sysentity'); 

        //$entity=$this->getEntity();
        $smtp->setEntity($entity);
        $result=$smtp->sendMail($mconfig);

        return $result;
    }
	
		function partialEmailIdentify($email){
		$p = explode("@",$email);
		$p1=$this->getUtildata()->getVaueOfArray($p,0);
		$len=strlen($p1);
		$avg=0;
		if($len >2 ){$avg=$len/2;}
		
		if($avg){
			$strpl="";
			for($i=0;$i<=$avg;$i++){$strpl.="*";}
			$email=substr_replace($email,$strpl,0,$avg);
		}
		return $email;
	}
}
