<?php

namespace  Badiu\System\UserBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class UserFormat extends BadiuFormat{
    private  $userdataoptions=null;
	private  $rolecpf=null;
     function __construct(Container $container) {
            parent::__construct($container);
            $this->userdataoptions=$this->getContainer()->get('badiu.system.user.user.form.dataopotions');
			$this->rolecpf=$this->getContainer()->get('badiu.util.document.role.cpf');
       } 

              
     
    public  function fullName($data){
            $value="";
			 $firstname="";
			 $lastname="";
			 
			if(isset($data["firstname"])) {$firstname=$data["firstname"];}
			if(isset($data["lastname"])){$lastname=$data["lastname"];}
			
			$value=$firstname ." ".$lastname;
			
            
        
        return $value; 
    } 
     
    public  function docnumber($data){
            $value="";
			$docnumber=null;
			$doctype=null;
			
			 
			if(isset($data["docnumber"])) {$docnumber=$data["docnumber"];}
			if(isset($data["doctype"])){$doctype=$data["doctype"];}
			
			//if($doctype=='CPF'){
					$docservice=
					
					$value=$docservice->format($docnumber);
			//}else{
			//	$value=$docnumber;
			//}
			
	  
        return $value; 
    } 
    
	public  function docnumbershowtype($data){
            $value="";
			$docnumber=null;
			$doctype=null;
			
			$docnumber=$this->getUtildata()->getVaueOfArray($data,'docnumber');
			$doctype=$this->getUtildata()->getVaueOfArray($data,'doctype');
			if($doctype=='CPF'){
				$docservice=$this->getContainer()->get('badiu.util.document.role.cpf');
				$value=$this->rolecpf->format($docnumber);
			}else{
				$value=$docnumber;
			}
			if(empty($doctype)){return null;}
			if(empty($docnumber)){return null;}
			
			$result="$doctype: $value";
			if($doctype=='EMAIL'){$result="E-mail: $value";}
			
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		   $formathiddetype=$badiuSession->getValue('badiu.util.document.type.param.config.formathiddetype');
		    if($formathiddetype){$result=$value;}
			
        return $result;  
    } 
    public  function personaladdress($data){
        $contactdata=$this->getUtildata()->getVaueOfArray($data,'contactdata');
        $contactdata=$this->getJson()->decode($contactdata,true); 
        $personalcontact=$this->getUtildata()->getVaueOfArray($contactdata,'personal');
        $personalcontact=$this->getContainer()->get('badiu.util.address.data.format')->defaultdd($personalcontact);
        return $personalcontact;
    }

    public  function professionaladdress($data){
        $contactdata=$this->getUtildata()->getVaueOfArray($data,'contactdata');
        $contactdata=$this->getJson()->decode($contactdata,true); 
        $professionalcontact=$this->getUtildata()->getVaueOfArray($contactdata,'professional');
        $professionalcontact=$this->getContainer()->get('badiu.util.address.data.format')->defaultdd($professionalcontact);
        return $professionalcontact;
    }

    public  function maritalstatus($data){
        $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
        $dconfig=$this->getJson()->decode($dconfig,true); 
        $maritalstatus=$this->getUtildata()->getVaueOfArray($dconfig,'profile.maritalstatus',true);
        $maritalstatus=$this->userdataoptions->maritalstatusLabel($maritalstatus);
        return $maritalstatus;
        
    }
    public  function dadname($data){
        $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
        $dconfig=$this->getJson()->decode($dconfig,true); 
        $value=$this->getUtildata()->getVaueOfArray($dconfig,'profile.dadname',true);
       
        return $value;
        
    }
    public  function mothername($data){
        $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
        $dconfig=$this->getJson()->decode($dconfig,true); 
        $value=$this->getUtildata()->getVaueOfArray($dconfig,'profile.mothername',true);
        return $value;
        
    }
    public  function sex($data){ 
        $sex=$this->getUtildata()->getVaueOfArray($data,'sex');
        $sex=$this->userdataoptions->sexLabel($sex);
        return $sex;
        
    }

    public  function documentrg($data){
        $documentdata=$this->getUtildata()->getVaueOfArray($data,'documentdata');
        $documentdata=$this->getJson()->decode($documentdata,true); 
        $value=$this->getUtildata()->getVaueOfArray($documentdata,'rg.number',true);
       
        return $value;
        
    }
    public  function profession($data){
        $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
        $dconfig=$this->getJson()->decode($dconfig,true); 
        $value=$this->getUtildata()->getVaueOfArray($dconfig,'profile.profession',true);
        return $value;
        
    }
    public  function schoollevel($data){
		/*echo "<pre>";
		print_r($data);
		echo "</pre>";exit;*/
		$schoollevel=$this->getUtildata()->getVaueOfArray($data,'schoollevel');
		if(!empty($schoollevel)){return $this->userdataoptions->schoollevelLabel($schoollevel);}
		
        $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
        $dconfig=$this->getJson()->decode($dconfig,true); 
        $value=$this->getUtildata()->getVaueOfArray($dconfig,'profile.schoollevel',true);
        $value=$this->userdataoptions->schoollevelLabel($value);
        return $value;
        
    }
	//temp review
	public  function adminFormHighereducationarea($data){
		 $highereducationarea=$this->getUtildata()->getVaueOfArray($data,'highereducationarea');
		  if(empty($highereducationarea)){return null;}
		$data= $this->getContainer()->get('badiu.admin.form.fieldoptions.data');
		$name=$data->getNameById($highereducationarea);
        return $name;
    }
    public  function placeofbirth($data){
        $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
        $dconfig=$this->getJson()->decode($dconfig,true); 
        $value=$this->getUtildata()->getVaueOfArray($dconfig,'profile.placeofbirth',true);
        
        return $value;
        
    }
    public  function nationalitystatus($data){
        $value=$this->getUtildata()->getVaueOfArray($data,'nationalitystatus');
        $value=$this->userdataoptions->getNationalitystatusLabel($value);
        return $value;
        
    }

    public  function statusregistericon($data){
        $icon="";
        $statusregister=$this->getUtildata()->getVaueOfArray($data,'statusregister');
        $label=$this->userdataoptions->getStatusRegisterLabel($statusregister);
       if($statusregister=='complete'){ $icon='<span class="btn btn-success btn-circle"><i class="fa fa-id-card"></i></span>'.$label;}
       else if($statusregister=='incomplete'){ $icon='<span class="btn btn-secondary btn-circle"><i class="fa fa-id-card"></i></span>'.$label;}

        return $icon;
    }
	public  function statusregister($data){
        $statusregister=$this->getUtildata()->getVaueOfArray($data,'statusregister');
		$registernextupadtetime=$this->getUtildata()->getVaueOfArray($data,'registernextupadtetime');
		
		if(empty($statusregister)){$statusregister='incomplete';}
		
		if($statusregister=='complete' && !empty($registernextupadtetime) && is_a($registernextupadtetime, 'DateTime')){
			$now=new \DateTime();
			if($registernextupadtetime->getTimestamp() < $now->getTimestamp()){$statusregister=='requireupdate';}
		}
		
		$statusregister=$this->userdataoptions->getStatusRegisterLabel($statusregister);
        return $statusregister;
    }
	
	
}
