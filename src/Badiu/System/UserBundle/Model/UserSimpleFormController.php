<?php

namespace  Badiu\System\UserBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class UserSimpleFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
              
              
      public function changeParamOnOpen() {
          if ($this->isEdit() || $this->isClone()) {
           
            $this->setForm();
			
        }
     
     }      
    public function changeParam() {
	
        $this->getForm();
		
     }
    
    public function validation() {
        
        $isdocumentvalid = $this->isDocumentValid();
        if ($isdocumentvalid != null) {
            return $isdocumentvalid;
        }
        
        $isemailvalid = $this->isEmailValid();
        if ($isemailvalid != null) {
            return $isemailvalid;
        }

        $isemailduplicate = $this->isEmailDuplicate();
        if ($isemailduplicate != null) {
            return $isemailduplicate;
        }
		
		$mvalidate=$this->isValidNameSpleted();
		if ($mvalidate != null) { return $mvalidate;}
		
       
		
        return null;
    }
    
     public function isDocumentValid() {
    
        $doctype = $this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield1');
        $docnumber = $this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield2');
        $entity =$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
         $id=$this->getParamItem('id');
        $data = $this->getContainer()->get($this->getKminherit()->data());
        if ($doctype == 'CPF') {
            
			if(empty($docnumber)){
				$message=array();
                $message['generalerror'] =$this->getTranslator()->trans('badiu.system.user.user.message.cpfrequired');
                //$message['document']= $this->getTranslator()->trans('badiu.system.user.user.message.ceprequired');
                $info = 'badiu.system.user.user.message.cpfrequired';
                return $this->getResponse()->denied($info, $message);
			}
            $cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
            $valid = $cpfdata->isValid($docnumber);
            if (!$valid) {
               
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.cpfnotvalid');
                $message['document']= $this->getTranslator()->trans('badiu.system.user.user.message.cpfnotvalid');
                $info = 'badiu.system.user.user.message.cpfnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
            
          $number = $cpfdata->clean($docnumber);
          
           
           $duplicatecpf=null;
           if($this->isedit()){$duplicatecpf = $data->existColumnEdit($id,$entity, 'docnumber', $number);}
           else{$duplicatecpf = $data->existColumnAdd($entity, 'docnumber', $number);}
          
           
          if ($duplicatecpf) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['document']= $this->getTranslator()->trans('badiu.system.user.user.message.cpfexist');
                $info = 'badiu.system.user.user.message.cpfexis';
                return $this->getResponse()->denied($info, $message);
      
            }
      }else  if ($doctype == 'EMAIL') {
          //set key message for document
         $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');
         $email = $docnumber;
        
        if ($email) {
            $validate = $validatemail->email($email);
            if (!$validate) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid');
                $message['document']= $this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid');
                $info = 'badiu.system.user.user.message.emailnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
            $duplicateemail=null;
            if($this->isedit()){$duplicateemail = $data->existColumnEdit($id,$entity, 'docnumber', $email);}
            else{$duplicateemail = $data->existColumnAdd($entity, 'docnumber', $email);}
          
            if ($duplicateemail) {
                $message=array();
                 $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail');
                $message['document']= $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                $info = 'badiu.system.user.user.message.duplicationemail';
                return $this->getResponse()->denied($info, $message);
            }  
        }
      }
       
      return null;
    }
    public function isEmailValid() {
        $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');
        $email = $this->getUtildata()->getVaueOfArray($this->getParam(), 'email');
        $email=trim($email);
        if ($email) {
            $validate = $validatemail->email($email);
            if (!$validate) {
                $message=array();
                 $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['email']=$this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid');
                $info = 'badiu.system.user.user.message.emailnotvalid';
                return $this->getResponse()->denied($info, $message);
            }

        }
        $this->addParamItem('email',$email);
        return null;
    }

    public function isEmailDuplicate() {
        $data = $this->getContainer()->get($this->getKminherit()->data());
        $email = $this->getUtildata()->getVaueOfArray($this->getParam(), 'email');
         $id=$this->getParamItem('id');
         $entity =$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
        if ($email) {
            $duplicateemail = null;
             if($this->isedit()){$duplicateemail = $data->existColumnEdit($id,$entity, 'email', $email);}
            else{$duplicateemail = $data->existColumnAdd($entity, 'email', $email);}
          
            if ($duplicateemail) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.duplication.message');
                $message['email']= $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                $info = 'badiu.system.user.user.message.duplicationemail';
                return $this->getResponse()->denied($info, $message);
            }
        }
        
    }
	
	 public function isValidName() {
        $userutil=$this->getContainer()->get('badiu.system.user.user.util');
		 $ownername=$this->getParamItem('name');
		if(!empty($ownername) && $userutil->hasSpecialCharacter($ownername)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.name.invalidcharacter');
            $info='badiu.admin.selection.loginsingin.namewithinvalidcharacter';
            return $this->getResponse()->denied($info, $message);
        }
		$ownername=$userutil->cleanNameSpace($ownername);
        $people=$userutil->splitFirtnameLastname($ownername);
		
		$this->addParamItem('firstname',$people->firtname);
		$this->addParamItem('lastname',$people->lastname);
        
        if(empty($people->firtname) || empty($people->lastname)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingin.message.namerequired');
            $info='badiu.admin.selection.loginsingin.namerequired';
            return $this->getResponse()->denied($info, $message);
        }
        
		$usernameuppercase=$this->getContainer()->get('badiu.system.access.session')->getValue('badiu.system.core.param.config.usernameuppercase');
		if($usernameuppercase){
			$this->addParamItem('firstname',mb_strtoupper($people->firtname,'UTF-8'));
			$this->addParamItem('lastname',mb_strtoupper($people->lastname,'UTF-8'));
		}
		
		//alternatename
		$enablealternatename=$this->getParamItem('enablealternatename');
		if(!$enablealternatename){$this->addParamItem('alternatename',null);}
		
		$alternatename=$this->getParamItem('alternatename');
		if(empty($alternatename)){return null;}
		
		$alternatename=$userutil->cleanNameSpace($alternatename);
		if($userutil->hasSpecialCharacter($alternatename)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.alternatename.invalidcharacter');
            $info='badiu.admin.selection.loginsingin.alternatenamewithinvalidcharacter';
            return $this->getResponse()->denied($info, $message);
        }
		
		if($usernameuppercase){
			$alternatename=mb_strtoupper($alternatename,'UTF-8');
		}
		$this->addParamItem('alternatename',$alternatename);
		$this->removeParamItem('name');
        return null;
    } 
	
	 public function isValidNameSpleted() {
		 
		 
        $userutil=$this->getContainer()->get('badiu.system.user.user.util');
		$firstname=$this->getParamItem('firstname');
		$firstname=trim($firstname);
		 if(empty($firstname)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.firstnamerequired');
            $info='badiu.system.user.user.firstnamerequired';
            return $this->getResponse()->denied($info, $message);
        }
		
		
		if($userutil->hasSpecialCharacter($firstname)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.name.invalidcharacter');
            $info='badiu.admin.selection.loginsingin.namewithinvalidcharacter';
            return $this->getResponse()->denied($info, $message);
        }
		$firstname=$userutil->cleanNameSpace($firstname);
       
		
		$this->addParamItem('firstname',$firstname);
		
        
		$usernameuppercase=$this->getContainer()->get('badiu.system.access.session')->getValue('badiu.system.core.param.config.usernameuppercase');
		if($usernameuppercase){
			$this->addParamItem('firstname',mb_strtoupper($this->getParamItem('firstname'),'UTF-8'));
		}
		
		//lastname
		
		$lastname=$this->getParamItem('lastname');
		$lastname=trim($lastname);
		if(!empty($lastname)){
			
			$lastname=$userutil->cleanNameSpace($lastname);
			if($userutil->hasSpecialCharacter($lastname)){
				$message=array();
				$message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.lastname.invalidcharacter');
				$info='badiu.admin.selection.loginsingin.lastnamewithinvalidcharacter';
				return $this->getResponse()->denied($info, $message);
			}
		
			if($usernameuppercase){
				$lastname=mb_strtoupper($lastname,'UTF-8');
			}
			$this->addParamItem('lastname',$lastname);
		}
		
		
		
		//alternatename
		$enablealternatename=$this->getParamItem('enablealternatename');
		if(!$enablealternatename){$this->addParamItem('alternatename',null);}
		
		$alternatename=$this->getParamItem('alternatename');
		if(empty($alternatename)){return null;}
		
		$alternatename=$userutil->cleanNameSpace($alternatename);
		if($userutil->hasSpecialCharacter($alternatename)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.alternatename.invalidcharacter');
            $info='badiu.admin.selection.loginsingin.alternatenamewithinvalidcharacter';
            return $this->getResponse()->denied($info, $message);
        }
		
		if($usernameuppercase){
			$alternatename=mb_strtoupper($alternatename,'UTF-8');
		}
		$this->addParamItem('alternatename',$alternatename);
        return null;
    } 
	
    
     function  getForm(){
       $newparam=$this->getParam();
       //document
        $newparam['doctype']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield1');
        $newparam['docnumber']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield2');
       
        $newparam['entity']=$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
        $newparam['deleted']=FALSE; 
        $newparam['confirmed']=TRUE;
        
         unset($newparam['document']);
         unset($newparam['document_badiuchoicetextfield1']);
         unset($newparam['document_badiuchoicetextfield2']);
         
         //password
         $password=$this->getUtildata()->getVaueOfArray($this->getParam(), 'password');
        if(!empty($password)){
            $password=md5($password);
            $newparam['password']=$password;
        }else{unset($newparam['password']);}
         
         
        $this->setParam($newparam);
      
   }
   
    function  setForm(){
       
            $param = $this->getParam();
             //doc
            $docconf=array();
            $docconf['field1'] = $this->getUtildata()->getVaueOfArray($this->getParam(), 'doctype');
            $docconf['field2'] =  $this->getUtildata()->getVaueOfArray($this->getParam(), 'docnumber');
            $param['document'] =  $docconf;
            
            //password
            $param['password'] = "";
            $this->setParam($param);
   }
}
