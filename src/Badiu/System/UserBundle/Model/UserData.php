<?php

namespace  Badiu\System\UserBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class UserData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }


public function delete($param) {
			$id=null;
			$entity=null;
			if(isset($param['id'])){$id=$param['id'];}
			if(isset($param['entity'])){$entity=$param['entity'];}
			
			if(empty($id)){return null;}
			if(empty($entity)){return null;}
			$hash=$this->deleteHash($id);
			
			$dto=$this->getGlobalColumnsValue('o.docnumber,o.email,o.username,o.idnumber,o.shortname',array('id'=>$id));
			
			$fparam=array('id'=>$id,'deleted'=>1,'timemodified'=>new \DateTime(),'entity'=>$entity);
			if(isset($dto['docnumber']) && !empty($dto['docnumber'])){
				$docnumber=$dto['docnumber'];
				$docnumber.=$hash;
				$fparam['docnumber']=$docnumber;
			}
			
			if(isset($dto['email']) && !empty($dto['email'])){
				$email=$dto['email'];
				$email.=$hash;
				$fparam['email']=$email;
			}
			
			if(isset($dto['username']) && !empty($dto['username'])){
				$username=$dto['username'];
				$username.=$hash;
				$fparam['username']=$username;
			}
			
			if(isset($dto['idnumber']) && !empty($dto['idnumber'])){
				$idnumber=$dto['idnumber'];
				$idnumber.=$hash;
				$fparam['idnumber']=$idnumber;
			}
			
			if(isset($dto['shortname']) && !empty($dto['shortname'])){
				$shortname=$dto['shortname'];
				$shortname.=$hash;
				$fparam['shortname']=$shortname;
			}
			
			$result=$this->updateNativeSql($fparam,false);
            
            return $result;
     }
	 
	  public function restore($param) {
			$id=null;
			$entity=null;
			if(isset($param['id'])){$id=$param['id'];}
			if(isset($param['entity'])){$entity=$param['entity'];}
			
			if(empty($id)){return null;}
			if(empty($entity)){return null;}
			
			$fparam=array('id'=>$id,'deleted'=>0,'timemodified'=>new \DateTime(),'entity'=>$entity);
			
			$dto=$this->getGlobalColumnsValue('o.doctype,o.docnumber,o.email,o.username,o.idnumber,o.shortname',array('id'=>$id));
			
			$fparam=array('id'=>$id,'deleted'=>1,'timemodified'=>new \DateTime(),'entity'=>$entity);
			
			
			if(isset($dto['docnumber']) && !empty($dto['docnumber'])){
				$docnumber=$dto['docnumber'];
				$doctype=null;
				if(isset($dto['doctype'])){$doctype=$dto['doctype'];}
				$docnumberwithouthash=$this->getDataWithoutDeleteHash($docnumber);
				if($docnumberwithouthash!=$docnumber){
					$dparam=array('doctype'=>$doctype);
					$exist=$this->existColumnEdit($id,$entity,'docnumber',$docnumberwithouthash,$dparam);
					if(!$exist){$fparam['docnumber']=$docnumberwithouthash;}
				}
			}
			
			if(isset($dto['email']) && !empty($dto['email'])){
				$email=$dto['email'];
				$emailwithouthash=$this->getDataWithoutDeleteHash($email);
				if($emailwithouthash!=$email){
					$exist=$this->existColumnEdit($id,$entity,'email',$emailwithouthash);
					if(!$exist){$fparam['email']=$emailwithouthash;}
				}
			}
			
			if(isset($dto['username']) && !empty($dto['username'])){
				$username=$dto['username'];
				$usernamewithouthash=$this->getDataWithoutDeleteHash($username);
				if($usernamewithouthash!=$username){
					$exist=$this->existColumnEdit($id,$entity,'username',$usernamewithouthash);
					if(!$exist){$fparam['username']=$usernamewithouthash;}
				}
			}
			
			if(isset($dto['idnumber']) && !empty($dto['idnumber'])){
				$idnumber=$dto['idnumber'];
				$idnumberwithouthash=$this->getDataWithoutDeleteHash($idnumber);
				if($idnumberwithouthash!=$idnumber){
					$exist=$this->existColumnEdit($id,$entity,'idnumber',$idnumberwithouthash);
					if(!$exist){$fparam['idnumber']=$idnumberwithouthash;}
				}
			}
			if(isset($dto['shortname']) && !empty($dto['shortname'])){
				$shortname=$dto['shortname'];
				$shortnamewithouthash=$this->getDataWithoutDeleteHash($shortname);
				if($shortnamewithouthash!=$shortname){
					$exist=$this->existColumnEdit($id,$entity,'shortname',$shortnamewithouthash);
					if(!$exist){$fparam['shortname']=$shortnamewithouthash;}
				}
			}
		// print_r($fparam);exit;
			
			$result=$this->updateNativeSql($fparam,false);
            
            return $result;
     }
    public function getMenu($id) {
       $sql="SELECT  o.id,o.firstname,o.lastname FROM ".$this->getBundleEntity()." o  WHERE o.id = :id";
       $query = $this->getEm()->createQuery($sql);
       $query->setParameter('id',$id);
       $result= $query->getSingleResult();
       $name='';
       if(isset($result['firstname'])){$name=$result['firstname'];}
       if(isset($result['lastname'])){$name.= ' '.$result['lastname'];}
       $result['name']=$name;
       return  $result;
    } 
 public function getFormChoice($entity,$param=array(),$orderby="") {
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id, CONCAT(o.firstname,' ',o.lastname) AS name FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $wsql $orderby";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return  $result;
    }	
	public function getNameById($id) {
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
		$sql="SELECT  o.firstname,o.lastname FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.id = :id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		 $query->setParameter('id',$id);
        $result= $query->getSingleResult();;
        $result=$result['firstname']. ' '.$result['lastname'] ;
      return  $result;
    } 
    
    public function existDocnumber($doctype,$docnumber,$entity=null) {
            if(empty($entity)){
                	$badiuSession=$this->getContainer()->get('badiu.system.access.session');
                    $entity=$badiuSession->get()->getEntity();
            }
	
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.doctype = :doctype AND o.docnumber = :docnumber";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query->setParameter('doctype',$doctype);
            $query->setParameter('docnumber',$docnumber);
            $result= $query->getSingleResult();;
            $result=$result['countrecord'] ;
            return  $result;
    } 
     public function getIdByDocnumber($doctype,$docnumber,$entity=null) {
            if(empty($entity)){
                	$badiuSession=$this->getContainer()->get('badiu.system.access.session');
                    $entity=$badiuSession->get()->getEntity();
            }
	
            $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.doctype = :doctype AND o.docnumber = :docnumber";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query->setParameter('doctype',$doctype);
            $query->setParameter('docnumber',$docnumber);
            $result= $query->getSingleResult();;
            $result=$result['id'] ;
            return  $result;
    } 
    public function getByDocnumber($doctype,$docnumber,$entity=null) {
            if(empty($entity)){
                	$badiuSession=$this->getContainer()->get('badiu.system.access.session');
                    $entity=$badiuSession->get()->getEntity();
            }
	
            $sql="SELECT  o.id,o.ukey,o.firstname,o.lastname,o.email,o.username,o.idnumber,o.doctype,o.docnumber,o.nationalitystatus FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.doctype = :doctype AND o.docnumber = :docnumber";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query->setParameter('doctype',$doctype);
            $query->setParameter('docnumber',$docnumber);
            $result= $query->getSingleResult();
             return  $result;
    } 
    
    public function getByLogin($username,$password,$entity=null) {
            if(empty($entity)){
                	$badiuSession=$this->getContainer()->get('badiu.system.access.session');
                    $entity=$badiuSession->get()->getEntity();
            }
	
            $sql="SELECT  o.id,o.ukey,o.firstname,o.lastname,o.email,o.username,o.idnumber,o.doctype,o.docnumber,o.nationalitystatus FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.username = :username AND o.password = :password";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query->setParameter('username',$username);
            $query->setParameter('password',md5($password));
            $result= $query->getSingleResult();
             return  $result;
    }
    public function validatePasswordById($id,$password) {
          
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.id = :id AND o.password = :password";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $query->setParameter('password',md5($password));
            $result= $query->getOneOrNullResult();
            $valid=false;
         
            if(isset($result['countrecord']) && $result['countrecord'] ==1){ $valid=true;}
            return  $valid;
    }
     public function getInfoByUsername($username,$entity=null) {
            if(empty($entity)){
                	$badiuSession=$this->getContainer()->get('badiu.system.access.session');
                    $entity=$badiuSession->get()->getEntity();
            }
	
            $sql="SELECT  o.id,o.ukey,o.firstname,o.lastname,o.email,o.idnumber,o.doctype,o.docnumber,o.nationalitystatus FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.username = :username ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query->setParameter('username',$username);
            $result= $query->getSingleResult();
             return  $result;
    }
    public function getInfoById($id) {
            $sql="SELECT  o.id,o.firstname,o.lastname,o.email,o.idnumber,o.doctype,o.docnumber,o.nationalitystatus,o.contactdata,o.documentdata,o.param,o.entity FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
            return  $result;
    }
    public function getMenuAddress($id) {
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit=$sysoperation->isEdit();
        
        if($isEdit ){
			$adata=$this->getContainer()->get('badiu.util.address.data.data');
			$userid=$adata->getModuleinstance($id);
            if(!empty($userid))  return  $this->getNameById($userid);
            else return  'user not found';
         }else{
           return  $this->getNameById($id);
        }
       
       
    }
    
    public function getMenuTelephone($id) {
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit=$sysoperation->isEdit();
        
        if($isEdit ){
			$adata=$this->getContainer()->get('badiu.util.address.telephone.data');
			$userid=$adata->getModuleinstance($id);
            if(!empty($userid))  return  $this->getNameById($userid);
            else return  'user not found';
         }else{
           return  $this->getNameById($id);
        }
       
       
    }
	public function getMenuDocument($id) {
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit=$sysoperation->isEdit();
        
        if($isEdit ){
			$adata=$this->getContainer()->get('badiu.util.document.data.data');
			$userid=$adata->getModuleinstance($id);
            if(!empty($userid))  return  $this->getNameById($userid);
            else return  'user not found';
         }else{
           return  $this->getNameById($id);
        }
       
       
    }
    //reviw it shout not have param $moduleinstanceextid. The function existIdByExteralid just do it . Check this function on sync moodle
	public function existModule($entity,$modulekey,$moduleinstance,$moduleinstanceextid) {
		$r=FALSE;
		$sql="SELECT   COUNT(o.id) AS countrecord  FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.moduleinstanceextid = :moduleinstanceextid  AND o.modulekey = :modulekey  AND o.moduleinstance = :moduleinstance  ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('moduleinstanceextid',$moduleinstanceextid);
		$query->setParameter('modulekey',$modulekey);
		$query->setParameter('moduleinstance',$moduleinstance);
        $result= $query->getSingleResult();;
        if($result['countrecord']>0){$r=TRUE;}
        return $r;
      
    } 
     //reviw it shout not have param $moduleinstanceextid. The function getIdByExteralid just do it. Check this function on sync moodle
	public function getIdByModule($entity,$modulekey,$moduleinstance,$moduleinstanceextid) {
		if(!$this->existModule($entity,$modulekey,$moduleinstance,$moduleinstanceextid)){return null; }
		$sql="SELECT   o.id FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.moduleinstanceextid = :moduleinstanceextid  AND o.modulekey = :modulekey  AND o.moduleinstance = :moduleinstance  ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('moduleinstanceextid',$moduleinstanceextid);
		$query->setParameter('modulekey',$modulekey);
		$query->setParameter('moduleinstance',$moduleinstance);
        $result= $query->getSingleResult();;
		$result= $result['id'];
        return $result;
      
    } 
   
	public function findByModule($entity,$modulekey,$moduleinstance,$moduleinstanceextid) {
		$sql="SELECT   o  FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.moduleinstanceextid = :moduleinstanceextid  AND o.modulekey = :modulekey  AND o.moduleinstance = :moduleinstance  ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('moduleinstanceextid',$moduleinstanceextid);
		$query->setParameter('modulekey',$modulekey);
		$query->setParameter('moduleinstance',$moduleinstance);
        $result= $query->getSingleResult();;
        return  $result;
    } 
    
      
    public function existByExteralid($param) {
            $modulekey=null;
            $moduleinstance=null;
            $moduleinstanceextid=null;
            $entity=null;
            
            if(isset($param['modulekey'])){$modulekey=$param['modulekey'];}
            if(isset($param['moduleinstance'])){$moduleinstance=$param['moduleinstance'];}
            if(isset($param['moduleinstanceextid'])){$moduleinstanceextid=$param['moduleinstanceextid'];}
            if(isset($param['entity'])){$entity=$param['entity'];}
            if(empty($entity)){
                	$badiuSession=$this->getContainer()->get('badiu.system.access.session');
                    $entity=$badiuSession->get()->getEntity();
            }
	
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.modulekey = :modulekey AND o.moduleinstance = :moduleinstance  AND o.moduleinstanceextid = :moduleinstanceextid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query->setParameter('modulekey',$modulekey);
            $query->setParameter('moduleinstance',$moduleinstance);
            $query->setParameter('moduleinstanceextid',$moduleinstanceextid);
            $result= $query->getSingleResult();
            $result=$result['countrecord'] ;
            return  $result;
    } 
    
     public function getIdByExteralid($param) {
            $modulekey=null;
            $moduleinstance=null;
            $moduleinstanceextid=null;
            $entity=null;
            
            if(isset($param['modulekey'])){$modulekey=$param['modulekey'];}
            if(isset($param['moduleinstance'])){$moduleinstance=$param['moduleinstance'];}
            if(isset($param['moduleinstanceextid'])){$moduleinstanceextid=$param['moduleinstanceextid'];}
            if(isset($param['entity'])){$entity=$param['entity'];}
            if(empty($entity)){
                	$badiuSession=$this->getContainer()->get('badiu.system.access.session');
                    $entity=$badiuSession->get()->getEntity();
            }
	
            $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.modulekey = :modulekey AND o.moduleinstance = :moduleinstance  AND o.moduleinstanceextid = :moduleinstanceextid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query->setParameter('modulekey',$modulekey);
            $query->setParameter('moduleinstance',$moduleinstance);
            $query->setParameter('moduleinstanceextid',$moduleinstanceextid);
            $result= $query->getSingleResult();
           $result=$result['id'] ;
            return  $result;
    } 
	
	public function existUsername($entity,$username) {
		$r=FALSE;
		$sql="SELECT   COUNT(o.id) AS countrecord  FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.username = :username  ";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('entity',$entity);
		$query->setParameter('username',$username);
		$result= $query->getSingleResult();;
                if($result['countrecord']>0){$r=TRUE;}
         return $r;
      
    } 
    
    public function findByUsername($entity,$username) {
		$r=FALSE;
		$sql="SELECT   o FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.username = :username  ";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('entity',$entity);
		$query->setParameter('username',$username);
		$result= $query->getSingleResult();;
                return $result;
      
    } 
     public function getInfoByUsernameForLogin($username) {
		$r=FALSE;
		$sql="SELECT   o.id,o.entity, CONCAT(o.firstname,' ',o.lastname) AS name,o.email,o.username FROM ".$this->getBundleEntity()." o  WHERE o.username = :username  ";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('username',$username);
		$result= $query->getOneOrNullResult();
                return $result;
      
    }
    public function getInfoByEmailForLogin($entity,$email) {
		$r=FALSE;
		$sql="SELECT  o.id,o.entity, CONCAT(o.firstname,' ',o.lastname) AS name,o.email,o.username FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity   AND o.email = :email  ";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('entity',$entity);
		$query->setParameter('email',$email);
		$result= $query->getOneOrNullResult();
                return $result;
      
    }
 
   public function getSynclogById($id) {
	       $sql="SELECT  o.synclog FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id',$id);
		$result= $query->getOneOrNullResult();
        if(!empty($result)){$result=$result['synclog'];}
        return $result;
      
    }
    
    
    public function getForAutocomplete() {
         $name=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
         $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        
        $wsql="";
        if(!empty($name)){
            $wsql=" AND LOWER(CONCAT(o.id,o.firstname,o.email,o.username))  LIKE :name ";
            $name=strtolower($name);
        }
         $sql="SELECT DISTINCT o.id,CONCAT(o.firstname,' ',o.lastname,' ',o.email) AS name FROM ".$this->getBundleEntity()." o WHERE  o.entity = :entity  $wsql ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(50);
        $result= $query->getResult();
        return  $result;
    }
  public function getNameByIdOnEditAutocomplete($id) {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT CONCAT(o.firstname,' ',o.lastname,' ',o.email) AS name FROM ".$this->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }

  public function getInfoForSync($id) {
    $badiuSession=$this->getContainer()->get('badiu.system.access.session');
    $sql="SELECT o.id,o.firstname,o.lastname,o.shortname,o.email,o.username,o.idnumber,o.doctype,o.docnumber,o.documentdata,o.contactdata,o.statusregister,o.dateofbirth,o.nationalitystatus FROM ".$this->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
    $query = $this->getEm()->createQuery($sql);
    $query->setParameter('entity',$badiuSession->get()->getEntity());
    $query->setParameter('id',$id);
    $result= $query->getOneOrNullResult();
    return $result;
}

  public function getIdsByMarker($enity,$marker) {
    $sql="SELECT o.id FROM ".$this->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.marker LIKE :marker ";
    $query = $this->getEm()->createQuery($sql);
    $query->setParameter('entity',$enity);
    $query->setParameter('marker',"%$marker%");
    $result= $query->getResult();
    return $result;
}

 public function changeRegisterUpadtetimeForAll($entity,$newdate) {
            if(empty($newdate)){return null;}
			if(empty($entity)){return null;}
             $sql="UPDATE  ".$this->getBundleEntity()." o SET o.registernextupadtetime=:registernextupadtetime  WHERE o.entity=:entity";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('registernextupadtetime', $newdate);
             $query->setParameter('entity', $entity);
            $result=$query->execute();
            return $result;
       }
}
