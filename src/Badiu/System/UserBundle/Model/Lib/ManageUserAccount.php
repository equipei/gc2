<?php

namespace Badiu\System\UserBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ManageUserAccount extends BadiuModelLib {
	
	private $param;
	
    function __construct(Container $container) {
               parent::__construct($container);
    }

    public function login($param){
			$entity=$this->getEntity();
			$username=$this->getUtildata()->getVaueOfArray($param,'username');
			$password=$this->getUtildata()->getVaueOfArray($param,'password');
			
			//review critica
			//username
          //  if(empty($username)){return $this->getResponse()->denied("badiu.system.user.saccount.username.is.empty");}
            
			//password
          //  if(empty($password)){return $this->getResponse()->denied("badiu.system.user.saccount.password.is.empty");}
            $data=$this->getContainer()->get('badiu.system.user.user.data');        
            $ckparm=array('username'=>$username,'password'=>md5($password),'entity'=>$entity);
            $result=$data->countNativeSql($ckparm,false);
            
           return $result;
               	
	}

	public function getInfoByUsername($param){
			$entity=$this->getEntity();
			$username=$this->getUtildata()->getVaueOfArray($param,'username');
		                    
            $data=$this->getContainer()->get('badiu.system.user.user.data');
            
			//review critica
			//username
            // if(empty($username)){return $this->getResponse()->denied("badiu.system.user.saccount.username.is.empty");}
                    
            $ckparm=array('username'=>$username,'entity'=>$entity);
            $exist=$data->countNativeSql($ckparm,false);
            if(!$exist){return $this->getResponse()->denied("badiu.system.user.saccount.username.not.found");}
            $result=$data->getInfoByUsername($username,$entity);
             return $result;
      }  
}
