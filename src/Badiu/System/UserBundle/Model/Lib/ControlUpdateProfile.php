<?php

namespace Badiu\System\UserBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;


class ControlUpdateProfile extends BadiuModelLib
{
    
    function __construct(Container $container)
    {
        parent::__construct($container);
    }
   
   
    public function requireUpdate($param) {
		$urlgoback=$this->getUtildata()->getVaueOfArray($param,'urlgoback');
		$outurl=$this->getUtildata()->getVaueOfArray($param,'outurl');
		return null;
		//check profile
		$userdb=$this->getContainer()->get('badiu.system.user.user.data');
		$userid=$this->getContainer()->get('badiu.system.access.session')->get()->getUser()->getId();
		$param=array('id'=>$userid,'entity'=>$this->getEntity());
        $pdto=$userdb->getGlobalColumnsValue('o.statusregister,o.registernextupadtetime',$param);
	
		$statusregister=$this->getUtildata()->getVaueOfArray($pdto,'statusregister');
		$registernextupadtetime=$this->getUtildata()->getVaueOfArray($pdto,'registernextupadtetime');
		
		$tryupdate=false;
		if($statusregister!='complete'){$tryupdate=true;}
		if(!$tryupdate && !empty($registernextupadtetime) && is_a($registernextupadtetime, 'DateTime')){
			$now=new \DateTime();
			if($registernextupadtetime->getTimestamp() < $now->getTimestamp()){$tryupdate=true;}
		}
		if($tryupdate){
			//redirect
			$param=array('_urlgoback'=>$urlgoback);
			$url=$this->getUtilapp()->getUrlByRoute('badiu.admin.client.changedefaultprofile.edit',$param);
			if($outurl){return $url; }
			header('Location: '.$url);
             exit;
		}
		
		return null;
	}
    
    
}
