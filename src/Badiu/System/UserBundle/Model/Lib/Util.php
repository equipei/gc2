<?php

namespace Badiu\System\UserBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;


class Util extends BadiuModelLib
{
    
    function __construct(Container $container)
    {
        parent::__construct($container);
    }
    
	function hasSpecialCharacter($name){
		$name= mb_strtoupper($name,'UTF-8');
		return preg_match('/[\£$%&*()}{@#?><>,;!":\[|=_+¬01234567\]\/\\\89]|ã/',  $name);
	}
	function cleanNameSpace($name){
		return str_replace('  ', ' ', $name);
    }

    function nameHasLastname($name){
        if(empty($name)){return false;}
        $name=trim($name);
        $name=str_replace('  ',' ',$name);
        $pos=stripos($name, ' ');
        if($pos=== false){return false;}
        $p=explode(" ",$name);
        if(sizeof($p) > 1 ){
            return true;
        }
        return false;
    }
    
    function splitFirtnameLastname($name){
         $people= new \stdClass();
         $people->firtname=$name;
         $people->lastname=null;
       
         $name=trim($name);
         $name=str_replace('  ',' ',$name);
         $pos=stripos($name, ' ');
         if($pos=== false){return $people;}
        
         if(!$this->nameHasLastname($name)){
             return $people;
         }
         $p=explode(" ",$name);
         if(sizeof($p) == 2){
             $people->firtname=$p[0];
             $people->lastname=$p[1];
         }else if(sizeof($p) >=3){
             $people->firtname=$p[0];
             $people->firtname.= ' '.$p[1];
             $cont=0;
             $people->lastname='';
             foreach ($p as $n) {
                 $cont++;
                 if($cont >2){
                     $people->lastname.= ' '.$n;
                 }
             }
         }
        
        return $people;
    }
    
	function partialEmailIdentify($email){
		$p = explode("@",$email);
		$p1=$this->getUtildata()->getVaueOfArray($p,0);
		$len=strlen($p1);
		$avg=0;
		if($len >2 ){$avg=$len/2;}
		
		if($avg){
			$strpl="";
			for($i=0;$i<=$avg;$i++){$strpl.="*";}
			$email=substr_replace($email,$strpl,0,$avg);
		}
		return $email;
	}
    
}
