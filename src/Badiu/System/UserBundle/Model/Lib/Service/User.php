<?php

namespace  Badiu\System\UserBundle\Model\Lib\Service;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
class User extends BadiuExternalService{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
	public function existByModule(){
		//check token
		$ckeckToken=$this->checkToken();
		if(!$ckeckToken){return $this->getResponse()->get();}
		
		//get param
		$dto=$this->getParamModule();
		$chekParam=$this->checkParamModule($dto);
		if(!$chekParam){return $this->getResponse()->get();}
		$data=$this->getContainer()->get('badiu.system.user.user.data');
		$exist=$data->existModule($this->getEntity(),'badiu.admin.server.service',$this->getServiceid(),$dto->moduleinstanceextid);
		$resp="false";
		if($exist){$resp="true";}
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($resp);
		return $this->getResponse()->get();	
		
	}
	
	public function getIdByModule(){
		//check token
		$ckeckToken=$this->checkToken();
		if(!$ckeckToken){return $this->getResponse()->get();}
		
		//get param
		$dto=$this->getParamModule();
		$chekParam=$this->checkParamModule($dto);
		if(!$chekParam){return $this->getResponse()->get();}
		$data=$this->getContainer()->get('badiu.system.user.user.data');
		$id=$data->getIdByModule($this->getEntity(),'badiu.admin.server.service',$this->getServiceid(),$dto->moduleinstanceextid);
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($id);
		return $this->getResponse()->get();	
		
	}
	public function save(){
		//check token
		$ckeckToken=$this->checkToken();
		if(!$ckeckToken){return $this->getResponse()->get();}
		
		//get param
		$dto=$this->getParam();
		$chekParam=$this->checkParam($dto);
		if(!$chekParam){return $this->getResponse()->get();}
		
		$data=$this->getContainer()->get('badiu.system.user.user.data');
		$exist=$data->existModule($this->getEntity(),$dto->getModulekey(),$dto->getModuleinstance(),$dto->getModuleinstanceextid());
		if($exist){
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.system.user.user.already.exists');
			return $this->getResponse()->get();
		}
		
		//check login duplication
		if($data->existUsername($this->getEntity(),$dto->getUsername())){
			$prefix=$dto->getModuleinstance().'-'.$dto->getModuleinstanceextid();
			$dto->setUsername($dto->getUsername().$prefix);
		}
		$data->setDto($dto);
		$result=$data->save();
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($data->getDto()->getId());
		return $this->getResponse()->get();	
	}
	public function update(){
	
	}
	
	public function getParam(){
		$dto=$this->getContainer()->get('badiu.system.user.user.entity');
		$dto=$this->initDefaultEntityData($dto);
		$dto->setFirstname($this->getContainer()->get('request')->get('firstname'));
		$dto->setLastname($this->getContainer()->get('request')->get('lastname'));
		$dto->setEmail($this->getContainer()->get('request')->get('email'));
		$dto->setUsername($this->getContainer()->get('request')->get('username'));
		$dto->setModulekey('badiu.admin.server.service');
		$dto->setModuleinstance($this->getServiceid());
		$dto->setModuleinstanceextid($this->getContainer()->get('request')->get('moduleinstanceextid'));
		return $dto;
	}
	
	public function checkParam($dto){
		$resp=TRUE;
		if(empty($dto->getFirstname())) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.system.user.user.firstname.is.empty');
		}
		if(empty($dto->getUsername())) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.system.user.user.username.is.empty');
		}
		if(empty($dto->getModuleinstanceextid())) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.system.user.user.moduleinstanceextid.is.empty');
		}
		return $resp;
	}
	
	public function getParamModule(){
		$param= new \stdClass();
		$param->moduleinstanceextid=$this->getContainer()->get('request')->get('moduleinstanceextid');
		
		return $param;
	}
	
	public function checkParamModule($param){
		$resp=TRUE;
		
		if(empty($param->moduleinstanceextid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.system.user.user.moduleinstanceextid.is.empty');
		}
		return $resp;
	}
}
