<?php

namespace  Badiu\System\UserBundle\Model\Lib\Service;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
class Saccount extends BadiuExternalService{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
      //{"_service": "badiu.system.user.saccount.service","_function": "save","_token": "13|6nkIRvDbj9cMZfW0H6MA12k4e4Jl9Z|2|mreport|19"}
	//http://fabrica.badiu.com.br/~colaborador7/badiunet/web/app_dev.php/system/service/process?_service=badiu.system.user.saccount.service&_function=save&_token=13|6nkIRvDbj9cMZfW0H6MA12k4e4Jl9Z|2|mreport|19
	public function save(){
		//check token
                try {
                    $ckeckToken=$this->checkToken();
                    if(!$ckeckToken){return $this->getResponse()->get();}
		
                    $qparam=$this->getHttpQuerStringParam();
                    
                
                    $data=$this->getContainer()->get('badiu.system.user.user.data');
                    $entity=null;
                    $doctype=null;
                    $docnumber=null;
                    $firstname=null;
                    $lastname='';
                    $email=null;
                    $username=null;
                    $password=null;
                    $nationalitystatus=null;
                
                    if(array_key_exists('entity',$qparam)){$entity=$qparam['entity'];}
                    if(array_key_exists('doctype',$qparam)){$doctype=$qparam['doctype'];}
                    if(array_key_exists('docnumber',$qparam)){$docnumber=$qparam['docnumber'];}
                    if(array_key_exists('firstname',$qparam)){$firstname=$qparam['firstname'];}
                    if(array_key_exists('lastname',$qparam)){$lastname=$qparam['lastname'];}
                    if(array_key_exists('email',$qparam)){$email=$qparam['email'];}
                    if(array_key_exists('username',$qparam)){$username=$qparam['username'];}
                    if(array_key_exists('password',$qparam)){$password=$qparam['password'];}
                    if(array_key_exists('nationalitystatus',$qparam)){$nationalitystatus=$qparam['nationalitystatus'];}
               
                    //doctype | docnumber
                    if(empty($doctype)){return $this->getResponse()->denied("badiu.system.user.saccount.doctype.is.empty");}
                    if(empty($docnumber)){return $this->getResponse()->denied("badiu.system.user.saccount.docnumber.is.empty");}
                    $ckparm=array('doctype'=>$doctype,'docnumber'=>$docnumber,'entity'=>$entity);
                    $exist=$data->countNativeSql($ckparm,false);
                    if($exist){return $this->getResponse()->denied("badiu.system.user.saccount.docnumber.just.exist");}
                    
                    //username
                    if(empty($username)){return $this->getResponse()->denied("badiu.system.user.saccount.username.is.empty");}
                    $ckparm=array('username'=>$username,'entity'=>$entity);
                    $exist=$data->countNativeSql($ckparm,false);
                    if($exist){return $this->getResponse()->denied("badiu.system.user.saccount.username.just.exist");}
                    
                    //password
                    if(empty($password)){return $this->getResponse()->denied("badiu.system.user.saccount.password.is.empty");}
                   
                    //firstname
                    if(empty($firstname)){return $this->getResponse()->denied("badiu.system.user.saccount.firstname.is.empty");}
                   
                     //lastname
                    if(empty($lastname)){$lastname=" ";}
                   
                    //email
                    if(empty($email)){return $this->getResponse()->denied("badiu.system.user.saccount.email.is.empty");}
                    if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {return $this->getResponse()->denied("badiu.system.user.saccount.email.is.notvalid");}
                    $ckparm=array('email'=>$email,'entity'=>$entity);
                    $exist=$data->countNativeSql($ckparm,false);
                    if($exist){return $this->getResponse()->denied("badiu.system.user.saccount.email.just.exist");}
                   
                   
                    //nationalitystatus
                    if(empty($nationalitystatus)){return $this->getResponse()->denied("badiu.system.user.saccount.nationalitystatus.is.empty");}
                    if($nationalitystatus!='native' && $nationalitystatus!='resident' && $nationalitystatus!='foreign' ){return $this->getResponse()->denied("badiu.system.user.saccount.nationalitystatus.has.nodefaultvalue","default value should be: native,resident or foreign ");}
                    
                     //insert
                    $iparam=array(
                        'entity'=>$entity,
                        'doctype'=>$doctype,
                        'docnumber'=>$docnumber,
                        'firstname'=>$firstname,
                        'lastname'=>$lastname,
                        'email'=>$email,
                        'username'=>$username,
                        'password'=>md5($password),
                        'nationalitystatus'=>$nationalitystatus,
                        'auth'=>'manual',
                        'confirmed'=>1,
                        'deleted'=>0,
                        'timecreated'=>new \DateTime()
                      );
                    $id=$data->insertNativeSql($iparam,false);
                    
                    //genarate ukey
                    $basekey="|$id|$entity";
                    $lenth=strlen($basekey);
                    $hash=$this->getContainer()->get('badiu.system.core.lib.util.hash');
                    $lenth=40-$lenth;
                   
		    $ukey=$hash->make($lenth);
                    $ukey=$ukey.$basekey;
                    $uparam=array('ukey'=>$ukey,'id'=>$id);
                    $result=$data->updateNativeSql($uparam,false);
                    if($result){
                        $rparam=array('id'=>$id,'ukey'=>$ukey);
                        return $this->getResponse()->accept($rparam);
                    }else{
                         return $this->getResponse()->accept(array('id'=>$id));
                    }
                } catch (Exception $ex) {
                  
                    return $this->getResponse()->denied("badiu.system.error.general",  $ex);
                }
		
             	
	}
	
	public function existDocument(){
		//check token
                try {
                    $ckeckToken=$this->checkToken();
                    if(!$ckeckToken){return $this->getResponse()->get();}
		
                    $qparam=$this->getHttpQuerStringParam();
                    
                
                    $data=$this->getContainer()->get('badiu.system.user.user.data');
                    $entity=null;
                    $doctype=null;
                    $docnumber=null;
                   
                    if(array_key_exists('entity',$qparam)){$entity=$qparam['entity'];}
                    if(array_key_exists('doctype',$qparam)){$doctype=$qparam['doctype'];}
                    if(array_key_exists('docnumber',$qparam)){$docnumber=$qparam['docnumber'];}
                   
                    //doctype | docnumber
                    if(empty($doctype)){return $this->getResponse()->denied("badiu.system.user.saccount.doctype.is.empty");}
                    if(empty($docnumber)){return $this->getResponse()->denied("badiu.system.user.saccount.docnumber.is.empty");}
                    $ckparm=array('doctype'=>$doctype,'docnumber'=>$docnumber,'entity'=>$entity);
                    $exist=$data->countNativeSql($ckparm,false);
                    $result="false";
                    if($exist){$result="true";}
                   return $this->getResponse()->accept($result);
                   
                } catch (Exception $ex) {
                  
                    return $this->getResponse()->denied("badiu.system.error.general",  $ex);
                }
		
             	
	}
        
       public function getByDocument(){
             
		//check token
                try {
                    $ckeckToken=$this->checkToken();
                    if(!$ckeckToken){return $this->getResponse()->get();}
		
                    $qparam=$this->getHttpQuerStringParam();
                    
                
                    $data=$this->getContainer()->get('badiu.system.user.user.data');
                    $entity=null;
                    $doctype=null;
                    $docnumber=null;
                   
                    if(array_key_exists('entity',$qparam)){$entity=$qparam['entity'];}
                    if(array_key_exists('doctype',$qparam)){$doctype=$qparam['doctype'];}
                    if(array_key_exists('docnumber',$qparam)){$docnumber=$qparam['docnumber'];}
                   
                    //doctype | docnumber
                    if(empty($doctype)){return $this->getResponse()->denied("badiu.system.user.saccount.doctype.is.empty");}
                    if(empty($docnumber)){return $this->getResponse()->denied("badiu.system.user.saccount.docnumber.is.empty");}
                    $ckparm=array('doctype'=>$doctype,'docnumber'=>$docnumber,'entity'=>$entity);
                    $exist=$data->countNativeSql($ckparm,false);
                    if(!$exist){return $this->getResponse()->denied("badiu.system.user.saccount.docnumber.not.exist");}
                    $result=$data->getByDocnumber($doctype,$docnumber,$entity);
                    return $this->getResponse()->accept($result);
                   
                } catch (Exception $ex) {
                  
                    return $this->getResponse()->denied("badiu.system.error.general",  $ex);
                }
		
             	
	}
        
        public function login(){
		//check token
                try {
                    $ckeckToken=$this->checkToken();
                    if(!$ckeckToken){return $this->getResponse()->get();}
		
                    $qparam=$this->getHttpQuerStringParam();
                    
                
                    $data=$this->getContainer()->get('badiu.system.user.user.data');
                    $entity=null;
                    $username=null;
                    $password=null;
                  
                    if(array_key_exists('entity',$qparam)){$entity=$qparam['entity'];}
                    if(array_key_exists('username',$qparam)){$username=$qparam['username'];}
                    if(array_key_exists('password',$qparam)){$password=$qparam['password'];}
                    
                    //username
                    if(empty($username)){return $this->getResponse()->denied("badiu.system.user.saccount.username.is.empty");}
                    //password
                    if(empty($password)){return $this->getResponse()->denied("badiu.system.user.saccount.password.is.empty");}
                   
                    $ckparm=array('username'=>$username,'password'=>md5($password),'entity'=>$entity);
                    $exist=$data->countNativeSql($ckparm,false);
                                      
                    $result="false"; 
                    if($exist){$result="true";}
                   return $this->getResponse()->accept($result);
                   
                } catch (Exception $ex) {
                  
                    return $this->getResponse()->denied("badiu.system.error.general",  $ex);
                }
		
             	
	}
        
       public function getByLogin(){
		//check token
                try {
                    $ckeckToken=$this->checkToken();
                    if(!$ckeckToken){return $this->getResponse()->get();}
		
                    $qparam=$this->getHttpQuerStringParam();
                    
                
                    $data=$this->getContainer()->get('badiu.system.user.user.data');
                    $entity=null;
                    $username=null;
                    $password=null;
                  
                    if(array_key_exists('entity',$qparam)){$entity=$qparam['entity'];}
                    if(array_key_exists('username',$qparam)){$username=$qparam['username'];}
                    if(array_key_exists('password',$qparam)){$password=$qparam['password'];}
                    
                    //username
                    if(empty($username)){return $this->getResponse()->denied("badiu.system.user.saccount.username.is.empty");}
                    //password
                    if(empty($password)){return $this->getResponse()->denied("badiu.system.user.saccount.password.is.empty");}
                   
                    $ckparm=array('username'=>$username,'password'=>md5($password),'entity'=>$entity);
                    $exist=$data->countNativeSql($ckparm,false);
                    if(!$exist){return $this->getResponse()->denied("badiu.system.user.saccount.loginandpasswor.not.found");}
                    $result=$data->getByLogin($username,$password,$entity);
                    return $this->getResponse()->accept($result);
                   
                } catch (Exception $ex) {
                  
                    return $this->getResponse()->denied("badiu.system.error.general",  $ex);
                }
		
             	
	}
      public function getInfoByUsername(){
		//check token
                try {
                    $ckeckToken=$this->checkToken();
                    if(!$ckeckToken){return $this->getResponse()->get();}
		
                    $qparam=$this->getHttpQuerStringParam();
                    
                
                    $data=$this->getContainer()->get('badiu.system.user.user.data');
                    $entity=null;
                    $username=null;
                    
                    if(array_key_exists('entity',$qparam)){$entity=$qparam['entity'];}
                    if(array_key_exists('username',$qparam)){$username=$qparam['username'];}
                      
                    //username
                    if(empty($username)){return $this->getResponse()->denied("badiu.system.user.saccount.username.is.empty");}
                    
                    $ckparm=array('username'=>$username,'entity'=>$entity);
                    $exist=$data->countNativeSql($ckparm,false);
                    if(!$exist){return $this->getResponse()->denied("badiu.system.user.saccount.username.not.found");}
                    $result=$data->getInfoByUsername($username,$entity);
                    return $this->getResponse()->accept($result);
                   
                } catch (Exception $ex) {
                  
                    return $this->getResponse()->denied("badiu.system.error.general",  $ex);
                }
		
             	
	}  
       public function searchByName(){
		//check token
                try {
                    $ckeckToken=$this->checkToken();
                    if(!$ckeckToken){return $this->getResponse()->get();}
		
                    $qparam=$this->getHttpQuerStringParam();
                    
                
                    $udata=$this->getContainer()->get('badiu.system.user.user.data');
                    $edata=$this->getContainer()->get('badiu.system.user.user.data');
                    $entity=null;
                    $name=null;
                    $type="physical_person";
                  
                  
                    if(array_key_exists('entity',$qparam)){$entity=$qparam['entity'];}
                    if(array_key_exists('name',$qparam)){$name=$qparam['name'];}
                    if(array_key_exists('password',$qparam)){$password=$qparam['password'];}
                    
                    //username
                    if(empty($username)){return $this->getResponse()->denied("badiu.system.user.saccount.username.is.empty");}
                    //password
                    if(empty($password)){return $this->getResponse()->denied("badiu.system.user.saccount.password.is.empty");}
                   
                    $ckparm=array('username'=>$username,'password'=>md5($password),'entity'=>$entity);
                    $exist=$data->countNativeSql($ckparm,false);
                    if(!$exist){return $this->getResponse()->denied("badiu.system.user.saccount.loginandpasswor.not.found");}
                    $result=$data->getByLogin($username,$password,$entity);
                    return $this->getResponse()->accept($result);
                   
                } catch (Exception $ex) {
                  
                    return $this->getResponse()->denied("badiu.system.error.general",  $ex);
                }
		
             	
	}
}
