<?php

namespace  Badiu\System\UserBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class UserForm extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function getDataFormEdit($userid) {
			 $fdata=array();
			 $fdata=$this->getUserDb($userid,$fdata);
			 $fdata=$this->getAddressDb($userid,$fdata);
			 $fdata=$this->getTelephoneFixeResidenceDb($userid,$fdata);
			 $fdata=$this->getTelephoneFixeProfessionalDb($userid,$fdata);
			 $fdata=$this->getTelephoneMobile1Db($userid,$fdata);
			 $fdata=$this->getTelephoneMobile2Db($userid,$fdata);
			 
			 return $fdata;
	}
	public function save($data) {
			$sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
			$isEdit=$sysoperation->isEdit();
			$userid=null;
			if($isEdit){
				$userid=$this->getContainer()->get('request')->get('id');
			}
			$dto=$data->getDto();
			
			//save user
			$data->setDto($this->getUserForm($userid,$dto));
			$result=$data->save();
			$userid=$data->getDto()->getId();
		
			//save address
			$addressDto=$this->getAddressForm($userid,$dto,$isEdit);
			$addressData=$this->getContainer()->get('badiu.util.address.data.data');
			$addressData->setDto($addressDto);
			$addressData->save();
		
		
			//tel
			$telData=$this->getContainer()->get('badiu.util.address.telephone.data');
		
			//save telfixeresidence
			$telfixeresidenceDto=$this->getTelephoneFixeResidenceForm($userid,$dto,$isEdit);
			$telData->setDto($telfixeresidenceDto);
			$telData->save();
		
			//save telfixeprofessional
			$telfixeprofessionalDto=$this->getTelephoneFixeProfessionalForm($userid,$dto,$isEdit);
			$telData->setDto($telfixeprofessionalDto);
			$telData->save();
		
			//save telmobile
			$telmobile1Dto=$this->getTelephoneMobile1Form($userid,$dto,$isEdit);
			$telData->setDto($telmobile1Dto);
			$telData->save();
			
			$telmobile2Dto=$this->getTelephoneMobile2Form($userid,$dto,$isEdit);
			$telData->setDto($telmobile2Dto);
			$telData->save();
				
			return $data->getDto();
		}
	public function getUserDb($userid,$fdata) {
			 
			$data=$this->getContainer()->get('badiu.system.user.user.data');
			$dto=$data->findById($userid);
			$fdata['firstname']= $dto->getFirstname();
                        $fdata['lastname']= $dto->getLastname();
			$fdata['email']=$dto->getEmail();
			$fdata['username']=$dto->getUsername();
			$fdata['password']=$dto->getPassword();
			
                         $fdata['document']=array();        
                         $fdata['document']['field1']=$dto->getDoctype();
                         $fdata['document']['field2']=$dto->getDocnumber();
			
				
			$fdata['idnumber']=$dto->getIdnumber();
			$fdata['param']=$dto->getParam();
			$fdata['description']=$dto->getDescription();
			return $fdata;
		
	}
	
	public function getUserForm($userid,$fdata) {
			$dto=null;
			if(!empty($userid)){
				$data=$this->getContainer()->get('badiu.system.user.user.data');
				$dto=$data->findById($userid);
			}else{
				$dto=$this->getContainer()->get('badiu.system.user.user.entity');
				$dto=$this->initDefaultEntityData($dto);
			}
			
			
			if(isset($fdata['name'])) {
				$dto->setFirstname($fdata['name']);
				$dto->setLastname(' ');
			}
			if(isset($fdata['email'])) {$dto->setEmail($fdata['email']);}
			if(isset($fdata['lastname'])) {$dto->setLastname($fdata['lastname']);}
			if(isset($fdata['firstname'])) {$dto->setFirstname($fdata['firstname']);}
                        if(isset($fdata['username'])) {$dto->setUsername($fdata['username']);}
			if(isset($fdata['password'])) {$dto->setPassword(md5($fdata['password']));}
			
			if(isset($fdata['idnumber'])) {$dto->setIdnumber($fdata['idnumber']);}
			if(isset($fdata['param'])) {$dto->setParam($fdata['param']);}
			if(isset($fdata['description'])) {$dto->setDescription($fdata['description']);}
			
                            $doctype=null;
                         $docnumber=null;
                        if(isset($fdata['document']['field1'])) {$doctype=$fdata['document']['field1'];}
                        if(isset($fdata['document']['field2'])) {$docnumber=$fdata['document']['field2'];}
        	
                        if ($doctype=='CPF') {
                                $docnumber=preg_replace('/[^0-9]/', '', $docnumber);
                         }
                        $dto->setDocnumber($docnumber);
                        $dto->setDoctype($doctype);
			
		
			return $dto;
		
	}
	
	
	public function getAddressDb($userid,$fdata) {
		
			$data=$this->getContainer()->get('badiu.util.address.data.data');
			$dto=$data->findLastByShortnameCategory('badiu.system.user.user',$userid,'residence');
			
			
			if($dto!=null){
				$stateid='';
				if(!empty($dto->getStateid())){$stateid=$dto->getStateid()->getId();}
				$fdata['stateid']=$stateid;
				$fdata['state']=$dto->getState();
				$fdata['city']=$dto->getCity();
				$fdata['address']=$dto->getAddress();
				$fdata['cep']=$dto->getCep();
				$fdata['neighborhood']=$dto->getNeighborhood();
				$fdata['addressnumber']=$dto->getAddressnumber();
				$fdata['addressinfo']=$dto->getAddressinfo();
				
			}
			return $fdata;
		}
	public function getAddressForm($userid,$fdata,$edit=FALSE) {
		
			$dto=null;
			if($edit){
				$data=$this->getContainer()->get('badiu.util.address.data.data');
				$dto=$data->findLastByShortnameCategory('badiu.system.user.user',$userid,'residence');
			}
			if(empty($dto)){
				$dto=$this->getContainer()->get('badiu.util.address.data.entity');
				$dto=$this->initDefaultEntityData($dto);
				$dto->setModulekey('badiu.system.user.user');
				$dto->setModuleinstance($userid);
				$dto->setCategoryid($this->getAddressCategory());
				
			}
		
		
			if(isset($fdata['stateid'])) {$dto->setStateid($this->getContainer()->get('badiu.util.address.state.data')->findById($fdata['stateid']));}
			if(isset($fdata['state'])) {$dto->setState($fdata['state']);}
			if(isset($fdata['city'])) {$dto->setCity($fdata['city']);}
			if(isset($fdata['address'])) {$dto->setAddress($fdata['address']);}
			if (isset($fdata['cep'])) {$dto->setCep($fdata['cep']);}
			if(isset($fdata['neighborhood'])) {$dto->setNeighborhood($fdata['neighborhood']);}
			if(isset($fdata['addressnumber'])) {$dto->setAddressnumber($fdata['addressnumber']);}
			if(isset($fdata['addressinfo'])) {$dto->setAddressinfo($fdata['addressinfo']);}
		
			
			return $dto;
		
	}
		public function getTelephoneFixeResidenceDb($userid,$fdata) {
		
			$data=$this->getContainer()->get('badiu.util.address.telephone.data');
			$dto=$data->findLastByShortnameCategory('badiu.system.user.user',$userid,'residence','fixe');
			$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
			$fdata=$telephonefromtypedata->updateFormDataByDto($dto,$fdata,'telfixeresidence');
			
			return $fdata;
		}
		public function getTelephoneFixeResidenceForm($userid,$fdata,$edit=FALSE) {
			$dto=null;
			if($edit){
				$data=$this->getContainer()->get('badiu.util.address.telephone.data');
				$dto=$data->findLastByShortnameCategory('badiu.system.user.user',$userid,'residence','fixe');
			}
			if(empty($dto)){
				$dto=$this->getContainer()->get('badiu.util.address.telephone.entity');
				$dto=$this->initDefaultEntityData($dto);
				$dto->setModulekey('badiu.system.user.user');
				$dto->setModuleinstance($userid);
				$dto->setDtype('fixe');
				$dto->setCategoryid($this->getAddressCategory());
			}
			$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
			$dto=$telephonefromtypedata->updateDtoByFormData($dto,$fdata,'telfixeresidence');
						
			return $dto;
		
	}
	public function getTelephoneFixeProfessionalDb($userid,$fdata) {
		
			$data=$this->getContainer()->get('badiu.util.address.telephone.data');
			$dto=$data->findLastByShortnameCategory('badiu.system.user.user',$userid,'professional','fixe');

				$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
				$fdata=$telephonefromtypedata->updateFormDataByDto($dto,$fdata,'telfixeprofessional');
			return $fdata;
		}
		public function getTelephoneFixeProfessionalForm($userid,$fdata,$edit=FALSE) {
			$dto=null;
			if($edit){
				$data=$this->getContainer()->get('badiu.util.address.telephone.data');
				$dto=$data->findLastByShortnameCategory('badiu.system.user.user',$userid,'professional','fixe');
			}
			if(empty($dto)){
				$dto=$this->getContainer()->get('badiu.util.address.telephone.entity');
				$dto=$this->initDefaultEntityData($dto);
				$dto->setModulekey('badiu.system.user.user');
				$dto->setModuleinstance($userid);
				$dto->setDtype('fixe');
				$dto->setCategoryid($this->getAddressCategory('professional'));
			}  
			$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
			$dto=$telephonefromtypedata->updateDtoByFormData($dto,$fdata,'telfixeprofessional');
			return $dto;
		
	}
	public function getTelephoneMobile1Db($userid,$fdata) {
			
			$data=$this->getContainer()->get('badiu.util.address.telephone.data');
			$dto=$data->findLastByShortnameCategory('badiu.system.user.user',$userid,'residence','mobile',1);

			$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
			$fdata=$telephonefromtypedata->updateFormDataByDto($dto,$fdata,'telmobile1');
			return $fdata;
		}
	
	public function getTelephoneMobile1Form($userid,$fdata,$edit=FALSE) {
	
		$dto=null;
			if($edit){
				$data=$this->getContainer()->get('badiu.util.address.telephone.data');
				$dto=$data->findLastByShortnameCategory('badiu.system.user.user',$userid,'residence','mobile',1);
			}
			if(empty($dto)){
				$dto=$this->getContainer()->get('badiu.util.address.telephone.entity');
				$dto=$this->initDefaultEntityData($dto);
				$dto->setModulekey('badiu.system.user.user');
				$dto->setModuleinstance($userid);
				$dto->setDtype('mobile');
				
				$dto->setCategoryid($this->getAddressCategory());
				$dto->setSortorder(1);
				
			}
			$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
			$dto=$telephonefromtypedata->updateDtoByFormData($dto,$fdata,'telmobile1');
		return $dto;
		
	}
	
	public function getTelephoneMobile2Db($userid,$fdata) {
			
			$data=$this->getContainer()->get('badiu.util.address.telephone.data');
			$dto=$data->findLastByShortnameCategory('badiu.system.user.user',$userid,'residence','mobile',2);

			$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
			$fdata=$telephonefromtypedata->updateFormDataByDto($dto,$fdata,'telmobile2');
			return $fdata;
		}
	
	public function getTelephoneMobile2Form($userid,$fdata,$edit=FALSE) {
		$dto=null;
			if($edit){
				$data=$this->getContainer()->get('badiu.util.address.telephone.data');
				$dto=$data->findLastByShortnameCategory('badiu.system.user.user',$userid,'residence','mobile',2);
			}
			if(empty($dto)){
				$dto=$this->getContainer()->get('badiu.util.address.telephone.entity');
				$dto=$this->initDefaultEntityData($dto);
				$dto->setModulekey('badiu.system.user.user');
				$dto->setModuleinstance($userid);
				$dto->setDtype('mobile');
				
				$dto->setCategoryid($this->getAddressCategory());
				$dto->setSortorder(2);
				
			}
			
			$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
			$dto=$telephonefromtypedata->updateDtoByFormData($dto,$fdata,'telmobile2');
		return $dto;
		
	}
	public function getAddressCategory($shortname='residence') {
		$data=$this->getContainer()->get('badiu.util.address.category.data');
		$dto=$data->findByShortname($this->getEntity(),$shortname);
		return $dto;
	}
	
	
}
