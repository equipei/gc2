<?php

namespace Badiu\System\UserBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;


class UserForm extends BadiuModelLib
{
    
    function __construct(Container $container)
    {
        parent::__construct($container);
    }
    
    public function getDataFormEdit($id)
    {
        $fdata = array();
        $fdata = $this->getDefaultDb($id, $fdata);
        
        return $fdata;
    }
    public function save($data)
    {
        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit       = $sysoperation->isEdit();
        $id           = null;
        if ($isEdit) {
            $id = $this->getContainer()->get('request')->get('id');
        }
        $dto = $data->getDto();
        
        //save user
        $data->setDto($this->getDefaultForm($id, $dto));
        $result = $data->save();
        $id     = $data->getDto()->getId();
        
        
        return $data->getDto();
    }
    public function getDefaultDb($id, $fdata)
    {
        
        $data               = $this->getContainer()->get('badiu.system.user.user.data');
        $dto                = $data->findById($id);
        $fdata['firstname'] = $dto->getFirstname();
        $fdata['lastname']  = $dto->getLastname();
        $fdata['email']     = $dto->getEmail();
        $fdata['username']  = $dto->getUsername();
        $fdata['password']  = $dto->getPassword();
        
        $fdata['document']           = array();
        $fdata['document']['field1'] = $dto->getDoctype();
        $fdata['document']['field2'] = $dto->getDocnumber();
        
        
        $fdata['idnumber']    = $dto->getIdnumber();
        $fdata['param']       = $dto->getParam();
        $fdata['description'] = $dto->getDescription();
        
        //contact
        $contact             = $this->getJson()->decode($dto->getContactdata(), true);
        $personalcontact     = $this->getUtildata()->getVaueOfArray($contact, 'personal');
        $professionalcontact = $this->getUtildata()->getVaueOfArray($contact, 'professional');
        
        $fdata['personalpostcode']          = $this->getUtildata()->getVaueOfArray($personalcontact, 'postcode');
        $fdata['personaladdress']           = $this->getUtildata()->getVaueOfArray($personalcontact, 'address');
        $fdata['personaladdressnumber']     = $this->getUtildata()->getVaueOfArray($personalcontact, 'addressnumber');
        $fdata['personaladdresscomplement'] = $this->getUtildata()->getVaueOfArray($personalcontact, 'addresscomplement');
        $fdata['personalneighborhood']      = $this->getUtildata()->getVaueOfArray($personalcontact, 'neighborhood');
        $fdata['personalcity']              = $this->getUtildata()->getVaueOfArray($personalcontact, 'city');
        $fdata['personalstateid']             = $this->getUtildata()->getVaueOfArray($personalcontact, 'stateid');
        $fdata['personalcountry']           = $this->getUtildata()->getVaueOfArray($personalcontact, 'country');
        $fdata['personalphone']             = $this->getUtildata()->getVaueOfArray($personalcontact, 'phone');
        $fdata['personalphonemobile']       = $this->getUtildata()->getVaueOfArray($personalcontact, 'phonemobile');
        $fdata['personalemail']             = $this->getUtildata()->getVaueOfArray($personalcontact, 'email');
        
        
        
        $fdata['professionalpostcode']          = $this->getUtildata()->getVaueOfArray($professionalcontact, 'postcode');
        $fdata['professionaladdress']           = $this->getUtildata()->getVaueOfArray($professionalcontact, 'address');
        $fdata['professionaladdressnumber']     = $this->getUtildata()->getVaueOfArray($professionalcontact, 'addressnumber');
        $fdata['professionaladdresscomplement'] = $this->getUtildata()->getVaueOfArray($professionalcontact, 'addresscomplement');
        $fdata['professionalneighborhood']      = $this->getUtildata()->getVaueOfArray($professionalcontact, 'neighborhood');
        $fdata['professionalcity']              = $this->getUtildata()->getVaueOfArray($professionalcontact, 'city');
        $fdata['professionalstateid']             = $this->getUtildata()->getVaueOfArray($professionalcontact, 'stateid');
        $fdata['professionalcountry']           = $this->getUtildata()->getVaueOfArray($professionalcontact, 'country');
        $fdata['professionalphone']             = $this->getUtildata()->getVaueOfArray($professionalcontact, 'phone');
        $fdata['professionalphonemobile']       = $this->getUtildata()->getVaueOfArray($professionalcontact, 'phonemobile');
        $fdata['professionalemail']             = $this->getUtildata()->getVaueOfArray($professionalcontact, 'email');
      
        return $fdata;
        
    }
    
    public function getDefaultForm($id, $fdata)
    {
        $dto = null;
        if (!empty($id)) {
            $data = $this->getContainer()->get('badiu.system.user.user.data');
            $dto  = $data->findById($id);
        } else {
            $dto = $this->getContainer()->get('badiu.system.user.user.entity');
            $dto = $this->initDefaultEntityData($dto);
        }
        
        
        if (isset($fdata['name'])) {
            $dto->setFirstname($fdata['name']);
            $dto->setLastname(' ');
        }
        if (isset($fdata['email'])) {
            $dto->setEmail($fdata['email']);
        }
        if (isset($fdata['lastname'])) {
            $dto->setLastname($fdata['lastname']);
        }
        if (isset($fdata['firstname'])) {
            $dto->setFirstname($fdata['firstname']);
        }
        if (isset($fdata['username'])) {
            $dto->setUsername($fdata['username']);
        }
        if (isset($fdata['password'])) {
            $dto->setPassword(md5($fdata['password']));
        }
        
        if (isset($fdata['idnumber'])) {
            $dto->setIdnumber($fdata['idnumber']);
        }
        if (isset($fdata['param'])) {
            $dto->setParam($fdata['param']);
        }
        if (isset($fdata['description'])) {
            $dto->setDescription($fdata['description']);
        }
        
        $doctype   = null;
        $docnumber = null;
        if (isset($fdata['document']['field1'])) {
            $doctype = $fdata['document']['field1'];
        }
        if (isset($fdata['document']['field2'])) {
            $docnumber = $fdata['document']['field2'];
        }
        
        if ($doctype == 'CPF') {
            $docnumber = preg_replace('/[^0-9]/', '', $docnumber);
        }
        $dto->setDocnumber($docnumber);
        $dto->setDoctype($doctype);
        
        //contact
        $personalcontact                      = array();
        $personalcontact['postcode']          = $this->getUtildata()->getVaueOfArray($fdata, 'personalpostcode');
        $personalcontact['address']           = $this->getUtildata()->getVaueOfArray($fdata, 'personaladdress');
        $personalcontact['addressnumber']     = $this->getUtildata()->getVaueOfArray($fdata, 'personaladdressnumber');
        $personalcontact['addresscomplement'] = $this->getUtildata()->getVaueOfArray($fdata, 'personaladdresscomplement');
        $personalcontact['neighborhood']      = $this->getUtildata()->getVaueOfArray($fdata, 'personalneighborhood');
        $personalcontact['city']              = $this->getUtildata()->getVaueOfArray($fdata, 'personalcity');
        $personalcontact['stateid']           = $this->getUtildata()->getVaueOfArray($fdata, 'personalstateid');
        $personalcontact['state']             = "";
        $personalcontact['stateshortname']    = "";

        if (!empty($personalcontact['stateid'])) {
            $stateinfo                         = $this->getContainer()->get('badiu.util.address.state.data')->getInfoById($personalcontact['stateid']);
            $personalcontact['state']          = $this->getUtildata()->getVaueOfArray($stateinfo, 'name');
            $personalcontact['stateshortname'] = $this->getUtildata()->getVaueOfArray($stateinfo, 'shortname');
        }
        
        //$personalcontact['country']  = $this->getUtildata()->getVaueOfArray($fdata, 'personalcountry');
        $personalcontact['country']     = "BR";
        $personalcontact['phone']       = $this->getUtildata()->getVaueOfArray($fdata, 'personalphone');
        $personalcontact['phonemobile'] = $this->getUtildata()->getVaueOfArray($fdata, 'personalphonemobile');
        $personalcontact['email']       = $this->getUtildata()->getVaueOfArray($fdata, 'email');
        
        
        $professionalcontact                      = array();
        $professionalcontact['postcode']          = $this->getUtildata()->getVaueOfArray($fdata, 'professionalpostcode');
        $professionalcontact['address']           = $this->getUtildata()->getVaueOfArray($fdata, 'professionaladdress');
        $professionalcontact['addressnumber']     = $this->getUtildata()->getVaueOfArray($fdata, 'professionaladdressnumber');
        $professionalcontact['addresscomplement'] = $this->getUtildata()->getVaueOfArray($fdata, 'professionaladdresscomplement');
        $professionalcontact['neighborhood']      = $this->getUtildata()->getVaueOfArray($fdata, 'professionalneighborhood');
        $professionalcontact['city']              = $this->getUtildata()->getVaueOfArray($fdata, 'professionalcity');
        $professionalcontact['stateid']             = $this->getUtildata()->getVaueOfArray($fdata, 'professionalstateid');
        $professionalcontact['state']             = "";
        $professionalcontact['stateshortname']    = ""; 
        
        //$professionalcontact['country']           = $this->getUtildata()->getVaueOfArray($fdata, 'professionalcountry');
        $professionalcontact['country']     = "BR";
        $professionalcontact['phone']       = $this->getUtildata()->getVaueOfArray($fdata, 'professionalphone');
        $professionalcontact['phonemobile'] = $this->getUtildata()->getVaueOfArray($fdata, 'professionalphonemobile');
        $professionalcontact['email']       = $this->getUtildata()->getVaueOfArray($fdata, 'professionalemail');
        
        if (!empty($professionalcontact['stateid'])) {
            $stateinfo                         = $this->getContainer()->get('badiu.util.address.state.data')->getInfoById($professionalcontact['stateid']);
            $professionalcontact['state']          = $this->getUtildata()->getVaueOfArray($stateinfo, 'name');
            $professionalcontact['stateshortname'] = $this->getUtildata()->getVaueOfArray($stateinfo, 'shortname');
        }
        
        
        $contact                 = array();
        $contact['personal']     = $personalcontact;
        $contact['professional'] = $professionalcontact;
        $contactdatajsont        = $this->getJson()->encode($contact);
        
      
        
        $dto->setContactdata($contactdatajsont);
        return $dto;
        
    }
    
    
    
}
