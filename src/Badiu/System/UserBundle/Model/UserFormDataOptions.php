<?php
namespace  Badiu\System\UserBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class UserFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
       
   
       public  function getRecoverPassworIdentify(){
        $list=array();
        $list['email']=$this->getTranslator()->trans('badiu.system.user.recoverpwd.typeidentifybyemail');
        $list['username']=$this->getTranslator()->trans('badiu.system.user.recoverpwd.typeidentifybyusername');
        return $list;
    }

    public  function sex(){
        $list = array();
        $list['male']=$this->getTranslator()->trans('badiu.system.user.user.sexmale');
        $list['female']=$this->getTranslator()->trans('badiu.system.user.user.sexfemale');
		$list['other']=$this->getTranslator()->trans('badiu.system.user.user.other');
        return $list;
    }

    public  function sexLabel($vkey){
        $label="";
        if($vkey=='male'){$label=$this->getTranslator()->trans('badiu.system.user.user.sexmale');}
        else if($vkey=='female'){$label=$this->getTranslator()->trans('badiu.system.user.user.sexfemale');}

        return  $label;
    }

    public  function maritalstatus(){
        $list = array();
        $list['single']=$this->getTranslator()->trans('badiu.system.user.user.maritalstatus.single');
        $list['married']=$this->getTranslator()->trans('badiu.system.user.user.maritalstatus.married');
        $list['separate']=$this->getTranslator()->trans('badiu.system.user.user.maritalstatus.separate');
        $list['widowed']=$this->getTranslator()->trans('badiu.system.user.user.maritalstatus.widowed');
        $list['other']=$this->getTranslator()->trans('badiu.system.user.user.maritalstatus.other');
        return $list;
    }

    public  function maritalstatusLabel($status){
        $label="";
        if($status=='single'){$label=$this->getTranslator()->trans('badiu.system.user.user.maritalstatus.single');}
        else if($status=='married'){$label=$this->getTranslator()->trans('badiu.system.user.user.maritalstatus.married');;}
        else if($status=='separate'){$label=$this->getTranslator()->trans('badiu.system.user.user.maritalstatus.separate');}
        else if($status=='widowed'){$label=$this->getTranslator()->trans('badiu.system.user.user.maritalstatus.widowed');}
        else if($status=='other'){$label=$this->getTranslator()->trans('badiu.system.user.user.maritalstatus.other');}
        return  $label;
    }
    public  function schoollevel(){
        $list = array();
        $list['elementaryschoolincomplete']=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.elementaryschoolincomplete');
        $list['elementaryschoolcompleted']=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.elementaryschoolcompleted');
        $list['highschoolincomplete']=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.highschoolincomplete');
        $list['highschoolcompleted']=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.highschoolcompleted');
        $list['technicaleducation']=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.technicaleducation');
        $list['universitygraduateincomplete']=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.universitygraduateincomplete');
        $list['universitygraduatecompleted']=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.universitygraduatecompleted');
        $list['universityspecialization']=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.universityspecialization');
        $list['universitymasters']=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.universitymasters');
        $list['universitydoctorate']=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.universitydoctorate');
        return $list;
    }

    public  function schoollevelLabel($scoollevel){
        $label="";
        //echo $scoollevel;exit;
        if($scoollevel=='elementaryschool'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.elementaryschool');}
        else if($scoollevel=='elementaryschoolincomplete'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.elementaryschoolincomplete');}
        else if($scoollevel=='elementaryschoolcompleted'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.elementaryschoolcompleted');}
        else if($scoollevel=='highschoolincomplete'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.highschoolincomplete');}
        else if($scoollevel=='highschoolcompleted'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.highschoolcompleted');}
        else if($scoollevel=='highschool'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.highschool');}
        else if($scoollevel=='technicaleducation'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.technicaleducation');}
        else if($scoollevel=='universitygraduatecompleted'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.universitygraduatecompleted');}
        else if($scoollevel=='universitygraduateincomplete'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.universitygraduateincomplete');}
        else if($scoollevel=='universitygraduate'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.universitygraduate');}
        else if($scoollevel=='universityspecialization'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.universityspecialization');}
        else if($scoollevel=='universitymasters'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.universitymasters');}
        else if($scoollevel=='universitydoctorate'){$label=$this->getTranslator()->trans('badiu.system.user.profile.schoollevel.universitydoctorate');}
         return  $label;
    }
    
    public  function getNationalitystatus(){
        $list=array();

        $list['native']=$this->getTranslator()->trans('badiu.system.user.user.nationalitystatus.br');
        $list['foreign']=$this->getTranslator()->trans('badiu.system.user.user.nationalitystatus.foreign');
       // $list['resident']=$this->getTranslator()->trans('badiu.system.user.user.nationalitystatus.br.foreign');
		
        return $list;
    }
    public  function getNationalitystatusLabel($nstaus){
        $label="";

        if($nstaus=='native'){ $label=$this->getTranslator()->trans('badiu.system.user.user.nationalitystatus.br');}
        if($nstaus=='foreign'){$label=$this->getTranslator()->trans('badiu.system.user.user.nationalitystatus.foreign');}
        if($nstaus=='resident'){$label=$this->getTranslator()->trans('badiu.system.user.user.nationalitystatus.br.foreign');}
		
        return  $label;
    }
    
     
    public  function getStatusRegister(){
        $list=array();
        $list['complete']=$this->getTranslator()->trans('badiu.system.user.user.statusregister.complete');
        $list['incomplete']=$this->getTranslator()->trans('badiu.system.user.user.statusregister.incomplete');
		$list['requireupdate']=$this->getTranslator()->trans('badiu.system.user.user.statusregister.requireupdate');
        return $list; 
    }

	public  function securitypolicyaccept(){
        $list=array();
        $list["1"]=$this->getTranslator()->trans('badiu.system.user.user.securitypolicyaccept');
        return $list; 
    }
    public  function getStatusRegisterLabel($status){
        $label="";
        if($status=='complete'){ $label=$this->getTranslator()->trans('badiu.system.user.user.statusregister.complete');}
        else if($status=='incomplete'){ $label=$this->getTranslator()->trans('badiu.system.user.user.statusregister.incomplete');}
		else if($status=='requireupdate'){ $label=$this->getTranslator()->trans('badiu.system.user.user.statusregister.requireupdate');}
        return  $label;
    }
	
	
	public  function getAdminFormHighereducationarea(){
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
		$data= $this->getContainer()->get('badiu.admin.form.fieldoptions.data');
		$entity=$badiuSession->get()->getEntity();
		$list=$data->getFormChoiceByFieldshortname($entity,'highereducationarea');
        return $list;
    }
	public  function getAdminFormHighereducationareaForAutocomplete(){
		$data= $this->getContainer()->get('badiu.admin.form.fieldoptions.data');
		$list=$data->getForAutocomplete('highereducationarea');
        return $list;
    }
	public  function getAdminFormHighereducationareaNameByIdOnEditAutocomplete($id){
		$data= $this->getContainer()->get('badiu.admin.form.fieldoptions.data');
		$name=$data->getNameByIdOnEditAutocomplete($id);
		return $name;
    }
}
