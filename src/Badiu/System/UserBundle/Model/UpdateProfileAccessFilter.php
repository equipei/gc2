<?php

namespace  Badiu\System\UserBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuAccessFilter;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class UpdateProfileAccessFilter extends  BadiuAccessFilter {

	function __construct(Container $container) {
		parent::__construct($container);
	}

       public function exec(){
		  
		  //check session
		   $badiuSession = $this->getContainer()->get('badiu.system.access.session');
           if (!$badiuSession->exist()) {return true;}
		   $userforceupdateprofile=$badiuSession->getValue('badiu.system.core.param.config.userforceupdateprofile');
		   if(!$userforceupdateprofile) {return true;}
		   
		  //check lista eception
		  $currentroute= $this->getContainer()->get('request')->get('_route');
		  $listexception=$this->listException(); 
		  if(in_array($currentroute ,$listexception)){return true;} 
		
		  $route= 'badiu.tms.my.studentfviewdefault.dashboard';
		  $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
		  $url=$utilapp->getUrlByRoute($route);
		  
		  $urlreferer=$badiuSession->getUrlReferer(); 
		   if(!empty($urlreferer)){$url=$urlreferer;}
		   $lparam=array('urlgoback'=>$url,'outurl'=>0);
		   $this->getContainer()->get('badiu.system.user.user.lib.controlupdateprofile')->requireUpdate($lparam);
	
        return true;
         } 
    
      private function listException(){
		 /* $badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$listkeys=$badiuSession->getValue('badiu.system.core.param.config.userforceupdateprofileroutesexception');
			$listkeys=preg_split("/\r\n|\n|\r/", $listkeys);
			return $listkeys;*/
		  $list=array();
		  //return  $list;
		  array_push($list, 'badiu.system.scheduler.task.cron'); 
          array_push($list, 'badiu.system.user.recoverpwd.add'); 
          array_push($list, 'badiu.system.user.recoverpwdchange.add');
          array_push($list, 'badiu.moodle.mreport.client.lib.moodleservice');
          array_push($list, 'badiu.admin.client.newentityaccount.add');
			array_push($list, 'badiu.financ.ecommerce.shop.index');
                array_push($list, 'badiu.financ.ecommerce.shop.view');
                array_push($list, 'badiu.financ.ecommerce.cart.dashboard');
                array_push($list, 'badiu.financ.ecommerce.login.add');
                array_push($list, 'badiu.financ.ecommerce.loginsingin.add');
                array_push($list, 'badiu.financ.ecommerce.client.add');
                array_push($list, 'badiu.financ.ecommerce.shopintegration.link');
                array_push($list, 'badiu.admin.selection.requestenroll.link');       
    			array_push($list, 'badiu.admin.selection.inforequest.dashboard');
				array_push($list, 'badiu.admin.selection.loginsingin.add');
				array_push($list, 'badiu.admin.selection.loginsingindoc.add');
				array_push($list, 'badiu.system.core.update.link');
                array_push($list, 'badiu.auth.ssogovbr.request.link');
                array_push($list, 'badiu.auth.ssogovbr.return.link'); 
				//array_push($list, 'badiu.tms.my.studentfviewdefault.dashboard');
				array_push($list, 'badiu.system.core.lib.qrcode.factorycreate.link');	

				array_push($list, 'badiu.system.core.service.file');  
				
				array_push($list, 'badiu.system.core.service.process');  
				
				array_push($list, 'badiu.system.user.default.index');  
				array_push($list, 'badiu.system.user.default.add');  
				array_push($list, 'badiu.system.user.default.edit');  
				
				array_push($list, 'badiu.admin.client.changedefaultprofile.add');
				array_push($list, 'badiu.admin.client.changedefaultprofile.edit');
				array_push($list, 'badiu.admin.client.defaultprofile.dashboard');
				
				array_push($list, 'badiu.auth.core.login.add');
				array_push($list, 'badiu.auth.core.synclogout.link');
				array_push($list, 'badiu.auth.core.logaut.index');
				array_push($list, 'badiu.admin.certificate.get.link');
				
				
				
				
				return $list;
		 
	  }		  
       
}
