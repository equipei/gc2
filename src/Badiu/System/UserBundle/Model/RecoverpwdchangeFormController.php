<?php

namespace  Badiu\System\UserBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class RecoverpwdchangeFormController extends BadiuFormController
{
    function __construct(Container $container) {
           parent::__construct($container);
              }

	

	 public function save() {
		
		 $valid=$this->validation();
		 if(!empty($valid)){return $valid;}
         $result=$this->changePwd();
              
        if($result){
			$msg=$this->getTranslator()->trans('badiu.system.user.recoverpwdchange.messagesucess');
			$info='badiu.system.user.recoverpwdchange.messagesucess';
			$outrsult=array('result'=>$result,'message'=>$msg,'urlgoback'=>null);
			return $this->getResponse()->accept($outrsult,$info);
		}
		$message=array();
        $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.recoverpwdchange.messagefailed');
        $info = 'badiu.system.user.recoverpwdchange.messagefailed';
        return $this->getResponse()->denied($info, $message);
    }

  public function validation() {
       $check = $this->checkPassword();
        if ($check!= null) {
            return $check;
        }
       return null;
    }


	public function checkPassword() {
		 $newpassword=$this->getParamItem('newpassword'); 
		 $newpasswodrconfirm=$this->getParamItem('newpasswodrconfirm'); 
		 $cpwtoken=$this->getParamItem('cpwtoken'); 
		 
		
		if(empty($cpwtoken)){
			 $message=array();
             $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.recoverpwdchange.tokenrequired');
             $info = 'badiu.system.user.recoverpwdchange.tokenrequired';
             return $this->getResponse()->denied($info, $message);
		 }
		 
		  $moduledata=$this->getContainer()->get('badiu.system.module.data.data');
		  //'bkey'=>'badiu.system.user',
          $param=array('modulekey'=>'badiu.system.user.recoverpwd','shortname'=>$cpwtoken,'customint1'=>1);
          
		  $exist=$moduledata->countNativeSql($param,false);
		  
		  if(!$exist){
			 $message=array();
             $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.recoverpwdchange.tokennotvalid');
             $info = 'badiu.system.user.recoverpwdchange.tokenrequired';
             return $this->getResponse()->denied($info, $message);
		 }
		
		if($newpassword != $newpasswodrconfirm){
			 $message=array();
             $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.recoverpwdchange.newpasswordconfirmcheck');
             $info = 'badiu.system.user.recoverpwdchange.newpasswordconfirmcheck';
             return $this->getResponse()->denied($info, $message);
		 }		 
		else if(strlen($newpassword) < 5){
			 $message=array();
             $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.recoverpwdchange.minlenth');
             $info = 'badiu.system.user.recoverpwdchange.minlenth';
             return $this->getResponse()->denied($info, $message);
		}
		return null;
	}


        

    
    public function changePwd() {
			$newpassword=$this->getParamItem('newpassword'); 
			$newpasswodrconfirm=$this->getParamItem('newpasswodrconfirm'); 
			$cpwtoken=$this->getParamItem('cpwtoken'); 
		
          
        //get user id
         $moduledata=$this->getContainer()->get('badiu.system.module.data.data');
		 //'bkey'=>'badiu.system.user',
         $fparam=array('modulekey'=>'badiu.system.user.recoverpwd','shortname'=>$cpwtoken,'customint1'=>1);
		
		  $mdata=$moduledata->getGlobalColumnsValue('o.id,o.moduleinstance',$fparam); 
		
		 $userid=$this->getUtildata()->getVaueOfArray($mdata,'moduleinstance');
         $moduleid=$this->getUtildata()->getVaueOfArray($mdata,'id');

		$userdata=$this->getContainer()->get('badiu.system.user.user.data');
         if($userid > 0){
             $param=array('id'=>$userid,'password'=>md5($newpassword));
			  $result1=$userdata->updateNativeSql($param,false);
             $result2=$moduledata->updateNativeSql(array('id'=>$moduleid,'customint1'=>0,'timemodified'=>new \DateTime()),false);
             
             return $result1;
         }
      return false;    
    }
}
