<?php

namespace Badiu\System\UserBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormError;

class RecoverpwdController extends BadiuController {

    private $email;
    private $entity;
    private $issserivecetoken = false;

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function addCheckForm($form, $data) {
        $check = true;
        $dto = $data->getDto();

        $typeidentify = null;
        $email = null;
        $username = null;
        $entity = $this->getContainer()->getParameter('badiu.system.access.defaultentity');

        if (isset($dto['typeidentify'])) {
            $typeidentify = $dto['typeidentify'];
        }
        if (isset($dto['email'])) {
            $email = $dto['email'];
        }
        if (isset($dto['username'])) {
            $username = $dto['username'];
        }
        if (isset($dto['rpwdentitytoken'])) {
            $entity = urldecode($dto['rpwdentitytoken']);
        }
        $this->email = $email;

        //check token of service server
        if (!empty($entity)) {
            //check is server
            $pos = strpos($entity, "|");
            if ($pos !== false) {
                $sserverdata = $this->getContainer()->get('badiu.admin.server.service.data');
                $param = array('servicetoken' => $entity);
                $exist = $sserverdata->countNativeSql($param, false);
                if (!$exist) {
                    $form->get('typeidentify')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.recoverpwd.tokennotvalid')));
                } else {
                    $this->issserivecetoken = true;
                    $p = explode("|", $entity);
                    if (isset($p[0])) {
                        $entity = $p[0];
                    }
                }
            }

            //chekc validate token
        }
        $this->entity = $entity;
        if ($typeidentify == 'email' && empty($email)) {
            $form->get('email')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.recoverpwd.emailrequired')));
            return false;
        }
        if ($typeidentify == 'email' && empty($entity)) {
            $form->get('typeidentify')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.recoverpwd.tokenrequired')));
        }
        if ($typeidentify == 'username' && empty($username)) {
            $form->get('username')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.recoverpwd.usernamerequired')));
            return false;
        }
        if ($typeidentify == 'email' && !empty($email)) {
            $param = array('email' => $email, 'entity' => $entity);
            $data = $this->getContainer()->get('badiu.system.user.user.data');
            $exist = $data->countNativeSql($param, false);
            if (!$exist) {
                $form->get('email')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.recoverpwd.emailnotfound')));
                return false;
            }
        }
        if ($typeidentify == 'username' && !empty($username)) {
            $param = array('username' => $username);
            $data = $this->getContainer()->get('badiu.system.user.user.data');
            $exist = $data->countNativeSql($param, false);
            if (!$exist) {
                $form->get('email')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.recoverpwd.usernamelnotfound')));
                return false;
            }
        }


        return $check;
    }

    public function save($data) {
        $dto = $data->getDto();
        $recoverykey = $this->resetPassword($dto);
        $dto['recoverykey'] = $recoverykey;
        $email = $this->email;
        $mailsend = false;
        try {
            $mailsend = $this->sendMail($email, $recoverykey);
        } catch (\Swift_TransportException $ex) {
            $mailsend = false;
        }
        $dto['mailsend'] = $mailsend;
        return $dto;
    }

    public function resetPassword($data) {
        $typeidentify = null;
        $email = null;
        $username = null;
        $entity = $this->getContainer()->getParameter('badiu.system.access.defaultentity');
        if (isset($data['typeidentify'])) {
            $typeidentify = $data['typeidentify'];
        }
        if (isset($data['email'])) {
            $email = $data['email'];
        }
        if (isset($data['username'])) {
            $username = $data['username'];
        }
        $this->email = $email;
        if ($typeidentify == 'email' && !empty($email)) {
            $param = array('email' => $email, 'entity' => $entity);
            $data = $this->getContainer()->get('badiu.system.user.user.data');

            $user = $data->getInfoByEmailForLogin($entity, $email);
            if (!empty($user)) {
                $userid = null;
                $name = null;
                if (isset($user['id'])) {
                    $userid = $user['id'];
                }
                if (isset($user['name'])) {
                    $name = $user['name'];
                }
                $reciverkey = $this->generateToken($entity, $userid, $name, $email);
                return $reciverkey;
            }
        } else if ($typeidentify == 'username' && !empty($username)) {
            $data = $this->getContainer()->get('badiu.system.user.user.data');
            $user = $data->getInfoByUsernameForLogin($username);
            if (!empty($user)) {
                $userid = null;
                $name = null;
                $email = null;
                $entity = null;
                if (isset($user['id'])) {
                    $userid = $user['id'];
                }
                if (isset($user['name'])) {
                    $name = $user['name'];
                }
                if (isset($user['email'])) {
                    $email = $user['email'];
                }
                if (isset($user['entity'])) {
                    $entity = $user['entity'];
                }

                $this->email = $email;

                $reciverkey = $this->generateToken($entity, $userid, $name, $email);

                return $reciverkey;
            }
        }
        return null;
    }

    public function generateToken($entity, $userid, $name, $email) {
        //genarate ukey
        $now = time();
        $basekey = "|$userid|$entity|$now";
        $lenth = strlen($basekey);
        $hash = $this->getContainer()->get('badiu.system.core.lib.util.hash');
        $lenth = 60 - $lenth;
        $recoverkey = $hash->make($lenth);
        $recover = $recoverkey . $basekey;
        $param = array('bkey' => 'badiu.system.user', 'modulekey' => 'badiu.system.user.recoverpwd', 'moduleinstance' => $userid, 'name' => $name, 'shortname' => $recover, 'customint1' => 1, 'customchar1' => $email, 'entity' => $entity, 'timecreated' => new \DateTime(), 'deleted' => 0);
        $data = $this->getContainer()->get('badiu.system.module.data.data');
        $result = $data->insertNativeSql($param, false);
        if ($result) {
            return $recover;
        }
    }

    function sendMail($email, $recoverykey) {
        
        $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
        $urlpwd=$utilapp->getUrlByRoute('badiu.system.user.recoverpwdchange.add',array('cpwtoken'=>$recoverykey));
        $link="<a href=\"$urlpwd\" target = \"_blank\" >$urlpwd</a>";
        $msg=$this->getTranslator()->trans('badiu.system.user.recoverpwd.messagemailcontent',array('%link%'=>$link));
        $mconfig=array();
      
        $mconfig['useridto']=null;
        $mconfig['userto']=null;
        $mconfig['recipient']['to']=$email;
        $mconfig['recipient']['bcc']= null;
        $mconfig['message']['subject']=$this->getTranslator()->trans('badiu.system.user.recoverpwd.messagemailsubject');
        $mconfig['message']['body']=$msg;
        $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
		
		$systemdata=$this->getContainer()->get('badiu.system.core.functionality.systemdata');
		$systemdata->setEntity($this->getEntity());
		$systemdata->init();
		$smtp->setSystemdata($systemdata);
		
        //$smtp->setUsersender($this->getUsersender());
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        //$badiuSession->setHashkey($this->getSessionhashkey());
        $entity= $badiuSession->get()->getEntity();

        $entity=$this->getEntity();
        $smtp->setEntity($entity);
        $result=$smtp->sendMail($mconfig);

        return $result;
    }
    /*
    function sendMail($email, $recoverykey) {
        $smtphost = $this->getContainer()->getParameter('mailer_host');
        $smtpport = $this->getContainer()->getParameter('mailer_port');
        $smtpuser = $this->getContainer()->getParameter('mailer_user');
        $smtppaswword = $this->getContainer()->getParameter('mailer_password');
        $smtpfromaddress = $this->getContainer()->getParameter('mailer_from');
        $smtpfromname = $this->getContainer()->getParameter('mailer_fromname');

        $transport = \Swift_SmtpTransport::newInstance($smtphost, $smtpport)
                ->setUsername($smtpuser)
                ->setPassword($smtppaswword);

        $mailer = \Swift_Mailer::newInstance($transport);

        $message = \Swift_Message::newInstance('Recuperação de senha')
                ->setFrom(array($smtpfromaddress => $smtpfromname))
                ->setContentType("text/html")
                //->setTo(array('receiver@domain.org', 'other@domain.org' => 'A name'))
                ->setTo(array($email))
                ->setCharset('UTF-8')
                //->setBody('Here is the message itself');
                ->setBody($this->getContainer()->get('templating')->render('BadiuSystemUserBundle:Recoverpassword:email.html.twig', array('recoverykey' => $recoverykey)));

        // Send the message
        $result = $mailer->send($message);

        return $result;
    }
    */

    function getEmail() {
        return $this->email;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function getEntity() {
        return $this->entity;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function getIssserivecetoken() {
        return $this->issserivecetoken;
    }

    function setIssserivecetoken($issserivecetoken) {
        $this->issserivecetoken = $issserivecetoken;
    }

}
