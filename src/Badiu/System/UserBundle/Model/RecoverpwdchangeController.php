<?php

namespace  Badiu\System\UserBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormError;
class RecoverpwdchangeController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
              
    public function addCheckForm($form,$data) {
           $check=true;
	   $newpassword=null;
           $newpasswodrconfirm=null;
           $cpwtoken=null;
         
         $dto=$data->getDto();
         if(isset($dto['newpassword'])){$newpassword=$dto['newpassword'];}
         if(isset($dto['newpasswodrconfirm'])){$newpasswodrconfirm=$dto['newpasswodrconfirm'];}
         if(isset($dto['cpwtoken'])){$cpwtoken=urldecode($dto['cpwtoken']);}
         
         if(empty($cpwtoken)){
              $form->get('newpassword')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.recoverpwdchange.tokenrequired')));
              return  false; 
          }
          
          $moduledata=$this->getContainer()->get('badiu.system.module.data.data');
        
          $param=array('bkey'=>'badiu.system.user','modulekey'=>'badiu.system.user.recoverpwd','shortname'=>$cpwtoken,'customint1'=>1);
          $exist=$moduledata->countNativeSql($param,false);
          if(!$exist){
              $form->get('newpassword')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.recoverpwdchange.tokennotvalid')));
              return  false; 
          }
         
          if($newpassword != $newpasswodrconfirm){
              $form->get('newpassword')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.recoverpwdchange.newpasswordconfirmcheck')));
              $check= false; 
         }
        else if(strlen($newpassword) < 5){
              $form->get('newpassword')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.recoverpwdchange.minlenth')));
              $check= false; 
         }
         
          return  $check;
       }
	   
	
    
	 public function save($data) {
               $dto=$data->getDto();
               $result=$this->changePwd($dto);
               $dto['changpwdsucess']=$result;
               return $dto;
    }    

    
    public function changePwd($dto) {
          $newpassword=null;
          $userid=null;
          $cpwtoken=null;
          $entity=$this->getContainer()->getParameter('badiu.system.access.defaultentity');
         if(isset($dto['newpassword'])){$newpassword=$dto['newpassword'];}
         if(isset($dto['cpwtoken'])){$cpwtoken=urldecode($dto['cpwtoken']);}
         
         //get entity
         $p=explode("|",$cpwtoken);
         if(isset($p[2])){$entity=$p[2];}
        
         //get user id
         $moduledata=$this->getContainer()->get('badiu.system.module.data.data');
         //$param=array('bkey'=>'badiu.system.user','modulekey'=>'badiu.system.user.recoverpwd','customint1'=>1); //review bkey field is not use as field in sqlnativeprocess
         $param=array('modulekey'=>'badiu.system.user.recoverpwd','customint1'=>1);
         
         $userid=$moduledata->getModuleinstanceByShortname($cpwtoken,$param);
        
         $moduleid=$moduledata->getIdByShortname($cpwtoken,$param=array());
         $userdata=$this->getContainer()->get('badiu.system.user.user.data');
         if($userid > 0){
             $param=array('id'=>$userid,'password'=>md5($newpassword));
             $result1=$userdata->updateNativeSql($param,false);
             $result2=$moduledata->updateNativeSql(array('id'=>$moduleid,'customint1'=>0),false);
             
             return $result1;
         }
      return false;    
    }
}
