<?php

namespace  Badiu\System\UserBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormError;
class UserController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    /*          
    public function addCheckForm($form,$data) {
           $check=true;
		 
         // if ($data->existEdit()) {
            //    $form->get('cpf')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.cpfnotvalid')));
//                $check= false; 
          //  }
           return  $check;
       }*/
	   
	 public function setDefaultDataOnAddForm($dto) {
            $defaultData=$this->getDefaultdata();
          if(!empty($defaultData)){
              foreach ($defaultData as $key => $value){
                  $dto[$key]=$value;
              }  
          }
         
        return $dto;
     }
	 
	 public function setDefaultDataOnOpenEditForm($dto) {
          $userlibform=$this->getContainer()->get('badiu.system.user.user.libform');
		  $id=$this->getContainer()->get('request')->get('id');
		  $dto=$userlibform->getDataFormEdit($id);
		  return $dto;
     }
    
	 public function save($data) {
		$dto=$data->getDto();
		$userlibform=$this->getContainer()->get('badiu.system.user.user.libform');
		$result=$data->setDto($userlibform->save($data));
		return $result;
    }   
	
	
}
