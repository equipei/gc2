<?php

namespace Badiu\System\EntityBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuAccessFilter;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EntityAccessFilter extends  BadiuAccessFilter {

	function __construct(Container $container) {
    parent::__construct($container);
    
     $this->orderdb=$this->getContainer()->get('badiu.financ.sell.sell.data');
	}

       public function exec(){
         $filter=true;
           
         $usermanageentity = $this->getContainer()->getParameter('badiu.system.access.usermanageentity');

         $badiuSession=$this->getContainer()->get('badiu.system.access.session');
         $dsession = $badiuSession->get();
         $currentuser=$dsession->getUser()->getId();
         if($usermanageentity!=$currentuser){
             echo 'Voce nao tem permissao para acessar essa area do sistema';
             $filter=false;
             echo exit; 
         }
     
        return $filter;
         }  
   
         
       
}
