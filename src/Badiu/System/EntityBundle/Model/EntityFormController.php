<?php

namespace Badiu\System\EntityBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EntityFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
            $this->setDbdefaultablevalue(false);
              }
    
     public function changeParamOnOpen() {
        
         if($this->isEdit()){
             $param=$this->getParam();
          
             $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
             $dconfig = $this->getJson()->decode($dconfig, true);
     
             $param['smtphost'] = $this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.host',true);
             $param['smtpport'] = $this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.port',true);
             $param['smtpuser'] = $this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.user',true);
             $param['smtppassword'] = $this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.password',true);
             $param['smtpsecurity'] = $this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.security',true);
             $param['smtpemailsender'] = $this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.emailsender',true);
             $param['smtpusersender'] = $this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.usersender',true);

             $this->setParam($param); 
         }
            
           
     }   
     
    public function changeParam() {

        $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
        if (empty($dconfig) || !is_array($dconfig)) {
            $dconfig = array();
        }
        $dconfig['defaultsmtp']['host']=$this->getParamItem('smtphost');
        $this->removeParamItem('smtphost');
        
        $dconfig['defaultsmtp']['port']=$this->getParamItem('smtpport');
        $this->removeParamItem('smtpport');
        
        $dconfig['defaultsmtp']['user']=$this->getParamItem('smtpuser');
        $this->removeParamItem('smtpuser');
        
        $dconfig['defaultsmtp']['password']=$this->getParamItem('smtppassword');
        $this->removeParamItem('smtppassword');
        
        $dconfig['defaultsmtp']['security']=$this->getParamItem('smtpsecurity');
        $this->removeParamItem('smtpsecurity');
        
        $dconfig['defaultsmtp']['emailsender']=$this->getParamItem('smtpemailsender');
        $this->removeParamItem('smtpemailsender');
        
        $dconfig['defaultsmtp']['usersender']=$this->getParamItem('smtpusersender');
        $this->removeParamItem('smtpusersender');
        
        $dconfig = $this->getJson()->encode($dconfig);
        
         if($this->isEdit()){ $param['timemodified']=new \DateTime();}
        else {$param['timecreated']=new \DateTime();}
        
        $param = $this->getParam();
        $param['dconfig'] = $dconfig;
        $this->setParam($param);
        
     }
     
      public function validation() {
          return null;
      }
    
}
