<?php

namespace Badiu\System\EntityBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class EntityData extends BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
        parent::__construct($container,$bundleEntity);
    }

}
