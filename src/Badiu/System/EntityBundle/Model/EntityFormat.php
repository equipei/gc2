<?php

namespace Badiu\System\EntityBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EntityFormat extends BadiuFormat {
        private $tcript;
         private $router;
	function __construct(Container $container) {
		parent::__construct($container);
                $this->tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
                 $this->router=$this->getContainer()->get("router");
	}

        public function access($data) {
            $id=$this->getUtildata()->getVaueOfArray($data,'id');
            $name=$this->getUtildata()->getVaueOfArray($data,'name');
            $token=$id.'______'.$name;
            $entitytoken=$this->tcript->encode('s1',$token);
            $lroute=$this->router->generate('badiu.system.core.service.process',array('_service'=>'badiu.auth.core.login.changeentity','_entitytoken'=>$entitytoken));
            $value="<a target=\"_blank\" href=\"$lroute\">Acessar</a>";
           return $value;
        }
        public function navactive($data) {
            
           
        }
        
   
}
