<?php

namespace Badiu\System\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemEntity
 *
 * @ORM\Table(name="system_entity", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_entity_shortname_uix", columns={"shortname"}),
 *      @ORM\UniqueConstraint(name="system_entity_idnumber_uix", columns={"idnumber"})}, 
 *       indexes={@ORM\Index(name="system_entity_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="system_entity_idnumber_ix", columns={"idnumber"}),
 *              @ORM\Index(name="system_entity_description_ix", columns={"description"}),
 *              @ORM\Index(name="system_entity_name_ix", columns={"name"}),
 *              @ORM\Index(name="system_entity_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="system_entity_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="system_entity_status_ix", columns={"statusid"}),
 *              @ORM\Index(name="system_entity_lastaccess_ix", columns={"lastaccess"}), 
 *              @ORM\Index(name="system_entity_useridadd_ix", columns={"useridadd"})})
 *          @ORM\Entity
 */
class SystemEntity {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

        /**
     * @var SystemEntityStatus
     *
     * @ORM\ManyToOne(targetEntity="SystemEntityStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
     private $statusid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string",length=255, nullable=true)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255,nullable=true)
	 */
	private $shortname;

        /**
	 * @var integer
	 *
	 * @ORM\Column(name="parentid", type="bigint", nullable=true)
	 */
	private $parentid;
	
		/**
	 * @var integer
	 *
	 * @ORM\Column(name="usercostumerid", type="bigint", nullable=true)
	 */
	private $usercostumerid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="enterprisecostumerid", type="bigint", nullable=true)
	 */
	private $enterprisecostumerid;
	
	
		/**
	 * @var string
	 *
	 * @ORM\Column(name="costumerstatus", type="string", length=50,nullable=true)
	 */
	private $costumerstatus;
	
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=255,nullable=true)
	 */
	private $description;

          /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted;
        
        /**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="lastaccess", type="datetime", nullable=true)
	 */
	private $lastaccess;

	public function getId() {
		return $this->id;
	}

	public function getIdnumber() {
		return $this->idnumber;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getParam() {
		return $this->param;
	}

	public function getTimecreated() {
		return $this->timecreated;
	}

	public function getTimemodified() {
		return $this->timemodified;
	}

	public function getUseridadd() {
		return $this->useridadd;
	}

	public function getUseridedit() {
		return $this->useridedit;
	}

	public function getDeleted() {
		return $this->deleted;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setIdnumber($idnumber) {
		$this->idnumber = $idnumber;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setParam($param) {
		$this->param = $param;
	}

	public function setTimecreated(\DateTime $timecreated) {
		$this->timecreated = $timecreated;
	}

	public function setTimemodified(\DateTime $timemodified) {
		$this->timemodified = $timemodified;
	}

	public function setUseridadd($useridadd) {
		$this->useridadd = $useridadd;
	}

	public function setUseridedit($useridedit) {
		$this->useridedit = $useridedit;
	}

	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}

	public function getDtype() {
		return $this->dtype;
	}

	public function setDtype($dtype) {
		$this->dtype = $dtype;
	}

	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}

	public function getShortname() {
		return $this->name;
	}
	public function setShortname($shortname) {
		$this->shortname = $shortname;
	}
        function getStatusid(){
            return $this->statusid;
        }

        function setStatusid(SystemEntityStatus $statusid) {
            $this->statusid = $statusid;
        }

        function getDconfig() {
            return $this->dconfig;
        }

        function setDconfig($dconfig) {
            $this->dconfig = $dconfig;
        }

        function getLastaccess(){
            return $this->lastaccess;
        }

        function setLastaccess(\DateTime $lastaccess) {
            $this->lastaccess = $lastaccess;
        }

        function getParentid() {
            return $this->parentid;
        }

        function setParentid($parentid) {
            $this->parentid = $parentid;
        }
	function getUsercostumerid() {
            return $this->usercostumerid;
        }

        function setUsercostumerid($usercostumerid) {
            $this->usercostumerid = $usercostumerid;
        }

	function getEnterprisecostumerid() {
            return $this->enterprisecostumerid;
        }

        function setEnterprisecostumerid($enterprisecostumerid) {
            $this->enterprisecostumerid = $enterprisecostumerid;
        }

	function getCostumerstatus() {
            return $this->costumerstatus;
        }

        function setCostumerstatus($costumerstatus) {
            $this->costumerstatus = $costumerstatus;
        }
}
