<?php

namespace Badiu\System\AccessBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class GlobalDataSession extends BadiuModelLib {

    /**
     * @var array
     */
    private $param=null;
     function __construct($container) {
        parent::__construct($container);
    }

public function init(){
		$entity=$this->getEntity(true);
		$param=array('type'=>'cache','entity'=>$entity);
		$cache=$this->getContainer()->get('badiu.system.core.lib.util.managecache');
		$cache->init($param);
		$this->param=$cache->getParam();
}
public function get($key)
    {
		 $modulekey=null;
		 $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		 if(!empty($parentid)){
			 $modulekey= $key.'.'.$parentid;
		 }
		 if(empty($this->param)){$this->init();}
		 if(!empty($modulekey) && array_key_exists($modulekey,$this->param)){
			 return $this->param[$modulekey];
		 }
		 if(array_key_exists($key,$this->param)){ return $this->param[$key]; }
		
		return null;
    }
		public function existKey($key) {
        $r=FALSE;
        if(!empty($this->param)){
           $r=array_key_exists($key,$this->param);
        } 
         return  $r;
     }
    function getParam() {
          return $this->param;
      }

   
      function setParam($param) {
          $this->param = $param;
      }
}
