<?php

namespace Badiu\System\AccessBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class DefaultPermissionRole extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function get($rolekey) {
         $result=null;
          $configkey='badiu.system.access.role.permission.'.$rolekey;
          if(!$this->getContainer()->hasParameter($configkey)){return null;}
          $lperms=$this->getContainer()->getParameter($configkey);
           if(empty($lperms)){return $result;}
           $pos=stripos($lperms, ",");
              if($pos=== false){
               $result=array();
               $result[0]=array('key'=>$lperms);
              }else{
                    $plist= explode(",",$lperms);
                    $cont=0;
                    foreach ($plist as $l) {
                         $result[$cont]=array('key'=>$l);
                         $cont++;
                    }
              }
           return $result;
    } 

}
