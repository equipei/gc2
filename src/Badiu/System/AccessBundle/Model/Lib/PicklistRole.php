<?php

namespace Badiu\System\AccessBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class PicklistRole extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function user(){
            $add=$this->getContainer()->get('request')->get('add');  
            $remove=$this->getContainer()->get('request')->get('rmv');  
	 
            if (isset($add)) {
		$this->addUser();
            }else if (isset($remove)) {
		 $this->removeUser();
            }
	 		
    }
	
   
    public function addUser(){
                        $fdata=$_GET['add'];
			$roleid=$this->getContainer()->get('request')->get('parentid'); 
			$roleuserdata=$this->getContainer()->get('badiu.system.access.roleuser.data');
                        //use native sql
			foreach ($fdata as $data){
				$userid=null;
				if(isset($data['id'])){$userid=$data['id'];}
				
                                //check if exist
                                $param=array('userid'=>$userid,'roleid'=>$roleid);
                                $exist=$roleuserdata->countNativeSql($param);
                                //add
                                if(!$exist){
                                    $param=array('userid'=>$userid,'roleid'=>$roleid,'deleted'=>false);
                                   $result=$roleuserdata->insertNativeSql($param);
                                }
				
			}
			
			
	}
      public function removeUser(){
                        $fdata=$_GET['rmv'];
			$enroldata=$this->getContainer()->get('badiu.ams.enrol.offer.data');
			foreach ($fdata as $id){
                               if(!empty($id)){
                                    $enroldata->remove($id);
                                }
				
			}
			
			
	}    
   
        
}
