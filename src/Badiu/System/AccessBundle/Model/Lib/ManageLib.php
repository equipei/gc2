<?php

namespace Badiu\System\AccessBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ManageLib extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function addUserRole($param) {
		$userid= $this->getUtildata()->getVaueOfArray($param, 'userid');
		$roleid= $this->getUtildata()->getVaueOfArray($param, 'roleid');
		$entity= $this->getUtildata()->getVaueOfArray($param, 'entity');
		$roleshortname= $this->getUtildata()->getVaueOfArray($param, 'roleshortname');
		
		if(empty($entity)){$entity=$this->getEntity();}
		
		if(empty($userid)){return null;}
		
		$rdata = $this->getContainer()->get('badiu.ams.role.role.data');
		if(!empty($roleshortname) && empty($roleid)){
			$roleid=  $rdata->getIdByShortname($entity,$roleshortname);
		}
	
		if(empty($roleid)){return null;}
		$sysroleid= $rdata->getSysroleid($roleid);
		if(empty($sysroleid)){return null;} 
		
      
		$result=0;  
		$roleuserdata= $this->getContainer()->get('badiu.system.access.user.data');
        if(!$roleuserdata->countNativeSql(array('entity'=>$entity,'roleid'=>$sysroleid,'userid'=>$userid),false)){
               $paramru=array('entity'=>$entity,'roleid'=>$sysroleid,'userid'=> $userid,'timecreated'=>new \DateTime(),'deleted'=>0); 
               $result=$roleuserdata->insertNativeSql($paramru, false);
        }
		return $result; 
	}
        
}
