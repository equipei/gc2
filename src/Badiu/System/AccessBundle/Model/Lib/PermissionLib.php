<?php

namespace Badiu\System\AccessBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class PermissionLib extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function add($param) {
          $roleshortname= $this->getUtildata()->getVaueOfArray($param, 'roleshortname');   
          $list= $this->getUtildata()->getVaueOfArray($param, 'permissions');   
          $entity= $this->getUtildata()->getVaueOfArray($param, 'entity');   
          if(empty($entity)){$entity=$this->getEntity();}
          $cont=0;
      
         $datarole = $this->getContainer()->get('badiu.system.access.role.data');
         $dataperm = $this->getContainer()->get('badiu.system.access.permission.data');
         $roleid=  $datarole->getIdByShortname($entity,$roleshortname);
         if(empty($roleid)){return 0;}
         foreach ($list as  $value) {
              $param=array();
              $param['entity']=$entity;
              $param['roleid']=$roleid;
              $param['pemissionkey']=$value;
              $param['timecreated']=new \DateTime();
              $param['deleted']=0;

              if(!$dataperm->countNativeSql(array('entity'=>$entity,'roleid'=>$roleid,'pemissionkey'=>$value),false)){
                  $result = $dataperm->insertNativeSql($param,false); 
                  if($result){$cont++;}
              }
         }
     return $cont;
     
    } 

	public function isKeyOfRoleWithEnablerestrictParentKey($key=null) {
		  if(empty($key)){
			 $routerlib = $this->getContainer()->get('badiu.system.core.lib.config.routerlib');
			 $key=$routerlib->getKey(); 
		  }
		  
		
		$permissiondata=$this->getContainer()->get('badiu.system.access.permission.data');
		$listperm=$permissiondata->getKeysRoleInfoByUserid($this->getEntity(),$this->getUserid());
		$countkey=0;
		$countkeyenablerp=0;
		if(!empty($listperm) && is_array($listperm)){
			foreach ($listperm as $row){
				 $pkey=$this->getUtildata()->getVaueOfArray($row,'key');
				 $enablerestrictparentkey=$this->getUtildata()->getVaueOfArray($row,'enablerestrictparentkey');
				 $pos=stripos($key,$pkey);
				 if($pos!== false){
					 $countkey++;
					 if($enablerestrictparentkey){$countkeyenablerp++;}
				}
			}
		
		}
		if($countkey > 0 && $countkey == $countkeyenablerp){return true;}
		return false;
	}
	
}
