<?php

namespace Badiu\System\AccessBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class BrowseLib extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    

	public function afterLoggingin($param=array()){
		$defaultroute='badiu.admin.client.client.frontpage';
		$url=null;
		$nextroutekey=$this->getUtildata()->getVaueOfArray($param,'nextroute.key',true);
		$nextrouteparam=$this->getUtildata()->getVaueOfArray($param,'nextroute.param',true);
		if(!empty($nextroutekey)){
			if(!is_array($nextrouteparam)){$nextrouteparam=array();}
			$url=$this->getUtilapp()->getUrlByRoute($defaultroleroute,$nextrouteparam);
			if(!empty($url)){return $url;}
		}
		$nexturl=$this->getUtildata()->getVaueOfArray($param,'nexturl');
		if(!empty($nexturl)){return $nexturl;}
		

		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		$badiuSession->setHashkey($this->getSessionhashkey());

		$urlreferer=$badiuSession->getUrlReferer(); 
		

		$defaultroleroute=$badiuSession->get()->getDefaultroute();
		$nextrouter=$badiuSession->getValue('badiu.auth.core.login.router.go.after.login');
		
		if(!empty($urlreferer)){$url=$urlreferer;}
        else{
                if(!empty($defaultroleroute)){$nextrouter=$defaultroleroute;}
                $url= $this->getUtilapp()->getUrlByRoute($nextrouter);
        }
       if(!empty($defaultroleroute)){
            $urlfrontpage= $this->getUtilapp()->getUrlByRoute('badiu.system.core.core.frontpage');
            if($urlfrontpage==$urlreferer){$url= $this->getUtilapp()->getUrlByRoute($defaultroleroute);}
             else  if($urlfrontpage."/"==$urlreferer){$url= $this->getUtilapp()->getUrlByRoute($defaultroleroute);}
        }

		if(empty($url)){$url= $this->getUtilapp()->getUrlByRoute($defaultroute);}

	
		return $url;
	}
        
}
