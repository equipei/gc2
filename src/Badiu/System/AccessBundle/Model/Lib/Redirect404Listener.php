<?php

namespace Badiu\System\AccessBundle\Model\Lib;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class Redirect404Listener
{
    /**
     * @var RouterInterface
     */
    private $router;

	private $container;
    /**
     * @var RouterInterface $router
     */
    public function __construct(RouterInterface $router, Container $container)
    {
        $this->router = $router;
		$this->container = $container;
    }

    /**
     * @var GetResponseForExceptionEvent $event
     * @return null
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // If not a HttpNotFoundException ignore
        if (!$event->getException() instanceof NotFoundHttpException) {
            return;
        }
		$this->startGuestSession(); 
		$badiuSession = $this->container->get('badiu.system.access.session');
		$error404route=$badiuSession->getValue('badiu.system.core.param.config.site.route.error404');
		if(empty($error404route)){return null;}
        // Create redirect response with url for the home page
		$this->container->get('request');
		$currenroute=$this->container->get('request')->getPathInfo();
	    $response = new RedirectResponse($this->router->generate($error404route,array('_currentroute'=>$currenroute)));

        // Set the response to be processed
        $event->setResponse($response);
    }
	
	 public function startGuestSession() {
       
                $badiuSession = $this->container->get('badiu.system.access.session');
				if ($badiuSession->exist()) {return false;}
				$sessiondata=$badiuSession->get();
				if ($sessiondata->getUser()->getAnonymous()){return false;}
				
				$badiuSession->start(null,true); 
				return null;

	}
}