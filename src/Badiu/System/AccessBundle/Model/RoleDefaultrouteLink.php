<?php

namespace Badiu\System\AccessBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class RoleDefaultrouteLink extends BadiuModelLib{

    function __construct(Container $container) {
            parent::__construct($container);
		    }
    
        
        public function exec() {
			
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $defaultroleroute=$badiuSession->get()->getDefaultroute();
			if(empty($defaultroleroute)){
				$permission=$this->getContainer()->get('badiu.system.access.permission');
				if($permission->has_access('badiu.system.core.core.frontpage',$this->getSessionhashkey())){
					echo "ssss";exit;
					$defaultroleroute='badiu.system.core.core.frontpage';
				}else{
					$defaultroleroute='badiu.admin.client.client.frontpage';
				}
			}
		    $url=$this->getUtilapp()->getUrlByRoute($defaultroleroute);
			header('Location: '.$url);
            exit;
			return null;
		  }
       
}
