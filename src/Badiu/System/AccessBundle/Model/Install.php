<?php

namespace Badiu\System\AccessBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Install extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
           $result= $this->initDbRole();
           $result+=$this->initDbCreateUserGuest();
		   $result+= $this->initPermissions();
    } 

     public function initDbRole() {
         $cont=0;
          $datarole = $this->getContainer()->get('badiu.system.access.role.data');
         $entity=$this->getEntity();
        $dataperm = $this->getContainer()->get('badiu.system.access.permission.data');
		
         //add admin
         $param=array();
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.system.access.role.default.admin');
         $param['shortname']='admin';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
		 
         if(! $datarole->existByShortname($entity,'admin')){
              $result =  $datarole->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         //add authenticateduser
         $param=array();
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.system.access.role.default.authenticateduser');
         $param['shortname']='authenticateduser';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         if(! $datarole->existByShortname($entity,'authenticateduser')){
              $result =  $datarole->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
		 //add permission authenticateduser
		 //permission added in amdmin/client
		/* $roleid=  $datarole->getIdByShortname($entity,'authenticateduser');
         $param=array();
         $param['entity']=$entity;
         $param['roleid']=$roleid;
         $param['pemissionkey']='badiu.admin.client';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;

         if(!$dataperm->countNativeSql(array('entity'=>$entity,'roleid'=>$roleid,'pemissionkey'=>'badiu.admin.client'),false)){
              $result = $dataperm->insertNativeSql($param,false); 
              if($result){$cont++;}
         }*/
		 
         //add guest
         $param=array();
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.system.access.role.default.guest');
         $param['shortname']='guest';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         if(! $datarole->existByShortname($entity,'guest')){
              $result =  $datarole->insertNativeSql($param,false); 
              if($result){$cont++;}
         }

         //add permission admin
         
         $roleid=  $datarole->getIdByShortname($entity,'admin');
          $param=array();
         $param['entity']=$entity;
         $param['roleid']=$roleid;
         $param['pemissionkey']='badiu';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;

         if(!$dataperm->countNativeSql(array('entity'=>$entity,'roleid'=>$roleid,'pemissionkey'=>'badiu'),false)){
              $result = $dataperm->insertNativeSql($param,false); 
              if($result){$cont++;}
         }

          //add permission guest

        return $cont; 
     }


     public function initDbCreateUserGuest() {
          $entity=$this->getEntity();
          $cont=0;
          $param=array();
          $param['entity']=$entity;
          $param['firstname']=$this->getTranslator()->trans('badiu.system.user.user.guest.firstname');
          $param['lastname']=$this->getTranslator()->trans('badiu.system.user.user.guest.lastname');
          $param['email']='guest'.$entity.'@guest.com';
          $param['username']='guest'.$entity;
          $param['auth']='manual';
          $param['shortname']='guest';
          $param['timecreated']=new \DateTime();
          $param['deleted']=0;
		  $param['confirmed']=1;

          $userdata= $this->getContainer()->get('badiu.system.user.user.data');
          if(! $userdata->existByShortname($entity,'guest')){
               $result=$userdata->insertNativeSql($param, false);
               if($result){$cont++;}
          }
        
 
          //add guest to role
          $datarole = $this->getContainer()->get('badiu.system.access.role.data');
          $roleid=  $datarole->getIdByShortname($entity,'guest');
          $userid=$userdata->getIdByShortname($entity,'guest');

          $roleuserdata= $this->getContainer()->get('badiu.system.access.user.data');
          if(!$roleuserdata->countNativeSql(array('entity'=>$entity,'roleid'=>$roleid,'userid'=>$userid),false)){
               $paramru=array('entity'=>$entity,'roleid'=>$roleid,'userid'=> $userid,'timecreated'=>new \DateTime(),'deleted'=>0);
               $result=$roleuserdata->insertNativeSql($paramru, false);
               if($result){$cont++;}
          }

         
          return $cont;
     }
	 
	
    public function initPermissions() {
        $cont=0;
		
        $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');

		//authenticateduser
		$param=array(
		  'entity'=>$this->getEntity(),
          'roleshortname'=>'guest',
          'permissions'=>array('badiu.auth.core.login.add',
          'badiu.system.user.recoverpwd.add',
          'badiu.system.user.recoverpwdchange.add',
		  'badiu.system.core.proxy404.link',
		  'badiu.system.core.service.process/badiu.system.core.functionality.form.service',
		 
		  )
        );
        $cont+=$libpermission->add($param);
	return $cont;
    }
}
