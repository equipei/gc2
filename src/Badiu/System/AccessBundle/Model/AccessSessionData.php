<?php

namespace Badiu\System\AccessBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class AccessSessionData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

	
 public function findByTkey($tkey) {
        $sql="SELECT  o.id,o.entity,o.sclient,o.dtype,o.status,o.dconfig,o.lastaccess,o.timetoexpire FROM ".$this->getBundleEntity()." o  WHERE o.tkey = :tkey  ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('tkey',$tkey);
        $result= $query->getOneOrNullResult();
        return  $result;
    }	
	
   /*
    *  public function findByTkey($entity,$userid) {
        $sql="SELECT  o FROM ".$this->getBundleEntity()." o INNER JOIN BadiuSystemAccessBundle:SystemAccessRoleUser rs WITH o.roleid=rs.roleid WHERE o.entity = :entity AND rs.userid=:userid ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('userid',$userid);
        $result= $query->getResult();
        return  $result;
    }	
	
    */
}
