<?php

namespace Badiu\System\AccessBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class UserRecordData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

	
 public function getModuleinstancesByUserid($entity,$userid) {
        $sql="SELECT  o.id,o.modulekey,o.moduleinstance FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.userid=:userid ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('userid',$userid);
        $result= $query->getResult();
        return  $result;
    }	
}
