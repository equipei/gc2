<?php
namespace Badiu\System\AccessBundle\Model\Auth;
use Badiu\GMoodle\CoreBundle\Model\Auth\AuthGMoodleVersion12;
use Badiu\System\AccessBundle\Model\Session\BadiuSession;
use Badiu\System\AccessBundle\Model\Session\BadiuSessionData;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;

class LoggedInUserListener {
	private $router;
	private $container;

	public function __construct($router, $container) {
		$this->router = $router;
		$this->container = $container;
	}
 
	public function onKernelRequest(GetResponseEvent $event) {
			              
               $manageremotesequest= $this->container->get('badiu.system.core.lib.util.manageremotesequest');
               $skipsession=$manageremotesequest->skipSession();
               if($skipsession){return null;}

				$request = $this->container->get('request');
				$currentroute = $request->get('_route');
				$routewithoutwession=$this->routeWithoutSession($currentroute);
			    if($routewithoutwession){return null;}
				
				$this->accessFilter();                     
                 $isguest=$this->startGuestSession();
                if($isguest){return null;}
            
				$this->proxyAnonymousDefaultRoute();
				 
                   $badiuSession = $this->container->get('badiu.system.access.session');
		           
                    $isloginpage = FALSE;
                    $isroutloginpage = FALSE; 
                   
                    $referer = $request->getUri();
                    $referer = $request->headers->get('referer');
                    if (empty($referer)) {$referer = $request->getUri();}
                    $badiuSession->saveUrlReferer($referer);

                    

                    if ($currentroute == "badiu.auth.core.login.add") {$isroutloginpage = TRUE;}
                   
                    //token login
                    $ltoken = $this->container->get('badiu.auth.core.login.token');
                    $ltoken->startSession(); //review
                   
                
                $this->proxyDefaultRoute($event);
		

	}
   
	public function exceptionList() {
        $badiuSession = $this->container->get('badiu.system.access.session');
		$listkeys=$badiuSession->getValue('badiu.system.core.param.config.routersguestpermission');
		$listkeys=preg_split("/\r\n|\n|\r/", $listkeys);
		if(!is_array($listkeys)){$listkeys= array();}
		return $listkeys;
		
	}
	public function startGuestSession() {
       
                $badiuSession = $this->container->get('badiu.system.access.session');
				if ($badiuSession->exist()) {return false;}
				$sessiondata=$badiuSession->get();
				if ($sessiondata->getUser()->getAnonymous()){return false;}
				
        $listException = $this->exceptionList();
        
		$request = $this->container->get('request');
		$currentroute = $request->get('_route');
                
                $hasException = false;
                if(empty($currentroute)){return true;}
		foreach ($listException as $list) {
			if ($list == $currentroute) {$hasException = true;break;}
		}
		
        $badiuSession->start(null,true); 
	
		if ($hasException) {
           
            $badiuSession = $this->container->get('badiu.system.access.session');
			$sessiondata=$badiuSession->get();
			if($sessiondata->getUser()!=null && $sessiondata->getUser()->getAnonymous()){return $hasException;}

          
		}  
		
              return $hasException;

	}
	public function validateToken() {
		$request = Request::createFromGlobals();
		$token = $request->query->get('badiusysiapptoken');

		if (!empty($token)) {
			$auth = new AuthGMoodleVersion12();
			$auth->setToken($token);
			$auth->initVars();
			$em = $this->container->get('doctrine')->getManager();
			$auth->setEm($em);
			if ($auth->validateToken()) {
				$sessaionData = new BadiuSessionData();
				$sessaionData->setUserid($auth->getUserid());
				$sessaionData->setDbapp($auth->getAppname());
				$sessaionData->setEntity($auth->getEntityid());
				$sessaionData->setTheme("");
				$sessaionData->setUserfullname("");

				$badiuSession = new BadiuSession($this->container);
				$badiuSession->save($sessaionData);

				$auth->deleteToken();
				return "token_ok";
			} else {return "token_not_valid";}

		}
		return "no_token";
	}

		 public function proxyAnonymousDefaultRoute() {
			 $request = $this->container->get('request');
			$currentroute = $request->get('_route');
           
		    $badiuSession = $this->container->get('badiu.system.access.session');
			 $datasession = $badiuSession->get();
			
			$utilapp=$this->container->get('badiu.system.core.lib.util.app');
			  //frontpagewihtoulogin
			 if((!$badiuSession->exist() || $datasession->getUser()->getAnonymous())){
				$frontpagewihtoulogin=$badiuSession->getValue('badiu.system.core.param.config.site.route.frontpagewihtoulogin');
				
				if(!empty($frontpagewihtoulogin) &&  $currentroute=='badiu.system.core.core.frontpage'){
					$url= $utilapp->getUrlByRoute($frontpagewihtoulogin);
					header('Location: '.$url);
					exit;
				}
			 }
			 
		 }
        public function proxyDefaultRoute($event) {
			
            $request = $this->container->get('request');
			$currentroute = $request->get('_route');
           
		    $badiuSession = $this->container->get('badiu.system.access.session');
		   $datasession = $badiuSession->get();
			 
			
            if($currentroute=='badiu.system.core.core.frontpage'){
				$permission=$this->container->get('badiu.system.access.permission');
                if($permission->has_access("badiu.system.module.module.add")){
					return null;
				}
               
                if(!$badiuSession->exist()){return null;}
               
                
                $url = $datasession->getDefaultroute();
               
				if(!empty($url)){
                    $url= $this->router->generate($url);
                    $event->setResponse(new RedirectResponse($url));
                 }
                  
            }
        }
     
        public function serviceRequestLogin() {
          $bresponse= $this->container->get('badiu.system.core.lib.util.jsonsyncresponse');
           $mresponse=$bresponse->denied('badiu.system.access.login.required','your are not logged in');  
           $jresponse =json_encode($mresponse);
           $response = new Response($jresponse);
      
        //$response->headers->set('HTTP/1.1 200 OK');
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Codingpedia,Authorization');
    
         return $response;
        }
        
        public function accessFilter() {
            $badiuSession = $this->container->get('badiu.system.access.session');
           
			$this->systemAccessFilter();  
          
			$routerlib = $this->container->get('badiu.system.core.lib.config.routerlib');
			$currentroute =$routerlib->getKey(); 
			 
            $keyutil=$this->container->get('badiu.system.core.lib.config.keyutil');
            $ckey1=$keyutil->removeLastItem($currentroute);
            $keyaccesfilter=$ckey1.'.accessfilter';
         
             if($this->container->has($keyaccesfilter)){
                $afilter=$this->container->get($keyaccesfilter);
                $result=$afilter->exec();
                if(!$result){echo "bloqueio de filtro";exit;}
            }else{
                $ckey2=$keyutil->removeLastItem($ckey1);
                $keyaccesfilter=$ckey2.'.accessfilter';
                 if($this->container->has($keyaccesfilter)){
					 $afilter=$this->container->get($keyaccesfilter);
                    $result=$afilter->exec();
                    if(!$result){echo "bloqueio de filtro";exit;}
                }else{
                    $ckey3=$keyutil->removeLastItem($ckey2);
                    $keyaccesfilter=$ckey3.'.accessfilter';
                     if($this->container->has($keyaccesfilter)){
                         $afilter=$this->container->get($keyaccesfilter);
                        $result=$afilter->exec();
                        if(!$result){echo "bloqueio de filtro";exit;}
                    }
                }
            }
        }
   
   
    public function systemAccessFilter() {
		 $badiuSession = $this->container->get('badiu.system.access.session');
         $userforceupdateprofile=$badiuSession->getValue('badiu.system.core.param.config.userforceupdateprofile');
		
		 if($userforceupdateprofile){
			$userupdateprofileservicefilter=	 $badiuSession->getValue('badiu.system.core.param.config.userupdateprofileservicefilter');
			if($this->container->has($userupdateprofileservicefilter)){
				 $useraccessfilter=$this->container->get($userupdateprofileservicefilter);
				$useraccessfilter->exec();  
			}
		 }
		  
		
	}

 public function routeWithoutSession($currentroute) {
	 if(empty($currentroute)){return false;}
	 if(!$this->container->hasParameter('badiu.system.core.route.withoutsession')){return false;}
	 $routes=$this->container->getParameter('badiu.system.core.route.withoutsession');
	 
	 $utildata = $this->container->get('badiu.system.core.lib.util.data');
	 $list=$utildata->castStringToArray($routes);
	 
	 if (is_array($list) && in_array($currentroute, $list)) {return true;}
	 return false;
 }
}