<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoggedInUserListener
 *
 * @author lino
 */
namespace Badiu\System\AccessBundle\Model\Session;

class BadiuSessionDataConfig {
  
    /**
     * @var array
     */ 
     private $param;
   
    function __construct() {
        $this->param=array();
    }
     public function add($key,$value) {
        if(!isset($this->param) || (empty($this->param))){$this->param=array();}
         $this->param[$key]=$value;
     }
     
   public function existKey($key) {
        $r=FALSE;
        if(!empty($this->param)){
           $r=array_key_exists($key,$this->param);
        } 
         return  $r;
     }
     
     public function existValue($value) {
          $r=FALSE;
        if(!empty($this->param)){
           $r=in_array($value,$this->param);
        } 
       
         return  $r;
     }
     
      public function getValue($key) {
          $r="";
        if($this->existKey($key)){
           $r=$this->param[$key];
        } 
        return  $r;
     }
     
     public function exist() {
         $cont=0;
         foreach ($this->param as $value) {
             $cont++;
             if($cont==1){return 1;}
         }
       
        return   $cont;
     }
     
     public function getParam() {
         return $this->param;
     }

     public function setParam($param) {
         $this->param = $param;
     }



   


}
