<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoggedInUserListener
 *
 * @author lino
 */
namespace Badiu\System\AccessBundle\Model\Session;
class BadiuSessionUser {
  
   
    /**
     * @var integer
     */
   private $id; 
    
   
    /**
     * @var string
     */
   private $firstname;
   
    /**
     * @var string
     */
   private $lastname;
   
   /**
     * @var string
     */
   private $fullname;
   
   private $alternatename;
   
    private $systemname;
   /**
     * @var string
     */
   private $username;
   
   /**
     * @var string
     */
   private $email;
     
    /**
     * @var string
     */
   private $anonymous=false;
    public function __construct()
    {
      $this->firstname=null;
      $this->lastname=null;
      $this->id=null;
      $this->email=null;
	  $this->alternatename=null;
	  $this->systemname=null;
    }    


    public function getId() {
        return $this->id;
    }

    public function getFirstname() {
        return $this->firstname;
    }

    public function getLastname() {
        return $this->lastname;
    }

    public function getFullname() {
        return $this->fullname;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    public function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    public function setFullname($fullname) {
        $this->fullname = $fullname;
    }
    function getAnonymous() {
        return $this->anonymous;
    }

    function setAnonymous($anonymous) {
        $this->anonymous = $anonymous;
    }

    function getUsername() {
        return $this->username;
    }

    function getEmail() {
        return $this->email;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setEmail($email) {
        $this->email = $email;
    }

public function  setAlternatename($alternatename) {
        $this->alternatename=$alternatename;
    }
	
	 public function getAlternatename() {
        return $this->alternatename;
    }


public function  setSystemname($systemname) {
        $this->systemname=$systemname;
    }
	 
	 public function getSystemname() {
        return $this->systemname;
    }
}
