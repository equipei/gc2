<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoggedInUserListener
 *
 * @author lino
 */

namespace Badiu\System\AccessBundle\Model\Session;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\BadiuSystemConstant;
use Badiu\System\AccessBundle\Model\Session\BadiuSessionData;

class BadiuSession {
 
    /**
     * @var Container
     */
    private $container;
    
     /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $hashkey;
	
	private $globaldata=null;  
	/**
     * @var integer
     */
    private $entity;
	private $systemdata;	
    public function __construct(Container $container) {
        $this->container=$container;
       }
    public function initHashkey($prefix) {
       
        $now=time();
        $hash = $this->getContainer()->get('badiu.system.core.lib.util.hash');
        $hkey = $hash->make(30);
        $k=$prefix."-".$now."-".$hkey;
        $this->hashkey=$k;
        return $this->hashkey;
    }
    /**
     * Check key session is generate from cron. If not, lock execution 
      * @return void
     */
    public function checkSessionType() {
        $ckey=$this->getHashkey();
        return null;
        $currentroute = $this->getContainer()->get('request')->get('_route');
        if($currentroute=='badiu.system.scheduler.task.cron' && $ckey=='defautwebuser'){
            $logcron= $this->getContainer()->get('badiu.system.log.core.lib.cron');
            $logcron->add("EXECSTOPED Cron execed with defautwebuser. Exec is locked sessionkey: $ckey " ,array('self'=>1));
          // echo 'Cron execed with defautwebuser. Exec is locked';exit;
        }
      
      $service=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_service');
        if($service=='badiu.system.scheduler.task.cron' && $ckey=='defautwebuser'){
            //echo 'Manual cron execed with defautwebuser. Exec is locked';exit;
        }else   if($service=='badiu.system.core.functionality.content.service' && $ckey=='defautwebuser'){
           // $error=error_get_last();
           // if(!empty($error)){echo print_r($error);}
           // echo 'User moodle navegation with defautwebuser. Exec is locked';//exit;
        }else   if($service=='badiu.auth.core.webservice.navegation' && $ckey=='defautwebuser'){
            echo 'User moodle auth with defautwebuser. Exec is locked';exit;
        }
      

    }
    public function addCronlogScheduler($sessiondata,$skey,$msg,$param=array()) {
		return null;
        $iscronlogscheduler=$sessiondata->getIscronlogscheduler();
        if(!$iscronlogscheduler){return null;}

        $info="";
        if(count($param)>0){
            $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
            $k=$utildata->getVaueOfArray($param,'key');
            $v=$utildata->getVaueOfArray($param,'value');
            if(is_array($v)){$v=json_encode($v);}
            $info=" | key: $k | value: $k";
        }

        $currentroute = $this->getContainer()->get('request')->get('_route');
        if($currentroute=='badiu.system.scheduler.task.cron'){
            $logcron=$this->getContainer()->get('badiu.system.log.core.lib.cron');
            $entity=$sessiondata->getEntity();
            $logcron->add("$msg | entity: $entity | sessionkey: $skey $info",array('self'=>1));
        }
    }
    public function start($param,$guest=false) {
		//skip setup
		
		if($guest && $this->getContainer()->get('request')->get('_route')=='badiu.system.core.update.link'){return null;}
        
        $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
		$entity =null;
		$userid=null;
        $firstname=null;
        $lastname=null;
        $email=null;
        $username=null;
		$enablealternatename=null;
		$alternatename=null;
        if(!$guest){
            $userid=$utildata->getVaueOfArray($param,'userid');
            if(empty($userid)){ $userid=$utildata->getVaueOfArray($param,'id');}
            $firstname=$utildata->getVaueOfArray($param,'firstname');
            $lastname=$utildata->getVaueOfArray($param,'lastname');
            $email=$utildata->getVaueOfArray($param,'email');
            $username=$utildata->getVaueOfArray($param,'username');
            $entity=$utildata->getVaueOfArray($param,'entity');
			$enablealternatename=$utildata->getVaueOfArray($param,'enablealternatename');
			$alternatename=$utildata->getVaueOfArray($param,'alternatename');
		
        }else{
			 $entity=$utildata->getVaueOfArray($param,'entity');
			
			 if(empty($entity)){
				$entity =  $this->container->getParameter('badiu.system.access.defaultentity');
				if(empty($entity)){$entity=1;}
			 }
            $userdata=$this->getContainer()->get('badiu.system.user.user.data');
            $paramfilter=array('entity'=>$entity,'shortname'=>'guest');
            $column=" o.id,o.entity,o.firstname,o.lastname,o.alternatename,o.enablealternatename,o.email,o.auth,o.username,o.idnumber,o.shortname,o.doctype,o.docnumber,o.ukey ";
			
            $param=$userdata->getGlobalColumnsValue($column,$paramfilter);

            $userid=$utildata->getVaueOfArray($param,'id');
            $firstname=$utildata->getVaueOfArray($param,'firstname');
            $lastname=$utildata->getVaueOfArray($param,'lastname');
			$enablealternatename=$utildata->getVaueOfArray($param,'enablealternatename');
			$alternatename=$utildata->getVaueOfArray($param,'alternatename');
			$lastname=$utildata->getVaueOfArray($param,'lastname');
            $email=$utildata->getVaueOfArray($param,'email');
            $username=$utildata->getVaueOfArray($param,'username');
          
        }
         
       
         
         //get session data
        $dsession=$this->getContainer()->get('badiu.system.access.session.data');
        $permd=$this->getContainer()->get('badiu.system.access.permission.data');
        $keyperms=$permd->getKeysByUserid($entity,$userid);
        
        $uroles=$this->getRoles($entity,$userid);
        $dsession->setRoles($uroles);
        
        $defaultroute=$this->getDefaultroute($entity,$userid);
      
        //if(empty($defaultroute)){$defaultroute="badiu.admin.client.client.frontpage";}
        $dsession->setDefaultroute($defaultroute);

        $accessedby=$utildata->getVaueOfArray($param,'accessedby');
        $dsession->setAccessby($accessedby);
        $keyperms=$this->getContainer()->get('badiu.system.access.permission')->getKeys( $keyperms);
        
		if(!$guest){
		
			$keypermsautuser=$permd->getKeysByRole($entity,'authenticateduser');
			$keypermsguestuser=$permd->getKeysByRole($entity,'guest');
			$accesspermission=$this->getContainer()->get('badiu.system.access.permission');
			$keyperms=$accesspermission->addKeys($keyperms,$keypermsautuser);
			$keyperms=$accesspermission->addKeys($keyperms,$keypermsguestuser);
		}
		
        $dsession->setPermissions($keyperms);
        $dsession->setPermissionsdenied(array());
        //record permission
         $listrecord=$this->getContainer()->get('badiu.system.access.userrecord.data')->getModuleinstancesByUserid($entity,$userid);
         $dsession->setRecordpermissions($listrecord);
                  
         $dsession->setEntity($entity);
         $dsession->getUser()->setId($userid);
         $dsession->getUser()->setFirstname($firstname);
         $dsession->getUser()->setLastname($lastname);
         $dsession->getUser()->setFullname($firstname.' '.$lastname);
         $dsession->getUser()->setEmail($email);
         $dsession->getUser()->setUsername($username);
		 $dsession->getUser()->setAlternatename($alternatename);
		 $dsession->getUser()->setSystemname($dsession->getUser()->getFullname());
         if($guest){ $dsession->getUser()->setAnonymous(true);}
         $this->save($dsession);
        
		
        //$this->initConfigEntity();
		$this->initCache(array('entity'=>$entity));
		$theme=$this->getValue('badiu.system.core.param.config.site.theme');
		
		if(!empty($theme)){
			$dsession->setTheme($theme);
			$this->save($dsession);
		}
		
		//alternate name
		$showalternatenameinuserprofile=$this->getValue('badiu.system.core.param.config.showalternatenameinuserprofile');
		
		if($showalternatenameinuserprofile && $enablealternatename && !empty($alternatename)){$dsession->getUser()->setSystemname($dsession->getUser()->getAlternatename()); $this->save($dsession);}
    }  
    public function get() {
          $this->checkSessionType();
          $session =$this->container->get('session');
          $data=null;
          $skey='_badiu.system.data.session_'.$this->getHashkey();
          $data=$session->get($skey);
         if(empty($data)){$data=new BadiuSessionData();}
         
         $this->addCronlogScheduler($data,$skey,'Session requested');
          return $data;
     }  
     public function save(BadiuSessionData $data) {
          $this->checkSessionType();
          $session =$this->container->get('session');
          $skey='_badiu.system.data.session_'.$this->getHashkey();
          $session->set($skey,$data);

          $this->addCronlogScheduler($data,$skey,'Session saved');
        
        
     }
     public function saveUrlReferer($url) {
        $urlbase=$this->getContainer()->get('badiu.system.core.lib.util.app')->getBaseUrl();
		
		//replace $url to https if $base is https
		if( (strpos($urlbase, 'https://') === 0) && (strpos($url, 'https://') !== 0)){
			$url=preg_replace('/^http:\/\//', 'https://', $url);
		}
		
        $urlrecoverpwd=$urlbase."/system/user/recoverpwd";
        $recoverpwdchange=$urlbase."/system/user/recoverpwdchange";
		$systemupdate=$urlbase."/system/update";
		$urllogin=$urlbase."/login";
		$urllogin1=$urlbase."/auth/core/login";
		$urlcorecommand=$urlbase."/system/core/command";
		
        if($url==$urlrecoverpwd){return null;}
		if($url==$systemupdate){return null;}
		if($url==$urllogin){return null;}
		if($url==$urllogin1){return null;}
		if($url==$urlcorecommand){return null;}

        $pos=stripos($url,$recoverpwdchange);
        if($pos!== false){return null;}
        
        //check if is external link
        $pos=stripos($url,$urlbase);
        if($pos=== false){return null;}  
        
        if($this->exist()) return null;
      
        $router = $this->container->get('router');
         
         $urllogin=$router->generate("badiu.auth.core.login.add");  
         $urllogin=$this->container->get('request')->getSchemeAndHttpHost().$urllogin;

         
         //$urlfrontpage=$router->generate("badiu.system.core.core.frontpage");
         ///$urlfrontpage=$this->container->get('request')->getSchemeAndHttpHost().$urlfrontpage;
         
         
          $session =$this->container->get('session');
        
         if(!empty($url) && $url!=$urllogin){
             $session->set(BadiuSystemConstant::$SESSION_DATA_URL_REFERER, $url);
             
         }
      
         return  null;
     }
       public function getUrlReferer() {
            $session =$this->container->get('session');
           $surlreferer=$session->get(BadiuSystemConstant::$SESSION_DATA_URL_REFERER);
           
           return $surlreferer;
       }
     public function delete() {
        $this->checkSessionType();
        $skey='_badiu.system.data.session_'.$this->getHashkey();
        $data=$this->get();
        $session =$this->container->get('session');
        $session->set($skey, null);
         $session->set(BadiuSystemConstant::$SESSION_DATA_URL_REFERER, null);

         $this->addCronlogScheduler($data,$skey,'Session deleted');
     }
    
     public function exist() {
       $sessionData=$this->get();
       if($sessionData!=null && $sessionData->getUser()!=null && $sessionData->getUser()->getId()!=null && !$sessionData->getUser()->getAnonymous()) return true;
        return false;
    }
	public function isRootUser() {
       if(!$this->exist()){return false;}
	   $usermanageentity = $this->getContainer()->getParameter('badiu.system.access.usermanageentity');
	   $sessionData=$this->get();
	
	   $currentuser=$sessionData->getUser()->getId();
       if($usermanageentity==$currentuser){return true;}
	   return false;
    }
	public function getRootUser() {
	  if(!$this->getContainer()->hasParameter('badiu.system.access.usermanageentity')){return 1;}
       $usermanageentity = $this->getContainer()->getParameter('badiu.system.access.usermanageentity');
	   return  $usermanageentity;
  }
    //review service badiu.ams.core.lib.lmsmoodledatautil
public function getParamExpression($expression) {
		 $sessionData=$this->get();
		if($expression=='__session_userid'){
	
			return $sessionData->getUser()->getId();
		}else if($expression=='__session_lang'){ 
			return $sessionData->getLang();
		}
		else if($expression=='__session_langparam'){ 
				 $lang = $this->container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('lang');	
				 if(empty( $lang)){ $lang=$sessionData->getLang();}
				return $lang;
		}
		else if($expression=='__session_defaultlmsuserid'){
                    if($this->container->has('badiu.moodle.core.lib.datautil')){
                        $lmsdefaultuserid=$this->container->get('badiu.moodle.core.lib.datautil')->getSessionDefualtlmsUserid();
                        return $lmsdefaultuserid;
                    }
                }else if($expression=='__session_defaultlms'){ 
                    if($this->container->has('badiu.moodle.core.lib.datautil')){
                        $lmsdefault=$this->container->get('badiu.moodle.core.lib.datautil')->getSessionDefualtlms();
                        return $lmsdefault;
                    }
                }else{ 
                    $pos=stripos($expression, "__session_");
                    if($pos=== false){return '';}
                    $key=str_replace("__session_","",$expression);
                    $value=$this->getValue($key);
                  
                    if($value==null || $value==""){return -1;}
                    return $value;
                }
      return '';
    }
    
     public function addValue($key,$value) {
          $datasession = $this->get();
          $datasession->getConfig()->add($key,$value);
          $this->save($datasession);
     
     }
     public function getValue($key,$level='all') {
		   if($level=='cache'){
			   $v=$this->getGlobaldata()->get($key);
			   return 	$v;		   
		   }
		    if($level=='session'){
				$v=$datasession->getConfig()->getValue($key);
				return 	$v;
			}
				
          $datasession = $this->get();
		  if($this->existValue($key)){
			   $v=$datasession->getConfig()->getValue($key);
		  }else{
			  if(!empty($this->getSystemdata())){return $this->getSystemdata()->getManagecache()->getCache($key);}
			 
			
			 $modulekey=null;
			$parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
			 if(!empty($parentid)){
				$modulekey= $key.'.p.'.$parentid;
				if($this->getGlobaldata()->existKey($modulekey)){return $this->getGlobaldata()->get($modulekey);}
			 }
			 
			 $v=$this->getGlobaldata()->get($key);
		  }
         
        return $v;
     }
     public function existValue($key) {
          $datasession = $this->get();
          $v=$datasession->getConfig()->existKey($key);
        return $v;
     }

	public function addValueTime($key,$value,$timecache) {
          return null;  //review it
          $datasession = $this->get();
          $datasession->getConfig()->add($key,$value);
		  $datasession->getConfig()->add($key.'_time',time()+$timecache);
          $this->save($datasession);
     
     }
     public function getValueTime($key) {
		  $result=new \stdClass();
		  $result->expire=true;
          $datasession = $this->get();
          $vt=$datasession->getConfig()->getValue($key.'_time');
		
		  if(empty($vt)){$result->expire=true;}
		
		  else if( $vt >=time()){$result->expire=false;}
		  $result->value=$datasession->getConfig()->getValue($key);
		  
         
         // $skey='_badiu.system.data.session_'.$this->getHashkey();
         // $this->addCronlogScheduler($datasession,$skey,'Session getvalue',array('key'=>$key,'value'=>$v));

        return $result;
     }
     public function addValueOff($key,$value) {
        $session =$this->container->get('session');
        $session->set($key,$value);
    }
   public function getValueOff($key) {
        $session =$this->container->get('session');
       $v= $session->get($key);
        return $v;
   }
   public function deleteValueOff($key) {
         $session =$this->container->get('session');
         $session->set($key,null);
   }
   public function existValueOff($key) {
        $datasession = $this->get();
        $v=$datasession->getConfig()->existKey($key);
        if($v==null || $v=""){ return false;}
      return true;
   }
     public function addPackageValue($package,$key,$value) {
          $pvalue=$this->getValue($package);
          if(empty($pvalue)){$pvalue=array();}
          $pvalue[$key]=$value;
          $this->addValue($key,$value);
     }
     public function getPackageValue($package,$key) {
          $pvalue=$this->getValue($package);
          $value=null;
          if(array_key_exists($key,$pvalue)){
              $value=$pvalue[$key];
          }
          
        return $value;
     }
     public function existPackageValue($package,$key) {
         $pvalue=$this->getValue($package);
         $value=array_key_exists($key,$pvalue);
         return $value;
     }
     public function changeTheme($theme) {
          $session =$this->container->get('session');
          $data=$this->get();
          $data->setTheme($theme);
          $this->save($data);
        
     }
     
     public function initConfigKeyFromDb($basekey, $moduleinstance) {
        $data=$this->get();
        $manageconfig = $this->container->get('badiu.system.module.lib.manageconfig');
        $manageconfig->setSessionhashkey($this->getHashkey());
        $pconf=$manageconfig->getByBaseKey($basekey, $moduleinstance);
        $cont=0;
        foreach ($pconf as $key => $value) {
                 $data->getConfig()->add($key,$value);
                 $cont++;
            }
      
        $this->save($data);
        return $cont;
      
   }
   
     public function initConfigFromDb() {
        $cont=$this->initConfigEntity();
         return $cont;
        
     }
   
     public function initConfigFromFile() {
           $data=$this->get();
          $pconf=array();
          
          $bundles = $this->container->getParameter('kernel.bundles');
          $param = array('importclient' => true, 'importcustom' => false, 'importextended' => false);
           $kparamconfig = $this->container->get('badiu.system.core.lib.config.paramconfig');
           $cont=0;
          
           foreach ($bundles as $key => $value) {
               $config = $this->container->get('badiu.system.core.config');
               $config->setBundle($key);
               $dconfig = $config->getDefaulFormConfig($param);
               foreach ($dconfig as $key => $value) {
                   $ctype = $kparamconfig->getType($value);
                   $cdefault = $kparamconfig->getDefault($value);
                   if ($ctype=='config') {
                         $value = $cdefault;
                    }
                    $pconf[$key]=$value;
                    $cont++;
               }
           }
           
          $data->getConfig()->setParam($pconf);
          $this->save($data);
        return $cont;
     }
     //review it same code with  initConfigFromDb()
     public function initConfigEntity_old() {
       //echo "sssssssss5";exit; 
        $data=$this->get();
        $entity=$data->getEntity();
        $manageconfig = $this->container->get('badiu.system.module.lib.manageconfig');
        $manageconfig->setSessionhashkey($this->getHashkey());
        $pconf=$manageconfig->getGlabalAndEntity($entity);
		/*$globaldatasession= $this->container->get('badiu.system.access.globaldatasession');
		$gs=array();
		$gs[$entity]['systemdata']=$pconf;
		$globaldatasession->setParam($gs);*/
		$data->getConfig()->setParam($pconf);
        $this->save($data);
        $cont=sizeof($pconf);
         return $cont;
 
        
}
 
    public function initConfigInstance($force=false) {
        $data=$this->get();
        $entity=$data->getEntity();
            //get instance 
        $parentid=$this->container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
        if(empty($parentid)){return null;}
        if(!$force){$force=$this->container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_force'); }
        
        $router=$this->getContainer()->get('request')->get('_route');
        $key=$this->container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_key');
        $dkey=$this->container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey');
        
        if(!empty($key)){
            $router=$key;
        }
        if(!empty($dkey) && empty($key)){
            $router=$dkey;
        }
       
        $modulekey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($router);
      
       
        $skey=$router.".__init.instance.session.__$parentid";
        
        $existpkey= 0;//$this->existValue($skey);
    
        if(!$existpkey){
             $data=$this->get();
            $manageconfig = $this->container->get('badiu.system.module.lib.manageconfig');
            $manageconfig->setSessionhashkey($this->getHashkey());
            $pconf=$manageconfig->getInstance($modulekey,$parentid,$entity);
            $pconf[$skey]=1;
            foreach ($pconf as $key => $value) {
                $this->addValue($key,$value);
            }
           $cont=sizeof($pconf);
          return $cont;
        }else if($force){
            $data=$this->get();
            $manageconfig = $this->container->get('badiu.system.module.lib.manageconfig');
            $manageconfig->setSessionhashkey($this->getHashkey());
            $pconf=$manageconfig->getInstance($modulekey,$parentid,$entity);
            $pconf[$skey]=1;
            foreach ($pconf as $key => $value) {
                 $this->addValue($key,$value);
            }
           $cont=sizeof($pconf);
           return $cont;
         }
        return null;
        
    }
	
public function initCache($param=array()) {
		$utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
		$entity=$utildata->getVaueOfArray($param,'entity');
		$foceupdate=$utildata->getVaueOfArray($param,'foceupdate');
		$level=$utildata->getVaueOfArray($param,'level');
		if(empty($level)){$level='all';}
		if(empty($entity)){$entity=$this->get()->getEntity();}
		if(empty($entity)){$entity= $this->container->getParameter('badiu.system.access.defaultentity');}
		
		$cache=$this->getContainer()->get('badiu.system.core.lib.util.managecache');
		$manageconfig = $this->container->get('badiu.system.module.lib.manageconfig');
        $manageconfig->setSessionhashkey($this->getHashkey());
		
		$globalconf=null;
		$entityconf=null;
		$instanceconf=null;
		
		//global
		if($level=='all' || $level=='system'){	
			$updateglobal=false;
			$param=array('type'=>'cache','entity'=>0,'onlycheckexist'=>1);
			$rcache=$cache->loadUpdate($param);
			$filercache=$utildata->getVaueOfArray($rcache,'filecreated');
			if(!$filercache){
				$globalconf=$manageconfig->getGlabalLevel();
				$param=array('type'=>'cache','entity'=>0,'items'=>$globalconf,'onlycheckexist'=>0,'replace'=>1);
				$rcache=$cache->loadUpdate($param);
				$updateglobal=true;
			}
			
			//force update
			if($foceupdate && !$updateglobal){
				if(empty($globalconf)){$globalconf=$manageconfig->getGlabalLevel();}
				$param=array('type'=>'cache','entity'=>0,'items'=>$globalconf,'onlycheckexist'=>0,'replace'=>1);
				$rcache=$cache->loadUpdate($param);
			}
		 
		}
		
		
		//entity
		if($level=='all' || $level=='entity'){	
		
			$updateentity=false;
			$param=array('type'=>'cache','entity'=>$entity,'onlycheckexist'=>1);
			$rcache=$cache->loadUpdate($param);
			$filercache=$utildata->getVaueOfArray($rcache,'filecreated');
			
			if(!$filercache){
				if(empty($globalconf)){$globalconf=$manageconfig->getGlabalLevel();}
				$entityconf=$manageconfig->getEntityLevel($globalconf,$entity);
				$instanceconf=$manageconfig-> getInstanceLevel($globalconf,$entityconf,$entity);
				$entityconf=array_replace($entityconf,$instanceconf);
				$param=array('type'=>'cache','entity'=>$entity,'items'=>$entityconf,'onlycheckexist'=>0,'replace'=>1);
				$rcache=$cache->loadUpdate($param);
				$updateentity=true;
			}
		
			//force update
			if($foceupdate && !$updateentity){
				if(empty($globalconf)){$globalconf=$manageconfig->getGlabalLevel();}
				$entityconf=$manageconfig->getEntityLevel($globalconf,$entity);
				$instanceconf=$manageconfig->getInstanceLevel($globalconf,$entityconf,$entity);
				$entityconf=array_replace($entityconf,$instanceconf);
				$param=array('type'=>'cache','entity'=>$entity,'items'=>$entityconf,'onlycheckexist'=>0,'replace'=>1);
				$rcache=$cache->loadUpdate($param);
			}
			
			 
		}
		if(empty($globalconf)){$globalconf=array();}
		if(empty($entityconf)){$entityconf=array();}
		// || $level='entity'
		$cont=0;
		if($level=='all'){	
			$cont=sizeof($globalconf);
			$cont+=sizeof($entityconf);
		}else if($level=='system'){$cont=sizeof($globalconf);}
		else if($level=='entity'){$cont=sizeof($entityconf);}
		
        return $cont;
 
         
}
public function getCache($key,$entity=null) {
		  if(empty($entity)){$entity=$this->get()->getEntity();}
		  if(empty($entity)){$entity=$this->getEntity();}
		  if(empty($entity)){$entity= $this->container->getParameter('badiu.system.access.defaultentity');}
		  $param=array('type'=>'cache','entity'=>$entity,'forceread'=>1);
		  $cache=$this->getContainer()->get('badiu.system.core.lib.util.managecache');
		  $cache->init($param);
		  $cparam=$cache->getParam();
		  
		  $modulekey=null;
		  $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		  if(!empty($parentid)){
			 $modulekey= $key.'.p.'.$parentid;
		  }
		 if(!empty($modulekey) && array_key_exists($modulekey,$cparam)){
			 return $cparam[$modulekey];
		 }
		 if(array_key_exists($key,$cparam)){ return $cparam[$key]; }
        return null;
     }
     public function getIdsOfRecordpermissions($basemodulekey) {
         $sessionData=$this->get();
         $list=$sessionData->getRecordpermissions();
         if(empty($list)){return null;}
         $cont=0;
         $listids="";
         foreach ($list as $row) {
             $modulekey=$row['modulekey'];
             $moduleinstance=$row['moduleinstance'];
             
             if($basemodulekey==$modulekey){
                 if($cont==0){$listids=$moduleinstance;}
                 else{$listids.=",$moduleinstance";}
                 $cont++;
             }
         }
        return $listids;
    }

    public function getRoles($entity,$userid) {
        $roleuser=$this->container->get('badiu.system.access.user.data');
       $roles=$roleuser->getRolesShortname($entity,$userid);
       $newroles=array();
       if(empty($roles) || !is_array($roles)){return $newroles;}
       $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
       foreach ( $roles as $row) {
        $role=$utildata->getVaueOfArray($row,'shortname');
        array_push($newroles,$role);
       }

     
       return $newroles;
    }
    public function  getDefaultroute($entity,$userid) {
        $roleuser=$this->container->get('badiu.system.access.user.data');
       $roles=$roleuser-> getDefaultroute($entity,$userid);
       $newroles=array();
       if(empty($roles) || !is_array($roles)){return null;}
       $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
       $countr=sizeof($roles);
       $droute=null;
      
       foreach ( $roles as $row) {
            $shortname=$utildata->getVaueOfArray($row,'shortname');
            $defaultroute=$utildata->getVaueOfArray($row,'defaultroute');
           if( $countr==1){
                $droute= $defaultroute;
                return $droute;
                break;
            }
            else {
                //review - definir prioridade do perfil que deve ficar com a rota
                if(!empty($defaultroute)){$droute= $defaultroute;}
            }
       }

     
       return  $droute;
    }
    public function hasParamPermission($param,$operation) {
        $result=false;
       
        $sessiondata=$this->get();
        //$permparam=$sessiondata->getConfig()->getValue($param);
		$permparam=$this->getValue($param);
       
        $queryString = $this->container->get('badiu.system.core.lib.http.querystring');
        $queryString->setQuery($permparam);
        $queryString->makeParam();
        $roles=$queryString->getValue($operation);
        $listroles=array();
        if(!empty($roles)){
            $pos=stripos($roles, ",");
            if($pos=== false){
                $listroles=array($roles);
            }else{
                $listroles= explode(",", $roles);
            }
        }
       
        $useroles=$this->get()->getRoles();
        foreach ( $listroles as $row) { 
            $result=in_array($row, $useroles);
            if($result){break;}
        }
       
        return $result;
    }
    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }


    public function getHashkey() {
		if(empty($this->hashkey)){$this->hashkey='defautwebuser';}
		$clientsession = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_clientsession','ajaxws');
		if(!empty($clientsession)){$this->hashkey=$clientsession;}
		
		$stoken = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_stoken','ajaxws');
		if(!empty($stoken)){$this->hashkey=$stoken;}
        return $this->hashkey;
    }

    public function setHashkey($hashkey) {
        $this->hashkey = $hashkey;
    }
	
 /**
     * @return string
     */
    public function getGlobaldata() {
		if(empty($this->globaldata)){
			$this->globaldata= $this->container->get('badiu.system.access.globaldatasession');
			$this->globaldata->setSessionhashkey($this->getHashkey());
			//for scheduler 
			if(!empty($this->getEntity())){$this->globaldata->setEntity($this->getEntity());}
		}
        return $this->globaldata;
    }

    /**
     * @param string $globaldata
     */
    public function setGlobaldata($globaldata) {
        $this->globaldata = $globaldata;
    }
	
	 public function getEntity() {
           return $this->entity;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }
	
		public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}	