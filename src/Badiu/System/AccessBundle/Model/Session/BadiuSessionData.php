<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoggedInUserListener
 *
 * @author lino
 */
namespace Badiu\System\AccessBundle\Model\Session;
use Badiu\System\AccessBundle\Model\Session\BadiuSessionDataConfig;

class BadiuSessionData {

	/**
	 * @var string
	 */
	private $dbapp;
	/**
	 * @var integer
	 */
	private $entity;

	/**
	 * @var BadiuSessionUser
	 */
	private $user;
	
	//rever
	/**
	 * @var integer
	 */
	private $userid; //delted

	//rever
	/**
	 * @var string
	 */
	private $userfullname; //delted

	/**
	 * @var array
	 */
	private $permissions;

	/**
	 * @var array
	 */
	private $permissionsdenied=array();
	/**
	 * @var array
	 */
	private $recordpermissions=array();

	/**
	 * @var string
	 */
	private $theme;

	/**
	 * @var string
	 */
	private $lang;
	
	/**
	 * @var BadiuSessionDataConfig
	 */
	private $config;
        
        /** 
	 * @var BadiuSessionDataConfig
	 */
	private $configcustom;
        /**
	 * @var string
	 */
	private $type='web'; //web | webservice | cron
        
        /**
	 * @var array
	 */
	private $webserviceinfo=null;

	    /**
	 * @var array
	 */
	private $roles=array();

	/**
	 * @var object
	 */
	private $defaultroute;

	/**
	 * @var object
	 */
	private $accessby;

	/**
	 * @var boolean
	 */
	private $iscronlogscheduler=false;

	public function __construct() {
		$this->theme = "BadiuThemeCoreBundle";
		$this->dbapp = "default";
		$this->entity = 1;
		$this->config = new BadiuSessionDataConfig();
                $this->configcustom = new BadiuSessionDataConfig();
		$this->user = new BadiuSessionUser();
		$this->accessby=null;
		$this->lang="pt";
	}

	public function getDbapp() {
		return $this->dbapp;
	}

	public function getEntity() {
		return $this->entity;
	}

	public function getUserid() {
		return $this->userid;
	}

	public function getUserfullname() {
		return $this->userfullname;
	}

	public function setDbapp($dbapp) {
		$this->dbapp = $dbapp;
	}

	public function setEntity($entity) {
		$this->entity = $entity;
	}

	public function setUserid($userid) {
		$this->userid = $userid;
	}

	public function setUserfullname($userfullname) {
		$this->userfullname = $userfullname;
	}
	public function getTheme() {
            return $this->theme;
	}

	public function setTheme($theme) {
		$this->theme = $theme;
	}

	public function getPermissions() {
		return $this->permissions;
	}

	public function getRecordpermissions() {
		return $this->recordpermissions;
	}

	public function setPermissions($permissions) {
		$this->permissions = $permissions;
	}

	public function setRecordpermissions($recordpermissions) {
		$this->recordpermissions = $recordpermissions;
	}
	public function getConfig() {
		return $this->config;
	}

	public function setConfig(BadiuSessionDataConfig $config) {
		$this->config = $config;
	}

	public function getUser() {
		return $this->user;
	}

	public function setUser(BadiuSessionUser $user) {
		$this->user = $user;
	}
        function getType() {
            return $this->type;
        }

        function setType($type) {
            $this->type = $type;
        }

        function getWebserviceinfo() {
            return $this->webserviceinfo;
        }

        function setWebserviceinfo($webserviceinfo) {
            $this->webserviceinfo = $webserviceinfo;
        }

        function getConfigcustom(){
            return $this->configcustom;
        }

        function setConfigcustom(BadiuSessionDataConfig $configcustom) {
            $this->configcustom = $configcustom;
        }

		function getRoles(){
			return $this->roles;
		}
	
		function setRoles($roles) {
			$this->roles = $roles;
		}
	function addRoles($roles) {
		  if(!is_array($this->roles)){$this->roles=array();}
			foreach ($roles as $v) {
				array_push($this->roles,$v);
			}
			 
		}
		function getAccessby(){
			return $this->accessby;
		}
	
		function setAccessby($accessby) {
			$this->accessby = $accessby;
		}

		function getIscronlogscheduler(){
			return $this->iscronlogscheduler;
		}
	
		function setIscronlogscheduler($iscronlogscheduler) {
			$this->iscronlogscheduler = $iscronlogscheduler;
		}

		public function getPermissionsdenied() {
			return $this->permissionsdenied;
		}
		public function setPermissionsdenied($permissionsdenied) {
			$this->permissionsdenied = $permissionsdenied;
		}	

		
    /**
     * @return string
     */
    public function getDefaultroute() {
        return $this->defaultroute;
    }

    /**
     * @param string $defaultroute
     */
    public function setDefaultroute($defaultroute) {
        $this->defaultroute = $defaultroute;
    }
	
	  /**
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang) {
        $this->lang = $lang;
    }
}
