<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoggedInUserListener
 *
 * @author lino
 */

namespace Badiu\System\AccessBundle\Model\Session;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\BadiuSystemConstant;
use Badiu\System\AccessBundle\Model\Session\BadiuSessionData;

//http://t1.badiu21.com.br/~dev1/badiunet/web/app_dev.php/system/service/process?_service=badiu.system.access.session&_function=initConfigFromFile
class BadiuGlobalSession {

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $data = array();

    public function __construct(Container $container) {
        $this->container = $container;
        $this->data = array();
    }

    public function exec() {
        echo "<pre>";
        print_r($this->data);
        echo "</pre>";
        exit;
    }

    public function addValue($key, $value) {
        $this->data[$key] = $value;
    }

    public function getValue($key) {
        if ($this->existValue($key)) {
            return $this->data[$key];
        }
        return null;
    }

    public function existValue($key) {
        $v = array_key_exists($key, $this->data);
        return $v;
    } 
	public function existKey($key) {
        $r=FALSE;
        if(!empty($this->data)){
           $r=array_key_exists($key,$this->data);
        } 
         return  $r;
     }
    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function getData() {
        return $this->data;
    }

}
