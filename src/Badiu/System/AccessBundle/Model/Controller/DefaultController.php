<?php

namespace Badiu\System\AccessBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/system/access")
 */
class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuSystemAccessBundle:Default:index.html.twig', array('name' => $name));
    }
    
    /**
 * @Route("/login", name="badiu.system.access.login")
 */
    public function loginAction()
    {
        return $this->render('BadiuSystemAccessBundle:Default:index.html.twig', array('name' => 'login'));
    }

      /**
 * @Route("/logout", name="badiu.system.access.logout")
 */
    public function logoutAction()
    {
        return $this->render('BadiuSystemAccessBundle:Default:index.html.twig', array('name' => 'login'));
    }

}
