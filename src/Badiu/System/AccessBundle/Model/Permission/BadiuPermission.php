<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoggedInUserListener
 *
 * @author lino
 */

namespace Badiu\System\AccessBundle\Model\Permission;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;


class BadiuPermission{
    
     
     /**
     * @var Container
     */
    private $container;
    
      private $utildata;
    public function __construct(Container $container) {
        $this->container=$container;
         $this->utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
    }
    
    public function has_access($resource,$hashkey=null) {
        // return TRUE; 
        $listException = $this->exceptionList();
        $request = $this->container->get('request');
	   // $currentroute = $request->get('_route');
		$currentroute = $resource;
        $dkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey');
		$key=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_key');
		if(!empty($key)){ $currentroute=$key;}
        if(!empty($dkey)){ $currentroute=$dkey;}
		
        $badiuSession = $this->container->get('badiu.system.access.session');
		if(!empty($hashkey)){$badiuSession->setHashkey($hashkey);}
        $datasession = $badiuSession->get();

        //permission daned
        $lpermdaned=$datasession->getPermissionsdenied();
        if(is_array($lpermdaned)){
            foreach ($lpermdaned as $list) {
                if ($list == $currentroute) {return 0;}
            }
        }
        //permission daned by sever service
        $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
        if(!empty($serviceid)){
            $lpermdanedservice=  $badiuSession->getValue("badiu.admin.server.service.".$serviceid.".permissions.denied");
            if(is_array($lpermdanedservice)){
                foreach ($lpermdanedservice as $list) {
                    if ($list == $currentroute) {return 0;}
                }
            }
        }
        
        $lperm=$datasession->getPermissions();
       
        foreach ($listException as $list) {
			if ($list == $currentroute) {return 1;}
		}
        if(!is_array($lperm)){return 0;}
		$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
        foreach ($lperm as $perm) {
            $has=$this->hasLinkPermission($perm,$resource,$param);
            if($has){return 1;}
          /*  $pos=strpos($resource,$perm);
            //echo " $resource -> $perm - $pos";
            if($pos !== FALSE && $pos==0){return true;}*/
        }
        
        return 0;
     }
     function hasLinkPermission($permkey,$linkkey,$param=array()) {
		
		//check service / function
		$service= $this->utildata->getVaueOfArray($param,'_service');
		$function= $this->utildata->getVaueOfArray($param,'_function');
		if(!empty($service)){
			if(!empty($service) && !empty($function) ){$linkkey=$linkkey.'/'.$service.'/'.$function;}
			else if(!empty($service) && empty($function) ){$linkkey=$linkkey.'/'.$service;}
		} 
		
        $pos=strpos($linkkey,$permkey);
        if($pos !== FALSE && $pos==0){return 1;}
        return 0;
    }
 public function getKeys($list){
     $newlist=array();
     foreach ($list as $l) {
        array_push($newlist,$l['key']);
     }
     return $newlist;
     }

     public function addKeys($listkey,$newlist){
         if(empty($newlist)){return $listkey;}
      foreach ($newlist as $l) {
        array_push($listkey,$l['key']);
     }
     return $listkey;
     }
     
     function getContainer() {
         return $this->container;
     }

     function setContainer(Container $container) {
         $this->container = $container;
     }
   
     public function exceptionList() {  
		 $badiuSession = $this->getContainer()->get('badiu.system.access.session');
		$listkeys=$badiuSession->getValue('badiu.system.core.param.config.routersguestpermission');
		$listkeys=preg_split("/\r\n|\n|\r/", $listkeys);
		if(!is_array($listkeys)){$listkeys= array();}
		return $listkeys;
	}
}
