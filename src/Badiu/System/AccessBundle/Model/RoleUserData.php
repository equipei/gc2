<?php

namespace Badiu\System\AccessBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class RoleUserData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

	
 public function getRolesShortname($entity,$userid) {
        $sql="SELECT  r.shortname FROM ".$this->getBundleEntity()." o JOIN o.roleid r WHERE o.entity = :entity AND o.userid=:userid ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('userid',$userid);
        $result= $query->getResult();
        return  $result;
    }	
    
    public function getDefaultroute($entity,$userid) {
        $sql="SELECT  r.shortname,r.defaultroute   FROM ".$this->getBundleEntity()." o JOIN o.roleid r WHERE o.entity = :entity AND o.userid=:userid ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('userid',$userid);
        $result= $query->getResult();
        return  $result;
    }	
   
 public function delete($param) {
			$id=null;
			if(isset($param['id'])){$id=$param['id'];}
			if(empty($id)){return null;}
            $sql="DELETE  FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id', $id);
            $result=$query->execute();
            return $result;
     }   
}
