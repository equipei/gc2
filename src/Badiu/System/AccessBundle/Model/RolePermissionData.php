<?php

namespace Badiu\System\AccessBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class RolePermissionData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

	
 public function getKeysByUserid($entity,$userid) {
        $sql="SELECT  o.pemissionkey AS key FROM ".$this->getBundleEntity()." o INNER JOIN BadiuSystemAccessBundle:SystemAccessRoleUser rs WITH o.roleid=rs.roleid WHERE o.entity = :entity AND rs.userid=:userid ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('userid',$userid);
        $result= $query->getResult();
        return  $result;
    }	
public function getKeysByRole($entity,$roleshortname) {
        $sql="SELECT  o.pemissionkey AS key FROM ".$this->getBundleEntity()." o JOIN o.roleid r WHERE o.entity = :entity AND r.shortname=:roleshortname ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('roleshortname',$roleshortname);
        $result= $query->getResult();
        return  $result;
    }		
 
public function getKeysRoleInfoByUserid($entity,$userid) {
        $sql="SELECT o.id, o.pemissionkey AS key,r.name,r.shortname,r.enablerestrictparentkey FROM ".$this->getBundleEntity()." o INNER JOIN BadiuSystemAccessBundle:SystemAccessRoleUser rs WITH o.roleid=rs.roleid JOIN rs.roleid r WHERE o.entity = :entity AND rs.userid=:userid ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('userid',$userid);
        $result= $query->getResult();
        return  $result;
    } 
}
