<?php

namespace Badiu\System\AccessBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class PermissionSqlFilter extends BadiuSqlFilter{
    /**
     * @var object
     */
   private  $sqlservice;
    function __construct(Container $container) {
            parent::__construct($container);
            $this->sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
       }

        function recordin($modulekey) {
           
           $badiuSession=$this->getContainer()->get('badiu.system.access.session');
           $listid= $badiuSession->getIdsOfRecordpermissions($modulekey);
           //print_r($listid);exit;
           //$listid=null;
           $sql="";
            if(!empty($listid)){$sql=" AND  o.id IN ($listid) ";}
          
            return $sql;
        }
        
        function getSqlservice() {
            return $this->sqlservice;
        }

        function setSqlservice($sqlservice) {
            $this->sqlservice = $sqlservice;
        }


}
