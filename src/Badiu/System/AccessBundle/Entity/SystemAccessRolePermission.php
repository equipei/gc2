<?php

namespace Badiu\System\AccessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemAccessRolePermission
 *
 * @ORM\Table(name="system_access_role_permission", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_access_role_permission_pemissionkey_uix", columns={"entity","roleid", "pemissionkey"})},
 *          indexes={@ORM\Index(name="system_access_role_permission_entity_ix", columns={"entity"}),
 *                  @ORM\Index(name="system_access_role_permission_pemissionkey_ix", columns={"pemissionkey"}),
  *                 @ORM\Index(name="system_access_role_permission_userid_ix", columns={"roleid"})})
 * @ORM\Entity
 */
class SystemAccessRolePermission
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

     /**
     * @var SystemAccessRole
     *
     * @ORM\ManyToOne(targetEntity="SystemAccessRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roleid", referencedColumnName="id")
     * })
     */
    private $roleid;
    
     /**
     * @var string
     *
     * @ORM\Column(name="pemissionkey", type="string", length=255, nullable=false)
     */
    private $pemissionkey;
    
      /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
  
      /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
   
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
	
	 /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getRoleid() {
        return $this->roleid;
    }

    public function getPemissionkey() {
        return $this->pemissionkey;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

   

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setRoleid(SystemAccessRole $roleid) {
        $this->roleid = $roleid;
    }

    public function setPemissionkey($pemissionkey) {
        $this->pemissionkey = $pemissionkey;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

   
    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }


public function getDeleted() {
        return $this->deleted;
    }

   public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

 public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }
	  public function getTimecreated() {
        return $this->timecreated;
    }

  public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }
	
    public function getTimemodified() {
        return $this->timemodified;
    }

}
