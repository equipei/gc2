<?php

namespace Badiu\System\AccessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemAccessRestrictedUser
 *
 * @ORM\Table(name="system_access_restricted_user", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_access_restricted_user_pemissionkey_uix", columns={"entity","module","userid","host"})},
 *       indexes={@ORM\Index(name="system_access_restricted_user_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="system_access_restricted_user_module_ix", columns={"module"}),
 *              @ORM\Index(name="system_access_restricted_user_host_ix", columns={"host"})})
 * @ORM\Entity
 */

class SystemAccessRestrictedUser
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id")
     * })
     */
    private $userid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="module", type="string", length=255, nullable=false)
     */
    private $module;
   
    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=255, nullable=false)
     */
    private $host;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getUserid() {
        return $this->userid;
    }

    public function getModule() {
        return $this->module;
    }

    public function getHost() {
        return $this->host;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setUserid(\Badiu\System\UserBundle\Entity\SystemUser $userid) {
        $this->userid = $userid;
    }

    public function setModule($module) {
        $this->module = $module;
    }

    public function setHost($host) {
        $this->host = $host;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }





}
