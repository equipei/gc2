<?php

namespace Badiu\System\AccessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemAccessRole
 *
 * @ORM\Table(name="system_access_user_record", uniqueConstraints={
  *      @ORM\UniqueConstraint(name="system_access_user_record_uix", columns={"entity","modulekey","userid","moduleinstance"})},
 *       indexes={@ORM\Index(name="system_access_user_record_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="system_access_user_record_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="system_access_user_record_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="system_access_user_record_userid_ix", columns={"userid"})})
 * @ORM\Entity
 */
class SystemAccessUserRecord
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

     
     /**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id")
     * })
     */
    private $userid;

    	 /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=false)
     */
    private $modulekey; 

  /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=false)
     */
    private $moduleinstance;
     
      /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig; 
    
      /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
  
   /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
	 /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
	
    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    
    public function getUserid() {
        return $this->userid;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

  

    public function setUserid(\Badiu\System\UserBundle\Entity\SystemUser $userid) {
        $this->userid = $userid;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }



public function getDeleted() {
        return $this->deleted;
    }

   public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

 public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }
	  public function getTimecreated() {
        return $this->timecreated;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }


}
