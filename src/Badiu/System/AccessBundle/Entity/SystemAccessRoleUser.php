<?php

namespace Badiu\System\AccessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemAccessRole
 *
 * @ORM\Table(name="system_access_role_user", uniqueConstraints={
  *      @ORM\UniqueConstraint(name="system_access_role_user_uix", columns={"entity","roleid","userid"})},
 *       indexes={@ORM\Index(name="system_access_role_user_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="system_access_role_user_roleid_ix", columns={"roleid"}),
 *              @ORM\Index(name="system_access_role_user_userid_ix", columns={"userid"})})
 * @ORM\Entity
 */
class SystemAccessRoleUser
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

     /**
     * @var SystemAccessRole
     *
     * @ORM\ManyToOne(targetEntity="SystemAccessRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roleid", referencedColumnName="id")
     * })
     */
    private $roleid;
    
     /**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id")
     * })
     */
    private $userid;

        
      /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
  
   /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
   
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
	 /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
	
    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getRoleid() {
        return $this->roleid;
    }

    public function getUserid() {
        return $this->userid;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setRoleid(SystemAccessRole $roleid) {
        $this->roleid = $roleid;
    }

    public function setUserid(\Badiu\System\UserBundle\Entity\SystemUser $userid) {
        $this->userid = $userid;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }



public function getDeleted() {
        return $this->deleted;
    }

   public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

 public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }
	  public function getTimecreated() {
        return $this->timecreated;
    }

}
