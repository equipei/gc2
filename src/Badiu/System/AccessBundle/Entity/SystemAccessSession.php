<?php

namespace Badiu\System\AccessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemAccessSession
 *
 * @ORM\Table(name="system_access_session", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_access_session_tkey_uix", columns={"tkey"})},
 *       indexes={@ORM\Index(name="system_access_session_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="system_access_session_tkey_ix", columns={"tkey"}),
 *              @ORM\Index(name="system_access_session_status_ix", columns={"status"}),
 *              @ORM\Index(name="system_access_timetoexpire_ix", columns={"timetoexpire"}),
 *              @ORM\Index(name="system_access_session_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="system_access_session_deleted_ix", columns={"sclient"})})
 
 * @ORM\Entity
 */
class SystemAccessSession
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /** 
     * @var integer
     *
     * @ORM\Column(name="sclient", type="bigint", nullable=false)
     */
    private $sclient; //fk of system_user or admin_server_sserivice
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='sservice'; // sservice | user
    
    /**
     * @var string
     *
     * @ORM\Column(name="tkey", type="string", length=255, nullable=false)
     */
    private $tkey;

    
    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status='active'; //active | inactive
  
     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    
            /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timetoexpire", type="datetime", nullable=false)
     */
    private $timetoexpire; 
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastaccess", type="datetime", nullable=false)
     */
    private $lastaccess;

    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

   

    function getDtype() {
        return $this->dtype;
    }

    function getTkey() {
        return $this->tkey;
    }

    function getStatus() {
        return $this->status;
    }

    function getParam() {
        return $this->param;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function getTimecreated(){
        return $this->timecreated;
    }

    function getLastaccess(){
        return $this->lastaccess;
    }
 
    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }


    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setTkey($tkey) {
        $this->tkey = $tkey;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setLastaccess(\DateTime $lastaccess) {
        $this->lastaccess = $lastaccess;
    }

    function getSclient() {
        return $this->sclient;
    }

    function setSclient($sclient) {
        $this->sclient = $sclient;
    }

    function getTimetoexpire(){
        return $this->timetoexpire;
    }

    function setTimetoexpire(\DateTime $timetoexpire) {
        $this->timetoexpire = $timetoexpire;
    }




}
