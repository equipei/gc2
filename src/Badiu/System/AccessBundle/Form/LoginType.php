<?php

namespace Badiu\System\AccessBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Translation\Translator;

class TicketFilterType  extends AbstractType {
    
     /**
     * @var Container
     */
    private $container;
    /**
     * @var Translator
     */
    private $translator;
    
  
    
   function __construct(Container $container) {
                $this->container=$container;
                $this->translator=$this->container->get('translator');
      }
     
       /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
       {
            $sformutil=$this->getContainer()->get('badiu.system.core.lib.form.util');     
            $fields=$this->getContainer()->get('badiu.system.core.lib.config.bundle')->getFormFields('badiu.support.ticket.channel.filter.form','badiu.support.ticket.config');
              foreach ($fields as $field) {
                  
                  if($field->getName()=='name'){ $builder->add('name', 'text',array('label'=>$this->getTranslator()->trans('name'),'required'  => $field->getRequired(),'attr' => array('class' => $field->getCssClasss())));}
                  else  if($field->getName()=='deleted'){$builder->add('deleted', 'choice',array('label'=>$this->getTranslator()->trans('garbage.record'),'required'  => $field->getRequired(), 'choices'=>$sformutil->getBooleanOptions(), 'attr' => array('class' => $field->getCssClasss())));}
              }
      }
    
     /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Badiu\Support\TicketBundle\Filter\ChannelFilter'
        ));
    }
    
     /**
     * @return string
     */
    public function getName()
    {
        return 'badiu_support_ticketbundle_supportchannelfilter';
    }
 
  
    public function getTranslator() {
        return $this->translator;
    }

    public function setTranslator(Translator $translator) {
        $this->translator = $translator;
    }
    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }





}
