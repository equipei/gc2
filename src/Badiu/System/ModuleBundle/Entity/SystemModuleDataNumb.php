<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of SystemModuleClientCustom
 *
 * @author lino
 */


/**
 * SystemModuleDataNumb
 *
 * @ORM\Table(name="system_module_data_numb", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_module_data_numb_uix", columns={"entity","modulekey","moduleinstance","name"})}, 
 *       indexes={@ORM\Index(name="system_module_data_numb_entity_ix", columns={"entity"}),
 *       @ORM\Index(name="system_module_data_numb_name_ix", columns={"name"}),
 *       @ORM\Index(name="system_module_data_numb_modulekey_ix", columns={"modulekey"}),
 *       @ORM\Index(name="system_module_data_numb_moduleinstance_ix", columns={"moduleinstance"}),
 *       @ORM\Index(name="system_module_data_numb_value_ix", columns={"value"}),
 *       @ORM\Index(name="system_module_data_numb_timecreated_ix", columns={"timecreated"}),
 *       @ORM\Index(name="system_module_data_numb_timemodified_ix", columns={"timemodified"})})
 * @ORM\Entity
 */


class SystemModuleDataNumb {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

	 /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=false)
     */
    private $modulekey;


    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=false)
     */
    private $moduleinstance;
    
	
	/**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

     /**
     * @var float
     *
     * @ORM\Column(name="value", type="float",  precision=10, scale=0, nullable=true)
     */
    private $value;
    
    

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;

    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param int $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    
	
     /**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string $moduleinstance
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }
    
    function getModulekey() {
        return $this->modulekey;
    }

    function getName() {
        return $this->name;
    }

  
    function getValue() {
        return $this->value;
    }

    
    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setName($name) {
        $this->name = $name;
    }


    function setValue($value) {
        $this->value = $value;
    }


    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

  

}
