<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of SystemModuleConfig
 *
 * @author lino
 */


/**
 * SystemModuleClientConfig
 *
 * @ORM\Table(name="system_module_client_config", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_module_client_config_uix", columns={"bkey","name"})},
 *       indexes={
 *			@ORM\Index(name="system_module_client_config_name_ix", columns={"name"}),
 *           @ORM\Index(name="system_module_client_config_bkey_ix", columns={"bkey"})})
 * @ORM\Entity
 */


class SystemModuleClientConfig {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="bkey", type="string", length=255, nullable=false)
     */
    private $bkey;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

		/**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;
	
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    
    /**
     * @return string
     */
    public function getBkey()
    {
        return $this->bkey;
    }

    /**
     * @param string $bkey
     */
    public function setBkey($bkey)
    {
        $this->bkey = $bkey;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return \DateTime
     */
    public function getTimecreated()
    {
        return $this->timecreated;
    }

    /**
     * @param \DateTime $timecreated
     */
    public function setTimecreated($timecreated)
    {
        $this->timecreated = $timecreated;
    }

    /**
     * @return \DateTime
     */
    public function getTimemodified()
    {
        return $this->timemodified;
    }

    /**
     * @param \DateTime $timemodified
     */
    public function setTimemodified($timemodified)
    {
        $this->timemodified = $timemodified;
    }

    /**
     * @return int
     */
    public function getUseridadd()
    {
        return $this->useridadd;
    }

    /**
     * @param int $useridadd
     */
    public function setUseridadd($useridadd)
    {
        $this->useridadd = $useridadd;
    }

    /**
     * @return int
     */
    public function getUseridedit()
    {
        return $this->useridedit;
    }

    /**
     * @param int $useridedit
     */
    public function setUseridedit($useridedit)
    {
        $this->useridedit = $useridedit;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

 /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }

}
