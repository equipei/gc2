<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * SystemModule
 *
 * @ORM\Table(name="system_module", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_module_bundle_uix", columns={"bundlekey"}),
 *      @ORM\UniqueConstraint(name="system_module_bundle_uix", columns={"bundle"})},
 *       indexes={@ORM\Index(name="system_module_bundlekey_ix", columns={"bundlekey"}),
 *              @ORM\Index(name="system_module_bundle_ix", columns={"bundle"}),
 *              @ORM\Index(name="system_module_cetegory_ix", columns={"cetegory"})})
 * @ORM\Entity
 */

class SystemModule {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="bundlekey", type="string", length=255, nullable=false)
     */
    private $bundlekey;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="bundle", type="string", length=255, nullable=false)
     */
    private $bundle;

    /**
 * @var string
 *
 * @ORM\Column(name="version", type="string", length=255, nullable=false)
 */
    private $version;

    /**
     * @var  \DateTime
     *
     * @ORM\Column(name="versiondate", type="bigint", nullable=true)
     */
    private $versiondate;
    /**
     * @var string
     *
     * @ORM\Column(name="cetegory", type="string", length=255, nullable=true)
     */
    private $cetegory;
     /**
     * @var boolean
     *
     * @ORM\Column(name="execcron", type="integer", nullable=false)
     */
     private $execcron;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="execcrontime", type="integer", nullable=true)
     */
     private $execcrontime;
     
      /**
     * @var  \DateTime
     *
     * @ORM\Column(name="lasttimecronexec", type="bigint", nullable=true)
     */
     private $lasttimecronexec;


    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="integer", nullable=false)
     */
  private $enabled;
    


     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

   
    function getId() {
        return $this->id;
    }

  
    function getBundle() {
        return $this->bundle;
    }

    function getVersion() {
        return $this->version;
    }

    function getVersiondate() {
        return $this->versiondate;
    }

    function getCetegory() {
        return $this->cetegory;
    }

    function getExeccron() {
        return $this->execcron;
    }

    function getExeccrontime() {
        return $this->execcrontime;
    }

    function getLasttimecronexec() {
        return $this->lasttimecronexec;
    }

    function getEnabled() {
        return $this->enabled;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function setId($id) {
        $this->id = $id;
    }

    

    function setBundle($bundle) {
        $this->bundle = $bundle;
    }

    function setVersion($version) {
        $this->version = $version;
    }

    function setVersiondate(\DateTime $versiondate) {
        $this->versiondate = $versiondate;
    }

    function setCetegory($cetegory) {
        $this->cetegory = $cetegory;
    }

    function setExeccron($execcron) {
        $this->execcron = $execcron;
    }

    function setExeccrontime($execcrontime) {
        $this->execcrontime = $execcrontime;
    }

    function setLasttimecronexec(\DateTime $lasttimecronexec) {
        $this->lasttimecronexec = $lasttimecronexec;
    }

    function setEnabled($enabled) {
        $this->enabled = $enabled;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }
    function getBundlekey() {
        return $this->bundlekey;
    }

    function setBundlekey($bundlekey) {
        $this->bundlekey = $bundlekey;
    }



}
