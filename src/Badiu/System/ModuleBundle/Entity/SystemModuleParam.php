<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;


/**
 * SystemSchedulerRole
 *
 * @ORM\Table(name="system_module_param", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_module_param_name_uix", columns={"entity","modulekey", "name"}), 
 *      @ORM\UniqueConstraint(name="system_module_param_shortname_uix", columns={"entity","modulekey", "shortname"}), 
 *      @ORM\UniqueConstraint(name="system_module_param_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="system_module_param_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="system_module_param_name_ix", columns={"name"}), 
 *              @ORM\Index(name="system_module_param_bundlekey_ix", columns={"bundlekey"}), 
 *              @ORM\Index(name="system_module_param_modulekey_ix", columns={"modulekey"}), 
 *              @ORM\Index(name="system_module_param_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="system_module_param_shortname_ix", columns={"shortname"}), 
*              @ORM\Index(name="system_module_param_deleted_ix", columns={"deleted"}), 
 *              @ORM\Index(name="system_module_param_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */


class SystemModuleParam {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

        /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;
    
     /**
     * @var string
     *
     * @ORM\Column(name="bundlekey", type="string", length=255, nullable=false)
     */
    private $bundlekey;
  /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=false)
     */
    private $modulekey;
  

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

 

    	 /**
		 * @var string
		 *
		 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
		 */
	private $shortname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype='email';// email | sms 
    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    	/**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
	 /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig; 
	
	     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
      /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    function getId() {
        return $this->id;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getName() {
        return $this->name;
    }

    function getValue() {
        return $this->value;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated(){
        return $this->timecreated;
    }

    function getTimemodified(){
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setValue($value) {
        $this->value = $value;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }


    function getShortname() {
        return $this->shortname;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }


    function getEntity() {
        return $this->entity;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function getDtype() {
        return $this->dtype;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }
    function getDeleted() {
        return $this->deleted;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


    function getBundlekey() {
        return $this->bundlekey;
    }

    function setBundlekey($bundlekey) {
        $this->bundlekey = $bundlekey;
    }

  function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }


}
