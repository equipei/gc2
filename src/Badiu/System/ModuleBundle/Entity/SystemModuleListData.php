<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of SystemModuleListData
 *
 * @author lino
 */


/**
 * SystemModuleListData
 *
 * @ORM\Table(name="system_module_list_data",  uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_module_list_data_name_uix", columns={"entity", "listid","moduleinstance"}), 
 *      @ORM\UniqueConstraint(name="system_module_list_data_shortname_uix", columns={"entity", "listid","shortname"})},
 *       indexes={
 *       @ORM\Index(name="system_module_list_data_entity_ix", columns={"entity"}),
 *       @ORM\Index(name="system_module_list_data_shortname_ix", columns={"shortname"}),
 *       @ORM\Index(name="system_module_list_data_listid_ix", columns={"listid"}),
 *       @ORM\Index(name="system_module_list_datamoduleinstance_ix", columns={"moduleinstance"}),
 *       @ORM\Index(name="system_module_list_data_name_ix", columns={"name"})})
 * @ORM\Entity
 */


class SystemModuleListData {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
      /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;
  
   
      /**
     * @var SystemModuleList
     *
     * @ORM\ManyToOne(targetEntity="SystemModuleList")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="listid", referencedColumnName="id")
     * })
     */
    private $listid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
    
       /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=false)
     */
    private $moduleinstance;
    

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;



    /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    
/**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;

       
      /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getListid(){
        return $this->listid;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }

    function getEmail() {
        return $this->email;
    }

    function getParam() {
        return $this->param;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setListid(SystemModuleList $listid) {
        $this->listid = $listid;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


  
}
