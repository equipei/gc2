<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of SystemModuleList
 *
 * @author lino
 */


/**
 * SystemModuleList
 *
 * @ORM\Table(name="system_module_list",  uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_module_list_name_uix", columns={"entity", "modulekey","name"}), 
 *      @ORM\UniqueConstraint(name="system_module_list_shortname_uix", columns={"entity","shortname"})},
*       indexes={
 *       @ORM\Index(name="system_module_list_entity_ix", columns={"entity"}),
 *       @ORM\Index(name="system_module_list_shortname_ix", columns={"shortname"}),
 *       @ORM\Index(name="system_module_list_bundlekey_ix", columns={"bundlekey"}),
 *       @ORM\Index(name="system_module_list_modulekeyref_ix", columns={"modulekeyref"}),
 *       @ORM\Index(name="system_module_list_modulekey_ix", columns={"modulekey"}),
 *       @ORM\Index(name="system_module_list_name_ix", columns={"name"})})
 * @ORM\Entity
 */
  

class SystemModuleList {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
      /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;
  
   
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
    
     /**
     * @var string
     *
     * @ORM\Column(name="bundlekey", type="string", length=255, nullable=false)
     */
    private $bundlekey;
    

    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;

    /**
     * @var string
     *
     * @ORM\Column(name="modulekeyref", type="string", length=255, nullable=true)
     */
    private $modulekeyref;

    /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    
/**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;

       
      /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function getBundlekey() {
        return $this->bundlekey;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getParam() {
        return $this->param;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function getTimecreated(){
        return $this->timecreated;
    }

    function getTimemodified(){
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    function setBundlekey($bundlekey) {
        $this->bundlekey = $bundlekey;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getModulekeyref() {
        return $this->modulekeyref;
    }

    function setModulekeyref($modulekeyref) {
        $this->modulekeyref = $modulekeyref;
    }

 
}
