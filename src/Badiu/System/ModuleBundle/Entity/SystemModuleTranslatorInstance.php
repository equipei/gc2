<?php

namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of SystemModuleTranslator
 *
 * @author lino
 */


/**
 * SystemModuleTranslatorInstance
 *
 * @ORM\Table(name="system_module_translator_instance", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_module_translator_instance_uix", columns={"entity","modulekey","moduleinstance","locale","mkey"})}, 
 *       indexes={@ORM\Index(name="system_module_translator_instance_entity_ix", columns={"entity"}),
  *       @ORM\Index(name="system_module_translator_instance_modulekey_ix", columns={"modulekey"}),
 *		@ORM\Index(name="system_module_translator_instance_mkey_ix", columns={"mkey"}),
 *		@ORM\Index(name="system_module_translator_instance_locale_ix", columns={"locale"}),
 *		@ORM\Index(name="system_module_translator_instance_ix", columns={"mstatus"}),
 *		@ORM\Index(name="system_module_translator_messageidx_ix", columns={"messageidx"}),  
  *		@ORM\Index(name="system_module_translator_reference_ix", columns={"reference"}),
 *       @ORM\Index(name="system_module_translator_instance_ix", columns={"moduleinstance"}),
  *		@ORM\Index(name="system_module_translator_instance_deleted_ix", columns={"deleted"})})
 * @ORM\Entity
 */


class SystemModuleTranslatorInstance {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;


    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=false)
     */
    private $moduleinstance;
    
	 /**
     * @var string
     *
     * @ORM\Column(name="mstatus", type="string", length=50, nullable=true)
     */
    private $mstatus;
	
	/**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=20, nullable=false)
     */
    private $locale;
    
	/**
     * @var string
     *
     * @ORM\Column(name="mkey", type="string", length=255, nullable=false)
     */
    private $mkey;
	
	/**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;
    
	
	/**
     * @var string
     *
     * @ORM\Column(name="messageidx", type="string", length=255, nullable=true)
     */
    private $messageidx;
	
		/**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;
	 
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getMkey() {
        return $this->mkey;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setMkey($mkey) {
        $this->mkey = $mkey;
    }

   

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

		
	

    function getModulekey() {
        return $this->modulekey;
    }
 
    function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
    /**
     * @return string
     */
    public function getMstatus() {
        return $this->mstatus;
    }

    /**
     * @param string $mstatus
     */
    public function setMstatus($mstatus) {
        $this->mstatus = $mstatus;
    }


 /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message) {
        $this->message = $message;
    }
	
	/**
     * @return string
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale) {
        $this->locale = $locale;
    }
	
	/**
     * @return string
     */
    public function getMessageidx() {
        return $this->messageidx;
    }

    /**
     * @param string $messageidx
     */
    public function setMessageidx($messageidx) {
        $this->messageidx = $messageidx;
    }
	
    /**
     * Get the value of the $reference property.
     *
     * @return mixed Returns the value of the $reference property.
     */
    public function getReference() {
        return $this->reference;
    }

    /**
     * Set a new value for the $reference property.
     *
     * @param mixed $reference The value to be set to the $reference property.
     */
    public function setReference($reference) {
        $this->reference = $reference;
    }
}
