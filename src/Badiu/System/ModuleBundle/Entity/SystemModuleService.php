<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;


/**
 * SystemSchedulerRole
 *
 * @ORM\Table(name="system_module_service", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_module_service_name_uix", columns={"entity","name"}), 
 *      @ORM\UniqueConstraint(name="system_module_service_shortname_uix", columns={"entity","shortname"}), 
 *      @ORM\UniqueConstraint(name="system_module_service_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="system_module_service_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="system_module_service_name_ix", columns={"name"}), 
 *              @ORM\Index(name="system_module_service_enable_ix", columns={"enable"}), 
 *              @ORM\Index(name="system_module_service_timestart_ix", columns={"timestart"}), 
 *              @ORM\Index(name="system_module_service_timeend_ix", columns={"timeend"}), 
 *              @ORM\Index(name="system_module_service_shortname_ix", columns={"shortname"}), 
*              @ORM\Index(name="system_module_service_deleted_ix", columns={"deleted"}), 
 *              @ORM\Index(name="system_module_service_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */


class SystemModuleService {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

        /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;
    


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

 

    	 /**
		 * @var string
		 *
		 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
		 */
	private $shortname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="stoken", type="string", length=255, nullable=false)
     */
    private $stoken;
	
	  /**
     * @var integer
     *
     * @ORM\Column(name="enable", type="integer", nullable=true)
     */
    private $enable;

	        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
	
	     /**
     * @var string
     *
     * @ORM\Column(name="ipallowed", type="text", nullable=true)
     */
    private $ipallowed;
  /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=false)
     */
    private $modulekey;
  
  /**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
  
  /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="bigint", nullable=true)
     */
    private $userid;
   

    	/**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
	 /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig; 
	
	     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
      /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    function getId() {
        return $this->id;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getName() {
        return $this->name;
    }


    function getParam() {
        return $this->param;
    }

    function getTimecreated(){
        return $this->timecreated;
    }

    function getTimemodified(){
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setName($name) {
        $this->name = $name;
    }

  
    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }


    function getShortname() {
        return $this->shortname;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }


    function getEntity() {
        return $this->entity;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

  

    function getDeleted() {
        return $this->deleted;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


    function getIpallowed() {
        return $this->ipallowed;
    }

    function setIpallowed($ipallowed) {
        $this->ipallowed = $ipallowed;
    }

  function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }


    function setTimestart($timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend($timeend) {
        $this->timeend = $timeend;
    }
	
	 function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }
	
	 function getEnable() {
        return $this->enable;
    }

    function setEnable($enable) {
        $this->enable = $enable;
    }
	
	 function getStoken() {
        return $this->stoken;
    }

    function setStoken($stoken) {
        $this->stoken = $stoken;
    }

  function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	  function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
	 function setUserid($userid) {
        $this->userid = $userid;
    }

     function getUserid() {
        return $this->userid;
    }

}
