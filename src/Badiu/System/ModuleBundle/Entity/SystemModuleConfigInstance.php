<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of SystemModuleCustom
 *
 * @author lino
 */


/**
 * SystemModuleConfigEntity
 *
 * @ORM\Table(name="system_module_config_instance",  uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_module_config_instance_uix", columns={"entity","name","modulekey","moduleinstance"})},
 *       indexes={@ORM\Index(name="system_module_config_instance_entity_ix", columns={"entity"}),
 *       @ORM\Index(name="system_module_config_instance_modulekey_ix", columns={"modulekey"}),
 *       @ORM\Index(name="system_module_config_instance_moduleinstance_ix", columns={"moduleinstance"}),
 *       @ORM\Index(name="system_module_config_instance_bundlekey_ix", columns={"bundlekey"}),  
 *       @ORM\Index(name="system_module_config_instance_enableinheritance_ix", columns={"enableinheritance"}),
 *       @ORM\Index(name="system_module_config_instance_name_ix", columns={"name"})})
 * @ORM\Entity
 */


class SystemModuleConfigInstance {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
      /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;
  
    /**
     * @var string
     *
     * @ORM\Column(name="bundlekey", type="string", length=255, nullable=false)
     */
    private $bundlekey;
    
    /**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

      /**
     * @var boolean
     *
     * @ORM\Column(name="enableinheritance", type="integer", nullable=true)
     */
    private $enableinheritance=true; // import var config of global if not defined
    /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    
        /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig; 
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;

       
      /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getBundlekey() {
        return $this->bundlekey;
    }

    function getName() {
        return $this->name;
    }

    function getValue() {
        return $this->value;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated(){
        return $this->timecreated;
    }

    function getTimemodified(){
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setBundlekey($bundlekey) {
        $this->bundlekey = $bundlekey;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setValue($value) {
        $this->value = $value;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }
    function getDeleted() {
        return $this->deleted;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getDconfig() {
        return $this->dconfig;
    }
    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }
    function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }


    function getEnableinheritance() {
        return $this->enableinheritance;
    }
    function setEnableinheritance($enableinheritance) {
        $this->dconfig = $enableinheritance;
    }

}
