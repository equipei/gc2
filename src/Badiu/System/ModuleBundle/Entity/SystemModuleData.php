<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of SystemModuleData
 *
 * @author lino
 */


/**
 * SystemModuleData
 *
 * @ORM\Table(name="system_module_data", 
 *       indexes={@ORM\Index(name="system_module_data_entity_ix", columns={"entity"}),
 *       @ORM\Index(name="system_module_data_name_ix", columns={"name"}),
 *       @ORM\Index(name="system_module_data_modulekey_ix", columns={"modulekey"}),
 *       @ORM\Index(name="system_module_data_moduleinstance_ix", columns={"moduleinstance"}),
 *       @ORM\Index(name="system_module_data_name_ix", columns={"name"}),
 *       @ORM\Index(name="system_module_data_shortname_ix", columns={"shortname"}),
 *       @ORM\Index(name="system_module_data_customint1_ix", columns={"customint1"}),
  *       @ORM\Index(name="system_module_data_customint2_ix", columns={"customint2"}),
 *       @ORM\Index(name="system_module_data_customint3_ix", columns={"customint3"}),
  *       @ORM\Index(name="system_module_data_customint4_ix", columns={"customint4"}),
  *       @ORM\Index(name="system_module_data_customint5_ix", columns={"customint5"}),
  *       @ORM\Index(name="system_module_data_customint6_ix", columns={"customint6"}),
  *       @ORM\Index(name="system_module_data_customint7_ix", columns={"customint7"}),
  *       @ORM\Index(name="system_module_data_customint8_ix", columns={"customint8"}),  
*       @ORM\Index(name="system_module_data_customdec1_ix", columns={"customdec1"}),
  *       @ORM\Index(name="system_module_data_customdec2_ix", columns={"customdec2"}),
 *       @ORM\Index(name="system_module_data_customdec3_ix", columns={"customdec3"}),
  *       @ORM\Index(name="system_module_data_customdec4_ix", columns={"customdec4"}),  
*       @ORM\Index(name="system_module_data_customchar1_ix", columns={"customchar1"}),
  *       @ORM\Index(name="system_module_data_customchar2_ix", columns={"customchar2"}),
 *       @ORM\Index(name="system_module_data_customchar3_ix", columns={"customchar3"}),
  *       @ORM\Index(name="system_module_data_customchar4_ix", columns={"customchar4"}),  
   *           @ORM\Index(name="system_module_data_bkey_ix", columns={"bkey"})})
 * @ORM\Entity
 */


class SystemModuleData {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

	
    /**
     * @var string
     *
     * @ORM\Column(name="bkey", type="string", length=255, nullable=true)
     */
    private $bkey;

    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;


    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
	
	  /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;

       /**
     * @var integer
     *
     * @ORM\Column(name="customint1", type="bigint",  nullable=true)
     */
    private $customint1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint2", type="bigint",  nullable=true)
     */
    private $customint2;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint3", type="bigint",  nullable=true)
     */
    private $customint3;
	
	    /**
     * @var integer
     *
     * @ORM\Column(name="customint4", type="bigint",  nullable=true)
     */
    private $customint4;
	
		    /**
     * @var integer
     *
     * @ORM\Column(name="customint5", type="bigint",  nullable=true)
     */
    private $customint5;
	
		    /**
     * @var integer
     *
     * @ORM\Column(name="customint6", type="bigint",  nullable=true)
     */
    private $customint6;
	
		    /**
     * @var integer
     *
     * @ORM\Column(name="customint7", type="bigint",  nullable=true)
     */
    private $customint7;
	
		    /**
     * @var integer
     *
     * @ORM\Column(name="customint8", type="bigint",  nullable=true)
     */
    private $customint8;
     /**
     * @var float
     *
     * @ORM\Column(name="customdec1", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec1;
    
    /**
     * @var float
     *
     * @ORM\Column(name="customdec2", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec2;
    /**
     * @var float
     *
     * @ORM\Column(name="customdec3", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec3;
	
	/**
     * @var float
     *
     * @ORM\Column(name="customdec4", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec4;
     /**
     * @var string
     *
     * @ORM\Column(name="customchar1", type="string", length=255, nullable=true)
     */
    private $customchar1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="customchar2", type="string", length=255, nullable=true)
     */
    private $customchar2;
    
     
    
    /**
     * @var string
     *
     * @ORM\Column(name="customchar3", type="string", length=255, nullable=true)
     */
    private $customchar3;
    
	 /**
     * @var string
     *
     * @ORM\Column(name="customchar4", type="string", length=255, nullable=true)
     */
    private $customchar4;
    
    /**
     * @var string
     *
     * @ORM\Column(name="customtext1", type="text", nullable=true)
     */
    private $customtext1;
    
     /**
     * @var string
     *
     * @ORM\Column(name="customtext2", type="text", nullable=true)
     */
    private $customtext2;
    
     /**
     * @var string
     *
     * @ORM\Column(name="customtext3", type="text", nullable=true)
     */
    private $customtext3;
   
 /**
     * @var string
     *
     * @ORM\Column(name="customtext4", type="text", nullable=true)
     */
    private $customtext4;
   
    
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;

        /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param int $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return string
     */
    public function getBkey()
    {
        return $this->bkey;
    }

    /**
     * @param string $bkey
     */
    public function setBkey($bkey)
    {
        $this->bkey = $bkey;
    }

   
	
	
     /**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string $moduleinstance
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }
    
    function getModulekey() {
        return $this->modulekey;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function getCustomint1() {
        return $this->customint1;
    }

    function getCustomint2() {
        return $this->customint2;
    }

    function getCustomint3() {
        return $this->customint3;
    }

    function getCustomint4() {
        return $this->customint4;
    }

    function getCustomdec1() {
        return $this->customdec1;
    }

    function getCustomdec2() {
        return $this->customdec2;
    }

    function getCustomdec3() {
        return $this->customdec3;
    }
function getCustomdec4() {
        return $this->customdec4;
    }
    function getCustomchar1() {
        return $this->customchar1;
    }

    function getCustomchar2() {
        return $this->customchar2;
    }

    function getCustomchar3() {
        return $this->customchar3;
    }

    function getCustomchar4() {
        return $this->customchar4;
    }

    function getCustomtext1() {
        return $this->customtext1;
    }

    function getCustomtext2() {
        return $this->customtext2;
    }

    function getCustomtext3() {
        return $this->customtext3;
    }

    function getCustomtext4() {
        return $this->customtext4;
    }

    function getDtype() {
        return $this->dtype;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    function setCustomint1($customint1) {
        $this->customint1 = $customint1;
    }

    function setCustomint2($customint2) {
        $this->customint2 = $customint2;
    }

    function setCustomint3($customint3) {
        $this->customint3 = $customint3;
    }

    function setCustomint4($customint4) {
        $this->customint4 = $customint4;
    }

    function setCustomdec1($customdec1) {
        $this->customdec1 = $customdec1;
    }

    function setCustomdec2($customdec2) {
        $this->customdec2 = $customdec2;
    }

    function setCustomdec3($customdec3) {
        $this->customdec3 = $customdec3;
    }
 function setCustomdec4($customdec4) {
        $this->customdec4 = $customdec4;
    }
    function setCustomchar1($customchar1) {
        $this->customchar1 = $customchar1;
    }

    function setCustomchar2($customchar2) {
        $this->customchar2 = $customchar2;
    }

    function setCustomchar3($customchar3) {
        $this->customchar3 = $customchar3;
    }

    function setCustomchar4($customchar4) {
        $this->customchar4 = $customchar4;
    }

    function setCustomtext1($customtext1) {
        $this->customtext1 = $customtext1;
    }

    function setCustomtext2($customtext2) {
        $this->customtext2 = $customtext2;
    }

    function setCustomtext3($customtext3) {
        $this->customtext3 = $customtext3;
    }

    function setCustomtext4($customtext4) {
        $this->customtext4 = $customtext4;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


  function setCustomint5($customint5) {
        $this->customint5 = $customint5;
    }

function getCustomint5() {
        return $this->customint5;
    }
	
	  function setCustomint6($customint6) {
        $this->customint6 = $customint6;
    }

function getCustomint6() {
        return $this->customint6;
    }

  function setCustomint7($customint7) {
        $this->customint7 = $customint7;
    }

function getCustomint7() {
        return $this->customint7;
    }

  function setCustomint8($customint8) {
        $this->customint8 = $customint8;
    }

function getCustomint8() {
        return $this->customint8;
    }	
}
