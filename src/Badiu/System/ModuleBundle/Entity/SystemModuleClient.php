<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * SystemModuleClient
 *
 * @ORM\Table(name="system_module_client", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_module_client_bkey_uix", columns={"bkey"}),
 *      @ORM\UniqueConstraint(name="system_module_client_bundle_uix", columns={"bundle"})},
 *       indexes={@ORM\Index(name="system_module_client_bkey_ix", columns={"bkey"}),
 *              @ORM\Index(name="system_module_client_bundle_ix", columns={"bundle"}),
 *              @ORM\Index(name="system_module_client_cetegory_ix", columns={"cetegory"})})
 * @ORM\Entity
 */

class SystemModuleClient {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="bkey", type="string", length=255, nullable=false)
     */
    private $bkey;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="bundle", type="string", length=255, nullable=false)
     */
    private $bundle;

    /**
 * @var string
 *
 * @ORM\Column(name="version", type="string", length=255, nullable=false)
 */
    private $version;

    /**
     * @var  \DateTime
     *
     * @ORM\Column(name="versiondate", type="bigint", nullable=true)
     */
    private $versiondate;
    /**
     * @var string
     *
     * @ORM\Column(name="cetegory", type="string", length=255, nullable=true)
     */
    private $cetegory;
     /**
     * @var boolean
     *
     * @ORM\Column(name="execcron", type="integer", nullable=false)
     */
     private $execcron;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="execcrontime", type="integer", nullable=true)
     */
     private $execcrontime;
     
      /**
     * @var  \DateTime
     *
     * @ORM\Column(name="lasttimecronexec", type="bigint", nullable=true)
     */
     private $lasttimecronexec;


    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="integer", nullable=false)
     */
  private $enabled;
    


     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getBkey()
    {
        return $this->bkey;
    }

    /**
     * @param string $bkey
     */
    public function setBkey($bkey)
    {
        $this->bkey = $bkey;
    }

    /**
     * @return string
     */
    public function getBundle()
    {
        return $this->bundle;
    }

    /**
     * @param string $bundle
     */
    public function setBundle($bundle)
    {
        $this->bundle = $bundle;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getCetegory()
    {
        return $this->cetegory;
    }

    /**
     * @param string $cetegory
     */
    public function setCetegory($cetegory)
    {
        $this->cetegory = $cetegory;
    }

    /**
     * @return boolean
     */
    public function isExeccron()
    {
        return $this->execcron;
    }

    /**
     * @param boolean $execcron
     */
    public function setExeccron($execcron)
    {
        $this->execcron = $execcron;
    }

    /**
     * @return int
     */
    public function getExeccrontime()
    {
        return $this->execcrontime;
    }

    /**
     * @param int $execcrontime
     */
    public function setExeccrontime($execcrontime)
    {
        $this->execcrontime = $execcrontime;
    }

    /**
     * @return \DateTime
     */
    public function getLasttimecronexec()
    {
        return $this->lasttimecronexec;
    }

    /**
     * @param \DateTime $lasttimecronexec
     */
    public function setLasttimecronexec($lasttimecronexec)
    {
        $this->lasttimecronexec = $lasttimecronexec;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return string
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * @param string $param
     */
    public function setParam($param)
    {
        $this->param = $param;
    }

    /**
     * @return \DateTime
     */
    public function getTimecreated()
    {
        return $this->timecreated;
    }

    /**
     * @param \DateTime $timecreated
     */
    public function setTimecreated($timecreated)
    {
        $this->timecreated = $timecreated;
    }

    /**
     * @return \DateTime
     */
    public function getTimemodified()
    {
        return $this->timemodified;
    }

    /**
     * @param \DateTime $timemodified
     */
    public function setTimemodified($timemodified)
    {
        $this->timemodified = $timemodified;
    }

    /**
     * @return \DateTime
     */
    public function getVersiondate()
    {
        return $this->versiondate;
    }

    /**
     * @param \DateTime $versiondate
     */
    public function setVersiondate($versiondate)
    {
        $this->versiondate = $versiondate;
    }



}
