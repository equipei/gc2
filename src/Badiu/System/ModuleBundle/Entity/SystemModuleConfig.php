<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\ModuleBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of SystemModuleConfig
 *
 * @author lino
 */


/**
 * SystemModuleConfig
 *
 * @ORM\Table(name="system_module_config", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_module_config_uix", columns={"bundlekey","name"})},
 *       indexes={
 *				@ORM\Index(name="system_module_config_name_ix", columns={"name"}),
 *				@ORM\Index(name="system_module_config_bundlekey_ix", columns={"bundlekey"}),
 *				@ORM\Index(name="system_module_config_ctype_ix", columns={"ctype"}),
 *				@ORM\Index(name="system_module_config_clevel_ix", columns={"clevel"})})
 * @ORM\Entity
 */


class SystemModuleConfig {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

      /**
     * @var string
     *
     * @ORM\Column(name="bundlekey", type="string", length=255, nullable=false)
     */
    private $bundlekey;
  

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

		/**
     * @var string
     *
     * @ORM\Column(name="ftype", type="string", length=255, nullable=true)
     */
    private $ftype;
	
	
		/**
     * @var string
     *
     * @ORM\Column(name="ctype", type="string", length=255, nullable=true)
     */
    private $ctype;
	
		/**
     * @var string
     *
     * @ORM\Column(name="cdefault", type="string", length=255, nullable=true)
     */
    private $cdefault;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="clevel", type="bigint",  nullable=true)
     */
    private $clevel;
    
    
	     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    function getId() {
        return $this->id;
    }

  
    function getName() {
        return $this->name;
    }

    function getValue() {
        return $this->value;
    }

    function getFtype() {
        return $this->ftype;
    }

    function getCtype() {
        return $this->ctype;
    }

    function getCdefault() {
        return $this->cdefault;
    }

    function getClevel() {
        return $this->clevel;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setValue($value) {
        $this->value = $value;
    }

    function setFtype($ftype) {
        $this->ftype = $ftype;
    }

    function setCtype($ctype) {
        $this->ctype = $ctype;
    }

    function setCdefault($cdefault) {
        $this->cdefault = $cdefault;
    }

    function setClevel($clevel) {
        $this->clevel = $clevel;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function getBundlekey() {
        return $this->bundlekey;
    }

    function setBundlekey($bundlekey) {
        $this->bundlekey = $bundlekey;
    }



}
