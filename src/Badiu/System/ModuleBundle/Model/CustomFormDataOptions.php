<?php

namespace Badiu\System\ModuleBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;

class CustomFormDataOptions extends BadiuFormDataOptions{
     
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       } 
              
    
    public  function getType(){
        $list=array();
        $list['entity']=$this->getTranslator()->trans('badiu.system.module.custom.typeentity');
        $list['user']=$this->getTranslator()->trans('badiu.system.module.custom.typeuser');
        $list['record']=$this->getTranslator()->trans('badiu.system.module.custom.typerecord');
	return $list;
    } 

}
