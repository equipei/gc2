<?php
namespace Badiu\System\ModuleBundle\Model\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class TranslatorInstanceData  extends  BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

    public function getListConsolidateByLocale($entity) {
		$r=FALSE;
        $sql="SELECT  o.locale, COUNT(o.id) AS countstring FROM ".$this->getBundleEntity()." o  WHERE o.entity=:entity  GROUP BY o.locale";
        $query = $this->getEm()->createQuery($sql);
		$query->setParameter('entity',$entity);
        $result= $query->getResult();
         return $result;
    }
	
	public function getList($param) {
		$maxrecord=20000;
		if(isset($param['maxrecord'])){
			$maxrecord=$param['maxrecord'];
			unset($param['maxrecord']);
		}
		
		$mkey="";
		if(isset($param['mkey'])){
			$mkey=$param['mkey'];
			unset($param['mkey']);
		}
		$wsql="";
		if(!empty($mkey)){
			 $mkey=addslashes($mkey);
			 $wsql=" AND o.mkey LIKE  '%".$mkey."%'";
			}
		$textfind="";
		$textcasesensitive="";
		if(isset($param['textfind'])){
			$textfind=$param['textfind'];
			unset($param['textfind']);
		}
		if(isset($param['textcasesensitive'])){
			$textcasesensitive=$param['textcasesensitive'];
			unset($param['textcasesensitive']);
		}
		if(!empty($textfind)){
			$wsql.=" AND o.messageidx LIKE  '%".$textfind."%' ";
			if(!$textcasesensitive){$wsql.=" AND LOWER(o.messageidx) LIKE  '%".strtolower($textfind)."%' ";}
		}
		
		if(isset($param['messagenotnull'])){
			$messagenotnull=$param['messagenotnull'];
			if($messagenotnull==1){$wsql.=" AND o.message IS NOT NULL "; }
			unset($param['messagenotnull']);
		}
		$pwsql=$this->makeSqlWhere($param);
		
		$fwsql="$wsql $pwsql";
	
        $sql="SELECT  o.id,o.mkey,o.message,o.modulekey,o.moduleinstance FROM ".$this->getBundleEntity()." o  WHERE o.id > 0 $fwsql ";
        $query = $this->getEm()->createQuery($sql);
		$query=$this->makeSqlFilter($query, $param);
		$query->setMaxResults($maxrecord);
        $result= $query->getResult();
         return $result;
    }
	
	public function getListForReplace($param) {
		$maxrecord=20000;
		if(isset($param['maxrecord'])){
			$maxrecord=$param['maxrecord'];
			unset($param['maxrecord']);
		}
		
		$mkey="";
		if(isset($param['mkey'])){
			$mkey=$param['mkey'];
			unset($param['mkey']);
		}
		$wsql="";
		if(!empty($mkey)){
			 $mkey=addslashes($mkey);
			 $wsql=" AND o.mkey LIKE  '%".$mkey."%'";
			}
		$textfind="";
		$textcasesensitive="";
		if(isset($param['textfind'])){
			$textfind=$param['textfind'];
			unset($param['textfind']);
		}
		if(isset($param['textcasesensitive'])){
			$textcasesensitive=$param['textcasesensitive'];
			unset($param['textcasesensitive']);
		}
		if(empty($textfind)){return null;}
		else{
			$wsql.=" AND o.messageidx LIKE  '%".$textfind."%' ";
			if(!$textcasesensitive){$wsql.=" AND LOWER(o.messageidx) LIKE  '%".strtolower($textfind)."%' ";}
		}
		$pwsql=$this->makeSqlWhere($param);
		
		$fwsql="$wsql $pwsql";
	
        $sql="SELECT  o.id,o.mkey,o.message,o.modulekey,o.moduleinstance FROM ".$this->getBundleEntity()." o  WHERE o.id > 0 $fwsql ";
        $query = $this->getEm()->createQuery($sql);
		$query=$this->makeSqlFilter($query, $param);
		$query->setMaxResults($maxrecord);
        $result= $query->getResult();
         return $result;
    }
}
