<?php

namespace Badiu\System\ModuleBundle\Model\Translator;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CoreFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
    }
    
     
    public function changeParam() {

        $message = $this->getParamItem('message');
        $tmanage=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
		$messageidx=$tmanage->getMessageidx($message);  
		$param = $this->getParam();
        $param['messageidx'] = $messageidx;
        $this->setParam($param);
        
     }
     
         
    
}
