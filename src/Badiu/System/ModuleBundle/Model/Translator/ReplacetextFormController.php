<?php

namespace Badiu\System\ModuleBundle\Model\Translator;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ReplacetextFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
    }
    
     
	 public function save() {
          $param = $this->getParam();
		
		   $tmanage=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
		   $countreplace=$tmanage->replaceText($param);
		   
		   		   
		   $msg=$this->getTranslator()->trans('badiu.system.module.translatorreplacetext.resultinfo',array('%countamout%'=>$countreplace));
		   $this->setSuccessmessage($msg);
		   $urlgoback=null;//$this->getContainer()->get('badiu.system.core.lib.util.app')->getUrlByRoute('badiu.system.module.translatorcommand.dashboard');
       
           $outrsult=array('result'=>$countreplace,'message'=>$this->getSuccessmessage(),'urlgoback'=>$urlgoback);
          $this->getResponse()->setStatus("accept");
          $this->getResponse()->setMessage($outrsult);
         return $this->getResponse()->get();
         
     }

}
