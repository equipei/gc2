<?php

namespace Badiu\System\ModuleBundle\Model\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;

class CoreFormDataOptions extends BadiuFormDataOptions{
     
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       } 
              
    
    public  function getOperation(){
        $list=array();
        $list['insert']=$this->getTranslator()->trans('badiu.system.module.translator.operation.insert');
        $list['update']=$this->getTranslator()->trans('badiu.system.module.translator.operation.update');
        $list['insertupdate']=$this->getTranslator()->trans('badiu.system.module.translator.operation.insertupdate');
	return $list;
    } 
 public  function getStatus(){
        $list=array();
		
        $list['imported']=$this->getTranslator()->trans('badiu.system.module.translator.status.imported');
		$list['pending']=$this->getTranslator()->trans('badiu.system.module.translator.status.pending');
        $list['inreview']=$this->getTranslator()->trans('badiu.system.module.translator.status.inreview');
        $list['complete']=$this->getTranslator()->trans('badiu.system.module.translator.status.complete');
	return $list;
    }
public  function getPackages(){
        $list=array();
		$tmanage=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
		$listpg=$tmanage->getPackages();
		foreach ($listpg as $pgitem) {
			$locale=$this->getUtildata()->getVaueOfArray($pgitem,'locale');
			$list[$locale]=$this->getTranslator()->trans('badiu.system.module.translator.package.'.$locale);
		}
   	 return $list;
    } 

}
