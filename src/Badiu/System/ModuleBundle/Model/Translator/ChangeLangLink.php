<?php

namespace Badiu\System\ModuleBundle\Model\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ChangeLangLink extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
			   
	function exec() {
		
		$lang=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('lang');
		$urlgoback=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urlgoback');
	
		if(empty($lang)){ return null;}
		$session=$this->getContainer()->get('badiu.system.access.session');
		$dsession=$session->get();
		$dsession->setLang($lang);
		$session->save($dsession);
		
		if(!empty($urlgoback)){
			header('Location: '.$urlgoback);
            exit;
		}else{echo "Required param _urlgoback";exit;}
		
	}
	
}			  