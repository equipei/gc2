<?php

namespace Badiu\System\ModuleBundle\Model\Translator;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CoreCopyFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
    }
    
     
	 public function save() {
          $param = $this->getParam();
		   $tmanage=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
		   $presult=$tmanage->copyPackage($param);
		   
		   $insert=$this->getUtildata()->getVaueOfArray($presult,'insert');
		   $update=$this->getUtildata()->getVaueOfArray($presult,'update');
		   
		   $amout=$update+$insert;
		   		   
		   $msg=$this->getTranslator()->trans('badiu.system.module.translatorcopy.resultinfo',array('%countamout%'=>$amout,'%countinsert%'=>$insert,'%countupdate'=>$update));
		   $this->setSuccessmessage($msg);
		   $urlgoback=$this->getContainer()->get('badiu.system.core.lib.util.app')->getUrlByRoute('badiu.system.module.translatorcommand.dashboard');
       
           $outrsult=array('result'=>$amout,'message'=>$this->getSuccessmessage(),'urlgoback'=>$urlgoback);
          $this->getResponse()->setStatus("accept");
          $this->getResponse()->setMessage($outrsult);
         return $this->getResponse()->get();
         
     }

}
