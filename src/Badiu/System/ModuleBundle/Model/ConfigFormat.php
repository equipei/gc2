<?php

namespace Badiu\System\ModuleBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;

class ConfigFormat extends BadiuFormat {
	private $markdownaparse;
    function __construct(Container $container) {
        parent::__construct($container);
		$this->markdownaparse=$this->getContainer()->get('badiu.system.core.lib.markdow.defaultparse');
    }

    public function pvalue($data) {
        $result = null;
        if (array_key_exists("value", $data)) {$result=$data["value"];}
        $len=strlen($result);
        if( $len > 50){
            $partial= substr($result,0,50);
            $partial.="...";
           $result="<a  data-toggle=\"tooltip\" href=\"#\" data-placement=\"top\" title=\"$result\">$partial</a>";
            
        }
        


        return $result;
    }

	public function codevalue($data) {
        $result = null;
        $value= $this->getUtildata()->getVaueOfArray($data, 'value');
        if(empty($value)){return null;}
		
		$txt='``` yml
        '.$value.'
	    ```';
		$result = $this->markdownaparse->text($txt);

        return $result;
    }
   
}
