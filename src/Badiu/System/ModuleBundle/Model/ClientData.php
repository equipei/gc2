<?php
namespace Badiu\System\ModuleBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ClientData  extends  BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

    public function exist($id) {

        $r=FALSE;
        $sql="SELECT  COUNT(o.bkey) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.bkey = :id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id',$id);
        $result= $query->getSingleResult();
        if($result['countrecord']>0){$r=TRUE;}
        return $r;
    }
}
