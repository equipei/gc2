<?php

namespace Badiu\System\ModuleBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class OverrideConfig extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
        
	public function get($param) {
      /*  $param=new \stdClass();
        $param->key='badiu.moodle.mreport.user.caccess.filter.form.config';
        $param->conflevel1='defaultdata=_datasource/servicesql|_serviceid/{parentid}&defaultdataonpenform=confirmed/1|deleted/0|recentaccess/[number:20,unittime:system_time_unit_minute]&maxlimit=recentaccess/[number:24,unittime:system_time_unit_hour]';
        $param->conflevel2='maxlimit=recentaccess/[number:12,unittime:system_time_unit_hour]';
        $param->override=true;*/
       
         $result=null;
         $key=$param->key;
         $conflevel1=$param->conflevel1;      
         $conflevel2=$param->conflevel2;
         $override=$param->override;
        if(empty($conflevel2) && !empty($conflevel1)){
          $result=$conflevel1;
           return $result;
        }else if(!empty($conflevel2) && empty($conflevel1)){
          $result=$conflevel2;
          return $result;
        }
        else if(empty($conflevel2) && empty($conflevel1)){return $result;}

        if(!$override){ 
            $result=$conflevel2;
            return $result;
        }
        $type=$this->getType($key);
       
        if($type==0){$result=$conflevel2;}
        else if($type==10){$result=$this->httpQueryString($conflevel1,$conflevel2);}
        else if($type==20){$result=$this->httpQueryString($conflevel1,$conflevel2,true);}
        else if($type==30){$result=$this->httpQueryStringValueWithCommaSeparated($conflevel1,$conflevel2);}
        else if($type==40){$result=$this->sqlCommand($conflevel1,$conflevel2);}
        
        return $result;

  } 
  /**
   * define type of key in number
   *  0 - for key without key/value
   * 10 - for key with http key/value
   * 20 - for key with hppt and subhttp key/value
   * 30 - for key with hppt and list value separate by comma
   * 40 - SQL command
   */
  public function getType($key) {
     $resut=0;
     $keyutil= $this->getContainer()->get('badiu.system.core.lib.config.keyutil');
     if($keyutil->endWith($key,'.page.config')){
        $resut=10;
     }else if($keyutil->endWith($key,'.form.config')){
        $resut=20;
     }else if($keyutil->endWith($key,'.form.fields.enable')){
        $resut=30;
     }else if($keyutil->endWith($key,'.form.fields.required')){
      $resut=10;
     }else if($keyutil->endWith($key,'.form.fields.cssclass')){
        $resut=10;
     }else if($keyutil->endWith($key,'.form.fields.type')){
      $resut=10;
    }else if($keyutil->endWith($key,'.form.fields.config')){
      $resut=10;
    }else if($keyutil->endWith($key,'.form.fields.choicelist')){
      $resut=10;
    }else if($keyutil->endWith($key,'.form.fields.label')){
      $resut=10;
    }else if($keyutil->endWith($key,'.form.fields.placeholder')){
      $resut=10;
    }
    
    else if($keyutil->endWith($key,'.fields.format')){
      $resut=10;
    }else if($keyutil->endWith($key,'.fields.table.view')){
      $resut=10;
    }else if($keyutil->endWith($key,'.fields.table.view.link')){
      $resut=10;
    }else if($keyutil->endWith($key,'.fields.table.view.label')){
      $resut=10;
    }else if($keyutil->endWith($key,'.dbsearch.sql.count')){
      $resut=40;
    }else if($keyutil->endWith($key,'.dbsearch.sql')){
      $resut=40;
    }
    
    //dashboard
    else if($keyutil->endWith($key,'.dashboard.db.sql.config')){
      $resut=10;
    } else if($keyutil->hasText($key,'dashboard.db.sql.')){
      $resut=10;
    } else if($keyutil->hasText($key,'dashboard.db.sql.')){
      $resut=10;
    }

    //menu
    else if($keyutil->endWith($key,'.menu')){
      $resut=10;
    }else if($keyutil->endWith($key,'.menu.global')){
      $resut=10;
    }
    else if($keyutil->endWith($key,'.menu.navbar')){
      $resut=10;
    }else if($keyutil->endWith($key,'.menu.navbar.global')){
      $resut=10;
    }
    else if($keyutil->endWith($key,'.menu.content')){
      $resut=10;
    }else if($keyutil->endWith($key,'.menu.content.global')){
      $resut=10;
    }
    return  $resut;
  }

  public function httpQueryString($conflevel1,$conflevel2,$subparam=false) {
      $querystring1= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
      $querystring2= $this->getContainer()->get('badiu.system.core.lib.http.querystring');

      $querystring1->setQuery($conflevel1);
      $querystring1->makeParam();
      
      $querystring2->setQuery($conflevel2);
      $querystring2->makeParam();

      if($subparam){
        $subquerystring1= $this->getContainer()->get('badiu.system.core.lib.http.subquerystring');
        $subquerystring2= $this->getContainer()->get('badiu.system.core.lib.http.subquerystring');
        foreach ($querystring2->getParam() as $key => $value2){
            if($this->isSubQuery($value2)){
               $subquerystring1->setQuery("");
              $subquerystring2->setQuery("");
              $value1=$querystring1->getValue($key);
              if(!empty($value1)){
                $subquerystring1->setQuery($value1);
                $subquerystring1->makeParam();
                
                $subquerystring2->setQuery($value2);
                $subquerystring2->makeParam();

                foreach ($subquerystring2->getParam() as $skey => $svalue){
                    $subquerystring1->add($skey,$svalue);
                }
                $subquerystring1->makeQuery();
                $nvalue=$subquerystring1->getQuery();
                 $querystring2->add($key,$nvalue);

              }


              
            }
          
        }
    
      }
      
      // Joint param
      foreach ($querystring2->getParam() as $key => $value){
        $querystring1->add($key,$value);
      }

      

      $querystring1->makeQuery();
      return  $querystring1->getQuery();


  }

  public function httpQueryStringValueWithCommaSeparated($conflevel1,$conflevel2) {

    //if  single form
    $pos=stripos($conflevel1, "=");
    if($pos=== false){

      $list=$this->valueWithCommaSeparated($conflevel1,$conflevel2);
      return $list;
    }
    
    //if advanced form 

    $querystring1= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
    $querystring2= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
  $querystring1->setQuery($conflevel1);
    $querystring1->makeParam();
    
    $querystring2->setQuery($conflevel2);
    $querystring2->makeParam();

      $subquerystring1= $this->getContainer()->get('badiu.system.core.lib.http.subquerystring');
      $subquerystring2= $this->getContainer()->get('badiu.system.core.lib.http.subquerystring');
      foreach ($querystring2->getParam() as $key => $value2){
        
            $subquerystring1->setQuery("");
            $subquerystring2->setQuery("");
            $value1=$querystring1->getValue($key);
            $list1=$this->getListvalueWithCommaSeparated($value1);
            $list2=$this->getListvalueWithCommaSeparated($value2);

            if(!empty($list1)){
              
              foreach ($list2 as $svalue2){
                array_push($list1, $svalue2);
              }

              $subquerystring1->makeQuery();
              $nvalue=$this->castArrayToCommaSeparated($list1);
               $querystring2->add($key,$nvalue);

            }
     }
  

    
    // Joint param
    foreach ($querystring2->getParam() as $key => $value){
      $querystring1->add($key,$value);
    }



    $querystring1->makeQuery();

    return  $querystring1->getQuery();


}



public function sqlCommand($conflevel1,$conflevel2) {

  //check if exist param
  $exitparam=false;
  $param1=stripos($conflevel2, "@filter");
  $param2=stripos($conflevel2, "@column");
  if($param1!== false){$exitparam=true;}
  if($param2!== false){$exitparam=true;}

  if(!$exitparam){
    return $conflevel2;
   }

   //if exist param
  $querystring2= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
  $querystring2->setQuery($conflevel2);
  $querystring2->makeParam();

  $filter=$querystring2->getValue("@filter");
  $column=$querystring2->getValue("@column");


  //replace @filter
  $conflevel1=str_replace("@filter",$filter,$conflevel1);

    //replace @column
    $conflevel1=str_replace("@column",$column,$conflevel1);

  return  $conflevel1;


}
public function valueWithCommaSeparated($conflevel1,$conflevel2) {
  $list1=$this->getListvalueWithCommaSeparated($conflevel1);
  $list2=$this->getListvalueWithCommaSeparated($conflevel2);
  
  foreach ($list2 as $value) {
    array_push($list1, $value);
  }
  $str=$this->castArrayToCommaSeparated($list1);
  return $str;
}

public function getListvalueWithCommaSeparated($param){
  $list=array();
  $pos1=stripos($param, ",");
  if($pos1=== false){
    $list=array($param);
  }else{
    $list= explode(",", $param);
  }
  return $list;
}

public function castArrayToCommaSeparated($param){
  $str="";
  $cont=0;
  foreach ($param as $value) {
    if($cont==0){$str=$value;}
    else{$str.=",".$value;}
    $cont++;
  }
  return  $str;
}
  public function isSubQuery($param){
    $result=false;
     $posk=stripos($param, "|");
    if($posk!== false){ $result=true;}
    $posv=stripos($param, "/");
    if($posv!== false){$result=true;}

    if($posk){$result=true;}
    if($posv){$result=true;}
    return $result;
  }
}
