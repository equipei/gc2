<?php

namespace Badiu\System\ModuleBundle\Model\Lib\Tag;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ManageTranslator extends BadiuModelLib {

	function __construct(Container $container) {
                parent::__construct($container);
           
       }
	
	public function  exportText($param){
		$cont=0;

		$package=$this->getUtildata()->getVaueOfArray($param,'package');
		$operation=$this->getUtildata()->getVaueOfArray($param,'operation');
		$status=$this->getUtildata()->getVaueOfArray($param,'status');
		 $entity=$this->getEntity();
		$listreport=$this->getList($param);
		
		if(!is_array($listreport)){return $cont;}
		$mparam=array('modulekey'=>'badiu.sync.freport.freport','locale'=>$package,'mstatus'=>$status,'entity'=>$entity,'operation'=>$operation);
		foreach($listreport as $row) {
			$value=$this->getUtildata()->getVaueOfArray($row,'value');
			$modulekey=$this->getUtildata()->getVaueOfArray($row,'modulekey');
			$moduleinstance=$this->getUtildata()->getVaueOfArray($row,'moduleinstance');
			$reference=$this->getUtildata()->getVaueOfArray($row,'reference');
			
			$mparam['mkey']=$reference;
			$mparam['message']=$value;
			$mparam['moduleinstance']=$moduleinstance;
			$r=$this->add($mparam);
			if($r){$cont++;}
			
		}
		
		return $cont;
	}
	
	  private function getList($param) {
		 $operation=$this->getUtildata()->getVaueOfArray($param,'operation');
		 $package=$this->getUtildata()->getVaueOfArray($param,'package');
		 $entity=$this->getEntity();
        $tagdata = $this->getContainer()->get('badiu.system.module.tag.data');
        $locale=$this->getLang();
		$wsql="";
		if($operation=='insert'){$wsql=" AND (SELECT COUNT(ti.id) FROM BadiuSystemModuleBundle:SystemModuleTranslatorInstance ti WHERE ti.moduleinstance=o.moduleinstance AND ti.entity=o.entity AND ti.modulekey=o.modulekey AND ti.locale='".$package."' ) =0 "; }
		else if ($operation=='update'){$wsql=" AND (SELECT COUNT(ti.id) FROM BadiuSystemModuleBundle:SystemModuleTranslatorInstance ti WHERE ti.moduleinstance=o.moduleinstance AND ti.entity=o.entity AND ti.modulekey=o.modulekey AND ti.locale='".$package."') > 0 "; }
        $sql="SELECT o.id,o.value, o.modulekey,o.moduleinstance,o.reference FROM BadiuSystemModuleBundle:SystemModuleTag o  WHERE o.entity= :entity AND o.locale=:locale $wsql";
		$query =$tagdata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('locale',$locale);
       
        $result = $query->getResult();
      
        return $result;
    }
	
	 private function add($param) {
		 $tlmanage=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
		 
		 $mkey=$this->getUtildata()->getVaueOfArray($param,'mkey');
		 $message=$this->getUtildata()->getVaueOfArray($param,'message');
		 $messageidx=$tlmanage->getMessageidx($message);
		 $parentid=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $locale=$this->getUtildata()->getVaueOfArray($param,'locale');
		 $mstatus=$this->getUtildata()->getVaueOfArray($param,'mstatus');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		
		 $operation=$this->getUtildata()->getVaueOfArray($param,'operation');
		 
		 $session=$this->getContainer()->get('badiu.system.access.session');
		 $sessiondata=$session->get();
		 $useridadd=$sessiondata->getUser()->getId();
		 
		$tdata=$this->getContainer()->get('badiu.system.module.translatorinstance.data');
        $paramadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$parentid,'mstatus'=>$mstatus,'locale'=>$locale,'mkey'=>$mkey,'message'=>$messageidx,'messageidx'=>$messageidx,'useridadd'=>$useridadd,'deleted'=>0,'timecreated'=>new \DateTime());
		$paramedit=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$parentid,'mstatus'=>$mstatus,'locale'=>$locale,'mkey'=>$mkey,'message'=>$messageidx,'messageidx'=>$messageidx,'useridadd'=>$useridadd,'deleted'=>0,'timemodified'=>new \DateTime());
		if($operation=='insert'){$paramedit=null;}
		
		$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$parentid,'locale'=>$locale,'mkey'=>$mkey);
        $result=$tdata->addNativeSql($paramcheckexist,$paramadd,$paramedit,true);
        $r=$this->getUtildata()->getVaueOfArray($result,'id');
		return $r;
    }
}
?>