<?php

namespace Badiu\System\ModuleBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ConfigForm extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
          	public function getDataFormEdit($instanceid) {
			$fdata=array();
                    	 $fdata=$this->getDefaultDb($instanceid,$fdata);
			return $fdata;
	}
	public function save($data) {
         
			$sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
			$isEdit=$sysoperation->isEdit();
			$instanceid=null;
			if($isEdit){
				$instanceid=$this->getContainer()->get('request')->get('id');
			}
			$dto=$data->getDto();
                         $param=$this->getDefaultForm($instanceid,$dto);
			//check if exist
                        $paramck=array('entity'=>$param['entity'],'modulekey'=>$param['modulekey'],'name'=>$param['name'],'dtype'=>$param['dtype'],'ftype'=>$param['ftype'],'instanceid'=>$param['instanceid'],'ctype'=>$param['ctype'],'cdefault'=>$param['cdefault'],'clevel'=>$param['clevel']);
			$exist=$data->countNativeSql($paramck, false);
                       $result=null; 
                      // echo $exist;exit;
                        if($exist){
                            $id=$data->getIdByParam($paramck);
                            $param['id']=$id;
                            $result=$data->updateNativeSql($param, false);
                        }else{
                           $result=$data->insertNativeSql($param, false); 
                          $param['id']=$result;
                        }
                       
                        
			
                       
		       return $param;
		}
	public function getDefaultDb($instanceid,$fdata) {
			$data=$this->getContainer()->get('badiu.system.module.config.data');
                        $dto=$data->findById($instanceid);
                   
			$fdata['name']=$dto->getName();
                        $fdata['value']=$dto->getValue();
                        $fdata['ftype']=$dto->getFtype();
                       // $fdata['instanceid']=$dto->getInstanceid();
                        $fdata['ctype']=$dto->getCtype();
                        $fdata['cdefault']=$dto->getCdefault();
                        $fdata['clevel']=$dto->getClevel();
                        $fdata['param']=$dto->getParam();
                        
                       
			return $fdata;
		
	}
	
	public function getDefaultForm($instanceid,$fdata) {
			$dto=null;
                        $dtoconf=null;
                        if(!empty($instanceid)){
				$data=$this->getContainer()->get('badiu.system.module.config.data');
				$dtoconf=$data->findById($instanceid);
                        }
                           if(!empty($dtoconf)){
                           $fdata['modulekey']=$dtoconf->getModulekey();
                       }
                       
                        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                        $entity=$badiuSession->get()->getEntity();
                        $fdata['entity']=$entity;
                        if(empty($fdata['dtype'])){
                            $fdata['dtype']='entity';
                            $fdata['instanceid']=$entity;
                        }
                     return $fdata;
		
	}   
}
