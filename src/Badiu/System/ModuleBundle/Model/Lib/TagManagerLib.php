<?php

namespace Badiu\System\ModuleBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class TagManagerLib extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    } 


	public function add($param) { 
	
		if(empty($param)){return null;}
		if(!is_array($param)){return null;}
		
		$modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		$moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		$tag=$this->getUtildata()->getVaueOfArray($param,'_badiu_value');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		if(empty($entity)){$entity=$this->getEntity();}
		if(empty($tag)){return null;}
		$tag=$this->getUtildata()->castStringToArray($tag);
		
		$tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
		
		//remove tag
		$fparam=array('modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'locale'=>$this->getLang());
		//$tagdata->removeByParam($entity,$fparam);
		$tagdata->removeWithoutDtype($entity,$fparam);
		$cont=0;
		foreach ($tag as $t) {
			$reference=$this->generateUniqueKey($t);
			$iparam=array('entity'=>$entity,'modulekey'=>$modulekey,'value'=>$t,'moduleinstance'=>$moduleinstance,'locale'=>$this->getLang(),'reference'=>$reference,'deleted'=>0,'timecreated'=>new \DateTime());
			$dresult=$tagdata->insertNativeSql($iparam,false);
			if($dresult){$cont++;}
		}
		return $dresult; 
	}
	
	public function get($param) { 
		$param['locale']=$this->getLang();
		$tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
		//$list=$tagdata->getGlobalColumnValues('value',$param);
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		if(empty($entity)){$entity=$this->getEntity();}
		$list=$tagdata->getValuesWithoutDtype($entity,$param);
		if(empty($list)){return null;}
		$newlist=array();
		foreach ($list as $t) {
			$v=$this->getUtildata()->getVaueOfArray($t,'value');
			array_push($newlist,$v);
		}
		return $newlist;
	}
	
	public function getForAutocomplete() {
		 $p=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		 $value=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
		 $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        
        $wsql="";
		
        if(!empty($value)){
            $wsql.=" AND LOWER(o.value)  LIKE :value ";
            $value=strtolower($value);
        }
		$tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
         $sql="SELECT DISTINCT o.value AS name FROM ".$tagdata->getBundleEntity()." o WHERE  o.entity = :entity  $wsql ";
       
        $query = $tagdata->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
       if(!empty($value)){$query->setParameter('value','%'.$value.'%');}
        $query->setMaxResults(15);
        $result= $query->getResult();
        return  $result;
    }
	
	
	
/**
 * Generates a unique key for a given text based on its characteristics and a SHA-256 hash.
 *
 * @param string $text The input text for which the key needs to be generated.
 * @return string The generated unique key truncated to 50 characters.
 */
function generateUniqueKey($text) {
    // a) Number of characters
    $charCount = strlen($text);
    
    // b) Number of white spaces
    $whiteSpaceCount = substr_count($text, ' ');
    
    // c) Number of vowels
    $vowelCount = preg_match_all('/[aeiouAEIOU]/', $text);
    
    // e) Number of non-vowel characters
    $nonVowelCount = $charCount - $vowelCount;

    // Concatenating the attributes to the original text
    $combinedText = $charCount . $whiteSpaceCount . $vowelCount . $nonVowelCount;

	//count vowels
	$countvowels=$this->countVowels($text);

    // Generating the SHA-256 hash
    $hash = hash('sha256', $text);

	$base="tag-$countvowels-$combinedText";
	
	//create key
     $key="tag-$hash-$combinedText-$countvowels";
    return $key;
}

/**
 * Generates a string representing the counts of vowels in the given text.
 *
 * @param string $text The input text for which the counts need to be generated.
 * @return string The concatenated string of vowel counts.
 */
function countVowels($text) {
    $vowels = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'];
    $result = "";

    foreach ($vowels as $vowel) {
        $count = substr_count($text, $vowel);
        $result .= $count;
      
    }

    return $result;
}
}
