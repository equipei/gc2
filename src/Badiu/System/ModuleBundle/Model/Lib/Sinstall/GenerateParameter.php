<?php

namespace Badiu\System\ModuleBundle\Model\Lib\Sinstall;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

Class GenerateParameter extends BadiuModelLib {

	private $directory;
	private $filename = 'parameters1.yml';
	private $paranfile;

	function __construct(Container $container) {
		parent::__construct($container);

		$this->directory = dirname($this->getContainer()->getParameter('kernel.root_dir')) . '/app/config/';
		$this->paranfile = $this->directory . $this->filename;
	}

	public function insert($v) {
		//$newarray = array('parameters' => array('database_driver' => 'pdo_mysql', 'database_host' => '127.0.0.1', 'database_name' => 'badiunet', 'database_password' => 'hrtlopes87', 'database_user' => 'root', 'mailer_transport' => 'gmail', 'mailer_auth_mode' => 'login', 'humberto' => 'fixola'));

		$data['parameters'] = $v;
		$this->generateParameters($data);
		print_r($data);
	}
	public function update() {
		$key_old = 'database_driver';
		$key_new = 'fixola';
		$nvalue = 'Fr';
		$this->updateParameters($key_old, $key_new, $nvalue);
	}
	public function generateParameters($newarray) {

		$default = array(
			'parameters' => array(),
		);

		if (!file_exists($this->paranfile)) {

			$comand_file = 'touch ' . $this->paranfile;

			shell_exec($comand_file);

			try {
				$yaml = Yaml::dump($default);

				file_put_contents($this->paranfile, $yaml);
			} catch (ParseException $e) {
				printf("Unable to parse the YAML string: %s", $e->getMessage());
			}
		}

		if (is_array($newarray)) {
			try {
				$origin = Yaml::parse(file_get_contents($this->paranfile));

				foreach ($newarray['parameters'] as $key => $v) {
					//if (!array_key_exists($key, $origin['parameters'])) {
					$origin['parameters'][$key] = $v;
					//}
				}

				$yaml = Yaml::dump($origin);
				file_put_contents($this->paranfile, $yaml);
			} catch (ParseException $e) {
				printf("Unable to parse the YAML string: %s", $e->getMessage());
			}
		} else {
			echo 'Verifie your format';
		}
	}

	public function updateParameters($key_old, $key_new, $nvalue) {
		try {
			$origin = Yaml::parse(file_get_contents($this->paranfile));

			if (array_key_exists($key_old, $origin['parameters'])) {
				unset($origin['parameters'][$key_old]);
				$origin['parameters'][$key_new] = $nvalue;
			}
			$yaml = Yaml::dump($origin);
			file_put_contents($this->paranfile, $yaml);
		} catch (ParseException $e) {
			printf("Unable to parse the YAML string: %s", $e->getMessage());
		}
	}

}
