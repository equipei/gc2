<?php

namespace Badiu\System\ModuleBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ManageConfig extends BadiuModelLib{

    function __construct(Container $container) {
            parent::__construct($container);
              }
    
        
	public function getByBaseKey($basekey, $moduleinstance=null) {
                if(empty($moduleinstance)){
                        $moduleinstance=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
                } 
                $list=array();
                $list[$basekey]=null;
                $entity=$this->getEntity();
                //get global
                //log
                $globaldata = $this->getContainer()->get('badiu.system.module.configglobal.data');
                $listglobal=$globaldata->getConfigByNameStartWith($basekey);
                
                //get entity
                $globalentity = $this->getContainer()->get('badiu.system.module.configentity.data');
                $listentity=$globalentity->getConfigByNameStartWith($entity,$basekey);

                //get instance
                $listinstace=array();
                if(!empty($moduleinstance)){
                        $globalinstance = $this->getContainer()->get('badiu.system.module.configinstance.data');
                        $listinstace=$globalinstance->getConfigByNameStartWith($entity,$moduleinstance,$basekey);
                }
               //print_R($listinstace);exit;
                if(is_array($listglobal) && sizeof($listglobal) > 0){
                        foreach ($listglobal as $row) {
                                $name=$this->getUtildata()->getVaueOfArray($row,'name');
                                $value=$this->getUtildata()->getVaueOfArray($row,'value');
                                $list[$name]=$value;
                        }
                   
                }

                if(is_array( $listentity) && sizeof( $listentity) > 0) {
                        foreach ( $listentity as $row) {
                                $name=$this->getUtildata()->getVaueOfArray($row,'name');
                                $value=$this->getUtildata()->getVaueOfArray($row,'value');
                                $list[$name]=$value;
                        }
                }
               
                if(!empty($moduleinstance) && is_array($listinstace) && sizeof($listinstace) > 0) {
                        foreach ($listinstace as $row) {
                                $name=$this->getUtildata()->getVaueOfArray($row,'name');
                                $value=$this->getUtildata()->getVaueOfArray($row,'value');
                                $list[$name]=$value;
                        }
                }
              //  print_r($list);exit;
                return $list;
        }   
       
	   /**
 * Retrieves an associative array of global variables and inherited global variables.
 * 
 * @return array An associative array containing both global variables and inherited global variables.
 * 
 * This method fetches the global configuration data from a service called 'badiu.system.module.configglobal.data' 
 * using the getContainer() method of the current class. It also fetches the inherited global configuration data 
 * whose names end with '.extends.bundle.key'. 
 * 
 * It then loops through the global configuration data, extracting the name and value from each element, and 
 * stores them in an associative array. This array is then passed to the addExtend() method along with the inherited 
 * global configuration data to add the inherited global variables.
 *
 * Finally, it returns the updated associative array.
 *
 * Usage:
 *
 * $globalVariables = $classInstance->getGlobal();
 *
 * foreach($globalVariables as $name => $value){
 *     echo "Name: {$name}, Value: {$value}";
 * }
 */
        public function getGlabalLevel() {
                $list=array();
              
                $globaldata = $this->getContainer()->get('badiu.system.module.configglobal.data');
                $listglobal=$globaldata->getConfig();
                $listglobalextends=$globaldata->getConfigByNameEndWith('.extends.bundle.key');
				
                                   
                if(is_array($listglobal) && sizeof($listglobal) > 0){
                        foreach ($listglobal as $row) {
                                $name=$this->getUtildata()->getVaueOfArray($row,'name');
                                $value=$this->getUtildata()->getVaueOfArray($row,'value');
                                $list[$name]=$value;
                        }
                      
                }
                $list=$this->addExtend($list,$listglobalextends);
                
                return $list;
        }  
		
		/**
 * Generates a list of entity variables based on a list of global variables and entity configuration, including override settings.
 *
 * @param array $listglobal An associative array containing global variables.
 * @param string|null $entity (optional) The entity for which the configuration is to be fetched. 
 *                            If no entity is provided, the current entity will be used.
 *
 * @return array An associative array containing the entity variables.
 *
 * This method first checks if the $entity parameter is empty. If it is, the current entity is fetched 
 * using the getEntity() method of the current class. 
 * 
 * Then, the method fetches entity-specific configuration data from the 'badiu.system.module.configentity.data' service,
 * and the service responsible for overriding configurations ('badiu.system.module.lib.overrideconfig') using the getContainer() method.
 *
 * The method then loops through the entity-specific configuration data. For each configuration, it fetches the name, 
 * value, and override settings. 
 *
 * These parameters are then used to create an object, which is passed to the override configuration service 
 * to get the final value, considering the overriding settings. 
 *
 * The final name-value pair is stored in the associative array which is then returned.
 *
 * Usage:
 *
 * $entityVariables = $classInstance->getEntity($globalVariables, $entityName);
 *
 * foreach($entityVariables as $name => $value){
 *     echo "Name: {$name}, Value: {$value}";
 * }
 */
        public function getEntityLevel($listglobal,$entity=null) {
               
				if(empty($entity)){$entity=$this->getEntity();}
                $list=array();
				
                $globalentity = $this->getContainer()->get('badiu.system.module.configentity.data');
                $listentity=$globalentity->getConfig($entity);
				 $overrideconfig= $this->getContainer()->get('badiu.system.module.lib.overrideconfig');
   
                if(is_array($listentity) && sizeof($listentity) > 0) {
                        foreach ($listentity as $row) {

                                $name=$this->getUtildata()->getVaueOfArray($row,'name');
                                $value=$this->getUtildata()->getVaueOfArray($row,'value');
                                $enableinheritance=$this->getUtildata()->getVaueOfArray($row,'enableinheritance');
                                $param=new \stdClass();
                                $param->key=$name;
                                $param->conflevel1=$this->getUtildata()->getVaueOfArray($listglobal,$name);
                                $param->conflevel2=$value;
                                $param->override=$enableinheritance;
                                $value=$overrideconfig->get($param);
                                $list[$name]=$value;

                               
                        }
                      
                }
              
                return $list;
        }  
		
	/**
 * Generates instance configuration based on global and entity configurations.
 *
 * @param array $globalconf An associative array containing global configuration variables.
 * @param array $entityconf An associative array containing entity configuration variables.
 * @param string $entity The entity for which the instance configuration is to be fetched.
 *
 * @return array An associative array containing the instance configurations.
 *
 * This method fetches instance-specific configuration data for a specific entity 
 * from the 'badiu.system.module.configinstance.data' service using the getContainer() method. 
 *
 * Then, the global and entity configurations are merged, with the entity configurations overriding the global configurations.
 *
 * The method then loops through the instance-specific configuration data. For each configuration, it fetches the name, 
 * value, and override settings. 
 *
 * These parameters are used to create an object, which is passed to the override configuration service 
 * to get the final value, considering the overriding settings.
 *
 * The final name-value pair (where the name includes the module instance name) is stored in the associative array 
 * which is then returned.
 *
 * Usage:
 *
 * $instanceConfig = $classInstance->getInstances($globalConfig, $entityConfig, $entityName);
 *
 * foreach($instanceConfig as $name => $value){
 *     echo "Name: {$name}, Value: {$value}";
 * }
 */
        public function getInstanceLevel($globalconf,$entityconf,$entity) {
				$instancentity = $this->getContainer()->get('badiu.system.module.configinstance.data');
                $listinstance=$instancentity->getConfigByEntity($entity);
                
                $list=array_replace($globalconf,$entityconf);
                $newlist=array();
                $overrideconfig= $this->getContainer()->get('badiu.system.module.lib.overrideconfig');
                if(is_array($listinstance) && sizeof($listinstance) > 0) {
                        foreach ($listinstance as $row) {

                                $name=$this->getUtildata()->getVaueOfArray($row,'name');
                                $value=$this->getUtildata()->getVaueOfArray($row,'value');
								$moduleinstance=$this->getUtildata()->getVaueOfArray($row,'moduleinstance');
                                $enableinheritance=$this->getUtildata()->getVaueOfArray($row,'enableinheritance');
                                $param=new \stdClass();
                                $param->key=$name;
                                $param->conflevel1=$this->getUtildata()->getVaueOfArray($list,$name);
                                $param->conflevel2=$value;
                                $param->override=$enableinheritance;
                                $value=$overrideconfig->get($param);
                                $keyentity=$name.'.p.'.$moduleinstance;
                                $newlist[$keyentity]=$value;
                        }
                }
        
                return $newlist;
        }

		/*
         * Get config global and entity. The entity override glogal
         *
         * @return void
         */
        public function getGlabalAndEntity($entity=null) {
                //$logcron=$this->getContainer()->get('badiu.system.log.core.lib.cron');
                $logparam=array('self'=>1);
              
				if(empty($entity)){$entity=$this->getEntity();}
                $list=array();
                //get global
                $globaldata = $this->getContainer()->get('badiu.system.module.configglobal.data');
                $listglobal=$globaldata->getConfig();
                $listglobalextends=$globaldata->getConfigByNameEndWith('.extends.bundle.key');
				
                //get entity
                $globalentity = $this->getContainer()->get('badiu.system.module.configentity.data');
                $listentity=$globalentity->getConfig($entity);
				
				// review extends for entity
				//$listentityextends=$globalentity->getConfigByNameEndWith($entity,'.extends.bundle.key');
				
				
                
                //$logcron->add("A total conf geral ".count($listglobal),$logparam);
               // $logcron->add("A total config entity $entity ".count($listentity),$logparam);
                                
                if(is_array($listglobal) && sizeof($listglobal) > 0){
                        foreach ($listglobal as $row) {
                                $name=$this->getUtildata()->getVaueOfArray($row,'name');
                                $value=$this->getUtildata()->getVaueOfArray($row,'value');
                                $list[$name]=$value;
                        }
                      
                }
                $list=$this->addExtend($list,$listglobalextends);
                $overrideconfig= $this->getContainer()->get('badiu.system.module.lib.overrideconfig');
   
                if(is_array($listentity) && sizeof($listentity) > 0) {
                        foreach ($listentity as $row) {

                                $name=$this->getUtildata()->getVaueOfArray($row,'name');
                                $value=$this->getUtildata()->getVaueOfArray($row,'value');
                                $enableinheritance=$this->getUtildata()->getVaueOfArray($row,'enableinheritance');
                                $param=new \stdClass();
                                $param->key=$name;
                                $param->conflevel1=$this->getUtildata()->getVaueOfArray($list,$name);
                                $param->conflevel2=$value;
                                $param->override=$enableinheritance;
                                $value=$overrideconfig->get($param);
                                $list[$name]=$value;

                                //log
                               /* if($name=='badiu.moodle.mreport.enrol.enrol.dbsearch.sql.count'){
                                      $ilogconfig=$param->conflevel1." --- ".$param->conflevel2 ." ---" .$enableinheritance ." ---- ".$value;
                                      $logcron->add("OVERRIDE: $ilogconfig",$logparam);
                                }*/
                                
                        }
                      
                }
              
              //  $logfinalconf=$this->getUtildata()->getVaueOfArray($list,'badiu.moodle.mreport.enrol.enrol.dbsearch.sql.count');
              //  $logcron->add("Configturação final: $logfinalconf",$logparam);
                

               // print_r($list);exit;
                return $list;
        }  
        
        
        /**
         * get instance config and override with entity and global config
         * $entitysession is used for multiple entity cron to isolate session
         * @return void
         */
        public function getInstance($modulekey,$moduleinstance) {
				if(!ctype_digit($moduleinstance)){return array();}
                $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                $badiuSession->setHashkey($this->getSessionhashkey());
                $entity=$badiuSession->get()->getEntity();
                $instancentity = $this->getContainer()->get('badiu.system.module.configinstance.data');
                $listinstance=$instancentity->getConfig($entity,$modulekey,$moduleinstance);
                
                $list=$badiuSession->get()->getConfig()->getParam();
                $newlist=array();
                $overrideconfig= $this->getContainer()->get('badiu.system.module.lib.overrideconfig');
                if(is_array($listinstance) && sizeof($listinstance) > 0) {
                        foreach ($listinstance as $row) {

                                $name=$this->getUtildata()->getVaueOfArray($row,'name');
                                $value=$this->getUtildata()->getVaueOfArray($row,'value');
                                $enableinheritance=$this->getUtildata()->getVaueOfArray($row,'enableinheritance');
                                $param=new \stdClass();
                                $param->key=$name;
                                $param->conflevel1=$this->getUtildata()->getVaueOfArray($list,$name);
                                $param->conflevel2=$value;
                                $param->override=$enableinheritance;
                                $value=$overrideconfig->get($param);
                                $keyentity=$name.'.'.$moduleinstance;
                                $newlist[$keyentity]=$value;
                        }
                }
        
                return $newlist;
        }

    
		/**
         * add extends list to base
         * @return void
         */
        public function addExtend($baselist,$extendlist) {	
			
			if(empty($extendlist)){return $baselist;}
			if(!is_array($extendlist)){return $baselist;}
			
			if(empty($baselist)){return $baselist;}
			if(!is_array($baselist)){return $baselist;}
			$newlistchild=array();
			
			foreach ($extendlist as $row) {
				$name=$this->getUtildata()->getVaueOfArray($row,'name');
                $value=$this->getUtildata()->getVaueOfArray($row,'value');
				
				$keychild=str_replace(".extends.bundle.key","",$name);
				
				foreach ($baselist as $k => $v) {
					$starwithmaterkey=strpos($k,$value);
					if($starwithmaterkey===0){
						$k=str_replace($value,$keychild,$k);
						$newlistchild[$k]=$v;
					}
				}
				
			}
			
			foreach ($newlistchild as $k => $v) {
			if (!array_key_exists($k,$baselist)){$baselist[$k]=$v;}
			}
		
			return $baselist;
		}
}
