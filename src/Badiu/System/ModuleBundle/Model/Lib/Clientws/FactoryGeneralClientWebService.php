<?php

namespace Badiu\System\ModuleBundle\Model\Lib\Clientws;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryGeneralClientWebService extends BadiuModelLib {
	private $param = null;
    function __construct(Container $container) {
        parent::__construct($container);
    } 
	/*public function teste() {
		
		$this->init(1);
		
		$mconfig=array();
		$mconfig['recipient']['to']='dpr001@gmail.com';
		$mconfig['message']['subject']='Teste MReport';
		$mconfig['message']['body']='Alou só alegria. MReport teste e, <b> 07/011/2022</b>';
		$resut=$this->sigleRequest($mconfig);
		echo $resut;exit;
	}*/
	public function init($clientwsid) {
		if(empty($clientwsid)){return null;}		
		$wsclintdata=$this->getContainer()->get('badiu.system.module.clientwsschedulerdelivery.data');
		$this->param=$wsclintdata->getGlobalColumnsValue('o.id,o.name,o.value,o.param',array('id'=>$clientwsid));
		
		
	}
	public function sigleRequest($mconfig) { 
	
		if(empty($mconfig)){return null;}
		if(!is_array($mconfig)){return null;}
		if(!is_array($this->param)){return null;}
		
		//single message send
		
		 $emailto=$this->getUtildata()->getVaueOfArray($mconfig,'recipient.to',true);
		 $subject=$this->getUtildata()->getVaueOfArray($mconfig,'message.subject',true);
         $message=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
		
		 $message=$this->htmlToPlainText($message);
		 $subject=$this->htmlToPlainText($subject);
		 //$message=json_encode($message);
		// $subject=json_encode($subject); 
		 $host=$reqparam=$this->getUtildata()->getVaueOfArray($this->param,'value');
		 $reqparam=$this->getUtildata()->getVaueOfArray($this->param,'param');
		 $reqparam=str_replace("__BADIU_ADDRESS_EMAIL",$emailto,$reqparam);
		 $reqparam=str_replace("__BADIU_MESSAGE_TITLE",$subject,$reqparam);
		 $reqparam=str_replace("__BADIU_MESSAGE_CONTENT",$message,$reqparam);
		
		$result=$this->request($host,$reqparam);
		$result = $this->getJson()->decode($result, true);
		// review to dynamic config
		$message=$this->getUtildata()->getVaueOfArray($result,'message');
		if($message=='Notify send with success'){return true;}
		return false; 
	}
	
	function request($host,$param) {
       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
		$cip=curl_getinfo($ch,CURLINFO_PRIMARY_IP);
		
        curl_close($ch);
      
      return $result;
    }
	
	function html2text($html) {
		$html=strip_tags($html);
		$html = addslashes($html);
		return $html; 
		$dom = new \DOMDocument();
		$html=strip_tags($html); 
		$dom->loadHTML("<body>" . $html. "</body>");
		$xpath = new \DOMXPath($dom);
		$node = $xpath->query('body')->item(0);
		return $node->textContent; // text
	}
	
	/**
 * Convert HTML to plain text while preserving line breaks and special characters.
 * 
 * @param string $html The HTML string.
 * 
 * @return string The converted plain text.
 */
function htmlToPlainText($html) {
    // Replace <br> tags with a newline
    $text = str_ireplace(['<p>', '</p>'], ["\n", "\n"], $html);
    $text = str_ireplace(['<br>', '<br/>', '<br />'], "\n", $text);
    
    // Decode HTML entities
    $text = html_entity_decode($text, ENT_QUOTES | ENT_HTML5, 'UTF-8');

    // Remove other HTML tags
    $text = strip_tags($text); 

    // Remove excessive white spaces and replace multiple consecutive newlines with a single newline
    $text = trim(preg_replace("/[\r\n]+/", "\n", $text));
	
	$text = $this->getJson()->escape($text);
    return $text;
}
    function getParam() {
        return $this->param;
    }

  
    function setParam($param) {
        $this->param = $param;
    }
}
