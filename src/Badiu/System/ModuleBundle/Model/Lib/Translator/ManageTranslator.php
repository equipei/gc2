<?php

namespace Badiu\System\ModuleBundle\Model\Lib\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;

class ManageTranslator extends BadiuModelLib{

    function __construct(Container $container) {
            parent::__construct($container);
              }
    
        
	public function importall() {
            $bundles = $this->getContainer()->getParameter('kernel.bundles');
			$kernel = $this->getContainer()->get('kernel');
			$translatorglogaldata=$this->getContainer()->get('badiu.system.module.translatorglobal.data');
			$listtfiles=array();
			foreach ($bundles as $key => $value) {
				if (strpos( $key, 'Badiu') === 0) {
					$dirtranlations=$kernel->locateResource('@'.$key).'Resources/translations';
					$tfiles=null; 
					
					if (is_dir($dirtranlations)) {
						$tfiles = scandir($dirtranlations);
						$tfiles = array_diff($tfiles, array('.', '..'));
					}
					if(is_array($tfiles)){
						foreach ($tfiles as $titem) {
							if (strtolower(pathinfo($titem, PATHINFO_EXTENSION)) == 'yml') {
								$ffullpath=$dirtranlations.'/'.$titem;
								array_push($listtfiles,$ffullpath);	
							} 
						}
					}
					
				}
			}
			$continsert=0;
			$contupdate=0;
			foreach ($listtfiles as $fvalue) {
				$tfname = basename($fvalue);
				$itemfile=explode('.',$tfname);
				$locale=$this->getUtildata()->getVaueOfArray($itemfile,1);
				$param= Yaml::parse($fvalue);
				
				foreach ($param as $mkey => $tvalue) {
					$gparamcheckexist=array('locale'=>$locale,'mkey'=>$mkey);
					$messageidx=$this->getMessageidx($tvalue);
					$gparamadd=array('locale'=>$locale,'mstatus'=>'imported','mkey'=>$mkey,'message'=>$tvalue,'messageidx'=>$messageidx,'deleted'=>0,'timecreated'=>new \DateTime(),'timemodified'=>new \DateTime());
					$gparamedit=array('locale'=>$locale,'mstatus'=>'imported','mkey'=>$mkey,'message'=>$tvalue,'messageidx'=>$messageidx,'timemodified'=>new \DateTime());
					$dresult=$translatorglogaldata->addNativeSql($gparamcheckexist,$gparamadd,$gparamedit);
					$pid=$this->getUtildata()->getVaueOfArray($dresult,'id');
					$pexist=$this->getUtildata()->getVaueOfArray($dresult,'exist');
					if(!empty($pid)){
						if($pexist){$contupdate++;}
						else {$continsert++;}
					}
				}
				
			}
			$result=$this->getTranslator()->trans('badiu.system.core.command.importalllanguaguetodb',array('%insert%'=>$continsert,'%update%'=>$contupdate));
			
			return $result;
			
        }   
    
	public function makeCache($param) {
		$translatorglogaldata=$this->getContainer()->get('badiu.system.module.translatorglobal.data');
		$translatorinstancedata=$this->getContainer()->get('badiu.system.module.translatorinstance.data');
		$level=$this->getUtildata()->getVaueOfArray($param,'level');
		$replace=$this->getUtildata()->getVaueOfArray($param,'replace');
		$usermanageentity=1;
		
		if($this->getContainer()->hasParameter('badiu.system.access.usermanageentity')){
			$usermanageentity = $this->getContainer()->getParameter('badiu.system.access.usermanageentity');
		}
       
	 
		$cache=$this->getContainer()->get('badiu.system.core.lib.util.managecache');
		$cont=0;
		//global
		if($level=='global'){
			$listpg=$this->getPackages();
			foreach ($listpg as $pgitem) {
				$locale=$this->getUtildata()->getVaueOfArray($pgitem,'locale');
				$fparam=array('locale'=>$locale);
				$listmsg=$translatorglogaldata->getGlobalColumnsValues("o.mkey,o.message",$fparam);
				$itemlist=array();
				foreach ($listmsg as $li) {
					$mkey=$this->getUtildata()->getVaueOfArray($li,'mkey');
					$message=$this->getUtildata()->getVaueOfArray($li,'message');
					$itemlist[$mkey]=$message;
					$cont++;
				}
				
				//for itens instance of syncfreport
				$fparam=array('locale'=>$locale,'modulekey'=>'badiu.sync.freport.freport','entity'=>$usermanageentity);
				$listmsg=$translatorinstancedata->getGlobalColumnsValues("o.mkey,o.message,o.moduleinstance",$fparam);
				foreach ($listmsg as $li) {
					$mkey=$this->getUtildata()->getVaueOfArray($li,'mkey');
					$moduleinstance=$this->getUtildata()->getVaueOfArray($li,'moduleinstance');
					$mkey='badiu.sync.freport.freport.'.$moduleinstance.'.'.$mkey;
					$message=$this->getUtildata()->getVaueOfArray($li,'message');
					if(!empty($message)){$itemlist[$mkey]=$message;$cont++;}
				}
				
				//for itens instance of syncfreport item
				$fparam=array('locale'=>$locale,'modulekey'=>'badiu.sync.freport.item','entity'=>$usermanageentity);
				$listmsg=$translatorinstancedata->getGlobalColumnsValues("o.mkey,o.message,o.moduleinstance",$fparam);
				foreach ($listmsg as $li) {
					$mkey=$this->getUtildata()->getVaueOfArray($li,'mkey');
					$moduleinstance=$this->getUtildata()->getVaueOfArray($li,'moduleinstance');
					$mkey='badiu.sync.freport.item.'.$moduleinstance.'.name';
					$message=$this->getUtildata()->getVaueOfArray($li,'message');
					if(!empty($message)){$itemlist[$mkey]=$message;$cont++;}
				}
				$param=array('type'=>'language','locale'=>$locale,'entity'=>0,'replace'=>$replace,'items'=>$itemlist);
				$rcache=$cache->loadUpdate($param);
			}
			
		}
		//entity
		else if($level=='entity'){
			$listpg=$this->getPackages();
			$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			$translatorentitydata=$this->getContainer()->get('badiu.system.module.translatorentity.data');
			foreach ($listpg as $pgitem) {
				$locale=$this->getUtildata()->getVaueOfArray($pgitem,'locale');
				$fparam=array('locale'=>$locale,'entity'=>$entity);
				$listmsg=$translatorentitydata->getGlobalColumnsValues("o.mkey,o.message",$fparam);
				$itemlist=array();
				foreach ($listmsg as $li) {
					$mkey=$this->getUtildata()->getVaueOfArray($li,'mkey');
					$message=$this->getUtildata()->getVaueOfArray($li,'message');
					$itemlist[$mkey]=$message;
					$cont++;
				}
				//for itens instance of syncfreport
				$fparam=array('locale'=>$locale,'modulekey'=>'badiu.sync.freport.freport','entity'=>$entity);
				$listmsg=$translatorinstancedata->getGlobalColumnsValues("o.mkey,o.message,o.moduleinstance",$fparam);
				foreach ($listmsg as $li) {
					$mkey=$this->getUtildata()->getVaueOfArray($li,'mkey');
					$moduleinstance=$this->getUtildata()->getVaueOfArray($li,'moduleinstance');
					$mkey='badiu.sync.freport.freport.'.$moduleinstance.'.'.$mkey;
					$message=$this->getUtildata()->getVaueOfArray($li,'message');
					if(!empty($message)){$itemlist[$mkey]=$message;$cont++;}
				}
				
				//for itens instance of syncfreport item
				$fparam=array('locale'=>$locale,'modulekey'=>'badiu.sync.freport.freport','entity'=>$entity);
				$listmsg=$translatorinstancedata->getGlobalColumnsValues("o.mkey,o.message,o.moduleinstance",$fparam);
				foreach ($listmsg as $li) {
					$mkey=$this->getUtildata()->getVaueOfArray($li,'mkey');
					$moduleinstance=$this->getUtildata()->getVaueOfArray($li,'moduleinstance');
					$mkey='badiu.sync.freport.item.'.$moduleinstance.'.name';
					$message=$this->getUtildata()->getVaueOfArray($li,'message');
					if(!empty($message)){$itemlist[$mkey]=$message;$cont++;}
				}
				$param=array('type'=>'language','locale'=>$locale,'entity'=>$entity,'replace'=>$replace,'items'=>$itemlist);
				$rcache=$cache->loadUpdate($param);
			}
			
		}
		//instance
		
		$result=$this->getTranslator()->trans('badiu.system.core.command.makecahce',array('%amount%'=>$cont));
		return $result;
	}	
	
	public function getPackages() {
		$translatorglogaldata=$this->getContainer()->get('badiu.system.module.translatorglobal.data');
		$param=array();
		
		$list=$translatorglogaldata->getGlobalColumnsValues("DISTINCT o.locale",$param);
		
		return 	$list;	
	}
	 
	public function copyPackage($param) {
		$sourcelevel=$this->getUtildata()->getVaueOfArray($param,'sourcelevel');
		$sourcepackage=$this->getUtildata()->getVaueOfArray($param,'sourcepackage');
		$sourcekey=$this->getUtildata()->getVaueOfArray($param,'sourcekey');
		$sourcemessageidx=$this->getUtildata()->getVaueOfArray($param,'sourcemessageidx');
		
		$sourcemodulekey=$this->getUtildata()->getVaueOfArray($param,'sourcemodulekey');
		$sourcemoduleinstance=$this->getUtildata()->getVaueOfArray($param,'sourcemoduleinstance');
		$sourceinstancemaster=$this->getUtildata()->getVaueOfArray($param,'sourceinstancemaster');
		
		$targetlevel=$this->getUtildata()->getVaueOfArray($param,'targetlevel');
		$targetpackage=$this->getUtildata()->getVaueOfArray($param,'targetpackage');
		$targetoperation=$this->getUtildata()->getVaueOfArray($param,'targetoperation');
		$targetstatus=$this->getUtildata()->getVaueOfArray($param,'targetstatus');
		$locale=$targetpackage;
		
		$session=$this->getContainer()->get('badiu.system.access.session');
		$entity=$session->get()->getEntity();
		$usermanageentity=1;
		if($this->getContainer()->hasParameter('badiu.system.access.usermanageentity')){
			$usermanageentity = $this->getContainer()->getParameter('badiu.system.access.usermanageentity');
		}
		$sdatakey="";
		if($sourcelevel=='system'){$sdatakey="badiu.system.module.translatorglobal.data";}
		else if($sourcelevel=='entity'){$sdatakey="badiu.system.module.translatorentity.data";}
		else if($sourcelevel=='instance'){$sdatakey="badiu.system.module.translatorinstance.data";}
	
		if(empty($sdatakey) || !$this->getContainer()->has($sdatakey)){return null;}
		$sdata=$this->getContainer()->get($sdatakey);
		$fparam=array('locale'=>$sourcepackage,'mkey'=>$sourcekey,'textfind'=>$sourcemessageidx,'textcasesensitive'=>0);
		
		if($sourcelevel=='entity'){$fparam['entity']=$entity;}
		if($sourcelevel=='instance'){
			$fparam['entity']=$entity;
			if($sourceinstancemaster){$fparam['entity']=$usermanageentity;}
			if($sourcemodulekey){$fparam['modulekey']=$sourcemodulekey;}
			if($sourcemoduleinstance){$fparam['moduleinstance']=$sourcemoduleinstance;}
		
		}
		$liststrings=$sdata->getList($fparam);
		
		$cont=0;
		if(!is_array($liststrings)){return $cont;}
		
		$tdatakey="";
		if($targetlevel=='system'){$tdatakey="badiu.system.module.translatorglobal.data";}
		else if($targetlevel=='entity'){$tdatakey="badiu.system.module.translatorentity.data";}
		else if($targetlevel=='instance'){$tdatakey="badiu.system.module.translatorinstance.data";}
		
		if(empty($tdatakey) || !$this->getContainer()->has($tdatakey)){return null;}
		$tdata=$this->getContainer()->get($tdatakey);
		
		
		
		$contupdate=0;
		$continsert=0;
		foreach ($liststrings as $pgitem) {
			$mkey=$this->getUtildata()->getVaueOfArray($pgitem,'mkey');
			$message=$this->getUtildata()->getVaueOfArray($pgitem,'message');
			$messageidx=$this->getUtildata()->getVaueOfArray($pgitem,'message');
			$messageidx=$this->getMessageidx($messageidx);
			
			$modulekey=$this->getUtildata()->getVaueOfArray($pgitem,'modulekey');
			$moduleinstance=$this->getUtildata()->getVaueOfArray($pgitem,'moduleinstance');
			
			$gparamcheckexist=array('locale'=>$locale,'mkey'=>$mkey);
			$gparamadd=array('locale'=>$locale,'mstatus'=>$targetstatus,'mkey'=>$mkey,'message'=>$message,'messageidx'=>$messageidx,'deleted'=>0,'timecreated'=>new \DateTime(),'timemodified'=>new \DateTime());
			$gparamedit=array('locale'=>$locale,'mstatus'=>$targetstatus,'mkey'=>$mkey,'message'=>$message,'messageidx'=>$messageidx,'timemodified'=>new \DateTime());
			
			if($targetlevel=='entity'){
				$gparamcheckexist['entity']=$entity;
				$gparamadd['entity']=$entity;
				$gparamedit['entity']=$entity;
			}
			if($targetlevel=='instance'){
				$gparamcheckexist['entity']=$entity;
				$gparamcheckexist['modulekey']=$modulekey;
				$gparamcheckexist['moduleinstance']=$moduleinstance;
				
				$gparamadd['entity']=$entity;
				$gparamadd['modulekey']=$modulekey;
				$gparamadd['moduleinstance']=$moduleinstance;
				
				$gparamedit['entity']=$entity;
				$gparamedit['modulekey']=$modulekey;
				$gparamedit['moduleinstance']=$moduleinstance;
				
			}
			
			if($targetoperation=='insert'){$gparamedit=null;}
			else if($targetoperation=='update'){$gparamadd=null;}
		
			$dresult=$tdata->addNativeSql($gparamcheckexist,$gparamadd,$gparamedit);
			$pid=$this->getUtildata()->getVaueOfArray($dresult,'id');
			$pexist=$this->getUtildata()->getVaueOfArray($dresult,'exist');
			
			if(!empty($pid)){
				if($pexist){$contupdate++;}
				else {$continsert++;}
			}
		}
		$result=array('insert'=>$continsert,'update'=>$contupdate);
		return 	$result;	
	}
	/**
 * Truncates the input message to a maximum length of characters, removing HTML tags and extra spaces.
 *
 * This function accepts a string ($message) as input and an optional maximum length ($maxLength) and performs
 * the following actions:
 * 1. Removes HTML tags using the strip_tags function.
 * 2. Replaces multiple consecutive spaces with a single space.
 * 3. Truncates the resulting string to the specified maximum length of characters (default is 252).
 *
 * @param string $message The input message to be processed.
 * @param int $maxLength (Optional) The maximum length of the resulting message. Default is 252.
 * @return string The processed and truncated message.
 */
	public function getMessageidx($message,$maxLength=252) {
			$messageidx=strip_tags($message);
			$messageidx = str_replace('  ', ' ', $messageidx);
			if (mb_strlen($messageidx) > $maxLength) {
				 $messageidx =mb_substr($messageidx, 0, $maxLength);
			}
			return $messageidx;
	}
	

public function replaceText($param) {
		$level=$this->getUtildata()->getVaueOfArray($param,'level');
		$package=$this->getUtildata()->getVaueOfArray($param,'package');
		$mkey=$this->getUtildata()->getVaueOfArray($param,'mkey');
		$entity=$this->getEntity();
		$textfind=$this->getUtildata()->getVaueOfArray($param,'textfind');
		$textreplace=$this->getUtildata()->getVaueOfArray($param,'textreplace');
		$textcasesensitive=$this->getUtildata()->getVaueOfArray($param,'textcasesensitive');
		
		
		$datakey="";
		if($level=='system'){$datakey="badiu.system.module.translatorglobal.data";}
		else if($level=='entity'){$datakey="badiu.system.module.translatorentity.data";}
		else if($level=='instance'){$datakey="badiu.system.module.translatorinstance.data";}
		
		if(empty($datakey) || !$this->getContainer()->has($datakey)){return null;}
		$ldata=$this->getContainer()->get($datakey);
		$fparam=array('locale'=>$package,'mkey'=>$mkey,'textfind'=>$textfind,'textcasesensitive'=>$textcasesensitive);
		if($level=='entity'){$fparam['entity']=$entity;}
		else if($level=='instance'){
			$fparam['entity']=$entity;
		}
		$listtxt=$ldata->getListForReplace($fparam);
		if(!is_array($listtxt)){return null;}
		$cont=0;
		
		foreach ($listtxt as $itext) {
			$message=$this->getUtildata()->getVaueOfArray($itext,'message');
			$id=$this->getUtildata()->getVaueOfArray($itext,'id');
			$message = str_replace("  ", " ",$message);
			if($textcasesensitive){
				$message = str_replace($textfind, $textreplace,$message);
				$cont++;
			}else {
				$message = str_ireplace($textfind, $textreplace, $message);
				$cont++;
			}
		
			$messageidx=$this->getMessageidx($message);
			
			$uparam=array('id'=>$id,'message'=>$message,'messageidx'=>$messageidx,'timemodified'=>new \DateTime());
			$result=$ldata->updateNativeSql($uparam,false);
			
		}
	return $cont;
}
	 
	public function makeTranslation($param) {
		$level=$this->getUtildata()->getVaueOfArray($param,'level');
		$package=$this->getUtildata()->getVaueOfArray($param,'package');
		$mkey=$this->getUtildata()->getVaueOfArray($param,'mkey');
		$status=$this->getUtildata()->getVaueOfArray($param,'status');
		$changestatus=$this->getUtildata()->getVaueOfArray($param,'statuschange');
		$textfind=$this->getUtildata()->getVaueOfArray($param,'textfind');
		
		$modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		$moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		$maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		$instruction=$this->getUtildata()->getVaueOfArray($param,'instruction');
		$tdatakey="";
		if($level=='system'){$tdatakey="badiu.system.module.translatorglobal.data";}
		else if($level=='entity'){$tdatakey="badiu.system.module.translatorentity.data";}
		else if($level=='instance'){$tdatakey="badiu.system.module.translatorinstance.data";}
		
		if(empty($tdatakey) || !$this->getContainer()->has($tdatakey)){return null;}
		$tdata=$this->getContainer()->get($tdatakey);
		
		$fparam=array('locale'=>$package,'maxrecord'=>$maxrecord,'mstatus'=>$status,'messagenotnull'=>1);
		if(!empty($mkey)){$fparam['mkey']=$mkey;}
		if(!empty($textfind)){$fparam['textfind']=$textfind;}
		
		if($level=='entity'){$fparam['entity']=$entity;}
		if($level=='instance'){
			$fparam['entity']=$entity;
			if(!empty($modulekey)){$fparam['modulekey']=$modulekey;}
			if(!empty($moduleinstance)){$fparam['moduleinstance']=$moduleinstance;}
		}
		$liststrings=$tdata->getList($fparam);
		$sresult = array('datashouldexec' => sizeof($liststrings), 'dataexec' => 0);
		$listmessage=array();
		if(!is_array($liststrings)){return null;}
		
		foreach ($liststrings as $row) {
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			$message=$this->getUtildata()->getVaueOfArray($row,'message');
			$key="m".$id;
			$listmessage[$key]=$message;
		}
		$msgyaml = Yaml::dump($listmessage);
		
		$requestservice=$this->getContainer()->get('badiu.admin.openai.lib.requestservice');
		$instruction="$instruction \n $msgyaml";
		$rparam=array('message'=>$instruction,'sessionid'=>time(),);
		$tresp=$requestservice->exec($rparam);
		$tresptranlator=$this->getUtildata()->getVaueOfArray($tresp,'service');
		$tresptranlator= Yaml::parse($tresptranlator);
		$cont=0;
		if(is_array($tresptranlator)){
			foreach ($tresptranlator as $k=> $v) {
				$k = (int) preg_replace('/\D/', '', $k);
				$messageidx=$this->getMessageidx($v);
				$uparam=array('id'=>$k,'message'=>$v,'messageidx'=>$messageidx,'mstatus'=>$changestatus,'timemodified'=>new \DateTime());
				$result=$tdata->updateNativeSql($uparam,false);
				if($result){$cont++;}
			}
		}else{return $tresp;}
		$sresult['dataexec']=$cont;
		return $sresult;
		 
	}
	
	public function makeTranslationTeste(){
		$param=array('level'=>'instance','package'=>'en','maxrecord'=>6,'status'=>'imported','entity'=>$this->getEntity(),'statuschange'=>'inreview','instruction'=>'Traduza para inglês o texto a seguir no formato yml. Retorne a resposta da tradução em no mesmo formato. A respota só deve ser em formato yml');
		$this->makeTranslation($param);
	}
}
