<?php
namespace Badiu\System\ModuleBundle\Model\Lib\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;


class TranslatorTask  extends  TaskGeneralExec{
  
     
	private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
                $this->cresult=array('datashouldexec' => 0, 'dataexec' => 0);
  } 

  public function import() {
	$this->makeTranslator();
    return  $this->cresult;
} 

public function update() {
    $this->makeTranslator();
	return  $this->cresult;
    }

public function makeTranslator() {
    $operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
	if($operation!='maketranslator'){return null;}
	$tmanager=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
	$param=$this->getParam();
	$param['entity']=$this->getTaskdto()->getEntity();
	$result=$tmanager->makeTranslation($param);
	
	$this->cresult = $this->addResult($this->cresult, $result); 
	
	return $this->cresult;
	
}

}
?>