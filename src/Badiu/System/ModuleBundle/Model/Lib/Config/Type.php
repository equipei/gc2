<?php

namespace Badiu\System\ModuleBundle\Model\Lib\Config;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Type extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function isParamConfg($key) {
	//echo "is param invocado $key";
			$result=false;
			//check if exist .param.config. on end of string
			 $pos = strrpos($key, '.param.config.');
			 if($pos){
				$keypc=$this->removeLastItemOfKey($key);
				//check key end wiht .param.config
				 $len = strlen('.param.config');
				 $onlyend = substr($keypc, strlen($keypc) - $len);
				if($onlyend=='.param.config') {$result=true;}
   
			 }
			// echo "result: $result";exit;
			 return $result;
	}
	
	 public function removeLastItemOfKey($key) {
           $pos = strrpos($key, '.');
           $mkey = substr($key,0, $pos);
           return  $mkey;
      }
	

}
