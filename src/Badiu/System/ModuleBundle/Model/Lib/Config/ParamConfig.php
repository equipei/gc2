<?php

namespace Badiu\System\ModuleBundle\Model\Lib\Config;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ParamConfig extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function getValue($param) {
			 $value='';
			 $querystring=$this->container->get('badiu.system.core.lib.http.querystring');
             $querystring->setQuery($param);
             $querystring->makeParam();
			 $lparam=$querystring->getParam();
			 if(array_key_exists("default",$lparam)){
				$value=$lparam["default"];
			 }
			 return $value;
	}
	

}
