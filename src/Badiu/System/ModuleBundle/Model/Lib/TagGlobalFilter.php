<?php

namespace Badiu\System\ModuleBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class TagGlobalFilter extends BadiuFormFilter{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
        
   public function execAfterSubmit() {
        
         $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
		 $name = $this->getUtildata()->getVaueOfArray($this->getParam(), 'name');
         $modulekey=$this->getModulekey();
		 $entity=$this->getEntity();
		 if(!empty($id ) && !empty($name )&& !empty($modulekey )){
				$tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
				$tagmanager=$this->getContainer()->get('badiu.system.module.tag.lib.manager');
				$reference=$tagmanager->generateUniqueKey($name);
				$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'dtype'=>'name','locale'=>$this->getLang());
				$paramadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','reference'=>$reference,'locale'=>$this->getLang(),'deleted'=>0,'timecreated'=>new \DateTime());
				$paramedit=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','reference'=>$reference,'locale'=>$this->getLang(),'timemodified'=>new \DateTime());
				$tagdata->addNativeSql($paramcheckexist,$paramadd,$paramedit);
			}
	  }
       
}
