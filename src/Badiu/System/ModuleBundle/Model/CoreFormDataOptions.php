<?php

namespace Badiu\System\ModuleBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;

class CoreFormDataOptions extends BadiuFormDataOptions{
     
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       } 
              
    
    public  function getLevel(){
        $list=array();
        $list['system']=$this->getTranslator()->trans('badiu.system.module.core.level.system');
        $list['entity']=$this->getTranslator()->trans('badiu.system.module.core.level.entity');
        $list['instance']=$this->getTranslator()->trans('badiu.system.module.core.level.instance');
	return $list;
    } 
	public  function getLevelNoUserRoot(){
        $list=array();
        if($isrootuser){$list['system']=$this->getTranslator()->trans('badiu.system.module.core.level.system');}
        $list['entity']=$this->getTranslator()->trans('badiu.system.module.core.level.entity');
        $list['instance']=$this->getTranslator()->trans('badiu.system.module.core.level.instance');
	return $list;
    } 
	public  function getLevel2(){
        $list=array();
        $list['system']=$this->getTranslator()->trans('badiu.system.module.core.level.system');
        $list['entity']=$this->getTranslator()->trans('badiu.system.module.core.level.entity');
		 $list['instance']=$this->getTranslator()->trans('badiu.system.module.core.level.instance');
		return $list;
    } 
	public  function getLevel2NoUserRoot(){
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
		$isrootuser=$badiuSession->isRootUser();
        $list=array();
       if($isrootuser){$list['system']=$this->getTranslator()->trans('badiu.system.module.core.level.system');}
        $list['entity']=$this->getTranslator()->trans('badiu.system.module.core.level.entity');
		$list['instance']=$this->getTranslator()->trans('badiu.system.module.core.level.instance');
		return $list;
    } 
}
