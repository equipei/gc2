<?php

namespace Badiu\System\ModuleBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ConfigInstanceFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
    }
    
     
    public function changeParam() {

        $name = $this->getParamItem('name');
        $modulekey=$this->getParamItem('modulekey');
        $bundlekey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($modulekey);
        $param = $this->getParam();
        $param['bundlekey'] = $bundlekey;
        $this->setParam($param);
        
     }
     
         
    
}
