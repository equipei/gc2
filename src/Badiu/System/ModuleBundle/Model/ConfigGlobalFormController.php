<?php

namespace Badiu\System\ModuleBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ConfigGlobalFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
    }
    
     
    public function changeParam() {
		//reveiw this permissions
		$session=$this->getContainer()->get('badiu.system.access.session');
		$isrootuser=$session->isRootUser();
		if(!$isrootuser){echo "without superuser permission";exit;}
        $this->setDbdefaultablevalue(false);
        $this->removeParamItem('entity');
       
     }
     
     public function validation() {
         
        //check exist without entity to add

        //check exist without entity to edit
        return null; 
     }
    
}
