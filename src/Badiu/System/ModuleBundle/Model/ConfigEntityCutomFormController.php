<?php

namespace Badiu\System\ModuleBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ConfigEntityCutomFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
    }
    
    public function changeParamOnOpen() {
		$param=$this->getDefaultValue();
	   $this->setParam($param);
	   parent::changeParamOnOpen();
	  
    }
   public function changeParam() {
	   $this->changeParamName();
	 
	}
     
   public function getDefaultValue() {
	  // print_r($this->getFormFieldList());exit;
	   if(!is_array($this->getFormFieldList())){return null;}
	     $newparam=array();
		 $badiuSession = $this->getContainer()->get('badiu.system.access.session');
		foreach ($this->getFormFieldList() as $key => $value) {
			$sesseionkey=str_replace("_", ".", $key);
			$nvalue=$badiuSession->getValue($sesseionkey);
			$newparam[$key]=$nvalue;
		}
		return $newparam;
   }
   
	public function changeParamName() {
	   if(!is_array($this->getParam())){return null;}
	     $newparam=array();
		 
		foreach ($this->getParam() as $key => $value) {
			
			$change=true;
			if($key=='_service' || $key=='_key'){$change=false;}
			if($change){$sesseionkey=str_replace("_", ".", $key);}
			else {$sesseionkey=$key;}
			$newparam[$sesseionkey]=$value;
		}
		$this->setParam($newparam);
		
   }  

	public function updateParam() {
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		$cont=0;
		//$configglobaldata=$this->getContainer()->get('badiu.system.module.configglobal.data');
		$configentitydata=$this->getContainer()->get('badiu.system.module.configentity.data');
		$entity=$this->getEntity();
		
		
		$itemchange=array();
		foreach ($this->getParam() as $key => $value) {
			$currentvalue=$badiuSession->getValue($key);
			if($currentvalue!=$value){
				
				$bundlekey=$this->getBundlekey($key);
				$paramcheckexist=array('entity'=>$entity,'name'=>$key);
				$paramadd=array('entity'=>$entity,'name'=>$key,'bundlekey'=>$bundlekey,'value'=>$value,'timecreated'=>new \DateTime(),'deleted'=>0);
				$paramedit=array('entity'=>$entity,'name'=>$key,'bundlekey'=>$bundlekey,'value'=>$value,'timemodified'=>new \DateTime());
				
				$cresult=$configentitydata->addNativeSql($paramcheckexist,$paramadd,$paramedit);
				$rid=$this->getUtildata()->getVaueOfArray($cresult,'id');
				if($rid){$cont++;}
				$badiuSession->addValue($key,$value);
				
				if($key!='id'){
					$itemchange[$key]=$value;
				}
				
			}
		}
		if(sizeof($itemchange)>0){
			$param=array('type'=>'cache','entity'=>$entity,'items'=>$this->getParam(),'onlycheckexist'=>0,'forceread'=>1);
			$cache=$this->getContainer()->get('badiu.system.core.lib.util.managecache');
			$cache->loadUpdate($param);
		}
		
		$result=array('countparam'=>sizeof($this->getParam()),'countparamupdated'=>$cont);
		$this->setResultexec($result);
		return $result;
	}

public function getBundlekey($key) {
	$pos = strpos($key, ".param.config.");
	$bundlekey=null;
	if($pos){
		$bundlekey=substr($key,0,$pos);
	}
	return $bundlekey;
	
}
public function editExec() {
		$this->updateParam();
		
	  }

	public function execResponse() {
		$countparam=$this->getUtildata()->getVaueOfArray($this->getResultexec(),'countparam');
		$countparamupdated=$this->getUtildata()->getVaueOfArray($this->getResultexec(),'countparamupdated');
		$this->setSuccessmessage($this->getTranslator()->trans('badiu.system.module.config.updateparamresult',array('%countparam%'=>$countparam,'%countparamupdated%'=>$countparamupdated)));
		$outrsult=array('result'=>$this->getResultexec(),'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
		$this->getResponse()->setStatus("accept");
		$this->getResponse()->setMessage($outrsult);
		return $this->getResponse()->get();
	}	
}
