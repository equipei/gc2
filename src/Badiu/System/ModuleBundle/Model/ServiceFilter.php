<?php

namespace Badiu\System\ModuleBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class ServiceFilter extends BadiuFormFilter{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
       
   public function execAfterSubmit() {
         $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
         $sdata=$this->getContainer()->get('badiu.system.module.service.data');
		 $token=$sdata->getGlobalColumnValue('stoken', array('id'=>$id));
		 if(!empty($token)){return null;}
		 
		 $entity=$this->getEntity();
		 $hash=$this->getContainer()->get('badiu.system.core.lib.util.hash');
		 $token=$hash->make(120);
		 $stoken="$entity|$token|$id";
		 $ssparam=array('id'=>$id,'stoken'=>$stoken);
		 $sdata->updateNativeSql($ssparam,false);
       
         
   } 
       
    
}
