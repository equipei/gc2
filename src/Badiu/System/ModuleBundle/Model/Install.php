<?php

namespace Badiu\System\ModuleBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Yaml\Yaml;
//http://54.156.96.187/badiunet/web/app_dev.php/system/service/process?_service=badiu.system.module.install&_function=setupentity
//http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_service=badiu.system.module.module.install&_function=setupentity
//http://fabrica.badiu.com.br/~colaborador7/badiunet/web/app_dev.php/system/service/process?_service=badiu.system.module.module.install&operation=update
//http://fabrica.badiu.com.br/~colaborador7/badiunet/web/app_dev.php/system/service/process?_service=badiu.system.module.module.install&operation=update&bundle=BadiuSystemCoreBundle
class Install extends BadiuModelLib {



    function __construct(Container $container) {
         parent::__construct($container);
    }

    public function exec() {
        $cont=$this->initTagLangDefaultValue();
		$cont+=$this->initTagUkeyDefaultValue();
		return $cont;
    }
    
    public function setupentity($entity=null,$listkeyinstall=null) {
        
        //list of modules
         $data = $this->getContainer()->get('badiu.system.module.module.data');
         $list=$data->getGlobalColumnValues('bundlekey',array());
         $cont=0;
       
         foreach ($list as $row) {
             $bundlekey = $this->getUtildata()->getVaueOfArray($row, 'bundlekey');
             $minstall=$bundlekey.".install";
			 $processinstall=true;
			 if(is_array($listkeyinstall) && sizeof($listkeyinstall) > 0 ){
				 $processinstall=$this->startsWith($listkeyinstall, $minstall);
			}
             if($this->getContainer()->has($minstall) && $processinstall){
                 $datainstall=$this->getContainer()->get($minstall);
                 if(!empty($entity)){$datainstall->setForceentity(true);$datainstall->setEntity($entity);}
                 if(method_exists($datainstall ,'exec')){
                      $cont+=$datainstall->exec();
					  
                 }
				
             }
         }
         return $cont;
    }
	
	/**
 * Checks if any string from an array of strings starts with a given key string.
 *
 * @param array  $arr        The array of strings to be checked.
 * @param string $keyString  The key string to be searched for at the start of each string in the $arr.
 *
 * @return bool  Returns true if any string in the $arr starts with the $keyString, false otherwise.
 *
 * @example
 * $myArray = ['badiu.system', 'badiu.admin.util', 'badiu.moodle.mreport'];
 * $keyStr = 'badiu.system.access.role';
 * $result = startsWith($myArray, $keyStr);
 * echo $result ? "True" : "False";  // Outputs: True
 *
 * @note The function uses the strpos function to determine the position of the substring.
 * It checks if the strpos of $keyString and an item from $arr is 0, meaning the item is found 
 * at the start of the $keyString. It also prints each array item and the $keyString 
 * separated by a '|' for visual confirmation (primarily useful for debugging).
 */
function startsWith($arr, $keyString) {
    foreach ($arr as $item) {
		if (strpos($keyString,$item) === 0) {
            return true;
        }
    }
    return false;
}
     //http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_service=badiu.system.module.module.install&_function=setupsystem&bundle=BadiuSystemModuleBundle
    public function setupsystem() {
       $operation=  $this->getContainer()->get('request')->get('operation');
       $defaultbundle=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('bundle');
      
       if(empty($operation)){$operation='new';}
     
        if(empty($defaultbundle)){$defaultbundle=null;}
        $result=$this->installBundle($operation,$defaultbundle);
        return $result;
    }

   
//   http://fabrica.badiu.com.br/~colaborador7/badiunet/web/app_dev.php/system/module/teste/install
    public function installBundle($operation='new',$defaultbundle) {
         $param = array('importclient' => true, 'importcustom' => false, 'importextended' => false);
        if(!empty($defaultbundle)){
            $config = $this->getContainer()->get('badiu.system.core.config');
            $config->setBundle($defaultbundle);
            $dconfig = $config->getDefaulFormConfig($param);
           $result=$this->addModule($defaultbundle, $dconfig,$operation);
		   //update install
		   $bundlekey=$this->getUtildata()->getVaueOfArray($dconfig,'badiu.module.key');
		   
		     $minstall=$bundlekey.".install";
       
             if($this->getContainer()->has($minstall)){
                 $datainstall=$this->getContainer()->get($minstall);
                 if(!empty($entity)){$datainstall->setEntity($entity);}
                 if(method_exists($datainstall ,'exec')){
                    $result['install']['operation'] =$datainstall->exec();
                 }
             }
		   
           return $result;
        }
        $bundles = $this->getContainer()->getParameter('kernel.bundles');
        $results=array();
        $cont=0;
        foreach ($bundles as $key => $value) {
            $config = $this->getContainer()->get('badiu.system.core.config');
            $config->setBundle($key);
            $dconfig = $config->getDefaulFormConfig($param);
            $resultmod=$this->addModule($key, $dconfig,$operation);
            $results[$cont]=$resultmod;
            $cont++;
           
        }
        
        return $results;
    }

    public function addModule($bundle, $dconfig,$operation) {

        if ($dconfig == null || sizeof($dconfig) == 0)
        return null;
        $version = null;
        $versionDate = null;
        $bkey = null;
        $category = null;


        if (array_key_exists('badiu.module.version', $dconfig)) {
            $version = $dconfig['badiu.module.version'];
        }

        if (array_key_exists('badiu.module.version.date', $dconfig)) {
            $versionDate = $dconfig['badiu.module.version.date'];
        }

        if (array_key_exists('badiu.module.key', $dconfig)) {
            $bkey = $dconfig['badiu.module.key'];
        }

        if (array_key_exists('badiu.module.category', $dconfig)) {
            $category = $dconfig['badiu.module.category'];
        }

        if ($version == null || $bkey == null){return null;}
            
       $data = $this->getContainer()->get('badiu.system.module.module.data');
        $param = array('bundlekey' => $bkey);
        $exist = $data->countNativeSql($param, false);

        if (!$exist) {
            $param = array('bundlekey' => $bkey, 'bundle' => $bundle, 'cetegory' => $category, 'timecreated' => new \Datetime(), 'execcron' => 0,'version'=>$version,'enabled'=>1);
            $result = $data->insertNativeSql($param, false);
        } else {
            if($operation=='update'){
                $paramfilter=array('bundlekey' => $bkey, 'bundle' => $bundle);
                $id=$data->getIdByParam($paramfilter);
                $param['id']=$id;
                $data->updateNativeSql($param, false);
            }
        }
        
        $resultconf=$this->addConfig($dconfig, $bundle,$operation);
        
        $result=array('bundlekey' => $bkey, 'bundle' => $bundle,'config'=>$resultconf);
        return $result;
    }

    public function addConfig($dconfig, $bundle,$operation) {
        if ($dconfig == null || sizeof($dconfig) == 0)
            return null;
        if (!array_key_exists('badiu.module.key', $dconfig)) {
            return null;
        }

        $data = $this->getContainer()->get('badiu.system.module.configglobal.data');
        $type = $this->getContainer()->get('badiu.system.module.lib.config.type');
        $mparamconfig = $this->getContainer()->get('badiu.system.module.lib.config.paramconfig');
        $kparamconfig = $this->getContainer()->get('badiu.system.core.lib.config.paramconfig');
        $keyutil = $this->getContainer()->get('badiu.system.core.lib.config.keyutil');
        $bkey = $dconfig['badiu.module.key'];
        $countinsert=0;
        $countupdate=0;
        
        foreach ($dconfig as $key => $value) {

            $key = trim($key);
            if ($key != 'badiu.module.version' || $key != 'badiu.module.version.date' || $key != 'badiu.module.key' || $key != 'badiu.module.category') {
                if (!is_array($value)) {
                    $param = null;
                   // $ctype = $kparamconfig->getType($value);
                   // $ftype = $keyutil->getType($key);
                   // $cdefault = $kparamconfig->getDefault($value);
                  //  $clevel = $kparamconfig->getLevel($value);
                    $configvalue=null;
                    
                    
                    /* if ($ctype=='config') {
                          $configvalue=$value;
                         $value = $cdefault;
                    }*/
                  

                    $param = array('bundlekey' => $bkey, 'name' => $key);
                    $exist = $data->countNativeSql($param, false);

                    if (!$exist) {
                       // $param = array('bundlekey' => $bkey, 'name' => $key, 'value' =>$value,'param'=> $configvalue, 'ftype' => $ftype, 'cdefault' => $cdefault, 'ctype' => $ctype, 'clevel' => $clevel, 'timecreated' => new \Datetime());
                       $param = array('bundlekey' => $bkey, 'name' => $key, 'value' =>$value,'timecreated' => new \Datetime());
                        $result = $data->insertNativeSql($param, false);
                        if($result){$countinsert++;}
                    } else {
                         $paramfilter=array('bundlekey' => $bkey, 'name' => $key);
                         $id=$data->getIdByParam($paramfilter);
                     
                         $param['id']=$id;
                         $param['value']=$value;
                         $result=$data->updateNativeSql($param, false);
                         
                        if($result){$countupdate++; }
                      
                    }
                    
                }
            }
        }
        
        $result=array('insert'=>$countinsert,'update'=>$countupdate);
        return $result;
    }
    public function addConfigSistem() {
        
        $data = $this->getContainer()->get('badiu.system.module.configglobal.data');
        $configpath=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('configpath');
        $bundlekey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('bundlekey');
       
        $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
       
        $file=$utilapp->getFilePath( $configpath);
        if(!file_exists($file)){
            echo "Arquivo $file não econtrado";exit;
         }
         $dconfig= Yaml::parse($file);
        
         if(!is_array($dconfig)){return null;}
    
        $countinsert=0;
        $countupdate=0;
        
        foreach ($dconfig as $key => $value) {

            $key = trim($key);
                 if (!is_array($value)) {
                    $param = null;
                    $configvalue=null;
                    $param = array('bundlekey' => $bundlekey, 'name' => $key);
                    $exist = $data->countNativeSql($param, false);

                    if (!$exist) {
                        $param = array('bundlekey' => $bundlekey, 'name' => $key, 'value' =>$value,'timecreated' => new \Datetime());
                        $result = $data->insertNativeSql($param, false);
                        if($result){$countinsert++;}
                    } else {
                         $paramfilter=array('bundlekey' => $bundlekey, 'name' => $key);
                         $id=$data->getIdByParam($paramfilter);
                     
                         $param['id']=$id;
                         $param['value']=$value;
                         $result=$data->updateNativeSql($param, false);
                         
                        if($result){$countupdate++; }
                      
                    }
                    
                }
            
        }
        
        $result=array('insert'=>$countinsert,'update'=>$countupdate);
        return $result;
    }
    public function addClientModule($bundle, $dconfig) {
        if ($dconfig == null || sizeof($dconfig) == 0)
            return null;
        $version = null;
        $versionDate = null;
        $bkey = null;
        $category = null;


        if (array_key_exists('badiu.module.version', $dconfig)) {
            $version = $dconfig['badiu.module.version'];
        }

        if (array_key_exists('badiu.module.version.date', $dconfig)) {
            $versionDate = $dconfig['badiu.module.version.date'];
        }

        if (array_key_exists('badiu.module.key', $dconfig)) {
            $bkey = $dconfig['badiu.module.key'];
        }

        if (array_key_exists('badiu.module.category', $dconfig)) {
            $category = $dconfig['badiu.module.category'];
        }

        if ($version == null || $bkey == null)
            return null;
        $entity = $this->getContainer()->get('badiu.system.module.client.entity');
        $entity->setVersion($version);
        //$entity->setVersiondate($versionDate);
        //$entity->setVersiondate(new \Datetime());
        $entity->setBkey($bkey);
        $entity->setBundle($bundle);
        $entity->setCetegory($category);
        $entity->setEnabled(true);
        $entity->setTimecreated(new \Datetime());
        $entity->setExeccron(false);
        $data = $this->getContainer()->get('badiu.system.module.client.data');
        if ($data->exist($bkey)) {
            echo "bundle ja cadastrado<br>";
        } else {
            $data->setDto($entity);
            $data->save();
            echo "bd saved<br>";
            //$this->addClientConfig($dconfig);
        }
        $this->addClientConfig($dconfig);
    }

    public function addClientConfig($dconfig) {
        if ($dconfig == null || sizeof($dconfig) == 0)
            return null;
        if (!array_key_exists('badiu.module.key', $dconfig)) {
            return null;
        }

        $data = $this->getContainer()->get('badiu.system.module.clientconfig.data');
        $bkey = $dconfig['badiu.module.key'];
        foreach ($dconfig as $key => $value) {
            if ($key != 'badiu.module.version' ||
                    $key != 'badiu.module.version.date' ||
                    $key != 'badiu.module.key' ||
                    $key != 'badiu.module.category') {


                $entity = $this->getContainer()->get('badiu.system.module.clientconfig.entity');
                $entity->setId(null);
                $entity->setEntity(1);
                $entity->setBkey($bkey);
                $entity->setName($key);
                $entity->setValue($value);
                $entity->setTimecreated(new \Datetime());
                $data->setDto($entity);
                if ($data->exist($key)) {
                    echo "bundle ja cadastrado<br>";
                } else {
                    $data->save();
                    echo "config bd saved<br>";
                }
            }
        }
    }

    public function makeEntity($bundle, $dconfig) {
        
    }

    //return new | different |  equal
    public function compareConfigData($newentity, $dbentity) {
        $strnewentity = "";
        $strdbentity = "";
        //	print_r($newentity);
        //	echo "<hr>";
        //	print_r($dbentity);exit;
        if ($dbentity == null)
            return 'new';
        //if (!array_key_exists("bkey",$dbentity)) return false;
        //print_r($dbentity);exit;
        $bkeynewentity = null;
        $namenewentity = null;
        $valuenewentity = null;
        $paramnewentity = null;

        if (array_key_exists("bkey", $newentity)) {
            $bkeynewentity = $newentity['bkey'];
        }
        if (array_key_exists("name", $newentity)) {
            $namenewentity = $newentity['name'];
        }
        if (array_key_exists("value", $newentity)) {
            $valuenewentity = $newentity['value'];
        }
        //if (array_key_exists("param",$newentity)){$paramnewentity=$newentity['param'];}


        if ($bkeynewentity != null) {
            $strnewentity.=$bkeynewentity;
        }
        if ($namenewentity != null) {
            $strnewentity.=$namenewentity;
        }
        if ($valuenewentity != null) {
            $strnewentity.=$valuenewentity;
        }
        //if($paramnewentity!=null){$strnewentity.=$paramnewentity;}


        $bkeydbentity = null;
        $namedbentity = null;
        $valuedbentity = null;
        $paramdbentity = null;

        if (array_key_exists("bkey", $dbentity)) {
            $bkeydbentity = $dbentity['bkey'];
        }
        if (array_key_exists("name", $dbentity)) {
            $namedbentity = $dbentity['name'];
        }
        if (array_key_exists("value", $dbentity)) {
            $valuedbentity = $dbentity['value'];
        }
        //if (array_key_exists("param",$dbentity)){$paramdbentity=$dbentity['param'];}

        if ($bkeydbentity != null) {
            $strdbentity.=$bkeydbentity;
        }
        if ($namedbentity != null) {
            $strdbentity.=$namedbentity;
        }
        if ($valuedbentity != null) {
            $strdbentity.=$valuedbentity;
        }
        //if($paramdbentity!=null){$strdbentity.=$paramdbentity;}
        // exit;
        if ($strdbentity != $strnewentity) {
            echo $strdbentity . "<br>";
            echo $strnewentity . "<br>";
            return 'different';
        }
        return 'equal';
    }

   //http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_service=badiu.system.module.module.install&_function=setupConfigInstance&configpath=BadiuMoodleMreportBundle:Resources/config/_extra/alante_enrol_course_completation_date.yml&modulekey=badiu.moodle.mreport.site.data&moduleinstance=38
   //http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/core/report/dynamic/38?_serviceid=38&_datasource=servicesql&_dkey=badiu.moodle.mreport.enrol.alantecoursecompletedfinalgrade.index
    public function setupConfigInstance() {
        $configpath=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('configpath');
        $modulekey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('modulekey');
        $bundlekey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($modulekey);
        $moduleinstance=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('moduleinstance');
        $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
        $data=$this->getContainer()->get('badiu.system.module.configinstance.data');
        $file=$utilapp->getFilePath( $configpath);
        if(!file_exists($file)){
            echo "Arquivo $file não econtrado";exit;
         }
         $param= Yaml::parse($file);
        
         if(!is_array($param)){return null;}
         $countnew=0;
         $countupadate=0;
         foreach ($param as $name => $value) {
            $paramdb=array();
            $paramd['name']=$name;
            $paramd['value']=$value;
            $paramd['modulekey']=$modulekey;
            $paramd['moduleinstance']=$moduleinstance;
            $paramd['bundlekey']=$bundlekey;
            
            $paramcheck=array('entity'=>$this->getEntity(),'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'name'=>$name);
            $exist = $data->countNativeSql($paramcheck, false);
            if (!$exist) {
                $result = $data->insertNativeSql($paramd);
                $countnew++;
            } else {
                $id=$data->getIdByParam($paramcheck);
                $param['id']=$id; 
                $result=$data->updateNativeSql($paramd);
                $countupadate++;        
            }
         }
        $result=array('insert'=>$countnew,'update'=>$countupadate);
        print_r($result);exit;
     
         return $result;
     }
     //http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_service=badiu.system.module.module.install&_function=setupConfigEntity&configpath=BadiuMoodleMreportBundle:Resources/config/_extra/alante_enrol_course_completation_date.yml&bundlekey=badiu.moodle.mreport
//     http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/core/report/dynamic/38?_serviceid=38&_datasource=servicesql&_dkey=badiu.moodle.mreport.enrol.alantecoursecompletedfinalgrade.index
public function setupConfigEntity() {
    $configpath=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('configpath');
    $bundlekey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('bundlekey');
    $enableinheritance=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('enableinheritance');
    $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
    $data=$this->getContainer()->get('badiu.system.module.configentity.data');
    $file=$utilapp->getFilePath( $configpath);
    if(!file_exists($file)){
        echo "Arquivo $file não econtrado";exit;
     } 
     $param= Yaml::parse($file);
    
     if(!is_array($param)){return null;}
     $countnew=0;
     $countupadate=0;

     foreach ($param as $name => $value) {
        $paramd=array();
        $paramd['name']=$name;
        $paramd['value']=$value;
        $paramd['bundlekey']=$bundlekey;
        $paramd['enableinheritance']=$enableinheritance;
        $paramcheck=array('entity'=>$this->getEntity(),'name'=>$name);
        
        $exist = $data->countNativeSql($paramcheck, false);

        if (!$exist) {
            $result = $data->insertNativeSql($paramd);
            $countnew++;
        } else {
            $id=$data->getIdByParam($paramcheck);
            $paramd['id']=$id; 
            $result=$data->updateNativeSql($paramd);
            if($result) {$countupadate++;}
        }
     }
    $result=array('insert'=>$countnew,'update'=>$countupadate);
    print_r($result);exit;
 
     return $result;
 }
 
  public function initTagLangDefaultValue() {
       
		 //get tag list without locale
		 $tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
		 //$param=array('entity'=>$this->getEntity());
		 $param=array();
		 $listid=$tagdata->getListidOfEmptyField("locale",$param);
		
		 $cont=0;
		 if(!is_array($listid)){return $cont;}
          foreach ($listid as $row) {
			   $id=$this->getUtildata()->getVaueOfArray($row,'id');
			   $uparam=array('id'=>$id,'locale'=>$this->getLang());
			   $cont+=$tagdata->updateNativeSql($uparam,false);
		  } 
		  return  $cont;
     }
 
 public function initTagUkeyDefaultValue() {
       
		//get tag list without ukey
		$tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
		 $param=array();
		 $listid=$tagdata->getListidvalueOfEmptyReference($param);
		
		 $cont=0;
		 if(!is_array($listid)){return $cont;}
		 $tagmanager=$this->getContainer()->get('badiu.system.module.tag.lib.manager');
          foreach ($listid as $row) {
			   $id=$this->getUtildata()->getVaueOfArray($row,'id');
			   $value=$this->getUtildata()->getVaueOfArray($row,'value');
			   $reference=$tagmanager->generateUniqueKey($value);
			   $uparam=array('id'=>$id,'reference'=>$reference);
			   $cont+=$tagdata->updateNativeSql($uparam,false);
		  }  
		  return  $cont;
     }
}
