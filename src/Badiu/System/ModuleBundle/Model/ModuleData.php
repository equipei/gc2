<?php
namespace Badiu\System\ModuleBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ModuleData  extends  BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

    public function exist($id) {
		$r=FALSE;
        $sql="SELECT  COUNT(o.bundlekey) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.bundlekey = :id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id',$id);
        $result= $query->getSingleResult();
        if($result['countrecord']>0){$r=TRUE;}
        return $r;
    }
	
	 public function getBundle($id) {
		$r=FALSE;
        $sql="SELECT  o.bundle FROM ".$this->getBundleEntity()." o  WHERE o.bundlekey = :id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id',$id);
        $result= $query->getSingleResult();
        $result=$result['bundle'];
        return $result;
    }
}
