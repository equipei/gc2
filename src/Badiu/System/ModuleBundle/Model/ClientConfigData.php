<?php

namespace Badiu\System\ModuleBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ClientConfigData extends BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
        parent::__construct($container,$bundleEntity);
    }

    public function exist($name) {

        $r=FALSE;
        $sql="SELECT  COUNT(o.name) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.name = :name";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('name',$name);
        $result= $query->getSingleResult();
        if($result['countrecord']>0){$r=TRUE;}
        return $r;
    }

    
     
}
