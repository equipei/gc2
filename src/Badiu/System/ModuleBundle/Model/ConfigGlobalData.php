<?php

namespace Badiu\System\ModuleBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ConfigGlobalData extends BadiuDataBase {

    function __construct(Container $container,$bundleEntity) {
        parent::__construct($container,$bundleEntity);
    }

    public function existByBkeyName($bundlekey,$name) {

        $r=FALSE;
        $sql="SELECT  COUNT(o.name) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE  o.bundlekey = :bundlekey AND o.name = :name";
        $query = $this->getEm()->createQuery($sql);
		$query->setParameter('bundlekey',$bundlekey);
        $query->setParameter('name',$name);
        $result= $query->getSingleResult();
        if($result['countrecord']>0){$r=TRUE;}
        return $r;
    }
	 public function findByBkeyName($bundlekey,$name) {
        $r=FALSE;
        $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.bundlekey = :bundlekey AND o.name = :name";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('bundlekey',$bundlekey);
		$query->setParameter('name',$name);
        $result= $query->getSingleResult();
       
        return $result;
    }
    public function getAllByBundle($bundle) {
        $moduledata=$this->getContainer()->get('badiu.system.module.module.data');
        $sql="SELECT  o.id,o.bundlekey,o.value,o.name,o.param FROM ".$this->getBundleEntity()." o JOIN ". $moduledata->getBundleEntity() ." m WITH o.bundlekey=m.bundlekey  WHERE m.bundle = :bundle";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('bundle',$bundle);
        $result= $query->getResult();
        return $result;
    }
    public function getConfig($param=array()) {
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id,o.name,o.value FROM ".$this->getBundleEntity()." o   WHERE o.id > 0 $wsql ";
        $query = $this->getEm()->createQuery($sql);
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return $result;
    }
    public function getConfigByNameStartWith($name) {
        $sql="SELECT  o.id,o.name,o.value FROM ".$this->getBundleEntity()." o   WHERE o.name LIKE :name ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('name',$name.'%');
        $result= $query->getResult();
        return $result;
    }
	
	public function getConfigByNameEndWith($name) {
        $sql="SELECT  o.id,o.name,o.value FROM ".$this->getBundleEntity()." o   WHERE o.name LIKE :name ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('name','%'.$name);
        $result= $query->getResult();
        return $result;
    }
    public function getValue($name) {
        $sql="SELECT  o.value FROM ".$this->getBundleEntity()." o   WHERE o.name = :name";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('name',$name);
        $result= $query->getOneOrNullResult();
        if(!empty($result)){$result=$result['value'];}
        return $result;
    }
  } 