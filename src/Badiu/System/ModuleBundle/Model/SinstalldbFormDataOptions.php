<?php

namespace Badiu\System\ModuleBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class SinstalldbFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }

	public  function getDbType(){
                $list=array();
                $list['pdo_mysql']=$this->getTranslator()->trans('badiu.system.module.sinstalldb.dbdriver.mysql');
		$list['pdo_pgsql']=$this->getTranslator()->trans('badiu.system.module.sinstalldb.dbdriver.pgsql');
		
        return $list;
    }

}
