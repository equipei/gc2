<?php

namespace Badiu\System\ModuleBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class ConfigEntityData extends BadiuDataBase {

    function __construct(Container $container, $bundleEntity) {
        parent::__construct($container, $bundleEntity);
    }

 public function getConfig($entity) {
        $sql="SELECT  o.id,o.enableinheritance,o.name,o.value FROM ".$this->getBundleEntity()." o   WHERE o.entity =:entity ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $result= $query->getResult();
        return $result;
    }

    public function getConfigByNameStartWith($entity,$name) {
        $sql="SELECT  o.id,o.enableinheritance,o.name,o.value FROM ".$this->getBundleEntity()." o   WHERE o.entity =:entity AND o.name LIKE :name";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('name',$name.'%');
        $result= $query->getResult();
        return $result;
    }
	 public function getConfigByNameEndWith($entity,$name) {
        $sql="SELECT  o.id,o.enableinheritance,o.name,o.value FROM ".$this->getBundleEntity()." o   WHERE o.entity =:entity AND o.name LIKE :name";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('name','%'.$name);
        $result= $query->getResult();
        return $result;
    }
}
