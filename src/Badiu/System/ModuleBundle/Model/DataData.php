<?php

namespace Badiu\System\ModuleBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class DataData extends BadiuDataBase {

    function __construct(Container $container, $bundleEntity) {
        parent::__construct($container, $bundleEntity);
    }

    public function getModuleinstanceByShortname($shortname,$param=array()) {
        $wsql=$this->makeSqlWhere($param);
        $sql = "SELECT  o.moduleinstance FROM " . $this->getBundleEntity() . " o  WHERE o.shortname=:shortname $wsql ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('shortname',$shortname);
        $query=$this->makeSqlFilter($query,$param);
        $result = $query->getOneOrNullResult();
        $result = $result['moduleinstance'];
        return $result;
    }
public function getIdByShortname($shortname,$param=array()) {
        $wsql=$this->makeSqlWhere($param);
        $sql = "SELECT  o.id FROM " . $this->getBundleEntity() . " o  WHERE o.shortname=:shortname $wsql ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('shortname',$shortname);
        $query=$this->makeSqlFilter($query,$param);
        $result = $query->getOneOrNullResult();
        $result = $result['id'];
        return $result;
    }
}
