<?php

namespace Badiu\System\ModuleBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormError;
class ConfigController extends BadiuController
{
     private $parentid;
    function __construct(Container $container) {
            parent::__construct($container);
              }
              
    public function addCheckForm($form,$data) {
           $check=true;
           return  $check;
       }

       public function setDefaultDataOnOpenEditForm($dto) {
                   $lib=$this->getContainer()->get('badiu.system.module.config.libform');
		   $id=$this->getContainer()->get('request')->get('id');
		   $dto=$lib->getDataFormEdit($id);
		  return $dto;
     }
	 public function save($data) {
                 $customdata=$this->getContainer()->get('badiu.system.module.custom.data');
                 $customdata->setDto($data->getDto());
             
		 $lib=$this->getContainer()->get('badiu.system.module.config.libform');
		 
                 $result= $lib->save($customdata);
                
		return $result;
    }   
	
  
	
}
