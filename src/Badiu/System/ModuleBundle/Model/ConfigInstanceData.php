<?php

namespace Badiu\System\ModuleBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class ConfigInstanceData extends BadiuDataBase {

    function __construct(Container $container, $bundleEntity) {
        parent::__construct($container, $bundleEntity);
    }

 public function getConfigByEntity($entity) {
        $sql="SELECT  o.id,o.enableinheritance,o.name,o.value,o.moduleinstance,o.modulekey FROM ".$this->getBundleEntity()." o   WHERE o.entity =:entity ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $result= $query->getResult();
        return $result;
    }
 public function getConfig($entity,$modulekey,$moduleinstance) {
        $sql="SELECT  o.id,o.enableinheritance,o.name,o.value FROM ".$this->getBundleEntity()." o   WHERE o.entity =:entity AND o.modulekey=:modulekey AND o.moduleinstance=:moduleinstance ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('modulekey',$modulekey);
        $query->setParameter('moduleinstance',$moduleinstance);
        $result= $query->getResult();
        return $result;
    }

    public function getConfigByNameStartWith($entity,$modulekey,$moduleinstance,$name) {
        $sql="SELECT  o.id,o.enableinheritance,o.name,o.value FROM ".$this->getBundleEntity()." o   WHERE o.entity =:entity AND o.modulekey=:modulekey AND o.moduleinstance=:moduleinstance AND o.name LIKE :name";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('modulekey',$modulekey);
        $query->setParameter('moduleinstance',$moduleinstance);
        $query->setParameter('name',$name.'%');
        $result= $query->getResult();
        return $result;
    }
}
