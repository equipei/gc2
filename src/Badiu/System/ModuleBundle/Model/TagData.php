<?php

namespace Badiu\System\ModuleBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class TagData extends BadiuDataBase {

    function __construct(Container $container, $bundleEntity) {
        parent::__construct($container, $bundleEntity);
    }

	public function removeWithoutDtype($entity,$param) {
            $wsql=$this->makeSqlWhere($param);
            $sql="DELETE  FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.dtype IS NULL $wsql ";
             $query = $this->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
             $query=$this->makeSqlFilter($query, $param);
             $result=$query->execute();
             return $result;
       }
	   
	 public function getValuesWithoutDtype($entity,$param) {
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  o.value FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.dtype IS NULL $wsql ORDER BY o.value ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
             $query=$this->makeSqlFilter($query, $param);
            $result= $query->getResult();
            return  $result;
       }
	   
	    public function getListidvalueOfEmptyReference($param,$parampaging=null) {
        $x=null;
        $y=null;
		
        if(!empty($parampaging) && is_array($parampaging)){
            if(array_key_exists('offset',$parampaging)){$x=$parampaging['offset'];}
            if(array_key_exists('limit',$parampaging)){$y=$parampaging['limit'];}
        }

        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id,o.value FROM ".$this->getBundleEntity()." o  WHERE o.id > 0 AND (o.reference ='' OR o.reference IS NULL)  $wsql ";
		
        $query = $this->getEm()->createQuery($sql);
         $query=$this->makeSqlFilter($query, $param);
         if($x!=null && $x >= 0){$query->setFirstResult($x);}
         if($y!=null && $y >= 0){$query->setMaxResults($y);}
         $result= $query->getResult();
         
         return $result;
   }
}
