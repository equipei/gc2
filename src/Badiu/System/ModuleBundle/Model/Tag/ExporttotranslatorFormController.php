<?php

namespace Badiu\System\ModuleBundle\Model\Tag;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ExporttotranslatorFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
    }
    
     
	 public function save() {
          $param = $this->getParam();
		  
		  $tmanage=$this->getContainer()->get('badiu.system.module.tag.lib.managetranslator');
		   $presult=$tmanage->exportText($param);
	 
	   
		   $msg=$this->getTranslator()->trans('badiu.system.module.tagexporttotranslator.result',array('%countstring%'=>$presult));
		   $this->setSuccessmessage($msg);
		   $urlgoback=null;
       
           $outrsult=array('result'=>$presult,'message'=>$this->getSuccessmessage(),'urlgoback'=>$urlgoback);
          $this->getResponse()->setStatus("accept");
          $this->getResponse()->setMessage($outrsult);
         return $this->getResponse()->get();
         
     }

}
