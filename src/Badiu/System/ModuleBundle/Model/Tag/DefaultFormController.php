<?php

namespace Badiu\System\ModuleBundle\Model\Tag;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class DefaultFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
    }
    
       public function changeParam() {
		
        $value= $this->getParamItem('value');
		$tagmanager=$this->getContainer()->get('badiu.system.module.tag.lib.manager');
        $reference=$tagmanager->generateUniqueKey($value);
		$this->addParamItem('reference',$reference);
	   }

}
