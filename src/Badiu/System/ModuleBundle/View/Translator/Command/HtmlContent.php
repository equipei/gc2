<?php
$command = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('command');

$deletecachecurrenturl=$utilapp->getUrlByRoute('badiu.system.core.command.dashboard',array('msgservicecommandexec'=>1,'command'=>'deletecache'));
$servicedeletecacheurlparam= array('_function'=>'requestCommand','_operation'=>'deletecache','_urlgoback'=>urlencode($deletecachecurrenturl));
$servicedeletecacheurl=$utilapp->getUrlByRoute('badiu.system.core.update.link',$servicedeletecacheurlparam);

$deleteuserssessionscurrenturl=$utilapp->getUrlByRoute('badiu.system.core.command.dashboard',array('msgservicecommandexec'=>1,'command'=>'deleteuserssessions'));
$servicedeleteuserssessionsurlparam= array('_function'=>'requestCommand','_operation'=>'deleteuserssessions','_urlgoback'=>urlencode($deleteuserssessionscurrenturl));
$servicedeleteuserssessionsurl=$utilapp->getUrlByRoute('badiu.system.core.update.link',$servicedeleteuserssessionsurlparam);

$badiuSession=$container->get('badiu.system.access.session');
$isrootuser=$badiuSession->isRootUser();
$entity=$badiuSession->get()->getEntity();
$responsecommand=badiu_system_core_command_exec($container,$page);
 
function badiu_system_core_command_exec($container,$page){
	$command = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('command');
	$appservercommand=$container->get('badiu.system.core.lib.appservercommand');
	$translator = $page->getTranslator();
	$result="";
	$result=$appservercommand->execCommand($command);
		
	if(!empty($result)){$result="<div class=\"alert alert-success\">$result</div>";}
	return $result;
}

function badiu_system_core_command_islinkactive($linkcommand, $command){
	if($linkcommand==$command){return " active ";}

	return "";
}
 $translatorglobaldata = $container->get('badiu.system.module.translatorglobal.data');
 $globalistbylocale=$translatorglobaldata->getListConsolidateByLocale();
 
  $translatorentitydata = $container->get('badiu.system.module.translatorentity.data');
 $entitylistbylocale=$translatorentitydata->getListConsolidateByLocale($entity);
 
 $translatorinstancedata = $container->get('badiu.system.module.translatorinstance.data');
 $instancelistbylocale=$translatorinstancedata->getListConsolidateByLocale($entity);
?>

<div id="_badiu_theme_core_dashboard_vuejs">
<?php echo $responsecommand;?>

<h4><?php echo $translator->trans('badiu.system.module.translatorcommand.dashboard');?></h4><hr /> 

 <div class="list-group">
	<?php if($isrootuser){?><a href="?command=importalllanguaguetodb" class="list-group-item <?php echo badiu_system_core_command_islinkactive('importalllanguaguetodb', $command);?>"><?php echo $translator->trans('badiu.system.module.translator.importalllanguaguetodb');?></a><?php }?>
	<?php if($isrootuser){?><a href="?command=makegloballanguaguecache" class="list-group-item <?php echo badiu_system_core_command_islinkactive('makegloballanguaguecache', $command);?>"><?php echo $translator->trans('badiu.system.module.translator.makegloballanguaguecache');?></a><?php }?>
	<a href="?command=makeentitylanguaguecache" class="list-group-item <?php echo badiu_system_core_command_islinkactive('makeentitylanguaguecache', $command);?>"><?php echo $translator->trans('badiu.system.module.translator.makeentitylanguaguecache');?></a>
	 
  </div>
<br />	

<h4><?php echo $translator->trans('badiu.system.module.translator.global');?></h4><hr /> 

<table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th><?php echo $translator->trans('badiu.system.module.translator.locale');?></th>
		 <th><?php echo $translator->trans('badiu.system.module.translator.string');?></th>
		<th><?php echo $translator->trans('badiu.system.module.translator.manage');?> </th>
       
      </tr>
    </thead>
    <tbody >
	<?php 
	$cont=0;
	foreach ($globalistbylocale as $grow) {
		$locale=$utildata->getVaueOfArray($grow,'locale');
		$countstring=$utildata->getVaueOfArray($grow,'countstring');
		$link="";
	
	?>
      <tr>
	  <td><?php echo $locale; ?></td>
		<td><?php echo $countstring; ?></td>
        <td><!--<a href="<?php echo $link;?>"><?php echo $translator->trans('badiu.system.core.command.moduleupdate');?></a>--></td>
      </tr>
		<?php };?>
     
    </tbody>
  </table>
  
  <br />
<h4><?php echo $translator->trans('badiu.system.module.translator.entity');?></h4><hr /> 
   
  <br>
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
         <th><?php echo $translator->trans('badiu.system.module.translator.locale');?></th>
		 <th><?php echo $translator->trans('badiu.system.module.translator.string');?></th>
		<th><?php echo $translator->trans('badiu.system.module.translator.manage');?> </th>
      
      </tr>
    </thead>
    <tbody id="badiunetllistallmaudlesbodytbl">
	<?php 
	$cont=0;
	$bundles=array();
	foreach ( $entitylistbylocale as $grow) {
		$locale=$utildata->getVaueOfArray($grow,'locale');
		$countstring=$utildata->getVaueOfArray($grow,'countstring');
		$link="";
	?>
      <tr>
	  <td><?php echo $locale; ?></td>
		<td><?php echo $countstring; ?></td>
        <td><!--<a href="<?php echo $link;?>"><?php echo $translator->trans('badiu.system.core.command.moduleupdate');?></a>--></td>
      </tr>
	<?php };?>
     
    </tbody>
  </table>
 
 
  <br />
<h4><?php echo $translator->trans('badiu.system.module.translator.instace');?></h4><hr /> 
 
  <br>
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
         <th><?php echo $translator->trans('badiu.system.module.translator.locale');?></th>
		 <th><?php echo $translator->trans('badiu.system.module.translator.string');?></th>
		<th><?php echo $translator->trans('badiu.system.module.translator.manage');?> </th>
      
      </tr>
    </thead>
    <tbody id="badiunetllistinstancebodytbl">
	<?php 
	$cont=0;
	$bundles=array();
	foreach ( $instancelistbylocale as $grow) {
		$locale=$utildata->getVaueOfArray($grow,'locale');
		$countstring=$utildata->getVaueOfArray($grow,'countstring');
		$link="";
	?>
      <tr>
	  <td><?php echo $locale; ?></td>
		<td><?php echo $countstring; ?></td>
        <td><!--<a href="<?php echo $link;?>"><?php echo $translator->trans('badiu.system.core.command.moduleupdate');?></a>--></td>
      </tr>
	<?php };?>
     
    </tbody>
  </table>

</div>