<?php

namespace Badiu\System\FileBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class FileData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

	

	public function exitAuthkey($authkey) {
		$sql="SELECT COUNT(o.id) AS countrecord  FROM ".$this->getBundleEntity()." o WHERE o.authkey=:authkey";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('authkey',$authkey);
        $result= $query->getSingleResult();
		$r=FALSE;
		if($result['countrecord']>0){$r=TRUE;}
        return $r;
       
        
    }
	
	public function getIdByAuthkey($authkey) {
		$sql="SELECT o.id  FROM ".$this->getBundleEntity()." o WHERE o.authkey=:authkey";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('authkey',$authkey);
        $result= $query->getSingleResult();
		$result=$result['id'];
        return $result;
       
        
    }
	
	public function findByAuthkey($authkey) {
		$sql="SELECT o  FROM ".$this->getBundleEntity()." o WHERE o.authkey=:authkey";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('authkey',$authkey); 
                $result= $query->getSingleResult();
		return $result;
     } 
     public function getInfoByAuthkey($authkey) {
		$sql="SELECT o.id,o.entity,o.name,o.codename,o.ftype,o.fpath,o.modulekey,o.authkey  FROM ".$this->getBundleEntity()." o WHERE o.authkey=:authkey";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('authkey',$authkey);
                $result= $query->getSingleResult();
		return $result;
     }
}
