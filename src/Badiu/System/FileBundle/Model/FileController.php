<?php

namespace Badiu\System\FileBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FileController extends BadiuController {
	function __construct(Container $container) {
		parent::__construct($container);
	}

	
	public function save($data) {
                $authkey=$data->getDto()->getAuthkey();
               if(empty($authkey)){
                    $hash=$this->getContainer()->get('badiu.system.core.lib.util.hash');
		    $key=$hash->make(40);
                    $authkey="file-defualtkey-".time()."-$key";
                   
               }
               $data->getDto()->setAuthkey($authkey);
               $data->save();
               $this->updateAuthkey($data);
	       return $data->getDto();
	}

        public function updateAuthkey($data) {
                 $authkey=$data->getDto()->getAuthkey();
                 $pos=strpos($authkey, "file-defualtkey-");
                 if ($pos !== false) {
                       $filelib=$this->getContainer()->get('badiu.system.file.file.lib');
                       $newkky=$filelib->generateAuthkey($data->getDto()->getId());
                       $data->getDto()->setAuthkey($newkky);
                       $data->save();
                 }
        }
}