<?php
 
namespace Badiu\System\FileBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }
 

    public function exec() {
       $result+=$this->initPermissions();
  		return $result;
	} 
	
	
	  public function initPermissions() {
        $cont=0;
		
        $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');

        //guest  
        $param=array(
		  'entity'=>$this->getEntity(),
          'roleshortname'=>'guest',
          'permissions'=>array('badiu.system.file.donwload.index',
          'badiu.system.file.upload.exec')
        );
        $cont+=$libpermission->add($param);

		return  $cont;
    }
 
}
