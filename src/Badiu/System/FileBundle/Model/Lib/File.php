<?php

namespace Badiu\System\FileBundle\Model\Lib;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
class File extends BadiuModelLib {

	function __construct(Container $container) {
		parent::__construct($container);
	}
	
	public function getOnlyAuthkeyOfAuthkeyWithname($authkeywithname) {
			$authkey=null;
			$pos=stripos($authkeywithname, "/");
			if($pos!== false){
				$p= explode("/", $authkeywithname);	
				$authkey=$this->getUtildata()->getVaueOfArray($p,0);
			}
			return $authkey;
	}
	public function getSystemBasePath() {
		$defaultpath =null;
		if($this->getContainer()->hasParameter('badiu.system.file.defaultpath')){
			$defaultpath =$this->getContainer()->getParameter('badiu.system.file.defaultpath');
		}
		if(empty($defaultpath)){
			$defaultpath=$this->getContainer()->get('kernel')->getRootDir().'/data';;
		}
		return $defaultpath;
	}
	public function getBasePath($param=array()) {
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		$authkey=$this->getUtildata()->getVaueOfArray($param,'authkey');
		$authkeywithname=$this->getUtildata()->getVaueOfArray($param,'authkeywithname');
		
		if(empty($entity)){$entity=$this->getEntity();}
		$defaultpath=$this->getSystemBasePath();
		$path=$defaultpath.'/files/'.$entity;
		$codename="";
		
		if(empty($authkey) && !empty($authkeywithname)){
			$pos=stripos($authkeywithname, "/");
			if($pos!== false){
				$p= explode("/", $authkeywithname);	
				$authkey=$this->getUtildata()->getVaueOfArray($p,0);
			}
			
		}
		if(!empty($authkey)){
			$filedata = $this->getContainer()->get('badiu.system.file.file.data');
			$codename=$filedata->getGlobalColumnValue('codename',$param=array('entity'=>$entity,'authkey'=>$authkey));
			if(!empty($codename)){$codename="/$codename";}
		}
		$path.=$codename;
		return $path; 
	}
	public function generateAuthkey($fileid,$length=20) {
		$hash = $this->getContainer()->get('badiu.system.core.lib.util.hash');
		$filestart = $fileid . "";
		$fileidsize = strlen($filestart);
		$hashsize = $length - $fileidsize;
		if($hashsize > 0){
			$key = $hash->make($hashsize);
			$key=strtolower($key);
			$fkey = $filestart . $key;
		}
		else {$fkey = $fileid;}
		return $fkey;
	}

	public function removeUrl($content) {
		$urlbase=$this->getUtilapp()->getBaseUrl();
		$fileurl=$urlbase."/system/file/get/{";
		$content=str_replace($fileurl,"{",$content);
		
		$fileurl=$urlbase."/system/file/get/";
		$content=str_replace($fileurl,"@@BADIUSYSTEMFILE_@@",$content);
		return $content;
	}
	public function addUrl($content) {
		$urlbase=$this->getUtilapp()->getBaseUrl();
		$fileurl=$urlbase."/system/file/get/";
		$content=str_replace("@@BADIUSYSTEMFILE_@@",$fileurl,$content);
		return $content;
	}
	public function add($fdata) {
		$filedata = $this->getContainer()->get('badiu.system.file.file.data');
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');

		
		$fdata['entity']=$this->getEntity();
	//	$fdata['name']=$this->getUtildata()->getVaueOfArray($param,'name');
		//$fdata['ftype']=$this->getUtildata()->getVaueOfArray($param,'type');
		//$fdata['fsize']=$this->getUtildata()->getVaueOfArray($param,'size');
	//	$fdata['fhost']=$this->getUtildata()->getVaueOfArray($param,'host');
		$fdata['dtype']='texteditor';
		//$fdata['authkey']=$this->getUtildata()->getVaueOfArray($param,'authkey');
		//$fdata['modulekey']=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		//$fdata['moduleinstance']=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		$fdata['timecreated']=new \DateTime();
		$fdata['useridadd']=$badiuSession->get()->getUser()->getId();
		$fdata['deleted']=0;
		$result=$filedata->insertNativeSql($fdata,false);
		return $result;
	}
	public function getUrl($fileinfo, $filesytem = 'linux') {
		$entity = null;
		$authkey = null;
		$fpath = null;
		$modulekey = null;
		$url = null;

		if (isset($fileinfo['entity'])) {$entity = $fileinfo['entity'];}
		if (isset($fileinfo['authkey'])) {$authkey = $fileinfo['authkey'];}
		if (isset($fileinfo['fpath'])) {$fpath = $fileinfo['fpath'];}
		if (isset($fileinfo['modulekey'])) {$modulekey = $fileinfo['modulekey'];}
		echo "sssf";print_r($fileinfo);exit;
		if (!empty($entity) && !empty($authkey) && !empty($fpath) && !empty($modulekey)) {
			//set sytem default path
			if ($fpath == 'default') {$fpath = '';}
			if ($filesytem == 'linux') {
				$url = $fpath . '/' . $entity . '/' . $modulekey . '/' . $authkey;
			}

		}

		return $url;
	}

	public function getMineType($filename) {

		$mime_types = null;

		$mime_types = array(

			'txt' => 'text/plain',
			'htm' => 'text/html',
			'html' => 'text/html',
			'php' => 'text/html',
			'css' => 'text/css',
			'js' => 'application/javascript',
			'json' => 'application/json',
			'xml' => 'application/xml',
			'swf' => 'application/x-shockwave-flash',
			'flv' => 'video/x-flv',

			// images
			'png' => 'image/png',
			'jpe' => 'image/jpeg',
			'jpeg' => 'image/jpeg',
			'jpg' => 'image/jpeg',
			'gif' => 'image/gif',
			'bmp' => 'image/bmp',
			'ico' => 'image/vnd.microsoft.icon',
			'tiff' => 'image/tiff',
			'tif' => 'image/tiff',
			'svg' => 'image/svg+xml',
			'svgz' => 'image/svg+xml',

			// archives
			'zip' => 'application/zip',
			'rar' => 'application/x-rar-compressed',
			'exe' => 'application/x-msdownload',
			'msi' => 'application/x-msdownload',
			'cab' => 'application/vnd.ms-cab-compressed',

			// audio/video
			'mp3' => 'audio/mpeg',
			'qt' => 'video/quicktime',
			'mov' => 'video/quicktime',

			// adobe
			'pdf' => 'application/pdf',
			'psd' => 'image/vnd.adobe.photoshop',
			'ai' => 'application/postscript',
			'eps' => 'application/postscript',
			'ps' => 'application/postscript',

			// ms office
			'doc' => 'application/msword',
			'rtf' => 'application/rtf',
			'xls' => 'application/vnd.ms-excel',
			'ppt' => 'application/vnd.ms-powerpoint',

			// open office
			'odt' => 'application/vnd.oasis.opendocument.text',
			'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
		);

		$spv = explode('.', $filename);
		$ext="";
		if(is_array($spv)){$ext=end($spv);}
		if (array_key_exists($ext, $mime_types)) {
			return $mime_types[$ext];
		} elseif (function_exists('finfo_open')) {
			$finfo = finfo_open(FILEINFO_MIME);
			$mimetype = finfo_file($finfo, $filename);
			finfo_close($finfo);
			return $mimetype;
		} else {
			return 'application/octet-stream';
		}

	}

	function downlaod($code,$forcedownload=false){
			$filedata = $this->getContainer()->get('badiu.system.file.file.data');
			$fileinfo=$filedata->getInfoByAuthkey($code);
		
			//$minetype=$this->getMineType($filename);
			$minetype=$this->getUtildata()->getVaueOfArray($fileinfo,'ftype');
			$filename=$this->getUtildata()->getVaueOfArray($fileinfo,'name');
			$filecodename=$this->getUtildata()->getVaueOfArray($fileinfo,'codename');
			$fpath=$this->getUtildata()->getVaueOfArray($fileinfo,'fpath');
			$entity=$this->getUtildata()->getVaueOfArray($fileinfo,'entity');
			
			$file=$this->getBasePath($fileinfo); 
			$response = new BinaryFileResponse($file);
			
		return $response;
	}
	
	function getContent($param){
			$bundlepath=$this->getUtildata()->getVaueOfArray($param,'bundlepath');
			//$bundlepath=str_replace("___", "/", $bundlepath);
			$allowed="BadiuSystemCoreBundle:Model/Lib/Qrcode/";
			
			 $pos=stripos($bundlepath, $allowed);
             if($pos!== 0){echo "file not allowed";exit;}
			$filepath=$this->getContainer()->get('badiu.system.core.lib.util.app')->getFilePath($bundlepath);
			//$filepath=$this->getUtildata()->getVaueOfArray($param,'filepath');
			//echo $filepath;exit; 
			$forcedownload=$this->getUtildata()->getVaueOfArray($param,'forcedownload');
			$minetype=$this->getMineType($filepath);
			$response = new BinaryFileResponse($filepath);
			return  $response;
	}

	
	function geFullPath($param){
			$filecodename=$this->getUtildata()->getVaueOfArray($param,'codename');
			$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			$fparam=array('entity'=>$entity);
			$path=$this->getBasePath($fparam);
			$file=$path.'/'.$filecodename;
			return  $file;
	}
}
