<?php
 
namespace Badiu\System\FileBundle\Model\Lib;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ManagePackage extends BadiuModelLib {

	function __construct(Container $container) {
		parent::__construct($container);
	}
 
	function decompress(){
		$filelib=$this->getContainer()->get('badiu.system.file.file.lib');
		$file=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('file');
		$authkey=$filelib->getOnlyAuthkeyOfAuthkeyWithname($file);
		
		$filedata=$this->getContainer()->get('badiu.system.file.file.data');
		$info=$filedata->getInfoByAuthkey($authkey);
		$entity=$this->getUtildata()->getVaueOfArray($info,'entity');
		//$filepath=$filelib->getUrl($info);
		//review basepath
		//$fpath=$this->getUtildata()->getVaueOfArray($info,'fpath');
		//$codename=$this->getUtildata()->getVaueOfArray($info,'codename');
		
		
		$filefullpath=$filelib->geFullPath($info);
		$fparam=array('entity'=>$entity);
		$fpath=$filelib->getBasePath($fparam);
		$patahpackage=$fpath."/".$authkey."_packge";
		if (!file_exists($patahpackage)) {
			mkdir($patahpackage, 0777, true);
		}
		
		$zip = new \ZipArchive;
		$res = $zip->open($filefullpath);
		if ($res === TRUE) {
			$zip->extractTo($patahpackage);
			$zip->close();
		}
		$pathfilepackage=$this->getPackagePath($patahpackage);
		$listpackagerootfiles=$this->formatListPackageFileRoot($pathfilepackage);
		return $listpackagerootfiles;
	}
   
   function getPackagePath($dir){
	    $subfolder1=$this->getDirElements($dir);
		$d1=$this->getUtildata()->getVaueOfArray($subfolder1,'dir');
		$f1=$this->getUtildata()->getVaueOfArray($subfolder1,'file');
		
		$result=array();
		if(!empty($f1) && is_array($f1) && sizeof($f1)> 0){
			$result['subpath']='';
			$result['files']=$f1;
			return $result;
		}
		$subfolder1="";
		if(!empty($d1) && is_array($d1) && sizeof($d1)> 0){
			if(sizeof($d1)==1){
				$subfolder1 = reset($d1);
			}else {
				return $result;
			}
		}
		$subfolder2=array();
		if(!empty($subfolder1)){
			$dir2=$dir."/".$subfolder1;
			$subfolder2=$this->getDirElements($dir2);
		}
		
		$d1=$this->getUtildata()->getVaueOfArray($subfolder2,'dir');
		$f1=$this->getUtildata()->getVaueOfArray($subfolder2,'file');
	
		if(!empty($f1) && is_array($f1) && sizeof($f1)> 0){
			$result['subpath']=$subfolder1;
			$result['files']=$f1;
			return $result;
		}
		$subfolder1="";
		if(!empty($d1) && is_array($d1) && sizeof($d1)> 0){
				return $result;
		}
		
		return $result;
   }
   
    function getDirElements($dir){
		 $files = scandir($dir);
		$list=array();
	    foreach ( $files as $k => $v) {
			$fpath="$dir/$v";
			if($v!='.' &&  $v!='..'){
				if(is_dir($fpath)){
					$list['dir'][$v]=$v;
				}else {$list['file'][$v]=$v;}
				
			}
	 }
	 return $list;
  }
   
    function formatListPackageFileRoot($listfile){
		$subpath=$this->getUtildata()->getVaueOfArray($listfile,'subpath');
		$files=$this->getUtildata()->getVaueOfArray($listfile,'files');
		$newlist=array();
		foreach ($files as $f){
			if(!empty($subpath)){$f="$subpath/$f";}
				$item=array('text'=>$f,'value'=>$f);
				array_push($newlist,$item);
				
			}
		
		return $newlist;
	}
}
