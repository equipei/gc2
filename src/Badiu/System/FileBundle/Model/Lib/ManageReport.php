<?php
namespace Badiu\System\FileBundle\Model\Lib;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Symfony\Component\HttpFoundation\Response;
use Box\Spout\Common\Entity\Style\CellAlignment;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
class ManageReport extends BadiuModelLib {

	private $fullpath;
	private $hashname;
	private $writer;
	private $format;
	private $deletefile;
	function __construct(Container $container) {
		parent::__construct($container);
		$this->deletefile=true;
	}
	function start($param=array()){
		$hashname= $this->getUtildata()->getVaueOfArray($param,'hashname');
		$hashnameappend= $this->getUtildata()->getVaueOfArray($param,'hashnameappend');
		$folderofdefaultpath= $this->getUtildata()->getVaueOfArray($param,'folderofdefaultpath');
		$path= $this->getUtildata()->getVaueOfArray($param,'path');

		if(!empty($hashname)){$this->hashname=$hashname;}
		else{
			$hash = $this->getContainer()->get('badiu.system.core.lib.util.hash');
			$hkey = $hash->make(45);
			$addkey="";
			if(!empty($hashnameappend)){$addkey='_'.$hashname;}
			$this->hashname='_report_'.$this->getUserid().'_'.time().'_'.$hkey.$addkey;
		}
		
		$pfaname=null;
		$filelib = $this->getContainer()->get('badiu.system.file.file.lib');
		$defaultpath=$filelib->getSystemBasePath();
		$basepath=$defaultpath.'/temp/'.$this->getEntity();
		if(!empty($folderofdefaultpath)){$basepath=$defaultpath.'/'.$folderofdefaultpath.'/'.$this->getEntity();}
		if(!empty($path)){$basepath=$path;}
		if (!file_exists($basepath)) {
			mkdir($basepath, 0777, true);
		}

		if($this->getFormat()=='csv'){
			$this->fullpath=$basepath.'/'.$this->hashname.'.csv';
			$this->writer = WriterEntityFactory::createCSVWriter();
		}else{
			$this->fullpath=$basepath.'/'.$this->hashname.'.xlsx';
			$this->writer = WriterEntityFactory::createXLSXWriter();
		}
        $this->writer->openToFile($this->fullpath); 
	} 
	function head($row){ 
		if (array_key_exists('_ctrl',$row)){unset($row['_ctrl']);}
		$style= (new StyleBuilder())
        //->setShouldWrapText()
		->setFontSize(11)
		->setFontBold()
		->build(); 
		$frow =null;
		if($this->getFormat()=='csv'){
			$frow = WriterEntityFactory::createRowFromArray($row);
		}else{
			$frow = WriterEntityFactory::createRowFromArray($row,$style);
		}
		
		$this->writer->addRow($frow);
	}
	function add($row){ 
	$frow =null;
		
		$frow = WriterEntityFactory::createRowFromArray($row);
		$this->writer->addRow($frow);
	}
	function addtable($table){
		if (array_key_exists('_ctrl',$table->getColumn()->getList())){unset($table->getColumn()->getList()['_ctrl']);}
		$style= (new StyleBuilder())
        //->setShouldWrapText()
		->setFontSize(11)
		->build(); 
		foreach ($table->getRow()->getList() as $row) {
           $rowitem=array();

             foreach ($table->getColumn()->getList() as  $ckey =>$value) {
                 $v = "";
                if (array_key_exists($ckey,$row)){ $v = $row[$ckey];}
                $v = $this->cleanHtml($v);
                array_push($rowitem,$v);
                
			 }
			 $nrow=null;
			 if($this->getFormat()=='csv'){
				$nrow = WriterEntityFactory::createRowFromArray($rowitem);
			}else{
				 $nrow = WriterEntityFactory::createRowFromArray($rowitem,$style);
			}
		
			 $this->writer->addRow($nrow);
		 }
		
	}
	//review has error
	function addrows($rows){
		
		$style= (new StyleBuilder())
        //->setShouldWrapText()
		->setFontSize(11)
		->build(); 

		foreach ($rows as $row) {
			$row=$this->cleanHtmlArray($row);
             $nrow = WriterEntityFactory::createRowFromArray($row,$style);
             $this->writer->addRow($nrow);
       }
		
	}
	function end(){	
		$this->writer->close();
	}
	function remove(){
		unlink($this->fullpath);
	}

	public function download() {
		$filename='BadiuReport_'.time().'.xlsx';
		 if($this->getFormat()=='csv'){$filename='BadiuReport_'.time().'.csv';}
		 $response = new Response();
		 $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		 if($this->getFormat()=='csv'){$response->headers->set('Content-Type', 'text/csv');}
	     $response->headers->set('Content-Disposition', 'attachment;filename='.$filename);
		 $response->headers->set('Cache-Control', 'max-age=0');
		 $response->sendHeaders();
		ob_end_clean();
		readfile($this->fullpath);  
		$this->remove();
 		 exit;
	}

	function makeTable($param){
		$maxrows= $this->getUtildata()->getVaueOfArray($param,'maxrows');
		$countrows= $this->getUtildata()->getVaueOfArray($param,'countrows');
		$countrowcurrentforceset=$this->getUtildata()->getVaueOfArray($param,'countrowcurrentforceset');
		$limit=$maxrows+1; //skip head
	//	$filePath = $this->fullpath;
		$reader = ReaderEntityFactory::createXLSXReader();
   		$reader->open($this->fullpath);

		$cont=0;
		$head=array();
		$rows=array();
    	foreach ($reader->getSheetIterator() as $sheet) {
			$rowcont=0;
        	foreach ($sheet->getRowIterator() as $row) {
				$columncount=0;
				$nerow=array();
			
				if($rowcont==0){
					foreach ($row->getCells() as $cell) {
						$key="key$columncount";
						$head[$key]=$cell->getValue();
						$columncount++;
					}
				}else{
					$cvalue = $row->toArray();
					$ih=0;
					foreach ($head as $k => $v) {
						$vl=null;
						if (array_key_exists($ih,$cvalue)){$vl=$cvalue[$ih];}
						$nerow[$k]=$vl;
						$ih++;
					}
					array_push($rows,$nerow) ;
				}
			
			
			$cont++;
			$rowcont++;
			if($rowcont >=$limit){break;}
		}
		if($rowcont >=$limit){break;}
	}
	$table=$this->getContainer()->get('badiu.system.core.lib.report.table');
	$table->getColumn()->setList($head);
	$table->getRow()->setList($rows);
	if($countrowcurrentforceset && ($rowcont < $countrows)){
		$table->setCountrowcurrentforceset(true);
		$table->setCountrowcurrent($countrows);
	}
	return $table;
}
public function attachToMail() {
	$filename='BadiuReport_'.time().'.xlsx';
 $attach= \Swift_Attachment::fromPath($this->fullpath)->setFilename($filename);
	return $attach;
 }
	public function cleanHtml($txt) {
        if(empty($txt)){return $txt;}
        $txt=str_replace("<br />","\n",$txt);
        $txt=str_replace("<br  />","\n",$txt);
        $txt=str_replace("<br>","\n",$txt);
       $txt= strip_tags($txt);
        return $txt;
	}
	public function cleanHtmlArray($row) {
        if(!is_array($row)){return $row;}
		$nrow=array();
		foreach($row as $k => $v) {
			$nrow[$k]=$this->cleanHtml($v);
		  }
        return $nrow;
    }
	public function getHashname() {
       return $this->hashname;
    }

    public function setHashname($hashname) {
        $this->hashname = $hashname;
	}
	
	public function geFullpath() {
        return $this->fullpath;
    }
	
	public function getFormat() {
       return $this->format;
    }

    public function setFormat($format) {
		if(empty($format)){$format='xls';}
        $this->format = $format;
	}
	
	public function getDeletefile() {
       return $this->deletefile;
    }

    public function setDeletefile($deletefile) {
		 $this->deletefile = $deletefile;
	}
}
