<?php

namespace Badiu\System\FileBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

class ReportTask  extends  TaskGeneralExec{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
				$this->cresult=array('datashouldexec' => 0, 'dataexec' => 0);
        } 

  public function import() {
    $result = array('datashouldexec' => 0, 'dataexec' => 0);
    
    return $result;
}

public function update() {
   $this->saveInFolder();
   return $this->cresult;
}
/**
operation: saveinfolder
filename:  nome of file can user expression CURRENT_DATE
key: key of report
filter - array parameter filter

*/
public function saveInFolder() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		$filename=$this->getUtildata()->getVaueOfArray($this->getParam(),'filename');
		$folderofdefaultpath=$this->getUtildata()->getVaueOfArray($this->getParam(),'folderofdefaultpath');
		$key=$this->getUtildata()->getVaueOfArray($this->getParam(),'_key');
		$filter=$this->getUtildata()->getVaueOfArray($this->getParam(),'filter');
		$dkey=$this->getUtildata()->getVaueOfArray($filter,'_dkey');
		$headaskey=$this->getUtildata()->getVaueOfArray($this->getParam(),'headaskey');
		
		if($operation!='saveinfolder'){return null;}
		if(empty($key)){return null;}
		
		
		$bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
		$kmbkey=$bkey;
        if(!empty($dkey)){
			$key=$dkey;
			$kmbkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
		}
	    if(is_array($filter)){
			$filter['entity']=$this->getTaskdto()->getEntity();
			
		}else{
			$filter=array();
			$filter['entity']=$this->getTaskdto()->getEntity();
		}
		
		if(!empty($filename)){$filename=$this->changeNemaExpression($filename);}

		$gerenralparamconfig=array('filemanagereportparam'=>array('hashname'=>$filename,'folderofdefaultpath'=>$folderofdefaultpath),'headaskey'=>$headaskey);
	
		$keymanger = $this->getContainer()->get('badiu.system.core.functionality.keymanger');
        $keymanger->setBaseKey($kmbkey);
		
	    $reportkey=$bkey.'.report';
		
		if(!$this->getContainer()->has($reportkey)){return null;}
		
		
		
		$report= $this->getContainer()->get($reportkey);
       
        $report->setKey($key);
		$report->setKeymanger($keymanger);
		  
		$report->setGerenralparamconfig($gerenralparamconfig);
		$layout='indexCrud';
		$type=null;
		
		$report->extractData($filter,$layout,$type);
		
		
				$message=null;
			   $resultmdl=array('datashouldexec' => 1, 'dataexec' => 1, 'message' => "");
			   $this->cresult = $this->addResult($this->cresult, $resultmdl); 
			  
		  return null;
	}

	private function changeNemaExpression($name) {
		$now=new \DateTime();
		$df=$now->format('d-m-Y');
		
		$name = str_replace('CURRENT_DATE',$df,$name);
		return $name;
	}	
}
?>