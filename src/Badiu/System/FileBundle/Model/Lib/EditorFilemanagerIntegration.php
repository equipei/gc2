<?php

namespace Badiu\System\FileBundle\Model\Lib;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EditorFilemanagerIntegration extends BadiuModelLib {
	private $filedata;
	function __construct(Container $container) {
		parent::__construct($container);
		$this->filedata=$this->getContainer()->get('badiu.system.file.file.data');
	}
	/**
	 * generate key and story in table. Return info to rename and path to save file 
	 * @return object 
	 */
	public function startKey() {
		$param=array();
		$param['name']=null;
		$param['ftype']=null;
		$param['fsize']=null;
		$param['fhost']='localhost';
		$param['dtype']='editorhtml';
		$param['authkey']=null;
		$param['modulekey']=null;
		$param['moduleinstance']=null;
		$param['entity']=$this->getEntity();
		return $param;
	}

	/**
	 * Update row with with file name and path. Info retorned in function makeKey is used to complate row table
	 *
	 * @return void
	 */
	public function updateKey() {

	}
	
}
