<?php

namespace Badiu\System\FileBundle\Model\Lib;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ManageUpload extends BadiuModelLib {

	function __construct(Container $container) {
		parent::__construct($container);
	}

	function single(){
		//return $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$filelib = $this->getContainer()->get('badiu.system.file.file.lib');
		$filedata = $this->getContainer()->get('badiu.system.file.file.data');
		$fdata=array();
		$fout=array();
		$tempid='BADIU_FILE_ID_PRESINSERT_'.$this->getEntity().'_'.time();
		$filecode=$filelib->generateAuthkey($tempid,50);

		$filename = $_FILES['file']['name'];
		$filetype = $_POST['filetype'];

		//$fdata['ftype']=$filetype;
		$fdata['name']=$filename;
		$fdata['fhost']='localhost';
		$fdata['authkey']=$filecode;
		
		$fout['name']=$filename;

		$pfaname=null;
		//$defaultpath =  $this->getContainer()->getParameter('badiu.system.file.defaultpath');
		//$basepath=$defaultpath.'/files/'.$this->getEntity();
		$basepath=$filelib->getBasePath();
		if (!file_exists($basepath)) {
			mkdir($basepath, 0777, true);
		}
		$filenameclean=$this->removeSpecialChar($filename);
		
		$filename=$filecode.'_'.$filenameclean;
		$location = $basepath.'/'.$filename;
		
	
		if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
			
		}
		$fdata['ftype']=mime_content_type($location);
		$fdata['fpath']=$location;
		$fdata['codename']=$filename;
		$fdata['fsize']=filesize($location);
		$fileid=$filelib->add($fdata);
		$newcode=$filelib->generateAuthkey($fileid);
		
		$pfaname=$newcode.'_'.$filenameclean;
		$newfilepath=$basepath.'/'.$pfaname;
		rename($basepath.'/'.$filename,$newfilepath);

		$fpath='default';
		$dconf=array();
		$dconf['keyname']=$newfilepath;
		$paramupd=array('id'=>$fileid,'authkey'=>$newcode,'fpath'=>$fpath,'codename'=>$pfaname);
		$filedata->updateNativeSql($paramupd,false);
		
		$fout['code']=$newcode;
		$fout['codename']=$pfaname;
		return $fout;
	}
    
	function removeSpecialChar($text) {
		$utf8 = array(
			'/[áàâãªä]/u'   =>   'a',
			'/[ÁÀÂÃÄ]/u'    =>   'A',
			'/[ÍÌÎÏ]/u'     =>   'I',
			'/[íìîï]/u'     =>   'i',
			'/[éèêë]/u'     =>   'e',
			'/[ÉÈÊË]/u'     =>   'E',
			'/[óòôõºö]/u'   =>   'o',
			'/[ÓÒÔÕÖ]/u'    =>   'O',
			'/[úùûü]/u'     =>   'u',
			'/[ÚÙÛÜ]/u'     =>   'U',
			'/ç/'           =>   'c',
			'/Ç/'           =>   'C',
			'/ñ/'           =>   'n',
			'/Ñ/'           =>   'N',
			'/–/'           =>   'a', // UTF-8 hyphen to "normal" hyphen
			'/_/'           =>   'a', 
			'/[’‘‹›‚]/u'    =>   'a', // Literally a single quote
			'/[“”«»„]/u'    =>   'a', // Double quote
			'/ /'           =>   'a', // nonbreaking space (equiv. to 0x160)
		); 
		$text= preg_replace(array_keys($utf8), array_values($utf8), $text);
		$text= preg_replace('/[^A-Za-z0-9. -]/', 'a', $text); 
		return $text;
 
	}
}
