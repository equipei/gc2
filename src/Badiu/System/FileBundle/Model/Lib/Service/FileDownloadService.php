<?php
namespace Badiu\System\FileBundle\Model\Lib\Service;
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
use Symfony\Component\HttpFoundation\Response;
class FileDownloadService extends BadiuExternalService
{

     private $authkey;
     private $url;
     private $info;

    public function __construct(Container $container)
    {
        parent::__construct($container);
		$this->response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
    }

   
   public function exec() {
		return $this->download();
	}
         public function download(){
             $this->authkey=$this->getContainer()->get('request')->get('authkey');
           
             $check=$this->checkAuthKey();
            
             if(!$check){return $this->getResponse()->get();}
             
              $data=$this->getContainer()->get('badiu.system.file.file.data');
            
              $this->info=$data->getInfoByAuthkey($this->authkey);
              
              $filelib=$this->getContainer()->get('badiu.system.file.file.lib');
              $check= $this->checkInfo();
               if(!$check)return $this->getResponse()->get();
             
              $this->url=$filelib->getUrl($this->info);
              
             $check= $this->checkUrl(); 
              if(!$check)return $this->getResponse()->get();
              
             //http://www.99bugs.com/controller-action-for-file-download-link-in-symfony2/
            // $file=$this->getFileContent();
            // print_r($this->info);exit;
             //  print_r($this->url);exit;
             $filename=$this->info['name'];
            $fileurl=$this->url;
            $minetype='application/pdf';
            //$minetype=mime_content_type($fileurl);
            $response = new Response();
            $response->headers->set('Content-Type',$minetype);
            $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);
            $response->headers->set('Content-length', filesize($fileurl));
            $response->setContent(file_get_contents($fileurl));
            $response->sendHeaders();
            $response->sendContent();
          
            exit;
              
         }
	
        public function checkAuthKey(){
            $authkey=$this->authkey;
          
            if(empty($authkey)){
                 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo('badiu.system.file.download.authkey.is.empty');
                 return false;
            }
            $exist=$this->getContainer()->get('badiu.system.file.file.data')->exitAuthkey($authkey);
          
            if(!$exist){
                 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo('badiu.system.file.download.authkey.is.not.valid');
                 return false;
            }
            
           
            return true;
        }
        
         public function checkInfo(){
           
            if(!isset($this->info['entity'])){
                 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo('badiu.system.file.download.entity.info.not.defined');
                 return false;
            }
            if(empty($this->info['entity'])){
                 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo('badiu.system.file.download.entity.is.empty');
                 return false;
            }
           
             if(!isset($this->info['authkey'])){
                 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo('badiu.system.file.download.authkey.info.not.defined');
                 return false;
            }
            if(empty($this->info['authkey'])){
                 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo('badiu.system.file.download.authkey.is.empty');
                 return false;
            }
            
               if(!isset($this->info['fpath'])){
                 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo('badiu.system.file.download.fpath.info.not.defined');
                 return false;
            }
            if(empty($this->info['fpath'])){
                 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo('badiu.system.file.download.fpath.is.empty');
                 return false;
            }
            
            if(!isset($this->info['modulekey'])){
                 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo('badiu.system.file.download.fpath.modulekey.not.defined');
                 return false;
            }
            if(empty($this->info['modulekey'])){
                 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo('badiu.system.file.download.modulekey.is.empty');
                 return false;
            }
            return true;
        }
        
          public function checkUrl(){
           
              $fileexist= file_exists ($this->url);
            if(!$fileexist){
                 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo('badiu.system.file.download.url.not.exist');
                  $this->getResponse()->setMessage($this->url);
                 return false;
            }
            return true;
          }
        function getAuthkey() {
            return $this->authkey;
        }

        function setAuthkey($authkey) {
            $this->authkey = $authkey;
        }

        function getUrl() {
            return $this->url;
        }

        function getInfo() {
            return $this->info;
        }

        function setUrl($url) {
            $this->url = $url;
        }

        function setInfo($info) {
            $this->info = $info;
        }


}
