<?php

namespace Badiu\System\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemFile
 *
 * @ORM\Table(name="system_file", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="system_file_authkey_uix", columns={"authkey"}),
 *      @ORM\UniqueConstraint(name="system_file_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="system_file_idnumber_uix", columns={"entity", "idnumber"}),
 *      @ORM\UniqueConstraint(name="system_file_codename_uix", columns={"codename"})},
 *       indexes={@ORM\Index(name="system_file_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="system_file_name_ix", columns={"name"}),
 *              @ORM\Index(name="system_file_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="system_file_authkey_ix", columns={"authkey"}),
*               @ORM\Index(name="system_file_fhost_ix", columns={"fhost"}),
 *              @ORM\Index(name="system_file_fpath_ix", columns={"fpath"}),
 *              @ORM\Index(name="system_file_fsize_ix", columns={"fsize"}),
 *              @ORM\Index(name="system_file_ftype_ix", columns={"ftype"}),
 *              @ORM\Index(name="system_file_shortname_ix", columns={"categoryid"}),
 *              @ORM\Index(name="system_file_codename_ix", columns={"codename"}),
 *              @ORM\Index(name="system_file_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="system_file_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class SystemFile {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

	/**
	 * @var SystemFileCategory
	 *
	 * @ORM\ManyToOne(targetEntity="SystemFileCategory")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
	 * })
	 */
	private $categoryid;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="codename", type="string", length=255, nullable=true)
	 */
	private $codename;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
	 */
	private $shortname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ftype", type="string", length=255, nullable=false)
	 */
	private $ftype = 'none';

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="fsize", type="bigint", nullable=false)
	 */
	private $fsize = 0;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="fhost", type="string", length=255, nullable=false)
	 */
	private $fhost; //localhost | amazons3 | xxxxxx(ip)
	/**
	 * @var string
	 *
	 * @ORM\Column(name="fpath", type="string", length=255, nullable=true)
	 */
	private $fpath;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="fversion", type="string", length=255, nullable=true)
	 */
	private $fversion;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype; //editorhtml | report | appfile

	        /**
	 * @var string
	 *
	 * @ORM\Column(name="dconfig", type="text", nullable=true)
	 */
	private $dconfig;

         /**
     * @var string
     *
     * @ORM\Column(name="authkey", type="string", length=255, nullable=false)
     */
        private $authkey;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;

	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted;

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getEntity() {
		return $this->entity;
	}

	/**
	 * @param int $entity
	 */
	public function setEntity($entity) {
		$this->entity = $entity;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getShortname() {
		return $this->shortname;
	}

	/**
	 * @param string $shortname
	 */
	public function setShortname($shortname) {
		$this->shortname = $shortname;
	}

	/**
	 * @return string
	 */
	public function getFtype() {
		return $this->ftype;
	}

	/**
	 * @param string $ftype
	 */
	public function setFtype($ftype) {

		$this->ftype = $ftype;
	}

	/**
	 * @return int
	 */
	public function getFsize() {
		return $this->fsize;
	}

	/**
	 * @param int $fsize
	 */
	public function setFsize($fsize) {
		$this->fsize = $fsize;
	}

	/**
	 * @return string
	 */
	public function getIdnumber() {
		return $this->idnumber;
	}

	/**
	 * @param string $idnumber
	 */
	public function setIdnumber($idnumber) {
		$this->idnumber = $idnumber;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getParam() {
		return $this->param;
	}

	/**
	 * @param string $param
	 */
	public function setParam($param) {
		$this->param = $param;
	}

	/**
	 * @return \DateTime
	 */
	public function getTimecreated() {
		return $this->timecreated;
	}

	/**
	 * @param \DateTime $timecreated
	 */
	public function setTimecreated($timecreated) {
		$this->timecreated = $timecreated;
	}

	/**
	 * @return \DateTime
	 */
	public function getTimemodified() {
		return $this->timemodified;
	}

	/**
	 * @param \DateTime $timemodified
	 */
	public function setTimemodified($timemodified) {
		$this->timemodified = $timemodified;
	}

	/**
	 * @return int
	 */
	public function getUseridadd() {
		return $this->useridadd;
	}

	/**
	 * @param int $useridadd
	 */
	public function setUseridadd($useridadd) {
		$this->useridadd = $useridadd;
	}

	/**
	 * @return int
	 */
	public function getUseridedit() {
		return $this->useridedit;
	}

	/**
	 * @param int $useridedit
	 */
	public function setUseridedit($useridedit) {
		$this->useridedit = $useridedit;
	}

	/**
	 * @return boolean
	 */
	public function isDeleted() {
		return $this->deleted;
	}

	/**
	 * @param boolean $deleted
	 */
	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}

	public function getCategoryid() {
		return $this->categoryid;
	}

	public function setCategoryid(SystemFileCategory $categoryid) {
		$this->categoryid = $categoryid;
	}

	function getFpath() {
		return $this->fpath;
	}
	function getFhost() {
		return $this->fhost;
	}
	
	function getFversion() {
		return $this->fversion;
	}

	function getDtype() {
		return $this->dtype;
	}

	function getModulekey() {
		return $this->modulekey;
	}

	function getModuleinstance() {
		return $this->moduleinstance;
	}
	
	
	function setFhost($fhost) {
		$this->fhost = $fhost;
	}
	function setFpath($fpath) {
		$this->fpath = $fpath;
	}

	function setFversion($fversion) {
		$this->fversion = $fversion;
	}

	function setDtype($dtype) {
		$this->dtype = $dtype;
	}

	function setModulekey($modulekey) {
		$this->modulekey = $modulekey;
	}

	function setModuleinstance($moduleinstance) {
		$this->moduleinstance = $moduleinstance;
	}

        function getAuthkey() {
            return $this->authkey;
        }

        function setAuthkey($authkey) {
            $this->authkey = $authkey;
        }

	 function getDconfig() {
            return $this->dconfig;
        }


		function setDconfig($dconfig) {
            $this->dconfig = $dconfig;
		}
		
		function getCodename() {
            return $this->codename;
        }


		function setCodename($codename) {
            $this->codename = $codename;
        }
}
