<?php

namespace Badiu\System\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuSystemFileBundle:Default:index.html.twig', array('name' => $name));
    }
}
