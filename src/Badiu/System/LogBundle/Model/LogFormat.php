<?php

namespace Badiu\System\LogBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class LogFormat extends BadiuFormat {

	function __construct(Container $container) {
		 parent::__construct($container);
	}

       public function sservuce($data) {
		  
            $param=$this->getUtildata()->getVaueOfArray($data,'param');
            $param=$this->getJson()->decode($param, true);
           
             $url=$this->getUtildata()->getVaueOfArray($param,'sserver.url',true);
            $name=$this->getUtildata()->getVaueOfArray($param,'sserver.name',true);
            
            $value="";
             $value="$name - $url";
           return $value;
        }
    public function ip($data) {
		  
            $param=$this->getUtildata()->getVaueOfArray($data,'param');
            $param=$this->getJson()->decode($param, true);
            $value=$this->getUtildata()->getVaueOfArray($param,'client.ip',true);
            return $value;
        }
     public function user($data) {
		  
            $param=$this->getUtildata()->getVaueOfArray($data,'param');
            $param=$this->getJson()->decode($param, true);
            $firstname=$this->getUtildata()->getVaueOfArray($param,'user.firstname',true);
            $lastname=$this->getUtildata()->getVaueOfArray($param,'user.lastname',true);
            $email=$this->getUtildata()->getVaueOfArray($param,'user.email',true);
            $value="$firstname $lastname $email";

            $accessby="";
            $accessbyusername=$this->getUtildata()->getVaueOfArray($param,'user.accessby.userfullname',true);
            $accessbyuseremail=$this->getUtildata()->getVaueOfArray($param,'user.accessby.useremail',true);
            if(!empty( $accessbyusername)){$accessby="<br><i>Acessado por  $accessbyusername $accessbyuseremail ";}
            $value.=$accessby;
            return $value;
        }
        
      public function description($data) {
	   $functionalitykey=$this->getUtildata()->getVaueOfArray($data,'functionalitykey');

            $value= $this->getTranslator()->trans($functionalitykey);
            return $value;
        }
      public function action($data) {
	    $action=$this->getUtildata()->getVaueOfArray($data,'action');

            if($action=='view'){
                 $value= $this->getTranslator()->trans('badiu.system.log.actionview');
            }else if($action=='create'){
                 $value= $this->getTranslator()->trans('badiu.system.log.actioncreate');
            }else if($action=='edit'){
                 $value= $this->getTranslator()->trans('badiu.system.log.actionedit');
            }else if($action=='delete'){
                 $value= $this->getTranslator()->trans('badiu.system.log.actiondelete');
            }else if($action=='remove'){
                 $value= $this->getTranslator()->trans('badiu.system.log.actionremove');
            }else{$value=$action;}
            
            $param=$this->getUtildata()->getVaueOfArray($data,'param');
            $param=$this->getJson()->decode($param, true);
            $format=$this->getUtildata()->getVaueOfArray($param,'query._format',true);
            if(!empty($format)){
                $format=$this->getTranslator()->trans('badiu.system.log.exportdata',array('%format%'=>$format));
                $value.="<br />$format";
            }
             $scheduler=$this->getUtildata()->getVaueOfArray($param,'scheduler');
             if(!empty($scheduler)){
                 $scheduler=$this->getTranslator()->trans($scheduler);
                 $value.="<br />$scheduler";
             }
            return $value;
        }
}
