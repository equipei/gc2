<?php

namespace Badiu\System\LogBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;


class Log extends BadiuModelLib
{
    
    function __construct(Container $container)
    {
        parent::__construct($container);
    }
    
    
    function add($param=array()) {
        $param['timecreated']=new \DateTime();
       
        $skipsession=$this->getUtildata()->getVaueOfArray($param,'_skipsession');

        //accessedby
        if($skipsession){
            if(!isset($param['entity'])){$param['entity']=  $this->getContainer()->getParameter('badiu.system.access.defaultentity');}
        }else{
           $badiuSession=$this->getContainer()->get('badiu.system.access.session');
            $badiuSession->setHashkey($this->getSessionhashkey());
            $dsession = $badiuSession->get();
            $accessby=$dsession->getAccessby();
            if(isset($accessby->userid) && !empty($accessby->userid)){ $param['useridaccessedby']=$accessby->userid;}
            $param=$this->addDefaultParam($param);
            if(!isset($param['entity'])){$param['entity']= $badiuSession->get()->getEntity();}
        }
       
       // if(!isset($param['userid'])){$param['userid']= $badiuSession->get()->getEntity();} //review access moodle
      
        $data= $this->getContainer()->get('badiu.system.log.log.data');
        if (array_key_exists('_skipsession',$param)){unset($param['_skipsession']);}
        $result=$data->insertNativeSql($param,false);
      
        return $result;
    }

   

    function addDefaultParam($logparam){
        if(isset($logparam['param'])){return $logparam;}

        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());   
        $param=array();
        $client=array('browser'=>$_SERVER['HTTP_USER_AGENT'],'ip'=>$_SERVER["REMOTE_ADDR"]);
        $param['client']=$client;
        $accessby=$badiuSession->get()->getAccessby();
        if(!empty($accessby)){$accessby=(array)$accessby;}
        $user=array('firstname'=>$badiuSession->get()->getUser()->getFirstname(),
                            'lastname'=>$badiuSession->get()->getUser()->getLastname(),
                            'id'=>$badiuSession->get()->getUser()->getId(),
                            'email'=>$badiuSession->get()->getUser()->getEmail(),
                            'roles'=>$badiuSession->get()->getRoles(),
                            'accessby'=>$accessby,
                        );
               $param['user']=$user;
             
              $query=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
              if(!isset($query['parentid']) && !empty($parentid)){$query['parentid']=$parentid;}
              $param['query']=$query; 
              $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
              $param=$json->encode($param);
              $logparam['param']= $param;
			  
			   if(!isset($logparam['functionalitykey'])){ 
					$currentroute = $this->getContainer()->get('request')->get('_route');
					 $dkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey');
					 if(!empty($dkey)){ $currentroute=$dkey;}
					$logparam['functionalitykey']=$currentroute;
			   }
			   
			 
            return $logparam;
            
    }
    
}
