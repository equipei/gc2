<?php

namespace Badiu\System\LogBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;


class CronLog 
{
    private $pathfile;
        /**
     * @var object
     */
    private $utildata = null;
        /**
     * @var Container
     */
    private $container;
    function __construct(Container $container)
    {
        $this->container = $container;
        $this->utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
    }
    
    
    function add($data,$param=array()) {
		return null;
        $ksession=$this->getUtildata()->getVaueOfArray($param,'ksession');
        $vsession=$this->getUtildata()->getVaueOfArray($param,'vsession');
        $self=$this->getUtildata()->getVaueOfArray($param,'self');
        if(!empty($ksession) && !empty($vsession) && !$self){
            $badiuSession=$this->getContainer()->get('badiu.system.access.session');
           $ckvalue= $badiuSession->getValue($ksession);
           //if($ckvalue!=$vsession){return null;}
        }
        $now=new \DateTime();
        $nowdate= $now->format('d/m/Y H:i:s.u');
        $tkey=" -- | -- | $nowdate | ";
        if(!$self){
            $badiuSession=$this->getContainer()->get('badiu.system.access.session');

            $gentity=$badiuSession->get()->getEntity();
            $sentity=$badiuSession->getEntity();
            
    
            $tkey="$gentity | $sentity | $nowdate | ";
        }
        
        $txt=null;
        if(is_string($data)){ $txt=$data; }
        else if(is_array($data)){
             $txt= json_encode($data);
          
        }
        if(empty($this->pathfile)){$this->setfileName();}
        $myfile = fopen($this->pathfile, "a") or die("Unable to open file!");
        
        $txt=$tkey.$txt;
        $txt.="\n";  
        fwrite($myfile, $txt);
        fclose($myfile);  
        return null;
    }

   

    function setfileName($file="log.txt"){
        $defaultpath =  $this->getContainer()->getParameter('badiu.system.file.defaultpath');
        $logfile="$defaultpath/$file";
        $this->pathfile=$logfile;
        return $logfile;
    }
    
    
    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }
    function getUtildata() {
        return $this->utildata;
    }

    function setUtildata($utildata) {
        $this->utildata = $utildata;
    }
}
