<?php

namespace Badiu\System\LogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuSystemLogBundle:Default:index.html.twig', array('name' => $name));
    }
}
