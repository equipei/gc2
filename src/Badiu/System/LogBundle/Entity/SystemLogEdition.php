<?php

namespace Badiu\System\LogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemLogEdition
 *
 * @ORM\Table(name="system_log_edition",
 *       indexes={@ORM\Index(name="system_log_entity_edition_ix", columns={"entity"}),
 *              @ORM\Index(name="system_log_edition_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="system_log_edition_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="system_log_edition_userid_ix", columns={"userid"})})
 * @ORM\Entity
 */
class SystemLogEdition {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

	/**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id", nullable=false)
     * })
     */
    private $userid; 
    
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;
        
        
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
    
	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;
    
    /**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

    
	function getId() {
		return $this->id;
	}

	function getEntity() {
		return $this->entity;
	}

	
    function getParam() {
		return $this->param;
	}

	function getTimecreated() {
		return $this->timecreated;
	}


	function setId($id) {
		$this->id = $id;
	}

	function setEntity($entity) {
		$this->entity = $entity;
	}

	
	function setModulekey($modulekey) {
		$this->modulekey = $modulekey;
	}

	function setModuleinstance($moduleinstance) {
		$this->moduleinstance = $moduleinstance;
	}


	function setParam($param) {
		$this->param = $param;
	}

	function setTimecreated(\DateTime $timecreated) {
		$this->timecreated = $timecreated;
	}

	

	function setTimemodified(\DateTime $timemodified) {
		$this->timemodified = $timemodified;
	}

	public function setUserid(\Badiu\System\UserBundle\Entity\SystemUser $userid) {
		$this->userid = $userid;
	}

	function getUserid() {
		return $this->userid;
	}

}
