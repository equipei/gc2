<?php

namespace Badiu\System\LogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Systemlog
 *
 * @ORM\Table(name="system_log",
 *       indexes={@ORM\Index(name="system_log_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="system_log_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="system_log_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="system_log_moduleinstance_ix", columns={"moduleinstance"}),
 *               @ORM\Index(name="system_log_action", columns={"action"}),
 *              @ORM\Index(name="system_log_ip", columns={"ip"}),
 *                @ORM\Index(name="system_log_url", columns={"url"}),
 *                @ORM\Index(name="system_log_urlreferer", columns={"urlreferer"}),
 *                @ORM\Index(name="system_log_timecreated", columns={"timecreated"}),
 *                @ORM\Index(name="system_log_timeexec", columns={"timeexec"}),
 *                @ORM\Index(name="system_log_functionalitykey", columns={"functionalitykey"}),
 *                @ORM\Index(name="system_log_useridaccessedby", columns={"useridaccessedby"}),
 *              @ORM\Index(name="system_log_userid_ix", columns={"userid"})})
 * @ORM\Entity
 */
class SystemLog {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="action", type="string", length=255, nullable=true)
	 */
	private $action;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ip", type="string", length=255, nullable=true)
	 */
	private $ip;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="url", type="string", length=255, nullable=true)
	 */
	private $url;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="urlreferer", type="string", length=255, nullable=true)
	 */
	private $urlreferer;

	/**
	 * @var \Badiu\System\UserBundle\Entity\SystemUser
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="userid", referencedColumnName="id")
	 * })
	 */
	private $userid;

	/**
	 * @var \Badiu\System\UserBundle\Entity\SystemUser
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="useridaccessedby", referencedColumnName="id")
	 * })
	 */
	private $useridaccessedby;
       /**
	 * @var integer
	 *
	 * @ORM\Column(name="targetid", type="bigint", nullable=true)
	 */
	private $targetid;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype='user'; //user|cron|auto|webservice

        /**
	 * @var string
	 *
	 * @ORM\Column(name="clientdevice", type="string", length=255, nullable=true)
	 */
	private $clientdevice; //pc | tablet | mobile
	/**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;
        
        
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;

        /**
	 * @var string
	 *
	 * @ORM\Column(name="functionalitykey", type="string", length=255, nullable=true)
	 */
	private $functionalitykey;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=255, nullable=true)
	 */
	private $description;

       
        /**
	 * @var string
	 *
	 * @ORM\Column(name="rkey", type="string", length=255, nullable=true)
	 */
	private $rkey;
        
	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;
        
        /**
	 * @var string
	 *
	 * @ORM\Column(name="dinfo", type="text", nullable=true)
	 */
	private $dinfo;
        
        /**
	 * @var string
	 *
	 * @ORM\Column(name="dnavegation", type="text", nullable=true)
	 */
	private $dnavegation;
        
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

    /**
     * @var integer
     *
     * @ORM\Column(name="timeexec", type="bigint", nullable=true)
     */
    private $timeexec;
    
	function getId() {
		return $this->id;
	}

	function getEntity() {
		return $this->entity;
	}

	function getDtype() {
		return $this->dtype;
	}

	function getModulekey() {
		return $this->modulekey;
	}

	function getModuleinstance() {
		return $this->moduleinstance;
	}

	
        function getParam() {
		return $this->param;
	}

	function getTimecreated() {
		return $this->timecreated;
	}


	function setId($id) {
		$this->id = $id;
	}

	function setEntity($entity) {
		$this->entity = $entity;
	}

	function setDtype($dtype) {
		$this->dtype = $dtype;
	}

	function setModulekey($modulekey) {
		$this->modulekey = $modulekey;
	}

	function setModuleinstance($moduleinstance) {
		$this->moduleinstance = $moduleinstance;
	}


	function setParam($param) {
		$this->param = $param;
	}

	function setTimecreated(\DateTime $timecreated) {
		$this->timecreated = $timecreated;
	}

	

	function setTimemodified(\DateTime $timemodified) {
		$this->timemodified = $timemodified;
	}

	public function setUserid(\Badiu\System\UserBundle\Entity\SystemUser $userid) {
		$this->userid = $userid;
	}

	function getUserid() {
		return $this->userid;
	}

	function getAction() {
		return $this->action;
	}

	function getIp() {
		return $this->ip;
	}

	function getUrl() {
		return $this->url;
	}

	function getUrlreferer() {
		return $this->urlreferer;
	}

	function setAction($action) {
		$this->action = $action;
	}

	function setIp($ip) {
		$this->ip = $ip;
	}

	function setUrl($url) {
		$this->url = $url;
	}

	function setUrlreferer($urlreferer) {
		$this->urlreferer = $urlreferer;
	}

        function getClientdevice() {
            return $this->clientdevice;
        }

        function getDescription() {
            return $this->description;
        }

        function setClientdevice($clientdevice) {
            $this->clientdevice = $clientdevice;
        }

        function setDescription($description) {
            $this->description = $description;
        }

        function getDinfo() {
            return $this->dinfo;
        }

        function getDnavegation() {
            return $this->dnavegation;
        }

        function setDinfo($dinfo) {
            $this->dinfo = $dinfo;
        }

        function setDnavegation($dnavegation) {
            $this->dnavegation = $dnavegation;
        }

        function getTargetid() {
            return $this->targetid;
        }

        function setTargetid($targetid) {
            $this->targetid = $targetid;
        }

        function getRkey() {
            return $this->rkey;
        }

        function setRkey($rkey) {
            $this->rkey = $rkey;
        }

        function getTimeexec() {
            return $this->timeexec;
        }

        function setTimeexec($timeexec) {
            $this->timeexec = $timeexec;
        }

        function getFunctionalitykey() {
            return $this->functionalitykey;
        }

        function setFunctionalitykey($functionalitykey) {
            $this->functionalitykey = $functionalitykey;
        }

		function getUseridaccessedby() {
            return $this->useridaccessedby;
        }

        function setUseridaccessedby($useridaccessedby) {
            $this->useridaccessedby = $useridaccessedby;
        }

}
