<?php
$command = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('command');

$deletecachecurrenturl=$utilapp->getUrlByRoute('badiu.system.core.command.dashboard',array('msgservicecommandexec'=>1,'command'=>'deletecache'));
$servicedeletecacheurlparam= array('_function'=>'requestCommand','_operation'=>'deletecache','_urlgoback'=>urlencode($deletecachecurrenturl));
$servicedeletecacheurl=$utilapp->getUrlByRoute('badiu.system.core.update.link',$servicedeletecacheurlparam);

$deleteuserssessionscurrenturl=$utilapp->getUrlByRoute('badiu.system.core.command.dashboard',array('msgservicecommandexec'=>1,'command'=>'deleteuserssessions'));
$servicedeleteuserssessionsurlparam= array('_function'=>'requestCommand','_operation'=>'deleteuserssessions','_urlgoback'=>urlencode($deleteuserssessionscurrenturl));
$servicedeleteuserssessionsurl=$utilapp->getUrlByRoute('badiu.system.core.update.link',$servicedeleteuserssessionsurlparam);

$badiuSession=$container->get('badiu.system.access.session');
$isrootuser=$badiuSession->isRootUser();
$responsecommand=badiu_system_core_command_exec($container,$page);

function badiu_system_core_command_exec($container,$page){
	$command = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('command');
	$appservercommand=$container->get('badiu.system.core.lib.appservercommand');
	$translator = $page->getTranslator();
	$result="";
	if($command=='deletecache'){ 
		$result=$appservercommand->execCommand($command);
		//$msgservicecommandexec = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('msgservicecommandexec');
		// if($msgservicecommandexec){$result=$translator->trans('badiu.system.core.command.clearcacheresult');}
	}else if($command=='updatedb'){
		$result=$appservercommand->execCommand($command);
	}else if($command=='updatesystemaappcache'){
		$msg=$appservercommand->execCommand($command);
		$result=$translator->trans('badiu.system.core.command.systemappcacheupdatedbresult',array('%msg%'=>$msg));
	}else if($command=='updateentityappcache'){
		$msg=$appservercommand->execCommand($command);
		$result=$translator->trans('badiu.system.core.command.entityappcacheupdatedbresult',array('%msg%'=>$msg));
	}
	else if($command=='updatebundle'){
		$bundle= $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('bundle');
		$result=$appservercommand->execCommand($command,array('bundle'=>$bundle));
	}
	else if($command=='phpinfo'){
		echo "<h2>PHP INFO</h2>";
		ob_start();
		phpinfo();
		$phpinfo = ob_get_clean();
		
		// Adiciona o prefixo a cada seletor de CSS
		$phpinfo = preg_replace('#(body,? .+?)\{#', '#phpinfoContainer $1 {', $phpinfo);
		
		$phpinfo = preg_replace_callback('/(<style type="text\/css">)(.*?)(<\/style>)/s',
			function ($matches) {
			// Adiciona '#phpinfoContainer ' no início de cada linha
			return $matches[1] . preg_replace('/(?<=^|\n)(\s*)/', '$1#phpinfoContainer ', $matches[2]) . $matches[3];
		},
		$phpinfo
		);

		?>

		<div id="phpinfoContainer">
			<?php echo $phpinfo; ?>
		</div>
		<?php
	}
	else{	
	
		$result=$appservercommand->execCommand($command);
	}	
	if(!empty($result)){$result="<div class=\"alert alert-success\">$result</div>";}
	return $result;
}

function badiu_system_core_command_islinkactive($linkcommand, $command){
	if($linkcommand==$command){return " active ";}

	return "";
}
 $bundles = $container->getParameter('kernel.bundles');

?>

<div id="_badiu_theme_core_dashboard_vuejs">
<?php echo $responsecommand;?>

<h4><?php echo $translator->trans('badiu.system.core.command.generalcommand');?></h4><hr /> 

 <div class="list-group">
	<?php if($isrootuser){?><a href="<?php echo $servicedeletecacheurl;?>" class="list-group-item <?php echo badiu_system_core_command_islinkactive('deletecache', $command);?>"><?php echo $translator->trans('badiu.system.core.command.clearcache');?></a><?php }?>
	<?php if($isrootuser){?><a href="?command=updatedb" class="list-group-item <?php echo badiu_system_core_command_islinkactive('updatedb', $command);?>"><?php echo $translator->trans('badiu.system.core.command.updatedb');?></a><?php }?>
	<?php if($isrootuser){?><a href="?command=updatesystemaappcache" class="list-group-item <?php echo badiu_system_core_command_islinkactive('updatesystemaappcache', $command);?>"><?php echo $translator->trans('badiu.system.core.command.updatesystemaappcache');?></a><?php }?>
    <a href="?command=updateentityappcache" class="list-group-item <?php echo badiu_system_core_command_islinkactive('updateentityappcache', $command);?>"><?php echo $translator->trans('badiu.system.core.command.updateentityappcache');?></a>
	<?php if($isrootuser){?><a href="?command=deleteuserssessions" class="list-group-item <?php echo badiu_system_core_command_islinkactive('deleteuserssessions', $command);?>"><?php echo $translator->trans('badiu.system.core.command.deleteuserssessions');?></a><?php }?>
	<?php if($isrootuser){?><a href="?command=importalllanguaguetodb" class="list-group-item <?php echo badiu_system_core_command_islinkactive('importalllanguaguetodb', $command);?>"><?php echo $translator->trans('badiu.system.module.translator.importalllanguaguetodb');?></a><?php }?>
	<?php if($isrootuser){?><a href="?command=makegloballanguaguecache" class="list-group-item <?php echo badiu_system_core_command_islinkactive('makegloballanguaguecache', $command);?>"><?php echo $translator->trans('badiu.system.module.translator.makegloballanguaguecache');?></a><?php }?>
	<a href="?command=makeentitylanguaguecache" class="list-group-item <?php echo badiu_system_core_command_islinkactive('makeentitylanguaguecache', $command);?>"><?php echo $translator->trans('badiu.system.module.translator.makeentitylanguaguecache');?></a>
	<a href="?command=phpinfo" class="list-group-item <?php echo badiu_system_core_command_islinkactive('phpinfo', $command);?>"><?php echo $translator->trans('badiu.system.core.command.phpinfo');?></a>
	 
  </div>
<br />	
<?php if($isrootuser){?>
<h4><?php echo $translator->trans('badiu.system.core.command.localmodules');?></h4><hr /> 

<table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th><?php echo $translator->trans('badiu.system.core.command.moduleseq');?></th>
		<th><?php echo $translator->trans('badiu.system.core.command.module');?></th>
        <th><?php echo $translator->trans('badiu.system.core.command.moduledirpath');?></th>
		<th><?php echo $translator->trans('badiu.system.core.command.moduleoperation');?> </th>
       
      </tr>
    </thead>
    <tbody >
	<?php 
	$cont=0;
	foreach ($bundles as $key => $value) {
		$link="?command=updatebundle&bundle=$key";
		$pos=stripos($key, "BadiuLocal");
        if($pos!== false){
		$cont++;
	?>
      <tr>
	  <td><?php echo $cont; ?></td>
		<td><?php echo $key; ?></td>
        <td><?php echo $value; ?></td>
        <td><a href="<?php echo $link;?>"><?php echo $translator->trans('badiu.system.core.command.moduleupdate');?></a></td>
      </tr>
		<?php }};?>
     
    </tbody>
  </table>
  
  <br />
<h4><?php echo $translator->trans('badiu.system.core.command.allmodules');?></h4><hr /> 
   <input class="form-control" id="badiunetllistallmaudles" type="text" placeholder="<?php echo $translator->trans('badiu.system.core.command.modulessearch.placeholder');?>">
  <br>
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th><?php echo $translator->trans('badiu.system.core.command.moduleseq');?></th>
		<th><?php echo $translator->trans('badiu.system.core.command.module');?></th>
        <th><?php echo $translator->trans('badiu.system.core.command.moduledirpath');?></th>
		<th><?php echo $translator->trans('badiu.system.core.command.moduleoperation');?> </th>
       
      </tr>
    </thead>
    <tbody id="badiunetllistallmaudlesbodytbl">
	<?php 
	$cont=0;
	foreach ($bundles as $key => $value) {
		$link="?command=updatebundle&bundle=$key";
		$cont++;
	?>
      <tr>
	  <td><?php echo $cont; ?></td>
		<td><?php echo $key; ?></td>
        <td><?php echo $value; ?></td>
        <td><a href="<?php echo $link;?>"><?php echo $translator->trans('badiu.system.core.command.moduleupdate');?></a></td>
      </tr>
	<?php };?>
     
    </tbody>
  </table>
 
 <?php }?> <!-- end isrootuser -->


<script>
$(document).ready(function(){
  $("#badiunetllistallmaudles").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#badiunetllistallmaudlesbodytbl tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
</div>