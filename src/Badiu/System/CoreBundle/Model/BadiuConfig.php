<?php

namespace Badiu\System\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;

class BadiuConfig  {
    
     /**
     * @var Container
     */
    private $container;
    
    
     /**
     * @var EntityManager
     */
    private $em; 
    
     /**
     * @var string
     */
    private $bundle; 
     
     function __construct(Container $container,$bundle=null) {
                $this->container=$container;
                $db=$this->container->get('badiu.system.access.session')->get()->getDbapp(); 
                $this->em=$this->container->get('doctrine')->getEntityManager($db); 
                $this->bundle=$bundle;
               
      }
     
      
    public function getDefaulFormConfig($param=array('importclient'=>true,'importcustom'=>true,'importextended'=>true)){  
        $kernel = $this->container->get('kernel');
        $fileconf=$kernel->locateResource('@'.$this->bundle).'/Resources/config/config.yml';

       // echo "xxx".$this->bundle;exit;
        if(!file_exists($fileconf)){
           return array(); 
        }
        $file= $kernel->locateResource('@'.$this->bundle.'/Resources/config/config.yml');
        $importclient=false;
        $importcustom=false;
        $importextended=false;
        if(isset($param['importclient'])){ $importclient=$param['importclient'];}
        if(isset($param['importcustom'])){ $importcustom=$param['importcustom'];}
        if(isset($param['importextended'])){ $importextended=$param['importextended'];}
       // $yaml = new Parser();
      //  $param=  $yaml->parse(file_get_contents($file));
        $param= Yaml::parse($file);
        $param= $this->getImported($param);
       if($importextended){$param= $this->getExtended($param);}
		if($importclient){
                    $param= $this->getClientConfig($param);
                    $param= $this->getImported($param);
               }
       if($importcustom){$param= $this->getCustomConfig($param);}
     
        return $param;
        
    }

	
    public function getImported($dconfig){
        if (is_array($dconfig) && array_key_exists('imports',$dconfig)){
            $import=$dconfig['imports'];
            foreach ($import as $imp) {
                $kernel = $this->container->get('kernel');
               $fconfig=$imp['resource'];
                $file=$kernel->locateResource('@'.$this->bundle).'/Resources/config/'.$fconfig;
                if(file_exists($file)){
                    $param= Yaml::parse($file);
                    if(!empty($param) && is_array($param)){
                        $dconfig=array_merge($dconfig,$param);
                    }

                }

            }


        }

        return $dconfig;
    }

	 public function getCustomConfig($dconfig){
              $badiuSession=$this->container->get('badiu.system.access.session');
              $entity=$badiuSession->get()->getEntity();
              
               $kernel = $this->container->get('kernel');
               $dir=$kernel->getRootDir();
               
               $file=$dir."/badiudata/config/config.yml";
               $fileentity=$dir."/badiudata/config/$entity-config.yml";
		if(file_exists($fileentity)){$file=$fileentity;}
               if(file_exists($file)){
                    $param= Yaml::parse($file);
					foreach ($param as $k => $v) {
						$dconfig[$k]=$v;
					}
                }


        

        return $dconfig;
    }
    public function getExtended($dconfig){
           $bundleExtends=null;
            foreach ($dconfig as $key => $value) {
                $pos = stripos($key, '.extends.bundle.name');
                if ($pos !== false) {
                    $bundleExtends= $dconfig[$key];
                    if(!empty($bundleExtends)) {
                        $kernel = $this->container->get('kernel');
                        $file=$kernel->locateResource('@'.$bundleExtends).'Resources/config/config.yml';

                        if(file_exists($file)){
                            $param= Yaml::parse($file);
                            if(!empty($param)){
                                $dconfig=array_merge($dconfig,$param);
                            }

                        }
                    }

                }
            }


        return $dconfig;
    }
	
	public function getClientConfig($dconfig){
        $kernel = $this->container->get('kernel');
        $fileconf=$kernel->locateResource('@'.$this->bundle).'/Resources/config/client.yml';

        if(!file_exists($fileconf)){
           return $dconfig;
        }
        $file= $kernel->locateResource('@'.$this->bundle.'/Resources/config/client.yml');

        $param= Yaml::parse($file);
		foreach ($param as $k => $v) {
			$dconfig[$k]=$v;
		}
        return $dconfig;
        
    }
     public function getBundle() {
         return $this->bundle;
     }
     public function setBundle($bundle) {
         $this->bundle = $bundle;
     }
}
