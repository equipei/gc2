<?php

namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class EmailTemplateFormat extends BadiuFormat{
      private $router;
     function __construct(Container $container) {
            parent::__construct($container);
             $this->router=$this->getContainer()->get("router");
       } 

   
    public  function subject($data){
           $value=null;
           $value= $this->getUtildata()->getVaueOfArray($data, 'value');
           
            $emailconf=$this->getJson()->decode($value,true);
            $value=$this->getUtildata()->getVaueOfArray($emailconf, 'subject');
            return $value; 
    } 
 public  function message($data){
           $value=null;
           $value= $this->getUtildata()->getVaueOfArray($data, 'value');
           $emailconf=$this->getJson()->decode($value,true);
           $value=$this->getUtildata()->getVaueOfArray($emailconf, 'message');
       
        return $value; 
    } 

    public  function name($data){
      $value=$this->getUtildata()->getVaueOfArray($data,'name');
      $value=$this->getTranslator()->trans($value);
      return $value; 
} 
}
