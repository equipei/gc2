<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Format;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**
 * Description of MakeSelect
 *
 * @author lino
 */
class ConfigFormat  extends BadiuModelLib{

    /**
     * @var Container
     */
    private $container;


    private $type;
    private $value;
    private $column;
    private $column1;
    private $key;
    private $row;
    private $function;
/**
     * @var Hour
     */
    private $hour;
    function __construct(Container $container) {
        parent::__construct($container);
        $this->type = null;
        $this->value = null;
        $this->function = null;
         $this->hour =$this->getContainer()->get('badiu.system.core.lib.date.hour');
    }

    public function init($paramconfig) {
        $pos = stripos($paramconfig, "|");
        if ($pos !== false) {
            $p = explode("|", $paramconfig);
            if (isset($p[0])) {
                $this->type = $p[0];
            }
            if (isset($p[1])) {
                $this->value = $p[1];
            }
            if (isset($p[2])) {
                $this->function = $p[2];
            }
            
          $pos = strpos($this->value, "/");
          if($pos!==false){
            $p=explode("/",$this->value);
            if(isset($p[0])){$this->value=$p[0];}
            if(isset($p[1])){$this->column=$p[1];}
            if(isset($p[2])){$this->column1=$p[2];}
        }
        }
         
    }
    public function clear() {
         $this->type = null;
         $this->value = null;
         $this->function = null;
    }
    public function get($key,$paramconfig, $value, $row = null) {
        $this->key=$key;
        $this->row=$row;
        $newvalue = null;
        $this->init($paramconfig);
        if ($this->type == 's' && $row != null) {
            $newvalue = $this->serviceType($row, $value);
        } else if ($this->type == 'd') {
            $newvalue = $this->defaultType($value);
        }else{
            $newvalue = $this->generalType($value);
        }
        $this->clear();
        return $newvalue;
    }

    public function dbrow($paramconfig, $viewconfig, $row) {
       
        if (empty($paramconfig) && empty($viewconfig)) {
            return $row;
        }
        //param config
        $queryString = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
        $queryString->setQuery($paramconfig);
        $queryString->makeParam();
        $paramconfig = $queryString->getParam();
        
        //view config
        $listview = null; 
        $hasgroup=false;
        if (!empty($viewconfig)) {
            $gpos = stripos($viewconfig, "=");
            if ($gpos !==false ) {$hasgroup=true;}
            $listview = array();
            if($hasgroup){
                $gqueryString = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
                $gqueryString->setQuery($viewconfig);
                $gqueryString->makeParam();
                $gparam=$gqueryString->getParam();
                
                foreach ($gparam as $gkey => $gvalue) {
                  $livalues=array();
                  $igpos = stripos($gvalue, ",");
                  if ($igpos !==false ) {
                    $livalues = explode(",", $gvalue);
                    foreach ( $livalues as $lv) {array_push($listview,$lv);}
                  }else{array_push($listview,$gvalue);} 
               
                }
               
            }else{
                $listview = array();
                $pos = stripos($viewconfig, ",");
                if ($pos !==false ) {
                    $listview = explode(",", $viewconfig);
                   
                } else {
                    array_push($listview, $viewconfig);
                }
                
               //add key not in list
               if(!empty($row) && is_array($row)){
                    foreach ($row as $key => $value) {
                        array_push($listview, $key);
                    }
                }

            }
           //add key not in list
           if(!empty($row) && is_array($row)){
                foreach ($row as $key => $value) {
                    array_push($listview, $key);
                }
            }
        }


        $nerow = array();

        //loop for list view
        if (!empty($listview) && is_array($listview)) {
            
            foreach ($listview as $key) {
               $value=null;
                if (is_array($row) && array_key_exists($key,$row)){$value=$row[$key];}
                $colmunparam = null;
                if (isset($paramconfig[$key])) {
                    $colmunparam = $paramconfig[$key];
                }
                $newvalue = $value;
                if (!empty($colmunparam)) {
                    $this->init($colmunparam);
                    if ($this->type == 's') {
                        $newvalue = $this->serviceType($row, $value);
                    } else if ($this->type == 'd') {
                        $newvalue = $this->defaultType($value);
                    }
                }
                 $this->clear();
                $nerow[$key] = $newvalue;
            }
             return $nerow;
        }
        //loop for row if listview is null
		if(is_array($row)){
          foreach ($row as $key => $value) {

            $colmunparam = null;
            if (isset($paramconfig[$key])) {
                $colmunparam = $paramconfig[$key];
            }
            $newvalue = $value;
            if (!empty($colmunparam)) {
                $this->init($colmunparam);
                if ($this->type == 's') {
                    $newvalue = $this->serviceType($row, $value);
                } else if ($this->type == 'd') {
                    $newvalue = $this->defaultType($value);
                }
            }
            $this->clear();
            $nerow[$key] = $newvalue;
        }
	   }
        return $nerow;
    }

    public function serviceType($row, $value) {
        $data = $value;
        if ($this->getContainer()->has($this->value)) {
            $fservice = $this->getContainer()->get($this->value);
            $function = $this->function; 
			$fservice->setSystemdata($this->getSystemdata());
            if (method_exists($fservice, 'setSessionhashkey')) {
                $data = $fservice->setSessionhashkey($this->getSessionhashkey());
            }
            if (method_exists($fservice, $function)) {
                $data = $fservice->$function($row);
            }
			
        }
        return $data;
    }

    public function defaultBoolean($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
     
        if ($value) {
           
            $data = $this->getTranslator()->trans('yes');
        } else  if ($value===false){
            $data = $this->getTranslator()->trans('no');
        } else  if ($value===0){
            $data = $this->getTranslator()->trans('no');
        }else{$data="";}
        return $data;
    }

    public function defaultDate($value) {
      
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
       $f=$this->getDateFormatLayout('date');
        if (is_object($data) && is_a($data, 'DateTime')) {
			$y = $data->format('Y');
			if($y < 1 ){return null;}
            $data = $data->format($f);
        }else  if (is_object($data) && is_a($data, 'MongoDate')) {
            $data= date($f, $data->sec);
        }
        else  if (is_object($data) && is_a($data, 'MongoDB\BSON\UTCDateTime')) {
            $data=$data->toDateTime();
            $data = $data->format($f);
        }
        return $data;
    }

    public function defaultDateFull($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
        $weekday=null; 
        $day=null; 
        $month= null;
        $year= null;
       
        if (is_object($data) && is_a($data, 'DateTime')) {
            $y = $data->format('Y');
			if($y < 1 ){return null;}
			
            $weekday=$data->format('w');
            $day= $data->format('d');
            $month= $data->format('m');
            $year=  $data->format('Y');
        }
        else  if (is_object($data) && is_a($data, 'MongoDB\BSON\UTCDateTime')) {
            $data=$data->toDateTime();
           
            $weekday=$data->format('w');
            $day= $data->format('d');
            $month= $data->format('m');
            $year=  $data->format('Y');
        }
        if(empty($data)){return null;}
        $fpdataoptions= $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
         $dayofweeklabel=$fpdataoptions->getDayOfWeekLabel($weekday);
         $month=$fpdataoptions->getMonthLabel($month);
         $data=$this->getTranslator()->trans('badiu.system.time.format.full',array('%dayofweek%'=>$dayofweeklabel,'%dayofmonth%'=>$day,'%month%'=>$month,'%year%'=>$year));
       return $data;
    }
    public function defaultDateTimestamp($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
		$f=$this->getDateFormatLayout('date');
        if($data >0 ){
            $data=date($f,$data);
        }else {$data=null;}
        return $data;
    }
    public function defaultDateTimeTimestamp($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
         if($data >0 ){
			  $f=$this->getDateFormatLayout('datetime');
           // date_default_timezone_set('UTC');
		   //$data=$data-3600;
             $newdate=new \DateTime();
             $newdate->setTimestamp($data);
            $data=$newdate->format($f);
        }else {$data=null;}
        return $data;
    }
	
	public function defaultTimeTimestamp($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
         if($data >0 ){
           // date_default_timezone_set('UTC');
		  // $data=$data-3600;
             $newdate=new \DateTime();
             $newdate->setTimestamp($data);
            $data=$newdate->format('H:i:s');
        }else {$data=null;}
        return $data;
    }
    public function defaultDateTime($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
		 $f=$this->getDateFormatLayout('datetime');
      
        if (is_object($data) && is_a($data, 'DateTime')) {
			$y= $data->format('Y'); 
			if($y=="-0001"){ return null;}
            $data = $data->format($f);
        }else  if (is_object($data) && is_a($data, 'MongoDate')) {
            $data= date($f, $data->sec);
        }else  if (is_object($data) && is_a($data, 'MongoDB\BSON\UTCDateTime')) {
            $data=$data->toDateTime();
            $data = $data->format($f);
        }
        return $data;
    }
    public function defaultDateTimeForm($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
     //   echo "<hr>";
     // print_r($data);exit;  
        if (is_object($data) && is_a($data, 'DateTime')) {
			$y = $data->format('Y');
			if($y < 1 ){return null;}
            $data = $data->format('Y-m-d\TH:i:s\Z');
        }else  if (is_object($data) && is_a($data, 'MongoDate')) {
            $data= date('Y-m-d\TH:i:s\Z', $data->sec);
        }else  if (is_object($data) && is_a($data, 'MongoDB\BSON\UTCDateTime')) {
            $data=$data->toDateTime();
            $data = $data->format('Y-m-d\TH:i:s\Z');
        }
        return $data;
    }
     public function defaultDateTimeHour($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
        if (is_object($data) && is_a($data, 'DateTime')) {
			$y = $data->format('Y');
			if($y < 1 ){return null;}
            $data = $data->format('H');
        }else  if (is_object($data) && is_a($data, 'MongoDate')) {
            $data= date('H', $data->sec);
        }else  if (is_object($data) && is_a($data, 'MongoDB\BSON\UTCDateTime')) {
            $data=$data->toDateTime();
             $data = $data->format('H');
        }
        return $data;
    }
   public function defaultDateTimeHourMinute($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
        if (is_object($data) && is_a($data, 'DateTime')) {
			$y = $data->format('Y');
			if($y < 1 ){return null;}
            $data = $data->format('H:i');
        }else  if (is_object($data) && is_a($data, 'MongoDate')) {
            $data= date('H:i', $data->sec);
        }else  if (is_object($data) && is_a($data, 'MongoDB\BSON\UTCDateTime')) {
            $data=$data->toDateTime();
             $data = $data->format('H:i');
        }
        return $data;
    }
      public function defaultDateTimeHourMinuteSecond($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
        if (is_object($data) && is_a($data, 'DateTime')) {
			$y = $data->format('Y');
			if($y < 1 ){return null;}
            $data = $data->format('H:i:s');
        }else  if (is_object($data) && is_a($data, 'MongoDate')) {
            $data= date('H:i:s', $data->sec);
        }else  if (is_object($data) && is_a($data, 'MongoDB\BSON\UTCDateTime')) {
            $data=$data->toDateTime();
            $data = $data->format('H:i:s');
        }
        return $data;
    }
	
	public function dateCountMinutePast($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
		$now=new \DateTime();
		$now=$now->getTimestamp();
        if (is_object($data) && is_a($data, 'DateTime')) {
			$y = $data->format('Y');
			if($y < 1 ){return null;}
            $data= $data->getTimestamp();
        }else  if (is_object($data) && is_a($data, 'MongoDate')) {
            $data= $data->getTimestamp();
        }else  if (is_object($data) && is_a($data, 'MongoDB\BSON\UTCDateTime')) {
            $data=$data->toDateTime();
             $data= $data->getTimestamp();
        }
		$result=0;
		
		if($data > 0 && $now > 0 ){
			$result=$now-$data;
			if($result> 0){$result=$result/60;}
		}
		return $result;
    }
	
    public function defaulfNumberLong($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
          if($data===null || $data===""){return "";}
        $data = number_format($value, 0, ',', '.');
        return $data;
    }

    public function defaulfNumberDouble($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
       $data = $value;
       if($data===null || $data===""){return "";}
       $data = number_format($value, 2, ',', '.');
        return $data;
    }
	public function defaultCastDouble($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
       $data = $value;
       if($data===null || $data===""){return "";}
       $data = str_replace(",",".",$data);
        return $data;
    }
	
	public function defaulfNumberDoublePercentageSymbol($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
       $data = $value;
       if($data===null || $data===""){return "";}
       $data = number_format($value, 2, ',', '.');
        return $data."%";
    }
     public function defaulfMoney($value) {
       if ($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
      
       $data = $value;
        if($data===null || $data===""){return "";}
       
     //  setlocale(LC_MONETARY, 'pt_BR'); //review for dinamyc config
     //  $data = money_format('%n',$value);
        $data='R$ '. number_format($value, 2, ',', '.');
     
        return $data;
    }
	
	public function defaultHourRound($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $fhour = "";
        if (empty($value)) {
            return $fhour;
        }
        $hour = $value / 60;
        $hour = floor($hour);
        $minutes = $value % 60;
        if($minutes > 30){$hour=$hour+1;}
       
        return $hour;
    }
	public function textHourRound($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $fhour = "";
        if (empty($value)) {
            return $fhour;
        }
        $hour = $value / 60;
        $hour = floor($hour);
        $minutes = $value % 60;
        if($minutes > 30){$hour=$hour+1;}
       
        return "$hour h";
    }
    public function defaultHour($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $fhour = "";
        if (empty($value)) {
            return $fhour;
        }
        $hour = $value / 60;
        $hour = floor($hour);
        if($hour < 10){$hour="0$hour";}
        $minutes = $value % 60;
        if($minutes < 10){$minutes="0$minutes";}
       
        $fhour="$hour:$minutes";
       
  
        return $fhour;
    }
    public function textHour($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $fhour = "";
        if (empty($value)) {
            return $fhour;
        }
        $hour = $value / 60;
        $hour = floor($hour);
        $minutes = $value % 60;
        if (!empty($hour)) {
            $fhour = "$hour h";
        }
        if (!empty($minutes)) {
            $fhour.=" e $minutes min ";
        }

        return $fhour;
    }
	
	 public function textHourDay($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $fhour = "";
        if (empty($value)) {
            return $fhour;
        }
		if($value >=86400){
			$day= $value / 86400;
			$day = floor($day);
			$hour=0;
			$rhour = $value % 86400;
			if($rhour >= 3600){$hour=$rhour /3600;$hour= floor($hour);}
			$daytext="";
			if($day > 0 && $hour > 0){$daytext="$day  dia e $hour h ";}
			else if($day > 0 && $hour = 0){$daytext="$day  dia ";}
			else if($day = 0 && $hour > 0){$daytext="$hour h ";}
			return $daytext;
		}
        $hour = $value / 3600;
        $hour = floor($hour);
        $minutes = $value % 60;
        if (!empty($hour)) {
            $fhour = "$hour h";
        }
        if (!empty($minutes)) {
            $fhour.=" $minutes min ";
        }

        return $fhour;
    }
     public function timepast($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        $data = $value;
        $result=null;
        if (is_object($data) && is_a($data, 'MongoDate')) {
            $d=new \DateTime();
            $d->setTimestamp($data->sec);
            $data=$d;
        }else  if (is_object($data) && is_a($data, 'MongoDB\BSON\UTCDateTime')) {
            $data=$data->toDateTime();
          
        }else if(is_numeric($data) && strlen($data.'') >=9 ){
            $d=new \DateTime();
            $d->setTimestamp($data);
            $data=$d;
        }
        
        
         if (is_object($data) && is_a($data, 'DateTime')) {
			 $y = $data->format('Y');
			if($y < 1 ){return null;}
              $now=new \DateTime();
              $interval=$data->diff($now);
              $interval= $interval->format('%R%a');
              $interval=$interval+0;
              
              if($interval == 1){$result=$this->getTranslator()->trans('badiu.system.time.countpast.day',array('%numberx%'=>$interval));}
              else if($interval > 1){$result=$this->getTranslator()->trans('badiu.system.time.countpast.days',array('%numberx%'=>$interval));}
         }
        return $result;
    }
     public function percentageshownumber($value) {
         $x=0;
         $y=0;
         $perc="";
         if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$x=$this->row[$this->column];}
         if(!empty($this->column1) && array_key_exists($this->column1,$this->row)){$y=$this->row[$this->column1];}
         
        
         if($x!=null && $y!=null){
             if($y >=0 && $x >=0 && $y>= $x){
                 if($y==0){$perc=0;}
                 else{
                     $perc= $x*100/$y;
                     $perc=number_format($perc, 0, ',', '.');
                 }
               $perc= $this->getTranslator()->trans('badiu.system.percentage.shownumber',array('%x%'=>$x,'%y%'=>$y,'%percent%'=>$perc)); 
            }
         }else{$perc="";}
         return $perc;
     }
	 public function percentageshowvaluewithsymbol($value) {
         $x=0;
         $y=0;
         $perc="";
         if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$x=$this->row[$this->column];}
         if(!empty($this->column1) && array_key_exists($this->column1,$this->row)){$y=$this->row[$this->column1];}
         
        
         if($x!=null && $y!=null){
             if($y >=0 && $x >=0 && $y>= $x){
                 if($y==0){$perc=0;}
                 else{
                     $perc= $x*100/$y;
                     $perc=number_format($perc, 0, ',', '.');
                 }
               $perc=$perc.'%';
            }
         }else{$perc="";}
         return $perc;
     }
     public function percentageprogressbar($value) {
        $x=0;
        $y=0;
        $perc="";
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$x=$this->row[$this->column];}
        if(!empty($this->column1) && array_key_exists($this->column1,$this->row)){$y=$this->row[$this->column1];}
        
        $percn=0;
        if($x!=null && $y!=null){
            if($y >=0 && $x >=0 && $y>= $x){
                if($y==0){$perc=0;} 
                else{
                    $percn= $x*100/$y;
                    $perc=number_format($percn, 2, ',', '.');
                }
             // $perc= $this->getTranslator()->trans('badiu.system.percentage.shownumber',array('%x%'=>$x,'%y%'=>$y,'%percent%'=>$perc)); 
             $perc="<progress  value=\"$percn\" max=\"100\"></progress> $perc%";
              
           }
        }else{$perc="";}
        return $perc;
    }
	
	public function percentageprogressbarinfo($value) {
        $x=0;
        $y=0;
        $perc="";
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$x=$this->row[$this->column];}
        if(!empty($this->column1) && array_key_exists($this->column1,$this->row)){$y=$this->row[$this->column1];}
        
        $percn=0;
        if($x!=null && $y!=null){
            if($y >=0 && $x >=0 && $y>= $x){
                if($y==0){$perc=0;}
                else{
                    $percn= $x*100/$y;
                    $perc=number_format($percn, 2, ',', '.');
                }
              $perc= $this->getTranslator()->trans('badiu.system.percentage.shownumber',array('%x%'=>$x,'%y%'=>$y,'%percent%'=>$perc)); 
             $perc="$perc <br/><progress  value=\"$percn\" max=\"100\"></progress>  ";
              
           }
        }else{$perc="";}
        return $perc;
    }
	
	public function hourpercentageprogressbarinfo($value) {
        $x=0;
        $y=0;
        $perc="";
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$x=$this->row[$this->column];}
        if(!empty($this->column1) && array_key_exists($this->column1,$this->row)){$y=$this->row[$this->column1];}
        
        $percn=0;
		if(empty($x)){$x=0;}
		if(empty($y)){$y=0;}  
       
            if($y >=0 && $x >=0 && $y>= $x){
                if($y==0){$perc=0;}
                else{
                    $percn= $x*100/$y;
                    $perc=number_format($percn, 2, ',', '.');
                }
				if($x>0){$x=$x/60;}
				$x="$x h";
				if($y>0){$y=$y/60;} 
				 $y="$y h";
              $perc= $this->getTranslator()->trans('badiu.system.percentage.shownumber',array('%x%'=>$x,'%y%'=>$y,'%percent%'=>$perc)); 
             $perc="$perc <br/><progress  value=\"$percn\" max=\"100\"></progress>  ";
              
           }
    
        return $perc;
    }
     public function percentageinverseshownumber($value) {
         $x=0;
         $y=0;
         $perc="";
         if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$x=$this->row[$this->column];}
         if(!empty($this->column1) && array_key_exists($this->column1,$this->row)){$y=$this->row[$this->column1];}
         
        
         if($x!=null && $y!=null){
             if($y >=0 && $x >=0 && $y>= $x){
                 $x=$y-$x;
                  if($y==0){$perc=0;}
                 else{
                     $perc= $x*100/$y;
                     $perc=number_format($perc, 0, ',', '.');
                 }
               $perc= $this->getTranslator()->trans('badiu.system.percentage.shownumber',array('%x%'=>$x,'%y%'=>$y,'%percent%'=>$perc)); 
            }
         }else{$perc="";}
         return $perc;
     }

     public function percentageaddsymbol($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
		
        $data = $value;
      
        if($data !=null || $data !=""){
            $data=$data."%";
			 $perc= $this->getTranslator()->trans('badiu.system.percentage.shownumber',array('%x%'=>$x,'%y%'=>$y,'%percent%'=>$perc)); 
             $perc="$perc <br/><progress  value=\"$percn\" max=\"100\"></progress>  ";
             
       }
         return $data;
    }
public function percentageaddprogressbar($value) {
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
		
        $data = $value;
      
        if($data !=null || $data !=""){
			$dataf=number_format($data, 2, ',', '.');
            $data="<progress  value=\"$data\" max=\"100\"></progress> $dataf%";
       }
         return $data;
    }
 public function castnulltozero($value) {
       if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
        
		if(!empty($value)){$value=trim($value);}
        if (empty($value)) {
            return 0;
        }
		

        return $value;
    }	
	public function listname($value) {
        $flist="";
		
        $keyservice=$this->column1;
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$flist=$this->row[$this->column];}
      
		if(empty($flist)){return null;}
		$flist=$this->getUtildata()->castStringToArray($flist,",");
		
	   
	   
	   $listoptions=null;
	   $sessionexpire=true;
	   
	   //manage session
	   
	   $badiuSession = $this->getContainer()->get('badiu.system.access.session');
       $badiuSession->setHashkey($this->getSessionhashkey());
	   $badiuSession->setSystemdata($this->getSystemdata());
	   $lsvalue=$badiuSession->getValueTime($keyservice."_listcommasvalues");
	
	   $sessionexpire=$lsvalue->expire;
	   $listoptions=$lsvalue->value;
	   if($sessionexpire){
		    $configkservice=array();
			if(!empty($keyservice)){
              $pos=stripos($keyservice, "-");
              if($pos=== false){
                  $configkservice=array($keyservice);
              }else{
                  $configkservice= explode("-", $keyservice);
              }
          }
		  
		  $skey=$this->getUtildata()->getVaueOfArray($configkservice,0);
			$function=$this->getUtildata()->getVaueOfArray($configkservice,1);
			$convertlist=$this->getUtildata()->getVaueOfArray($configkservice,2);
	   
			if(empty($function)){$function='getFormChoiceShortname';}
			if(empty($convertlist)){$convertlist='y';}
	    
			if(empty($skey)){return null;}
			if(!$this->getContainer()->has($skey)){return null;}
			$service=$this->getContainer()->get($skey);
			if (!method_exists($service,$function)){return null;}
	   
	   
			
			if($function=='getFormChoiceShortname'){$listoptions=$service->$function($this->getEntity());}
			else {$listoptions=$service->$function();}
	   
			if(empty($listoptions)){return null;}
			if($convertlist=='y'){$listoptions=$this->getContainer()->get('badiu.system.core.lib.form.util')->convertBdArrayToListOptions($listoptions);}

			//add session
			$badiuSession->addValueTime($keyservice."_listcommasvalues",$listoptions,300);
	   } 
	  
		  
	   
	   
	  $rvalue="";
	   $cont=0;
	   foreach ($flist as $v) {
				$value=$this->getUtildata()->getVaueOfArray($listoptions,$v);
				$separator=", ";
				if($cont==0){$separator="";}
				$rvalue.=$separator.$value;
				$cont++;
			}
       return  $rvalue;
    }
	
	public function treepathname($value) {
		
		$keyservice=$this->column1;
        if($this->column!==null && $this->column!=="" && array_key_exists($this->column,$this->row)){$value=$this->row[$this->column];}
     
		if(empty($value)){return null;}
		
	   $listoptions=null;
	   $sessionexpire=true;
	   
	   //manage session
	   
	   $badiuSession = $this->getContainer()->get('badiu.system.access.session');
       $badiuSession->setHashkey($this->getSessionhashkey());
	   $badiuSession->setSystemdata($this->getSystemdata());
	   $lsvalue=$badiuSession->getValueTime($keyservice."_listree");
	
	   $sessionexpire=$lsvalue->expire;
	   $listoptions=$lsvalue->value;
	   if($sessionexpire){
		    $configkservice=array();
			if(!empty($keyservice)){
              $pos=stripos($keyservice, "-");
              if($pos=== false){
                  $configkservice=array($keyservice);
              }else{
                  $configkservice= explode("-", $keyservice);
              }
          }
		  
		  $skey=$this->getUtildata()->getVaueOfArray($configkservice,0);
			$function=$this->getUtildata()->getVaueOfArray($configkservice,1);
			$convertlist=$this->getUtildata()->getVaueOfArray($configkservice,2);
	   
			if(empty($function)){$function='getFormChoiceParentKeyId';}
			if(empty($convertlist)){$convertlist='y';}
	    
			if(empty($skey)){return null;}
			if(!$this->getContainer()->has($skey)){return null;}
			$service=$this->getContainer()->get($skey);
			if (!method_exists($service,$function)){return null;}
	   
	   
			
			$listoptions=$service->$function();
	   
			if(empty($listoptions)){return null;}
			if($convertlist=='y'){$listoptions=$this->getContainer()->get('badiu.system.core.lib.form.util')->convertBdArrayToListOptions($listoptions);}
			//add session
			$badiuSession->addValueTime($keyservice."_listree",$listoptions,300);
	   } 
	  
		
		$pthv=$this->getUtildata()->getVaueOfArray($listoptions,$value);
		
       return  $pthv;
    }
    public function defaulfTimeDuration($value) {
        $result=$this->hour->convertSeconds($value,'h');
        return $result;
        
    }
  public function generalType($value) {
       $data = $value;
         if(is_float($data)) {
               
        }else if(is_string($data)) {
               
        } else if(is_object($data)) {
              $format=$this->getDateFormatLayout('date');
              if (is_a($data, 'DateTime')) {$data=$data->format($format);}
        }  
        return $data;
    }
    public function defaultType($value) {
        $data = $value;
        if ($this->value == 'boolean' || $this->value == 'defaultboolean') {
            $data = $this->defaultBoolean($value);
        } else if ($this->value == 'defaultdate') {
            $data = $this->defaultDate($value);
        }else if ($this->value == 'defaultdatefull') {
            $data = $this->defaultDateFull($value);
        } else if ($this->value == 'defaultdatetime') {
            $data = $this->defaultDateTime($value);
        } else if ($this->value == 'defaultdatetimeform') {
            $data = $this->defaultDateTimeForm($value);
        }
        else if ($this->value == 'timestampdefaultdate') {
            $data = $this->defaultDateTimestamp($value);
        } else if ($this->value == 'timestampdefaultdatetime') {
            $data = $this->defaultDateTimeTimestamp($value);
        }
		else if ($this->value == 'timestampdefaulttime') {
            $data = $this->defaultTimeTimestamp($value);
        }
        
         else if ($this->value == 'defaultdatetimehour') {
            $data = $this->defaultDateTimeHour($value);
        } 
        else if ($this->value == 'defaultdatetimehourminute') {
            $data = $this->defaultDateTimeHourMinute($value);
        }
        else if ($this->value == 'defaultdatetimehourminutesecond') {
            $data = $this->defaultDateTimeHourMinuteSecond($value);
        } else if ($this->value == 'datecountminutepast') {
            $data = $this->dateCountMinutePast($value);
        } else if ($this->value == 'defaultnumberlong') {
            $data = $this->defaulfNumberLong($value);
        } else if ($this->value == 'defaultnumberdouble') {
            $data = $this->defaulfNumberDouble($value);
        }else if ($this->value == 'defaulfnumberdoublepercentagesymbol') {
            $data = $this->defaulfNumberDoublePercentageSymbol($value);
        }
		else if ($this->value == 'defaulcastdouble') {
            $data = $this->defaultCastDouble($value);
        }
        else if ($this->value == 'defaultmoney') {
            $data = $this->defaulfMoney($value);
        }
         else if ($this->value == 'defaulthourround') {
            $data = $this->defaultHourRound($value);
        }else if ($this->value == 'defaulthour') {
            $data = $this->defaultHour($value);
        }else if ($this->value == 'texthour') {
            $data = $this->textHour($value);
        }else if ($this->value == 'texthourday') {
            $data = $this->textHourDay($value);
        } else if ($this->value == 'defaulttimeduration') {
            $data = $this->defaulfTimeDuration($value);
        }
        else if ($this->value == 'timepast') {
            $data = $this->timepast($value);
        }
        else if ($this->value == 'defaultdayofweek') {
            $data = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions')->getDayOfWeekLabel($value);
        } else if ($this->value == 'defaultdaysofweek') {
            $data = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions')->getDaysOfWeekLabel($value);
        } else if ($this->value == 'percentageshownumber') {
            $data = $this->percentageshownumber($value);
        } 
		else if ($this->value == 'percentageshowvaluewithsymbol') {
            $data = $this->percentageshowvaluewithsymbol($value);
        }
         else if ($this->value == 'percentageinverseshownumber') {
            $data = $this->percentageinverseshownumber($value);
        } else if ($this->value == 'percentageaddsymbol') {
              $data = $this->percentageaddsymbol($value);
        } 
		else if ($this->value == 'percentageaddprogressbar') {
              $data = $this->percentageaddprogressbar($value);
        } 
		
        else if ($this->value == 'percentageprogressbar') {
            $data = $this->percentageprogressbar($value);
        }  else if ($this->value == 'percentageprogressbarinfo') {
            $data = $this->percentageprogressbarinfo($value);
        } 
		else if ($this->value == 'hourpercentageprogressbarinfo') {
            $data = $this->hourpercentageprogressbarinfo($value);
        }else if ($this->value == 'castnulltozero') {
            $data = $this->castnulltozero($value);
        }else if ($this->value == 'listname') {
            $data = $this->listname($value);
        } else if ($this->value == 'treepathname') {
            $data = $this->treepathname($value);
        }	
		
        return $data;
    }

  function getDateFormatLayout($type='datetime') {
		$f='d/m/Y H:i:s';
	  if($this->getLang()=='en'){
		  if($type=='datetime'){$f='Y-m-d H:i:s';}
		  else if($type=='date'){$f='Y-m-d';}
		  else if($type=='time'){$f='H:i:s';}
	  }else{
		  if($type=='datetime'){$f='d/m/Y H:i:s';}
		  else if($type=='date'){$f='d/m/Y';}
		  else if($type=='time'){$f='H:i:s';}
	  }
	 
 	 
	return $f;	 
  }
    
    function getType() {
        return $this->type;
    }

    function getValue() {
        return $this->value;
    }

    function getColumn() {
        return $this->column;
    }

    function getFunction() {
        return $this->function;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setValue($value) {
        $this->value = $value;
    }

    function setColumn($column) {
        $this->column = $column;
    }

    function setFunction($function) {
        $this->function = $function;
    }
    function getKey() {
        return $this->key;
    }

    function getRow() {
        return $this->row;
    }

    function setKey($key) {
        $this->key = $key;
    }

    function setRow($row) {
        $this->row = $row;
    }

  function getHour() {
        return $this->hour;
    }

    function setHour($hour) {
        $this->hour = $hour;
    }

    function getColumn1() {
        return $this->column1;
    }

    function setColumn1($column1) {
        $this->column1 = $column1;
    }


}
