<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Format;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;
/**
 * Description of MakeSelect
 *
 * @author lino
 */

class GeneralFormat{
    
  /**
     * @var Container
     */
    private $container;
 
   /**
     * @var Translator
     */
    private $translator;

    
         /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
	private $systemdata;
    function __construct(Container $container) {
                $this->container=$container;
                 $this->translator=$this->container->get('translator');
          
      }
      
      
        //review it. $key with current columun name is not necessary yet
      public function get($key,$row,$data,$info=array()){
          
          //print_r($info);echo "<hr>";
           $paramconfig=null;
            if(is_array($info) && array_key_exists("format",$info)){
               $paramconfig=$info['format'];
               $paramconfig=trim($paramconfig); 
               if(empty($paramconfig)){$paramconfig=null;}
           }
           $configformat = $this->container->get('badiu.system.core.lib.format.configformat');
           $configformat->setSessionhashkey($this->getSessionhashkey());
		   $configformat->setSystemdata($this->getSystemdata());
           $value=$configformat->get($key,$paramconfig,$data,$row);
           
          
          return $value;
      }
      
      //review it use ConfigFormat
    #  public function get($key,$row,$data,$info=array()){

          //desable type
         /*  $type=null;
           if(array_key_exists("type",$info)){
               $type=$info['type'];
               $type=trim($type); 
               if(empty($type)){$type=null;}
           }*/
   
          /*----  INICIO----
          $format=null;
            if(array_key_exists("format",$info)){
               $format=$info['format'];
               $format=trim($format); 
               if(empty($format)){$format=null;}
           }

          if(!empty($format)){

              $pos=stripos($format, "|");
              $type=null;
              $value=null;
              $function=$key;

              if($pos!== false){
                  $p = explode("|", $format);
                  if(isset($p[0])) {$type=$p[0];}
                  if(isset($p[1])) {$value=$p[1];}
                  if(isset($p[2])) {$function=$p[2];}
                  if($type=='s' && !empty($value)){
                      $fservice=$this->container->get($value);
                      //$data=$fservice->exec($row,$function);
                      $data=$fservice->$function($row);
                      return $data;
                    }
                  else if($type=='d' && $value=='boolean'){
                     if($data){$data=$this->getTranslator()->trans('yes');}
                     else {$data=$this->getTranslator()->trans('no');}
                     return $data;
                  }
		  else if($type=='d' && $value=='timestampdate'){
                     if($data >0 ){$data=date('d/m/Y',$data);}
                     else {$data="";}
                     return $data;
                  }
		 else if($type=='d' && $value=='timestampdatetime'){
                     if($data >0 ){$data=date('d/m/Y H:i:s',$data);}
                     else {$data="";}
                     return $data;
                  }
                 //date time defaultdate
                  else if($type=='d' && $value=='defaultdate'){
                      if(is_object($data) && is_a($data, 'DateTime')){
                          $data=$data->format('d/m/Y');
                      }
                    
                  }
                  //date time defaultdate
                  else if($type=='d' && $value=='defaultdatetime'){
                      if(is_object($data) && is_a($data, 'DateTime')){
                          $data=$data->format('d/m/Y H:i:s');
                      }
                    
                  }
                }
            }

            //default
            if(is_float($data)) {
               
            }else if(is_string($data)) {
               
            } else if(is_object($data)) {
               // if(empty($format)){$format='d/m/Y H:i:s';}
                if(empty($format)){$format='d/m/Y';}
                if (is_a($data, 'DateTime')) {$data=$data->format($format);}
              //  if (is_a($data, 'Date')) {$data=$data->format('d/m/Y');} //nao esta funcionando
            }  
              -----FIM----
            */
          /* if($type!=null){
               if($type=='boolean'){
                   if($data){$data=$this->getTranslator()->trans('yes');}
                   else {$data=$this->getTranslator()->trans('no');}
               }
           }
           else{

                if(is_numeric($data)) {

                    if(!empty($pattern)){

                        $pos=stripos($pattern, "|");
                        $patterntype=null;
                        $patternvalue=null;
                        $function=$key;

                        if($pos!== false){
                            $p = explode("|", $pattern);
                            if(isset($p[0])) {$patterntype=$p[0];}
                            if(isset($p[1])) {$patternvalue=$p[1];}
                            if(isset($p[2])) {$function=$p[2];}
                            if($patterntype=='s' && !empty($patternvalue)){
                                 $fservice=$this->container->get($patternvalue);
                                return $fservice->exec($row,$function);
                            }
                        }
                    }

                }else if(is_float($data)) {
               
                }else if(is_string($data)) {
               
                } else if(is_object($data)) {
                    if(empty($pattern)){$pattern='d/m/Y H:i:s';}
                    if (is_a($data, 'DateTime')) {$data=$data->format($pattern);}
                 } 
          
           }*/
          
          
      #    return $data;
     # }
      
     
      public function getContainer() {
          return $this->container;
      }

      public function getTranslator() {
          return $this->translator;
      }

      public function setContainer(Container $container) {
          $this->container = $container;
      }

      public function setTranslator(Translator $translator) {
          $this->translator = $translator;
      }



      public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }
 	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
