<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Format;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;

/**
 * Description of MakeSelect
 *
 * @author lino
 */
class DateFormat {

	/**
	 * @var Container
	 */
	private $container;

	/**
	 * @var Translator
	 */
	private $translator;
        private $dataoptions;
	function __construct(Container $container) {
		$this->container = $container;
		$this->translator = $this->container->get('translator');
               $this->dataoptions=$this->container->get('badiu.system.core.lib.date.period.form.dataoptions');
	}

	public function period(\DateTime $timestart, \DateTime $timeend, $type = 'hour',$formattext=false) {
		$dateformat = "";
		$sameyear = false;
		$samemonth = false;
		$sameday = false;
		$samehour = false;
		$sameminute = false;
		$samesecond = false;

		$secondstart = $timestart->format('s');
		$secondend = $timeend->format('s');
		if ($secondstart == $secondend) {
			$samesecond = true;
		}

		$minutestart = $timestart->format('i');
		$minuteend = $timeend->format('i');
		if ($minutestart == $minuteend) {
			$sameminute = true;
		}

		$hourstart = $timestart->format('H');
		$hourend = $timeend->format('H');
		if ($hourstart == $hourend) {
			$samehour = true;
		}

		$daystart = $timestart->format('d');
		$dayend = $timeend->format('d');
		if ($daystart == $dayend) {
			$sameday = true;
		}

		$monthstart = $timestart->format('m');
		$monthend = $timeend->format('m');
		if ($monthstart == $monthend) {
			$samemonth = true;
		}

		$yearstart = $timestart->format('y');
		$yearend = $timeend->format('y');
		if ($yearstart == $yearend) {
			$sameyear = true;
		}

		if ($type == 'second') {
			if ($sameyear && $samemonth && $sameday && $samehour && $sameminute && $samesecond) {
				$dateformat = $timestart->format('d/m/Y H:i:s');
				return $dateformat;
			} else {
				$dateformat .= $timestart->format('d/m/Y H:i:s');
				$dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.til');
				$dateformat .= " ";
				$dateformat .= $timeend->format('d/m/Y H:i:s');
				return $dateformat;
			}
		} else if ($type == 'hour') {
			//same hour
			if ($sameyear && $samemonth && $sameday) {
                                $dayofweek=$timestart->format('w');
                                $dayofweeklabel=$this->dataoptions->getDayOfWeekLabel($dayofweek);
				$dateformat =$dayofweeklabel;
                                $dateformat .= " ";
                                $dateformat .= $timestart->format('d');
				$dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.from');
				$dateformat .= " ";
				$dateformat .= $this->dataoptions->getMonthLabel($timeend->format('m'),true);
				$dateformat .= " ";
				$dateformat .=$timestart->format('Y');  //review show if year is not current year
				$dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.from');
				$dateformat .= " ";
				$dateformat .= $timestart->format('H:i');
				$dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.at');
                                $dateformat .= " ";
				$dateformat .= $timeend->format('H:i');

				return $dateformat;
			} else if ($sameyear && $samemonth && !$sameday) {
				$dateformat = $timestart->format('d');
				$dateformat .= " ";
				//$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.to');
                                $dateformat .= 'e'; //revew it
				$dateformat .= " ";
                                $dateformat .= $timeend->format('d');
                                $dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.from');
				$dateformat .= " ";
				$dateformat .= $this->dataoptions->getMonthLabel($timeend->format('m'),true);
				$dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.from');
				$dateformat .= " ";
				$dateformat .= $timestart->format('H:i');
				$dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.at');
                                $dateformat .= " ";
				$dateformat .= $timeend->format('H:i');

				return $dateformat;
			} else {
				$dateformat .= $timestart->format('d/m/Y H:i:s');
				$dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.til');
				$dateformat .= " ";
				$dateformat .= $timeend->format('d/m/Y H:i:s');
				return $dateformat;
			}

		}
                 else if ($type == 'day') {
			//same hour
			if ($sameyear && $samemonth && $sameday) {
				$dateformat = $timestart->format('d');
				$dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.from');
				$dateformat .= " ";
				$dateformat .= $this->dataoptions->getMonthLabel($timeend->format('m'),true);
				
				return $dateformat;
			} else if ($sameyear && $samemonth && !$sameday) {
				$dateformat = $timestart->format('d');
				$dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.to');
				$dateformat .= " ";
                                $dateformat .= $timeend->format('d');
                                $dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.from');
				$dateformat .= " ";
				$dateformat .= $this->dataoptions->getMonthLabel($timeend->format('m'),true);
				
                                return $dateformat;
			} else {
				$dateformat .= $timestart->format('d/m/Y H:i:s');
				$dateformat .= " ";
				$dateformat .= $this->getTranslator()->trans('badiu.system.time.format.period.til');
				$dateformat .= " ";
				$dateformat .= $timeend->format('d/m/Y H:i:s');
				return $dateformat;
			}

		}

		return $dateformat;
	}


	public function getContainer() {
		return $this->container;
	}

	public function getTranslator() {
		return $this->translator;
	}

	public function setContainer(Container $container) {
		$this->container = $container;
	}

	public function setTranslator(Translator $translator) {
		$this->translator = $translator;
	}

}
