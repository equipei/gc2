<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Config;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
/**
 * Description of MakeSelect
 *
 * @author lino
 */
class EntityParent {
    //badiu.ams.curriculum.cdiscipline.data.parent.config: entity=badiu.ams.curriculum.curriculum&notfoundmsg=curriculum.notfound
   
    /**
     * @var string
     */
    private $entity;

    /**
     * @var string
     */
    private $functionAdd; 

     /**
     * @var string
     */
    private $functionFind;

    /**
     * @var string
     */
    private $service;    //it is necessry if  functionAdd and functionFind insted methodAdd and methodFind
    /**
     * @var string
     */
    private $notFoundMessage;

    /**
     * @var string
     */
    private $methodAdd;  //should to use to override function functionAdd

     /**
     * @var string
     */
    private $methodFind; ////should to use to override function functionFind
    
    function __construct(Container $container,$param=null) {
        $this->entity="";
        $this->functionAdd="";
        $this->functionFind="";
        $this->service="";
        $this->notFoundMessage="entity.master.notfound";
        $this->methodAdd="";
        $this->methodFind="";
        if($param!=null){$this->initParam($param);}


    }

    function initParam($param) {
        if (array_key_exists("entity",$param)){$this->entity=$param["entity"];}
        if (array_key_exists("notfoundmsg",$param)){$this->notFoundMessage=$param["notfoundmsg"];}
        if (array_key_exists("functionadd",$param)){$this->functionAdd=$param["functionadd"];}
        if (array_key_exists("functionfind",$param)){$this->functionFind=$param["functionfind"];}
        if (array_key_exists("service",$param)){$this->service=$param["service"];}
        
         if (array_key_exists("methodadd",$param)){$this->methodAdd=$param["methodadd"];}
        if (array_key_exists("methodfind",$param)){$this->methodFind=$param["methodfind"];}

    }

    
     public function getEntity() {
         return $this->entity;
     }

     public function getNotFoundMessage() {
         return $this->notFoundMessage;
     }

    

     public function setEntity($entity) {
         $this->entity = $entity;
     }
   public function setNotFoundMessage($notFoundMessage) {
         $this->notFoundMessage = $notFoundMessage;
     }

     public function setFunctionAdd($functionAdd) {
         $this->functionAdd = $functionAdd;
     }  

    public function getFunctionAdd() {
         return $this->functionAdd;
     }

  public function setFunctionFind($functionFind) {
         $this->functionFind = $functionFind;
     }

    public function getFunctionFind() {
         return $this->functionFind;
     }
    

     public function setService($service) {
         $this->service = $service;
     }

    public function getService() {
         return $this->service;
     }
     
     function getMethodAdd() {
         return $this->methodAdd;
     }

     function getMethodFind() {
         return $this->methodFind;
     }

     function setMethodAdd($methodAdd) {
         $this->methodAdd = $methodAdd;
     }

     function setMethodFind($methodFind) {
         $this->methodFind = $methodFind;
     }


}
