<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Config;

/**
 *  *
 * @author lino
 */
class CrudMessage {
    
   
    /**
     * @var string
     */
    private $deletelabel;
  /**
     * @var string
     */
    private $deletecdfield;

	 /**
     * @var string
     */
    private $deletelabelsuccess;
    
      /**
     * @var string
     */
    private $removelabel;
  /**
     * @var string
     */
    private $removecdfield;

	 /**
     * @var string
     */
    private $removelabelsuccess;
    function __construct() {
              
           }
    
           function getDeletelabel() {
               return $this->deletelabel;
           }

           function getDeletecdfield() {
               return $this->deletecdfield;
           }

           function getDeletelabelsuccess() {
               return $this->deletelabelsuccess;
           }

           function getRemovelabel() {
               return $this->removelabel;
           }

           function getRemovecdfield() {
               return $this->removecdfield;
           }

           function getRemovelabelsuccess() {
               return $this->removelabelsuccess;
           }

           function setDeletelabel($deletelabel) {
               $this->deletelabel = $deletelabel;
           }

           function setDeletecdfield($deletecdfield) {
               $this->deletecdfield = $deletecdfield;
           }

           function setDeletelabelsuccess($deletelabelsuccess) {
               $this->deletelabelsuccess = $deletelabelsuccess;
           }

           function setRemovelabel($removelabel) {
               $this->removelabel = $removelabel;
           }

           function setRemovecdfield($removecdfield) {
               $this->removecdfield = $removecdfield;
           }

           function setRemovelabelsuccess($removelabelsuccess) {
               $this->removelabelsuccess = $removelabelsuccess;
           }



         
}
