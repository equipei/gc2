<?php

namespace Badiu\System\CoreBundle\Model\Lib\Config;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class PageMenuConfig {

    /**
     * @var Container
     */
    private $container;
    private $label;
    private $type='web'; //web|vuejs
    private $action;
    private $target;

   
    function __construct(Container $container, $param = null) {
        $this->container = $container;

        $this->parenttablecolumn=null;
         if ($param != null) {
            $this->initParam($param);
        }
    }
    function initParam($param) {
        $subQueryString = $this->container->get('badiu.system.core.lib.http.subquerystring');
       
        
        if (array_key_exists("label", $param)) {
            $this->label = $param["label"];
        } 
         if (array_key_exists("type", $param)) {
            $this->type = $param["type"];
        } 
         if (array_key_exists("action", $param)) {
            $this->action = $param["action"];
        } 
         if (array_key_exists("target", $param)) {
            $this->target = $param["target"];
        } 
         
    }
    function getContainer(){
        return $this->container;
    }

  
    function setContainer(Container $container) {
        $this->container = $container;
    }
    function getLabel() {
        return $this->label;
    }

    function getType() {
        return $this->type;
    }

    function getAction() {
        return $this->action;
    }

    function getTarget() {
        return $this->target;
    }

    function setLabel($label) {
        $this->label = $label;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setAction($action) {
        $this->action = $action;
    }

    function setTarget($target) {
        $this->target = $target;
    }


  

}
