<?php

namespace Badiu\System\CoreBundle\Model\Lib\Config;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class PageConfig {

    /**
     * @var Container
     */
    private $container;

    //this config replace each item block of layout head,hmenu,vlmenu,navbar,content etc
    /**
     * @var array
     */
    private $themelayoutblock; 

      //this config remove item block of layout head,hmenu,vlmenu,navbar,content etc
    /**
     * @var array
     */
    private $themelayoutblockremove; 
   
    private $themelayout;
   
   
    private $fileprocessindex;
    private $fileprocessindextemplate;
    private $fileprocessindexhtmlcontent;
    private $fileprocessindexhtmlcontentportionbefore;
    private $fileprocessindexhtmlcontentportionafter;
    private $fileprocessindexjsvariable;
    private $fileprocessindexjsmethod;
    private $fileprocessindexjswatch;
    private $fileprocessindexjscreated;
    private $fileprocessindexscheduler;
    private $fileprocessindexjscomponent;

    private $fileprocessfrontpage; //process php file to import in twig content on frontpage (default)
    
    /**
     * @var string
     */
    private $fileprocessview; //process php file to import in twig content on view
    private $fileprocessviewtemplate;
    private $fileprocessviewhtmlcontent;
    private $fileprocessviewjsvariable;
    private $fileprocessviewjsmethod;
    private $fileprocessviewjswatch; 
    private $fileprocessviewjscreated;
    private $fileprocessviewjsgeneral;
    private $fileprocessviewjscomponent;
    /**
     * @var string
     */
    private $fileprocessadd; //process php file to import in twig content on form add
    
     /**
     * @var string
     */
 //   private $fileprocessedit; //process php file to import in twig content on form edit
    
    private $fileprocessdashboard;
    private $fileprocessdashboardtemplate;
    private $fileprocessdashboardhtmlcontent;
    private $fileprocessdashboardjsvariable;
    private $fileprocessdashboardjsmethod;
    private $fileprocessdashboardjswatch; 
    private $fileprocessdashboardjscreated;
    private $fileprocessdashboardjsgeneral;
    private $fileprocessdashboardjscomponent;
   
    private $dashboarddatatype=null;
   
    private $fileprocessformfilter;
    private $fileprocessformfilterjsvariable;
    private $fileprocessformfilterjsmethod;
    private $fileprocessformfilterjswatch;
    private $fileprocessformfilterjscreated;
    private $fileprocessformfilterjsgeneral;
  
    private $fileprocessformadd;
    private $fileprocessformaddhtmlcontent;
	private $fileprocessformaddcontentportionbefore;
    private $fileprocessformaddcontentportionafter;
    private $fileprocessformaddjsvariable;
    private $fileprocessformaddjsmethod;
    private $fileprocessformaddjswatch;
    private $fileprocessformaddjscreated;
    private $fileprocessformaddjsgeneral;
    private $parenttablecolumn;
    private $schedulermessageemailcolumn=null; 
    private $schedulermessageusercolumn=null;
    private $schedulersystemrecipient=null;
    private $type=null; //scheduler
	
	
    function __construct(Container $container, $param = null) {
        $this->container = $container;

        $this->themelayoutblock = array();
        $this->themelayoutblockremove = array();
        $this->themelayout = null;
        $this->fileprocessfrontpage = "BadiuThemeCoreBundle:View/Frontpage.php";
        $this->fileprocessindextemplate ="badiu.system.core.lib.template.vuejs.index";
        $this->fileprocessindex = "BadiuThemeCoreBundle:View/Index.php";
        $this->fileprocessindexhtmlcontent = "BadiuThemeCoreBundle:View/Index/HtmlContent.php";
        $this->fileprocessindexhtmlcontentportionbefore = "BadiuThemeCoreBundle:View/Index/HtmlContentPortionBefore.php";
        $this->fileprocessindexhtmlcontentportionafter = "BadiuThemeCoreBundle:View/Index/HtmlContentPortionAfter.php";
        $this->fileprocessindexjsvariable = "BadiuThemeCoreBundle:View/Index/VuejsData.php";
        $this->fileprocessindexjsmethod = "BadiuThemeCoreBundle:View/Index/VuejsMethod.php";
        $this->fileprocessindexjswatch = "BadiuThemeCoreBundle:View/Index/VuejsWatch.php";
        $this->fileprocessindexjscreated = "BadiuThemeCoreBundle:View/Index/VuejsCreated.php";
        $this->fileprocessindexjsgeneral = "BadiuThemeCoreBundle:View/Index/JsGeneral.php";
        $this->fileprocessindexjscomponent = "BadiuThemeCoreBundle:View/Index/VuejsComponent.php";
        $this->fileprocessindexscheduler = "BadiuThemeCoreBundle:View/Index/Scheduler/MailReport.php";
       
        $this->fileprocessviewtemplate ="badiu.system.core.lib.template.vuejs.view";
        $this->fileprocessview = "BadiuThemeCoreBundle:View/View.php";
        $this->fileprocessviewhtmlcontent = "BadiuThemeCoreBundle:View/View/HtmlContent.php";
        $this->fileprocessviewjsvariable = "BadiuThemeCoreBundle:View/View/VuejsData.php";
        $this->fileprocessviewjsmethod = "BadiuThemeCoreBundle:View/View/VuejsMethod.php";
        $this->fileprocessviewjswatch = "BadiuThemeCoreBundle:View/View/VuejsWatch.php";
        $this->fileprocessviewjscreated = "BadiuThemeCoreBundle:View/View/VuejsCreated.php";
        $this->fileprocessviewjsgeneral = "BadiuThemeCoreBundle:View/View/JsGeneral.php";
        $this->fileprocessviewjscomponent = "BadiuThemeCoreBundle:View/View/VuejsComponent.php";
        
        $this->fileprocessdashboardtemplate ="badiu.theme.core.lib.template.vuejs.dashboard";
        $this->fileprocessdashboard = "BadiuThemeCoreBundle:View/Dashboard.php";
        $this->fileprocessdashboardhtmlcontent = "BadiuThemeCoreBundle:View/Dashboard/HtmlContent.php";
        $this->fileprocessdashboardjsvariable = "BadiuThemeCoreBundle:View/Dashboard/VuejsData.php";
        $this->fileprocessdashboardjsmethod = "BadiuThemeCoreBundle:View/Dashboard/VuejsMethod.php";
        $this->fileprocessdashboardjswatch = "BadiuThemeCoreBundle:View/Dashboard/VuejsWatch.php";
        $this->fileprocessdashboardjscreated = "BadiuThemeCoreBundle:View/Dashboard/VuejsCreated.php";
        $this->fileprocessdashboardjsgeneral = "BadiuThemeCoreBundle:View/Dashboard/JsGeneral.php";
        $this->fileprocessdashboardjscomponent = "BadiuThemeCoreBundle:View/Dashboard/VuejsComponent.php";
        
        $this->fileprocessformfilter = "BadiuThemeCoreBundle:View/Index/Filter/FactoryForm.php";
        $this->fileprocessformfilterjsvariable = "BadiuThemeCoreBundle:View/Index/Filter/VuejsData.php";
        $this->fileprocessformfilterjsmethod = "BadiuThemeCoreBundle:View/Index/Filter/VuejsMethod.php";
        $this->fileprocessformfilterjswatch = "BadiuThemeCoreBundle:View/Index/Filter/VuejsWatch.php";
        $this->fileprocessformfilterjscreated = "BadiuThemeCoreBundle:View/Index/Filter/VuejsCreated.php";
        $this->fileprocessformfilterjsgeneral = "BadiuThemeCoreBundle:View/Index/Filter/JsGeneral.php";
         
        $this->fileprocessformadd = "BadiuThemeCoreBundle:View/Add.php"; 
        $this->fileprocessformaddhtmlcontent = "BadiuThemeCoreBundle:View/Form/Add/HtmlContent.php";
		$this->fileprocessformaddcontentportionbefore = "BadiuThemeCoreBundle:View/Add/HtmlContentPortionBefore.php";
        $this->fileprocessformaddcontentportionafter = "BadiuThemeCoreBundle:View/Add/HtmlContentPortionAfter.php";
        
        $this->fileprocessformaddjsvariable = "BadiuThemeCoreBundle:View/Form/Add/VuejsData.php";
        $this->fileprocessformaddjsmethod = "BadiuThemeCoreBundle:View/Form/Add/VuejsMethod.php";
        $this->fileprocessformaddjswatch = "BadiuThemeCoreBundle:View/Form/Add/VuejsWatch.php";
        $this->fileprocessformaddjscreated = "BadiuThemeCoreBundle:View/Form/Add/VuejsCreated.php";
        $this->fileprocessformaddjsgeneral = "BadiuThemeCoreBundle:View/Form/Add/JsGeneral.php";
        $this->parenttablecolumn=null;
         if ($param != null) {
            $this->initParam($param);
        }
    }
    function initParam($param) {
        $subQueryString = $this->container->get('badiu.system.core.lib.http.subquerystring');
       
       if (array_key_exists("themelayoutblock", $param)) {
                $themelayoutblock=$param["themelayoutblock"];
                $subQueryString->setQuery($themelayoutblock);
                $subQueryString->makeParam();
                $this->themelayoutblock = $subQueryString->getParam();
           }
       if (array_key_exists("themelayoutblockremove", $param)) {
            $themelayoutblockremove=$param["themelayoutblockremove"];
            
            if(!empty($themelayoutblockremove)){
                $listbr=array();
                $posbr=stripos($themelayoutblockremove, ",");
                if($posbr=== false){
                    $listbr[$themelayoutblockremove]=$themelayoutblockremove;
                }else{
                    $listbrofc= explode(",", $themelayoutblockremove);
                    if(is_arrary($listbrofc)){
                        foreach ($listbrofc as $vbr) {
                            $listbr[$vbr]=$vbr;
                        }
                    }
                    

                }
                $this->themelayoutblockremove =$listbr;
            }
            
       }
        if (array_key_exists("themelayout", $param)) {
            $this->themelayout = $param["themelayout"];
        }
        
        if (array_key_exists("fileprocessindextemplate", $param)) {
            $this->fileprocessindextemplate = $param["fileprocessindextemplate"];
        }
        
        if (array_key_exists("fileprocessindexhtmlcontent", $param)) {
            $this->fileprocessindexhtmlcontent = $param["fileprocessindexhtmlcontent"];
        }
        if (array_key_exists("fileprocessindexhtmlcontentportionbefore", $param)) {
            $this->fileprocessindexhtmlcontentportionbefore = $param["fileprocessindexhtmlcontentportionbefore"];
        }
        if (array_key_exists("fileprocessindexhtmlcontentportionafter", $param)) {
            $this->fileprocessindexhtmlcontentportionafter = $param["fileprocessindexhtmlcontentportionafter"];
        }
        if (array_key_exists("fileprocessindex", $param)) {
            $this->fileprocessindex = $param["fileprocessindex"];
        }
        if (array_key_exists("fileprocessindexjsvariable", $param)) {
            $this->fileprocessindexjsvariable = $param["fileprocessindexjsvariable"];
        }
        if (array_key_exists("fileprocessindexjsmethod", $param)) {
            $this->fileprocessindexjsmethod = $param["fileprocessindexjsmethod"];
        }
        if (array_key_exists("fileprocessindexjswatch", $param)) {
            $this->fileprocessindexjswatch = $param["fileprocessindexjswatch"];
        }
        
        if (array_key_exists("fileprocessindexjscreated", $param)) {
            $this->fileprocessindexjscreated = $param["fileprocessindexjscreated"];
        }
        if (array_key_exists("fileprocessindexjsgeneral", $param)) {
            $this->fileprocessindexjsgeneral = $param["fileprocessindexjsgeneral"];
        }

        if (array_key_exists("fileprocessindexjsgeneral", $param)) {
            $this->fileprocessindexjsgeneral = $param["fileprocessindexjsgeneral"];
        }
        if (array_key_exists("fileprocessindexjscomponent", $param)) {
            $this->fileprocessindexjscomponent = $param["fileprocessindexjscomponent"];
        }

        if (array_key_exists("fileprocessindexscheduler", $param)) {
            $this->fileprocessindexscheduler = $param["fileprocessindexscheduler"];
        }
        
        if (array_key_exists("fileprocessviewtemplate", $param)) {
            $this->fileprocessviewtemplate = $param["fileprocessviewtemplate"];
        }
        
         if (array_key_exists("fileprocessview", $param)) {
            $this->fileprocessview = $param["fileprocessview"];
        }
       
        if (array_key_exists("fileprocessviewhtmlcontent", $param)) {
            $this->fileprocessviewhtmlcontent = $param["fileprocessviewhtmlcontent"];
        }
         if (array_key_exists("fileprocessviewjsvariable", $param)) {
            $this->fileprocessviewjsvariable = $param["fileprocessviewjsvariable"];
        }
        if (array_key_exists("fileprocessviewjsmethod", $param)) {
            $this->fileprocessviewjsmethod = $param["fileprocessviewjsmethod"];
        }
        if (array_key_exists("fileprocessviewjswatch", $param)) {
            $this->fileprocessviewjswatch = $param["fileprocessviewjswatch"];
        }
        if (array_key_exists("fileprocessviewjscreated", $param)) {
            $this->fileprocessviewjscreated = $param["fileprocessviewjscreated"];
        }
        if (array_key_exists("fileprocessviewjsgeneral", $param)) {
            $this->fileprocessviewjsgeneral = $param["fileprocessviewjsgeneral"];
        }

        if (array_key_exists("fileprocessviewjscomponent", $param)) {
            $this->fileprocessviewjscomponent = $param["fileprocessviewjscomponent"];
        }
        
        if (array_key_exists("fileprocessdashboardtemplate", $param)) {
            $this->fileprocessdashboardtemplate = $param["fileprocessdashboardtemplate"];
        }
        
        if (array_key_exists("fileprocessdashboard", $param)) {
            $this->fileprocessdashboard = $param["fileprocessdashboard"];
        }
        if (array_key_exists("fileprocessdashboardhtmlcontent", $param)) {
            $this->fileprocessdashboardhtmlcontent = $param["fileprocessdashboardhtmlcontent"];
        }
         if (array_key_exists("fileprocessdashboardjsvariable", $param)) {
            $this->fileprocessdashboardjsvariable = $param["fileprocessdashboardjsvariable"];
        }
        if (array_key_exists("fileprocessdashboardjsmethod", $param)) {
            $this->fileprocessdashboardjsmethod = $param["fileprocessdashboardjsmethod"];
        }
        if (array_key_exists("fileprocessdashboardjswatch", $param)) {
            $this->fileprocessdashboardjswatch = $param["fileprocessdashboardjswatch"];
        }
        if (array_key_exists("fileprocessdashboardjscreated", $param)) {
            $this->fileprocessdashboardjscreated = $param["fileprocessdashboardjscreated"];
        }
        if (array_key_exists("fileprocessdashboardjsgeneral", $param)) {
            $this->fileprocessdashboardjsgeneral = $param["fileprocessdashboardjsgeneral"];
        }
        if (array_key_exists("fileprocessdashboardjscomponent", $param)) {
            $this->fileprocessdashboardjscomponent = $param["fileprocessdashboardjscomponent"];
        }
         if (array_key_exists("fileprocessformfilter", $param)) {
            $this->fileprocessformfilter = $param["fileprocessformfilter"];
        }
        if (array_key_exists("fileprocessformfilterjsvariable", $param)) {
            $this->fileprocessformfilterjsvariable = $param["fileprocessformfilterjsvariable"];
        }
        if (array_key_exists("fileprocessformfilterjsmethod", $param)) {
            $this->fileprocessformfilterjsmethod = $param["fileprocessformfilterjsmethod"];
        }
        if (array_key_exists("fileprocessformfilterjswatch", $param)) {
            $this->fileprocessformfilterjswatch = $param["fileprocessformfilterjswatch"];
        }
        
         if (array_key_exists("fileprocessformfilterjscreated", $param)) {
            $this->fileprocessformfilterjscreated = $param["fileprocessformfilterjscreated"];
        }
        if (array_key_exists("fileprocessformfilterjsgeneral", $param)) {
            $this->fileprocessformfilterjsgeneral = $param["fileprocessformfilterjsgeneral"];
        }
        
        
         if (array_key_exists("fileprocessformadd", $param)) {
            $this->fileprocessformadd = $param["fileprocessformadd"];
        }
        if (array_key_exists("fileprocessformaddhtmlcontent", $param)) {
            $this->fileprocessformaddhtmlcontent = $param["fileprocessformaddhtmlcontent"];
        }
		
		if (array_key_exists("fileprocessformaddcontentportionbefore", $param)) {
            $this->fileprocessformaddcontentportionbefore = $param["fileprocessformaddcontentportionbefore"];
        }
        if (array_key_exists("fileprocessformaddcontentportionafter", $param)) {
            $this->fileprocessformaddcontentportionafter = $param["fileprocessformaddcontentportionafter"];
        }
		
        if (array_key_exists("fileprocessformaddjsvariable", $param)) {
            $this->fileprocessformaddjsvariable = $param["fileprocessformaddjsvariable"];
        }
        if (array_key_exists("fileprocessformaddjsmethod", $param)) {
            $this->fileprocessformaddjsmethod = $param["fileprocessformaddjsmethod"];
        }
        if (array_key_exists("fileprocessformaddjsgeneral", $param)) {
            $this->fileprocessformaddjsgeneral = $param["fileprocessformaddjsgeneral"];
        }
        if (array_key_exists("fileprocessformaddjswatch", $param)) {
            $this->fileprocessformaddjswatch = $param["fileprocessformaddjswatch"];
        }
          if (array_key_exists("fileprocessformaddjscreated", $param)) {
            $this->fileprocessformaddjscreated = $param["fileprocessformaddjscreated"];
        }
        
        if (array_key_exists("parenttablecolumn", $param)) {
            $this->parenttablecolumn = $param["parenttablecolumn"];
        }
        
        if (array_key_exists("schedulermessageemailcolumn", $param)) {
            $this->schedulermessageemailcolumn = $param["schedulermessageemailcolumn"];
        } 
        
         if (array_key_exists("schedulermessageusercolumn", $param)) {
            $this->schedulermessageusercolumn = $param["schedulermessageusercolumn"];
        }
        if (array_key_exists("schedulersystemrecipient", $param)) {
            $this->schedulersystemrecipient = $param["schedulersystemrecipient"];
        } 
        if (array_key_exists("type", $param)) {
            $this->type = $param["type"];
        } 
		if (array_key_exists("dashboarddatatype", $param)) {
            $this->dashboarddatatype = $param["dashboarddatatype"];
        } 
		
    }
    function getContainer(){
        return $this->container;
    }

  
    function setContainer(Container $container) {
        $this->container = $container;
    }

  

    

    function getTheme() {
        return $this->theme;
    }

    function getLayout() {
        return $this->layout;
    }

    function getFileprocessindex() {
        return $this->fileprocessindex;
    }

   

    function setTheme($theme) {
        $this->theme = $theme;
    }

    function setLayout($layout) {
        $this->layout = $layout;
    }

    function setFileprocessindex($fileprocessindex) {
        $this->fileprocessindex = $fileprocessindex;
    }

    

    function getFileprocessview() {
        return $this->fileprocessview;
    }

    function setFileprocessview($fileprocessview) {
        $this->fileprocessview = $fileprocessview;
    }


    function getThemelayoutblock() {
        return $this->themelayoutblock;
    }
    function getThemelayoutblockremove() {
        return $this->themelayoutblockremove;
    }
    function getThemelayout() {
        return $this->themelayout;
    }

    function getFileprocessdashboard() {
        return $this->fileprocessdashboard;
    }

    function setThemelayoutblock($themelayoutblock) {
        $this->themelayoutblock = $themelayoutblock;
    }
    function setThemelayoutblockremove($themelayoutblockremove) {
        $this->themelayoutblockremove = $themelayoutblockremove;
    }
    function setThemelayout($themelayout) {
        $this->themelayout = $themelayout;
    }

    function setFileprocessdashboard($fileprocessdashboard) {
        $this->fileprocessdashboard = $fileprocessdashboard;
    }

    function getFileprocessformfilter() {
        return $this->fileprocessformfilter;
    }

    function getFileprocessformadd() {
        return $this->fileprocessformadd;
    }
    function getFileprocessformaddhtmlcontent() {
        return $this->fileprocessformaddhtmlcontent;
    }

    function setFileprocessformfilter($fileprocessformfilter) {
        $this->fileprocessformfilter = $fileprocessformfilter;
    }

    function setFileprocessformadd($fileprocessformadd) {
        $this->fileprocessformadd = $fileprocessformadd;
    }
    function setFileprocessformaddhtmlcontent($fileprocessformaddhtmlcontent) {
        $this->fileprocessformaddhtmlcontent = $fileprocessformaddhtmlcontent;
    }
    function getFileprocessfrontpage() {
        return $this->fileprocessfrontpage;
    }

    function setFileprocessfrontpage($fileprocessfrontpage) {
        $this->fileprocessfrontpage = $fileprocessfrontpage;
    }
    function getParenttablecolumn() {
        return $this->parenttablecolumn;
    }

    function setParenttablecolumn($parenttablecolumn) {
        $this->parenttablecolumn = $parenttablecolumn;
    }

    function getFileprocessformfilterjsvariable() {
        return $this->fileprocessformfilterjsvariable;
    }

    function getFileprocessformfilterjsmethod() {
        return $this->fileprocessformfilterjsmethod;
    }

    function getFileprocessformfilterjswatch() {
        return $this->fileprocessformfilterjswatch;
    }

    function setFileprocessformfilterjsvariable($fileprocessformfilterjsvariable) {
        $this->fileprocessformfilterjsvariable = $fileprocessformfilterjsvariable;
    }

    function setFileprocessformfilterjsmethod($fileprocessformfilterjsmethod) {
        $this->fileprocessformfilterjsmethod = $fileprocessformfilterjsmethod;
    }

    function setFileprocessformfilterjswatch($fileprocessformfilterjswatch) {
        $this->fileprocessformfilterjswatch = $fileprocessformfilterjswatch;
    }


    function getFileprocessdashboardjsvariable() {
        return $this->fileprocessdashboardjsvariable;
    }

    function getFileprocessdashboardjsmethod() {
        return $this->fileprocessdashboardjsmethod;
    }

    function getFileprocessdashboardjswatch() {
        return $this->fileprocessdashboardjswatch;
    }

    function getFileprocessformaddjsvariable() {
        return $this->fileprocessformaddjsvariable;
    }

    function getFileprocessformaddjsmethod() {
        return $this->fileprocessformaddjsmethod;
    }

    function getFileprocessformaddjswatch() {
        return $this->fileprocessformaddjswatch;
    }

    function setFileprocessdashboardjsvariable($fileprocessdashboardjsvariable) {
        $this->fileprocessdashboardjsvariable = $fileprocessdashboardjsvariable;
    }

    function setFileprocessdashboardjsmethod($fileprocessdashboardjsmethod) {
        $this->fileprocessdashboardjsmethod = $fileprocessdashboardjsmethod;
    }

    function setFileprocessdashboardjswatch($fileprocessdashboardjswatch) {
        $this->fileprocessdashboardjswatch = $fileprocessdashboardjswatch;
    }

    function setFileprocessformaddjsvariable($fileprocessformaddjsvariable) {
        $this->fileprocessformaddjsvariable = $fileprocessformaddjsvariable;
    }

    function setFileprocessformaddjsmethod($fileprocessformaddjsmethod) {
        $this->fileprocessformaddjsmethod = $fileprocessformaddjsmethod;
    }

    function setFileprocessformaddjswatch($fileprocessformaddjswatch) {
        $this->fileprocessformaddjswatch = $fileprocessformaddjswatch;
    }

    function getFileprocessindexjsvariable() {
        return $this->fileprocessindexjsvariable;
    }

    function getFileprocessindexjsmethod() {
        return $this->fileprocessindexjsmethod;
    }

    function getFileprocessindexjswatch() {
        return $this->fileprocessindexjswatch;
    }

    function setFileprocessindexjsvariable($fileprocessindexjsvariable) {
        $this->fileprocessindexjsvariable = $fileprocessindexjsvariable;
    }

    function setFileprocessindexjsmethod($fileprocessindexjsmethod) {
        $this->fileprocessindexjsmethod = $fileprocessindexjsmethod;
    }

    function setFileprocessindexjswatch($fileprocessindexjswatch) {
        $this->fileprocessindexjswatch = $fileprocessindexjswatch;
    }
    function getFileprocessindexhtmlcontent() {
        return $this->fileprocessindexhtmlcontent;
    }

    function setFileprocessindexhtmlcontent($fileprocessindexhtmlcontent) {
        $this->fileprocessindexhtmlcontent = $fileprocessindexhtmlcontent;
    }
function getFileprocessindexjsgeneral() {
        return $this->fileprocessindexjsgeneral;
    }
 
    function setFileprocessindexjsgeneral($fileprocessindexjsgeneral) {
        $this->fileprocessindexjsgeneral = $fileprocessindexjsgeneral;
    }
    function getFileprocessindexhtmlcontentportionbefore() {
        return $this->fileprocessindexhtmlcontentportionbefore;
    }

    function setFileprocessindexhtmlcontentportionbefore($fileprocessindexhtmlcontentportionbefore) {
        $this->fileprocessindexhtmlcontentportionbefore = $fileprocessindexhtmlcontentportionbefore;
    }

    function getFileprocessindexhtmlcontentportionafter() {
        return $this->fileprocessindexhtmlcontentportionafter;
    }

    function setFileprocessindexhtmlcontentportionafter($fileprocessindexhtmlcontentportionafter) {
        $this->fileprocessindexhtmlcontentportionafter = $fileprocessindexhtmlcontentportionafter;
    }
    function getFileprocessdashboardhtmlcontent() {
        return $this->fileprocessdashboardhtmlcontent;
    }

    function setFileprocessdashboardhtmlcontent($fileprocessdashboardhtmlcontent) {
        $this->fileprocessdashboardhtmlcontent = $fileprocessdashboardhtmlcontent;
    }

    function getFileprocessformaddjsgeneral() {
        return $this->fileprocessformaddjsgeneral;
    }

    function setFileprocessformaddjsgeneral($fileprocessformaddjsgeneral) {
        $this->fileprocessformaddjsgeneral = $fileprocessformaddjsgeneral;
    }

function getFileprocessformaddcontentportionbefore() {
        return $this->fileprocessformaddcontentportionbefore;
    }

    function setFileprocessformaddcontentportionbefore($fileprocessformaddcontentportionbefore) {
        $this->fileprocessformaddcontentportionbefore = $fileprocessformaddcontentportionbefore;
    }

    function getFileprocessformaddcontentportionafter() {
        return $this->fileprocessformaddcontentportionafter;
    }

    function setFileprocessformaddcontentportionafter($fileprocessformaddcontentportionafter) {
        $this->fileprocessformaddcontentportionafter = $fileprocessformaddcontentportionafter;
    }
    function getFileprocessdashboardjscreated() {
        return $this->fileprocessdashboardjscreated;
    }

    function getFileprocessdashboardjsgeneral() {
        return $this->fileprocessdashboardjsgeneral;
    }
    function getFileprocessdashboardjscomponent() {
        return $this->fileprocessdashboardjscomponent;
    }
    function getFileprocessformfilterjscreated() {
        return $this->fileprocessformfilterjscreated;
    }

    function getFileprocessformfilterjsgeneral() {
        return $this->fileprocessformfilterjsgeneral;
    }

    function getFileprocessformaddjscreated() {
        return $this->fileprocessformaddjscreated;
    }

    function setFileprocessdashboardjscreated($fileprocessdashboardjscreated) {
        $this->fileprocessdashboardjscreated = $fileprocessdashboardjscreated;
    }

    function setFileprocessdashboardjsgeneral($fileprocessdashboardjsgeneral) {
        $this->fileprocessdashboardjsgeneral = $fileprocessdashboardjsgeneral;
    }
    function setFileprocessdashboardjscomponent($fileprocessdashboardjscomponent) {
        $this->fileprocessdashboardjscomponent = $fileprocessdashboardjscomponent;
    }

    function setFileprocessformfilterjscreated($fileprocessformfilterjscreated) {
        $this->fileprocessformfilterjscreated = $fileprocessformfilterjscreated;
    }

    function setFileprocessformfilterjsgeneral($fileprocessformfilterjsgeneral) {
        $this->fileprocessformfilterjsgeneral = $fileprocessformfilterjsgeneral;
    }

    function setFileprocessformaddjscreated($fileprocessformaddjscreated) {
        $this->fileprocessformaddjscreated = $fileprocessformaddjscreated;
    }

    function getFileprocessindexscheduler() {
        return $this->fileprocessindexscheduler;
    }

    function setFileprocessindexscheduler($fileprocessindexscheduler) {
        $this->fileprocessindexscheduler = $fileprocessindexscheduler;
    }
    function getSchedulermessageemailcolumn() {
        return $this->schedulermessageemailcolumn;
    }

    function setSchedulermessageemailcolumn($schedulermessageemailcolumn) {
        $this->schedulermessageemailcolumn = $schedulermessageemailcolumn;
    }
    function getSchedulersystemrecipient() {
        return $this->schedulersystemrecipient;
    }

    function setSchedulersystemrecipient($schedulersystemrecipient) {
        $this->schedulersystemrecipient = $schedulersystemrecipient;
    }
    function getSchedulermessageusercolumn() {
        return $this->schedulermessageusercolumn;
    }

    function getType() {
        return $this->type;
    }

    function setSchedulermessageusercolumn($schedulermessageusercolumn) {
        $this->schedulermessageusercolumn = $schedulermessageusercolumn;
    }

    function setType($type) {
        $this->type = $type;
    }

    function getFileprocessviewhtmlcontent() {
        return $this->fileprocessviewhtmlcontent;
    }

    function getFileprocessviewjsvariable() {
        return $this->fileprocessviewjsvariable;
    }

    function getFileprocessviewjsmethod() {
        return $this->fileprocessviewjsmethod;
    }

    function getFileprocessviewjswatch() {
        return $this->fileprocessviewjswatch;
    }

    function getFileprocessviewjscreated() {
        return $this->fileprocessviewjscreated;
    }

    function getFileprocessviewjsgeneral() {
        return $this->fileprocessviewjsgeneral;
    }

    function setFileprocessviewhtmlcontent($fileprocessviewhtmlcontent) {
        $this->fileprocessviewhtmlcontent = $fileprocessviewhtmlcontent;
    }

    function setFileprocessviewjsvariable($fileprocessviewjsvariable) {
        $this->fileprocessviewjsvariable = $fileprocessviewjsvariable;
    }

    function setFileprocessviewjsmethod($fileprocessviewjsmethod) {
        $this->fileprocessviewjsmethod = $fileprocessviewjsmethod;
    }

    function setFileprocessviewjswatch($fileprocessviewjswatch) {
        $this->fileprocessviewjswatch = $fileprocessviewjswatch;
    }

    function setFileprocessviewjscreated($fileprocessviewjscreated) {
        $this->fileprocessviewjscreated = $fileprocessviewjscreated;
    }

    function setFileprocessviewjsgeneral($fileprocessviewjsgeneral) {
        $this->fileprocessviewjsgeneral = $fileprocessviewjsgeneral;
    }

    function getFileprocessindexjscreated() {
        return $this->fileprocessindexjscreated;
    }

    function getFileprocessadd() {
        return $this->fileprocessadd;
    }

    function setFileprocessindexjscreated($fileprocessindexjscreated) {
        $this->fileprocessindexjscreated = $fileprocessindexjscreated;
    }

    function setFileprocessadd($fileprocessadd) {
        $this->fileprocessadd = $fileprocessadd;
    }

    function getFileprocessindextemplate() {
        return $this->fileprocessindextemplate;
    }

    function getFileprocessviewtemplate() {
        return $this->fileprocessviewtemplate;
    }

    function getFileprocessdashboardtemplate() {
        return $this->fileprocessdashboardtemplate;
    }

    function setFileprocessindextemplate($fileprocessindextemplate) {
        $this->fileprocessindextemplate = $fileprocessindextemplate;
    }

    function setFileprocessviewtemplate($fileprocessviewtemplate) {
        $this->fileprocessviewtemplate = $fileprocessviewtemplate;
    }

    function setFileprocessdashboardtemplate($fileprocessdashboardtemplate) {
        $this->fileprocessdashboardtemplate = $fileprocessdashboardtemplate;
    }

    function getFileprocessviewjscomponent() {
        return $this->fileprocessviewjscomponent;
    }
	
 function setFileprocessviewjscomponent($fileprocessviewjscomponent) {
        $this->fileprocessviewjscomponent = $fileprocessviewjscomponent;
    }
    function getFileprocessindexjscomponent() {
        return $this->fileprocessindexjscomponent;
    }
	
 function setFileprocessindexjscomponent($fileprocessindexjscomponent) {
        $this->fileprocessindexjscomponent = $fileprocessindexjscomponent;
    }

 function getDashboarddatatype() {
        return $this->dashboarddatatype;
    }
	
 function setDashboarddatatype($dashboarddatatype) {
        $this->dashboarddatatype = $dashboarddatatype;
    }

}
