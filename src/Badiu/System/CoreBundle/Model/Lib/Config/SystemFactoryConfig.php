<?php
namespace Badiu\System\CoreBundle\Model\Lib\Config;
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class SystemFactoryConfig extends BadiuExternalService
{

	/**
      * @var SystemFactoryData
     */
    private $data;
	
	private $inconfig;
	private $outconfig;
	private $bundle;

	private $response;
        private $queryparam=null;
    public function __construct(Container $container)
    {
        parent::__construct($container);
		$this->inconfig=array();
		$this->outconfig=array();
		$this->response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
		
		
		
    }

	

   public function exec() {
   
		$bkey=$this->getBasekey();
		//$this->initDataconfig($bkey);
		$this->initBudleKeymangerConfig($bkey);
		
		$menucontent=$this->getConfigMenu('content');
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($menucontent);
		return $this->getResponse()->get();
		
	}
	
	public function getBasekey() {
                $bkey=$this->getQueyParameter('bkey');
		if(empty($bkey)){
			$key=$this->getQueyParameter('key');
			$bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
		}
		return $bkey;
	}
	public function getModulekey() {
                $mkey=$this->getQueyParameter('mkey');
              
            	if(empty($mkey)){
			$bkey=$this->getQueyParameter('bkey');
			$mkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($bkey);
		}
		
		if(empty($mkey)){
			$key=$this->getQueyParameter('key');
			$keyutil=$this->getContainer()->get('badiu.system.core.lib.config.keyutil');
			$bkey=$keyutil->removeLastItem($key);
			$mkey=$keyutil->removeLastItem($bkey);
		}
		return $mkey;
	}
	public function getPermissionsData() {
		$mkey=$this->getModulekey();
	
		$this->initBudleConfig($mkey);
		
		$plist=array();
		$keyutil=$this->getContainer()->get('badiu.system.core.lib.config.keyutil');
	
                //loop config
		foreach ($this->getInconfig() as $key => $value){
			if($keyutil->endWith($key,'.permission')){
				$keywp=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key) ;
				$plist[$keywp]=$value;
			}
		}
		$outdata=array();
		$outdata['permissions']=$plist;
		$outdata['permissions.label']=$this->getLabel($plist);
		
		return $outdata;
		
	}
        public function getPermissions() {
		$outdata=$this->getPermissionsData();
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($outdata);
		
		return $this->getResponse()->get();
		
	}
	public function getFunctionalityData() {
          
		$mkey=$this->getModulekey();
                $this->initBudleConfig($mkey);
		$plist=array();
		$keyutil=$this->getContainer()->get('badiu.system.core.lib.config.keyutil');
                
		//loop config
		foreach ($this->getInconfig() as $key => $value){
			if($keyutil->endWith($key,'.functionality')){
				$keywp=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key) ;
                                 $httpQueryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
                                $httpQueryString->setQuery($value);
                                $httpQueryString->makeParam();
                               $plist[$keywp]=$httpQueryString->getParam();
			}
		}
		$outdata=array();
		$outdata['functionality']=$plist;
                 return $outdata;
		//$outdata['permissions.label']=$this->getLabel($plist);
		//$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		//$this->getResponse()->setMessage($outdata);
		
		//return $this->getResponse()->get();
		
	}
	public function getCrudRoutes() {
		$bkey=$this->getBasekey();
		$lroutes=$this->getContainer()->get('router')->getRouteCollection()->all();
		$list=array();
		$label=array();
		$index=$bkey.'.index';
		$add=$bkey.'.add';
		$edit=$bkey.'.edit';
		$copy=$bkey.'.copy';
		$delete=$bkey.'.delete';
		$remove=$bkey.'.remove';
		
		
		if (array_key_exists($index,$lroutes)){
			$list['index']=$index;
			$label[$index]=$this->getTranslator()->trans($index);
		}
		if (array_key_exists($add,$lroutes)){
			$list['add']=$add;
			$label[$add]=$this->getTranslator()->trans($add);
		}
		if (array_key_exists($edit,$lroutes)){
			$list['edit']=$edit;
			$label[$edit]=$this->getTranslator()->trans($edit);
		}
		
		if (array_key_exists($copy,$lroutes)){
			$list['copy']=$copy;
			$label[$copy]=$this->getTranslator()->trans($copy);
		}
		if (array_key_exists($delete,$lroutes)){
			$list['delete']=$delete;
			$label[$delete]=$this->getTranslator()->trans($delete);
		}
		if (array_key_exists($remove,$lroutes)){
			$list['remove']=$remove;
			$label[$remove]=$this->getTranslator()->trans($remove);
		}
		$out=array();
		$out['routes']=$list;
		$out['labels']=$label;
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($out);
		
		return $this->getResponse()->get();
	}
	public function initBudleConfig($mkey) {
		
		//get bundle by bkey
                $bundle=null;
                $bundledata=$this->getContainer()->get('badiu.system.module.module.data');
                if($bundledata->exist($mkey)){
                    $bundle=$bundledata->getBundle($mkey);
                }
                //remove last item
                if(empty($bundle)){
                    $mkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($mkey) ;
                    if($bundledata->exist($mkey)){
                        $bundle=$bundledata->getBundle($mkey);
                    }
                }
                 //remove last item
                if(empty($bundle)){
                    $mkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($mkey) ;
                    if($bundledata->exist($mkey)){
                        $bundle=$bundledata->getBundle($mkey);
                    }
                }
		if(!empty($bundle)){
                    $apconfg=$this->getContainer()->get('badiu.system.core.aplic.config');
                    $apconfg->setBundle($bundle);
                    $this->inconfig=$apconfg->getDefaulFormConfig();
                }
		
		
	}
	public function initBudleKeymangerConfig($bkey) {
		 $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
		 $keymanger->setBaseKey($bkey);
                  $this->bundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
		 $this->bundle->setKeymanger($keymanger);
		 $this->bundle->initKeymangerExtend($keymanger);	
		
	}
	public function getLabel($param) {
		$lparam=array();
		foreach ($param as $key => $value){
                        $pos=stripos($key, "_record");
                        if($pos===false){$lparam[$key]=$this->getTranslator()->trans($key);}
                        else{$lparam[$key]=$this->getLabelByService($value);}
		}
		return $lparam;
	}
        public function getLabelByService($config) {
          
                if(empty($config)){return null;}
                $p=explode("|",$config);
                $keyservice=null;
                $function=null;
                $value=null;
              
                if(isset($p[2])){ $keyservice=$p[2];}
                if(isset($p[3])){ $function=$p[3];}
                
                if(!empty($keyservice) && !empty($function)){
                        $execfuncionservice=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice');
                        $paramValue=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
                        $value=$execfuncionservice->exec($keyservice,$function,$paramValue);
                        
                        
                    
                }
		 
		return $value;
	}
	public function getConfigMenu($position="") {
                //check on level2
		$key=$this->getBundle()->getKeymanger()->moduleMenu($position,true);
		$keyExtend=$this->getBundle()->getKeymangerExtend()->moduleMenu($position);
		$param=$this->getBundle()->getValueQueryString($key,$keyExtend);
                
                //check on  level 1
                if(empty($param)){
                    $key=$this->getBundle()->getKeymanger()->moduleMenu($position);
                    $keyExtend=$this->getBundle()->getKeymangerExtend()->moduleMenu($position);
                    $param=$this->getBundle()->getValueQueryString($key,$keyExtend);
                }
               $param=$this->getConfigMenuGlobal($param);
		$kmenu="menu";
		if(!empty($position)){$kmenu='menu.'.$position;}
		$lmenu=array();
		$lmenu[$kmenu]=$param;
		$lmenu[$kmenu.'.label']=$this->getLabel($param);
		return $lmenu;
	}
        public function getConfigMenuGlobal($param) {
            if(!is_array($param)){return $param;}
            $newparam=array();
            $globalkey=null;
          
            if(sizeof($param)==1){
                $cont=0;
                foreach ($param as $k => $v) {
                   $globalkey=trim($k);
               }
             }
            if(empty($globalkey)){return $param;}
             $pos=stripos($globalkey, "l|");
              if($pos!== false && $pos==0){
                 $pl=explode("|",$globalkey);
                 if(isset($pl[1])){$globalkey=$pl[1];}
                 if(!empty($globalkey)){
                     $param=$this->getBundle()->getValueQueryString($globalkey);
                 }
                  
              }
            
           return $param;
        }
	public function getMenu() {
                $bkey=$this->getBasekey();
		$this->initBudleKeymangerConfig($bkey);
		$lmenu=$this->getConfigMenu();
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($lmenu);
                return $this->getResponse()->get();
	}
	public function getMenuContent() {
                $bkey=$this->getBasekey();
		$this->initBudleKeymangerConfig($bkey);
		$lmenu=$this->getConfigMenu('content');
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($lmenu);
		return $this->getResponse()->get();
	}
	public function getMenuNavBar() {
                $bkey=$this->getBasekey();
		$this->initBudleKeymangerConfig($bkey);
		$lmenu=$this->getConfigMenu('navbar');
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($lmenu);
		return $this->getResponse()->get();
	}
	
        public function getAllMenu() {
           
                $bkey=$this->getBasekey();
                
		$this->initBudleKeymangerConfig($bkey);
                
                $lmenu=array();
                $key=$this->getQueyParameter('key');
                $lmenu['keybase']=$key;
                $lmenu['keybaselabel']=$this->getTranslator()->trans($key);
                $menu=$this->getConfigMenu();
               
                array_push($lmenu,$menu);
                
                $menucontent=$this->getConfigMenu('content');
                array_push($lmenu,$menucontent);
             
		$menunavbar=$this->getConfigMenu('navbar');
                array_push($lmenu,$menunavbar);
              
                $lfuncionality=$this->getFunctionalityData();
                 array_push($lmenu,$lfuncionality);
                 
              /* echo "<pre>";
               print_r($lmenu);
                echo "</pre>";exit;*/
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($lmenu);
                return $this->getResponse()->get();
	}
        //desable it return array e not json
	public function getFormConfig() {
		$filter=$this->getQueyParameter('filter');
		$bkey=$this->getBasekey();
		$this->initBudleKeymangerConfig($bkey);
		
		//get menu navbar
		$ltmenunavbar=$this->getConfigMenu('navbar');
		
		$fields=$this->getBundle()->getFormFields($filter);
		//set choicelistvalues
		$param=array();
		$cont=0;
		 foreach ($fields as $field) {
                     
			$optionslistvalues=$this->getFormDefaultOptions($field);
                       	$field->setChoicelistvalues($optionslistvalues);
			$param[$cont]=$field;
			$cont++;
		 }
		
		//
		 $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
         $param = $serializer->serialize($param, 'json');
         $param=json_decode($param, true);
		 
		 //add tranlator label
		 $fieldconfig=array();
		 foreach ($param as $key => $value){
			$value['label']=$this->getTranslator()->trans($value['label']);
			$fieldconfig[$key]=$value;
		 }
                 $formGroupList=null;
                 $formFieldList=null;
		 if($filter){
                        $formGroupList=$this->getBundle()->getGroupOfField($this->getBundle()->getKeymanger()->formFilterFieldsEnable(),$this->getBundle()->getKeymangerExtend()->formFilterFieldsEnable());
                         $formFieldList=$this->getBundle()->getFieldsInGroup($this->getBundle()->getKeymanger()->formFilterFieldsEnable(),$this->getBundle()->getKeymangerExtend()->formFilterFieldsEnable());
             
                 }else{
                     $formGroupList=$this->getBundle()->getGroupOfField($this->getBundle()->getKeymanger()->formDataFieldsEnable(),$this->getBundle()->getKeymangerExtend()->formDataFieldsEnable());
                    $formFieldList=$this->getBundle()->getFieldsInGroup($this->getBundle()->getKeymanger()->formDataFieldsEnable(),$this->getBundle()->getKeymangerExtend()->formDataFieldsEnable());
                  }
		
		//label form group
		$formGroupListLabel=array();
		if(!empty($formGroupList)){
				foreach ($formGroupList as $p){
				$formGroupListLabel[$p]=$this->getTranslator()->trans($p);
			}
		}
		
		 
		 //form config
		
		 $type='data';
		 if($filter==1){$type='filter';}
		 $fconfig=$this->getBundle()->getValueQueryString($this->getBundle()->getKeymanger()->formConfig($type),$this->getBundle()->getKeymangerExtend()->formConfig($type));
		
		$out=array();
		$out['menu.navbar']=$ltmenunavbar['menu.navbar'];
		$out['menu.navbar.label']=$ltmenunavbar['menu.navbar.label'];
		$out['form.config']=$fconfig;
		$out['form.config']=$fconfig;
		$out['form.fields']=$fieldconfig;
		$out['form.group']=$formGroupList;
		$out['form.group.label']=$formGroupListLabel;
		$out['form.fields.ingroup']=$formFieldList;
                $out['report.content.file']=$this->getBundle()->getValueQueryString($this->getBundle()->getKeymanger()->reportContentFile(),$this->getBundle()->getKeymangerExtend()->reportContentFile());
		
		
		 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		 $this->getResponse()->setMessage($out);
		/* echo "<pre>";
		 print_r($out);
		 echo "</pre>";exit;*/
			return $this->getResponse()->get();
	}
	// to replace getFormConfig. return only form config after rename to getFormConfig
        public function getFormConfigDefault($returnjson=true) {
		 $filter=$this->getQueyParameter('filter');
                
		$bkey=$this->getBasekey();
		$this->initBudleKeymangerConfig($bkey);
		
		//get menu navbar
		//$ltmenunavbar=$this->getConfigMenu('navbar');
		
		$fields=$this->getBundle()->getFormFields($filter);
		//set choicelistvalues
		$param=array();
		$cont=0;
		 foreach ($fields as $field) {
                     
			$optionslistvalues=$this->getFormDefaultOptions($field);
                       	$field->setChoicelistvalues($optionslistvalues);
			$param[$cont]=$field;
			$cont++;
		 }
		
		//
		 $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
         $param = $serializer->serialize($param, 'json');
         $param=json_decode($param, true);
		 
		 //add tranlator label
		 $formfield=array();
		 foreach ($param as $key => $value){
			$value['label']=$this->getTranslator()->trans($value['label']);
			$formfields[$key]=$value;
		 }
                 $formGroupList=null;
                 $formFieldList=null;
		 if($filter){
                        $formGroupList=$this->getBundle()->getGroupOfField($this->getBundle()->getKeymanger()->formFilterFieldsEnable(),$this->getBundle()->getKeymangerExtend()->formFilterFieldsEnable());
                         $formFieldList=$this->getBundle()->getFieldsInGroup($this->getBundle()->getKeymanger()->formFilterFieldsEnable(),$this->getBundle()->getKeymangerExtend()->formFilterFieldsEnable());
             
                 }else{
                     $formGroupList=$this->getBundle()->getGroupOfField($this->getBundle()->getKeymanger()->formDataFieldsEnable(),$this->getBundle()->getKeymangerExtend()->formDataFieldsEnable());
                    $formFieldList=$this->getBundle()->getFieldsInGroup($this->getBundle()->getKeymanger()->formDataFieldsEnable(),$this->getBundle()->getKeymangerExtend()->formDataFieldsEnable());
                  }
		
		//label form group
		$formGroupListLabel=array();
		if(!empty($formGroupList)){
                                
				foreach ($formGroupList as  $key => $p){
                                   $value=array('name'=>$p,'label'=>$this->getTranslator()->trans($p));
				$formGroupListLabel[$key]=$value;
			}
                       $formGroupList= $formGroupListLabel;
		}
		
              
                //add field group 
                $paramfield =$formfields;
		 $formfields=array();
		 foreach ($paramfield as $key => $value){
                        $group=null;
                        $fieldkey=$value['name'];
                        if(isset($formFieldList[$fieldkey])){$group=$formFieldList[$fieldkey];}
			$value['group']=$group;
			$formfields[$key]=$value;
		 }
		 
		 //form config
		
		 $type='data';
		 if($filter==1){$type='filter';}
		 $fconfig=$this->getBundle()->getValueQueryString($this->getBundle()->getKeymanger()->formConfig($type),$this->getBundle()->getKeymangerExtend()->formConfig($type));
		
		$out=array();
		//$out['menu.navbar']=$ltmenunavbar['menu.navbar'];
		//$out['menu.navbar.label']=$ltmenunavbar['menu.navbar.label'];
		//$out['form.config']=$fconfig;
		$out['formconfig']=$fconfig;
		$out['formfields']=$formfields;
		$out['formgroup']=$formGroupList;
		$out['formgrouplabel']=$formGroupListLabel;
		//$out['formfieldsingroup']=$formFieldList;
                $out['reportcontentfile']=$this->getBundle()->getValueQueryString($this->getBundle()->getKeymanger()->reportContentFile(),$this->getBundle()->getKeymangerExtend()->reportContentFile());
		
                if(!$returnjson){return $out;}
		 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		 $this->getResponse()->setMessage($out);
		/* echo "<pre>";
		 print_r($out);
		 echo "</pre>";exit;*/
			return $this->getResponse()->get();
	}
	public function getFormDefaultOptions($field) {
		$options=array();
		if($field->getType()=='choice'){
		
			$formfactoy=new \Badiu\System\CoreBundle\Model\Lib\Form\FormFactory($this->getContainer());
			$options=$formfactoy->getDefaultChoice($field);
		}else if($field->getType()=='entity'){
		
			$formfactoy=new \Badiu\System\CoreBundle\Model\Lib\Form\FormFactory($this->getContainer());
			$options=$formfactoy->getEntityChoice($field);
		}
		return $options;
		
	}
	public function getTableViewLink() {
		$bkey=$this->getBasekey();
		$this->initBudleKeymangerConfig($bkey);
		$param=$this->getBundle()->getValueQueryString($this->getBundle()->getKeymanger()->dbsearchFieldsTableViewLink(),$this->getBundle()->getKeymangerExtend()->dbsearchFieldsTableViewLink());
		 $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		 $this->getResponse()->setMessage($param);
		return $this->getResponse()->get();
	}
	public function getDbsearch() {
		
	}
	
        
        function getQueyParameter($key) {
        $value=null;
        if(empty($this->queryparam)){$this->queryparam=$this->getHttpQuerStringParam();}
        if(isset($this->queryparam[$key])){$value=$this->queryparam[$key];}
        return $value;
    }        
	   /**
     * @return SystemFactoryData
     */
    public function getSystemFactoryData()
    {
        return $this->systemFactoryData;
    }


    public function setSystemFactoryData(SystemFactoryData $systemFactoryData)
    {
        $this->systemFactoryData = $systemFactoryData;
    }
	
	  public function getBundle()
    {
        return $this->bundle;
    }


    public function setBundle($bundle)
    {
        $this->bundle = $bundle;
    }  
	  public function getInconfig()
    {
        return $this->inconfig;
    }


    public function setInconfig($inconfig)
    {
        $this->inconfig = $inconfig;
    } 
	  public function getOutconfig()
    {
        return $this->outconfig;
    }


    public function setOutconfig($outconfig)
    {
        $this->outconfig = $outconfig;
    } 
	
	 public function getResponse() {
          return $this->response;
      }


      public function setResponse($response) {
          $this->response = $response;
      }
      function getData() {
          return $this->data;
      }

      function getQueryparam() {
          return $this->queryparam;
      }

      function setData(SystemFactoryData $data) {
          $this->data = $data;
      }

      function setQueryparam($queryparam) {
          $this->queryparam = $queryparam;
      }


}
