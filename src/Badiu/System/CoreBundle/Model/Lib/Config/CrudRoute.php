<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Config;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger;
/**
 * Description of MakeSelect
 *
 * @author lino
 */
class CrudRoute {
     
   /**
     * @var Container
     */
    private $container;

     /**
      * @var BadiuKeyManger
     */
    private $keymanger;

      /**
     * @var string
     */
    private $index;
    /**
     * @var string
     */
    private $view; 
    
    /**
     * @var string
     */
    private $detail;

     /**
     * @var string
     */
    private $add;

     /**
     * @var string
     */
    private $edit;

    /**
     * @var string
     */
    private $copy;

    /**
     * @var string
     */
    private $delete;

     /**
     * @var string
     */
    private $remove;

   function __construct(Container $container,$param=null,BadiuKeyManger $keymanger=null) {
                $this->view="";
                $this->index="";
                $this->detail="";
                $this->add="";
                $this->edit="";
                $this->copy="";
                $this->delete="";
                $this->remove="";
                 if($param!=null){$this->initParam($param);}
                
                if($keymanger!=null){
                    $this->keymanger=$keymanger;
                    $this->initDefault();
                }

                  

     }
    
     function initParam($param) {

               if (array_key_exists("view",$param)){$this->view=$param["view"];}
               if (array_key_exists("index",$param)){$this->index=$param["index"];}
               if (array_key_exists("detail",$param)){$this->detail=$param["detail"];}
               if (array_key_exists("add",$param)){$this->add=$param["add"];}
               if (array_key_exists("edit",$param)){$this->edit=$param["edit"];}
               if (array_key_exists("copy",$param)){$this->copy=$param["copy"];}
               if (array_key_exists("delete",$param)){$this->delete=$param["delete"];}
               if (array_key_exists("remove",$param)){$this->remove=$param["remove"];}
            }  
   
    function initDefault() {
     
      
            if(empty($this->view)){$this->view=$this->keymanger->routeView();}
            if(empty($this->index)){$this->index=$this->keymanger->routeIndex();}
            if(empty($this->detail)){$this->detail=$this->keymanger->routeDetail();}
            if(empty($this->add)){$this->add=$this->keymanger->routeAdd();}
            if(empty($this->edit)){$this->edit=$this->keymanger->routeEdit();}
            if(empty($this->copy)){$this->copy=$this->keymanger->routeCopy();}
            if(empty($this->delete)){$this->delete=$this->keymanger->routeDelete();}
            if(empty($this->remove)){$this->remove=$this->keymanger->routeRemove();}

         }
         public function getContainer() {
             return $this->container;
         }

         public function getKeymanger() {
             return $this->keymanger;
         }

         public function getView() {
             return $this->view;
         }

         public function getDetail() {
             return $this->detail;
         }

         public function getAdd() {
             return $this->add;
         }

         public function getEdit() {
             return $this->edit;
         }

         public function getCopy() {
             return $this->copy;
         }

         public function getDelete() {
             return $this->delete;
         }

         public function getRemove() {
             return $this->remove;
         }

         public function setContainer(Container $container) {
             $this->container = $container;
         }

         public function setKeymanger(BadiuKeyManger $keymanger) {
             $this->keymanger = $keymanger;
         }

         public function setView($view) {
             $this->view = $view;
         }

         public function setDetail($detail) {
             $this->detail = $detail;
         }

         public function setAdd($add) {
             $this->add = $add;
         }

         public function setEdit($edit) {
             $this->edit = $edit;
         }

         public function setCopy($copy) {
             $this->copy = $copy;
         }

         public function setDelete($delete) {
             $this->delete = $delete;
         }

         public function setRemove($remove) {
             $this->remove = $remove;
         }
         function getIndex() {
             return $this->index;
         }

         function setIndex($index) {
             $this->index = $index;
         }



}
