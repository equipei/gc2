<?php


namespace Badiu\System\CoreBundle\Model\Lib\Config;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Config\KEYDOMAINTABLE;
class ParamConfig{
  
     /**
     * @var Container
     */
    private $container;
	
	/**
     * @var string
     */
    private $param;
	
	function __construct(Container $container) {
                $this->container=$container;
			}
			
  public function initParam($param) {
  
	if(empty($param))return array(); 
	 if(is_array($param))return array(); 
	 $querystring=$this->container->get('badiu.system.core.lib.http.querystring');
	 $querystring->setQuery($param);
	 $querystring->makeParam(); 
	 $param=$querystring->getParam();
	
	 return $param;
  }
  
    public function getValue($param,$key) {
			$value=null;
			if(!empty($param)){$param=$this->initParam($param);}
			else {$param=$this->getParam();}
			if(empty($param)) return null;
			if (array_key_exists($key,$param)){$value=$param[$key];}
			return $value;
      }
  public function getType($param=null) {
		$value=$this->getValue($param,"type");
		return $value;
      }
	
	 public function getDefault($param=null) {
			$value=$this->getValue($param,"default");
			return $value;
      }
	 public function getLevel($param=null) {
			$value=$this->getValue($param,"level");
			return $value;
      }  
	 public function getChoices($param) {
			
           return  false;
      }
	
	  public function getContainer() {
           return $this->container;
       }

       public function setContainer(Container $container) {
           $this->container = $container;
       }

	    public function getParam() {
         return $this->param;
     }
	 
	   public function setParam($param) {
           $this->param = $param;
       }
}
