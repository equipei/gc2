<?php

namespace Badiu\System\CoreBundle\Model\Lib\Config;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class RouterLib extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }



	
	public function hasParentParameter($key=null) {
		
		 if(empty($key)){$key=$this->getKey($key);}
		
		$parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		if(!empty($parentid)){
			$dkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey');
			if(!empty($dkey)){return true;}
			$pkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_key');
			if(!empty($pkey)){return true;}
		}
			
		$router=$this->getContainer()->get('router');
		$rfinfo=$router->getRouteCollection()->get($key);
		$path=$rfinfo->getPath();
		$pos=stripos($path, "{parentid}");
        if($pos!== false){return true;}
		return false;
	}
	
	public function getKey() {
		$key=null;
		
			$dkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey');
			$pkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_key');
			if(!empty($pkey)){$key=$pkey;}
			if(!empty($dkey)){$key=$dkey;}
		    if(empty($key)){$key= $this->getContainer()->get('request')->get('_route');}
	
		return $key;
	}
}
