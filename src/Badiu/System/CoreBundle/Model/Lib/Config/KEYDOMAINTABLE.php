<?php
namespace Badiu\System\CoreBundle\Model\Lib\Config;
class KEYDOMAINTABLE {

	
    public static  $PERMISSION   ="permission";
    public static  $MENU   ="menu";
	public static  $LABEL   ="label";
	public static  $CONFIG   ="config";
	public static  $PARENT   ="parent";
	public static  $FORM_FILTER   ="formfilter";
	public static  $FORM_DATA   ="formdata";
    public static  $DB_SEARCH   ="dbsearch";
	public static  $DASHBOARD   ="dashboard";

}
?>