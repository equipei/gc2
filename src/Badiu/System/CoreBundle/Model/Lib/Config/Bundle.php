<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Config;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Form\FormField;
use Badiu\System\CoreBundle\Model\Lib\Config\DbField; 
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger;    
use Badiu\System\CoreBundle\Model\Lib\Form\FormConfig;
use Badiu\System\CoreBundle\Model\Lib\Config\CrudRoute;
/**
 * Description of MakeSelect
 *
 * @author lino
 */
class Bundle{
    
  /**
     * @var Container
     */
    private $container;


      /**
      * @var BadiuKeyManger
     */
    private $keymanger;

    /**
     * @var BadiuKeyManger
     */
    private $keymangerExtend;
    
    /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
	private $systemdata;
    function __construct(Container $container) {
                $this->container=$container;
          
      }
       public function getFormFields($filter=false,BadiuKeyManger $keymanger=null){
           if($keymanger!=null){$this->keymanger=$keymanger;}
           $this->initKeymangerExtend($this->keymanger);

            $fields=null;
            if($filter){$fields=$this->getFieldsGrouped($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());}
            else {$fields=$this->getFieldsGrouped($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());}
           
           //required
           $fieldRequired=null;
           if($filter){ $fieldRequired=$this->getValueQueryString($this->getKeymanger()->formFilterFieldsRequired(),$this->getKeymangerExtend()->formFilterFieldsRequired());}
           else { $fieldRequired=$this->getValueQueryString($this->getKeymanger()->formDataFieldsRequired(),$this->getKeymangerExtend()->formDataFieldsRequired());}

           //class
           $fieldCssClass=null;
           if($filter){$fieldCssClass=$this->getValueQueryString($this->getKeymanger()->formFilterFieldsCssClass(),$this->getKeymangerExtend()->formFilterFieldsCssClass());}
           else {$fieldCssClass=$this->getValueQueryString($this->getKeymanger()->formDataFieldsCssClass(),$this->getKeymangerExtend()->formDataFieldsCssClass());}
           
            //class
           $fieldFormat=null;
           if($filter){$fieldFormat=$this->getValueQueryString($this->getKeymanger()->formFilterFieldsFormat(),$this->getKeymangerExtend()->formFilterFieldsFormat());}
           else {$fieldFormat=$this->getValueQueryString($this->getKeymanger()->formDataFieldsFormat(),$this->getKeymangerExtend()->formDataFieldsFormat());}
           
           //type
            $types=null;
            if($filter){$types=$this->getValueQueryString($this->getKeymanger()->formFilterFieldsType(),$this->getKeymangerExtend()->formFilterFieldsType());}
            else {$types=$this->getValueQueryString($this->getKeymanger()->formDataFieldsType(),$this->getKeymangerExtend()->formDataFieldsType());}
            
            //config
            $configs=null;
            if($filter){$configs=$this->getValueQueryString($this->getKeymanger()->formFilterFieldsConfig(),$this->getKeymangerExtend()->formFilterFieldsConfig());}
            else {$configs=$this->getValueQueryString($this->getKeymanger()->formDataFieldsConfig(),$this->getKeymangerExtend()->formDataFieldsConfig());}
          
		  //action
		 
            $actions=null;
            if($filter){$actions=$this->getValueQueryString($this->getKeymanger()->formFilterFieldsAction(),$this->getKeymangerExtend()->formFilterFieldsAction());}
            else {$actions=$this->getValueQueryString($this->getKeymanger()->formDataFieldsAction(),$this->getKeymangerExtend()->formDataFieldsAction());}
       
           //choicelist
           $choicelists=null;
            if($filter){$choicelists=$this->getValueQueryString($this->getKeymanger()->formFilterFieldsChoicelist(),$this->getKeymangerExtend()->formFilterFieldsChoicelist());}
            else {$choicelists=$this->getValueQueryString($this->getKeymanger()->formDataFieldsChoicelist(),$this->getKeymangerExtend()->formDataFieldsChoicelist());}
            
			
            
            //label
            $labels=null;
            if($filter){$labels=$this->getValueQueryString($this->getKeymanger()->formFilterFieldsLabel(),$this->getKeymangerExtend()->formFilterFieldsLabel());}
            else {$labels=$this->getValueQueryString($this->getKeymanger()->formDataFieldsLabel(),$this->getKeymangerExtend()->formDataFieldsLabel());}
            
             //placeholder
            $placeholders=null;
            if($filter){$placeholders=$this->getValueQueryString($this->getKeymanger()->formFilterFieldsPlaceholder(),$this->getKeymangerExtend()->formFilterFieldsPlaceholder());}
            else {$placeholders=$this->getValueQueryString($this->getKeymanger()->formDataFieldsPlaceholder(),$this->getKeymangerExtend()->formDataFieldsPlaceholder());}
         
             $translator = $this->getContainer()->get('translator');
            //
           //loop fields
           $data=array();
           if(!empty($fields)){
               foreach ($fields as $field) {
                 
                   //required
                   $required=false;
                   if(array_key_exists($field,$fieldRequired)){
                       $required=$fieldRequired[$field];
                       if($required=='0'){$required=false;}
                       else if($required=='1'){$required=true;}
                   }
                   
                    //type
                    $type="text";
                    if(array_key_exists($field,$types)){
                        $type=$types[$field];
                    }
                     //config
                     $config="";
                     if(array_key_exists($field,$configs)){
                        $config=$configs[$field];
                     }
					
					//action
                     $action="";
                     if(array_key_exists($field,$actions)){
                        $action=$actions[$field];
						$action=str_replace("|","=",$action);
					 } 
					 if($field=="gsearch" && empty($action)){$action='@keyup.enter="search()"';}
                     // css 
                   $cssClasse=" form-control ";
				   if($type=='textarea' && $field!= 'param'){$cssClasse=" form-control  ckeditor";}
                    if(array_key_exists($field,$fieldCssClass)){
                        $cssClasse=$fieldCssClass[$field];
                    }
                    
                   // format
                   $format="";
		    if($type=='date'){$format='dd/MM/yyyy';}
                   
                    if(array_key_exists($field,$fieldFormat)){
                        $format=$fieldFormat[$field];
                        if($format=='defaultdate'){$format='dd/MM/yyyy';}
                        else if($format=='defaultdatetime'){$format='dd/MM/yyyy HH:mm';}
                    }
                    
                    //choicelist
                    $choicelist=null;
                    if(array_key_exists($field,$choicelists)){
                        $choicelist=$choicelists[$field];
                    }
                    
                    //label
                     $label=$field;
                  
                      if(array_key_exists($field,$labels)){
                         $label=$labels[$field];
                    }
                 //placeholder
                    $placeholder=null;
                  
                      if(array_key_exists($field,$placeholders)){
                         $placeholder=$placeholders[$field];
                         $placeholder=$translator->trans($placeholder);
                    }
                   
                    
                   $dconfig=new FormField();
                   $dconfig->setName($field);
                   $dconfig->setRequired($required);
                   $dconfig->setCssClasss($cssClasse);
                   $dconfig->setFormat($format);
                   $dconfig->setType($type);
                   $dconfig->setConfig($config);
				    $dconfig->setAction($action);
                   $dconfig->setChoicelist($choicelist);
                   $dconfig->setLabel($label);
                   $dconfig->setPlaceholder($placeholder);
                   
                   array_push($data,$dconfig);
               } 
           }
         
         return $data;
           
           
      }
    
    //review delete
    public function getDbSearchFields(BadiuKeyManger $keymanger=null){
            if($keymanger!=null){$this->keymanger=$keymanger;}
           $fields=$this->getValue($this->getKeymanger()->dbsearchFieldsEnable());
                if(empty($fields)){
                $this->initKeymangerExtend($this->keymanger);
                $fields=$this->getValue($this->getKeymangerExtend()->dbsearchFieldsEnable());
            }
           return $fields;
       } 
     public function getDbTableViewFields(BadiuKeyManger $keymanger=null,$scheduler=false,$param=array()){
                 if($keymanger!=null){$this->keymanger=$keymanger;}
            $this->initKeymangerExtend($this->keymanger);

		 $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
		 $addcolumndynamically= $utildata->getVaueOfArray($param,'_addcolumndynamically');
		 $listcolumndynamically= $utildata->getVaueOfArray($param,'_listcolumndynamically');

            $fields=null;
            if($scheduler){ 
                $fields=$this->getFields($this->getKeymanger()->dbsearchFieldsTableViewScheduler(),$this->getKeymangerExtend()->dbsearchFieldsTableViewScheduler());
                if(empty($fields)){$fields=$this->getFields($this->getKeymanger()->dbsearchFieldsTableView(),$this->getKeymangerExtend()->dbsearchFieldsTableView());}
            }
            else{$fields=$this->getFields($this->getKeymanger()->dbsearchFieldsTableView(),$this->getKeymangerExtend()->dbsearchFieldsTableView());}
         
			//check if is for webservice
			$stoken = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_stoken','ajaxws');
			if(!empty($stoken)){
				$wsfields=$this->getFields($this->getKeymanger()->dbsearchFieldsTableViewWebservice(),$this->getKeymangerExtend()->dbsearchFieldsTableViewWebservice());
				if(!empty($wsfields)){$fields=$wsfields;}
			}
		 
		
		 if($addcolumndynamically && is_array($listcolumndynamically)){$fields=$listcolumndynamically;}
         
		 //else{   

           //type - desable it
         //  $fieldType=$this->getValueQueryString($this->getKeymanger()->dbsearchFieldsType(),$this->getKeymangerExtend()->dbsearchFieldsType());
           
           //format
           $fieldPatternFormat=$this->getValueQueryString($this->getKeymanger()->dbsearchFieldsFormat(),$this->getKeymangerExtend()->dbsearchFieldsFormat());
          
           //link
           $fieldLink=$this->getValueQueryString($this->getKeymanger()->dbsearchFieldsTableViewLink(),$this->getKeymangerExtend()->dbsearchFieldsTableViewLink());
           
		   //label
            $labels=null;
            $labels=$this->getValueQueryString($this->getKeymanger()->dbsearchFieldsTableViewLabel(),$this->getKeymangerExtend()->dbsearchFieldsTableViewLabel());
             $dashboardSql=$this->getValueQueryString($this->getKeymanger()->dbsearchFieldsTableViewDashboardSqlKey(),$this->getKeymangerExtend()->dbsearchFieldsTableViewDashboardSqlKey());
         
		      $data=$this->dataView($fields,$fieldPatternFormat,$fieldLink,$labels,$dashboardSql);
           
         return $data;
         
       }

 public function getDbTableViewFieldsType(BadiuKeyManger $keymanger=null){
	   if($keymanger!=null){$this->keymanger=$keymanger;}
       $this->initKeymangerExtend($this->keymanger);

       $fieldstype=$this->getValueQueryString($this->getKeymanger()->dbsearchFieldsTableViewType(),$this->getKeymangerExtend()->dbsearchFieldsTableViewType());
	   return $fieldstype;
 }
  public function getDbRowTableViewFields(BadiuKeyManger $keymanger=null,$param=array()){
                 if($keymanger!=null){$this->keymanger=$keymanger;}
         $this->initKeymangerExtend($this->keymanger);
		 $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
		 $addcolumndynamically= $utildata->getVaueOfArray($param,'_addcolumndynamically');
		 $listcolumndynamically= $utildata->getVaueOfArray($param,'_listcolumndynamically');

		 $fields=array();
		 if($addcolumndynamically && is_array($listcolumndynamically)){$fields=$listcolumndynamically;}
         else{   $fields=$this->getFieldsGrouped($this->getKeymanger()->dbgetrowFieldsTableView(),$this->getKeymangerExtend()->dbgetrowFieldsTableView());}

           //type - desable it
          // $fieldType=$this->getValueQueryString($this->getKeymanger()->dbgetrowFieldsType(),$this->getKeymangerExtend()->dbgetrowFieldsType());
           
           //format
           $fieldPatternFormat=$this->getValueQueryString($this->getKeymanger()->dbgetrowFieldsFormat(),$this->getKeymangerExtend()->dbgetrowFieldsFormat());
          
           //link
           $fieldLink=$this->getValueQueryString($this->getKeymanger()->dbgetrowFieldsTableViewLink(),$this->getKeymangerExtend()->dbgetrowFieldsTableViewLink());
           
       //label
            $labels=null;
            $labels=$this->getValueQueryString($this->getKeymanger()->dbgetrowFieldsTableViewLabel(),$this->getKeymangerExtend()->dbgetrowFieldsTableViewLabel());

            $dashboardSql=$this->getValueQueryString($this->getKeymanger()->dbgetrowFieldsTableViewDashboardSqlKey(),$this->getKeymangerExtend()->dbgetrowFieldsTableViewDashboardSqlKey());
           

          $data=$this->dataView($fields,$fieldPatternFormat,$fieldLink,$labels,$dashboardSql);
           
         return $data;
         
       }
	   
	   
       public function dataView($fields,$fieldPatternFormat,$fieldLink,$labels,$dashboardSql){

            $htmllink=$this->getContainer()->get('badiu.system.core.lib.html.link');
            //loop fields
           $data=array();
           if(!empty($fields)){
               foreach ($fields as $field) {
                   
                   //add required
                /*   $type=null;
                   if(array_key_exists($field,$fieldType)){
                       $type=$fieldType[$field];
                    }*/
                   
                     //add css class
                   $patternFormat=null;
                    if(array_key_exists($field,$fieldPatternFormat)){
                        $patternFormat=$fieldPatternFormat[$field];
                    }
                
                    $link=null;
                    if(array_key_exists($field,$fieldLink)){
                        $link=$fieldLink[$field];
                    }
        
           //label
                     $label=$field;
                  
                      if(array_key_exists($field,$labels)){
                         $label=$labels[$field];
                      }
                      $dbsqlIndex="";
                      $dbsqlKey="";
                       if(array_key_exists($field,$dashboardSql)){
                         $dbsql=$dashboardSql[$field];
                         $p=explode("|", $dbsql);
                         if(isset($p[0])){$dbsqlIndex=$p[0];}
                         if(isset($p[1])){$dbsqlKey=$p[1];}
                      }
                      
                   $plink=$htmllink->makeParamByConfig($link);
                   $fconfig=new DbField();
                   $fconfig->setName($field);
                   $fconfig->setLabel($label);

                   //$fconfig->setInfo(array('type'=>$type,'pattern'=>$patternFormat,'link'=>$plink));
                    $fconfig->setInfo(array('format'=>$patternFormat,'link'=>$plink,'dashboardsqlindex'=>$dbsqlIndex,'dashboardsqlkey'=>$dbsqlKey));
                  
                   array_push($data,$fconfig);
               }
           }
           
        return $data;

       }
       
        public function  getDbTableViewOperationConfirm(BadiuKeyManger $keymanger=null){
            if($keymanger!=null){$this->keymanger=$keymanger;}
           $this->initKeymangerExtend($this->keymanger);
           //get lebel
           $clabels=$this->getValueQueryString($this->getKeymanger()->dbsearchFieldsTableViewOperationConfirm(),$this->getKeymangerExtend()->dbsearchFieldsTableViewOperationConfirm());
          $msg=new CrudMessage();
          if(isset($clabels['deletelabel'])&& !empty($clabels['deletelabel'])){$msg->setDeletelabel($clabels['deletelabel']);}
          if(isset($clabels['deletecdfield'])&& !empty($clabels['deletecdfield'])){$msg->setDeletecdfield($clabels['deletecdfield']);}
          if(isset($clabels['deletelabelsuccess'])&& !empty($clabels['deletelabelsuccess'])){$msg->setDeletelabelsuccess($clabels['deletelabelsuccess']);}
          
          if(isset($clabels['removelabel'])&& !empty($clabels['removelabel'])){$msg->setremovelabel($clabels['removelabel']);}
          if(isset($clabels['removecdfield'])&& !empty($clabels['removecdfield'])){$msg->setremovecdfield($clabels['removecdfield']);}
          if(isset($clabels['removelabelsuccess'])&& !empty($clabels['removelabelsuccess'])){$msg->setremovelabelsuccess($clabels['removelabelsuccess']);}
              
         return $msg;
         
       }
       public function getDbRowViewFields(BadiuKeyManger $keymanger=null){
            if($keymanger!=null){$this->keymanger=$keymanger;}
           $this->initKeymangerExtend($this->keymanger);
           //get fields
           $fields=$this->getFields($this->getKeymanger()->dbsearchFieldsRowView(),$this->getKeymangerExtend()->dbsearchFieldsRowView());
           
           //type
           $fieldType=$this->getValueQueryString($this->getKeymanger()->dbsearchFieldsType(),$this->getKeymangerExtend()->dbsearchFieldsType());
           
           $fieldPatternFormat=$this->getValueQueryString($this->getKeymanger()->dbsearchFieldsPatternFormat(),$this->getKeymangerExtend()->dbsearchFieldsPatternFormat());
          
         
           //loop fields
           $data=array();
           if(!empty($fields)){
               foreach ($fields as $field) {
                   
                   //add required
                   $type=null;
                   if(array_key_exists($field,$fieldType)){
                       $type=$fieldType[$field];
                    }
                   
                     //add css class
                   $patternFormat=null;
                    if(array_key_exists($field,$fieldPatternFormat)){
                        $patternFormat=$fieldPatternFormat[$field];
                    }
                   $fconfig=new DbField();
                   $fconfig->setName($field);
                   $fconfig->setInfo(array('type'=>$type,'pattern'=>$patternFormat));
                  
                   array_push($data,$fconfig);
               }
           }
           
           
         return $data;
         
       }

     public function getFormConfig($type,BadiuKeyManger $keymanger=null){
         
         if($keymanger!=null){$this->keymanger=$keymanger;}
         $this->initKeymangerExtend($this->keymanger);
            //config
          
           $pconfig=$this->getValueQueryString($this->getKeymanger()->formConfig($type),$this->getKeymangerExtend()->formConfig($type));
           $formConfig=new FormConfig($this->getContainer(),$pconfig,$type);
           $formConfig->setSessionhashkey($this->getSessionhashkey());
		   $formConfig->setSystemdata($this->getSystemdata());
           $formConfig->setBasekey($this->getKeymangerExtend()->getBaseKey());
           $formConfig->overrideDbConfig();
         
         return $formConfig;
         
       }
    public function getPageConfig(BadiuKeyManger $keymanger=null){
         if($keymanger!=null){$this->keymanger=$keymanger;}
         $this->initKeymangerExtend($this->keymanger);
            //config
          
           $pconfig=$this->getValueQueryString($this->getKeymanger()->pageConfig(),$this->getKeymangerExtend()->pageConfig());

           $pageConfig=new  PageConfig($this->getContainer(),$pconfig);

           
         return $pageConfig;
         
       }
        public function getCrudRoute(BadiuKeyManger $keymanger=null){
         if($keymanger!=null){$this->keymanger=$keymanger;}
          
            //config
           $pconfig=$this->getValueQueryString($this->getKeymanger()->routeGrud());
           $grudRoute=new CrudRoute($this->getContainer(),$pconfig,$keymanger);

           
         return $grudRoute;
         
       }

    public function getEntityParent(BadiuKeyManger $keymanger=null){
        if($keymanger!=null){$this->keymanger=$keymanger;}
         $this->initKeymangerExtend($this->keymanger);
        //config
        $pconfig=$this->getValueQueryString($this->getKeymanger()->parentConfig(),$this->getKeymangerExtend()->parentConfig());
        $entityParent=new EntityParent($this->getContainer(),$pconfig);


        return $entityParent;

    }

     public function getValue($key){
         $value=null;
         $inSession=FALSE;
         
        //get in session
        $value=$this->getSession($key);
        return $value;
        
        //echo "$key - data in session: $value";exit;
            
        //get in database
         if($value==null && trim($value)==""){
             //make logic to get in database
         }
         
        // get in config
          if($value==null || trim($value)==""){
              //echo "Acesso config ----------------------------<br />";
            $value=$this->getConfig($key);
           
         }
         //save in session
        /* if(!$inSession && $value!=null && trim($value)!=""){
             $this->saveSession($key,$value);
         }*/
          return $value;
      }  
      
    
       public function getSession($key){
			//if(!empty($this->getSystemdata())){return $this->getSystemdata()->getManagecache()->getCache($key);}
            $badiuSession=$this->getContainer()->get('badiu.system.access.session');
            $badiuSession->setHashkey($this->getSessionhashkey());
			$badiuSession->setSystemdata($this->getSystemdata());
            $value= $badiuSession->getValue($key);
            return $value;
			
			if(!$badiuSession->get()->getConfig()->exist()){
				 $badiuSession->initConfigEntity();
            }
			
			/*$globaldatasession= $this->getContainer()->get('badiu.system.access.globaldatasession');
			$sdata=$globaldatasession->getParam();
            if(!is_array($sdata)){
			 $badiuSession->initConfigEntity();
            }
			if(is_array($sdata) && sizeof($sdata)==0){
				$badiuSession->initConfigEntity();
            }*/
            $parentid=$this->container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
            if(!empty($parentid)){
                $badiuSession->initConfigInstance();
                $intancekey=$key.'.'.$parentid;
                $existeinstancekey=$badiuSession->existValue($intancekey);
                if($existeinstancekey){
                   $value= $badiuSession->getValue($intancekey);
                   return $value;
                } 
            }
           
            $value= $badiuSession->getValue($key);
            return $value;
       }
      public function saveSession($key,$value){
         $badiuSession=$this->getContainer()->get('badiu.system.access.session');
         $badiuSession->setHashkey($this->getSessionhashkey());
         $sessionData=$badiuSession->get();
         $sessionData->getConfig()->add($key,$value);
         $badiuSession->save($sessionData);
        
      }
      
      public function getConfig($key){
         $paramConfig=$this->getContainer()->get($this->getKeymanger()->moduleConfig())->getDefaulFormConfig();
       
         if(array_key_exists($key,$paramConfig)){
             return $paramConfig[$key];
         }
         else return null;
       }
      public function getFields($key=null,$keyExtend=null){
          $order=array();

         $value=$this->getValue($key);
         if(empty($value)){$value=$this->getValue($keyExtend);}
          if(!empty($value)){
              $pos=stripos($value, ",");
              if($pos=== false){
                  $order=array($value);
              }else{
                  $order= explode(",", $value);
              }
          }
          return $order;
      }

    public function getFieldsGrouped($key=null,$keyExtend=null){
        //check if existe group by find string &
        $value=$this->getValue($key);
        if(empty($value)){$value=$this->getValue($keyExtend);}
        $pos=stripos($value, "&");
        if($pos=== false){
			$pos=stripos($value, "=");
			if($pos=== false){return $this->getFields($key,$keyExtend);}
			else{
			
				$gfieds= explode("=", $value);
				$olfields=$gfieds[1];
				$lfields=explode(",", $olfields);
				return $lfields;
			}
            return $this->getFields($key,$keyExtend);
        }

        //get grouped
        $fieldsGroup=$this->getValueQueryString($key,$keyExtend);
        $orders=array();
        if(empty($fieldsGroup)) return  $orders;

        foreach ($fieldsGroup as $value) {
            $pos=stripos($value, ",");
            if($pos=== false){
                $order=array($value);
            }else{
                $order= explode(",", $value);
            }
            $orders=array_merge($orders,$order);
        }


        return $orders;
    }

     public function getGroupOfField($key=null,$keyExtend=null){
        //check if existe group by find string &
        $value=$this->getValue($key);
        if(empty($value)){$value=$this->getValue($keyExtend);}
        $pos=stripos($value, "&");
        if($pos=== false){
		
			$pos=stripos($value, "=");
			if($pos=== false){return null;}
			else{
				$order= explode("=", $value);
				$group=array();
				$group[0]=$order[0];
				
				return $group;
			}
            return null;
        }

        //get grouped
        $fieldsGroup=$this->getValueQueryString($key,$keyExtend);
        $group=array();
        if(empty($fieldsGroup)) return  null;
        $cont=0;
        foreach ($fieldsGroup as $key => $value) {
            $group[$cont]=$key;
            $cont++;
        }


        return $group;
    }
       
    public function getFieldsInGroup($key=null,$keyExtend=null){
        //check if existe group by find string &
        $value=$this->getValue($key);
        if(empty($value)){$value=$this->getValue($keyExtend);}
        $pos=stripos($value, "&");
        if($pos=== false){
			$pos=stripos($value, "=");
			if($pos=== false){return null;}
			else{
				$gfieds= explode("=", $value);
				$group=$gfieds[0];
				$olfields=$gfieds[1];
				$lfields=explode(",", $olfields);
				$fieldgrup=array();
				 foreach ($lfields  as $gf) {
					$fieldgrup[$gf]=$group;
				 }
				return $fieldgrup;
			}
            return null;
        }

        //get grouped
        $fieldsGroup=$this->getValueQueryString($key,$keyExtend);
        $fields=array();
        if(empty($fieldsGroup)) return  null;

        foreach ($fieldsGroup  as $key => $value) {
            $pos=stripos($value, ",");
            if($pos=== false){
                $fields[$value]= $key;
            }else{
                $list= explode(",", $value);
                foreach ($list  as $l) {
                      $fields[$l]= $key;
                }

            }
            
        }


        return $fields;
    }
    public function getValueQueryString($key,$keyExtend=null){
            $value=$this->getValue($key);
           if(empty($value)){$value=$this->getValue($keyExtend);}
           
           $httpQueryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
           $httpQueryString->setQuery($value);
           $httpQueryString->makeParam();
           return $httpQueryString->getParam();
       }
 
    public function getValueInherit($key,$keyExtend){
            $value=$this->getValue($key);
           if(empty($value)){$value=$this->getValue($keyExtend);}
           
           return $value;
       }   
    public function initKeymangerExtend(BadiuKeyManger $keymanger) {
        if($this->keymanger==null){$this->keymanger=$keymanger;}
        $extendKey=$keymanger->extendModuleKey();
        $extendValue= $this->getValue($extendKey);
        $currentBaseKey=$keymanger->getBaseKey();
        $dyncoriginalKey=$keymanger->getBaseKey();
        if(empty($extendValue)) {
            $extendesKeyManeger=new BadiuKeyManger();
            $extendesKeyManeger->setBaseKey($currentBaseKey);
            $extendesKeyManeger->setBaseKeydyncoriginal($dyncoriginalKey);
            
			$extendesKeyManeger->setContainer($this->container);
            $this->keymangerExtend=$extendesKeyManeger;

        }else{
            $listCurrentKey = explode(".", $currentBaseKey);
            $lastItemKey=end($listCurrentKey);
            $extendBaseKey=$extendValue.".".$lastItemKey;

            $extendesKeyManeger=new BadiuKeyManger();
            $extendesKeyManeger->setBaseKey($extendBaseKey);
            $extendesKeyManeger->setBaseKeydyncoriginal($dyncoriginalKey);
			$extendesKeyManeger->setContainer($this->container);
            $this->keymangerExtend=$extendesKeyManeger;

        }


    }
      public function getContainer() {
          return $this->container;
      }

    
      public function setContainer(Container $container) {
          $this->container = $container;
      }


   
  public function getKeymanger() {
      return $this->keymanger;
  }

  public function setKeymanger(BadiuKeyManger $keymanger) {
      $this->keymanger = $keymanger;
  }

    /**
     * @return BadiuKeyManger
     */
    public function getKeymangerExtend()
    {
        return $this->keymangerExtend;
    }


    public function setKeymangerExtend(BadiuKeyManger $keymangerExtend)
    {
        $this->keymangerExtend = $keymangerExtend;
    }
    public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }
 	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }        
}
