<?php


namespace Badiu\System\CoreBundle\Model\Lib\Config;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Config\KEYDOMAINTABLE;
class KeyUtil{
   
    public function endWith($key,$prefix) {
			$prefixlenth=strlen($prefix);
			$keylenth=strlen($key);
            $end = substr($key,$keylenth-$prefixlenth,$keylenth);
           if($end==$prefix) {
				return true;
		   }
           return  false;
      }
	   
	   public function removeLastItem($key) {
           $pos = strrpos($key, '.');
           $mkey = substr($key,0, $pos);
           return  $mkey;
      }
       public function hasText($key,$text) {
			$pos=stripos($key, $text);
			if($pos=== false)return false;
			else return true;
         
      }
	public function getType($key) {
		 $type=null;
          if($this->endWith($key,KEYDOMAINTABLE::$PERMISSION )){$type=KEYDOMAINTABLE::$PERMISSION;}
		  else if($this->hasText($key,".menu.title")){$type=KEYDOMAINTABLE::$LABEL;}
		  else if($this->hasText($key,".menu.")){$type=KEYDOMAINTABLE::$MENU;}
		  else if($this->hasText($key,".param.config.")){$type=KEYDOMAINTABLE::$CONFIG;}
		  else if($this->hasText($key,".config.parent")){$type=KEYDOMAINTABLE::$PARENT;}
		  else if($this->hasText($key,".filter.form.")){$type=KEYDOMAINTABLE::$FORM_FILTER;}
		  else if($this->hasText($key,".data.form.")){$type=KEYDOMAINTABLE::$FORM_DATA;}
		  else if($this->hasText($key,".dbsearch.")){$type=KEYDOMAINTABLE::$DB_SEARCH;}
		  else if($this->hasText($key,".dashboard.")){$type=KEYDOMAINTABLE::$DASHBOARD;}
           return $type;
      }
	
}
