<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Config;

/**
 *  *
 * @author lino
 */
class DbField {
    
    /**
     * @var string
     */
    private $name;
  /**
     * @var array
     */
    private $info;

	 /**
     * @var string
     */
    private $label;
    
    
    
    function __construct() {
                $this->type='text';
                $this->info=array();
				 $this->label=null;
           }
    
           public function getName() {
               return $this->name;
           }

           public function getInfo() {
               return $this->info;
           }

           public function setName($name) {
               $this->name = $name;
           }

           public function setInfo($info) {
               $this->info = $info;
           }

		public function getLabel() {
          return $this->label;
      }

      public function setLabel($label) {
          $this->label = $label;
      }


         
}
