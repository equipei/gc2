<?php
namespace Badiu\System\CoreBundle\Model\Lib\Client;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class SystemFactoryData extends BadiuModelLib
{

    
    /**
     * @var array
     */
    private $menu;
	
	  /**
     * @var array
     */
    private $menuContent;
	
	
	  /**
     * @var array
     */
    private $menuNavbar;
	
		  /**
     * @var array
     */
    private $formFilter;
	
		  /**
     * @var array
     */
    private $formData;
	
		  /**
     * @var array
     */
    private $dbsearch;
	
	
	public function __construct(Container $container)
    {
        parent::__construct($container);
		$this->menu=array();
		$this->menuContent=array();
		$this->menuNavbar=array();
		$this->formFilter=array();
		$this->formData=array();
		$this->dbsearch=array();
		
    }

     public function getMenu()
    {
        return $this->menu;
		
    }


    public function setMenu($menu)
    {
        $this->menu = $menu;
    }
   public function getMenuContent()
    {
        return $this->menuContent;
    }


    public function setMenuContent($menuContent)
    {
        $this->menuContent = $menuContent;
    }

	   public function getMenuNavbar()
    {
        return $this->menuNavbar;
    }


    public function setMenuNavbar($menuNavbar)
    {
        $this->menuNavbar = $menuNavbar;
    }
   public function getFormFilter()
    {
        return $this->formFilter;
    }


    public function setFormFilter($formFilter)
    {
        $this->formFilter = $formFilter;
    }

	   public function getFormData()
    {
        return $this->formData;
    }


    public function setFormData($formData)
    {
        $this->formData = $formData;
    }

	   public function getDbsearch()
    {
        return $this->dbsearch;
    }


    public function setDbsearch($dbsearch)
    {
        $this->dbsearch = $dbsearch;
    }
}
