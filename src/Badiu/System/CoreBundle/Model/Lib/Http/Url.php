<?php
namespace Badiu\System\CoreBundle\Model\Lib\Http;

//review funcitonality in Uti
class Url {
    
     public static function getCurrentUrl() {
        $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https')=== FALSE ? 'http' : 'https';
        $currentUrl = $protocol . '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        return $currentUrl;
     }
    
     public  static function addParam($url,$key,$value){
         $starParam="";
        if(strpos($url,'?')=== FALSE){
            $starParam="?";
        } 
        $url=$url.$starParam."&$key=$value";
        return $url;
    }
}
