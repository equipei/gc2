<?php
namespace Badiu\System\CoreBundle\Model\Lib\Http;
class HttpQueryString {
     /**
     * @var string
     */ 
     private $query; 
     
     /**
     * @var array
     */ 
     private $param; 
     
     function __construct($query=null) {
                $this->param=array();
                 if(!empty($query)){
                     $this->query=$query;
                     $this->makeParam();
                 }
                
                  
     }
     public function clear(){
         $this->param=array();
          $this->query="";
     }
     public function makeParam(){
         $this->param=array();
         if(!empty($this->query)){
             $listkeyvalue=explode("&",$this->query);
             if(!empty($listkeyvalue)){
                  foreach ($listkeyvalue  as $v) {
                  $keyvalue=explode("=",$v);
                  $key='';
                  $value='';
                  if(isset($keyvalue[0])){$key=$keyvalue[0];}
                   if(isset($keyvalue[1])){$value=$keyvalue[1];}
                  $this->param[$key]=$value;
              }
             }
             
         }
     }
     
      public function makeQuery($enableurlencode=false){
          $this->query="";
          if(!empty($this->param)){
              $cont=0;
              foreach ($this->param  as $key => $value) {
                  if($enableurlencode){$value=urlencode($value);}
                  if($cont==0){$this->query="$key=$value";}
                  else{$this->query.="&$key=$value";}
                  $cont++;
           }
        } 
     }
     public function add($key,$value) {
        if(!isset($this->param) || (empty($this->param))){$this->param=array();}
         $this->param[$key]=$value;
     }
      public function remove($key) {
         unset($this->param[$key]);
     }
     public function existKey($key) {
        $r=FALSE;
        if(!empty($this->param)){
           $r=array_key_exists($key,$this->param);
        } 
         return  $r;
     }
     
     public function existValue($value) {
          $r=FALSE;
        if(!empty($this->param)){
           $r=in_array($value,$this->param);
        } 
       
         return  $r;
     }
     
      public function getValue($key) {
          $r="";
        if($this->existKey($key)){
           $r=$this->param[$key];
        }
          $r=urldecode($r);
        return  $r;
     }
     public function getQuery() {
         return $this->query;
     }

     public function setQuery($query) {
         $this->query = $query;
     }

  
     public function getParam() {
         return $this->param;
     }

     public function setParam($param) {
         $this->param = $param;
     }


}
