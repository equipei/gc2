<?php
namespace Badiu\System\CoreBundle\Model\Lib\Http;
use Badiu\System\CoreBundle\Model\Lib\Http\HttpQueryString;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class HttpQueryStringSystem extends HttpQueryString {

   /**
     * @var Container
     */
    private $container;
	 function __construct(Container $container,$query=null) {
                $this->container=$container;
				parent::__construct($query);
      }
   
    public function getParameters(){
		        $isparamjson=true;
                        $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
                        $jparam=file_get_contents('php://input');
                        $param=$json->decode($jparam, true);
                       
                        if(empty($param)){
                            $queryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
                            $query=$_SERVER["QUERY_STRING"];
                            if(!empty($query)){$query=urldecode($query);}
                            $queryString->setQuery($query);
                            $queryString->makeParam();
                            $param=$queryString->getParam();
                            $isparamjson=false;
                          
                        }
                        if(!$isparamjson && empty($_SERVER["QUERY_STRING"])){ // for post
                            $param=$_POST;
                            $isparamjson=false;
                        } 
		     
			return $param;
     }
     //review it
   public function getParameter($param,$typerequest='all'){
		$value='';
		
		//typerequest ajaxws is only to route badiu.system.core.service.process
		if($typerequest=='ajaxws'){
			$sroute=$request = $this->getContainer()->get('request')->get('_route');
			if($sroute!='badiu.system.core.service.process'){return null;}
		} 		
		
		$json=$this->getContainer()->get('badiu.system.core.lib.util.json');
                $jparam=file_get_contents('php://input');
                $lparam=$json->decode($jparam, true);
                if(!empty($lparam) && isset($lparam[$param])){ return $lparam[$param];}
               // if($typerequest=='ajaxws'){return null;}  
		$querystring=$_SERVER["QUERY_STRING"];
		$pos=stripos($querystring, "badiu_system_core_form_type");
		if($pos=== false){
			$value= $this->container->get('request')->get($param);
			return $value;
		}
		
		$this->setQuery($querystring);
		$query=$this->getQuery();
		$pkey="badiu_system_core_form_type[$param]";
		$cont=0;
		if(!empty($query)){
             $listkeyvalue=explode("&",$query);
			    if(!empty($listkeyvalue)){
                  foreach ($listkeyvalue  as  $v) {
				  	$keyvalue=explode("=",$v);
					$key='';
					
					if(isset($keyvalue[0])){$key=$keyvalue[0];}
					if(isset($keyvalue[1])){$value=$keyvalue[1];}
					//echo "value= $value";
					
					$qk=urldecode($key);
					if($qk==$pkey){
						$cont++;
						break;
					}
					  
                  
              }
             }
             
         }
		 //if param is routing
		if($cont==0){$value= $this->container->get('request')->get($param);}
		return $value;
     }
  public function makeParam(){
         $query=$this->getQuery();
		 $param=$this->getParam();
         if(!empty($query)){
             $listkeyvalue=explode("&",$query);
			 
             if(!empty($listkeyvalue)){
                  foreach ($listkeyvalue  as $v) {
				       $pos=stripos($v, "badiu_system_core_form_type");
					  if($pos=== false){
							$keyvalue=explode("=",$v);
							$key='';
							$value='';
							if(isset($keyvalue[0])){$key=$keyvalue[0];}
							if(isset($keyvalue[1])){$value=$keyvalue[1];}
							$param[$key]=$value;
					  }
                  
              }
             }
             
         }
		 $this->setParam($param);
     }
	
 public function updateParam($data){
  
	$this->setQuery($_SERVER["QUERY_STRING"]);
	$this->setParam($data);
	$this->makeParam();
	$list=$this->getParam();
	if(!empty($list)){
              foreach ($list as $key => $value){
		    if(!empty($value)&& !is_array($value)){$value=urldecode($value); }
		    $data[$key]= $value; 
              }  
     }
	return $data;
 }
  	
  public function getContainer() {
      return $this->container;
  }
	
  public function setContainer(Container $container) {
      $this->container = $container;
  }
	
}
