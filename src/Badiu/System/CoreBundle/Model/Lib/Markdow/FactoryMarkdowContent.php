<?php
 
namespace Badiu\System\CoreBundle\Model\Lib\Markdow;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**
 * @author lino
 */ 
class FactoryMarkdowContent extends BadiuModelLib{
    
  
   function __construct(Container $container) {
                parent::__construct($container);
		  } 
       
     
	public  function change($content){
		$managestring=$this->getContainer()->get('badiu.system.core.lib.util.managestring');
		$content=$managestring->getExpressions($content,'[[[',']]]',3);
		
		$content=$this->replaceExpressions($managestring->getPcontent(),$content);
		return  $content;
		
	}
	
     public function replaceExpressions($listexpression,$content){
		if(!is_array($listexpression)){return $content;}
		$newlist=array();
	
		foreach ($listexpression as $key => $value) {
			$expression=$this->getUtildata()->getVaueOfArray($value,'currentexpression');
			$tempexpression=$this->getUtildata()->getVaueOfArray($value,'tempexpression');
			$exprcontent=$this->execServiceExpresssion($expression);
			
			$content=str_replace($tempexpression,$exprcontent,$content);
			
		}
		
	return $content;
  }
    
    public function getExpression($value){
		
			$value=str_replace("{","",$value);
			$value=str_replace("}","",$value);
		
		
		
		return $value;
	}
	
	public function execServiceExpresssion($texexpression){
		 $defaultparse=$this->getContainer()->get('badiu.system.core.lib.markdow.defaultparse');
		
		 $texexpression=str_replace("[[[","```",$texexpression);
		 $texexpression=str_replace("]]]","\n```",$texexpression);
		 $texexpression=$this->getUtildata()->castHtmlToText($texexpression);
		 //echo $texexpression;exit;
		 $texexpression=$defaultparse->text($texexpression);
	  return $texexpression;
	}
}
