<?php

namespace Badiu\System\CoreBundle\Model\Lib\Date;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FactoryWorkingHour extends BadiuModelLib {

    public $timestart;
    public $timeend;
    public $timeutil = array();

    public function __construct(Container $container) {
        parent::__construct($container);
        $this->setTimestart(new \DateTime('2016-12-31 08:00:00'));
        $this->setTimeend(new \DateTime('2017-01-01 18:00:00'));
        //$this->setTimeutil(array('1' => '08:00:00', '09:00:00', '10:00:00', '11:00:00', '12:00:00', '14:00:00', '15:00:00', '16:00:00', '17:00:00', '18:00:00'));

        $this->timeutil['defaulthour'] = array(8, 9, 10, 11, 14, 15, 16, 17);
        $this->timeutil['defaultday'] = array(1, 2, 3, 4, 5);
    }

    public function sameDay() {
        $hourstart = $this->getTimestart()->format('H');

        $hourstart = $hourstart * 1;

        $hourend = $this->getTimeend()->format('H');
        $hourend = $hourend * 1;

        $result = $this->factoryHour($hourstart, $hourend);
        return $result;
    }

    public function sameMonth() {
        $hourstart = $this->getTimestart()->format('H');
        $hourstart = $hourstart * 1;

        $hourend = $this->getTimeend()->format('H');
        $hourend = $hourend * 1;

        $daystart = $this->getTimestart()->format('d');
        $daystart = $daystart * 1;

        $dayend = $this->getTimeend()->format('d');
        $dayend = $dayend * 1;
        // contar hora
        $counthour = $this->factoryHourMonth($hourstart, $hourend, $daystart, $dayend);

        return $counthour;
    }

    function getNumberDayOfMonth($month, $year) {

        $month = $month; // Mês desejado
        $year = $year;
        $lastday = date("t", mktime(0, 0, 0, $month, '01', $year)); // Mágica

        return $lastday;
    }

    public function differentYear() {
        
        //ano de inicio
        $yearstart = $this->getTimestart()->format('y');
        $yearstart = $yearstart * 1;       
        
        //ano de fim
        $yearend = $this->getTimeend()->format('y');
        $yearend = $yearend * 1;

        $hourstart = $this->getTimestart()->format('H');
        $hourstart = $hourstart * 1;

        $hourend = $this->getTimeend()->format('H');
        $hourend = $hourend * 1;

        $daystart = $this->getTimestart()->format('d');
        $daystart = $daystart * 1;

        $dayend = $this->getTimeend()->format('d');
        $dayend = $dayend * 1;

        $monthstart = $this->getTimestart()->format('m');
        $monthstart = $monthstart * 1;

        $monthend = $this->getTimeend()->format('m');
        $monthend = $monthend * 1;
        
        //conta hora
        $counthour = 0;        
        
        if($yearend >= $yearstart){            
           //contador            
            $count = 0;
            //contador auxiliar
            $countaux = $monthstart;            
            
             for ($x = $yearstart; $x <= $yearend; $x++) {               
                 //variavel que o mes inicia
                 $defaultdmonthestart = 1;
                 //variavel que o mes finda
                 $defaultdmontheend = 12;
                 
                 if($count == 0){
                     //variavel recebe o mes do inicio
                     $defaultdmonthestart = $monthstart;
                    
                 }
                 if ($countaux == $monthend) {
                    //variavel que recebe dia do fim
                    $defaultdmontheend = $monthend;
                    
                    echo 'xxx';
                }
                 
             }
        }
        
        
    }

    public function sameYear() {

        //ano de inicio
        $currentyear = $this->getTimestart()->format('y');
        $currentyear = $currentyear * 1;

        $hourstart = $this->getTimestart()->format('H');
        $hourstart = $hourstart * 1;

        $hourend = $this->getTimeend()->format('H');
        $hourend = $hourend * 1;

        $daystart = $this->getTimestart()->format('d');
        $daystart = $daystart * 1;

        $dayend = $this->getTimeend()->format('d');
        $dayend = $dayend * 1;

        $monthstart = $this->getTimestart()->format('m');
        $monthstart = $monthstart * 1;

        $monthend = $this->getTimeend()->format('m');
        $monthend = $monthend * 1;

        //conta hora
        $counthour = 0;
        //se o mes final eh maior que o mes inicial
        if ($monthend >= $monthstart) {
            //contador
            $count = 0;
            //contador auxiliar
            $countaux = $monthstart;
            for ($x = $monthstart; $x <= $monthend; $x++) {
                //variavel que o dia inicia
                $defaultdaystart = 1;
                //numero de dias num mes
                $defaultdayend = $this->getNumberDayOfMonth($x, $currentyear);
                
                if ($count == 0) {
                    //variavel recebe dia do inicio
                    $defaultdaystart = $daystart;
                    
                }
                if ($countaux == $monthend) {
                    //variavel que recebe dia do fim
                    $defaultdayend = $dayend;
                }
                //contador do dia
                $contday = 0;
                //variavel que recebe dia do inicio
                $contdayaux = $defaultdaystart;

                for ($y = $defaultdaystart; $y <= $defaultdayend; $y++) {
                    //echo $y."<br>";
                    $defaulthourstart = 0;
                    $defaulthourend = 23;

                    if ($count == 0 && $count == 0) {
                        $defaulthourstart = $hourstart;
                    }

                    if ($contdayaux == $defaultdayend && $countaux == $monthend) {
                        $defaulthourend = $hourend;
                    }

                    $counthour += $this->factoryHour($defaulthourstart, $defaulthourend);
                    $contday++;
                    $contdayaux++;
                }

                $count++;
                $countaux++;
            }
        }
        return $counthour;
    }

    public function factoryHourMonth($hourstart, $hourend, $daystart, $dayend) {

        $counthour = 0;
        if ($dayend >= $daystart) {
            $count = 0;
            $countaux = $daystart;
            for ($x = $daystart; $x <= $dayend; $x++) {
                $startdayhour = 0;
                $enddayhour = 23;

                if ($count == 0) {
                    $startdayhour = $hourstart;
                }
                if ($countaux == $dayend) {
                    $enddayhour = $hourend;
                }
                // echo "$x - $count - $startdayhour -  $enddayhour - \n";
                $counthour += $this->factoryHour($startdayhour, $enddayhour);

                $count++;
                $countaux++;
            }
        }

        return $counthour;
    }

    public function factoryHour($hourstart, $hourend) {
        $count = 0;
        if ($hourend >= $hourstart) {

            for ($x = $hourstart; $x <= $hourend; $x++) {

                if (in_array($x, $this->timeutil['defaulthour'])) {
                    $count++;
                }
            }
        }
        return $count;
    }

    public function getTimestart() {
        return $this->timestart;
    }

    public function getTimeend() {
        return $this->timeend;
    }

    public function getTimeutil() {
        return $this->timeutil;
    }

    public function setTimestart($timestart) {
        $this->timestart = $timestart;
    }

    public function setTimeend($timeend) {
        $this->timeend = $timeend;
    }

    public function setTimeutil($timeutil) {
        $this->timeutil = $timeutil;
    }

}
