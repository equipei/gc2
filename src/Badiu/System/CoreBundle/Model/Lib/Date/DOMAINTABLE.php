<?php

namespace Badiu\System\CoreBundle\Model\Lib\Date;
class DOMAINTABLE {

    //timeunit: second | minute | hour | day | week | month | year
        public static  $TIME_UNIT_SECOND   ="system_time_unit_second";
        public static  $TIME_UNIT_MINUTE   ="system_time_unit_minute";
        public static  $TIME_UNIT_HOUR   ="system_time_unit_hour";
        public static  $TIME_UNIT_DAY   ="system_time_unit_day";
        public static  $TIME_UNIT_WEEK   ="system_time_unit_week";
        public static  $TIME_UNIT_MONTH   ="system_time_unit_month";
        public static  $TIME_UNIT_YEAR   ="system_time_unit_year";
        
	//timeinterval|daily|monthly|weekly|once
        public static  $TIMEINTERVAL   ="system_timeinterval";
        public static  $DAILY   ="system_daily";
        public static  $MONTHLY   ="system_monthly";
        public static  $WEEKLY   ="system_weekly";
	public static  $ONCE="once";
	
	public static  $PARAM_MINUTE="minute";
	public static  $PARAM_HOUR="hour";
	public static  $PARAM_DAY="day";
	public static  $PARAM_WEEKY="week";
	public static  $PARAM_TIME_TYPE="timetype"; //minute|hour|day|month
	public static  $PARAM_TIME_VALUE="timevalue"; // number of time. If type is minute the value is the number of minute


        public static  $OPERATOR_TEXT_FROM="from";
	public static  $OPERATOR_TEXT_AFTER="after";
        public static  $OPERATOR_TEXT_UNTIL="until";
        public static  $OPERATOR_TEXT_BEFORE="before";
        public static  $OPERATOR_TEXT_ON="on";
	public static  $OPERATOR_TEXT_DIFFERENT="different"; 
        public static  $OPERATOR_TEXT_BETWEEN="between";

        public static  $OPERATOR_PAST_TEXT_FROM="pastfrom"; //ocorreu  nos ultimos x dias
        public static  $OPERATOR_PAST_TEXT_UNTIL="pastuntil"; //ocorreu antes de
        public static  $OPERATOR_PAST_TEXT_ON="paston"; //ocorreu há

        public static  $OPERATOR_FUTURE_TEXT_UNTIL="futureuntil"; //ocorrera ate
	public static  $OPERATOR_FUTURE_TEXT_AFTER="futureafter"; //ocorrera a partir de
        public static  $OPERATOR_FUTURE_TEXT_ON="futureon"; ///ocorrera em
        public static  $OPERATOR_FUTURE_TEXT_FROM="futurefrom"; ///ocorrera nos proximos 


}
?>