<?php

namespace Badiu\System\CoreBundle\Model\Lib\Date;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class TimeParam{
    private $param;
    /*
     * @var Container
     */
    private $container;
     function __construct(Container $container) {
          $this->container=$container;
     }
    
     function initParam($param) {
       $this->param=$param;
     }
     /* function exec() {
          $param="10,11,12:10-13:20,14-16,21-20";
          $var=$this->getList($param);
          print_r($var);
      }*/
     function getList($param) {
         if(empty($param)){return null;}
         $value=array();
         $hasinterval=false;
         $haslist=false;
         $posi=stripos($param, "-");
         if($posi!== false){$hasinterval=true;}
         
         $posl=stripos($param, ",");
         if($posl!== false){ $haslist=true;}
         
         //without interval (-) and without list
          if(!$hasinterval && !$haslist){$value[$this->castNumber($param)]=$param;}
          
        //without interval (-) and with list
          else if(!$hasinterval && $haslist){
             $list=explode(",",$param);
            foreach ($list as $v) {$value[$this->castNumber($v)]=$v;}
          }
        
         //with interval (-) and without list
          if($hasinterval && !$haslist){
                $value=$this->getIntervalList($param);
          }
          
        //with interval (-) and with list
          else if($hasinterval && $haslist){
             $list=explode(",",$param);
             foreach ($list as $l) {
                 $haslinterval=stripos($l, "-");
                 if($haslinterval===false){$value[$this->castNumber($l)]=$l;}
                 else{
                   
                     $ivalues=$this->getIntervalList($l);
                 //    print_r($ivalues);
                    foreach ($ivalues as $k => $iv) {
                         $value[$this->castNumber($k)]=$iv;
                     }
                     
                 }
                 
             }
          }
         
         return $value;
     }
     
     function getIntervalList($param) {
      
          $hashour=stripos($param, ":");
          if($hashour!==false){
              $value=$this->getIntervalListHourMinutes($param);
              return $value;
          }
          
         $value=array();
            $list=explode("-",$param);
            $start=-1;
            $end=-1;
            if(isset($list[0])){$start=$list[0];}
            if(isset($list[1])){$end=$list[1];}
            $cont=0;
            if($start >=0 &&  $end>=0){
                for ($i = $start; $i <= $end; $i++) {
                    $value[$i]=$i;
                    $cont++;
                    if($cont>2500){break;}
                }
            }
        return $value;
     }
     function getIntervalListHourMinutes($param) {
       
            $value=array();
            $list=explode("-",$param);
            $start=-1;
            $end=-1;
            $starthour=-1;
            $startminutes=-1;
            $endhour=-1;
            $endminutes=-1;
            
            if(isset($list[0])){$start=$list[0];}
            if(isset($list[1])){$end=$list[1];}
           
             if($start < 0 ||  $end < 0){return $value; }
             $posstart=stripos($start, ":");
             $posend=stripos($end, ":");
             
             if($posstart=== false || $posend===false){return $value; }
             
             $listhmstart=explode(":",$start);
             $listhmend=explode(":",$end);
            
            if(isset($listhmstart[0])){$starthour=$listhmstart[0];}
            if(isset($listhmstart[1])){$startminutes=$listhmstart[1];}
           
             if(isset($listhmend[0])){$endhour=$listhmend[0];}
            if(isset($listhmend[1])){$endminutes=$listhmend[1];}
           
            if($starthour < 0  ||$startminutes < 0 || $endhour < 0 || $endminutes < 0){return $value; }
            if($starthour > 23  || $endhour > 23 ){return $value; }
            
            //same hour
            if($starthour==$endhour){
                $value[$starthour]=$starthour;
                $minutes=array();
                $cont=0;
                if($startminutes >=0 &&  $startminutes <=59  && $endminutes >=0 &&  $endminutes <=59 && $startminutes < $endminutes ){
                  
                     for ($i = $startminutes; $i <= $endminutes; $i++) {
                        $minutes[$i]=$i;
                        $cont++;
                    } 
                }
              
                if($cont > 0 ){
                    $value['minutes-'.$starthour]=$minutes;
                }
              
            }
        
            //different hour
           if($starthour!=$endhour){ 
               //add hour list
               for ($i = $starthour; $i <= $endhour; $i++) {$value[$i]=$i;}
               
               //add minites start
               $minutesstart=array();
                $cont=0;
                if($startminutes >=0 &&  $startminutes <=59){
                     for ($i = $startminutes; $i <= 59; $i++) {
                        $minutesstart[$i]=$i;
                        $cont++;
                    }
                }
                if($cont > 0 ){
                    $value['minutes-'.$starthour]=$minutesstart;
                }
                
                 //add minites end
               $minutesend=array();
                $cont=0;
                if($endminutes >=0 &&  $startminutes <=59){
                     for ($i = 0; $i <=  $endminutes; $i++) {
                        $minutesend[$i]=$i;
                        $cont++;
                    }
                }
                if($cont > 0 ){
                    $value['minutes-'.$endhour]=$minutesend;
                }
           }
            
            
            
        return $value;
     }
     function castNumber($v) {
         if($v==null){return $v;}
         if(ctype_digit($v)){$v=$v+0;}
         return $v;
     }
      function getPeriodName($period) {
         $name=null;
        if($period==DOMAINTABLE::$DAILY){$name=$this->getTranslator()->trans('badiu.system.time.period.daily');}
        if($period==DOMAINTABLE::$WEEKLY){$name=$this->getTranslator()->trans('badiu.system.time.period.weekly');}
	if($period==DOMAINTABLE::$MONTHLY){$name=$this->getTranslator()->trans('badiu.system.time.period.monthly');}
	if($period==DOMAINTABLE::$TIMEINTERVAL){$name=$this->getTranslator()->trans('badiu.system.time.period.interval');}
	if($period==DOMAINTABLE::$ONCE){$name=$this->getTranslator()->trans('badiu.system.time.period.once');}
        return $name;
     }
     function getParam() {
         return $this->param;
     }

     function setParam($param) {
         $this->param = $param;
     }


     function getContainer() {
         return $this->container;
     }

     function setContainer($container) {
         $this->container = $container;
     }


}
