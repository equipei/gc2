<?php
namespace Badiu\System\CoreBundle\Model\Lib\Date;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Badiu\System\CoreBundle\Model\Lib\Date\DOMAINTABLE;
class DateFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
     
    public  function getPeriod(){
        $list=array();
        $list[DOMAINTABLE::$DAILY]=$this->getTranslator()->trans('badiu.system.time.period.daily');
        $list[DOMAINTABLE::$WEEKLY]=$this->getTranslator()->trans('badiu.system.time.period.weekly');
	$list[DOMAINTABLE::$MONTHLY]=$this->getTranslator()->trans('badiu.system.time.period.monthly');
	$list[DOMAINTABLE::$TIMEINTERVAL]=$this->getTranslator()->trans('badiu.system.time.period.interval');
	$list[DOMAINTABLE::$ONCE]=$this->getTranslator()->trans('badiu.system.time.period.once');
        return $list;
    }

    public  function getOperatorText(){
        $list=array();
        $list[DOMAINTABLE::$OPERATOR_TEXT_FROM]=$this->getTranslator()->trans('badiu.system.time.operatortext.from');
        $list[DOMAINTABLE::$OPERATOR_TEXT_AFTER]=$this->getTranslator()->trans('badiu.system.time.operatortext.after');
	$list[DOMAINTABLE::$OPERATOR_TEXT_UNTIL]=$this->getTranslator()->trans('badiu.system.time.operatortext.until');
	$list[DOMAINTABLE::$OPERATOR_TEXT_BEFORE]=$this->getTranslator()->trans('badiu.system.time.operatortext.before');
	$list[DOMAINTABLE::$OPERATOR_TEXT_ON]=$this->getTranslator()->trans('badiu.system.time.operatortext.on');
        $list[DOMAINTABLE::$OPERATOR_TEXT_DIFFERENT]=$this->getTranslator()->trans('badiu.system.time.operatortext.different');
        $list[DOMAINTABLE::$OPERATOR_TEXT_BETWEEN]=$this->getTranslator()->trans('badiu.system.time.operatortext.between');
        return $list;
    }
    
    public  function getOperatorTextLabel($operator){
        $label="";

        if($operator==DOMAINTABLE::$OPERATOR_TEXT_FROM){$label=$this->getTranslator()->trans('badiu.system.time.operatortext.from');}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_AFTER){$label=$this->getTranslator()->trans('badiu.system.time.operatortext.after');}
	    else if($operator==DOMAINTABLE::$OPERATOR_TEXT_UNTIL){$label=$this->getTranslator()->trans('badiu.system.time.operatortext.until');}
	    else if($operator==DOMAINTABLE::$OPERATOR_TEXT_BEFORE){$label=$this->getTranslator()->trans('badiu.system.time.operatortext.before');}
	    else if($operator==DOMAINTABLE::$OPERATOR_TEXT_ON){$label=$this->getTranslator()->trans('badiu.system.time.operatortext.on');}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_DIFFERENT){$label=$this->getTranslator()->trans('badiu.system.time.operatortext.different');}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_BETWEEN){$label=$this->getTranslator()->trans('badiu.system.time.operatortext.between');}
        return $label;
    }
    
    public  function getHours($withlabel=false){
        $list=array();
        $list['']='';
        $label="";
        if($withlabel){$label=" h";}
        for ($x = 0; $x < 24; $x++) {
            $key=$x;
            $value=$x;
            if($value<10 ){$value="0$x $label";}
            else {$value="$x $label";}
            $list[$key]=$value;
        }
        return $list;
    }
    
     public  function getMinutes($withlabel=false){
        $list=array();
        $label="";
        $list['']='';
        if($withlabel){$label=" min";}
        for ($x = 0; $x < 60; $x++) {
            $key=$x;
            $value=$x;
            if($value<10 ){$value="0$x $label";}
            else {$value="$x $label";}
            $list[$key]=$value;
        }
        return $list;
    }
    public  function getTimeUnit(){
        $list=array();
        $list[DOMAINTABLE::$TIME_UNIT_SECOND]=$this->getTranslator()->trans('badiu.system.time.second');
        $list[DOMAINTABLE::$TIME_UNIT_MINUTE]=$this->getTranslator()->trans('badiu.system.time.minute');
	$list[DOMAINTABLE::$TIME_UNIT_HOUR]=$this->getTranslator()->trans('badiu.system.time.hour');
	$list[DOMAINTABLE::$TIME_UNIT_DAY]=$this->getTranslator()->trans('badiu.system.time.day');
	$list[DOMAINTABLE::$TIME_UNIT_WEEK]=$this->getTranslator()->trans('badiu.system.time.week');
        $list[DOMAINTABLE::$TIME_UNIT_MONTH]=$this->getTranslator()->trans('badiu.system.time.month');
        $list[DOMAINTABLE::$TIME_UNIT_YEAR]=$this->getTranslator()->trans('badiu.system.time.year');
        return $list;
    }
    public  function getTimeUnitShortKey($unittime){
        $short=null;
        if($unittime==DOMAINTABLE::$TIME_UNIT_SECOND){$short='second';}
        else if ($unittime==DOMAINTABLE::$TIME_UNIT_MINUTE){$short='minute';}
	else if ($unittime==DOMAINTABLE::$TIME_UNIT_HOUR){$short='hour';}
	else if ($unittime==DOMAINTABLE::$TIME_UNIT_DAY){$short='day';}
	else if ($unittime==DOMAINTABLE::$TIME_UNIT_WEEK){$short='week';}
        else if ($unittime==DOMAINTABLE::$TIME_UNIT_MONTH){$short='month';}
        else if ($unittime==DOMAINTABLE::$TIME_UNIT_YEAR){$short='year';}
        return $short;
    }
     public  function getTimeUnitFullKey($unittime){
        $full=null;
        if($unittime=='second'){$full=DOMAINTABLE::$TIME_UNIT_SECOND;}
        else if ($unittime=='minute'){$full=DOMAINTABLE::$TIME_UNIT_MINUTE;}
	else if ($unittime=='hour'){$full=DOMAINTABLE::$TIME_UNIT_HOUR;}
	else if ($unittime=='day'){$full=DOMAINTABLE::$TIME_UNIT_DAY;}
	else if ($unittime=='week'){$full=DOMAINTABLE::$TIME_UNIT_WEEK;}
        else if ($unittime=='month'){$full=DOMAINTABLE::$TIME_UNIT_MONTH;}
        else if ($unittime=='year'){$full=DOMAINTABLE::$TIME_UNIT_YEAR;}
        return $full;
    }
    public  function getTimeUnitMinute(){
        $list=array();
        $list[DOMAINTABLE::$TIME_UNIT_SECOND]=$this->getTranslator()->trans('badiu.system.time.second');
        $list[DOMAINTABLE::$TIME_UNIT_MINUTE]=$this->getTranslator()->trans('badiu.system.time.minute');
	return $list;
    }
    public  function getTimeUnitHour(){
        $list=array();
        $list[DOMAINTABLE::$TIME_UNIT_SECOND]=$this->getTranslator()->trans('badiu.system.time.second');
        $list[DOMAINTABLE::$TIME_UNIT_MINUTE]=$this->getTranslator()->trans('badiu.system.time.minute');
	$list[DOMAINTABLE::$TIME_UNIT_HOUR]=$this->getTranslator()->trans('badiu.system.time.hour');
	return $list;
    }
    public  function getTimeUnitDay(){
        $list=array();
        $list[DOMAINTABLE::$TIME_UNIT_SECOND]=$this->getTranslator()->trans('badiu.system.time.second');
        $list[DOMAINTABLE::$TIME_UNIT_MINUTE]=$this->getTranslator()->trans('badiu.system.time.minute');
	$list[DOMAINTABLE::$TIME_UNIT_HOUR]=$this->getTranslator()->trans('badiu.system.time.hour');
        $list[DOMAINTABLE::$TIME_UNIT_DAY]=$this->getTranslator()->trans('badiu.system.time.day');
	return $list;
    }
    
    public  function getDayOfWeek(){
        $list=array();
        $list[0]=$this->getTranslator()->trans('badiu.system.time.month.name.sunday');
        $list[1]=$this->getTranslator()->trans('badiu.system.time.month.name.monday');
	$list[2]=$this->getTranslator()->trans('badiu.system.time.month.name.tuesday');
        $list[3]=$this->getTranslator()->trans('badiu.system.time.month.name.wednesday');
        $list[4]=$this->getTranslator()->trans('badiu.system.time.month.name.thursday');
        $list[5]=$this->getTranslator()->trans('badiu.system.time.month.name.friday');
        $list[6]=$this->getTranslator()->trans('badiu.system.time.month.name.saturday');
	return $list;
    }
    
     public  function getDayOfWeekLabel($dayofweek,$lower=false){
           $label="";
           $plower="";
           if($lower){$plower=".lower";}
            if($dayofweek==0){$label=$this->getTranslator()->trans('badiu.system.time.month.name.sunday'.$plower);}
            else if($dayofweek==1){$label=$this->getTranslator()->trans('badiu.system.time.month.name.monday'.$plower);}
            else if($dayofweek==2){$label=$this->getTranslator()->trans('badiu.system.time.month.name.tuesday'.$plower);}
            else if($dayofweek==3){$label=$this->getTranslator()->trans('badiu.system.time.month.name.wednesday'.$plower);}
            else if($dayofweek==4){$label=$this->getTranslator()->trans('badiu.system.time.month.name.thursday'.$plower);}
            else if($dayofweek==5){$label=$this->getTranslator()->trans('badiu.system.time.month.name.friday'.$plower);}
            else if($dayofweek==6){$label=$this->getTranslator()->trans('badiu.system.time.month.name.saturday'.$plower);}
            return $label;
       }
     public  function getDaysOfWeekLabel($daysofweek,$lower=false){
           $list=array();
			$plower="";
            if($lower){$plower=".lower";}
            $list[0]=$this->getTranslator()->trans('badiu.system.time.month.name.sunday'.$plower);
            $list[1]=$this->getTranslator()->trans('badiu.system.time.month.name.monday'.$plower);
            $list[2]=$this->getTranslator()->trans('badiu.system.time.month.name.tuesday'.$plower);
            $list[3]=$this->getTranslator()->trans('badiu.system.time.month.name.wednesday'.$plower);
            $list[4]=$this->getTranslator()->trans('badiu.system.time.month.name.thursday'.$plower);
            $list[5]=$this->getTranslator()->trans('badiu.system.time.month.name.friday'.$plower);
            $list[6]=$this->getTranslator()->trans('badiu.system.time.month.name.saturday'.$plower);
			
			$label="";
			$listvalue=array();
			$pos=stripos($daysofweek, ",");
			if($pos=== false){$listvalue=array($daysofweek);}
			else {
				$listvalue=explode(",",$daysofweek);
			}
			$cont=0;
			foreach ($listvalue as $v) {
				$dlabel=$this->getUtildata()->getVaueOfArray($list,$v);
				$separator=", ";
				if($cont==0){$separator="";}
				$label.=$separator.$dlabel;
				$cont++;
			}
            return $label;
       }
     public  function getMonth(){
        
         $list=array();
        $list[1]=$this->getTranslator()->trans('badiu.system.time.month.name.january');
	$list[2]=$this->getTranslator()->trans('badiu.system.time.month.name.february');
        $list[3]=$this->getTranslator()->trans('badiu.system.time.month.name.march');
        $list[4]=$this->getTranslator()->trans('badiu.system.time.month.name.april');
        $list[5]=$this->getTranslator()->trans('badiu.system.time.month.name.may');
        $list[6]=$this->getTranslator()->trans('badiu.system.time.month.name.june');
        $list[7]=$this->getTranslator()->trans('badiu.system.time.month.name.july');
        $list[8]=$this->getTranslator()->trans('badiu.system.time.month.name.august');
        $list[9]=$this->getTranslator()->trans('badiu.system.time.month.name.september');
        $list[10]=$this->getTranslator()->trans('badiu.system.time.month.name.october');
        $list[11]=$this->getTranslator()->trans('badiu.system.time.month.name.november');
        $list[12]=$this->getTranslator()->trans('badiu.system.time.month.name.december');
	return $list;
    }
    
    public  function getMonthLabel($month,$lower=false){
       $label="";
       $plower="";
           if($lower){$plower=".lower";}
         if($month==1){$label=$this->getTranslator()->trans('badiu.system.time.month.name.january'.$plower);}
	else if($month==2){$label=$this->getTranslator()->trans('badiu.system.time.month.name.february'.$plower);}
        else if($month==3){$label=$this->getTranslator()->trans('badiu.system.time.month.name.march'.$plower);}
        else if($month==4){$label=$this->getTranslator()->trans('badiu.system.time.month.name.april'.$plower);}
        else if($month==5){$label=$this->getTranslator()->trans('badiu.system.time.month.name.may'.$plower);}
        else if($month==6){$label=$this->getTranslator()->trans('badiu.system.time.month.name.june'.$plower);}
        else if($month==7){$label=$this->getTranslator()->trans('badiu.system.time.month.name.july'.$plower);}
        else if($month==8){$label=$this->getTranslator()->trans('badiu.system.time.month.name.august'.$plower);}
        else if($month==9){$label=$this->getTranslator()->trans('badiu.system.time.month.name.september'.$plower);}
        else if($month==10){$label=$this->getTranslator()->trans('badiu.system.time.month.name.october'.$plower);}
        else if($month==11){$label=$this->getTranslator()->trans('badiu.system.time.month.name.november'.$plower);}
        else if($month==12){$label=$this->getTranslator()->trans('badiu.system.time.month.name.december'.$plower);}
	return $label;
    }

/*
        public static  $OPERATOR_PAST_TEXT_FROM="pastfrom"; // nos ultimos x dias
	public static  $OPERATOR_PAST_TEXT_UNTIL="pastuntil"; //ocorreu ate
        public static  $OPERATOR_PAST_TEXT_ON="paston"; //ocorreu há

        public static  $OPERATOR_FUTURE_TEX_UNTIL="futureuntil"; //ocorrera ate
	public static  $OPERATOR_FUTURE_TEXT_AFTER="futureafter"; //ocorrera a partir de
        public static  $OPERATOR_FUTURE_TEXT_ON="futureon"; ///ocorrera em
        public static  $OPERATOR_FUTURE_TEXT_FROM="futurefrom"; ///ocorrera nos proximos 
*/

public function getLastYears() {
		 $now =new \DateTime();
		$currentyear=$now->format('Y');
        $list=array();
		$cont=0;
		$exec=true;
         while ($exec) {
			$list[$currentyear]=$currentyear;
			$currentyear--;
			$cont++;
			if($cont==10){$exec=false;break;}
		}
        return $list;
    }
public  function getOperatorFullPeriod(){
        $list=array();
        //past
        $list[DOMAINTABLE::$OPERATOR_PAST_TEXT_FROM]=$this->getTranslator()->trans('badiu.system.time.format.period.pastfrom');
        $list[DOMAINTABLE::$OPERATOR_PAST_TEXT_UNTIL]=$this->getTranslator()->trans('badiu.system.time.format.period.pastuntil');
	    $list[DOMAINTABLE::$OPERATOR_PAST_TEXT_ON]=$this->getTranslator()->trans('badiu.system.time.format.period.paston');
        //future
        $list[DOMAINTABLE::$OPERATOR_FUTURE_TEXT_FROM]=$this->getTranslator()->trans('badiu.system.time.format.period.futurefrom');
        //$list[DOMAINTABLE::$OPERATOR_FUTURE_TEXT_UNTIL]=$this->getTranslator()->trans('badiu.system.time.format.period.futureuntil');//is pasta and future review label
        $list[DOMAINTABLE::$OPERATOR_FUTURE_TEXT_AFTER]=$this->getTranslator()->trans('badiu.system.time.format.period.futureafter');
        $list[DOMAINTABLE::$OPERATOR_FUTURE_TEXT_ON]=$this->getTranslator()->trans('badiu.system.time.format.period.futureon');
        
        return $list;
    }
    public  function getOperatorPastPeriod(){
        $list=array();
        //past
        $list[DOMAINTABLE::$OPERATOR_PAST_TEXT_FROM]=$this->getTranslator()->trans('badiu.system.time.format.period.pastfrom');
        $list[DOMAINTABLE::$OPERATOR_PAST_TEXT_UNTIL]=$this->getTranslator()->trans('badiu.system.time.format.period.pastuntil');
        $list[DOMAINTABLE::$OPERATOR_PAST_TEXT_ON]=$this->getTranslator()->trans('badiu.system.time.format.period.paston');
      return $list;
    }

    public  function getOperatorFuturePeriod(){
        $list=array();
         //future
         $list[DOMAINTABLE::$OPERATOR_FUTURE_TEXT_FROM]=$this->getTranslator()->trans('badiu.system.time.format.period.futurefrom');
       // $list[DOMAINTABLE::$OPERATOR_FUTURE_TEXT_UNTIL]=$this->getTranslator()->trans('badiu.system.time.format.period.futureuntil'); //is pasta and future review label
        $list[DOMAINTABLE::$OPERATOR_FUTURE_TEXT_AFTER]=$this->getTranslator()->trans('badiu.system.time.format.period.futureafter');
        $list[DOMAINTABLE::$OPERATOR_FUTURE_TEXT_ON]=$this->getTranslator()->trans('badiu.system.time.format.period.futureon');

        return $list;
    }
 

    
public  function getOperatorPeriodLabel($operator){
    $list=array();
    $label="";
    //past
   if($operator==DOMAINTABLE::$OPERATOR_PAST_TEXT_FROM){$label=$this->getTranslator()->trans('badiu.system.time.format.period.'.$operator);}
    else if($operator==DOMAINTABLE::$OPERATOR_PAST_TEXT_UNTIL){$label=$this->getTranslator()->trans('badiu.system.time.format.period.'.$operator);}
    else if($operator==DOMAINTABLE::$OPERATOR_PAST_TEXT_ON){$label=$this->getTranslator()->trans('badiu.system.time.format.period.paston'.$operator);}
    //future
    else if($operator==DOMAINTABLE::$OPERATOR_FUTURE_TEXT_FROM){$label=$this->getTranslator()->trans('badiu.system.time.format.period.'.$operator);}
    else if($operator==DOMAINTABLE::$OPERATOR_FUTURE_TEXT_AFTER){$label=$this->getTranslator()->trans('badiu.system.time.format.period.'.$operator);}
    else if($operator==DOMAINTABLE::$OPERATOR_FUTURE_TEXT_ON){$label=$this->getTranslator()->trans('badiu.system.time.format.period.'.$operator);}
    
    return $label;
}

public  function getTtimeunitLabel($type,$lower=false,$withnumber=false){
    $list=array();
    $label="";
    $endkey="";
    if($lower){$endkey='.lower';}
    if($lower && $withnumber){$endkey='.lowerwithnumber';}
    if($type==DOMAINTABLE::$TIME_UNIT_SECOND){$label=$this->getTranslator()->trans('badiu.system.time.second'.$endkey);}
    else if($type==DOMAINTABLE::$TIME_UNIT_MINUTE){$label=$this->getTranslator()->trans('badiu.system.time.minute'.$endkey);}
	else if($type==DOMAINTABLE::$TIME_UNIT_HOUR){$label=$this->getTranslator()->trans('badiu.system.time.hour'.$endkey);}
	else if($type==DOMAINTABLE::$TIME_UNIT_DAY){$label=$this->getTranslator()->trans('badiu.system.time.day'.$endkey);}
	else if($type==DOMAINTABLE::$TIME_UNIT_WEEK){$label=$this->getTranslator()->trans('badiu.system.time.week'.$endkey);}
    else if($type==DOMAINTABLE::$TIME_UNIT_MONTH){$label=$this->getTranslator()->trans('badiu.system.time.month'.$endkey);}
    else if($type==DOMAINTABLE::$TIME_UNIT_YEAR){$label=$this->getTranslator()->trans('badiu.system.time.year'.$endkey);}

    return $label;
}
}
