<?php

namespace Badiu\System\CoreBundle\Model\Lib\Date;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

//http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_function=teste&_service=badiu.system.core.lib.date.calendartimeutil
class CalendarTimeUtil extends BadiuModelLib {
	/**
	*array param of holidays,working days and hour
	*/
    public $param=null;
    public function __construct(Container $container) {
        parent::__construct($container);
    }

   
	public function teste() {
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
		$calendarconfig=$badiuSession->getValue('badiu.system.core.param.config.date.calendar');
		$calendarconfig='monthholidaysdays=01-01,04-21,05-01,09-07,10-12,11-02,11-15,12-25&yearholidaysdays=2020-02-24,2020-02-25,2020-04-10,2020-06-11&weekworkingdays=1-5&monthworkingdays=11-03:19-23&yearworkingdays=2020-11-04:8-9|12-13,2020-12-24&dayworkinghours=08-12,14-18';
	
		$this->init($calendarconfig);
		
			echo "<pre>";
		print_r($this->param);
		echo "</pre>";
		
		
		$date1 =new \DateTime();  
	    $date1->setTimestamp(1604535841);
		echo $date1->format('d/m/Y: H');
		$date2=new \DateTime();  
	    $date2->setTimestamp(1604618641); 
		
		$countnwd=$this->countNonWorkingHour($date1,$date2);
		//$wd=$this->isWorkingDay($date);
		/*echo "<pre>";
		print_r($this->param);
		echo "</pre>";
		echo "<hr />"; 
		echo $date1->format('d/m/Y');*/
		echo "<hr />";
		echo $date2->format('d/m/Y: H');
		echo "<hr />";
		echo "hora nao util: ".$countnwd;
		exit;
	}
   public function init($config=null) {
	    $param=array();
	   if (empty($config)){return null;}
	    $querystring = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
		$querystring->setQuery($config);
		$querystring->makeParam();
		$this->param=$querystring->getParam();
		
		$monthholidaysdays=$this->getUtildata()->getVaueOfArray($this->param,'monthholidaysdays');
		$monthholidaysdays=$this->getUtildata()->castStringToArray($monthholidaysdays,",");
		$monthholidaysdays=$this->arraySetValueAsKey($monthholidaysdays);
		$this->param['monthholidaysdays']=$monthholidaysdays;
		
		$yearholidaysdays=$this->getUtildata()->getVaueOfArray($this->param,'yearholidaysdays');
		$yearholidaysdays=$this->getUtildata()->castStringToArray($yearholidaysdays,",");
		$yearholidaysdays=$this->arraySetValueAsKey($yearholidaysdays);
		$this->param['yearholidaysdays']=$yearholidaysdays;
		
		$weekworkingdays=$this->getUtildata()->getVaueOfArray($this->param,'weekworkingdays');
		$weekworkingdays=$this->getUtildata()->castStringToArray($weekworkingdays,",");
		$weekworkingdays=$this->castListTimeSequence($weekworkingdays,'week');
		$weekworkingdays=$this->arraySetValueAsKey($weekworkingdays);
		$this->param['weekworkingdays']=$weekworkingdays;
		
		
		$monthworkingdays=$this->getUtildata()->getVaueOfArray($this->param,'monthworkingdays');
		$monthworkingdays=$this->getUtildata()->castStringToArray($monthworkingdays,",");
		$monthworkingdays=$this->arraySetValueAsKey($monthworkingdays);
		$this->param['monthworkingdays']=$monthworkingdays;
		
		$yearworkingdays=$this->getUtildata()->getVaueOfArray($this->param,'yearworkingdays');
		$yearworkingdays=$this->getUtildata()->castStringToArray($yearworkingdays,",");
		$yearworkingdays=$this->arraySetValueAsKey($yearworkingdays);
		$this->param['yearworkingdays']=$yearworkingdays;
				
		$dayworkinghours=$this->getUtildata()->getVaueOfArray($this->param,'dayworkinghours');
		$dayworkinghours=$this->getUtildata()->castStringToArray($dayworkinghours,",");
		$dayworkinghours=$this->castListTimeSequence($dayworkinghours,'hour');
		$dayworkinghours=$this->arraySetValueAsKey($dayworkinghours);
		$this->param['dayworkinghours']=$dayworkinghours;
		 
		/* remove
		$monthworkinghours=$this->getUtildata()->getVaueOfArray($this->param,'monthworkinghours');
		$monthworkinghours=$this->getUtildata()->castStringToArray($monthworkinghours,",");
		$monthworkinghours=$this->arraySetValueAsKey($monthworkinghours);
		$this->param['monthworkinghours']=$monthworkinghours;
		
		
		$yearworkinghours=$this->getUtildata()->getVaueOfArray($this->param,'yearworkinghours');
		$yearworkinghours=$this->getUtildata()->castStringToArray($yearworkinghours,",");
		$yearworkinghours=$this->arraySetValueAsKey($yearworkinghours);
		$this->param['yearworkinghours']=$yearworkinghours;
		*/
	
	   
   }


public function castTimeSequence($param,$type='week'){   
   $newlist=array();
	if(empty($param)){return $newlist;}
	$listday=$this->getUtildata()->castStringToArray($param,"-");
	
	//for week
	$tmin=0;
	$tmax =6;
	if($type=='hour'){
		$tmin=0;
		$tmax =23;
	}
	if($type=='day'){
		$tmin=1;
		$tmax =31;
	}
	if(sizeof($listday)==1){ return $listday;}
	else if(sizeof($listday)==2){
		$startday=$this->getUtildata()->getVaueOfArray($listday,0);
		$endday=$this->getUtildata()->getVaueOfArray($listday,1);
		
		if(ctype_digit($startday)){$startday=intval($startday);}
		else {return $newlist;}
		
		if(ctype_digit($endday)){$endday=intval($endday);}
		else {return $newlist;}
		
		if($startday < $tmin ||  $endday  > $tmax){return $newlist;}
		
		if($startday > $endday ){return $newlist;}
		if($startday == $endday ){array_push($newlist,$startday); return $newlist;}
		if($type=='week'){$endday++;}
		for ($x = $startday; $x < $endday; $x++) {
			 array_push($newlist,$x);
		}
	}
	
	return $newlist;
}

public function castListTimeSequence($list,$type){   
   $newlist=array();
	if(!is_array($list)){return $newlist;}
	foreach ($list as $row) {
		$result=$this->castTimeSequence($row,$type);
		if(is_array($result)){
			$newlist=array_merge($newlist,$result);
		}
		
	}
	return $newlist;
}
public function arraySetValueAsKey($list){ 
	$newlist=array();
	if(!is_array($list)){return $newlist;}
	foreach ($list as $row) {
		//if data has hour, split it exampele: 2020-11-08:08-12|14-16
		$rlist=$this->getUtildata()->castStringToArray($row,":");
		if(is_array($rlist)&& sizeof($rlist)==2){
			$key=$this->getUtildata()->getVaueOfArray($rlist,0);
			$value=$this->getUtildata()->getVaueOfArray($rlist,1);
			$hlist=$this->getUtildata()->castStringToArray($value,"|");
			$hlist=$this->castListTimeSequence($hlist,'hour');
			$hlist=$this->arraySetValueAsKey($hlist);
			$newlist[$key]=$hlist;
		}else{
			$newlist[$row]=$row; 
		}
		
	}
	return $newlist;
} 
/**
* count total day is not working start by $timestart
*/

public function countNonWorkingDay($timestart,$timeend,$limit=1825){ 
	$result=0;
	if(empty($timestart)){return $result;}
	if(empty($timeend)){return $result;}
	
	$startdatef=$timestart->format('Y-m-d');
    $enddatef=$timeend->format('Y-m-d');
	
	if($startdatef==$enddatef){return $result;}
	if($timestart->getTimestamp() > $timeend->getTimestamp()){return $result;}
	
	$addday=true;
	$conuntnonworkday=0;
	$cont=0;
	$wd=$this->isWorkingDay($timestart);
	if($wd==0){$conuntnonworkday++;}
	
	while ($addday) {
		
		$timestart->modify('+1 day');
		$wd=$this->isWorkingDay($timestart);
		if($wd==0){$conuntnonworkday++;}
		$startdatemf=$timestart->format('Y-m-d');
		if($startdatemf==$enddatef){$addday=false;break;}
		$cont++;
		if($cont > $limit){$addday=false;break;}
		
   }
	return $conuntnonworkday;
}


/**
* count total hour is not working start by $timestart
*/

public function countNonWorkingHour($timestart,$timeend,$limit=43800){ 
	$result=0;
	if(empty($timestart)){return $result;}
	if(empty($timeend)){return $result;}
	
	$startdatef=$timestart->format('Y-m-d-H');
    $enddatef=$timeend->format('Y-m-d-H');
	
	if($startdatef==$enddatef){return $result;}
	if($timestart->getTimestamp() > $timeend->getTimestamp()){return $result;}
	
	$addhour=true;
	$conuntnonworkhour=0;
	$cont=0;
	$wh=$this->isWorkingHour($timestart);
	if($wh==0){$conuntnonworkhour++;}

	while ($addhour) {
		
		$timestart->modify('+1 hours');
		$wh=$this->isWorkingHour($timestart);
		if($wh==0){$conuntnonworkhour++;}
		$startdatemf=$timestart->format('Y-m-d-H');
		if($startdatemf==$enddatef){$addhour=false;break;}
		$cont++;
		if($cont > $limit){$addhour=false;break;}
		
		
   }
	return $conuntnonworkhour;
}

public function isWorkingDay($date){ 
	$result=1;
	if (!is_a($date, 'DateTime')){return -1;}
	
	//monthholidaysdays
	$monthholidaysdays=$this->getUtildata()->getVaueOfArray($this->param,'monthholidaysdays');
	 $md=$date->format('m-d');
	if(array_key_exists($md,$monthholidaysdays)){$result=0;}
	
	
	//yearholidaysdays
	 $yearholidaysdays=$this->getUtildata()->getVaueOfArray($this->param,'yearholidaysdays');
	 $ymd=$date->format('Y-m-d');
	 if(array_key_exists($ymd,$yearholidaysdays)){$result=0;}

	//weekworkingdays
	 $weekworkingdays=$this->getUtildata()->getVaueOfArray($this->param,'weekworkingdays');
	 if(is_array($weekworkingdays) && sizeof($weekworkingdays)> 0){
		$w=$date->format('w');
		if(!array_key_exists($w,$weekworkingdays)){$result=0;} 
		
	 }
	
	//monthworkingdays
	 $monthworkingdays=$this->getUtildata()->getVaueOfArray($this->param,'monthworkingdays');
	 $md=$date->format('m-d');
	 if(array_key_exists($md, $monthworkingdays)){$result=1;}
	 
	 
	//yearworkingdays
	 $yearworkingdays=$this->getUtildata()->getVaueOfArray($this->param,'yearworkingdays');
	 $ymd=$date->format('Y-m-d');
	 if(array_key_exists($ymd,$yearworkingdays)){$result=1;}
	 
	 return $result;
}


public function isWorkingHour($date){ 
	$result=1;
	if (!is_a($date, 'DateTime')){return -1;}
	
	$hour=$date->format('H');
	$hour=$hour+0;
	
	$dayworkinghours=$this->getUtildata()->getVaueOfArray($this->param,'dayworkinghours');
	
	//monthholidaysdays
	$monthholidaysdays=$this->getUtildata()->getVaueOfArray($this->param,'monthholidaysdays');
	$md=$date->format('m-d');
	if(array_key_exists($md,$monthholidaysdays)){$result=0;}
	
	
	//yearholidaysdays
	 $yearholidaysdays=$this->getUtildata()->getVaueOfArray($this->param,'yearholidaysdays');
	 $ymd=$date->format('Y-m-d');
	 if(array_key_exists($ymd,$yearholidaysdays)){$result=0;}
	
	
	//weekworkingdays
	 $weekworkingdays=$this->getUtildata()->getVaueOfArray($this->param,'weekworkingdays');
	 if(is_array($weekworkingdays) && sizeof($weekworkingdays)> 0){
		$w=$date->format('w');
		if(!array_key_exists($w,$weekworkingdays)){$result=0;} 
		
	 }
	
	 //hour working
	 if($result==1){
		if(!array_key_exists($hour,$dayworkinghours)){$result=0;}
	 }
	 //monthworkingdays
	 $monthworkingdays=$this->getUtildata()->getVaueOfArray($this->param,'monthworkingdays');
	 $md=$date->format('m-d');
	 if(array_key_exists($md, $monthworkingdays)){
		 $hours=$this->getUtildata()->getVaueOfArray($this->param,'monthworkingdays.'.$md,true);
		 
		 if(is_array($hours)){
			  if(array_key_exists($hour,$hours)){ $result=1;}
			  else{ $result=0;}
		 }else{
			if(array_key_exists($hour,$dayworkinghours)){$result=1;} 
		 }
		 
	}

	 
	//yearworkingdays
	 $yearworkingdays=$this->getUtildata()->getVaueOfArray($this->param,'yearworkingdays');
	 $ymd=$date->format('Y-m-d');
	 if(array_key_exists($ymd,  $yearworkingdays)){
		 $hours=$this->getUtildata()->getVaueOfArray($this->param,'yearworkingdays.'.$ymd,true);
		 if(is_array($hours)){
			  if(array_key_exists($hour,$hours)){ $result=1;}
			   else{ $result=0;}
		 }else{
			 if(array_key_exists($hour,$dayworkinghours)){$result=1;} 
		 }
		
	}
	 
	 return $result;
}
public function getParam() {
    return $this->param;
}

public function setParam($param) {
   $this->param = $param;
}
}
