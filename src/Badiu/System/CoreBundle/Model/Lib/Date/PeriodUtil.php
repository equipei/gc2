<?php

namespace Badiu\System\CoreBundle\Model\Lib\Date;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Badiu\System\CoreBundle\Model\Lib\Date\DOMAINTABLE;
class PeriodUtil  extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
      
    }

    /*@param array('number'=>10,'unittime'=>'system_time_unit_day')
    */
     function castTotimestamp($param) {
            $number=$this->getUtildata()->getVaueOfArray($param, 'number');
            $unittime=$this->getUtildata()->getVaueOfArray($param, 'unittime');
            $timestemap=0;
            if(empty($number)){return 0;}
            if($unittime=='system_time_unit_second'){
                $timestemap=$number;
            }else if($unittime=='system_time_unit_minute'){
                $timestemap=$number*60;
            }else if($unittime=='system_time_unit_hour'){
                $timestemap=$number*60*60;
            }else if($unittime=='system_time_unit_day'){
                $timestemap=$number*60*60*24;
            }else if($unittime=='system_time_unit_week'){
                $timestemap=$number*60*60*24*7;
            }else if($unittime=='system_time_unit_month'){
                $timestemap=$number*60*60*24*30;
            }else if($unittime=='system_time_unit_year'){
                $timestemap=$number*60*60*24*365;
            }  
            return $timestemap;
     }

     function modifyDate($date, $unittime,$number, $matopertator="+") {
            $param=array('unittime'=>$unittime,'number'=>$number);
            $calctimestamp=$this->castTotimestamp($param);
            $currenttimestap=$date->getTimestamp();
            if($matopertator=="+"){$newtimestamp=$currenttimestap+$calctimestamp;}
            else if($matopertator=="-"){$newtimestamp=$currenttimestap-$calctimestamp;}
            $date->setTimestamp($newtimestamp);
            return $date;
     }
     function startEndDatePerido($date1,$date2, $unittime) {
        $starthour = $date1->format('H');
        $startminutes = $date1->format('i');

        $endhour = $date2->format('H');
        $endminutes = $date2->format('i');

        
        if($unittime=='system_time_unit_minute'){
            $date1->setTime ($starthour,$startminutes,0);
            $date2->setTime ($endhour,$endminutes,59);
        }else if($unittime=='system_time_unit_hour'){
            $date1->setTime ($starthour,0,0);
            $date2->setTime ($endhour,59,59);
        }else if($unittime=='system_time_unit_day' || $unittime=='system_time_unit_week' || $unittime=='system_time_unit_month' || $unittime=='system_time_unit_year'  ){
            $date1->setTime (0,0,0);
            $date2->setTime (23,59,59);
        }
        $dateperiod = new \stdClass();
          $dateperiod->date1=$date1;
        $dateperiod->date2=$date2;

        return  $dateperiod;
     }
	 
	 function startEndDatePeriod($param) {
		 $dateperiod = new \stdClass();
         $dateperiod->date1=null;
         $dateperiod->date2=null;
		 
		 $year=$this->getUtildata()->getVaueOfArray($param, 'year');
         $month=$this->getUtildata()->getVaueOfArray($param, 'month');
		 if(empty($year)){return $dateperiod;}
		 
		 $monthstart=1;
		 $monthend=12;
		 
		 if(!empty($month)){
			 $monthstart=$month;
			$monthend=$month;
		 }
		 $d1=new \DateTime();
		 $d1->setDate($year,$monthstart, 1);
		 $d1->setTime (0,0,0);
		 
		
		 $d2=new \DateTime();
		 $d2->setDate($year,$monthend, 1);
		 $d2->setTime (23,59,59);
		$lastmonthday=$d2->format('t');
		
		 $d2->setDate($year,$monthend, $lastmonthday);
		
         $dateperiod->date1=$d1;
         $dateperiod->date2=$d2;

        return  $dateperiod;
     }
     function makeDateByOperator($param) {
           
            $dateperiod = new \stdClass();
            $dateperiod->date1=null;
            $dateperiod->date2=null;

            $number=$this->getUtildata()->getVaueOfArray($param, 'number');
            $unittime=$this->getUtildata()->getVaueOfArray($param, 'unittime');
            $operator=$this->getUtildata()->getVaueOfArray($param, 'operator');
            if(empty($number)){return $dateperiod;}
            if(empty($unittime)){return $dateperiod;}
            if(empty($operator)){return $dateperiod;}
            $time1=new \DateTime();
            $time2=new \DateTime();
            //past
            if($operator==DOMAINTABLE::$OPERATOR_PAST_TEXT_FROM){
                $time1=$this->modifyDate($time1, $unittime,$number, "-");
            }
            else if($operator==DOMAINTABLE::$OPERATOR_PAST_TEXT_UNTIL){
                $time1=$this->modifyDate($time1, $unittime,$number, "-");
                $time2=null;
            }
            else if($operator==DOMAINTABLE::$OPERATOR_PAST_TEXT_ON){
                $last=$number;
               
                $time1=$this->modifyDate($time1, $unittime,$last, "-");
                $time2=$this->modifyDate($time2, $unittime,$number, "-");
               $periodstend=$this->startEndDatePerido($time1,$time2, $unittime);
               $time1=$periodstend->date1;
               $time2=$periodstend->date2;
            }
            
    
            //future
            else if($operator==DOMAINTABLE::$OPERATOR_FUTURE_TEXT_UNTIL){
                $time1=$this->modifyDate($time1, $unittime,$number, "+");
                $time2=null;
            }
            else if($operator==DOMAINTABLE::$OPERATOR_FUTURE_TEXT_AFTER){
                $time1=$this->modifyDate($time1, $unittime,$number, "+");
                $time2=null;
            }
            else if($operator==DOMAINTABLE::$OPERATOR_FUTURE_TEXT_ON){
                $next=$number;
               
                $time1=$this->modifyDate($time1, $unittime,$number, "+");
                $time2=$this->modifyDate($time2, $unittime,$next, "+");
               $periodstend=$this->startEndDatePerido($time1,$time2, $unittime);
               $time1=$periodstend->date1;
               $time2=$periodstend->date2;
            }
            else if($operator==DOMAINTABLE::$OPERATOR_FUTURE_TEXT_FROM){
                $time2=$this->modifyDate($time2, $unittime,$number, "+");
            }
           
             $dateperiod->date1=$time1;
            $dateperiod->date2=$time2;

            return  $dateperiod;

     }

   
     function getUnittimeLabel($param) {
        
     }
    
     function castTimeToLabel($time) {

            $label=null;
            $t1=null;
            $t2=null;
            $t3=null;
            //year
          /*  $u=60*60*24*365;
            
            if($time > $u){
                $t1=$time/$u;
                if($t1 != round($t1)){$t1=floor($t1);}
                $r1= $time % $u;
                
                //month
                $u1=60*60*24*30;
                if($r1 > 0 && $r1 > $u1){
                    $t2=$r1/$u1;
                    if($t2 != round($t2)){$t2=floor($t2);}
                }

                //day
                $u2=60*60*24;
                $r2= $r1 % $u2;
                if($r2 > 0 && $r2 > $u2){
                    $t3=$r2/$u2;
                    if($t3 != round($t3)){$t3=floor($t3);}
                }

                if(!empty($t1) && !empty($t2) && !empty($t3)){

                    return $label;
                }
                if(!empty($t1) && !empty($t2) && empty($t3)){

                    return $label;
                }

                if(!empty($t1) && empty($t2) && empty($t3)){

                    return $label;
                }
            }*/
            //month

            //day
            $u=60*60*24;
            if($time >= $u){
                $t1=$time/$u;
                if($t1 != round($t1)){$t1=floor($t1);}
               if($t1 > 1){$label=$this->getTranslator()->trans('badiu.system.time.label.days',array('%day%'=>$t1));}
               else {$label=$this->getTranslator()->trans('badiu.system.time.label.day',array('%day%'=>$t1));}
               return $label;
            }
             //hour

             //minutes

            //seconds

            

           
            return $time;

        
    }
	
	 function castDbStringToDate($date){
		 if (!is_a($date, 'DateTime') && is_string($date)) {
				$date = \DateTime::createFromFormat('Y-m-d H:i:s', $date);
			}
		return $date;
	 }
}
