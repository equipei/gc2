<?php

namespace Badiu\System\CoreBundle\Model\Lib\Date;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class SchedulerRoutineFormat extends BadiuFormat{
     private $configformat;
     private $router;
    
     function __construct(Container $container) {
            parent::__construct($container);
             $this->configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
            
             $this->router=$this->getContainer()->get("router");
       } 

    public  function routineexec($data){
        $value="";
        $timeroutine=$this->getUtildata()->getVaueOfArray($data,'timeroutine');
         $timeroutineparam=$this->getUtildata()->getVaueOfArray($data,'timeroutineparam');
         $timeexeconce=$this->getUtildata()->getVaueOfArray($data,'timeexeconce');
        $param=$this->getJson()->decode($timeroutineparam,true);
		
		$calendarkey=$this->getUtildata()->getVaueOfArray($param,'calendar.key',true);
		$calendartimetype=$this->getUtildata()->getVaueOfArray($param,'calendar.timetype',true);
		
		$calendarmsg="";
		if( !empty($calendarkey)){
			if($calendartimetype== 'onlyworkingdays'){$calendarmsg="  ".$this->getTranslator()->trans('badiu.system.scheduler.calendartimetype.onlyworkingdays.routineinfo');}
			else if($calendartimetype== 'onlyworkingdaysandhours'){$calendarmsg="  ".$this->getTranslator()->trans('badiu.system.scheduler.calendartimetype.onlyworkingdaysandhours.routineinfo');}
		}
         if($timeroutine=='once'){
             if(!empty($timeexeconce)){
                 $texttime=$this->configformat->defaultDatetime($timeexeconce);
                 $value= $this->getTranslator()->trans('badiu.system.time.schedulerroutine.once',array('%texttime%'=>$texttime));
                
             }
          }if($timeroutine=='system_timeinterval'){
              $texttime=$this->getRoutinePeriodLabel($timeroutine,$timeroutineparam);
              $value= $this->getTranslator()->trans('badiu.system.time.schedulerroutine.interval',array('%texttime%'=>$texttime));
          }else if($timeroutine=='system_daily'){
                  $hour=$this->getUtildata()->getVaueOfArray($param,'hour');
                  $minute=$this->getUtildata()->getVaueOfArray($param,'minute');
                  if($hour <10){$hour="0$hour";}
                   if($minute <10){$minute="0$minute";}
                   $hourtxt="$hour:$minute";
                  $value=$this->getTranslator()->trans('badiu.system.time.schedulerroutine.intervalday',array('%time%'=>$hourtxt));
                 $value.=$calendarmsg;
                return $value;
            }
        else if($timeroutine=='system_weekly'){
                  $dayofweek=$this->getUtildata()->getVaueOfArray($param,'day');
                  $dayofweeklabel=$this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions')->getDayOfWeekLabel($dayofweek,true);
                  $hour=$this->getUtildata()->getVaueOfArray($param,'hour');
                  $minute=$this->getUtildata()->getVaueOfArray($param,'minute');
                  if($hour <10){$hour="0$hour";}
                   if($minute <10){$minute="0$minute";}
                   $hourtxt="$hour:$minute";
                  if($dayofweek==0 || $dayofweek==6){
                      $value=$this->getTranslator()->trans('badiu.system.time.schedulerroutine.intervalallm',array('%day%'=>$dayofweeklabel,'%time%'=>$hourtxt));
                  }else{
                      $value=$this->getTranslator()->trans('badiu.system.time.schedulerroutine.intervalallf',array('%day%'=>$dayofweeklabel,'%time%'=>$hourtxt));
                  }
				   $value.=$calendarmsg;
                return $value;
            }else if($timeroutine=='system_monthly'){
                  $dayofmonth=$this->getUtildata()->getVaueOfArray($param,'day');
                  $hour=$this->getUtildata()->getVaueOfArray($param,'hour');
                  $minute=$this->getUtildata()->getVaueOfArray($param,'minute');
                  if($hour <10){$hour="0$hour";}
                   if($minute <10){$minute="0$minute";}
                   $hourtxt="$hour:$minute";
                  $value=$this->getTranslator()->trans('badiu.system.time.schedulerroutine.intervaldayofmonth',array('%day%'=>$dayofmonth,'%time%'=>$hourtxt));
                 $value.=$calendarmsg ;
                return $value;
            }
			 $value.=$calendarmsg;
         return $value;
    }
    public  function periodexec($data){
        $value=""; 
        $timestart=$this->getUtildata()->getVaueOfArray($data,'execperiodstart');
        $timeend=$this->getUtildata()->getVaueOfArray($data,'execperiodend');
        
        if(!empty($timestart)){$timestart=$this->configformat->defaultDatetime($timestart);}
        if(!empty($timeend)){$timeend=$this->configformat->defaultDatetime($timeend);}
        
       
        
        if(!empty($timestart) && empty($timeend)){
            $value= $this->getTranslator()->trans('badiu.system.time.periodstartat',array('%timestart%'=>$timestart));
        } else if(empty($timestart) && !empty($timeend)){
            $value= $this->getTranslator()->trans('badiu.system.time.periodendat',array('%timeend%'=>$timeend));
        }
         else if(!empty($timestart) && !empty($timeend)){
            $value= $this->getTranslator()->trans('badiu.system.time.periodbetween',array('%timestart%'=>$timestart,'%timeend%'=>$timeend));
        }
         return $value;
    }
    
  public  function getRoutinePeriodLabel($timeroutine,$timeroutineparam){
            $label="";
            $param=$this->getJson()->decode($timeroutineparam,true);
            if($timeroutine=='system_timeinterval'){
                 $value=$this->getUtildata()->getVaueOfArray($param,'value');
                $timeunit=$this->getUtildata()->getVaueOfArray($param,'timeunit');
                $timeunit= $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions')->getTimeUnitFullKey($timeunit);
                if($timeunit=='system_time_unit_second'){
                    if($value==1 || $value==0 ){$label=$this->getTranslator()->trans('badiu.system.time.countpast.second',array('%numberx%'=>$value));}
                    else {$label=$this->getTranslator()->trans('badiu.system.time.countpast.seconds',array('%numberx%'=>$value));}
                }else if($timeunit=='system_time_unit_minute'){
                    if($value==1 || $value==0 ){$label=$this->getTranslator()->trans('badiu.system.time.countpast.minute',array('%numberx%'=>$value));}
                    else {$label=$this->getTranslator()->trans('badiu.system.time.countpast.minutes',array('%numberx%'=>$value));}
                }if($timeunit=='system_time_unit_hour'){
                    if($value==1 || $value==0 ){$label=$this->getTranslator()->trans('badiu.system.time.countpast.hour',array('%numberx%'=>$value));}
                    else {$label=$this->getTranslator()->trans('badiu.system.time.countpast.hours',array('%numberx%'=>$value));}
                }if($timeunit=='system_time_unit_day'){
                    if($value==1 || $value==0 ){$label=$this->getTranslator()->trans('badiu.system.time.countpast.day',array('%numberx%'=>$value));}
                    else {$label=$this->getTranslator()->trans('badiu.system.time.countpast.days',array('%numberx%'=>$value));}
                }if($timeunit=='system_time_unit_week'){
                    if($value==1 || $value==0 ){$label=$this->getTranslator()->trans('badiu.system.time.countpast.week',array('%numberx%'=>$value));}
                    else {$label=$this->getTranslator()->trans('badiu.system.time.countpast.weeks',array('%numberx%'=>$value));}
                }if($timeunit=='system_time_unit_month'){
                    if($value==1 || $value==0 ){$label=$this->getTranslator()->trans('badiu.system.time.countpast.month',array('%numberx%'=>$value));}
                    else {$label=$this->getTranslator()->trans('badiu.system.time.countpast.months',array('%numberx%'=>$value));}
                }if($timeunit=='system_time_unit_year'){
                    if($value==1 || $value==0 ){$label=$this->getTranslator()->trans('badiu.system.time.countpast.year',array('%numberx%'=>$value));}
                    else {$label=$this->getTranslator()->trans('badiu.system.time.countpast.years',array('%numberx%'=>$value));}
                }  
            }else if($timeroutine=='system_weekly'){
                  $dayofweek=$this->getUtildata()->getVaueOfArray($param,'day');
                  $dayofweeklabel=$this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions')->getDayOfWeekLabel($dayofweek);
                  $hour=$this->getUtildata()->getVaueOfArray($param,'hour');
                  $minute=$this->getUtildata()->getVaueOfArray($param,'minute');
                  
                  if($hour <10){$hour="0$hour";}
                   if($minute <10){$minute="0$minute";}
                  
                  $hourtxt="$hour:$minute";
                 $label=$this->getTranslator()->trans('badiu.system.time.schedulerroutine.dayofweekhour',array('%dayofweek%'=>$dayofweeklabel,'%hour%'=>$hourtxt));
                  
            }
        return $label; 
     }
     
     

  
  public  function reportfilter($data){
      return null; 
  }
       
}
