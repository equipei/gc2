<?php

namespace Badiu\System\CoreBundle\Model\Lib\Date;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
//REVIEW IF $this->timelastexec IS FALSE
class SchedulerRoutine extends BadiuModelLib {
      private $timeroutine;//timeinterval|daily|monthly|weekly|once
    private $timeroutineparam; //timeinterval -> timeunit: minute|hour|day|week|month  / value: xxx
                               //daily -> hour: xxx |minute: xxx
                               //monthly -> day: xxx | hour: xxx
                               //weekly -> day: 0-6 | hour: xxx | minute: xxx
                               //once  -> date: xxx
    /**
     * @var \DateTime
    */
    private $timelastexec;
     /**
     * @var \DateTime
    */
    private $timeexeconce;
    
     /**
     * @var \DateTime
    */
    private $execperiodstart;
    
     /**
     * @var \DateTime
    */
    private $execperiodend;         
    
	/**
     * @var Object
    */
    private $calendar; 
	
     public function __construct(Container $container) {
        parent::__construct($container);
    }
    
     function initParam($param) {
       
         if(isset($param['timeroutine'])){$this->timeroutine=$param['timeroutine'];}
         if(isset($param['timeroutineparam'])){$this->timeroutineparam=$param['timeroutineparam'];}
         if(isset($param['timelastexec'])){$this->timelastexec=$param['timelastexec'];}
         if(isset($param['timeexeconce'])){$this->timeexeconce=$param['timeexeconce'];}
         if(isset($param['execperiodstart'])){$this->execperiodstart=$param['execperiodstart'];}
         if(isset($param['execperiodend'])){$this->execperiodend=$param['execperiodend'];}
		 $this->initCalendar();
		
         
     }
	 
	  function initCalendar() {
		 $this->calendar=$this->getContainer()->get('badiu.system.core.lib.date.calendartimeutil');
		 $calendarkey=null;
		 if(isset($this->timeroutineparam->calendar->key)){ $calendarkey=$this->timeroutineparam->calendar->key;}
		 if(isset($this->timeroutineparam->calendar->timetype)){
			 $calendartimetype=$this->timeroutineparam->calendar->timetype;
			 if(empty($calendartimetype)){$calendarkey=null;}
		 }else{$calendarkey=null;}
		 if($calendarkey=='default'){
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			$badiuSession->setSystemdata($this->getSystemdata());
			$calendarconfig=$badiuSession->getValue('badiu.system.core.param.config.date.calendar');
			$this->calendar->init($calendarconfig);
			
		 }
		 
	  }
	  
     function canExec() {
         $result=FALSE;
        
         $result=$this->checkPeriodOfExec();
        
         if(!$result){return $result;}
        
         //review it
        // if(empty($this->timelastexec)){return true;} 
       
         if($this->timeroutine=='system_timeinterval'){$result=$this->timeinterval();}
         else if($this->timeroutine=='system_daily'){$result=$this->daily();}
         else if($this->timeroutine=='system_monthly'){$result=$this->monthly();}
         else if($this->timeroutine=='system_weekly'){$result=$this->weekly();}
         else if($this->timeroutine=='once'){$result=$this->once();}
         return $result;
         
         
     }
      function checkPeriodOfExec() {
          $result=false;//$now->getTimestamp()
           $now =new \Datetime();
           
           // check calendar day hour working
		    $twork=$this->checkCalendarWorkingTime();
			//echo "tw: $twork ";exit;
			if(!$twork){return false;}
			
            //without time
            if(empty($this->execperiodstart) && empty($this->execperiodend)){return true;}
           
            //only timestart
            if(!empty($this->execperiodstart) && empty($this->execperiodend)){
                if($this->execperiodstart->getTimestamp() < $now->getTimestamp()){return true;}
                else if ($this->execperiodstart->getTimestamp() > $now->getTimestamp()){return false;}
            }

           //only timeend
           if(!empty($this->execperiodend) && empty($this->execperiodstart)){
                if($this->execperiodend->getTimestamp() < $now->getTimestamp()){$result=false;}
                else if($this->execperiodend->getTimestamp() > $now->getTimestamp()){$result=true;}
           }

           //timestart and timeend
           if(!empty($this->execperiodstart) && !empty($this->execperiodend)){
               if($this->execperiodstart->getTimestamp() > $this->execperiodend->getTimestamp()){return false;}
              
               if($this->execperiodstart->getTimestamp() < $now->getTimestamp() && $this->execperiodend->getTimestamp() > $now->getTimestamp()){return true;}
                
           } 
         
          return $result;
      }   
     
      function timeinterval() {
          $timeunit='hour';
          $value=2;
          if(empty($this->timelastexec)){return true;} 
          if(isset($this->timeroutineparam->timeunit)){$timeunit=$this->timeroutineparam->timeunit;}
          if(isset($this->timeroutineparam->value)){$value=$this->timeroutineparam->value;}
         
		   $now =new \Datetime();
		   
		   $hournotwork=0;
		   $daynotwork=0;
		   if( isset($this->timeroutineparam->calendar->key) &&  isset($this->timeroutineparam->calendar->timetype) && is_array($this->calendar->getParam())){
			   
			   $date1=clone $this->timelastexec;
			   $date2=clone $now;
			   
			   $d0=$date1->format('d/m/Y: H');
			   if($timeunit=='minute'){}
			   else if($timeunit=='hour'){
				   $hournotwork=$this->calendar->countNonWorkingHour($date1,$date2);
			   }else{
				    $daynotwork=$this->calendar->countNonWorkingDay($date1,$date2);
			   }
          
		   }
		   
          if($timeunit=='minute'){
              $this->timelastexec->modify('+'.$value.' minutes');
          }else if($timeunit=='hour'){
			  $value=$value+$hournotwork;
              $this->timelastexec->modify('+'.$value.' hours');
          }else if($timeunit=='day'){
			   $value=$value+$daynotwork;
              $this->timelastexec->modify('+'.$value.' day');
          }else if($timeunit=='week'){
              $value=$value*7;
			  $value=$value+$daynotwork;
              $this->timelastexec->modify('+'.$value.' day');
          }else if($timeunit=='month'){
              $this->timelastexec->modify('+'.$value.' month');
			  if($daynotwork > 0){ $this->timelastexec->modify('+'.$daynotwork.' day');}
          }else if($timeunit=='year'){
              $value=$value*12;
              $this->timelastexec->modify('+'.$value.' month');
			  if($daynotwork > 0){ $this->timelastexec->modify('+'.$daynotwork.' day');}
          }
          $result=false;
         
           if($this->timelastexec->getTimestamp() < $now->getTimestamp()){  $result=true;}
           return $result;
      }
     function daily() {
         //daily -> hour: xxx |minute: xxx
     
         $hour=0;
         $minute=0;
          if(isset($this->timeroutineparam->hour)){$hour=$this->timeroutineparam->hour;}
          if(isset($this->timeroutineparam->minute)){$minute=$this->timeroutineparam->minute;}
          
      
          $lasttimeexecf= null;
          $nowf= null;
          if(!empty( $this->timelastexec)){$lasttimeexecf= $this->timelastexec->format('Y-m-d');}
          $now =new \Datetime();
                   
          $nowf=$now->format('Y-m-d');
          
          $result=false;
        
          if($lasttimeexecf!=null && $lasttimeexecf==$nowf){ 
              $result=false;
              return $result; 
          }
         
           $nowhour = $now->format('H');
           $nowminutes = $now->format('i');
          if($nowhour >=$hour && $nowminutes>=$minute){ $result=true;}
          return $result; 
     }
     function monthly() {
          //monthly -> day: xxx | hour: xxx
     
         $day=1;
         $hour=0;
          if(isset($this->timeroutineparam->day)){$day=$this->timeroutineparam->day;}
          if(isset($this->timeroutineparam->hour)){$hour=$this->timeroutineparam->hour;}
          
          $lasttimeexecf= null;
          $nowf= null;
          if(!empty( $this->timelastexec)){$lasttimeexecf= $this->timelastexec->format('Y-m-d');}
          $now =new \Datetime();

          $nowf=$now->format('Y-m-d');
          
          $result=false;
          if($lasttimeexecf!=null && $lasttimeexecf==$nowf){ 
              $result=false;
               return $result; 
          }
          
          $nowday= $now->format('d');
          $nowhour = $now->format('H');
        
          if($nowday == $day && $nowhour >= $hour){ $result=true;}
          
          return $result; 
     }
     
     function weekly() {
          //once  -> date: xxx
     
         $day=1;
         $hour=0;
         $minutes=0;
       
          if(isset($this->timeroutineparam->day)){$day=$this->timeroutineparam->day;}
          if(isset($this->timeroutineparam->hour)){$hour=$this->timeroutineparam->hour;}
          if(isset($this->timeroutineparam->minute)){$minutes=$this->timeroutineparam->minute;}
          
        
          
          $lasttimeexecf= null;
          $nowf= null;
          if(!empty( $this->timelastexec)){$lasttimeexecf= $this->timelastexec->format('Y-m-d');}
          $now =new \Datetime();
         
          $nowweekday= $now->format('w');
          $nowf=$now->format('Y-m-d');
        
          $result=false;
          if($lasttimeexecf!=null && $lasttimeexecf==$nowf){ 
              $result=false;
              return $result; 
          }
          
         if($nowweekday!=$day){ 
              $result=false;
              return $result; 
         }
         
          $nowhour = $now->format('H');
          $nowminutes=$now->format('i');
          
          
          if($nowhour >= $hour){ 
             if($nowhour == $hour && $nowminutes >=$minutes ){ $result=true;}
             else{$result=true;}
          }
          
          return $result; 
     }
     
     function once() {
         if(!empty($this->timelastexec)){return false;}
         $date=$this->timeexeconce;
        
         if(!empty($date)){
             $timedate=$date->getTimestamp();
             $now =new \Datetime();
             $nowtimedate=$now->getTimestamp();
             
             $stdatenow=$now->format('Y-m-d');
             $stdateconf=$date->format('Y-m-d');
             if($nowtimedate>=$timedate && $stdatenow==$stdateconf){return true;}
         } 
           return false;
     }
	 
	 function checkCalendarWorkingTime() {
		
		 if(!isset($this->timeroutineparam->calendar->key)){return true;}
		 if(!isset($this->timeroutineparam->calendar->timetype)){return true;}
		 $timetype=$this->timeroutineparam->calendar->timetype;
		 if(!($timetype == 'onlyworkingdays' || $timetype == 'onlyworkingdaysandhours' )){return true;}
		 if(!is_array($this->calendar->getParam())){return true;}
		 $date =new \DateTime();  
		 if ($timetype == 'onlyworkingdays'){
			 $iswork=$this->calendar->isWorkingDay($date);
			 if($iswork==0){return false;}
		 }
		 else if ($timetype == 'onlyworkingdaysandhours'){
			 $iswork=$this->calendar->isWorkingDay($date);
			 if($iswork==0){return false;}
			 
			 $isworkh=$this->calendar->isWorkingHour($date);
			 if($isworkh==0){return false;}
		 }
		 return true;
		 
	 }
     function getTimeroutineparam() {
         return $this->timeroutineparam;
     }

     function getTimelastexec() {
         return $this->timelastexec;
     }

     function setTimeroutineparam($timeroutineparam) {
         $this->timeroutineparam = $timeroutineparam;
     }

     function setTimelastexec($timelastexec) {
         $this->timelastexec = $timelastexec;
     }

     function getTimeroutine() {
         return $this->timeroutine;
     }

     function setTimeroutine($timeroutine) {
         $this->timeroutine = $timeroutine;
     }

     function getTimeexeconce() {
         return $this->timeexeconce;
     }

     function getExecperiodstart() {
         return $this->execperiodstart;
     }

     function getExecperiodend() {
         return $this->execperiodend;
     }

     function setTimeexeconce(\DateTime $timeexeconce) {
         $this->timeexeconce = $timeexeconce;
     }

     function setExecperiodstart(\DateTime $execperiodstart) {
         $this->execperiodstart = $execperiodstart;
     }

     function setExecperiodend(\DateTime $execperiodend) {
         $this->execperiodend = $execperiodend;
     }

	 function setCalendar($calendar) {
         $this->calendar = $calendar;
     }

     function getCalendar() {
         return $this->calendar;
     }

}
