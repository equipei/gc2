<?php

namespace Badiu\System\CoreBundle\Model\Lib\Date;

class Hour {

   //format h|t h: hh:mm:ss t: xh x min e xsecon
    public function convertSeconds($seconds,$format='h') {
        
        $h="00";
        $m="00";
        $s="00";
        if(empty($seconds)){return null; }
        if($seconds==0){return null; }
        $time="";
        if($seconds < 60) {
            $s=$this->format2Digit($seconds);
            $time="$h:$m:$s";
            
        }
        else if($seconds < 3600) {
            $m=round($seconds/60);
            $m=$this->format2Digit($m);
            
            $s=$seconds%60;
            $s=$this->format2Digit($s);
            
            $time="$h:$m:$s";
       } else if($seconds >= 3600) {
            $h=floor($seconds/60/60);
            $sm=$seconds-($h*60*60);
            
            $m=floor($sm/60);
            $s=$sm-($m*60);
            $s=$this->format2Digit($s);
            
            
            $h=$this->format2Digit($h);
            $m=$this->format2Digit($m);
            $s=$this->format2Digit($s);
            $time="$h:$m:$s";
       }
        return $time;
    }

     private function format2Digit($number) {
         $result="00";
         
         if(empty($number)){ return  $result;}
         $number=(int)$number;
         if((int)$number < 10) { $result="0".$number;}
        else {$result=$number;}
          return  $result;
     }

      public function isBusiness($date) {
          
          $weekday=$date->format('w');
          if($weekday==0){return false;}
          if($weekday==6){return false;}

          $hour = $date->format('H');
         if($hour >= 18 ){return false;}
         if($hour ==12){return false;}
         if($hour ==13){return false;}
         if($hour < 8){return false;}
          
         return true;
     }

}
