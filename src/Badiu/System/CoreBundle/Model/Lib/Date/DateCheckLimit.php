<?php

namespace Badiu\System\CoreBundle\Model\Lib\Date;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class DateCheckLimit  extends BadiuModelLib {

    private $fparam;
    private $fconfig;
    private $periodutil;
    private $formdefaultdata;
    private $formcasttimeperiod; 
    private $fieldlabel1;
    private $fieldlabel2;
    function __construct(Container $container) {
        parent::__construct($container);
        $this->periodutil=$this->getContainer()->get('badiu.system.core.lib.date.periodutil');
        $this->formdefaultdata= $this->getContainer()->get('badiu.system.core.lib.form.defaultdata');
        $this->formcasttimeperiod= $this->getContainer()->get('badiu.system.core.lib.form.casttimeperiod');
        $this->fieldlabel1=null;
        $this->fieldlabel2=null;
    }
    function init($fparam,$fconfig) {
        $this->fparam=$fparam;
        $this->fconfig=$fconfig;

    }

    function periodinfo($paramekey,$forcevalue=false) {
        $result=new \stdClass();
        $result->interval=0;
        $result->limitoftime=0;
        $result->lock=0;
        $result->message=null;
        $result->existvalue=false;
        
        $result->interval=$this->formcasttimeperiod->paramkeyToArray($this->getFparam(),$paramekey);

        $existkey=$this->formcasttimeperiod->existParamkey($this->getFparam(),$paramekey);
        $existvalue=$this->formcasttimeperiod->existParamkeyValue($this->getFparam(),$paramekey);

        if($existkey && !$existvalue){
            $result->existvalue=false;
        }else if($existkey && $existvalue){ 
            $result->existvalue=true;
        }else if(!$existkey && !$result->existvalue){ 
            $result->existvalue=true;// its seted by defult limit value
        }
    

        $limitoftime=$this->getUtildata()->getVaueOfArray($this->getFconfig()->getMaxlimit(), $paramekey);
   
        $limitoftime=$this->formdefaultdata->getArray($limitoftime);
  

        $limitoftime=$this->periodutil->castTotimestamp($limitoftime);
        $result->interval=$this->periodutil->castTotimestamp($result->interval);


        $result->limitoftime=$limitoftime;
        if(empty($result->limitoftime)){
            $result->lock=0;
            return  $result;
        } 
        if($forcevalue && !$result->existvalue){
            $result->lock=1;
           $result->message=$this->getTranslator()->trans('badiu.system.date.checklimit.datefieldrequired',array('%fieldlabel%'=>$this->getFieldlabel1()));
           //"Nao foi definido nenhum valor de filtro de periodo no campo $paramekey";
        }
        if($result->interval > $result->limitoftime  ){
            $labellimittime=$this->getContainer()->get('badiu.system.core.lib.date.periodutil')->castTimeToLabel($result->limitoftime);
            $result->lock=2;
            $result->message=$this->getTranslator()->trans('badiu.system.date.checklimit.datelimitexceeded',array('%fieldlabel%'=>$this->getFieldlabel1(),'%maxperiod%'=>$labellimittime));
            //"O filtro de perido feito no campo $paramekey ($result->interval) ultrapassou o valor limite configurado $result->limitoftime ";
        }

        return  $result;
    }

    function period($paramekey,$orrorcode,$forcevalue=false) {
        $result=$this->periodinfo($paramekey,$forcevalue); 
        
        if($result->lock==0){return null;}

        $param=array();
        $param['error.code']=$orrorcode;
        $param['error.message']=$result->message;

        return $param;
    }

    function dateinfo($paramekey,$forcevalue=false) {
        $badiutdate=$this->getContainer()->get('badiu.system.core.lib.form.castdate');
        $vdate=$badiutdate->paramkeyToArray($this->getFparam(),$paramekey,true);
        $result=$this->getDateInterval($vdate);
        $result->limitoftime=$this->getUtildata()->getVaueOfArray($this->getFconfig()->getMaxlimit(), $paramekey);
        $result->limitoftime=$this->formdefaultdata->getArray($result->limitoftime);
        $result->limitoftime=$this->periodutil->castTotimestamp($result->limitoftime);
       
        if(empty($result->limitoftime)){
            $result->lock=0;
            return  $result;
        }

        if($result->lock > 0){return $result;}

        if($forcevalue && !$result->existvalue){
            $result->lock=1;
            $result->message=$this->getTranslator()->trans('badiu.system.date.checklimit.datefieldrequired',array('%fieldlabel%'=>$this->getFieldlabel1()));
            //"Nao foi definido nenhum valor de filtro de periodo no campo $paramekey";
        }
        
        if($result->interval > $result->limitoftime  ){
            $labellimittime=$this->getContainer()->get('badiu.system.core.lib.date.periodutil')->castTimeToLabel($result->limitoftime);
            $result->lock=2;
            $result->message=$this->getTranslator()->trans('badiu.system.date.checklimit.datelimitexceeded',array('%fieldlabel%'=>$this->getFieldlabel1(),'%maxperiod%'=>$labellimittime));
            //"O filtro de perido feito no campo $paramekey ($result->interval) ultrapassou o valor limite configurado $result->limitoftime ";
        }

        return  $result;

    }
    function date($paramekey,$orrorcode,$forcevalue=false) {
        $result=$this->dateinfo($paramekey,$forcevalue); 
        if($result->lock==0){return null;}

        $param=array();
        $param['error.code']=$orrorcode;
        $param['error.message']=$result->message;
         return $param;
    
    }
    function periodordate($paramekeyp,$paramekey,$orrorcode) {
        $resultperid=$this->periodinfo($paramekeyp,false); 
        $resultdate=$this->dateinfo($paramekey,false); 
        
        if(empty($resultperid->limitoftime) && empty($resultdate->limitoftime)){
            return null;
        }
       if(!$resultperid->existvalue && !$resultdate->existvalue){
            $param=array();
            $param['error.code']=$orrorcode;
            $param['error.message']=$this->getTranslator()->trans('badiu.system.date.checklimit.datefieldsrequired',array('%fieldlabel1%'=>$this->getFieldlabel1(),'%fieldlabel2%'=>$this->getFieldlabel2()));
            //"Nao foi feito filtro de nenhuma data  seja no campo $paramekeyp ou no campo $paramekey. Faca filtro em pelo menos um dos campos ";
            return $param;
       }
       if($resultperid->existvalue ){
            $result=$this->period($paramekeyp,$orrorcode,true); 
            if(!empty($result)){return $result;}
       }

       if($resultdate->existvalue ){
            $result=$this->date($paramekey,$orrorcode,true); 
             if(!empty($result)){return $result;}
       }

       return null;
    
    }
    function getDateInterval($param) {
        $result=new \stdClass();
        $result->interval=0;
        $result->limitoftime=0;
        $result->lock=0;
        $result->message=null;
        $result->existvalue=false;
       
      
        $operator=$this->getUtildata()->getVaueOfArray($param,'operator');
        $date1=$this->getUtildata()->getVaueOfArray($param,'date1');
        $date2=$this->getUtildata()->getVaueOfArray($param,'date2');

        if(empty($operator)){
            return  $result;
        }
        if(!empty($operator) && !empty($date1)){$result->existvalue=true;}
      
        if($operator=='from' || $operator=='until' || $operator=='before'){
            //review
            $result->lock=11;
             $result->message=$this->getTranslator()->trans('badiu.system.date.checklimit.dateoperatoropen');
             //"O filtro de data usa operador que pode gerar quantidade de dados alem do limite configurado";
            //review
            $now=new \DateTime();
            $now=$now->getTimestamp();
            if($date1 >0 && $now > $date1){ $result->interval=$now-$date1;}
            else if($date1 >0 && $now < $date1){ $result->interval=$date1-$now;}

        }else if($operator=='different'){
             $result->lock=10;
             $result->message=$this->getTranslator()->trans('badiu.system.date.checklimit.dateoperatoropen');
             //"O filtro de data usa operador diferente isso gera um filtro de dados maior que o limite permitido";
          }else if($operator=='on'){
             $result->lock=0;
        }else if($operator=='between'){
            if($date1==0){ 
                 $result->lock=11;
                 $result->message=$this->getTranslator()->trans('badiu.system.date.checklimit.datefieldrequireddate1',array('%fieldlabel%'=>$this->getFieldlabel1()));
                 //"O filtro de data usa operador no periodo de ficou faltando a definicao da primeira data";
            }
            if($date2==0){
                 $result->lock=12;
                 $result->message=$this->getTranslator()->trans('badiu.system.date.checklimit.datefieldrequireddate2',array('%fieldlabel%'=>$this->getFieldlabel1()));
                 //"O filtro de data usa operador no periodo de e ficou faltando a definicao da segunda data";
                }
           if($date1 >0 && $date2 > 0){ 
                if($date1 > $date2){
                    $result->lock=13;
                    $result->message=$this->getTranslator()->trans('badiu.system.date.checklimit.datefielddate1biggerthendate2',array('%fieldlabel%'=>$this->getFieldlabel1()));
                    //"O filtro de data usa operador no periodo de e primeira data nao pode ser maior que a segunda data";
                }else{$result->interval=$date2-$date1;}
               
            }

          
        }
      return   $result;
    }

    function getFparam() {
        return $this->fparam;
    }

    function setFparam($fparam) {
        $this->fparam = $fparam;
    }


    function getPeriodutil() {
        return $this->periodutil;
    }

    function setPeriodutil($periodutil) {
        $this->periodutil = $periodutil;
    }

    function getFormdefaultdata() {
        return $this->formdefaultdata;
    }

    function setFormdefaultdata($formdefaultdata) {
        $this->formdefaultdata = $formdefaultdata;
    }
    function getFormcasttimeperiod() {
        return $this->formcasttimeperiod;
    }

    function setFormcasttimeperiod($formcasttimeperiod) {
        $this->formcasttimeperiod = $formcasttimeperiod;
    }

    function getFconfig() {
        return $this->fconfig;
    }

    function setFconfig($fconfig) {
        $this->fconfig = $fconfig;
    }

    function getFieldlabel1() {
        return $this->fieldlabel1;
    }

    function setFieldlabel1($fieldlabel1) {
        $this->fieldlabel1 = $fieldlabel1;
    }

    function getFieldlabel2() {
        return $this->fieldlabel2;
    }

    function setFieldlabel2($fieldlabel2) {
        $this->fieldlabel2 = $fieldlabel2;
    }
}
