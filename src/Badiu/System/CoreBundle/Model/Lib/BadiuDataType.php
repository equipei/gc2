<?php
namespace Badiu\System\CoreBundle\Model\Lib;
class BadiuDataType {
     public static $TYPE_STRING="string";
     public static $TYPE_TEXT="text"; 
     public static $TYPE_INTEGER="integer"; 
     public static $TYPE_NUMERIC="numeric"; 
     public static $TYPE_TIME="time"; 
     public static $TYPE_DATE_TIME="date_time";
    
      
}
