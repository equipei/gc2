<?php
namespace Badiu\System\CoreBundle\Model\Lib\Report;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class SummarizeData  extends BadiuModelLib{

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
	function countByYear($param){
		  $period=$this->getUtildata()->getVaueOfArray($param,'_period'); 
		  if(!empty($period)){unset($param['_period']);}
		  
		  $result=array();
		  $data=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
		  $periodutil=$this->getContainer()->get('badiu.system.core.lib.date.periodutil');
		  if($period=='allyears'){
			  $perioddto=$this->getMinMaxYear($param);
			
			  $firstdate=$this->getUtildata()->getVaueOfArray($perioddto,'firstdate'); 
			  $firstdate= $periodutil->castDbStringToDate($firstdate);
			  if(empty($firstdate)){return $result;}
			  $firstdate=$firstdate->format('Y');
			  
			  $lasdate=$this->getUtildata()->getVaueOfArray($perioddto,'lastdate'); 
			 
			  $lasdate= $periodutil->castDbStringToDate($lasdate);
			  if(empty( $lasdate)){return $result;}
			 
			  $lasdate=$lasdate->format('Y');
			if($firstdate < 1950){$firstdate=$lasdate-5;}
			 if(ctype_digit($firstdate) && ctype_digit($lasdate) && ($lasdate >= $firstdate)){
				$limit=50;
				$exec=true;
			 
				while ($exec){
						
					$tparam=array('year'=>$firstdate);
					$dateperiod= $periodutil->startEndDatePeriod($tparam);
					$param['_date1']=$dateperiod->date1;
					$param['_date2']=$dateperiod->date2;
					$param['_year']=$firstdate;
			  
					$result=$this->countByYearSearch($param,$result);
					$firstdate++;
					
					if($firstdate > $lasdate){$exec=false;break;}
				}
				
			 }
			   
			  
		  }else if($period=='currentyear'){
			  $now=new \DateTime();
			  $currentyear = $now->format('Y');
			  $tparam=array('year'=>$currentyear);
			  $dateperiod= $periodutil->startEndDatePeriod($tparam);
			  $param['_date1']=$dateperiod->date1;
			  $param['_date2']=$dateperiod->date2;
			  $param['_year']=$currentyear;
			  
			  $result=$this->countByYearSearch($param,$result);
		  }
		 return $result;
	 }
	 
	function  countByYearSearch($param,$dresult){return null;}
	function  getMinMaxYear($param){return null;}
}
