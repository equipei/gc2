<?php

namespace Badiu\System\CoreBundle\Model\Lib\Report;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class BadiuPagination {
     /*count of record is in database
    * @var integer
    */
   private $countDbrows;
    
   /*limit of record to select on database
    * @var integer
    */
   private $limitDbrows;
   
    /*count number of paging index
    * @var integer
    */
   private $countPaging;
   
  /*limit of paging view on the screen
    * @var integer
    */
   private $limitPagingView;
   
    /* link to start of $pageIndex. Make loop of link till $pagingViewEnd
    * @var integer
    */
   private $pagingViewStart;
   
   /* link to end of $pageIndex.  Make loop of link start in $pagingViewStar and in $pagingViewEnd
    * @var integer
    */
   private $pagingViewEnd;
   
    /* array of pages. The array is loop star with $pagingViewStart and with $pagingViewEnd
    * @var array
    */
   private $pages;
   /*page user is navegation
    * @var integer
    */
   private $pageIndex;

    /*paging database start
    * @var integer
    */
   private $offset;
   
   
  
   /* text with pagina navegation
    * @var integer
    */
   private $navegationInfo;
   
       /**
     * @var Container
     */
    private $container;
    
    /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;

	private $systemdata;
    function __construct(Container $container) {
         $this->container=$container;
      }
   public function start($countDbrows=0,$pageIndex=1) {
        $this->countDbrows=$countDbrows;
        $this->pageIndex=$pageIndex;
        $this->init();
        $this->process($countDbrows,$pageIndex);
        
    }
    
       /*
     * process pagination  
     */
    
     public  function process($countDbrows=0,$pageIndex=1) {
         $this->countDbrows=$countDbrows;
         $this->pageIndex=$pageIndex;
         if($this->countDbrows==0){
             $this->init();
             return null;
         }
         
         $this->processPaging();
         $this->updatePage();
         $this->processPagingView();
    }
   
    /*
     * int default var 
     */
    
     public  function init() {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
         $cofigcustom = $badiuSession->get()->getConfig();
		 $paginationlimitdbrows=null;
		 $paginationlimitpageview=null;
        if(!empty($this->getSystemdata())){
			$paginationlimitdbrows=$this->getSystemdata()->getManagecache()->getCache('badiu.system.core.param.config.report.paginationlimitdbrows');
			$paginationlimitpageview=$this->getSystemdata()->getManagecache()->getCache('badiu.system.core.param.config.report.paginationlimitpageview');
        }
		else{
		  $paginationlimitdbrows=$cofigcustom->getValue('badiu.system.core.param.config.report.paginationlimitdbrows');
          $paginationlimitpageview=$cofigcustom->getValue('badiu.system.core.param.config.report.paginationlimitpageview');
       }
        
         
         $this->limitDbrows=10;
         $this->limitPagingView=10;
         
         if(!empty($paginationlimitdbrows)){$this->limitDbrows=$paginationlimitdbrows;}
         if(!empty($paginationlimitpageview)){$this->limitPagingView=$paginationlimitpageview;}
         
         $this->pagingViewStart=1;
         $this->pagingViewEnd=$this->limitPagingView;
         $this->offset=1;
        $this->pagingEnd=$this->limitDbrows;
    }
   
   /*
     * count number of pagination data  
     */
    
     public  function processPaging() {
        $this->countPaging=$this->countDbrows/$this->limitDbrows;
        if($this->countPaging != round($this->countPaging)){
              $this->countPaging=floor($this->countPaging)+1;
         }
   }
   public  function updatePage() {
       //if $pageIndex is is greater than $limitDbrows
       if($this->pageIndex>$this->countPaging){
           $this->pageIndex=$this->countPaging;
           $countPaging=$this->countDbrows/$this->limitDbrows;
          if($countPaging != round($countPaging)){
              $this->pageIndex=$this->pageIndex-1;
          }
       }
       
       if($this->pageIndex==1){
            $this->offset=0;
      }else if($this->pageIndex>1){
           $index=$this->pageIndex-1;
           $this->offset=($index*$this->limitDbrows)-1;
          
       }
        else{
            $this->offset=0;
       }
   }
   
   public  function processPagingView() {
       //countPaging is not upper than countPaging limitPagingView
       if($this->countPaging <= $this->limitPagingView){
           $this->pagingViewStart=1;
           $this->pagingViewEnd=$this->countPaging;
       }else{
        //countPaging is upper than countPaging limitPagingView
           $middleLimitPagingView=$this->limitPagingView/2;
           if($this->pageIndex>$middleLimitPagingView){
               $this->pagingViewStart=$this->pageIndex-$middleLimitPagingView;
                $this->pagingViewEnd=$this->pageIndex+$middleLimitPagingView;
                
                    if($this->pagingViewEnd>$this->countPaging){
                        $this->pagingViewEnd=$this->countPaging;
                    }
             }
           
       }
    
       //update array $pages
       $pages=array();
       for($i=$this->pagingViewStart;$i<=$this->pagingViewEnd;$i++){
          array_push($pages,$i);
       }
       $this->pages=$pages;
   }
   
    public function getInfo() {
          $info="";
          $x=0;
          $y=0;
          $x=0;
          if($this->countDbrows==0){
              $info=$this->countDbrows;
              return $info;
          }
          
          if($this->pageIndex==0){
              $x=1;
              $y=$this->limitDbrows;
           }else{
              $indexX=$this->pageIndex;
              $indexY=$this->pageIndex+1;
              $x=$indexX*$this->limitDbrows;
              $x++;
              $y=$indexY*$this->limitDbrows;

           }
           if($x > $this->countDbrows){$x=$this->countDbrows;}
           if($y > $this->countDbrows){$y=$this->countDbrows;}
    
          $info= "$x - $y de ".$this->countDbrows;
         
          return $info;
          //return $this->countDbrows;
       }  
   public function getCountDbrows() {
       return $this->countDbrows;
   }

   public function getLimitDbrows() {
       return $this->limitDbrows;
   }

   public function getCountPaging() {
       return $this->countPaging;
   }

   public function getLimitPagingView() {
       return $this->limitPagingView;
   }

   public function getPageIndex() {
       return $this->pageIndex;
   }

   public function getOffset() {
       return $this->offset;
   }

   public function getNavegationInfo() {
       return $this->navegationInfo;
   }

   public function setCountDbrows($countDbrows) {
       $this->countDbrows = $countDbrows;
   }

   public function setLimitDbrows($limitDbrows) {
       $this->limitDbrows = $limitDbrows;
   }

   public function setCountPaging($countPaging) {
       $this->countPaging = $countPaging;
   }

   public function setLimitPagingView($limitPagingView) {
       $this->limitPagingView = $limitPagingView;
   }

   public function setPageIndex($pageIndex) {
       $this->pageIndex = $pageIndex;
   }

   public function setOffset($offset) {
       $this->offset = $offset;
   }

   public function setNavegationInfo($navegationInfo) {
       $this->navegationInfo = $navegationInfo;
   }

   public function getPagingViewStart() {
       return $this->pagingViewStart;
   }

   public function getPagingViewEnd() {
       return $this->pagingViewEnd;
   }

   public function setPagingViewStart($pagingViewStart) {
       $this->pagingViewStart = $pagingViewStart;
   }

   public function setPagingViewEnd($pagingViewEnd) {
       $this->pagingViewEnd = $pagingViewEnd;
   }

   public function getPages() {
       return $this->pages;
   }

   public function setPages($pages) {
       $this->pages = $pages;
   }


   function getContainer() {
       return $this->container;
   }

   function setContainer(Container $container) {
       $this->container = $container;
   }



   
   public function getSessionhashkey() {
    return $this->sessionhashkey;
}

public function setSessionhashkey($sessionhashkey) {
   $this->sessionhashkey = $sessionhashkey;
}

	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
	
}
