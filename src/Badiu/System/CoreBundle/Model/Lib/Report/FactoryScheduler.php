<?php

namespace Badiu\System\CoreBundle\Model\Lib\Report;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

use Badiu\System\CoreBundle\Model\Lib\Http\Url; //review
use Badiu\System\CoreBundle\Model\Lib\Http\HttpQueryString; //review
use Badiu\System\CoreBundle\Model\Report\BadiuPagination; //review
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
class FactoryScheduler extends TaskGeneralExec {

     private $key; 
    private $bkey;
    private $bkeydynamicroute;
    private $layout='indexCrud'; 
    private $kminherit;
    private $param;
    private $page;
    private $usersender=null;
   private $action='send.report'; //send.menssage.touserinreport | send.report
   private $dynamicroute=false;
   private $maxrowperprocess=150000;
   private $maxrowperconnection=2500;
   private $maxrowtomailbody=120;
   
   private $dconfig;

    function __construct(Container $container) {
        parent::__construct($container);
         $this->key=null;
        $this->page=$this->getContainer()->get('badiu.system.core.page');
        $this->page->setIsexternalclient(TRUE);
        $this->page->setIsschedule(TRUE);
        }

    
     public function init() {
            $reportfilter=$this->getTaskdto()->getReportfilter();
            $reportfilter=$this->getJson()->decode($reportfilter,true);
            $this->action=$this->getTaskdto()->getReportaction();

           
            //dconfig
            $this->dconfig=$this->getTaskdto()->getDconfig();
            $this->dconfig=$this->getJson()->decode($this->dconfig,true);

            $this->dynamicroute=false;
            $this->key=$this->getUtildata()->getVaueOfArray($reportfilter,'config.router',true);
            $dkey=$this->getUtildata()->getVaueOfArray($reportfilter,'filter._dkey',true);
           
            if(!empty($dkey)){
                $this->key=$dkey;
                $this->dynamicroute=true;
            }
            $this->param=$this->getUtildata()->getVaueOfArray($reportfilter,'filter');
			$this->param['entity']=$this->getTaskdto()->getEntity(); //for process doctrine
            $this->initUsersendergetKey($reportfilter); 
         // print_r($this->param);exit;
           $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
                         
            if($sysoperation->isView($this->key)){$this->layout='viewDetail';}
            else if($sysoperation->isIndex($this->key)){$this->layout='indexCrud';}
            else if($sysoperation->isDashboard($this->key)){$this->layout='dashboard';}
            else if($sysoperation->isFrontpage($this->key)){$this->layout='frontpage';}

            $this->bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($this->key);
            $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
            $keymanger->setBaseKey($this->bkey);
            if($this->dynamicroute){
                $this->bkeydynamicroute='badiu.system.core.report.dynamic.index';
                $parentid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter.parentid',true);
                if(!empty($parentid)){$this->bkeydynamicroute='badiu.system.core.report.dynamicp.index';}
                $this->bkeydynamicroute=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($this->bkeydynamicroute);
                $keymanger->setBaseKeydyncoriginal($this->bkeydynamicroute);
            }
            $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $cbundle->setSessionhashkey($this->getSessionhashkey());
			$cbundle->setSystemdata($this->getSystemdata());
            $cbundle->setKeymanger($keymanger);
            $cbundle->initKeymangerExtend($keymanger);
            $this->kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
            $this->kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
            
			$this->initFreportDynamic($this->param);
            $cpage=$cbundle->getPageConfig($keymanger);
            $this->page->setConfig($cpage);
            $croute=$cbundle->getCrudRoute($keymanger);
            $this->page->setCrudroute($croute);
            $this->page->setKey($this->key);
           // $formGroupList=$cbundle->getGroupOfField($keymanger->formFilterFieldsEnable(),$cbundle->getKeymangerExtend()->formFilterFieldsEnable());
           // $formFieldList=$cbundle->getFieldsInGroup($keymanger->formFilterFieldsEnable(),$cbundle->getKeymangerExtend()->formFilterFieldsEnable());
           
           // $this->page->addData('badiu_form1_group_list',$formGroupList);
           // $this->page->addData('badiu_form1_field_list',$formFieldList);
            
             $this->page->addData('badiu_form1_group_list',null);
            $this->page->addData('badiu_form1_field_list',null);
           
           // $dasheboardsqlsinlgleconfig=$cbundle->getValueQueryString($keymanger->dashboardDbSqlSingleConfig(),$cbundle->getKeymangerExtend()->dashboardDbSqlSingleConfig());
          //  $dasheboardsqlconfig=$cbundle->getValueQueryString($keymanger->dashboardDbSqlConfig(),$cbundle->getKeymangerExtend()->dashboardDbSqlConfig());

        //    $this->page->addData('badiu_list_data_row.config',$dasheboardsqlsinlgleconfig);
         //   $this->page->addData('badiu_list_data_rows.config',$dasheboardsqlconfig);
          
            $cform=$cbundle->getFormConfig('filter',$keymanger);
            $this->page->setFormconfig($cform);
           
          //  $fc=$this->getContainer()->get('badiu.system.core.lib.form.factoryconfig');
           //  $fc->setIsexternalclient(true);
        
            
            //refresh key
            $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
            $keymanger->setBaseKey($this->bkey);
           
            if($this->dynamicroute){
                $keymanger->setBaseKeydyncoriginal($this->bkeydynamicroute);
            } 
            $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $cbundle->setSessionhashkey($this->getSessionhashkey());
			$cbundle->setSystemdata($this->getSystemdata());
            $cbundle->setKeymanger($keymanger);
            $cbundle->initKeymangerExtend($keymanger);
            $this->kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
            $this->kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
           
     }
    public function exec() {
       
               $this->init();
               $result=null;
                if($this->action=='send.report'){
                    $result=$this->sendReport(); 
                    return $result;
                }else  if($this->action=='send.menssage.touserinreport'){
                     //$result=$this->sendMessageToEachUserOfReport_temp(); 
                     $result=$this->sendMessageToEachUserOfReport(); 
                     return $result;
                }
                else  if($this->action=='export.todatabase'){
                    $result=$this->exportTodatabase(); 
                    return $result;
               }else  if($this->action=='moodle.mreport.backuprestorecourse'){
                $result=$this->moodleBackupRestoreCourse(); 
                return $result;
            }

            
                return  $result;
    }
    
    private function getContent($paginindex=0,$maxrow=2500,$cparam=array()) {
        $this->param['_page']=$paginindex; 
  
        $report= $this->getContainer()->get($this->getKminherit()->report());
        $report->setSessionhashkey($this->getSessionhashkey());
		$report->setSystemdata($this->getSystemdata());
        $report->setPaginglimit($maxrow); 
        $report->setIsexternalclient(true);
        $report->setIsschedule(true);
        $report->getKeymanger()->setBaseKey($this->bkey);
        if($this->dynamicroute){
            $report->getKeymanger()->setBaseKeydyncoriginal($this->bkeydynamicroute);
       } 
       
        $cform=$this->page->getFormconfig();
       // $report->setDefaultdatafilterconfig($cform->getDefaultdata());
        $report->setDefaultdatafilterconfigonopenform($cform->getDefaultdataonpenform());
        $report->setFconfig($cform);
         $type=null;
        
         if($this->layout=='indexCrud'){$type='dbsearch.fields.table.view'; }
         else if($this->layout=='viewDetail'){$type='dbgetrow.fields.table.view'; }
        $report->extractData($this->param,$this->layout,$type,$cparam);
        
        $format=$this->getUtildata()->getVaueOfArray($this->param,'_format');
        $data=array();
        if($this->layout=='indexCrud'){
             $report->makeTable($this->layout);
            
             $data['badiu_list_data_row']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row');
             $data['badiu_list_data_rows']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows');
             $data['badiu_list_data_countrows']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'countrows');
             $data['badiu_table1']=$report->getTable();
             $dataout=$this->getUtildata()->getVaueOfArray($cparam,'dataout');
             if($dataout=='withouttable'){return $report->getTable()->getRow()->getList();}
            
             $addcolumnhead=$this->getUtildata()->getVaueOfArray($cparam,'addcolumnhead');
             if($addcolumnhead){
                 $report->makeTableColumnsTitle();
                $data['badiu_table1_columnshead']=$report->getTable()->getColumn()->getList();
             }
        }else if($this->layout=='dashboard'){
            $data['badiu_list_data_row']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row');
            $data['badiu_list_data_rows']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows');
          }else if($this->layout=='viewDetail'){
            $data['badiu_list_data_row']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row');
            $data['badiu_list_data_rows']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows');
          }
         
        return $data;
        
    }

    private function sendReport() {

        //sendReport with SystemRecipient (not implemented pagination)
        $mconfig= $this->getTaskdto()->getReportmessage();
        $mconfig=$this->getJson()->decode($mconfig,true);
        $sysrecipient=$this->getUtildata()->getVaueOfArray($mconfig,'recipient.sysuser',true);
        if(!empty($sysrecipient)){return $this->sendReportSystemRecipient();}
 
        //sendReport without SystemRecipient

        $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
        if(empty($smtpservice)){$smtpservice='badiunetconfig';}

        $maxrowperconnection=$this->maxrowperconnection;
        $maxrowtoexec=$this->maxrowperprocess;
        $subprocess=0;
        $pagingindex=0;
        $countprocessed=0;
        $countprocessedmailsend=0;
        $data=$this->getContent($pagingindex,$maxrowperconnection,array('partialdata'=>'countrows'));
        $countrecord=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_countrows');
       
        if($countrecord > $maxrowperconnection){$subprocess=$countrecord/$maxrowperconnection;}
        if($subprocess != round($subprocess)){
            $subprocess=floor($subprocess)+1;
        }
       if($subprocess == 0){$subprocess=1;}

       $filemanagereport=$this->getContainer()->get('badiu.system.file.lib.managereport');
       $filemanagereport->setSessionhashkey($this->getSessionhashkey());
       $filemanagereport->setSystemdata($this->getSystemdata());
        
        if($subprocess > 0){
            $paramfileh=array('hashnameappend'=>$this->getTaskdto()->getId());
            $filemanagereport->start($paramfileh);
            $columnhead=array();
            for ($i = 0; $i < $subprocess; $i++) {
                $paramfilter=array('partialdata'=>'getrows');
                if($i==0){ $paramfilter=array('partialdata'=>'getrows','addcolumnhead'=>1);}
                $idata=$this->getContent($i,$maxrowperconnection,$paramfilter);
                if($i==0){$columnhead= $this->getUtildata()->getVaueOfArray($idata,'badiu_table1_columnshead');}
                $itable=$this->getUtildata()->getVaueOfArray($idata,'badiu_table1');
                if($i==0){$itable->getColumn()->setList($columnhead);}
               // $idata=$itable->getRow()->getList();

                $icount=0;
                 //add head
                 if($i==0){
                  $filemanagereport->head($columnhead);
                } 

                if(!empty($itable) &&  is_array($itable->getRow()->getList())){
                    $icount=sizeof($itable->getRow()->getList());
                    $countprocessed=$countprocessed+$icount;
                    $filemanagereport->addtable($itable);
                    $idata=null;
                    $itable=null;

                }
                if($icount==0 || $countprocessed>=$maxrowtoexec){break;}
            }
        }
        $filemanagereport->end();
        $mtparam=array('maxrows'=>$this->maxrowtomailbody,'countrowcurrentforceset'=>true,'countrows'=>$countrecord,'limitrowshow'=>$this->maxrowtomailbody);
        $ptable=$filemanagereport->makeTable($mtparam);
        $mailreportcontentbody=$this->getReportContentToBodyMail($ptable);
        
        $mparam=array();
        
        $mparam['reportcountrows']=$countrecord;
        $mparam['typemessage']='sendreport';

        if($countrecord > 0) {$mconfig['attach'][0]=$filemanagereport->attachToMail();}
        $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
        $customcontent.="<br />";
        $customcontent.=$mailreportcontentbody;
        $mconfig['message']['body']=$customcontent;
        $resultsendmail=$this->sendMail($mconfig,$mparam);

        
         //upate task with processed
         $result['resultinfo']['datashouldexec']=1;
         $result['resultinfo']['dataexec']=$resultsendmail;
         $result['resultinfo']['reportrowsize']=$countprocessed;
         $filemanagereport->remove();
     return $result; 
    
     }
     //review it and add new architeture process in file large data
     private function sendReportSystemRecipient() {
        $page=$this->page;
         $container=$this->getContainer();
         $router=$this->getContainer()->get("router");
          $contents=null;
         $app=$this->getContainer()->get('badiu.system.core.lib.util.app'); 
         $result=array();
         $result['resultinfo']['datashouldexec']=1;
         $maxrow=$this->maxrowperconnection;
         $paginindex=0;
       
         $data=$this->getContent($paginindex,$maxrow,array('fulldatawithutopaging'=>1));
         $page->addData('badiu_list_data_row', $this->getUtildata()->getVaueOfArray($data,'badiu_list_data_row'));
         $page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows'));
         $page->addData('badiu_table1',$this->getUtildata()->getVaueOfArray($data,'badiu_table1'));
 
         if($this->layout=='indexCrud'){
             $table=$this->getUtildata()->getVaueOfArray($data,'badiu_table1');
             $reportcountrows=count($table->getRow()->getList());
             
             $fileprocess=$page->getConfig()->getFileprocessindexscheduler();
             $file=$app->getFilePath($fileprocess,false);
            
             if(!file_exists($file)){return "file $fileprocess | $file not found"; }
           
             ob_start();
              include($file);
              $contents = ob_get_contents();
             ob_end_clean();
             
             $recipientparam=array();
             
             //add page data
            
             $mconfig= $this->getTaskdto()->getReportmessage();
             $mconfig=$this->getJson()->decode($mconfig,true);
            
 
             $recipientparam['page']=$page;
             $recipientparam['paramfilter']=$this->param;
             
              $recipientparam['reportmessage']=$mconfig;
              $recipientparam['entity']=$this->getTaskdto()->getEntity();
 
              $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
             if(empty($smtpservice)){$smtpservice='badiunetconfig';}
              
              $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
              $customcontent.="<br />";
              $customcontent.=$contents;
              $mconfig['message']['body']=$customcontent;

              //attach
              if($smtpservice=='badiunetconfig'){
                    $excelexport=$this->getContainer()->get('badiu.system.core.lib.report.excelexport');
                    $excelexport->setTable($table);
                    $attach=$excelexport->attachToMail();
                    $mconfig['attach'][0]=$attach; 
              }
              $sendonlyreportwithrecord=$this->getUtildata()->getVaueOfArray($mconfig,'sendonlyreportwithrecord');
              if($sendonlyreportwithrecord==null || $sendonlyreportwithrecord==''){$sendonlyreportwithrecord=1;}
              $sendmail=true;
              if($sendonlyreportwithrecord==1 && $reportcountrows==0){ $sendmail=false;}

              $mparam=array();
              $mparam['reportcountrows']=$reportcountrows;
              $mparam['typemessage']='sendreport';
              $execsendmail=$this->sendMail($mconfig,$mparam);
             
              $result['resultinfo']['dataexec']=$execsendmail;
              $result['resultinfo']['reportrowsize']=$reportcountrows; //review with pagination
 
              //send to system recipient
            
              $recipientparam['resultinfo']=$result;
              $recipientparam['sendmail']=$sendmail;
              $recipientparam['layout']='indexCrud';
              $recipientparam['formatedcontent']=$customcontent;
              $recipientparam['usersender']=$this->getUsersender();
              $recipientparam['smtpservice']=$smtpservice;
              //process system recipient
              $result= $this->execSystemRecipient($recipientparam);
              return $result;
              
         
           
         }
      
         return null;
     }
  /*
   private function sendReport() {
       $page=$this->page;
        $container=$this->getContainer();
        $router=$this->getContainer()->get("router");
         $contents=null;
        $app=$this->getContainer()->get('badiu.system.core.lib.util.app'); 
        $result=array();
        $result['resultinfo']['datashouldexec']=1;
        $maxrow=$this->maxrowperconnection;
        $maxrow=15000;
        $paginindex=0;
      
        $data=$this->getContent($paginindex,$maxrow,array('fulldatawithutopaging'=>1));
        $page->addData('badiu_list_data_row', $this->getUtildata()->getVaueOfArray($data,'badiu_list_data_row'));
        $page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows'));
        $page->addData('badiu_table1',$this->getUtildata()->getVaueOfArray($data,'badiu_table1'));

        if($this->layout=='indexCrud'){
            $table=$this->getUtildata()->getVaueOfArray($data,'badiu_table1');
            $reportcountrows=count($table->getRow()->getList());
            
            $fileprocess=$page->getConfig()->getFileprocessindexscheduler();
            $file=$app->getFilePath($fileprocess,false);
           
            if(!file_exists($file)){return "file $fileprocess | $file not found"; }
          
            ob_start();
             include($file);
             $contents = ob_get_contents();
            ob_end_clean();
            
            $recipientparam=array();
            
            //add page data
           
            

            $recipientparam['page']=$page;
            $recipientparam['paramfilter']=$this->param;
            
             $mconfig= $this->getTaskdto()->getReportmessage();
             $mconfig=$this->getJson()->decode($mconfig,true);
            
             $recipientparam['reportmessage']=$mconfig;
             $recipientparam['entity']=$this->getTaskdto()->getEntity();

             $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
             if(empty($smtpservice)){$smtpservice='badiunetconfig';}
              
            
             $sendonlyreportwithrecord=$this->getUtildata()->getVaueOfArray($mconfig,'sendonlyreportwithrecord');
             if($sendonlyreportwithrecord==null || $sendonlyreportwithrecord==''){$sendonlyreportwithrecord=1;}
             $sendmail=true;
             if($sendonlyreportwithrecord==1 && $reportcountrows==0){ $sendmail=false;}
             
             $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
             $customcontent.="<br />";
             $customcontent.=$contents;
             $mconfig['message']['body']=$customcontent;
             
            
             $execsendmail=false;
             $emailsto=$mconfig['recipient']['to'];
             if($smtpservice=='badiunetconfig'){
                $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
                $smtp->setUsersender($this->getUsersender());
                $entity=$this->getTaskdto()->getEntity();
                $smtp->setEntity($entity); 
               
                if($sendmail && !empty($emailsto)){
                    $excelexport=$this->getContainer()->get('badiu.system.core.lib.report.excelexport');
                    $excelexport->setTable($table);
                    $attach=$excelexport->attachToMail();
                    $mconfig['attach'][0]=$attach; 
                    $execsendmail=$smtp->sendMail($mconfig);
                }
                else{$execsendmail=-1;}
             }else if ($smtpservice=='moodleconfig'){
                $reportfilter=$this->getTaskdto()->getReportfilter();
                $reportfilter=$this->getJson()->decode($reportfilter,true);
                $parentid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter.parentid',true);
                $smtpserviceparam=array(); 
                $smtpserviceparam['_serviceid']=$parentid;
                $smtpserviceparam['mail']['subject']=$this->getUtildata()->getVaueOfArray($mconfig,'message.subject',true);
                $smtpserviceparam['mail']['message']=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
                $smtpserviceparam['mail']['to']=$emailsto;
                if($sendmail && !empty($emailsto)){
                    $mdlsmtpservice=$this->getContainer()->get('badiu.moodle.mreport.lib.smtpservice');
                    $mdlsmtpservice->setSessionhashkey($this->getSessionhashkey());
                    $smtpserviceparam=$mdlsmtpservice->castListEmail($smtpserviceparam);
                    $resultmdlsms=$mdlsmtpservice->exec($smtpserviceparam);
                    $emailsendto=$mdlsmtpservice->getFirstEmail($emailsto);
                    $execsendmail=$mdlsmtpservice->checkResponse($resultmdlsms,$emailsendto);
                 }
             }
             
             
             $result['resultinfo']['dataexec']=$execsendmail;
             $result['resultinfo']['reportrowsize']=$reportcountrows; //review with pagination

             //send to system recipient
           
             $recipientparam['resultinfo']=$result;
             $recipientparam['sendmail']=$sendmail;
             $recipientparam['layout']='indexCrud';
             $recipientparam['formatedcontent']=$customcontent;
             $recipientparam['usersender']=$this->getUsersender();
             $recipientparam['smtpservice']=$smtpservice;
             //process system recipient
             $result= $this->execSystemRecipient($recipientparam);
             return $result;
             
        
          
        }
     
        return null;
    }
    */
   
       
     /*function sendMessageToEachUserOfReport_temp(){
         
             $mconfig= $this->getTaskdto()->getReportmessage();
             $mconfig=$this->getJson()->decode($mconfig,true);
             $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
             $customcsubject=$this->getUtildata()->getVaueOfArray($mconfig,'message.subject',true);

             $columnemail=$this->page->getConfig()->getSchedulermessageemailcolumn();
             $columnuser=$this->page->getConfig()->getSchedulermessageusercolumn();
             
             $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
             if(empty($smtpservice)){$smtpservice='badiunetconfig';}
            
             $mdlsmtpservice=$this->getContainer()->get('badiu.moodle.mreport.lib.smtpservice');
             $mdlsmtpservice->setSessionhashkey($this->getSessionhashkey());
             $maxrow=$this->maxrowperconnection;
            $maxrow=15000;
            $paginindex=0;
            $data=$this->getContent($paginindex,$maxrow);

             $table=$this->getUtildata()->getVaueOfArray($data,'badiu_table1');
             $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
             $smtp->setUsersender($this->getUsersender());
             $entity=$this->getTaskdto()->getEntity();
             $smtp->setEntity($entity);
             
            
             $contshouldprocess=0;
             $contprocess=0;
             $listusersend=array();
             $result=array();
             $result['resultinfo']['datashouldexec']=count($table->getRow()->getList());
             
        foreach ($table->getRow()->getList() as $row) {
                   $contshouldprocess++;
                   $email=$this->getUtildata()->getVaueOfArray($row,$columnemail);
                   if(!empty($email)){$email=strip_tags($email);}
                   
                   $user=$this->getUtildata()->getVaueOfArray($row,$columnuser);
                   if(!empty($user)){$user=strip_tags($user);}
                
                   $reportfilter=$this->getTaskdto()->getReportfilter();
                    $reportfilter=$this->getJson()->decode($reportfilter,true);
                    $parentid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter.parentid',true);

                    
                   $content=$this->processExpression($customcontent,$row);
                   $subject=$this->processExpression($customcsubject,$row);
                  
                   if($smtpservice=='badiunetconfig'){
                        $mconfig['message']['subject']= $subject;
                        $mconfig['message']['body']= $content;
                        $mconfig['recipient']['to']=$email;
                        if(!empty($email)){
                            $send=$smtp->sendMail($mconfig);
                            if($send){ 
                                $contprocess++;
                                $usersp=array('email'=>$email,'user'=>$user);
                                $usersplog=array('email'=>$email,'user'=>$user,'subject'=>$subject,'content'=>$content);
                                $this->saveReportOfUserNotified($usersplog);
                                array_push($listusersend,$usersp);
                            }
                        }
                   }else if ($smtpservice=='moodleconfig'){
                    
                        $smtpserviceparam=array(); 
                        $smtpserviceparam['_serviceid']=$parentid;
                        $smtpserviceparam['mail']['subject']=$subject;
                        $smtpserviceparam['mail']['message']= $content;
                        $smtpserviceparam['mail']['to']=array('email'=>$email,'name'=>$user);
                        if(!empty($email)){
                            $resultmdlsms=$mdlsmtpservice->exec($smtpserviceparam);
                            $send=$mdlsmtpservice->checkResponse($resultmdlsms,$email);
                            if($send){
                                $contprocess++;
                                $usersp=array('email'=>$email,'user'=>$user);
                                $usersplog=array('email'=>$email,'user'=>$user,'subject'=>$subject,'content'=>$content);
                                $this->saveReportOfUserNotified($usersplog);
                                array_push($listusersend,$usersp);
                            }
                     }
                   }
                  
                   
           
             }
             $result['resultinfo']['dataexec']=$contprocess;
             $result['smtpservice']=$smtpservice;
            // $this->saveReportOfUsersNotified($listusersend);
             $this->sendReportOfUserNotified_temp($listusersend);
          
         return $result; 
      } */

      
      function sendMessageToEachUserOfReport(){
         
        $mconfig= $this->getTaskdto()->getReportmessage();
        $mconfig=$this->getJson()->decode($mconfig,true);
        $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
        if(empty($smtpservice)){$smtpservice='badiunetconfig';}

        $maxrowperconnection=$this->maxrowperconnection;
        $maxrowtoexec=$this->maxrowperprocess;
        $subprocess=0;
        $pagingindex=0;
        $countprocessed=0;
        $countprocessedmailsend=0;
        $data=$this->getContent($pagingindex,$maxrowperconnection,array('partialdata'=>'countrows'));
        $countrecord=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_countrows');
       
        if($countrecord > $maxrowperconnection){$subprocess=$countrecord/$maxrowperconnection;}
        if($subprocess != round($subprocess)){
            $subprocess=floor($subprocess)+1;
        }
       if($subprocess == 0){$subprocess=1;}
        if($subprocess > 0){
            
            for ($i = 0; $i < $subprocess; $i++) {
                $idata=$this->getContent($i,$maxrowperconnection,array('partialdata'=>'getrows'));
                $itable=$this->getUtildata()->getVaueOfArray($idata,'badiu_table1');
                $idata=$itable->getRow()->getList();
                $icount=0;
                if(is_array($idata)){
                    $icount=sizeof($idata);
                    $countprocessed=$countprocessed+$icount;
                    $countprocessedmailsend+=$this->sendMessageToEachUserOfReportPartial($idata,$countrecord);
                    $itable=null;
                    $idata=null;
                }
                if($icount==0 || $countprocessed>=$maxrowtoexec){break;}
            }
        }

        //get list of notifyed
        $this->sendReportOfUserNotified($countrecord);
        //upate task with processed
        $result['resultinfo']['datashouldexec']=$countrecord;
        $result['resultinfo']['dataexec']=$countprocessedmailsend;
        $result['smtpservice']=$smtpservice;
       // $this->saveReportOfUsersNotified($listusersend);
       
     
    return $result; 
 }
 /* delete
    function getListUserForNotifyInReport($data){
        $table=$this->getUtildata()->getVaueOfArray($data,'badiu_table1');
        $newlist=array();
        if(empty($table)){return $newlist;}
        $columnemail=$this->page->getConfig()->getSchedulermessageemailcolumn();
        $columnuser=$this->page->getConfig()->getSchedulermessageusercolumn();

        foreach ($table->getRow()->getList() as $row) {
            $email=$this->getUtildata()->getVaueOfArray($row,$columnemail);
            if(!empty($email)){$email=strip_tags($email);}
            
            $user=$this->getUtildata()->getVaueOfArray($row,$columnuser);
            if(!empty($user)){$user=strip_tags($user);}

            $iuser=array('email'=>$email,'user'=>$user);
            array_push($newlist,$iuser);
        }
        return $newlist;
    }*/
    function sendMessageToEachUserOfReportPartial($list,$totalrows){

        $contshouldprocess=0;
        $contprocess=0;
        
        $result=array();
        $result['resultinfo']['datashouldexec']=$totalrows;
        
        $mconfig= $this->getTaskdto()->getReportmessage();
        $mconfig=$this->getJson()->decode($mconfig,true);

        $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
        $customcsubject=$this->getUtildata()->getVaueOfArray($mconfig,'message.subject',true);

        $columnemail=$this->page->getConfig()->getSchedulermessageemailcolumn();
        $columnuser=$this->page->getConfig()->getSchedulermessageusercolumn();

        $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
        if(empty($smtpservice)){$smtpservice='badiunetconfig';}

       
        $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
		$smtp->setSessionhashkey($this->getSessionhashkey());
		$smtp->setSystemdata($this->getSystemdata());
        $smtp->setUsersender($this->getUsersender());
        $entity=$this->getTaskdto()->getEntity();
        $smtp->setEntity($entity);
		$smtp->init();
		
       // $mdlsmtpservice=$this->getContainer()->get('badiu.moodle.mreport.lib.smtpservice');
       // $mdlsmtpservice->setSessionhashkey($this->getSessionhashkey());
        
        $deliverychanel=$this->getUtildata()->getVaueOfArray($mconfig,'delivery.chanel',true);
		if(empty($deliverychanel) || !is_array($deliverychanel) || sizeof($deliverychanel)==0){$deliverychanel=array("email");}
		
        foreach ($list as $row) {
            $contshouldprocess++;
            $email=$this->getUtildata()->getVaueOfArray($row,$columnemail);
            if(!empty($email)){$email=strip_tags($email);}
            
            $user=$this->getUtildata()->getVaueOfArray($row,$columnuser);
            if(!empty($user)){$user=strip_tags($user);}
         
            $reportfilter=$this->getTaskdto()->getReportfilter();
             $reportfilter=$this->getJson()->decode($reportfilter,true);
             $parentid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter.parentid',true);
			 $sserviceid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter._serviceid',true);
			 $mconfig['_serviceid']=$sserviceid;
             
            $content=$this->processExpression($customcontent,$row);
            $subject=$this->processExpression($customcsubject,$row);
           
            $mconfig['message']['subject']= $subject;
            $mconfig['message']['body']= $content;
            $mconfig['recipient']['to']=$email;
			
			$channelinfo=array();
			$channelshouldexec=sizeof($deliverychanel);
			$channelexec=0;
			$channeldelivered="";
			$wsname=array();
			foreach ($deliverychanel as $dchanel) {
				if($dchanel=="email"){
					$mparam=array();
					$channelinfo['email']=0;
					$r=$this->sendMail($mconfig,$mparam,$smtp);
					if($r){$channelexec++;$channelinfo['email']=1;$channeldelivered.="email_sucess ";}
					else {$channeldelivered.="email_failure ";}
				}else if($dchanel=="moodlemessage"){
					$channelinfo['moodlemessage']=0;
					$r=$this->sendMoodleMessage($mconfig);
					if($r){$channelexec++;$channelinfo['moodlemessage']=1;$channeldelivered.="moodlemessage_sucess ";}
					else {$channeldelivered.="moodlemessage_failure ";}
				}else if($dchanel=="moodlenotification"){
					$channelinfo['moodlenotification']=0;
					$r=$this->sendMoodleNotification($mconfig);
					if($r){$channelexec++;$channelinfo['moodlenotification']=1;$channeldelivered.="moodlenotification_sucess ";}
					else {$channeldelivered.="moodlenotification_failure ";}
				}else if(ctype_digit($dchanel)){
					$wsshortname=$this->getUtildata()->getVaueOfArray($wsname,$dchanel);
					if(empty($wsshortname)){
						$wsshortname=$this->getContainer()->get('badiu.system.module.clientwsschedulerdelivery.data')->getGlobalColumnValue("shortname",array('id'=>$dchanel));
						$wsname[$dchanel]=$wsshortname;
					}
					$channelinfo[$wsshortname]=0;
					$r=$this->sendServiceMessage($mconfig,$dchanel);
					if($r){$channelexec++;$channelinfo[$wsshortname]=1;$channeldelivered.=$wsshortname."_sucess ";}
					else {$channeldelivered.=$wsshortname."_failure ";}
				}
			}
            
            if($channelexec){$contprocess++;}
		
            $channelinfo=$this->getJson()->encode($channelinfo);
            $usersplog=array('email'=>$email,'user'=>$user,'subject'=>$subject,'content'=>$content,'channelexec'=>$channelexec,'channelshouldexec'=>$channelshouldexec,'channelinfo'=>$channelinfo,'channeldelivered'=>$channeldelivered);
            $this->saveReportOfUserNotified($usersplog);
             
    
      }
      return $contprocess;
    }
      function saveReportOfUsersNotified($listuser){
          $lmdata=$this->getContainer()->get('badiu.system.scheduler.tasklogmessage.data');
          foreach ($listuser as $row) {
               $email=$this->getUtildata()->getVaueOfArray($row,'email');
               $user=$this->getUtildata()->getVaueOfArray($row,'user');
               $param=array();
               $param['entity']=$this->getTaskdto()->getEntity();
               //$param['name']=null; //db shoud remove
               $param['userrecipient']=$user;
               $param['usercontact']=$email;
               $param['dtype']='mailsendtouserreporterfilter';
               $param['tasklogid']= $this->getTasklogid();
               $param['timecreated']=new \DateTime();
               $lmdata->insertNativeSql($param,false);
               
          }
      }

      function saveReportOfUserNotified($row){
            $lmdata=$this->getContainer()->get('badiu.system.scheduler.tasklogmessage.data');
            $email=$this->getUtildata()->getVaueOfArray($row,'email');
             $user=$this->getUtildata()->getVaueOfArray($row,'user');
             $subject=$this->getUtildata()->getVaueOfArray($row,'subject');
             $content=$this->getUtildata()->getVaueOfArray($row,'content');
			 $channelshouldexec=$this->getUtildata()->getVaueOfArray($row,'channelshouldexec');
			 $channelexec=$this->getUtildata()->getVaueOfArray($row,'channelexec');
			 $channelinfo=$this->getUtildata()->getVaueOfArray($row,'channelinfo');
			 $channeldelivered=$this->getUtildata()->getVaueOfArray($row,'channeldelivered');
             $param=array();
             $param['entity']=$this->getTaskdto()->getEntity();
             //$param['name']=null; //db shoud remove
             $param['userrecipient']=$user;
             $param['usercontact']=$email;
             $param['msgsubject']=$subject;
             $param['msgcontent']=$content;
			 $param['channelshouldexec']=$channelshouldexec;
			 $param['channelexec']=$channelexec;
			 $param['channelinfo']=$channelinfo;
			 $param['channeldelivered']=$channeldelivered;
             $param['dtype']='mailsendtouserreporterfilter';
             $param['tasklogid']= $this->getTasklogid();
             $param['timecreated']=new \DateTime();
            $result=$lmdata->insertNativeSql($param,false);
            
            return  $result;
       
    }

    function castArrayToTable($param){
        $head= $this->getUtildata()->getVaueOfArray($param,'head');
        $rows= $this->getUtildata()->getVaueOfArray($param,'rows');
        $countrowcurrentforceset= $this->getUtildata()->getVaueOfArray($param,'countrowcurrentforceset');
        $countrows= $this->getUtildata()->getVaueOfArray($param,'countrows');
        $limitrowshow=$this->getUtildata()->getVaueOfArray($param,'limitrowshow');
        if(empty($limitrowshow)){$limitrowshow=$this->getMaxrowtomailbody();}
        $table=$this->getContainer()->get('badiu.system.core.lib.report.table');
        $table->getColumn()->setList($head);
        $table->getRow()->setList($rows);
        if($countrowcurrentforceset && is_array($rows) && (sizeof($rows) < $countrows) ){
            $table->setCountrowcurrentforceset(true);
            $table->setCountrowcurrent($countrows);
        }
        $table->setLimitrowshow($limitrowshow); //review and test
        return $table;
    }
    function getReportContentToBodyMail($table){
        $this->page->addData('badiu_table1',$table);
        $fileprocess=$this->page->getConfig()->getFileprocessindexscheduler();
       
        $container=$this->getContainer();
        $router=$this->getContainer()->get("router");
        $page=$this->page;
        $app=$this->getContainer()->get('badiu.system.core.lib.util.app'); 
         $file=$app->getFilePath($fileprocess,false);
        $contents=null;
        ob_start();
           include($file);
            $contents = ob_get_contents();
        ob_end_clean();

        return  $contents;
   }
    /*  function sendReportOfUserNotified_temp($listuser){
          $reportcountrows=count($listuser);
          $mconfig= $this->getTaskdto()->getReportmessage();
          $mconfig=$this->getJson()->decode($mconfig,true);
          
          $sendonlyreportwithrecord=$this->getUtildata()->getVaueOfArray($mconfig,'sendonlyreportwithrecord');
          if($sendonlyreportwithrecord==null || $sendonlyreportwithrecord==''){$sendonlyreportwithrecord=1;}
          if($sendonlyreportwithrecord==1 && $reportcountrows==0){return null;}
          
          $sendto=$this->getUtildata()->getVaueOfArray($mconfig,'recipient.to',true);
          if(empty($sendto)){return null;}
          
          $table=$this->getContainer()->get('badiu.system.core.lib.report.table');
           $app=$this->getContainer()->get('badiu.system.core.lib.util.app'); 
           $container=$this->getContainer();
           $router=$this->getContainer()->get("router");
          $table->getColumn()->add('user', $this->getTranslator()->trans('user'), null);
          $table->getColumn()->add('email', $this->getTranslator()->trans('email'), null);
           foreach ($listuser as $lu) {
              $table->getRow()->add($lu);
          }
         
                                         
          $this->page->addData('badiu_table1',$table);
	  $fileprocess=$this->page->getConfig()->getFileprocessindexscheduler();
          $page=$this->page;
          $file=$app->getFilePath($fileprocess,false);
	  $contents=null;
	  ob_start();
             include_once($file);
             $contents = ob_get_contents();
          ob_end_clean();
	  
	 
        
          $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
         $customcontent.="<br />";
         $customcontent.=$contents;
         $mconfig['message']['body']=$customcontent;
         $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
        $smtp->setUsersender($this->getUsersender());
         $entity=$this->getTaskdto()->getEntity(); 
         $smtp->setEntity($entity);
         $sendmsg=$smtp->sendMail($mconfig);
         return $sendmsg;
      }*/

      function sendReportOfUserNotified($countrecordglobal){
        $lmdata=$this->getContainer()->get('badiu.system.scheduler.tasklogmessage.data');
        //get logid
        $tasklogid=$this->getTasklogid();
        $entity=$this->getTaskdto()->getEntity();
        //count user
        $lmdata=$this->getContainer()->get('badiu.system.scheduler.tasklogmessage.data');
        $paramfilter=array('entity'=>$entity,'tasklogid'=>$tasklogid);
        
        $maxrowperconnection=$this->maxrowperconnection;
        $maxrowtoexec=$this->maxrowperprocess;
        $subprocess=0;
        $pagingindex=0;
        $countprocessed=0;
       
        $countrecord= $lmdata->countGlobalRow($paramfilter);
        if($countrecord > $maxrowperconnection){$subprocess=$countrecord/$maxrowperconnection;}
        if($subprocess != round($subprocess)){
            $subprocess=floor($subprocess)+1;
        }
        if($subprocess == 0){$subprocess=1;}
        if($subprocess > 0){
            $filemanagereport=$this->getContainer()->get('badiu.system.file.lib.managereport');
            $filemanagereport->setSessionhashkey($this->getSessionhashkey());
			$filemanagereport->setSystemdata($this->getSystemdata());
            $paramfileh=array('hashnameappend'=>$this->getTaskdto()->getId());
            $filemanagereport->start($paramfileh);
            $fileheader=array('userrecipient'=>$this->getTranslator()->trans('user'),'usercontact'=>$this->getTranslator()->trans('email'));
           $filemanagereport->head($fileheader);
             for ($i = 0; $i < $subprocess; $i++) {
                $parampaging=array('offset'=>$i,'limit'=>$maxrowperconnection);
                $idata=$lmdata->getGlobalColumnsValues("o.userrecipient,o.usercontact",$paramfilter,$parampaging);
                $icount=0;
                if(is_array($idata)){
                    $icount=sizeof($idata);
                    $countprocessed=$countprocessed+$icount;
                    $filemanagereport->addrows($idata);
                }
                if($icount==0 || $countprocessed>=$maxrowtoexec){break;}
            }
            $filemanagereport->end();

        }
        

        //add 120 record to send in e-mail body
        $maxrowtomailbody=$this->getMaxrowtomailbody();
        $parampaging=array('offset'=>0,'limit'=>$maxrowtomailbody);
        $datatobodymail=$lmdata->getGlobalColumnsValues(" o.userrecipient AS user,o.usercontact AS email ",$paramfilter,$parampaging);

        $tparam=array();
        $tparam['head']=array('user'=>$this->getTranslator()->trans('user'),'email'=> $this->getTranslator()->trans('email'));
        $tparam['rows']=$datatobodymail;
        $tparam['countrowcurrentforceset']=true;
        $tparam['countrows']=$countrecord;
        
        $table=$this->castArrayToTable($tparam);
        $mailreportcontentbody=$this->getReportContentToBodyMail($table);
        //send e-mail 
        $mparam=array();
        
        $mparam['reportcountrows']=$countrecord;
        $mparam['typemessage']='sendreport';
       
        $mconfig= $this->getTaskdto()->getReportmessage();
        $mconfig=$this->getJson()->decode($mconfig,true);
       if($countrecord > 0) {$mconfig['attach'][0]=$filemanagereport->attachToMail();}
        $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
        $customcontent.="<br />";
        $customcontent.=$mailreportcontentbody;
        $mconfig['message']['body']=$customcontent;
        
        $resultsendmail=$this->sendMail($mconfig,$mparam);
        $filemanagereport->remove();
        //send by badiusmtp

        //send by moodlesmtp

  }
      function initUsersendergetKey($reportfilter) {
        $key=$this->getUtildata()->getVaueOfArray($reportfilter,'config.router',true);
        $dkey=$this->getUtildata()->getVaueOfArray($reportfilter,'filter._dkey',true);
        if(!empty($dkey)){$key=$dkey;}
        $parentid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter.parentid',true);
        $pos=strpos($key,'badiu.moodle.mreport.');
        
        if($pos !== FALSE && $pos==0 && !empty($parentid)){
            $servicedata = $this->getContainer()->get('badiu.admin.server.service.data');
           $servicename= $servicedata->getGlobalColumnValue('name',array('id'=>$parentid));
           $this->setUsersender($servicename);
        }
      }

      
      function processExpression($message,$data) {
          
        $serviceexprekey=$this->kminherit->schedulerexpression();
        if(!$this->getContainer()->has($serviceexprekey)){
            $serviceexprekey=$this->kminherit->schedulerexpressionparent();
            if(!$this->getContainer()->has($serviceexprekey)){
                $serviceexprekey=$this->kminherit->schedulerexpressionparent(2);
               if(!$this->getContainer()->has($serviceexprekey)){
                $serviceexprekey=$this->kminherit->schedulerexpressionparent(3);
                  if(!$this->getContainer()->has($serviceexprekey)){return $message;}
                }
             }
        }
        if(!$this->getContainer()->has($serviceexprekey)){return $message;}

                
        $serviceexpredata=$this->getContainer()->get($serviceexprekey);
        if(!method_exists($serviceexpredata, 'init')){return $message;}
        if(!method_exists($serviceexpredata, 'replace')){return $message;}
        if(!method_exists($serviceexpredata, 'setSessionhashkey')){return $message;}
        $serviceexpredata->setSessionhashkey($this->getSessionhashkey());
		$serviceexpredata->setSystemdata($this->getSystemdata());
        $serviceexpredata->init($message,$data);
        $parentid=$this->getUtildata()->getVaueOfArray($this->param,'parentid');
        $serviceexpredata->setParentid($parentid);
        $serviceexpredata->changeData();
        $serviceexpredata->replace();
        $message= $serviceexpredata->getMessage();

      
        return  $message;
  }
    function sendMail($mconfig,$param,$smtp=null) {
       
        $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
        if(empty($smtpservice)){$smtpservice='badiunetconfig';}
         
       $typemessage=$this->getUtildata()->getVaueOfArray($param,'typemessage');
        $sendonlyreportwithrecord=$this->getUtildata()->getVaueOfArray($mconfig,'sendonlyreportwithrecord');
        if($sendonlyreportwithrecord==null || $sendonlyreportwithrecord==''){$sendonlyreportwithrecord=1;}
        $sendmail=true;
        if($typemessage=='sendreport'){
            $reportcountrows=$this->getUtildata()->getVaueOfArray($param,'reportcountrows');
            if($sendonlyreportwithrecord==1 && $reportcountrows==0){ $sendmail=false;}
        }
        $execsendmail=false;
        $emailsto=$mconfig['recipient']['to'];
        if($smtpservice=='badiunetconfig'){
           if(empty($smtp)){
			   $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
			   $smtp->setSessionhashkey($this->getSessionhashkey());
			   $smtp->setSystemdata($this->getSystemdata());
			   $smtp->setUsersender($this->getUsersender());
               $entity=$this->getTaskdto()->getEntity();
               $smtp->setEntity($entity); 
			}
		   
          
           if($sendmail && !empty($emailsto)){
               $execsendmail=$smtp->sendMail($mconfig);
           }
           else{$execsendmail=-1;}
        }else if ($smtpservice=='moodleconfig'){
           $reportfilter=$this->getTaskdto()->getReportfilter();
           $reportfilter=$this->getJson()->decode($reportfilter,true);
           $parentid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter.parentid',true);
           $smtpserviceparam=array(); 
           $smtpserviceparam['_serviceid']=$parentid;
           $smtpserviceparam['mail']['subject']=$this->getUtildata()->getVaueOfArray($mconfig,'message.subject',true);
           $smtpserviceparam['mail']['message']=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
           $smtpserviceparam['mail']['to']=$emailsto;
           if($sendmail && !empty($emailsto)){
               $mdlsmtpservice=$this->getContainer()->get('badiu.moodle.mreport.lib.smtpservice');
               $mdlsmtpservice->setSessionhashkey($this->getSessionhashkey());
			   $mdlsmtpservice->setSystemdata($this->getSystemdata());
               $smtpserviceparam=$mdlsmtpservice->castListEmail($smtpserviceparam);
               $resultmdlsms=$mdlsmtpservice->exec($smtpserviceparam);
               $emailsendto=$mdlsmtpservice->getFirstEmail($emailsto);
               $execsendmail=$mdlsmtpservice->checkResponse($resultmdlsms,$emailsendto);
            }
        }
        return $execsendmail;
    }
	 function sendMoodleMessage($mconfig) {
       
	    $service="badiu.moodle.core.lib.message|sendFromReportScheduler";
		
		$result=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($service,$mconfig);
        
        return $result;
    }
	function sendMoodleNotification($mconfig) {
       
	    $service="badiu.moodle.core.lib.message|sendNotificationFromReportScheduler";
		
		$result=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($service,$mconfig);
        
        return $result;
    }
	function sendServiceMessage($mconfig,$clientwsid) {
       
	    $fgclientwebservice=$this->getContainer()->get('badiu.system.module.clientws.lib.factorygeneralclientwebservice');
		$fgclientwebservice->init($clientwsid);
		$result=$fgclientwebservice->sigleRequest($mconfig);
		return $result;
    }
    function execSystemRecipient($param) {
        $resultinfo=$this->getUtildata()->getVaueOfArray($param,'resultinfo');
        $sendmail=$this->getUtildata()->getVaueOfArray($param,'sendmail');

        if(!$sendmail){return $resultinfo;}
       
        $recipientparam=$this->page->getConfig()->getSchedulersystemrecipient();
        if(empty($recipientparam)){return $resultinfo;}

        $subQueryString = $this->getContainer()->get('badiu.system.core.lib.http.subquerystring');
		$subQueryString->setQuery($recipientparam);
		$subQueryString->makeParam();

        $service=$subQueryString->getValue('service');
        if(empty($service)){return $resultinfo;}

        $columnkey=$subQueryString->getValue('columnkey');
       if(empty($columnkey)){return $resultinfo;}

        if(!$this->getContainer()->has($service)){return $resultinfo;}
        $service=$this->getContainer()->get($service);
        $service->setSessionhashkey($this->getSessionhashkey());
		$service->setSystemdata($this->getSystemdata());
        $param['columnkey']=$columnkey;

        $service->init($param);
        $columnkeyvalue=$this->getUtildata()->getVaueOfArray($this->param,$columnkey);

        //if report has only one key value of columnkey
        if(!empty($columnkeyvalue)){
            $resultwsplt=$service->sendMessageWihtouSplit();
            return $resultwsplt;
           }else{
            //if report has multiple  key value of columnkey
            $resultsplt=$service->sendMessageSplited();
            return $resultsplt;
        }
        
        return $resultinfo;
    }


    public function exportTodatabase(){
        $subprocessstatus=$this->getTaskdto()->getSubprocessstatus();

        $result=null;
        if($subprocessstatus=='none' || $subprocessstatus=='completedcontinue'){ 
           $result= $this->exportTodatabaseWithStatusStart();
        }else if($subprocessstatus=='incomplete'){
            $result= $this->exportTodatabaseWithStatusIncomplete();
        }
        return $result;
    }

    public function exportTodatabaseWithStatusStart(){
       
        $typedbexport=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.type',true);
        if(empty($typedbexport)){ $typedbexport='legacydata';} //legacydata | incrementaldata

        $maxrow=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.maxrowperconnection',true);
        if(empty($maxrow)){ $maxrow=$this->getMaxrowperconnection();}
       
        $maxrowtoexec=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.maxrowperprocess',true);
        if(empty($maxrowtoexec)){ $maxrow=$this->getMaxrowperprocess();}
       
        $maxerror=5;
        $pagingindex=0;
        $data=$this->getContent($pagingindex,$maxrow,array('partialdata'=>'countrows'));
        $countrecord=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_countrows');
       
        $subprocess=0;
        $datashouldexecinlastpaging=$maxrowtoexec;
        if($countrecord > $maxrow){$subprocess=$countrecord/$maxrow;}
        if($subprocess != round($subprocess)){
            $subprocess=floor($subprocess)+1;
            $datashouldexecinlastpaging=$countrecord%$maxrow;
        }

        $tasklogid=$this->getTasklogid();
        $entity=$this->getTaskdto()->getEntity();
        $taskid=$this->getTaskdto()->getId();

        $tasksubprocessdata=$this->getContainer()->get('badiu.system.scheduler.tasksubprocess.data');
        $tasklogdata=$this->getContainer()->get('badiu.system.scheduler.tasklog.data');
        $taskdata=$this->getContainer()->get('badiu.system.scheduler.task.data');
        
        if($subprocess > 0){
            
            //add subprocess
            $maxrowperconnection=$this->getMaxrowperconnection();
            $continsert=0;
            $seq=1;
            
            for ($i = 0; $i < $subprocess; $i++) {
                   if($seq==$subprocess){$maxrowperconnection= $datashouldexecinlastpaging;}
                    $parami=array('entity'=>$entity,'taskid'=>$taskid,'tasklogid'=>$tasklogid,'status'=>'programmed','pagingindex'=>$i,'datashouldexec'=>$maxrowperconnection,'dtype'=>'automatic','timecreated'=>new \DateTime());
                    $result=$tasksubprocessdata->insertNativeSql($parami,false);
                   if($result){$continsert++;}
                   $seq++;
            }
           
            //update task status of subprocess
            $paramtakup=array('id'=>$taskid,'subprocessstatus'=>'processing','subprocessprogress'=>0);
            $resulttup=$taskdata->updateNativeSql($paramtakup,false);
            
            $paramtaklogup=array('id'=>$tasklogid,'subprocessstatus'=>'processing','subprocessprogress'=>0);
            $resultltup=$tasklogdata->updateNativeSql($paramtakup,false);
           
            //get list subprocess
            $listsubprocess=$tasksubprocessdata->getListToExec($taskid,'programmed');
            
            //exec each subprocess
            $coundataexec=0;
            $countsubprocessexec=0;
            $countsubprocess=0;
            $couterror=0;
            $countdatashouldexec=0;
            foreach ($listsubprocess as $sprow) {
                $pagingindex=$this->getUtildata()->getVaueOfArray($sprow,'pagingindex');
                $subprocessid=$this->getUtildata()->getVaueOfArray($sprow,'id');
                $datashouldexec=$this->getUtildata()->getVaueOfArray($sprow,'datashouldexec');
                if($countsubprocess==0){$maxrow=$datashouldexec;}
                $countdatashouldexec+=$datashouldexec;
                $timestart=new \DateTime();
                $parampi=array('id'=>$subprocessid,'status'=>'processing','timestart'=>$timestart);
                $tasksubprocessdata->updateNativeSql($parampi,false);

                //process 
                $cparam=array('partialdata'=>'getrows');
                $precessresutl=$this->exportTodatabaseProcessData($pagingindex,$maxrow,$cparam);

                $resultstatus=$this->getUtildata()->getVaueOfArray($precessresutl,'status');
                $resultprocessed=$this->getUtildata()->getVaueOfArray($precessresutl,'message');

                $timeend=new \DateTime();
                $timeexec=$timeend->getTimestamp()-$timestart->getTimestamp();
                $parampi=array('id'=>$subprocessid,'status'=>'success','timeend'=>$timeend,'timeexec'=>$timeexec,'timemodified'=>new \DateTime());
               
                if($resultstatus=='accept'){
                    $parampi['dataexec']=$resultprocessed;
                    $tasksubprocessdata->updateNativeSql($parampi,false);
                    $coundataexec+=$resultprocessed;
                    $countsubprocessexec++;
                    if( $coundataexec >=$maxrowtoexec){break;}
                }else{
                    $parampi['status']='failed';
                    $param=array();
                    $param['error']=$resultstatus;
                    $param= $this->getJson()->encode($param);
                    $parampi['param']=$param;
                    $tasksubprocessdata->updateNativeSql($parampi,false);
                    $couterror++;
                    if($couterror>=$maxerror){break;}
                }
                $countsubprocess++;
           
            }

            $totalsubprocess=$tasksubprocessdata->count($taskid);
            $totalsubprocessexeccussess=$tasksubprocessdata->count($taskid,'success');

            $taskpercent=0;
            if($totalsubprocess > 0 && $totalsubprocessexeccussess >0){
                $taskpercent=$totalsubprocessexeccussess * 100 / $totalsubprocess;;
             }
            $taskprocesspercent=0;
            if($totalsubprocess > 0 && $countsubprocess >0){
                $taskprocesspercent=$countsubprocess * 100 / $totalsubprocess;;
            }

            //update process
            $paramtaklogup=array('id'=>$tasklogid,'subprocessstatus'=>'completed','subprocessprogress'=>$taskprocesspercent);
            $resulttlup=$tasklogdata->updateNativeSql($paramtaklogup,false);
            //update task
            $subprocessstatus='incomplete';
            if($taskpercent==100){
                $subprocessstatus='completed';
                if( $typedbexport=='incremental'){$subprocessstatus='completedcontinue';}
            }
            $paramtakup=array('id'=>$taskid,'subprocessstatus'=>$subprocessstatus,'subprocessprogress'=>$taskpercent);
            $result=$taskdata->updateNativeSql($paramtakup,false);

            $processresult=array();
            $processresult['resultinfo']['datashouldexec']=$countdatashouldexec;
            $processresult['resultinfo']['dataexec']=$coundataexec;
            return $processresult;
        }else{
             
            $cparam=array('partialdata'=>'getrows');
            $precessresutl=$this->exportTodatabaseProcessData($pagingindex,$maxrow,$cparam);
            $resultstatus=$this->getUtildata()->getVaueOfArray($precessresutl,'status');
            $resultprocessed=$this->getUtildata()->getVaueOfArray($precessresutl,'message');

            if($resultstatus=='accept'){ 
                $processresult=array();
                $processresult['resultinfo']['datashouldexec']=$countrecord;
                $processresult['resultinfo']['dataexec']=$resultprocessed;
                return  $processresult;
            }else{
                //errro register
                exit;
            }
        }
    }

    public function exportTodatabaseWithStatusIncomplete(){
        $maxrow=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.maxrowperconnection',true);
        if(empty($maxrow)){ $maxrow=$this->getMaxrowperconnection();}
       
        $maxrowtoexec=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.maxrowperprocess',true);
        if(empty($maxrowtoexec)){ $maxrow=$this->getMaxrowperprocess();}
       
        $maxerror=5;
        $paginindex=0;
        
        $tasklogid=$this->getTasklogid();
        $entity=$this->getTaskdto()->getEntity();
        $taskid=$this->getTaskdto()->getId();

        $tasksubprocessdata=$this->getContainer()->get('badiu.system.scheduler.tasksubprocess.data');
        $tasklogdata=$this->getContainer()->get('badiu.system.scheduler.tasklog.data');
        $taskdata=$this->getContainer()->get('badiu.system.scheduler.task.data');
       
        
            
        //update task status of subprocess
        $paramtakup=array('id'=>$taskid,'subprocessstatus'=>'processing');
        $resulttup=$taskdata->updateNativeSql($paramtakup,false);
            
        $paramtaklogup=array('id'=>$tasklogid,'subprocessstatus'=>'processing','subprocessprogress'=>0);
        $resultltup=$tasklogdata->updateNativeSql($paramtakup,false);

        //get list subprocess
        $listsubprocess=$tasksubprocessdata->getListToExec($taskid,'programmed');
            
        //exec each subprocess
        $coundataexec=0;
        $countsubprocessexec=0;
        $countsubprocess=0;
        $couterror=0;
        $countdatashouldexec=0;
        foreach ($listsubprocess as $sprow) {
                $pagingindex=$this->getUtildata()->getVaueOfArray($sprow,'pagingindex');
                $subprocessid=$this->getUtildata()->getVaueOfArray($sprow,'id');
                $datashouldexec=$this->getUtildata()->getVaueOfArray($sprow,'datashouldexec');
                if($countsubprocess==0){$maxrow=$datashouldexec;}
                $countdatashouldexec+=$datashouldexec;
                $timestart=new \DateTime();
                $parampi=array('id'=>$subprocessid,'status'=>'processing','timestart'=>$timestart);
                $tasksubprocessdata->updateNativeSql($parampi,false);

                //process 
                $cparam=array('partialdata'=>'getrows');
                $precessresutl=$this->exportTodatabaseProcessData($pagingindex,$maxrow,$cparam);

                $resultstatus=$this->getUtildata()->getVaueOfArray($precessresutl,'status');
                $resultprocessed=$this->getUtildata()->getVaueOfArray($precessresutl,'message');

                $timeend=new \DateTime();
                $timeexec=$timeend->getTimestamp()-$timestart->getTimestamp();
                $parampi=array('id'=>$subprocessid,'status'=>'success','timeend'=>$timeend,'timeexec'=>$timeexec,'timemodified'=>new \DateTime());
               
                if($resultstatus=='accept'){
                    $parampi['dataexec']=$resultprocessed;
                    $tasksubprocessdata->updateNativeSql($parampi,false);
                    $coundataexec+=$resultprocessed;
                    $countsubprocessexec++;
                    if( $coundataexec >=$maxrowtoexec){break;}
                }else{
                    $parampi['status']='failed';
                    $param=array();
                    $param['error']=$resultstatus;
                    $param= $this->getJson()->encode($param);
                    $parampi['param']=$param;
                    $tasksubprocessdata->updateNativeSql($parampi,false);
                    $couterror++;
                    if($couterror>=$maxerror){break;}
                }
                $countsubprocess++;
           
        }

            $totalsubprocess=$tasksubprocessdata->count($taskid);
            $totalsubprocessexeccussess=$tasksubprocessdata->count($taskid,'success');

            $taskpercent=0;
            if($totalsubprocess > 0 && $totalsubprocessexeccussess >0){
                $taskpercent=$totalsubprocessexeccussess * 100 / $totalsubprocess;;
             }
            $taskprocesspercent=0;
            if($totalsubprocess > 0 && $countsubprocess >0){
                $taskprocesspercent=$countsubprocess * 100 / $totalsubprocess;;
            }

            //update process
            $paramtaklogup=array('id'=>$tasklogid,'subprocessstatus'=>'completed','subprocessprogress'=>$taskprocesspercent);
            $resulttlup=$tasklogdata->updateNativeSql($paramtaklogup,false);
            //update task
            $subprocessstatus='incomplete';
            if($taskpercent==100){$subprocessstatus='completed';}
            $paramtakup=array('id'=>$taskid,'subprocessstatus'=>$subprocessstatus,'subprocessprogress'=>$taskpercent);
            $result=$taskdata->updateNativeSql($paramtakup,false);

            $processresult=array();
            $processresult['resultinfo']['datashouldexec']=$countdatashouldexec;
            $processresult['resultinfo']['dataexec']=$coundataexec;
            return $processresult;
       
    }

   
    public function exportTodatabaseProcessData($paginindex,$maxrow,$cparam){
        $data=$this->getContent($paginindex,$maxrow,$cparam);
        $table=$this->getUtildata()->getVaueOfArray($data,'badiu_table1');
        $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
        $sqlservice->setSessionhashkey($this->getSessionhashkey());
		$sqlservice->setSystemdata($this->getSystemdata()); 
		$moodleidtarget=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.targetconfig.systeminstance',true);
        $tabletarget=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.targetconfig.table',true);
        $servicexportexec=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.targetconfig.service',true);
        $synckey=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.targetconfig.synckey',true);
        $synckey=$this->castUniquekeyToArray($synckey);

        $otherconfig=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.otherconfig',true);

        if(empty($servicexportexec)){$servicexportexec='badiu.moodle.mreport.lib.generalexportdbmoodlesummarize';}
        $param['_serviceid']=$moodleidtarget;
        $param['_serviceidsource']=$this->getUtildata()->getVaueOfArray($this->param,'_serviceid');
        $param['_key']='local.badiunet.mdata.dbfactory.addorupdaterows';
        $param['mdata']['tbl']=$tabletarget;
        $param['mdata']['ku']= $synckey;//array(array('key'=>'moodleid'),array('key'=>'userid'),array('key'=>'courseid'),array('key'=>'activityid'),array('key'=>'customint2'),array('key'=>'responseid'));
        $param['mdata']['rows']=$table->getRow()->getList();
        $param['otherconfig']=$otherconfig;
       
        $generalexportdbmoodlesummarize=$this->getContainer()->get($servicexportexec);
        $generalexportdbmoodlesummarize->setSessionhashkey($this->getSessionhashkey());
		$generalexportdbmoodlesummarize->setSystemdata($this->getSystemdata());
        $param=$generalexportdbmoodlesummarize->changedata($param);
        
        $result=$sqlservice->service($param);
       
        return $result;
    }


    public function castUniquekeyToArray($paramuk){
        $luk=null;
        if(empty($paramuk)){return null;}
        $txtuk=str_replace(";",",",$paramuk);
        if(!empty($txtuk)){$txtuk=str_replace(' ','', $txtuk);}
        $pos=stripos($txtuk, ",");
        if($pos=== false){
                $luk=array($txtuk);
        }else{
                $luk= explode(",",$txtuk);
        }
        $listkey=array();
        foreach ($luk as $k) {
            array_push( $listkey,array('key'=>$k));
           
        }
        return $listkey;
    }


    public function moodleBackupRestoreCourse(){
		//echo $this->getTasklogid();exit;
        $service=$this->getUtildata()->getVaueOfArray($this->dconfig,'moodlebackuprestorecourse.service',true);
        $moodleidtarget=$this->getUtildata()->getVaueOfArray($this->dconfig,'moodlebackuprestorecourse.moodleidtarget',true);
        $coursecategorytarget=$this->getUtildata()->getVaueOfArray($this->dconfig,'moodlebackuprestorecourse.coursecategorytarget',true);
        $includeuserinteraction=$this->getUtildata()->getVaueOfArray($this->dconfig,'moodlebackuprestorecourse.includeuserinteraction',true);
        $setcoursetimestart=$this->getUtildata()->getVaueOfArray($this->dconfig,'moodlebackuprestorecourse.setcoursetimestart',true);

        $moodleidsourece=$this->getUtildata()->getVaueOfArray($this->param,'_serviceid');
       
	    $servicedata=$this->getContainer()->get('badiu.admin.server.service.data');
	    $moodleurlsourece=$servicedata->getGlobalColumnValue('url',array('id'=> $moodleidsourece)); 
        
		$maxrow=$this->getMaxrowperconnection();
        $maxrowtoexec=$this->getMaxrowperprocess();
        $pagingindex=0;
        $datacount=$this->getContent($pagingindex,$maxrow,array('partialdata'=>'countrows'));
        $countrecord=$this->getUtildata()->getVaueOfArray($datacount,'badiu_list_data_countrows');
      
        $subprocess=0;
        $srows=array();
        $cparam=array('partialdata'=>'getrows','dataout'=>'withouttable');
        if($countrecord > $maxrow){$subprocess=$countrecord/$maxrow;}
        if($subprocess != round($subprocess)){ $subprocess=floor($subprocess)+1;}
        if($subprocess == 0){$subprocess=1;}
        $countprocessed=0;
       
        for ($i = 0; $i < $subprocess; $i++) {
           $irows=$this->getContent($i,$maxrow,$cparam);
             $icount=0;
            if(is_array($irows)){
                foreach ($irows as $irow) {array_push($srows,$irow);$icount++;$countprocessed++;}
            }
            if($icount==0 || $countprocessed>=$maxrowtoexec){break;}
        }
        
        $pservice=null;
        if($this->getContainer()->has($service)){
            $pservice=$this->getContainer()->get($service);
            $pservice->setSessionhashkey($this->getSessionhashkey());
			$pservice->setSystemdata($this->getSystemdata());
        }else{
            //register failure
            return null;
        }
        $parambckrst=array();
        $hash=$this->getContainer()->get('badiu.system.core.lib.util.hash');
        $token=$hash->make(30);
        foreach ($srows as $srow) {
            $courseprocess=array();
            $courseid=$this->getUtildata()->getVaueOfArray($srow,'courseid');
            if(empty($courseid)){$courseid=$this->getUtildata()->getVaueOfArray($srow,'id');}
            $bckparam=array('courseid'=>$courseid,'sserviceid'=> $moodleidsourece,'includeuserinteraction'=>$includeuserinteraction);
            $result =$pservice->makeBackup($bckparam);
            
            $backupstatus=$this->getUtildata()->getVaueOfArray($result,'status');
            $backupmessage=$this->getUtildata()->getVaueOfArray($result,'message');
            $courseprocess['backupprocess']=$result;
            //add key
            
            if($backupstatus=='accept'){
                $bcckey="badiu.moodle.mreport.backup.ckey.".$courseid.".".time().".".$hash->make(10);
                $bccvalue=$hash->make(150);
                
                $bccadtkey="badiu.moodle.mreport.backup.ckeyad.".$courseid.".".time().".".$hash->make(10);
                $bccadtvalue=$hash->make(200);

                $bccconfigkey="badiu.moodle.mreport.backup.ckeycfg.".$courseid.".".time().".".$hash->make(10);
                $bccconfigvalue= $this->getJson()->encode($backupmessage);

                $dbrows=array();	
				$dbrows[0]=array('name'=>$bcckey,'value'=>$bccvalue,'timecreated'=>time());
				$dbrows[1]=array('name'=>$bccadtkey,'value'=>$bccadtvalue,'timecreated'=>time());
				$dbrows[2]=array('name'=>$bccconfigkey,'value'=>$bccconfigvalue,'timecreated'=>time());
				
				
				$ckcong=array();
                $ckcong['_keyautdbcheckk']=array('name'=>$bcckey,'value'=>$bccvalue);
                $ckcong['_keyautdbcheckkadtional']=array('name'=>$bccadtkey,'value'=>$bccadtvalue);
                $ckcong['_keyautdbcheckkfconfig']=array('name'=>$bccconfigkey,'value'=>$bccconfigvalue);
                $courseprocess['authfordownloadfilebackup']=$ckcong;
               
                //insert auth to source moodle
                $resultadrd=$pservice->addRemoteData($bparam=array('_serviceid'=>$moodleidsourece,'rows'=>$dbrows));
				$courseprocess['authfordownloadfilebackupsoursesaved']=$resultadrd;
               
               
				//restaure in target moodle
				$rstparam=array();
				$rstparam['source']['keys']=$ckcong;
				$rstparam['source']['url']=$moodleurlsourece;
				$rstparam['sserviceid']=$moodleidtarget;
				$rstparam['target']['categoryid']=$coursecategorytarget;
				$rstparam['target']['userinteraction']=$includeuserinteraction;
				$rstparam['target']['coursetimestart']=$setcoursetimestart;
               
				$resultrst =$pservice->makeRestore($rstparam);
				
              //  print_r($resultrst);
				// echo "<hr>";
				  echo "<pre>";
				 print_r($resultrst);
				  echo "</pre>";
                echo "<hr>";exit; 
                

            }
            

           
           
            $parambckrst[$courseid]=$ckcong;
            echo "remote result:";
            print_r($result);
            echo "<hr />===============================";
        }
       // echo "<pre>";
       /* print_r($this->dconfig);
        echo "<hr>";
        print_r($this->param);
        echo "<hr>";
        print_r($srows);
        echo "</pre>";*/
        echo "<BR />******************b1*******************";exit;
        return null;
    }


	public function initFreportDynamic($fparam) {
		 
		 $freportid=$this->getUtildata()->getVaueOfArray($fparam, '_freportid');
		 $force = $this->getUtildata()->getVaueOfArray($fparam, '_force');
		 $dkey = $this->getUtildata()->getVaueOfArray($fparam, '_dkey');
		if(empty($dkey)){$dkey=$this->getKey();}
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
		$badiuSession->setSystemdata($this->getSystemdata());
        
		$sessionkey=$this->getKey().'.'.$freportid;
		$hassession=$badiuSession->getValue($sessionkey);
		if($hassession && !$force){return null;} 
		
		$factorysdk=$this->getContainer()->get('badiu.sync.freport.freport.lib.factoryservicereportdynamickey');
		$factorysdk->setSessionhashkey($this->getSessionhashkey());
		$factorysdk->setSystemdata($this->getSystemdata());
		$faparam=array('freportid'=>$freportid,'dkey'=>$dkey);
		$factorysdk->addSession($faparam);
    }
        function getKey() {
            return $this->key;
        }

        function getBkey() {
            return $this->bkey;
        }

        function getLayout() {
            return $this->layout;
        }

        function getKminherit() {
            return $this->kminherit;
        }

        function getParam() {
            return $this->param;
        }

        function getPage() {
            return $this->page;
        }

        function setKey($key) {
            $this->key = $key;
        }

        function setBkey($bkey) {
            $this->bkey = $bkey;
        }

        function setLayout($layout) {
            $this->layout = $layout;
        }

        function setKminherit($kminherit) {
            $this->kminherit = $kminherit;
        }

        function setParam($param) {
            $this->param = $param;
        }

        function setPage($page) {
            $this->page = $page;
        }

        function getAction() {
            return $this->action;
        }

        function setAction($action) {
            $this->action = $action;
        }

      

        function getUsersender() {
            return $this->usersender;
        }
    
        function setUsersender($usersender) {
            $this->usersender = $usersender;
        }

        function getMaxrowperprocess() {
            return $this->maxrowperprocess;
        }
    
        function setMaxrowperprocess($maxrowperprocess) {
            $this->maxrowperprocess = $maxrowperprocess;
        }

        function getMaxrowperconnection() {
            return $this->maxrowperconnection;
        }
    
        function setMaxrowperconnection($maxrowperconnection) {
            $this->maxrowperconnection = $maxrowperconnection;
        }

        function getDconfig() {
            return $this->dconfig;
        }
    
        function setDconfig($dconfig) {
            $this->dconfig = $dconfig;
        }
        function getMaxrowtomailbody() {
            return $this->maxrowtomailbody;
        }
    
        function setMaxrowtomailbody($maxrowtomailbody) {
            $this->maxrowtomailbody = $maxrowtomailbody;
        }
}
