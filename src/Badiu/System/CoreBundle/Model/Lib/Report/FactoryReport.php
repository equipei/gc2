<?php

namespace Badiu\System\CoreBundle\Model\Lib\Report;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**
 *
 * @author lino
 */
class FactoryReport extends BadiuModelLib {

 private $paginglimit=null;
    function __construct(Container $container) {
        parent::__construct($container);
    }

 public function teste() {
	 $fparam=array('_key'=>'badiu.moodle.mreport.enrol.enrol.index','_query'=>'parentid=4&_serviceid=4&_datasource=servicesql');
	 $this->exec($fparam);
 }
   
   public function exec($fparam) {
	    $key=""; 
		$bkey="";
		$bkeydynamicroute="";
		$kminherit="";
		$kminheritfilter="";
		$dynamicroute=false;
		$currentkeyroutefilter="";
		$layout="";
	
      $key=$this->getUtildata()->getVaueOfArray($fparam,'_key');
	  
	  $query=$this->getUtildata()->getVaueOfArray($fparam,'_query');

	  if(!empty($query)){
		  $querystring=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
		  $querystring->setQuery($query);
		  $querystring->makeParam();
		  $querystring->add('_key',$key);
		  $fparam=$querystring->getParam();
	  }
	  
	  
	  $dkey=$this->getUtildata()->getVaueOfArray($fparam,'_dkey');
      if(empty($key)){$key=$this->getUtildata()->getVaueOfArray($fparam,'_key');}
	  
	  
	  if(!empty($dkey)){
		   $key=$dkey;
		   $dynamicroute=true;
	  }
	  
	  $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
      if($sysoperation->isView($key)){$layout='viewDetail';}
      else if($sysoperation->isIndex($key)){$layout='indexCrud';}
      else if($sysoperation->isDashboard($key)){$layout='dashboard';}
      


	  $bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
      $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
      $keymanger->setBaseKey($bkey);
      if($dynamicroute){
				$bkeydynamicroute=$this->getUtildata()->getVaueOfArray($fparam,'_key');
				$bkeydynamicroute=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($bkeydynamicroute);
                $keymanger->setBaseKeydyncoriginal($bkeydynamicroute);
             
       }
	   
	   $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
       $cbundle->setSessionhashkey($this->getSessionhashkey());
	   $cbundle->setSystemdata($this->getSystemdata());
       $cbundle->setKeymanger($keymanger);
       $cbundle->initKeymangerExtend($keymanger);
       $kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
       $kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
       $kminheritfilter=$kminherit;
	   $changecurrentroute=null; 
	   $currentkeyroutefilter=$key; 
       if($dynamicroute){ 
			$nr=$this->initFreportChangeRoute($fparam);
				if(!empty($nr)){$changecurrentroute=$nr;}
		}
	   
	    $keymangerfilter=$keymanger;
		if($dynamicroute && !empty( $changecurrentroute)){
			   $currentkeyroutefilter=$changecurrentroute;
			   $baseKeydyncoriginal = $this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($changecurrentroute);
				
				$keymangerfilter=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
				$keymangerfilter->setBaseKey($baseKeydyncoriginal);
            
                $cbundlefilter=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
				$cbundlefilter->setSessionhashkey($this->getSessionhashkey());
				$cbundlefilter->setSystemdata($this->getSystemdata());
				$cbundlefilter->setKeymanger($keymangerfilter);
				$cbundlefilter->initKeymangerExtend($keymanger);
				$kminheritfilter=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
				$kminheritfilter->init($keymangerfilter,$cbundlefilter->getKeymangerExtend()); ;
				
           }
			$this->initFreportDynamic($fparam,$key);
			
			$cform=$cbundle->getFormConfig('filter',$keymangerfilter);
			
			$report= $this->getContainer()->get($kminherit->report());
			$report->setSessionhashkey($this->getSessionhashkey());
			$report->setSystemdata($this->getSystemdata());
			$report->setKey($key);
			$report->setTranslator($this->getTranslator());
			$badiuSession=$this->getSession();
			$badiuSession->setHashkey($this->getSessionhashkey());
			
			$clienttype=$badiuSession->get()->getType();
			
			if(empty($this->paginglimit)){$this->paginglimit=$badiuSession->get()->getConfig()->getValue('badiu.system.core.param.config.report.paginationlimitdbrowstoexport');}
			
			if($clienttype=='webservice' || $clienttype=='webservicesynceduser'){ 
				$report->setIsexternalclient(true);
			}
			
			$report->getKeymanger()->setBaseKey($bkey);
			if($dynamicroute){
				$report->getKeymanger()->setBaseKeydyncoriginal($bkeydynamicroute);
			} 
			
			$report->setDefaultdatafilterconfig($cform->getDefaultdata());
			$report->setDefaultdatafilterconfigonopenform($cform->getDefaultdataonpenform());
			$report->setFconfig($cform);
			$type=null;
        
			if($layout=='indexCrud'){$type='dbsearch.fields.table.view'; }
			else if($layout=='viewDetail'){$type='dbgetrow.fields.table.view'; }
			
			$report->setIsservice(true);
			
			$serviceid=$this->getUtildata()->getVaueOfArray($fparam,'_serviceid');			
			$report->setServiceid($serviceid);
			$report->setPaginglimit($this->paginglimit);  
			
			$report->extractData($fparam,$layout,$type);
			if($layout=='indexCrud'){
				$report->makeTable($layout);
				return $report->getTable()->castToArray();
			}
			return null;
   } 
 
     
   public function initFreportDynamic($fparam,$key) {
		 if(!$this->getContainer()->has('badiu.sync.freport.freport.lib.factoryservicereportdynamickey')){return null;}
		 $factorysdk=$this->getContainer()->get('badiu.sync.freport.freport.lib.factoryservicereportdynamickey');
		 $factorysdk->setSessionhashkey($this->getSessionhashkey());
		 $factorysdk->setSystemdata($this->getSystemdata());
		 $freportid=$this->getUtildata()->getVaueOfArray($fparam, '_freportid');
		 $force = $this->getUtildata()->getVaueOfArray($fparam, '_force');
		 $dkey = $this->getUtildata()->getVaueOfArray($fparam, '_dkey');
		 if(empty($dkey)){$dkey=$key;}
		 $faparam=array('_freportid'=>$freportid,'_dkey'=>$dkey,'_force'=>$force);
		 
		 $factorysdk->initSession($faparam);
	 }
	public function initFreportChangeRoute($fparam) {
		if(!$this->getContainer()->has('badiu.sync.freport.freport.lib.factoryservicereportdynamickey')){return null;}
		$factorysdk=$this->getContainer()->get('badiu.sync.freport.freport.lib.factoryservicereportdynamickey');
		 $factorysdk->setSessionhashkey($this->getSessionhashkey());
		$factorysdk->setSystemdata($this->getSystemdata());
		
		$freportid=$this->getUtildata()->getVaueOfArray($fparam, '_freportid');
		$dkey = $this->getUtildata()->getVaueOfArray($fparam, '_dkey');
		$fdkey = $this->getUtildata()->getVaueOfArray($fparam, '_fdkey');
		$force = $this->getUtildata()->getVaueOfArray($fparam, '_force');
		if(empty($dkey)){return null;}
	    if(empty($fdkey)){return null;}
	   
		$faparam=array('_freportid'=>$freportid,'_dkey'=>$dkey,'_fdkey'=>1,'_force'=>$force);
		$nk=$factorysdk->initSessionOfDynmickey($faparam);
		return $nk;
    }
	
	function getPaginglimit() {
        return $this->paginglimit;
    }

    function setPaginglimit($paginglimit) {
        $this->paginglimit = $paginglimit;
    }
}
