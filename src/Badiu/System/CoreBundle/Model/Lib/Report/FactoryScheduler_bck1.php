<?php

namespace Badiu\System\CoreBundle\Model\Lib\Report;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

use Badiu\System\CoreBundle\Model\Lib\Http\Url; //review
use Badiu\System\CoreBundle\Model\Lib\Http\HttpQueryString; //review
use Badiu\System\CoreBundle\Model\Report\BadiuPagination; //review
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
class FactoryScheduler extends TaskGeneralExec {

     private $key; 
    private $bkey;
    private $bkeydynamicroute;
    private $layout='indexCrud'; 
    private $kminherit;
    private $param;
    private $page;
    private $usersender=null;
   private $action='send.report'; //send.menssage.touserinreport | send.report
   private $dynamicroute=false;
   private $maxrowperprocess=50000;
   private $maxrowperconnection=2500;
   private $dconfig;

    function __construct(Container $container) {
        parent::__construct($container);
         $this->key=null;
        $this->page=$this->getContainer()->get('badiu.system.core.page');
        $this->page->setIsexternalclient(TRUE);
        $this->page->setIsschedule(TRUE);
        }

    
     public function init() {
            $reportfilter=$this->getTaskdto()->getReportfilter();
            $reportfilter=$this->getJson()->decode($reportfilter,true);
            $this->action=$this->getTaskdto()->getReportaction();

           
            //dconfig
            $this->dconfig=$this->getTaskdto()->getDconfig();
            $this->dconfig=$this->getJson()->decode($this->dconfig,true);

            $this->dynamicroute=false;
            $this->key=$this->getUtildata()->getVaueOfArray($reportfilter,'config.router',true);
            $dkey=$this->getUtildata()->getVaueOfArray($reportfilter,'filter._dkey',true);
           
            if(!empty($dkey)){
                $this->key=$dkey;
                $this->dynamicroute=true;
            }
            $this->param=$this->getUtildata()->getVaueOfArray($reportfilter,'filter');
            $this->initUsersendergetKey($reportfilter); 
         // print_r($this->param);exit;
           $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
                         
            if($sysoperation->isView($this->key)){$this->layout='viewDetail';}
            else if($sysoperation->isIndex($this->key)){$this->layout='indexCrud';}
            else if($sysoperation->isDashboard($this->key)){$this->layout='dashboard';}
            else if($sysoperation->isFrontpage($this->key)){$this->layout='frontpage';}

            $this->bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($this->key);
            $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
            $keymanger->setBaseKey($this->bkey);
            if($this->dynamicroute){
                $this->bkeydynamicroute='badiu.system.core.report.dynamic.index';
                $parentid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter.parentid',true);
                if(!empty($parentid)){$this->bkeydynamicroute='badiu.system.core.report.dynamicp.index';}
                $this->bkeydynamicroute=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($this->bkeydynamicroute);
                $keymanger->setBaseKeydyncoriginal($this->bkeydynamicroute);
            }
            $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $cbundle->setSessionhashkey($this->getSessionhashkey());
            $cbundle->setKeymanger($keymanger);
            $cbundle->initKeymangerExtend($keymanger);
            $this->kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
            $this->kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
            
            $cpage=$cbundle->getPageConfig($keymanger);
            $this->page->setConfig($cpage);
            $croute=$cbundle->getCrudRoute($keymanger);
            $this->page->setCrudroute($croute);
            $this->page->setKey($this->key);
           // $formGroupList=$cbundle->getGroupOfField($keymanger->formFilterFieldsEnable(),$cbundle->getKeymangerExtend()->formFilterFieldsEnable());
           // $formFieldList=$cbundle->getFieldsInGroup($keymanger->formFilterFieldsEnable(),$cbundle->getKeymangerExtend()->formFilterFieldsEnable());
           
           // $this->page->addData('badiu_form1_group_list',$formGroupList);
           // $this->page->addData('badiu_form1_field_list',$formFieldList);
            
             $this->page->addData('badiu_form1_group_list',null);
            $this->page->addData('badiu_form1_field_list',null);
           
           // $dasheboardsqlsinlgleconfig=$cbundle->getValueQueryString($keymanger->dashboardDbSqlSingleConfig(),$cbundle->getKeymangerExtend()->dashboardDbSqlSingleConfig());
          //  $dasheboardsqlconfig=$cbundle->getValueQueryString($keymanger->dashboardDbSqlConfig(),$cbundle->getKeymangerExtend()->dashboardDbSqlConfig());

        //    $this->page->addData('badiu_list_data_row.config',$dasheboardsqlsinlgleconfig);
         //   $this->page->addData('badiu_list_data_rows.config',$dasheboardsqlconfig);
          
            $cform=$cbundle->getFormConfig('filter',$keymanger);
            $this->page->setFormconfig($cform);
           
          //  $fc=$this->getContainer()->get('badiu.system.core.lib.form.factoryconfig');
           //  $fc->setIsexternalclient(true);
        
            
            //refresh key
            $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
            $keymanger->setBaseKey($this->bkey);
           
            if($this->dynamicroute){
                $keymanger->setBaseKeydyncoriginal($this->bkeydynamicroute);
            } 
            $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $cbundle->setSessionhashkey($this->getSessionhashkey());
            $cbundle->setKeymanger($keymanger);
            $cbundle->initKeymangerExtend($keymanger);
            $this->kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
            $this->kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
           
     }
    public function exec() {
       
               $this->init();
               $result=null;
                if($this->action=='send.report'){
                    $result=$this->sendReport(); 
                    return $result;
                }else  if($this->action=='send.menssage.touserinreport'){
                     $result=$this->sendMessageToEachUserOfReport(); 
                     return $result;
                }
                else  if($this->action=='export.todatabase'){
                    $result=$this->exportTodatabase(); 
                    return $result;
               }else  if($this->action=='moodle.mreport.backuprestorecourse'){
                $result=$this->moodleBackupRestoreCourse(); 
                return $result;
            }

            
                return  $result;
    }
    
    private function getContent($paginindex=0,$maxrow=2500,$cparam=array()) {
        $this->param['_page']=$paginindex; 
      
        $report= $this->getContainer()->get($this->getKminherit()->report());
        $report->setSessionhashkey($this->getSessionhashkey());
        $report->setPaginglimit($maxrow); 
        $report->setIsexternalclient(true);
        $report->setIsschedule(true);
        $report->getKeymanger()->setBaseKey($this->bkey);
        if($this->dynamicroute){
            $report->getKeymanger()->setBaseKeydyncoriginal($this->bkeydynamicroute);
       } 
       
        $cform=$this->page->getFormconfig();
       // $report->setDefaultdatafilterconfig($cform->getDefaultdata());
        $report->setDefaultdatafilterconfigonopenform($cform->getDefaultdataonpenform());
        $report->setFconfig($cform);
         $type=null;
       // print_r($this->param);exit;
         if($this->layout=='indexCrud'){$type='dbsearch.fields.table.view'; }
         else if($this->layout=='viewDetail'){$type='dbgetrow.fields.table.view'; }
        $report->extractData($this->param,$this->layout,$type,$cparam);
       
        $format=$this->getUtildata()->getVaueOfArray($this->param,'_format');
        $data=array();
        if($this->layout=='indexCrud'){
             $report->makeTable($this->layout);
            
             $data['badiu_list_data_row']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row');
             $data['badiu_list_data_rows']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows');
             $data['badiu_list_data_countrows']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'countrows');
             $data['badiu_table1']=$report->getTable();
             $dataout=$this->getUtildata()->getVaueOfArray($cparam,'dataout');
             if($dataout=='withouttable'){return $report->getTable()->getRow()->getList();}
        }else if($this->layout=='dashboard'){
            $data['badiu_list_data_row']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row');
            $data['badiu_list_data_rows']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows');
          }else if($this->layout=='viewDetail'){
            $data['badiu_list_data_row']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row');
            $data['badiu_list_data_rows']=$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows');
          }
         
        return $data;
        
    }

  
   private function sendReport() {
       $page=$this->page;
        $container=$this->getContainer();
        $router=$this->getContainer()->get("router");
         $contents=null;
        $app=$this->getContainer()->get('badiu.system.core.lib.util.app'); 
        $result=array();
        $result['resultinfo']['datashouldexec']=1;
        $maxrow=$this->maxrowperconnection;
        $maxrow=15000;
        $paginindex=0;
      
        $data=$this->getContent($paginindex,$maxrow,array('fulldatawithutopaging'=>1));
        $page->addData('badiu_list_data_row', $this->getUtildata()->getVaueOfArray($data,'badiu_list_data_row'));
        $page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows'));
        $page->addData('badiu_table1',$this->getUtildata()->getVaueOfArray($data,'badiu_table1'));

        if($this->layout=='indexCrud'){
            $table=$this->getUtildata()->getVaueOfArray($data,'badiu_table1');
            $reportcountrows=count($table->getRow()->getList());
            
            $fileprocess=$page->getConfig()->getFileprocessindexscheduler();
            $file=$app->getFilePath($fileprocess,false);
           
            if(!file_exists($file)){return "file $fileprocess | $file not found"; }
          
            ob_start();
             include($file);
             $contents = ob_get_contents();
            ob_end_clean();
            
            $recipientparam=array();
            
            //add page data
           
            

            $recipientparam['page']=$page;
            $recipientparam['paramfilter']=$this->param;
            
             $mconfig= $this->getTaskdto()->getReportmessage();
             $mconfig=$this->getJson()->decode($mconfig,true);
            
             $recipientparam['reportmessage']=$mconfig;
             $recipientparam['entity']=$this->getTaskdto()->getEntity();

             $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
             if(empty($smtpservice)){$smtpservice='badiunetconfig';}
              
            
             $sendonlyreportwithrecord=$this->getUtildata()->getVaueOfArray($mconfig,'sendonlyreportwithrecord');
             if($sendonlyreportwithrecord==null || $sendonlyreportwithrecord==''){$sendonlyreportwithrecord=1;}
             $sendmail=true;
             if($sendonlyreportwithrecord==1 && $reportcountrows==0){ $sendmail=false;}
             
             $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
             $customcontent.="<br />";
             $customcontent.=$contents;
             $mconfig['message']['body']=$customcontent;
             
            
             $execsendmail=false;
             $emailsto=$mconfig['recipient']['to'];
             if($smtpservice=='badiunetconfig'){
                $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
                $smtp->setUsersender($this->getUsersender());
                $entity=$this->getTaskdto()->getEntity();
                $smtp->setEntity($entity); 
               
                if($sendmail && !empty($emailsto)){
                    $excelexport=$this->getContainer()->get('badiu.system.core.lib.report.excelexport');
                    $excelexport->setTable($table);
                    $attach=$excelexport->attachToMail();
                    $mconfig['attach'][0]=$attach; 
                    $execsendmail=$smtp->sendMail($mconfig);
                }
                else{$execsendmail=-1;}
             }else if ($smtpservice=='moodleconfig'){
                $reportfilter=$this->getTaskdto()->getReportfilter();
                $reportfilter=$this->getJson()->decode($reportfilter,true);
                $parentid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter.parentid',true);
                $smtpserviceparam=array(); 
                $smtpserviceparam['_serviceid']=$parentid;
                $smtpserviceparam['mail']['subject']=$this->getUtildata()->getVaueOfArray($mconfig,'message.subject',true);
                $smtpserviceparam['mail']['message']=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
                $smtpserviceparam['mail']['to']=$emailsto;
                if($sendmail && !empty($emailsto)){
                    $mdlsmtpservice=$this->getContainer()->get('badiu.moodle.mreport.lib.smtpservice');
                    $mdlsmtpservice->setSessionhashkey($this->getSessionhashkey());
                    $smtpserviceparam=$mdlsmtpservice->castListEmail($smtpserviceparam);
                    $resultmdlsms=$mdlsmtpservice->exec($smtpserviceparam);
                    $emailsendto=$mdlsmtpservice->getFirstEmail($emailsto);
                    $execsendmail=$mdlsmtpservice->checkResponse($resultmdlsms,$emailsendto);
                 }
             }
             
             
             $result['resultinfo']['dataexec']=$execsendmail;
             $result['resultinfo']['reportrowsize']=$reportcountrows; //review with pagination

             //send to system recipient
           
             $recipientparam['resultinfo']=$result;
             $recipientparam['sendmail']=$sendmail;
             $recipientparam['layout']='indexCrud';
             $recipientparam['formatedcontent']=$customcontent;
             $recipientparam['usersender']=$this->getUsersender();
             $recipientparam['smtpservice']=$smtpservice;
             //process system recipient
             $result= $this->execSystemRecipient($recipientparam);
             return $result;
             
        
          
        }
     
        return null;
    }
    
   
      
      function sendMessageToEachUserOfReport(){
         
             $mconfig= $this->getTaskdto()->getReportmessage();
             $mconfig=$this->getJson()->decode($mconfig,true);
             $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
             $customcsubject=$this->getUtildata()->getVaueOfArray($mconfig,'message.subject',true);

             $columnemail=$this->page->getConfig()->getSchedulermessageemailcolumn();
             $columnuser=$this->page->getConfig()->getSchedulermessageusercolumn();
             
             $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
             if(empty($smtpservice)){$smtpservice='badiunetconfig';}
            
             $mdlsmtpservice=$this->getContainer()->get('badiu.moodle.mreport.lib.smtpservice');
             $mdlsmtpservice->setSessionhashkey($this->getSessionhashkey());
             $maxrow=$this->maxrowperconnection;
            $maxrow=15000;
            $paginindex=0;
            $data=$this->getContent($paginindex,$maxrow);

             $table=$this->getUtildata()->getVaueOfArray($data,'badiu_table1');
             $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
             $smtp->setUsersender($this->getUsersender());
             $entity=$this->getTaskdto()->getEntity();
             $smtp->setEntity($entity);
             
            
             $contshouldprocess=0;
             $contprocess=0;
             $listusersend=array();
             $result=array();
             $result['resultinfo']['datashouldexec']=count($table->getRow()->getList());
             
        foreach ($table->getRow()->getList() as $row) {
                   $contshouldprocess++;
                   $email=$this->getUtildata()->getVaueOfArray($row,$columnemail);
                   if(!empty($email)){$email=strip_tags($email);}
                   
                   $user=$this->getUtildata()->getVaueOfArray($row,$columnuser);
                   if(!empty($user)){$user=strip_tags($user);}
                
                   $reportfilter=$this->getTaskdto()->getReportfilter();
                    $reportfilter=$this->getJson()->decode($reportfilter,true);
                    $parentid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter.parentid',true);

                    
                   $content=$this->processExpression($customcontent,$row);
                   $subject=$this->processExpression($customcsubject,$row);
                  
                   if($smtpservice=='badiunetconfig'){
                        $mconfig['message']['subject']= $subject;
                        $mconfig['message']['body']= $content;
                        $mconfig['recipient']['to']=$email;
                        if(!empty($email)){
                            $send=$smtp->sendMail($mconfig);
                            if($send){ 
                                $contprocess++;
                                $usersp=array('email'=>$email,'user'=>$user);
                                $usersplog=array('email'=>$email,'user'=>$user,'subject'=>$subject,'content'=>$content);
                                $this->saveReportOfUserNotified($usersplog);
                                array_push($listusersend,$usersp);
                            }
                        }
                   }else if ($smtpservice=='moodleconfig'){
                    
                        $smtpserviceparam=array(); 
                        $smtpserviceparam['_serviceid']=$parentid;
                        $smtpserviceparam['mail']['subject']=$subject;
                        $smtpserviceparam['mail']['message']= $content;
                        $smtpserviceparam['mail']['to']=array('email'=>$email,'name'=>$user);
                        if(!empty($email)){
                            $resultmdlsms=$mdlsmtpservice->exec($smtpserviceparam);
                            $send=$mdlsmtpservice->checkResponse($resultmdlsms,$email);
                            if($send){
                                $contprocess++;
                                $usersp=array('email'=>$email,'user'=>$user);
                                $usersplog=array('email'=>$email,'user'=>$user,'subject'=>$subject,'content'=>$content);
                                $this->saveReportOfUserNotified($usersplog);
                                array_push($listusersend,$usersp);
                            }
                     }
                   }
                  
                   
           
             }
             $result['resultinfo']['dataexec']=$contprocess;
             $result['smtpservice']=$smtpservice;
            // $this->saveReportOfUsersNotified($listusersend);
             $this->sendReportOfUserNotified($listusersend);
          
         return $result; 
      }

      
     /* function sendMessageToEachUserOfReport(){
         
        $mconfig= $this->getTaskdto()->getReportmessage();
        $mconfig=$this->getJson()->decode($mconfig,true);
        $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
        $customcsubject=$this->getUtildata()->getVaueOfArray($mconfig,'message.subject',true);

        $columnemail=$this->page->getConfig()->getSchedulermessageemailcolumn();
        $columnuser=$this->page->getConfig()->getSchedulermessageusercolumn();
        
        $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
        if(empty($smtpservice)){$smtpservice='badiunetconfig';}
       
        $mdlsmtpservice=$this->getContainer()->get('badiu.moodle.mreport.lib.smtpservice');
        $mdlsmtpservice->setSessionhashkey($this->getSessionhashkey());
        
        $maxrowperconnection=$this->maxrowperconnection;
        $maxrowtoexec=$this->maxrowperprocess;
        $subprocess=0;
        $pagingindex=0;
        $countprocessed=0;

        $data=$this->getContent($pagingindex,$maxrowperconnection,array('partialdata'=>'countrows'));
        $countrecord=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_countrows');
       
        if($countrecord > $maxrowperconnection){$subprocess=$countrecord/$maxrowperconnection;}
        if($subprocess != round($subprocess)){
            $subprocess=floor($subprocess)+1;
        }
       if($subprocess == 0){$subprocess=1;}
        if($subprocess > 0){
            
            for ($i = 0; $i < $subprocess; $i++) {
                $idata=$this->getContent($i,$maxrowperconnection,array('partialdata'=>'getrows'));
                $icount=0;
                if(is_array($idata)){
                    $icount=sizeof($idata);
                    $countprocessed=$countprocessed+$icount;
                    //process here
                    $plistuser=$this->getListUserForNotifyInReport($idata);
                    $this->sendMessageToEachUserOfReportPartial($listusers);
                }
                if($icount==0 || $countprocessed>=$maxrowtoexec){break;}
            }
        }

        $table=$this->getUtildata()->getVaueOfArray($data,'badiu_table1');
        $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
        $smtp->setUsersender($this->getUsersender());
        $entity=$this->getTaskdto()->getEntity();
        $smtp->setEntity($entity);
        
       
        $contshouldprocess=0;
        $contprocess=0;
        $listusersend=array();
        $result=array();
        $result['resultinfo']['datashouldexec']=count($table->getRow()->getList());
        
   foreach ($table->getRow()->getList() as $row) {
              $contshouldprocess++;
              $email=$this->getUtildata()->getVaueOfArray($row,$columnemail);
              if(!empty($email)){$email=strip_tags($email);}
              
              $user=$this->getUtildata()->getVaueOfArray($row,$columnuser);
              if(!empty($user)){$user=strip_tags($user);}
           
              $reportfilter=$this->getTaskdto()->getReportfilter();
               $reportfilter=$this->getJson()->decode($reportfilter,true);
               $parentid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter.parentid',true);

               
              $content=$this->processExpression($customcontent,$row);
              $subject=$this->processExpression($customcsubject,$row);
             
              if($smtpservice=='badiunetconfig'){
                   $mconfig['message']['subject']= $subject;
                   $mconfig['message']['body']= $content;
                   $mconfig['recipient']['to']=$email;
                   if(!empty($email)){
                       $send=$smtp->sendMail($mconfig);
                       if($send){ 
                           $contprocess++;
                           $usersp=array('email'=>$email,'user'=>$user);
                           $usersplog=array('email'=>$email,'user'=>$user,'subject'=>$subject,'content'=>$content);
                           $this->saveReportOfUserNotified($usersplog);
                           array_push($listusersend,$usersp);
                       }
                   }
              }else if ($smtpservice=='moodleconfig'){
               
                   $smtpserviceparam=array(); 
                   $smtpserviceparam['_serviceid']=$parentid;
                   $smtpserviceparam['mail']['subject']=$subject;
                   $smtpserviceparam['mail']['message']= $content;
                   $smtpserviceparam['mail']['to']=array('email'=>$email,'name'=>$user);
                   if(!empty($email)){
                       $resultmdlsms=$mdlsmtpservice->exec($smtpserviceparam);
                       $send=$mdlsmtpservice->checkResponse($resultmdlsms,$email);
                       if($send){
                           $contprocess++;
                           $usersp=array('email'=>$email,'user'=>$user);
                           $usersplog=array('email'=>$email,'user'=>$user,'subject'=>$subject,'content'=>$content);
                           $this->saveReportOfUserNotified($usersplog);
                           array_push($listusersend,$usersp);
                       }
                }
              }
             
              
      
        }
        $result['resultinfo']['dataexec']=$contprocess;
        $result['smtpservice']=$smtpservice;
       // $this->saveReportOfUsersNotified($listusersend);
        $this->sendReportOfUserNotified($listusersend);
     
    return $result; 
 }*/
    function getListUserForNotifyInReport($data){
        $table=$this->getUtildata()->getVaueOfArray($data,'badiu_table1');
        $newlist=array();
        if(empty($table)){return $newlist;}
        $columnemail=$this->page->getConfig()->getSchedulermessageemailcolumn();
        $columnuser=$this->page->getConfig()->getSchedulermessageusercolumn();

        foreach ($table->getRow()->getList() as $row) {
            $email=$this->getUtildata()->getVaueOfArray($row,$columnemail);
            if(!empty($email)){$email=strip_tags($email);}
            
            $user=$this->getUtildata()->getVaueOfArray($row,$columnuser);
            if(!empty($user)){$user=strip_tags($user);}

            $iuser=array('email'=>$email,'user'=>$user);
            array_push($newlist,$iuser);
        }
        return $newlist;
    }
    function sendMessageToEachUserOfReportPartial($listusers){

    }
      function saveReportOfUsersNotified($listuser){
          $lmdata=$this->getContainer()->get('badiu.system.scheduler.tasklogmessage.data');
          foreach ($listuser as $row) {
               $email=$this->getUtildata()->getVaueOfArray($row,'email');
               $user=$this->getUtildata()->getVaueOfArray($row,'user');
               $param=array();
               $param['entity']=$this->getTaskdto()->getEntity();
               //$param['name']=null; //db shoud remove
               $param['userrecipient']=$user;
               $param['usercontact']=$email;
               $param['dtype']='mailsendtouserreporterfilter';
               $param['tasklogid']= $this->getTasklogid();
               $param['timecreated']=new \DateTime();
               $lmdata->insertNativeSql($param,false);
               
          }
      }

      function saveReportOfUserNotified($row){
            $lmdata=$this->getContainer()->get('badiu.system.scheduler.tasklogmessage.data');
            $email=$this->getUtildata()->getVaueOfArray($row,'email');
             $user=$this->getUtildata()->getVaueOfArray($row,'user');
             $subject=$this->getUtildata()->getVaueOfArray($row,'subject');
             $content=$this->getUtildata()->getVaueOfArray($row,'content');
             $param=array();
             $param['entity']=$this->getTaskdto()->getEntity();
             //$param['name']=null; //db shoud remove
             $param['userrecipient']=$user;
             $param['usercontact']=$email;
             $param['msgsubject']=$subject;
             $param['msgcontent']=$content;
             $param['dtype']='mailsendtouserreporterfilter';
             $param['tasklogid']= $this->getTasklogid();
             $param['timecreated']=new \DateTime();
            $result=$lmdata->insertNativeSql($param,false);
            
            return  $result;
       
    }
      function sendReportOfUserNotified($listuser){
          $reportcountrows=count($listuser);
          $mconfig= $this->getTaskdto()->getReportmessage();
          $mconfig=$this->getJson()->decode($mconfig,true);
          
          $sendonlyreportwithrecord=$this->getUtildata()->getVaueOfArray($mconfig,'sendonlyreportwithrecord');
          if($sendonlyreportwithrecord==null || $sendonlyreportwithrecord==''){$sendonlyreportwithrecord=1;}
          if($sendonlyreportwithrecord==1 && $reportcountrows==0){return null;}
          
          $sendto=$this->getUtildata()->getVaueOfArray($mconfig,'recipient.to',true);
          if(empty($sendto)){return null;}
          
          $table=$this->getContainer()->get('badiu.system.core.lib.report.table');
           $app=$this->getContainer()->get('badiu.system.core.lib.util.app'); 
           $container=$this->getContainer();
           $router=$this->getContainer()->get("router");
          $table->getColumn()->add('user', $this->getTranslator()->trans('user'), null);
          $table->getColumn()->add('email', $this->getTranslator()->trans('email'), null);
           foreach ($listuser as $lu) {
              $table->getRow()->add($lu);
          }
         
                                         
          $this->page->addData('badiu_table1',$table);
	  $fileprocess=$this->page->getConfig()->getFileprocessindexscheduler();
          $page=$this->page;
          $file=$app->getFilePath($fileprocess,false);
	  $contents=null;
	  ob_start();
             include_once($file);
             $contents = ob_get_contents();
          ob_end_clean();
	  
	 
        
          $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
         $customcontent.="<br />";
         $customcontent.=$contents;
         $mconfig['message']['body']=$customcontent;
         $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
        $smtp->setUsersender($this->getUsersender());
         $entity=$this->getTaskdto()->getEntity(); 
         $smtp->setEntity($entity);
         $sendmsg=$smtp->sendMail($mconfig);
         return $sendmsg;
      }

      function initUsersendergetKey($reportfilter) {
        $key=$this->getUtildata()->getVaueOfArray($reportfilter,'config.router',true);
        $dkey=$this->getUtildata()->getVaueOfArray($reportfilter,'filter._dkey',true);
        if(!empty($dkey)){$key=$dkey;}
        $parentid=$this->getUtildata()->getVaueOfArray($reportfilter,'filter.parentid',true);
        $pos=strpos($key,'badiu.moodle.mreport.');
        
        if($pos !== FALSE && $pos==0 && !empty($parentid)){
            $servicedata = $this->getContainer()->get('badiu.admin.server.service.data');
           $servicename= $servicedata->getGlobalColumnValue('name',array('id'=>$parentid));
           $this->setUsersender($servicename);
        }
      }

      
      function processExpression($message,$data) {
          
        $serviceexprekey=$this->kminherit->schedulerexpression();
        if(!$this->getContainer()->has($serviceexprekey)){
            $serviceexprekey=$this->kminherit->schedulerexpressionparent();
            if(!$this->getContainer()->has($serviceexprekey)){
                $serviceexprekey=$this->kminherit->schedulerexpressionparent(2);
               if(!$this->getContainer()->has($serviceexprekey)){
                $serviceexprekey=$this->kminherit->schedulerexpressionparent(3);
                  if(!$this->getContainer()->has($serviceexprekey)){return $message;}
                }
             }
        }
        if(!$this->getContainer()->has($serviceexprekey)){return $message;}

                
        $serviceexpredata=$this->getContainer()->get($serviceexprekey);
        if(!method_exists($serviceexpredata, 'init')){return $message;}
        if(!method_exists($serviceexpredata, 'replace')){return $message;}
        if(!method_exists($serviceexpredata, 'setSessionhashkey')){return $message;}
        $serviceexpredata->setSessionhashkey($this->getSessionhashkey());
        $serviceexpredata->init($message,$data);
        $parentid=$this->getUtildata()->getVaueOfArray($this->param,'parentid');
        $serviceexpredata->setParentid($parentid);
        $serviceexpredata->changeData();
        $serviceexpredata->replace();
        $message= $serviceexpredata->getMessage();

      
        return  $message;
  }

    function execSystemRecipient($param) {
        $resultinfo=$this->getUtildata()->getVaueOfArray($param,'resultinfo');
        $sendmail=$this->getUtildata()->getVaueOfArray($param,'sendmail');

        if(!$sendmail){return $resultinfo;}
       
        $recipientparam=$this->page->getConfig()->getSchedulersystemrecipient();
        if(empty($recipientparam)){return $resultinfo;}

        $subQueryString = $this->getContainer()->get('badiu.system.core.lib.http.subquerystring');
		$subQueryString->setQuery($recipientparam);
		$subQueryString->makeParam();

        $service=$subQueryString->getValue('service');
        if(empty($service)){return $resultinfo;}

        $columnkey=$subQueryString->getValue('columnkey');
       if(empty($columnkey)){return $resultinfo;}

        if(!$this->getContainer()->has($service)){return $resultinfo;}
        $service=$this->getContainer()->get($service);
        $service->setSessionhashkey($this->getSessionhashkey());
        $param['columnkey']=$columnkey;

        $service->init($param);
        $columnkeyvalue=$this->getUtildata()->getVaueOfArray($this->param,$columnkey);

        //if report has only one key value of columnkey
        if(!empty($columnkeyvalue)){
            $resultwsplt=$service->sendMessageWihtouSplit();
            return $resultwsplt;
           }else{
            //if report has multiple  key value of columnkey
            $resultsplt=$service->sendMessageSplited();
            return $resultsplt;
        }
        
        return $resultinfo;
    }


    public function exportTodatabase(){
        $subprocessstatus=$this->getTaskdto()->getSubprocessstatus();
    
        $result=null;
        if($subprocessstatus=='none' || $subprocessstatus=='completedcontinue'){ 
           $result= $this->exportTodatabaseWithStatusStart();
        }else if($subprocessstatus=='incomplete'){
            $result= $this->exportTodatabaseWithStatusIncomplete();
        }
        return $result;
    }

    public function exportTodatabaseWithStatusStart(){
        
        $typedbexport=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.type',true);
        if(empty($typedbexport)){ $typedbexport='legacydata';} //legacydata | incrementaldata

        $maxrow=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.maxrowperconnection',true);
        if(empty($maxrow)){ $maxrow=$this->getMaxrowperconnection();}
       
        $maxrowtoexec=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.maxrowperprocess',true);
        if(empty($maxrowtoexec)){ $maxrow=$this->getMaxrowperprocess();}
       
        $maxerror=5;
        $pagingindex=0;
        $data=$this->getContent($pagingindex,$maxrow,array('partialdata'=>'countrows'));
        $countrecord=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_countrows');
       
        $subprocess=0;
        $datashouldexecinlastpaging=$maxrowtoexec;
        if($countrecord > $maxrow){$subprocess=$countrecord/$maxrow;}
        if($subprocess != round($subprocess)){
            $subprocess=floor($subprocess)+1;
            $datashouldexecinlastpaging=$countrecord%$maxrow;
        }

        $tasklogid=$this->getTasklogid();
        $entity=$this->getTaskdto()->getEntity();
        $taskid=$this->getTaskdto()->getId();

        $tasksubprocessdata=$this->getContainer()->get('badiu.system.scheduler.tasksubprocess.data');
        $tasklogdata=$this->getContainer()->get('badiu.system.scheduler.tasklog.data');
        $taskdata=$this->getContainer()->get('badiu.system.scheduler.task.data');
        
        if($subprocess > 0){
            
            //add subprocess
            $maxrowperconnection=$this->getMaxrowperconnection();
            $continsert=0;
            $seq=1;
            
            for ($i = 0; $i < $subprocess; $i++) {
                   if($seq==$subprocess){$maxrowperconnection= $datashouldexecinlastpaging;}
                    $parami=array('entity'=>$entity,'taskid'=>$taskid,'tasklogid'=>$tasklogid,'status'=>'programmed','pagingindex'=>$i,'datashouldexec'=>$maxrowperconnection,'dtype'=>'automatic','timecreated'=>new \DateTime());
                    $result=$tasksubprocessdata->insertNativeSql($parami,false);
                   if($result){$continsert++;}
                   $seq++;
            }
           
            //update task status of subprocess
            $paramtakup=array('id'=>$taskid,'subprocessstatus'=>'processing','subprocessprogress'=>0);
            $resulttup=$taskdata->updateNativeSql($paramtakup,false);
            
            $paramtaklogup=array('id'=>$tasklogid,'subprocessstatus'=>'processing','subprocessprogress'=>0);
            $resultltup=$tasklogdata->updateNativeSql($paramtakup,false);
           
            //get list subprocess
            $listsubprocess=$tasksubprocessdata->getListToExec($taskid,'programmed');
            
            //exec each subprocess
            $coundataexec=0;
            $countsubprocessexec=0;
            $countsubprocess=0;
            $couterror=0;
            $countdatashouldexec=0;
            foreach ($listsubprocess as $sprow) {
                $pagingindex=$this->getUtildata()->getVaueOfArray($sprow,'pagingindex');
                $subprocessid=$this->getUtildata()->getVaueOfArray($sprow,'id');
                $datashouldexec=$this->getUtildata()->getVaueOfArray($sprow,'datashouldexec');
                if($countsubprocess==0){$maxrow=$datashouldexec;}
                $countdatashouldexec+=$datashouldexec;
                $timestart=new \DateTime();
                $parampi=array('id'=>$subprocessid,'status'=>'processing','timestart'=>$timestart);
                $tasksubprocessdata->updateNativeSql($parampi,false);

                //process 
                $cparam=array('partialdata'=>'getrows');
                $precessresutl=$this->exportTodatabaseProcessData($pagingindex,$maxrow,$cparam);

                $resultstatus=$this->getUtildata()->getVaueOfArray($precessresutl,'status');
                $resultprocessed=$this->getUtildata()->getVaueOfArray($precessresutl,'message');

                $timeend=new \DateTime();
                $timeexec=$timeend->getTimestamp()-$timestart->getTimestamp();
                $parampi=array('id'=>$subprocessid,'status'=>'success','timeend'=>$timeend,'timeexec'=>$timeexec,'timemodified'=>new \DateTime());
               
                if($resultstatus=='accept'){
                    $parampi['dataexec']=$resultprocessed;
                    $tasksubprocessdata->updateNativeSql($parampi,false);
                    $coundataexec+=$resultprocessed;
                    $countsubprocessexec++;
                    if( $coundataexec >=$maxrowtoexec){break;}
                }else{
                    $parampi['status']='failed';
                    $param=array();
                    $param['error']=$resultstatus;
                    $param= $this->getJson()->encode($param);
                    $parampi['param']=$param;
                    $tasksubprocessdata->updateNativeSql($parampi,false);
                    $couterror++;
                    if($couterror>=$maxerror){break;}
                }
                $countsubprocess++;
           
            }

            $totalsubprocess=$tasksubprocessdata->count($taskid);
            $totalsubprocessexeccussess=$tasksubprocessdata->count($taskid,'success');

            $taskpercent=0;
            if($totalsubprocess > 0 && $totalsubprocessexeccussess >0){
                $taskpercent=$totalsubprocessexeccussess * 100 / $totalsubprocess;;
             }
            $taskprocesspercent=0;
            if($totalsubprocess > 0 && $countsubprocess >0){
                $taskprocesspercent=$countsubprocess * 100 / $totalsubprocess;;
            }

            //update process
            $paramtaklogup=array('id'=>$tasklogid,'subprocessstatus'=>'completed','subprocessprogress'=>$taskprocesspercent);
            $resulttlup=$tasklogdata->updateNativeSql($paramtaklogup,false);
            //update task
            $subprocessstatus='incomplete';
            if($taskpercent==100){
                $subprocessstatus='completed';
                if( $typedbexport=='incremental'){$subprocessstatus='completedcontinue';}
            }
            $paramtakup=array('id'=>$taskid,'subprocessstatus'=>$subprocessstatus,'subprocessprogress'=>$taskpercent);
            $result=$taskdata->updateNativeSql($paramtakup,false);

            $processresult=array();
            $processresult['resultinfo']['datashouldexec']=$countdatashouldexec;
            $processresult['resultinfo']['dataexec']=$coundataexec;
            return $processresult;
        }else{
             
            $cparam=array('partialdata'=>'getrows');
            $precessresutl=$this->exportTodatabaseProcessData($pagingindex,$maxrow,$cparam);
            $resultstatus=$this->getUtildata()->getVaueOfArray($precessresutl,'status');
            $resultprocessed=$this->getUtildata()->getVaueOfArray($precessresutl,'message');

            if($resultstatus=='accept'){ 
                $processresult=array();
                $processresult['resultinfo']['datashouldexec']=$countrecord;
                $processresult['resultinfo']['dataexec']=$resultprocessed;
                return  $processresult;
            }else{
                //errro register
                exit;
            }
        }
    }

    public function exportTodatabaseWithStatusIncomplete(){
        $maxrow=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.maxrowperconnection',true);
        if(empty($maxrow)){ $maxrow=$this->getMaxrowperconnection();}
       
        $maxrowtoexec=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.maxrowperprocess',true);
        if(empty($maxrowtoexec)){ $maxrow=$this->getMaxrowperprocess();}
       
        $maxerror=5;
        $paginindex=0;
        
        $tasklogid=$this->getTasklogid();
        $entity=$this->getTaskdto()->getEntity();
        $taskid=$this->getTaskdto()->getId();

        $tasksubprocessdata=$this->getContainer()->get('badiu.system.scheduler.tasksubprocess.data');
        $tasklogdata=$this->getContainer()->get('badiu.system.scheduler.tasklog.data');
        $taskdata=$this->getContainer()->get('badiu.system.scheduler.task.data');
       
        
            
        //update task status of subprocess
        $paramtakup=array('id'=>$taskid,'subprocessstatus'=>'processing');
        $resulttup=$taskdata->updateNativeSql($paramtakup,false);
            
        $paramtaklogup=array('id'=>$tasklogid,'subprocessstatus'=>'processing','subprocessprogress'=>0);
        $resultltup=$tasklogdata->updateNativeSql($paramtakup,false);

        //get list subprocess
        $listsubprocess=$tasksubprocessdata->getListToExec($taskid,'programmed');
            
        //exec each subprocess
        $coundataexec=0;
        $countsubprocessexec=0;
        $countsubprocess=0;
        $couterror=0;
        $countdatashouldexec=0;
        foreach ($listsubprocess as $sprow) {
                $pagingindex=$this->getUtildata()->getVaueOfArray($sprow,'pagingindex');
                $subprocessid=$this->getUtildata()->getVaueOfArray($sprow,'id');
                $datashouldexec=$this->getUtildata()->getVaueOfArray($sprow,'datashouldexec');
                if($countsubprocess==0){$maxrow=$datashouldexec;}
                $countdatashouldexec+=$datashouldexec;
                $timestart=new \DateTime();
                $parampi=array('id'=>$subprocessid,'status'=>'processing','timestart'=>$timestart);
                $tasksubprocessdata->updateNativeSql($parampi,false);

                //process 
                $cparam=array('partialdata'=>'getrows');
                $precessresutl=$this->exportTodatabaseProcessData($pagingindex,$maxrow,$cparam);

                $resultstatus=$this->getUtildata()->getVaueOfArray($precessresutl,'status');
                $resultprocessed=$this->getUtildata()->getVaueOfArray($precessresutl,'message');

                $timeend=new \DateTime();
                $timeexec=$timeend->getTimestamp()-$timestart->getTimestamp();
                $parampi=array('id'=>$subprocessid,'status'=>'success','timeend'=>$timeend,'timeexec'=>$timeexec,'timemodified'=>new \DateTime());
               
                if($resultstatus=='accept'){
                    $parampi['dataexec']=$resultprocessed;
                    $tasksubprocessdata->updateNativeSql($parampi,false);
                    $coundataexec+=$resultprocessed;
                    $countsubprocessexec++;
                    if( $coundataexec >=$maxrowtoexec){break;}
                }else{
                    $parampi['status']='failed';
                    $param=array();
                    $param['error']=$resultstatus;
                    $param= $this->getJson()->encode($param);
                    $parampi['param']=$param;
                    $tasksubprocessdata->updateNativeSql($parampi,false);
                    $couterror++;
                    if($couterror>=$maxerror){break;}
                }
                $countsubprocess++;
           
        }

            $totalsubprocess=$tasksubprocessdata->count($taskid);
            $totalsubprocessexeccussess=$tasksubprocessdata->count($taskid,'success');

            $taskpercent=0;
            if($totalsubprocess > 0 && $totalsubprocessexeccussess >0){
                $taskpercent=$totalsubprocessexeccussess * 100 / $totalsubprocess;;
             }
            $taskprocesspercent=0;
            if($totalsubprocess > 0 && $countsubprocess >0){
                $taskprocesspercent=$countsubprocess * 100 / $totalsubprocess;;
            }

            //update process
            $paramtaklogup=array('id'=>$tasklogid,'subprocessstatus'=>'completed','subprocessprogress'=>$taskprocesspercent);
            $resulttlup=$tasklogdata->updateNativeSql($paramtaklogup,false);
            //update task
            $subprocessstatus='incomplete';
            if($taskpercent==100){$subprocessstatus='completed';}
            $paramtakup=array('id'=>$taskid,'subprocessstatus'=>$subprocessstatus,'subprocessprogress'=>$taskpercent);
            $result=$taskdata->updateNativeSql($paramtakup,false);

            $processresult=array();
            $processresult['resultinfo']['datashouldexec']=$countdatashouldexec;
            $processresult['resultinfo']['dataexec']=$coundataexec;
            return $processresult;
       
    }

   
    public function exportTodatabaseProcessData($paginindex,$maxrow,$cparam){
        $data=$this->getContent($paginindex,$maxrow,$cparam);
        $table=$this->getUtildata()->getVaueOfArray($data,'badiu_table1');
        $sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
        $sqlservice->setSessionhashkey($this->getSessionhashkey());
        $moodleidtarget=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.targetconfig.systeminstance',true);
        $tabletarget=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.targetconfig.table',true);
        $servicexportexec=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.targetconfig.service',true);
        $synckey=$this->getUtildata()->getVaueOfArray($this->dconfig,'exporttodatabase.targetconfig.synckey',true);
        $synckey=$this->castUniquekeyToArray($synckey);
        if(empty($servicexportexec)){$servicexportexec='badiu.moodle.mreport.lib.generalexportdbmoodlesummarize';}
        $param['_serviceid']=$moodleidtarget;
        $param['_serviceidsource']=$this->getUtildata()->getVaueOfArray($this->param,'_serviceid');
        $param['_key']='local.badiunet.mdata.dbfactory.addorupdaterows';
        $param['mdata']['tbl']=$tabletarget;
        $param['mdata']['ku']= $synckey;//array(array('key'=>'moodleid'),array('key'=>'userid'),array('key'=>'courseid'),array('key'=>'activityid'),array('key'=>'customint2'),array('key'=>'responseid'));
        $param['mdata']['rows']=$table->getRow()->getList();
        $generalexportdbmoodlesummarize=$this->getContainer()->get($servicexportexec);
        $generalexportdbmoodlesummarize->setSessionhashkey($this->getSessionhashkey());
        $param=$generalexportdbmoodlesummarize->changedata($param);
       
        $result=$sqlservice->service($param);
        return $result;
    }


    public function castUniquekeyToArray($paramuk){
        $luk=null;
        if(empty($paramuk)){return null;}
        $txtuk=str_replace(";",",",$paramuk);
        if(!empty($txtuk)){$txtuk=str_replace(' ','', $txtuk);}
        $pos=stripos($txtuk, ",");
        if($pos=== false){
                $luk=array($txtuk);
        }else{
                $luk= explode(",",$txtuk);
        }
        $listkey=array();
        foreach ($luk as $k) {
            array_push( $listkey,array('key'=>$k));
           
        }
        return $listkey;
    }


    public function moodleBackupRestoreCourse(){
        $service=$this->getUtildata()->getVaueOfArray($this->dconfig,'moodlebackuprestorecourse.service',true);
        $moodleidtarget=$this->getUtildata()->getVaueOfArray($this->dconfig,'moodlebackuprestorecourse.moodleidtarget',true);
        $coursecategorytarget=$this->getUtildata()->getVaueOfArray($this->dconfig,'moodlebackuprestorecourse.coursecategorytarget',true);
        $includeuserinteraction=$this->getUtildata()->getVaueOfArray($this->dconfig,'moodlebackuprestorecourse.includeuserinteraction',true);
        $setcoursetimestart=$this->getUtildata()->getVaueOfArray($this->dconfig,'moodlebackuprestorecourse.setcoursetimestart',true);

        $moodleidsourece=$this->getUtildata()->getVaueOfArray($this->param,'_serviceid');
       
        $maxrow=$this->getMaxrowperconnection();
        $maxrowtoexec=$this->getMaxrowperprocess();
        $pagingindex=0;
        $datacount=$this->getContent($pagingindex,$maxrow,array('partialdata'=>'countrows'));
        $countrecord=$this->getUtildata()->getVaueOfArray($datacount,'badiu_list_data_countrows');
      
        $subprocess=0;
        $srows=array();
        $cparam=array('partialdata'=>'getrows','dataout'=>'withouttable');
        if($countrecord > $maxrow){$subprocess=$countrecord/$maxrow;}
        if($subprocess != round($subprocess)){ $subprocess=floor($subprocess)+1;}
        if($subprocess == 0){$subprocess=1;}
        $countprocessed=0;
       
        for ($i = 0; $i < $subprocess; $i++) {
           $irows=$this->getContent($i,$maxrow,$cparam);
             $icount=0;
            if(is_array($irows)){
                foreach ($irows as $irow) {array_push($srows,$irow);$icount++;$countprocessed++;}
            }
            if($icount==0 || $countprocessed>=$maxrowtoexec){break;}
        }
        
        $pservice=null;
        if($this->getContainer()->has($service)){
            $pservice=$this->getContainer()->get($service);
            $pservice->setSessionhashkey($this->getSessionhashkey());
        }else{
            //register failure
            return null;
        }
        
        foreach ($srows as $srow) {
            $courseid=$this->getUtildata()->getVaueOfArray($srow,'courseid');
            if(empty($courseid)){$courseid=$this->getUtildata()->getVaueOfArray($srow,'id');}
            $bckparam=array('courseid'=>$courseid,'sserviceid'=> $moodleidsourece,'includeuserinteraction'=>$includeuserinteraction);
            $result =$pservice->makeBackup($bckparam);
            echo "<hr />";
            echo "remote result:";
            print_r($result);
            echo "<hr />===============================";
        }
       // echo "<pre>";
       /* print_r($this->dconfig);
        echo "<hr>";
        print_r($this->param);
        echo "<hr>";
        print_r($srows);
        echo "</pre>";*/
        echo "<BR />******************b1*******************";exit;
        return null;
    }

        function getKey() {
            return $this->key;
        }

        function getBkey() {
            return $this->bkey;
        }

        function getLayout() {
            return $this->layout;
        }

        function getKminherit() {
            return $this->kminherit;
        }

        function getParam() {
            return $this->param;
        }

        function getPage() {
            return $this->page;
        }

        function setKey($key) {
            $this->key = $key;
        }

        function setBkey($bkey) {
            $this->bkey = $bkey;
        }

        function setLayout($layout) {
            $this->layout = $layout;
        }

        function setKminherit($kminherit) {
            $this->kminherit = $kminherit;
        }

        function setParam($param) {
            $this->param = $param;
        }

        function setPage($page) {
            $this->page = $page;
        }

        function getAction() {
            return $this->action;
        }

        function setAction($action) {
            $this->action = $action;
        }

      

        function getUsersender() {
            return $this->usersender;
        }
    
        function setUsersender($usersender) {
            $this->usersender = $usersender;
        }

        function getMaxrowperprocess() {
            return $this->maxrowperprocess;
        }
    
        function setMaxrowperprocess($maxrowperprocess) {
            $this->maxrowperprocess = $maxrowperprocess;
        }

        function getMaxrowperconnection() {
            return $this->maxrowperconnection;
        }
    
        function setMaxrowperconnection($maxrowperconnection) {
            $this->maxrowperconnection = $maxrowperconnection;
        }

        function getDconfig() {
            return $this->dconfig;
        }
    
        function setDconfig($dconfig) {
            $this->dconfig = $dconfig;
        }

}
