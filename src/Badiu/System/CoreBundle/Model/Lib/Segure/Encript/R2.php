<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Segure\Encript;

/**
 * Description of R2
 *
 * @author lino
 */
class R2 {
   
    
        //codificar
	 public   function encode($key,$text){
        //  echo "cripty_text $text<br>";
          $text_cripted="";
          $list_char= str_split($text);
           for ($i=0; $i<sizeof( $list_char);$i++){
               $value=$list_char[$i];
               $numb_ascii=ord($value);
               $numb_ascii_critpy= $numb_ascii-32;
               $valuer= substr($key,$numb_ascii_critpy,1);
               $text_cripted.=$valuer;
           }
          return $text_cripted;
      }
	
       public   function decode($key,$text){
         // echo "descripty_text $text<br>";
          $text_descripted="";
          $list_char= str_split($text);
           for ($i=0; $i<sizeof( $list_char);$i++){
               $value=$list_char[$i];
               $order_cripty=strpos($key,$value); 
               $order_cripty_seq=$order_cripty+32;
               $txtd=chr($order_cripty_seq);
               $text_descripted.=$txtd;
           }
          return $text_descripted;
      }

      public    function decodeView($code){
            if(empty($code)) return null;
            $code_list=explode(":",$code);
            $codewv="";
            for ($i=0; $i<sizeof($code_list);$i++){
                   $asc_num=$code_list[$i];
                    $asc_num=preg_replace('/[^0-9]/', '', $asc_num);
                   $codewv.=chr($asc_num);
                } 
            return  $codewv;
        }
        
	
       
        
	public   function format($text,$breakLine,$newLine,$start="",$end=""){
		if(empty($code)) return "";
		 $list_char=text.toCharArray();
		 $sb="";
		 $sb.=$start;
		 $sb.=$newLine;
		 $cont=0;
		 $ontx=0;
		  for ($y=0;$y<sizeof($list_char);$y++){
			 $cont++;
			 $contx=$y+1;
			$c=$list_char[$y];
			$sb.=$c;
			if($cont==$breakLine){  
				$sb.=$newLine;
				$cont=0;
			}
			if(($cont!=$breakLine) && ($contx==sizeof($list_char))){
				$sb.=$newLine;
				
			} 
			
		 }
		 $sb.=$end;
		  $sb.="\n";
		return $sb;
	}
      public    function gererateCode($unique,$min,$max,$keyLength,$exclude){
		   $key="";
		    $lentgth=0;
		    if($unique && $keyLength>($max-$min-1))return null;
		    $excludeAscii=ord($exclude);
		    while ($lentgth < $keyLength){
		        $r=$min + rand(0,$max-$min);
		        
		        if($r==$excludeAscii){
		             if($r==$min)$r++;
		             else if($r==$max)$r--;
		             else $r++;
		        }
		            
		        $s="".$r;
		       
		        if(!$unique){$key.=$s;}
		        else{
		        	if(strpos($key,$s)=== FALSE){
                                     $key.=$s;
                                     $key=trim($key);
                                };
                             
		               
		            }
		       
		        $lentgth=strlen($key);  
		       
		    }
		    

		    return $key;
		    }
		
		 
	
}
