<?php
namespace Badiu\System\CoreBundle\Model\Lib\Calendar;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CalendarDashboardLayout extends BadiuModelLib
{

	function __construct(Container $container)
         {
                 parent::__construct($container);
	  }

         public function makeFullCalendarLayout($data) {
             
             $list=array();
             $lrow=null;
             $lrows=null;
             $cont=0;
             if(isset($data['row'])){
                 $lrow=$data['row'];
             }
              if(isset($data['rows'])){
                 $lrows=$data['rows'];
             }
             
            //data row
             if(!empty($lrow)){
                   foreach ($lrow as $row) {
                       $cont++;
                      $rv=$this->convertLayout($cont,$row);
                      array_push($list,$rv);
                   }
             }
           
               if(!empty($lrows)){
                   foreach ($lrows as $rows) {
                      
                        foreach ($rows as $row) {
                            $cont++;
                           $rv=$this->convertLayout($cont,$row);
                            array_push($list,$rv);
                        }
                   }
             }
             $list;
           return $list;
         }  
          
          public function convertLayout($id,$row){
              //[{"id":"8","title":"la na xixa","start":"2016-09-24 00:00:00","end":"2016-09-25 00:00:00","url":"xxxx","allDay":"false"}
              $nrow=array();
              $id=$id;//review it
              $title="";
              $start="";
              $end="";
              $url="";
           
              if(isset($row['eventtitle'])){$title=$row['eventtitle'];}
              if(isset($row['eventtimestart'])){$start=$row['eventtimestart'];}
              if(isset($row['eventtimetend'])){$end=$row['eventtimetend'];}
             
               if(is_object($start) && is_a($start, 'DateTime')){
                   $start=$start->format('Y-m-d H:i:s');
               }
              if(is_object($end) && is_a($end, 'DateTime')){
                   $end=$end->format('Y-m-d H:i:s');
               } 
               
              $nrow['id']=$id;
              $nrow['title']=$title;
              $nrow['start']=$start;
              $nrow['end']=$end;
              $nrow['url']=$url;
              return $nrow;
          }
}
