<?php

namespace Badiu\System\CoreBundle\Model\Lib\Mongodb;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FactoryFilter {

    /**
     * @var Container
     */
    private $container;
    private $cleannull=false;
    private $utildata;
    function __construct(Container $container) {
        $this->container = $container;
        $this->utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
    }

    function makeParam($searchparam, $mongparam) {
       
        //print_r($mongparam);
       // print_r($searchparam);exit; 
        $searchparam=$this->formCastChangeParam($searchparam);
       // print_r($searchparam);exit;
        $newfilter = array();
        foreach ($mongparam as $key => $value) {
            $haskey = $this->hasKey($value);
            $keytype = $this->getTypeKey($value);
            $kvalue = $this->getKey($value, $keytype);
            $kvaluereal=$this->getKey($value, $keytype);
            if($keytype=='bfnumber'){
                $kvalue=$kvalue."_badiunumberoperator";
            }
            //if($keytype=='math'){$key=$kvalue;}
          
          // echo "$kvalue | $key | $keytype <br>";
            if (!$haskey) {
               $value=$this->castNumeric($value);
                $newfilter[$key] = $value;
             
            } else {
                  
                if (array_key_exists($kvalue, $searchparam)) {
                    $fvalue = $searchparam[$kvalue];
                    $mongoutil=$this->getContainer()->get('badiu.system.core.lib.mongodb.util');
                    $fvalue=$mongoutil->cast($fvalue);
                  
                    if (is_object($fvalue) || $fvalue == 0 || !empty($fvalue)) {
                           $ftype = gettype($fvalue);
                        
                        if ($keytype == 'single') {
                         
                            if($this->isBadiuDate($fvalue)) {
                                $value = $this->getDateFilter($fvalue);
                                if(!empty($value)){$newfilter[$key] = $value;}
                             } else if (is_numeric($fvalue)) {
                                $value = $fvalue + 0;
                                $newfilter[$key] = $value; 
                              
                            }else if($ftype=="string" && !empty(trim($fvalue))){$newfilter[$key] = $value;}
                        } else if ($keytype == 'in') {
                            if (is_array($fvalue)) {
                                $value = array('$in' => $fvalue);
                                $newfilter[$key] = $value;
                            }
                        }else if ($keytype == 'like') {
                              if($ftype=="string" && !empty(trim($fvalue))){
                                 $value = array('$regex' => "$fvalue",'$options'=> "i");
                                  $newfilter[$key] = $value;
                              }
                               
                         }else if ($keytype == 'bfdate') {
                             $date1=$this->getUtildata()->getVaueOfArray($searchparam, $kvaluereal.'_badiudate1');
                             $date2=$this->getUtildata()->getVaueOfArray($searchparam, $kvaluereal.'_badiudate2');
                             if($fvalue=='between'){
                                if((empty($date1) && !empty($date2))){
                                  $newfilter[$key] = array('$gte'=> $date1,'$lte'=> $date2);
                                }
                             }else{
                                  $foperator=$this->getOperator($fvalue);
                                    if(!empty($date1)){
                                        $newfilter[$key] = array($foperator=> $date1);
                                    }
                             }
                            
                           
                               
                         }else if ($keytype == 'bfnumber') {
                             $number1=$this->getUtildata()->getVaueOfArray($searchparam, $kvaluereal.'_badiunumber1');
                             $number2=$this->getUtildata()->getVaueOfArray($searchparam, $kvaluereal.'_badiunumber2');
                             if($fvalue=='between'){
                                if(($number1===0 || !empty($number1)) && ($number2===0 || !empty($number2))){
                                   $number1=$number1+0;
                                   $number2=$number2+0;
                                   $newfilter[$key] = array('$gte'=> $number1,'$lte'=> $number2);
                                }
                             }else{
                                 
                                  $foperator=$this->getOperator($fvalue);
                                    if($number1===0 || !empty($number1)){
                                        $number1=$number1+0;
                                        $newfilter[$key] = array($foperator=> $number1);
                                    }
                            }
                            
                               
                         }
                         else if ($keytype == 'math') {
                             if (is_numeric($fvalue)){$fvalue = $fvalue + 0;}
                             $newv=array();
                             foreach ($value as $k => $v) {
                                 $newv[$k]=$fvalue;
                             }
                             $newfilter[$key] = $newv;
                             
                               
                         }

                        
                    }
                }
                
            }
        }
      //   echo "<hr>xx";  print_r($newfilter); 
      // print_r($newfilter);
        if($this->cleannull){$newfilter=$this->removeFilterWithoutValue($newfilter);}
      //  echo "<hr>yyy"; 
     //  print_r($newfilter);exit;
        
        
        return $newfilter;
    }

    function removeFilterWithoutValue($filter) {
       $newfilter=array();
        if(!is_array($filter)){return $newfilter;}
        foreach ($filter as $key => $value) {
            $hasvalue=false;
            if(!is_array($value)){
                if(is_object($value) || $value===0 || !empty($value) ){$hasvalue=true;}
            }else{
                foreach ($value as $vi) {
                    if(is_object($vi) || $vi===0 || !empty($vi) ){$hasvalue=true;}
                  }
            }
            if($hasvalue){$newfilter[$key]=$value;}
        } 
        return $newfilter;
    }
    function getKey($value, $keytype) {
        $kvalue = null;
        if ($keytype == 'single') {
            $kvalue = str_replace(":", "", $value);
        } else if ($keytype == 'in') {
            $kvalue = str_replace("in:", "", $value);
        }
        else if ($keytype == 'like') {
            $kvalue = str_replace("like:", "", $value);
        } else if ($keytype == 'like') {
            $kvalue = str_replace("like:", "", $value);
        }
        else if ($keytype == 'bfnumber') {
            $kvalue = str_replace("bfnumber:", "", $value);
        } else if ($keytype == 'bfdate') {
            $kvalue = str_replace("bfdate:", "", $value);
        }
       else if ($keytype == 'math') {
            if(is_array($value)){
                foreach ($value as $k => $v) {
                    $kvalue = str_replace(":", "",  $v);
                }
            }
            
        }  
        
        return $kvalue;
    }

    function hasKey($value) {
        $result = false;
        if(is_array($value)){
            $cont=0;
            foreach ($value as $v) {
                $pos = strpos($v, ":");
                if ($pos !== false) {
                    $cont++;
                }
            }
            if($cont>0){$result = true;}
            return $result;
        }
        $value = trim($value);
        $pos = strpos($value, ":");
        if ($pos !== false) {
            $result = true;
        }
        return $result;
    }

    function getTypeKey($value) {
        $keytype = 'single'; // single | in ;
        
        //type math
         if(is_array($value)){
             $keytype = 'math';
             return $keytype;
         }
          //type math desable config yml reviw 
       /*  if(is_array($value)){
            foreach ($value as $v) {
                if($v==0 || !empty($v)){  $keytype = 'math';break;}
            }
             return $keytype;
         }*/
        $value = trim($value);
        //type in
        $pos = strpos($value, "in:");
        if ($pos !== false) {
            $keytype = 'in';
             return $keytype;
        }
         //type like
        $pos = strpos($value, "like:");
        if ($pos !== false) {
            $keytype = 'like';
             return $keytype;
        }
        //type bfnumber
        $pos = strpos($value, "bfnumber:");
        if ($pos !== false) {
            $keytype = 'bfnumber';
             return $keytype;
        }
        //type bfdate
        $pos = strpos($value, "bfdate:");
        if ($pos !== false) {
            $keytype = 'bfdate';
             return $keytype;
        }
        //type math
        //mathopsv:$gte:xxxxxx
        //type math static value
        //mathopsv:$gte:0
        return $keytype;
    }

    function getDateFilter($param) {
        //print_r($param);
        $dfiler = null;
        $operator = null;
        $timestart = null;
        $timeend = null;

        if (isset($param['badiudate1'])) {
            $timestart = $param['badiudate1'];
        }
        if (isset($param['badiudate2'])) {
            $timeend = $param['badiudate2'];
        }
        if (isset($param['badiudateoperator'])) {
            $operator = $param['badiudateoperator'];
        }
    //  print_r($timestart);
     // echo "<br>";
      // print_r($timeend);
        //between
        if ($operator == 'between' && !empty($timestart) && !empty($timeend)) {
            $dfiler = array('$gte' => new \MongoDate($timestart->getTimestamp()), '$lte' => new \MongoDate($timeend->getTimestamp()));
        } else {
            if(!empty($timestart)){
                $mongoperator = $this->getOperator($operator);
                $dfiler = array($mongoperator => new \MongoDate($timestart->getTimestamp()));
                 //$dfiler = array($mongoperator => new \MongoDate(strtotime("2010-01-15 00:00:00")));
            }
            
        }

        return $dfiler;
    }
     function isBadiuDate($param) {
         $result=false;
        if (isset($param['badiudateoperator'])) {$result=true;}
         return $result;
     }
     //review it
    function getOperator($operator) {
        $mongodboperator = null;
        if ($operator == 'from' || $operator == 'bigger') {
            $mongodboperator = '$gt';
        } else if ($operator == 'after' || $operator == 'biggerorequal') {
            $mongodboperator = '$gte';
        }else if ($operator == 'until'  || $operator == 'lessorequal') {
            $mongodboperator = '$lte';
        } else if ($operator == 'before'  || $operator == 'less') {
            $mongodboperator = '$lt';
        } else if ($operator == 'on'  || $operator == 'equal') {
            $mongodboperator = '$eq';
        } else if ($operator == 'different'  || $operator == 'different') {
            $mongodboperator = '$ne';
        }
        return $mongodboperator;
    }

     function castNumeric($var){
         if(is_numeric($var)){$var=$var+0;}
         if(is_array($var)){
             $newvar=array();
             foreach ($var as $key => $value) {
                  if(is_numeric($value)){$value=$value+0;}
                 $newvar[$key]=$value;
             }
            $var= $newvar;
         }
         return $var;
     }
     public function formCastChangeParam($param) {
        //$param= clone $paramfilter;
        $filterdate= $this->container->get('badiu.system.component.form.cast.badiufilterdate');
	$param=$filterdate->changeParam($param);
        
         $filternumber= $this->container->get('badiu.system.component.form.cast.badiufilternumber');
	$param=$filternumber->changeParam($param);
        
        $badiutimeperiod= $this->container->get('badiu.system.component.form.cast.badiutimeperiod');
	$param=$badiutimeperiod->changeParam($param,true);
        
        return  $param;
    }
    function getContainer() {
        return $this->container;
    }

    function setContainer(Container $container) {
        $this->container = $container;
    }

    function getCleannull() {
        return $this->cleannull;
    }

    function setCleannull($cleannull) {
        $this->cleannull = $cleannull;
    }

    function getUtildata() {
        return $this->utildata;
    }

    function setUtildata($utildata) {
        $this->utildata = $utildata;
    }


}
