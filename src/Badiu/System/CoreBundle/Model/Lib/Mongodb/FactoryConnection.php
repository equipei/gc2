<?php

namespace Badiu\System\CoreBundle\Model\Lib\Mongodb;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FactoryConnection {

    /**
     * @var Container
     */
    private $container;

    /**
     * @var string
     */
    private $database;

    /**
     * @var string
     */
    private $host;

    /**
     * @var integer
     */
    private $port;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var array
     */
    private $filter = array();

    /**
     * @var array
     */
    private $fields = array();

    /**
     * @var array
     */
    private $orderby = array();

    /**
     * @var array
     */
    private $aggregatedata = array();
    
    /**
     * @var array
     */
    private $index = array();

    /**
     * @var string
     */
    private $collection;

    /**
     * @var string
     */
    private $operation;

    /**
     * @var object
     */
    private $conn;
    
    function __construct(Container $container) {
        $this->container = $container;
        $this->initParameter();
    }

    function initConfig($paramconf, $paramfilter = null) {
        if (isset($paramconf['config']['collection'])) {
            $this->collection = $paramconf['config']['collection'];
        }
        if (isset($paramconf['config']['function'])) {
            $this->operation = $paramconf['config']['function'];
        }
        if (isset($paramconf['filter'])) {
            $this->filter = $paramconf['filter'];
        }
        if (isset($paramconf['fields'])) {
            $this->fields = $paramconf['fields'];
        }
        if (isset($paramconf['orderby'])) {
            $this->orderby = $paramconf['orderby'];
        }
       
        $factoryfilter = $this->getContainer()->get('badiu.system.core.lib.mongodb.factoryfilter');
        $factoryfilter->setCleannull(true);
        $this->filter = $factoryfilter->makeParam($paramfilter, $this->filter);
        
       
        $this->factoryConn();
        //$this->initCollection($this->collection);
    }

    function initParameter() {
        $this->database = $this->getContainer()->getParameter('mongodb_database');
        $this->port = $this->getContainer()->getParameter('mongodb_port');
        $this->host = $this->getContainer()->getParameter('mongodb_host');
        $this->user = $this->getContainer()->getParameter('mongodb_user');
        $this->password = $this->getContainer()->getParameter('mongodb_password');
    }

    function initConfigBySservice($sservice, $paramconf = null, $paramfilter = null) {
        $this->initParameterBySservice($sservice);
        
/*echo "<pre>";
print_r($paramconf);
echo "</pre>";*/
        if (isset($paramconf['config']['collection'])) {
            $this->collection = $paramconf['config']['collection'];
        }
        if (isset($paramconf['config']['index'])) { $this->index = $paramconf['config']['index'];}
        if (isset($paramconf['config']['function'])) {
            $this->operation = $paramconf['config']['function'];
        }
        if (isset($paramconf['filter'])) {
            $this->filter = $paramconf['filter'];
        }
        
        if (isset($paramconf['fields'])) {
            $this->fields = $paramconf['fields'];
        }
        if (isset($paramconf['orderby'])) {
            $this->orderby = $paramconf['orderby'];
        }
       // echo "<br>collection: ".$this->collection;
        $factoryfilter = $this->getContainer()->get('badiu.system.core.lib.mongodb.factoryfilter');
        $this->filter = $factoryfilter->makeParam($paramfilter, $this->filter);
        
        //$this->initCollection($this->collection);
        $this->factoryConn();
        $this->createIndex();
    }

    function initParameterBySservice($sservice) {
        if (empty($sservice)) {
            return null;
        }
        $this->database = $sservice->getDbname();
        $this->port = $sservice->getDbport();
        $this->host = $sservice->getDbhost();
        $this->user = $sservice->getDbuser();
        $this->password = $sservice->getDbpwd();
    }

   

    /* function configFilter($paramfilter) {
      print_r($paramfilter);
      echo "<hr>";
      print_r($this->filter);
      $newfilter=array();
      foreach ($this->filter as $key => $value) {
      $kvalue=str_replace(":","",$value);
      if(array_key_exists($kvalue,$paramfilter)){
      $fvalue=$paramfilter[$kvalue];

      if($fvalue==0 || !empty($fvalue)){
      $value=str_replace(":$kvalue:",$fvalue,$value);
      if(is_numeric($value)){$value=$value+0;}
      $newfilter[$key]=$value;
      }
      }

      }
      $this->filter=$newfilter;
      print_r($this->filter);
      } */

    function factoryConn() {
        $db = $this->database;
        $port = $this->port;
        $host = $this->host;
        $user = $this->user;
        $pwd = $this->password;
        $conn = new \MongoDB\Driver\Manager("mongodb://$host:$port", array("socketTimeoutMS" => 190000));
        $this->conn=$conn;
        
        return $conn;
    }
/* DELETE
     function initCollection($collection) {

        $db = $this->conn();
        $col = new \MongoCollection($db, $collection);
        $this->setCollection($col);
    }*/
    function initCollection($collection) {
        $this->setCollection($collection);
         $this->factoryConn();
    }
function createIndex($single=true) {
        if(empty($this->index)){return null;}
        $indexdata=array();
        if(!$single){ $indexdata=$this->index;}
        if($single){
             foreach ($this->index as $row) {
            $cont=0;
            $indexitem=array();
            foreach ($row as $key => $value) {
                
                $ikey=array();
                if($cont==0){
                     $indexitem['name']=$key;
                      $ikey[$key]=$value;
                      $indexitem['key']=$ikey;
                 }
                else if($cont==1){$indexitem['key']['unique']=$value;}
                $cont++;
            }
           array_push($indexdata,$indexitem);
         }
        }
      
         $result=null;
        try {
           
               $command = new \MongoDB\Driver\Command(array("createIndexes" => $this->collection,"indexes"=>$indexdata));
             
               $result=$this->conn->executeCommand($this->database, $command);
              
           } catch (Exception $exc) {} 
          $this->index=null;
       return    $result;        
   }
    function count($filter=null) {
       
        $pfilter=$this->filter;
        if(!empty($filter)){$pfilter=$filter;}
       
        $command = new \MongoDB\Driver\Command(array("count" => $this->collection, "query" => $pfilter));
        $result=$this->conn->executeCommand($this->database, $command); 
        $res = current($result->toArray());
        $count = $res->n;
        
        return  $count;
    }

    function find($offset = 0, $limit = 10) {
/*
  print_r($this->orderby); 
        print_r($this->fields);
          print_r($this->filter);
          echo "<hr>";exit;*/
        $options = array('projection' => $this->fields,'limit' => $limit,'skip' => $offset,'sort'=>$this->orderby);
      
        $query = new \MongoDB\Driver\Query($this->filter, $options);
        $dbcollection=$this->database.'.'.$this->collection;
        $result = $this->conn->executeQuery($dbcollection, $query);
      
        $result->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
        $result=$result->toArray();
 
        return $result;
    }
    
    //review limit 
     function distinct($field,$offset = 0, $limit = 10) {
            $pfilter=$this->filter;
         if(!empty($filter)){$pfilter=$filter;}
          $command = new \MongoDB\Driver\Command(array("distinct" => $this->collection, "key"=>$field,"query" => $pfilter));
          $result=$this->conn->executeCommand($this->database, $command); 
         $result->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
         $result=$result->toArray();
         if(isset($result[0]['values'])){$result=$result[0]['values'];}
       
        return $result;
    }
    function aggregate($offset = 0, $limit = 10) {
    
        if(sizeof($this->aggregatedata)==0){
            
            //filter
            if(isset($this->filter) && sizeof($this->filter)>0){
                array_push($this->aggregatedata,array( '$match'=>$this->filter));
           }
           //filds
            array_push($this->aggregatedata,array('$group'=>$this->fields));
            //limit
             if($limit > 0){ array_push($this->aggregatedata,array('$limit' => $limit ));}
             if($offset >= 0){ array_push($this->aggregatedata,array('$skip' => $offset ));}
             
             //sort
            if(isset($this->orderby) && sizeof($this->orderby)>0){
                array_push($this->aggregatedata,array( '$sort'=>$this->orderby));
           }
             
        } 
        /*echo "<pre>";
        print_r($this->aggregatedata);
        echo "</pre>";*/
        $command = new \MongoDB\Driver\Command(array('aggregate' => $this->collection, 'pipeline' =>$this->aggregatedata));
         $result=$this->conn->executeCommand($this->database, $command);
         $res = $result->toArray();
        //echo "<hr>";
 
       /* echo "<pre>";
       print_r($res);
        echo "</pre>";exit;*/
        $this->aggregatedata=array();
        return $res;
    }
    function update($filter, $param, $options = array()) {
     /*  echo "<pre>";
        print_r($filter);echo "<hr>";
         print_r($param);echo "<hr>";
          print_r($options);echo "<hr>";
        echo "</pre>";
        exit;*/
        
        $writeConcern = new \MongoDB\Driver\WriteConcern(\MongoDB\Driver\WriteConcern::MAJORITY, 1000);
         
        $bulk = new \MongoDB\Driver\BulkWrite(['ordered' => true]);
        $bulk->update($filter, $param, $options);
        
        $dbcollection=$this->database.'.'.$this->collection;
        $result =null;
        try {
           
            $result = $this->conn->executeBulkWrite($dbcollection, $bulk, $writeConcern);
         } catch (\MongoDB\Driver\Exception\BulkWriteException $e) {
            $result = $e->getWriteResult();
         }
        return $result;
    }
  //review $options
    function insert($param, $options = array()) {
        $writeConcern = new \MongoDB\Driver\WriteConcern(\MongoDB\Driver\WriteConcern::MAJORITY, 1000);
        $bulk = new \MongoDB\Driver\BulkWrite(['ordered' => true]);
        $bulk->insert($param);
        $dbcollection=$this->database.'.'.$this->collection;
       $result =0;
        try {
            $result = $this->conn->executeBulkWrite($dbcollection, $bulk, $writeConcern);
             $result = $result->getInsertedCount();
         } catch (\MongoDB\Driver\Exception\BulkWriteException $e) {
            $result = $e->getWriteResult();
            $result =0;
         }
        return $result;
    
    }
   // review $options
    function batchInsert($params, $options = array()) {
         $writeConcern = new \MongoDB\Driver\WriteConcern(\MongoDB\Driver\WriteConcern::MAJORITY, 1000);
        $bulk = new \MongoDB\Driver\BulkWrite(['ordered' => true]);
        foreach ($params as $param) {
            $bulk->insert($param);
        }
        $dbcollection=$this->database.'.'.$this->collection;
        $result =0;
        try {
            $result = $this->conn->executeBulkWrite($dbcollection, $bulk, $writeConcern);
             $result = $result->getInsertedCount();
         } catch (\MongoDB\Driver\Exception\BulkWriteException $e) {
            $result = $e->getWriteResult();
            $result =0;
         }
        return $result;
     
    }

    function getContainer() {
        return $this->container;
    }

    function getFilter() {
        return $this->filter;
    }

    function getFields() {
        return $this->fields;
    }

    function getDatabase() {
        return $this->database;
    }

    function getCollection() {
        return $this->collection;
    }

    function setContainer(Container $container) {
        $this->container = $container;
    }

    function setFilter($filter) {
        $this->filter = $filter;
    }

    function setFields($fields) {
        $this->fields = $fields;
    }

    function setDatabase($database) {
        $this->database = $database;
    }

    function setCollection($collection) {
        $this->collection = $collection;
    }

    function getHost() {
        return $this->host;
    }

    function getPort() {
        return $this->port;
    }

    function getUser() {
        return $this->user;
    }

    function getPassword() {
        return $this->password;
    }

    function getOperation() {
        return $this->operation;
    }

    function setHost($host) {
        $this->host = $host;
    }

    function setPort($port) {
        $this->port = $port;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setOperation($operation) {
        $this->operation = $operation;
    }

    function getOrderby() {
        return $this->orderby;
    }

    function setOrderby($orderby) {
        $this->orderby = $orderby;
    }

    function getIndex() {
        return $this->index;
    }

    function setIndex($index) {
        $this->index = $index;
    }

    function getAggregatedata() {
        return $this->aggregatedata;
    }

    function setAggregatedata($aggregatedata) {
        $this->aggregatedata = $aggregatedata;
    }


}
