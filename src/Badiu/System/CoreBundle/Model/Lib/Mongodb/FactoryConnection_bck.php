<?php

namespace Badiu\System\CoreBundle\Model\Lib\Mongodb;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FactoryConnection {

    /**
     * @var Container
     */
    private $container;

    /**
     * @var string
     */
    private $database;

    /**
     * @var string
     */
    private $host;

    /**
     * @var integer
     */
    private $port;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var array
     */
    private $filter = array();

    /**
     * @var array
     */
    private $fields = array();

    /**
     * @var array
     */
    private $orderby = array();

    /**
     * @var array
     */
    private $aggregatedata = array();
    
    /**
     * @var array
     */
    private $index = array();

    /**
     * @var string
     */
    private $collection;

    /**
     * @var string
     */
    private $operation;

    function __construct(Container $container) {
        $this->container = $container;
        $this->initParameter();
    }

    function initConfig($paramconf, $paramfilter = null) {
        if (isset($paramconf['config']['collection'])) {
            $this->collection = $paramconf['config']['collection'];
        }
        if (isset($paramconf['config']['function'])) {
            $this->operation = $paramconf['config']['function'];
        }
        if (isset($paramconf['filter'])) {
            $this->filter = $paramconf['filter'];
        }
        if (isset($paramconf['fields'])) {
            $this->fields = $paramconf['fields'];
        }
        if (isset($paramconf['orderby'])) {
            $this->orderby = $paramconf['orderby'];
        }
        $factoryfilter = $this->getContainer()->get('badiu.system.core.lib.mongodb.factoryfilter');
        $this->filter = $factoryfilter->makeParam($paramfilter, $this->filter);
        $this->initCollection($this->collection);
    }

    function initParameter() {
        $this->database = $this->getContainer()->getParameter('mongodb_database');
        $this->port = $this->getContainer()->getParameter('mongodb_port');
        $this->host = $this->getContainer()->getParameter('mongodb_host');
        $this->user = $this->getContainer()->getParameter('mongodb_user');
        $this->password = $this->getContainer()->getParameter('mongodb_password');
    }

    function initConfigBySservice($sservice, $paramconf = null, $paramfilter = null) {
        $this->initParameterBySservice($sservice);
/*echo "<pre>";
print_r($paramconf);
echo "</pre>";*/
        if (isset($paramconf['config']['collection'])) {
            $this->collection = $paramconf['config']['collection'];
        }
        if (isset($paramconf['config']['index'])) { $this->index = $paramconf['config']['index'];}
        if (isset($paramconf['config']['function'])) {
            $this->operation = $paramconf['config']['function'];
        }
        if (isset($paramconf['filter'])) {
            $this->filter = $paramconf['filter'];
        }
        if (isset($paramconf['fields'])) {
            $this->fields = $paramconf['fields'];
        }
        if (isset($paramconf['orderby'])) {
            $this->orderby = $paramconf['orderby'];
        }
       // echo "<br>collection: ".$this->collection;
        $factoryfilter = $this->getContainer()->get('badiu.system.core.lib.mongodb.factoryfilter');
        $this->filter = $factoryfilter->makeParam($paramfilter, $this->filter);
        $this->initCollection($this->collection);
        $this->createIndex();
    }

    function initParameterBySservice($sservice) {
        if (empty($sservice)) {
            return null;
        }
        $this->database = $sservice->getDbname();
        $this->port = $sservice->getDbport();
        $this->host = $sservice->getDbhost();
        $this->user = $sservice->getDbuser();
        $this->password = $sservice->getDbpwd();
    }

   

    /* function configFilter($paramfilter) {
      print_r($paramfilter);
      echo "<hr>";
      print_r($this->filter);
      $newfilter=array();
      foreach ($this->filter as $key => $value) {
      $kvalue=str_replace(":","",$value);
      if(array_key_exists($kvalue,$paramfilter)){
      $fvalue=$paramfilter[$kvalue];

      if($fvalue==0 || !empty($fvalue)){
      $value=str_replace(":$kvalue:",$fvalue,$value);
      if(is_numeric($value)){$value=$value+0;}
      $newfilter[$key]=$value;
      }
      }

      }
      $this->filter=$newfilter;
      print_r($this->filter);
      } */

    function conn() {
        $db = $this->database;
        $port = $this->port;
        $host = $this->host;
        $user = $this->user;
        $pwd = $this->password;
        //$conn = new \MongoClient("mongodb://$user:$pwd@$host:$port");
        //$conn = new \Mongo("mongodb://$host:$port",array("username" =>$user, "password" =>$pwd,"db" =>$this->database ));
        //$server = "mongodb://$user:$pwd@$host:$port";
        // $conn = new \MongoClient( $server );
        //  $conn = new \Mongo("mongodb://$host:$port",array("username" =>$user, "password" =>$pwd,"db" =>$this->database ));
        //  $conn = new \Mongo("mongodb://$user:$pwd@$host:$port/$db");
        //$db=$conn->selectDB($this->database);
        //$db->authenticate($user,$pwd);
        $conn = new \MongoDB\Driver\Manager("mongodb://$host:$port", array("socketTimeoutMS" => 190000));
       //  $conn = new \MongoClient("mongodb://$host:$port");
        $db = $conn->selectDB($this->database);
        //$conn = $mongo->selectDB($this->database); 
        //\MongoLog::setLevel(MongoLog::ALL);
        // \MongoLog::setModule(MongoLog::ALL);
        return $db;
    }

    function initCollection($collection) {

        $db = $this->conn();
        $col = new \MongoCollection($db, $collection);
        $this->setCollection($col);
    }
function createIndex() {
        if(empty($this->index)){return null;}
        foreach ($this->index as $row) {
           
            $column=array();
            $unique=array();
            $cont=0;
            foreach ($row as $key => $value) {
                if($cont==0){$column[$key]=$value;}
                else if($cont==1){$unique[$key]=$value;}
                $cont++;
            }
            if(isset($row[0])){$column=$row[0];}
            if(isset($row[1])){$unique=$row[1];}
            //print_r($column);
           // echo"-------------";
           // print_r($unique);
           try {$this->collection->createIndex($column,$unique); } catch (Exception $exc) {}
           
            //db.collection.getIndexes()
         }
         
        
   }
    function count($filter=null) {
         $pfilter=$this->filter;
         if(!empty($filter)){$pfilter=$filter;}
        //  $conn=$this->conn();
        //  $collection =new \MongoCollection($conn, $this->collection);
        // $count = $collection->count($this->filter);
         
        $count = $this->collection->count($pfilter);
       
        return $count;
    }

    function find($offset = 0, $limit = 10) {
 
        // $conn=$this->conn();
    /*   print_r($this->orderby); 
          print_r($this->fields);
          print_r($this->filter);*/
      
        // $collection =new \MongoCollection($conn, $this->collection);
        $result = $this->collection->find($this->filter, $this->fields)
                ->limit($limit)
                ->skip($offset)
                ->sort($this->orderby);
        
        return $result;
    }
    function aggregate($offset = 0, $limit = 10) {
        if(sizeof($this->aggregatedata)==0){
            array_push($this->aggregatedata,$this->filter);
            array_push($this->aggregatedata,$this->fields);
            array_push($this->aggregatedata,$this->orderby);
            if($limit > 0){ array_push($this->aggregatedata,array('$limit' => $limit ));}
            if($offset >= 0){ array_push($this->aggregatedata,array('$skip' => $offset ));}
            
        }
      //  print_r($this->aggregatedata);exit;
        //echo "<hr>";
        $result = $this->collection->aggregate($this->aggregatedata);
        $this->aggregatedata=array();
        return $result;
    }
    function update($filter, $param, $options = array()) {
        $result = $this->collection->update($filter, $param, $options);
        return $result;
    }

    function insert($param, $options = array()) {
        $result = $this->collection->insert($param);
        return $result;
    }

    function batchInsert($params, $options = array()) {
        $result = $this->collection->batchInsert($params);
        return $result;
    }

    function getContainer() {
        return $this->container;
    }

    function getFilter() {
        return $this->filter;
    }

    function getFields() {
        return $this->fields;
    }

    function getDatabase() {
        return $this->database;
    }

    function getCollection() {
        return $this->collection;
    }

    function setContainer(Container $container) {
        $this->container = $container;
    }

    function setFilter($filter) {
        $this->filter = $filter;
    }

    function setFields($fields) {
        $this->fields = $fields;
    }

    function setDatabase($database) {
        $this->database = $database;
    }

    function setCollection($collection) {
        $this->collection = $collection;
    }

    function getHost() {
        return $this->host;
    }

    function getPort() {
        return $this->port;
    }

    function getUser() {
        return $this->user;
    }

    function getPassword() {
        return $this->password;
    }

    function getOperation() {
        return $this->operation;
    }

    function setHost($host) {
        $this->host = $host;
    }

    function setPort($port) {
        $this->port = $port;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setOperation($operation) {
        $this->operation = $operation;
    }

    function getOrderby() {
        return $this->orderby;
    }

    function setOrderby($orderby) {
        $this->orderby = $orderby;
    }

    function getIndex() {
        return $this->index;
    }

    function setIndex($index) {
        $this->index = $index;
    }

    function getAggregatedata() {
        return $this->aggregatedata;
    }

    function setAggregatedata($aggregatedata) {
        $this->aggregatedata = $aggregatedata;
    }


}
