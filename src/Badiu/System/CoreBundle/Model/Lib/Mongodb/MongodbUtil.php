<?php

namespace Badiu\System\CoreBundle\Model\Lib\Mongodb;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class MongodbUtil {

    /**
     * @var Container
     */
    private $container;

    function __construct(Container $container) {
        $this->container = $container;
    }

     public function castRowToInsert($row) {
        if(empty($row)) {return $row;}       
       $nrow=array();
       foreach ($row as $key => $value) {
         $value=$this->cast($value);
         $nrow[$key]=$value;
       }
     
       return $nrow;
   }
   public function castRowsToInsert($rows) {
       $list=array();
       foreach ($rows as $key => $row) {
        $list[$key]=$this->castRowToInsert($row);
       }
      return $list;
   }
   
    public function cast($value) {
         $type=gettype($value);
        
          if($type=="string" && is_numeric($value)){
              $value=$value+0;
          }else if($type=="object" && is_a($value, 'DateTime') ){
              //$value=new \MongoDate($value->getTimestamp()-(60*60*2));
            
              $timestamp=$value->getTimestamp();
              $timestamp=$timestamp-(60*60*2);
              $timestamp=$timestamp*1000;
              
              
              $value=new \MongoDB\BSON\UTCDateTime($timestamp);
          }
          return $value;
    }
    //desable
    /*public function initConnection($param) {
        $database=null;
        $collection=null;
        
         if(isset($param['database'])) {$database=$param['database'];}
         if(isset($param['collection'])) {$collection=$param['collection'];}
       
       $mongo = new \MongoClient();
       $db = $mongo->selectDB($database); 
       $collection= $col = new \MongoCollection($db,$collection);
      
       return $collection;
       
       
    }*/
    
}
