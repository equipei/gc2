<?php
namespace Badiu\System\CoreBundle\Model\Lib\Math;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Badiu\System\CoreBundle\Model\Lib\Math\DOMAINTABLE;
class NumberFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
     
   
    public  function getOperatorText(){
        $list=array();
        $list[DOMAINTABLE::$OPERATOR_MATH_BIGGER]=$this->getTranslator()->trans('badiu.system.math.operatortext.bigger');
        $list[DOMAINTABLE::$OPERATOR_MATH_BIGGEROREQUAL]=$this->getTranslator()->trans('badiu.system.math.operatortext.biggerorequal');
	$list[DOMAINTABLE::$OPERATOR_MATH_LESS]=$this->getTranslator()->trans('badiu.system.math.operatortext.less');
	$list[DOMAINTABLE::$OPERATOR_MATH_LESSOREQUAL]=$this->getTranslator()->trans('badiu.system.math.operatortext.lessorequal');
	$list[DOMAINTABLE::$OPERATOR_MATH_EQUAL]=$this->getTranslator()->trans('badiu.system.math.operatortext.equal');
        $list[DOMAINTABLE::$OPERATOR_MATH_DIFFERENT]=$this->getTranslator()->trans('badiu.system.math.operatortext.different');
        $list[DOMAINTABLE::$OPERATOR_MATH_BETWEEN]=$this->getTranslator()->trans('badiu.system.math.operatortext.between');
        return $list;
    }
    
    
}
