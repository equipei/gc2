<?php 
namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class SystemCheck extends BadiuModelLib{
   
    

    function __construct(Container $container) {
          parent::__construct($container);
     } 
    /**
	return string: missingdbparamconf | errordbconfig | notinstaled | instaled | requireuupdate
	*/
	public function getStatus(){
		//check parameter
		
		$dbconfig=true;
		if(!$this->getContainer()->hasParameter('database_driver') || empty($this->getContainer()->getParameter('database_driver'))){$dbconfig=false;}
		if(!$this->getContainer()->hasParameter('database_host') || empty($this->getContainer()->getParameter('database_host'))){$dbconfig=false;}
		if(!$this->getContainer()->hasParameter('database_name') || empty($this->getContainer()->getParameter('database_name'))){$dbconfig=false;}
		if(!$this->getContainer()->hasParameter('database_user') || empty($this->getContainer()->getParameter('database_user'))){$dbconfig=false;}
		
		if(!$dbconfig){return 'missingdbparamconf';}
		
		$dbconn=$this->isDbConnection();
		if(!$dbconn){return 'errordbconfig';}
		
		$existbluser=$this->exitUserTable();
		if(!$existbluser){return 'notinstaled';}
		
		$completedinstall=$this->isInstallationCompleted();
		
		//check require update
		
		if($completedinstall){return 'instaled';}
		return 'none';
	}
	public function isDbConnection(){
		$mdata=$this->getContainer()->get('badiu.system.user.user.data');
		$mdata->getEm()->getConnection()->connect();
		$connected = $mdata->getEm()->getConnection()->isConnected();
		return $connected;
	}
	
	public function exitUserTable(){
		$existusertbl=0;
		$mdata=$this->getContainer()->get('badiu.system.user.user.data');
		$schemaManager=$mdata->getEm()->getConnection()->getSchemaManager();
		if($schemaManager->tablesExist(array('system_user')) == true){$existusertbl=1;}
		return $existusertbl;
	}
	
	public function isInstallationCompleted(){
		$data= $this->getContainer()->get('badiu.system.module.configglobal.data'); 
		$vparamcheckexist=array('bundlekey'=>'badiu.system.packge','name'=>'badiu.system.packge.version');		 
		$isintalled=$data->countGlobalRow($vparamcheckexist);
		return $isintalled;
	}
}

?>
