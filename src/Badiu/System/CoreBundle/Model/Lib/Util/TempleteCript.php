<?php

namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class TempleteCript {
    
      /**
     * @var Container
     */
    private $container;
    private $type=null;
    private $identify=null;
    private $cript = null;
    private $utildata = null;
    /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
    function __construct(Container $container) {
                $this->container=$container;
		$this->utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
               $this->cript= $this->container->get('badiu.system.core.lib.util.cript');
      }
    
    function decode($content,$criptkey=null,$debug=0) {
      $p= explode("|",$content);
      $type=$this->utildata->getVaueOfArray($p, 0);
     
      $identify=$this->utildata->getVaueOfArray($p, 1);
      $this->identify=$identify;
      //type shoud be: s1|k1|k2|ks
      if(!empty($type)){
        
          $lenghscriptconf=strlen("$type|$identify|");
          $lenghscontent=strlen($content);
          $content=substr($content,$lenghscriptconf, $lenghscontent);
          if(!empty($criptkey)){$ckey=base64_decode($criptkey);}
          else{$ckey=$this->getCriptKey($type,$identify);}
          $this->cript->setKey($ckey);
          $content=$this->cript->decode($content); 
        
          
      }
      
      return $content;
    }
    function encode($type,$content,$identify=0) {
        $ckey=$this->getCriptKey($type,$identify);
        $this->cript->setKey($ckey);
        $content=$this->cript->encode($content);
        $content="$type|$identify|$content";
        return $content;
    }
    
    function getCriptKey($type,$identify=0) {
        $ckey=null;
        if($type=='s1'){
            $ckey= $this->cript->getKey();
        }else if($type=='k1' || $type=='k2'){
            $sskey="badiu.system.uitl.crip.template.".$type.".".$identify;
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $badiuSession->setHashkey($this->getSessionhashkey());
            $ckey=$badiuSession->getValue($sskey);
           
            if(empty($ckey)){
                $data=$this->getContainer()->get('badiu.admin.server.service.data');
                $json = $this->getContainer()->get('badiu.system.core.lib.util.json');
                $dconfig=$data->getGlobalColumnValue('dconfig',array('id'=>$identify)); 
                $dconfig= $json->decode($dconfig,true);
           
                $criptk1=$this->getUtildata()->getVaueOfArray($dconfig,'cript.k1',true);
                $criptk2=$this->getUtildata()->getVaueOfArray($dconfig,'cript.k2',true);
                if($type=='k1'){$ckey=$criptk1;}
                else if($type=='k2'){$ckey=$criptk2;}
                
                $ckey=base64_decode($ckey);
                
                $badiuSession->addValue($sskey,$ckey);
            }
            
         }else if($type=='ks'){
             $badiuSession = $this->getContainer()->get('badiu.system.access.session');
             $badiuSession->setHashkey($this->getSessionhashkey());
             $criptinfo= $badiuSession->get()->getWebserviceinfo();
             $criptykey=$this->getUtildata()->getVaueOfArray($criptinfo,'criptykey');
             $ckey=base64_decode($criptykey);
             
          }
        return $ckey;
    }
    
     function makeKey($lenght=250,$base64encode=true) {
         $key=openssl_random_pseudo_bytes($lenght);
         if($base64encode){
             $key=base64_encode($key);
         }
         return $key;
     }
    function getType() {
        return $this->type;
    }


    function getCript() {
        return $this->cript;
    }

    function setType($type) {
        $this->type = $type;
    }

    

    function setCript($cript) {
        $this->cript = $cript;
    }
  function getIdentify() {
        return $this->identify;
    }

    function setIdentify($identify) {
        $this->identify = $identify;
    }

    function getContainer() {
        return $this->container;
    }

    function getUtildata() {
        return $this->utildata;
    }

    function setContainer(Container $container) {
        $this->container = $container;
    }

    function setUtildata($utildata) {
        $this->utildata = $utildata;
    }

    public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }

}
 
?>
