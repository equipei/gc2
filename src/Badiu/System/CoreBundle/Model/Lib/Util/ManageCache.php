<?php
namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**
  *
 * @author lino
 */
class ManageCache  extends BadiuModelLib{
	/**
     * @var array
     */ 
     private $param;
	 private $file;
	 private $subdir;
	 private $dentity;
	function __construct(Container $container) {
        parent::__construct($container);
		$this->param=array();
		$this->file=null;
		$this->subdir="badiu";
	 }
	 
	  public function init($param) {
		 $this->param=$this->load($param);
		 $type=$this->getUtildata()->getVaueOfArray($param,'type');
		 
		 
		 
		 
		 if($type=='language'){
			$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			$globalconf=array();
			$param['entity']=0;
			$globalconf=$this->load($param);
			
			$langbase="";
			$langbaseconf=array();
			$locale=$this->getUtildata()->getVaueOfArray($param,'locale');
			
			if (strpos($locale, "_") !== false) {
				$slbp=explode("_", $locale);
				$langbase=$this->getUtildata()->getVaueOfArray($slbp,0);
			}
			if(!empty($langbase)){
				 $param['locale']=$langbase;
				 $param['entity']=$entity;
				 $langbaseconf=$this->load($param);
			}
			
			$this->param= array_replace($globalconf,$langbaseconf,$this->param);
			 // echo "---lang loaded ----";
			 
		 }else if($type=='cache'){
			$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			$globalconf=array();
			$param['entity']=0;
			$globalconf=$this->load($param);
			
			 $entityconf=array();
			 $param['entity']=$entity;
			 $entityconf=$this->load($param);
			
			$this->param= array_replace($globalconf,$entityconf,$this->param);
			// echo "---cache loaded ----";
		 }
     }
	 
	 public function getPathDir($param) {
		
		$kernel = $this->getContainer()->get('kernel');
		$env=$kernel->getEnvironment();
		
		if($this->getContainer()->hasParameter('badiu.system.file.appcache')){
			$custompath =$this->getContainer()->getParameter('badiu.system.file.appcache');
			
			if(!empty($custompath)){
				$custompath=$custompath.'/'.$env; 
				return $custompath; 
			} 
		}
		$filelib=$this->getContainer()->get('badiu.system.file.file.lib');
		$defaultpath=$filelib->getSystemBasePath();
		
		$path=$defaultpath.'/appcache/'.$env; 
        return $path;
	   //$cacheDir = $kernel->getCacheDir();
	   //return $cacheDir.'/'.$this->subdir;
     }
	 
	  public function createDir($basepath) {
		if (!file_exists($basepath)) {
			mkdir($basepath, 0777, true);
		}
		
	  }
	 public function makeFileName($param) {
		 $type=$this->getUtildata()->getVaueOfArray($param,'type');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'type');
		 $filename="undefined.php";
		 if($type=='language'){
			 $locale=$this->getUtildata()->getVaueOfArray($param,'locale');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 $filename="_system_core_badiu_language_".$entity."_".$locale.".php";
		 }else if($type=='cache'){
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 $filename="_system_core_badiu_cache_".$entity.".php";
		 }
		
		return $filename;
	  }
	 public function loadUpdate($param) {
		 $onlycheckexist=$this->getUtildata()->getVaueOfArray($param,'onlycheckexist');
		 $replace=$this->getUtildata()->getVaueOfArray($param,'replace');
		
		 $result=array();
		 $basepath=$this->getPathDir($param);
		 $this->createDir($basepath);
		 
		 $filename=$this->makeFileName($param);
		 $fpathfile= $basepath.'/'.$filename;
		 if($onlycheckexist){
			if(!file_exists($fpathfile)){$result['filecreated']=0;}
			else {$result['filecreated']=1;}
			return $result;			
		 }
		 $previouscontent=array();
		 if(!$replace){$previouscontent=$this->load($param);}
		 $items=$this->getUtildata()->getVaueOfArray($param,'items');
		 $item=$this->getUtildata()->getVaueOfArray($param,'item');
		 
		 if(!is_array($items)){ $items=array();}
		 if(!is_array($item)){ $item=array();}
		 $content=array();
		 if(!$replace){
			 $content= array_replace($previouscontent,$items);
			 $content= array_replace($content,$item);
		 }else{
			if(sizeof($item)==0){$content=$items;} 
			else {$content= array_replace($items,$item); }
		 }
		 
		  $content="<?php\n\$_badiusystemcorecache = " . var_export($content, true) . ";\n?>";
		 $file = fopen($fpathfile, "w");
		 fwrite($file,$content);
		fclose($file);
		 $result['filecreated']=1;
	  }
	  public function load($param) {
		 $basepath=$this->getPathDir($param);
		 $filename=$this->makeFileName($param);
		 $fpathfile= $basepath.'/'.$filename;
		 $_badiusystemcorecache=array(); 
		 
		 if (file_exists($fpathfile)) {
			require($fpathfile);
		 } 
		 if(!is_array($_badiusystemcorecache)){$_badiusystemcorecache=array();}
		 return  $_badiusystemcorecache; 
		 
		 
	  }
	  
	  public function getCache($key) {
		  
		  if(sizeof($this->param)==0){
			  if(empty($entity)){$entity=$this->getDentity();} 
			  if(empty($entity)){return null;}
			  $param=array('type'=>'cache','entity'=>$entity);
			  $this->init($param);
		  }
		  $modulekey=null;
		  $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		  if(!empty($parentid)){
			 $modulekey= $key.'.p.'.$parentid;
		  }
		 if(!empty($modulekey) && $this->existKey($modulekey)){
			 return $this->getValue($modulekey);
		 }
		return $this->getValue($key);
       
     }
	 public function add($key,$value) {
        if(!isset($this->param) || (empty($this->param))){$this->param=array();}
         $this->param[$key]=$value;
     }
     
   public function existKey($key) {
        $r=FALSE;
        if(!empty($this->param)){
           $r=array_key_exists($key,$this->param);
        } 
         return  $r;
     }
     
     public function existValue($value) {
          $r=FALSE;
        if(!empty($this->param)){
           $r=in_array($value,$this->param);
        } 
       
         return  $r;
     }
     
      public function getValue($key) {
          $r="";
        if($this->existKey($key)){
           $r=$this->param[$key];
        } 
        return  $r;
     }
     
     public function exist() {
         $cont=0;
         foreach ($this->param as $value) {
             $cont++;
             if($cont==1){return 1;}
         }
       
        return   $cont;
     }
     
     public function getParam() {
         return $this->param;
     }

     public function setParam($param) {
         $this->param = $param;
     }
	 
	  public function getFile() {
         return $this->file;
     }

     public function setFile($file) {
         $this->file = $file;
     }
	 
	   public function getSubdir() {
         return $this->subdir;
     }

     public function setSubdir($subdir) {
         $this->subdir = $subdir;
     }

	public function getDentity() {
         return $this->dentity;
     }

     public function setDentity($dentity) {
         $this->dentity = $dentity;
     }
}
