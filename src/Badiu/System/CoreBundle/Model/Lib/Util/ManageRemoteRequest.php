<?php 
namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class ManageRemoteRequest extends BadiuModelLib{
   

    function __construct(Container $container) {
          parent::__construct($container);
     } 
    
     function skipSession() {
		
        $request = $this->getContainer()->get('request');
        $currentroute = $request->get('_route');
       
        $service=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_service'); 
      
        $list = array();
        $result=false;
        
        array_push($list, 'badiu.system.scheduler.task.cron'); 

        if($service=='badiu.auth.core.webservice.navegation'){
            array_push($list, 'badiu.system.core.service.process');  
        }else if($service=='badiu.system.core.functionality.content.service'){
            array_push($list, 'badiu.system.core.service.process');  
        }else if($service=='badiu.system.core.functionality.exec.service'){
            array_push($list, 'badiu.system.core.service.process');  
        }else if($service=='badiu.moodle.core.client.lib.moodleservice'){
            array_push($list, 'badiu.system.core.service.process');  
        }else if($service=='badiu.moodle.mreport.client.lib.moodleservice'){
            array_push($list, 'badiu.system.core.service.process');  
        }
        else if($service=='badiu.moodle.core.lib.remoteaccess'){
            array_push($list, 'badiu.system.core.service.process');  
        }
		/*else if($service=='badiu.system.scheduler.task.lib.report'){
            array_push($list, 'badiu.system.core.service.process');  
        }
		*/ 
		/*else if($service=='badiu.moodle.core.formdataoptions'){
            array_push($list, 'badiu.system.core.service.process');  
        }*/else if($service=='badiu.system.core.lib.util.cleandata'){
            array_push($list, 'badiu.system.core.service.process');  
        }else if($service=='badiu.moodle.mreport.schedulertasksite.data'){
            array_push($list, 'badiu.system.core.service.process');  
        }else if($service=='badiu.moodle.mreport.lib.freportlib'){
            array_push($list, 'badiu.system.core.service.process');  
        }/*else if($service=='badiu.admin.openai.lib.requestservice'){
            array_push($list, 'badiu.system.core.service.process');  
        } */
		
		
        foreach ($list as $l) {
			if ($l == $currentroute) {$result = true;break;}
        }
      
        return $result;
     }

     function processService() {
        $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters(); 
		
        $serviceaddress=$this->getUtildata()->getVaueOfArray($param,'_service');
        $function=$this->getUtildata()->getVaueOfArray($param,'_function');
        if(empty($function)){$function="exec";}
    
        $response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
      
        if(!$this->getContainer()->has($serviceaddress)){
            return $response->denied('badiu.system.service.error.service.is.not.valid',"the service with address $serviceaddress  not found");
        }
        
        $service=$this->getContainer()->get($serviceaddress);
        if (!method_exists($service, $function)) {
            return $response->denied('badiu.system.service.error.function.is.not.valid',"the  function $function not exist in  service with address $serviceaddress ");
        }
        $result=$service->$function();
       return $result;

     }
}

?>
