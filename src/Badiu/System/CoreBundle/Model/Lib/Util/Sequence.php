<?php

namespace Badiu\System\CoreBundle\Model\Lib\Util;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 *
 * @author lino
 */
class Sequence {

    /**
     * @var Container
     */
    private $container;

     /**
     * @var array
     */
    private $listseq;
    
    function __construct(Container $container) {
        $this->container = $container;
        $this->listseq=array();
        $this->listseq['ABC']= array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $this->listseq['abc']= array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    }
/*
public function exec(){
   $r=$this->next(50, 'III');
   return $r;
}  */  
    public function next($sequencenumber, $type) {
       
        $result=null;
         if($type=='ABC' || $type=='abc'){
              if($sequencenumber==0 || $sequencenumber > 26){return $sequencenumber;}
            $p=$sequencenumber-1;
            $result=$this->listseq[$type][$p];
        }else if($type=='III'){
             $result=$this->convertRomanNumber($sequencenumber);
        }
        return $result;
    }

    function convertRomanNumber($numero) {
        if ($numero <= 0 || $numero > 3999) {
            return $numero;
        }

        $n = (int) $numero;
        $y = '';

        // Nivel 1
        while (($n / 1000) >= 1) {
            $y .= 'M';
            $n -= 1000;
        }
        if (($n / 900) >= 1) {
            $y .= 'CM';
            $n -= 900;
        }
        if (($n / 500) >= 1) {
            $y .= 'D';
            $n -= 500;
        }
        if (($n / 400) >= 1) {
            $y .= 'CD';
            $n -= 400;
        }

        // Nivel 2
        while (($n / 100) >= 1) {
            $y .= 'C';
            $n -= 100;
        }
        if (($n / 90) >= 1) {
            $y .= 'XC';
            $n -= 90;
        }
        if (($n / 50) >= 1) {
            $y .= 'L';
            $n -= 50;
        }
        if (($n / 40) >= 1) {
            $y .= 'XL';
            $n -= 40;
        }

        // Nivel 3
        while (($n / 10) >= 1) {
            $y .= 'X';
            $n -= 10;
        }
        if (($n / 9) >= 1) {
            $y .= 'IX';
            $n -= 9;
        }
        if (($n / 5) >= 1) {
            $y .= 'V';
            $n -= 5;
        }
        if (($n / 4) >= 1) {
            $y .= 'IV';
            $n -= 4;
        }

        // Nivel 4
        while ($n >= 1) {
            $y .= 'I';
            $n -= 1;
        }

        return $y;
    }

    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }

    function getListseq() {
        return $this->listseq;
    }

    function setListseq($listseq) {
        $this->listseq = $listseq;
    }


}
