<?php
namespace Badiu\System\CoreBundle\Model\Lib\Util;
class SysKey  {
    
      
      public function makeHash($numberchar){ 
                $listchar='ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789abcdefghijklmntuvxyz'; 
		$coutnchar = strlen($listchar); 
		$coutnchar--; 
		$thash=NULL; 
		for($x=1;$x<=$numberchar;$x++){ 
			$pos = rand(0,$coutnchar); 
			$thash .= substr($listchar,$pos,1); 
		} 
		return $thash; 
	} 
     
      
     public function verifyingDigitSumTemplate1($txt){
                $caractere= str_split($txt);
                $peso=3;
		$soma=0;
		$w=0;
		$z=0;
		for ($y=0;$y<sizeof($caractere);$y++)
			{	
				$w=ord($caractere[$y]);
				$z=$w*$peso;
				$soma+=$z;
				$peso++;
				if($peso>8){$peso=3;}
			}
		return $soma;
      }
      
        public function verifyingDigitSumTemplate2($txt){
           $caractere= str_split($txt);
           $y=  sizeof($caractere);
	   $peso=9;
           $soma=0;
	    $w=0;
	    $z=0;
	   while($y>0)
			{	
				$w=intval($caractere[$y-1]);
				$z=$w*$peso;
				$soma+=$z;
				$y--;
				$peso--;
				if($peso<4){$peso=9;}
			}
		return $soma;
       }
       
       //verifying digit
       public function verifyingDigitTemplate11($numbersum){
           	$dv=0;
		$resto=$numbersum%11;
		$dv=11-$resto;
		if($dv==0 || $dv==10 || $dv==11){$dv=1;}
	
		return $dv;
          
       }
       
        public function  makeVerifyingDigit($txt,$limitdigit=30){
           $sum1=$this->verifyingDigitSumTemplate1($txt);
          $dv1=$this->verifyingDigitTemplate11($sum1);
          $ntxt=$txt."".$dv1;
          $value="";
          for($i=0;$i<=$limitdigit-1;$i++){
              $sumd=$this->verifyingDigitSumTemplate2($ntxt);
              $dvd=$this->verifyingDigitTemplate11($sumd);
              $ntxt=$ntxt."".$dvd;
              $value=$value."".$dvd;
          }
          return $value;
            
       }   

  
    
}
