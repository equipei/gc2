<?php


namespace Badiu\System\CoreBundle\Model\Lib\Util;

class SpecialCharactere{
    
      public function encodeForJavaScriptArray($text) {
            $result=str_replace('[','\[',$text);
            $result=str_replace(']','\]',$result);
            $result=str_replace('"','\"',$result);
            return $result;
      }
	function removeSpecialChar($text) {
		$utf8 = array(
			'/[áàâãªä]/u'   =>   'a',
			'/[ÁÀÂÃÄ]/u'    =>   'A',
			'/[ÍÌÎÏ]/u'     =>   'I',
			'/[íìîï]/u'     =>   'i',
			'/[éèêë]/u'     =>   'e',
			'/[ÉÈÊË]/u'     =>   'E',
			'/[óòôõºö]/u'   =>   'o',
			'/[ÓÒÔÕÖ]/u'    =>   'O',
			'/[úùûü]/u'     =>   'u',
			'/[ÚÙÛÜ]/u'     =>   'U',
			'/ç/'           =>   'c',
			'/Ç/'           =>   'C',
			'/ñ/'           =>   'n',
			'/Ñ/'           =>   'N',
			'/–/'           =>   'a', // UTF-8 hyphen to "normal" hyphen
			'/_/'           =>   'a', 
			'/[’‘‹›‚]/u'    =>   'a', // Literally a single quote
			'/[“”«»„]/u'    =>   'a', // Double quote
			'/ /'           =>   'a', // nonbreaking space (equiv. to 0x160)
		); 
		$text= preg_replace(array_keys($utf8), array_values($utf8), $text);
		$text= preg_replace('/[^A-Za-z0-9. -]/', 'a', $text); 
		return $text;
 
	}

}
