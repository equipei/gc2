<?php


namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
/**
  *
 * @author lino
 */
class JsonSyncResponse{
    
  /**
     * @var Container
     */
    private $container;
 
	private $status;
	private $info;
	private $message;
	
    function __construct(Container $container) {
                $this->container=$container;
				$this->status='';
				$this->info='';
				$this->message='';
      }
       
      
      public function get() {
			$response=array();
			$response['status']=$this->status;
			$response['info']=$this->info;
			$response['message']=$this->message;
			return $response;
      }
      public function accept($message,$info='') {
			$response=array();
			$response['status']=SERVICEDOMAINTABLE::$REQUEST_ACCEPT;
			$response['info']=$info;
			$response['message']=$message;
			return $response;
      } 
      public function denied($info,$message='') {
			$response=array();
			$response['status']=SERVICEDOMAINTABLE::$REQUEST_DENIED;
			$response['info']=$info;
			$response['message']=$message;
			return $response;
      }
      
      public function set($data) {
                        if(is_array($data)){
                            if(isset($data['status'])){ $this->status=$data['status'];}
                            if(isset($data['info'])){ $this->info=$data['info'];}
                            if(isset($data['info'])){ $this->info=$data['info'];}
                            if(isset($data['message'])){ $this->message=$data['message'];}
                           
                        }
			
      }
      
	   public function getStatus() {
          return $this->status;
      }


      public function setStatus($status) {
          $this->status = $status;
      }
	  
	   public function getInfo() {
          return $this->info;
      }


      public function setInfo($info) {
          $this->info = $info;
      }
	   public function getMessage() {
          return $this->message;
      }


      public function setMessage($message) {
          $this->message = $message;
      }
      public function getContainer() {
          return $this->container;
      }


      public function setContainer(Container $container) {
          $this->container = $container;
      }


}
