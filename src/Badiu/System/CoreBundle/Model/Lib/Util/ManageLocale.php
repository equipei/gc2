<?php
namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**
  *
 * @author lino
 */
class ManageLocale  extends BadiuModelLib{
	private $locale;
	function __construct(Container $container) {
        parent::__construct($container);
		$this->locale="";
	 }
	 
	/*
	 $level = 1 - check locale configured first. If not configured check server contex 
	 $level = 2 - configured check server contex  
	 */
	function getContexLocale($level=1){
		if($level==1 &&!empty($this->locale) && strlen($strlang) == 5){
			$lang=substr($this->locale,0,2);
			$country=substr($this->locale,3,2);
			$conf=array('lang'=>$lang,'country'=>$country,'countrylang'=>$this->locale);
			return $conf;
		}
		$strlang="";
		if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
			$strlang=$_SERVER['HTTP_ACCEPT_LANGUAGE'];
		}
		if(empty($strlang)){return array();}
		if(strlen($strlang) < 4){return array();}
		$lang=substr($strlang,0,2);
		$country=substr($strlang,3,2);
		$countrylang=$lang."-".$country;
		$conf=array('lang'=>$lang,'country'=>$country,'countrylang'=>$countrylang);
		
		return $conf;
	}
	
	function getCountry($level=1) {
		$info=$this->getContexLocale($level);
		$country=$this->getUtildata()->getVaueOfArray($info,'country');
		return $country;
	}

	public function getLocale() {
		return $this->locale;
	}
	public function seLlocale($locale) {
		$this->locale = $locale;
	}	
}
