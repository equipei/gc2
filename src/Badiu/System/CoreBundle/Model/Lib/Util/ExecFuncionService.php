<?php


namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**
 * @author lino
 */
class ExecFuncionService extends BadiuModelLib{
    
  
   function __construct(Container $container) {
                parent::__construct($container);
		  } 
       
      
      public function exec($service,$function,$param) {
           $value=null;
          if ($this->getContainer()->has($service)) {
               $dservice=$this->getContainer()->get($service);
               if (method_exists($dservice,'setSessionhashkey')) {$dservice->setSessionhashkey($this->getSessionhashkey());}
              if (method_exists($dservice, $function) && !empty($param)) {$value= $dservice->$function($param);}
          }
         
          
        
          return $value;
      }
      
	public function execsrt($serviceaddress,$param,$systemdata=null){
		$result=$param;
		$servicekey=null;
		$function=null;
		$service=null;
		$eresult=null;
		
		if(!empty($serviceaddress)){
			$pos=stripos($serviceaddress, "|");
            if($pos!== false){
				$p=explode("|",$serviceaddress);
				$servicekey=$this->getUtildata()->getVaueOfArray($p,0);
				$function=$this->getUtildata()->getVaueOfArray($p,1);
			}
			if(empty($function)){
				$pos=stripos($serviceaddress, "/");
				if($pos!== false){
					$p=explode("/",$serviceaddress);
					$servicekey=$this->getUtildata()->getVaueOfArray($p,0);
					$function=$this->getUtildata()->getVaueOfArray($p,1);
				}
			}
			if(empty($servicekey)){$servicekey=$serviceaddress;}
			if(empty($function)){$function='exec';}
		}
		if(! empty($servicekey) && $this->getContainer()->has($servicekey)){
			$service=$this->getContainer()->get($servicekey);
			
			if(method_exists($service,'setSystemdata')){
				$service->setSystemdata($systemdata);
			}
			if(method_exists($service,$function)){
				if(!empty($param)){$result=$service->$function($param);}
				else {$result=$service->$function();}
				
			}
			
		}
	  return $result;
	}
     
    
}
