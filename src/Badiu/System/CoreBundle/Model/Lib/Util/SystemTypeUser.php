<?php 
namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class SystemTypeUser extends BadiuModelLib{
   
    
     private $id;
     private $type; // user | enterprise
     private $data;
    function __construct(Container $container) {
          parent::__construct($container);
     } 
    
     public function init($type,$id){
        $this->type=$type;
        $this->id=$id;
        $this->getInfo();
    }

    public function getInfo(){
      $userdata=null;
      $info=null;
      if($this->type=='user'){
         $userdb = $this->getContainer()->get('badiu.system.user.user.data');
         $this->data=$userdb->getInfoForSync($this->getId());
      }
      else if($this->type=='enterprise'){
         $userdb = $this->getContainer()->get('badiu.admin.enterprise.enterprise.data');
         $this->data=$userdb->getInfoForSync($this->getId());
      }
      
    }

    function getAddress() {
      $address = null;
      $useraddress = $this->getUtildata()->getVaueOfArray($this->getData(),'contactdata');
      $useraddress = $this->getJson()->decode($useraddress, true);
      if($this->type=='user'){
         $useraddress = $this->getUtildata()->getVaueOfArray($useraddress,'personal');
      }   
      else if($this->type=='enterprise'){
         $useraddress = $this->getUtildata()->getVaueOfArray($useraddress,'default');
      }
      
      return $useraddress;
  }
 
  
  function getIdentification($withaddress=true) {
   $identification=array();
     if($this->type=='user'){
      $userid = $this->getUtildata()->getVaueOfArray($this->getData(),'id');
	  $name = $this->getUtildata()->getVaueOfArray($this->getData(),'firstname');
      $name.=" ". $this->getUtildata()->getVaueOfArray($this->getData(),'lastname');
      $email = $this->getUtildata()->getVaueOfArray($this->getData(),'email');
      $doctype = $this->getUtildata()->getVaueOfArray($this->getData(),'doctype');
      $docnumber = $this->getUtildata()->getVaueOfArray($this->getData(),'docnumber');
	  $dateofbirth = $this->getUtildata()->getVaueOfArray($this->getData(),'dateofbirth');
	  $nationalitystatus = $this->getUtildata()->getVaueOfArray($this->getData(),'nationalitystatus');
      $identification = array("email" => $email, "name" => $name, "doctype" => $doctype, "docnumber" => $docnumber,"dateofbirth"=>$dateofbirth,"nationalitystatus"=>$nationalitystatus);
   }   
   else if($this->type=='enterprise'){
      $userid = $this->getUtildata()->getVaueOfArray($this->getData(),'id');
      $name = $this->getUtildata()->getVaueOfArray($this->getData(),'name');
      $email = $this->getUtildata()->getVaueOfArray($this->getData(),'email');
      $doctype = $this->getUtildata()->getVaueOfArray($this->getData(),'doctype');
      $docnumber = $this->getUtildata()->getVaueOfArray($this->getData(),'docnumber');
      $identification= array("email" => $email, "name" => $name, "doctype" => $doctype, "docnumber" => $docnumber);
   } 
     
   
      
      if($withaddress){
         $address=$this->getAddress();
         if(is_array($address)){
            foreach ($address as $key => $row) {
				if (array_key_exists($key, $identification) && !empty($identification[$key])){}
				else {$identification[$key]=$row;} 
               
            }
        }
      }
	   
	    
        return $identification; 
    } 
    
    public function validateDoc($doctype,$docnumber){
         if($doctype=='CPF'){
            $cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
            $valid = $cpfdata->isValid($docnumber);
            return $valid;
         }else if($doctype=='CNPJ'){
            $cnpjdata = $this->getContainer()->get('badiu.util.document.role.cnpj');
            $valid = $cnpjdata->isValid($docnumber);
           return $valid;
         }

         return false;
    }

    public function cleanFormatDoc($doctype,$docnumber){
      if($doctype=='CPF'){
         $cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
         $docnumber= $cpfdata->clean($docnumber);
         return $docnumber;
      }else if($doctype=='CNPJ'){
         $cnpjdata = $this->getContainer()->get('badiu.util.document.role.cnpj');
         $docnumber = $cnpjdata->clean($docnumber);
        return $docnumber;
      }

      return $docnumber;
 }
    public function getId(){
        return  $this->id;
     }
     public function setId($id){
          $this->id=$id;
   }
  
   public function getType(){
      return  $this->type;
   }
   public function setType($type){
        $this->type=$type;
  }
   
  public function getData(){
    return  $this->data;
 }
 public function setData($data){
      $this->data=$data;
}
}

?>
