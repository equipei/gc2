<?php


namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**
  *
 * @author lino
 */
class ManageString extends BadiuModelLib {
    
	private $pcontent;
   function __construct(Container $container) {
        parent::__construct($container);
		$this->pcontent=array();
    }

	
	function getExpressions($text,$charstart,$charend,$sum=1) {
		$exec=true;
		$this->pcontent=array();
		$cont=0;
		while($exec){
			$partial=$this->getFirstPartialText($text,$charstart,$charend,$sum);
			if(empty($partial)){$exec=false;break;}
			$newtext='[[#'.$cont.'#]]';
			$this->pcontent[$cont]=array('currentexpression'=>$partial, 'tempexpression'=>$newtext);
			$text=$this->replaceItem($text,$charstart,$charend,$sum,$newtext);
			$cont++;
		}
		return $text;
	}
	function replaceText($text,$charstart,$charend) {
		$exec=true;
		while($exec){
			$partial=$this->getFirstPartialText($text,$charstart,$charend);
			if(empty($partial)){$exec=false;break;}
			$text=$this->replaceItem($text,$charstart,$charend);
		}
		return $text;
  }
   function replaceItem($text,$charstart,$charend,$sum,$newtext) {
		$partial=$this->getFirstPartialText($text,$charstart,$charend,$sum);
	
		if(empty($partial)){return $text;}
		$text=str_replace($partial,$newtext,$text);
		return $text;
}
	function getFirstPartialText($text,$charstart,$charend,$sum=1) {
	
		$startposition=strpos($text,$charstart,0);
		if($startposition===FALSE){return null;}
		$endposition=strpos($text,$charend,$startposition);
		$tsize=$endposition-$startposition+$sum;
		$partial=substr($text, $startposition,$tsize); 
		return $partial;
	}


	public function getPcontent() {
		return $this->pcontent;
	}
	public function setPcontent($pcontent) {
		$this->pcontent = $pcontent;
	}
}
