<?php 
namespace Badiu\System\CoreBundle\Model\Lib\Util;
class Cript  {
    private $key="FORA53XExXpTOktYc9j4RcDMq+ikaOjXcRRKexhfULHRb25gDBZdSEh0lbmBXM0M8O8t1tA2PVyVmTkDCtHb5eDA770Znu7lSy4yctIlAnt0Ue5CEDVUGbhKbUFSK8lmnZL0eW0T3I38G64APjG4V60CHaE0ORT5n3J94yAKBUx5aT89YMm5u5H0EHZFVGRfzglFKzieHv7aLVAHkmCfkHCAv6LMmBWxSlXAjNNDCftfsuHz+wQCDvN07zBADIU";
    private $method="AES-256-OFB";
    
    function __construct($key=null) {
        if(!empty($key)){$this->key=$key;}
     } 
     
     function encode($txt) {
        $lenght=openssl_cipher_iv_length($this->method);
        $iv=openssl_random_pseudo_bytes($lenght);
        $txtcripted=openssl_encrypt($txt ,$this->method,$this->key,OPENSSL_RAW_DATA, $iv);
        $txtcripted=base64_encode($iv.$txtcripted);
        return $txtcripted;
     }
     
     function decode($txt) {
         $lenght=openssl_cipher_iv_length($this->method);
         $txtcripted=base64_decode($txt);
         $iv=mb_substr($txtcripted, 0,$lenght, '8bit');
         $txtcripted=mb_substr($txtcripted, $lenght, null, '8bit');
         $txtorigin=openssl_decrypt($txtcripted,$this->method,$this->key,OPENSSL_RAW_DATA, $iv);
        return $txtorigin;
     }
     
     function getKey() {
         return $this->key;
     }

     function getMethod() {
         return $this->method;
     }

     function setKey($key) {
         $this->key = $key;
     }

     function setMethod($method) {
         $this->method = $method;
     }


}

?>
