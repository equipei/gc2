<?php


namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;
/**
 * Description of MakeSelect
 *
 * @author lino
 */
class BadiuSystemOperation{
    
  /**
     * @var Container
     */
    private $container;
 
    /**
     * @var string
     */
    private $route;

    function __construct(Container $container) {
                $this->container=$container;
       }
       
      public function initCurrentRoute() {
            $this->route=$this->container->get('request')->get('_route');
      }

      public function getOperation($route=null) {
          if(!empty($route)){$this->route=$route;}
          else{$this->initCurrentRoute();}
         
          $operation=null;
          $currenteRoute = $this->route;
          $listfunc=explode(".",$currenteRoute);
          $endRoute=end($listfunc) ;
          $operation =$endRoute;  
        
          return $operation;
      }

       public function isView($route=null) {
          $operation=$this->getOperation($route);
          $value=false;
          if($operation=='view'){$value=true;}
          
          return $value;
       }

      public function isEdit($route=null) {
          $operation=$this->getOperation($route);
          $value=false;
          if($operation=='edit'){$value=true;}
          return $value;
       }

        public function isAdd($route=null) {
          $operation=$this->getOperation($route);
          $value=false;
          if($operation=='add'){$value=true;}
          return $value;
       }
       public function isIndex($route=null) {
          $operation=$this->getOperation($route);
          $value=false;
          if($operation=='index'){$value=true;}
         
          return $value;
       }
       public function isDashboard($route=null) {
          $operation=$this->getOperation($route);
          $value=false;
          if($operation=='dashboard'){$value=true;}
          return $value;
       }
       public function isFrontpage($route=null) {
          $operation=$this->getOperation($route);
          $value=false;
          if($operation=='frontpage'){$value=true;}
          return $value;
       }
       public function setContainer(Container $container) {
          $this->container = $container;
      }
      public function getContainer() {
          return $this->container;
      }

        public function setRoute($route) {
          $this->route = $route;
      }
      public function getRoute() {
          return $this->route;
      }


}
