<?php 
namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class RemoteConnection extends BadiuModelLib{
   

    function __construct(Container $container) {
          parent::__construct($container);
     } 
    
     function get($param){
        $host=$this->getUtildata()->getVaueOfArray($param,'host');
        $contenttype=$this->getUtildata()->getVaueOfArray($param,'contenttype');
        $querysetring=$this->getUtildata()->getVaueOfArray($param,'querysetring');
        $castarraytoquerystring=$this->getUtildata()->getVaueOfArray($param,'castarraytoquerystring');
        
        $method=$this->getUtildata()->getVaueOfArray($param,'method');
        if($castarraytoquerystring){$querysetring=http_build_query($querysetring);}
        if($method=='GET'){$host=$host.'?'.$querysetring;}
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$host);
       // curl_setopt($ch, CURLOPT_POST, 0);
       // curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
       // curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $contenttype"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
    
        return $result;
     }
    
}

?>
