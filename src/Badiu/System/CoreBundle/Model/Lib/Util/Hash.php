<?php


namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
/**
  *
 * @author lino
 */
class Hash{
    
  /**
     * @var Container
     */
    private $container;
	

    private $listchar='ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789abcdefghijklmntuvxyz'; 
    public $s1domaintable =array(0=>'t',1=>'m',2=>'z',3=>'a',4=>'c',5=>'d',6=>'g',7=>'j',8=>'k',9=>'u');
 

    function __construct(Container $container) {
                $this->container=$container;
      }
      
      function make($numberchar){ 
		$listchar = $this->listchar;
		$coutnchar = strlen($listchar); 
		$coutnchar--; 
		$hash=NULL; 
		for($x=1;$x<=$numberchar;$x++){ 
			$pos = rand(0,$coutnchar); 
			$hash .= substr($listchar,$pos,1); 
		} 
		return $hash; 
  } 
  
  function s1encode($number) {
    $number=$number."";
    $list=str_split($number);
    $newvar="";
    $listkey=$this->s1domaintable;
    foreach ($list as $n){
        $lt=$listkey[$n];
        $newvar.=$lt;
    }
    return $newvar;
}

public function s1decode($key) {
    $listkey=$this->s1domaintable;
    $list=str_split($key);
    $newvar="";
    foreach ($list as $n){
        $index = array_search ($n,  $listkey);
        $newvar.=$index;
    }
    return $newvar;
}
      public function getContainer() {
          return $this->container;
      }


      public function setContainer(Container $container) {
          $this->container = $container;
      }

}
