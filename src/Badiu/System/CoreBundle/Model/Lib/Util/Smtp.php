<?php 
namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class Smtp extends BadiuModelLib{
   
  //  private $entityconf=null; 
     private $paramconfig=null;
     private $usersender=null;
    function __construct(Container $container) {
          parent::__construct($container);
     } 
    
     function init(){
		  
		 //configcore
		 $configcorehost=$this->getContainer()->getParameter('mailer_host');
         $configcoreport=$this->getContainer()->getParameter('mailer_port');
         $configcoreuser=$this->getContainer()->getParameter('mailer_user');
         $configcorepassword=$this->getContainer()->getParameter('mailer_password');
         $configcoresecurity=$this->getContainer()->getParameter('mailer_encryption');
         $configcoreemailsender=$this->getContainer()->getParameter('mailer_from');
         $configcoreusersender=$this->getContainer()->getParameter('mailer_fromname');
		 $configcoreemailreplay=null;
		 if($this->getContainer()->hasParameter('mailer_replay')){$configcoreemailreplay=$this->getContainer()->getParameter('mailer_replay');}
		  
		 $configcoreuserclientname=$this->getContainer()->getParameter('mailer_userclientnameinfromname');
		 //configsystem
		  
		 $configsystemhost=$this->getSystemdata()->getManagecache()->getCache('badiu.system.core.param.config.smtphost');
         $configsystemport=$this->getSystemdata()->getManagecache()->getCache('badiu.system.core.param.config.smtpport');
         $configsystemuser=$this->getSystemdata()->getManagecache()->getCache('badiu.system.core.param.config.smtpuser');
         $configsystempassword=$this->getSystemdata()->getManagecache()->getCache('badiu.system.core.param.config.smtppassword');
         $configsystemsecurity=$this->getSystemdata()->getManagecache()->getCache('badiu.system.core.param.config.smtpsecurity');
         $configsystememailsender=$this->getSystemdata()->getManagecache()->getCache('badiu.system.core.param.config.smtpemailsender');
         $configsystemusersender=$this->getSystemdata()->getManagecache()->getCache('badiu.system.core.param.config.smtpusersender');
		 $configsystememailreplay=$this->getSystemdata()->getManagecache()->getCache('badiu.system.core.param.config.smtpemailreplay');
		   
		 if(empty($configsystemhost)){$configsystemhost=$configcorehost;}
		 if(empty($configsystemport)){$configsystemport=$configcoreport;}
		 if(empty($configsystemuser)){$configsystemuser=$configcoreuser;}
		 if(empty($configsystempassword)){$configsystempassword=$configcorepassword;}
		 if(empty($configsystemsecurity)){$configsystemsecurity=$configcoresecurity;}
		 if(empty($configsystememailsender)){$configsystememailsender=$configcoreemailsender;}
		 if(empty($configsystemusersender)){
			 $configsystemusersender=$configcoreusersender;
			  if($configcoreuserclientname==1 && !empty($this->usersender)){
				$configsystemusersender=$this->usersender;
			  }
		  }
		 if(empty($configsystememailreplay)){$configsystememailreplay=$configcoreemailreplay;}
		 
 
		 $config=array();
         $config['host']=$configsystemhost;
         $config['port']=$configsystemport;
         $config['user']=$configsystemuser;
         $config['password']=$configsystempassword;
         $config['security']=$configsystemsecurity;
         $config['emailsender']=$configsystememailsender;
         $config['usersender']=$configsystemusersender;
		 $config['emailreplay']=$configsystememailreplay;
 
         /*$config=$this->getEntityConfig();
         $host=$this->getUtildata()->getVaueOfArray($config, 'host');
         if (empty($host)){
             $config=$this->getDefaultConfig();
         }*/
         $this->paramconfig=$config;
         return $config;
     }
     /*delete*/
	 /*
     function getDefaultConfig(){
         $config=array();
         $config['host']=$this->getContainer()->getParameter('mailer_host');
         $config['port']=$this->getContainer()->getParameter('mailer_port');
         $config['user']=$this->getContainer()->getParameter('mailer_user');
         $config['password']=$this->getContainer()->getParameter('mailer_password');
         $config['security']=$this->getContainer()->getParameter('mailer_encryption');
         $config['emailsender']=$this->getContainer()->getParameter('mailer_from');
         $config['usersender']=$this->getContainer()->getParameter('mailer_fromname');
         $userclientname=$this->getContainer()->getParameter('mailer_userclientnameinfromname');
        if($userclientname==1 && !empty($this->usersender)){
            $config['usersender']=$this->usersender;
        }
         return  $config;
            
     }*/
     
	  /*delete*/
	 /*
     function getEntityConfig(){
         $config=array();
        
         $data=$this->getContainer()->get('badiu.system.entity.entity.data');
         
         $dconfig=$data->getGlobalColumnValue('dconfig',array('id'=>$this->getEntity(true)));
         $dconfig = $this->getJson()->decode($dconfig, true);
      
         $config['host']=$this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.host',true);
         $config['port']=$this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.port',true);
         $config['user']=$this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.user',true);
         $config['password']=$this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.password',true);
         $config['security']=$this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.security',true);
         $config['emailsender']=$this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.emailsender',true);
         $config['usersender']=$this->getUtildata()->getVaueOfArray($dconfig, 'defaultsmtp.usersender',true);

         $userclientname=$this->getContainer()->getParameter('mailer_userclientnameinfromname');
         if($this->getEntity()==11){ $userclientname=false;}//review set in db
        if($userclientname==1 && !empty($this->usersender)){
            $config['usersender']=$this->usersender;
        }
         return  $config;
         
     }
     */
    
       function sendMail($param){
            
            $to=$this->getUtildata()->getVaueOfArray($param,'recipient.to',true);
            $cc=$this->getUtildata()->getVaueOfArray($param,'recipient.cc',true);
            $bcc=$this->getUtildata()->getVaueOfArray($param,'recipient.bcc',true);
            $subject=$this->getUtildata()->getVaueOfArray($param,'message.subject',true);
            $body=$this->getUtildata()->getVaueOfArray($param,'message.body',true);
            $attach1=$this->getUtildata()->getVaueOfArray($param,'attach.0',true);
          
          
           
          //email of destinary 
          $emailto=$this->castListEmailToArray($to);
          $emailcc=$this->castListEmailToArray($cc);
          $emailbcc=$this->castListEmailToArray($bcc);
         
          if(empty($this->paramconfig)){$this->init();}
          $smttpconf=$this->paramconfig;
         // print_r($smttpconf);exit;  
          $smtphost=$this->getUtildata()->getVaueOfArray($smttpconf, 'host');
          $smtpport=$this->getUtildata()->getVaueOfArray($smttpconf, 'port');
          if(empty($smtpport)){$smtpport=25;}
          $smtpuser=$this->getUtildata()->getVaueOfArray($smttpconf, 'user');
          $smtppasword=$this->getUtildata()->getVaueOfArray($smttpconf, 'password');
          $smtpfromaddress=$this->getUtildata()->getVaueOfArray($smttpconf, 'emailsender');
          $smtpfromname=$this->getUtildata()->getVaueOfArray($smttpconf, 'usersender');
		  $emailreplay=$this->getUtildata()->getVaueOfArray($smttpconf, 'emailreplay');
          $security=$this->getUtildata()->getVaueOfArray($smttpconf, 'security'); //tls|ssl
           
        $transport =null;
      
        if(!empty($security)){ 
            $transport = \Swift_SmtpTransport::newInstance($smtphost, $smtpport,$security)
            ->setUsername($smtpuser)
            ->setPassword($smtppasword);
        }else{
            $transport = \Swift_SmtpTransport::newInstance($smtphost, $smtpport)
            ->setUsername($smtpuser)
            ->setPassword($smtppasword);
        }
       
        
        $mailer = \Swift_Mailer::newInstance($transport);
         
        $message = \Swift_Message::newInstance($subject);
        $message->setFrom(array($smtpfromaddress => $smtpfromname));
		if(!empty($emailreplay)){$message->setReplyTo(array($emailreplay => $smtpfromname));}
        $message->setContentType("text/html");
        $message->setTo($emailto);
       if(!empty($emailbcc)){$message->setBcc($emailbcc);}
       if(!empty($emailcc)){$message->setCc($emailcc);}
        $message->setCharset('UTF-8');
        $message->setBody($body);
        if(!empty($attach1)){$message->attach($attach1);}
         $result=false;
         try {
             $result = $mailer->send($message);
           }   catch (\Swift_TransportException $e) {
            echo "-------ERROR 1--------";
             echo $e->getMessage();
        } catch (Exception $e) {
            echo "-------ERROR 2--------";
             echo $e->getMessage();
        }
        
        return $result;   
      }

      function castListEmailToArray($list) {
        $emailto=null;
        if(empty($list)){return null;}
        $to=str_replace(";",",",$list);
        if(!empty($to)){$to=str_replace(' ','', $to);}
        $pos=stripos($to, ",");
        if($pos=== false){
                $emailto=array($to);
        }else{
                $emailto= explode(",", $to);
        }
        return $emailto;
    }

    /*  function getEntityconf() {
          return $this->entityconf;
      }

      function setEntityconf($entityconf) {
          $this->entityconf = $entityconf;
      }*/
      function getParamconfig() {
          return $this->paramconfig;
      }

      function setParamconfig($paramconfig) {
          $this->paramconfig = $paramconfig;
      }

      function getUsersender() {
        return $this->usersender;
    }

    function setUsersender($usersender) {
        $this->usersender = $usersender;
    }
      

}

?>
