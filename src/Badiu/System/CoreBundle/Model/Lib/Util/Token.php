<?php


namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**
  *
 * @author lino
 */
class Token  extends BadiuModelLib{
    
  /**
     * @var Container
     */
    private $container;
	

    private $userid;
	private $entity;
 

    function __construct(Container $container) {
        parent::__construct($container);
      }
       
		public function serverServiceValidade($token) {
					$result=FALSE;
					$servicedata=$this->getContainer()->get('badiu.admin.server.service.data');
					$result=$servicedata->exitToken($token);
					return $result;
				}
				
      public function init($token) {
			$p = explode("|",$token);
			
			$entity=null;
			$userid=null;
			$tokencode=null;
			if(isset($p[0])) {$entity=$p[0];}
            if(isset($p[1])) {$tokencode=$p[1];}
            if(isset($p[2])) {$userid=$p[2];}
			$this->userid=$userid;
			$this->entity=$entity;
      }
      //review

      public function generate($param) {
            $stoken="";
            $hash=$this->getContainer()->get('badiu.system.core.lib.util.hash');
            $token=$hash->make(30);
            $user=$this->getUtildata()->getVaueOfArray($param, 'user');
            $plugin=$this->getUtildata()->getVaueOfArray($param, 'module');
            $entity=$this->getUtildata()->getVaueOfArray($param, 'entity');
            $instanceid=$this->getUtildata()->getVaueOfArray($param, 'instanceid');
            $stoken="$entity|$token|$user|$plugin|$instanceid";
            return $stoken;
       }

     
     
}
