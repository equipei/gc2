<?php

namespace Badiu\System\CoreBundle\Model\Lib\Util;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;

/**
 *
 * @author lino
 */
class Json {

    /**
     * @var Container
     */
    private $container;

    function __construct(Container $container) {
        $this->container = $container;
    }

    public function decode($input, $outArray) {

        $result = null;
        //if (version_compare(PHP_VERSION, '5.4.0', '>=') && !(defined('JSON_C_VERSION') && PHP_INT_SIZE > 4)) {
        //	$result = json_decode($input, $outArray, 512, JSON_BIGINT_AS_STRING);
        //} else {

        $max_int_length = strlen((string) PHP_INT_MAX) - 1;
        $json_without_bigints = preg_replace('/:\s*(-?\d{' . $max_int_length . ',})/', ': "$1"', $input);
        $result = json_decode($json_without_bigints, $outArray);
        //}
        return $result;
    }

    function escape($value) {
        $escapers = array("\\", "/", "\"",  "'", "\n", "\r", "\t", "\x08", "\x0c");
        $replacements = array("\\\\", "\\/", "\\\"", "\'","\\n", "\\r", "\\t", "\\f", "\\b");
        $result = str_replace($escapers, $replacements, $value);
        return $result;
    }
function unescape($value) {
    $unescapers = array("\\\\", "\\/", "\\\"", "\'", "\\n", "\\r", "\\t", "\\f", "\\b");
    $replacements = array("\\", "/", "\"",  "'", "\n", "\r", "\t", "\x0c", "\x08");
    $result = str_replace($unescapers, $replacements, $value);
    return $result;
}
    public function encode($input) {
        $json = json_encode($input);
        return $json;
    }

    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }

}
