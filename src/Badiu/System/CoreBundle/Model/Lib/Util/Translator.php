<?php
namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Bundle\FrameworkBundle\Translation\Translator as CoreTranslator;
/**
  *
 * @author lino
 */

class Translator  extends CoreTranslator{
	
       /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
	private $systemdata;
	public function __construct(ContainerInterface $container, MessageSelector $selector, $loaderIds = array(), array $options = array()){
		parent::__construct($container,$selector, $loaderIds,$options);
		
	}
	
	
	/**
     * {@inheritdoc}
     *
     * @api
     */
    public function trans($id, array $parameters = array(), $domain = null, $locale = null)
    {
		
        if (null === $locale) {
            $locale = $this->getLocale();
        }
		
		$session=$this->container->get('badiu.system.access.session');
		$session->setHashkey($this->getSessionhashkey());
		$locale =$session->get()->getLang();
		$entity=$session->get()->getEntity();
		
		
		$showlanguagestringkey=$session->getValue('badiu.system.core.core.param.config.showlanguagestringkey');
		if($showlanguagestringkey){
			$paramshowlanguagestringkey=$this->container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_showlangstring');
			if($paramshowlanguagestringkey==1){$showlanguagestringkey=true;}
			else {$showlanguagestringkey=false;}
		}
		if(isset($this->systemdata) && $this->systemdata!=null){
			$cache=$this->systemdata->getManagelang();
			
		}else{
			$cache=$this->container->get('badiu.system.core.lib.util.managecache');
			$param=array('type'=>'language','locale'=>$locale,'entity'=>$entity);
			$cache->init($param); 
		}
		
		$cvalue=$cache->getValue($id);
		$cvalue=str_replace(array_keys($parameters), array_values($parameters), $cvalue);
		
		if(!empty($cvalue)){
			if($showlanguagestringkey){$cvalue.=" [$id]";}
			return $cvalue;
		}
		
        if (null === $domain) {
            $domain = 'messages';
        }

        if (!isset($this->catalogues[$locale])) {
            $this->loadCatalogue($locale);
        }

       $cvalue= strtr($this->catalogues[$locale]->get((string) $id, $domain), $parameters);
	   if($showlanguagestringkey){$cvalue.=" [$id]";}
	  
	   return $cvalue;
    }

   public function getSessionhashkey() {
		$clientsession = $this->container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_clientsession','ajaxws');
		if(!empty($clientsession)){$this->sessionhashkey=$clientsession;}
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }
   
    public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
