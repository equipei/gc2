<?php


namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
  *
 * @author lino
 */
class Autocomplete{
    
  /**
     * @var Container
     */
    private $container;
	

    private $userid;
	private $entity;
 

    function __construct(Container $container) {
                $this->container=$container;
      }
 
     public function getId($text) {
         $p=explode("-",$text);
         $id=null;
         if(isset($p[0])){$id=$p[0];}
         if(!empty($id)){$id=trim($id);}
         return $id;
      }

      
      public function getContainer() {
          return $this->container;
      }


      public function setContainer(Container $container) {
          $this->container = $container;
      }


}
