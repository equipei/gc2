<?php

namespace Badiu\System\CoreBundle\Model\Lib\Util;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 *
 * @author lino
 */
class BadiuString {

    /**
     * @var Container
     */
    private $container;

    function __construct(Container $container) {
        $this->container = $container;
    }

    public function getContainer() {
        return $this->container;
    }

    function castToKey($txt) {

        if (empty($txt)) {
            return $txt;
        }
        $txt = strip_tags($txt); //clean HTML
     
        if (!empty($txt)) {
            $txt = str_replace(' ', '', $txt);
            $txt = str_replace('.', '', $txt);
            $txt = str_replace(',', '', $txt);
            $txt = str_replace(';', '', $txt);
            $txt = strtolower($txt);
            $map = array(
                '�' => 'a',
                '�' => 'a',
                '�' => 'a',
                '�' => 'a',
                '�' => 'e',
                '�' => 'e',
                '�' => 'i',
                '�' => 'o',
                '�' => 'o',
                '�' => 'o',
                '�' => 'u',
                '�' => 'u',
                '�' => 'c',
                '�' => 'A',
                '�' => 'A',
                '�' => 'A',
                '�' => 'A',
                '�' => 'E',
                '�' => 'E',
                '�' => 'I',
                '�' => 'O',
                '�' => 'O',
                '�' => 'O',
                '�' => 'U',
                '�' => 'U',
                '�' => 'C'
            );
        
            $txt = strtr(utf8_decode($txt), $map);
             $txt = utf8_encode($txt);
            $txt = strtolower($txt);
          
        }

        return $txt;
    }
    function replaceExpression($expression,$value,$txt) {
        if($value==null){$value='';}
        if(empty($expression)){return $txt;}
        $txt = str_replace($expression, $value, $txt);
        return $txt;
    }
	
	
    public function setContainer(Container $container) {
        $this->container = $container;
    }

}
