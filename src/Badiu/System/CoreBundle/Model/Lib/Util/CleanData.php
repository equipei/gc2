<?php

namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class CleanData extends BadiuModelLib {
    
   private $permission;
   private $key;
     function __construct(Container $container) {
               parent::__construct($container);
				$this->permission=$this->getContainer()->get('badiu.system.access.permission');
           }
 
 function hasPermission($key=null) {
	 if(!empty($key)){
		 $perm= $this->permission->has_access($key,$this->getSessionhashkey());
		 return $perm;
	 }
	 if(empty($this->key)){return false;}
	$perm= $this->permission->has_access($this->getKey(),$this->getSessionhashkey());
	return $perm;
 }
#system/service/process?_service=badiu.system.core.lib.util.cleandata&_function=removeItems
public function delete($dparam=null) {
	
		$param=$dparam;
		if(empty($param)){$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();}
		$key=$filter=$this->getUtildata()->getVaueOfArray($param,'key');
		$id=$filter=$this->getUtildata()->getVaueOfArray($param,'id');
	;
		$bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
	  
	  $filter=array();
	  $filter['entity']=$this->getEntity();
	  $filter['id']=$id;
	 
	  $keydata=$bkey.'.data';
	  $keyperm=$bkey.'.delete';
	  $haspermi=$this->hasPermission($keyperm);
	  $validkeyservice=$this->getContainer()->has($keydata);
	  $result=null;
	 
	  if( $haspermi && $validkeyservice){
		  $data=$this->getContainer()->get($keydata);
		  $count= $data->countNativeSql($filter,false);
		  if(empty($count)){return null;}
		  $result= $data->delete($filter);
		
	  }
	  
	  
	  return $result;
    }

public function restore($dparam=null) {
		$param=$dparam;
		if(empty($param)){$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();}
		$key=$filter=$this->getUtildata()->getVaueOfArray($param,'key');
		$id=$filter=$this->getUtildata()->getVaueOfArray($param,'id');
	
		$bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
	  
	  $filter=array();
	  $filter['entity']=$this->getEntity();
	  $filter['id']=$id;
	 
	  $keydata=$bkey.'.data';
	  $keyperm=$bkey.'.restore';
	  $haspermi=$this->hasPermission($keyperm);
	  $validkeyservice=$this->getContainer()->has($keydata);
	  $result=null;
	 
	  if( $haspermi && $validkeyservice){
		 $data=$this->getContainer()->get($keydata);
		  $count= $data->countNativeSql($filter,false);
		  if(empty($count)){return null;}
		  $result= $data->restore($filter);
		
	  }
	  
	  
	  return $result;
    }	
	
  public function remove($param) {
	  $result=array();
	  $filter=$this->getUtildata()->getVaueOfArray($param,'filter');
	  $key=$this->getUtildata()->getVaueOfArray($param,'key');
	  $exec=$this->getUtildata()->getVaueOfArray($param,'exec'); 
	  $withoutentity=$this->getUtildata()->getVaueOfArray($param,'withoutentity'); 
	  $bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
	  
	  if(!is_array($filter)){$filter=array();}
	  if(!$withoutentity){$filter['entity']=$this->getEntity();}
	  unset($filter['withoutentity']);
	  
	  $keydata=$bkey.'.data';
	  $keyperm=$bkey.'.remove';
	  $haspermi=$this->hasPermission($keyperm);
	  $validkeyservice=$this->getContainer()->has($keydata);
	  $remove=0;
	  $count=0;
	  if( $haspermi && $validkeyservice){
		  $data=$this->getContainer()->get($keydata);
		  $count= $data->countNativeSql($filter,false);
		if($count > 0 && $exec){$remove= $data->removeBatchNativeSql($filter,false);}
	  }
	  
	  $result['key']=$key;
	  $result['validkeyservice']=$validkeyservice;
	  $result['filter']=$filter;
	  $result['rows']= $count;
	  $result['exec']=$exec;
	  $result['remove']=$remove;
	  $result['haspermission']=$haspermi;
	  return $result;
    }

#/system/service/process?_service=badiu.system.core.lib.util.cleandata&_function=removeItems&syslistkey=badiu.tms.core.cleanalldata&exec=1
  public function removeItems($dparam=null) {
	    $param=$dparam;
		if(empty($param)){$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();}
		$syslistkey=$this->getUtildata()->getVaueOfArray($param,'syslistkey');
		$plistkey=$this->getUtildata()->getVaueOfArray($param,'plistkey');
		$exec=$this->getUtildata()->getVaueOfArray($param,'exec');
		$listkeys=null;
		
		if(!empty($syslistkey)){
			
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$listkeys=$badiuSession->getValue($syslistkey);
			
			$listkeys=preg_split("/\r\n|\n|\r/", $listkeys);
		}else if(!empty($plistkey)){
			if(!empty($plistkey)){
              $pos=stripos($plistkey, ",");
              if($pos=== false){
                  $listkeys=array($plistkey);
              }else{
                  $listkeys= explode(",", $plistkey);
              }
          }
		}
		
		
		$result=array();
		$fparam=$dparam;
		if(empty($dparam)){
			$fparam=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
			unset($fparam['exec']);
			unset($fparam['syslistkey']);
			unset($fparam['plistkey']);
			unset($fparam['_service']);
			unset($fparam['_function']);
			
			
		}
		else {$fparam=$this->getUtildata()->getVaueOfArray($dparam,'filter');}
	

		if(is_array($listkeys)){
			foreach ($listkeys as $key) {
				$pparam=array('filter'=>$fparam,'key'=>$key,'exec'=>$exec);
				$rresult=$this->remove($pparam);
				array_push($result,$rresult);
			}
		}
		
		echo "<pre>";
		print_r($result);
		echo "</pre>";exit;
		return $result;
  }	  
function getPermission() {
    return $this->permission;
}

function setPermission($permission) {
    $this->permission = $permission;
}

 function getKey() {
        return $this->key;
    }

	
	function setKey($key) {
        $this->key = $key;
    }


}
