<?php

namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;


/**
 *
 * @author lino
 */
class FactoryMenu extends BadiuModelLib {

   function __construct(Container $container) {
        parent::__construct($container);
	 }

   public function make($param) {
	   $result=array();
	  // return  $result;
	   if(empty($param)){return  $result;}
	 
	   if(!is_array($param)){ $param= $this->getJson()->decode($param, true); }
	   if(!is_array($param)){return  $result;}
	   
	   $permission=$this->getContainer()->get('badiu.system.access.permission');
	   $permissionedit=$permission->has_access('badiu.tms.offer.classeenrol.edit',$this->getSessionhashkey());
	   $router=$this->getContainer()->get("router");
	   foreach ($param as $row){
		    //{"label": "xx","url": "xx","route": "xx","ckeckperm": 1,"type": "item"}
			 $routekey=$this->getUtildata()->getVaueOfArray($row,'route.key',true);
			 $routeparam=$this->getUtildata()->getVaueOfArray($row,'route.param',true);
			 $ckeckperm=$this->getUtildata()->getVaueOfArray($row,'ckeckperm');
			 
			$hasperm=true;
			 if(!empty($routekey)){
				 if($ckeckperm){ $hasperm=$permission->has_access($routekey,$this->getSessionhashkey());}
				 if($hasperm){
					 if(!is_array($routeparam)){ $routeparam=array();}
					 $rurl=null;
					 if ($router->getRouteCollection()->get($routekey) !== null ) { $rurl=$this->getUtilapp()->getUrlByRoute($routekey,$routeparam);}
					 $row['url']=$rurl;
				 }
				
			 }
			
			 $label=$this->getUtildata()->getVaueOfArray($row,'label');
			 $url=$this->getUtildata()->getVaueOfArray($row,'url');
		
			 if($hasperm && !empty($label) && !empty($url)){array_push($result,$row);}
			
			
	   }
		
	   return  $result;
   }
   
   
}
