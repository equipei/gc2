<?php

namespace Badiu\System\CoreBundle\Model\Lib\Util;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;


/**
 *
 * @author lino
 */
class Data {

    function __construct(Container $container) {
        $this->container = $container;
    }

    public function getVaueOfArray($array, $key, $pointseparator = false) {
        $value = null;
		if (is_object($array)) {$array=(array)$array;}
        if (!$pointseparator) {
            if (isset($array[$key])) {
                $value = $array[$key];
            }
            return $value;
        if ($pos === false) {
        }

        //without separate point $pointseparator=true
        $pos = stripos($key, ".");


            if (isset($array[$key])) {
                $value = $array[$key];
            }
            return $value;
        }

        //with point separator
        $listkey = explode(".", $key);

        $search = true;
        $maxloop = 100;
        $cont = 0;
        $currentvalue = null;
        while ($search) {

            $skey = null;

            if (array_key_exists($cont, $listkey)) {
                $skey = $listkey[$cont];
                if ($cont == 0) {
                    if (is_array($array) && array_key_exists($skey, $array)) {
                        $currentvalue = $array[$skey];
                    } else {
                        $currentvalue = null;
                    }
                } else {
                    if (is_array($currentvalue) && array_key_exists($skey, $currentvalue)) {
                        $currentvalue = $currentvalue[$skey];
                    } else {
                        $currentvalue = null;
                    }
                }
            } else {
                break;
            }

            if ($cont > 100) {
                break;
            }

            $cont++;
        }


        return $currentvalue;
    }


public function castArrayToString($list, $separator=',', $jointtoeachstring=null) {
		if(!is_array($list)){
				if(is_string($list)){
					$v=$list;
					if(!empty($jointtoeachstring)){$v=$jointtoeachstring.$v.$jointtoeachstring;}
					return $v;
				}
			return "";
		}
		$result="";
		$cont=0;
		foreach($list as $v) {
			if(is_string($v)){
				if(!empty($jointtoeachstring)){$v=$jointtoeachstring.$v.$jointtoeachstring;}
				if($cont==0){$result=$v;}
				else{$result.=$separator.$v;}
				$cont++;
			}
		}
		
		return $result;
	}
	public function castStringToArray($text, $separator=',') {
		if($text==""){return null;}
		if(empty($separator)){return null;}
		$result=array();
		 $pos=stripos($text, $separator);
         if($pos=== false){
            $result=array($text);
        }else{
             $result= explode($separator, $text);
        }
		
		return $result;
	}
	public function castTextToArrayByBreakLine($text) {
		if(empty($text)){return null;}
		$list=preg_split("/\r\n|\n|\r/", $text);
		if(!is_array($list)){$list=array($text);}
		else if(sizeof($list)==0){$list=array($text);}
        
		return $list;
	}
	public function existStringInTextList($find,$text,$separator=',') {
		$result=false;
		$list=$this->castStringToArray($text, $separator);
		if(!is_array($list)){return $result;}
		$result=in_array($find,$list);
		return $result;
		
	}
	public function castStringToLabel($text, $separator,$param=array()) {
		$inputsepartor=$this->getVaueOfArray($param,'inputseparator');	
		$outputsepartor=$this->getVaueOfArray($param,'outputseparator');	
		$domaintable=$this->getVaueOfArray($param,'domaintable');
		
		if(empty($inputsepartor)){$inputsepartor=",";}
		if(empty($outputsepartor)){$outputsepartor=",";}
		
		$listcat=$this->castStringToArray($text, $inputsepartor);
		
		$label="";
		if(!is_array($listcat)){return $label;}
		foreach ($listcat as $v) {
				$dlabel=$this->getVaueOfArray($domaintable,$v);
				$separator="$outputsepartor ";
				if($cont==0){$separator="";}
				$label.=$separator.$dlabel;
				$cont++;
			}
		return $label;
	}
	
	public function joinList($list1,$list2) {
		
		if(!is_array($list1) && !is_array($list2)){return $newlist;}
		if(!is_array($list1) && is_array($list2)){return $list2;}
		if(is_array($list1) && !is_array($list2)){return $list1;}
		if(sizeof($list1)==0){return $list2;}
		if(sizeof($list2)==0){return $list1;}
		$newlist=array();
		foreach ($list1 as $item) {array_push($newlist,$item);}
		foreach ($list2 as $item) {array_push($newlist,$item);}
		return $newlist;
	}
	function removeSpecialChar($text) {
		$utf8 = array(
			'/[áàâãªä]/u'   =>   'a',
			'/[ÁÀÂÃÄ]/u'    =>   'A',
			'/[ÍÌÎÏ]/u'     =>   'I',
			'/[íìîï]/u'     =>   'i',
			'/[éèêë]/u'     =>   'e',
			'/[ÉÈÊË]/u'     =>   'E',
			'/[óòôõºö]/u'   =>   'o',
			'/[ÓÒÔÕÖ]/u'    =>   'O',
			'/[úùûü]/u'     =>   'u',
			'/[ÚÙÛÜ]/u'     =>   'U',
			'/ç/'           =>   'c',
			'/Ç/'           =>   'C',
			'/ñ/'           =>   'n',
			'/Ñ/'           =>   'N',
			'/–/'           =>   'a', // UTF-8 hyphen to "normal" hyphen
			'/_/'           =>   'a', 
			'/[’‘‹›‚]/u'    =>   'a', // Literally a single quote
			'/[“”«»„]/u'    =>   'a', // Double quote
			'/ /'           =>   'a', // nonbreaking space (equiv. to 0x160)
		); 
		$text= preg_replace(array_keys($utf8), array_values($utf8), $text);
		$text= preg_replace('/[^A-Za-z0-9. -]/', 'a', $text); 
		return $text;
 
	}
	
	function castHtmlToText($html) {
		$html=str_replace("<br>","\n",$html);
		$html=str_replace("<br/>","\n",$html);
		$html=str_replace("<br />","\n",$html);
		$dom = new \DOMDocument();
		$html=strip_tags($html); 
		$dom->loadHTML("<body>" . $html. "</body>");
		$xpath = new \DOMXPath($dom);
		$node = $xpath->query('body')->item(0);
		return $node->textContent; // text
	}
}
