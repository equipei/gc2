<?php

namespace Badiu\System\CoreBundle\Model\Lib\Util;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 *
 * @author lino
 */
class App {

    /**
     * @var Container
     */
    private $container;

             /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
	
	
     /**
     * @var boolean
     */
    private $isexternalclient=FALSE; 
	private $systemdata;
    function __construct(Container $container) {
        $this->container = $container;
    }

    public function getContainer() {
        return $this->container;
    }
    function setContainer(Container $container) {
        $this->container = $container;
    }

    //delete param resource
   public function getFilePath($bundlefile) { 
         $p=explode(":",$bundlefile);
         $bundle=null;
         $file=null;
        
         if(isset($p[0])){$bundle=$p[0];}
         if(isset($p[1])){$file=$p[1];}
         if(empty($bundle) || empty($file)){ return null;}
         
        $kernel = $this->getContainer()->get('kernel');
     
        $file=$kernel->locateResource('@'.$bundle).$file;
        
        return $file;
    }
	
	public function getFileUrl($sysfile) { 
         $baseurl=$this->getBaseUrl();
         $url= $baseurl."/system/file/get/".$sysfile;
        
        return $url;
    }
	public function getWebviewUrl($path) { 
         $baseurl=$this->getBaseUrl();
		  $baseurl=str_replace("/app_dev.php","",$baseurl);
		  $baseurl=str_replace("/app.php","",$baseurl);
         $url= $baseurl."/webview.php$path";
        
        return $url;
    }
    public  function getBaseUrl($addbaseurl=true) {
		$wwwroot=null;
		$forcehttps=false;
		if($this->getContainer()->hasParameter('badiu.system.core.wwwroot')){$wwwroot=  $this->getContainer()->getParameter('badiu.system.core.wwwroot');}
				
		if(!empty($wwwroot)){return $wwwroot;}
        $url=$this->getContainer()->get('request')->getSchemeAndHttpHost();
        $base=$this->getContainer()->get('router')->getContext()->getBaseUrl();
		
		if($this->getContainer()->hasParameter('badiu.system.core.forcehttps')){$forcehttps=  $this->getContainer()->getParameter('badiu.system.core.forcehttps');}
		if($forcehttps){$url=preg_replace('/http:\/\//i', 'https://',$url);} 
		
		if($addbaseurl){$urlbase =$url.$base;}
	    else {$urlbase =$url;}
		
        return $urlbase;
    }

    public  function getCurrentUrl($withoutquerystring=false) {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $clienttype=$badiuSession->get()->getType();
		
        if($clienttype=='webservice' || $clienttype=='webservicesynceduser'  || $this->getIsexternalclient()){ 
            $currenturl="BADIU_CORE_SERVICE_CLIENTE_URLBASE";
            if(!$withoutquerystring){
                $wsqparam=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
                $queryString = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
                $queryString->setParam($wsqparam);
                $queryString->remove('_tokensession'); 
                $queryString->remove('_param'); 
                $queryString->makeQuery();
                $query=$queryString->getQuery();
                 $currenturl.="?$query";
            }
        }else{
            // $url=$this->getContainer()->get('request')->getSchemeAndHttpHost();
			 $url=$this->getBaseUrl(false);
            $uri=$this->getContainer()->get('request')->getRequestUri();
            $currenturl=$url.$uri;
            if($withoutquerystring){
                $hasquery = strpos($currenturl, "?");
               if ($hasquery !== false) {
               $p= explode("?",$currenturl);    
               if(isset($p[0])){$currenturl=$p[0];}
               }
            }
        }
       
         return $currenturl;
     }
      public  function getServiceUrl() {
         $urlbase=$this->getBaseUrl();
         $urservicebase="$urlbase/system/service/process";
        return $urservicebase;
     }
      public  function getResourseUrl($path) {
       /* $url=$this->getContainer()->get('request')->getSchemeAndHttpHost();
        $resource=$this->getContainer()->get('templating.helper.assets')->getUrl($path);
        $urlbase =$url.$resource;*/
		
		 $resource=$this->getContainer()->get('templating.helper.assets')->getUrl($path);
		 $urlbase=$this->getBaseUrl(false);
         $urlbase.=$resource; 
		 
        return $urlbase;
    }
    public  function getUrlByRoute($routekey,$param=array()) {
        $url="";
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $clienttype=$badiuSession->get()->getType();
        if($clienttype=='webservice' || $clienttype=='webservicesynceduser' || $this->getIsexternalclient()){
			 $baseurl="BADIU_CORE_SERVICE_CLIENTE_URLBASE";
			$wsinfo=$badiuSession->get()->getWebserviceinfo();
			if(isset($wsinfo['client']['defaulturlservice'])){ $baseurl=$wsinfo['client']['defaulturlservice'];}
			
            $param['_key']=$routekey;
            $queryString = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
            $queryString->setParam($param);
            $queryString->remove('_param'); 
            $queryString->remove('_tokensession'); 
            $queryString->remove('_param'); 
            $queryString->makeQuery();
            $query=$queryString->getQuery();
            $url= $baseurl.="?$query";
            
        }else{
            $router=$this->getContainer()->get("router");
            $urlroute=$router->generate($routekey,$param);
           // $url= $this->getContainer()->get('request')->getSchemeAndHttpHost(). $urlroute; 
		   $url=$this->getBaseUrl(false). $urlroute; 
        }

       
        return $url;
     }
     public  function addParamToUrl($url,$key,$value){
         $starParam="";
        if(strpos($url,'?')=== FALSE){
            $starParam="?";
        } 
        $url=$url.$starParam."&$key=$value";
        return $url;
    }

    public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }
   
    function getIsexternalclient() {
        return $this->isexternalclient;
    }

    function setIsexternalclient($isexternalclient) {
        $this->isexternalclient = $isexternalclient;
    }
	
		public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
