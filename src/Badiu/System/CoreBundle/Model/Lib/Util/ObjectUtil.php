<?php


namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
/**
  *
 * @author lino
 */
class ObjectUtil{
    
  /**
     * @var Container
     */
    private $container;
	

    function __construct(Container $container) {
                $this->container=$container;
      }
       
	
      public function castEntityToArrayForForm($object) {
            $newlist=array();
            if(empty($object)){return $newlist; }
            $listobject=get_class_methods($object);
            foreach ($listobject as $method) {
                  $ovalue=false;
                   
                   $start=substr($method,0,3);
                   
                   if($start=='get'){
                        $variable=strtolower(substr($method,3));
                        $data=$object->$method();
                        $type=gettype($data);
                        if($type=='object' && is_a($data, 'DateTime')){$ovalue=$data->getTimestamp();}
                        else if($type=='object' &&  method_exists ($data,'getId' )){$ovalue=$data->getId();}
                        else {$ovalue=$data;}
                        $newlist[$variable]=$ovalue;
                   }
             }
           
                return $newlist;
      }
      
      public function catToArray($object,$revomeSublist=true) {
         ;
                $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
                $result = $serializer->serialize($object, 'json');
                print_r($result);exit;
                $result=json_decode($result, true);
                if($revomeSublist){$result=$this->revomeSublist($result);}
                return $result;
      }
      
       public function revomeSublist($arrayv) {
               $nlist=array();
               foreach ($arrayv as $key => $value) {
                   if(is_array($value) && array_key_exists("id",$value)){
                        $value= $value['id'];
                   }else if(is_array($value) && array_key_exists("lastErrors",$value) && array_key_exists("lastErrors",$value)){
                        $value= $value['id'];
                   }
                   $nlist[$key]= $value;
               }  
                return $nlist;
      }

      function getContainer() {
          return $this->container;
      }

      function setContainer(Container $container) {
          $this->container = $container;
      }


}
