<?php 
namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;

class SystemAppServerCommand extends BadiuModelLib{
   
    

    function __construct(Container $container) {
          parent::__construct($container);
     } 
   
	public function execCommand($command,$param=array()){
		$result="";	
		$rootuserpermcheck=$this->chekUserRootPermission($command);
		if(!empty($rootuserpermcheck)){return $rootuserpermcheck;}
		$session=$this->getContainer()->get('badiu.system.access.session');
		
		if($command=='clearcache'){$result=$this->deleteCache();/*$result=$this->symfonyCommand('cache:clear');*/}
		else if($command=='deletecache'){$result=$this->deleteCache();}
		else if($command=='deleteuserssessions'){$result=$this->deleteSessions();}
		else if($command=='updatedb'){$result=$this->symfonyCommand('doctrine:schema:update --force');}
		else if($command=='updatesystemaappcache'){$result=$session->initCache(array('foceupdate'=>1,'level'=>'system'));}			
		else if($command=='updateentityappcache'){$result=$session->initCache(array('foceupdate'=>1,'level'=>'entity'));}			
		else if($command=='updatebundle'){
			$bundle=$this->getUtildata()->getVaueOfArray($param,'bundle');
			$module=$this->getContainer()->get('badiu.system.module.module.install');
			$result=$module->installBundle('new',$bundle);
			$session=$this->getContainer()->get('badiu.system.access.session');
			//$session->initConfigFromDb();
			$session->initCache(array('foceupdate'=>1,'level'=>'system')); 
			
			$resultinsert=$this->getUtildata()->getVaueOfArray($result,'config.insert',true);
			$resulupdate=$this->getUtildata()->getVaueOfArray($result,'config.update',true);
			$resulupdate=$this->getUtildata()->getVaueOfArray($result,'config.update',true);
			$resulinstallexec=$this->getUtildata()->getVaueOfArray($result,'install.operation',true);
			$result=$this->getTranslator()->trans('badiu.system.core.command.updatebundleresultinstall',array('%module%'=>$bundle,'%insert%'=>$resultinsert,'%update%'=>$resulupdate,'%installexec%'=>$resulinstallexec));
		}else if($command=='importalllanguaguetodb'){
			$translatormanage=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
			 $result=$translatormanage->importall();
		}else if($command=='makegloballanguaguecache'){
			$translatormanage=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
			$fparam=array('level'=>'global','replace'=>1);
			$result=$translatormanage->makeCache($fparam);
		}else if($command=='makeentitylanguaguecache'){
			$translatormanage=$this->getContainer()->get('badiu.system.module.translator.lib.manage');
			$fparam=array('level'=>'entity','entity'=>$session->get()->getEntity(),'replace'=>1);
			$result=$translatormanage->makeCache($fparam);
		}
		return $result;
	}
function symfonyCommand($commandName) {
		 $kernel = $this->getContainer()->get('kernel');
        $app = new Application($kernel);

		$request=$this->getContainer()->get('request');
        $params = $request->get('params');

        if (!is_array($params)) {
            // Bad params
            $params = array();
        }

        $options = $request->get('options');

        if (!is_array($options)) {
            // Bad options
            $options = array();
        }

        $preparedOptions = array();
        foreach ($options as $option => $value) {
            $preparedOptions['--' . $option] = $value;
        }

        $string = $commandName;

        foreach ($params as $param) {
            $string .= ' ' . $param;
        }

        foreach ($preparedOptions as $key => $option) {
            if (empty($value)) {
                $string .= ' ' . $key;
            } else {
                $string .= ' ' . sprintf('%s=%s', $key, $option);
            }
        }

        $input = new StringInput($string);
        $output = new StreamOutput(fopen('php://temp', 'w'));

        // Run the command
        $app->doRun($input, $output);

        rewind($output->getStream());
        $response = stream_get_contents($output->getStream());  
		return  $response;
	} 

 public function deleteFiles($path){
	
		$bpth= $this->getContainer()->get('kernel')->getRootDir();
		$path=$bpth.'/'.$path;
		//echo $path;exit;
		//if(!file_exists($path)){return null;}
		$result="";
		if (PHP_OS === 'Windows'){$result=exec(sprintf("rd /s /q %s", escapeshellarg($path)));}
		else{$result=exec(sprintf("rm -rf %s", escapeshellarg($path)));}
		
		return $result; 
	 }



	 public function deleteCache(){
		 $cont=0;
		 if (function_exists('opcache_reset')) {
			opcache_reset();
			$cont++;
		}
		
		$this->deleteFiles("data/logs/dev.log");
		$cont++;
		$this->deleteFiles("data/logs/prod.log");
		$cont++;
		$this->deleteFiles("data/cache/dev");
		$cont++;
		$this->deleteFiles("data/cache/prod"); 
		$cont++;
		
		
		return 'Files deleted'; 
	 }
	 public function deleteSessions(){
		 $cont=0;
		
		$this->deleteFiles("data/sessions/dev");
		$cont++;
		$this->deleteFiles("data/sessions/prod"); 
		$cont++;
		return 'Files deleted'; 
	 }
	
	public function chekUserRootPermission($command){
		$session=$this->getContainer()->get('badiu.system.access.session');
		$isrootuser=$session->isRootUser();
		$rootcommand=array('clearcache','updatesystemaappcache','deletecache','deleteuserssessions','updatedb','updatebundle','importalllanguaguetodb','makegloballanguaguecache');
		
		if(in_array($command, $rootcommand) && !$isrootuser){ 
			$msg=$this->getTranslator()->trans('badiu.system.core.command.withoutpermission',array('%command%'=>$command));
			return $msg;
		}
		return null;
		
	}

}

?>
