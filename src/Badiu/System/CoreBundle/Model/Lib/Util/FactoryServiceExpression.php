<?php
 

namespace Badiu\System\CoreBundle\Model\Lib\Util;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**
 * @author lino
 */ 
class FactoryServiceExpression extends BadiuModelLib{
    
  
   function __construct(Container $container) {
                parent::__construct($container);
		  } 
       
     
	public  function change($content){
		
		$managestring=$this->getContainer()->get('badiu.system.core.lib.util.managestring');
		$content=$managestring->getExpressions($content,'{badiu.','}');
		
		$content=$this->replaceExpressions($managestring->getPcontent(),$content);
		return  $content;
		
	}
	
     public function replaceExpressions($listexpression,$content){
		if(!is_array($listexpression)){return $content;}
		$newlist=array();
	
		foreach ($listexpression as $key => $value) {
			$expression=$this->getUtildata()->getVaueOfArray($value,'currentexpression');
			$tempexpression=$this->getUtildata()->getVaueOfArray($value,'tempexpression');
			$exprcontent=$this->execServiceExpresssion($expression);
			
			$content=str_replace($tempexpression,$exprcontent,$content);
			
		}
		
	return $content;
  }
    
    public function getExpression($value){
		
			$value=str_replace("{","",$value);
			$value=str_replace("}","",$value);
		
		
		
		return $value;
	}
	
	public function execServiceExpresssion($texexpression){
		 $serviceaddress=$this->getExpression($texexpression);
		 
		
		$result=null;
		$servicekey=null;
		$function=null;
		$service=null;
		$shortname=null;
		$p1=null;
		$p2=null;
		$p3=null;
		$p4=null;
		$p5=null;
		$p6=null;
		$p7=null;
		$p8=null;
		$p9=null;
		$p10=null;
		$eresult=null;
		
		if(!empty($serviceaddress)){
			$pos=stripos($serviceaddress, "|");
            if($pos!== false){
				$p=explode("|",$serviceaddress);
				$servicekey=$this->getUtildata()->getVaueOfArray($p,0);
				$function=$this->getUtildata()->getVaueOfArray($p,1);
				$shortname=$this->getUtildata()->getVaueOfArray($p,2);
				$p1=$this->getUtildata()->getVaueOfArray($p,2);
				$p2=$this->getUtildata()->getVaueOfArray($p,3);
				$p3=$this->getUtildata()->getVaueOfArray($p,4);
				$p4=$this->getUtildata()->getVaueOfArray($p,5);
				$p5=$this->getUtildata()->getVaueOfArray($p,6);
				$p6=$this->getUtildata()->getVaueOfArray($p,7);
				$p8=$this->getUtildata()->getVaueOfArray($p,9);
				$p9=$this->getUtildata()->getVaueOfArray($p,10);
				$p10=$this->getUtildata()->getVaueOfArray($p,11);
				
			}
			
			if(empty($servicekey)){$servicekey=$serviceaddress;}
			if(empty($function)){$function='exec';}
		}
		$param=array();
		if(!empty($shortname)){$param['shortname']=$shortname;}
		if(!empty($p1)){$param[0]=$p1;}
		if(!empty($p2)){$param[1]=$p2;}
		if(!empty($p3)){$param[2]=$p3;}
		if(!empty($p4)){$param[3]=$p4;}
		if(!empty($p5)){$param[4]=$p5;}
		if(!empty($p6)){$param[5]=$p6;}
		if(!empty($p7)){$param[6]=$p7;}
		if(!empty($p8)){$param[7]=$p8;}
		if(!empty($p9)){$param[8]=$p9;}
		if(!empty($p10)){$param[9]=$p10;}
		if(! empty($servicekey) && $this->getContainer()->has($servicekey)){
			$service=$this->getContainer()->get($servicekey);
			if(method_exists($service,$function)){
				if(!empty($param)){$result=$service->$function($param);}
				else {$result=$service->$function();}
				
			}
		}
		
	  return $result;
	}
}
