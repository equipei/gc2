<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Html;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**
 * Description of MakeSelect
 *
 * @author lino
 */
class Link extends BadiuModelLib{
    
  /**
     * @var Container
     */
    private $container;

    /**
     * @var integer
     */
    private $parentid;
  
    /**
     * @var boolean
     */
    private $isexternalclient;
    /**
     * @var boolean
     */
    private $isschedule = FALSE;
	  /**
     * @var boolean
     */
    private $isservice = FALSE;
    function __construct(Container $container) {
        parent::__construct($container);
      }
       
    
       public function makeParamByConfig($var,$position=null){
           $clinkparm=$this->getParamByType($var);
           return $clinkparm;

       }

    public function getParamByType($var){
		
        $listparam=array();
        if(empty($var))  return null;
        $p=array();
        $pos=stripos($var, "|");
        if($pos=== false){
            $p[0]=$var;
        }else{
            $p = explode("|", $var);
        }
       
        //check exist type on config
        $existType=false;
        $type=null;
        // r - route | s - service | l - link | p - route with parent | b - label
        if(isset($p[0]) &&( $p[0]=='r' || $p[0]=='s' || $p[0]=='l' || $p[0]=='p' || $p[0]=='b')){
            $existType=true;
            $type= $p[0];
        }else{
            $type='r';
        }
       //route
        if($type=='r'){
            $route=null;
            $target=null;
            $param=null;
            $lparam=null;
            if($existType){
                if(isset($p[1])) {$route=$p[1];}
                if(isset($p[3])) {$target=$p[3];}
                if(isset($p[2])) {$param=$p[2];}
            }else{
                if(isset($p[0])) {$route=$p[0];}
                if(isset($p[2])) {$target=$p[2];}
                if(isset($p[1])) {$param=$p[1];}
            }

            if(!empty($param)){
                $pos=stripos($param, ",");
                if($pos=== false){
                    $lparam=array($param);
                }else{
                    $lparam= explode(",", $param);
                }
            }
            
			$description=$this->getDescription($route,$lparam);
			$dkeyroute=$this->getDkey($lparam);
            $listparam=array('type'=>$type,'route'=>$route,'target'=>$target,'params'=>$lparam,'description'=>$description,'dkeyroute'=>$dkeyroute);
            return $listparam;
        }

        //service
        if($type=='s'){
            $route=null;
            $service=null;
            $function='d';
            $target=null;
            $param=null;
            $lparam=null;
            if(isset($p[1])) {$route=$p[1];}
            if(isset($p[2])) {$service=$p[2];}
            if(isset($p[3])) {$function=$p[3];}
			if(isset($p[4])) {$param=$p[4];}
            if(isset($p[5])) {$target=$p[5];}

			 if(!empty($param)){
                $pos=stripos($param, ",");
                if($pos=== false){
                    $lparam=array($param);
                }else{
                    $lparam= explode(",", $param);
                }
            }
			
			$description=$this->getDescription($route,$lparam);
			$dkeyroute=$this->getDkey($lparam);
            $listparam=array('type'=>$type,'route'=>$route,'service'=>$service,'function'=>$function,'params'=>$lparam,'target'=>$target,'description'=>$description,'dkeyroute'=>$dkeyroute);
            
            return $listparam;
        }
        //linkd

        //parent param
       else  if($type=='p'){
           $route=null;
           $service=null;
           $function=null;
           $target=null;
          // $param='parentid';
		   $param=null;
           $lparam=null;
         
           if(isset($p[1])) {$route=$p[1];}
           if(isset($p[2])) {$service=$p[2];}
           if(isset($p[3])) {$function=$p[3];}
           if(isset($p[4])) {$target=$p[4];}
		   
		   // if(isset($p[2])) {$param=$p[2];}
		   if(sizeof($p)==3){$param=$p[2];}
           if(!empty($param)){
                $pos=stripos($param, ",");
                if($pos=== false){
                    $lparam=array($param);
                }else{
                    $lparam= explode(",", $param);
                }
            }
		
          $description=$this->getDescription($route,$lparam);
			$dkeyroute=$this->getDkey($lparam);
            $listparam=array('type'=>$type,'route'=>$route,'service'=>$service,'params'=>$lparam,'function'=>$function,'target'=>$target,'description'=>$description,'dkeyroute'=>$dkeyroute);
            return $listparam;
        }
        
        else if($type=='b'){
             
             $route=null;
             $service=null;
             $function=null;
             $target=null;
            
         
            if(isset($p[1])) {$route=$p[1];}
             if(isset($p[2])) {$service=$p[2];}
            if(isset($p[3])) {$function=$p[3];}
            if(isset($p[4])) {$target=$p[4];}
           $dkeyroute="";
             $listparam=array('type'=>$type,'route'=>$route,'service'=>$service,'function'=>$function,'target'=>$target,'description'=>'','dkeyroute'=>$dkeyroute);
               return $listparam;
         }
         
         //parent param
        else if($type=='l'){
           $url=null;
           $param=null;
           $target=null;
           
           if(isset($p[1])) {$url=$p[1];}
           if(isset($p[2])) {$param=$p[2];}
           if(isset($p[3])) {$target=$p[3];}
		   $dkeyroute="";
            $listparam=array('type'=>$type,'url'=>$url,'target'=>$target,'description'=>'','dkeyroute'=>$dkeyroute);
            return $listparam;
        }
        
        return $listparam;
    }
       public function get($info,$row,$data){
		 
            if ($this->getIsschedule()) {return  $data; }
			if ($this->getIsservice()) {return  $data; }
            if(empty($info)) return  $data;
            if(!isset($info['link']))    return  $data;    
            if(!isset($info['link']['route']))    return  $data;



            $query=array();
            $queryparam= $info['link']['params'];
            if(!empty($queryparam)){
                    foreach ($queryparam as $q) {
						
						$pos=stripos($q, "/");
						if($pos!== false){
							$lparam= explode("/", $q);
							$keyp=$lparam[0];
							$value=$lparam[1];
							$query[$keyp]= $this->getValueOfHttpRequestParam($value);
						}else{
							
							$qkey=$this->getParam($q,'key');
							$qroute=$this->getParam($q,'route');
							$pos=stripos($qkey, "__db.");
							if($pos=== 0){$query[$qroute]=$this->getParamDb($qkey,$row);}
							else {
								if(isset($row[$qkey])) {
								$query[$qroute]=$row[$qkey];
							}
							}
							
						}
                    }
            }

            $router=$this->getContainer()->get("router");
            $url=$this->makeUrl($info['link']['route'],$query);
            
              $target=" ";
              if(isset($info['link']['target'])) {
                    $patarget=$info['link']['target'];
                  $target=" target=\"$patarget\" ";
              }
             
              
             $html="";
             $html.="<a href=\"$url\" $target> $data</a>  ";
             return $html;
       }
      
       public function getHtml($key,$plink,$out='html'){
           $html=null;
			//check permission if is menunavbar
			$position=$this->getUtildata()->getVaueOfArray($plink,'permposition');
			$proute=$this->getUtildata()->getVaueOfArray($plink,'route');
			if($position=='navbar'){
				$libperm=$this->getContainer()->get('badiu.system.access.permission');
				$haspermission=$libperm->has_access($proute,$this->getSessionhashkey());
				if(!$haspermission){$plink['type']='b';}
			}
            if($plink['type']=='r'){ $html=$this->getHtmlRoute($key,$plink,$out);}
            else if ($plink['type']=='s'){ $html=$this->getHtmlService($key,$plink,$out);}
            else if ($plink['type']=='p'){ $html=$this->getHtmlRouteParent($key,$plink,$out);}
            else if ($plink['type']=='b'){ $html=$this->getHtmlLabel($key,$plink,$out);}
            else if ($plink['type']=='l'){ $html=$this->getHtmlUrl($key,$plink,$out); } //review it
          
             return $html;
       }


    public function getParam($param,$return='key') {
		
        $pdata=strtolower($param);
        $pos= stripos($pdata," as ");  
        if(!$pos) {return $param;}
        $p= explode(" as ", $pdata);
        if(sizeof($p)!=2) {return $param;}
        if($return=='key') {return trim($p[0]);}
        else  if($return=='route') return trim($p[1]);
    }
	public function getParamDb($keyconf,$row) {
		$keys = explode(".", $keyconf);
		$colunm=$this->getUtildata()->getVaueOfArray($keys,1);
		$columnkey="";
		$value="";
		if(is_array($keys)){
			$cont=0;
			$contkey=0;
			foreach ($keys as $kv){
				
				if($cont > 1){
					if($contkey==0){$columnkey=$kv;}
					else {$columnkey.='.'.$kv;}
					$contkey++;
				}
				$cont++;
			}
		}
		$dconfig=null;
		if(!empty($colunm)){$dconfig=$this->getUtildata()->getVaueOfArray($row,$colunm);}
		
		if(!empty($dconfig)){
			$dconfig = $this->getJson()->decode($dconfig, true);
			$value=$this->getUtildata()->getVaueOfArray($dconfig,$columnkey,true);
		}
		
		return $value;
    }

    public function getHtmlRoute($key,$plink,$out='html'){

        if(empty($key)) return  null;
        if(empty($plink))    return   null;
        if(!isset($plink['route']))    return  null;

        $query=array();
        $queryparam=null;

        if(isset($plink['params'])){$queryparam=$plink['params'];}

        if(!empty($queryparam)){
            foreach ($queryparam as $q) {
                $pos=stripos($q, "/");
				
                if($pos!== false){
                    $lparam= explode("/", $q);
					$keyp=$lparam[0];
                    $value=$lparam[1];
					$value= $this->getValueOfHttpRequestParam($value);
                    $query[$keyp]= $value;
                }
            }
        }

		
        $router=$this->getContainer()->get("router");
        $url=$this->makeUrl($plink['route'],$query);

        $target=" ";
        if(isset($plink['target'])) {
            $patarget=$plink['target'];
            $target=" target=\"$patarget\" ";
        }
        $translator=$this->getContainer()->get('translator');
        $data=$translator->trans($key);
        $html="";
		$classkey= str_replace(".","-",$key);
        if($out=='html'){ $html.="<a href=\"$url\" $target class=\"$classkey\" id=\"$classkey-id\"> $data</a>  ";}
        else if ($out=='elements'){ $html= array('id'=>$classkey,'url'=>$url,'target'=>$target,'name'=>$data,'description'=>$plink['description']);}

        return $html;
    }

     public function getHtmlLabel($key,$plink,$out='html'){

        if(empty($key)) return  null;
        $data=null;
         $function=$this->getUtildata()->getVaueOfArray($plink,'function'); 
        $service=$this->getUtildata()->getVaueOfArray($plink,'service');
      
        if(!empty($service)&& !empty($function)){
            if($this->getContainer()->has($service)){
                
                $dservice=$this->getContainer()->get($service);
                $id=$this->getContainer()->get('request')->get('parentid');
                if (method_exists($dservice, $function)) {
                  $data = $dservice->$function($id);
                  
                  if(is_array($data)){
                      if(isset($data['name'])){$data=$data['name'];}
                  }
                }
            }
         
        }
        if(empty($data)){
             $translator=$this->getContainer()->get('translator');
             $data=$translator->trans($key);
        }
           
       $classkey= str_replace(".","-",$key);
        $html="";
        if($out=='html'){ $html.=$data;}
        else if ($out=='elements'){ $html= array('id'=>$classkey,'url'=>null,'target'=>null,'name'=>$data,'description'=>null);}

        return $html;
    }
public function getHtmlRouteParent($key,$plink,$out='html'){
		
        if(empty($key)) return  null;
        if(empty($plink))    return   null;
        if(!isset($plink['route']))    return  null;

        $function=$plink['function']; 
        $service=$plink['service'];
        $route=$plink['route'];

        $paramRequest="parentid";
        $paramRoute="parentid";
        $paramValue=null;
        $query=array();
        
        $qstringsystem=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem');
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        //dasabled the param parenteid is set on edit and view
       /* if($sysoperation->isEdit()){
            //$paramRequestValue=$this->getContainer()->get('request')->get('id');
			$paramRequestValue=$qstringsystem->getParameter('id');
            $execfuncionservice=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice');
            $result=$execfuncionservice->exec($service,$function,$paramRequestValue);
            if($result!=null){
                if(is_array($result)){$paramValue=$result['id'];} 
                else{$paramValue=$result; }
            }
             if($sysoperation->isView($route)){$paramRoute="id";}
        }
        else if($sysoperation->isView()){
            $paramRoute="id";
            $paramRequest="id";
            if(!$sysoperation->isView($route)){$paramRoute="parentid";}
             
            //$paramValue=$this->getContainer()->get('request')->get($paramRequest);
			$paramValue=$qstringsystem->getParameter($paramRequest);
        }
        else{
             if($sysoperation->isView($route)){$paramRoute="id";}
             //$paramValue=$this->getContainer()->get('request')->get($paramRequest);
			 $paramValue=$qstringsystem->getParameter($paramRequest);
        }*/
       
        $paramValue=$qstringsystem->getParameter($paramRequest);
        $query[$paramRoute]= $paramValue;
        if($sysoperation->isView($route)){
            $paramRoute="id";
            $id=$this->getContainer()->get('request')->get('id');
            if(empty($id)){$id=$paramValue;}
             $query['id']= $id;
        }
		
		
		
        $queryparam=null;
		
        if(isset($plink['params'])){$queryparam=$plink['params'];}

        if(!empty($queryparam)){
            foreach ($queryparam as $q) {
                $pos=stripos($q, "/");
				
                if($pos!== false){
                    $lparam= explode("/", $q);
					$keyp=$lparam[0];
                    $value=$lparam[1];
					$value= $this->getValueOfHttpRequestParam($value);
                    $query[$keyp]= $value;
                }
            }
        }
		
       $url=$this->makeUrl($route,$query);
       
        $target=" ";
        if(isset($plink['target'])) {
            $patarget=$plink['target'];
            $target=" target=\"$patarget\" ";
        }
        $translator=$this->getContainer()->get('translator');
        $data=$translator->trans($key);
        $html="";
		$classkey= str_replace(".","-",$key);
        if($out=='html'){ $html.="<a href=\"$url\" $target class=\"$classkey\" id=\"$classkey-id\"> $data</a>  ";}
        else if ($out=='elements'){ $html= array('id'=>$classkey,'url'=>$url,'target'=>$target,'name'=>$data,'description'=>$plink['description']);}

        return $html;
    }
    
    public function getHtmlUrl($key,$plink,$out='html'){
	//revisar inacabado	
        if(empty($key)) return  null;
        if(empty($plink))    return   null;
        if(!isset($plink['url']))    return  null;

        $url=$plink['url'];

       
        $target=" ";
        if(isset($plink['target'])) {
            $patarget=$plink['target'];
            $target=" target=\"$patarget\" ";
        }
       
        $data=$key;
        $html="";
        if($out=='html'){ $html.="<a href=\"$url\" $target> $data</a>  ";}
        else if ($out=='elements'){ $html= array('url'=>$url,'target'=>$target,'name'=>$data,'description'=>$plink['description']);}

        return $html;
    }
    public function getHtmlService($key,$plink,$out='html'){
      
        if(empty($key)) return  null;
        if(empty($plink))    return   null;
        if(!isset($plink['route']))    return  null;
        if(!isset($plink['service']))    return  null;

        $function=$plink['function']; 
        $service=$plink['service'];
        $route=$plink['route'];

		$query=array();
        $queryparam=null;
		
        if(isset($plink['params'])){$queryparam=$plink['params'];}

        if(!empty($queryparam)){
            foreach ($queryparam as $q) {
                $pos=stripos($q, "/");
				
                if($pos!== false){
                    $lparam= explode("/", $q);
					$keyp=$lparam[0];
                    $value=$lparam[1];
					$value= $this->getValueOfHttpRequestParam($value);
                    $query[$keyp]= $value;
                }
            }
        }
		
        $paramRequest="parentid";
        $paramRoute="parentid";

        if($function=='d'){$function="getNameById";}

		$qstringsystem=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem');
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        /*if($sysoperation->isEdit() || $sysoperation->isView()){
            $paramRequest="id";
        }*/

      /*  if($sysoperation->isView($route) || $sysoperation->isEdit($route)){
           $paramRoute="id";
        }*/

        $paramValue=$this->getContainer()->get('request')->get($paramRequest);
        
        //$paramValue=$qstringsystem->getParameter($paramRequest);
		$paramValue=$qstringsystem->getParameter($paramRequest);
      
        $paramRouteValue=$paramValue;
        
        $label=null;
        $execfuncionservice=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice');
        $execfuncionservice->setSessionhashkey($this->getSessionhashkey());
        $result=$execfuncionservice->exec($service,$function,$paramValue);
     
        if($result!=null){ 
            if(is_array($result)){
                 
                if(isset($result['id'])){$paramRouteValue=$result['id']; }
                if(isset($result['name'])){ $label=$result['name']; }
            }else{
                 $label=$result; 
            }
        }
        $paramtorout=array();
        $paramtorout['parentid']=$paramRouteValue;
        if($sysoperation->isView($route)){
          
            $id=$this->getContainer()->get('request')->get('id');
            if(empty($id)){$id=$paramRouteValue;}
            $paramtorout['id']=$id;
            $query['id']=$id;
        }else{ $query['parentid']=$paramRouteValue;}

         $url=$this->makeUrl($route,$query);   
		//if(sizeof($query)==0){$url=$router->generate($route,array($paramRoute=>$paramRouteValue));}
      //  if(sizeof($query)==0){$url=$this->makeUrl($route,$paramtorout);}
       // else {$url=$this->makeUrl($route,$query);}
        

        $target=" ";
        if(isset($plink['target'])) {
            $patarget=$plink['target'];
            $target=" target=\"$patarget\" ";
        }

		$classkey= str_replace(".","-",$key);
        $html="";
        if($out=='html'){$html.="<a href=\"$url\" $target class=\"$classkey\" id=\"$classkey-id\"> $label</a>  ";}
        else if ($out=='elements'){ $html= array('id'=>$classkey,'url'=>$url,'target'=>$target,'name'=> $label,'description'=>$plink['description']);}
     
        return $html;
    }

    public function getParentidByRoute() {
            $router=$this->getContainer()->get("router");
            $keymanager=$this->getContainer()->get("badiu.system.core.functionality.keymanger");
            $confbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
			$confbundle->setSystemdata($this->getSystemdata());
            $confbundle->setSessionhashkey($this->getSessionhashkey());
            $keymanager->setBaseKey($router);
            $confbundle->setKeymanger($keymanager);
            $confbundle->initKeymangerExtend($keymanager);




    }
     
	    public function getValueOfHttpRequestParam($vparam){
				
				$pos=stripos($vparam, "{");
				if($pos!== false){
					$qstringsystem=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem');
					$vparam=str_replace("{","",$vparam);
					$vparam=str_replace("}","",$vparam);
					//$vparam=$this->getContainer()->get('request')->get($vparam);
					$vparam=$qstringsystem->getParameter($vparam);
				}else{
                                    $formdefultdata=$this->getContainer()->get('badiu.system.core.lib.form.defaultdata');
                                    $formdefultdata->setSystemdata($this->getSystemdata());
									$vparam=$formdefultdata->getValue($vparam);
                                }
				return $vparam;
		 }
                 
       public function makeUrl($route,$query) {
            $url=null;
			
            if(!$this->isexternalclient){
                      $router=$this->getContainer()->get("router");
					   if ($router->getRouteCollection()->get($route) === null ) {return null;}
                     $url=$router->generate($route,$query);
               }
            else{
                $query['_key']=$route;
                $querystring = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
                $querystring->setParam($query);
                $querystring->makeQuery();
                $pquery=$querystring->getQuery();
                $url="BADIU_CORE_SERVICE_CLIENTE_URLBASE?$pquery";
            } 
            return $url;
       } 

       public function getDkey($param) {
            $result=null;
            if(!is_array($param)){return null;}
            $subquerystring1= $this->getContainer()->get('badiu.system.core.lib.http.subquerystring');
            
            foreach ($param as $p){
                $subquerystring1->setQuery($p);
                $subquerystring1->makeParam();
                $value=$subquerystring1->getValue('_dkey');
                if(!empty($value)){
                    $result=$value;
                    break;
                 }
            }
        
            return $result;
       }

	public function getDescription($route,$lparam){
		$keymanger=$this->getContainer()->get("badiu.system.core.functionality.keymanger");
        $translator=$this->getContainer()->get('translator');
        $description=$route;
        $valuedkey=$this->getDkey($lparam);
        if(!empty($valuedkey)){$description=$valuedkey;}
        $description=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($description);
        $description.=".description"; 
		$description=$translator->trans($description);
		return $description;
	}
    /**
     * @return int
     */
    public function getParentid()
    {
        return $this->parentid;
    }

    /**
     * @param int $parentid
     */
    public function setParentid($parentid)
    {
        $this->parentid = $parentid;
    }


    function getIsexternalclient() {
        return $this->isexternalclient;
    }

    function setIsexternalclient($isexternalclient) {
        $this->isexternalclient = $isexternalclient;
    }


    function setIsschedule($isschedule) {
        $this->isschedule = $isschedule;
    }

    function getIsschedule() {
        return $this->isschedule;
    }
 
 	function getIsservice() {
        return $this->isservice;
    }
 function setIsservice($isservice) {
        $this->isservice = $isservice;
    }
}
