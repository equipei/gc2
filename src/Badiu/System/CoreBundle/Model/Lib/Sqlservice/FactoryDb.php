<?php

namespace Badiu\System\CoreBundle\Model\Lib\Sqlservice;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 *
 * @author lino
 */
class FactoryDb {
    
   /**
     * @var Container
     */
    private $container;
   
    
    /**
     * @var array
     */
    private $config;

    
     function __construct(Container $container) {
                $this->container=$container;
                $this->config=array();
      }
 
		public function exec($sql,$type='S',$wp=1,$x=null,$y=null) {
			$param=$this->getParam();
			
			$paging="";
			if($type=='S' && $wp==1){$paging="";}
			else if($type=='M' && $wp==1){$paging="&wp=1&x=$x&y=$y";}
			else if($type=='M' && $wp==0){$paging="&wp=0";}
			$sql=urlencode($sql);
			$ptoken="&tk=".$param->token;
			$servicerurl=$param->servicerurl;
			$pquery="q=$sql&t=$type$paging$ptoken";
			$siteurl=$param->url;
			
			$url=$siteurl.$servicerurl.'?'.$pquery;
			
			
			$data=file_get_contents($url);
			//echo "<br>";		
			//echo $data;exit;
			
			$array=true;
			$result = json_decode($data, $array, 512, JSON_BIGINT_AS_STRING);
		
			//if($type=='S'){$result= $result['countrecord'];}
			return $result;
	
	}
      
	  public function getParam() {
			$param =new \stdClass();
			$param->url="";
			$param->token="";
			$param->servicerurl="";
			
			if(isset($this->config['url'])) {$param->url=$this->config['url'];}
			if(isset($this->config['servicerurl'])) {$param->servicerurl=$this->config['servicerurl'];}
			if(isset($this->config['token'])) {$param->token=$this->config['token'];}
			return $param;
	  }
	  

	   public function getRecord($sql) {
             $result = $this->exec($sql,'S',0); 
             return $result;
       }
       
       public function getRecords($sql,$x=null,$y=null) {
			 $result = $this->exec($sql,'M',1,$x,$y); 
             return $result;
	   }
	   
       public function getContainer() {
           return $this->container;
       }

       public function setContainer(Container $container) {
           $this->container = $container;
       }
       
             
       public function getConfig() {
           return $this->config;
       }

       public function setConfig($config) {
           $this->config = $config;
       }



}
