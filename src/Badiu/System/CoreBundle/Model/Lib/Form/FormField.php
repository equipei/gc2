<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Form;

/**
 * Description of MakeSelect
 *
 * @author lino
 */
class FormField {
    
  /**
     * @var string
     */
    private $name;

        
    /**
     * @var boolean
     */
    private $required;
    
      /**
     * @var string
     */
    private $cssClasss; 
    
       /**
     * @var string
     */
    private $type; 

    /**
     * @var string
     */
    private $config;
	
	/**
     * @var string
     */
    private $action;
     /**
     * @var string
     */
    private $choicelist;
    
	 /**
     * @var string
     */
    private $choicelistvalues; // set array of defult values its use for client
    /**
     * @var string
     */
    private $label;
    
     /**
     * @var string
     */
    private $format;
    
      /**
     * @var string
     */
    private $value;
    
     /**
     * @var string
     */
    private  $placeholder;
    function __construct() {
                $this->name=null;
                $this->required=false;
                $this->cssClasss='';
                $this->type=null;
                $this->choicelist=null;
		$this->choicelistvalues=null;
                $this->label=null;
                $this->format=null;
                $this->value=null;
                $this->placeholder=null;
          
      } 
    public function init($configdata) {
      
                    if(array_key_exists('name', $configdata)){$this->name=$configdata['name'];}
                    if(array_key_exists('required', $configdata)){$this->required=$configdata['required'];}
                    if(array_key_exists('cssClasss', $configdata)){$this->cssClasss=$configdata['cssClasss'];}
                    if(array_key_exists('type', $configdata)){$this->type=$configdata['type'];}
                    if(array_key_exists('config', $configdata)){$this->config=$configdata['config'];}
					if(array_key_exists('action', $configdata)){$this->action=$configdata['action'];}
                    if(array_key_exists('choicelist', $configdata)){$this->choicelist=$configdata['choicelist'];}
                    if(array_key_exists('choicelistvalues', $configdata)){$this->choicelistvalues=$configdata['choicelistvalues'];}
                    if(array_key_exists('label', $configdata)){$this->label=$configdata['label'];}
                    if(array_key_exists('placeholder', $configdata)){$this->placeholder=$configdata['placeholder'];}
                    if(array_key_exists('cssClasss', $configdata)){$this->cssClasss=$configdata['cssClasss'];}
                    if(array_key_exists('value', $configdata)){$this->value=$configdata['value'];}
                    if(array_key_exists('format', $configdata)){$this->format=$configdata['format'];}
    
      }
      
      public function getName() {
          return $this->name;
      }

      public function getRequired() {
          return $this->required;
      }

      public function getCssClasss() {
          return $this->cssClasss;
      }

      public function setName($name) {
          $this->name = $name;
      }

      public function setRequired($required) {
          $this->required = $required;
      }

      public function setCssClasss($cssClasss) {
          $this->cssClasss = $cssClasss;
      }


       public function getType() {
          return $this->type;
      }
      
      public function setType($type) {
          $this->type = $type;
      }

       public function getChoicelist() {
          return $this->choicelist;
      }
      
      public function setChoicelist($choicelist) {
          $this->choicelist = $choicelist;
      }
      
      public function getLabel() {
          return $this->label;
      }

      public function setLabel($label) {
          $this->label = $label;
      }
   public function getChoicelistvalues() {
          return $this->choicelistvalues;
      }
      
      public function setChoicelistvalues($choicelistvalues) {
          $this->choicelistvalues = $choicelistvalues;
      }

      function getFormat() {
          return $this->format;
      }

      function setFormat($format) {
          $this->format = $format;
      }

      function getValue() {
          return $this->value;
      }

      function setValue($value) {
          $this->value = $value;
      }

      function getPlaceholder() {
          return $this->placeholder;
      }

      function setPlaceholder($placeholder) {
          $this->placeholder = $placeholder;
      }
      function getConfig() {
        return  $this->config;
    }

    function setConfig($config) {
        $this->config = $config;
    }

  function getAction() {
        return  $this->action;
    }

    function setAction($action) {
        $this->action = $action;
    }
}
