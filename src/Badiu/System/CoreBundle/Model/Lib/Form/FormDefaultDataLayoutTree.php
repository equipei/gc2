<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FormDefaultDataLayoutTree extends BadiuModelLib {

   private $param;
    private $kminherit;
   private $defaultparam=array();
    public function __construct(Container $container) {
        parent::__construct($container);
    }

     function exec() {
         $formdefultdata=$this->getContainer()->get('badiu.system.core.lib.form.defaultdata');
         $formdefultdata->setType('data');
         $formdefultdata->setParam($this->defaultparam);
        
         $dtype=$formdefultdata->get('dtype');
        
         $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
         $factorykeytree->init($this->getKminherit()->data());
         
         $idpath = $this->getUtildata()->getVaueOfArray($this->param, 'idpath');
         
         $entity = $this->getEntity();

        $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
        $level = $factorykeytree->getLevel($newidtree);
        $order = $factorykeytree->getOrder($newidtree);
        $this->param['idpath']=$newidtree;
        $this->param['level']=$level;
        $this->param['orderidpath']=$order;
        if (!empty($idpath)) {
                 $parentinfo = $factorykeytree->getParentInfo($entity, $idpath, $dtype);
                $this->param = $factorykeytree->makePath($parentinfo, $this->param);
        }
      
     }
    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }
    function getKminherit() {
        return $this->kminherit;
    }

    function setKminherit($kminherit) {
        $this->kminherit = $kminherit;
    }

    function getDefaultparam() {
        return $this->defaultparam;
    }

    function setDefaultparam($defaultparam) {
        $this->defaultparam = $defaultparam;
    }



}
