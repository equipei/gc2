<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuCheckForm;

class FormDefaultCheck extends BadiuCheckForm {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function validation() {
         $datalayout = $this->getCform()->getDatalayout();
        $data = $this->getContainer()->get($this->getKminherit()->data());
        $id = $this->getParamItem('id');
        $name = $this->getParamItem('name');
        $existfieldname=false;
		if(is_array($this->getParam())){$existfieldname=array_key_exists("name",$this->getParam());}
        $paramparent = array();
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $entity = $badiuSession->get()->getEntity();
        $duplicatename = false;
    
        $defaultdata=$this->getCform()->getDefaultdata();
        $dtype=$this->getUtildata()->getVaueOfArray($defaultdata, 'dtype');
       
        if (($datalayout == 'datawithname' || $datalayout == 'datawitparentname' || $datalayout == ' datadtypewithname' ) && $existfieldname) {
           if($datalayout == 'datawitparentname'){
                $parentid = $this->getUtildata()->getVaueOfArray($this->getParam(),$this->getCpage()->getParenttablecolumn());
               if( $parentid > 0){ $paramparent[$this->getCpage()->getParenttablecolumn()] =$parentid;}
           }
          
           if($datalayout == 'datadtypewithname'){
            $paramparent['dtype']=$dtype;
           }
            if ($this->isedit()) {
                $duplicatename = $data->existColumnEdit($id, $entity, 'name', $name, $paramparent);
            } else {
                $duplicatename = $data->existColumnAdd($entity, 'name', $name, $paramparent);
             }

            if ($duplicatename) {

                $message = array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.duplication.message');
                $message['name'] = $this->getTranslator()->trans('badiu.system.duplication.record.name');
                $info = 'badiu.system.error.duplication.record.name';
                return $this->getResponse()->denied($info, $message);
            }
        }
		//review
		//$validetadate=$this->defaultdate($this->getParam());
		//return $validetadate;
        return null;
    }
	
	
	public function defaultdate($fparam) { 
		$castdatedefault=$this->getContainer()->get('badiu.system.core.lib.form.castdatedefault');
        $newparam=array();
         foreach ($fparam as $key => $value) {
             $pos=stripos($key, "_badiudatedefault");
             if($pos!== false){
                 $p= explode("_", $key);
                 $keyfield=null;
                 $nvalue=null;
                 if(isset($p[0])){
                       $keyfield=$p[0];
					   if(!empty($value)){
						    $nvalue=$castdatedefault->convertToDbDate($value);
							if(empty($nvalue)){
								$message = array();
								$message['generalerror'] = $this->getTranslator()->trans('badiu.system.time.datenotvalid');
								$message[$keyfield] = $this->getTranslator()->trans('badiu.system.time.datenotvalid');
								$info = 'badiu.system.time.datenotvalid';
								return $this->getResponse()->denied($info, $message);
							}
					   }
                      
					   
                       
                   } 
             }
             
         }
        
        return null;
   }  

}
