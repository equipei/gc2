<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Form;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * 
 *
 * @author lino
 */
class FormConfig {

    //action=&method=GET&label=discipline.add&attr=role/form&submitlabel=add&submitattr=class/btn btn-default&pageviewaftersubmit=&routetoredirectaftersubmit=
    /**
     * @var Container
     */
    private $container;

    /**
     * @var string
     */
    private $action; //desabled

    /**
     * @var string
     */
    private $method; //desabled

    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $labeledit;

    /**
     * @var string
     */
    private $labelcopy;

    /**
     * @var array
     */
    private $attr;

    /**
     * @var string
     */
    private $submitlabel; 

    /**
     * @var string
     */
    private $submitlabeledit;

    /**
     * @var array
     */
    private $submitattr;


    /**
     * @var string
     */
    private $pageviewaftersubmit; 
    
    /**
     * @var string
     */
    private $routetoredirectaftersubmit;
    
    /**
     * @var string
     */
    private $datalayout = 'datawithname'; //datawithname | datawitparentname | datawithoutname  | datatreewithname | datatreewithoutname | dataforserveraltable;

    /**
     * @var string
     */
    private $type = 'data'; //tipo de form: pesquisa  cadastro de dados data | filter

    /**
     * @var string
     */
    private $filerole; //list of twig file separate by commas

    /**
     * @var array
     */
    private $filereport; // file of report
     /**
     * @var array
     */
    private $filebeforereport; //list of twig file separate by commas
    
       /**
     * @var array
     */
    private $fileafterreport; //list of twig file separate by commas
    
           /**
     * @var string
     */
    private $serviceexecbeforesubmit; //list of service/funcition separate by commas
    
     /**
     * @var string
     */
    private $serviceexecaftersubmit; //list of service/funcition separate by commas
    
    /**
     * @var array
     */
    private $defaultdata; //change form data and force defult data on fields

    /**
     * @var array
     */
    private $defaultdataonpenform; //set default data on open add or search form

     /**
     * @var array
     */
    private $maxlimit; //set default of limit data to filter in database. If filter is upper is override with maxlimit
    /**
     * @var array
     */
    private $linkimenu; //link for internal menu on the filter form. menu to picklist for exemple. Only show yet on filter

    //linkimenu1=rota/xxx|label/xxx|type/button
    
    /**
     * @var array
     */
    private $themelayoutblockhidden; //themelayoutblockhidden=lmenu,rmenu,navbar
    /**
     * @var string
     */
    private $themelayout; //override this variable of page
                           
    
    /**
     * @var array
     */
    private $filter; //list of service separtate by commas
   
    private $basekey; //basekey of keymanager current navegation

         /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
	private $systemdata;
	private $data;//all config data
    function __construct(Container $container, $param = null, $type = 'data') {
         $this->container = $container;
		$this->data  = $param;
		$this->initData();
		$this->type = $type;
       
        $this->action = "";
        $this->method = "POST";
        if ($this->type == 'filter') {
            $this->method = "GET";
        }
        $this->label =null;// 'add';
        $this->labeledit = 'edit'; //rever apagar
        $this->labelcopy = 'copy'; //rever apagar
        $this->attr = array('role' => 'form');
        $this->submitlabel = 'submitadd'; //rever apagar
        $this->submitlabeledit = 'submitedit'; //rever apagar

        $this->submitattr = array('class' => 'btn btn-default');
        $this->pageviewaftersubmit = '';
        $this->routetoredirectaftersubmit = '';
        $this->datalayout = 'datawithname';

        if ($this->type == 'filter') {
            $this->label = 'search';
            $this->submitlabel = 'research';
        } else if ($this->type == 'edit') {
            $this->label = 'editrecord';
            $this->submitlabel = 'edit';
        }
        $this->filerole = array();
        $this->fileafterreport = array();
        $this->filebeforereport = array(); 
        $this->defaultdata = null;
        $this->linkimenu = array();
        $this->themelayoutblockhidden = array();
        $this->themelayout=null;
        $this->filter= array();
        if ($param != null) {
            $this->initParam($param);
        }
       
    }

    function initParam($param) {

        $subQueryString = $this->container->get('badiu.system.core.lib.http.subquerystring');
        if (array_key_exists("type", $param)) {
            $this->type = $param["type"];
        }
        if (array_key_exists("action", $param)) {
            $this->action = $param["action"];
        }
        if (array_key_exists("method", $param)) {
            $this->method = $param["method"];
        }
        if (array_key_exists("label", $param)) {
            $this->label = $param["label"];
        }

        if (array_key_exists("labeledit", $param)) {
            $this->labeledit = $param["labeledit"];
        }
        if (array_key_exists("labelcopy", $param)) {
            $this->labelcopy = $param["labelcopy"];
        }
        if (array_key_exists("attr", $param)) {

            $attr = $param["attr"];
            $subQueryString->setQuery($attr);
            $subQueryString->makeParam();
            $this->attr = $subQueryString->getParam();
        }
        if (array_key_exists("submitlabel", $param)) {
            $this->submitlabel = $param["submitlabel"];
        }

        if (array_key_exists("submitlabeledit", $param)) {
            $this->submitlabeledit = $param["submitlabeledit"];
        }
        if (array_key_exists("submitattr", $param)) {
            $submitattr = $param["submitattr"];
            $subQueryString->setQuery($submitattr);
            $subQueryString->makeParam();
            $this->submitattr = $subQueryString->getParam();
        }

        if (array_key_exists("pageviewaftersubmit", $param)) {
            $this->pageviewaftersubmit = $param["pageviewaftersubmit"];
        }
        if (array_key_exists("routetoredirectaftersubmit", $param)) {
            $this->routetoredirectaftersubmit = $param["routetoredirectaftersubmit"];
        }
        if (array_key_exists("datalayout", $param)) {
            $this->datalayout = $param["datalayout"];
        }

        if (array_key_exists("filerole", $param)) {
            $lfiles=$param["filerole"];
             $pos = stripos($lfiles, ",");
            if ($pos === false ) {
               $this->filerole=array(0=>$lfiles);
            }else{
                $this->filerole=explode(",",$lfiles);
            }
           
        }

          if (array_key_exists("filereport", $param)) {
             $this->filereport=$param["filereport"];
            
           }
         if (array_key_exists("filebeforereport", $param)) {
            $lfiles=$param["filebeforereport"];
             $pos = stripos($lfiles, ",");
            if ($pos === false ) {
               $this->filebeforereport=array(0=>$lfiles);
            }else{
                $this->filebeforereport=explode(",",$lfiles);
            }
           
        }
         if (array_key_exists("fileafterreport", $param)) {
            $lfiles=$param["fileafterreport"];
             $pos = stripos($lfiles, ",");
            if ($pos === false ) {
               $this->fileafterreport=array(0=>$lfiles);
            }else{
                $this->fileafterreport=explode(",",$lfiles);
            }
           
        }
        
      if (array_key_exists("serviceexecbeforesubmit", $param)) {
            $lservices=$param["serviceexecbeforesubmit"];
             $pos = stripos($lservices, ",");
            if ($pos === false ) {
               $this->serviceexecbeforesubmit=array(0=>$lservices);
            }else{
                $this->serviceexecbeforesubmit=explode(",",$lservices);
            }
           
        }
        if (array_key_exists("serviceexecaftersubmit", $param)) {
            $lservices=$param["serviceexecaftersubmit"];
             $pos = stripos($lservices, ",");
            if ($pos === false ) {
               $this->serviceexecaftersubmit=array(0=>$lservices);
            }else{
                $this->serviceexecaftersubmit=explode(",",$lservices);
            }
           
        }
        if (array_key_exists("defaultdata", $param)) {
            $defaultdata = $param["defaultdata"];
            $subQueryString->setQuery($defaultdata);
            $subQueryString->makeParam();
            $this->defaultdata = $subQueryString->getParam();
        }
        if (array_key_exists("defaultdataonpenform", $param)) {
            $defaultdataonpenform = $param["defaultdataonpenform"];
            $subQueryString->setQuery($defaultdataonpenform);
            $subQueryString->makeParam();
            $this->defaultdataonpenform = $subQueryString->getParam();
        }
        if (array_key_exists("maxlimit", $param)) {
            $maxlimit = $param["maxlimit"];
            $subQueryString->setQuery($maxlimit);
            $subQueryString->makeParam();
            $this->maxlimit = $subQueryString->getParam();
        }
        if (array_key_exists("themelayoutblockhidden", $param)) {
            $lfiles=$param["themelayoutblockhidden"];
             $pos = stripos($lfiles, ",");
            if ($pos === false ) {
               $this->themelayoutblockhidden=array(0=>$lfiles);
            }else{
                $this->themelayoutblockhidden=explode(",",$lfiles);
            }
           
        }
        
         if (array_key_exists("themelayout", $param)) {
             $this->themelayout=$param["themelayout"];
            
           }
           
        $existlinkimenu = true;
        $cont = 0;
        while ($existlinkimenu) {
            $cont++;
            if (array_key_exists("linkimenu$cont", $param)) {
                $linkimenu = $param["linkimenu$cont"];
                $subQueryString->setQuery($linkimenu);
                $subQueryString->makeParam();
                $this->linkimenu[$cont] = $subQueryString->getParam();
            } else {
                $existlinkimenu = false;
            }
        }
      
        if (array_key_exists("filter", $param)) {
            $lfiles=$param["filter"];
             $pos = stripos($lfiles, ",");
            if ($pos === false ) {
               $this->filter=array(0=>$lfiles);
            }else{
                $this->filter=explode(",",$lfiles);
            }
           
        }
    }

	function initData() {
		$newlist=array();
		$subQueryString = $this->getContainer()->get('badiu.system.core.lib.http.subquerystring');
		 foreach ($this->data as $key => $value) {
			 
			  $pos=stripos($value, "/");
              if($pos=== false){$newlist[$key]=$value;}
			  else{
				  $subQueryString->setQuery($value);
				  $subQueryString->makeParam();
				  $spram= $subQueryString->getParam();
				  $newlist[$key]=$spram;
			  }
			 
		 }
		$this->data=$newlist;
	}
    function overrideDbConfig() {
        $this->overrideDefaultdataonopenformWithConfigCustom();
        $this->overrideMaxlimitWithConfigCustom();
       //review it. When date is not period get error 
	   // $this->checkMaxLimitConfig(); 
    }
    /*
     * overrride config defaultdatafilterconfigonopenform if defined on database config custom
     */
    
    function overrideDefaultdataonopenformWithConfigCustom() {
        $badiusession= $this->getContainer()->get('badiu.system.access.session');
        $badiusession->setHashkey($this->getSessionhashkey());
        if(!$badiusession->exist()){return null;} 
        $param= $badiusession->get()->getConfigcustom()->getParam();
        
        $confcustomkaybase=$this->basekey."filterformconfigdefaultdataonpenform";
       if($this->type=='data'){$confcustomkaybase=$this->basekey."dataformconfigdefaultdataonpenform";}
        foreach ($param as $key => $value) {
             $pos=stripos($key, $confcustomkaybase);
             if($pos!== false){
                 $key=str_replace($confcustomkaybase,"",$key);
                 $value=$this->castCustomArrayToConfigArray($value);
                 $this->defaultdataonpenform[$key]=$value;
             }
            
        }
       
     }
     function overrideMaxlimitWithConfigCustom() {

        $badiusession= $this->getContainer()->get('badiu.system.access.session');
        $badiusession->setHashkey($this->getSessionhashkey());
        if(!$badiusession->exist()){return null;}
        $param= $badiusession->get()->getConfigcustom()->getParam();
        
        $confcustomkaybase=$this->basekey."filterformconfigmaxlimit";
       if($this->type=='data'){$confcustomkaybase=$this->basekey."dataformconfigmaxlimit";}
        foreach ($param as $key => $value) {
             $pos=stripos($key, $confcustomkaybase);
             if($pos!== false){
                 $key=str_replace($confcustomkaybase,"",$key);
                 $value=$this->castCustomArrayToConfigArray($value);
                 $this->maxlimit[$key]=$value;
             }
            
        }
       
     }
      public function castCustomArrayToConfigArray($str) {
         if(empty($str)) return $str;
         $posstartcustomarray=stripos($str, "{");
         $posendcustomarray=stripos($str, "}");
         $posseparatecustomarray=stripos($str, ":");
         if($posstartcustomarray!== false && $posendcustomarray!==false && $posseparatecustomarray!==false){
            $str=str_replace("{","[",$str); 
            $str=str_replace("}","]",$str); 
         }
         return $str;
    }

    function checkMaxLimitConfig() {
         if(empty($this->maxlimit)){return null;}
         $periodutil=$this->container->get('badiu.system.core.lib.date.periodutil');
         $formdefaultdata= $this->getContainer()->get('badiu.system.core.lib.form.defaultdata');
         
         foreach ($this->maxlimit as $key => $value) {
             if (is_array($this->defaultdataonpenform)&&array_key_exists($key,$this->defaultdataonpenform)){
                 $defualtvalue=$this->defaultdataonpenform[$key];
                 
                 if(is_numeric($value)){
                     if($defualtvalue==null || $defualtvalue==''){$this->defaultdataonpenform[$key]=$value;}
                     else if(!is_numeric($defualtvalue)){$this->defaultdataonpenform[$key]=$value;}
                     else if($defualtvalue > $value){$this->defaultdataonpenform[$key]=$value;}
                 }//check if is array
                 else{
                    
                    if($defualtvalue==null || $defualtvalue==''){$this->defaultdataonpenform[$key]=$value;}
                    else{
                        $typex=$formdefaultdata->getParamType($defualtvalue);
                        $typey=$formdefaultdata->getParamType($value);
                        if($typex=='array' && $typey=='array'){
                            $defualtvalueaux=$formdefaultdata->getArray($defualtvalue);
                            $defualtvaluecasttime=$periodutil->castTotimestamp($defualtvalueaux);

                            $valueaux=$formdefaultdata->getArray($value);
                            $valuecasttime=$periodutil->castTotimestamp($valueaux);
                           
                            if($defualtvaluecasttime > $valuecasttime){$this->defaultdataonpenform[$key]=$value;}
                        }
                        
                    }
                 } 
             }else{
                 $this->defaultdataonpenform[$key]=$value;
             }
         }
         //print_r($this->maxlimit);
        // print_r($this->defaultdataonpenform);exit;
     }
    public function getAction() {
        return $this->action;
    }

    public function getMethod() {
        return $this->method;
    }

    public function getLabel() {
        return $this->label;
    }

    public function getAttr() {
        return $this->attr;
    }

    public function getSubmitlabel() {
        return $this->submitlabel;
    }

    public function getSubmitattr() {
        return $this->submitattr;
    }

  

    public function setAction($action) {
        $this->action = $action;
    }

    public function setMethod($method) {
        $this->method = $method;
    }

    public function setLabel($label) {
        $this->label = $label;
    }

    public function setAttr($attr) {
        $this->attr = $attr;
    }

    public function setSubmitlabel($submitlabel) {
        $this->submitlabel = $submitlabel;
    }

    public function setSubmitattr($submitattr) {
        $this->submitattr = $submitattr;
    }



    /**
     * @return string
     */
    public function getLabeledit() {
        return $this->labeledit;
    }

    /**
     * @param string $labeledit
     */
    public function setLabeledit($labeledit) {
        $this->labeledit = $labeledit;
    }

    /**
     * @return string
     */
    public function getLabelcopy() {
        return $this->labelcopy;
    }

    /**
     * @param string $labelcopy
     */
    public function setLabelcopy($labelcopy) {
        $this->labelcopy = $labelcopy;
    }

    /**
     * @return string
     */
    public function getSubmitlabeledit() {
        return $this->submitlabeledit;
    }

    /**
     * @param string $submitlabeledit
     */
    public function setSubmitlabeledit($submitlabeledit) {
        $this->submitlabeledit = $submitlabeledit;
    }

    /**
     * @return string
     */
    public function getDatalayout() {
        return $this->datalayout;
    }

    /**
     * @param string $datalayout
     */
    public function setDatalayout($datalayout) {
        $this->datalayout = $datalayout;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getFilerole() {
        return $this->filerole;
    }

    /**
     * @param string $filerole
     */
    public function setFilerole($filerole) {
        $this->filerole = $filerole;
    }

    /**
     * @return array
     */
    public function getDefaultdata() {
        return $this->defaultdata;
    }

    /** 	 
     * @param string $filerole
     */
    public function setDefaultdata($defaultdata) {
        $this->defaultdata = $defaultdata;
    }

    public function setLinkimenu($linkimenu) {
        $this->linkimenu = $linkimenu;
    }

    public function getLinkimenu() {
        return $this->linkimenu;
    }

    function getContainer() {
        return $this->container;
    }

    function getDefaultdataonpenform() {
        return $this->defaultdataonpenform;
    }

    function setContainer(Container $container) {
        $this->container = $container;
    }

    function setDefaultdataonpenform($defaultdataonpenform) {
        $this->defaultdataonpenform = $defaultdataonpenform;
    }
    function getFilebeforereport() {
        return $this->filebeforereport;
    }

    function getFileafterreport() {
        return $this->fileafterreport;
    }

    function setFilebeforereport($filebeforereport) {
        $this->filebeforereport = $filebeforereport;
    }

    function setFileafterreport($fileafterreport) {
        $this->fileafterreport = $fileafterreport;
    }

    function getServiceexecbeforesubmit() {
        return $this->serviceexecbeforesubmit;
    }

    function getServiceexecaftersubmit() {
        return $this->serviceexecaftersubmit;
    }

    function setServiceexecbeforesubmit($serviceexecbeforesubmit) {
        $this->serviceexecbeforesubmit = $serviceexecbeforesubmit;
    }

    function setServiceexecaftersubmit($serviceexecaftersubmit) {
        $this->serviceexecaftersubmit = $serviceexecaftersubmit;
    }

    function getFilereport() {
        return $this->filereport;
    }

    function setFilereport($filereport) {
        $this->filereport = $filereport;
    }

    function getPageviewaftersubmit() {
        return $this->pageviewaftersubmit;
    }

    function getRoutetoredirectaftersubmit() {
        return $this->routetoredirectaftersubmit;
    }

    function setPageviewaftersubmit($pageviewaftersubmit) {
        $this->pageviewaftersubmit = $pageviewaftersubmit;
    }

    function setRoutetoredirectaftersubmit($routetoredirectaftersubmit) {
        $this->routetoredirectaftersubmit = $routetoredirectaftersubmit;
    }
    function getThemelayoutblockhidden() {
        return $this->themelayoutblockhidden;
    }

    function getThemelayout() {
        return $this->themelayout;
    }

    function setThemelayoutblockhidden($themelayoutblockhidden) {
        $this->themelayoutblockhidden = $themelayoutblockhidden;
    }

    function setThemelayout($themelayout) {
        $this->themelayout = $themelayout;
    }


    function getFilter() {
        return $this->filter;
    }

    function setFilter($filter) {
        $this->filter = $filter;
    }


    function getBasekey() {
        return $this->basekey;
    }

    function setBasekey($basekey) {
        $this->basekey = $basekey;
    }
    function getMaxlimit() {
        return $this->maxlimit;
    }

    function setMaxlimit($maxlimit) {
        $this->maxlimit = $maxlimit;
    }

    public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }
   
function getData() {
        return $this->data;
    }

function setData($data) {
        $this->data = $data;
    }
	
		public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
