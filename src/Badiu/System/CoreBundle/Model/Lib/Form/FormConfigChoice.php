<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FormConfigChoice  extends BadiuModelLib {
 public function __construct(Container $container) {
        parent::__construct($container);
    }

	 public function getDefaultChoiceList($param) {
		 $ftype=$this->getUtildata()->getVaueOfArray($param,'type'); //choice | badiuautocomplete
		 $config=$this->getUtildata()->getVaueOfArray($param,'config'); //$field->getChoicelist()
		 
		 if($ftype=='badiuchoiceautocomplete'){
			 $list=$this->getBadiuchoiceautocomplete($config);
			 return $list;
		 }
		 $type=null;
		 if(empty($type)){$type=='choice';}
        
		$list = array();

        //deprecated defaultboolean without type
        if ($config == 'defaultboolean' || $config == 's|defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
            return $list;
        }
        if ($config == 's|percentage') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getPercentage();
            return $list;
        }
       if ($config == 's|percentagedesc') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getPercentageDesc();
		
            return $list;
        }
		
        $p = explode("|",$config);
        $type = 's';
        $dataservice = null;
        $function = null;
        $converlist = 'y';
		$param=null;
        if (isset($p[0])) {
            $type = $p[0];
        }
        if (isset($p[1])) {
            $dataservice = $p[1];
        }
        if (isset($p[2])) {
            $function = $p[2];
        }
        if (isset($p[3])) {
            $converlist = $p[3];
        } 

        //if type not defined
        if ($type != 's' &&  $type != 'sp' ) {
            $dataservice = $type;
        }
		
		if ($type == 'sp') {
			 $fp = explode("-", $function);
			  if (isset($fp[0])) { $function = $fp[0];}
			  if (isset($fp[1])) {$param = $fp[1]; }
          
        }
		  //implementation default of type s 
        if (!$this->getContainer()->has($dataservice)){return $list;}
        if (empty($function)){$function = 'getFormChoice';}
        $data = $this->getContainer()->get($dataservice);
        $data->setSessionhashkey($this->getSessionhashkey());
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $list = null;
		
		
        if ($function == 'getFormChoice' || $function == 'getFormChoiceShortname') {
            $list = $data->$function($badiuSession->get()->getEntity());
        } else {
			if ($type == 'sp') {$list = $data->$function($badiuSession->get()->getEntity(),$param);}
			else{$list = $data->$function();}      
        }

        if ($converlist == 'y') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->convertBdArrayToListOptions($list);
        }


        return $list;
    }
	
    public function getDefaultChoice(FormField $field) {
        $fparam=array('config'=>$field->getChoicelist(),'type'=>$field->getType()); //add type by field->type
		$list =$this->getDefaultChoiceList($fparam);
		return $list;
        //deprecated defaultboolean without type
       /* if ($field->getChoicelist() == 'defaultboolean' || $field->getChoicelist() == 's|defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
            return $list;
        }
        if ($field->getChoicelist() == 's|percentage') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getPercentage();
            return $list;
        }
       if ($field->getChoicelist() == 's|percentagedesc') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getPercentageDesc();
		
            return $list;
        }
		
        $p = explode("|", $field->getChoicelist());
        $type = 's';
        $dataservice = null;
        $function = null;
        $converlist = 'y';
		
        if (isset($p[0])) {
            $type = $p[0];
        }
        if (isset($p[1])) {
            $dataservice = $p[1];
        }
        if (isset($p[2])) {
            $function = $p[2];
        }
        if (isset($p[3])) {
            $converlist = $p[3];
        } 

        //if type not defined
        if ($type != 's') {
            $dataservice = $type;
        }
        //implementation default of type s 
        if (!$this->getContainer()->has($dataservice)){return $list;}
        if (empty($function)){$function = 'getFormChoice';}
        $data = $this->getContainer()->get($dataservice);
        $data->setSessionhashkey($this->getSessionhashkey());
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $list = null;
        if ($function == 'getFormChoice' || $function == 'getFormChoiceShortname') {
            $list = $data->$function($badiuSession->get()->getEntity());
        } else {
            $list = $data->$function();
        }

        if ($converlist == 'y') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->convertBdArrayToListOptions($list);
        }


        return $list;*/
    }
    
     public function getEntityChoice(FormField $field) {
        $list = array();
		
        $p = explode("|", $field->getChoicelist());
        $pclass = null;
		
        $function = 'getFormChoice';

        if (isset($p[2])) {
            $pclass = $p[2];
        }
        if (isset($p[3])) {
            $function = $p[3];
        }

        if (!empty($pclass)) {
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $badiuSession->setHashkey($this->getSessionhashkey());
			if(!$this->getContainer()->has($pclass)){return null;}
            $data = $this->getContainer()->get($pclass);
            if ($function == 'getFormChoice') {
                $list = $data->getFormChoice($badiuSession->get()->getEntity());
            } else {
                $list = $data->$function();
            }
            if ($list == null) {
                return null;
            }
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->convertBdArrayToListOptions($list);
        }
        return $list;
    }
    
    
    public function getConfig($param) {
        $list = array();
        $p = explode("|", $param);
        $type = 's';
        $dataservice = null;
        $function = null;
        $converlist = 'y';
		$param =null; //for autocomplete
        if (isset($p[0])) {
            $type = $p[0];
        }
        if (isset($p[1])) {
            $dataservice = $p[1];
        }
        if (isset($p[2])) {
            $function = $p[2];
        }
        if (isset($p[3])) {
            $converlist = $p[3];
        }
		if (isset($p[4])) {
            $param = $p[4];
			$param=$this->getUtildata()->castStringToArray($param);
        }
        //if type not defined
        if ($type != 's') {
            $dataservice = $type;
        }
        $list['type']=$type;
        $list['service']=$dataservice;
        $list['function']=$function;
        $list['converlist']=$converlist;
		$list['param']=$param;
		
        return $list;
    }
	
	     public function getBadiuchoiceautocomplete($config) {
			 if(empty($config)){return null;}
			 $p = explode("|", $config);
			 
			 $dataservice = null;
			 $function = null;
				if (isset($p[1])) {$dataservice = $p[1];}
				if (isset($p[4])) {$function = $p[4];}
				if (isset($p[5])) {$function = $p[5];}
				if(empty($dataservice)){return null;}
				if(empty($function)){return null;}
				if(!$this->getContainer()->has($dataservice)){return null;}
				$data = $this->getContainer()->get($dataservice);
				$data->setSessionhashkey($this->getSessionhashkey());
				
				if(!method_exists($data, $function)){return null;}
				$list=$data->$function();
			
			 return $list;
			 
		 }
}
