<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Date\DOMAINTABLE;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class BadiuFilterDateFormCast extends BadiuModelLib {
 
    public function __construct(Container $container) {
        parent::__construct($container);
    }
    public function changeParam($filter) {
        $formcastdate=$this->getContainer()->get('badiu.system.core.lib.form.castdate');
        $newfilter=array();
        $isparamofhtmlform=true;
		if(!is_array($filter)){return $filter;} 
        foreach ($filter as $key => $value) {
           if(is_array($value)) {
               if(array_key_exists("badiudateoperator",$value) && array_key_exists("badiudate1",$value)&& array_key_exists("badiudate2",$value)){
                  $isparamofhtmlform=false;
                   $operator=$value['badiudateoperator'];
                   $operatoraux=null;
                   $badiudate1=$value['badiudate1'];
                   $badiudate2=$value['badiudate2'];
                   
                 /* if($operator==DOMAINTABLE::$OPERATOR_TEXT_ON){
                      $operator=DOMAINTABLE::$OPERATOR_TEXT_BETWEEN;
                      if(!empty($badiudate1)) {
                          $badiudate1->setTime(0, 0, 0);
                          $badiudate2=$value['badiudate1'];
                          $badiudate2->setTime(23, 59, 59);
                      }
                  }*/
                 if($operator !=DOMAINTABLE::$OPERATOR_TEXT_BETWEEN){$badiudate2=null;}
                   
                   $keyoperator="::".$key."operator";
                   $keybadiudate1=$key."1";
                   $keybadiudate2=$key."2";
                   $matoperator=$this->getOperator($operator);
                   
                   $newfilter[$keyoperator]=$matoperator;
                   $newfilter[$keybadiudate1]=$badiudate1;
                   $newfilter[$keybadiudate2]=$badiudate2;
                   
               }else{
                
                    $newfilter[$key]=$value;
               }
           }else{
               $newfilter[$key]=$value;
           }
        }
        //html form
        if($isparamofhtmlform){
           $newfilter=array();
           $operationf=null;
           $operationaux=null;
           $keydate1=null;
           $keydate2=null;
           
           
           foreach ($filter as $key => $value) {
                $pos=stripos($key, "_badiudateoperator");
                $bskey=null;
                if($pos!== false){
                    $originalkey=$key;
                    $key="::".$key;
                    $operationaux=$value;
                    $value=$this->getOperator($value);
                    $operationf=$value;
                    //set value of date1 and date2 for star and end day
                     if($operationaux==DOMAINTABLE::$OPERATOR_TEXT_ON){
                         $p=explode("_",$originalkey);
                         if(sizeof($p)==2){
                             $bskey=$p[0];
                             $time1=$filter[$bskey.'_badiudate1'];
                             if(!empty($time1) && $time1 !='null'){
                                 //$time1=date_create_from_format('d/m/Y', $time1);
                                 $time1=$formcastdate->convertToDbDate($time1);
                                 $time2=clone $time1;
                                 $time1->setTime(0, 0, 0);
                                 $time2->setTime(23, 59, 59);
                                 $newfilter[$bskey.'1']=$time1;
                                  $newfilter[$bskey.'2']=$time2;
                             }
                             
                         }
                         
                     }
                }
               
                //change date
                $posd1=stripos($key, "_badiudate1");
                $posd2=stripos($key, "_badiudate2");
                if($posd1!== false || $posd2!== false){
                   if(!empty($value)){
                    $p=explode("_",$key);
                    if(sizeof($p)==2){ $bskey=$p[0];}
                       $paramhm=array();
                       if($posd1!== false){
                            $fhour =$this->getUtildata()->getVaueOfArray($filter, $bskey.'_badiuhour1');
                            $fminute =$this->getUtildata()->getVaueOfArray($filter, $bskey.'_badiuminute1');
                            $paramhm['hour']=$fhour;
                            $paramhm['minute']=$fminute;
                        }else if($posd2!== false){
                            $fhour =$this->getUtildata()->getVaueOfArray($filter, $bskey.'_badiuhour2');
                            $fminute =$this->getUtildata()->getVaueOfArray($filter, $bskey.'_badiuminute2');
                            if($fhour=='' && $fminute==''){$fhour=23;$fminute=59;$paramhm['second']=59;}
                            $paramhm['hour']=$fhour;
                            $paramhm['minute']=$fminute;
                            
                        }
                        //$value= date_create_from_format('d/m/Y', $value);
                        $value=$formcastdate->convertToDbDate($value,$paramhm);
                    }
                }
                $key= str_replace("_badiudate","",$key);
                 if(!array_key_exists($key,$newfilter)){$newfilter[$key]=$value;}
                
           }
           //clean date2
          // if( $operationf !=DOMAINTABLE::$OPERATOR_TEXT_BETWEEN){$newfilter[$keydate2]=null;}
        }
       
        
        return $newfilter;
    }
   
    
public function changeParamFromformCasToArray($filter) {
    $newfilter=array();
    $listkeytoremove=array();
	if(!is_array($filter)){return $filter;} 
    foreach ($filter as $key => $value) {
        if(!is_array($value)){
            $pos=stripos($key, "_badiudateoperator");
            
            if($pos!== false){
                $p= explode("_", $key);
                $keyfield=null;
             
                if(isset($p[0])){
                    $keyfield=$p[0];
                    $existoperator=array_key_exists($keyfield,$newfilter);
                    if(!$existoperator){
                        
                        $operatorkey=$keyfield.'_badiudateoperator';
                        $date1key=$keyfield.'_badiudate1';
                        $date2key=$keyfield.'_badiudate2';
                       
                       
                        $operator=$this->getUtildata()->getVaueOfArray($filter,$operatorkey);
                        $date1=$this->getUtildata()->getVaueOfArray($filter,$date1key);
                        $date2=$this->getUtildata()->getVaueOfArray($filter,$date2key);
                         
                        $paramdp=array('operator'=>$operator,'date1'=>$date1,'date2'=>$date2);
                        $newfilter[$keyfield]=$paramdp;
                      
                        array_push($listkeytoremove,$operatorkey);
                        array_push($listkeytoremove,$date1key);
                        array_push($listkeytoremove,$date2key);
                    }
                    
            }else{$newfilter[$key]=$value;}
                 
            }else{$newfilter[$key]=$value;}
        }else{$newfilter[$key]=$value;}

        foreach ($listkeytoremove as $rkey) {
            if(array_key_exists($rkey,$newfilter)){
                unset($newfilter[$rkey]);
            }
        }

    }
    return $newfilter;
}
    public function changeSql($filter,$sql) {
        foreach ($filter as $key => $value) {
            $pos = stripos($key, "::");
            if ($pos !== false){
                $sql = str_replace($key, $value,$sql);
            }
        }
        return $sql;
    }
    
    public function getOperator($operator) {
        $op=" > ";
        if($operator==DOMAINTABLE::$OPERATOR_TEXT_FROM){$op=" >= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_AFTER){$op=" > ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_UNTIL){$op=" <= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_BEFORE){$op=" < ";}
       // else if($operator==DOMAINTABLE::$OPERATOR_TEXT_ON){$op=" = ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_ON){$op=" >= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_DIFFERENT){$op=" != ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_BETWEEN){$op=" >= ";}
        return $op;
    }
    
}
