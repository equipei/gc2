<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;

class BadiuHourFormCast {

   public function convertToForm($fminutes) {
      
       $hour=null;
       $minutes=null;
      
       
        if(!empty($fminutes) &&!is_array($fminutes)){ 
           $hour=$fminutes/60;
        
           $hour=floor($hour);
            $minutes=$fminutes % 60;
       }
        $formhour=array();
        $formhour['hour']=$hour;
        $formhour['minute']=$minutes;
        return $formhour;
   }

   public function convertFromForm($fparam) {
        $keyminute=null;
        $newparam=array();
		if(!is_array($fparam)){return $fparam;} 
         foreach ($fparam as $key => $value) {
             $pos=stripos($key, "_badiutimehour");
             if($pos!== false){
                 $p= explode("_", $key);
                 $hour=$value;
                 $minute=null;
                 $keyfield=null;
                 
                 if(isset($p[0])){
                       $keyfield=$p[0];
                       $keyminute=$keyfield.'_badiutimeminute';
                       if (array_key_exists($keyminute,$fparam)){$minute=$fparam[$keyminute];}
                       $nvalue=$this->convertToMinutes($hour,$minute);
                       $newparam[$keyfield]= $nvalue;
                   }
             }else{
                 $newparam[$key]= $value;
             }
             
         }
         unset($newparam[$keyminute]);
        return $newparam;
   }  
   
   public function convertToMinutes($hour,$minute) {
       if(empty($hour) && empty($minute)){return null;}
	    if (!ctype_digit($hour)) {$hour=0;}
		if (!ctype_digit($minute)) {$minute=0;}
       if(empty($minute)){$minute=0;}
        $mtime=$hour*60;
        $mtime=$mtime+$minute;
        $fminutes = $mtime;
        return $fminutes;
   }
}
