<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
//badiu.system.core.lib.form.factoryconfig
class FormFactoryConfig extends BadiuModelLib {

    private $key = null;
    private $bundle;
    private $kminherit;
    private $type='filter'; //data | filter
    private $defaultparam=array();
    private $datalayout;
    private $isexternalclient;
	private $cform;
	private $formFieldList;
	private $isedit=false;
	private $systemdata;
    public function __construct(Container $container) {
        parent::__construct($container);
    }

    public function get($key=null) {
        if(!empty($key)){$this->key=$key;}
       
        if(empty($this->key)){return null;}
        $bkey = $this->getBasekey();
         $this->initBudleKeymangerConfig($bkey);
        $isfilter=true;
        if($this->type=='data'){ $isfilter=false;}
        $fields = $this->getBundle()->getFormFields($isfilter);
          $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
          if($sysoperation->isEdit($this->key)){$this->isedit=true;}
        //set choicelistvalues
        $param = array();
        $cont = 0;
        foreach ($fields as $field) {
            //print_r($field);echo "<hr>";
            $optionslistvalues = $this->getFormDefaultOptions($field);
            $field->setChoicelistvalues($optionslistvalues);
            $param[$cont] = $field;
            $cont++;
        }
        
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
        $param = $serializer->serialize($param, 'json');
        $param = json_decode($param, true);

        //add tranlator label
        $formfield = array();
        $formfields = array();
        foreach ($param as $key => $value) {
            $value['label'] = $this->getTranslator()->trans($value['label']);
            $formfields[$key] = $value;
        }
        $formGroupList = null;
        $formFieldList = null;
        if ($this->type=='filter') {
            $formGroupList = $this->getBundle()->getGroupOfField($this->getBundle()->getKeymanger()->formFilterFieldsEnable(), $this->getBundle()->getKeymangerExtend()->formFilterFieldsEnable());
            $formFieldList = $this->getBundle()->getFieldsInGroup($this->getBundle()->getKeymanger()->formFilterFieldsEnable(), $this->getBundle()->getKeymangerExtend()->formFilterFieldsEnable());
        } else {
            $formGroupList = $this->getBundle()->getGroupOfField($this->getBundle()->getKeymanger()->formDataFieldsEnable(), $this->getBundle()->getKeymangerExtend()->formDataFieldsEnable());
            $formFieldList = $this->getBundle()->getFieldsInGroup($this->getBundle()->getKeymanger()->formDataFieldsEnable(), $this->getBundle()->getKeymangerExtend()->formDataFieldsEnable());
        }
		$this->setFormFieldList($formFieldList);
        //label form group
        $formGroupListLabel = array();
        if (!empty($formGroupList)) {

            foreach ($formGroupList as $key => $p) {
                $value = array('name' => $p, 'label' => $this->getTranslator()->trans($p));
                $formGroupListLabel[$key] = $value;
            }
            $formGroupList = $formGroupListLabel;
        }

		$type = $this->type;
		
        $cform=$this->getBundle()->getFormConfig($type,$this->getBundle()->getKeymanger());
		$this->setCform($cform);
        
        //add field group 
        $paramfield = $formfields;
        $formfields = array();
        $parametes=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
        $foperation=$this->getUtildata()->getVaueOfArray($parametes, 'foperation');
		
        $formdefultdata=$this->getContainer()->get('badiu.system.core.lib.form.defaultdata');
        $formdefultdata->setType($this->type);
       
        $this->changeDefaultparam();
        $formdefultdata->setParam($this->defaultparam);
      
	
        foreach ($paramfield as $key => $value) {
            $group = null;
            
            $fieldkey = $value['name'];
            $type = $value['type'];
			$defaultvalueonopernform=null;
			if($this->isedit || $foperation=='clone'){ $defaultvalueonopernform=$this->getUtildata()->getVaueOfArray($this->defaultparam,$fieldkey);}
           else{ $defaultvalueonopernform=$formdefultdata->get($fieldkey);}
         
         
            if (isset($formFieldList[$fieldkey])) {
                $group = $formFieldList[$fieldkey];
            }
            $value['group'] = $group;
			$value['grouplabel'] =$this->getTranslator()->trans($group);
            if($type=='badiufilterdate'){
                $confvalue=$this->getDefaultDataFromDateFilter($parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            }else if($type=='badiutimeperiod'){
                $confvalue=$this->getDefaultDataFromTimePeriod($parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            }else if($type=='badiuhour'){
                $confvalue=$this->getDefaultDataFromTimeHour($parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            }else if($type=='badiuhourtime'){
                $confvalue=$this->getDefaultDataFromTimeHourTime($parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            }else if($type=='date'){
                $confvalue=$this->getDefaultDataFromDate($parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            }else if($type=='datetime'){
                $confvalue=$this->getDefaultDataFromDate($parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            }
            else if($type=='badiudatedefault'){ 
                $confvalue=$this->getDefaultDataFromDatedefault($parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            }else if($type=='badiufilternumber'){
                $confvalue=$this->getDefaultDataFromNumberFilter($parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            }else if($type=='badiuchoicetext'){
                $confvalue=$this->getDefaultDataFromChoicetext($parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            }else if($type=='checkbox'){
                $confvalue=$this->getDefaultDataFromCheckbox($parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            }  else if($type=='badiuautocomplete' || $type=='badiuchoiceautocomplete'){
              
                $confvalue=$this->getDefaultDataFromAutocomplete($value,$parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            } 
			 else if($type=='badiuautocompleteselectmultiple'){
              
                $confvalue=$this->getDefaultDataFromAutocompleteselectmultiple($value,$parametes,$fieldkey,$defaultvalueonopernform);
                $value['value']=$confvalue;
            } 
            else{
                if (!array_key_exists($fieldkey,$parametes)){ $value['value']=$defaultvalueonopernform;}
                else{$value['value']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey);}
                
            }
            $formfields[$key] = $value;
        }
	
		
		$type = $this->type;
		$fconfig = $this->getBundle()->getValueQueryString($this->getBundle()->getKeymanger()->formConfig($type), $this->getBundle()->getKeymangerExtend()->formConfig($type));
		
        $formfields=$this->addFiedId($formfields);
        $formfields=$this->changeParamForTree($formfields);
        //form config

        $out = array();
        $out['formconfig'] = $fconfig;
        $out['formfields'] = $formfields;
        $out['formgroup'] = $formGroupList;
        $out['formgrouplabel'] = $formGroupListLabel;
        $out['reportcontentfile'] = $this->getBundle()->getValueQueryString($this->getBundle()->getKeymanger()->reportContentFile(), $this->getBundle()->getKeymangerExtend()->reportContentFile());
/*echo "<pre>";
	 print_r($out);
	 echo "</pre><hr>";exit;*/
		$out=$this->addSystemForm($out);
//echo "<pre>";        print_r($out);echo "</pre>";exit;
      
        return $out;
    }

    private function getBasekey() {
        $key = $this->key;
        $bkey = $this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
        return $bkey;
    }

   


    private function initBudleKeymangerConfig($bkey) {
        $keymanger = $this->getContainer()->get('badiu.system.core.functionality.keymanger');
        $keymanger->setBaseKey($bkey);
        $this->bundle = $this->getContainer()->get('badiu.system.core.lib.config.bundle');
        $this->bundle->setSessionhashkey($this->getSessionhashkey());
		$this->bundle->setSystemdata($this->getSystemdata());
        $this->bundle->setKeymanger($keymanger);
        $this->bundle->initKeymangerExtend($keymanger);
        
        $this->kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
        $this->kminherit->init($keymanger,$this->bundle->getKeymangerExtend()); 
    }

    private function getLabel($param) {
        $lparam = array();
        foreach ($param as $key => $value) {
            $pos = stripos($key, "_record");
            if ($pos === false) {
                $lparam[$key] = $this->getTranslator()->trans($key);
            } else {
                $lparam[$key] = $this->getLabelByService($value);
            }
        }
        return $lparam;
    }

    private function getLabelByService($config) {

        if (empty($config)) {
            return null;
        }
        $p = explode("|", $config);
        $keyservice = null;
        $function = null;
        $value = null;

        if (isset($p[2])) {
            $keyservice = $p[2];
        }
        if (isset($p[3])) {
            $function = $p[3];
        }

        if (!empty($keyservice) && !empty($function)) {
            $execfuncionservice = $this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice');
            $paramValue = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
            $value = $execfuncionservice->exec($keyservice, $function, $paramValue);
        }

        return $value;
    }

    private function getFormDefaultOptions($field) {
        $options = array();
        $configchoice = $this->getContainer()->get('badiu.system.core.lib.form.configchoice');
        $configchoice->setSessionhashkey($this->getSessionhashkey());
		$configchoice->setSystemdata($this->getSystemdata());
        if ($field->getType() == 'choice' || $field->getType() == 'badiuchoiceautocomplete') {
            $options = $configchoice->getDefaultChoice($field);
        }if ($field->getType() == 'radio') {
            $options = $configchoice->getDefaultChoice($field);
        }else if ($field->getType() == 'checkbox') {
            $options = $configchoice->getDefaultChoice($field);
        }else if ($field->getType() == 'badiuchoicetext') {
            $options = $configchoice->getDefaultChoice($field);
        }
        else if ($field->getType() == 'entity') {
             $options = $configchoice->getEntityChoice($field);
        }else if ($field->getType() == 'badiufilterdate') {
            $dateformoption = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
            $dateformoption->setSessionhashkey($this->getSessionhashkey());
            $options= $dateformoption->getOperatorText();
          
        }else if ($field->getType() == 'badiufilternumber') {
            $dateformoption = $this->getContainer()->get('badiu.system.core.lib.math.form.dataoptions');
            $dateformoption->setSessionhashkey($this->getSessionhashkey());
            $options= $dateformoption->getOperatorText();
           
        } 
       else  if ($field->getType() == 'badiutimeperiod') {
            $dateformoption = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
            $dateformoption->setSessionhashkey($this->getSessionhashkey());
            $options= $dateformoption->getTimeUnit();
          
        }else  if ($field->getType() == 'badiufulltimeperiod') {
            $dateformoption = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
            $dateformoption->setSessionhashkey($this->getSessionhashkey());
           // $options= $dateformoption->getOperatorFullPeriod();
           $options= $dateformoption->getTimeUnit();
          
        }
        else  if ($field->getType() == 'badiuhour') {
            $dateformoption = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
            $dateformoption->setSessionhashkey($this->getSessionhashkey());
            $options= $dateformoption->getMinutes();
         } 
     
        return $options;
    }

     function getDefaultDataFromDateFilter($parametes,$fieldkey,$defaultvalueonopernform) {
      
         if (!array_key_exists($fieldkey.'_badiudateoperator',$parametes)){return $defaultvalueonopernform;}
         $formcastdate=$this->getContainer()->get('badiu.system.core.lib.form.castdate');
         $dafaultdata=array();
         $dafaultdata['operator']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiudateoperator');
        
         $date1=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiudate1');
         $hour1=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiuhour1');
         $minute1=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiuminute1');
         $date2=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiudate2');
         $hour2=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiuhour2');
         $minute2=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiuminute2');
       
         $date1=$formcastdate->convertToDbDate($date1);
         $date2=$formcastdate->convertToDbDate($date2);

         $date1=$formcastdate->convertToForm($date1);
         $date2=$formcastdate->convertToForm($date2);

         $dafaultdata['date1']= $date1;
         $dafaultdata['hour1']= $hour1;
         $dafaultdata['minute1']= $minute1;
         
         $dafaultdata['date2']=$date2;
         $dafaultdata['hour2']= $hour2;
         $dafaultdata['minute2']= $minute2;
        // $dafaultdata['date1']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiudate1');
         //$dafaultdata['date2']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiudate2');
        // exit;
        return $dafaultdata;
    }

    function getDefaultDataFromDatedefault($parametes,$fieldkey,$defaultvalueonopernform) {
        $formcastdate=$this->getContainer()->get('badiu.system.core.lib.form.castdatedefault');
        $dafaultdata=$formcastdate->convertToForm($defaultvalueonopernform);
        return $dafaultdata;
   }
    function getDefaultDataFromNumberFilter($parametes,$fieldkey,$defaultvalueonopernform) {
         if (!array_key_exists($fieldkey.'_badiunumberoperator',$parametes)){return $defaultvalueonopernform;}
         $dafaultdata=array();
         $dafaultdata['operator']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiunumberoperator');
         $dafaultdata['number1']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiunumber1');
         $dafaultdata['number2']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiunumber2');
       
        return $dafaultdata;
    }
  function getDefaultDataFromTimePeriod($parametes,$fieldkey,$defaultvalueonopernform) {
		//check if is json
		if(!is_array($defaultvalueonopernform)){
			$rjson=$this->getJson()->decode($defaultvalueonopernform,true);
			if(is_array($rjson) && array_key_exists("unittime",$rjson)){return $rjson;}
		}
		
        if (!array_key_exists($fieldkey.'_badiutimeperiodunittime',$parametes)){return $defaultvalueonopernform;}
         $dafaultdata=array();
        $operator=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiutimeperiodoperator'); 
        if(!empty($operator)){$dafaultdata['operator']= $operator;} //delete on review all periodtime
        // $dafaultdata['operator']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiutimeperiodoperator'); //enable on review all periodtime
         $dafaultdata['unittime']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiutimeperiodunittime');
         $dafaultdata['number']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiutimeperiodnumber');
         
        return $dafaultdata;
    }

    function getDefaultDataFromTimeHour($parametes,$fieldkey,$defaultvalueonopernform) {
         $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
         $foperation=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
        if($sysoperation->isEdit($this->getKey()) ||  $foperation=='clone'){
             $dafaultdata=array();
             
             //get default service key .autocomplete edif funcion
             //print_r($this->defaultparam);exit;
              $minutes=$this->getUtildata()->getVaueOfArray($this->defaultparam,$fieldkey);
              $badiuhour=$this->getContainer()->get('badiu.system.core.lib.form.casthour');
              $dafaultdata=$badiuhour->convertToForm($minutes);
              return $dafaultdata;
        }
        if (!array_key_exists($fieldkey.'_badiutimehour',$parametes)){return $defaultvalueonopernform;}
         $dafaultdata=array();
         $dafaultdata['hour']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiutimehour');
         $dafaultdata['minute']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiutimeminute');
         
        return $dafaultdata;
    }
    function getDefaultDataFromTimeHourTime($parametes,$fieldkey,$defaultvalueonopernform) {
         $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
         $foperation=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
        if($sysoperation->isEdit($this->getKey()) ||  $foperation=='clone'){
             $dafaultdata=array();
             
             //get default service key .autocomplete edif funcion
             //print_r($this->defaultparam);exit;
              $minutes=$this->getUtildata()->getVaueOfArray($this->defaultparam,$fieldkey);
              $badiuhour=$this->getContainer()->get('badiu.system.core.lib.form.casthourtime');
              $dafaultdata=$badiuhour->convertToForm($minutes);
              return $dafaultdata;
        }
        if (!array_key_exists($fieldkey.'_badiutimehour',$parametes)){return $defaultvalueonopernform;}
         $dafaultdata=array();
         $dafaultdata['hour']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiutimehour');
         $dafaultdata['minute']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiutimeminute');
         
        return $dafaultdata;
    }
    function getDefaultDataFromDate($parametes,$fieldkey,$defaultvalueonopernform) {
      
         $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
         $foperation=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
        if($sysoperation->isEdit($this->getKey()) ||  $foperation=='clone'){
             $dafaultdata=null;             
              $timestamp=$this->getUtildata()->getVaueOfArray($this->defaultparam,$fieldkey);
             
              if(!empty($timestamp )){
                   $date=new \DateTime();
                   $date->setTimestamp($timestamp);
                   $castdate=$this->getContainer()->get('badiu.system.core.lib.form.castdate');
                   $castdate->setSessionhashkey($this->getSessionhashkey());
                   $dafaultdata=$castdate->convertToForm($date);
               }
             
              return $dafaultdata;
        }
        if (!array_key_exists($fieldkey,$parametes)){return $defaultvalueonopernform;}
      
         $dafaultdata=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey);
         if(empty($dafaultdata)){$dafaultdata=$this->getUtildata()->getVaueOfArray($this->defaultparam,$fieldkey);}
         
        return $dafaultdata;
    }
    function getDefaultDataFromChoicetext($parametes,$fieldkey,$defaultvalueonopernform) {
        //if is used on edit form
        
         if (!array_key_exists($fieldkey, $this->defaultparam) && isset($this->defaultparam[$fieldkey]) && is_array($this->defaultparam[$fieldkey])){return $this->defaultparam[$fieldkey];}
         
        if (!array_key_exists($fieldkey.'_badiutchoicetextfield1',$parametes)){return $defaultvalueonopernform;}
         $dafaultdata=array();
         $dafaultdata['field1']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiutchoicetextfield1');
         $dafaultdata['field2']=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey.'_badiutchoicetextfield2');
         
        return $dafaultdata;
    }
	function  getDefaultDataFromCheckbox($parametes,$fieldkey,$defaultvalueonopernform) {
        $dafaultdata=array();
        
		if(!empty($defaultvalueonopernform)){
			$dafaultdata=array();
			$pos=stripos($defaultvalueonopernform, ",");
              if($pos=== false){
                  $dafaultdata=array($defaultvalueonopernform);
              }else{
                  $dafaultdata= explode(",", $defaultvalueonopernform);
              }
		}
          
        return $dafaultdata;
    }
   
    function getDefaultDataFromAutocomplete($fieldconfig,$parametes,$fieldkey,$defaultvalueonopernform) {
        
         $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
         $foperation=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
         //if($sysoperation->isEdit($this->getKey()) ||  $foperation=='clone' || empty($defaultvalueonopernform)){
             $dafaultdata=array();
    
             //get default service key .autocomplete edif funcion
             $id=null;//$this->getUtildata()->getVaueOfArray($this->defaultparam,$fieldkey);

             if(empty($id) && !is_array($defaultvalueonopernform)){$id=$defaultvalueonopernform;}
             if(empty($id)){  $id=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey);}
             
			 $itemname=$this->getItemSelectedNomeOfAutocompleteOnEdit($fieldconfig,$id,$this->defaultparam);
              $dafaultdata['field1']=$id;
              $dafaultdata['field2']=$itemname;
			  
			  //for badiuchoiceautocomplete
			  $fieldkeyv1=$fieldkey.'v1';
			  
			 if (array_key_exists($fieldkeyv1,$parametes)){
				 $valuev1=$this->getUtildata()->getVaueOfArray($parametes,$fieldkeyv1);
				 $dafaultdata['field3']=$valuev1;
			 }
			 
              return $dafaultdata;
        /*}else{
             return $defaultvalueonopernform;
         }*/
       
        return null;
    }
    
    function getItemSelectedNomeOfAutocompleteOnEdit($fieldconfig,$id,$dto) {
        $paramchoiconfig=$this->getUtildata()->getVaueOfArray($fieldconfig,'choicelist');
		$type=$this->getUtildata()->getVaueOfArray($fieldconfig,'type');
        $itemname='';
		
        if(empty($paramchoiconfig)){return '';}
        $p=explode ("|",$paramchoiconfig);
        $servicekey=$this->getUtildata()->getVaueOfArray($p,1);
        $function=$this->getUtildata()->getVaueOfArray($p,3);
        $functionparam=$this->getUtildata()->getVaueOfArray($p,4);
		$functionparamclone= $functionparam;
		
		if(empty($functionparam)){$functionparam=$id;}
        else{$functionparam=$this->getUtildata()->getVaueOfArray($dto,$functionparam);}
        if($type=='badiuchoiceautocomplete' || $type=='badiuautocomplete'  && $functionparamclone != 'dconfig'){$functionparam=$id;}
        if(empty( $function)){ $function='getNameById';}
        else if($function=='n' || $function=='y'){ $function='getNameById';}
        if(empty($servicekey)){return '';}
        if ($this->getContainer()->has($servicekey)) {
            $service=$this->getContainer()->get($servicekey);
            if (method_exists($service, $function)) {$itemname=$service->$function($functionparam);}
        }
        
        return $itemname;
    }
	
	function getDefaultDataFromAutocompleteselectmultiple($fieldconfig,$parametes,$fieldkey,$defaultvalueonopernform) {
        
         $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
         $foperation=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
         //if($sysoperation->isEdit($this->getKey()) ||  $foperation=='clone' || empty($defaultvalueonopernform)){
             $dafaultdata=array();
    
             //get default service key .autocomplete edif funcion
             $lata=$this->getUtildata()->getVaueOfArray($this->defaultparam,$fieldkey);

             if(empty($lata) && !is_array($defaultvalueonopernform)){$lata=$defaultvalueonopernform;}
             if(empty($lata)){  $lata=$this->getUtildata()->getVaueOfArray($parametes,$fieldkey);}
             if(empty($lata)){$lata="[]";}
			 else{
				$lata= $this->getJson()->encode($lata);
			 }
			  $dafaultdata['field1']=$lata;
			 return $dafaultdata;
        /*}else{
             return $defaultvalueonopernform;
         }*/
       
        return null;
    }
    function addFiedId($formfields) {
    $fiedid=null;
    $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
     $lastfield=end($formfields);
     $group=$this->getUtildata()->getVaueOfArray($lastfield,'group');
    if($sysoperation->isEdit($this->getKey())){
        if(!empty($formfields) && is_array($formfields) && sizeof($formfields)> 0){
            $idvaule=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('id');
             $fiedid=array('name'=>'id','required' => 1,'cssClasss' =>  ' ','type' => 'hidden','choicelist' => null,'label' => 'ID','choicelistvalues' => null,'format' => null,'value' => $idvaule,'group' => $group);
             array_push($formfields,$fiedid);
        }
       
        
        
     }
     if($this->getIsexternalclient()){
          $parentid = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
          if($parentid){
             $fiedid=array('name'=>'parentid','required' => 1,'cssClasss' =>  ' ','type' => 'hidden','choicelist' => null,'label' => 'parentid','choicelistvalues' => null,'format' => null,'value' => $parentid,'group' => $group);
             array_push($formfields,$fiedid);
          }
            $key = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_key');
          if($key){
              $fiedid=array('name'=>'_key','required' => 1,'cssClasss' =>  ' ','type' => 'hidden','choicelist' => null,'label' => '_key','choicelistvalues' => null,'format' => null,'value' => $key,'group' => $group);
             array_push($formfields,$fiedid);
          }
     }
     return $formfields;
}

    function changeDefaultparam(){
            $controller=null;
            if(!$this->getContainer()->has($this->kminherit->formcontroller())){
                $controller=$this->getContainer()->get('badiu.system.core.functionality.form.controller');
             }else{$controller=$this->getContainer()->get($this->kminherit->formcontroller());}
            $controller->setParam($this->defaultparam);
            $controller->setKminherit($this->kminherit);
			$controller->setCform($this->getCform());
			$controller->setFormFieldList($this->getFormFieldList());
			$controller->setKey($this->getKey());
            $controller->changeParamOnOpen();
            $this->defaultparam=$controller->getParam();
           
            
    }
    function changeParamForTree($formfields){

         $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
         if($sysoperation->isEdit($this->getKey()) && $this->datalayout=='datatreewithname'){
             $newformfields=array();
              foreach ($formfields as $field) {
                  $name=$this->getUtildata()->getVaueOfArray($field, 'name');
                  if($name=='idpath'){
                      $value= $this->getUtildata()->getVaueOfArray($field, 'value');
                      $defaultlist=$this->getUtildata()->getVaueOfArray($field, 'choicelistvalues');
                      unset($defaultlist[$value]);
                      $factorykeytree=$this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
                      $factorykeytree->setSessionhashkey($this->getSessionhashkey());
                      $value= $factorykeytree->getFather($value);
                       $field['value']=$value;    
                       $field['choicelistvalues']=$defaultlist;
                 }
                   array_push($newformfields,$field);
              }
             $formfields= $newformfields;
           }
      
        return $formfields;
    }
	
	 function addSystemForm($fdata) {
		 
		$sysform=$this->getUtildata()->getVaueOfArray($fdata,'formconfig.sysform',true);
		if(empty($sysform)){return $fdata;}
		
		$adminformattemptid=null;
		$lfields=$this->getUtildata()->getVaueOfArray($fdata,'formfields');
		foreach ($lfields as $key => $row) {
			$fname=$this->getUtildata()->getVaueOfArray($row,'name');
			$fvalue=$this->getUtildata()->getVaueOfArray($row,'value');
			if($fname=='adminformattemptid'){$adminformattemptid=$fvalue;break;}
		}
		$formfactorydata=$this->getContainer()->get('badiu.admin.form.lib.factorydata');
		$fparam=array('formshortname'=>$sysform,'attemptid'=>$adminformattemptid);
		$sffdata=$formfactorydata->make($fparam);
		
		$sourcefields=$this->getUtildata()->getVaueOfArray($sffdata,'fields');
		//$targetfields=$this->getUtildata()->getVaueOfArray($fdata,'formfields');
		foreach ($sourcefields as $row) {
			array_push($fdata['formfields'],$row);
		}
		//$fdata['formfields']=$targetfields;
		
		$sourcegroups=$this->getUtildata()->getVaueOfArray($sffdata,'groups');
		$sysformshowafter=$this->getUtildata()->getVaueOfArray($fdata,'formconfig.sysformshowafter',true);
		//echo $sysformshowafter;exit;
		if(empty($sysformshowafter)){
			foreach ($sourcegroups as $key => $row) {
				$ngroup=array('name'=>$key,'label'=>$row);
				if(is_array($fdata['formgrouplabel'])){array_push($fdata['formgrouplabel'],$ngroup);}
				if(is_array($fdata['formgroup'])){array_push($fdata['formgroup'],$ngroup);}
			}
		}else{
			$newformgroup=array();
			$newformgrouplabel=array();
			
			$cformgroup=$this->getUtildata()->getVaueOfArray($fdata,'formgroup');
			$cformgrouplabel=$this->getUtildata()->getVaueOfArray($fdata,'formgrouplabel');
			
			//group
			$contadd=0;
			foreach ($cformgroup as $key => $row) {
				$fgname=$this->getUtildata()->getVaueOfArray($row,'name');
				array_push($newformgroup,$row);
				if($fgname==$sysformshowafter){
					foreach ($sourcegroups as $key1 => $row1) {
						$ngroup1=array('name'=>$key1,'label'=>$row1);
						array_push($newformgroup,$ngroup1);$contadd++;
					}
				}
			}
			if($contadd==0){
				foreach ($sourcegroups as $key1 => $row1) {
						$ngroup1=array('name'=>$key1,'label'=>$row1);
						array_push($newformgroup,$ngroup1);
				}
			}
			
			
			//groulabel
			
			$contadd=0;
			foreach ($cformgrouplabel as $key => $row) {
				$fgname=$this->getUtildata()->getVaueOfArray($row,'name');
				array_push($newformgrouplabel,$row);
				if($fgname==$sysformshowafter){
					foreach ($sourcegroups as $key1 => $row1) {
						$ngroup1=array('name'=>$key1,'label'=>$row1);
						array_push($newformgrouplabel,$ngroup1);$contadd++;
					}
				}
			}
			if($contadd==0){
				foreach ($sourcegroups as $key1 => $row1) {
						$ngroup1=array('name'=>$key1,'label'=>$row1);
						array_push($newformgrouplabel,$ngroup1);
				}
			}
			$fdata['formgroup']=$newformgroup;
			$fdata['formgrouplabel']=$newformgrouplabel;
			
		}
	/*	 echo "<pre>";
	 print_r($fdata);
	 echo "</pre><hr>";exit;
	 
		 echo "<pre>";
	 print_r($sffdata);
	 echo "</pre>";exit;*/
		return $fdata;
	 }
    function getKey() {
        return $this->key;
    }

    function setKey($key) {
        $this->key = $key;
    }

    function getBundle() {
        return $this->bundle;
    }

    function setBundle($bundle) {
        $this->bundle = $bundle;
    }

    function getType() {
        return $this->type;
    }

    function setType($type) {
        $this->type = $type;
    }
    function getDefaultparam() {
        return $this->defaultparam;
    }

    function setDefaultparam($defaultparam) {
        $this->defaultparam = $defaultparam;
    }

    function getKminherit() {
        return $this->kminherit;
    }

    function setKminherit($kminherit) {
        $this->kminherit = $kminherit;
    }

    function getIsexternalclient() {
        return $this->isexternalclient;
    }

    function setIsexternalclient($isexternalclient) {
        $this->isexternalclient = $isexternalclient;
    }


    function getDatalayout() {
        return $this->datalayout;
    }

    function setDatalayout($datalayout) {
        $this->datalayout = $datalayout;
    }


  function getCform() {
      return $this->cform;
  }
  
   function setCform($cform) {
      $this->cform = $cform;
  }
  
   public function setFormFieldList($formFieldList)
    {
        $this->formFieldList = $formFieldList;
	}
	
	public function getFormFieldList()
    {
        return $this->formFieldList;
    }
	
	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
