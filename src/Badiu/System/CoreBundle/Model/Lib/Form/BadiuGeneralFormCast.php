<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class BadiuGeneralFormCast extends BadiuModelLib {
private $formFieldTypeList;
    public function __construct(Container $container) {
        parent::__construct($container);
    }
   public function convertToForm($fparam) {
        if(empty($this->getFormFieldTypeList())){return $fparam;}
		if(!is_array($this->getFormFieldTypeList())){return $fparam;}
		
		if(empty($fparam)){return $fparam;}
		if(!is_array($fparam)){return $fparam;}
		
		 $filelib=$this->getContainer()->get('badiu.system.file.file.lib');
		
        $newparam=array();
         foreach ($fparam as $key => $value) {
           $type = $this->getUtildata()->getVaueOfArray($this->getFormFieldTypeList(), $key);
            if($type=='textarea'){
				
				
			
			}
			$newparam[$key]=$value;
         }
		
        return $newparam;
   }

   public function convertFromForm($fparam) { 
    	
		if(empty($this->getFormFieldTypeList())){return $fparam;}
		if(!is_array($this->getFormFieldTypeList())){return $fparam;}
		
		if(empty($fparam)){return $fparam;}
		if(!is_array($fparam)){return $fparam;}
		
		 $filelib=$this->getContainer()->get('badiu.system.file.file.lib');
		
        $newparam=array();
         foreach ($fparam as $key => $value) {
           $type = $this->getUtildata()->getVaueOfArray($this->getFormFieldTypeList(), $key);
            if($type=='numberint'){
				if(!empty( $value)){ $value= str_replace(".", "", $value);}
				 $value= preg_replace("/[^0-9]/", "",  $value);
			}else  if($type=='numberdouble'){
				if(!empty( $value)){ $value= str_replace(".", "", $value);}
				if(!empty( $value)){ $value= str_replace(",", ".", $value);}
				 $value= preg_replace("/[^0-9.]/", "",  $value);
			}
			$newparam[$key]=$value;
         }
		
        return $newparam;
   }  
   
 

 public function setFormFieldTypeList($formFieldTypeList)
    {
        $this->formFieldTypeList = $formFieldTypeList;
	}
	
	public function getFormFieldTypeList()
    {
        return $this->formFieldTypeList;
    }	
}
