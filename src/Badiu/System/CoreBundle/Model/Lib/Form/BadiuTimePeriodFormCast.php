<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Date\DOMAINTABLE;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class BadiuTimePeriodFormCast extends BadiuModelLib{

    function __construct(Container $container) {
        parent::__construct($container);
  
}
   public function convertToForm($value) {
   
        return null;
   }

   public function convertFromForm($form,$timeback=true) {
        $number=null;
        $unittime=null;
        $operator="-";
       
        if(!$timeback){$operator="+";}
        if(isset($form['number'])){$number=$form['number'];}
        if(isset($form['unittime'])){$unittime=$form['unittime'];}
        
        $now=null;
        if($number >0 && !empty($unittime)){
             $now =new \Datetime();
          
             if($unittime==DOMAINTABLE::$TIME_UNIT_SECOND){$now->modify("$operator $number seconds ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_MINUTE){$now->modify("$operator $number minutes ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_HOUR){$now->modify("$operator $number hours ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_DAY){$now->modify("$operator $number day ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_WEEK){ $number=$number*7;$now->modify("$operator $number day ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_MONTH){$now->modify("$operator $number month ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_YEAR){$number=$number*12; $now->modify("$operator $number month ");}
        }
        
        return $now;
   }
 
   /*
public function changeParam($filter,$timeback=true) {

        $newfilter=array();
        $isparamofhtmlform=false;
        $newfilter=array();
      
           foreach ($filter as $key => $value) {
               
                $form=array();
                $pos=stripos($key, "_badiutimeperiodunittime");
                if($pos!== false){
                  
                    $isparamofhtmlform=true;
                   $p= explode("_", $key);
                   
                   $keynumber=null;
                   $number=null;
                   $operatortxt=null;
                   $operatorkey=null;
                   $keyfield=null;
                   if(isset($p[0])){
                       $keyfield=$p[0];
                       $keynumber=$keyfield.'_badiutimeperiodnumber';
                       $operatorkey=$keyfield.'_badiutimeperiodoperator';
                       if (array_key_exists($operatorkey,$filter)){$operatortxt=$filter[$operatorkey];}
                   }
                   $newfilter=$this->makeDateByOpertor($newfilter,$value,$number,$keyfield,$operatortxt);
                   $form['unittime']=$value; //review this item DELETE
                   $form['number']=$number; //review this item DELETE
                  
                   $timeback=$this->isTimeBack($keyfield);
                   $value=$this->convertFromForm($form,$timeback);
                  // echo $keyfield;print_r($value);
                  
                    $newfilter[$keyfield]=$value; //review this  DELETE
                   if(!empty($value)){ $newfilter[$keyfield."now"]=new \Datetime();} //review this item  DELETE

                   
                }else{
                    if(!array_key_exists($key,$newfilter)){$newfilter[$key]=$value;}
            
                }
               
           }
        
    
         if(!$isparamofhtmlform){
            
             
             foreach ($filter as $key => $value) {
        
           if(!empty($value) && is_array($value)) {
               if(array_key_exists("unittime",$value) && array_key_exists("number",$value)){
                    $timeback=$this->isTimeBack($key);
                   
                   $newvalue=$this->convertFromForm($value,$timeback);
                     $newfilter[$key]=$newvalue;
                     if(!empty($newvalue)){ $newfilter[$key."now"]=new \Datetime();}
                 }
                 
           }else{
               $newfilter[$key]=$value;
           }
        }
         }
       
      
        return $newfilter;
    }  
*/

public function changeParam($filter) {
    $isform=$this->isFromform($filter);
    if($isform){
		if(!is_array($filter)){return $filter;} 
        foreach ($filter as $key => $value) {
            if(is_array($value)){unset($filter[$key]);}
        }
    }
    $newfilter=array();
    if($isform){
        $newfilter=$this->changeParamFromform($filter);
    }else{
        $newfilter=$this->changeParamDefault($filter);
    }
    
  /*echo "<pre>";
    print_r($newfilter);
    echo "</pre>";exit;    */
    return $newfilter;
}
public function isFromform($filter) {
    $result=false;
    foreach ($filter as $key => $value) {
        $pos=stripos($key, "_badiutimeperiodunittime");
        if($pos!== false){
            $result=true;
            break;
       }
    }
   return $result;
}

public function changeParamFromform($filter,$out='fdate') {
    $newfilter=array();
	if(!is_array($filter)){return $filter;} 
    foreach ($filter as $key => $value) {
        if(!is_array($value)){
			$isperiodtime=false;
			$pos=stripos($key, "_badiutimeperiodunittime");
			if($pos!== false){$isperiodtime=true;}
			$pos=stripos($key, "_badiutimeperiodnumber");
			if($pos!== false){$isperiodtime=true;}
			$pos=stripos($key, "_badiutimeperiodoperator");
			if($pos!== false){$isperiodtime=true;}
            $keyoperator=$key."_badiutimeperiodoperator";
			
            $pos=stripos($key, "_badiutimeperiodunittime");
            $keyoperator=$key."_badiutimeperiodoperator";
            if($isperiodtime){
                $p= explode("_", $key);
                $keyfield=null;
             
                if(isset($p[0]) && $out=='fdate'){
                    $keyfield=$p[0];
                    $keynumber=$keyfield.'_badiutimeperiodnumber';
                    $operatorkey=$keyfield.'_badiutimeperiodoperator';
                    $unittimekey=$keyfield.'_badiutimeperiodunittime';
                   
                    $existoperator=array_key_exists($operatorkey,$filter);
                    if($existoperator && !empty($filter[$operatorkey])){
                        $operator=$filter[$operatorkey];
                        $unittime=$filter[$unittimekey];
                        $number=$filter[$keynumber];
                        $newfilter=$this->makeDateByOpertor($newfilter,$unittime,$number,$keyfield,$operator);
                 
                }else{
                     $newfilter=$this->changeParamSingle($filter,$newfilter,$key,$value);//review delete
                  }
            }else  if(isset($p[0]) && $out=='json'){
					$keyfield=$p[0];
                    $operator=$this->getUtildata()->getVaueOfArray($filter,$keyfield.'_badiutimeperiodoperator');
                    $unittime=$this->getUtildata()->getVaueOfArray($filter,$keyfield.'_badiutimeperiodunittime');
                    $number=$this->getUtildata()->getVaueOfArray($filter,$keyfield.'_badiutimeperiodnumber');
						
					$fdata=array('operator'=>$operator,'number'=>$number,'unittime'=>$unittime); 
					$fdata=$this->getJson()->encode($fdata);
					
					$newfilter[$keyfield]=$fdata;
					if(array_key_exists($keyfield.'_badiutimeperiodoperator',$newfilter)){unset($newfilter[$keyfield.'_badiutimeperiodoperator']);}
					if(array_key_exists($keyfield.'_badiutimeperiodunittime',$newfilter)){unset($newfilter[$keyfield.'_badiutimeperiodunittime']);}
					if(array_key_exists($keyfield.'_badiutimeperiodnumber',$newfilter)){unset($newfilter[$keyfield.'_badiutimeperiodnumber']);}
					
					
				}
			else{$newfilter[$key]=$value;}
                 
          }else{$newfilter[$key]=$value;}
        }else{$newfilter[$key]=$value;}
    }
	
	
    return $newfilter;
}


public function changeParamFromformCasToArray($filter) {
    $newfilter=array();
    $listkeytoremove=array();
    foreach ($filter as $key => $value) {
        if(!is_array($value)){
            $pos=stripos($key, "_badiutimeperiodunittime");
            
            if($pos!== false){
                $p= explode("_", $key);
                $keyfield=null;
             
                if(isset($p[0])){
                    $keyfield=$p[0];
                    $existoperator=array_key_exists($keyfield,$newfilter);
                    if(!$existoperator){
                        $keynumber=$keyfield.'_badiutimeperiodnumber';
                        $operatorkey=$keyfield.'_badiutimeperiodoperator';
                        $unittimekey=$keyfield.'_badiutimeperiodunittime';
                       
                        $operator='pastfrom';
                        $unittime=$this->getUtildata()->getVaueOfArray($filter,$unittimekey);
                        $number=$this->getUtildata()->getVaueOfArray($filter,$keynumber);
                        $existoperator=array_key_exists($operatorkey,$filter);
                        if($existoperator){ $operator=$this->getUtildata()->getVaueOfArray($filter,$operatorkey);}
                        $paramdp=array('number'=>$number,'unittime'=>$unittime,'operator'=>$operator);
                        $newfilter[$keyfield]=$paramdp;
                        array_push($listkeytoremove,$keynumber);
                        array_push($listkeytoremove,$operatorkey);
                        array_push($listkeytoremove,$unittimekey);
                    }
                    
            }else{$newfilter[$key]=$value;}
                 
            }else{$newfilter[$key]=$value;}
        }else{$newfilter[$key]=$value;}

        foreach ($listkeytoremove as $rkey) {
            if(array_key_exists($rkey,$newfilter)){
                unset($newfilter[$rkey]);
            }
        }

    }
    return $newfilter;
}

public function changeParamDefault($filter) {
    $newfilter=array();
    foreach ($filter as $key => $value) {
        if(is_array($value)){
            if(array_key_exists("unittime",$value) && array_key_exists("number",$value)&& array_key_exists("operator",$value)){
                $isparamofhtmlform=false;
                $operator=$value['operator'];
                $unittime=$value['unittime'];
                $number=$value['number'];
                $newfilter=$this->makeDateByOpertor($newfilter,$unittime,$number,$key,$operator);
            } else if(array_key_exists("unittime",$value) && array_key_exists("number",$value)&& !array_key_exists("operator",$value)){
                $newfilter=$this->changeParamSingle($filter,$newfilter,$key,$value);//review delete 
            }
        }else{$newfilter[$key]=$value;}
    }
    return $newfilter;
}
//review delte

public function changeParamSingle($filter,$newfilter,$key,$value) {

   
            $isparamofhtmlform=false;
    
  
           
            $form=array();
            $pos=stripos($key, "_badiutimeperiodunittime");
            if($pos!== false){
               
                $isparamofhtmlform=true;
               $p= explode("_", $key);
               
               $keynumber=null;
               $number=null;
               $keyfield=null;
               if(isset($p[0])){
                   $keyfield=$p[0];
                   $keynumber=$keyfield.'_badiutimeperiodnumber';
                   if (array_key_exists($keynumber,$filter)){$number=$filter[$keynumber];}
               }
              
               $form['unittime']=$value;
               $form['number']=$number;
               $timeback=$this->isTimeBack($keyfield);
               $value=$this->convertFromForm($form,$timeback);
                 $newfilter[$keyfield]=$value;
               if(!empty($value)){ $newfilter[$keyfield."now"]=new \Datetime();}
            }else{
                if(!array_key_exists($key,$newfilter)){$newfilter[$key]=$value;}
        
            }
           

    
 
     if(!$isparamofhtmlform){
        
         
 
    
       if(!empty($value) && is_array($value)) {
           if(array_key_exists("unittime",$value) && array_key_exists("number",$value)){
                $timeback=$this->isTimeBack($key);
               
               $newvalue=$this->convertFromForm($value,$timeback);
                 $newfilter[$key]=$newvalue;
                 if(!empty($newvalue)){ $newfilter[$key."now"]=new \Datetime();}
             }
             
       }else{
           $newfilter[$key]=$value;
       }

     }
   
  
    return $newfilter;
}  

    public function changeSql($filter,$sql) {
        foreach ($filter as $key => $value) {
            $pos = stripos($key, "::");
            if ($pos !== false){
                $sql = str_replace($key, $value,$sql);
            }
        }
        return $sql;
    }

    //review and delete
public function isTimeBack($key){
    
    $pos=strrpos($key,"will");
    if ($pos !== false && strlen("will") + $pos == strlen($key)){
      
        return false;
    }
   
    return true;
}


public function paramkeyToArray($filter,$paramkey) {

        $newp=array();
        $newp['operator']= $this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiutimeperiodoperator');
        $newp['number']= $this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiutimeperiodnumber');
        $newp['unittime']= $this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiutimeperiodunittime');
       
    return $newp;
    }  

    public function existParamkey($filter,$paramkey) {
        $r=true;
        if (!array_key_exists($paramkey.'_badiutimeperiodnumber',$filter)){$r=false;}
        if (!array_key_exists($paramkey.'_badiutimeperiodunittime',$filter)){$r=false;}
        return $r;
       
    } 
    public function existParamkeyValue($filter,$paramkey) {
        $r=false;
        $key=
        $number=$this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiutimeperiodnumber');
        $unittime=$this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiutimeperiodunittime');
        if(!empty($number) && !empty($unittime)){
            $r=true;
        }
        return $r;
       
    }
    public function makeDateByOpertor($filter,$unittime,$number,$fieldkey,$operator) {
       
        if(empty($operator)) {return $filter;}
        $badiudate1=null;
        $badiudate2=null;
        $paramdp=array('number'=>$number,'unittime'=>$unittime,'operator'=>$operator);
        $dateperiod=$this->getContainer()->get('badiu.system.core.lib.date.periodutil')->makeDateByOperator($paramdp);
        $badiudate1=$dateperiod->date1;
        $badiudate2=$dateperiod->date2;

        $keyoperator="::".$fieldkey."operator";
        $keybadiudate1=$fieldkey."1";
        $keybadiudate2=$fieldkey."2";
        $matoperator=$this->getOperator($operator);
        
        $filter[$keyoperator]=$matoperator;
        $filter[$keybadiudate1]=$badiudate1;
        $filter[$keybadiudate2]=$badiudate2;

        return $filter;
    }
    public function getOperator($operator) {
        $op=" > ";
        //past
        if($operator==DOMAINTABLE::$OPERATOR_PAST_TEXT_FROM){$op=" >= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_PAST_TEXT_UNTIL){$op=" < ";}
        else if($operator==DOMAINTABLE::$OPERATOR_PAST_TEXT_ON){$op=" >= ";}
      

        //future
        else if($operator==DOMAINTABLE::$OPERATOR_FUTURE_TEXT_UNTIL){$op=" <= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_FUTURE_TEXT_AFTER){$op=" >= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_FUTURE_TEXT_ON){$op=" >= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_FUTURE_TEXT_FROM){$op=" >= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_BETWEEN){$op=" >= ";}
        return $op;
    }
}
