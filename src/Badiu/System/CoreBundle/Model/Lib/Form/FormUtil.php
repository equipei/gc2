<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Form;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;
/**
 * Description of MakeSelect
 *
 * @author lino
 */
class FormUtil {
    
  /**
     * @var Container
     */
    private $container;

      /**
     * @var Translator
     */
    private $translator;
    
    function __construct(Container $container) {
                $this->container=$container;
                $this->translator=$this->container->get('translator');
      }
     public function getBooleanOptions() {
         $list=array();
         $list[0]=$this->getTranslator()->trans('no');
         $list[1]=$this->getTranslator()->trans('yes');
         return $list;
     } 
     public function getBooleanOptionsLabel($key) {
        $value="";
        if($key==0){$value=$this->getTranslator()->trans('no');}
        if($key==1){$value=$this->getTranslator()->trans('yes');}
        return $value;
    }
     public function getPercentage() {
        $list=array();
        for($i=0;$i<101;$i++){
            $list[$i]="$i %";
        }
        return $list;
    } 
   public function getPercentageDesc() {
		$process=true;
		$max=100;
		
	   while ($process) {
			$list[$max]="$max %";
			$max--;
			if($max < 0){$process=false;break;}
		}
        
        return $list;
    } 
      public function convertBdArrayToListOptions($rows) {
          $list=array();
		  $valuelist=0;
          foreach ($rows  as $row) {
                $cont=0;
                $k="";
                $v="";
                $v1="";
                foreach ($row  as $key =>$value) {
                    if($cont==0){$k=$value;}
                    if($cont==1){$v=$value;}
					if($cont==2){$v1=$value;}
                    $cont++;
                }
				if(!empty($v1)){$valuelist=1;}
				if($valuelist){$v=array('name'=>$v,'shortname'=>$v1);}
                $list[$k]=$v;
           }
           
          return $list;
     } 
     
     public function getContainer() {
         return $this->container;
     }

     public function setContainer(Container $container) {
         $this->container = $container;
     }

     public function getTranslator() {
         return $this->translator;
     }

     public function setTranslator(Translator $translator) {
         $this->translator = $translator;
     }


         
}
