<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class BadiuDateFormCast extends BadiuModelLib {

    public function __construct(Container $container) {
        parent::__construct($container);
    }
   public function convertToForm($data) {
        if (is_object($data) && is_a($data, 'DateTime')) {
             $data = $data->format('Y-m-d\TH:i:s\Z');
        }
        return $data;
   }

   public function convertFromForm($fparam) { 
      //echo "sssd";exit;
        $newparam=array();
		if(!is_array($fparam)){return $fparam;} 
         foreach ($fparam as $key => $value) {
             $pos=stripos($key, "_badiutdate");
             if($pos!== false){
                 $p= explode("_", $key);
                 $keyfield=null;
                 $nvalue=null;
                 if(isset($p[0])){
                       $keyfield=$p[0];
                       $nvalue=$this->convertToDbDate($value);
                       $newparam[$keyfield]= $nvalue;
                   } 
             }else{
                 $newparam[$key]= $value;
             }
             
         }
         
        return $newparam;
   }  
   
   public function convertToDbDate($date,$param=array()) {
        if(empty($date)){return null;}
        if($date=='null'){return null;}
         $p=explode("(",$date);
         if(isset($p[0])){
             $ndate=trim($p[0]);
              $ndate=new \DateTime($ndate);
              $ndate->setTimezone(new \DateTimeZone('America/Sao_Paulo'));
              $hour =$this->getUtildata()->getVaueOfArray($param, 'hour');
              if(empty($hour)){$hour=0;}
              $minute =$this->getUtildata()->getVaueOfArray($param, 'minute');
              if(empty($minute)){$minute=0;}
              $second =$this->getUtildata()->getVaueOfArray($param, 'second');
              if(empty($second)){$second=0;}
              if(sizeof($param)>0)  {$ndate->setTime ($hour,$minute,$second);}
              return $ndate;
         }
        
        return null;
   }

   public function paramkeyToArray($filter,$paramkey,$castotimestamp) {

    $newp=array();
    $date1=$this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiudate1');
    $hour1=$this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiuhour1');
    $minute1=$this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiuminute1');
    $paramhm1=array('hour'=>$hour1,'minute'=>$minute1);
    $date1=$this->convertToDbDate($date1,$paramhm1);
    
    
    $date2=$this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiudate2');
    $hour2=$this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiuhour2');
    $minute2=$this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiuminute2');
    $paramhm2=array('hour'=>$hour2,'minute'=>$minute2);
    $date2=$this->convertToDbDate($date2,$paramhm2);
    if($castotimestamp){
        if(!empty($date1)){$date1=$date1->getTimestamp();}
        else {$date1=0;}

        if(!empty($date2)){$date2=$date2->getTimestamp();}
        else {$date2=0;}
    }
    $newp['operator']= $this->getUtildata()->getVaueOfArray($filter, $paramkey.'_badiudateoperator');
    $newp['date1']= $date1;
    $newp['hour1']= $hour1;
    $newp['minute1']= $minute1;

    $newp['date2']= $date2;
    $newp['hour2']= $hour2;
    $newp['minute2']= $minute2;
    
   
return $newp;
}
}
