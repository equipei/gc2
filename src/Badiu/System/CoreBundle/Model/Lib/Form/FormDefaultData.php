<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FormDefaultData extends BadiuModelLib {

    private $param;
    private $type = 'filter';
	private $systemdata;
    public function __construct(Container $container) {
        parent::__construct($container);
    }

    public function initParam($confparam) {
        
    }

    public function get($field) {
        $value = $this->getUtildata()->getVaueOfArray($this->param, $field);

        if ($value != 0 && empty($value)) {
            return null;
        }if (is_array($value)) {
            return $value;
        }
        $value = $this->getValue($value);
        return $value;
    }

    public function getValue($value) {
        $paramtype = $this->getParamType($value);
      
        if ($paramtype == 'session') {
            $value = $this->getBySession($value);
        } else if ($paramtype == 'service') {
            $value = $this->getByService($value);
        } else if ($paramtype == 'array') {
            $value = $this->getArray($value);
        }
        else if ($paramtype == 'dateexpression') {
            $value = $this->getDateExpression($value);
        }
        else if ($paramtype == 'querystringparam') {
            $value = $this->getQuerystringparam($value);
        }
       
        return $value;
    }

    public function setDefaultParamOnOpenForm($paramfilter) {

        if (!is_array($this->param)) {
            return $paramfilter;
        }
        foreach ($this->param as $key => $value) {
            if (!array_key_exists($key, $paramfilter)) {
                $value = $this->get($key);
                $paramfilter[$key] = $value;
            }
        }

        return $paramfilter;
    }

    public function setDefaultParam($paramfilter) {

        if (!is_array($this->param)) {
            return $paramfilter;
        }
        foreach ($this->param as $key => $value) {
            $nvalue = $this->get($key);
			if(empty($nvalue) && isset($paramfilter[$key]) && !empty($paramfilter[$key])){$nvalue=$paramfilter[$key];}
			$paramfilter[$key] = $nvalue;
        }

        return $paramfilter;
    }

    public function getBySession($value) {
        $badiuSession= $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
		$badiuSession->setSystemdata($this->getSystemdata());
		$value = $badiuSession->getParamExpression($value);
		return $value;
    }

    public function getParamType($value) {

        $pos = stripos($value, "__session_");
        if ($pos !== false) {
            return 'session';
        }

        $pos = stripos($value, ":::");
        if ($pos !== false) {
            return 'service';
        }

        $pos = stripos($value, "[");
        if ($pos !== false) {
            return 'array';
        }
        $pos = stripos($value, "__DATE_");
        if ($pos !== false) {
            return 'dateexpression';
        }

        $pos1 = stripos($value, "{");
        $pos2 = stripos($value, "}");
		//exclude css and other string like html
		$iscsshtml=false;
				
		$pos3 = stripos($value, ":");
		if ($pos3 !== false){$iscsshtml=true;}
		
		$pos3 = stripos($value, ";");
		if ($pos3 !== false){$iscsshtml=true;}
		
		$pos3 = stripos($value, " ");
		if ($pos3 !== false){$iscsshtml=true;}
		
		$pos3 = stripos($value, "<");
		if ($pos3 !== false){$iscsshtml=true;}
		
		$pos3 = stripos($value, ">");
		if ($pos3 !== false){$iscsshtml=true;}
		
		$pos3 = stripos($value, "#");
		if ($pos3 !== false){$iscsshtml=true;}
		
        if ($pos1 !== false && $pos2 !== false && !$iscsshtml) {
            return 'querystringparam';
        }
        return 'default';
    }

    private function getByService($param) {
        $type = null;
        $service = null;
        $shortname = null;
        $function = null;

        $value = null;

        $p = explode(":::", $param);
        if (isset($p[0])) {
            $type = $p[0];
        }
        if (isset($p[1])) {
            $service = $p[1];
        }
        if (isset($p[2])) {
            $shortname = $p[2];
            $function = $p[2];
        }

        //internal service key
        if ($type == 'e' || $type == 'c') {
            if ($this->getContainer()->has($service)) {
                $data = $this->getContainer()->get($service);
                if (method_exists($data, 'getIdByShortname') && !empty($shortname)) {
                    if ($type == 'c') {
                        $value = $data->getIdByShortname($this->getEntity(), $shortname);
                    } else if ($type == 'e') {
                        $value = $data->findByShortname($this->getEntity(), $shortname);
                    }
                }
            }
        } else if ($type == 'cs') {
            if ($this->getContainer()->has($service)) {
                $data = $this->getContainer()->get($service);
                if (!empty($function) && method_exists($data, $function)) {
                    $value = $data->$function();
                }
            }
        }
        return $value;
    }

    public function getArray($value) {

        if (empty($value)){ return null;}

        //check if is subparamconfig
        $possubparamv = stripos($value, "/");
        $possubparamkv = stripos($value, "|");
        if ($possubparamv!== false) {return $value;}
        if ($possubparamkv!== false) {return $value;}

        $lcharacter = str_split($value);
        $size = sizeof($lcharacter);
        if (is_array($lcharacter) && $lcharacter > 0) {
            $lastchar = $lcharacter[$size - 1];
            $fistchar = $lcharacter[0];
            if ($lastchar != ']' || $fistchar != '[') {
                return null;
            }
        }
        $field = array();
        //clean [ ]
        $value = str_replace("[", "", $value);
        $value = str_replace("]", "", $value);
        $pos = stripos($value, ",");
        if ($pos !== false) {
            $lparamx = explode(",", $value);
            foreach ($lparamx as $x) {
                $lparamy = explode(":", $x);
                $k = null;
                $v = null;
               
                if (array_key_exists(0,$lparamy)) {
                    $k = $lparamy[0];
                }
               if (array_key_exists(1,$lparamy)) {
                    $v = $lparamy[1];
                }
               
                $field[$k] = $v;
                
            }
        }else{
             $lparamy = explode(":", $value);
                $k = null;
                $v = null;
               
                if (array_key_exists(0,$lparamy)) {
                    $k = $lparamy[0];
                }
               if (array_key_exists(1,$lparamy)) {
                    $v = $lparamy[1];
                }
                $field[$k] = $v;
        }


        return $field;
    }
    public function getDateExpression($value) {
        if (empty($value)){ return null;}
        $date=null;
        if($value=='__DATE_NOW'){$date= new \DateTime();}
        $pos = stripos($value, "__DATE_ADD");
        if ($pos !== false) {
            $value=str_replace("__DATE_ADD","",$value);
            if(empty($value)){return null;}
            $value=strtolower($value);
            $value=trim($value);
            $now =new \DateTime();
            $now->modify($value);
            $date= $now;
        }
       if (!empty($date) && $this->type=='data'){$date = $date->format('Y-m-d\TH:i:s\Z');}
        return $date; 
    }
    public function getQuerystringparam($value) {
        $key=str_replace("{","",$value);
        $key=str_replace("}","",$key);
        $pv=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter($key);
		
        return $pv; 
    }
    
    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function getType() {
        return $this->type;
    }

    function setType($type) {
        $this->type = $type;
    }

	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
