<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class BadiuDateDefaultFormCast extends BadiuModelLib {

    public function __construct(Container $container) {
        parent::__construct($container);
    }
   public function convertToForm($data) {
        if (is_object($data) && is_a($data, 'DateTime')) {
			$y = $data->format('Y');
			if($y < 1 ){return null;}
             $data = $data->format('d/m/Y');
        }else if(is_numeric($data)){
            $dt= new \DateTime();
            $dt->setTimestamp($data);
            $data = $dt->format('d/m/Y');
			
			$y = $dt->format('Y');
			if($y < 1 ){return null;}
        }
        return $data;  
   }

   public function convertFromForm($fparam) { 
      
        $newparam=array();
		if(!is_array($fparam)){return $fparam;} 
         foreach ($fparam as $key => $value) {
             $pos=stripos($key, "_badiudatedefault");
             if($pos!== false){
                 $p= explode("_", $key);
                 $keyfield=null;
                 $nvalue=null;
                 if(isset($p[0])){
                       $keyfield=$p[0];
                       $nvalue=$this->convertToDbDate($value);
                       $newparam[$keyfield]= $nvalue;
                   } 
             }else{
                 $newparam[$key]= $value;
             }
             
         }
        
        return $newparam;
   }  
   
   public function convertToDbDate($date) {
        
        if(empty($date)){return null;}
        if($date=='null'){return null;}
        $date= trim($date);
       // $regexp='/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g';
        //if (preg_match($regexp, $date)) { $ndate= DateTime::createFromFormat('d/m/Y', $date);}
        $pos=stripos($date, "/");
        $p=array();
        if($pos!== false){
            $p = explode("/",$date);
        } 
        
        $d=$this->getUtildata()->getVaueOfArray($p,0);
        $m=$this->getUtildata()->getVaueOfArray($p,1);
        $y=$this->getUtildata()->getVaueOfArray($p,2);
        if(sizeof($p)==3 && ($d >=0 && $d <=31 ) && ($m >=1 && $m <=12 ) && strlen($y)==4 && checkdate($m,$d,$y)){
            $ndate= new \DateTime();
            $ndate->setDate($y, $m, $d);;
            return  $ndate;
       }
       
        
        return $date;
   }

}
