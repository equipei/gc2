<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Form;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Form\FormBuilderInterface;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyMangerInherit;

/**
 * Description of MakeSelect
 *
 * @author lino
 */
class FormFactory {

    /**
     * @var Container
     */
    private $container;

    /**
     * @var BadiuKeyManger
     */
    private $keymanger;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var FormBuilderInterface
     */
    private $builder;

    /**
     * @var BadiuKeyMangerInherit
     */
    private $kminherit;

    function __construct(Container $container, $baseKey = null) {
        $this->container = $container;
        $this->translator = $this->container->get('translator');

        $this->keymanger = $this->getContainer()->get('badiu.system.core.functionality.keymanger');
        if ($baseKey != null) {
            $this->keymanger->setBaseKey($baseKey);
        }
    }

    public function initKminherit() {
        if ($this->kminherit == null) {
            $cbundle = $this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $cbundle->initKeymangerExtend($this->getKeymanger());
            $this->kminherit = $this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
            $this->kminherit->init($this->getKeymanger(), $cbundle->getKeymangerExtend());
        }
    }

    public function add(FormField $field) {
        $this->initKminherit();
        if ($field->getType() == 'text') {
            $this->makeText($field);
        } else if ($field->getType() == 'hidden') {
            $this->makeHidden($field);
        } else if ($field->getType() == 'textarea') {
            $this->makeTextarea($field);
        } else if ($field->getType() == 'choice') {
            $this->makeChoice($field);
        } else if ($field->getType() == 'choicejs') {
            $this->makeChoice($field);
        } else if ($field->getType() == 'checkbox') {
            $this->makeCheckbox($field);
        } else if ($field->getType() == 'radio') {
            $this->makeRadio($field);
        } else if ($field->getType() == 'entity') {
            $this->makeEntity($field);
        } else if ($field->getType() == 'date') {
            $this->makeDate($field); 
        } else if ($field->getType() == 'password') {
            $this->makePassword($field);
        } else if ($field->getType() == 'email') {
            $this->makeEmail($field);
        } else if ($field->getType() == 'badiuchoice2text') {
            $this->makeBadiuChoice2Text($field);
        } 
        else if ($field->getType() == 'badiuchoicetext') {
            $this->makeBadiuChoiceText($field);
        } 
        else if ($field->getType() == 'badiufilterdate') {
            $this->makeBadiuFilterDate($field);
        } else if ($field->getType() == 'badiufilternumber') {
            $this->makeBadiuFilterNumber($field);
        } else if ($field->getType() == 'badiuhour') {
            $this->makeBadiuHour($field);
        }else if ($field->getType() == 'badiutimehour') {
            $this->makeBadiuTimeHour($field);
        } 
        else if ($field->getType() == 'badiutimeperiod') {
            $this->makeBadiuTimePeriod($field);
        } 
        else if ($field->getType() == 'badiuusertype') {
            $this->makeBadiuUserType($field);
        } 
         
        else if ($field->getType() == 'badiuhtml') {
            $this->makeBadiuHtml($field);
        }
    }

    public function makeText(FormField $field) {
        $this->getBuilder()->add($field->getName(), 'text', array('label' => $this->getTranslator()->trans($field->getLabel()), 'required' => $field->getRequired(), 'attr' => array('class' => $field->getCssClasss())));
    }

    public function makeHidden(FormField $field) {
        $this->getBuilder()->add($field->getName(), 'hidden', array('required' => $field->getRequired()));
    }

    public function makePassword(FormField $field) {
        $this->getBuilder()->add($field->getName(), 'password', array('label' => $this->getTranslator()->trans($field->getLabel()), 'required' => $field->getRequired(), 'attr' => array('class' => $field->getCssClasss())));
    }

    public function makeEmail(FormField $field) {
        $this->getBuilder()->add($field->getName(), 'email', array('label' => $this->getTranslator()->trans($field->getLabel()), 'required' => $field->getRequired(), 'attr' => array('class' => $field->getCssClasss())));
    }

    public function makeDate(FormField $field) {
        $this->getBuilder()->add($field->getName(), 'date', array('widget' => 'single_text', 'format' => $field->getFormat(), 'label' => $this->getTranslator()->trans($field->getLabel()), 'required' => $field->getRequired(), 'attr' => array('class' => $field->getCssClasss())));
    }

    public function makeTextarea(FormField $field) {
        $this->getBuilder()->add($field->getName(), 'textarea', array('label' => $this->getTranslator()->trans($field->getLabel()), 'required' => $field->getRequired(), 'attr' => array('class' => $field->getCssClasss())));
    }

    public function makeChoice(FormField $field) {

        $list = array();

        $pchoiceconfig = $field->getChoicelist();
        if (!empty($pchoiceconfig)) {
            $list = $this->getDefaultChoice($field);
        } else {
            if ($this->getContainer()->has($this->kminherit->formDataOptions())) {
                $fdataoptions = $this->getContainer()->get($this->kminherit->formDataOptions());
                if (method_exists($fdataoptions, $field->getName())) {
                    $list = $fdataoptions->get($field->getName());
                }
            }
        }
        $this->getBuilder()->add($field->getName(), 'choice', array('label' => $this->getTranslator()->trans($field->getLabel()), 'required' => $field->getRequired(), 'choices' => $list, 'empty_value' => '---', 'attr' => array('class' => $field->getCssClasss())));
    }

    /* old deve ser removido 'defaultboolean' foi movido para getDefaultChoice
      public function makeChoice(FormField $field) {

      $list=array();
      if($field->getChoicelist()=='defaultboolean'){
      $sformutil=$this->getContainer()->get('badiu.system.core.lib.form.util');
      $list=$sformutil->getBooleanOptions();
      }else{
      $pchoiceconfig=$field->getChoicelist();
      if(!empty($pchoiceconfig)) {$list=$this->getDefaultChoice($field);}
      else{
      $fdataoptions=$this->getContainer()->get($this->kminherit->formDataOptions());
      if (method_exists($fdataoptions,$field->getName())){$list=$fdataoptions->get($field->getName());}
      }

      }
      $this->getBuilder()->add($field->getName(), 'choice',array('label'=>$this->getTranslator()->trans($field->getLabel()),'required'  => $field->getRequired(), 'choices'=>$list,'empty_value' => '---', 'attr' => array('class' => $field->getCssClasss())));

      } */

    public function makeCheckbox(FormField $field) {

        $list = array();

        if ($field->getChoicelist() == 'defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
        } else {
            $pchoiceconfig = $field->getChoicelist();
            if (!empty($pchoiceconfig)) {
                $list = $this->getDefaultChoice($field);
            } else {
                if ($this->getContainer()->has($this->kminherit->formDataOptions())) {
                    $fdataoptions = $this->getContainer()->get($this->kminherit->formDataOptions());
                    if (method_exists($fdataoptions, $field->getName())) {
                        $list = $fdataoptions->get($field->getName());
                    }
                }
            }
        }


        $this->getBuilder()->add($field->getName(), 'choice', array('label' => $this->getTranslator()->trans($field->getLabel()), 'required' => $field->getRequired(), 'choices' => $list, 'multiple' => true, 'expanded' => false, 'attr' => array('class' => $field->getCssClasss())));
    }

    //review dont work
    public function makeRadio(FormField $field) {

        $list = array();

        if ($field->getChoicelist() == 'defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
        } else {
            $pchoiceconfig = $field->getChoicelist();
            if (!empty($pchoiceconfig)) {
                $list = $this->getDefaultChoice($field);
            } else {
                if ($this->getContainer()->has($this->kminherit->formDataOptions())) {
                    $fdataoptions = $this->getContainer()->get($this->kminherit->formDataOptions());
                    if (method_exists($fdataoptions, $field->getName())) {
                        $list = $fdataoptions->get($field->getName());
                    }
                }
            }
        }


        $this->getBuilder()->add($field->getName(), 'choice', array('label' => $this->getTranslator()->trans($field->getLabel()), 'required' => $field->getRequired(), 'choices' => $list, 'multiple' => false, 'expanded' => false, 'attr' => array('class' => $field->getCssClasss())));
    }

    public function makeEntity(FormField $field) {
        $list = array();
        $p = explode("|", $field->getChoicelist());
        $pentity = null;
        $pclass = null;
        $pproperty = null;
        $function = null;
        if (isset($p[0])) {
            $pentity = $p[0];
        }
        if (isset($p[1])) {
            $pproperty = $p[1];
        }
        if (isset($p[2])) {
            $pclass = $p[2];
        }
        if (isset($p[3])) {
            $function = $p[3];
        }

        if (!empty($pclass)) {
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $data = $this->getContainer()->get($pclass);
            if (empty($function)) {
                $list = $data->findByEntity($badiuSession->get()->getEntity());
            } else {
                $list = $data->$function();
            }
        }
        $this->getBuilder()->add($field->getName(), 'entity', array('class' => $pentity, 'property' => $pproperty, 'required' => $field->getRequired(), 'choices' => $list, 'empty_value' => '---', 'label' => $this->getTranslator()->trans($field->getLabel()), 'attr' => array('class' => $field->getCssClasss())));
    }

    public function getDefaultChoice(FormField $field) {
        $list = array();

        //deprecated defaultboolean without type
        if ($field->getChoicelist() == 'defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
            return $list;
        }
       
        $p = explode("|", $field->getChoicelist());
        $type = 's';
        $dataservice = null;
        $function = null;
        $converlist = 'y';
        if (isset($p[0])) {
            $type = $p[0];
        }
        if (isset($p[1])) {
            $dataservice = $p[1];
        }
        if (isset($p[2])) {
            $function = $p[2];
        }
        if (isset($p[3])) {
            $converlist = $p[3];
        }

        //if type not defined
        if ($type != 's') {
            $dataservice = $type;
        }
        //implementation default of type s 
        if (!$this->getContainer()->has($dataservice)){return $list;}
        $data = $this->getContainer()->get($dataservice);
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $list = null;
        if (empty($function) || $function == 'getFormChoice') {
            $list = $data->getFormChoice($badiuSession->get()->getEntity());
        } else {
            $list = $data->$function();
        }

        if ($converlist == 'y') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->convertBdArrayToListOptions($list);
        }


        return $list;
    }

    public function makeBadiuChoice2Text(FormField $field) {

        $list = array();
        if ($field->getChoicelist() == 'defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
        } else {
            $pchoiceconfig = $field->getChoicelist();
            if (!empty($pchoiceconfig)) {
                $list = $this->getDefaultChoice($field);
            } else {
                if ($this->getContainer()->has($this->kminherit->formDataOptions())) {
                    $fdataoptions = $this->getContainer()->get($this->kminherit->formDataOptions());
                    if (method_exists($fdataoptions, $field->getName())) {
                        $list = $fdataoptions->get($field->getName());
                    }
                }
            }
        }

        // $this->getBuilder()->add($field->getName(), 'badiuchoice2text',array('label'=>$this->getTranslator()->trans($field->getLabel()),'required'  => $field->getRequired(), 'choices'=>$list,'empty_value' => '---', 'attr' => array('class' => $field->getCssClasss())));

        $this->getBuilder()->add($field->getName(), 'badiuchoice2text', array(
            'label' => $this->getTranslator()->trans($field->getLabel()),
            'required' => $field->getRequired(),
            //pode passar valores aqui de escolha dinamicamente
            'choices' => $list,
            //opcoes do label, caso nao for definido existe por default
            'labelfield1' => 'Operadora',
            'labelfield2' => 'DD',
            'labelfield3' => 'N° de telefone'
        ));
    }

    public function makeBadiuChoiceText(FormField $field) {
          
        $list = null;


        if ($field->getChoicelist() == 'defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
        } else {
            $pchoiceconfig = $field->getChoicelist();
            if (!empty($pchoiceconfig)) {
                $list = $this->getDefaultChoice($field);
            } else {
                if ($this->getContainer()->has($this->kminherit->formDataOptions())) {
                    $fdataoptions = $this->getContainer()->get($this->kminherit->formDataOptions());
                    if (method_exists($fdataoptions, $field->getName())) {
                        $list = $fdataoptions->get($field->getName());
                    }
                }
            }
        }

      
        $this->getBuilder()->add($field->getName(), 'badiuchoicetext', array(
            'label' => $this->getTranslator()->trans($field->getLabel()),
            'required' => $field->getRequired(),
            //pode passar valores aqui de escolha dinamicamente
             'choices' => $list,
             'attr' => array('class' => $field->getCssClasss()),
            'field1' => '',
            'field2' => ''
        ));
        
    }
    public function makeBadiuHour(FormField $field) {

        $list = null;


        if ($field->getChoicelist() == 'defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
        } else {
            $pchoiceconfig = $field->getChoicelist();
            if (!empty($pchoiceconfig)) {
                $list = $this->getDefaultChoice($field);
            } else {
                if ($this->getContainer()->has($this->kminherit->formDataOptions())) {
                    $fdataoptions = $this->getContainer()->get($this->kminherit->formDataOptions());
                    if (method_exists($fdataoptions, $field->getName())) {
                        $list = $fdataoptions->get($field->getName());
                    }
                }
            }
        }

        //if has no config get default list
        if (empty($list)) {
            //default data
            $dateformoption = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
            $list = $dateformoption->getMinutes();
        }
        // $this->getBuilder()->add($field->getName(), 'badiuchoice2text',array('label'=>$this->getTranslator()->trans($field->getLabel()),'required'  => $field->getRequired(), 'choices'=>$list,'empty_value' => '---', 'attr' => array('class' => $field->getCssClasss())));

        $this->getBuilder()->add($field->getName(), 'badiuhour', array(
            'label' => $this->getTranslator()->trans($field->getLabel()),
            'required' => $field->getRequired(),
            //pode passar valores aqui de escolha dinamicamente
            'choices' => $list,
            'badiuhour' => 'Hora',
            'badiuminute' => 'Minuto'
        ));
    }
 public function makeBadiuTimeHour(FormField $field) {

        $list = null;


        if ($field->getChoicelist() == 'defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
        } else {
            $pchoiceconfig = $field->getChoicelist();
            if (!empty($pchoiceconfig)) {
                $list = $this->getDefaultChoice($field);
            } else {
                if ($this->getContainer()->has($this->kminherit->formDataOptions())) {
                    $fdataoptions = $this->getContainer()->get($this->kminherit->formDataOptions());
                    if (method_exists($fdataoptions, $field->getName())) {
                        $list = $fdataoptions->get($field->getName());
                    }
                }
            }
        }

        //if has no config get default list
        if (empty($list)) {
            //default data
            $dateformoption = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
            $list = $dateformoption->getMinutes();
        }
        // $this->getBuilder()->add($field->getName(), 'badiuchoice2text',array('label'=>$this->getTranslator()->trans($field->getLabel()),'required'  => $field->getRequired(), 'choices'=>$list,'empty_value' => '---', 'attr' => array('class' => $field->getCssClasss())));

        $this->getBuilder()->add($field->getName(), 'badiutimehour', array(
            'label' => $this->getTranslator()->trans($field->getLabel()),
            'required' => $field->getRequired(),
            //pode passar valores aqui de escolha dinamicamente
            'choices' => array('hours'=>$list,'minutes'=>$list),
            //'choicesminute' => $list,
            'badiutimehour' => 'Hora',
            'badiutimeminute' => 'Minuto'
        ));
    }
    
     public function makeBadiuTimePeriod(FormField $field) {

        $list = null;


        if ($field->getChoicelist() == 'defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
        } else {
            $pchoiceconfig = $field->getChoicelist();
            if (!empty($pchoiceconfig)) {
                $list = $this->getDefaultChoice($field);
            } else {
                if ($this->getContainer()->has($this->kminherit->formDataOptions())) {
                    $fdataoptions = $this->getContainer()->get($this->kminherit->formDataOptions());
                    if (method_exists($fdataoptions, $field->getName())) {
                        $list = $fdataoptions->get($field->getName());
                    }
                }
            }
        }

        //if has no config get default list
        if (empty($list)) {
            //default data
            $dateformoption = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
            $list = $dateformoption->getTimeUnit();
        }
        // $this->getBuilder()->add($field->getName(), 'badiuchoice2text',array('label'=>$this->getTranslator()->trans($field->getLabel()),'required'  => $field->getRequired(), 'choices'=>$list,'empty_value' => '---', 'attr' => array('class' => $field->getCssClasss())));

        $this->getBuilder()->add($field->getName(), 'badiutimeperiod', array(
            'label' => $this->getTranslator()->trans($field->getLabel()),
            'required' => $field->getRequired(),
            //pode passar valores aqui de escolha dinamicamente
            'choices' => $list,
            'number' => "Digite o valor",
            'unittime' => "Selecione unidade de tempo"
        ));
    }
    
    public function makeBadiuUserType(FormField $field) {

        $list = null;


        if ($field->getChoicelist() == 'defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
        } else {
            $pchoiceconfig = $field->getChoicelist();
            if (!empty($pchoiceconfig)) {
                $list = $this->getDefaultChoice($field);
            } else {
                if ($this->getContainer()->has($this->kminherit->formDataOptions())) {
                    $fdataoptions = $this->getContainer()->get($this->kminherit->formDataOptions());
                    if (method_exists($fdataoptions, $field->getName())) {
                        $list = $fdataoptions->get($field->getName());
                    }
                }
            }
        }

        //if has no config get default list
        if (empty($list)) {
            //default data
            $dateformoption = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
            $list = $dateformoption->getMinutes();
        }
        // $this->getBuilder()->add($field->getName(), 'badiuchoice2text',array('label'=>$this->getTranslator()->trans($field->getLabel()),'required'  => $field->getRequired(), 'choices'=>$list,'empty_value' => '---', 'attr' => array('class' => $field->getCssClasss())));

        $this->getBuilder()->add($field->getName(), 'badiuusertype', array(
            'label' => $this->getTranslator()->trans($field->getLabel()),
            'required' => $field->getRequired(),
            //pode passar valores aqui de escolha dinamicamente
             'choices' => $list,
            'type' => 'Type',
            'name' => 'Nome'
        ));
    }
    public function makeBadiuHtml(FormField $field) {

        $list = null;



        // $this->getBuilder()->add($field->getName(), 'badiuchoice2text',array('label'=>$this->getTranslator()->trans($field->getLabel()),'required'  => $field->getRequired(), 'choices'=>$list,'empty_value' => '---', 'attr' => array('class' => $field->getCssClasss())));

        $this->getBuilder()->add($field->getName(), 'badiuhtml', array(
            'label' => $this->getTranslator()->trans($field->getLabel()),
            'required' => $field->getRequired(),
            //pode passar valores aqui de escolha dinamicamente
            'choices' => $list,
            'badiuhtml' => 'xxxxx',
        ));
    }

    public function makeBadiuFilterDate(FormField $field) {


        $list = null;
        if ($field->getChoicelist() == 'defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
        } else {
            $pchoiceconfig = $field->getChoicelist();
            if (!empty($pchoiceconfig)) {
                $list = $this->getDefaultChoice($field);
            } else {
                if ($this->getContainer()->has($this->kminherit->formDataOptions())) {
                    $fdataoptions = $this->getContainer()->get($this->kminherit->formDataOptions());
                    if (method_exists($fdataoptions, $field->getName())) {
                        $list = $fdataoptions->get($field->getName());
                    }
                }
            }
        }

        //if has no config get default list
        if (empty($list)) {
            //default data
            $dateformoption = $this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
            $list = $dateformoption->getOperatorText();
        }
        $this->getBuilder()->add($field->getName(), 'badiufilterdate', array(
            'label' => $this->getTranslator()->trans($field->getLabel()),
            'choices' => $list,
            //opcoes do label, caso nao for definido existe por default
            'labelbadiudateoperator' => '--',
            'labelbadiudate1' => '',
            'labelbadiudate2' => '',
            'attr' => array('class' => $field->getCssClasss())
        ));
    }

    public function makeBadiuFilterNumber(FormField $field) {


        $list = null;
        if ($field->getChoicelist() == 'defaultboolean') {
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->getBooleanOptions();
        } else {
            $pchoiceconfig = $field->getChoicelist();
            if (!empty($pchoiceconfig)) {
                $list = $this->getDefaultChoice($field);
            } else {
                if ($this->getContainer()->has($this->kminherit->formDataOptions())) {
                    $fdataoptions = $this->getContainer()->get($this->kminherit->formDataOptions());
                    if (method_exists($fdataoptions, $field->getName())) {
                        $list = $fdataoptions->get($field->getName());
                    }
                }
            }
        }

        //if has no config get default list
        if (empty($list)) {
            //default data
            $dateformoption = $this->getContainer()->get('badiu.system.core.lib.math.form.dataoptions');
            $list = $dateformoption->getOperatorText();
        }
        $this->getBuilder()->add($field->getName(), 'badiufilternumber', array(
            'label' => $this->getTranslator()->trans($field->getLabel()),
            'choices' => $list,
            //opcoes do label, caso nao for definido existe por default
            'labeloperator' => '--',
            'labelnumber1' => '',
            'labelnumber2' => '',
            'attr' => array('class' => $field->getCssClasss())
        ));
    }

    public function getEntityChoice(FormField $field) {
        $list = array();
        $p = explode("|", $field->getChoicelist());
        $pclass = null;
        $function = 'getFormChoice';

        if (isset($p[2])) {
            $pclass = $p[2];
        }
        if (isset($p[3])) {
            $function = $p[3];
        }

        if (!empty($pclass)) {
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $data = $this->getContainer()->get($pclass);
            if ($function == 'getFormChoice') {
                $list = $data->getFormChoice($badiuSession->get()->getEntity());
            } else {
                $list = $data->$function();
            }
            if ($list == null) {
                return null;
            }
            $sformutil = $this->getContainer()->get('badiu.system.core.lib.form.util');
            $list = $sformutil->convertBdArrayToListOptions($list);
        }
        return $list;
    }

    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }

    public function getTranslator() {
        return $this->translator;
    }

    public function setTranslator(Translator $translator) {
        $this->translator = $translator;
    }

    public function getBuilder() {
        return $this->builder;
    }

    public function setBuilder(FormBuilderInterface $builder) {
        $this->builder = $builder;
    }

    public function getKeymanger() {
        return $this->keymanger;
    }

    public function setKeymanger(BadiuKeyManger $keymanger) {
        $this->keymanger = $keymanger;
    }

}
