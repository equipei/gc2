<?php

namespace Badiu\System\CoreBundle\Model\Lib\Form;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
/**

 *
 * @author lino
 */
class FormFilterInfo extends BadiuModelLib {
    private $key;
    private $layout;
    private $param;
    public function __construct(Container $container) {
        parent::__construct($container);
    }

    public function exec() {
        //$reportfilter='{"config":{"router":"badiu.moodle.mreport.enrol.enrol.index"},"filter":{"user":"lino","timestartp_badiutimeperiodoperator":"pastfrom","timestartp_badiutimeperiodunittime":"system_time_unit_month","timestartp_badiutimeperiodnumber":"12","course":"","enrolrole":"","_addcolumndynamicallymdluserprofile":"0","g2coursestatus":"","timeendp_badiutimeperiodoperator":"futurefrom","timeendp_badiutimeperiodunittime":"system_time_unit_day","timeendp_badiutimeperiodnumber":"","timestart_badiudateoperator":"","timestart_badiudate1":"","timestart_badiudate2":"","timeend_badiudateoperator":"","timeend_badiudate1":"","timeend_badiudate2":"","enrolwithcourseaccess":"","enroleaccessperiod_badiutimeperiodoperator":"","enroleaccessperiod_badiutimeperiodunittime":"system_time_unit_day","enroleaccessperiod_badiutimeperiodnumber":"","enrolmethod":"","enrolmethodstatus":"0","enrolstatus":"0","enroltimevalidate":"1","coursevisible":"1","coursetimestartp_badiutimeperiodoperator":"pastfrom","coursetimestartp_badiutimeperiodunittime":"system_time_unit_day","coursetimestartp_badiutimeperiodnumber":"","coursestartdate_badiudateoperator":"","coursestartdate_badiudate1":"","coursestartdate_badiudate2":"","courseidnumber":"","coursecatname":"","_serviceid":"38","_datasource":"servicesql","__groupname":"","__cohortid":"","city":"","country":"","useridnumber":"","userconfirmed":"1","userdeleted":"0","usersuspended":"0","parentid":"38"},"reportcolumnemail":"email"}';
        //$reportfilter=$this->getJson()->decode($reportfilter,true);
        $reportfilter= $this->getParam();
        
        $this->key=$this->getUtildata()->getVaueOfArray($reportfilter,'config.router',true);
        $reportfilter=$this->getUtildata()->getVaueOfArray($reportfilter,'filter');

        /*echo "<hr><pre>";
        print_r($reportfilter);
        echo "</pre>";*/
        
        $newfilter=$this->getContainer()->get('badiu.system.core.lib.form.casttimeperiod')->changeParamFromformCasToArray($reportfilter);
        $newfilter=$this->getContainer()->get('badiu.system.core.lib.form.castfilterdate')->changeParamFromformCasToArray($newfilter);
        
        $newfilter=$this->castConfigToLabel($newfilter);
       
        
        //$this->key='badiu.moodle.mreport.enrol.enrol.index';
        //get type key
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
           
        if($sysoperation->isView($this->key)){$this->layout='viewDetail';}
        else if($sysoperation->isIndex($this->key)){$this->layout='indexCrud';}
        else if($sysoperation->isDashboard($this->key)){$this->layout='dashboard';}
        else if($sysoperation->isFrontpage($this->key)){$this->layout='frontpage';}
        
        $bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($this->key);
        $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
        $keymanger->setBaseKey($bkey);
        $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
        $cbundle->setSessionhashkey($this->getSessionhashkey());
        $cbundle->setKeymanger($keymanger);
        $cbundle->initKeymangerExtend($keymanger);
        
        $this->kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
        $this->kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
       
        //get type
        $typeconfig=$cbundle->getValueQueryString($keymanger->formFilterFieldsType(),$cbundle->getKeymangerExtend()->formFilterFieldsType());
       //get choicelist
       $choicelist=$cbundle->getValueQueryString($keymanger->formFilterFieldsChoicelist(),$cbundle->getKeymangerExtend()->formFilterFieldsChoicelist());
        
        //get label
        $labelconfig=$cbundle->getValueQueryString($keymanger->formFilterFieldsLabel(),$cbundle->getKeymangerExtend()->formFilterFieldsLabel());
      
        $newfilter=$this->addLabel($newfilter,$labelconfig,$typeconfig,$choicelist);
       /* echo "<hr><pre>";
        print_r($newfilter);
        echo "</pre>";
        exit;*/
        
        return $newfilter;
        
      
        //set value filter

        //format value for info
        //getValueInherit
    }

    public function castConfigToLabel($lparam) {
        $newlist=array();
        foreach ($lparam as $key => $value) {
            $value=$this->getTimeperiod($value);
            $value=$this->getFilterdate($value);
            $newlist[$key]= $value;
            }
        return $newlist;
        }
    
    public function getTimeperiod($param) {
            if(!is_array($param)){return $param;}
            $exitnumber=array_key_exists('number', $param);
            $exitunittime=array_key_exists('unittime', $param);
            $exitoperator=array_key_exists('number', $param);
            if(!$exitnumber && !$exitunittime && !$exitoperator){return $param;}

            $number=$this->getUtildata()->getVaueOfArray($param, 'number');
            $unittime=$this->getUtildata()->getVaueOfArray($param, 'unittime');
            $operator=$this->getUtildata()->getVaueOfArray($param, 'operator');
        
            $txt="";
            if(!empty($number) && !empty($unittime) && !empty($operator)){
                $pdataoptions=$this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
                $labelunittime=$pdataoptions->getTtimeunitLabel($unittime,true,true);
                $labeloperator=$pdataoptions->getOperatorPeriodLabel($operator);
                $txt="$labeloperator $number $labelunittime";
            } 
        
        return $txt;
    }

    public function getFilterdate($param) {
        if(!is_array($param)){return $param;}
        $exitoperator=array_key_exists('operator', $param);
        $exitdate1=array_key_exists('date1', $param);
        
        if(!$exitoperator && !$exitdate1){return $param;}

        $operator=$this->getUtildata()->getVaueOfArray($param, 'operator');
        $date1=$this->getUtildata()->getVaueOfArray($param, 'date1');
        $date2=$this->getUtildata()->getVaueOfArray($param, 'date2');
    
        $txt="";
        if(!empty($operator) && !empty($date1)){
            $pdataoptions=$this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
            $configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
            $configformat->setSessionhashkey($this->getSessionhashkey());
            $firstdate= $configformat->defaultDate($date1);
            $seconddate="";
            if(!empty($date2)){$seconddate= $configformat->defaultDate($date2);}
             
            $labelunittime=$pdataoptions->getOperatorTextLabel($operator);
            $labeloperator=$pdataoptions->getOperatorPeriodLabel($operator);
            //
            //
            if($operator=='between'){$txt=$this->getTranslator()->trans('badiu.system.time.twodate',array('%text%'=>$labeloperator,'%timestart%'=>$firstdate,'%timeend%'=>$seconddate));}
            else {$txt="$labeloperator $firstdate";}
            
        } 
    
    return $txt;
}

public function addLabel($lparam,$labelconfig,$typeconfig,$choicelist) {
    $newlist=array();
    foreach ($lparam as $key => $value) {
        $lkey=$this->getUtildata()->getVaueOfArray($labelconfig, $key);
         $type=$this->getUtildata()->getVaueOfArray($typeconfig, $key);
         $choice=$this->getUtildata()->getVaueOfArray($choicelist, $key);
      //   echo $key;echo "<hr/>";
      //   print_r( $value);
        if($value!="" || $value!=null){
            $text=$value;
            if($type=='choice'){$text=$this->getLabelOfChoiceValueSelected($choice,$value);}
            if(empty($lkey)){ $label=$this->getTranslator()->trans($key);}
            else{$label=$this->getTranslator()->trans($lkey);}
            // $text=$value ."::  $type | $choice ";
            $newlist[$key]=array('label'=>$label,'text'=>$text);
        }
        
    }
 //   exit;
    return $newlist;
}

public function getLabelOfChoiceValueSelected($choice,$value) {
        $result=$value;
      
        if($choice=='defaultboolean' || $choice=='d|defaultboolean'){
            $result= $this->getContainer()->get('badiu.system.core.lib.form.util')->getBooleanOptionsLabel($value);
        }else{
           
            $service=null;
            $function=null;
            $p=explode("|",$choice);
          
            if(sizeof($p)>=2){
               
                $stype=$this->getUtildata()->getVaueOfArray($p,0);
                $service=$this->getUtildata()->getVaueOfArray($p,1);
                $function=$this->getUtildata()->getVaueOfArray($p,2);
                $type=$this->getUtildata()->getVaueOfArray($p,3);
               
                if(!empty($service) && !empty($function) && $this->getContainer()->has($service)){
                  
                    if($type=='n'){
                       
                        $function=$function."Label";
                        $service=$this->getContainer()->get($service);
                        
                        $serviceid=$this->getUtildata()->getVaueOfArray($this->param,'filter._serviceid',true);
                        if(!empty($serviceid) && method_exists($service, 'setServiceid')){$service->setServiceid($serviceid);}
                        if(method_exists($service, $function)){
                            $result=$service->$function($value);
                        }
                    }
                    
                }
            }
        }
        return $result;
}

function setParam($param) {
    $this->param = $param;
}
function getParam() {
    return $this->param;
}
}
