<?php

namespace Badiu\System\CoreBundle\Model\Lib\Keytree;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class KeyTreeDefaultSqlFilter extends BadiuSqlFilter{
    /**
     * @var object
     */
   private  $sqlservice;
    function __construct(Container $container) {
            parent::__construct($container);
            $this->sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
       }

        function getParentWithChildren() {
           
           $param= $this->getUtildata()->getVaueOfArray($this->getParam(),'idpath');
           $sql=" ";
           if(!empty($param)){$sql=" AND ( o.idpath =' ".$param."' OR  o.idpath LIKE '".$param.".%' )";}
            return $sql;
        }
        function getParentWithChildrenFK() {
           
           $param=  $this->getUtildata()->getVaueOfArray($this->getParam(),'categoryid');
           $sql=" ";
           if(!empty($param)){$sql=" AND ( ct.idpath =' ".$param."' OR  ct.idpath LIKE '".$param.".%' )";}
            return $sql;
        }
         
  
        function getSqlservice() {
            return $this->sqlservice;
        }

        function setSqlservice($sqlservice) {
            $this->sqlservice = $sqlservice;
        }


}
