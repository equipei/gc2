<?php

namespace Badiu\System\CoreBundle\Model\Lib\Keytree;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FactoryKeyTreeChildren extends BadiuModelLib{
    
    private $list=array();
    private $currentid;
    private $enablesession=true;
    private $servicekey;
    private $servicedata;
    function __construct(Container $container) {
            parent::__construct($container);
              }


    function teste() {
      $this->init(15,"badiu.admin.cms.resourcestructure.data",false);
      print_r( $this->path);
    } 
    function init($currentid,$servicekey,$enablesession=true) {
              $this->currentid=$currentid; 
              $this->servicekey=$servicekey;
              $this->enablesession=$enablesession;
              $path=$this->makePath();
              return $path;
    }

    function initServicedata() {
      if(!$this->getContainer()->has($this->servicekey)){
        $this->path=array();
        return null;
       }
       $this->servicedata=$this->getContainer()->get($this->servicekey);
       return $this->servicedata;
    }
    function makePath() {
        $sd=$this->initServicedata();
       if(empty($sd)){
        $this->path=array();
        return null;
       }
       if(empty($this->currentid)){return null;}

       //get parent of currentkey
       $listidpath= $this->servicedata->getGlobalColumnValue('path',array('id'=>$this->currentid));

       //cast parent strign (ex: /68/70/) as array
       $listparent=$this->getParentidByPath($listidpath);
      if(!is_array($listparent)){$listparent=array();}
       array_push($listparent,$this->currentid);
       $listpath=array();
      foreach ($listparent as $row) {
          $tinfo=$this->getInfoById($row);
          $name=$this->getUtildata()->getVaueOfArray($tinfo,'name');
          $idpath=$this->getUtildata()->getVaueOfArray($tinfo,'idpath');
          $infop=array('id'=>$row,'name'=>$name,'idpath'=>$idpath);
          $listpath[$row]= $infop;
      }
      $this->path=$listpath;
     return $this->path;
      
  }

  function getParentidByPath($path) {
      if(empty($path)){return null;}
      $sp=explode("/",$path);
      if(!is_array($sp)){return null;}
      $listparent=array();
      foreach ($sp as $row) {
          if(!empty($row)){array_push($listparent,$row);}
      }
    return $listparent;
}

    function getInfoById($id) {
     
       if($this->enablesession){
          $badiuSession= $this->getContainer()->get('badiu.system.access.session');
          $badiuSession->setHashkey($this->getSessionhashkey());
	
          $keytreesession=$this->servicekey.'.facotykeytreepath.'.$id;
          $value=$badiuSession->getValue($keytreesession);
          if(!empty($value)){return $value;}
        }
        if(empty($this->servicedata)){$this->initServicedata();}
        if(empty($this->servicedata)){return null;}
        $tinfo= $this->servicedata->getGlobalColumnsValue('o.name,o.idpath',array('id'=>$id));
       if($this->enablesession){
          $badiuSession= $this->getContainer()->get('badiu.system.access.session');
          $badiuSession->setHashkey($this->getSessionhashkey());
	
          $keytreesession=$this->servicekey.'.facotykeytreepath.'.$id;
          $badiuSession->addValue($keytreesession,$tinfo);
        }
        return $tinfo;
    }

    function getList() {
          return $this->list;
    }
 
    function setList($list) {
        $this->list = $list;
    }
  

    function getCurrentid() {
      return $this->currentid;
   }

  function setCurrentid($currentid) {
      $this->currentid = $currentid;
  }

  function getEnablesession() {
    return $this->enablesession;
}

function setEnablesession($enablesession) {
    $this->enablesession = $enablesession;
}

function getservicekey() {
  return $this->servicekey;
}

function setservicekey($servicekey) {
  $this->servicekey = $servicekey;
}
}
