<?php

namespace Badiu\System\CoreBundle\Model\Lib\Keytree;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FactoryKeyTree  extends BadiuModelLib {
    
  
     /**
     * @var Container
     */
    private $container;

     /**
     * @var Object
     */
    private $moduledata;
   
	
      function __construct(Container $container) {
        parent::__construct($container);
                $this->moduledata=null;
               
      }
     public function init($moduledatakey){
         $this->setModuledata($this->getContainer()->get($moduledatakey));
      }
      public function getNext($entity,$key,$dtype=null){
         $sql=null;
         $level=1;
        
         $mewkey=null;
        
        //has key
        if(!empty($key)){
             $level=$this->getLevel($key); 
             $level++;
             $haskey=$this->hasKey($entity,$key,$level,$dtype);
            
             //get first next
             if($haskey){
                 $lastkey=$this->getLastKey($entity,$key,$level,$dtype);
                 
                 $mewkey=$this->getNextSequence($lastkey);
             }
             //generate first sequence
             else{
                 $mewkey=$key.'.1';
             }
         //without key  
        }else{
            $lastkey=$this->getLastKeyWitoutLevel($entity,$dtype);
            if(empty($lastkey)) { $mewkey=1;}
            else{
                 $mewkey=$lastkey+1;
            }
        }
     
        return $mewkey;
    }
    
     public function getLevel($key) {
		$level=0;
		if(!empty($key)){
                   $level=substr_count($key,".");
		}
                
                return $level;

	}
  public function getOrder($key) {
               $base=100;
               $value=null;
               $skey=null;
               $vkey=array();
               $pkey=array();
               $unit=0;
               $scale=0;
               $perct=0;
               $vperct=0;
               $level=$this->getLevel($key);
               
               if($level==0){
                    $value=$key*100;
                }else{
                    $skey = explode(".", $key);
                    $cont=0;
                    foreach ($skey as $v) {
                        
                         if($cont==0){
                             $vaux=$v+1;
                             $vkey[$cont]=$v*$base;
                             $pkey[$cont]=$vaux*$base;
                         }else{
                             $unit=$v+1;
                             $scale=$pkey[$cont-1]-$vkey[$cont-1];
                             $perct=$scale/$unit;
                             $vperct=$scale-$perct;
                             
                             //calculate key
                             $vkey[$cont]=$vkey[$cont-1]+$vperct;
                             $value=$vkey[$cont];
                            
                             //calculate next key
                            $unit=$unit+1;
                            $perct=$scale/$unit;
                            $vperct=$scale-$perct;
                            $pkey[$cont]=$vkey[$cont-1]+$vperct;
                         }
                        $cont++;
                    }
                }
                
                return $value;
	}
        
      
 public function hasKey($entity,$key,$level,$dtype=null){
             $wdtype =" ";
            if(!empty($dtype)){$wdtype =" AND  o.dtype=:dtype ";}
            $filterkey="$key.%";
            
            $filterkeywhre="";
           if(!empty($key)){ $filterkeywhre=" AND o.idpath LIKE :idpath ";}
            $r=FALSE;
            $sql="SELECT COUNT(o.id) AS countrecord FROM ".$this->getModuledata()->getBundleEntity()." o WHERE o.entity=:entity $wdtype AND o.level=:level $filterkeywhre";
            $query = $this->getModuledata()->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
            if(!empty($dtype)){$query->setParameter('dtype',$dtype);}
            $query->setParameter('level',$level);
           if(!empty($key)){ $query->setParameter('idpath',$filterkey);}
             
            $result= $query->getSingleResult();
           if($result['countrecord']>0){$r=TRUE;}
            return $r;
	 } 
         
  public function getLastKey($entity,$key,$level,$dtype=null){
             $wdtype =" ";
            if(!empty($dtype)){$wdtype =" AND o.dtype=:dtype ";}
            $filterkey="$key.%";
            $sql="SELECT o.idpath FROM ".$this->getModuledata()->getBundleEntity()." o WHERE  o.entity=:entity $wdtype AND o.level=:level  AND o.idpath LIKE :idpath ORDER BY  o.orderidpath DESC";
            $query = $this->getModuledata()->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
            if(!empty($dtype)){$query->setParameter('dtype',$dtype);}
            $query->setParameter('level',$level);
            $query->setParameter('idpath',$filterkey);
            $query->setFirstResult(0);
	    $query->setMaxResults(1);
            $result= $query->getOneOrNullResult();
            $r=$result['idpath'];
           
            return $r;
	 }

  public function getLastKeyWitoutLevel($entity,$dtype=null){
             $wdtype =" ";
            if(!empty($dtype)){$wdtype =" AND o.dtype=:dtype ";}
            
            $sql="SELECT o.idpath FROM ".$this->getModuledata()->getBundleEntity()." o WHERE  o.entity=:entity $wdtype AND o.level=:level  ORDER BY  o.orderidpath DESC";
            $query = $this->getModuledata()->getEm()->createQuery($sql);
              $query->setParameter('entity',$entity);
            if(!empty($dtype)){$query->setParameter('dtype',$dtype);}
            $query->setParameter('level',0);
            $query->setFirstResult(0);
	    $query->setMaxResults(1);
            $result= $query->getOneOrNullResult();
            $r=null;
			if(isset($result['idpath'])){$r=$result['idpath'];}
            return $r;
	 }         
        public function getNextSequence($key){
                $p = explode(".", $key);
                $lenght=sizeof($p);
                $lastlkey=$p[$lenght-1];
                $nextkey=$lastlkey+1;
                $fatherkey="";
                $cont=0;
                foreach ($p as $v) {
                    if($cont==0){
                        $fatherkey=$v;
                    }else{
                       $fatherkey=$fatherkey.'.'.$v; 
                    }
                    $cont++;
                    if($cont+1==$lenght){break;}
                }
                $nextkey=$fatherkey.'.'.$nextkey;
                return $nextkey;
        }
       
        public function getParentInfo($entity,$key,$dtype=null){
            if(empty($key)) return null;
             $wdtype =" ";
            if(!empty($dtype)){$wdtype =" AND o.dtype=:dtype ";}
            
            $sql="SELECT o.id,o.idpath,o.path,o.orderidpath FROM ".$this->getModuledata()->getBundleEntity()." o WHERE  o.entity=:entity $wdtype AND o.idpath=:idpath";
            
            $query = $this->getModuledata()->getEm()->createQuery($sql);
              $query->setParameter('entity',$entity);
            if(!empty($dtype)){$query->setParameter('dtype',$dtype);}
            $query->setParameter('idpath',$key);
            $result= $query->getOneOrNullResult();
            return  $result;
            
	 } 
         public function getParentInfoById($id){
            
            $sql="SELECT o.id,o.idpath,o.path,o.orderidpath FROM ".$this->getModuledata()->getBundleEntity()." o WHERE  o.id=:id";
            $query = $this->getModuledata()->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
            return  $result;
            
	 } 
         
         public function makePath($parentinfo,$dto){
                    $parentid = null;
                    $path = null;
                   if(empty($parentinfo)){return null;}
                    if (isset($parentinfo['id'])) {
                        $parentid = $parentinfo['id'];
                    }
                    if (isset($parentinfo['path'])) {
                        $path = $parentinfo['path'];
                    }
                    $dto['parent']=$parentid;
                    if (empty($path)) {
                        $path = '/' . $parentid . '/';
                    } else {
                        $path = $path . '' . $parentid . '/';
                    }
                    $dto['path']=$path; 
             return $dto;
         }  
         /* delete old version
        public function makePath($parentinfo,$dto){
                    $parentid = null;
                    $path = null;
                    if (isset($parentinfo['id'])) {
                        $parentid = $parentinfo['id'];
                    }
                    if (isset($parentinfo['path'])) {
                        $path = $parentinfo['path'];
                    }
                    $dto->setParent($parentid);
                    if (empty($path)) {
                        $path = '/' . $parentid . '/';
                    } else {
                        $path = $path . '' . $parentid . '/';
                    }
                    $dto->setPath($path); 
             return $dto;
         }  */
//$format lastend | lastfirst | onlyparentpath
    public function getPath($list,$currentid,$fullpaht=true,$format='lastend'){
        //get list of path
       
        $currenrow=null;
        $currentname="";
        $currentpath=null;
        $parentsname="";
        if (array_key_exists($currentid,$list)){
            $currenrow=$list[$currentid];
            $currentname=$currenrow['name'];
            $currentpath=$currenrow['path'];
        }
        
        if(!empty($currentpath)){
            $p=explode("/",$currentpath);
            
            foreach ($p as $k) {
                if(!empty($k) && is_numeric($k)){
                 
                      if (array_key_exists($k,$list)){
                            $row=$list[$k];
                            $name=$row['name'];
                           
                            if(empty($parentsname)){$parentsname=$name;}
                            else{$parentsname.="/".$name;}
                            
                        }
                }
                
            }
        }
        
        if($fullpaht){
            if(empty($parentsname) && $format!='onlyparentpath'){$parentsname=$currentname;}
            else{ 
                if($format=='lastend'){$parentsname.="/".$currentname;}
                else  if($format=='lastfirst') {$parentsname=$currentname." - ".$parentsname; }
            }
        } 
       return  $parentsname;
    }         
    public function getFather($key){
            $level=substr_count($key,".");
            if($level==0) {return "";}
             $p = explode(".", $key);
             $lenght=sizeof($p);
             $lenght=$lenght-1;
             $cont=1;
             $newkey="";
    
             foreach ($p as $v) {
                    if($cont==1){
                        $newkey=$v;
                      }else{
                           $newkey.='.'.$v;
                      }
                      if($cont==$lenght){return $newkey;}
                     $cont++;
             } 
             return $newkey;
        }
        
       public function addSessionParentName($list,$key){
          $badiuSession=$this->getContainer()->get('badiu.system.access.session');
          $badiuSession->setHashkey($this->getSessionhashkey());
           $listparent=$badiuSession->getValue($key);
           
           foreach ($list as $k => $v) {
               $listparent[$k]=$v;
           }
           $badiuSession->addValue($key,$listparent);
       }
    public function initSession($keymaneger,$entity){
      
         $sdata=$keymaneger->data();   
         $sdata=$this->getContainer()->get($sdata);
         $sessionkey=$keymaneger->session();
         $param=array('entity'=>$entity,'fullpath'=>false);
         $list=$sdata->getFormChoiceParent($param);
         
         $badiuSession=$this->getContainer()->get('badiu.system.access.session');
         $badiuSession->setHashkey($this->getSessionhashkey());
         $badiuSession->addValue($sessionkey,$list);
        
    }
    
    public function updateSession($keymaneger,$idpath){
    //   echo "<br>B updateSession INVOCADO";
        $sessionkey=$keymaneger->session();
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
       $entity=$badiuSession->get()->getEntity();
        if(!$badiuSession->existValue($sessionkey)){
            $this->initSession($keymaneger,$entity);
        }
        else{
           //  echo "<br>C add item ---- ";
            $sdata=$keymaneger->data();   
            $sdata=$this->getContainer()->get($sdata); 
             $param=array('entity'=>$entity,'fullpath'=>false,'idpath'=>$idpath,'idpathfilterfather'=>true);
             $list=$sdata->getFormChoiceParent($param);
             foreach ($list as $row) {
               //  echo "<br> ---------------- ";
              //   print_r($row);
                 $badiuSession->addPackageValue($sessionkey,$row['id'],$row['name']);
             }
         }
        
         
    }
    
    

      function getModuledata() {
          return $this->moduledata;
      }

      function setModuledata($moduledata) {
          $this->moduledata = $moduledata;
      }


}