<?php

namespace Badiu\System\CoreBundle\Model\Lib\Keytree;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FactoryKeyTreeChangeOrder  extends BadiuModelLib {
       /**
     * @var Object
     */
    private $moduledata;
   
	 /**
     * @var integer
     */
    private $scale;
      function __construct(Container $container) {
        parent::__construct($container);
                $this->moduledata=null;
				$this->scale=100;
               
      }
     public function init($moduledatakey){
		$this->setModuledata($this->getContainer()->get($moduledatakey));
		  }
      
     function exec($param) {
		 $oderbefore=$this->getUtildata()->getVaueOfArray($param,'oderbefore');
		 if(empty($oderbefore)){return null;}
		 
		 $currentid=$this->getUtildata()->getVaueOfArray($param,'currentid');
		 if(empty($currentid)){return null;}
		 
		 $datakey=$this->getUtildata()->getVaueOfArray($param,'datakey');
		 if(empty($this->getModuledata())){$this->init($datakey);}
		 
		 
		 //get item to change
		  $itcfpram=$this->getUtildata()->getVaueOfArray($param,'filter');
		  $itcfpram['id']=$currentid;
		  $itemtochange=$this->getItem($itcfpram); 
		  
		 //get positon to set
		 $ipfpram=$this->getUtildata()->getVaueOfArray($param,'filter');
		 $ipfpram['id']=$oderbefore;
		 $itempositionset=$this->getItem($ipfpram);
		 
		 
		 //change item level 0 on next position level 0 set end
		 $result=$this->changeFirstLevelToLastPositionFirstLevel($param,$itemtochange,$itempositionset);
		 if(!empty($result)){return null;}
		 
		 //change item level 0 on next position level 0
		 $result=$this->changeFirstLevelToAnyPositionFirstLevel($param,$itemtochange,$itempositionset);
		 if(!empty($result)){return null;}
		
		
	 }
    
	 function getLastItem($fparam) {
		$wsql=$this->getModuledata()->makeSqlWhere($fparam);
        $sql="SELECT o.id,o.idpath,o.path,o.orderidpath FROM ".$this->getModuledata()->getBundleEntity()." o  WHERE o.id > :bnetctrid $wsql ORDER BY o.orderidpath DESC";
        $query = $this->getModuledata()->getEm()->createQuery($sql);
        $query->setParameter('bnetctrid',0);
        $query=$this->getModuledata()->makeSqlFilter($query, $fparam);
		$query->setMaxResults(1);
        $result= $query->getOneOrNullResult();
		return $result;
	 }

	
	function getItem($fparam) {
		$wsql=$this->getModuledata()->makeSqlWhere($fparam);
        $sql="SELECT o.id,o.level,o.parent,o.idpath,o.path,o.orderidpath FROM ".$this->getModuledata()->getBundleEntity()." o  WHERE o.id > :bnetctrid $wsql ";
        $query = $this->getModuledata()->getEm()->createQuery($sql);
        $query->setParameter('bnetctrid',0);
        $query=$this->getModuledata()->makeSqlFilter($query, $fparam);
		$query->setMaxResults(1);
        $result= $query->getOneOrNullResult();
		return $result;
	 }
	 
	 function getNextItem($fparam) {
		$currentorderipath=$this->getUtildata()->getVaueOfArray($fparam,'currentorderipath');
		if(empty($currentorderipath)){return null;}
		unset($fparam['currentorderipath']);
		$wsql=$this->getModuledata()->makeSqlWhere($fparam);
        $sql="SELECT o.id,o.idpath,o.path,o.orderidpath FROM ".$this->getModuledata()->getBundleEntity()." o  WHERE o.orderidpath > :currentorderipath  $wsql ORDER BY o.orderidpath ";
        $query = $this->getModuledata()->getEm()->createQuery($sql);
        $query->setParameter('currentorderipath',$currentorderipath);
        $query=$this->getModuledata()->makeSqlFilter($query, $fparam);
		$query->setMaxResults(1);
        $result= $query->getOneOrNullResult();
		return $result;
	 }
	function getNextItems($fparam) {
		$currentorderipath=$this->getUtildata()->getVaueOfArray($fparam,'currentorderipath');
		if(empty($currentorderipath)){return null;}
		unset($fparam['currentorderipath']);
		$wsql=$this->getModuledata()->makeSqlWhere($fparam);
        $sql="SELECT o.id,o.idpath,o.path,o.orderidpath FROM ".$this->getModuledata()->getBundleEntity()." o  WHERE o.orderidpath > :currentorderipath  $wsql ORDER BY o.orderidpath ";
        $query = $this->getModuledata()->getEm()->createQuery($sql);
        $query->setParameter('currentorderipath',$currentorderipath);
        $query=$this->getModuledata()->makeSqlFilter($query, $fparam);
		
        $result= $query->getResult();
		return $result;
	 }
	 function getChildrens($param) {
		 $idpath=$this->getUtildata()->getVaueOfArray($param,'idpath');
		 if(empty($idpath)){return null;}
		 if(isset($param['idpath'])){ unset($param['idpath']);}
		 
		$wsql=$this->getModuledata()->makeSqlWhere($param);
		$wsql .=" AND  o.idpath LIKE '".$idpath.".%' ";
        $sql="SELECT o.id,o.level,o.parent,o.idpath,o.path,o.orderidpath FROM ".$this->getModuledata()->getBundleEntity()." o  WHERE o.level >  :level $wsql ";
       
		$query = $this->getModuledata()->getEm()->createQuery($sql);
        $query->setParameter('level',0);
        $query=$this->getModuledata()->makeSqlFilter($query, $param);
		$result= $query->getResult();
		return $result;
	 }
	 //change item level 0 on next position level 0
	function changeFirstLevelToLastPositionFirstLevel($param,$itemtochange,$itempositionset){
		 $itemtochangelevel=$this->getUtildata()->getVaueOfArray($itemtochange,'level');
		 $itempositionsetlevel=$this->getUtildata()->getVaueOfArray($itempositionset,'level');
		 if($itemtochangelevel!=0){return null;}
		 if($itempositionsetlevel!=0){return null;}
		 
		 $fparam=$this->getUtildata()->getVaueOfArray($param,'filter');
		 $fparam['level']=0;
		 $lastitem=$this->getLastItem($fparam);
		 $lastitemid=$this->getUtildata()->getVaueOfArray($lastitem,'id');
		 $itempositionsetid=$this->getUtildata()->getVaueOfArray($itempositionset,'id');
		 
		 
		 if($lastitemid!=$itempositionsetid){return null;}
		 $this->changeItem($param,$itemtochange,$lastitem);
		 /*$currentitemid=$this->getUtildata()->getVaueOfArray($itemtochange,'id');
		 $currentitemorderidpath=$this->getUtildata()->getVaueOfArray($itemtochange,'orderidpath');
		 $lastorderidpath=$this->getUtildata()->getVaueOfArray($lastitem,'orderidpath');
		 $neworderidpath=$lastorderidpath+$this->scale;
		 
		 $lastidpath=$this->getUtildata()->getVaueOfArray($lastitem,'idpath');
		 $newidpath=$lastidpath+1;
		 
		 $upparam=array('id'=>$currentitemid,'orderidpath'=>$neworderidpath,'idpath'=>$newidpath);
		 $this->getModuledata()->updateNativeSql($upparam,false);
		 
		 //update childrens
		 $valueaddorderidpath=$neworderidpath-$currentitemorderidpath;
		 $currentitemidpath=$this->getUtildata()->getVaueOfArray($itemtochange,'idpath');
		 $fparam=$this->getUtildata()->getVaueOfArray($param,'filter');
		 $fparam['idpath']=$currentitemidpath;
		 $listchildren=$this->getChildrens($fparam);
		 if(is_array($listchildren)){
			  foreach ($listchildren as $lc){
				  $upitemparam=array();
				  $upitemparam['id']=$this->getUtildata()->getVaueOfArray($lc,'id');
				  $upitemparam['orderidpath']=$valueaddorderidpath+$this->getUtildata()->getVaueOfArray($lc,'orderidpath');
				  $childrentochance=$this->getUtildata()->getVaueOfArray($lc,'idpath');
				  $upitemparam['idpath']=$this->changeIdpath($newidpath,$childrentochance);
				  $this->getModuledata()->updateNativeSql($upitemparam,false);
				 //print_r($upitemparam);
				  
			  }
		 }*/
		 //exit;
    
		 return true;

	 }
	 
	  //change item level 0 on next position level 0
	function changeFirstLevelToAnyPositionFirstLevel($param,$itemtochange,$itempositionset){
		 $itemtochangelevel=$this->getUtildata()->getVaueOfArray($itemtochange,'level');
		 $itempositionsetlevel=$this->getUtildata()->getVaueOfArray($itempositionset,'level');
		 if($itemtochangelevel!=0){return null;}
		 if($itempositionsetlevel!=0){return null;}
		 
		 $fparam=$this->getUtildata()->getVaueOfArray($param,'filter');
		 $fparam['level']=0;
		 $lastitem=$this->getLastItem($fparam);
		 $lastitemid=$this->getUtildata()->getVaueOfArray($lastitem,'id');
		 $itempositionsetid=$this->getUtildata()->getVaueOfArray($itempositionset,'id');
		 
		 
		 if($lastitemid==$itempositionsetid){return null;}
		 
		 $itempositionsetorderidpath=$this->getUtildata()->getVaueOfArray($itempositionset,'orderidpath');
		 
		 $fparam=$this->getUtildata()->getVaueOfArray($param,'filter');
		 $fparam['level']=0;
		 $fparam['currentorderipath']=$itempositionsetorderidpath;
		 
		 //next items
		 $nextitem=$this->getNextItem($fparam);
		 $nextitems=$this->getNextItems($fparam);
		 
		 if(is_array($nextitems)){
			  $contseqitc=2;
			   foreach ($nextitems as $newitemtochange){
				    $lastitem=$itemtochange;
					$lastitem['idpath']=$lastitem['idpath']+$contseqitc;
					$lastitem['orderidpath']=$lastitem['orderidpath']+($contseqitc*$this->scale);
				    $this->changeItem($param,$newitemtochange,$lastitem);
					$contseqitc++;
					//print_r($lastitem);
			   }
		  }
    
		//change current
		$this->changeItem($param,$itemtochange,$itempositionset);
		 return true;

	 }
	 function changeItem($param,$itemtochange,$lastitem) {
		  $currentitemid=$this->getUtildata()->getVaueOfArray($itemtochange,'id');
		 $currentitemorderidpath=$this->getUtildata()->getVaueOfArray($itemtochange,'orderidpath');
		 $lastorderidpath=$this->getUtildata()->getVaueOfArray($lastitem,'orderidpath');
		 $neworderidpath=$lastorderidpath+$this->scale;
		 
		 $lastidpath=$this->getUtildata()->getVaueOfArray($lastitem,'idpath');
		 $newidpath=$lastidpath+1;
		 
		 $upparam=array('id'=>$currentitemid,'orderidpath'=>$neworderidpath,'idpath'=>$newidpath);
		 $this->getModuledata()->updateNativeSql($upparam,false);
		
		 //update childrens
		 $valueaddorderidpath=$neworderidpath-$currentitemorderidpath;
		 $currentitemidpath=$this->getUtildata()->getVaueOfArray($itemtochange,'idpath');
		 $fparam=$this->getUtildata()->getVaueOfArray($param,'filter');
		 $fparam['idpath']=$currentitemidpath;
		 $listchildren=$this->getChildrens($fparam);
		 if(is_array($listchildren)){
			  foreach ($listchildren as $lc){
				  $upitemparam=array();
				  $upitemparam['id']=$this->getUtildata()->getVaueOfArray($lc,'id');
				  $upitemparam['orderidpath']=$valueaddorderidpath+$this->getUtildata()->getVaueOfArray($lc,'orderidpath');
				  $childrentochance=$this->getUtildata()->getVaueOfArray($lc,'idpath');
				  $upitemparam['idpath']=$this->changeIdpath($newidpath,$childrentochance);
				  $this->getModuledata()->updateNativeSql($upitemparam,false);
					 
			  }
		 }
	 }
	  function changeIdpath($newfather,$childrentochance) {
			if(empty($childrentochance)){return $childrentochance;}
            $pos=stripos($childrentochance, ".");
            if($pos=== false){return $childrentochance;}
			$order= explode(".", $childrentochance);
			$order[0]=$newfather;
			
			$newekey="";
			$cont=0;
			foreach ($order as $item){
				if($cont==0){$newekey=$item; }
				else {$newekey.='.'.$item;}
				$cont++;
			}
		 return $newekey; 
	  }
      function getModuledata() {
          return $this->moduledata;
      }

      function setModuledata($moduledata) {
          $this->moduledata = $moduledata;
      }


}