<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Dashboard;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;

/**
 * Description of MakeSelect
 *
 * @author lino
 */
class DashboardData {

	/**
	 * @var Container
	 */
	private $container;

	/**
	 * @var Translator
	 */
	private $translator;

	         /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
	private $systemdata;
	function __construct(Container $container) {
		$this->container = $container;
		$this->translator = $this->container->get('translator');
	}

	
        public function confg($index,$rows, $querystring,$psqlconfigkey) {
            if(empty($rows))return $rows;
             $key="fieldkeysql$index";
             $savesessionkey="savesession$index";
             $fieldkey= $querystring->getValue($key);
             $savesession=$querystring->getValue($savesessionkey);
            $list=array();
          
            if(!empty($fieldkey)){
              foreach ($rows as $row){
                  $vkey=$row[$fieldkey];
                  $list[$vkey]=$row;
              }
            }else {$list=$rows;}
            
            if($savesession){
                   $keyutil=$this->getContainer()->get('badiu.system.core.lib.config.keyutil');
                   $keysesion= $keyutil->removeLastItem($psqlconfigkey);
                   $keysesion=$keysesion.'.'.$index;
                   $session =$this->getContainer()->get('session');
                   $session->set($keysesion, $list);
                 
            }
            return  $list;
        }

        public function label($labelconf,$viewconfig) {
          $queryString = $this->container->get('badiu.system.core.lib.http.querystring');
           $queryString->setQuery($labelconf);
          $queryString->makeParam();
          $param=$queryString->getParam();
          $lebalparam=array();
           $lresult=array();
         
          foreach ($param as $key => $value) {
                  $lebalparam[$key]=$this->translator->trans($value);
                }
            
            $listview = array();
            $hasgroup=false;
          
          if (!empty($viewconfig)) {
            $gpos = stripos($viewconfig, "=");
            if ($gpos !==false ) {$hasgroup=true;}
            if($hasgroup){
                $gqueryString = $this->container->get('badiu.system.core.lib.http.querystring');
                $gqueryString->setQuery($viewconfig);
                $gqueryString->makeParam();
                $gparam=$gqueryString->getParam();
               
                foreach ($gparam as $gkey => $gvalue) {
                  $livalues=array();
                  $igpos = stripos($gvalue, ",");
                  if ($igpos !==false ) {
                    $livalues = explode(",", $gvalue);
                  }else{$livalues=array($gvalue);} 
                  $listview[$gkey]=$livalues;
                }
                
            }else{
              $pos = stripos($viewconfig, ",");
              if ($pos !==false ) {
                  $listview = explode(",", $viewconfig);
              } 
            }
            
          }
          
          if($hasgroup){
              foreach ($listview as $lkey => $value) {
                  $ngvalue=array();
                  foreach ($value as $iv) {
                    if(array_key_exists($iv,$lebalparam)){
                      $ngvalue[$iv]=$lebalparam[$iv];
                    }else{ $ngvalue[$iv]=$this->translator->trans($iv);}
                  }
                  $lresult[$lkey]=$ngvalue;
               
            }
          }else{ 
            foreach ($listview as  $value) {
              if(array_key_exists($value,$lebalparam)){
                $lresult[$value]=$lebalparam[$value];
              }else{ $lresult[$value]=$this->translator->trans($value);}
          }
          }
		  if(sizeof($lresult)==0){$lresult=$lebalparam;}
	   
           return $lresult;
         } 
            
        public function title($sqlconf,$index,$single=false) {
            $queryString = $this->container->get('badiu.system.core.lib.http.querystring');
            $queryString->setQuery($sqlconf);
            $queryString->makeParam();
                   
            $key="title$index";
			if($single){$key="stitle$index";}
            $value=$queryString->getValue($key);
            if(empty($value)){return null;}
            $value=$this->translator->trans($value);
             return $value;
           }  
		public function ckey($sqlconf,$index,$key) {
            $queryString = $this->container->get('badiu.system.core.lib.http.querystring');
            $queryString->setQuery($sqlconf);
            $queryString->makeParam();
                   
            $key=$key.$index;
			$value=$queryString->getValue($key);
            return $value;
           }  
      public function formatList($formatconfig,$viewconfig,$rows) {
          $newlist=array();
          foreach ($rows as $row) {
              $frow=$this->formatSingle($formatconfig,$viewconfig,$row);
              array_push($newlist,$frow);
          }
         
           return $newlist;
        }
        public function formatSingle($formatconfig,$viewconfig,$row) {
            $configformat = $this->container->get('badiu.system.core.lib.format.configformat');
			$configformat->setSystemdata($this->getSystemdata());
			$configformat->setSessionhashkey($this->getSessionhashkey());
            $row=$configformat->dbrow($formatconfig,$viewconfig,$row);
           return $row;
        }
	public function getContainer() {
		return $this->container;
	}

	public function getTranslator() {
		return $this->translator;
	}

	public function setContainer(Container $container) {
		$this->container = $container;
	}

	public function setTranslator(Translator $translator) {
		$this->translator = $translator;
	}
    public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }
   	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
