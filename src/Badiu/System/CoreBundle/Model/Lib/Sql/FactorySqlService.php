<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Sql;

use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use PDOException;
use Exception;
/**
 *
 * @author lino
 */
class FactorySqlService extends BadiuExternalService {

    /**
     * @var string
     */
    private $table;

    /**
     * @var string
     */
    private $operation;

    /**
     * @var string
     */
    private $bkey;

    /**
     * @var array
     */
    private $param;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var string
     */
    private $bundleEntity;

    /**
     * @var boolean
     */
    private $webserviceprocess = true;

      /**
     * @var boolean
     */
    private $defaultablevalue = false;
    function __construct(Container $container) {
        parent::__construct($container);
    }

    function exec() {
        $this->initParam();
        $this->initTableName();
       // echo "table name: " . $this->getTable();
        return null;
    }

    function init() {
         
        if ($this->webserviceprocess) {
            $this->initParam();
            $this->initEm();
            
        }
        
        $this->initTableName();
    }

    function initParam() {
        
        $this->bkey = $this->getBasekey();
        $this->param = $this->getHttpQuerStringParam();
        $operation = $this->getContainer()->get('request')->get('_operation');
    }

    public function getBasekey() {
        $bkey = $this->getContainer()->get('request')->get('bkey');
        if (empty($bkey)) {
            $key = $this->getContainer()->get('request')->get('key');
            $bkey = $this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
           
        }
        return $bkey;
    }

    public function initEm() {
       
        $datakey = $this->bkey . '.data';
        $data = $this->getContainer()->get($datakey);
        $this->setEm($data->getEm());
        $this->bundleEntity = $data->getBundleEntity();
    }

    public function initTableName() {
        $tableName = $this->getEm()->getClassMetadata($this->bundleEntity)->getTableName();
        $this->setTable($tableName);
    }

    public function count() {
        $this->init();
        $table = $this->table;
        $lparam = array('em' => $this->getEm(),'bundleentity'=>$this->bundleEntity, 'param' => $this->param, 'operation' => 'select');
        $factorysqlserviceutil = $this->getContainer()->get('badiu.system.core.lib.sql.factorysqlserviceutil');
         $factorysqlserviceutil->setDefaultablevalue($this->getDefaultablevalue());
        $chekcolumns = $factorysqlserviceutil->existColumn($lparam);

        if (!$chekcolumns->exist) {
            if ($this->webserviceprocess) {
                $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                $this->getResponse()->setInfo($datakey = $this->bkey . '.data.columns.not.exist');
                $this->getResponse()->setMessage($chekcolumns->nocolumns);
                return $this->getResponse()->get();
            }
        }


        $wsql = $factorysqlserviceutil->makeFilter($lparam);
        $columns = $wsql->key;
        $params = $wsql->params;
        $sql = "SELECT COUNT(id) AS countrecord FROM $table WHERE $columns ";
       // echo $sql;
        //print_r($params);exit;
        $stmt = $this->getEm()->getConnection()->prepare($sql);
        foreach ($params as $key => $value) {
            $stmt->bindValue($key, $value);
        }

        $stmt->execute();
        $result = $stmt->fetch();
        $result = $result['countrecord'];
        if ($this->webserviceprocess) {
            $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
            $this->getResponse()->setMessage($result);
            return $this->getResponse()->get();
        } else {
            return $result;
        }
    }

  
    public function insert() {
        $this->init();
       
        $table = $this->table;
        $lparam = array('em' => $this->getEm(),'bundleentity'=>$this->bundleEntity, 'param' => $this->param, 'operation' => 'insert');
        $factorysqlserviceutil = $this->getContainer()->get('badiu.system.core.lib.sql.factorysqlserviceutil');
        $factorysqlserviceutil->setDefaultablevalue($this->getDefaultablevalue());
        $chekcolumns = $factorysqlserviceutil->existColumn($lparam);
        if (!$chekcolumns->exist) {
            if ($this->webserviceprocess) {
                $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                $this->getResponse()->setInfo($datakey = $this->bkey . '.data.columns.not.exist');
                $this->getResponse()->setMessage($chekcolumns->nocolumns);
                return $this->getResponse()->get();
            }
        }


        $wsql = $factorysqlserviceutil->makeInsert($lparam);
        $columns = $wsql->key;
        $values = $wsql->value;
        $params = $wsql->params;
        $sql = "INSERT INTO $table ($columns)  VALUES ($values)";
      //   print_r($this->param);exit;
       // echo $sql; 
       /* $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
        $urservicebase=$utilapp->getServiceUrl();
        $querystring = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
  //  echo "sdfsdfsdfs: ". $this->bkey;exit;
        $querystring->setParam($this->param);
        $querystring->add('bkey',$this->bkey);
        $querystring->add('_function','insert');
        $querystring->add('_service','badiu.system.core.lib.sql.factorysqlservice');
        $querystring->makeQuery();
        $query=$querystring->getQuery();
        $url="$urservicebase?$query";
       // echo $url;exit;*/
        
        $conn=$this->getEm()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute($params);
        $result =$conn->lastInsertId();
        if ($this->webserviceprocess) {
             $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
            $this->getResponse()->setMessage($result);
            return $this->getResponse()->get();
        }else {
            return $result;
        }
       
    }

    public function update() {
      
        $this->init();
      //   print_r($this->param);exit;
        $table = $this->table;
        if(empty($table) && $this->webserviceprocess){
                $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo($datakey = $this->bkey . '.table.isnull');
                 $this->getResponse()->setMessage('Table is null. Check if entity is defined');
                 return $this->getResponse()->get();
        }
       
        $lparam = array('em' => $this->getEm(),'bundleentity'=>$this->bundleEntity, 'param' => $this->param, 'operation' => 'update');
        $factorysqlserviceutil = $this->getContainer()->get('badiu.system.core.lib.sql.factorysqlserviceutil');
         $factorysqlserviceutil->setDefaultablevalue($this->getDefaultablevalue());
        $chekcolumns = $factorysqlserviceutil->existColumn($lparam);
        if (!$chekcolumns->exist) {
              if ($this->webserviceprocess) {
                  $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                  $this->getResponse()->setInfo($datakey = $this->bkey . '.data.columns.not.exist');
                  $this->getResponse()->setMessage($chekcolumns->nocolumns);
                   return $this->getResponse()->get();
              }
            
        }

        $wsql = $factorysqlserviceutil->makeUpdate($lparam);
        $columns = $wsql->key;
        $id = $wsql->idvalue;
        $params = $wsql->params;
        $sql = "UPDATE $table SET $columns  WHERE id=:id";

      // print_r($params);
       // echo $sql;exit;
        $stmt = $this->getEm()->getConnection()->prepare($sql);
        foreach ($params as $key => $value) {
          $stmt->bindValue($key, $value);
        }
         $result = null;
         try{
              $result = $stmt->execute();
           
         } catch (Exception $ex) {
             if ($this->webserviceprocess) {
                  $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                  $this->getResponse()->setInfo('badiu.system.core.lib.sql.factorysqlservice.generalerror');
                  $this->getResponse()->setMessage($ex->getMessage());
                   return $this->getResponse()->get();
              }else{ echo print_r($ex->getMessage());exit;} //review it
         }
       
        // $result=$stmt->execute($params);
        if ($this->webserviceprocess) {
          
            $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
            $this->getResponse()->setMessage($result);
            return $this->getResponse()->get();
        }else {
    
            return $result;
        }
        
    }
    
      public function remove() {
        $this->init();
      //   print_r($this->param);exit;
        $table = $this->table;
        if(empty($table) && $this->webserviceprocess){
                $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo($datakey = $this->bkey . '.table.isnull');
                 $this->getResponse()->setMessage('Table is null. Check if entity is defined');
                 return $this->getResponse()->get();
        }
        $id= $this->param['id'];
        $sql = "DELETE FROM $table  WHERE id=$id";
        $stmt = $this->getEm()->getConnection()->prepare($sql);
        $result = null;
         try{
              $result = $stmt->execute();
         } catch (Exception $ex) {
             if ($this->webserviceprocess) {
                  $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                  $this->getResponse()->setInfo('badiu.system.core.lib.sql.factorysqlservice.generalerror');
                  $this->getResponse()->setMessage($ex->getMessage());
                   return $this->getResponse()->get();
              }else{ }
         }
       
        // $result=$stmt->execute($params);
        if ($this->webserviceprocess) {
            $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
            $this->getResponse()->setMessage($result);
            return $this->getResponse()->get();
        }else {
            return $result;
        }
        
    }
	
	 public function removeBatch() {
        $this->init();
      //   print_r($this->param);exit;
        $table = $this->table;
        if(empty($table) && $this->webserviceprocess){
                $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                 $this->getResponse()->setInfo($datakey = $this->bkey . '.table.isnull');
                 $this->getResponse()->setMessage('Table is null. Check if entity is defined');
                 return $this->getResponse()->get();
        }
		$lparam = array('em' => $this->getEm(),'bundleentity'=>$this->bundleEntity, 'param' => $this->param, 'operation' => 'delete');
		$factorysqlserviceutil = $this->getContainer()->get('badiu.system.core.lib.sql.factorysqlserviceutil');
         $factorysqlserviceutil->setDefaultablevalue($this->getDefaultablevalue());
		 
		 $wsql = $factorysqlserviceutil->makeFilter($lparam);
        $columns = $wsql->key;
        $params = $wsql->params;
		
		
        $sql = "DELETE FROM $table  WHERE $columns ";
			
        $stmt = $this->getEm()->getConnection()->prepare($sql);
		foreach ($params as $key => $value) {
			$stmt->bindValue($key, $value);
		}
        $result = null;
			try{
			   $result = $stmt->execute();
			   $result = $stmt->fetch();
         } catch (Exception $ex) {
             if ($this->webserviceprocess) {
                  $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
                  $this->getResponse()->setInfo('badiu.system.core.lib.sql.factorysqlservice.generalerror');
                  $this->getResponse()->setMessage($ex->getMessage());
                   return $this->getResponse()->get();
              }else{ }
         }
       
        // $result=$stmt->execute($params);
        if ($this->webserviceprocess) {
            $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
            $this->getResponse()->setMessage($result);
            return $this->getResponse()->get();
        }else {
            return $result;
        }
        
    }
    //review it
   /* function execBachSQL() {
        $db="default";//$this->getContainer()->get('badiu.system.access.session')->get()->getDbapp(); 
         $em=$this->getContainer()->get('doctrine')->getEntityManager($db); 
         $lsql='';
         $psql=explode(";",$lsql);
         $contf=0;
         $conts=0;
         foreach ($psql as $sql) {
             try{
                  $stmt = $em->getConnection()->prepare($sql);
                   $result = $stmt->execute();
                 $conts++;
             } catch (\Doctrine\ORM\ORMException  $ex) {
                $contf++;
             }catch (PDOException $ex) {
                $contf++;
             }catch (DBALException $e) {
            // $message = sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage());
            } catch (PDOException $e) {
            // $message = sprintf('PDOException [%i]: %s', $e->getCode(), $e->getMessage());
            } catch (ORMException $e) {
           // $message = sprintf('ORMException [%i]: %s', $e->getCode(), $e->getMessage());
            } catch (Exception $e) {
            //$message = sprintf('Exception [%i]: %s', $e->getCode(), $e->getMessage());
             }
            echo "sucess: $conts | failed:   $contf";
         }
          
        
         
        return null;
    }*/
    
    function getOperation() {
        return $this->operation;
    }

    function getBkey() {
        return $this->bkey;
    }

    function setOperation($operation) {
        $this->operation = $operation;
    }

    function setBkey($bkey) {
        $this->bkey = $bkey;
    }

    function getTable() {
        return $this->table;
    }

    function setTable($table) {
        $this->table = $table;
    }

    function getParam() {
        return $this->param;
    }

    function getEm() {
        return $this->em;
    }

    function getBundleEntity() {
        return $this->bundleEntity;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setEm($em) {
        $this->em = $em;
    }

    function setBundleEntity($bundleEntity) {
        $this->bundleEntity = $bundleEntity;
    }

    function getWebserviceprocess() {
        return $this->webserviceprocess;
    }

    function setWebserviceprocess($webserviceprocess) {
        $this->webserviceprocess = $webserviceprocess;
    }
    function getDefaultablevalue() {
        return $this->defaultablevalue;
    }

    function setDefaultablevalue($defaultablevalue) {
        $this->defaultablevalue = $defaultablevalue;
    }


}
