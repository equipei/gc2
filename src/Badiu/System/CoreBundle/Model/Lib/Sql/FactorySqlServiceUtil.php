<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Sql;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;


/**
 *
 * @author lino
 */
class FactorySqlServiceUtil extends BadiuModelLib{
         /**
     * @var boolean
     */
    private $defaultablevalue = false;
    function __construct(Container $container) {
                 parent::__construct($container);
                 
       
          
                
      }
     
       public function existColumn($lparam) {
           $em=$lparam['em'];
           $bundleentity=$lparam['bundleentity'];
           $param=$lparam['param'];
           $metadata= $em->getClassMetadata($bundleentity);
           $nocolumns="";
           $exist=TRUE;
           $cont=0;
           $result= new  \stdClass();
		   if(is_array($param)){
           foreach ($param as $key => $value) {
              
               if(!$this->isReservedParam($key)){
                  // echo " $key -  ". print_r($value);
                  if(!$metadata->hasField($key)){
                       if($cont==0){$nocolumns="$key";}
                       else {$nocolumns.=",$key";}
                       $exist=FALSE;
                     $cont++;
                   }
               }
              
           }
		  }
           $result->exist=$exist;
           $result->nocolumns=$nocolumns;
           return $result;
       }
       public function getColumnDada($lparam) {
           $em=$lparam['em'];
           $bundleentity=$lparam['bundleentity'];
           $param=$lparam['param'];
           $operation=$lparam['operation'];
           
          // $type=$lparam['type'];
          // $out=$lparam['out']; // arrayofkey | arrayofvalue | arrayofkeyvalue | sqlinsertkey | sqlinsertkey | sqlupdate | sqlfilter 
         
         //  $operation='select';
         //  if($out=='sqlinsertkey' || $out=='sqlinsertkey'){$operation='insert';}
          // else if($out=='sqlupdate'){$operation='update';}
           
           if(empty($operation)){$out='select';}
           
           $date=new \Datetime();
           $dateformated=$date->format('Y-m-d H:i:s');
           
           $columns=array();
           
           $metadata= $em->getClassMetadata($bundleentity);
		   if(is_array($param)){
            foreach ($param as $key => $value) {
                 if(!$this->isReservedParam($key)){
                  //if($metadata->hasField($key)){
                     
                      $ftype=$metadata->getTypeOfField($key);
                       $value=$this->formatValue($ftype,$value);
                       $columns[$key]=$value;
                    
                  // }
               }
           }
		  }
         /*  echo "<pre>";
           print_r($columns);
           echo "</pre>";exit;*/
           if($this->getDefaultablevalue()){
                 $columns['entity']=$this->getEntity();
                  $columns['deleted']=0;
                 if($operation=='insert'){ $columns['timecreated']=$dateformated; }
                 else if($operation=='update'){ $columns['timemodified']=$dateformated; }
           }
          
           
          
           return $columns;
       }
       
        public function makeFilter($lparam) {
            $datasql=$this->getColumnDada($lparam);
          
            $contf=0;
           $wsql= new  \stdClass();
           $columns="";
           $params=array();
           // get entity
           foreach ($datasql as $key => $value) {
                   if($key=='id' || $key=='entity' ){
                        if($contf==0){$columns=" $key=:$key ";}
                        else {$columns.=" AND $key=:$key ";}
                        $contf++;
                   }
                 
           }
           $contf=0;
           //not entity
           foreach ($datasql as $key => $value) {
                  if($key!='id' && $key!='entity' ){
                      if($contf==0 && empty($columns) ){$columns.=" $key=:$key ";}
                      else {$columns.=" AND $key=:$key ";}
                      $contf++;
                  }
                  
           }
           
            foreach ($datasql as $key => $value) {
                   $key=":".$key;
                   $params[$key]=$value;
         }
          $wsql->key=$columns;
          $wsql->params=$params;
          return $wsql;
       }
       public function makeInsert($lparam) {
            $datasql=$this->getColumnDada($lparam);
            $columns="";
            $contf=0;
            $wsql= new  \stdClass();
            $columns="";
            $values="";
            $params=array();
           //columns
           foreach ($datasql as $key => $value) {
                if($contf==0){$columns=" $key";}
                else {$columns.=",$key";}
                 $contf++;
           }
           $contf=0; 
           //not entity
           foreach ($datasql as $key => $value) {
                    if($contf==0){$values.=":$key ";}
                      else {$values.=",:$key ";}
                      $contf++;
         }
          foreach ($datasql as $key => $value) {
                   $key=":".$key;
                   $params[$key]=$value;
         }
         
          $wsql->key=$columns;
          $wsql->value=$values;
          $wsql->params=$params;
          return $wsql;
       }
       
        public function makeUpdate($lparam) {
            $datasql=$this->getColumnDada($lparam);
            $columns="";
            $contf=0;
            $wsql= new  \stdClass();
            $idvalue="";
            $params=array();
           //columns
           foreach ($datasql as $key => $value) {
               if($key!='id'){
                   if($key!='entity'){
                       if($contf==0){$columns=" $key=:$key";}
                        else {$columns.=",$key=:$key";}
                        $contf++;
                   }
                    
               }else{
                   $idvalue=$value;
               }
               
           }
          
          foreach ($datasql as $key => $value) {
                 if($key!='entity'){ $params[$key]=$value;}
            }
         
          $wsql->key=$columns;
          $wsql->idvalue=$idvalue;
          $wsql->params=$params;
          return $wsql;
       }
       public function isReservedParam($key) {
           if($key=='bkey1'  //review
                   || $key== "key"  //review it
                   || $key== "bkey"  //review it
                   || $key== "_bkey"  //review it
                   || $key=="_service"
                   || $key=="_token"
                   || $key=="_function"
                  // || $key=="sserviceid" //review it
                   || $key=="entity1" //review it
                   ){
               return true;
           
            }
            return false;
       }
      
          public function formatValue($type,$value) {
           
              //review it
              if($type=='string'){
                 if($value!==0 && empty($value)){$value=NULL;}
              }else if ($type=='datetime' && is_object($value)) {
                    $value=$value->format('Y-m-d H:i:s');
              }else if ($type=='boolean') { //if postgresql set TRUE | FALSE and not 1 | 0
                    if($value==1){$value=1;}
                    else if($value==0){$value=0;}
					else if($value===0){$value=0;}
					else if($value=='0'){$value=0;}
                    else if($value==true){$value=1;}
                    else if($value==false){$value=0;}
                     else {$value='NULL';}
              }else if ($type=='integer' || $type=='bigint') { 
                    if($value!==0 && empty($value)){$value=NULL;}
              }else if ($type=='float') { 
					if($value===0){$value=0;}
					else if($value=='0'){$value=0;}
					else if($value!==0 && empty($value)){$value=NULL;}
			  }
			  else{
                  if($value!==0 && empty($value)){$value=NULL;}
              } 
              return $value;
          }

  function getDefaultablevalue() {
        return $this->defaultablevalue;
    }

    function setDefaultablevalue($defaultablevalue) {
        $this->defaultablevalue = $defaultablevalue;
    }

}
