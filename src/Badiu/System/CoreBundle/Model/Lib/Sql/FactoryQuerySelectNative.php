<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Badiu\System\CoreBundle\Model\Lib\Date\DOMAINTABLE;
/**
 *
 * @author lino
 */
class FactoryQuerySelectNative extends BadiuModelLib {
    
      function __construct(Container $container) {
               parent::__construct($container);
               
      }
    

      function period($paramfilter,$keyfilter,$dbcolumn,$datetimestamp=true) {
        $operator=$this->getUtildata()->getVaueOfArray($paramfilter, '::'.$keyfilter.'operator');    
        $date1=$this->getUtildata()->getVaueOfArray($paramfilter, $keyfilter.'1');
        $date2=$this->getUtildata()->getVaueOfArray($paramfilter, $keyfilter.'2');

        $wsql="";
        if(!empty($date1)){
            if($datetimestamp){$date1=$date1->getTimestamp();}
            else {$date1="'".$date1->format('Y-m-d H:i:s')."'";}
             $wsql=" AND $dbcolumn $operator $date1 ";
        
          }
          if(!empty($date2)){
            if($datetimestamp){$date2=$date2->getTimestamp();}
            else {$date2="'".$date2->format('Y-m-d H:i:s')."'";}
            $wsql.=" AND $dbcolumn <= $date2 ";
          }
         if(empty($wsql) && empty($operator)){
            $valueperiodsingle=$this->getUtildata()->getVaueOfArray($paramfilter, $keyfilter.'now');
            $currentvalue=$this->getUtildata()->getVaueOfArray($paramfilter, $keyfilter);
            if(!empty($currentvalue) && !empty($valueperiodsingle)){
               $date1=$currentvalue;
               if($datetimestamp){$date1=$date1->getTimestamp();}
               else {$date1="'".$date1->format('Y-m-d H:i:s')."'";}
               $wsql=" AND $dbcolumn >= $date1 ";
            }
          }
        
      
          return $wsql;
     }

     function number($paramfilter,$keyfilter,$dbcolumn,$param=array()) {
      $operator=$this->getUtildata()->getVaueOfArray($paramfilter, '::'.$keyfilter.'operator');    
      $v1=$this->getUtildata()->getVaueOfArray($paramfilter, $keyfilter.'1');
      $v2=$this->getUtildata()->getVaueOfArray($paramfilter, $keyfilter.'2');
	  $originaloperator=$this->getUtildata()->getVaueOfArray($paramfilter, $keyfilter.'_badiunumberoperator');

	  if($v1==""){return null;}
	  if($v1==null){return null;}
	  if(!is_numeric($v1)){return null;}
	  $type=$this->getUtildata()->getVaueOfArray($param,'type');
	  if(empty($type)){$type='default';}
      $wsql=" ";
	  if($type=='default'){
		   if(is_numeric($v1)){
				$wsql=" AND $dbcolumn $operator $v1 ";
           }
		 if(is_numeric($v2)){
			$wsql.=" AND $dbcolumn <= $v2 ";
         }
		 return $wsql;
	  }else if($type=='date'){
		  $timestamp=$this->getUtildata()->getVaueOfArray($param,'timestamp');
		  $unittime=$this->getUtildata()->getVaueOfArray($param,'unittime');
		  $withoutrecord=$this->getUtildata()->getVaueOfArray($param,'withoutrecord');
		  $doperator=null;
		  $operator=trim($operator);
		
		  if($operator=='>' || $operator=='>=' || $operator=='<' || $operator=='<=' ){$doperator=DOMAINTABLE::$OPERATOR_PAST_TEXT_FROM;}
		  else if($operator=='='){$doperator=DOMAINTABLE::$OPERATOR_PAST_TEXT_ON;}
		  else if($operator=='='){$doperator=DOMAINTABLE::$OPERATOR_PAST_TEXT_ON;}
		 $doperator=DOMAINTABLE::$OPERATOR_PAST_TEXT_ON; 
		  $number=$v1;
		  
		  $periodutil=$this->getContainer()->get('badiu.system.core.lib.date.periodutil');
		  $fparam=array('unittime'=>$unittime,'operator'=>$doperator,'number'=>$number);
		  $pdate=$periodutil->makeDateByOperator($fparam);
		  
		
		  //$timestamp=0;
		  $date1=$pdate->date1;
		  $date2=$pdate->date2;
		  if(!empty($date1)){
			  if($timestamp){$date1=$date1->getTimestamp();}
			  else {$date1="'".$date1->format('Y-m-d H:i:s')."'";}
		  } 
		  if(!empty($date2)){
			  if($timestamp){$date2=$date2->getTimestamp();}
			  else {$date2="'".$date2->format('Y-m-d H:i:s')."'";}
		  } 
		  
		    //lastdaystart
		  $fparam=array('unittime'=>$unittime,'operator'=>$doperator,'number'=>$number-1);
		  $lastdaydatestart=$periodutil->makeDateByOperator($fparam);
		  $lastdaydatestartdate1=$lastdaydatestart->date1;
		  $lastdaydatestartdate2=$lastdaydatestart->date2;
		  if(!empty($lastdaydatestartdate1)){
			  if($timestamp){$lastdaydatestartdate1=$lastdaydatestartdate1->getTimestamp();}
			  else {$lastdaydatestartdate1="'".$lastdaydatestartdate1->format('Y-m-d H:i:s')."'";}
		  } 
		  if(!empty($lastdaydatestartdate2)){
			  if($timestamp){$lastdaydatestartdate2=$lastdaydatestartdate2->getTimestamp();}
			  else {$lastdaydatestartdate2="'".$lastdaydatestartdate2->format('Y-m-d H:i:s')."'";}
		  } 
		 
		//lastdayend
		  $fparam=array('unittime'=>$unittime,'operator'=>$doperator,'number'=>1);
		  $lastdaydateend=$periodutil->makeDateByOperator($fparam);
		  $lastdaydateenddate1=$lastdaydateend->date1;
		  $lastdaydateenddate2=$lastdaydateend->date2;
		  if(!empty($lastdaydateenddate1)){
			  if($timestamp){$lastdaydateenddate1=$lastdaydateenddate1->getTimestamp();}
			  else {$lastdaydateenddate1="'".$lastdaydateenddate1->format('Y-m-d H:i:s')."'";}
		  } 
		  if(!empty($lastdaydateenddate2)){
			  if($timestamp){$lastdaydateenddate2=$lastdaydateenddate2->getTimestamp();}
			  else {$lastdaydateenddate2="'".$lastdaydateenddate2->format('Y-m-d H:i:s')."'";}
		  } 		 
		  if($withoutrecord){
			  $wsqlczero="";
			  if($timestamp){$wsqlczero=" OR $dbcolumn =0 ";}
			  if($operator=='>'){$wsql=" AND ( $dbcolumn < $date1 OR $dbcolumn IS NULL $wsqlczero )";}
			  if($operator=='>=' && $originaloperator!='between'){$wsql=" AND ( $dbcolumn < $date2 OR $dbcolumn IS NULL $wsqlczero )";}
			  
			  if($operator=='<'){$wsql=" AND ( $dbcolumn > $date2 AND  $dbcolumn  < $lastdaydateenddate2 )";}
			  if($operator=='<='){$wsql=" AND ( $dbcolumn > $date1 AND $dbcolumn  < $lastdaydateenddate2 )";}
			  
			   if($operator=='='){$wsql=" AND  $dbcolumn >= $date1 AND $dbcolumn <= $date2  ";}
			   if($operator=='!='){$wsql=" AND  NOT ( $dbcolumn >= $date1 AND $dbcolumn <= $date2  ) ";}
			   
			   if(is_numeric($v2) && $originaloperator=='between'){
				  //lastvalue
					$fparam=array('unittime'=>$unittime,'operator'=>$doperator,'number'=>$v2);
					$lastvaluedateend=$periodutil->makeDateByOperator($fparam);
					$lastvaluedateenddate1=$lastvaluedateend->date1;
					$lastvaluedateenddate2=$lastvaluedateend->date2;
					if(!empty($lastvaluedateenddate1)){
						if($timestamp){$lastvaluedateenddate1=$lastvaluedateenddate1->getTimestamp();}
						else {$lastvaluedateenddate1="'".$lastvaluedateenddate1->format('Y-m-d H:i:s')."'";}
					} 
					if(!empty($lastvaluedateenddate2)){
						if($timestamp){$lastvaluedateenddate2=$lastvaluedateenddate2->getTimestamp();}
						else {$lastvaluedateenddate2="'".$lastvaluedateenddate2->format('Y-m-d H:i:s')."'";}
					}
				   $wsql=" AND $dbcolumn >= $lastvaluedateenddate1 AND $dbcolumn <= $date2  ";
			
			   }
		  }
		
		 return $wsql;
		  
	  }
      
     
        return $wsql;
   }
     function like($paramfilter,$keyfilter,$dbcolumn) {
         $wsql="";
         $value=$this->getUtildata()->getVaueOfArray($paramfilter, $keyfilter);
         if($value !=null && $value !=""){
            $value=strtolower($value);
            $wsql=" AND $dbcolumn LIKE '%".$value."%'";
         }
        return  $wsql;
     }
     function equal($paramfilter,$keyfilter,$dbcolumn,$number=true,$emptychecknull=false) {
        $wsql="";
        $value=$this->getUtildata()->getVaueOfArray($paramfilter, $keyfilter);
        if($value !=null && $value !=""){
            if(!$number){$value="'".$value."'";}
           $wsql=" AND $dbcolumn = $value ";
		   
		   if($number && $emptychecknull && ($value=='0' || $value==0) ){
			 $wsql=" AND ($dbcolumn = $value OR $dbcolumn IS NULL ) "; 
		   }
        }
       return  $wsql;
    } 
	
	public function listFilterWord($param) {
		$query= $this->getUtildata()->getVaueOfArray($param,'query');
		$column= $this->getUtildata()->getVaueOfArray($param,'column');
		$operator= $this->getUtildata()->getVaueOfArray($param,'operator');
		$sizeshortwordtoremove= $this->getUtildata()->getVaueOfArray($param,'sizeshortwordtoremove');
		if(empty($sizeshortwordtoremove)){$sizeshortwordtoremove=3;}
		if(empty($operator)){$operator= " AND ";}
		if(empty($query)){return "";}
		$query=trim($query);
		$query = preg_replace('/ +/', ' ', $query);
		$query = preg_replace("/\b\w{1,$sizeshortwordtoremove}\b/", '', $query);
		$query = preg_replace('/ +/', ' ', trim($query));
		$list = explode(' ', $query);
		if(!is_array($list)){return "";}
		$csql="";
		$cont=0;
		foreach ($list as $item) {
			$item="'%".$item."%'";
			$sep=" ";
			if($cont > 0 ){$sep=$operator;}
			$csql.="$sep $column LIKE $item ";
			$cont++;
		}
		return $csql;
		
	}
}
