<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Description of MakeSelect
 *
 * @author lino
 */
class DbPagination {
    
   
      
     function __construct() {
      
         }
    
    public function getSqlWithLimit($dbtype,$sql,$x=0,$y=10) {
        if($x===NULL) {$x=0;}
        if($y===NULL || $y==0) {$y=10;}
         
        if($dbtype=='mysql'){
            $limit= " LIMIT $x ,$y ";
            $sql.=$limit;
        } else  if($dbtype=='pgsql'){
            $limit= " LIMIT $y OFFSET $x ";
            $sql.=$limit;
        }	
        else  if($dbtype=='mssql'){
           $sql=$this->mssqlCommand($sql,$x,$y);
         }
        
  	return $sql;
       }
      
       public function mssqlCommand($sql,$x,$y) {
           
            //$cmssql=" SELECT TOP (2) * FROM (SELECT  DISTINCT row_number() OVER (ORDER BY id ) AS row_number, id,email FROM [Moodle].VW_Moodle_InfoOperador WHERE  id> 200 ) TABLEROWSEQ WHERE   row_number > 50";
            $orderby=" ORDER BY id ";
            if($this->hasOrderBy($sql)){
                $orderby=$this->getOrderBy($sql);
            }
            $rownumber= " row_number() ";
            $distinct= $this->hasDistinct($sql);
            if($distinct){
                    $distinct="  DISTINCT  ";
                    $rownumber= " RANK() ";
                    
            }
            else {$distinct=" ";}
            
            $partilsql=$this->getMssqlPartialSql($sql);
            $cmssql=" SELECT TOP ($y) * FROM (SELECT $distinct $rownumber OVER ($orderby ) AS row_number, $partilsql ) TABLEROWSEQ WHERE   row_number > $x ";
      
            $sql=$cmssql;
            return $sql;
       }
       
       public function hasDistinct($sql) {
           $result =false;
             if(strpos(strtoupper($sql),' DISTINCT ')>0){
                 $result =true;
             }
           return $result;
       }

       
        public function hasOrderBy($sql) {
           $result =false;
             if(strpos(strtoupper($sql),' ORDER ')>0){
                 $result =true;
             }
             
           return $result;
       }

      
     public function getOrderBy($sql) {
           $pos = strpos(strtoupper($sql), ' ORDER ');
           $result = substr($sql, $pos, strlen($sql));
           return $result;
       }

         public function getMssqlPartialSql($sql) {
              if($this->hasDistinct($sql)){
                $sql=str_replace(" distinct "," ",strtolower($sql));                  
              }
              $sql=" ".$sql;
              $sql=str_replace(" select "," ",strtolower($sql));
              $result=$sql;
              if($this->hasOrderBy($sql)){
                  $pos = strpos(strtoupper($sql), ' ORDER ');
                  $result = substr($sql,0, $pos);
              }
            
           return $result;
       }


}
