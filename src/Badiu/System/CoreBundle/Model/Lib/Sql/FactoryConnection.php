<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Description of MakeSelect
 *
 * @author lino
 */
class FactoryConnection {
    
   /**
     * @var Container
     */
    private $container;
   
     /**
     * @var EntityManager
     */
    private $em; 
    
    /**
     * @var array
     */
    private $config;
    /**
     * @var string
     */
    private $dbapp;
    
     function __construct(Container $container) {
                $this->container=$container;
                $db ="default";//$this->container->get('badiu.system.access.session')->get()->getDbapp(); 
                $this->em=$this->container->get('doctrine')->getEntityManager($db); 
                $this->config=array();
      }
    
    public function get($config=null,$dbapp=null) {
            if(empty($config)){
                $config=$this->getConfig();
            }
             if(empty($dbapp)){
                $dbapp=$this->getDbapp();
            }
             if(!empty($dbapp)){
                 $this->em=$this->container->get('doctrine')->getEntityManager($dbapp); 
             }
             
           $em = \Doctrine\ORM\EntityManager::create($config, $this->getEm()->getConfiguration(), $this->getEm()->getEventManager());
           $connection=$em->getConnection();
           return $connection;
       }
      
      
    
       public function getContainer() {
           return $this->container;
       }

       public function setContainer(Container $container) {
           $this->container = $container;
       }
       
       public function getEm() {
           return $this->em;
       }

       public function setEm(EntityManager $em) {
           $this->em = $em;
       }
       
       public function getConfig() {
           return $this->config;
       }

       public function setConfig($config) {
           $this->config = $config;
       }

       public function getDbapp() {
           return $this->dbapp;
       }

       public function setDbapp($dbapp) {
           $this->dbapp = $dbapp;
       }



}
