<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;


/**
 *
 * @author lino
 */
class FactorySql{
    
  
     /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $data;

     /**
     * @var string
     */
    private $table;

	  /**
     * @var string
     */
     private $pk;

	
      function __construct(Container $container) {
                $this->container=$container;
               
      }
    
       public function insert() {
			$columns="";
			$values="";
			$cont=0;
            foreach ($this->data  as $key => $value) {
				if($cont==0){
					$columns=$key;
					$values=$value;
				}else{
					$columns.=",".$key;
					$values.=",".$value;
				}
				$cont++;
			}
			$sql="INSERT INTO ($columns) VALUES ($values) ";
			return $sql;
       }
     public function update() {
            $keyvalue="";
			$cont=0;
            foreach ($this->data  as $key => $value) {
				if($cont==0){
					$keyvalue="$key= $value";
					$values=$value;
				}else{
					$keyvalue.=", $key= $value";
				}
				$cont++;
			}
			$sql="UPDATE  $this->table SET $keyvalue WHERE $this->pk=$this->data[$this->pk] ";
			return $sql;
       }
       public function delete() {
           $sql="DELETE FROM $this->table WHERE $this->pk=$this->data[$this->pk] ";
            return $sql;
       }
      
		public function existRecord() {
			$sql="SELECT COUNT($this->pk) AS countrecord FROM $this->table WHERE $this->pk=$this->data[$this->pk] ";
            return $sql;
       }
       public function getContainer() {
           return $this->container;
       }

       public function setContainer(Container $container) {
           $this->container = $container;
       }
       
      public function getData() {
           return $this->data;
       }

       public function setData($data) {
           $this->data= $data;
       }

        public function getTable() {
           return $this->table;
       }

       public function setTable($table) {
           $this->table= $table;
       }


       public function getPk() {
           return $this->pk;
       }

       public function setPk($pk) {
           $this->pk= $pk;
       }
	   
}
