<?php

namespace Badiu\System\CoreBundle\Model\Lib\Sql;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 *
 * @author lino
 */
class FactoryQuery extends FactoryConnection {

    /**
     * @var Container
     */
    function __construct(Container $container) {
        $this->container = $container;
    }

    //http://fabrica.badiu.com.br/~colaborador7/badiunet/web/app_dev.php/system/service/process?_service=badiu.system.core.lib.sql.factoryquery
    private $container;
    //private $sql;
    /**
     * @var string
     */
    private $datasource = 'dbdoctrine'; // dbdoctrine|servicesql

    /**
     * @var string
     */
    private $dbtype = 'mysql'; // msysql |postgresql | mssql |oracle  //for native sql 

    /**
     * @var string
     */
    private $datetype = 'timestamp'; // date | timestamp  //for native sql 

         /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
	private $systemdata; 
    public function makeSqlSearch($sql, $param, $datasource = 'dbdoctrine') {
        // echo $sql."<hr>";
       
        if (is_object($param)) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            $param = $serializer->serialize($param, 'json');
            $param = json_decode($param, true);
        }
        
        //echo "A<BR>";
        //print_r($param);exit;
        $param = $this->formCastChangeParam($param);
        
        $this->datasource = $datasource;
        $sql = $this->addSqlTxtSpace($sql);
        $sql = $this->serviceChangeSql($param, $sql);
        $sql = $this->serviceChangeSql($param, $sql); //it is necessary to call again if @filter is inside service
        $sql = $this->serviceChangeSql($param, $sql);
      
        //  echo "<hr>";
        //  print_r($param);
        $sql = $this->formCastChangeSql($param, $sql);
       
        

        //deprecated
        if ($this->datasource == 'servicesql') {
            $this->cleanDeprecatedExpressionInServicesql($sql);
        }
        $hasexpre = TRUE;


        $cont = 0;
        $sql = $this->changeEntityExpression($sql);
       
        while ($hasexpre) {

            $lastposition = strripos($sql, ":");
            if ($lastposition === false) {
                $hasexpre = FALSE;
                break;
            }
            $expression = $this->getExpressionKey($sql, $lastposition);
            $expression = $this->cleanEspecialCharacter($expression);
           
            /*   echo "<pre>"; 
              echo "<BR> $expression -- $sql";
              echo "</pre>" ;
              echo "<BR><BR>-----------------------------------------------------"; */
            $sql = $this->changeExpression($sql, $expression, $lastposition, $param);
           
            $cont++;
            // if($cont==10){echo "-----------------";exit;}
            if ($cont == 10000) {
                break;
            }
        }


        $sql = $this->cleanSqlForExec($sql);
        $sql = $this->removeWhere($sql);
       
        //applay to nativesql
        if ($this->datasource == 'servicesql') {
            $sql = $this->changeExpressionToValueForNativeSql($sql, $param);
        }
        //echo $cont;
       /*echo "<pre>";
         print_r($param);
         echo "</pre> $sql"; exit;*/
        //echo "<br /><br /><br /><br /><hr>" . $sql; 
      
        return $sql;
    }

    public function makeParamFilter($query, $sql, $param) {
        
        if (is_object($param)) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            $param = $serializer->serialize($param, 'json');
            $param = json_decode($param, true);
        }
        
        $param = $this->formCastChangeParam($param);
     
        //$sql=$this->formCastChangeSql($param,$sql);
       
        foreach ($param as $key => $value) {
            //echo "$key --";
            // print_r($value);

            $hasexpression = FALSE;
            $pos = stripos($sql, ":$key");
            if ($pos !== false) {
                $hasexpression = TRUE;
            }
            // $value=trim($value);
            $hasvalue = $this->hasValue($value);

            if ($hasvalue && $hasexpression) {

                $operator = $this->getOperator($key, $sql);
                //    echo "<br>key: $key - $operator";
                $value = $this->changeValueByOperator($operator, $value);
                $query->setParameter($key, $value);
            }
        }
         
        //echo $query->getSql()."<hr>";
        return $query;
    }

    public function getExpressionKey($textsql, $position) {
        $expression = "";

        //find next space
        $pos = stripos($textsql, " ", $position);
        if ($pos !== false) {
            $posend = $pos - $position;
            $expression = substr($textsql, $position + 1, $posend - 1);
            $expression = $this->cleanEspecialCharacter($expression);
            return $expression;
        }



        //remove ) on the end of expression
        $pos = stripos($textsql, ")", $position);
        if ($pos !== false) {
            $posend = $pos - $position;
            $expression = substr($textsql, $position + 1, $posend - 1);
            $expression = $this->cleanEspecialCharacter($expression);
            return $expression;
        }



        $pos = stripos($textsql, ")", $position);
        if ($pos !== false) {
            $posend = $pos - $position;
            $expression = substr($textsql, $position + 1, $posend - 1);
            $expression = $this->cleanEspecialCharacter($expression);
            return $expression;
        }//find next )
        $pos = stripos($textsql, ")", $position);
        if ($pos !== false) {
            $posend = $pos - $position;
            $expression = substr($textsql, $position + 1, $posend - 1);
            $expression = $this->cleanEspecialCharacter($expression);
            return $expression;
        }

        if ($this->datasource == 'servicesql') {
            //for servicesql remove %' on the end of expression
            $pos = stripos($textsql, "%'", $position);
            if ($pos !== false) {
                $posend = $pos - $position;
                $expression = substr($textsql, $position + 1, $posend - 1);
                $expression = $this->cleanEspecialCharacter($expression);
                return $expression;
            }
            //for servicesql remove ' on the end of expression
            $pos = stripos($textsql, "'", $position);
            if ($pos !== false) {
                $posend = $pos - $position;
                $expression = substr($textsql, $position + 1, $posend - 1);
                $expression = $this->cleanEspecialCharacter($expression);
                return $expression;
            }
        }


        //
        //expressão has not next text is end
        $expression = substr($textsql, $position + 1);
        $expression = $this->cleanEspecialCharacter($expression);
        //find ) on the end key
        return $expression;
    }

    public function changeEntityExpression($textsql) {
        //replace expression

        $textsql = str_replace(" :", " |;;;;|", $textsql);
        $textsql = str_replace("(:", "(|;;;;|", $textsql);
        $textsql = str_replace("=:", "= |;;;;|", $textsql);
        $textsql = str_replace(">:", "> |;;;;|", $textsql);
        $textsql = str_replace("<:", "< |;;;;|", $textsql);
        $textsql = str_replace("LIKE:", "LIKE |;;;;|", $textsql);
        $textsql = str_replace("'%:", "'%|;;;;|", $textsql);
        $textsql = str_replace("':", "'|;;;;|", $textsql);



        //replace entity
        $textsql = str_replace(":", "<;;;;>", $textsql);

        //back tor original expression
        $textsql = str_replace("|;;;;|", ":", $textsql);
        return $textsql;
    }

    public function changeExpression($textsql, $expression, $lastposition, $param) {
        $ntsq = $textsql;

        //expression with param
        if (array_key_exists($expression, $param) && isset($param[$expression]) && $this->hasValue($param[$expression])) {
            //replace : to <;;;;>
            $ntsq = substr_replace($textsql, "<;;;;>", $lastposition, 1);
        } else {
            $ntsq = $this->removeExpression($expression, $ntsq);
        }


        return $ntsq;
    }

    public function removeExpression($epression, $textsql) {
        $ntsql = $textsql;

        //   echo "\n\n$textsql";
        $lsql = preg_split("/:$epression /i", $textsql);

        $tsql1 = "";
        $tsql2 = "";
        if (isset($lsql[0])) {
            $tsql1 = $lsql[0];
        }
        if (isset($lsql[1])) {
            $tsql2 = $lsql[1];
        }

        //  echo "<br>$epression <br><br>sql $textsql <br><br> tsq: $tsql2 <br>--------------------------------------------------------------------<br><br><br>";
        //remove ) after
        /*  $tsql2=trim($tsql2);
          $pos=stripos($tsql2,")");
          $posin=stripos($tsql2," IN ");
          $posclose=stripos($tsql2,"(");



          //  echo "<br>$tsql2\n posin: $posin  posclose: $posclose";
          if($pos!==false && $pos==0 && $posin!==false && $posclose!=false && $posin > $posclose ){$tsql2=substr($tsql2,1,strlen($tsql2));}

          else{
          echo "====================";
          //see last position in all string for exemple IN (:expression) to remove last )
          $posexp=strripos($textsql,$epression);
          $posstart=strripos($textsql,"(");
          $posclose=strripos($textsql,")");
          $posin=strripos($textsql," IN ");
          $posselect=strripos($textsql,"SELECT ");

          // echo  "\n\n$textsql";
          echo  "\n\n posexp: $posexp posstart: $posstart posclose: $posclose posin: $posin posselect: $posselect";
          if($posin!==false
          && $posstart!==false
          && $posclose!==false
          && $posexp!=false
          && $posin < $posexp
          && $posstart < $posclose
          && $posclose > $posexp
          && $posselect < $posin){$tsql2=substr($tsql2,2,strlen($tsql2));}


          } */

        //remove ) if has IN(:expression)
        $tsql2 = trim($tsql2);
        $pos = stripos($tsql2, ")");
        if ($pos !== false && $pos == 0) {
            $tsql2 = substr($tsql2, 1, strlen($tsql2));
            //count amout of ( and ) if is different remove first 
            $sqlfull = $tsql1 . " " . $tsql2;
            $countcharx = substr_count($sqlfull, ')');
            $countchary = substr_count($sqlfull, '(');
            // echo "<br> countcharx:$countcharx  countchary:$countchary 2 sql: $tsql2";
            if ($countcharx != $countchary) {

                $tsql2 = substr($tsql2, 1, strlen($tsql2));
            } else {
                $tsql2 = ") " . $tsql2;
            }//dont remove
        }

        if ($this->datasource == 'servicesql') {
            //  echo "<br>l1: $tsql2 ";
            //remove %' after
            $pos = stripos($tsql2, "%'");
            if ($pos !== false && $pos == 0) {
                $tsql2 = substr($tsql2, 2, strlen($tsql2));
            }


            //remove ' after
            /*    $pos = stripos($tsql2, "'");
              if ($pos !== false && $pos==0) {
              $tsql2 = substr($tsql2, 1, strlen($tsql2));
              } */
            //   echo "<br>l2: $tsql2 ";
        }

        $positionhwere = strripos($tsql1, " WHERE ");
        //find and

        $lastoperator = strripos($tsql1, " AND ");
        // echo "\npositionhwere: $positionhwere  lastoperator: $lastoperator";
        if ($lastoperator !== false) {


            if ($positionhwere < $lastoperator) {
                $ntsql = substr($tsql1, 0, $lastoperator);
            } else {
                $ntsql = substr($tsql1, 0, $positionhwere);
            }

            $ntsql = $ntsql . " " . $tsql2;


            return $ntsql;
        }
        //find or
        $lastoperator = strripos($tsql1, " OR ");
        if ($lastoperator !== false) {

            if ($positionhwere < $lastoperator) {
                $ntsql = substr($tsql1, 0, $lastoperator);
            } else {
                $ntsql = substr($tsql1, 0, $positionhwere);
            }
            $ntsql = $ntsql . " " . $tsql2;
            return $ntsql;
        }
        //find where
        $lastoperator = strripos($tsql1, " WHERE ");
        if ($lastoperator !== false) {

            $ntsql = substr($tsql1, 0, $lastoperator + 7);
            $ntsql = $ntsql . " " . $tsql2;
            return $ntsql;
        }
        return $ntsql;
    }

    public function removeWhere($textsql) {
        $ntsql = $textsql;

        $lsql = preg_split("/where/i", $textsql);
        //print_r($lsql);
        $tsql1 = "";
        $tsql2 = "";
        if (isset($lsql[0])) {
            $tsql1 = $lsql[0];
        }
        if (isset($lsql[1])) {
            $tsql2 = $lsql[1];
        }

        $tsql2 = trim($tsql2);
        if (empty($tsql2)) {
            $ntsql = $tsql1;
        } else {
            //remove AND operator after 
            $pos = stripos($tsql2, "AND ");
            if ($pos !== false && $pos == 0) {

                $tsql2 = substr($tsql2, 3, strlen($tsql2));
                $ntsql = $tsql1 . " WHERE " . $tsql2;
                return $ntsql;
            }

            //remove OR operator after
            $pos = stripos($tsql2, "OR ");
            if ($pos !== false && $pos == 0) {

                $tsql2 = substr($tsql2, 2, strlen($tsql2));
                $ntsql = $tsql1 . " WHERE " . $tsql2;
                return $ntsql;
            }

            //remove where if has GROUP BY
            $pos = stripos($tsql2, "GROUP ");
            if ($pos !== false && $pos == 0) {
                $ntsql = $tsql1 . " " . $tsql2;
                return $ntsql;
            }

            //remove where if has LIMIT
            $pos = stripos($tsql2, "LIMIT ");
            if ($pos !== false && $pos == 0) {
                $ntsql = $tsql1 . " " . $tsql2;
                return $ntsql;
            }

            //remove where if has ORDER BY
            $pos = stripos($tsql2, "ORDER ");
            if ($pos !== false && $pos == 0) {
                $ntsql = $tsql1 . " " . $tsql2;
                return $ntsql;
            }
        }
        return $ntsql;
    }

    public function cleanSqlForExec($textsql) {
        $sql = str_replace("<;;;;>", ":", $textsql);
        return $sql;
    }

    public function cleanDeprecatedExpressionInServicesql($textsql) {
        $sql = str_replace("::", ":", $textsql);
        return $sql;
    }

    public function cleanEspecialCharacter($textsql) {
        $sql = str_replace(")", "", $textsql);
        $sql = str_replace("%'", "", $sql);
        $sql = str_replace("%", "", $sql);
        $sql = str_replace("'", "", $sql);
        return $sql;
    }

    public function hasValue($value) {
        $type = $this->valueType($value);
        if ($type == "string") {
            if (empty($value)) {
                return false;
            }
            return true;
        }
        if ($type == "integer") {
            if ($value == 0) {
                return true;
            }
            if (empty($value)) {
                return false;
            }
            return true;
        }
        if ($type == "numeric") {
            if ($value == 0) {
                return true;
            }
            if (empty($value)) {
                return false;
            }
            return true;
        }
        if ($type == "NULL") {
            return false;
        }


        if ($type == "array") {
            $cont = 0;
            foreach ($value as $v) {
                $cont++;
            }
            if ($cont == 0) {
                return false;
            }
            return true;
        }
        if ($type == "object") {
            return true;
        }

        return true;
    }

    function valueType($var) {
        if (is_array($var))
            return "array";
        if (is_bool($var))
            return "boolean";
        if (is_float($var))
            return "float";
        if (is_int($var))
            return "integer";
        if (is_null($var))
            return "NULL";
        if (is_numeric($var))
            return "numeric";
        if (is_object($var))
            return "object";
        if (is_resource($var))
            return "resource";
        if (is_string($var))
            return "string";
        return "unknown type";
    }

    public function changeExpressionToValueForNativeSql($sql, $param) {
        $sql = $sql . ' '; //add space on the end of sql
        
        foreach ($param as $key => $value) {
            $value = $this->changeValueToNativeSql($value);
            $expression = ":$key ";
            $hasexpression = FALSE;
            $pos = stripos($sql, $expression);
            if ($pos !== false) {
                $hasexpression = TRUE;
            }
            $hasvalue = $this->hasValue($value);
            //echo "<br>$expression - $value";
            $operator = $this->getOperator($key, $sql);
            $operator = strtolower($operator);
            if ($operator == "like") {
                $value = strtolower($value);
                $value = "%".addslashes($value)."%";
            }
            if ($hasvalue && $hasexpression) {
                if (!ctype_digit($value)) {
                    $value = "'" . addslashes($value) . "'";
                }
                $sql = str_replace($expression, $value, $sql);
                //  echo "<br>$sql"; 
            }
        }
        return $sql;
    }

    public function changeValueToNativeSql($value) {
        $type = $this->valueType($value);

        if ($type == 'object' && is_a($value, 'DateTime')) {
            if ($this->datetype == 'timestamp') {
                $value = $value->getTimestamp();
            }
        }
        return $value;
    }

    public function changeValueByOperator($operator, $value) {

        if (empty($operator))
            return $value;
        $operator = strtolower($operator);

        if ($operator == "like") {
            $value = "%$value%";
        }

        return $value;
    }

    public function getOperator($epression, $sql) {
        $sql=$sql. " ";
        $lsql = preg_split("/:$epression /i", $sql);
        $tsql1 = "";
        if (isset($lsql[0])) {
            $tsql1 = $lsql[0];
        }

        //remove ( if exit expression is IN (:expression)
        $tsql1 = str_replace("(", "", $tsql1);
        $tsql1 = trim($tsql1);


        $positionlastspace = strripos($tsql1, " ");
        $operator = substr($tsql1, $positionlastspace, strlen($tsql1));
        $operator = trim($operator);
        return $operator;
    }

    public function setDataForExpressionExceptforsqlin($param, $sql, $expression) {
        $nsql = $sql;
        //exit word exceptforsqlin
        $pos = stripos($expression, "exceptforsqlin");
        if ($pos === false)
            return $sql;

        //get data
        $data = $this->getDataByExpressionExceptforsqlin($param, $expression);
        //format data with comma
        //replace expression
        //:badiu.ams.offer.disciplinemdlcoursepicklistavailableexceptforsqlin
        return $nsql;
    }

    public function getDataByExpressionExceptforsqlin($param, $expression) {
        $this->getSearch()->getKeymanger()->setBaseKey($expression);
        $result = $this->getSearch()->searchList($param);
        return $result;
    }
//change compenentes class to core form lib
    public function formCastChangeParam($param) {
        //$param= clone $paramfilter;
        
        // $filterdate = $this->container->get('badiu.system.component.form.cast.badiufilterdate'); delete this service
         $filterdate = $this->container->get('badiu.system.core.lib.form.castfilterdate'); 
        $param = $filterdate->changeParam($param);
       
       /* echo "<hr>";
        echo "<pre>";
        print_r($param);
        echo "</pre>";//exit;*/
        $filternumber = $this->container->get('badiu.system.component.form.cast.badiufilternumber');
        $param = $filternumber->changeParam($param);
         
        $badiutimeperiod = $this->container->get('badiu.system.core.lib.form.casttimeperiod');
        $param = $badiutimeperiod->changeParam($param, true);
        
        return $param;
    }


    public function formCastChangeSql($param, $sql) {
        $filterdate = $this->container->get('badiu.system.core.lib.form.castfilterdate');
        $sql = $filterdate->changeSql($param, $sql);

        $filternumber = $this->container->get('badiu.system.component.form.cast.badiufilternumber');
        $sql = $filternumber->changeSql($param, $sql);

        $badiutimeperiod = $this->container->get('badiu.system.core.lib.form.casttimeperiod');
        $sql = $badiutimeperiod->changeSql($param, $sql);
        return $sql;
    }

    public function serviceChangeSql($param, $sql) {
   
        //replace @filter
        $sql=str_replace("@filter"," ",$sql);

        //replace @column
        $sql=str_replace("@column"," ",$sql);

        $exist = true;
   
        $cont = 0;
        $newsql = "";
        $pos = stripos($sql, "|:|");
        if ($pos === false) {
            $exist = false;
        }
         
        if (!$exist) {return $sql;}
            $lkey = explode("|:|", $sql);

            foreach ($lkey as $stxt) {
                $cont++;
                if (($cont % 2) == 0) {
                    $servicefunctionkey = "";
                    $servicefunctionkeyexpression = "";
                    $servicekey = "";
                    $function = "exec";
                    $fparam="";
                    $value = "";
                    
                    $servicefunctionkey = $stxt;
               
                    if (!empty($servicefunctionkey)) {

                        $pos = stripos($servicefunctionkey, "/");
                        if ($pos === false) {
                            $servicekey = $servicefunctionkey;
                        } else {
                            $sfkey = explode("/", $servicefunctionkey);
                            if (isset($sfkey[0])) {
                                $servicekey = $sfkey[0];
                            }
                            if (isset($sfkey[1])) {
                                $function = $sfkey[1];
                            }
                            if (isset($sfkey[2])) {
                                $fparam = $sfkey[2];
                            }
                        }
                     //echo "$servicekey / $function <hr >"; 
                    
                        if ($this->container->has($servicekey)) {
                            $servidedata = $this->container->get($servicekey);
                            if (method_exists($servidedata, 'setSessionhashkey')) {
                                 $servidedata->setSessionhashkey($this->getSessionhashkey());
                             }
                            if (method_exists($servidedata, 'setParam')) {
                                $servidedata->setParam($param);
                            }
                            if (method_exists($servidedata, $function)) {
                                $value = $servidedata->$function($fparam);
                            }
                           
                        }

                       $newsql.=$value;
                    }
                } else {
                    $newsql.=$stxt;
                }
            }
         
        return $newsql;
    }

    public function addSqlTxtSpace($sql) {
        $sql = str_replace(" ", "  ", $sql);
        $sql = str_replace(")", " ) ", $sql);

        return $sql;
    }

    public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }
 	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }  
}
