<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Doctrine\DBAL\Connection;

//http://www.doctrine-project.org/api/dbal/2.5/class-Doctrine.DBAL.Connection.html
//http://www.doctrine-project.org/api/dbal/2.5/class-Doctrine.DBAL.Statement.html
/**
 *
 * @author lino
 */
class FactoryDb extends FactoryConnection{
    
  
     /**
     * @var Connection
     */
    private $connection; 
    
      function __construct(Container $container) {
                 parent::__construct($container);
               
      }
    
       public function insert($sql) {
             $this->startConnection();
            $r=$this->getConnection()->exec($sql); 
            $this->endConnection();
          
           return $r;
       }
       
       public function inserts($listsql) {
             $this->startConnection();
             $result=0;
             foreach ($listsql as $sql) {
                 $result+=$this->getConnection()->exec($sql); 
             }
            $this->endConnection();
           return $result;
       }
      
         public function batchInsert($sql,$params=array()) {
             $this->startConnection();
             $stmt = $this->getConnection()->prepare($sql);
             $stmt->execute($params);
             $result=$stmt->rowCount();
             $this->endConnection();
           return $result;
       }
        
     public function update($sql) {
             $this->startConnection();
            $r=$this->getConnection()->exec($sql); 
            $this->endConnection();
           return $r;
       }
        public function updates($listsql) {
             $this->startConnection();
             $result=0;
             foreach ($listsql as $sql) {
                  $result+=$this->getConnection()->exec($sql); 
             }
            $this->endConnection();
           return $result;
       }
         public function batchUpdate($sql,$params=array()) {
             $this->startConnection();
             $stmt = $this->getConnection()->prepare($sql);
              $stmt->execute($params);
             $result=$stmt->rowCount();
             $this->endConnection();
           return $result;
       }
       public function delete($sql) {
             $this->startConnection();
            $r=$this->getConnection()->exec($sql); 
            $this->endConnection();
           return $r;
       }
        public function getRecord($sql) {
            $this->startConnection();
             $statement = $this->getConnection()->prepare($sql);
             $statement->execute();
             $result = $statement->fetch(); 
            $this->endConnection();
             return $result;
       }
       
       public function getRecords($sql,$x=null,$y=null) {
             $this->startConnection();
             if($x!==null && $y!==null){
                  $dbpagination=$this->getContainer()->get('badiu.system.core.lib.sql.dbpagination');
                  $bdtype=$this->getConfig()['type'];
                  $sql=$dbpagination->getSqlWithLimit($bdtype,$sql,$x,$y);
                 
            }
          //  echo "$sql<br><br>";
            $statement = $this->getConnection()->prepare($sql);
             $statement->execute();
             $result = $statement->fetchAll(); 
             $this->endConnection();
             return $result;
       }
       public function startConnection() {
           if($this->connection==null || $this->connection->isConnected()){
              $this->connection=$this->get();
          }
       }
       public function endConnection() {
         // echo "endconnection invocado<br>";
          // if(!$this->connection->isConnected()){
              $this->connection->close();
             //  echo "endconnection exec<br>";
       // }

       }
      public function getConnection() {
             return $this->connection;
      }

      public function setConnection(Connection $connection) {
          $this->connection = $connection;
      }




}
