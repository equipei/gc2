<?php

namespace Badiu\System\CoreBundle\Model\Lib\Sql;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FactorySqlExpressions extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


  public function filter($param) {
	  $newparam=array();
	  foreach ($param as $key=>$value) {
		   if(!empty($value) && !is_array($value) && !is_object($value)){
			  $pos = stripos($value, "__DATE");
				if ($pos !== false) {
				$nvalue=$this->makeDate($value);
				$newparam[$key]=$nvalue;
				}else{
				  $newparam[$key]=$value;
				} 
		   }else {$newparam[$key]=$value;}
		   
	  }
	  return $newparam;
  }

	
	public function makeDate($expression,$defaultresult=0) {
		$type=$this->getTypeExpression($expression);
		$value=$expression;
		if(empty($value)){return $defaultresult;}
		
		$value=str_replace("__DATE_ADD_START","",$value);
		$value=str_replace("__DATE_ADD_END","",$value);
		$value=str_replace("__DATE_ADD","",$value);
		
		$value=str_replace("__DATET_ADD_START","",$value);
		$value=str_replace("__DATET_ADD_END","",$value);
		$value=str_replace("__DATET_ADD","",$value);
		
        $value=strtolower($value);
        $value=trim($value);
			
		$now =new \DateTime();
        $now->modify($value);
		
		$cyear=$now->format('Y');
		$cmonth = $now->format('n');
		$cthour = $now->format('H');
        $cminutes = $now->format('i');
		$lastmonthday=$now->format('t');
		//if start
		
		if($type->timereference=='start'){
			if($type->unit=='minutes'){
				$now->setTime ($cthour,$cminutes,0);
			}else if($type->unit=='hour'){
				$now->setTime ($cthour,0,0);
			}else if($type->unit=='day'){
				$now->setTime (0,0,0);
			}else if($type->unit=='month'){
				$now->setDate($cyear,$cmonth, 1);
				$now->setTime (0,0,0);
			}else if($type->unit=='year'){
				$now->setDate($cyear,1, 1);
				$now->setTime (0,0,0);
			}
		}
		//if end
		else if($type->timereference=='end'){
			if($type->unit=='minutes'){
				$now->setTime ($cthour,$cminutes,59);
			}else if($type->unit=='hour'){
				$now->setTime ($cthour,59,59);
			}else if($type->unit=='day'){
				$now->setTime (23,59,59);
			}else if($type->unit=='month'){
				$now->setDate($cyear,$cmonth,$lastmonthday);
				$now->setTime (23,59,59);
			}else if($type->unit=='year'){
				$now->setDate($cyear,12,$lastmonthday);
				$now->setTime (23,59,59);
			}
		}
		
		//format
		if($type->format=='timestamp'){$now=$now->getTimestamp();}
		if(empty($now)){return $defaultresult;}
		 
		return $now;
	}
	/**
	* return object stdClass with properties:  format | timereference | unit | reference
	*/
	public function getTypeExpression($expression) {
		$value=$expression;
		$type = new \stdClass();
		$type->format='default'; // default | timestamp
		$type->timereference='current'; // current | start | end
		$type->unit='none'; //none | minutes | hour | day | month | year
		$type->reference='current';
		
		 $pos = stripos($value, "__DATE_ADD");
		 if ($pos !== false) {$type->format='default';}
		 
		 $pos = stripos($value, "__DATET_ADD");
		 if ($pos !== false) {$type->format='timestamp';}
		 
		 
		 $pos = stripos($value, "_ADD_START"); 
		 if ($pos !== false) {$type->timereference='start';}
		 
		 $pos = stripos($value, "_ADD_END"); 
		 if ($pos !== false) {$type->timereference='end';}
		 
		 $pos = stripos($value, " minutes"); 
		 if ($pos !== false) {$type->unit='minutes';}
		 
		  $pos = stripos($value, " hour"); 
		 if ($pos !== false) {$type->unit='hour';}

		 		 
		 $pos = stripos($value, " day"); 
		 if ($pos !== false) {$type->unit='day';}
		 
		  $pos = stripos($value, " month"); 
		 if ($pos !== false) {$type->unit='month';}
		 
		$pos = stripos($value, " year"); 
		 if ($pos !== false) {$type->unit='year';}
		 
		
		return $type;
	
	}
}
