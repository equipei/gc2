<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Lib\Sql;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

/**
 *
 * @author lino
 */
class FactorySqlCommand extends BadiuModelLib {

    /**
     * @var array
     */
    private $excludcolumns = array('_id');

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function batchInsert($table, $rows,$withparam=false) {
        $sql =" ";
        $cont=0;
        foreach ($rows as $row) {
            $dvalues=null;
            if($cont==0){
                 $dcolumns = $this->getColumnsToInsert($row);
                  
                 if($withparam){$dvalues = $this->getColumnsKeyToInsert($cont,$row);}
                 else {$dvalues = $this->getValuesToInsert($row);}
                 $sql = "INSERT INTO $table ($dcolumns) VALUES $dvalues  ";
            }else{
                if($withparam){$dvalues = $this->getColumnsKeyToInsert($cont,$row);}
                else {$dvalues = $this->getValuesToInsert($row);}
                
                $kvalues = $this->getColumnsKeyToInsert($cont,$row);
                $sql.=",$dvalues ";
            }
           
            
           
           $cont++;
        }
        //echo $sql;exit;
        return $sql;
    }

    public function makeListSqlInsert($table, $rows) {
        $newlist = array();
        foreach ($rows as $row) {
            $dcolumns = $this->getColumnsToInsert($row);
            $dvalues = $this->getValuesToInsert($row);
            $sql = "INSERT INTO $table ($dcolumns) VALUES ($dvalues) ";
            array_push($newlist, $sql);
        }
        return $newlist;
    }
    private function getColumnsToInsert($row) {
        $contf = 0;
        $txt = "";
        $excludcolumns = $this->initExcludcolumns();
        foreach ($row as $key => $value) {
            if (!array_key_exists($key, $excludcolumns)) {
                if ($contf == 0) {
                    $txt .= $key;
                } else {
                    $txt .= ",$key ";
                }
                $contf++;
            }
        }
        return $txt;
    }
    private function getColumnsKeyToInsert($index,$row) {
        $contf = 0;
        $txt = "";
        $excludcolumns = $this->initExcludcolumns();
        foreach ($row as $key => $value) {
            $keyindex=$key.'_'.$index;
            if (!array_key_exists($key, $excludcolumns)) {
                if ($contf == 0) {
                    $txt .= $keyindex;
                } else {
                    $txt .= ",$keyindex ";
                }
                $contf++;
            }
        }
        $txt="($txt)";
        return $txt;
    }
    
    private function getValuesToInsert($row) {
        $contf = 0;
        $txt = "";
        $excludcolumns = $this->initExcludcolumns();
        foreach ($row as $key => $value) {
            if (!array_key_exists($key, $excludcolumns)) {
                $cvalue = $this->cast($value);
                if ($contf == 0) {
                    $txt .= $cvalue;
                } else {
                    $txt .= ",$cvalue ";
                }
                $contf++;
            }
        }
        $txt="($txt)";
        return $txt;
    }

    private function cast($value) {
        $sqlvalue = $value;
        $type = $this->valueType($value);
        if ($type == 'string') {
              if($sqlvalue!=null){$sqlvalue=trim($sqlvalue);}
             $sqlvalue=addslashes($sqlvalue);
            $sqlvalue = "'" . $sqlvalue . "'";
        }
        else if ($type == 'NULL') {
            $sqlvalue='NULL';
        }
        else if ($type == 'object') {
            $data = $value;
            if (is_object($data) && is_a($data, 'DateTime')) {
                $data = $data->format('Y-m-d H:i:s');
            } else if (is_object($data) && is_a($data, 'MongoDate')) {
                $data = date('Y-m-d H:i:s', $data->sec);
            } else if (is_object($data) && is_a($data, 'MongoDB\BSON\UTCDateTime')) {
                $data = $data->toDateTime();
                $data = $data->format('Y-m-d H:i:s');
            }
            $sqlvalue = $data;
            $sqlvalue = "'" . $sqlvalue . "'";
        }
       if(($sqlvalue==null) || ($sqlvalue=='')){$sqlvalue='NULL';}
      
        return $sqlvalue;
    }

    // the same class has in FactoryQuery. After put in same place
    function valueType($var) {
        if (is_array($var))
            return "array";
        if (is_bool($var))
            return "boolean";
        if (is_float($var))
            return "float";
        if (is_int($var))
            return "integer";
        if (is_null($var))
            return "NULL";
        if (is_numeric($var))
            return "numeric";
        if (is_object($var))
            return "object";
        if (is_resource($var))
            return "resource";
        if (is_string($var))
            return "string";
        return "unknown type";
    }

    private function initExcludcolumns() {
        $list = array();
        foreach ($this->excludcolumns as $value) {
            $list[$value] = $value;
        }
        return $list;
    }

    function getExcludcolumns() {
        return $this->excludcolumns;
    }

    function setExcludcolumns($excludcolumns) {
        $this->excludcolumns = $excludcolumns;
    }

}
