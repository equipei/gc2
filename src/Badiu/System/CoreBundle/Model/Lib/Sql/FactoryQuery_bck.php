<?php


namespace Badiu\System\CoreBundle\Model\Lib\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
/**
 *
 * @author lino
 */
class FactoryQuery extends FactoryConnection{
  
    /**
     * @var Container
     */
    
    function __construct(Container $container) {
                $this->container=$container;
                
      }
      
    private $container;
   /**
      * @var string
     */
     private $datasource='dbdoctrine';// dbdoctrine|servicesql
    public function makeSqlSearch($sql,$param,$datasource='dbdoctrine') {
     if(is_object ($param)){
         $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
         $param = $serializer->serialize($param, 'json');
         $param=json_decode($param, true);
     }
		$this->datasource=$datasource;

         $tsql= $this->textSql($sql);
       $select=$tsql->selectfrom;
       $where= " ";
       $filter =" ";
       $grouporder=" " ;
       if(!empty($tsql->where)){
             $grouporder=$tsql->grouporder;
            
             $cont=0;
             foreach ($tsql->filterexpr  as $key => $value) {
					//echo "key: $key -- value: $value    <br>";
                     
					 $posbexp=strripos($key, "__badiu_no_expression_");
					 if($posbexp !==false ){
						$filter.=  $value;
					 }
					 else if (array_key_exists($key,$param) && isset($param[$key]) && !empty($param[$key])){
                       $cont++;
					   if($datasource=='dbdoctrine'){
							$filter.=  $value;
					   }
					   else {
							$value=$this->replaceExpressionToValue($key,$value,$param[$key]);
							$filter.=  $value;
					   }
                         
                     }
             }
           if($cont>0){
               $filter=$this->removeOperatorStart($filter) ;
               $where= " WHERE ";
               
           }
       }
        $csql="  $select $where $filter $grouporder";
               // echo  $csql;
				
              //  exit;
         return $csql;
        //return $sql;
    }
    
	public function makeParamFilter($query,$sql,$param) {
        if(is_object ($param)){
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            $param = $serializer->serialize($param, 'json');
            $param=json_decode($param, true);
        }

       
         $tsql= $this->textSql($sql);


         if(!empty($tsql->where)){
           
            foreach ($tsql->filterexpr  as $key => $value) {

                      if (array_key_exists($key,$param) && isset($param[$key]) && !empty($param[$key])){
                         // echo "key: $param[$key]  - $value <br>";
                          $dvalue=$this->valueOperator($param[$key], $value);
                         // echo "key: $dvalue  - $value <br>";
                       $query->setParameter($key, $dvalue);
                        
                     }
             }
          
       }

        return $query;
    }
  
  
  public function textSql($sql) {
       
        //$sql=strtolower($sql);
        $tsql=new \stdClass();
        $tsql->selectfrom="";
        $tsql->where="";
        $tsql->grouporder="";
        $tsql->filter="";
        $tsql->filterexpr=array();
        $tsql->filternoexpr="";
        
        
        $filter=array();
       //  FROM GROUP BY  ORDER BY 
       
       //get $selectfrom e $where
        $pos=stripos($sql, " where ");
        if($pos=== false){
             $tsql->selectfrom=$sql;
        }else{
           $lsql=  preg_split("/where/i", $sql);   
          // $lsql= explode(" WHERE ", $sql);
           $tsql->selectfrom =$lsql[0];
           $tsql->where= $lsql[1];
        }
       
       //get $grouporder e $filter
       $pos=strripos($tsql->where, "group by");
        if($pos=== false){$pos=strripos($tsql->where, "order by");}
        if($pos !== false ){
            $tsql->filter=substr($tsql->where,0,$pos); 
            $size=strlen($tsql->where);
            $tsql->grouporder =substr($tsql->where,$pos,$size);
        }else{
            $tsql->filter=$tsql->where;
        }

     //get filterexpr
     $tsql->filterexpr=$this->getSplitExpression($tsql->filter);

        return $tsql;
    }
    
    
    private function getSplitExpression($filter) {
        $spltext=array();
        $lexp=array();
        if(!empty($filter)){
          $pos=strripos($filter, " ");
           if($pos=== false){
               $spltext=array($filter);
           } else{
            $spltext= explode(" ", $filter);
           }
        }

       if(sizeof($spltext)==1){
            $expr= $this->getExpression($spltext[0]);
            $lexp= array($expr=>$spltext[0]) ;
       }else{  
              $cont=0; 
              $conttr=0; 
              $command=""; 
			  $coutnoexpr=0;
			 
              foreach ($spltext as $spt) {
                     $command.=" $spt";  
                     
					 $posnexp=strripos($spt, "::");
					 $pos=strripos($spt, ":");
				  if($posnexp !==false ){
                        $expr='__badiu_no_expression_'.$coutnoexpr;
                        $lexp[$expr]= $this->cleanNoExpression($command);
                        $command="";
						$coutnoexpr++;
                    }
					
                  if($pos !==false ){
                        $expr= $this->getExpression($spt);
                        $expr=$this->cleanExpression($expr);
                        $lexp[$expr]= $command;
                        $command="";
                    }
                   if($cont==0){
                        $expr= $this->getExpression($spt);
                        $lexp[$expr]= $spt;
                        $conttr=0;
                   }else{
                      if($conttr==1){
                      
                      }else{
                      
                      }
                   }
                   $cont++;
              }
       }
        return $lexp;
    }
    
    private function getExpression($text) {
        $exp="";
        $pos=strripos($text, ":");
        if($pos !==false ){
            $size=strlen($text);
            $exp=substr($text,$pos+1,$size); 
        }
        
        return $exp;
    }   
    
    private function removeOperatorStart($text) {
        $text=ltrim($text);
        $tstat=substr($text,0,4);

        $size=strlen($text);
        // revome AND
        $pos=strrpos($tstat, "AND ");

        if($pos !== false){
             $text=substr($text,3,$size);
        }else{
            $pos=strrpos($tstat, "OR ");
            if($pos !== false){
              $text=substr($text,2,$size);
            }
        }

       return $text;
    }

  private function valueOperator($value, $sqlfilter) {
        $pos=strrpos($sqlfilter, " LIKE ");
       if($pos !== false){
			$value="%$value%";
           
       }
        return $value;
    }
  private function valueOperatorNativSql($value, $sqlfilter) {
        $pos=strrpos($sqlfilter, " LIKE ");
       if($pos !== false){
			$value="'"."%$value%"."'";
           
       }
        return $value;
    }
    /*
     *   WHERE s.id=:subjectid
     *   public function makeSqlFilter($query,$param) {
              $wsql= " ";
              //foreach($param as $key => $value){
                  $query->setParameter('subjectid',1);
            //  }
              return  $query;
          }
     */

      public function cleanExpression($txt) {
        $txt=str_replace("(","",$txt);
        $txt=str_replace(")","",$txt);
		$txt=str_replace("::","",$txt);
        return $txt;
    }
	  public function cleanNoExpression($txt) {
			$txt=str_replace("::","",$txt);
        return $txt;
    }
    public function getContainer() {
        return $this->container;
    }

	 public function replaceExpressionToValue($key,$expression,$data) {
		$value =$expression;
		$texpression=":$key";
		$data=$this->valueOperatorNativSql($data, $expression);
		$value=str_replace($texpression,$data,$expression);
		
       return $value;
    }
	
    public function setContainer(Container $container) {
        $this->container = $container;
    }

}
