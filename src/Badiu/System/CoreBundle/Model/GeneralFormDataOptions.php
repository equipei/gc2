<?php

namespace Badiu\System\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Badiu\System\CoreBundle\Model\DOMAINTABLE;
class GeneralFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
   
    public  function getUserType(){
        $list=array();
        $list[DOMAINTABLE::$USER_TYPE_PHYSICAL_PERSON]=$this->getTranslator()->trans('badiu.system.user.type.physical');
        $list[DOMAINTABLE::$USER_TYPE_LEGAL_PERSON]=$this->getTranslator()->trans('badiu.system.user.type.legal');
        return $list;
    }

public  function getOperationType(){
        $list=array();
        $list['automatic']=$this->getTranslator()->trans('badiu.system.operation.automatic');
        $list['manual']=$this->getTranslator()->trans('badiu.system.operation.manual');
        return $list;
    }

}
