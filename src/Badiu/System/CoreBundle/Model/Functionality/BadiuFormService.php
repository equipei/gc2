<?php

namespace Badiu\System\CoreBundle\Model\Functionality;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
class BadiuFormService extends BadiuExternalService {

     private $key; 
     private $controller;
     private $operaction='add'; 
     private $kminherit;
     private $response;
  
      public function __construct(Container $container) {
        parent::__construct($container);
        $this->response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
       
    }

    public function exec() {
            
                $this->init(); 
               
                if($this->operaction =='add'){
                    $result=$this->controller->save();
                     return $result;
                }else if($this->operaction =='edit'){
                     $result=$this->controller->edit();
                     return $result; 
                }else if($this->operaction =='index'){
                    $result=$this->controller->save();
                    return $result; 
               }else if($this->operaction =='delete'){
                  $result=$this->controller->delete();
                  return $result; 
              }else if($this->operaction =='remove'){
                 $result=$this->controller->remove();
                 return $result; 
            }
               
                
    }
    public function initParam() {
        $this->setParam($this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters());
    }
    public function init() {
    
            $this->param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
           
            $this->key=$this->getUtildata()->getVaueOfArray($this->param, '_key');
            $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
            
            if($sysoperation->isAdd($this->key)){$this->operaction ='add';}
            else if($sysoperation->isEdit($this->key)){$this->operaction ='edit';}
          
            $bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($this->key);
            $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
            $keymanger->setBaseKey($bkey);
            $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $cbundle->setKeymanger($keymanger);
            $cbundle->initKeymangerExtend($keymanger);
            $this->kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
            $this->kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
            $cform=$cbundle->getFormConfig('data',$keymanger);
            $cpage=$cbundle->getPageConfig($keymanger);
            
             if(!$this->getContainer()->has($this->kminherit->formcontroller())){
           
                 $this->controller=$this->getContainer()->get('badiu.system.core.functionality.form.controller');
             }else{$this->controller=$this->getContainer()->get($this->kminherit->formcontroller());}
           
             $this->controller->setParam($this->param);
             $this->controller->setKey($this->key);
             $this->controller->setKminherit($this->kminherit);
             $this->controller->setResponse($this->response);
           
             $this->controller->setCform($cform);
             $this->controller->setCpage($cpage);
             
            $formoperation=$this->controller->getOperaction();
            if(!empty($formoperation)){$this->operaction=$formoperation;}

            $operaction=$this->getUtildata()->getVaueOfArray($this->param, '_operation');
            if(!empty($operaction)){$this->operaction=$operaction;}
   }
   
    function getKey() {
        return $this->key;
    }

    function getController() {
        return $this->controller;
    }

    function setKey($key) {
        $this->key = $key;
    }

    function setController($controller) {
        $this->controller = $controller;
    }

    function getOperaction() {
        return $this->operaction;
    }

    function setOperaction($operaction) {
        $this->operaction = $operaction;
    }

    function getKminherit() {
        return $this->kminherit;
    }

    function setKminherit($kminherit) {
        $this->kminherit = $kminherit;
    }
    function getResponse() {
        return $this->response;
    }

    function setResponse($response) {
        $this->response = $response;
    }


   
}
