<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class BadiuTreeFormat extends BadiuFormat{
     
     private $listparent;     
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
            $this->listparent=null;
       } 
        
   public  function initListparent(){
     
   if(empty($this->getBaseKey())){return null;}
       $datakey=$this->getBaseKey().'.data';
     
       if($this->getContainer()->has($datakey)){
           $data=$this->getContainer()->get($datakey);
       
           $this->listparent=$data->getFormChoiceParentOnly();
       }
       
       
   }
       
    
    public  function getParentName($data){
        $id=$this->getUtildata()->getVaueOfArray($data,'id');
        
        if(empty($this->listparent)){$this->initListparent();}
     
        $parent=$this->getUtildata()->getVaueOfArray($this->listparent,$id);
        return $parent;
    }
    
    function getListparent() {
        return $this->listparent;
    }

    function setListparent($listparent) {
        $this->listparent = $listparent;
    }


}
