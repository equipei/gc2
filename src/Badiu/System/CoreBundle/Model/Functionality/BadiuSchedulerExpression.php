<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class BadiuSchedulerExpression  extends BadiuModelLib{
     
     /**
     * @var string
     */
    private $message;
     /**
     * @var array
     */
    private $data;
     /**
     * @var array
     */
    private $tblexpresion;

     /**
     * @var interger
     */
    private $parentid; //it is necessary for server site service

    function __construct(Container $container) {
        parent::__construct($container);
        $this->message=null;
        $this->data=null;
        $this->tblexpresion=null;
    }


   public function init($message,$data){
        $this->message=$message;
        $this->data=$data;
         $this->initTblexpresion();
   }
   public function initTblexpresion(){
    
   }
   public function add($key,$value){
       if(empty($this->tblexpresion)){$this->tblexpresion=array();}
       $this->tblexpresion[$key]=$value;
   }
   public function clearall($key,$value){
    $this->tblexpresion=array();
   }
   /*
   *This function is to replace array of data
   */
   public function changeData(){
   
   }
   public function dataAddItem($key,$value){
        $this->data[$key]=$value;
    }

    public function replace(){
        $this->changeData();
        if(empty($this->message)){return null;}
        if(empty($this->data)){return null;}
        if(empty($this->tblexpresion)){return null;}

        foreach ($this->tblexpresion as $key => $value) {
            $ditem=  $result =$this->getUtildata()->getVaueOfArray($this->data,$key);
            $texp='{'.$value.'}';
            $this->message=str_replace($texp,$ditem,$this->message);
        }
        
    }
   function getMessage() {

    return $this->message;
}


function setMessage($message) {
    $this->message = $message;
}

function getData() {
    return $this->data;
}

function setData($data) {
    $this->data = $data;
}


function getTblexpresion() {
    return $this->tblexpresion;
}

function setTblexpresion($tblexpresion) {
    $this->tblexpresion = $tblexpresion;
}


function getParentid() {
    return $this->parentid;
}

function setParentid($parentid) {
    $this->parentid = $parentid;
}
}
