<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Doctrine\ORM\EntityManager;
class BadiuFormat {
    
     /**
     * @var Container
     */
    private $container;
    
      /**
     * @var Translator
     */
    private $translator;
        /**
     * @var object
     */
    private $utildata = null;
    
       /**
     * @var object
     */
    private $utilapp = null;
    
         /**
     * @var object
     */
       private $configformat;
       
            /**
     * @var object
     */
     private $router;
    
      /**
     * @var object
     */
    private $json = null;
    
      /**
     * @var object
     */
    private $outputtype ='html';
    
     /**
     * @var string 
     */
    private $baseKey; 

          /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
	private $systemdata;
    function __construct(Container $container,$baseKey=null) {
                $this->container=$container;
                $this->translator=$this->container->get('translator');
                 $this->utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
                 $this->configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
               $this->router=$this->getContainer()->get("router");
               $this->json = $this->getContainer()->get('badiu.system.core.lib.util.json');
               $this->baseKey=$baseKey;
                $this->init();
      }
      
       public function init() {
           $format=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters('_format');
           if($format=='xls'){$this->outputtype='xls';}
           if($format=='json'){$this->outputtype='json';}
           if($format=='csv'){$this->outputtype='csv';}
           else {$this->outputtype='html';}
       }
  public function getCommandBreakLine() {
            $command="<br />";
            if($this->outputtype=='html'){$command="<br />";}
            else if($this->outputtype=='xls' || $this->outputtype=='json' ||$this->outputtype=='csv'){
                $command="\n";
            }
           $format=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters('_format');
           if($format=='xls'){$this->outputtype='xls';}
           if($format=='json'){$this->outputtype='json';}
           if($format=='csv'){$this->outputtype='csv';}
           else {$this->outputtype='html';}
		   return $command;
       }
       public function getContainer() {
           return $this->container;
       }

       public function setContainer(Container $container) {
           $this->container = $container;
       }
       
     
  public function getTranslator() {
	  $this->translator->setSystemdata($this->getSystemdata());
      return $this->translator;
  } 

  public function setTranslator(Translator $translator) {
      $this->translator = $translator;
  }

 function getUtildata() {
        return $this->utildata;
    }

    function setUtildata($utildata) {
        $this->utildata = $utildata;
    }
    
    function getConfigformat() {
        return $this->configformat;
    }

    function getRouter() {
        return $this->router;
    }

    function setConfigformat($configformat) {
        $this->configformat = $configformat;
    }

    function setRouter($router) {
        $this->router = $router;
    }

    function getJson() {
        return $this->json;
    }

    function setJson($json) {
        $this->json = $json;
    }

    function getOutputtype() {
        return $this->outputtype;
    }

    function setOutputtype($outputtype) {
        $this->outputtype = $outputtype;
    }

    function getBaseKey() {
        return $this->baseKey;
    }

    function setBaseKey($baseKey) {
        $this->baseKey = $baseKey;
    }
    public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }

   function getUtilapp() {
    if(empty($this->utilapp)){
        $this->utilapp =$this->getContainer()->get('badiu.system.core.lib.util.app');
        $this->utilapp->setSessionhashkey($this->getSessionhashkey());
    }
    return $this->utilapp;
}

function setUtilapp($utilapp) {
    $this->utilapp = $utilapp;
}

	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
