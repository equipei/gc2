<?php

namespace Badiu\System\CoreBundle\Model\Functionality;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class BadiuSqlFilter extends BadiuModelLib {

    
     /**
     * @var object
     */
    private $param = null;

   /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
    function __construct(Container $container) {
        parent::__construct($container);
    
    }

    function addcolumns() {
        return "";
    }
    function addfilter() {
        return "";
    }
   

    function getParam() {
        return $this->param;
    }

  
    function setParam($param) {
        $this->param = $param;
    }

   
}
