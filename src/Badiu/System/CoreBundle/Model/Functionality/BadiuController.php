<?php

namespace Badiu\System\CoreBundle\Model\Functionality;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;

class BadiuController {

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var string
     */
    private $datalayout;

    /**
     * @var array
     */
    private $defaultdata;

    /**
     * @var array
     */
    private $defaultdataonpenform;
    private $entity;
    private $keymanger;
    
      private $parentid; //injeted in controller children by setParentid when layout is serveraltable when editAction has parentid

    function __construct(Container $container) {
        $this->container = $container;
        $this->translator = $this->container->get('translator');
        $this->datalayout = 'datawithname';
        $this->defaultdata = null;
        $this->defaultdataonpenform = null;
        $this->keymanger = null;
        $badiuSession = $this->container->get('badiu.system.access.session');

        $this->entity = $badiuSession->get()->getEntity();
    }

    public function searchCheckForm($form) {
        $check = true;

        return $check;
    }

    public function setDefaultDataOnSearchForm($dto) {
        //print_r($dto);
        $defaultData = $this->getDefaultdata();
        if (!empty($defaultData)) {
            foreach ($defaultData as $key => $value) {

                $pos = stripos($value, "__session_");
                if ($pos !== false) {
                    $value = $this->container->get('badiu.system.access.session')->getParamExpression($value);
                    $dto[$key] = $value;
                } else {
                    $dto[$key] = $value;
                }
            }
        }

        return $dto;
    }

    public function setDefaultDataOnAddForm($dto) {
        $defaultData = $this->getDefaultdata();

        if (!empty($defaultData)) {

            if ($this->datalayout == 'dataforserveraltable') {
                foreach ($defaultData as $key => $value) {

                    $pos = stripos($value, "__session_");
                    if ($pos !== false) {
                        $value = $this->container->get('badiu.system.access.session')->getParamExpression($value);
                        $dto[$key] = $value;
                    } else {
                        $dto[$key] = $value;
                    }
                }

                return $dto;
            }


            foreach ($defaultData as $key => $value) {
                $pos = stripos($value, "__session_");
                if ($pos !== false) {
                    $value = $this->container->get('badiu.system.access.session')->getParamExpression($value);
                    $dto->$key($value);
                } else {
                    $dto->$key($value);
                }
            }
        }

        if ($this->datalayout == 'dataforserveraltable') {
            return $dto;
        }

        $badiuSession = $this->container->get('badiu.system.access.session');
        $dto->setDeleted(false);
        $dto->setEntity($badiuSession->get()->getEntity());
        $dto->setTimecreated(new \Datetime());
        $dto->setDeleted(FALSE);

        if ($this->datalayout == 'datatreewithname') {

            $dtype = null;
            if (method_exists($dto, 'getDtype')) {
                $dtype = $dto->getDtype();
            }

            $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
            $factorykeytree->init($this->getKeymanger()->data());

            $idpath = $dto->getIdpath();
            $dtype = $dto->getDtype();
            $entity = $dto->getEntity();

            $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
            $level = $factorykeytree->getLevel($newidtree);
            $order = $factorykeytree->getOrder($newidtree);
            $dto->setIdpath($newidtree);
            $dto->setLevel($level);
            $dto->setOrderidpath($order);


            if (!empty($idpath)) {
                $parentinfo = $factorykeytree->getParentInfo($entity, $idpath, $dtype);
                $dto = $factorykeytree->makePath($parentinfo, $dto);
            }
        }

        return $dto;
    }

    public function setDefaultDataOnEditForm($dto) {


        if ($this->datalayout == 'dataforserveraltable') {
            return $dto;
        }

        $dto->setTimemodified(new \Datetime());


        if ($this->datalayout == 'datatreewithname') {

            $dtype = null;
            if (method_exists($dto, 'getDtype')) {
                $dtype = $dto->getDtype();
            }
            $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
            $factorykeytree->init($this->getKeymanger()->data());

            $parentinfo = $factorykeytree->getParentInfoById($dto->getId());
            $idpath = null;
            $path = null;
            $orderidpath = null;

            if (isset($parentinfo['idpath'])) {
                $idpath = $parentinfo['idpath'];
            }
            if (isset($parentinfo['path'])) {
                $path = $parentinfo['path'];
            }
            if (isset($parentinfo['orderidpath'])) {
                $orderidpath = $parentinfo['orderidpath'];
            }
            $countseparator = substr_count($path, '/');
            $idpathnew = $dto->getIdpath();
            $idpathfather = $factorykeytree->getFather($idpath);

            $editkey = false;
            $changefatherkey = false;
            if ($idpathnew != $idpathfather) {
                $editkey = true;
                $changefatherkey = true;
            }
            if (empty($orderidpath)) {
                $editkey = true;
            }
            if ($countseparator == 0) {
                $editkey = true;
            }

            if ($editkey) {

                $dtype = $dto->getDtype();
                $dto->setPath(null);
                $newidtree = $factorykeytree->getNext($idpathnew, $dtype);
                $level = $factorykeytree->getLevel($newidtree);
                $order = $factorykeytree->getOrder($newidtree);
                $dto->setIdpath($newidtree);
                $dto->setLevel($level);
                $dto->setOrderidpath($order);



                if ($changefatherkey) {

                    if (!empty($idpathnew)) {

                        $parentinfo = $factorykeytree->getParentInfo($idpathnew, $dtype);
                        $dto = $factorykeytree->makePath($parentinfo, $dto);
                    } else {

                        $dto->setPath(null);
                        $dto->setParent(null);
                    }
                } else {

                    $dto = $factorykeytree->makePath($parentinfo, $dto);
                }
            } else {

                $dto->setIdpath($idpath);
            }
        }
       
        //manege cache
        //if news add to cache
        //if edit replace parte of tree
        return $dto;
    }

    //remove it 
    /* public function changeDataOnSearchForm($dto) {
      //print_r($dto);
      return $dto;
      } */

    //remove it
    /*  public function changeDataOnAddForm($dto) {

      if ($this->datalayout == 'dataforserveraltable') {
      return $dto;
      }
      $badiuSession = $this->container->get('badiu.system.access.session');
      $dto->setDeleted(false);
      $dto->setEntity($badiuSession->get()->getEntity());
      $dto->setTimecreated(new \Datetime());
      $dto->setDeleted(FALSE);
      if ($this->datalayout == 'datatreewithname') {
      $dto->setIdpath('1');
      $dto->setPath('1');
      $dto->setorderidpath(100.00);
      }
      // $data->setDto($dto);

      return $dto;
      }
     */


    //remove it
    /*  public function changeDataOnEditForm($dto) {

      if ($this->datalayout == 'dataforserveraltable') {
      return $dto;
      }
      //get sessionn data
      $badiuSession = $this->container->get('badiu.system.access.session');
      $dto->setDeleted(false);
      $dto->setEntity($badiuSession->get()->getEntity());
      $dto->setTimecreated(new \Datetime());
      $dto->setDeleted(FALSE);
      if ($this->datalayout == 'datatreewithname') {
      $dto->setIdpath('1');
      $dto->setPath('1');
      $dto->setorderidpath(100.00);
      }
      //  $data->setDto($dto);

      return $dto;
      } */

    public function setDefaultDataOnOpenEditForm($dto) {
        if ($this->datalayout == 'dataforserveraltable') {
            return $dto;
        }
        //get sessionn data
        $badiuSession = $this->container->get('badiu.system.access.session');
        $dto->setDeleted(false);
        $dto->setEntity($badiuSession->get()->getEntity());
        $dto->setTimecreated(new \Datetime());
        $dto->setDeleted(FALSE);
        if ($this->datalayout == 'datatreewithname') {
            $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
            $idpath = $factorykeytree->getFather($dto->getIdpath());
            $dto->setIdpath($idpath);
        }
        return $dto;
    }

    public function setDefaultDataOnOpenAddForm($dto) {
        $defaultData = $this->getDefaultdataonpenform();

        if (!empty($defaultData)) {

            if ($this->datalayout == 'dataforserveraltable') {
                foreach ($defaultData as $key => $value) {

                    $pos = stripos($value, "__session_");
                    if ($pos !== false) {
                        $value = $this->container->get('badiu.system.access.session')->getParamExpression($value);
                        $dto[$key] = $value;
                    }

                    $pos = stripos($value, ":::");

                    if ($pos !== false) {
                        $value = $this->getValueOfDefaultDataOnOpenFromService($value);
                        if (!empty($value)) {
                            $dto[$key] = $value;
                        }
                    } else {
                        $dto[$key] = $value;
                    }
                    
                  
                    
                }

                return $dto;
            }

            foreach ($defaultData as $key => $value) {
                $pos = stripos($value, "__session_");
                if ($pos !== false) {
                    $value = $this->container->get('badiu.system.access.session')->getParamExpression($value);
                    if (!empty($value)) {
                        $dto->$key($value);
                    }
                }

                $pos = stripos($value, ":::");
                if ($pos !== false) {
                    $value = $this->getValueOfDefaultDataOnOpenFromService($value);
                    if (!empty($value)) {
                        $dto->$key($value);
                    }
                } else {
                    $dto->$key($value);
                }
            }
        }

        return $dto;
    }

    public function setDefaultDataOnOpenSearchForm($dto) {
        $defaultData = $this->getDefaultdataonpenform();
       //print_r($defaultData);exit;
        if (empty($defaultData)) {
            return $dto;
        }


        foreach ($defaultData as $key => $value) {

            $pos = stripos($value, "__session_");
            if ($pos !== false) {
                $value = $this->container->get('badiu.system.access.session')->getParamExpression($value);
                $dto[$key] = $value;
            }

            $pos = stripos($value, ":::");

            if ($pos !== false) {
                $value = $this->getValueOfDefaultDataOnOpenFromService($value);
                if (!empty($value)) {
                    $dto[$key] = $value;
                }
            } else {
                $dto[$key] = $value;
            }
            
              //array field
            $pos = stripos($value, "[");

             if ($pos !== false) {
                 $value = $this->getArrayField($value);
                if (!empty($value)) {
                       $dto[$key] = $value;
                 }else{$dto[$key] = $value;}
              } else {
                $dto[$key] = $value;
            }
        }

        return $dto;
    }

    private function getValueOfDefaultDataOnOpenFromService($param) {
        $type = null;
        $service = null;
        $shortname = null;
        $function = null;

        $value = null;

        $p = explode(":::", $param);
        if (isset($p[0])) {
            $type = $p[0];
        }
        if (isset($p[1])) {
            $service = $p[1];
        }
        if (isset($p[2])) {
            $shortname = $p[2];
            $function = $p[2];
        }

        //internal service key
        if ($type == 'e' || $type == 'c') {
            if ($this->container->has($service)) {
                $data = $this->container->get($service);
                if (method_exists($data, 'getIdByShortname') && !empty($shortname)) {
                    if ($type == 'c') {
                        $value = $data->getIdByShortname($this->getEntity(), $shortname);
                    } else if ($type == 'e') {
                        $value = $data->findByShortname($this->getEntity(), $shortname);
                    }
                }
            }
        } else if ($type == 'cs') {
            if ($this->container->has($service)) {
                $data = $this->container->get($service);
                if (!empty($function) && method_exists($data, $function)) {
                    $value = $data->$function(); 
                }
            }
        }
        return $value;
    }
  public function getArrayField($value){
     
      if(empty($value)) return null;
      $lcharacter=str_split($value);
      $size=sizeof($lcharacter);
      if(is_array($lcharacter) && $lcharacter >0 ){
          $lastchar=$lcharacter[$size-1];
          $fistchar=$lcharacter[0];
          if($lastchar!=']' || $fistchar !='['){return null;}
      }
       $field=array();
      //clean [ ]
      $value=str_replace("[","",$value);
      $value=str_replace("]","",$value);
      $lparamx=explode(",",$value);
      foreach ($lparamx as  $x) {
          $lparamy=explode(":",$x);
          $k=null;
          $v=null;
          if(isset($lparamy[0])){$k=$lparamy[0];}
          if(isset($lparamy[1])){$v=$lparamy[1];}
         if(!empty($k) && !empty($v)){ $field[$k]=$v;}
      }

      return $field;
  }
    public function addCheckForm($form, $data) {
        $check = true;
       
        $checkformkey=$this->getKeymanger()->checkform();
         if ($this->getContainer()->has($checkformkey)) {
             $checkform=$this->getContainer()->get($checkformkey);
             if (method_exists($checkform, 'addCheckForm')) {
                 $check =$checkform->addCheckForm($form, $data);
             }
             
             $datakey=$this->getKeymanger()->data();
             if ($this->getContainer()->has($checkformkey)) {
                  $data=$this->getContainer()->get($datakey);
                  $checkform->setData($data);
             }
             
         }
        if ($this->datalayout == 'datawithname') {
            
            
            if ($data->existAdd()) {
                $form->get('name')->addError(new FormError($this->getTranslator()->trans('badiu.system.duplication.record.name')));
                $check = false;
            }
            
             if ($data->existAddShortname()) {
                $form->get('shortname')->addError(new FormError($this->getTranslator()->trans('badiu.system.duplication.record.shortname')));
                $check = false;
            }
            
            if ($data->existAddIdnumber()) {
                $form->get('idnumber')->addError(new FormError($this->getTranslator()->trans('badiu.system.duplication.record.idnumber')));
                $check = false;
            }
        }

        return $check;
    }

    public function editCheckForm($form, $data) {
         $check = true;
       
        $checkformkey=$this->getKeymanger()->checkform();
   
         if ($this->getContainer()->has($checkformkey)) {
             $checkform=$this->getContainer()->get($checkformkey);
             if (method_exists($checkform, 'editCheckForm')) {
                 $check =$checkform->editCheckForm($form, $data);
            
             }
              $datakey=$this->getKeymanger()->data();
             if ($this->getContainer()->has($checkformkey)) {
                  $data=$this->getContainer()->get($datakey);
                  $checkform->setData($data);
             }
         }
         
        if ($this->datalayout == 'datawithname') {
            if ($data->existEdit()) {
                $form->get('name')->addError(new FormError($this->getTranslator()->trans('badiu.system.duplication.record.name')));
                 $check = false;
            }
            if ($data->existEditShortname()) {
                $form->get('shortname')->addError(new FormError($this->getTranslator()->trans('badiu.system.duplication.record.shortname')));
                $check = false;
            }
            if ($data->existEditIdnumber()) {
                $form->get('idnumber')->addError(new FormError($this->getTranslator()->trans('badiu.system.duplication.record.name')));
               $check = false;
            }
        }
        return $check;
    }

    public function save($data) {
        if ($this->datalayout == 'dataforserveraltable') {
            return $data;
        }
        $result = $data->save();
        
        //session
         if ($this->datalayout == 'datatreewithname') {
           //  $idpath=$data->getDto()->getIdpath();
            //  $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
             // $data->manageSession(false,$idpath);
              //$factorykeytree->updateSession($this->getKeymanger(),$idpath);//DELETE
         }
        return $data->getDto();
    }

    public function extractData($filter, $report, $layout = 'indexCrud') {

        if (!isset($filter['_page'])) {
            $filter['_page'] = 0;
        }
        if ($layout == 'indexCrud' || $layout == 'viewDetail') {
            $type = null;
            if ($layout == 'indexCrud') {
                $type = 'dbsearch.fields.table.view';
            } else if ($layout == 'viewDetail') {
                $type = 'dbgetrow.fields.table.view';
            }
            //$report->extractData($filter, 'dashboard', $type);
            $report->extractData($filter, $layout, $type);
            $report->makeTable($layout);
        } else if ($layout == 'dashboard') {
            $report->extractData($filter, 'dashboard');
        }

      
        return $report;
    }

    public function nextUrlAfterSave() {
        $lroute = null;
        /*   $lroute=array();
          $lroute['route']='xxx.xxx.xxx.';
          $lroute['param']=array('param1'=>0,'param2'=>1); */

        return $lroute;

        return $data->getDto();
    }

    public function getTranslator() {
        return $this->translator;
    }

    public function getContainer() {
        return $this->container;
    }

    public function setTranslator(Translator $translator) {
        $this->translator = $translator;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getDatalayout() {
        return $this->datalayout;
    }

    /**
     * @param mixed $datalayout
     */
    public function setDatalayout($datalayout) {
        $this->datalayout = $datalayout;
    }

    /**
     * @return array
     */
    public function getDefaultdata() {
        return $this->defaultdata;
    }

    /**
     * @param string $filerole
     */
    public function setDefaultdata($defaultdata) {
        $this->defaultdata = $defaultdata;
    }

    public function out() {
        
    }

     public function htmlcontent() {
         $html="";
        // echo $this->getKeymanger()->htmlcontent();exit ;
         if($this->getContainer()->has($this->getKeymanger()->htmlcontent())){
             $service=$this->getContainer()->get($this->getKeymanger()->htmlcontent());
              $html= $service->exec();
         }
       
         return $html;
     }
    public function serviceProcess() {
        $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
        $jparam=file_get_contents('php://input');
        $param=$json->decode($jparam, true); 
        
       
        $pservice=null;
        $function=null;
        $checkkminherit=null;//review it
        if(!empty($param)){
            if(array_key_exists('_service',$param)){$pservice=$param['_service'];}
            if(array_key_exists('_function',$param)){$function=$param['_function'];}
            if(array_key_exists('_checkkminherit',$param)){$checkkminherit=$param['_checkkminherit'];} //review it
        }
        
         
        if(empty($pservice)){$pservice = $this->getContainer()->get('request')->get('_service');}
        if(empty($function)){$function = $this->getContainer()->get('request')->get('_function');}
        if(empty($checkkminherit)){$checkkminherit = $this->getContainer()->get('request')->get('_checkkminherit');}


        //process keymanagerherit review it
        if ($checkkminherit == 1) {
            $keymanger = $this->getContainer()->get('badiu.system.core.functionality.keymanger');
            $bkey = $this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($pservice);
            $keymanger->setBaseKey($bkey);
            $cbundle = $this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $cbundle->setKeymanger($keymanger);
            $cbundle->initKeymangerExtend($keymanger);
            $kminherit = $this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
            $kminherit->init($keymanger, $cbundle->getKeymangerExtend());
            $pservice = $kminherit->generalService($pservice);
        } 



        
        //check if service is empty
        if (empty($pservice)) {
            $response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
            $response->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
            $response->setInfo(SERVICEDOMAINTABLE::$ERROR_SERVICE_EMPTY);
            return $response->get();
        }
         
        //chek if service exist
        if (!$this->getContainer()->has($pservice)) {

            $response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
            $response->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
            $response->setInfo(SERVICEDOMAINTABLE::$ERROR_SERVICE_NOT_VALID.' - '.$pservice);

            return $response->get();
        }
 
        if (empty($function)) {
            $function = 'exec';
        }
        $service = $this->getContainer()->get($pservice);

        $result = $service->$function();
      
        return $result;
    }

    public function execService($dto, $listservice) {
        if (empty($listservice)) {
            return $dto;
        }
        foreach ($listservice as $itemservice) {

            $pos = stripos($itemservice, "/");

            if ($pos !== false) {
                $p = explode("/", $itemservice);
                $servicekey = null;
                $function = null;


                if (isset($p[0])) {
                    $servicekey = $p[0];
                }
                if (isset($p[1])) {
                    $function = $p[1];
                }
                if (!empty($servicekey) && !empty($function)) {
                    if ($this->getContainer()->has($servicekey)) {
                        $service = $this->getContainer()->get($servicekey);
                        if (method_exists($service, $function)) {
                            $dto = $service->$function($dto);
                        }
                    }
                }
            }
        }


        return $dto;
    }

    public function serviceExecBeforeSubmit($dto, $listservice) {
        $dto = $this->execService($dto, $listservice);
        return $dto;
    }

    public function serviceExecAfterSubmit($dto, $listservice) {
        $dto = $this->execService($dto, $listservice);
        return $dto;
    }

    public function addEntityToClass($dto, $dtoParent, $method) {
        $dto->$method($dtoParent);
        return $dto;
    }

    public function findEndityIdInClass($dto, $method) {

        return $dto->$method()->getId();
    }

    function getDefaultdataonpenform() {
        return $this->defaultdataonpenform;
    }

    function setDefaultdataonpenform($defaultdataonpenform) {
        $this->defaultdataonpenform = $defaultdataonpenform;
    }

    function getEntity() {
        return $this->entity;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function getKeymanger() {
        return $this->keymanger;
    }

    function setKeymanger($keymanger) {
        $this->keymanger = $keymanger;
    }
    function getParentid() {
        return $this->parentid;
    }

    function setParentid($parentid) {
        $this->parentid = $parentid;
    }


}
