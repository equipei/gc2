<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class BadiuFormFilter extends BadiuModelLib {
    
    /**
     * @var array
     */
    private $param;
   
    /**
     * @var object
     */
    private $kminherit;
     
	 /**
	 * @var string
	 */
	 private $modulekey;
	 
     function __construct(Container $container) {
               parent::__construct($container);
         
      }
 
    function execBeforeSubmit() {
            
        }
    function execAfterSubmit() {
            
        }
    
        
    function addParamItem($item,$value) {
         $this->param[$item]=$value;
    }
   function removeParamItem($item) {
       if (array_key_exists($item,$this->param)){
            unset($this->param[$item]);
       }
   } 
   
    function getParamItem($item) {
        $value=null;
       if (array_key_exists($item,$this->param)){
            $value=$this->param[$item];
       }
       return $value;
   }  
    function getParam() {
          return $this->param;
      }

    function getKminherit() {
          return $this->kminherit;
    }

      function setParam($param) {
          $this->param = $param;
      }

      function setKminherit($kminherit) {
          $this->kminherit = $kminherit;
      }
 function getModulekey() {
        return $this->modulekey;
    }
function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }


}
