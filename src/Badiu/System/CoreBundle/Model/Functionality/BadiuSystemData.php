<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class BadiuSystemData {

 /**
     * @var Container
     */
    private $container;

    /**
     * @var integer
     */
    private $entity;
	
		/**
	 * @var string
	 */
	private $lang;
	
	/**
     * @var ManageCache
     */
    private $managecache;
	
		/**
     * @var ManageLang
     */
    private $managelang;
	
	/* deprected after remove cron session. Setted year only to keep compatibility */
	private $sessionhashkey;
 function __construct(Container $container) {
        $this->container = $container;
		}
	public function init()
    {
		 $entity=$this->getEntity();
		 $this->managecache=$this->getContainer()->get('badiu.system.core.lib.util.managecache');
		 
		 $this->managecache->setDentity($entity);
		 if(empty($entity)){return null;}
		 $param=array('type'=>'cache','entity'=>$entity);
		 $this->managecache->init($param);
		 
		 $this->managelang=$this->getContainer()->get('badiu.system.core.lib.util.managecache');
		 $entity=$this->getEntity();
		 $this->managelang->setDentity($entity);
		 if(empty($entity)){return null;}
		 $param=array('type'=>'language','entity'=>$entity,'locale'=>$this->getLang());
		 $this->managelang->init($param); 
    }
 
 /**
     * Get the value of container
     *
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Set the value of container
     *
     * @param Container $container
     * @return void
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Get the value of entity
     *
     * @return integer
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set the value of entity
     *
     * @param integer $entity
     * @return void
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * Get the value of cache
     *
     * @return ManageCache
     */
    public function getManagecache()
    {
		if(empty($this->managecache)){
			$this->init();
		}
        return $this->managecache;
    }

    /**
     * Set the value of cache
     *
     * @param ManageCache $cache
     * @return void
     */
    public function setManagecache($managecache)
    {
		$this->managecache = $managecache;
    }

	// Getter for managelang
    public function getManagelang() {
        return $this->managelang;
    }

    // Setter for managelang
    public function setManagelang($managelang) {
        $this->managelang = $managelang;
    }
	
	   /**
     * @return string
     */
    public function getLang() {
		if(empty($this->lang)){
			$badiuSession = $this->container->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			$this->lang=$badiuSession->get()->getLang();
		}
        return $this->lang;
    }
	 /**
     * @param string $lang
     */
    public function setLang($lang) {
        $this->lang = $lang;
    }
	
	  public function getSessionhashkey() {
    return $this->sessionhashkey;
}

public function setSessionhashkey($sessionhashkey) {
   $this->sessionhashkey = $sessionhashkey;
 }

}
