<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class BadiuTreeDataBase extends BadiuDataBase {
   
     
   function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
     
    
       public function save() {
           $this->getEm()->persist($this->getDto());
           $this->getEm()->flush();
           
           $idpath=$this->getDto()->getIdpath();
           $this->manageSession(false,$idpath);
       }    
 
    
       public function geChildrens($param) {
           
            $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');

            $id=$utildata->getVaueOfArray($param,'id');
            if(array_key_exists('id',$param)){unset($param['id']);}
            $idpath= null;
            if(!empty($id)){
                $idpath= $this->getGlobalColumnValue('idpath',array('id'=>$id));
                if(!array_key_exists('entity',$param)){$param['entity']=$this->getGlobalColumnValue('entity',array('id'=>$id));}
            }
            else{$idpath=$utildata->getVaueOfArray($param,'idpath');}
            if(array_key_exists('idpath',$param)){unset($param['idpath']);}

            $grandchildren=$utildata->getVaueOfArray($param,'grandchildren');
            if(array_key_exists('grandchildren',$param)){unset($param['grandchildren']);}

            if(!$grandchildren){
                $level=substr_count($idpath,'.');
                $level++;
            }
            if(empty($idpath)){$level=0;}

          
        $wsql =" ";
        $fwsql=$this->makeSqlWhere($param);
        if(!empty($idpath)){
            $idpath=$idpath.'.';
            $wsql .=" AND  o.idpath LIKE '".$idpath."%' ";
        }
         $wsql .=" AND  o.level=:level ";
       
        $sql="SELECT o.id,o.name,o.idpath,o.path FROM ".$this->getBundleEntity()." o  WHERE  o.id > 0 $wsql  $fwsql  ORDER BY o.orderidpath ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('level',$level);
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return $result;
    } 
//getFormChoiceParent
 public function getFormChoiceParent($param=array()) {

        $entity=null;
        $dtype=null;
        $orderby=" ORDER BY o.orderidpath ";
        $level=null;
        $outdata=null;
        $fparam=array();
        $typeofkey='path';
        if(isset($param['entity'])){$entity=$param['entity'];}
        if(isset($param['dtype'])){$dtype=$param['dtype'];}
        if(isset($param['orderby'])){$orderby=$param['orderby'];}
        if(isset($param['level'])){$level=$param['level'];}
        if(isset($param['outdata'])){$outdata=$param['outdata'];}
        if(isset($param['fparam'])){$fparam=$param['fparam'];}
        if(array_key_exists('typeofkey',$param)){ $typeofkey=$param['typeofkey'];}
        $wsql =" ";
        $fwsql=$this->makeSqlWhere($fparam);
         if(!empty($dtype)){$wsql =" AND  o.dtype=:dtype";}
         if( is_numeric($level) ){$wsql .=" AND  o.level=:level";}
         if($entity==null){
             $badiuSession=$this->getContainer()->get('badiu.system.access.session');
             $badiuSession->setHashkey($this->getSessionhashkey());
             $entity=$badiuSession->get()->getEntity();
        }
       
        $sql="SELECT   o.id,o.name,o.idpath,o.path FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.deleted=:deleted $wsql $fwsql $orderby";
       // echo $sql;exit;
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('deleted',0);
         if(!empty($dtype)){$query->setParameter('dtype',$dtype);}
         if( is_numeric($level) ){$query->setParameter('level',$level);}
         $query=$this->makeSqlFilter($query, $fparam);
        $result= $query->getResult();
        
       // return  $result;
        $fullpath=true;
        $format='lastend';
        if(isset($param['fullpath'])){$fullpath=$param['fullpath'];}
        if(isset($param['format'])){$format=$param['format'];}
        
           $listkid=array();
            foreach ($result as $value) {
                $id=$value['id'];
                $listkid[$id]=$value;
            }
            $parentnameslist=array();
            $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
            foreach ($result as $value) {
                 $id=$value['id'];
                 $idpath=$value['idpath'];
                 $name=$factorykeytree->getPath($listkid,$id,$fullpath,$format); 
                 if($typeofkey=='id'){$idpath=$id;}
                 if($outdata=='pkwithparent'){$parentnameslist[$id]=$name;}
                 else{
                     $param=array('id'=>$idpath,'name'=>$name);
                    array_push($parentnameslist,$param);
                 }
                
            } 
         
            return  $parentnameslist;
    } 
public function getFormChoiceParentKeyId($param=array('typeofkey'=>'id')) {
        $result=$this->getFormChoiceParent($param);
        return $result;
}
public function getFormChoiceParentFirstLevel($param=array('level'=>0)) {
        $result=$this->getFormChoiceParent($param);
        return $result;
}

public function getFormChoiceParentOnly($param=array('fullpath'=>false,'outdata'=>'pkwithparent')) {
        $result=$this->getFormChoiceParent($param);
        return $result;
}
 public function getFormChoiceDefault($param=array()){
            $entity=null;
            $fullpath=true;
            $format='lastend';
            $dtype=null;
            $level=null;
            $outdata=null;
            if(isset($param['entity'])){$entity=$param['entity'];}
            if(isset($param['format'])){$format=$param['format'];}
             if(isset($param['dtype'])){$dtype=$param['dtype'];}
            if(isset($param['fullpath'])){$fullpath=$param['fullpath'];}
            if(isset($param['level'])){$level=$param['level'];}
            if(isset($param['outdata'])){$outdata=$param['outdata'];}
             $wdtype =" ";
            if(!empty($dtype)){$wdtype =" AND  o.dtype=:dtype";}
            if( is_numeric($level) ){$wdtype .=" AND  o.level=:level";}
             if($entity==null){
                $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                $badiuSession->setHashkey($this->getSessionhashkey());
                $entity=$badiuSession->get()->getEntity();
            }
            $sql="SELECT o.id,o.name,o.idpath,o.path FROM ".$this->getBundleEntity()." o WHERE  o.entity = :entity AND o.deleted=:deleted $wdtype ORDER BY o.orderidpath";
            if($outdata=='entity'){$sql="SELECT o FROM ".$this->getBundleEntity()." o WHERE  o.entity = :entity AND o.deleted=:deleted  $wdtype ORDER BY o.orderidpath";}
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			 $query->setParameter('deleted',0);
            if(!empty($dtype)){$query->setParameter('dtype',$dtype);}
             if( is_numeric($level) ){$query->setParameter('level',$level);}
            $result= $query->getResult();
             
            $listkid=array();
            if($outdata=='entity'){
                 foreach ($result as $value) {
                  $id=$value->getId();
                    $padata=array('id'=>$value->getId(),'name'=>$value->getName(),'idpath'=>$value->getIdpath(),'path'=>$value->getPath());
                   $listkid[$id]=$padata;
               }
            }else{
                 foreach ($result as $value) {
                $id=$value['id'];
                $listkid[$id]=$value;
            } 
            }
           
            $parentnameslist=array();
            $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
            if($outdata=='entity'){
                 foreach ($result as $value) {
                 $id=$value->getId();
                 $name=$factorykeytree->getPath($listkid,$id,$fullpath,$format); 
                 $value->setName($name);
                 array_push($parentnameslist,$value);
              } 
            }else{
                foreach ($result as $value) {
                 $id=$value['id'];
                 $name=$factorykeytree->getPath($listkid,$id,$fullpath,$format); 
                 $param=array('id'=>$id,'name'=>$name);
                 array_push($parentnameslist,$param);
              } 
            }
            
         
            return  $parentnameslist;
            
	 } 


public function getFormChoiceLastKeyFirst($param=array()){
            //$entity=null,$fullpath=true,$format='lastfirst',$dtype=null 
            $param['format']='lastfirst';
            $list=$this->getFormChoiceDefault($param);
            return $list;
    }    
    
    
    //delete this function
    public function manageSession($read=true,$idpath=null) {
       // echo "$read= | $idpath ";
        $keysession=$this->getBundleEntity();
        $entity=null;
        $dtype=null;
        if($read){$idpath=null;}
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $entity=$badiuSession->get()->getEntity();
        $sessionkey=$this->getBundleEntity()."_parentpaht";
        $sessionexist=false;
        if($badiuSession->existValue($sessionkey)){$sessionexist=true;}
        if(!$sessionexist){$idpath=null;}
        
        if($sessionexist && $read){
            $slist=$badiuSession->getValue($sessionkey);
            return $slist;
         }
        
        $wsql =" ";
        if(!empty($idpath)){$wsql .=" AND ( o.idpath = :idpath OR  o.idpath LIKE :idpathwithfather )";}
       $sql="SELECT   o.id,o.name,o.idpath,o.path FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.deleted=:deleted $wsql ";
       // echo $sql;exit;
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('deleted',0);
        if(!empty($idpath)){
            $query->setParameter('idpath',$idpath);
            $query->setParameter('idpathwithfather',"$idpath.%");
         }
        // $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        
       // return  $result;
        $fullpath=true;
        $format='lastend';
         
           $listkid=array();
            foreach ($result as $value) {
                $id=$value['id'];
                $listkid[$id]=$value;
            }
            $parentnameslist=array();
            $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
            foreach ($result as $value) {
                 $id=$value['id'];
                 $idpath=$value['idpath'];
                 $name=$factorykeytree->getPath($listkid,$id,$fullpath,$format); 
                $param=array('id'=>$idpath,'name'=>$name);
                array_push($parentnameslist,$param);
            } 
         
             if(!$sessionexist){$badiuSession->addValue($sessionkey,$parentnameslist);}
             else{
                 foreach ($parentnameslist as $row) {
                 $badiuSession->addPackageValue($sessionkey,$row['id'],$row['name']);
             }
             }
            $slist=$badiuSession->getValue($sessionkey);
            /*echo "<pre>";
            print_r($slist);
             echo "</pre>";*/
            return  $slist;
    }   
}
