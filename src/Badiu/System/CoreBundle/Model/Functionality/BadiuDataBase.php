<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Doctrine\ORM\EntityManager;

class BadiuDataBase  {
    /**
     * @var Container
     */
    private $container;
     /**
     * @var EntityManager
     */
    private $em; 
    
       /**
     * @var string
     */
    private $bundleEntity;   
    /**
     * @var object
     */
    private $dto;
   
     /**
     * @var object
     */
    private $sqlservice=null;
    
   /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
   function __construct(Container $container,$bundleEntity) {
                $this->container=$container;
                $db="default";//$this->container->get('badiu.system.access.session')->get()->getDbapp(); 
                $this->em=$this->container->get('doctrine')->getEntityManager($db); 
                $this->bundleEntity=$bundleEntity;
      }
     
       public function initSqlservice($webserviceprocess=false) {
           if(empty($this->sqlservice)){
               $this->sqlservice=$this->container->get('badiu.system.core.lib.sql.factorysqlservice');
               $this->sqlservice->setBundleEntity($this->bundleEntity);
               $this->sqlservice->setEm($this->em);
               $this->sqlservice->setWebserviceprocess($webserviceprocess);
           }
           
       }
    
       public function addNativeSql($paramcheckexist,$paramadd,$paramedit,$defaultablevalue=false) {
            $this->initSqlservice();
            $result=array();
			 $exist = $this->countNativeSql($paramcheckexist,$defaultablevalue);
			
		  if($exist && $paramedit!=null ){
                $id=$this->getIdByParam($paramcheckexist);
				$paramedit['id']=$id;
                $execresult = $this->updateNativeSql($paramedit, $defaultablevalue);
                $result['exist']=1;
                $result['id']=$id;
                $result['result']=$execresult;
				
           }else if(!$exist && $paramadd!=null ){
			   
			    $execresult = $this->insertNativeSql($paramadd,$defaultablevalue);
				$result['exist']=0;
				$result['id']=$execresult;
				$result['result']=$execresult;
			 }
            return $result;
    }    
       public function insertNativeSql($param,$defaultablevalue=true) {
            $this->initSqlservice();
            $this->sqlservice->setDefaultablevalue($defaultablevalue);
            $this->getSqlservice()->setParam($param);
            $retsult=$this->getSqlservice()->insert();
            return $retsult;
       }    
       public function updateNativeSql($param,$defaultablevalue=true) {
            $this->initSqlservice();
            $this->sqlservice->setDefaultablevalue($defaultablevalue);
            $this->getSqlservice()->setParam($param);
            $retsult=$this->getSqlservice()->update();
            return $retsult;
       }
       public function updateNativeSqlService() {
            $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
            unset($param['_service']);
            unset($param['_function']);
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $entity=$badiuSession->get()->getEntity();
            $param['entity']=$entity;
            $retsult=$this->updateNativeSql($param,false);
            return $retsult;
       }
        public function countNativeSql($param,$defaultablevalue=true) {
            $this->initSqlservice();
            $this->sqlservice->setDefaultablevalue($defaultablevalue);
            $this->getSqlservice()->setParam($param);
            $retsult=$this->getSqlservice()->count();
            return $retsult;
       } 
       public function removeNativeSql($param,$defaultablevalue=true) {
            $this->initSqlservice();
            $this->sqlservice->setDefaultablevalue($defaultablevalue);
            $this->getSqlservice()->setParam($param);
            $retsult=$this->getSqlservice()->remove();
            return $retsult;
       }
       public function removeNativeSqlService() {
            $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
            unset($param['_service']);
            unset($param['_function']);
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $entity=$badiuSession->get()->getEntity();
            $param['entity']=$entity;
            $retsult=$this->removeNativeSql($param,false);
            return $retsult;
       }
	    
	   public function removeBatchNativeSql($param,$defaultablevalue=true) {
            $this->initSqlservice();
            $this->sqlservice->setDefaultablevalue($defaultablevalue);
            $this->getSqlservice()->setParam($param);
            $retsult=$this->getSqlservice()->removeBatch();
            return $retsult;
       }
	   /*public function removeBatchNativeSqlService() {
            $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
            unset($param['_service']);
            unset($param['_function']);
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $entity=$badiuSession->get()->getEntity();
            $param['entity']=$entity;
            $retsult=$this->removeBatchNativeSql($param,false);
            return $retsult;
       }*/
	   
       public function save() {
           $this->getEm()->persist($this->getDto());
           $this->getEm()->flush();
       }    
       
        
       
       public function edit() {
            $this->save();
       } 
       
       
        public function deleteHash($id) {
			$hash="_deleted_".time()."_".$id;
			return $hash;
		}
		public function getDataWithoutDeleteHash($txt) {
			 $pos=stripos($txt, "_deleted_");
			 if($pos=== false){return $txt;}
			 $p= explode("_deleted_", $txt);
			 if(isset($p[0])){return $p[0];}
			return $txt;
		}
      public function delete($param) {
			$id=null;
			$entity=null;
			if(isset($param['id'])){$id=$param['id'];}
			if(isset($param['entity'])){$entity=$param['entity'];}
			
			if(empty($id)){return null;}
			if(empty($entity)){return null;}
			$hash=$this->deleteHash($id);
			$hasname= $this->hasColumn('name');
			$name=null;
			if($hasname){
				$name=$this->getGlobalColumnValue('name',array('id'=>$id));
				if(!empty($name)){$name=$name.$hash;}
			}
			
			$hasshortname=$this->hasColumn('shortname');
			$shortname=null;
			if($hasshortname){
				$shortname=$this->getGlobalColumnValue('shortname',array('id'=>$id));
				if(!empty($shortname)){$shortname=$shortname.$hash;}
			}
			$fparam=array('id'=>$id,'deleted'=>1,'timemodified'=>new \DateTime(),'entity'=>$entity);
			if(!empty($name)){$fparam['name']=$name;}
			if(!empty($shortname)){$fparam['shortname']=$shortname;}
			
			$result=$this->updateNativeSql($fparam,false);
            
            return $result;
     }
   
    public function restore($param) {
			$id=null;
			$entity=null;
			if(isset($param['id'])){$id=$param['id'];}
			if(isset($param['entity'])){$entity=$param['entity'];}
			
			if(empty($id)){return null;}
			if(empty($entity)){return null;}
			
			$fparam=array('id'=>$id,'deleted'=>0,'timemodified'=>new \DateTime(),'entity'=>$entity);
			
			$hasname= $this->hasColumn('name');
			if($hasname){
				$name=$this->getGlobalColumnValue('name',array('id'=>$id));
				if(!empty($name)){
					$namewithouthash=$this->getDataWithoutDeleteHash($name);
					if($namewithouthash!=$name){
						$exist=$this->existColumnEdit($id,$entity,'name',$namewithouthash);
						if(!$exist){$fparam['name']=$namewithouthash;}
					}
				}
				
			}
			$hasshortname=$this->hasColumn('shortname');
			if($hasshortname){
				$shortname=$this->getGlobalColumnValue('shortname',array('id'=>$id));
				if(!empty($shortname)){
					$shortnamewithouthash=$this->getDataWithoutDeleteHash($shortname);
					if($shortnamewithouthash!=$shortname){
						$exist=$this->existColumnEdit($id,$entity,'shortname',$shortnamewithouthash);
						if(!$exist){$fparam['shortname']=$shortnamewithouthash;}
					}
				}
			}
			
			$result=$this->updateNativeSql($fparam,false);
            
            return $result;
     }
     public function remove($id) {
            $sql="DELETE  FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id', $id);
            $result=$query->execute();
            return $result;
     }
      public function removeByParam($entity,$param) {
            $wsql=$this->makeSqlWhere($param);
            $sql="DELETE  FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $wsql ";
             $query = $this->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
             $query=$this->makeSqlFilter($query, $param);
             $result=$query->execute();
             return $result;
       }
       public function exist($id) {
           $r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.id = :id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
       }
    
       public function countRow($entity,$param) {
          
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $wsql";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
             $query=$this->makeSqlFilter($query, $param);
             $result= $query->getOneOrNullResult();
             if(isset($result['countrecord'])){ $result=$result['countrecord'];}
             return $result;
       }
       public function countGlobalRow($param) {
          
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.id > :bnetctrid $wsql";
			 $query = $this->getEm()->createQuery($sql);
             $query->setParameter('bnetctrid',0);
             $query=$this->makeSqlFilter($query, $param);
			 $result= $query->getOneOrNullResult();
             if(isset($result['countrecord'])){ $result=$result['countrecord'];}
             else {return null;}
             return $result;
       }
       
       public function sumColumn($entity,$column,$param) {
          
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  SUM(o.$column) AS total FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $wsql";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
             $query=$this->makeSqlFilter($query, $param);
             $result= $query->getOneOrNullResult();
             if(isset($result['total'])){ $result=$result['total'];}
             else {return null;}
             return $result;
       }
       public function sumGlobalColumn($column,$param) {
          
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  SUM(o.$column) AS total FROM ".$this->getBundleEntity()." o  WHERE o.id > :bnetctrid $wsql";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('bnetctrid',0);
             $query=$this->makeSqlFilter($query, $param);
             $result= $query->getOneOrNullResult();
             //$result=$result['total'];
             if(isset($result['total'])){ $result=$result['total'];}
             else {return null;}
             return $result;
       }
       
       
        public function getColumnValue($entity,$column,$param) {
          
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  o.$column FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $wsql";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
             $query=$this->makeSqlFilter($query, $param);
             $result= $query->getOneOrNullResult();
              if(isset($result[$column])){ $result=$result[$column];}
              else {return null;}
             return $result;
       }
       public function getColumnValues($entity,$column,$param) {
          
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  o.$column FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $wsql";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
             $query=$this->makeSqlFilter($query, $param);
             $result= $query->getResult();
            return $result;
       }
       public function getGlobalColumnValue($column,$param) {
          
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  o.$column FROM ".$this->getBundleEntity()." o  WHERE o.id > :bnetctrid $wsql";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('bnetctrid',0);
             $query=$this->makeSqlFilter($query, $param);
             $result= $query->getOneOrNullResult();
             if(isset($result[$column])){ $result=$result[$column];}
             else {return null;}
             return $result;
       }
       public function getGlobalColumnsValue($column,$param) {
          
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  $column FROM ".$this->getBundleEntity()." o  WHERE o.id > :bnetctrid $wsql";
        $query = $this->getEm()->createQuery($sql);
         $query->setParameter('bnetctrid',0);
         $query=$this->makeSqlFilter($query, $param);
         $result= $query->getOneOrNullResult();
        return $result;
   }
       public function getGlobalColumnValues($column,$param,$parampaging=null) {
            $x=null;
            $y=null;
            if(!empty($parampaging) && is_array($parampaging)){
                if(array_key_exists('offset',$parampaging)){$x=$parampaging['offset'];}
                if(array_key_exists('limit',$parampaging)){$y=$parampaging['limit'];}
            }
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT DISTINCT o.$column FROM ".$this->getBundleEntity()." o  WHERE o.id > :bnetctrid $wsql";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('bnetctrid',0);
             $query=$this->makeSqlFilter($query, $param);
             if($x!=null && $x >= 0){$query->setFirstResult($x);}
             if($y!=null && $y >= 0){$query->setMaxResults($y);}
             $result= $query->getResult();
             
             return $result;
       }
       public function getGlobalColumnsValues($column,$param,$parampaging=null) {
        $x=null;
        $y=null;
		$orderby= "";
		if(isset($param['_orderby'])){
			$orderby=$param['_orderby'];
			unset($param['_orderby']);
			}
        if(!empty($parampaging) && is_array($parampaging)){
            if(array_key_exists('offset',$parampaging)){$x=$parampaging['offset'];}
            if(array_key_exists('limit',$parampaging)){$y=$parampaging['limit'];}
        }

        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  $column FROM ".$this->getBundleEntity()." o  WHERE o.id > :bnetctrid $wsql $orderby";
		
        $query = $this->getEm()->createQuery($sql);
         $query->setParameter('bnetctrid',0);
         $query=$this->makeSqlFilter($query, $param);
         if($x!=null && $x >= 0){$query->setFirstResult($x);}
         if($y!=null && $y >= 0){$query->setMaxResults($y);}
         $result= $query->getResult();
         
         return $result;
   }
         public function getMaxColumnValue($entity,$column,$param) {
          
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  MAX(o.$column) AS value FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $wsql";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
             $query=$this->makeSqlFilter($query, $param);
             $result= $query->getOneOrNullResult();
              if(isset($result['value'])){ $result=$result['value'];}
              else {return null;}
             return $result;
       }
       public function getMaxGlobalColumnValue($column,$param) {
          
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  MAX(o.$column) AS value FROM ".$this->getBundleEntity()." o  WHERE o.id > :bnetctrid $wsql";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('bnetctrid',0);
             $query=$this->makeSqlFilter($query, $param);
             $result= $query->getOneOrNullResult();
              if(isset($result['value'])){ $result=$result['value'];}
              else {return null;}
             return $result;
       }
	   
	    public function getListidOfNullField($column,$param,$parampaging=null) {
        $x=null;
        $y=null;
		
        if(!empty($parampaging) && is_array($parampaging)){
            if(array_key_exists('offset',$parampaging)){$x=$parampaging['offset'];}
            if(array_key_exists('limit',$parampaging)){$y=$parampaging['limit'];}
        }

        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE 	o.".$column." IS NULL $wsql ";
		
        $query = $this->getEm()->createQuery($sql);
         $query=$this->makeSqlFilter($query, $param);
         if($x!=null && $x >= 0){$query->setFirstResult($x);}
         if($y!=null && $y >= 0){$query->setMaxResults($y);}
         $result= $query->getResult();
         
         return $result;
   }
   
    public function getListidOfEmptyField($column,$param,$parampaging=null) {
        $x=null;
        $y=null;
		
        if(!empty($parampaging) && is_array($parampaging)){
            if(array_key_exists('offset',$parampaging)){$x=$parampaging['offset'];}
            if(array_key_exists('limit',$parampaging)){$y=$parampaging['limit'];}
        }

        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE 	o.".$column." ='' $wsql ";
		
        $query = $this->getEm()->createQuery($sql);
         $query=$this->makeSqlFilter($query, $param);
         if($x!=null && $x >= 0){$query->setFirstResult($x);}
         if($y!=null && $y >= 0){$query->setMaxResults($y);}
         $result= $query->getResult();
         
         return $result;
   }
        public function existByParam($entity,$param) {
            $r=FALSE;
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $wsql";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
             $query=$this->makeSqlFilter($query, $param);
             $result= $query->getOneOrNullResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
       }
      public function existAdd() {
          $r=FALSE;
           $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND UPPER(o.name) = :name ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('name',strtoupper($this->getDto()->getName()) );
            $result= $query->getOneOrNullResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
            
            
       }
       public function existEdit() {
           $r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.id != :id AND o.entity = :entity AND UPPER(o.name) = :name ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$this->getDto()->getId());
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('name',strtoupper($this->getDto()->getName()) );
            $result= $query->getOneOrNullResult();
            if($result['countrecord']>0){$r=TRUE;}
             return $r;
          
       }
       
        public function existColumnAdd($entity,$column,$value,$param=array()) {
          $r=FALSE;
          $wsql=$this->makeSqlWhere($param);
           $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND UPPER(o.$column) = :$column $wsql";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query->setParameter($column,strtoupper($value) );
             $query=$this->makeSqlFilter($query, $param);
            $result= $query->getOneOrNullResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
            
            
       }
       public function existColumnEdit($id,$entity,$column,$value,$param=array()) {
           $r=FALSE;
             $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.id != :id AND o.entity = :entity AND UPPER(o.$column) = :$column  $wsql ";
           $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $query->setParameter('entity',$entity);
            $query->setParameter($column,strtoupper($value) );
             $query=$this->makeSqlFilter($query, $param);
            $result= $query->getOneOrNullResult();
          
            if($result['countrecord']>0){$r=TRUE;}
             return $r;
          
       }
       public function existAddIdnumber() {
           $idnumber=$this->getDto()->getIdnumber();
            if(empty($idnumber)) return false;
          $r=FALSE;
           $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND UPPER(o.idnumber) = :idnumber ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('idnumber',strtoupper($this->getDto()->getIdnumber()) );
            $result= $query->getOneOrNullResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
            
            
       }
       public function existEditIdnumber() {
           $idnumber=$this->getDto()->getIdnumber();
            if(empty($idnumber)) return false;
           $r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.id != :id AND o.entity = :entity AND UPPER(o.idnumber) = :idnumber ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$this->getDto()->getId());
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('idnumber',strtoupper($this->getDto()->getIdnumber()) );
            $result= $query->getOneOrNullResult();
            if($result['countrecord']>0){$r=TRUE;}
             return $r;
          
       }
       
        public function existAddShortname() {
           $shortname=$this->getDto()->getShortname();
            if(empty($shortname)) return false;
          $r=FALSE;
           $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND UPPER(o.shortname) = :shortname ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('shortname',strtoupper($this->getDto()->getShortname()) );
            $result= $query->getOneOrNullResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
            
            
       }
       public function existEditShortname() {
           $shortname=$this->getDto()->getShortname();
            if(empty($shortname)) return false;
           $r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.id != :id AND o.entity = :entity AND UPPER(o.shortname) = :shortname ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$this->getDto()->getId());
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('shortname',strtoupper($this->getDto()->getShortname()) );
            $result= $query->getOneOrNullResult();
            if($result['countrecord']>0){$r=TRUE;}
             return $r;
          
       }
        public function existByIdnumber($entity,$idnumber) {
         
          $r=FALSE;
           $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND UPPER(o.idnumber) = :idnumber ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query->setParameter('idnumber',strtoupper($idnumber));
            $result= $query->getOneOrNullResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
            
            
       }
          public function existByShortname($entity,$shortname) {
          
          $r=FALSE;
           $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND UPPER(o.shortname) = :shortname ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query->setParameter('shortname',strtoupper($shortname));
            $result= $query->getOneOrNullResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
          }   
            
       public function findById($id,$returnArray=false) {
            $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.id = :id ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result=null;
            if($returnArray){
                $result= $query->getArrayResult();
                return  $result[0];
            }
            else {
                $result= $query->getOneOrNullResult();
                 return  $result;
           }
           
       }
	   public function findByIdnumber($entity,$idnumber) {
            $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.idnumber = :idnumber ";
            $query = $this->getEm()->createQuery($sql);
			 $query->setParameter('entity',$entity);
            $query->setParameter('idnumber',$idnumber);
            $result= $query->getOneOrNullResult();
            return  $result;
           
           
       }
	    public function getIdByIdnumber($entity,$idnumber) {
            $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.idnumber = :idnumber ";
            $query = $this->getEm()->createQuery($sql);
			 $query->setParameter('entity',$entity);
            $query->setParameter('idnumber',$idnumber);
            $result= $query->getOneOrNullResult();
			if(!empty($result)){$result=$result['id'];}
            return  $result;
           
           
       }
	   public function findByShortname($entity,$shortname) {
            $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.shortname = :shortname ";
            $query = $this->getEm()->createQuery($sql);
			 $query->setParameter('entity',$entity);
            $query->setParameter('shortname',$shortname);
			$result= $query->getOneOrNullResult();
            return  $result;
           
       }
	    public function getIdByShortname($entity,$shortname) {
            $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.shortname = :shortname ";
            $query = $this->getEm()->createQuery($sql);
	    $query->setParameter('entity',$entity);
            $query->setParameter('shortname',$shortname);
			$result= $query->getOneOrNullResult();
                       if(!empty($result)){$result=$result['id'];}
            return  $result;
           
       }
        public function getShortnameById($id) {
            $sql="SELECT  o.shortname FROM ".$this->getBundleEntity()." o  WHERE o.id = :id";
            $query = $this->getEm()->createQuery($sql);
	    $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
            if(!empty($result)){$result=$result['shortname'];}
            return  $result;
           
       }
       public function getIdnumberById($id) {
            $sql="SELECT  o.idnumber FROM ".$this->getBundleEntity()." o  WHERE o.id = :id";
            $query = $this->getEm()->createQuery($sql);
	    $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
            if(!empty($result)){$result=$result['idnumber'];}
            return  $result;
           
       }
       public function findOnyByParam($entity,$param,$returnArray=false) {
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $wsql";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
             $query=$this->makeSqlFilter($query, $param);
            $result=null;
            if($returnArray){$result= $query->getArrayResult();}
            else {$result= $query->getResult();}
            return  $result[0];
       }
        public function findByParam($entity,$param, $orderby="", $returnArray=false) {
            $wsql=$this->makeSqlWhere($param);
            $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $wsql $orderby";
            $query = $this->getEm()->createQuery($sql);
             $query->setParameter('entity',$entity);
             $query=$this->makeSqlFilter($query, $param);
            $result=null;
            if($returnArray){$result= $query->getArrayResult();}
            else {$result= $query->getResult();}
            return  $result;
       }
       public function findByEntity($entity,$orderby="",$deleted=false) {
             $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.deleted=:deleted $orderby";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
		    $query->setParameter('deleted',$deleted);
            $result= $query->getResult();
            return  $result; 
        }
        
     public function getIdByParam($param=array()) {
        $wsql=$this->makeSqlWhere($param);
        $sql = "SELECT  o.id FROM " . $this->getBundleEntity() . " o  WHERE o.id > 0  $wsql ";
        $query = $this->getEm()->createQuery($sql);
        $query=$this->makeSqlFilter($query,$param);
        $result = $query->getOneOrNullResult();
        if(!empty($result)){$result = $result['id'];}
     
        return $result;
    }
    //duplicate existe same function with different param
    /* 
 public function existByParam($param=array()) {
        $wsql=$this->makeSqlWhere($param);
        $sql = "SELECT  o.id FROM " . $this->getBundleEntity() . " o  WHERE o.id > 0  $wsql ";
        $query = $this->getEm()->createQuery($sql);
        $query=$this->makeSqlFilter($query,$param);
        $result = $query->getOneOrNullResult();
        if(!empty($result)){$result = $result['id'];}
     
        return $result;
    }*/
    
     public function getNameById($id) {
        $sql="SELECT  o.name FROM ".$this->getBundleEntity()." o  WHERE o.id = :id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        $result=$result['name'];
      return  $result;
    } 
	 public function getNameByShortname($entity,$shortname) {
        $sql="SELECT  o.name FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND  o.shortname = :shortname";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('shortname',$shortname);
        $result= $query->getOneOrNullResult();
        $result=$result['name'];
      return  $result;
    } 
     public function getMenu($id) {
       $sql="SELECT  o.id,o.name FROM ".$this->getBundleEntity()." o  WHERE o.id = :id";
       $query = $this->getEm()->createQuery($sql);
       $query->setParameter('id',$id);
       $result= $query->getOneOrNullResult();
       return  $result;
    } 
  
        public function getIdNameByEntity($entity,$orderby="") {
             $sql="SELECT  o.id,o.name FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $orderby";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $result= $query->getResult();
            return  $result;
        }
         public function getIdNameByParam($entity,$param,$orderby="") {
             $wsql=$this->makeSqlWhere($param);
             $sql="SELECT  o.id,o.name FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity $wsql $orderby";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query=$this->makeSqlFilter($query, $param);
            $result= $query->getResult();
            return  $result;
        }

    public function getFormChoice($entity,$param=array(),$orderby="") {
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id,o.name,o.shortname FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.deleted=:deleted $wsql $orderby";
      
       $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('deleted',0);
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return  $result;
    }
    public function getFormChoiceShortname($entity,$param=array(),$orderby="") {
		$listshortnames=null;
		$shwsql="";
		
		if(isset($param['_shortnames'])){
			$listshortnames=$param['_shortnames'];
			if(!is_array($listshortnames)){$listshortnames=null;}
			unset($param['_shortnames']);
		}
		
		if(!empty($listshortnames)){$shwsql=" AND o.shortname IN(:shortnames) "; }
  
		$wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.shortname AS id,o.name FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity  AND o.deleted=:deleted $wsql $shwsql $orderby";
      
       $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('deleted',0);
        $query=$this->makeSqlFilter($query, $param);
		if(!empty($listshortnames)){$query->setParameter('shortnames',$listshortnames);}
        $result= $query->getResult();
        return  $result;
    }

     public function getFormChoiceWithoutEntity($param=array(),$orderby="") {
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id,o.name FROM ".$this->getBundleEntity()." o  $wsql $orderby";
        $query = $this->getEm()->createQuery($sql);
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return  $result; 
    }
    
          public function makeSqlWhere($param) {
              $wsql= " ";
              if(is_array($param) && sizeof($param) > 0 ){
                  foreach($param as $key => $value){
                   $tblcol=$this->getTableColumnAlias($key);
                  $wsql.= " AND ".$tblcol->tablecolumn."=:".$tblcol->column;
                }
              }
              
              return  $wsql;
          }
        public function makeSqlFilter($query,$param) {
              $wsql= " ";
               if(is_array($param) && sizeof($param) > 0 ){
                      foreach($param as $key => $value){
                    $tblcol=$this->getTableColumnAlias($key);
                    $key=$tblcol->column;
                    $query->setParameter($key,$value);
                }
               }
           
              return  $query;
          }
        
         public function getTableColumnAlias($column) {
             
             $result=new \stdClass();
             $result->table='';
             $result->column='';
             $result->tablecolumn='';
            
              $pos = stripos($column, ".");
              if ($pos !== false) {
                 
                  $p=explode(".",$column);
                  $tbl=null;
                  $col=null;
                  if(isset($p[0])){$tbl=$p[0];}
                  if(isset($p[1])){$col=$p[1];}
                  
                  $result->table=$tbl;
                  $result->column=$col;
                  $result->tablecolumn=$tbl.'.'.$col;
                 
                   
             }
              else{
                  $tbl='o';
                  $col=$column;
                  $result->table=$tbl;
                  $result->column=$col;
                  $result->tablecolumn=$tbl.'.'.$col;
              }
             
              return $result;
         }
		 
		   public function hasColumn($column) {
			    $metadata= $this->getEm()->getClassMetadata($this->getBundleEntity());
				$hascolumn=$metadata->hasField($column);
				return $hascolumn;
		   }
       public function getEm() {
           return $this->em;
       }

       public function setEm(EntityManager $em) {
           $this->em = $em;
       }


       public function getTranslator() {
           return $this->translator;
       }

       public function getDto() {
           return $this->dto;
       }

       public function setDto($dto) {
           $this->dto = $dto;
       }

       public function getContainer() {
           return $this->container;
       }

       public function setContainer(Container $container) {
           $this->container = $container;
       }

       public function getBundleEntity() {
           return $this->bundleEntity;
       }

       public function setBundleEntity($bundleEntity) {
           $this->bundleEntity = $bundleEntity;
       }

       function getSqlservice() {
           return $this->sqlservice;
       }

       function setSqlservice($sqlservice) {
           $this->sqlservice = $sqlservice;
       }
       public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }

}
