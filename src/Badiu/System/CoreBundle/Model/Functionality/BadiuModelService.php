<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class BadiuModelService  {
    /**
     * @var Container
     */
    private $container;
    
	 private $param;

   public static  $EXEC_AFTER_ADD   ="serviceexecafteradd";
   public static  $EXEC_BEFORE_ADD   ="serviceexecbeforeadd";
   public static  $EXEC_AFTER_EDIT   ="serviceexecafteredit";
   public static  $EXEC_BEFORE_EDIT   ="serviceexecbeforeedit";

   function __construct(Container $container) {
                $this->container=$container;
                $this->param=null;
				
      }
     

	public function afterAdd($data) {
			 $result= exec(self::$EXEC_AFTER_ADD,$data);
      return $result;
		}
  public function beforeAdd($data) {
       $result= exec(self::$EXEC_BEFORE_ADD,$data);
      return $result;
    }

  public function afterEdit($data) {
      $result= exec(self::$EXEC_AFTER_EDIT,$data);
      return $result;
    }
  public function beforeEdit($data) {
     $result= exec(self::$EXEC_BEFORE_EDIT,$data);
      return $result;
    }
 public function exec($operation,$data) {
      $sinfo=$this->getInfo($data,$operation);
      if(!empty($sinfo->service)  && !empty($sinfo->function)){
        $service=$this->getContainer()->get($sinfo->service);
        $function=$sinfo->function;
        $result=$service->$function($data);
        return $result;
      }
    }
  private function getInfo($data,$operation) {
      $param=$this->param;
      if(empty($param)){
          if(is_array($data) && isset($data['param'])){
              $param=$data['param'];
          }else if(is_object($data)){
              $param=$data->getParam();
          }
      }
      

      $sinfo= new \stlclass();
      $sinfo->service=null;
      $sinfo->function=null;

      $httpQueryString= $this->getContainer()->get('badiu.system.core.lib.http.querystring');
      $httpQueryString->setQuery($param); 
      $httpQueryString->makeParam();
      if($httpQueryString->existKey($operation)){
         $var=$httpQueryString->getValue($operation);
        if(!empty($var)){
             $p = explode("|", $var);
             if(isset($p[0])){$sinfo->service=$p[0];}
             if(isset($p[1])){$sinfo->function=$p[1];}
        }
        
      }
         
      return $sinfo;

  }

       public function getContainer() {
           return $this->container;
       }

       public function setContainer(Container $container) {
           $this->container = $container;
       }

public function getParam()
    {
        return $this->param;
    }

  public function setParam($param)
    {
        $this->param = $param;
    }

}
