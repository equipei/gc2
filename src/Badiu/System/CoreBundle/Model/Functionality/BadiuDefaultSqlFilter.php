<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class BadiuDefaultSqlFilter extends BadiuSqlFilter{
  
    function __construct(Container $container) {
            parent::__construct($container);
      
       }

    public function exec(){
         $kminherit=$this->getKminherit();
          $keyservice=$kminherit->filtersql();
          $result="";
         if($this->getContainer()->has($keyservice)) {
            $service = $this->getContainer()->get($keyservice);
            if (method_exists($service, 'setSessionhashkey')) {$service->setSessionhashkey($this->getSessionhashkey()); }
            if (method_exists($service, 'exec')) {$result=$service->exec();}
         }
         return $result;
    }
    private function getKminherit(){
        $key= $this->getContainer()->get('request')->get('_route');
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $datasession=$badiuSession->get();
        
        if($datasession->getType()=='webservice'){
            $webserviceinfo=$datasession->getWebserviceinfo();
            $key=$this->getUtildata()->getVaueOfArray($webserviceinfo,'_param._key',true);
        }
      
        $bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
        $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
        $keymanger->setBaseKey($bkey);
        $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
        $cbundle->setSessionhashkey($this->getSessionhashkey());
        $cbundle->setKeymanger($keymanger);
        $cbundle->initKeymangerExtend($keymanger);
        $kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
        return $kminherit;
    }
}
