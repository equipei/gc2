<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class BadiuExecService extends BadiuModelLib {

    public function __construct(Container $container) {
        parent::__construct($container);
        $this->response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
       
    }
    private $param;
   
    public function exec() {
       
                $this->logtimestart=new \DateTime();
                $auth=$this->getContainer()->get('badiu.auth.core.webservice.navegation');
                $this->param=$auth->authRequest();
                $this->setSessionhashkey($auth->getSyssession()->getHashkey());
                $badiuSession = $this->getContainer()->get('badiu.system.access.session');
                $badiuSession->setHashkey($this->getSessionhashkey());
                $this->addRequestParamToSession($this->param);
             
                //exec service
                $skey=$this->getUtildata()->getVaueOfArray($this->param,'_key');
                
                $function=$this->getUtildata()->getVaueOfArray($this->param,'_kfunction');
                if(empty($function)){$function="exec";}
               
                if(empty($skey)){
                    $response=$this->response->denied('badiu.system.core.exec.service.keysisempty', $this->getTranslator()->trans('badiu.system.core.exec.service.message.keysisempty'));
                    return $response;
                }
                
                else if(!$this->getContainer()->has($skey)){
                    $response=$this->response->denied('badiu.system.core.exec.service.keysisnotvalid',  $this->getTranslator()->trans('badiu.system.core.exec.service.message.keysisnotvalid'));
                    return $response;
                }
               
                $service=$this->getContainer()->get($skey);
                if(!method_exists($service, $function)){
                    $response= $this->response->denied('badiu.system.core.exec.service.functionnotvalid',  $this->getTranslator()->trans('badiu.system.core.exec.service.message.functionnotvalid'));
                    return $response;
                }
                $service->setSessionhashkey($this->getSessionhashkey());
                $result=$service->$function($this->param);

                $response=$this->response->accept($result);
                $this->getContainer()->get('badiu.auth.core.webservice.navegation');
                $this->initNavegationLogParam();
                $this->addlog();  
                $badiuSession->delete();

                return $response;
    }
    
   
      
          
       private function addlog() {
           if(!$this->log){return null;}
           
           $dloglib=$this->getContainer()->get('badiu.system.log.core.lib');
           $dloglib->setSessionhashkey($this->getSessionhashkey()); 
           $logtimeend=new \DateTime();
           if(!empty($this->logtimestart)){
               $start=$this->logtimestart->getTimestamp();
               $end=$logtimeend->getTimestamp();
               $timeexec=$end-$start; 
               $this->log['timeexec']=$timeexec;
           }
           $this->log['dtype']='webservice';
           $this->log['clientdevice']='server';
           $this->log['modulekey']=null;
           $this->log['moduleinstance']=null;
           $this->log['functionalitykey']=$this->getUtildata()->getVaueOfArray($this->param,'_key');;
           
           $param['rkey']='badiu.system.core.functionality.exec.service';
           $dloglib->add($this->log);
           
       }
       
        private function initNavegationLogParam() {
                 $badiuSession = $this->getContainer()->get('badiu.system.access.session');
                 $badiuSession->setHashkey($this->getSessionhashkey());
                $logparam=$badiuSession->get()->getWebserviceinfo();
                unset($logparam['cripty']);
                unset($logparam['criptykey']);
                unset($logparam['sessiondna']);
                $logparam['response']['status']='accept';
                $logparam['response']['info']='';
                $logparam['response']['message']='data successul send';
                $logparam['query']=$this->param;
                $this->log['ip']=$this->getUtildata()->getVaueOfArray($logparam,'client.ip',true);
                $this->log['dtype']='webservice';
                $this->log['modulekey']=null;
                $this->log['moduleinstance']=null;
                $this->log['functionalitykey']=$this->getUtildata()->getVaueOfArray($this->param,'_key');
                $this->log['rkey']=$this->getUtildata()->getVaueOfArray($this->param,'_service');
                $this->log['ip']=$this->getUtildata()->getVaueOfArray($logparam,'client.ip',true);
             
                $logparam=$this->getJson()->encode($logparam);
                $this->log['param']= $logparam;
                $this->log['entity']= $badiuSession->get()->getEntity();
                $this->log['action']='execservice';
        }

   
    function addRequestParamToSession($param) {
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $datasession=$badiuSession->get();
        $webserviceinfo=$datasession->getWebserviceinfo();
        $webserviceinfo['_param']=$param;
        $datasession->setWebserviceinfo($webserviceinfo);
        $badiuSession->save($datasession);

    }
    
    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }
}
