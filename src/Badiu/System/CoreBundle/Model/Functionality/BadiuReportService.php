<?php
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;

class BadiuReportService extends BadiuExternalService
{

	//private $response;
    private $kminherit;
    
    private $queryparam=null;
    public function __construct(Container $container)
    {
        parent::__construct($container);
		$this->response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
    }

   
   public function exec() {
		//$bkey=$this->getBasekey();
		$result=$this->search();
		return $this->getResponse()->get();
	}
        public function initKminherit($bkey) {
             //service of key maneger that do inherit key of service
                $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
		$keymanger->setBaseKey($bkey);
                $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
		$cbundle->setKeymanger($keymanger);
		$cbundle->initKeymangerExtend($keymanger);
                $this->kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
                $this->kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
               
        }
	public function getBasekey() {  
               $bkey=$this->getQueyParameter('bkey');
             
		if(empty($bkey)){
                        $key=$this->getQueyParameter('key');
                      	$bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
		}
		return $bkey;
	}
	 public function search() {
		$bkey=$this->getBasekey();
		$param=$this->getHttpQuerStringParam();
               
                $layout='indexCrud';
                $format='json';
                if(isset($param['layout'])){$layout=$param['layout'];}
                if(isset($param['format'])){$format=$param['format'];}
                
		if(empty($layout)){$layout='dashboard';}
                
               $this->initKminherit($bkey);
               
		if($layout=='dashboard'){
			//$report= $this->getContainer()->get($bkey.'.report');
                        $report= $this->getContainer()->get($this->getKminherit()->report());
                        $report->getKeymanger()->setBaseKey($bkey);
			$report->extractData($param,$layout);
			
			$result=$report->getDashboardData();
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
			$this->getResponse()->setMessage($result);
		}
		else if($layout=='indexCrud'){
                   
                        $type='dbsearch.fields.table.view'; 
			//$report= $this->getContainer()->get($bkey.'.report');
                         $report= $this->getContainer()->get($this->getKminherit()->report());
                         $report->getKeymanger()->setBaseKey($bkey);
			//$report->initBaseKey($bkey);
			
			$report->extractData($param,$layout,$type);
                         
			$report->makeTable($layout);
                        if($format=='xls'){$report->exportTable($format);}
                    
			$result=$report->getListData();
                        
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
			$this->getResponse()->setMessage($result);
			
		}else{
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('layout not implemented');
		}
                
		return $this->getResponse()->get();
	 }
	 
	 public function dashboard() {
               
		$bkey=$this->getBasekey();
		if(empty($bkey)){ 
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.system.core.functionality.search.bkey.is.empty');
			return $this->getResponse()->get();
		}
                
		$param=$this->getHttpQuerStringParam();
                
		$result=array();
		$search= $this->getContainer()->get('badiu.system.core.functionality.search');
		$search->getKeymanger()->setBaseKey($bkey);
		$result['row']= $search->searchListSingle($param);
		$result['rows']= $search->searchList($param);
                
                //calendar layout
                if(isset($param['_layout'])&& $param['_layout']=='calendar'){
                    $calendarlayout=$this->getContainer()->get('badiu.system.core.lib.calendar.dashboardlayout');
                    $calendarResult=$calendarlayout->makeFullCalendarLayout($result);
                    return $calendarResult;
                }else{
                     $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
                    $this->getResponse()->setMessage($result);
                    return $this->getResponse()->get();
                }
               
	}

    public function dashboardSingle() {
               
		$bkey=$this->getBasekey();
		if(empty($bkey)){ 
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.system.core.functionality.search.bkey.is.empty');
			return $this->getResponse()->get();
		}
                $param=$this->getHttpQuerStringParam();
                $this->getSearch()->getKeymanger()->setBaseKey($bkey);
                $result = $this->getSearch()->searchListSingle($param);
		
                $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
                $this->getResponse()->setMessage($result);
                return $this->getResponse()->get();
               
               
	}
         public function dashboardList() {
               
		$bkey=$this->getBasekey();
		if(empty($bkey)){ 
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.system.core.functionality.search.bkey.is.empty');
			return $this->getResponse()->get();
		}
                $param=$this->getHttpQuerStringParam();
                $this->getSearch()->getKeymanger()->setBaseKey($bkey);
                $result = $this->getSearch()->searchList($param);
		
                $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
                $this->getResponse()->setMessage($result);
                return $this->getResponse()->get();
               
               
	}
     public function findById() {
		$bkey=$this->getBasekey();
		$id=$this->getQueyParameter('id');
		if(empty($id)){ 
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.system.core.functionality.findbyid.id.is.empty');
			return $this->getResponse()->get();
		}
                
               $this->initKminherit($bkey);
               
               $data= $this->getContainer()->get($this->getKminherit()->data());
               $result= $data->findById($id);
              
                
                $objectutil=$this->getContainer()->get('badiu.system.core.lib.util.objectutil');
               $result= $objectutil->catToArray($result);
                $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($result);
              
		return $this->getResponse()->get();
	 }
    
        public function delete() {
		$bkey=$this->getBasekey();
		$id=$this->getQueyParameter('id');
		if(empty($id)){ 
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.system.core.functionality.findbyid.id.is.empty');
			return $this->getResponse()->get();
		}
                
               $this->initKminherit($bkey);
               
               $data= $this->getContainer()->get($this->getKminherit()->data());
               $result= $data->delete($id);
              
                $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($result);
              
		return $this->getResponse()->get();
	 }
    public function remove() {
		$bkey=$this->getBasekey();
		$id=$this->getQueyParameter('id');
		if(empty($id)){ 
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.system.core.functionality.findbyid.id.is.empty');
			return $this->getResponse()->get();
		}
                
               $this->initKminherit($bkey);
               
               $data= $this->getContainer()->get($this->getKminherit()->data());
               $result= $data->remove($id);
              
                $this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($result);
              
		return $this->getResponse()->get();
	 }

    function getQueyParameter($key) {
        $value=null;
        if(empty($this->queryparam)){$this->queryparam=$this->getHttpQuerStringParam();}
        if(isset($this->queryparam[$key])){$value=$this->queryparam[$key];}
        return $value;
    }        
   function getKminherit() {
       return $this->kminherit;
   }

   function setKminherit($kminherit) {
       $this->kminherit = $kminherit;
   }

   function getQueryparam() {
       return $this->queryparam;
   }

   function setQueryparam($queryparam) {
       $this->queryparam = $queryparam;
   }


}
