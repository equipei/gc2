<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class BadiuInstall extends BadiuModelLib {
       /**
     * @var array
     */
    private $forceupdate;
   
     function __construct(Container $container) {
               parent::__construct($container);
				$this->forceupdate=false;
				
				$this->forceupdate=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_forceupdate');
           }
 

function getForceupdate() {
    return $this->forceupdate;
}

function setForceupdate($forceupdate) {
    $this->forceupdate = $forceupdate;
}




}
