<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger;

/**
 * Description of BadiuKeyManger
 * @author lino
 */
class BadiuKeyMangerInherit{
    
  /**
     * @var Container
     */
    private $container;

    /**
     * @var Container
     */
    private $keymanger;
    /**
     * @var Container
     */
    private $keymangerExtend;
    function __construct(Container $container) {
        $this->container=$container;

    }


    public function init(BadiuKeyManger $keymanger,BadiuKeyManger $keymangerExtend) {
       $this->keymanger=$keymanger;
        $this->keymangerExtend=$keymangerExtend;
    }

    public function moduleMenu(){

       $serviceKey= $this->getKeymanger()->moduleMenu();
       if(empty($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->moduleMenu();
        }
        return $serviceKey;
    }

    public function controller(){
        $serviceKey= $this->getKeymanger()->controller();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->controller();
        }
        return $serviceKey;
    }
 public function formcontroller(){
        $serviceKey= $this->getKeymanger()->formcontroller();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->formcontroller();
        }
        return $serviceKey;
    }

    public function report(){
        $serviceKey= $this->getKeymanger()->report();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->report();
        }
        return $serviceKey;
    }
    public function reportController($level=0){
        $serviceKey= $this->getKeymanger()->reportController($level);
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->reportController($level);
        }
        return $serviceKey;
    }
     public function reportcontent(){
        $serviceKey= $this->getKeymanger()->reportcontent();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->reportcontent();
        }
        return $serviceKey;
    }
     public function htmlcontent(){
        $serviceKey= $this->getKeymanger()->htmlcontent();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->htmlcontent();
        }
        return $serviceKey;
    }
	
	public function outputchangedata(){
        $serviceKey= $this->getKeymanger()->outputchangedata();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->outputchangedata();
        }
	  return $serviceKey;
    }
        public function reportchangedata(){
        $serviceKey= $this->getKeymanger()->reportchangedata();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->reportchangedata();
        }
        return $serviceKey;
    }
    public function routeIndex(){
        $serviceKey= $this->getKeymanger()->routeIndex();
        if(empty($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->routeIndex();
        }
        return $serviceKey;
    }

    public function routeAdd(){
        $serviceKey= $this->getKeymanger()->routeAdd();
        if(empty($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->routeAdd();
        }
        return $serviceKey;
    }

    public function routeEdit(){
        $serviceKey= $this->getKeymanger()->routeEdit();
        if(empty($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->routeEdit();
        }
        return $serviceKey;
    }

    public function cron(){
        $serviceKey= $this->getKeymanger()->cron();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->cron();
        }
        return $serviceKey;
    }
    public function session(){
        $serviceKey= $this->getKeymanger()->session();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->session();
        }
        return $serviceKey;
    }
     public function checkform(){
        $serviceKey= $this->getKeymanger()->checkform();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->checkform();
        }
        return $serviceKey;
    }
    public function defaultcheckform(){
        $serviceKey= $this->getKeymanger()->defaultcheckform();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->defaultcheckform();
        }
        return $serviceKey;
    }
     public function data(){
        $serviceKey= $this->getKeymanger()->data();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->data();
        }
        return $serviceKey;
    }
    public function entity(){
        $serviceKey= $this->getKeymanger()->entity();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->entity();
        }
        return $serviceKey;
    }

    public function formType(){
        $serviceKey= $this->getKeymanger()->formType();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->formType();
        }
        return $serviceKey;
    }
    public function formFilterType(){
        $serviceKey= $this->getKeymanger()->formFilterType();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->formFilterType();
        }
        return $serviceKey;
    }
    public function filter(){
        $serviceKey= $this->getKeymanger()-> filter();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->filter();
        }
        return $serviceKey;
    }
    public function filtersql(){
        $serviceKey= $this->getKeymanger()-> filtersql();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->filtersql();
        }
        return $serviceKey;
    }
    public function schedulerexpression(){
        $serviceKey= $this->getKeymanger()-> schedulerexpression();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->schedulerexpression();
        }
        return $serviceKey;
    }
    public function schedulerexpressionparent($level=1){
        $serviceKey= $this->getKeymanger()->schedulerexpressionparent($level);
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->schedulerexpressionparent($level);
        }
        return $serviceKey;
    }
    public function formDataOptions(){
        $serviceKey= $this->getKeymanger()->formDataOptions();
        if(!$this->container->has($serviceKey)){
            $serviceKey= $this->getKeymangerExtend()->formDataOptions();
        }
        return $serviceKey;
    }
    
     public function generalService($serviceKey){
        if(!$this->container->has($serviceKey)){
            $serviceKey=str_replace($this->getKeymanger()->getBaseKey(),$this->getKeymangerExtend()->getBaseKey(),$serviceKey);
        }
         return $serviceKey; 
    }
    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param Container $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return Container
     */
    public function getKeymangerExtend()
    {
        return $this->keymangerExtend;
    }

    /**
     * @param Container $keymangerExtend
     */
    public function setKeymangerExtend($keymangerExtend)
    {
        $this->keymangerExtend = $keymangerExtend;
    }

    /**
     * @return Container
     */
    public function getKeymanger()
    {
        return $this->keymanger;
    }

    /**
     * @param Container $keymanger
     */
    public function setKeymanger($keymanger)
    {
        $this->keymanger = $keymanger;
    }



 
}
