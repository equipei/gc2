<?php
namespace Badiu\System\CoreBundle\Model\Functionality;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;

class BadiuExternalService extends BadiuModelLib
{
/**
     * @var Container
     */
    private $container;

	private $response;
	
	private $serviceid;
	function __construct(Container $container)
    {
        parent::__construct($container);
		$this->response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
      }

	  //review it
	public function exec()
    {	
		//get param
	/*	$param=$this->getParam();
		
		$checkParam=$this->checkParam($param);
		if(empty($chkParam)){return $this->getResponse()->get();}
		
	
		$chekToken=$this->checkToken($param->token);
		if(!$chekToken){return $this->getResponse()->get();}
		
		//exec service
						
		return $response;*/
    }
	
	public function getParam(){
		//$param= new \stdClass();
		//$param->token=$this->getContainer()->get('request')->get('_token');
		
		//return $param;
	}
	
	public function checkParam($param){
		/*$resp=TRUE;
		if(empty($param->token)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo(SERVICEDOMAINTABLE::$ERROR_TOKEN_EMPTY);
		}*/
	
	}
        public function getToken(){
		//$param=$this->getHttpQuerStringParam(); //deu erro rever
                //$token=$this->getUtildata()->getVaueOfArray($param, '_token');
               $token=null; 
                $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
                $jparam=file_get_contents('php://input');
                $param=$json->decode($jparam, true); 
                if(!empty($param) && array_key_exists('_token',$param)){$token=$param['_token'];}
                if(empty($token)){$token=$this->getContainer()->get('request')->get('_token');}
                return $token;
	}
	public function checkToken() 
    {          
		$resp=TRUE;
                $token=$this->getToken();
                
		if(empty($token)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo(SERVICEDOMAINTABLE::$ERROR_TOKEN_EMPTY);
			return $resp;
		}
		
		$stoken=$this->getContainer()->get('badiu.system.core.lib.util.token');
		$validate=$stoken->serverServiceValidade($token);
		
		
		if(!$validate) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo(SERVICEDOMAINTABLE::$ERROR_TOKEN_NOT_VALID);
			return $resp;
		}
		
		return $resp;
    }
	
	public function getServiceid(){
			$token=$this->getToken();
                        if(empty($token))return null;
                        $servicedata=$this->getContainer()->get('badiu.admin.server.service.data');
                        $result=$servicedata->getIdByToken($token);
                        return $result;
	}
	public function getService(){
			$token=$this->getToken();
		$servicedata=$this->getContainer()->get('badiu.admin.server.service.data');
		$result=$servicedata->findByToken($token);
		return $result;
	}
	
		public function getHttpQuerStringParam() {
                    $isparamjson=true;
                        $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
                        $jparam=file_get_contents('php://input');
                        $param=$json->decode($jparam, true);
                       
                        if(empty($param)){
                            $queryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
                            $queryString->setQuery($_SERVER["QUERY_STRING"]);
                            $queryString->makeParam();
                            $param=$queryString->getParam();
                            $isparamjson=false;
                          
                        }
                        if(!$isparamjson && empty($_SERVER["QUERY_STRING"])){ // for post
                            $param=$_POST;
                            $isparamjson=false;
                        } 
		     
			$nparam=array();
                     
			foreach ($param as $key => $value){
                                if(!$isparamjson){$value=$this->getContainer()->get('request')->get($key);}
				$value=$this->converToArrayParamFilter($key,$value);
				//clean ___ for key
				$key= str_replace('___', '', $key);
				$nparam[$key]=$value;
				 
			}
			$nparam['entity']=$this->getEntity();
                        $nparam['entity1']=$this->getEntity(); //for select in picklist with option not in
                        
                        //gsearch if exist add gsearch1 it is necessary for select in picklist with option not in
                      /*  if (array_key_exists('gsearch',$nparam)){
                             $nparam['gsearch1']= $nparam['gsearch'];
                        }*/
                      
			$nparam=$this->addParamServerServiceid($nparam);
			return $nparam;
		} 
			//conver ___key to array
		public function converToArrayParamFilter($key,$value) {
				if(empty($value)) return $value;
				$pos=stripos($key, "___");
				if($pos!== false){
					$posv=stripos($value, ",");
					if($posv=== false){$value=array($value);}
					else {$value=explode(",",$value);}
				}
				return $value;
		}
		public function addParamServerServiceid($nparam) {
                        
                        
			//check if client request for all server service
			if (array_key_exists("contextsservice",$nparam) && $nparam['contextsservice']='allservice'){ return $nparam;}
			else {
				$nparam['sserviceid']=$this->getServiceid();
				return $nparam;
			}
			return $nparam;
		}
		
	 public function getResponse() {
          return $this->response;
      }


      public function setResponse($response) {
          $this->response = $response;
      }
	  
	
}
