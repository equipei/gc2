<?php

namespace Badiu\System\CoreBundle\Model\Functionality;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class BadiuSearch  {
    
    /**
     * @var Container
     */
    private $container;
     /**
     * @var EntityManager
     */
    private $em;
         
       /**
      * @var BadiuKeyManger
     */
    private $keymanger;
	
	/**
      * @var string
     */
     private $datasource='dbdoctrine';// dbdoctrine|servicesql|webservice | mongodb

     /**
      * @var integer
     */
     private $serviceid;
     
	 /**
      * @var array
     */
     private $serviceconf=null;
     
     /**
      * @var integer
     */
     private $paginglimit=10;
     private $utildata;
  
          /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
	private $systemdata;
     function __construct(Container $container,$baseKey=null) {
                $this->container=$container;
                $db="default";//$this->container->get('badiu.system.access.session')->get()->getDbapp(); 
                $this->em=$this->container->get('doctrine')->getEntityManager($db); 
               
                $this->keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
                $this->utildata=$this->getContainer()->get('badiu.system.core.lib.util.data');
               
                if($baseKey!=null){$this->keymanger->setBaseKey($baseKey);}
				
				//$datasource=$this->container->get('request')->get('_datasource');
				$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_datasource');
		
				if(!empty($datasource)){$this->datasource=$datasource;}
				$this->initDatasourceService();

              
      }
     
      public function searchCount($psql){
		 
         $this->initDatasourceService($psql);	
		 
          $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
		  $cbundle->setSystemdata($this->getSystemdata());
          $cbundle->setSessionhashkey($this->getSessionhashkey());
     $cbundle->setKeymanger($this->getKeymanger());
	  
          $sqlconfig=$cbundle->getValue($this->getKeymanger()->dbsearchSqlCount());
         
          if(empty($sqlconfig)){
              $cbundle->initKeymangerExtend($this->getKeymanger());
              $sqlconfig=$cbundle->getValue($cbundle->getKeymangerExtend()->dbsearchSqlCount());
         }
         if(empty($sqlconfig)){return null;}
         
        $this->checkDatasourceTypeByParamConfig($sqlconfig);
        
	
          $factoryquery=null;
          $sql=null;
          $mongodbfactoryconn=null;
         
          if($this->datasource=='mongodb'){
              $mongodbfactoryconn=$this->getContainer()->get('badiu.system.core.lib.mongodb.factoryconnection');
              $mongodbfactoryconn->initConfig($sqlconfig,$psql);
          }else{
			  
			   $factorysqlexpressions=$this->getContainer()->get('badiu.system.core.lib.sql.factorysqlexpressions');
			   $psql=$factorysqlexpressions->filter($psql);
               $factoryquery=$this->getContainer()->get('badiu.system.core.lib.sql.factoryquery');
               $factoryquery->setSessionhashkey($this->getSessionhashkey());
			   $factoryquery->setSystemdata($this->getSystemdata());
			   $sql=$factoryquery->makeSqlSearch($sqlconfig,$psql,$this->datasource);
               
          }
         
          
		  $result=null;
                 // 	print_r($psql);  
		 //echo "sql sc ".$sql;exit; 
		  if($this->datasource=='dbdoctrine'){
			  $psql=$this->castParamTypeDb($psql);
			  $sql=$this->castParamTypeDbSql($sql);
				$query = $this->getEm()->createQuery($sql);
				
				$query=$factoryquery->makeParamFilter($query,$sql,$psql);
				$result= $query->getSingleResult();
                               
			}
		else if($this->datasource=='servicesql'){
                $result=$this->connSqlService($sql);
			 /*  if($this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid')==38){
                    echo "<pre>";
                    print_r( $result); 
                    echo "</pre>";exit;  
                   }*/
                            // print_r($result);exit;
                                $result=$result['message'];
                         
			}
                else if($this->datasource=='mongodb'){
			
				$result=$mongodbfactoryconn->count();
                                return $result;
            }
                 if($result!=null && is_array($result) && array_key_exists('countrecord',$result)){$result= $result['countrecord'];}
                $result=$this->changeData($result,'searchCount');
              
          return $result;
       }
  
       public function search($psql){
           $this->initDatasourceService($psql);
         
               $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
               $cbundle->setSessionhashkey($this->getSessionhashkey());
			   $cbundle->setSystemdata($this->getSystemdata());
			   $cbundle->setKeymanger($this->getKeymanger());
               $sqlconfig=$cbundle->getValue($this->getKeymanger()->dbsearchSql());
				
			//limit
				//$badiuSession= $this->getContainer()->get('badiu.system.access.session');
              //  $badiuSession->setHashkey($this->getSessionhashkey());                    
                     
			  // if(empty($this->paginglimit)){$this->paginglimit=$badiuSession->get()->getConfig()->getValue('badiu.system.core.param.config.report.paginationlimitdbrows');}
             //  if(empty($this->paginglimit)){$this->paginglimit=10;}
			   
              if(empty($sqlconfig)){
                    $cbundle->initKeymangerExtend($this->getKeymanger());
                    $sqlconfig=$cbundle->getValue($cbundle->getKeymangerExtend()->dbsearchSql());
              }
             
            //  echo "sql: ". $sqlconfig;
              if(empty($sqlconfig)){return null;}
               $this->checkDatasourceTypeByParamConfig($sqlconfig);
               $factoryquery=null;
                $sql=null;
                $mongodbfactoryconn=null;
               
                if($this->datasource=='mongodb'){
                    $mongodbfactoryconn=$this->getContainer()->get('badiu.system.core.lib.mongodb.factoryconnection');
                    $mongodbfactoryconn->initConfig($sqlconfig,$psql);
                    
                }else{
                     $factorysqlexpressions=$this->getContainer()->get('badiu.system.core.lib.sql.factorysqlexpressions');
					 $psql=$factorysqlexpressions->filter($psql);
					 
					 $factoryquery=$this->getContainer()->get('badiu.system.core.lib.sql.factoryquery');
                     $factoryquery->setSessionhashkey($this->getSessionhashkey());
					 $factoryquery->setSystemdata($this->getSystemdata());
                     $sql=$factoryquery->makeSqlSearch($sqlconfig,$psql,$this->datasource); 
                     

                 //   $sg=$this->getContainer()->get('badiu.system.access.globlasession');
                 //   print_r($sg->getData());exit;
                //  echo "<hr>". $sql;exit;   
                }
               
                        	$x=$psql['_page'];
                                $y=$this->getPaginglimit();
                                //if(isset($psql['_maxrow'])&& !empty($psql['_maxrow'])){$y=$psql['_maxrow'];}
				
				if($x>0){$x=$x*$y;}
				/*if(isset($psql['_format']) && $psql['_format']=='xls'){
					$x=0;
					$y=2500; 
                    $badiuSession= $this->getContainer()->get('badiu.system.access.session');
                    $badiuSession->setHashkey($this->getSessionhashkey());                    
                    $paginationlimitdbrowstoexport =  $badiuSession->get()->getConfig()->getValue('badiu.system.core.param.config.report.paginationlimitdbrowstoexport');
                                       
                    $y=$paginationlimitdbrowstoexport;
                    //if(!empty($paginationlimitdbrowstoexport)){$y=24000;}
				}*/
             $result=null;
            
			  if($this->datasource=='dbdoctrine'){
				   $psql=$this->castParamTypeDb($psql);
				   $sql=$this->castParamTypeDbSql($sql);
				 // echo $sql;exit;
					$query = $this->getEm()->createQuery($sql);
					$query=$factoryquery->makeParamFilter($query,$sql,$psql);
					//echo $query->getSQL();EXIT;
					 $query->setFirstResult($x);
					$query->setMaxResults($y);
                    $result= $query->getResult();
                    
                                       // print_r($result);exit;
				}
				else if($this->datasource=='servicesql'){
					
                   	$x=$psql['_page']; 
                    $result=$this->connSqlService($sql,'M',$x,$y);
                    $result=$result['message'];
                                       
                                 /* echo "<pre>";
                                       print_r( $result); 
                                       echo "</pre>";exit;  */
				}
               
                            else if($this->datasource=='mongodb'){
			
				$result=$mongodbfactoryconn->find($x,$y);
                                return $result;
                            }
                          
               $result=$this->changeData($result,'search');
               return $result;
       } 
      

      public function searchListSingle($psql,$type=null){
		
		$this->initDatasourceService($psql);
          $result=array(); 
          $hasNex=true;
          $cont=1;
          $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
          $cbundle->setSessionhashkey($this->getSessionhashkey());
		  $cbundle->setSystemdata($this->getSystemdata());
	 $cbundle->setKeymanger($this->getKeymanger());
          $cbundle->initKeymangerExtend($this->getKeymanger());
		  
		  $factorysqlexpressions=$this->getContainer()->get('badiu.system.core.lib.sql.factorysqlexpressions');
		  $psql=$factorysqlexpressions->filter($psql);
			   
          $factoryquery=$this->getContainer()->get('badiu.system.core.lib.sql.factoryquery');
          $factoryquery->setSessionhashkey($this->getSessionhashkey());
		  $factoryquery->setSystemdata($this->getSystemdata());

             $psqlconfigkey=$this->getKeymanger()->dashboardDbSqlSingleConfig($type);
         
          $psqlconfig=$sqlconfig=$cbundle->getValue($psqlconfigkey);
          if(empty($psqlconfig)){
              $psqlconfigkey=$cbundle->getKeymangerExtend()->dashboardDbSqlConfig($type);
              $psqlconfig=$cbundle->getValue($psqlconfigkey);
          }
           
          while ($hasNex){
		 
              $sqlconfig=$cbundle->getValue($this->getKeymanger()->dashboardDbSqlSingle($cont,$type));
              
              if(empty($sqlconfig)){
                 $sqlconfig=$cbundle->getValue($cbundle->getKeymangerExtend()->dashboardDbSqlSingle($cont,$type));
              }
              
              $labelconfig=$cbundle->getValue($this->getKeymanger()->dashboardDbSqlSingleLabel($cont,$type));
              if(empty($labelconfig)){
                 $labelconfig=$cbundle->getValue($cbundle->getKeymangerExtend()->dashboardDbSqlSingleLabel($cont,$type));
              }
              
               $formatconfig=$cbundle->getValue($this->getKeymanger()->dashboardDbSqlSingleFormat($cont,$type));
              if(empty($formatconfig)){
                 $formatconfig=$cbundle->getValue($cbundle->getKeymangerExtend()->dashboardDbSqlSingleFormat($cont,$type));
              }
              
               $viewconfig=$cbundle->getValue($this->getKeymanger()->dashboardDbSqlSingleView($cont,$type));
              if(empty($viewconfig)){
                 $viewconfig=$cbundle->getValue($cbundle->getKeymangerExtend()->dashboardDbSqlSingleView($cont,$type));
              }
              $dashboarddata=$this->getContainer()->get('badiu.system.core.lib.dashboard.dashboarddata');
              $dashboarddata->setSessionhashkey($this->getSessionhashkey());
              $dashboarddata->setSystemdata($this->getSystemdata());
              if(empty($sqlconfig)){ $hasNex=false;}
              else{
					 $sql=$factoryquery->makeSqlSearch($sqlconfig,$psql,$this->datasource);
				
					if($this->datasource=='dbdoctrine'){
                         $psql=$this->castParamTypeDb($psql);
						$sql=$this->castParamTypeDbSql($sql);						 
						$query = $this->getEm()->createQuery($sql);
						$query=$factoryquery->makeParamFilter($query,$sql,$psql);
						$row= $query->getOneOrNullResult();
						
                                                $row=$dashboarddata->formatSingle($formatconfig,$viewconfig,$row);
                                                $result['data'][$cont]=$row;
                                                 $labeldata=$dashboarddata->label($labelconfig,$viewconfig);
                                                $result['label'][$cont]=$labeldata;
                                                $titledata=$dashboarddata->title($psqlconfig,$cont,true);
                                                $result['title'][$cont]=$titledata;
                                                $result['dtype'][$cont]=$dashboarddata->ckey($psqlconfig,$cont,'sdtype');
					}
					else if($this->datasource=='servicesql'){
						$row=$this->connSqlService($sql);
                                                $row=$row['message'];
                                                $row=$dashboarddata->formatSingle($formatconfig,$viewconfig,$row);
						$result['data'][$cont]=$row;
                                                $labeldata=$dashboarddata->label($labelconfig,$viewconfig);
                                                $result['label'][$cont]=$labeldata;
                                                $titledata=$dashboarddata->title($psqlconfig,$cont,true);
                                                $result['title'][$cont]=$titledata;
												$result['dtype'][$cont]=$dashboarddata->ckey($psqlconfig,$cont,'sdtype');
					}
               }
               $cont++;

          }
         
             $result=$this->changeData($result,'searchListSingle');
          
          return $result;
        }
 
      public function searchList($psql,$type=null){
			$this->initDatasourceService($psql);
          $result=array();
          $hasNex=true;
		  $cont=1;
		  $sqlindex=null;
		  if(isset($psql['_sqlindex'])){
			  $sqlindex=$psql['_sqlindex'];
			  $cont=$sqlindex;
		   } 
		 
          $dashboarddata=$this->getContainer()->get('badiu.system.core.lib.dashboard.dashboarddata');
		  $dashboarddata->setSessionhashkey($this->getSessionhashkey());
		  $dashboarddata->setSystemdata($this->getSystemdata());
          $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
		  $cbundle->setSystemdata($this->getSystemdata());
          $cbundle->setSessionhashkey($this->getSessionhashkey());
	  $cbundle->setKeymanger($this->getKeymanger());
          $cbundle->initKeymangerExtend($this->getKeymanger());
		  
		  $factorysqlexpressions=$this->getContainer()->get('badiu.system.core.lib.sql.factorysqlexpressions');
		  $psql=$factorysqlexpressions->filter($psql);
          $factoryquery=$this->getContainer()->get('badiu.system.core.lib.sql.factoryquery');
          $factoryquery->setSessionhashkey($this->getSessionhashkey());
		  $factoryquery->setSystemdata($this->getSystemdata());
          $psqlconfigkey=$this->getKeymanger()->dashboardDbSqlConfig($type);
          $psqlconfig=$sqlconfig=$cbundle->getValue($psqlconfigkey);
          if(empty($psqlconfig)){
              $psqlconfigkey=$cbundle->getKeymangerExtend()->dashboardDbSqlConfig($type);
              $psqlconfig=$cbundle->getValue($psqlconfigkey);
          }

          $querystring=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
          $querystring->setQuery($psqlconfig);
          $querystring->makeParam();
          
          
          
         
          while ($hasNex){
              
               $labelconfig=$cbundle->getValue($this->getKeymanger()->dashboardDbSqlLabel($cont,$type));
              if(empty($labelconfig)){
                 $labelconfig=$cbundle->getValue($cbundle->getKeymangerExtend()->dashboardDbSqlLabel($cont,$type));
              }
            
            $formatconfig=$cbundle->getValue($this->getKeymanger()->dashboardDbSqlFormat($cont,$type));
           if(empty($formatconfig)){
                 $formatconfig=$cbundle->getValue($cbundle->getKeymangerExtend()->dashboardDbSqlFormat($cont,$type));
            }
          
          $viewconfig=$cbundle->getValue($this->getKeymanger()->dashboardDbSqlView($cont,$type));
           if(empty($viewconfig)){
                 $viewconfig=$cbundle->getValue($cbundle->getKeymangerExtend()->dashboardDbSqlView($cont,$type));
            }
            
              $sqlconfig=$cbundle->getValue($this->getKeymanger()->dashboardDbSql($cont,$type));
		
              if(empty($sqlconfig)){
                 $sqlconfig=$cbundle->getValue($cbundle->getKeymangerExtend()->dashboardDbSql($cont,$type));
              }
			 
			   
              if(empty($sqlconfig)){ $hasNex=false;}
              else{
              
				//  $x=$psql['_page']; //review it
				//   $y=100;
                 // if($x>0){$x=$x*$y;}
                 // $query->setFirstResult($x); 
                 // $query->setMaxResults($y);
					$x=0;
                                        $y=$this->getPaginglimit();
                                        $keypaginationlimit="paginationlimitdbrows$cont";
					$paginationlimit=$querystring->getValue($keypaginationlimit);
                                        if(!empty($paginationlimit)&& is_numeric($paginationlimit)){
                                            $y=$paginationlimit;
                                        }
                                       
					$sql=$factoryquery->makeSqlSearch($sqlconfig,$psql,$this->datasource);
					if($this->datasource=='dbdoctrine'){
						 $psql=$this->castParamTypeDb($psql);
						 $sql=$this->castParamTypeDbSql($sql);
						 
						$query = $this->getEm()->createQuery($sql);
						$query=$factoryquery->makeParamFilter($query,$sql,$psql);
                // echo $sql;exit;
						$query->setFirstResult($x);
						$query->setMaxResults($y);
						$rows= $query->getResult();
					
                                                $rows=$dashboarddata->formatList($formatconfig,$viewconfig,$rows);
                                                $rows=$dashboarddata->confg($cont,$rows, $querystring,$psqlconfigkey);
												
                                                $labeldata=$dashboarddata->label($labelconfig,$viewconfig);
												$result['label'][$cont]=$labeldata;
												$result['data'][$cont]=$rows;
                                                $titledata=$dashboarddata->title($psqlconfig,$cont);
                                                $result['title'][$cont]=$titledata;
												$result['dtype'][$cont]=$dashboarddata->ckey($psqlconfig,$cont,'dtype');
												$result['route'][$cont]=$dashboarddata->ckey($psqlconfig,$cont,'route');
                                               
					}
					else if($this->datasource=='servicesql'){
							//$x=$psql['_page']; //review it
                                                $x=0;
                                                $y=$this->getPaginglimit();
                                                if(isset($psql['_maxrow'])&& !empty($psql['_maxrow'])){$y=$psql['_maxrow'];}
						//review change 5000 to $this->getPaginglimit();	
                                                if($y <= 100){$y=2500; };//review
                                                if(!empty($paginationlimit)&& is_numeric($paginationlimit)){
                                                    $y=$paginationlimit;
                                                }
						$rows=$this->connSqlService($sql,'M',$x,$y);//review
                                                $rows=$rows['message'];
						 $rows=$dashboarddata->formatList($formatconfig,$viewconfig,$rows);
                                                $rows=$dashboarddata->confg($cont,$rows, $querystring,$psqlconfigkey);
                                                 $labeldata=$dashboarddata->label($labelconfig,$viewconfig);
                                                $result['label'][$cont]=$labeldata;
						$result['data'][$cont]=$rows;
                                                $titledata=$dashboarddata->title($psqlconfig,$cont);
                                                $result['title'][$cont]=$titledata; 
												$result['dtype'][$cont]=$dashboarddata->ckey($psqlconfig,$cont,'dtype');
												$result['route'][$cont]=$dashboarddata->ckey($psqlconfig,$cont,'route');
					}
				
                 
              }
               $cont++;
			   if($sqlindex){$hasNex=false;}
          }
	
       /* echo "<pre>";
          print_r($result);
           echo "</pre>";exit;*/
           $result=$this->changeData($result,'searchList');
          return $result;
        }

       
      
       public function find($psql){
          
           $this->initDatasourceService($psql);
          $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
          $cbundle->setSessionhashkey($this->getSessionhashkey());
		  $cbundle->setSystemdata($this->getSystemdata());
          $sqlconfig=$cbundle->getValue($this->getKeymanger()->dbgetrowSql());

          if(empty($sqlconfig)){
              $cbundle->initKeymangerExtend($this->getKeymanger());
              $sqlconfig=$cbundle->getValue($cbundle->getKeymangerExtend()->dbgetrowSql());

          }
          $factorysqlexpressions=$this->getContainer()->get('badiu.system.core.lib.sql.factorysqlexpressions');
		  $psql=$factorysqlexpressions->filter($psql);
			   
          $factoryquery=$this->getContainer()->get('badiu.system.core.lib.sql.factoryquery');
          $factoryquery->setSessionhashkey($this->getSessionhashkey());
		  $factoryquery->setSystemdata($this->getSystemdata());
          $sql=$factoryquery->makeSqlSearch($sqlconfig,$psql,$this->datasource);
          $query = $this->getEm()->createQuery($sql);
          $query=$factoryquery->makeParamFilter($query,$sql,$psql);
 
          //$result= $query->getSingleResult();
          $result= $query->getResult();
       
          $result=$this->changeData($result,'find');
          return $result;
       }


        public function changeData($data,$function){
          
            $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $cbundle->setSessionhashkey($this->getSessionhashkey());
			$cbundle->setSystemdata($this->getSystemdata());
            $cbundle->setKeymanger($this->getKeymanger());
            $cbundle->initKeymangerExtend($this->getKeymanger());
            $kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
            $kminherit->init($this->getKeymanger(),$cbundle->getKeymangerExtend());
     
           $reportchangedatakey=$kminherit->reportchangedata();
          
      
           if (!empty($reportchangedatakey) && $this->getContainer()->has($reportchangedatakey)) {
              
               $reportchangedata=$this->getContainer()->get($reportchangedatakey);
             if (method_exists($reportchangedata, $function)) {
                 
                 $data =$reportchangedata->$function($data);
             }
         }
       
         return $data;
        }
         
	public function initDatasourceService($param=null) {
			
          if(!$this->getContainer()->has('badiu.admin.server.service.data')){return null;}
           
		   $data=$this->getContainer()->get('badiu.admin.server.service.data');      
		  
		  //force update by param
		  if(isset($param['_serviceidfocerupdate']) && $param['_serviceidfocerupdate']==1 &&  isset($param['_serviceid'])){
			  $serviceid=$param['_serviceid'];
			  $this->datasource=$param['_datasource'];
			  $result=$data->getInfoOfServiceForDs($serviceid);
              $this->setServiceconf($result);
			  return null;
		  }
		   //init by default param

           $forceupadeservice=true;
           //if(empty($this->serviceconf)){ $forceupadeservice=false;}

           $internserviceid=$this->getUtildata()->getVaueOfArray($this->serviceconf,'id');
           $paramserviceid=$this->getUtildata()->getVaueOfArray($this->serviceconf,'_serviceid');
           if(!empty($internserviceid) && !empty($paramserviceid) && ($internserviceid=$paramserviceid)){$forceupadeservice=false;}
		   if(!empty($param) && $forceupadeservice){
				if(isset($param['_datasource']) && $param['_datasource']=='servicesql' && isset($param['_serviceid']) && $param['_serviceid'] > 0  ) {
					$serviceid=$param['_serviceid'];
					$this->datasource=$param['_datasource'];
					$result=$data->getInfoOfServiceForDs($serviceid);
                    $this->setServiceconf($result);
					return null;
				}
			}
			
			 if($this->datasource=='dbdoctrine') {return null;}
		  //get serviceid
              
		  //$serviceid=$this->container->get('request')->get('_serviceid');
		  $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
           
           if(empty($serviceid)){$serviceid=$this->serviceid;}
                 
                  //if has no param get parentid. its for bundle like mereport
		 if(empty($serviceid)){$serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');}
		 
          if(empty($serviceid) &&  isset($param['_serviceid'] )) {$serviceid=$param['_serviceid'];}
		  $data=$this->getContainer()->get('badiu.admin.server.service.data');
           $result=$data->getInfoOfServiceForDs($serviceid);
		   $this->setServiceconf($result);
		 
		  return $result;
		  
	}
        //method get
	public function connSqlService($sql,$type='S',$offset=null,$limit=null) {
		
                $sseriviceid=$this->serviceconf['id'];
		        $siteurl=$this->serviceconf['url'];
		        $sitesevice=$this->serviceconf['servicerurl']; 
		        $token=$this->serviceconf['servicetoken'];
                $host=$siteurl.$sitesevice;
                $param=array('token'=>$token,'query'=>$sql,'type'=>$type,'offset'=>$offset,'limit'=>$limit);
                $result =$this->connClienteService($param,$host);
                return $result;
        }
        public function connKeyService($param) {
            $this->datasource='servicesql';
            $this->setServiceid($param['_serviceid']);
            $this->initDatasourceService();
            unset($param['_serviceid']);
            $sseriviceid=$this->serviceconf['id'];
            $siteurl=$this->serviceconf['url'];
            $sitesevice=$this->serviceconf['servicerurl']; 
            $token=$this->serviceconf['servicetoken'];
            $host=$siteurl.$sitesevice;
            $param['token']=$token;
           // $param['_token']=$token;
           
            $result =$this->connClienteService($param,$host);
            return $result;
    }
        public function connClienteService($param,$host) {
		  
           //cript msg 
           $execript=false;
           $tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
           $tcript->setSessionhashkey($this->getSessionhashkey());
           $badiuSession = $this->getContainer()->get('badiu.system.access.session');
           $badiuSession->setHashkey($this->getSessionhashkey());
		   $badiuSession->setSystemdata($this->getSystemdata());
           $criptinfo= $badiuSession->get()->getWebserviceinfo();
      
           $criptykey=$this->getUtildata()->getVaueOfArray($criptinfo,'criptykey');
           $templatetcript=$this->getUtildata()->getVaueOfArray($criptinfo,'cripty');
           $sessionid=$this->getUtildata()->getVaueOfArray($criptinfo,'tkey');
          
           $typekey='k1';
           if(!empty($criptykey)){$typekey='ks';}
           unset($criptinfo['client']);
           unset($criptinfo['user']);
          if($templatetcript==100){
               $execript=true;
           }
          $paramjson = json_encode($param);
         
          if($execript){
             $paramjson=$tcript->encode($typekey,$paramjson,$sessionid);
          }
       
             $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $host);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $paramjson);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 100);
            
            $result = curl_exec($ch);
            
            if($execript){
                 $result =$tcript->decode($result,$criptykey);
            }
         
            curl_close($ch);
            $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
           
            $result =$json->decode($result, true);
           
            return $result;
    }
        //post
       /* public function connSqlService($sql,$type='S',$x=null,$y=null) {
            
                $param=array();
                $sql=urlencode($sql); 
                $param['t']=$type;
                if($type!='S'){
                     $param['x']=$x;
                     $param['y']=$y;
                }
               $param['q']=$sql;
         
                
		$siteurl=$this->serviceconf['url'];
		//if wordpress check ifter
                $sitesevice="/report/badiunet/synchttp/expdata.php";
                $host=$siteurl.$sitesevice;
                
                $paramjson = json_encode($param);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $host);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $paramjson);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                
                curl_close($ch);
                $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
		$result =$json->decode($result, true);
               
                $result=$result['message'];
		return $result;
	
	}*/
        public function connWebService($param) {
		$siteurl=$this->serviceconf['url'];
                $sitesevice=$this->serviceconf['servicerurl']; 
                if($this->serviceconf['dtype']=='site_moodle'){ $sitesevice=$this->serviceconf['servicerurl1'];}
                $param['_token']=$this->serviceconf['servicetoken'];
                if($this->serviceconf['dtype']=='site_wordpress'){  $param['token']=$this->serviceconf['servicetoken']; }//delete after change wordpress plugin
		//if wordpress check ifter add on database
                //$sitesevice="/?badiuws";//review this
               // if(isset($param['servicerurl']) && !empty($param['servicerurl'])){ $sitesevice=$param['servicerurl'];}
                $host=$siteurl.$sitesevice;
          
                unset($param['servicerurl']);
                unset($param['_serviceid']);
                unset($param['_service']);
                 unset($param['_function']);
                 
                $paramjson = json_encode($param);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $host);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $paramjson);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_MAXREDIRS, 100);
                $result = curl_exec($ch);
                
                curl_close($ch);
                $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
		$result =$json->decode($result, true);
                return $result;
                
	
	}
        // DELETED review it. //usado no sistema pa
         public function getDataFromWebService($param) {
              $this->setDatasource('webservice');
              $this->setServiceid($param['_serviceid']);
              $this->initDatasourceService();
              $result=  $this->connWebService($param);
              return $result;
         }
  public function execWebService($param) {
              $this->setDatasource('webservice');
              $this->setServiceid($param['_serviceid']);
              $this->initDatasourceService();
              $result=  $this->connWebService($param);
              return $result;
         }
    //delete this change ne to execWebService
     public function searchWebService($param=null){
          $this->datasource='webservice';
         if(empty($param)){
                $queryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
                $queryString->setQuery($_SERVER["QUERY_STRING"]);
                $queryString->makeParam();
                $param=$queryString->getParam();
         }
    
	 $this->setDatasource('webservice');
         $this->setServiceid($param['_serviceid']);
         $this->initDatasourceService();
         $result=  $this->connWebService($param);
        
         return $result;
     }
     
      public function execSqlService($param=null){
          $this->datasource='servicesql';
         if(empty($param)){
                $queryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
                $queryString->setQuery($_SERVER["QUERY_STRING"]);
                $queryString->makeParam();
                $param=$queryString->getParam();
         }
   
	 $this->setDatasource('servicesql');
         $this->setServiceid($param['_serviceid']);
        $this->initDatasourceService();
         $sql=null;
         $type='S';
         $offset=0;
         $limit=100;
         $token=null;
         if(isset($param['query'])){$sql=$param['query'];}
         if(isset($param['type'])){$type=$param['type'];}
         if(isset($param['offset'])){$offset=$param['offset'];}
         if(isset($param['limit'])){$limit=$param['limit'];}
         if(isset($param['token'])){$token=$param['token'];}

         $result=$this->connSqlService($sql,$type,$offset,$limit);
         return $result;
     }
     //delete it
	  public function castParamTypeDb($param) {
		  return $param;
		  $databasedriver=$this->getContainer()->getParameter('database_driver');
		  
		  if($databasedriver=='pdo_pgsql'){
				$utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
				$deleted=$utildata->getVaueOfArray($param, 'deleted');
				if($deleted==1){$param['deleted']="TRUE";}
				if($deleted=="0"){$param['deleted']="FALSE";}
		  }
		 return $param;
	  }
	   //delete it
	  public function castParamTypeDbSql($sql) {
		  return $sql;
		  $databasedriver=$this->getContainer()->getParameter('database_driver');
		  
		  if($databasedriver=='pdo_pgsql'){
				$sql=str_replace("  "," ",$sql);
				$sql=str_replace("deleted=0","deleted=FALSE",$sql);
				$sql=str_replace("deleted= 0","deleted=FALSE",$sql);
				$sql=str_replace("deleted =0","deleted=FALSE",$sql);
				$sql=str_replace("deleted =0 ","deleted=FALSE",$sql);
				
				$sql=str_replace("deleted=1","deleted=TRUE",$sql);
				$sql=str_replace("deleted= 1","deleted=TRUE",$sql);
				$sql=str_replace("deleted =1","deleted=TRUE",$sql);
				$sql=str_replace("deleted =1 ","deleted=TRUE",$sql);
				
		  }
		 return $sql;
	  }
      public function checkDatasourceTypeByParamConfig($pconfig){
          if(is_array($pconfig)){$this->datasource='mongodb';}
          
      }
       public function getEm() {
           return $this->em;
       }

       public function setEm(EntityManager $em) {
           $this->em = $em;
       }

    
       public function getKeymanger() {
      return $this->keymanger;
    }
/*
 public function initDatasourceParam($param) {
     
     $datasource=$this->getUtildata()->getVaueOfArray($param,'_datasource');
     $serviceid=$this->getUtildata()->getVaueOfArray($param,'_serviceid');
     if(($datasource=='servicesql' || $datasource=='webservice') && !empty($serviceid) ){
         $this->datasource=$datasource;
         $this->serviceid=$serviceid;
         $this->initDatasourceService();
     }
  }*/
  public function setKeymanger(BadiuKeyManger $keymanger) {
      $this->keymanger = $keymanger;
  }
  public function getContainer() {
      return $this->container;
  }

  public function setContainer(Container $container) {
      $this->container = $container;
  }

  public function getDatasource() {
      return $this->datasource;
  }

  public function setDatasource($datasource) {
      $this->datasource = $datasource;
  }
  
    public function getServiceconf() {
      return $this->serviceconf;
  }

  public function setServiceconf($serviceconf) {
      $this->serviceconf = $serviceconf;
  }
  
  function getPaginglimit() {
      return $this->paginglimit;
  }

  function setPaginglimit($paginglimit) {
      $this->paginglimit = $paginglimit;
  }
  function getServiceid() {
      return $this->serviceid;
  }

  function setServiceid($serviceid) {
      $this->serviceid = $serviceid;
  }

  function getUtildata() {
      return $this->utildata;
  }

  function setUtildata($utildata) {
      $this->utildata = $utildata;
  }

  public function getSessionhashkey() {
    return $this->sessionhashkey;
}

public function setSessionhashkey($sessionhashkey) {
   $this->sessionhashkey = $sessionhashkey;
}

	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
	
}
