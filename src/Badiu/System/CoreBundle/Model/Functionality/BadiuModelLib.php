<?php

namespace Badiu\System\CoreBundle\Model\Functionality;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class BadiuModelLib {

    /**
     * @var Container
     */
    private $container;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var integer
     */
    private $entity;

    /**
     * @var integer
     */
    private $userid;
    private $sysoperation;

    /**
     * @var object
     */
    private $search;

    /**
     * @var object
     */
    private $utildata = null;

     /**
     * @var object
     */
    private $utilapp = null;
 /**
     * @var object
     */
    private $json = null;
    private $error=null;

         /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
	private $response=null;
	/**
	 * @var string
	 */
	private $lang;
	
	private $systemdata=null;
	private $forceentity=false;
    function __construct(Container $container) {
        $this->container = $container;
        $this->translator = $this->container->get('translator');
		//$this->translator->setLocale('pt');
        //$badiuSession = $this->container->get('badiu.system.access.session');
        $this->entity = null;//$badiuSession->get()->getEntity();
        $this->userid = null;//$badiuSession->get()->getUser()->getId();
        $this->sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $this->search =null;// $this->getContainer()->get('badiu.system.core.functionality.search');
        $this->utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
        $this->json = $this->getContainer()->get('badiu.system.core.lib.util.json');
		$this->lang=null;
      }

	public function initSystemdata($param=null) {
		
		if(empty($param)){
			$lang=$this->getLang();
			$entity=$this->getEntity();
		}else{
			$lang=$this->getUtildata()->getVaueOfArray($param,'lang');
			$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		}
		 $systemdata=$this->getContainer()->get('badiu.system.core.functionality.systemdata');
		 $systemdata->setEntity($entity);
		 $systemdata->setLang($lang);
		 $systemdata->init();
		$this->setSystemdata($systemdata);
	}
	public function getSession($param=null) {
		$sessionkey=null;
		if(empty($param)){
			$sessionkey=$this->getSessionhashkey();
		}else{
			$sessionkey=$this->getUtildata()->getVaueOfArray($param,'_clientsession');
		}
		 $badiuSession = $this->container->get('badiu.system.access.session');
         $badiuSession->setHashkey($sessionkey);
		 
		 if(empty($this->getSystemdata())){$this->initSystemdata($param);}
		  $badiuSession->setSystemdata($this->getSystemdata());
		  return $badiuSession;
	}
    public function initDefaultEntityData($dto) {
        $dto->setDeleted(0);
        $dto->setEntity($this->getEntity());
        $dto->setTimecreated(new \Datetime());
        return $dto;
    }

	public function getRouterKey($keybase=false) {
			
			$currentroute=$this->container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey'); 
			if(empty($currentroute)){$currentroute=$this->getContainer()->get('request')->get('_route');}
			if($keybase){$currentroute=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($currentroute);}
			return $currentroute;
    }
    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }

    public function getTranslator() {
		//echo "lang: ".$this->getLang();exit;
		$this->translator->setLocale($this->getLang());
		$this->translator->setSessionhashkey($this->getSessionhashkey()); 
		$this->translator->setSystemdata($this->getSystemdata());
        return $this->translator;
    }

    public function setTranslator(Translator $translator) {
        $this->translator = $translator;
    }

    public function getEntity($force=false) {
		if($this->forceentity){ return $this->entity;}
        if(empty($this->entity) || !$force){
            $badiuSession = $this->container->get('badiu.system.access.session');
            $badiuSession->setHashkey($this->getSessionhashkey());
            $this->entity = $badiuSession->get()->getEntity();
        }
        
        return $this->entity;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function getUserid() {
        if(empty($this->userid)){
            $badiuSession = $this->container->get('badiu.system.access.session');
            $badiuSession->setHashkey($this->getSessionhashkey());
            $this->userid=$badiuSession->get()->getUser()->getId();
        }
       return $this->userid;
    }

    public function setUserid($userid) {
        $this->userid = $userid;
    }

    public function getSysoperation() {
        return $this->sysoperation;
    }

    public function setSysoperation($sysoperation) {
        $this->sysoperation = $sysoperation;
    }

    function getSearch() {
        if(empty($this->search)){
            $this->search =$this->getContainer()->get('badiu.system.core.functionality.search');
            $this->search->setSessionhashkey($this->getSessionhashkey());
        }
       return $this->search;
    }

    function setSearch($search) {
        $this->search = $search;
    }
    function getUtildata() {
        return $this->utildata;
    }

    function setUtildata($utildata) {
        $this->utildata = $utildata;
    }
    function getJson() {
        return $this->json;
    }

    function setJson($json) {
        $this->json = $json;
    }


    public function getError(){
        return  $this->error;
     }
    
     public function setError($error){
          $this->error=$error;
    }
   
    public function getSessionhashkey() {
		$clientsession = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_clientsession','ajaxws');
		if(!empty($clientsession)){$this->sessionhashkey=$clientsession;}
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }

   function getUtilapp() {
    if(empty($this->utilapp)){
        $this->utilapp =$this->getContainer()->get('badiu.system.core.lib.util.app');
        $this->utilapp->setSessionhashkey($this->getSessionhashkey());
    }
    return $this->utilapp;
}

function setUtilapp($utilapp) {
    $this->utilapp = $utilapp;
}

	public function getResponse() {
		if(empty($this->response)){
			$this->response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
		}
          return $this->response;
      }


      public function setResponse($response) {
          $this->response = $response;
      }
	  
	   /**
     * @return string
     */
    public function getLang() {
		if(empty($this->lang)){
			$badiuSession = $this->container->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			$this->lang=$badiuSession->get()->getLang();
		}
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang) {
        $this->lang = $lang;
    }
	
 public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
	
	/** Get the value of forceentity
     */
    public function getForceentity() {
        return $this->forceentity;
    }

    /**
     * Set the value of forceentity
     *
     * @param mixed $forceentity
     * @return self
     */
    public function setForceentity($forceentity) {
        $this->forceentity = $forceentity;
        return $this; // this is optional, but allows for method chaining
    }
}
