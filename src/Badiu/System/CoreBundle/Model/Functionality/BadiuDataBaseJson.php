<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class BadiuDataBaseJson extends BadiuModelLib {
    
    private $datakey;
    private $data;
    private $param;
    private $field;
    
     private $id;
     
     function __construct(Container $container) {
               parent::__construct($container);
               $this->field='dconfig';
               $this->param=array();
      }
    function init() {
         if(!empty($this->datakey) && empty($this->data)){
             if($this->getContainer()->has($this->datakey)){
               $this->data= $this->getContainer()->get($this->datakey);
            }
         }
    }
      
      function findParam() {
          $this->init();
         
          if(empty($this->data) || empty($this->field) || empty($this->id)){return null;}
             $param=  $this->data->getGlobalColumnValue($this->field,array('id'=>$this->id));
            $this->param=$this->getJson()->decode($param,true);
         }
       
       
       function updateParam() {
            $this->init();
           if(empty($this->data) || empty($this->field) || empty($this->id)){return null;}
             $param=$this->getJson()->encode($this->param); 
             $uparam=array('id'=>$this->id,$this->field => $param);
            $result=$this->data->updateNativeSql($uparam);
          }
       
      function getDatakey() {
          return $this->datakey;
      }

      function getField() {
          return $this->field;
      }

      function getId() {
          return $this->id;
      }

      function setDatakey($datakey) {
          $this->datakey = $datakey;
      }

      function setField($field) {
          $this->field = $field;
      }

      function setId($id) {
          $this->id = $id;
      }

      function getParam() {
          return $this->param;
      }

      function setParam($param) {
         $this->param = $param;
      }

      function getData() {
          return $this->data;
      }

      function setData($data) {
          $this->data = $data;
      }


}
