<?php

namespace Badiu\System\CoreBundle\Model\Functionality;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Translation\Translator;

class BadiuFormType  extends AbstractType {
    
     /**
     * @var Container
     */
    private $container;
    /**
     * @var Translator
     */
    private $translator;
    
    /**
     * @var string
     */
    private $baseKey; 
    
    /**
     * @var boolean
     */
    private $filter;
    
    /**
     * @var string
     */
    private $class;
    
    
    
   function __construct(Container $container,$baseKey,$filter=true,$class='Badiu\System\CoreBundle\Model\Functionality\BadiuFilter') {
                $this->container=$container;
                $this->translator=$this->container->get('translator');
                $this->baseKey=$baseKey;
                $this->filter=$filter;
                $this->class=array();//$class;
      }
     
       /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
       {
            $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
            $keymanger->setBaseKey($this->getBaseKey());
            
            $formfactory=$this->getContainer()->get('badiu.system.core.lib.form.factory');
            $formfactory->setBuilder($builder);
            $formfactory->setKeymanger($keymanger);
            
            $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $fields=$cbundle->getFormFields($this->getFilter(),$keymanger);
            
            
              foreach ($fields as $field) {
                   // echo $field->getName();
                   // echo "<br>";
			 $formfactory->add($field);
                }
            $builder=$formfactory->getBuilder();
      }
    
     /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
           // 'data_class' =>$this->getClass(),
            'csrf_protection'   => false

        ));
    }
    
     /**
     * @return string
     */
    public function getName()
    {
        return 'badiu_system_core_form_type';
    }
 
  
    public function getTranslator() {
        return $this->translator;
    }

    public function setTranslator(Translator $translator) {
        $this->translator = $translator;
    }
    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }


 public function setBaseKey($baseKey) {
          $this->baseKey = $baseKey;
      }
 public function getBaseKey() {
          return $this->baseKey;
      }

   public function setFilter($filter) {
          $this->filter = $filter;
      }
 public function getFilter() {
          return $this->filter;
      }
      
       public function setClass($class) {
          $this->class = $class;
      }
  public function getClass() {
          return $this->class;
      }

}
