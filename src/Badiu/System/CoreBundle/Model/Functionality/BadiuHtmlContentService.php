<?php

namespace Badiu\System\CoreBundle\Model\Functionality;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Badiu\System\CoreBundle\Model\Lib\Http\Url; //review
use Badiu\System\CoreBundle\Model\Lib\Http\HttpQueryString; //review
//use Badiu\System\CoreBundle\Model\Report\BadiuPagination; //review
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
class BadiuHtmlContentService extends BadiuExternalService {

    private $key; 
    private $bkey;
    private $bkeydynamicroute;
    private $layout='indexCrud'; //indexCrud | viewDetail | dashboard
    private $kminherit;
	private $kminheritfilter;
    private $param;
    private $page;
    private $log;
    private $logtimestart;
    private $typecontent='formated';
    private $dynamicroute=false;
    private $syssession;
	private $currentkeyroutefilter;
	private $systemdata;
    public function __construct(Container $container) {
        parent::__construct($container);
        $this->response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
        $this->key=null;
        $this->page=$this->getContainer()->get('badiu.system.core.page');
        $this->page->setIsexternalclient(TRUE);
        $this->log=array();
        $this->logtimestart=null;
    }
 
     public function init() {
        $this->page->setSessionhashkey($this->getSyssession()->getHashkey());
		//$this->page->setSystemdata($this->getSystemdata());
		$this->getTranslator()->setLocale($this->getLang());
		$this->getTranslator()->setSessionhashkey($this->getSyssession()->getHashkey());
		$this->getTranslator()->setSystemdata($this->getSystemdata());
		$this->page->setTranslator($this->getTranslator());
		$this->page->getTranslator()->setLocale($this->getLang());
		$this->page->addData('_param',$this->param);
        $this->key=$this->getUtildata()->getVaueOfArray($this->param, '_key');
        $this->dynamicroute=false;
        if($this->key=='badiu.system.core.report.dynamic.index'){
            $this->key=$this->getUtildata()->getVaueOfArray($this->param, '_dkey');
            $this->dynamicroute=true;
        }else if($this->key=='badiu.system.core.report.dynamicp.index'){
            $this->key=$this->getUtildata()->getVaueOfArray($this->param, '_dkey');
            $this->dynamicroute=true;
       }else if($this->key=='badiu.system.core.report.dynamicd.dashboard'){
            $this->key=$this->getUtildata()->getVaueOfArray($this->param, '_dkey');
            $this->dynamicroute=true;
       }else if($this->key=='badiu.system.core.report.dynamicdp.dashboard'){
            $this->key=$this->getUtildata()->getVaueOfArray($this->param, '_dkey');
            $this->dynamicroute=true;
       }else if($this->key=='badiu.system.core.data.dynamic.add' || $this->key=='badiu.system.core.data.dynamic.edit' || $this->key=='badiu.system.core.data.dynamic.copy'){
            $this->key=$this->getUtildata()->getVaueOfArray($this->param, '_dkey');
            $this->dynamicroute=true;
       }else if($this->key=='badiu.system.core.data.dynamicp.add' || $this->key=='badiu.system.core.data.dynamicp.edit' || $this->key=='badiu.system.core.data.dynamicp.copy'){
            $this->key=$this->getUtildata()->getVaueOfArray($this->param, '_dkey');
            $this->dynamicroute=true;
       }else{
            $this->key=$this->getUtildata()->getVaueOfArray($this->param, '_key');
       }
    
            
            $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
          
            if($sysoperation->isView($this->key)){$this->layout='viewDetail';}
            else if($sysoperation->isIndex($this->key)){$this->layout='indexCrud';}
            else if($sysoperation->isDashboard($this->key)){$this->layout='dashboard';}
            else if($sysoperation->isFrontpage($this->key)){$this->layout='frontpage';}
			else if($sysoperation->isAdd($this->key)){$this->layout='add';}
			else if($sysoperation->isEdit($this->key)){$this->layout='edit';}

            $this->initPageviewsytemtype();
            
            $this->bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($this->key);
            $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
            $keymanger->setBaseKey($this->bkey);
            if($this->dynamicroute){
                $this->bkeydynamicroute=$this->getUtildata()->getVaueOfArray($this->param, '_key');
                $this->bkeydynamicroute=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($this->bkeydynamicroute);
                $keymanger->setBaseKeydyncoriginal($this->bkeydynamicroute);
             
            }
           
            $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
			$cbundle->setSystemdata($this->getSystemdata());
            $cbundle->setSessionhashkey($this->getSyssession()->getHashkey());
            $cbundle->setKeymanger($keymanger);
            $cbundle->initKeymangerExtend($keymanger);
            $this->kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
            $this->kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
            $this->kminheritfilter=$this->kminherit;
			 $changecurrentroute=null; 
		   $this->currentkeyroutefilter=$this->key; 
           if($this->dynamicroute){ 
				$nr=$this->initFreportChangeRoute();
				if(!empty($nr)){$changecurrentroute=$nr;}
		   }
		   $keymangerfilter=$keymanger;
		   if($this->dynamicroute && !empty( $changecurrentroute)){
			   $this->currentkeyroutefilter=$changecurrentroute;
			    $baseKeydyncoriginal = $this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($changecurrentroute);
				
				$keymangerfilter=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
				$keymangerfilter->setBaseKey($baseKeydyncoriginal);
            
                $cbundlefilter=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
				$cbundlefilter->setSessionhashkey($this->getSyssession()->getHashkey());
				$cbundlefilter->setSystemdata($this->getSystemdata());
				$cbundlefilter->setKeymanger($keymangerfilter);
				$cbundlefilter->initKeymangerExtend($keymanger);
				$this->kminheritfilter=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
				$this->kminheritfilter->init($keymangerfilter,$cbundlefilter->getKeymangerExtend()); ;
				
           }
			$this->initFreportDynamic($this->param);
            $cpage=$cbundle->getPageConfig($keymanger);
          
            $this->page->setConfig($cpage);
            $croute=$cbundle->getCrudRoute($keymanger);
            $this->page->setCrudroute($croute);
             $this->page->setKey($this->key);
             $this->page->setOperation($this->getTranslator()->trans($this->key));
            $formGroupList=null;
			$formFieldList=null;
			if($this->layout=='add' || $this->layout=='edit'){
				$formGroupList=$cbundle->getGroupOfField($keymanger->formDataFieldsEnable(),$cbundle->getKeymangerExtend()->formDataFieldsEnable());
				$formFieldList=$cbundle->getFieldsInGroup($keymanger->formDataFieldsEnable(),$cbundle->getKeymangerExtend()->formDataFieldsEnable());
			}else{
				$formGroupList=$cbundle->getGroupOfField($keymanger->formFilterFieldsEnable(),$cbundle->getKeymangerExtend()->formFilterFieldsEnable());
				$formFieldList=$cbundle->getFieldsInGroup($keymanger->formFilterFieldsEnable(),$cbundle->getKeymangerExtend()->formFilterFieldsEnable());
			}
			
            $this->page->addData('badiu_form1_group_list',$formGroupList);
            $this->page->addData('badiu_form1_field_list',$formFieldList);
            
            $dasheboardsqlsinlgleconfig=$cbundle->getValueQueryString($keymanger->dashboardDbSqlSingleConfig(),$cbundle->getKeymangerExtend()->dashboardDbSqlSingleConfig());
            $dasheboardsqlconfig=$cbundle->getValueQueryString($keymanger->dashboardDbSqlConfig(),$cbundle->getKeymangerExtend()->dashboardDbSqlConfig());
          
            $this->page->addData('badiu_list_data_row.config',$dasheboardsqlsinlgleconfig);
            $this->page->addData('badiu_list_data_rows.config',$dasheboardsqlconfig);
           $cform=null;
		   if($this->layout=='add' || $this->layout=='edit'){$cform=$cbundle->getFormConfig('data',$keymanger);}
           else{ $cform=$cbundle->getFormConfig('filter',$keymangerfilter);}
          
            $this->page->setFormconfig($cform);
           
            $fc=$this->getContainer()->get('badiu.system.core.lib.form.factoryconfig');
            if($this->layout=='add' || $this->layout=='edit'){
				$fc->setType('data');
				$fc->setDatalayout($cform->getDatalayout());
			}
            $fc->setSessionhashkey($this->getSyssession()->getHashkey());
			$fc->setSystemdata($this->getSystemdata());
            $fc->setIsexternalclient(true);
            $fc->setDefaultparam($cform->getDefaultdataonpenform());
			
            $this->page->addData('badiu_formconfig',$fc->get($this->currentkeyroutefilter));
           			 
           if($this->layout=='dashboard'){
                $this->page->addData('badiu_formconfig1',$fc->get($this->currentkeyroutefilter));
				$this->page->addData('badiu_formconfig1_defaultdata',$cform->getDefaultdata());
				$this->page->addData('badiu_formconfig1_defaultdataonpenform',$cform->getDefaultdataonpenform());
		
                //data
                $cformd=$cbundle->getFormConfig('data',$keymanger);
                $fc->setType('data');
                $fc->setDatalayout($cformd->getDatalayout());
                $fc->setDefaultparam($cformd->getDefaultdataonpenform());
                $formGroupList=$cbundle->getGroupOfField($keymanger->formDataFieldsEnable(),$cbundle->getKeymangerExtend()->formDataFieldsEnable());
                $formFieldList=$cbundle->getFieldsInGroup($keymanger->formDataFieldsEnable(),$cbundle->getKeymangerExtend()->formDataFieldsEnable());
          
                $this->page->addData('badiu_form2_group_list',$formGroupList);
                $this->page->addData('badiu_form2_field_list',$formFieldList);
                $this->page->addData('badiu_formconfig2',$fc->get($this->currentkeyroutefilter));
				
            }
             
            
            //menu 
            $menu=$this->getContainer()->get($this->kminherit->moduleMenu());
            $menu->setSessionhashkey($this->getSyssession()->getHashkey());
            $menu->setIsexternalclient(true);
            //$keymanger->setBaseKey($this->key);
           
            $menu->setKeymanger($keymanger);
            $additionalContent=array();
           
            $defaultmenu=$menu->get();
           
            $menuBeforeContent=$menu->get('content');
           
            $navbar=$menu->get('navbar');
            //remove site moodle menu.navbar
           
            $ismerpotmodule=stripos($this->key, "badiu.moodle.mreport.");
           
            if($ismerpotmodule!== false){
                $newnavbar=array();
                foreach ($navbar as $mnb){
                    $url=$this->getUtildata()->getVaueOfArray($mnb,'link.url',true);
                    $ismerpotmodulelink=stripos($url, "_key=badiu.moodle.mreport.sitedata.frontpage");
                    if($ismerpotmodulelink=== false){
                        array_push($newnavbar,$mnb);
                    }
                }
                $navbar=$newnavbar;
            }
           
            array_push($additionalContent,$defaultmenu);
            array_push($additionalContent,$navbar);
            array_push($additionalContent,$menuBeforeContent);
          
            $this->page->setAdditionalContents($additionalContent); 
            
            //refresh key
            $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
            $keymanger->setBaseKey($this->bkey);
            if($this->dynamicroute){
                $keymanger->setBaseKeydyncoriginal($this->bkeydynamicroute);
            } 
           
            $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $cbundle->setSessionhashkey($this->getSyssession()->getHashkey());
			$cbundle->setSystemdata($this->getSystemdata());
            $cbundle->setKeymanger($keymanger);
            $cbundle->initKeymangerExtend($keymanger);
            $this->kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
            $this->kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
           
     }
    public function exec() {
      
                $this->logtimestart=new \DateTime();
                $auth=$this->getContainer()->get('badiu.auth.core.webservice.navegation');
                $this->param=$auth->authRequest();
				$this->setLang($auth->getSyssession()->get()->getLang());
                $this->systemdata=$this->getContainer()->get('badiu.system.core.functionality.systemdata');
				$this->systemdata->setEntity($auth->getSyssession()->get()->getEntity());
				$this->systemdata->setLang($auth->getSyssession()->get()->getLang());
				$this->systemdata->init();
				
                $this->setSyssession($auth->getSyssession());
				$this->getSyssession()->setSystemdata($this->systemdata);
                $this->setTypecontent($auth->getTypecontent()); 
               $this->setSessionhashkey($this->getSyssession()->getHashkey());
                $rekey=$this->getUtildata()->getVaueOfArray($this->param,'_key');
				 $this->page->setSystemdata($this->getSystemdata());
                //|| $rekey=='badiu.moodle.mreport.schedulertask.view' 
                if($rekey=='badiu.moodle.mreport.schedulertask.index' || $rekey=='badiu.moodle.mreport.schedulertasklog.index' || $rekey=='badiu.moodle.mreport.schedulertasklogmessage.index'  || $rekey=='badiu.moodle.mreport.syslog.index' || $rekey=='badiu.moodle.mreport.sitepermission.index'  ){echo "Sem permissao de acesso";exit;}
              
				//permission
				$dkey=$this->getUtildata()->getVaueOfArray($this->param,'_dkey');
			    if(!empty($dkey)){$rekey=$dkey;}
				$permission=$this->getContainer()->get('badiu.system.access.permission');
			
				//review
				$export=false;
				 $tokensession=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_tokensession');
				$format=$this->getUtildata()->getVaueOfArray($this->param,'_format');
				if(!empty($tokensession)){$export=true;}
				
				if($format=='xls' && $export ){$export=true;}
				else {$export=false;}
				
				
				if(!$permission->has_access($rekey,$this->getSyssession()->getHashkey()) && !$export){
					echo "Sem permissao de acesso ao recurso solicitado";exit;
				}
				
				$this->addRequestParamToSession($this->param);
                
                $isresponse=$this->isServiceResponse($this->param);
                if($isresponse){return $this->param;}
                 $this->init();
                
                 $content=$this->mekeContent();
                $operation=array('key'=>$this->key,'label'=>$this->page->getOperation());
                $outpage=array('links'=>$this->page->getAdditionalContents(),'content'=>$content,'operation'=>$operation);
                $outpage= $this->criptRespose($outpage);
                
                //$sserveraparam=$badiuSession->get()->getWebserviceinfo();
                $this->initNavegationLogParam();
                $this->addlog();  
               
                $this->getSyssession()->delete();
                return  $outpage;//$this->getResponse()->get();
    }
    
    private function mekeContent() {
      
        $result=array();
        if($this->layout=='frontpage'){
            $content=$this->mekeContentWithFile(); 
            return $content;
        }
        
        $report= $this->getContainer()->get($this->getKminherit()->report());
		$report->setSystemdata($this->getSystemdata());
        $report->setSessionhashkey($this->getSyssession()->getHashkey());
        $report->setKey($this->key);
        $report->setIsexternalclient(true);
        $report->setTranslator($this->getTranslator()); 
       
        $report->getKeymanger()->setBaseKey($this->bkey);
       if($this->dynamicroute){
            $report->getKeymanger()->setBaseKeydyncoriginal($this->bkeydynamicroute);
       } 
        $cform=$this->page->getFormconfig();
        $report->setDefaultdatafilterconfig($cform->getDefaultdata());
        $report->setDefaultdatafilterconfigonopenform($cform->getDefaultdataonpenform());
        $report->setFconfig($cform);
         $type=null;
        
         if($this->layout=='indexCrud'){$type='dbsearch.fields.table.view'; }
         else if($this->layout=='viewDetail'){$type='dbgetrow.fields.table.view'; }
        
    $report->extractData($this->param,$this->layout,$type);
    
        $format=$this->getUtildata()->getVaueOfArray($this->param,'_format');
        
        if($this->layout=='indexCrud'){
            
            if(!$report->getCachereport()){ $report->makeTable($this->layout);}
            
             $this->page->addData('badiu_error_code',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.code'));
             $this->page->addData('badiu_error_message',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.message'));
             $this->page->addData('badiu_list_data_row',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row'));
            $this->page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows'));
            if($this->getTypecontent()=='notformated'){$this->page->addData('badiu_table1',$report->getTable()->castToArray());}
             else {$this->page->addData('badiu_table1',$report->getTable());}
          
           
        }else if($this->layout=='dashboard'){
            $this->page->addData('badiu_error_code',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.code'));
            $this->page->addData('badiu_error_message',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.message'));
         
            $this->page->addData('badiu_list_data_row',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row'));
            $this->page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows'));
        }else if($this->layout=='viewDetail'){
          
            $report->makeTable($this->layout);
           
            $this->page->addData('badiu_error_code',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.code'));
            $this->page->addData('badiu_error_message',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.message'));
            $this->page->addData('badiu_list_data_row',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row'));
            $this->page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows'));
            if($this->getTypecontent()=='notformated'){$this->page->addData('badiu_table1',$report->getTable()->castToArray());}
            else {$this->page->addData('badiu_table1',$report->getTable());}
            
        }
       
      if(!empty($format)&& $format!='html' && $format !='json' ){
       
          $this->initNavegationLogParam();
          $this->addlog();
          $this->getSyssession()->delete();
          $report->exportTable($format);
          
      }
      
      
       
        $content=$this->mekeContentWithFile(); 
      
        return $content;
    }
    
   private function mekeContentWithFile() {
	   $this->page->getTranslator()->setSessionhashkey($this->getSyssession()->getHashkey());
	   $this->page->getTranslator()->setSystemdata($this->getSystemdata());
       $page=$this->page;
      
	   if($this->getTypecontent()=='notformated'){return $page->getData();}
        $container=$this->getContainer();
        $router=$this->getContainer()->get("router");
         $contents=null;
        $app=$this->getContainer()->get('badiu.system.core.lib.util.app'); 
        $app->setSessionhashkey($this->getSyssession()->getHashkey());
		$app->setSystemdata($this->getSystemdata());
        if($this->layout=='indexCrud'){
            $table=$page->getData()['badiu_table1'];
            
            $fileprocessindex=$page->getConfig()->getFileprocessindex();
       
            $file=$app->getFilePath($fileprocessindex,false);
            if(!file_exists($file)){return "file $fileprocessindex | $file not found"; }
           
             //$pagination = new BadiuPagination($table->getCountrow(), $table->getPageindex());
               $pagination=$this->getContainer()->get('badiu.system.core.report.pagination');
               $pagination->setSessionhashkey($this->getSyssession()->getHashkey());
			   $pagination->setSystemdata($this->getSystemdata());
                $pagination->start($table->getCountrow(), $table->getPageindex());
              
        $infoResult = $pagination->getInfo();
       
                $urltoextport=$this->getUrlToExport();
               
		//$urltoextport = Url::getCurrentUrl();
		//$urltoextport = Url::addParam($url, "_format", "xls");
                $pagingout=$this->pagination($table->getCountrow(), $table->getPageindex());
            
             ob_start();
              include_once($file); 
             $contents = ob_get_contents();
            ob_end_clean();
            
        } else  if($this->layout=='viewDetail'){
            $fileprocessview=$page->getConfig()->getFileprocessview();
            $file=$app->getFilePath($fileprocessview,false);
            if(!file_exists($file)){return "file $fileprocessview | $file not found"; }
          
              ob_start();
             include_once($file);
             $contents = ob_get_contents();
            ob_end_clean();
        }
        
        else  if($this->layout=='frontpage'){
            $fileprocessfrontpage=$page->getConfig()->getFileprocessfrontpage();
            $file=$app->getFilePath($fileprocessfrontpage,false);
      
            if(!file_exists($file)){return "file $fileprocessfrontpage | $file not found"; }
          
              ob_start();
             include_once($file);
             $contents = ob_get_contents();
            ob_end_clean();
        }else  if($this->layout=='dashboard'){
            $fileprocessdashboard=$page->getConfig()->getFileprocessdashboard();	
            $file=$app->getFilePath($fileprocessdashboard,false);
			
            if(!file_exists($file)){return "file $fileprocessdashboard | $file not found"; }
          
              ob_start();
             include_once($file);
             $contents = ob_get_contents();
            ob_end_clean();
			
        }else  if($this->layout=='add' || $this->layout=='edit'){
			$fileprocessformadd=$page->getConfig()->getFileprocessformadd();	 //review it
            $file=$app->getFilePath($fileprocessformadd,false);
            if(!file_exists($file)){return "file $fileprocessformadd | $file not found"; }
          
              ob_start();
             include_once($file);
             $contents = ob_get_contents();
            ob_end_clean();
        }
		 $contents="<div class=\"badiunet-content-nformated-service\"> $contents</div>";
       $contents=utf8_encode($contents);
        return $contents;
    }
    
     private function getUrlToExport() {
        /* $app=$this->getContainer()->get('badiu.system.core.lib.util.app'); 
         //$url ="BADIU_CORE_SERVICE_CLIENTE_URLBASE?_service=badiu.system.core.functionality.content.service&_format=xls&_key=".$this->key; //review use Util/App as service
         $url =$app->getCurrentUrl();
         $url.="?_service=badiu.system.core.functionality.content.service&_format=xls&_key=".$this->key;//."&_token=13|lMSuOabIjYICcB2Kbc6Xxza0d0UUuG|2|mreport|33";
         $url.= $_SERVER["QUERY_STRING"]."&_format=xls";*/
         $queryString = new HttpQueryString();
         $queryString->setParam($this->param);
         $queryString->add('_format','xls');
         $queryString->remove('_page');
         $tokensession=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_tokensession');
         $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_param');
         $queryString->add('_tokensession',$tokensession);
         $queryString->add('_param',$param);
	 $queryString->makeQuery(true);
         $query = $queryString->getQuery();
         $url= "BADIU_CORE_SERVICE_CLIENTE_CURRENT_URL?$query";
         
         return $url;
     }
    private function pagination($countDbrows = 0, $pageIndex = 0) {
		// echo " pageIndex: $pageIndex";
		if ($countDbrows == 0) {
			return "";
		}

		//$pagination = new BadiuPagination($countDbrows, $pageIndex);
                $pagination=$this->getContainer()->get('badiu.system.core.report.pagination');
                $pagination->setSessionhashkey($this->getSyssession()->getHashkey());
                $pagination->start($countDbrows, $pageIndex);

		// if($pageIndex==0){$pageIndex=1;}
        $output = "";
        if($this->page->getViewsytemtype()=='bootstrap2'){
            $output .= "<div class=\"pagination\">";
            $output .= "<ul>";
        }else{
            $output .= "<nav aria-label=\"Page navigation\">";
            $output .= "<ul class=\"pagination\">";
        }
      

		$url ="BADIU_CORE_SERVICE_CLIENTE_URLBASE?_key=".$this->key; //review use Util/App as service
		$pos = strripos($url, "?");
		if ($pos === false) {$url .= "?";} else {
			$url .= "&";
			//$qtring =$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
			$qtring =$this->param;
                        if (!empty($qtring)) {
				$urlWithoutParam = substr($url, 0, $pos);

				$queryString = new HttpQueryString();
                                $queryString->setParam($qtring);
				$queryString->remove('_page');
                                $queryString->makeQuery();
				$query = $queryString->getQuery();
				$url = $urlWithoutParam . "?" . $query . "&";
			}

		}

		$cont = 0;
		// print_r($pagination->getPages());
		foreach ($pagination->getPages() as $p) {
			$cont++;
			$pbd = $p - 1;
			$link = $url . "_page=" . $pbd;
			$active = "";
			if ($pageIndex == $pbd) {
				$active = " active";
			}
            if($this->page->getViewsytemtype()=='bootstrap2'){
                if(!empty($active )){$active =" class=\" $active\"";}
                $output .= "<li $active ><a href=\"$link\" >$p </a></li>";}
			else {$output .= "<li class=\"page-item$active\"><a href=\"$link\"  class=\"page-link\">$p </a></li>";}
        }
        if($this->page->getViewsytemtype()=='bootstrap2'){
            $output .= "</ul>";
            $output .= "</div>";
        }else{
            $output .= "</ul>";
            $output .= "</nav>";
        }
       
		if ($cont == 1) {$output = "";}
		return $output;
	}
     
     function criptRespose($response) {
              
              $criptinfo= $this->getSyssession()->get()->getWebserviceinfo();
              $criptykey=$this->getUtildata()->getVaueOfArray($criptinfo,'criptykey');
              $templatetcript=$this->getUtildata()->getVaueOfArray($criptinfo,'cripty');
              if(!empty(!empty($criptykey)) && $templatetcript==100){
                 $tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
                 $tcript->setSessionhashkey($this->getSyssession()->getHashkey());
                 $response=$this->getJson()->encode($response);
                 $response=$tcript->encode('ks',$response);
              }
             
              return $response;
          }
      
       function isServiceResponse($response) {
            if(is_array($response)){
               if ( array_key_exists('status',$response) 
                       && array_key_exists('info',$response) 
                       && array_key_exists('message',$response) 
                       && !array_key_exists('_key',$response) 
                       && !array_key_exists('_service',$response)){return true;}
                  
           }
         return false;
       }  
          
       private function addlog() {
           if(!$this->log){return null;}
           
           $dloglib=$this->getContainer()->get('badiu.system.log.core.lib');
           $dloglib->setSessionhashkey($this->getSyssession()->getHashkey());
           $logtimeend=new \DateTime();
           if(!empty($this->logtimestart)){
               $start=$this->logtimestart->getTimestamp();
               $end=$logtimeend->getTimestamp();
               $timeexec=$end-$start; 
               $this->log['timeexec']=$timeexec;
           }
           $this->log['dtype']='webservice';
           $this->log['clientdevice']='server';
           $this->log['modulekey']=$this->bkey;
           $this->log['moduleinstance']=$this->getUtildata()->getVaueOfArray($this->param,'parentid');
           $this->log['functionalitykey']=$this->key;
           
           $param['rkey']='badiu.system.core.functionality.content.service';
           $dloglib->add($this->log);
           
       }
       
        private function initNavegationLogParam() {
                 
                $logparam=$this->getSyssession()->get()->getWebserviceinfo();
                unset($logparam['cripty']);
                unset($logparam['criptykey']);
                unset($logparam['sessiondna']);
                $logparam['response']['status']='accept';
                $logparam['response']['info']='';
                $logparam['response']['message']='data successul send';
                $logparam['query']=$this->param;
                $this->log['ip']=$this->getUtildata()->getVaueOfArray($logparam,'client.ip',true);
                $this->log['dtype']='webservice';
                $this->log['modulekey']=$this->bkey;
                $this->log['moduleinstance']=$this->bkey;
                $this->log['functionalitykey']=$this->getUtildata()->getVaueOfArray($this->param,'_key');
                $this->log['rkey']=$this->getUtildata()->getVaueOfArray($this->param,'_service');
                $this->log['ip']=$this->getUtildata()->getVaueOfArray($logparam,'client.ip',true);
             
                $logparam=$this->getJson()->encode($logparam);
                $this->log['param']= $logparam;
                $this->log['entity']= $this->getSyssession()->get()->getEntity();
                $this->log['action']='view';
        }

    function initPageviewsytemtype() {
        $sserveraparam=$this->getSyssession()->get()->getWebserviceinfo();
        $manualcutombootstrap=$this->getUtildata()->getVaueOfArray($sserveraparam,'sserver.theme',true);
        if(!empty($manualcutombootstrap)){$this->page->setViewsytemtype($manualcutombootstrap);}
        else{
            $clientversion=$this->getUtildata()->getVaueOfArray($sserveraparam,'sserver.versionnumber',true);
            if(!empty($clientversion)){$clientversion=intval($clientversion);}
            if(!empty($clientversion) && $clientversion <=2016052317) {$this->page->setViewsytemtype('bootstrap2');} //if less or igual moodle 3.1 set bootstrap2
         }
        }

    function addRequestParamToSession($param) {
        $datasession=$this->getSyssession()->get();
        $webserviceinfo=$datasession->getWebserviceinfo();
        $webserviceinfo['_param']=$param;
        $datasession->setWebserviceinfo($webserviceinfo);
        $this->getSyssession()->save($datasession);

    }

    function hasLinkPermission($permkey,$linkkey) {
        $pos=strpos($linkkey,$permkey);
        if($pos !== FALSE && $pos==0){return true;}
        return false;
    }

public function initFreportDynamic($fparam) {
		 if(!$this->getContainer()->has('badiu.sync.freport.freport.lib.factoryservicereportdynamickey')){return null;}
		 $factorysdk=$this->getContainer()->get('badiu.sync.freport.freport.lib.factoryservicereportdynamickey');
		 $factorysdk->setSessionhashkey($this->getSyssession()->getHashkey());
		 $factorysdk->setSystemdata($this->getSystemdata());
		 
		 $freportid=$this->getUtildata()->getVaueOfArray($fparam, '_freportid');
		 $force = $this->getUtildata()->getVaueOfArray($fparam, '_force');
		 $dkey = $this->getUtildata()->getVaueOfArray($fparam, '_dkey');
		 if(empty($dkey)){$dkey=$this->getKey();}
		 $faparam=array('_freportid'=>$freportid,'_dkey'=>$dkey,'_force'=>$force);
		 
		 $factorysdk->initSession($faparam);
	 }
	public function initFreportChangeRoute($fparam=null) {
		if(!$this->getContainer()->has('badiu.sync.freport.freport.lib.factoryservicereportdynamickey')){return null;}
		$factorysdk=$this->getContainer()->get('badiu.sync.freport.freport.lib.factoryservicereportdynamickey');
		 $factorysdk->setSessionhashkey($this->getSyssession()->getHashkey());
		 $factorysdk->setSystemdata($this->getSystemdata());
		if(empty($fparam)){$fparam=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();}
		$freportid=$this->getUtildata()->getVaueOfArray($fparam, '_freportid');
		$dkey = $this->getUtildata()->getVaueOfArray($fparam, '_dkey');
		$fdkey = $this->getUtildata()->getVaueOfArray($fparam, '_fdkey');
		$force = $this->getUtildata()->getVaueOfArray($fparam, '_force');
		if(empty($dkey)){return null;}
	    if(empty($fdkey)){return null;}
	   
		$faparam=array('_freportid'=>$freportid,'_dkey'=>$dkey,'_fdkey'=>1,'_force'=>$force);
		$nk=$factorysdk->initSessionOfDynmickey($faparam);
		return $nk;
    } 
    function getKey() {
        return $this->key;
    }

    function setKey($key) {
        $this->key = $key;
    }

    function getLayout() {
        return $this->layout;
    }

    function setLayout($layout) {
        $this->layout = $layout;
    }

    function getKminherit() {
        return $this->kminherit;
    }

    function setKminherit($kminherit) {
        $this->kminherit = $kminherit;
    }

    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }


    function getBkey() {
        return $this->bkey;
    }

    function setBkey($bkey) {
        $this->bkey = $bkey;
    }

   
    function getPage() {
        return $this->page;
    }

    function setPage($page) {
        $this->page = $page;
    }

    function getTypecontent() {
        return $this->typecontent;
    }

    function setTypecontent($typecontent) {
        $this->typecontent = $typecontent;
    }

    function getSyssession() {
        return $this->syssession;
    }

    function setSyssession($syssession) {
        $this->syssession = $syssession;
    }
	
	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
