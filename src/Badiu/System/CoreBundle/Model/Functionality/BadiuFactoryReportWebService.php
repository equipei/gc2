<?php

namespace Badiu\System\CoreBundle\Model\Functionality;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class BadiuFactoryReportWebService extends BadiuModelLib {
	
    function __construct(Container $container) {
        parent::__construct($container);
    } 
	
	public function exec() {

		$param = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$key=$this->getUtildata()->getVaueOfArray($param, '_key');
				
		$dynamicroute=false;
        if($key=='badiu.system.core.report.dynamic.index'){
            $key=$this->getUtildata()->getVaueOfArray($param, '_dkey');
            $dynamicroute=true;
        }else if($key=='badiu.system.core.report.dynamicp.index'){
            $key=$this->getUtildata()->getVaueOfArray($param, '_dkey');
            $dynamicroute=true;
       }else if($key=='badiu.system.core.report.dynamicd.dashboard'){
            $key=$this->getUtildata()->getVaueOfArray($param, '_dkey');
            $dynamicroute=true;
       }else if($key=='badiu.system.core.report.dynamicdp.dashboard'){
            $key=$this->getUtildata()->getVaueOfArray($param, '_dkey');
            $dynamicroute=true;
       }else{
            $key=$this->getUtildata()->getVaueOfArray($param, '_key');
       }
	   
	   $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
       $layout='';   
            if($sysoperation->isView($key)){$layout='viewDetail';}
            else if($sysoperation->isIndex($key)){$layout='indexCrud';}
            else if($sysoperation->isDashboard($key)){$layout='dashboard';}
            else if($sysoperation->isFrontpage($key)){$layout='frontpage';}
	


			$bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
            $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
            $keymanger->setBaseKey($bkey);
            if($dynamicroute){
                $bkeydynamicroute=$this->getUtildata()->getVaueOfArray($param, '_key');
                $bkeydynamicroute=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($bkeydynamicroute);
                $keymanger->setBaseKeydyncoriginal($bkeydynamicroute);
             
            }
           
            $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $cbundle->setSessionhashkey($this->getSessionhashkey());
            $cbundle->setKeymanger($keymanger);
            $cbundle->initKeymangerExtend($keymanger);
            $kminherit=$this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
            $kminherit->init($keymanger,$cbundle->getKeymangerExtend()); 
            
		
			$this->initFreportDynamic($param);	
			
			$report= $this->getContainer()->get($kminherit->report());
        $report->setSessionhashkey($this->getSessionhashkey());
        $report->setKey($key);
        $report->setIsexternalclient(true);
       
       $page=$this->getContainer()->get('badiu.system.core.page');
        $report->getKeymanger()->setBaseKey($bkey);
       if($dynamicroute){
            $report->getKeymanger()->setBaseKeydyncoriginal($bkeydynamicroute);
       } 
         $cform=$cbundle->getFormConfig('filter',$keymanger);
        $report->setDefaultdatafilterconfig($cform->getDefaultdata());
        $report->setDefaultdatafilterconfigonopenform($cform->getDefaultdataonpenform());
        $report->setFconfig($cform);
         $type=null;
        
         if($layout=='indexCrud'){$type='dbsearch.fields.table.view'; }
         else if($layout=='viewDetail'){$type='dbgetrow.fields.table.view'; }
    $report->setPaginglimit(2500);
    $report->extractData($param,$layout,$type);
	
        
        if($layout=='indexCrud'){
            $report->makeTable($layout);
            /*$page->addData('badiu_error_code',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.code'));
             $page->addData('badiu_error_message',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.message'));
             $page->addData('badiu_list_data_row',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row'));
            $page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows'));
            $page->addData('badiu_table1',$report->getTable()->castToArray());*/
            return $report->getTable()->castToArray();
         
           
        }else if($layout=='dashboard'){
            $page->addData('badiu_error_code',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.code'));
            $page->addData('badiu_error_message',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.message'));
         
            $page->addData('badiu_list_data_row',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row'));
            $page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows'));
        }else if($layout=='viewDetail'){
          
            $report->makeTable($layout);
           
           /* $page->addData('badiu_error_code',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.code'));
            $page->addData('badiu_error_message',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.message'));
            $page->addData('badiu_list_data_row',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row'));
            $page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows'));
            $page->addData('badiu_table1',$report->getTable()->castToArray());*/
           
			return $report->getTable()->castToArray();
			
        }
		
	return $page->getData();	
	}
	
	
	public function initFreportDynamic($fparam) {
		 
		 $freportid=$this->getUtildata()->getVaueOfArray($fparam, '_freportid');
		 $force = $this->getUtildata()->getVaueOfArray($fparam, '_force');
		 $dkey = $this->getUtildata()->getVaueOfArray($fparam, '_dkey');
		 $key = $this->getUtildata()->getVaueOfArray($fparam, '_key');
		if(empty($dkey)){$dkey=$key;}
		$badiuSession =$this->getContainer()->get('badiu.system.access.session');
		$badiuSession->setHashkey($this->getSessionhashkey());
       
		$sessionkey=$key.'.'.$freportid;
		$hassession=$badiuSession->getValue($sessionkey);
		if($hassession && !$force){return null;} 
		
		$factorysdk=$this->getContainer()->get('badiu.sync.freport.freport.lib.factoryservicereportdynamickey');
		$factorysdk->setSessionhashkey($this->getSessionhashkey());
		$faparam=array('freportid'=>$freportid,'dkey'=>$dkey);
		$factorysdk->addSession($faparam);
    }
}
