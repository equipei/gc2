<?php

namespace Badiu\System\CoreBundle\Model\Functionality;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Report\Data\Table\BadiuReportDataTable;
use Badiu\System\CoreBundle\Model\Report\Data\BadiuReportData;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Badiu\System\CoreBundle\Model\Report\Export\BadiuReporExport;
use Symfony\Component\Translation\Translator;
use Badiu\System\CoreBundle\Model\Report\Data\BadiuReportDataInterface;
use Badiu\System\CoreBundle\Model\Report\Data\BadiuReportDataSelectRow;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger;

class BadiuReport implements BadiuReportDataInterface {

    /**
     * @var BadiuKeyManger
     */
    private $keymanger;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $dashboardData;

    /**
     * @var array
     */
    private $listData;

    /**
     * @var array
     */
    private $dashboardsqlconfig = array();

    /**
     * @var array
     */
    private $defaultdatafilterconfig = array();

    /**
     * @var array
     */
    private $defaultdatafilterconfigonopenform = array();

    /**
     * @var boolean
     */
    private $isexternalclient;

    /**
     * @var string
     */
    private $datasource = 'dbdoctrine'; // dbdoctrine|servicesql|webservice | mongodb

    /**
     * @var integer
     */
    private $serviceid;

    /**
     * @var boolean
     */
    private $isschedule = FALSE;

  /**
     * @var boolean
     */
    private $isservice = FALSE;
    /**
     * @var array
     */
    private $fparam; //param for filter
    private $fconfig; // form config
    private $paginglimit=null;

    /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;

    private $key; 
        /**
     * @var object
     */
    private $utildata = null;

            /**
     * @var object
     */
    private $filemanagereport = null;
	private $gerenralparamconfig = null;
    private $cachereport=false;
	private $systemdata;
	private $translator;
    function __construct(Container $container, $baseKey) {
        $this->container = $container;

        $db ="default"; //$this->container->get('badiu.system.access.session')->get()->getDbapp();
        $this->em = $this->container->get('doctrine')->getEntityManager($db);

        $this->translator = $this->container->get('translator');
        $this->router = $this->container->get("router");
        $this->data = new BadiuReportData();
        $this->table = new BadiuReportDataTable();
        $this->link = array();

        $this->keymanger = $this->getContainer()->get('badiu.system.core.functionality.keymanger');
        $this->keymanger->setBaseKey($baseKey);

        $this->dashboardData = array();
        $this->listData = array();
        $this->fparam = array();
        
        $routek=$this->getContainer()->get('request')->get('_route');
        $dkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey');
        if(!empty($dkey)){$routek=$dkey;}
        $this->setKey($routek);
    }

    function extractData($fparam, $layout = 'indexCrud', $type = null,$param=array()) {
       
      $this->initFreportDynamic($fparam);
        //echo $this->keymanger->getBaseKey();echo "<br>";
      //  print_r($this->getDefaultdatafilterconfigonopenform());exit;
        //set default data if param not exist  
        $formdefaultdata = $this->getContainer()->get('badiu.system.core.lib.form.defaultdata');
        $formdefaultdata->setSessionhashkey($this->getSessionhashkey());
		$formdefaultdata->setSystemdata($this->getSystemdata());
		
        $formdefaultdata->setParam($this->getDefaultdatafilterconfig());
		
        $fparam = $formdefaultdata->setDefaultParam($fparam);
		
        //	print_r($this->getDefaultdatafilterconfigonopenform());exit;		
        $formdefaultdata->setParam($this->getDefaultdatafilterconfigonopenform());
		
        $fparam = $formdefaultdata->setDefaultParamOnOpenForm($fparam);
        $this->fparam = $fparam;
      
	/*echo "<pre>";
	  print_r($this->getDefaultdatafilterconfigonopenform());
	  echo "</pre>";exit;*/
        //check param limit
        $rcontroller=$this->controller();
        if(!empty($rcontroller)){
            $this->dashboardData = $rcontroller; 
            return $rcontroller;
        }

        $fparam=$this->fparam;
   
        $data = array();
        $data['badiu_filter_param'] = $fparam;
        $search = $this->getContainer()->get('badiu.system.core.functionality.search');
        $search->setSessionhashkey($this->getSessionhashkey());
		$search->setSystemdata($this->getSystemdata());
        
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
		$badiuSession->setSystemdata($this->getSystemdata());
		
		//limit
		if(empty($this->paginglimit)){$this->paginglimit=$badiuSession->get()->getConfig()->getValue('badiu.system.core.param.config.report.paginationlimitdbrows');}
		if(!empty($this->paginglimit)){$search->setPaginglimit($this->paginglimit);}
		
        $search->setKeymanger($this->keymanger);
		$utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
		$datasource = $utildata->getVaueOfArray($fparam, '_datasource');
        $serviceid = $utildata->getVaueOfArray($fparam, '_serviceid');
		
        if ($this->getIsschedule()) {
            if ($datasource == 'servicesql' || $datasource == 'webservice') {
                 $search->setServiceid($serviceid);
                $search->setDatasource($datasource);
                $search->initDatasourceService();
            }
        }
		if ($this->getIsservice()) {
			if(!empty($datasource)){$search->setDatasource($datasource);}
			if(empty($serviceid)){$serviceid=$this->getServiceid();}
			if(!empty($serviceid)){ 
				$search->setServiceid($serviceid);
				$search->initDatasourceService();
			}
		}
		
        if (!isset($fparam['_page'])) {
            $fparam['_page'] = 0;
        }
		
 	  
       if ($layout == 'indexCrud') {
            $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
            $partialdata = $utildata->getVaueOfArray($param, 'partialdata');
            $fulldatawithutopaging = $utildata->getVaueOfArray($param, 'fulldatawithutopaging');
            $this->getTable()->setPageindex($fparam['_page']);
            $this->getTable()->setPagemaxrow( $search->getPaginglimit());
            if($partialdata=='countrows' && !$fulldatawithutopaging){
                $countrows = $search->searchCount($fparam);
                $this->dashboardData = array('row' => null, 'rows' => null, 'reportcontent' => null,'countrows'=>$countrows);
                return $this->dashboardData;
            }else if($partialdata=='getrows' && !$fulldatawithutopaging){
                $rows = $search->search($fparam);
                $this->getData()->setCount(0);
                $this->getData()->setRows($rows);
                $this->dashboardData = array('row' => null, 'rows' =>  $rows, 'reportcontent' => null,'countrows'=>null);
                return $this->dashboardData;
            }
            
            //to export
            $format= $utildata->getVaueOfArray($fparam, '_format');
            $isexportdata=false;
            if($format=='xls'){$isexportdata=true;}
			if($format=='csv'){$isexportdata=true;}
            if($fulldatawithutopaging){$isexportdata=true;}
          
            $countrecord=$search->searchCount($fparam);
		
           
            $srows=array();
            if(!$isexportdata){$srows=$search->search($fparam);}
            else{
				$maxrowperconnection=$badiuSession->get()->getConfig()->getValue('badiu.system.core.param.config.scheduler.maxrowperconnection');
                if(empty($maxrowperconnection)){$maxrowperconnection=2500;}
               
			    $maxrowtoexec=$badiuSession->get()->getConfig()->getValue('badiu.system.core.param.config.scheduler.maxrowperprocess');
                if(empty($maxrowtoexec)){$maxrowtoexec=250000;}
               
                $subprocess=0; 
				$fileparamconfig= $utildata->getVaueOfArray($this->getGerenralparamconfig(), 'filemanagereportparam'); 
                $this->initFilemanagereport($format);
                $this->getFilemanagereport()->start($fileparamconfig);
                $this->setCachereport(true);
                
                if($countrecord > $maxrowperconnection){$subprocess=$countrecord/$maxrowperconnection;}
                if($subprocess != round($subprocess)){
                    $subprocess=floor($subprocess)+1;
                }
                if($subprocess == 0){$subprocess=1;}
                if($subprocess > 0){
                   
                    $search->setPaginglimit($maxrowperconnection);
                    $countprocessed=0;
                    
                    $data['badiu_table1_count'] = $countrecord;
                    $data['badiu_list_data_row']=null;
                    $data['badiu_list_data_row'] = null;
                    $data['badiu_list_data_rows'] =null;
                    $this->getData()->setCount($data['badiu_table1_count']);
                    for ($i = 0; $i < $subprocess; $i++) {
                        $fparam['_page']=$i;
                        $irows=$search->search($fparam);
                        //add head
                        if($i==0){
                            $this->makeTableColumnsTitle();
                            $headaskey= $utildata->getVaueOfArray($this->getGerenralparamconfig(), 'headaskey'); 
							
							if($headaskey){
								$khead=array();
								foreach ($this->getTable()->getColumn()->getList() as $hk => $hv){$khead[$hk]=$hk;}
								 $this->getFilemanagereport()->head($khead);
							}else{ $this->getFilemanagereport()->head($this->getTable()->getColumn()->getList());}
                        }
                         $icount=0;
                        if(is_array($irows)){
                            $icount=sizeof($irows);
                            $countprocessed=$countprocessed+$icount;
                            $data['badiu_table1_rows'] =$irows;
                        }else{ $data['badiu_table1_rows']=null;}
						$data['badiu_list_data_row'] = $search->searchListSingle($fparam, $type);
						$data['badiu_list_data_rows'] = $search->searchList($fparam, $type);
						
                        $data = $this->changeData($data);
                        $this->getData()->setRows($data['badiu_table1_rows']);
                        $this->makeTable($layout,array('addrows'=>1,'changedata'=>1)); 
                        $this->getFilemanagereport()->addtable($this->getTable());
                        $this->getTable()->getRow()->clean();
                        if($icount==0 || $countprocessed>=$maxrowtoexec){break;}
                       
                 }
                }
                $this->getFilemanagereport()->end();
               //break exec review it
                return null; 
            }
            
            $data['badiu_table1_count'] = $countrecord;
            $data['badiu_table1_rows'] =$srows;

            $data['badiu_list_data_row'] = $search->searchListSingle($fparam, $type);
            $data['badiu_list_data_rows'] = $search->searchList($fparam, $type);
            
            $data = $this->changeData($data);

           
            $entity = $utildata->getVaueOfArray($fparam, 'entity');
            if(empty($entity)){
                $badiuSession = $this->container->get('badiu.system.access.session');
                $badiuSession->setHashkey($this->getSessionhashkey());
                $entity =  $badiuSession->get()->getEntity();
            }
            $parentid=null; 
            if(isset($fparam['parentid'])){$parentid=$fparam['parentid'];}
            $countscheduler= $this->countSchedulerTask($entity,$this->getKey(),$parentid);
           
            $this->getData()->setCount($data['badiu_table1_count']);
            $this->getData()->setRows($data['badiu_table1_rows']);
            $this->getData()->setCountschedulertask($countscheduler);
           // $this->getData()->setRows($data['badiu_table1_countscheduler']);

            $this->dashboardData = array('row' => $data['badiu_list_data_row'], 'rows' => $data['badiu_list_data_rows'], 'reportcontent' => null,'countrows'=>$data['badiu_table1_count']);
        } else if ($layout == 'viewDetail') {

            $data['badiu_table1_rows'] = $search->find($fparam);

            $data['badiu_list_data_row'] = $search->searchListSingle($fparam, $type);
            $data['badiu_list_data_rows'] = $search->searchList($fparam, $type);
            $data = $this->changeData($data);
            $this->getData()->setRows($data['badiu_table1_rows']);

            $this->dashboardData = array('row' => $data['badiu_list_data_row'], 'rows' => $data['badiu_list_data_rows'], 'reportcontent' => null);
        } else if ($layout == 'dashboard') {

            $data['badiu_list_data_row'] = $search->searchListSingle($fparam, $type);
            $data['badiu_list_data_rows'] = $search->searchList($fparam, $type);
            $data = $this->changeData($data);
            $this->dashboardData = array('row' => $data['badiu_list_data_row'], 'rows' => $data['badiu_list_data_rows'], 'reportcontent' => null);
        }

        $this->addReportContent($data);

        return $this->dashboardData;
    } 

    public function addReportContent($data) {


        $dcontent = array();
        $cbundle = $this->getContainer()->get('badiu.system.core.lib.config.bundle');
        $cbundle->setSessionhashkey($this->getSessionhashkey());
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->setKeymanger($this->getKeymanger());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $contentkey = $cbundle->getKeymangerExtend()->reportcontent();
        if (!$this->getContainer()->has($contentkey)) {
            return null;
        } else {
            $reportcontent = $this->getContainer()->get($contentkey);
            $reportcontent->setData($data);

            $dcontent['head'] = $reportcontent->head();
            $dcontent['middle'] = $reportcontent->middle();
            $dcontent['footer'] = $reportcontent->footer();
        }

        $datadashbord = $this->dashboardData;
        $datadashbord['reportcontent'] = $dcontent; //review
        $datadashbord['badiu_table1'] = $this->getTable(); //review
        $this->dashboardData = $datadashbord;

        //  return $this->dashboardData;
    }

    public function changeData($data) {
       
        $cbundle = $this->getContainer()->get('badiu.system.core.lib.config.bundle');
        $cbundle->setSessionhashkey($this->getSessionhashkey());
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->setKeymanger($this->getKeymanger());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $kminherit = $this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(), $cbundle->getKeymangerExtend());
        $reportchangedatakey = $kminherit->reportchangedata();


        if (!empty($reportchangedatakey) && $this->getContainer()->has($reportchangedatakey)) {

            $reportchangedata = $this->getContainer()->get($reportchangedatakey);
			$reportchangedata->setSessionhashkey($this->getSessionhashkey());
			$reportchangedata->setSystemdata($this->getSystemdata());
            $function = "all";
            if (method_exists($reportchangedata, $function)) {

                $data = $reportchangedata->$function($data);
            }
        }
        return $data;
    }

    public function changeReport($table) {

        $cbundle = $this->getContainer()->get('badiu.system.core.lib.config.bundle');
        $cbundle->setSessionhashkey($this->getSessionhashkey());
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->setKeymanger($this->getKeymanger());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $kminherit = $this->getContainer()->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(), $cbundle->getKeymangerExtend());
        $reportchangedatakey = $kminherit->reportchangedata();


        if (!empty($reportchangedatakey) && $this->getContainer()->has($reportchangedatakey)) {

            $reportchangedata = $this->getContainer()->get($reportchangedatakey);
			$reportchangedata->setSessionhashkey($this->getSessionhashkey());
			$reportchangedata->setSystemdata($this->getSystemdata());
            $function = "report";
            if (method_exists($reportchangedata, $function)) {

                $table = $reportchangedata->$function($table);
            }
        }
        return $table;
    }

    public function makeTableColumnsTitle($layout = 'indexCrud') {
        $cbundle = $this->getContainer()->get('badiu.system.core.lib.config.bundle');
        
        $cbundle->setSessionhashkey($this->getSessionhashkey());
		$cbundle->setSystemdata($this->getSystemdata());
        $fields = null;
		$fieldstype = null;
        if ($layout == 'indexCrud') {

            $fields = $cbundle->getDbTableViewFields($this->getKeymanger(),$this->getIsschedule());
			$fieldstype=$cbundle->getDbTableViewFieldsType($this->getKeymanger());
        } else if ($layout == 'viewDetail') {
            $fields =$cbundle->getDbRowTableViewFields($this->getKeymanger(),$this->getIsschedule());

      
            $cbundle->setKeymanger($this->getKeymanger());
            $cbundle->initKeymangerExtend($this->getKeymanger());
  
            $fieldGroupList=$cbundle->getGroupOfField($this->getKeymanger()->dbgetrowFieldsTableView(),$cbundle->getKeymangerExtend()->dbgetrowFieldsTableView());
            $fieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->dbgetrowFieldsTableView(),$cbundle->getKeymangerExtend()->dbgetrowFieldsTableView());
            $this->getTable()-> makeColumngroup($fieldGroupList,$fieldList); 
        }
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
		$badiuSession->setSystemdata($this->getSystemdata());
        $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');

		if(!empty($fieldstype)){$this->getTable()->setColumntype($fieldstype);}
		
        foreach ($fields as $field) {
            //check column in sesion
            $pos = stripos($field->getName(), "_addcolumndynamically");
            if ($pos === false) {
				$this->getTable()->getColumn()->add($field->getName(), $this->getTranslator()->trans($field->getLabel()), $field->getInfo());
            } else {
               
                $fieldvaule = $utildata->getVaueOfArray($this->fparam, $field->getName());
               
                if ($fieldvaule) {
                   
                    $listcolumns = $badiuSession->getValue($field->getName());
                 
                    if (!empty($listcolumns)) {
						$dclumns=array();
						$dconfields=null;
						$dconfieldskey=null;
						
						foreach ($listcolumns as $key => $value) {array_push($dclumns,$key);}
						$dcolumparam=array('_addcolumndynamically'=>1,'_listcolumndynamically'=>$dclumns);
						if ($layout == 'indexCrud') {
							$dconfields=$cbundle->getDbTableViewFields($this->getKeymanger(),$this->getIsschedule(),$dcolumparam);
						}else if ($layout == 'viewDetail') {
							$dconfields=$cbundle->getDbRowTableViewFields($this->getKeymanger(),$this->getIsschedule(),$dcolumparam);
						}
						if(is_array($dconfields)){foreach ($dconfields as $drow) {$ifield=$drow->getName();$dconfieldskey[$ifield]=$drow;}}
                        foreach ($listcolumns as $key => $value) {
							$dfielconf=$utildata->getVaueOfArray($dconfieldskey, $key);
							$dfieldinfo=null;
							if(!empty($dfielconf)){$dfieldinfo= $dfielconf->getInfo();}
                            $this->getTable()->getColumn()->add($key, $value, $dfieldinfo);
                        }
                    }
                }
            }
        }
    }

    public function makeTableRows() {
       
        $this->getTable()->setCountrow($this->getData()->getCount());
        $this->getTable()->setCountschedulertask($this->getData()->getCountschedulertask());
      
        $format = $this->getContainer()->get('badiu.system.core.lib.format.generalformat');
        $format->setSessionhashkey($this->getSessionhashkey());
		$format->setSystemdata($this->getSystemdata());
        $htmllink = $this->getContainer()->get('badiu.system.core.lib.html.link');
        $htmllink->setSessionhashkey($this->getSessionhashkey());
		$htmllink->setSystemdata($this->getSystemdata());
        $htmllink->setIsexternalclient($this->getIsexternalclient());
        $htmllink->setIsschedule($this->getIsschedule());
		$htmllink->setIsservice($this->getIsservice());
        $formatreport = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_format');
        $selectrow = $this->getContainer()->get('badiu.system.core.report.data.selectrow');
        $selectrow->setColumns($this->getTable()->getColumn()->getList());
        $selectrow->setFormat($formatreport);
        $selectrow->init();
        
        $columnWithControl = $selectrow->addColumns($this->getTable()->getColumn()->getList());
        $this->getTable()->getColumn()->setList($columnWithControl);
        
        $_showhead = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_showhead'); //$this->getContainer()->get('request')->get('_showhead');
        $_infopaging = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_infopaging'); //$this->getContainer()->get('request')->get('_infopaging');
        if ($_showhead == 1) {
            array_push($this->listData, $columnWithControl);
        }
        $dbcolumn = $this->getDbRowColumn();
        if(!is_array($this->getData()->getRows())){return null;}
        if (empty($this->getData()->getRows()) || sizeof($this->getData()->getRows()) == 0) {
            return null;
        }
        
        foreach ($this->getData()->getRows() as $row) {
            $cont = 0;
            $drow = array();
            $listData = array();
            // foreach ($this->getTable()->getColumn()->getList()  as $key => $value) {
            foreach ($dbcolumn as $key => $value) {
                //echo "$key | $value <br/>";
                $info = array();
                if (array_key_exists($key, $this->getTable()->getColumn()->getInfo())) {
                    $info = $this->getTable()->getColumn()->getInfo()[$key];
                }
                if ($key == "_ctrl") {
                    if (empty($formatreport)) {
                        $drow[$key] = $this->makeLink($row);
                    }
                } else if ($key == "_checkbox") {
                    $drow[$key] = $selectrow->addData(0);
                } else {
                    $row = $this->getRowExtraValue($row, $key, $info);
                    if (is_array($row) && array_key_exists($key, $row)) {
                        $value = $format->get($key, $row, $row[$key], $info);
                        if (empty($formatreport)) { 
                              $value = $htmllink->get($info, $row, $value);
                          
                        }
                        $drow[$key] = $value;
                    }
                } 
                $listData[$key] = $value;
                $cont++;
            }
           
            $this->getTable()->getRow()->add($drow);
            array_push($this->listData, $listData);
        }

        
        if ($_infopaging) {
            $data = array();
            $pagination = $this->getContainer()->get('badiu.system.core.report.pagination');
			$pagination->setSystemdata($this->getSystemdata());
            $pagination->setSessionhashkey($this->getSessionhashkey());
            $pagination->setCountDbrows($this->getTable()->getCountrow());
            $pagination->setPageIndex($this->getTable()->getPageindex());
            $data['paginginfo'] = $pagination->getInfo();
            $data['counrows'] = $this->getTable()->getCountrow();
            $data['rows'] = $this->listData;
            $data['dashboard'] = $this->dashboardData;
            $this->listData = $data;
        }
    }


    public function makeTableInfo() {

    }

    public function makeTable($layout = 'indexCrud',$param=array('addcolumnstitle'=>1,'addinfo'=>1,'addrows'=>1,'changedata'=>1)) {
        $addcolumnstitle= $this->getUtildata()->getVaueOfArray($param,'addcolumnstitle');
        $addrows= $this->getUtildata()->getVaueOfArray($param,'addrows');
        $addinfo= $this->getUtildata()->getVaueOfArray($param,'addinfo');
        $changedata= $this->getUtildata()->getVaueOfArray($param,'changedata');
       if($addcolumnstitle){$this->makeTableColumnsTitle($layout);}
       if($addrows){$this->makeTableRows();}
       if($addinfo){$this->makeTableInfo();}
       if($changedata){$this->table = $this->changeReport($this->table);}
    }

    // public function getData(){;}
    // public function setData(BadiuReportData $em){;}
    //public function getTable(){;}
    // public function setTable(BadiuReportDataTable $table){;}
    //this start $this->dashboardsqlconfig
    public function getDbRowColumn() {
        $cont = 0;
        $column = array();
        if (is_array($this->getData()->getRows())) {
            foreach ($this->getData()->getRows() as $row) {
                if (is_array($row)) {
                    foreach ($row as $key => $value) {
                        $column[$key] = $key;
                    }
                }
                if ($cont == 0) {
                    break;
                }
                $cont++;
            }
        }
        //add dashboard config key on list of column
        $cbundle = $this->getContainer()->get('badiu.system.core.lib.config.bundle');
        $cbundle->setSessionhashkey($this->getSessionhashkey());
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->setKeymanger($this->getKeymanger());
        $cbundle->initKeymangerExtend($this->getKeymanger());

        $sqlkeyconf = $cbundle->getValue($cbundle->getKeymanger()->dbsearchFieldsTableViewDashboardSqlKey());
        if (empty($sqlkeyconf)) {
            $sqlkeyconf = $cbundle->getValue($cbundle->getKeymangerExtend()->dbsearchFieldsTableViewDashboardSqlKey());
        }
        if (!empty($sqlkeyconf)) {
            $httpQueryString = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
            $httpQueryString->setQuery($sqlkeyconf);
            $httpQueryString->makeParam();

            foreach ($httpQueryString->getParam() as $key => $value) {
                $keysqlindex = "$key/dashboardsqlindex";
                $keysqlkey = "$key/dashboardsqlkey";

                $p = explode("|", $value);
                $keyindex = null;
                $keykey = null;
                if (isset($p[0])) {
                    $keyindex = $p[0];
                }
                if (isset($p[1])) {
                    $keykey = $p[1];
                }
                $this->dashboardsqlconfig[$keysqlindex] = $keyindex;
                $this->dashboardsqlconfig[$keysqlkey] = $keykey;

                $column[$key] = $key;
            }
        }


        foreach ($this->getTable()->getColumn()->getList() as $key => $value) {
            $column[$key] = $key;
        }
        return $column;
    }

    public function exportTable($format) {
        if($this->getCachereport()){$this->getFilemanagereport()->download();}
        $listcolumn = $this->getTable()->getColumn()->getList();
        unset($listcolumn['_ctrl']);
        $this->getTable()->getColumn()->setList($listcolumn);
        $xport = new BadiuReporExport($this->getTable(), $format);
        $xport->download();
        
        
    }
    public function getMailAttach($format) {
        $listcolumn = $this->getTable()->getColumn()->getList();
        unset($listcolumn['_ctrl']);
        $this->getTable()->getColumn()->setList($listcolumn);
        $xport = new BadiuReporExport($this->getTable(), $format);
        return $xport->attachToMail();
    }
    public function getRowExtraValue($row, $columun, $info) {
        // print_r($columun);exit;
        if (is_array($row) && !array_key_exists($columun, $row)) {
            $row[$columun] = null;
            $datakey = null;
            $indexsql = null;

            if (isset($info['dashboardsqlindex'])) {
                $indexsql = $info['dashboardsqlindex'];
            }
            if (isset($info['dashboardsqlkey'])) {
                $datakey = $info['dashboardsqlkey'];
            }

            if (empty($indexsql)) {
                $kd = "$columun/dashboardsqlindex";
                if (isset($this->dashboardsqlconfig[$kd])) {
                    $indexsql = $this->dashboardsqlconfig[$kd];
                }
            }
            if (empty($datakey)) {
                $ki = "$columun/dashboardsqlkey";
                if (isset($this->dashboardsqlconfig[$ki])) {
                    $datakey = $this->dashboardsqlconfig[$ki];
                }
            }
            if (!empty($datakey) && !empty($indexsql)) {

                $dkey = null;
                if (array_key_exists($datakey, $row)) {
                    $dkey = $row[$datakey];
                }
                $ddashboard = $this->getDashboardData();

                if (isset($ddashboard['rows'][$indexsql][$dkey][$columun])) {
                    $data = $ddashboard['rows'][$indexsql][$dkey][$columun];
                    $row[$columun] = $data;
                }
            }
        }

        return $row;
    }

    // public function getEm(){;}
    //  public function setEm(EntityManager $em){;}
    //  public function getTranslator(){;}
    //  public function setTranslator(Translator $translator){;}
    //news that is not in interface add after
    public function getRowDetail($id) {
        $data = $this->getContainer()->get($this->getKeymanger()->data());
        $dto = $data->findById($id, true);
        $cbundle = $this->getContainer()->get('badiu.system.core.lib.config.bundle');
        $cbundle->setSessionhashkey($this->getSessionhashkey());
		$cbundle->setSystemdata($this->getSystemdata());
        $fields = $cbundle->getDbRowViewFields($this->getKeymanger());
        $detail = array();
        foreach ($fields as $field) {
            $data = null;
            $value = null;
            if ($field->getName() == 'id') {
                $data = array('label' => $this->getTranslator()->trans('id'), 'value' => $value);
            }
            if ($field->getName() == 'name') {
                $data = array('label' => $this->getTranslator()->trans('name'), 'value' => $value);
            }
            if ($field->getName() == 'description') {
                $data = array('label' => $this->getTranslator()->trans('description'), 'value' => $value);
            }
            if ($field->getName() == 'param') {
                $data = array('label' => $this->getTranslator()->trans('param'), 'value' => $value);
            }
            if ($field->getName() == 'timecreated') {
                $data = array('label' => $this->getTranslator()->trans('timecreated'), 'value' => $value);
            }
            if ($field->getName() == 'timemodified') {
                $data = array('label' => $this->getTranslator()->trans('timemodified'), 'value' => $value);
            }
            if ($field->getName() == 'deleted') {
                $data = array('label' => $this->getTranslator()->trans('deleted'), 'value' => $value);
            }
            // if($field->getName()=='_ctrl'){$this->getTable()->getColumn()->add("_ctrl","");}
            array_push($data, $detail);
        }
        return $detail;
    }

    function addLink($key, $value) {
        $this->link[$key] = $value;
    }

    public function makeLink($row) {
        $id = $this->getUtildata()->getVaueOfArray($row,'id');
        $html = "";
        return "";
        /*  $editicon=$this->getContainer()->get('templating.helper.assets')->getUrl('bundles/badiuthemebase/image/icons/edit.gif');
          $copyicon=$this->getContainer()->get('templating.helper.assets')->getUrl('bundles/badiuthemebase/image/icons/copy.gif');
          $deleteicon=$this->getContainer()->get('templating.helper.assets')->getUrl('bundles/badiuthemebase/image/icons/delete.gif');
          $removeicon=$this->getContainer()->get('templating.helper.assets')->getUrl('bundles/badiuthemebase/image/icons/remove.gif');

          $html.="<a href='".$this->getRouter()->generate($this->getLink()['edit'],array('id'=>$id))."'> <img src=\"$editicon\" alt=\"Editar\"/></a> | ";
          $html.="<a href='".$this->getRouter()->generate($this->getLink()['copy'],array('id'=>$id))."'>  <img src=\"$copyicon\" alt=\"Copiar\"/></a> | ";
          $html.="<a href='".$this->getRouter()->generate($this->getLink()['delete'],array('id'=>$id))."'><img src=\"$deleteicon\" alt=\"Copiar\"/></a> | ";
          $html.="<a href='".$this->getRouter()->generate($this->getLink()['remove'],array('id'=>$id))."'> <img src=\"$removeicon\" alt=\"Excluir\"/></a>  ";
          $html.="<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\"> <img src=\"$removeicon\" alt=\"Excluir\"/></a>  ";
         */

        $parentid = $this->getContainer()->get('request')->get('parentid');
        if (empty($parentid)) {
            if ($this->getRouter()->getRouteCollection()->get($this->getLink()['edit']) !== null) {
                $html .= "<a href='" . $this->getRouter()->generate($this->getLink()['edit'], array('id' => $id)) . "' class=\"icon\"><span class=\"glyphicon glyphicon-pencil\"></span></a>  ";
            }
        } else {
            if ($this->getRouter()->getRouteCollection()->get($this->getLink()['edit']) !== null) {
                $html .= "<a href='" . $this->getRouter()->generate($this->getLink()['edit'], array('id' => $id, 'parentid' => $parentid)) . "' class=\"icon\"><span class=\"glyphicon glyphicon-pencil\"></span></a>  ";
            }
        }
        if ($this->getRouter()->getRouteCollection()->get($this->getLink()['copy']) !== null) {
            $html .= "<a href='" . $this->getRouter()->generate($this->getLink()['copy'], array('id' => $id)) . "' class=\"icon\">  <span class=\"glyphicon glyphicon-copy\"></span></a>  ";
        }

        if ($this->getRouter()->getRouteCollection()->get($this->getLink()['delete']) !== null) {
            $html .= "<a class=\"icon\" href=\"#\"> <span class=\"glyphicon glyphicon-trash\" data-_service=\"badiu.system.core.functionality.report.service\"   data-bkey=\"" . $this->keymanger->getBaseKey() . "\"   data-_function=\"delete\"   data-id=\"$id\"  data-toggle=\"modal\" data-target=\"#badiu_modal_table_delete\" ></span></a>  ";
            //$html.="<a href='".$this->getRouter()->generate($this->getLink()['delete'],array('id'=>$id))."' class=\"icon\"><span class=\"glyphicon glyphicon-trash\"></span></a>  ";
        }

        if ($this->getRouter()->getRouteCollection()->get($this->getLink()['remove']) !== null) {
            // $html.="<a href='".$this->getRouter()->generate($this->getLink()['remove'],array('id'=>$id))."' class=\"icon\"> <span class=\"glyphicon glyphicon-remove\"></span></a>  ";
            $html .= "<a class=\"icon\" href=\"#\"> <span class=\"glyphicon glyphicon-remove\" data-_service=\"badiu.system.core.functionality.report.service\"   data-bkey=\"" . $this->keymanger->getBaseKey() . "\"   data-_function=\"remove\"   data-id=\"$id\"  data-toggle=\"modal\" data-target=\"#badiu_modal_table_delete\" ></span></a>  ";
        }



        return $html;
    }
    

    public function controller() {
       
        $rcontrollerkey='badiu.system.core.functionality.report.controller';
        $cbundle = $this->getContainer()->get('badiu.system.core.lib.config.bundle');
        $cbundle->setSessionhashkey($this->getSessionhashkey());
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->setKeymanger($this->getKeymanger());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        //level 0
        $rcontrollerkeycustum= $cbundle->getKeymangerExtend()->reportController();
      
        //level 1
        if(!$this->getContainer()->has($rcontrollerkeycustum)){
            $rcontrollerkeycustum= $cbundle->getKeymangerExtend()->reportController(1);
        }
      
        //level 2
        if(!$this->getContainer()->has($rcontrollerkeycustum)){
             $rcontrollerkeycustum= $cbundle->getKeymangerExtend()->reportController(2);
        }
   
        //level 3
        if(!$this->getContainer()->has($rcontrollerkeycustum)){ 
            $rcontrollerkeycustum= $cbundle->getKeymangerExtend()->reportController(3);
        }
        
        //set $rcontrollerkey as default
        if(!$this->getContainer()->has($rcontrollerkeycustum)){
           $rcontrollerkeycustum=$rcontrollerkey;
        }

      
        if(!$this->getContainer()->has( $rcontrollerkeycustum)){
            return null;
        }
      
        $rcontroller=$this->getContainer()->get($rcontrollerkeycustum);
        $limitservice=$this->getContainer()->get($rcontrollerkeycustum);
        $limitservice->setFconfig($this->getFconfig());
        $limitservice->setFparam($this->getFparam());
		$result=$limitservice->exec();
        $this->setFparam($limitservice->getFparam());
        return $result;
    }
     
    
    public function countSchedulerTask($entity,$key,$parentid) {
        $sctask = $this->getContainer()->get('badiu.system.scheduler.task.data');
        $param=array('entity'=>$entity,'deleted'=>0,'functionalitykey'=>$key);
        if(!empty($parentid)){$param['moduleinstance']=$parentid;} 
		//print_r($param);exit;
       $result= $sctask->countGlobalRow($param);
       return $result;
    }

    
    public function initFilemanagereport($format) {
        $this->filemanagereport=$this->getContainer()->get('badiu.system.file.lib.managereport');
        $this->filemanagereport->setSessionhashkey($this->getSessionhashkey());
		$this->filemanagereport->setSystemdata($this->getSystemdata());
		$this->filemanagereport->setFormat($format);
	
    }
	 public function initFreportDynamic($fparam) {
		if(!$this->getContainer()->has('badiu.sync.freport.freport.lib.factoryservicereportdynamickey')){return null;}
		$factorysdk=$this->getContainer()->get('badiu.sync.freport.freport.lib.factoryservicereportdynamickey');
		$factorysdk->setSessionhashkey($this->getSessionhashkey());
		$factorysdk->setSystemdata($this->getSystemdata());
		
		$freportid=$this->getUtildata()->getVaueOfArray($fparam, '_freportid');
		$force = $this->getUtildata()->getVaueOfArray($fparam, '_force');
		$dkey = $this->getUtildata()->getVaueOfArray($fparam, '_dkey');
		if(empty($dkey)){$dkey=$this->getKey();}
		$faparam=array('_freportid'=>$freportid,'_dkey'=>$dkey,'_force'=>$force);
		$factorysdk->initSession($faparam);
    }
	
    public function getContainer() {
        return $this->container;
    }

    public function getEm() {
        return $this->em;
    }

    public function getTranslator() {
		if(!empty($this->translator) && empty($this->translator->getSystemdata())){$this->translator->setSystemdata($this->getSystemdata());}
		return $this->translator;
    }

    public function getRouter() {
        return $this->router;
    }

    public function getData() {
        return $this->data;
    }

    public function getTable() {
        return $this->table;
    }

    public function getLink() {
        return $this->link;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }

    public function setEm(EntityManager $em) {
        $this->em = $em;
    }

    public function setTranslator(Translator $translator) {
        $this->translator = $translator;
    }

    public function setRouter(Router $router) {
        $this->router = $router;
    }

    public function setData(BadiuReportData $data) {
        $this->data = $data;
    }

    public function setTable(BadiuReportDataTable $table) {
        $this->table = $table;
    }

    public function setLink($link) {
        $this->link = $link;
    }

    public function getModule() {
        return $this->module;
    }

    public function getFunctionality() {
        return $this->functionality;
    }

    public function setModule($module) {
        $this->module = $module;
    }

    public function setFunctionality($functionality) {
        $this->functionality = $functionality;
    }

    public function getKeymanger() {
        return $this->keymanger;
    }

    public function setKeymanger(BadiuKeyManger $keymanger) {
        $this->keymanger = $keymanger;
    }

    public function getDashboardData() {
        return $this->dashboardData;
    }

    public function setDashboardData($dashboardData) {
        $this->dashboardData = $dashboardData;
    }

    public function getListData() {
        return $this->listData;
    }

    public function setListData($listData) {
        $this->listData = $listData;
    }

    function getDashboardsqlconfig() {
        return $this->dashboardsqlconfig;
    }

    function setDashboardsqlconfig($dashboardsqlconfig) {
        $this->dashboardsqlconfig = $dashboardsqlconfig;
    }

    function getDefaultdatafilterconfig() {
        return $this->defaultdatafilterconfig;
    }

    function setDefaultdatafilterconfig($defaultdatafilterconfig) {
        $this->defaultdatafilterconfig = $defaultdatafilterconfig;
    }

    function getIsexternalclient() {
        return $this->isexternalclient;
    }

    function setIsexternalclient($isexternalclient) {
        $this->isexternalclient = $isexternalclient;
    }

    function getDefaultdatafilterconfigonopenform() {
        return $this->defaultdatafilterconfigonopenform;
    }

    function setDefaultdatafilterconfigonopenform($defaultdatafilterconfigonopenform) {
        $this->defaultdatafilterconfigonopenform = $defaultdatafilterconfigonopenform;
    }

    function getDatasource() {
        return $this->datasource;
    }

    function getServiceid() {
        return $this->serviceid;
    }

    function getIsschedule() {
        return $this->isschedule;
    }

    function setDatasource($datasource) {
        $this->datasource = $datasource;
    }

    function setServiceid($serviceid) {
        $this->serviceid = $serviceid;
    }

    function setIsschedule($isschedule) {
        $this->isschedule = $isschedule;
    }

    function getFparam() {
        return $this->fparam;
    }

    function setFparam($fparam) {
        $this->fparam = $fparam;
    }
    function getPaginglimit() {
        return $this->paginglimit;
    }

    function setPaginglimit($paginglimit) {
        $this->paginglimit = $paginglimit;
    }

    function getFconfig() {
        return $this->fconfig;
    }

    function setFconfig($fconfig) {
        $this->fconfig = $fconfig;
    }
 
    	
    public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }

    function getKey() {
        return $this->key;
    }

    function setKey($key) {
        $this->key = $key;
    }

    function getUtildata() {
       if(empty($this->utildata)){ $this->utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');}
        return $this->utildata;
    }

    function setUtildata($utildata) {
        $this->utildata = $utildata;
    }

    function getFilemanagereport() {
        return $this->filemanagereport;
    }

    function setFilemanagereport($filemanagereport) {
        $this->filemanagereport = $filemanagereport;
    }

    function getCachereport() {
        return $this->cachereport;
    }

    function setCachereport($cachereport) {
        $this->cachereport = $cachereport;
    }
	
	  function getGerenralparamconfig() {
        return $this->gerenralparamconfig;
    }

    function setGerenralparamconfig($gerenralparamconfig) {
        $this->gerenralparamconfig = $gerenralparamconfig;
    }
	
	function getIsservice() {
        return $this->isservice;
    }
 function setIsservice($isservice) {
        $this->isservice = $isservice;
    }
	
		public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
