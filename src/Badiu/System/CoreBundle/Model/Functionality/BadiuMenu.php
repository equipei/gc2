<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class BadiuMenu  {
    /**
     * @var Container
     */
    private $container;
    
       /**
      * @var BadiuKeyManger
     */
    private $keymanger;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var integer
     */
    private $parentid;
	/**
     * @var boolean
     */
    private $isexternalclient;

     /** its used for multiple entity cron to isolate session
     * @var integer 
     */ 
    private $sessionhashkey;
	
	 private $title;
	 
	 /**
	 * @var string
	 */
	private $lang;
	private $systemdata;
   function __construct(Container $container,$baseKey=null) {
                $this->container=$container;
                $this->keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
                $this->translator=$this->container->get('translator');
                if($baseKey!=null){$this->keymanger->setBaseKey($baseKey);}
               $this->title=null;
			   $this->lang=null;
      }
    
       public function get($position="") {
         
           $htmllink=$this->getContainer()->get('badiu.system.core.lib.html.link');
           $htmllink->setParentid($this->getParentid());
           $htmllink->setIsexternalclient($this->isexternalclient);
           $htmllink->setSessionhashkey($this->getSessionhashkey());
           $confbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
           $confbundle->setSessionhashkey($this->getSessionhashkey());
           $confbundle->setKeymanger($this->keymanger);
           $keymenu=$this->getKeymanger()->moduleMenu($position);
           $custumKey=$this->getKeyCustumMenuBar($position);
          $factoryserviceexpression=$this->getContainer()->get('badiu.system.core.lib.util.factoryserviceexpression');
           if(!empty($custumKey)){

               $custumKeyValue=$confbundle->getValue($custumKey);
               			  
               //check if is menu global
               if(strpos($custumKeyValue,'l|')!== false){
                   $p=explode("|",$custumKeyValue);
                  
                   if(isset($p[1])){$custumKey=$p[1];}
                 
               }
              
               if(!empty($custumKeyValue)){
                   $keymenu=$custumKey;
               }
           }
		   
         
		  $confmenu=$confbundle->getValue($keymenu);
		  $confmenu=str_replace("e{","{",$confmenu);
		  $confmenu=$factoryserviceexpression->change($confmenu);
		
		  $httpQueryString=$this->getContainer()->get('badiu.system.core.lib.http.querystring');
           $httpQueryString->setQuery($confmenu);
           $httpQueryString->makeParam();
           $confmenu= $httpQueryString->getParam();
		
           $confmenu=$this->checkPermission($keymenu,$confmenu);
           $confmenu=$this->execFilter($confmenu);
        
           $keyofcategory=$keymenu.'.category';
          
           $confmenucategory=$confbundle->getValueQueryString($keyofcategory);
           $menuitemcat=$this->getListItemmenuCagegory($confmenucategory);
           $categorymeuitem=$this->getListCagegoryItemMenu($confmenucategory);
          
         
           $menu=array();
           $currenteRoute = $this->getContainer()->get('request')->get('_route');
           $dkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey');
           if(!empty($dkey)){$currenteRoute=$dkey;}
         
           $current=0;
          
           foreach ($confmenu as $key => $value){
		$category=null;
               
               if(isset($menuitemcat[$key])){$category=$menuitemcat[$key];}
               if(empty($category)){
				  
				
                    $plink=$htmllink->makeParamByConfig($value);
					 $plink['permposition']=$position;
					 $link=$htmllink->getHtml($key,$plink,'elements');
					 

                     //review delete  - dont show subrout with db  data
                    if($currenteRoute== $plink['route']) {$current=1;}
					else if($currenteRoute== $plink['dkeyroute']) {$current=1;}
                    else { $current=0;}
               
                    
                     
                    if(empty($position)){$position= 'menu';}
                    if(!empty($link)){array_push($menu,array('type'=>'link','position'=>$position,'link'=>$link,'current'=>$current));}
                    /*if ($position=='navbar'){
                        if($current==0){ array_push($menu,array('type'=>'link','position'=>$position,'link'=>$link,'current'=>$current));}
                    }else{
                         array_push($menu,array('type'=>'link','position'=>$position,'link'=>$link,'current'=>$current));
                     }*/
               }
             
           }
           //loop category
         
           foreach ($categorymeuitem as $cmenu){
               if(empty($position)){$position= 'menu';}
               $lcatmenu=array();
               $name=null;
               $itemsmenu=null;
               if(isset($cmenu['name'])){$name=$cmenu['name'];}
               if(isset($cmenu['subitems'])){$itemsmenu=$cmenu['subitems'];}
               $lcatmenu['name']=$name;
               $lcatmenu['position']=$position;
               $lcatmenu['type']='category';
			   $lcatmenu['current']=0;
               $bsubmenuitem=array();
               
               foreach ($itemsmenu as $imv) {
				    $value='';
                   if(isset($confmenu[$imv])){$value=$confmenu[$imv];}
                   
                  if(!empty($value)){
                      $plink=$htmllink->makeParamByConfig($value);
					  
					  $plink['permposition']=$position;
                      $link=$htmllink->getHtml($imv,$plink,'elements');

                    //review delete  - dont show subrout with db  data
                    if($currenteRoute== $plink['route'] ) {$current=1;$lcatmenu['current']=1;}
					else if($currenteRoute== $plink['dkeyroute']) {$current=1;$lcatmenu['current']=1;}
					else { $current=0;}
                    array_push($bsubmenuitem,array('link'=>$link,'current'=>$current));
					
                   }
               }
               
                $lcatmenu['items']=$bsubmenuitem;
                array_push($menu,$lcatmenu);
           }
 
           //nav bar
           if ($position=='navbar'){
                 $label=$this->translator->trans($currenteRoute);
				 if(!empty($this->title)){$label=$this->title;}
				// echo $currenteRoute;exit;
               array_push($menu,array('position'=>$position,'link'=>array('url'=>null,'name'=>$label,'target'=>null),'current'=>1));
           }
		
        /*echo "<pre>";
           print_r($menu);
           echo "</pre>";exit;*/
//exit;

          return $menu;
      }
	  
	  public function makeRouteLinkParam() {

    }
   

    public function getKeyCustumMenuBar($position) {
       // if($position!='navbar') return null;
        
        //check if route is view (end with view)
       $systemoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
       $isView=$systemoperation->isView();
       $currenteRoute=$this->container->get('request')->get('_route'); 
       $querystringsystem=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem');
       $dkey=$querystringsystem->getParameter('_dkey');
       $mkey=$querystringsystem->getParameter('_mkey');
	   $mkeyparam=$this->getMenuKeyParam($mkey);
	  
       if(!empty($dkey)){ $currenteRoute=$dkey;}
	   
       if(!empty($mkeyparam->key)){ 
				if($position=='navbar' && $mkeyparam->position=='navbar'){$currenteRoute=$mkeyparam->key;}
				else if($mkeyparam->position!='navbar'){$currenteRoute=$mkeyparam->key;}
				$isView=$systemoperation->isView($mkeyparam->key); 
		  }
		
       if($this->isexternalclient){
           $currenteRoute=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_key');
           $dkey=$querystringsystem->getParameter('_dkey');
           $mkey=$querystringsystem->getParameter('_mkey');
		   $mkeyparam=$this->getMenuKeyParam($mkey);
		 
           if(!empty($dkey)){ $currenteRoute=$dkey;}
           if(!empty($mkeyparam->key)){ 
				if($position=='navbar' && $mkeyparam->position=='navbar'){$currenteRoute=$mkeyparam->key;}
				else if($mkeyparam->position!='navbar'){$currenteRoute=$mkeyparam->key;}
		  }
		   
           $isView=$systemoperation->isView($currenteRoute);
        }
     
       if($isView){
			
            $key= $currenteRoute;
			
			if(!empty($position)){$key=$key.".menu.".$position;}
            else{ $key=$key.".menu";}
            return $key;
        }
 
        $pos = strrpos($currenteRoute, '.');
        $key = substr($currenteRoute,0, $pos);
        if(!empty($position)){$key=$key.".menu.".$position;}
        else{ $key=$key.".menu";}
		
       return $key;
    }

	public function getMenuKeyParam($mkey) {
		 $result=new \stdClass();
		  $result->position="";
		  $result->key="";
		 if(empty($mkey)){return $result;}
		  $pos=stripos($mkey, "-");
         if($pos=== false){ $result->key=$mkey;}
		 $p=explode("-",$mkey);
		
		 if(isset($p[0]) && isset($p[1])){
			 $result->position=$p[0];
			 $result->key=$p[1];
		 }
        return $result;
	}
     public function getListItemmenuCagegory($dataconf) {
         $menuitemcat=array();
         foreach ($dataconf  as $key => $value){
             $pos=stripos($value, ",");
             $pvalue=array();
             if($pos=== false){
                 if(!empty($value)){$menuitemcat[$value]=$key;}
            }else{
                $p= explode(",", $value);
                foreach ($p as $v) {
                    $menuitemcat[$v]=$key;
                }
            }
         }
         
         return $menuitemcat;
     }
     
      public function getListCagegoryItemMenu($dataconf) {
         
         $categorymeuitem=array();
         foreach ($dataconf  as $key => $value){
                $newitem=array();
                $newitem['name'] = $this->translator->trans($key);
                $newitem['key']=$key;
                $itemmenu=array();
                $pos=stripos($value, ",");
              
                if($pos=== false){
                  if(!empty($value)){array_push($itemmenu,$value);}
                }else{
                     $p= explode(",", $value);
                     $itemmenu=$p;
                 }
               $newitem['subitems']=$itemmenu;
               array_push($categorymeuitem,$newitem);
         }
         
         return $categorymeuitem;
     }
     
     public function execFilter($confmenu) {
  
        $newconfig=array();
        foreach ($confmenu as $key => $value){
            $pos=stripos($key, "_filter");
            if($pos=== false){$newconfig[$key]=$value;}
            else{
               
                $posp=stripos($value, "|");
                if($posp!== false){
                    $p = explode("|",$value);
                    $type='s';
                  
                    $service=null;
                    $function='exec';
                    if(isset($p[0])) {$type=$p[0];}
                    if(isset($p[1])) {$service=$p[1];}
                    if(isset($p[2])) {$function=$p[2];}
                    if($type=='s' && $this->getContainer()->has($service)){
                        
                        $dataservice=$this->getContainer()->get($service);
                        if(method_exists($dataservice,$function)){
                           
                            $flist=$dataservice->$function();
                            if(!empty($flist)){
                                foreach ($flist as $fkey => $fvalue){
                                    $newconfig[$fkey]=$fvalue;
                                }
                            }
                        }
                    }
                }
            }
            
        }
        return $newconfig;
     }
     public function checkPermission($key,$listlink) {
        
        $keyutil=$this->getContainer()->get('badiu.system.core.lib.config.keyutil');
        $isnavbar=$keyutil->hasText($key,".menu.navbar");
        //process if is menu or menu.content
        if($isnavbar){return $listlink;}
       
        $newlistmenu=array();
        $libperm=$this->getContainer()->get('badiu.system.access.permission');
        $badiuSession = $this->container->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $datasession = $badiuSession->get();
        
        
   
        $listpem=$datasession->getPermissions();
        foreach ($listlink as $lkey=> $ulink) {

		  if(is_array($listpem)){																								
            foreach ($listpem as $keyperm) {
                $routlink=$this->getRouteOfLink($ulink);
                if(!empty($routlink)){

                    $haspermission=$libperm->hasLinkPermission($keyperm,$routlink);
                     if($haspermission){
                        $newlistmenu[$lkey]=$ulink;
                        break;
                    }
                }
                
            }
		  }
        } //end foreach ($listlink as $lkey=> $ulink) {

         
        //check daned and remove from list
        $listpemdenied=$datasession->getPermissionsdenied();
        if(is_array($listpemdenied) && sizeof($listpemdenied)> 0){
            foreach ($listlink as $lkey=> $ulink) {
                foreach ($listpemdenied as $keyperm) {
                    $routlink=$this->getRouteOfLink($ulink);
                    if(!empty($routlink)){
        
                        $haspermission=$libperm->hasLinkPermission($keyperm,$routlink);
                        if($haspermission){
                            if (array_key_exists($lkey,$newlistmenu)){unset($newlistmenu[$lkey]);}
                        }
                    }
                
                }
            }
        }
        
        

             //check daned by serverservice and remove from list
           /*  echo "<pre>";
             print_r($newlistmenu);
             echo "<pre>";echo "<hr>";*/
          $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
          if(!empty($serviceid)){
             $lpermdanedservice=  $badiuSession->getValue("badiu.admin.server.service.".$serviceid.".permissions.denied");
              if(is_array($lpermdanedservice) && sizeof($lpermdanedservice)> 0){
                foreach ($listlink as $lkey=> $ulink) {
                    foreach ($lpermdanedservice as $keyperm) {
                      
                        $routlink=$this->getRouteOfLink($ulink);
                        
                        if(!empty($routlink)){
                              $haspermission=$libperm->hasLinkPermission($keyperm,$routlink);
                             // echo "$haspermission |$keyperm |$routlink <br>";
                             if($haspermission){
                                if (array_key_exists($lkey,$newlistmenu)){unset($newlistmenu[$lkey]);}
                            }
                        }
                        
                    } 
                }
                
              }
          }//exit;
        
              
            
            // print_r($newlistmenu);exit;
      /*print_r($listlink); "<hr>";
      print_r($listpem);echo "<hr>";
      print_r($newlistmenu);echo "<hr>";exit;*/
        return $newlistmenu;
     }

     public function getRouteOfLink($conf) {
        $utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
         $result=null;
         $pos=stripos($conf, "|");
         if($pos=== false){return $conf;}
         $lconfig= explode("|",$conf);
          
         $type=$utildata->getVaueOfArray($lconfig,0);
         if(strlen($type)==1){
           $result=$utildata->getVaueOfArray($lconfig,1);
           $subparam=$utildata->getVaueOfArray($lconfig,2);
           $dkey=$this->getDkeyByParam($subparam);
           if(!empty($dkey)){$result=$dkey;}
         }else{
            $subparam=$utildata->getVaueOfArray($lconfig,1);
            $dkey=$this->getDkeyByParam($subparam);
            $result=$type;
            if(!empty($dkey)){$result=$dkey;}
         }
         
         
         return $result;
    }
    public function getDkeyByParam($subparam) {
        $result=null;
        if(empty($subparam)){return null;}
        $listparam=array();
        $pos=stripos($subparam, ",");
        if($pos=== false){
            $listparam=array($subparam);
        }else{
            $listparam= explode(",", $subparam);
        }
        $result=$this->getContainer()->get('badiu.system.core.lib.html.link')->getDkey($listparam);
       
        return $result;
   }
      public function getContainer() {
          return $this->container;
      }

      public function getKeymanger() {
          return $this->keymanger;
      }

      public function setContainer(Container $container) {
          $this->container = $container;
      }

      public function setKeymanger(BadiuKeyManger $keymanger) {
          $this->keymanger = $keymanger;
      }

    public function getTranslator() {
		if(!empty($this->translator) && empty($this->translator->getSystemdata())){$this->translator->setSystemdata($this->getSystemdata());}
		$this->translator->setLocale($this->getLang());
        return $this->translator;
    }

    public function setTranslator(Translator $translator) {
        $this->translator = $translator;
    }

    /**
     * @return int
     */
    public function getParentid()
    {
        return $this->parentid;
    }

    /**
     * @param int $parentid
     */
    public function setParentid($parentid)
    {
        $this->parentid = $parentid;
    }

    function getIsexternalclient() {
        return $this->isexternalclient;
    }

    function setIsexternalclient($isexternalclient) {
        $this->isexternalclient = $isexternalclient;
    }
    public function getSessionhashkey() {
        return $this->sessionhashkey;
   }

   public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
   }
 
  public function getTitle() {
        return $this->title;
   }

   public function setTitle($title) {
       $this->title = $title;
   }
   
      /**
     * @return string
     */
    public function getLang() {
		if(empty($this->lang)){
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			$this->lang=$badiuSession->get()->getLang();
		}
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang) {
        $this->lang = $lang;
    }
	
		public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
