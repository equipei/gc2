<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Doctrine\ORM\EntityManager;
class BadiuFormDataOptions {
    
     /**
     * @var Container
     */
    private $container;
    
       /**
      * @var BadiuKeyManger
     */
    private $keymanger;
    
    /**
     * @var EntityManager
     */
    private $em; 
    
    
    /**
     * @var Translator
     */
    private $translator;
    
        /**
     * @var object
     */
    private $utildata = null;
    
        private $json = null;
    
             /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
		private $systemdata;
      function __construct(Container $container,$baseKey=null) {
                $this->container=$container;
                $db ="default"; //$this->container->get('badiu.system.access.session')->get()->getDbapp(); 
                $this->em=$this->container->get('doctrine')->getEntityManager($db); 
               
                $this->keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
                if($baseKey!=null){$this->keymanger->setBaseKey($baseKey);}
                
                
                 $this->translator=$this->container->get('translator');
                 $this->utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
                 $this->json = $this->getContainer()->get('badiu.system.core.lib.util.json');
      }
      
    public function get($key){
         return array();
     }
 
     
       public function getContainer() {
           return $this->container;
       }

       public function setContainer(Container $container) {
           $this->container = $container;
       }
       
       public function getEm() {
           return $this->em;
       }

       public function setEm(EntityManager $em) {
           $this->em = $em;
       }

       
  public function getKeymanger() {
      return $this->keymanger;
  }

  public function setKeymanger(BadiuKeyManger $keymanger) {
      $this->keymanger = $keymanger;
  }
  
  public function getTranslator() {
	   $this->translator->setSystemdata($this->getSystemdata());
      return $this->translator;
  }

  public function setTranslator(Translator $translator) {
      $this->translator = $translator;
  }

  function getUtildata() {
      return $this->utildata;
  }

  function setUtildata($utildata) {
      $this->utildata = $utildata;
  }

  function getJson() {
      return $this->json;
  }

  function setJson($json) {
      $this->json = $json;
  }

  public function getSessionhashkey() {
    return $this->sessionhashkey;
}

public function setSessionhashkey($sessionhashkey) {
   $this->sessionhashkey = $sessionhashkey;
   $this->translator->setSessionhashkey($sessionhashkey);
}


	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
