<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class BadiuCheckForm  extends BadiuModelLib {
    
    /**
     * @var array
     */
    private $param;
    /**
     * @var object
     */
     private $response;
     
      /**
     * @var object
     */
     private $kminherit;
     
    private $cpage;
    private $cform;
    private $key;
    function __construct(Container $container) {
                parent::__construct($container);
                 $this->response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
    }
    
     public function validatation(){
         
     }
 
      
   
    function getParamItem($item) {
        $value=null;
       if (is_array($this->param) && array_key_exists($item,$this->param)){
            $value=$this->param[$item];
       }
       return $value;
   }  
   
function isedit() {
        $isedit=false;
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        if($sysoperation->isEdit($this->getKey())){$isedit=true;}
      return $isedit;
  }
     function getParam() {
         return $this->param;
     }

     function setParam($param) {
         $this->param = $param;
     }

     function getResponse() {
         return $this->response;
     }

     function getKminherit() {
         return $this->kminherit;
     }

     function setResponse($response) {
         $this->response = $response;
     }

     function setKminherit($kminherit) {
         $this->kminherit = $kminherit;
     }

     function getCpage() {
         return $this->cpage;
     }

     function getCform() {
         return $this->cform;
     }

     function setCpage($cpage) {
         $this->cpage = $cpage;
     }

     function setCform($cform) {
         $this->cform = $cform;
     }

     function getKey() {
         return $this->key;
     }

     function setKey($key) {
         $this->key = $key;
     }


}
