<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class BadiuWebService extends BadiuModelLib{

      /**
     * @var integer
     */
    private $serviceid=null;
    function __construct(Container $container) {
            parent::__construct($container);
            $querystringsystem=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem');
            $this->serviceid=$querystringsystem->getParameter('_serviceid');
        }
        
        function execSql($sql,$type,$offset=0,$limit=10) {
                $response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
                $param['_key']='sql.command.exec';
                $param['query']=$sql;
                $param['type']= $type;
                $param['_serviceid']=$this->serviceid;
                $param['offset']=$offset;
                $param['limit']=$limit;
                $wsresponse=$this->getSearch()->execWebService($param);
                $response->set($wsresponse);
                $result=$response->getMessage();
                return $result;
         }
        function exeCommand($param) {
				$serviceid=$this->getUtildata()->getVaueOfArray($param,'_serviceid');
				if(empty($serviceid)){$param['_serviceid']=$this->serviceid;}
                $response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
                $wsresponse=$this->getSearch()->execWebService($param);
				$response->set($wsresponse);
                $result=$response->getMessage();
                return $result;
        }     
        function searchSingle($sql) {
                  $result=$this->execSql($sql,"S");
                return $result;
           }
         function search($sql,$offset=0,$limit=30) {
              $result=$this->execSql($sql,"M",$offset,$limit);
                return $result;
           }
        
           
              function getServiceid() {
                  return $this->serviceid;
              }

              function setServiceid($serviceid) {
                  $this->serviceid = $serviceid;
              }


}
