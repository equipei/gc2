<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class BadiuReportChangeData extends BadiuModelLib {
    
     function __construct(Container $container) {
               parent::__construct($container);
         
      }
  private $systemdata;
  public function searchCount($value) {
     
      return $value;
  }
  
 public function search($list) {
   
      return $list;
  }
  
  public function searchListSingle($list) {
     
      return $list;
  }
  
  public function searchList($list) {
     
      return $list;
  }
  
   public function find($data){
     
      return $data;
  }
  
  public function all($data){
     
      return $data;
  }
  
  public function report($table){
     
      return $table;
  }
  
  	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }
}
