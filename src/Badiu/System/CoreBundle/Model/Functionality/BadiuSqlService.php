<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class BadiuSqlService extends BadiuModelLib{

      /**
     * @var integer
     */
    private $serviceid=null;
    function __construct(Container $container) {
            parent::__construct($container);
            $querystringsystem=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem');
            $this->serviceid=$querystringsystem->getParameter('_serviceid');
        }
        
        function execSql($sql,$type,$offset=0,$limit=10,$originalresponse=false) {
                if(empty($this->serviceid)){return array();}
                $this->getSearch()->setSessionhashkey($this->getSessionhashkey());
				$this->getSearch()->setSystemdata($this->getSystemdata());
                $response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
                $param['query']=$sql; 
                $param['type']= $type;
                $param['_serviceid']=$this->serviceid;
                $param['offset']=$offset;
                $param['limit']=$limit;
                $wsresponse=$this->getSearch()->execSqlService($param);
				if($originalresponse){return $wsresponse;}
                $response->set($wsresponse);
				$result=$response->getMessage();

                return $result;
         }
        function exec() {
             if(empty($this->serviceid)){return array();}
            $querystringsystem=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem');
            $sql=$querystringsystem->getParameter('query');
            $type=$querystringsystem->getParameter('type');
            $result=$this->execSql($sql,$type);
            return $result;
        }     
        function searchSingle($sql) {
                  $result=$this->execSql($sql,"S");
                 return $result;
           }
         function search($sql,$offset=0,$limit=100) {
                $result=$this->execSql($sql,"M",$offset,$limit);
                return $result;
           }
        function service($param) {
            $this->getSearch()->setSessionhashkey($this->getSessionhashkey());
			$this->getSearch()->setSystemdata($this->getSystemdata());
            $wsresponse=$this->getSearch()->connKeyService($param);
           // $response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
           // $response->set($wsresponse);
            return $wsresponse;
        }
         function serviceResponse($wsresponse) {
			$response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
			 $response->set($wsresponse);
             $result=$response->getMessage();
             return $result;
		 }			 
              function getServiceid() {
                  return $this->serviceid;
              }

              function setServiceid($serviceid) {
                  $this->serviceid = $serviceid;
              }


}
