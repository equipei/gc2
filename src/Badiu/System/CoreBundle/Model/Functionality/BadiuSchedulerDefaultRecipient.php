<?php
namespace Badiu\System\CoreBundle\Model\Functionality;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class BadiuSchedulerDefaultRecipient extends BadiuModelLib{

  /**
   * lisf http filter param
   *
   * @var array
   */
   private $paramfilter;

   /**
    * Systema page object with out data
    *
    * @var object
    */
   private $page;

   /**
    * layout type can has vaule: indexCrud| viewDetail |frontpage | dashboard
    *
    * @var string
    */
   private $layout;

   /**
    * column key for split report to send ot each systemrecipient
    *
    * @var string
    */
    private $columnkey;

    /**
    * value column key for split report to send ot each systemrecipient
    *
    * @var string
    */
    private $columnkeyvalue;

     /**
   * config message to send
   *
   * @var array
   */
  private $reportmessage;

  /**
   * menssage formted to send when has onle one columnkey
   * 
   * @var string
   */
  private $formatedcontent;
   /**
    * result of previus scheduler exec with layout $result['resultinfo']['datashouldexec'] $result['resultinfo']['dataexec'] $result['resultinfo']['reportrowsize']
    *
    * @var array
    */
   private $resultinfoofexec;

   /**
    * nome of smtp user mail sender
    *
    * @var string
    */
   private $usersender=null;

    function __construct(Container $container) {
        parent::__construct($container);
   }


   public function init($param) {
    $this->paramfilter=$this->getUtildata()->getVaueOfArray($param,'paramfilter');
    $this->page=$this->getUtildata()->getVaueOfArray($param,'page');
    $this->layout=$this->getUtildata()->getVaueOfArray($param,'layout');
    $this->reportmessage=$this->getUtildata()->getVaueOfArray($param,'reportmessage');
    $this->resultinfoofexec=$this->getUtildata()->getVaueOfArray($param,'resultinfo');
    $this->columnkey=$this->getUtildata()->getVaueOfArray($param,'columnkey');
    $this->formatedcontent=$this->getUtildata()->getVaueOfArray($param,'formatedcontent');
    $this->setEntity($this->getUtildata()->getVaueOfArray($param,'entity'));
    $this->usersender=$this->getUtildata()->getVaueOfArray($param,'usersender');
  }

   public function getFormoptions() {

   }

   public function getRecipients() {

  }

  /**
   * Send report message for all report without split
   *
   * @return array
   */
  public function sendMessageWihtouSplit() {
    $table1= $this->getUtildata()->getVaueOfArray($this->page->getData(),'badiu_table1');
    $reportcountrows=count($table1->getRow()->getList());

    $mconfig=$this->getReportmessage();
    $sysrecipient=$this->getUtildata()->getVaueOfArray($mconfig,'recipient.sysuser',true);
    if(empty($sysrecipient)){return $this->getResultinfoofexec();}

    $this->setColumnkeyvalue($this->getUtildata()->getVaueOfArray($this->getParamfilter(),$this->getColumnkey()));
    if(empty($this->getColumnkeyvalue())){return $this->getResultinfoofexec();}

    $sysrrecipienttolist=$this->getRecipients();
    $sysrrecipientto=$this->castRecipientToStrinList($sysrrecipienttolist);
    if(empty($sysrrecipientto)){return $this->getResultinfoofexec();}
    
    $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
    if(empty($smtpservice)){$smtpservice='badiunetconfig';}
    $this->resultinfoofexec['smtpservice']=$smtpservice;

    $sendmsg=false;
    if($smtpservice=='badiunetconfig'){
      $mconfig['recipient']['to']=$sysrrecipientto;
      $mconfig['message']['body']=$this->getFormatedcontent();
  
      $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
	  $smtp->setSessionhashkey($this->getSessionhashkey());
	  $smtp->setSystemdata($this->getSystemdata());
      $smtp->setUsersender($this->getUsersender());
      $entity=$this->getEntity();
      $smtp->setEntity($entity);
      $sendmsg=$smtp->sendMail($mconfig);
    }else if ($smtpservice=='moodleconfig'){
     
        $parentid=$this->getUtildata()->getVaueOfArray($this->paramfilter,'parentid');
        $smtpserviceparam=array(); 
        $smtpserviceparam['_serviceid']=$parentid;
        $smtpserviceparam['mail']['subject']=$this->getUtildata()->getVaueOfArray($mconfig,'message.subject',true);
        $smtpserviceparam['mail']['message']=$this->getFormatedcontent();
        $smtpserviceparam['mail']['to']=$sysrrecipientto;
      
        $mdlsmtpservice=$this->getContainer()->get('badiu.moodle.mreport.lib.smtpservice');
        $mdlsmtpservice->setSessionhashkey($this->getSessionhashkey());
		$mdlsmtpservice->setSystemdata($this->getSystemdata());
        $smtpserviceparam=$mdlsmtpservice->castListEmail($smtpserviceparam);
        $resultmdlsms=$mdlsmtpservice->exec($smtpserviceparam);
        $emailsendto=$mdlsmtpservice->getFirstEmail($sysrrecipientto);
        $sendmsg=$mdlsmtpservice->checkResponse($resultmdlsms,$emailsendto);
          
    }

    
    $sysusrresult=array();
    foreach ($sysrrecipienttolist as $srow) {
      $srow['reportrowsize']=$reportcountrows;
      $srow['dataexec']=$sendmsg;
      $srow['role']=$sysrecipient;
      array_push($sysusrresult,$srow); 
    }
    
    
    $this->resultinfoofexec['resultinfo']['sysuser']=$sysusrresult;
    return $this->getResultinfoofexec();
  }

  /**
   * Send report message splited for each system recipient
   *
   * @return array
   */
  public function sendMessageSplited() {
   
     $table1= $this->getUtildata()->getVaueOfArray($this->page->getData(),'badiu_table1');
    $mconfig=$this->getReportmessage();

    //create copy of listcolumn
    $originallistcolumn=array();
    if(!empty($table1) && is_array($table1->getRow()->getList())){
      foreach ($table1->getRow()->getList() as $trow) {
        array_push($originallistcolumn,$trow);
      }
    }

    
    $sysrecipient=$this->getUtildata()->getVaueOfArray($mconfig,'recipient.sysuser',true);
    if(empty($sysrecipient)){return $this->getResultinfoofexec();}

    //get list of columnkey values
    $listvaluesofcolumnkeyvalues=array();
    if(!empty($table1) && is_array($table1->getRow()->getList())){
      foreach ($table1->getRow()->getList() as $trow) {
          $colunmnvalue=$this->getUtildata()->getVaueOfArray($trow,$this->getColumnkey());
          $listvaluesofcolumnkeyvalues[$colunmnvalue]=$colunmnvalue;
      }
    }
    
    //get list of email sysuser
    $listsysrecipientemail=array();
    $listsysrecipientemailunique=array();
    
    foreach ($listvaluesofcolumnkeyvalues as $lcv) {
        $this->setColumnkeyvalue($lcv);
        $currentlistemail=$this->getRecipients();
        $listsysrecipientemail[$lcv]=$currentlistemail;
        foreach ($currentlistemail as $ule) {
          $email=$this->getUtildata()->getVaueOfArray($ule,'email');
          $listsysrecipientemailunique[$email]=$ule;
        }
   }
   
   //list email and columnkeyvalue 
   $listsysrecipeintwithcolumnvaluekeys=array();
   foreach ($listsysrecipientemail as $ckey => $lcrow) {
      
      if(is_array($lcrow)){
         foreach ($lcrow as $lrp) {
            $email=$this->getUtildata()->getVaueOfArray($lrp,'email');
            if(array_key_exists($email, $listsysrecipeintwithcolumnvaluekeys)){
               $currentvalue=$listsysrecipeintwithcolumnvaluekeys[$email];
               array_push($currentvalue,$ckey);
               $listsysrecipeintwithcolumnvaluekeys[$email]=$currentvalue;
            }else{
              $listsysrecipeintwithcolumnvaluekeys[$email]=array($ckey);
            }
          }
      }
   }

    //send mail for each recipient
    $sysusrresultlist=array();
    $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
	$smtp->setSessionhashkey($this->getSessionhashkey());
	$smtp->setSystemdata($this->getSystemdata());
    $smtpservice=$this->getUtildata()->getVaueOfArray($mconfig,'smtpservice');
    if(empty($smtpservice)){$smtpservice='badiunetconfig';}
    $this->resultinfoofexec['smtpservice']=$smtpservice;
  
    $parentid=$this->getUtildata()->getVaueOfArray($this->paramfilter,'parentid');
    $mdlsmtpservice=$this->getContainer()->get('badiu.moodle.mreport.lib.smtpservice');
    $mdlsmtpservice->setSessionhashkey($this->getSessionhashkey());
	$mdlsmtpservice->setSystemdata($this->getSystemdata());
    foreach ($listsysrecipeintwithcolumnvaluekeys as $email => $lckv) {
        $listrow=array();
        foreach ($lckv as $rcolumnvalue) {
            foreach ($originallistcolumn as $trow) {
              $colunmnvalue=$this->getUtildata()->getVaueOfArray($trow,$this->getColumnkey());
              if($colunmnvalue==$rcolumnvalue){array_push($listrow,$trow);}
            }
        }
        $reportcountrows=count($listrow);
        $customcontent=$this->loadFileContent($listrow);
        if(empty($customcontent)){return $this->getResultinfoofexec();}
        $sendmsg=false;
        if($smtpservice=='badiunetconfig'){
            $mconfig['message']['body']=$customcontent;
            $mconfig['recipient']['to']=$email;
            $smtp->setUsersender($this->getUsersender());
            $entity=$this->getEntity();
            $smtp->setEntity($entity);
            $sendmsg=$smtp->sendMail($mconfig);
        }else if ($smtpservice=='moodleconfig'){
          $smtpserviceparam=array(); 
          $smtpserviceparam['_serviceid']=$parentid;
          $smtpserviceparam['mail']['subject']=$this->getUtildata()->getVaueOfArray($mconfig,'message.subject',true);
          $smtpserviceparam['mail']['message']=$customcontent;
          $smtpserviceparam['mail']['to']=array('email'=>$email);
          $resultmdlsms=$mdlsmtpservice->exec($smtpserviceparam);
       
         
          $emailsendto=$mdlsmtpservice->getFirstEmail($email);
          $sendmsg=$mdlsmtpservice->checkResponse($resultmdlsms,$emailsendto);
          
    }
       

       
       $sysusrresult= $this->getUtildata()->getVaueOfArray($listsysrecipientemailunique,$email);
       $sysusrresult['reportrowsize']=$reportcountrows;
       $sysusrresult['dataexec']=$sendmsg;
       $sysusrresult['role']=$sysrecipient;
      array_push($sysusrresultlist,$sysusrresult);
    }
   /* echo "<pre>";
    print_r($sysusrresultlist);
     echo "</pre>";*/
     $this->resultinfoofexec['resultinfo']['sysuser']=$sysusrresultlist;
    return $this->getResultinfoofexec();
  }

   /**
   * convet array list of recipient to list email splited by commas
   *
   * @return array
   */
  public function castRecipientToStrinList($list) {
     $txt="";
     if(empty($list)){return null;}
     if(!is_array($list)){return null;}
     $cont=0;
     foreach ($list as $row) {
        $email=$this->getUtildata()->getVaueOfArray($row,'email');
        if($cont==0){$txt=$email;}
        else {$txt.=";".$email;}
        $cont++;
     }
     return $txt;
  }

     /**
   * convet array list of recipient to list email splited by commas
   *
   * @return array
   */
  public function loadFileContent($listrow) {
        $container=$this->getContainer();
        $router=$this->getContainer()->get("router");
        $mconfig=$this->getReportmessage();
        $page= $this->getPage();
       $tablec=  $this->getUtildata()->getVaueOfArray($this->page->getData(),'badiu_table1');
        $tablec->getRow()->setList($listrow);
       $page->addData('badiu_table1',$tablec);

        $fileprocess=$page->getConfig()->getFileprocessindexscheduler();
        $app=$this->getContainer()->get('badiu.system.core.lib.util.app');
        $file=$app->getFilePath($fileprocess,false);
        $contents="";
   
        if(!file_exists($file)){return null;}
        ob_start();
        include($file);
        $contents = ob_get_contents();
       ob_end_clean();
      
       $customcontent=$this->getUtildata()->getVaueOfArray($mconfig,'message.body',true);
       $customcontent.="<br />";
       $customcontent.=$contents;
      return $customcontent;
  }
  public function getParamfilter(){
    return  $this->paramfilter;
 }
 public function setParamfilter($paramfilter){
      $this->paramfilter=$paramfilter;
}


public function getPage(){
  return  $this->page;
}
public function setPage($page){
    $this->page=$page;
}

public function getLayout(){
  return  $this->layout;
}
public function setLayout($layout){
    $this->layout=$layout;
}


public function getResultinfoofexec(){
  return  $this->resultinfoofexec;
}
public function setResultinfoofexec($resultinfoofexec){
    $this->resultinfoofexec=$resultinfoofexec;
}


public function getReportmessage(){
  return  $this->reportmessage;
}
public function setReportmessage($reportmessage){
    $this->reportmessage=$reportmessage;
}

public function getColumnkey(){
  return  $this->columnkey;
}
public function setColumnkey($columnkey){
    $this->columnkey=$columnkey;
}

public function getColumnkeyvalue(){
  return  $this->columnkeyvalue;
}
public function setColumnkeyvalue($columnkeyvalue){
    $this->columnkeyvalue=$columnkeyvalue;
}
public function getFormatedcontent(){
  return  $this->formatedcontent;
}
public function setFormatedcontent($formatedcontent){
    $this->formatedcontent=$formatedcontent;
}


function getUsersender() {
  return $this->usersender;
}

function setUsersender($usersender) {
  $this->usersender = $usersender;
}
}
