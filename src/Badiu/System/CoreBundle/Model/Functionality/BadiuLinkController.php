<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class BadiuLinkController extends BadiuModelLib {
       /**
     * @var array
     */
    private $param;
    private $response;
    private $successmessage; 
    private $urlgoback;
    private $resultout;
	private $clientsession;
	
     function __construct(Container $container) {
               parent::__construct($container);
               $this->response = $container->get('badiu.system.core.lib.util.jsonsyncresponse');
               $this->successmessage="";
               $this->urlgoback="";
               $this->resultout="";
			   $this->clientsession=new \stdClass();
			   $this->clientsession->anable=false;
			   $this->clientsession->data=null;
			   $this->clientsession->userislogin=0;
	           $this->clientsession->usersyncid=null;
			   $this->clientsession->error=null;

      }
  
      public function exec() {
        $this->init();
        $this->changeParam();
        $check=$this->validation();
        if($check != null){return $check;}

        $result=$this->process();
        if(!empty($result)){ return $result;}

        $this->setSuccessmessage($this->getTranslator()->trans('badiu.system.crud.message.add.sucess'));
        $outrsult=array('result'=>$this->resultout,'message'=>$this->successmessage,'urlgoback'=>$this->urlgoback);
        $this->getResponse()->setStatus("accept");
        $this->getResponse()->setMessage($outrsult);
        return $this->getResponse()->get();

       
    }
  
   
  public function changeParam() {
      
  }

 public function validation() {
     //check default
      
      return null;
  }  
  public function process() {
     
      $outrsult=array('result'=>null,'message'=>null,'urlgoback'=>null);
      $this->getResponse()->setStatus("accept");
      $this->getResponse()->setMessage($outrsult);
      return $this->getResponse()->get();
    
  }
  
  public function init() {
    $this->param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
	// review it just implemented in global serviçe
	//$this->initClientsession(); 
  }


  public function initClientsession() {
	$sessionkey=$this->getParamItem('_clientsession');
	if(empty($sessionkey)){return null;}
	$clientsession=$this->getContainer()->get('badiu.auth.core.login.clientsession');
	$csresult=$clientsession->validateSession($sessionkey);
	$status=$this->getUtildata()->getVaueOfArray($csresult,'status');
	$message=$this->getUtildata()->getVaueOfArray($csresult,'message');
	$syncstatus=$this->getUtildata()->getVaueOfArray($csresult,'message.user.serversync.status',true);
	$syncsysuserid=$this->getUtildata()->getVaueOfArray($csresult,'message.user.serversync.message',true);
	if($status=='acept'){
		$this->clientsession->anable=true;
		$this->clientsession->data=$message;
		$this->clientsession->error=null;
	
		if($syncstatus=='acept' && !empty($syncsysuserid)){
			$this->clientsession->usersyncid=$syncsysuserid;
			$this->clientsession->userislogin=1; 
		}
		
		$this->setSessionhashkey($sessionkey);
	}else if($status=='danied'){
		$this->clientsession->anable=false;
		$this->clientsession->data=null;
		$this->clientsession->error=$message;
	}
	

		
 }
     public function getClientsessionUrl($param=array()) {
		 if(!$this->clientsession->anable){return null;}
		 if(empty($this->clientsession->data)){return null;}
		
		 $queryparam=$this->getUtildata()->getVaueOfArray($this->clientsession->data,'_param');
		 foreach ($param as $key => $value) {
			 $queryparam[$key]=$value;
		 } 
		 $queryString = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
         $queryString->setParam($queryparam);
		 $queryString->makeQuery();
		 $query=$queryString->getQuery();
		$url=$this->getUtildata()->getVaueOfArray($this->clientsession->data,'sserver.url',true);
		$mdlservice="/local/badiunet/fcservice/index.php?";
		$furl=$url.$mdlservice. $query;
	
		return $furl;
	 }


 public function addlog($action) {
		$paramlog=array();
           $dloglib=$this->get('badiu.system.log.core.lib');
           $logtimeend=new \DateTime();
           if(!empty($this->logtimestart)){
               $start=$this->logtimestart->getTimestamp();
               $end=$logtimeend->getTimestamp();
               $timeexec=$end-$start; 
               $paramlog['timeexec']=$timeexec;
           }
           
           $dloglib->add($paramlog);
           
           
       }
    function addParamItem($item,$value) {
         $this->param[$item]=$value;
    }
    function addParamItems($items) {
        if(!empty($items) && is_array($items)){
             foreach ($items as $key => $value) {
                $this->param[$key]=$value;
            }
        }
       
         
    }
   function removeParamItem($item) {
       if (array_key_exists($item,$this->param)){
            unset($this->param[$item]);
       }
   } 
   
    function getParamItem($item) {
        $value=null;
       if (is_array($this->param) && array_key_exists($item,$this->param)){
            $value=$this->param[$item];
       }
       return $value;
   }  
   
   
  function getParam() {
      return $this->param;
  }

  function setParam($param) {
      $this->param = $param;
  }


  function getResponse() {
      return $this->response;
  }

  function setResponse($response) {
      $this->response = $response;
  }


  function getUrlgoback() {
      return $this->urlgoback;
  }

  function setUrlgoback($urlgoback) {
      $this->urlgoback = $urlgoback;
  }


  function getSuccessmessage() {
    return $this->successmessage;
}

function setSuccessmessage($successmessage) {
    $this->successmessage = $successmessage;
}


function getResultout() {
    return $this->resultout;
}

function setResultout($resultout) {
    $this->resultout = $resultout;
}


function getClientsession() {
    return $this->clientsession;
}

function setClientsession($clientsession) {
    $this->clientsession = $clientsession;
}


}
