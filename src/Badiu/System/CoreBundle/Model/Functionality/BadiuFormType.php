<?php

namespace Badiu\System\CoreBundle\Model\Functionality;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Translation\Translator;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
class BadiuFormType  extends AbstractType {
    
     /**
     * @var Container
     */
    private $container;
    /**
     * @var Translator
     */
    private $translator;
    
    /**
     * @var string
     */
    private $baseKey; 
    
    /**
     * @var boolean
     */
    private $filter;
    
    /**
     * @var string
     */
    private $class;

    /**
     * @var @var BadiuKeyManger
     */
    private $keymanger;

	private $fields;//for addEventListener
    
   function __construct(Container $container,$baseKey,$filter=true,$class=null) {
                $this->container=$container;
                $this->translator=$this->container->get('translator');
                $this->baseKey=$baseKey;
                $this->filter=$filter;
                $this->class=$class;
				$this->fields=null;
      }
     
       /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
       {
           if(empty($this->keymanger)){
               $this->keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
               $this->keymanger->setBaseKey($this->getBaseKey());
           }


            $formfactory=$this->getContainer()->get('badiu.system.core.lib.form.factory');
            $formfactory->setBuilder($builder);
            $formfactory->setKeymanger($this->keymanger);
            
            $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
            $fields=$cbundle->getFormFields($this->getFilter(),$this->keymanger);
            
            $this->fields=$fields;
              foreach ($fields as $field) {
                   // echo $field->getName();
                   // echo "<br>";
			 $formfactory->add($field);
                }
            $builder=$formfactory->getBuilder();
			
			$builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
				$data = $event->getData();
				$form = $event->getForm();

				foreach ($this->fields as $field) {
					if($field->getType()=='choicejs'){
						if ($form->has($field->getName())) {
							$form->remove($field->getName());
						}
						$form->add($field->getName(), 'choice', array('choices' => array($data[$field->getName()]=>null),));
					}
				}	
			});
      }
    
     /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $defaultCongig=array('csrf_protection'   => false);
        if(!empty($this->class)){
            $defaultCongig=array('data_class' =>$this->getClass(),'csrf_protection'   => false);
        }
        $resolver->setDefaults($defaultCongig);
    }
    
     /**
     * @return string
     */
    public function getName()
    {
        return 'badiu_system_core_form_type';
    }
 
  
    public function getTranslator() {
        return $this->translator;
    }

    public function setTranslator(Translator $translator) {
        $this->translator = $translator;
    }
    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }


 public function setBaseKey($baseKey) {
          $this->baseKey = $baseKey;
      }
 public function getBaseKey() {
          return $this->baseKey;
      }

   public function setFilter($filter) {
          $this->filter = $filter;
      }
 public function getFilter() {
          return $this->filter;
      }
      
       public function setClass($class) {
          $this->class = $class;
      }
  public function getClass() {
          return $this->class;
      }

    /**
     * @return Container
     */
    public function getKeymanger()
    {
        return $this->keymanger;
    }

    /**
     * @param BadiuKeyManger $keymanger
     */
    public function setKeymanger($keymanger)
    {
        $this->keymanger = $keymanger;
    }



}
