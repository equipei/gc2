<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use  Badiu\System\CoreBundle\Model\Lib\Config\KeyUtil;    
/**
 * Description of BadiuKeyManger
 * @author lino
 */
class BadiuKeyManger{
    
  /**
     * @var Container
     */
    private $container;

      /**
     * @var string
     */
    private $baseKey; 

          /** the original basekey if key is dynamic passed by param _dkey
     * @var string
     */
    private $baseKeydyncoriginal=null;
    
     function __construct(Container $container=null){
           if(!empty($container)) $this->container=$container;
     }
   
      
      public function moduleConfig() {
              $key= $this->getBaseKey();
			  $keyutil=new KeyUtil();
              $mkey=$keyutil->removeLastItem($key) ;
           return  $mkey.'.config';
      }

    public function moduleMenu($position="",$level2=false) {
                 if(!empty($position)){$position= ".$position";}
                $key= $this->getBaseKey();
                $mkey=$key;
		if(!$level2){
                    $keyutil=new KeyUtil();
                    $mkey=$keyutil->removeLastItem($key) ;
                }
           return  $mkey.'.menu'.$position;
      }

   public function menuTitle() {
            $sysoperation=$this->container->get('badiu.system.core.lib.util.systemoperation');
            if($sysoperation->isView()){
                $currentRoute=$this->container->get('request')->get('_route'); 
                $key=$currentRoute.'.menu.title';
                return  $key;
            }else{
              $key= $this->getBaseKey();
              return  $key.'.menu.title';
            }


            
    }
    public function extendModuleKey() {
        $key= $this->getBaseKey();
		$keyutil=new KeyUtil();
        $mkey=$keyutil->removeLastItem($key) ;
        return  $mkey.'.extends.bundle.key';
    }

    public function cron() {
           return  $this->getBaseKey().'.cron';
      }
    public function session() {
           return  $this->getBaseKey().'.session';
      }
      public function checkform() {
           return  $this->getBaseKey().'.checkform';
      }
      public function defaultcheckform() {
           return  $this->getBaseKey().'.defaultcheckform';
      }
      public function data() {
            $basekey=$this->getBaseKeydyncoriginal();
            if(empty($basekey)){$basekey=$this->getBaseKey();}
           return  $basekey.'.data';
      }
      public function datasession($entity) {
           return  $this->getBaseKey().'.data.session.'.$entity;
      }      
      public function search() {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
           return $basekey.'.search';
      }
      
      public function reportcontent() {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
          return  $basekey.'.report.content';
      }
      public function htmlcontent() {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
          return  $basekey.'.htmlcontent';
      }
	   public function outputchangedata() {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
          return  $basekey.'.output.changedata';
      }
      public function reportchangedata() {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
          return  $basekey.'.report.changedata';
      }
      public function report() {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
          return  $basekey.'.report';
      }
      public function reportController($level=0) {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
          $keyutil=new KeyUtil();
           $bk=null;
           if($level==0){$bk=$basekey;}
           else if($level==1){
               $bk=$this->baseKey;
               $bk=$keyutil->removeLastItem($bk) ;
           }else if($level==2){
               $bk=$this->baseKey;
               $bk=$keyutil->removeLastItem($bk) ;
               $bk=$keyutil->removeLastItem($bk) ;
           }else if($level==3){
               $bk=$this->baseKey;
               $bk=$keyutil->removeLastItem($bk) ;
               $bk=$keyutil->removeLastItem($bk) ;
               $bk=$keyutil->removeLastItem($bk) ;
           }else{

           }
          return  $bk.'.report.controller';
      }
      public function entity() {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
          return $basekey.'.entity';
      }
      
       public function controller() {
               $basekey=$this->getBaseKeydyncoriginal();
               if(empty($basekey)){$basekey=$this->getBaseKey();}
               return  $basekey.'.controller';
         
      }
      public function formcontroller() {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
               return  $basekey.'.formcontroller';
         
      }
      public function filter() {
          return  $this->getBaseKey().'.filter';
      }
      public function filtersql() {
          return  $this->getBaseKey().'.filtersql';
      }
      public function schedulerexpression() {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
          return  $basekey.'.schedulerexpression';
      }
      public function schedulerexpressionparent($level=1) {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
           $keyutil=new KeyUtil();
     
          $pakey=$basekey;
          $pkey=$keyutil->removeLastItem($pakey) ;
          if($level==2){$pkey=$keyutil->removeLastItem($pkey) ;}
          else if($level==3){
               $pkey=$keyutil->removeLastItem($pkey) ;
               $pkey=$keyutil->removeLastItem($pkey) ;
           }
          return  $pkey.'.schedulerexpression';
      } 
       public function formFilterType() {
           return  $this->getBaseKey().'.form.filter.type';
      }
      public function formType() {
           return  $this->getBaseKey().'.form.type';
      }
      
       public function formDataOptions() {
           return  $this->getBaseKey().'.form.dataoptions';
      }
      
      public function formFilterFieldsAvailable() {
           return  $this->getBaseKey().'.filter.form.fields.available';
      }
       public function formFilterFieldsEnable() {
           return  $this->getBaseKey().'.filter.form.fields.enable';
      }
      public function formFilterFieldsRequired() {
           return  $this->getBaseKey().'.filter.form.fields.required';
      }
       public function formFilterFieldsCssClass() {
           return  $this->getBaseKey().'.filter.form.fields.cssclass';
      }
      public function formFilterFieldsFormat() {
           return  $this->getBaseKey().'.filter.form.fields.format';
      }
       public function formFilterFieldsType() {
           return  $this->getBaseKey().'.filter.form.fields.type';
      }
      public function formFilterFieldsConfig() {
          return  $this->getBaseKey().'.filter.form.fields.config';
     }
	   public function formFilterFieldsAction() {
          return  $this->getBaseKey().'.filter.form.fields.action';
     }
       public function formFilterFieldsChoicelist() {
           return  $this->getBaseKey().'.filter.form.fields.choicelist';
      }
      public function formFilterFieldsLabel() {
           return  $this->getBaseKey().'.filter.form.fields.label';
      }
      public function formFilterFieldsPlaceholder() {
           return  $this->getBaseKey().'.filter.form.fields.placeholder';
      }      
   /* public function formFilterFieldsGroupList() {
        return  $this->getBaseKey().'.filter.form.group.list';
    }

    public function formFilterFieldsGroupLabel() {
        return  $this->getBaseKey().'.filter.form.group.label';
    }*/
       public function formConfig($type) {
           //review the type for edit  after
           $typeforkey=$type;
           if($typeforkey=='edit'){$typeforkey='data';}
           return  $this->getBaseKey().".$typeforkey.form.config";
      }
	
      public function pageConfig() {
           return  $this->getBaseKey().".page.config";
      }
      public function parentConfig() {
           return  $this->getBaseKey().".config.parent";
      }
      public function formDataFieldsAvailable() {
           return  $this->getBaseKey().'.data.form.fields.available';
      }
       public function formDataFieldsEnable() {
           return  $this->getBaseKey().'.data.form.fields.enable';
      }
      public function formDataFieldsRequired() {
           return  $this->getBaseKey().'.data.form.fields.required';
      }
       public function formDataFieldsCssClass() {
           return  $this->getBaseKey().'.data.form.fields.cssclass';
      }
        public function formDataFieldsFormat() {
           return  $this->getBaseKey().'.data.form.fields.format';
      }
       public function formDataFieldsType() {
           return  $this->getBaseKey().'.data.form.fields.type';
      }
      public function formDataFieldsConfig() {
          return  $this->getBaseKey().'.data.form.fields.config';
     }
	  public function formDataFieldsAction() {
          return  $this->getBaseKey().'.data.form.fields.action';
     }
       public function formDataFieldsChoicelist() {
           return  $this->getBaseKey().'.data.form.fields.choicelist';
       }
     public function formDataFieldsLabel() {
           return  $this->getBaseKey().'.data.form.fields.label';
       }
            public function formDataFieldsPlaceholder() {
           return  $this->getBaseKey().'.data.form.fields.placeholder';
       }
       public function dbsearchSqlCount() {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
           return  $basekey.'.dbsearch.sql.count';
      }
       
      public function dbsearchSql() {
          $basekey=$this->getBaseKeydyncoriginal();
          if(empty($basekey)){$basekey=$this->getBaseKey();}
           return  $basekey.'.dbsearch.sql';
      }
      
      public function dbsearchFieldsAvailable() {
           return  $this->getBaseKey().'.dbsearch.fields.available';
      }
      //review deleted
      public function dbsearchFieldsEnable() {
           return  $this->getBaseKey().'.dbsearch.fields.enable';
      }
      //desabled
      /*public function dbsearchFieldsType() {
            return  $this->getBaseKey().'.dbsearch.fields.type';
      }*/
      public function dbsearchFieldsFormat() {
            return  $this->getBaseKey().'.dbsearch.fields.format';
      }
       public function dbsearchFieldsTableView() {
            return  $this->getBaseKey().'.dbsearch.fields.table.view';
      }
	   public function dbsearchFieldsTableViewType() {
            return  $this->getBaseKey().'.dbsearch.fields.table.view.type';
      }
      public function dbsearchFieldsTableViewScheduler() {
          return  $this->getBaseKey().'.dbsearch.fields.table.view.scheduler';
    }
	public function dbsearchFieldsTableViewWebservice() {
          return  $this->getBaseKey().'.dbsearch.fields.table.view.webservice';
    }
       public function dbsearchFieldsTableViewLink() {
            return  $this->getBaseKey().'.dbsearch.fields.table.view.link';
      }
	   public function dbsearchFieldsTableViewLabel() {
            return  $this->getBaseKey().'.dbsearch.fields.table.view.label';
      }
      
         public function dbsearchFieldsTableViewOperationConfirm() {
            return  $this->getBaseKey().'.dbsearch.fields.table.view.operationconfirm';
      }
      public function dbsearchFieldsTableViewDashboardSqlKey() {
            return  $this->getBaseKey().'.dbsearch.fields.table.view.dashboard.db.sql.key';
      }
      //review deleted
      public function dbsearchFieldsRowView() {
            return  $this->getBaseKey().'.dbsearch.fields.row.view';
      }
      
    public function dbgetrowSql() {
           return  $this->getBaseKey().'.dbgetrow.sql';
      } 
      public function dbgetrowFieldsAvailable() {
           return  $this->getBaseKey().'.dbgetrow.fields.available';
      }
      //review deleted
      public function dbgetrowFieldsEnable() {
           return  $this->getBaseKey().'.dbgetrow.fields.enable';
      }
      /* desabled
      public function dbgetrowFieldsType() {
            return  $this->getBaseKey().'.dbgetrow.fields.type';
      }*/
      public function dbgetrowFieldsFormat() {
            return  $this->getBaseKey().'.dbgetrow.fields.format';
      }
       public function dbgetrowFieldsTableView() {
            return  $this->getBaseKey().'.dbgetrow.fields.table.view';
      }
       public function dbgetrowFieldsTableViewLink() {
            return  $this->getBaseKey().'.dbgetrow.fields.table.view.link';
      }
     public function dbgetrowFieldsTableViewLabel() {
            return  $this->getBaseKey().'.dbgetrow.fields.table.view.label';
      }
     public function dbgetrowFieldsTableViewDashboardSqlKey() {
            return  $this->getBaseKey().'.dbgetrow.fields.table.view.dashboard.db.sql.key';
      }
      
      public function dashboardDbSqlSingle($seq,$type=null) {
            $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().$dtype.'.dashboard.db.sql.single.'.$seq;
      }
       public function dashboardDbSqlSingleFormat($seq,$type=null) {
            $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().$dtype.'.dashboard.db.sql.single.format.'.$seq;
      }
       public function dashboardDbSqlSingleLabel($seq,$type=null) {
            $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().$dtype.'.dashboard.db.sql.single.label.'.$seq;
      }
       public function dashboardDbSqlSingleView($seq,$type=null) {
            $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().$dtype.'.dashboard.db.sql.single.view.'.$seq;
      }
       public function dashboardDbSqlSingleConfig($type=null) {
             $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().$dtype.'.dashboard.db.sql.single.config';
      }
       public function dashboardDbSql($seq,$type=null) {
             $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().$dtype.'.dashboard.db.sql.'.$seq;
      }
    public function dashboardDbSqlFormat($seq,$type=null) {
             $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().$dtype.'.dashboard.db.sql.format.'.$seq;
      }
      public function dashboardDbSqlLabel($seq,$type=null) {
             $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().$dtype.'.dashboard.db.sql.label.'.$seq;
      }
      public function dashboardDbSqlView($seq,$type=null) {
             $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().$dtype.'.dashboard.db.sql.view.'.$seq;
      }
       public function dashboardDbSqlConfig($type=null) {
             $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().$dtype.'.dashboard.db.sql.config';
      }
       public function dashboardService($seq,$type=null) {
            $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().'.dashboard.service.'.$seq;

      }

     public function dashboardFileContent($seq,$type=null) {
              $dtype="";
            if(!empty($type)){$dtype=".".$type;}
            return  $this->getBaseKey().$dtype.'.dashboard.file.content.'.$seq;
      }
       public function reportContentFile() {
           return  $this->getBaseKey().'.report.content.file';
      }
      //review permission
      public function permissionView() {
            return  $this->getBaseKey().'.view';
      }
      public function permissionViewDetail() {
            return  $this->getBaseKey().'.view.detail';
      }
      public function permissionAdd() {
            return  $this->getBaseKey().'.add';
      }
      public function permissionEdit() {
            return  $this->getBaseKey().'.edit';
      }
      public function permissionDelete() {
            return  $this->getBaseKey().'.delete';
      }
       public function permissionRemove() {
          return  $this->getBaseKey().'.remove';
      }
      public function routeIndex() {
            return  $this->getBaseKey().'.index';
      }
      //route
       public function routeGrud() {
            return  $this->getBaseKey().'.route.grud';
      }
      public function routeView() {
            return  $this->getBaseKey().'.view';
      }
      public function routeDetail() {
            return  $this->getBaseKey().'.detail';
      }
      public function routeAdd() {
            return  $this->getBaseKey().'.add';
      }
      public function routeEdit() {
            return  $this->getBaseKey().'.edit';
      }
      public function routeCopy() {
            return  $this->getBaseKey().'.copy';
      }
       public function routeDelete() {
            return  $this->getBaseKey().'.delete';
      }
       public function routeRemove() {
            return  $this->getBaseKey().'.remove';
      }
      public function functionality() { 
            return  $this->getBaseKey().'.functionality';
      }
      
      public function getContainer() {
          return $this->container;
      }

      public function getBaseKey() {
          return $this->baseKey;
      }

      public function setContainer(Container $container) {
          $this->container = $container;
      }

      public function setBaseKey($baseKey) {
          $this->baseKey = $baseKey;
      }

      public function getBaseKeydyncoriginal() {
          return $this->baseKeydyncoriginal;
      }
      public function setBaseKeydyncoriginal($baseKeydyncoriginal) {
          $this->baseKeydyncoriginal = $baseKeydyncoriginal;
      }
}
