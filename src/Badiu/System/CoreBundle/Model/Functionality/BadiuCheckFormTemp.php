<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Doctrine\ORM\EntityManager;
class BadiuCheckFormTemp {
    
     /**
     * @var Container
     */
    private $container;
    
      /**
     * @var Translator
     */
    private $translator;
        /**
     * @var object
     */
    private $utildata = null;
   
         /**
     * @var object
     */
    private $data = null;
    
     /**
     * @var integer
     */
    private $entity;
    
       /**
     * @var integer
     */
    private $id;
    
      function __construct(Container $container) {
                $this->container=$container;
                $this->translator=$this->container->get('translator');
                $this->utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
               
                 $badiuSession = $this->container->get('badiu.system.access.session');
                $this->entity = $badiuSession->get()->getEntity();
                $this->id=$this->container->get('request')->get('id');
      }
      
   public function addCheckForm($form, $data) {
        $check = true;
        
        return $check;
   }
   
   public function editCheckForm($form, $data) {
        $check = true;
        
        return $check;
   }
       
       public function getContainer() {
           return $this->container;
       }

       public function setContainer(Container $container) {
           $this->container = $container;
       }
       
     
  public function getTranslator() {
      return $this->translator;
  }

  public function setTranslator(Translator $translator) {
      $this->translator = $translator;
  }

  function getUtildata() {
      return $this->utildata;
  }

  function setUtildata($utildata) {
      $this->utildata = $utildata;
  }

  function getData() {
      return $this->data;
  }

  function setData($data) {
      $this->data = $data;
  }

  function getEntity() {
      return $this->entity;
  }

 
  function setEntity($entity) {
      $this->entity = $entity;
  }

  function getId() {
      return $this->id;
  }

  function setId($id) {
      $this->id = $id;
  }



   

}
