<?php

namespace Badiu\System\CoreBundle\Model\Functionality;

class BadiuFilter{

    /**
     * @var  integer
     */
    private $entity;

    /**
     * @var  integer
     */
    private $bpage;

  /**
     * @var string
      */
    private $name;


    /**
     * @var string
     */
    private $shortname;
    /**
     * @var string
      */
    private $idnumber;
     /**
     * @var boolean
      */
    private $deleted;
    
    
    function __construct() {
       $this->deleted=0;
     }
     public function getName() {
         return $this->name;
     }

     public function getIdnumber() {
         return $this->idnumber;
     }

     public function setName($name) {
         $this->name = $name;
     }

     public function setIdnumber($idnumber) {
         $this->idnumber = $idnumber;
     }

     public function getDeleted() {
         if($this->deleted==null || $this->deleted==''){$this->deleted=0;}
         return $this->deleted;
     }

     public function setDeleted($deleted) {
         $this->deleted = $deleted;
     }

    /**
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * @param string $shortname
     */
    public function setShortname($shortname)
    {
        $this->shortname = $shortname;
    }

    /**
     * @return int
     */
    public function getBpage()
    {
        if($this->bpage==null || $this->bpage=='' ) {$this->bpage=0;}

        return $this->bpage;
    }

    /**
     * @param int $bpage
     */
    public function setBpage($bpage)
    {
        $this->bpage = $bpage;
    }

    /**
     * @return int
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param int $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }



}
