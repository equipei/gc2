<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class BadiuReportContent extends BadiuModelLib {
       /**
     * @var array
     */
    private $data;
   
    
     function __construct(Container $container) {
               parent::__construct($container);
         
      }

  public function head() {
     
      return null;
  }
  
   public function footer() {
       
      return null;
  }
   
   public function middle() {
      
      return null;
  }

  function getData() {
      return $this->data;
  }

  function setData($data) {
      $this->data = $data;
  }


}
