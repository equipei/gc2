<?php

namespace Badiu\System\CoreBundle\Model\Functionality;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class BadiuFormController extends BadiuModelLib {
       /**
     * @var array
     */
    private $param;
   
       /**
     * @var string
     */
    private $key;
    
    /**
     * @var object
     */
    private $kminherit;
    
    private $response;
    private $cpage;
    private $cform;
	private $formFieldList;
    private $haserror=false; //delete
   private $operaction; 
   private $successmessage; 
   private $dbdefaultablevalue=true;
   private $urlgoback;
   private $resultexec;
   private $clientsession;
   private $moduleintancetemp;
   private $paramtemp;
     function __construct(Container $container) {
               parent::__construct($container);
              $this->operaction=null;
              $this->successmessage="";
              $this->urlgoback="";
              $this->response = $container->get('badiu.system.core.lib.util.jsonsyncresponse');
			  $this->resultexec=null;
			   $this->clientsession=new \stdClass();
			   $this->clientsession->anable=false;
			   $this->clientsession->data=null;
			   $this->clientsession->userislogin=0;
	           $this->clientsession->usersyncid=null;
			   $this->clientsession->error=null;
			   $this->moduleintancetemp=time();
			   $this->paramtemp=array();
      }
  
      //this function is colled by  Badiu\System\CoreBundle\Model\Lib\Form\FormFactoryConfig on method  changeDefaultparam()
  public function changeParamOnOpen() {
	  
      $this->getFieldProcessOnOpen();
	

	  $fiedltyplist=$this->getFormFieldsType();
	 
	  $badiutexteditor=$this->getContainer()->get('badiu.system.core.lib.form.casttexteditor');
	  $badiutexteditor->setFormFieldTypeList($fiedltyplist);
      $nparam=$badiutexteditor->convertToForm($this->getParam());
	 
      $this->setParam($nparam);
  }
  
 
  public function changeParam() {
      
  }
  
  public function initParam() {
    $this->setParam($this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters());
	// review it just implemented in global serviçe
	//$this->initClientsession();
}
  public function castData() {
      $fiedltyplist=$this->getFormFieldsType();
	  //badiuhour
      $badiuhour=$this->getContainer()->get('badiu.system.core.lib.form.casthour');
      $nparam=$badiuhour->convertFromForm($this->getParam());
      $this->setParam($nparam);
      
      //badiuhourtime
      $badiuhourtime=$this->getContainer()->get('badiu.system.core.lib.form.casthourtime');
      $nparam=$badiuhourtime->convertFromForm($this->getParam());
      $this->setParam($nparam);
      //badiutdate
      $badiutdate=$this->getContainer()->get('badiu.system.core.lib.form.castdate');
      $nparam=$badiutdate->convertFromForm($this->getParam());
      $this->setParam($nparam);
	  
	   //badiuttimeperiod
      $badiutperiod=$this->getContainer()->get('badiu.system.core.lib.form.casttimeperiod');
      $nparam=$badiutperiod->changeParamFromform($this->getParam(),'json');
	
      $this->setParam($nparam);

     
      //badiutdatedefault
      $badiutdatedefault=$this->getContainer()->get('badiu.system.core.lib.form.castdatedefault');
      $nparam=$badiutdatedefault->convertFromForm($this->getParam());
      $this->setParam($nparam);
    
      //badiutexteditor
      $badiutexteditor=$this->getContainer()->get('badiu.system.core.lib.form.casttexteditor');
	  $badiutexteditor->setFormFieldTypeList($fiedltyplist);
      $nparam=$badiutexteditor->convertFromForm($this->getParam());
      $this->setParam($nparam);
       
	   //general
      $badiutgeneralcast=$this->getContainer()->get('badiu.system.core.lib.form.castgeneral');
	  $badiutgeneralcast->setFormFieldTypeList($fiedltyplist);
      $nparam=$badiutgeneralcast->convertFromForm($this->getParam());
      $this->setParam($nparam);
        
	   
      
  }
  public function removeColumns() {
      if(!is_array($this->param)) {return null;}
     
      
      $newlist=array();
      foreach ($this->param as $key => $value) {
          $fcompose=$this->isCompositeFfield($key);
          if(!$fcompose){
              $newlist[$key]=$value;
          }
          $this->param=$newlist;
      }
      unset($this->param['_key']);
      unset($this->param['_service']);
      unset($this->param['_function']);
	  unset($this->param['_clientsession']);
      unset($this->param['_operation']);
      if(isset($this->param['parentid'])){ unset($this->param['parentid']);}
  } 
  
   public function sysFormBefore() {
	   if(!$this->getContainer()->has('badiu.admin.form.lib.factorydata')){return null;}
	    $modulekey=$this->getUtildata()->getVaueOfArray($this->getCform()->getData(),'modulekey');
		if(empty($modulekey)){
			$modulekey=$this->getKey();
			$modulekey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($modulekey);
		}
	  $confparam=array('key'=>$modulekey,'isedit'=>$this->isEdit(),'moduleinstance'=>$this->getModuleintancetemp());
      $this->param=$this->getContainer()->get('badiu.admin.form.lib.factorydata')->addFormControllerData($this->param,$confparam);
	   
  }
   public function sysFormAfter() {
	   if(!$this->getContainer()->has('badiu.admin.form.lib.factorydata')){return null;}
	  $this->param=$this->getContainer()->get('badiu.admin.form.lib.factorydata')->updateModuleinstanceFormControllerData($this->param);
	  
  }
  public function addDefaultData() {
      $listdefaultdata=$this->getCform()->getDefaultdata();
	  $fdefaultdata=$this->getContainer()->get('badiu.system.core.lib.form.defaultdata');
     
	  if(!empty($listdefaultdata)){
          foreach ($listdefaultdata as $key => $value) {
             $this->param[$key]=$fdefaultdata->getValue($value); //review it do add default value with querystring param ex: moduleinstance/{parenteid}
			 //$this->param[$key]= $value;
          }
      }
      //datalayout tree
       $datalayout = $this->getCform()->getDatalayout();
       if ($datalayout == 'datatreewithname'){
            $defaultdatalayouttree=$this->getContainer()->get('badiu.system.core.lib.form.defaultdatalayouttree');
            $defaultdatalayouttree->setKminherit($this->kminherit);
            $defaultdatalayouttree->setDefaultparam($this->cform->getDefaultdata());
            $defaultdatalayouttree->setParam($this->param);
            $defaultdatalayouttree->exec();
			
            $this->param=$defaultdatalayouttree->getParam();
			//review it
			if($this->isEdit()){
				if(isset($this->param['idpath'])){unset($this->param['idpath']);}
				if(isset($this->param['orderidpath'])){unset($this->param['orderidpath']);}
			}
			
			$paramtemp=$this->getParamtemp();
			$paramtemp['oderbefore']=$this->getParamItem('oderbefore'); 
			$paramtemp['parenttablecolumn']=array('key'=>$this->getCpage()->getParenttablecolumn(),'value'=>$this->getUtildata()->getVaueOfArray($this->getParam(),$this->getCpage()->getParenttablecolumn()));
	
			$this->removeParamItem('oderbefore');
			$this->setParamtemp($paramtemp);
       }
     
      
  }
 public function validation() {
     //check default
      $defaultcheckformkey=$this->kminherit->defaultcheckform();
     
     if(!$this->getContainer()->has($defaultcheckformkey)){$defaultcheckformkey='badiu.system.core.lib.form.defaultcheck';}
    
     if($this->getContainer()->has($defaultcheckformkey)){
          $servicedcheckform=$this->getContainer()->get($defaultcheckformkey);
          $servicedcheckform->setKminherit($this->kminherit);
          $servicedcheckform->setParam($this->param);
          $servicedcheckform->setCform($this->getCform());
          $servicedcheckform->setCpage($this->getCpage());
          $servicedcheckform->setKey($this->getKey());
          $resultdcheck=$servicedcheckform->validation();
         
          if($resultdcheck!=null ){return $resultdcheck;}
       }
       
     if($this->getContainer()->has($this->kminherit->checkform())){
       
          $servicecheckform=$this->getContainer()->get($this->kminherit->checkform());
          $servicecheckform->setKminherit($this->kminherit);
          $servicecheckform->setParam($this->param);
          $servicecheckform->setCform($this->getCform());
          $servicecheckform->setCpage($this->getCpage());
          $resultcheck=$servicecheckform->validation();
           
          if($resultcheck!=null ){return $resultcheck;}
       }
     
      
      return null;
  }  
  public function save() {
     // try {
   
      $this->addDefaultData();
      $this->castData();
      $check=$this->validation();
      if($check != null){return $check;}
      $this->changeParam();
      $this->removeColumns();
	  $this->sysFormBefore(); 
      $this->execBefore();
	   $this->fieldProcessBefore();
      //print_r($this->param);exit; 
      $this->saveExec();
	  $this->sysFormAfter();
	  $this->execAfter();
	  $this->fieldProcessAfter();
	 
	  $this->setSuccessmessage($this->getTranslator()->trans('badiu.system.crud.message.add.sucess'));
      
	  $outrsult=$this->execResponse();
	
	  return $outrsult;
	    
  }
  
    public function saveExec() {
		$data=$this->getContainer()->get($this->kminherit->data());
        $result=$data->insertNativeSql($this->param,$this->getDbdefaultablevalue());
        $this->param['id']=$result;
		$this->setResultexec($result);
		return $result;
	}
  public function edit() {
	 
   
      $this->addDefaultData();
      $this->castData();
      
      $check=$this->validation();
      if($check != null){return $check;}
       $this->changeParam();
      $this->removeColumns();
     $this->sysFormBefore(); 
	   $this->fieldProcessBefore();
      $this->execBefore();
      $result=$this->editExec();
	 $this->sysFormAfter();
	  $this->execAfter();
	  $this->fieldProcessAfter();
      $this->setSuccessmessage($this->getTranslator()->trans('badiu.system.crud.message.edit.sucess'));
      $outrsult=$this->execResponse();
      return $outrsult;
     
  }

	public function editExec() {
		$this->fieldProcessBefore();
		$data=$this->getContainer()->get($this->kminherit->data());
		$result=$data->updateNativeSql($this->param,$this->getDbdefaultablevalue()); 
		$this->setResultexec($result);
		return $result;
	}
	public function execResponse() {
		$outrsult=array('result'=>$this->getResultexec(),'message'=>$this->getSuccessmessage(),'urlgoback'=>$this->getUrlgoback());
		$this->getResponse()->setStatus("accept");
		$this->getResponse()->setMessage($outrsult);
		return $this->getResponse()->get();
	}
  public function delete() {
    // try {
  
    // $this->addDefaultData();
   //  $this->castData();
     
     $check=$this->validation();
     if($check != null){return $check;}
     
     $this->changeParam();
     
    // $this->removeColumns();
    
     $this->execBefore();
     
    $data=$this->getContainer()->get($this->kminherit->data());
    $id = $this->getUtildata()->getVaueOfArray($this->param, 'id');
    $fparam=array('id'=>$id,'deleted'=>1);
    $result=$data->updateNativeSql( $fparam,$this->getDbdefaultablevalue()); 
     $this->execAfter();
    
     $this->setSuccessmessage($this->getTranslator()->trans('badiu.system.crud.message.edit.sucess'));
     $outrsult=array('result'=>$result,'message'=>$this->successmessage,'urlgoback'=>$this->urlgoback);
     
     $this->getResponse()->setStatus("accept");
     $this->getResponse()->setMessage($outrsult);
     return $this->getResponse()->get();
    
 }
 public function remove() {
    // try {
  
    // $this->addDefaultData();
   //  $this->castData();
     
     $check=$this->validation();
     if($check != null){return $check;}
     
     $this->changeParam();
     
    // $this->removeColumns();
    
     $this->execBefore();
     
    $data=$this->getContainer()->get($this->kminherit->data());
    $id = $this->getUtildata()->getVaueOfArray($this->param, 'id');
    $fparam=array('id'=>$id);
    $result=$data->removeNativeSql( $fparam,$this->getDbdefaultablevalue()); 
     $this->execAfter();
    
     $this->setSuccessmessage($this->getTranslator()->trans('badiu.system.crud.message.edit.sucess'));
     $outrsult=array('result'=>$result,'message'=>$this->successmessage,'urlgoback'=>$this->urlgoback);
     
     $this->getResponse()->setStatus("accept");
     $this->getResponse()->setMessage($outrsult);
     return $this->getResponse()->get();
    
 }
  public function isCompositeFfield($fieldname) {
      $pos = stripos($fieldname, "_badiu");
      $result=false;
      if ($pos !== false){$result=true;}
      return $result;
  }
  
  public function execBefore(){
      $listservice=$this->cform->getFilter();
	  $this->execService($listservice,true);
   }
  public function execAfter(){
        $listservice= $this->cform->getFilter();
        $this->execService($listservice,false);
		
		$datalayout = $this->getCform()->getDatalayout();
        if ($datalayout == 'datatreewithname'){
		   if($this->isEdit()){ 
				$oderbefore=$this->getUtildata()->getVaueOfArray($this->getParamtemp(),'oderbefore');
				$datakey=$this->getKminherit()->data();
		
				$factorykeytreechangeorder=$this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytreechangeorder');
				$factorykeytreechangeorder->init($datakey);
		 
				$parenttablecolumnkey=$this->getUtildata()->getVaueOfArray($this->getParamtemp(),'parenttablecolumn.key',true);
				$parenttablecolumnvalue=$this->getUtildata()->getVaueOfArray($this->getParamtemp(),'parenttablecolumn.value',true);
		 
				$fparam=array('datakey'=>$datakey,'oderbefore'=>$oderbefore,'currentid'=>$this->getParamItem('id'),'filter'=>array('entity'=>$this->getEntity()));
				if(!empty($parenttablecolumnkey) && !empty($parenttablecolumnvalue)){
					$fparam['filter'][$parenttablecolumnkey]=$parenttablecolumnvalue;
				}
				$factorykeytreechangeorder->exec($fparam); 
        
			}
	    }
   }
 
  public function execService($listservice,$before=true) {
     
        if (empty($listservice)) {
            return null;
        }
      
	   $modulekey=$this->getUtildata()->getVaueOfArray($this->getCform()->getData(),'modulekey');
		if(empty($modulekey)){
			$modulekey=$this->getKey();
			$modulekey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($modulekey);
		}
        foreach ($listservice as $itemservice) {

                 $function = "execBeforeSubmit";
                    if(!$before){$function = "execAfterSubmit";}
                 
                 if ($this->getContainer()->has($itemservice)) {
						
                       $service = $this->getContainer()->get($itemservice);
                        if (method_exists($service, 'setParam')) {$service->setParam($this->getParam());}
                        if (method_exists($service, 'setKminherit')) { $service->setKminherit($this->getKminherit());}
						if (method_exists($service, 'setModulekey')) {$service->setModulekey($modulekey);}
                        if (method_exists($service, $function)) {$service->$function();}
                        if (method_exists($service, 'getParam')) {$this->setParam($service->getParam());}
						
						
                    }
         }

    }
    
	   public function getFormFieldsType() {
         $bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($this->getKey());
  
         $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
         $keymanger->setBaseKey($bkey);
         $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
         $cbundle->setKeymanger($keymanger);
         $cbundle->initKeymangerExtend($keymanger);
         $fields= $cbundle->getValueQueryString($cbundle->getKeymanger()->formDataFieldsType(),$cbundle->getKeymangerExtend()->formDataFieldsType());
		 return $fields;
	   }
     public function getParamOfGroupFields($groupoffields,$fieldadd='dconfig',$fieldjson='reportfilter') {
         $bkey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($this->getKey());
  
         $keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
         $keymanger->setBaseKey($bkey);
         $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
         $cbundle->setKeymanger($keymanger);
         $cbundle->initKeymangerExtend($keymanger);
         $fields= $cbundle->getFormFields();
        
         $formFieldList = $cbundle->getFieldsInGroup($cbundle->getKeymanger()->formDataFieldsEnable(), $cbundle->getKeymangerExtend()->formDataFieldsEnable());
        $fparam = array(); 
        $cont = 0;
        foreach ($fields as $field) {
           $fparam[$cont] = $field;
           $cont++; 
        }
        
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
        $fparam = $serializer->serialize($fparam, 'json');
        $fparam = json_decode($fparam, true);
         $reportfilter=array();
       foreach ($fparam as $value) {
           $fieldkey = $this->getUtildata()->getVaueOfArray($value, 'name');
            $type = $this->getUtildata()->getVaueOfArray($value, 'type');
             $group = $this->getUtildata()->getVaueOfArray($formFieldList, $fieldkey);
             if($group==$groupoffields){
                $reportfilter[$fieldkey]=$this->getUtildata()->getVaueOfArray($this->getParam(), $fieldkey);
                $this->removeParamItem($fieldkey);
             }
             
       }
       $dconfig=array();
       $dconfig[$fieldjson]=$reportfilter;
       $dconfig = $this->getJson()->encode($dconfig);
       $this->addParamItem($fieldadd,$dconfig) ;
       
         
     }
     
    function addParamItem($item,$value) {
         $this->param[$item]=$value;
    }
    function addParamItems($items) {
        if(!empty($items) && is_array($items)){
             foreach ($items as $key => $value) {
                $this->param[$key]=$value;
            }
        }
       
         
    }
   function removeParamItem($item) {
       if (array_key_exists($item,$this->param)){
            unset($this->param[$item]);
       }
   } 
   function existParamItem($item) {
    $r=array_key_exists($item,$this->param);
    return $r;
} 
    function getParamItem($item) {
        $value=null;
       if (is_array($this->param) && array_key_exists($item,$this->param)){
            $value=$this->param[$item];
       }
       return $value;
   }  
   
    function isEdit() {
        $isedit=false;
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        if($sysoperation->isEdit($this->getKey())){$isedit=true;}
      return $isedit;
  }
     function isClone() {
        $isclone=false;
        $foperation=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
         if($foperation=='clone'){ $isclone=true;}
      return $isclone;
  }
  
  
   public function initClientsession() {
	$sessionkey=$this->getParamItem('_clientsession');
	if(empty($sessionkey)){return null;}
	$clientsession=$this->getContainer()->get('badiu.auth.core.login.clientsession');
	$csresult=$clientsession->validateSession($sessionkey);
	$status=$this->getUtildata()->getVaueOfArray($csresult,'status');
	$message=$this->getUtildata()->getVaueOfArray($csresult,'message');
	$syncstatus=$this->getUtildata()->getVaueOfArray($csresult,'message.user.serversync.status',true);
	$syncsysuserid=$this->getUtildata()->getVaueOfArray($csresult,'message.user.serversync.message',true);
	if($status=='acept'){
		$this->clientsession->anable=true;
		$this->clientsession->data=$message;
		$this->clientsession->error=null;
	
		if($syncstatus=='acept' && !empty($syncsysuserid)){
			$this->clientsession->usersyncid=$syncsysuserid;
			$this->clientsession->userislogin=1; 
		}
		
		$this->setSessionhashkey($sessionkey);
	}else if($status=='danied'){
		$this->clientsession->anable=false;
		$this->clientsession->data=null;
		$this->clientsession->error=$message;
	}
	

		
 }
     public function getClientsessionUrl($param=array()) {
		 if(!$this->clientsession->anable){return null;}
		 if(empty($this->clientsession->data)){return null;}
		
		 $queryparam=$this->getUtildata()->getVaueOfArray($this->clientsession->data,'_param');
		 foreach ($param as $key => $value) {
			 $queryparam[$key]=$value;
		 } 
		 $queryString = $this->getContainer()->get('badiu.system.core.lib.http.querystring');
         $queryString->setParam($queryparam);
		 $queryString->makeQuery();
		 $query=$queryString->getQuery();
		$url=$this->getUtildata()->getVaueOfArray($this->clientsession->data,'sserver.url',true);
		$mdlservice="/local/badiunet/fcservice/index.php?";
		$furl=$url.$mdlservice. $query;
	
		return $furl;
	 }
	 
	 public function fieldProcessBefore() {
		$cformdata=$this->getCform()->getData();
		$fieldprocess=$this->getUtildata()->getVaueOfArray($cformdata,'fieldprocess');
		if(empty($fieldprocess)){return null;}
		
		if(!is_array($this->param)) {return null;}
	  foreach ($this->param as $key => $value) {
          $fpvalue=$this->getUtildata()->getVaueOfArray($fieldprocess,$key);
		  if($fpvalue){
			  $this->paramtemp['fieldprocess'][$key]= $value;
			  unset($this->param[$key]);
		  }
	    }
		
	}
	
	public function fieldProcessAfter() {
		$modulekey=$this->getUtildata()->getVaueOfArray($this->getCform()->getData(),'modulekey');
		if(empty($modulekey)){
			$modulekey=$this->getKey();
			$modulekey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($modulekey);
		}
		$moduleinstance=$this->getUtildata()->getVaueOfArray($this->param,'id');
		
		$result=null;
		$listfieds=$this->getUtildata()->getVaueOfArray( $this->paramtemp,'fieldprocess');
		if(empty($listfieds)){return null;}
		if(!is_array($listfieds)){return null;}
		foreach ($listfieds as $key => $value) {
		  if(!empty($value)){
			$fparam=array('modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'_badiu_value'=>$value);
			$servicekey=$this->getUtildata()->getVaueOfArray($this->getCform()->getData(),'fieldprocess.'.$key,true);
			if($this->getContainer()->has($servicekey)){
				$servicedata=$this->getContainer()->get($servicekey);
				if(method_exists($servicedata,'add')){$result=$servicedata->add($fparam);}
				else if(method_exists($servicedata,'exec')){$result=$servicedata->exec($fparam);}
			}
		  }
		} 
	}
	
	 public function getFieldProcessOnOpen() {
		 if(!$this->isEdit()){return null;}
		 
		 if(empty($this->getCform()) ){return null;}
		 if(empty($this->getFormFieldList() )){return null;}
		 
		$cformdata=$this->getCform()->getData();
		$fieldprocess=$this->getUtildata()->getVaueOfArray($cformdata,'fieldprocess');
		if(empty($fieldprocess)){return null;}
		
		$modulekey=$this->getUtildata()->getVaueOfArray($cformdata,'modulekey');
		if(empty($modulekey)){
			$modulekey=$this->getKey();
			$modulekey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($modulekey);
		}
		
		$moduleinstance=$this->getUtildata()->getVaueOfArray($this->param,'id');
		 if(empty($moduleinstance)){return null;}
		$result=null;
		$listfieds=$this->getUtildata()->getVaueOfArray( $this->paramtemp,'fieldprocess');
		
		foreach ($this->getFormFieldList() as $key => $value) {
			
			$fparam=array('modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'entity'=>$this->getEntity());
			$servicekey=$this->getUtildata()->getVaueOfArray($this->getCform()->getData(),'fieldprocess.'.$key,true);
			if(!empty($servicekey)){
			  if($this->getContainer()->has($servicekey)){
				$servicedata=$this->getContainer()->get($servicekey);
				if(method_exists($servicedata,'get')){$result=$servicedata->get($fparam);}
				$this->param[$key]= $result;
			}
		  }
		} 
		return $result;
	}
  function getParam() {
      return $this->param;
  }

  function setParam($param) {
      $this->param = $param;
  }


  function getKey() {
      return $this->key;
  }

  function setKey($key) {
      $this->key = $key;
  }

  function getKminherit() {
      return $this->kminherit;
  }

  function setKminherit($kminherit) {
      $this->kminherit = $kminherit;
  }

  function getResponse() {
      return $this->response;
  }

  function setResponse($response) {
      $this->response = $response;
  }


  function getCpage() {
      return $this->cpage;
  }

  function getCform() {
      return $this->cform;
  }

  function setCpage($cpage) {
      $this->cpage = $cpage;
  }

  function setCform($cform) {
      $this->cform = $cform;
  }
  function getHaserror() {
      return $this->haserror;
  }

  function setHaserror($haserror) {
      $this->haserror = $haserror;
  }

  function getOperaction() {
      return $this->operaction;
  }

  function setOperaction($operaction) {
      $this->operaction = $operaction;
  }

  function getSuccessmessage() {
      return $this->successmessage;
  }

  function setSuccessmessage($successmessage) {
      $this->successmessage = $successmessage;
  }

  function getDbdefaultablevalue() {
      return $this->dbdefaultablevalue;
  }

  function setDbdefaultablevalue($dbdefaultablevalue) {
      $this->dbdefaultablevalue = $dbdefaultablevalue;
  }

  function getUrlgoback() {
      return $this->urlgoback;
  }

  function setUrlgoback($urlgoback) {
      $this->urlgoback = $urlgoback;
  }


function getClientsession() {
    return $this->clientsession;
}

function setClientsession($clientsession) {
    $this->clientsession = $clientsession;
}


function getModuleintancetemp() {
	if(empty($this->moduleintancetemp)){$this->moduleintancetemp=time();}
    return $this->moduleintancetemp;
}

function setModuleintancetemp($moduleintancetemp) {
    $this->moduleintancetemp = $moduleintancetemp;
}

 function getResultexec() {
      return $this->resultexec;
  }

  function setResultexec($resultexec) {
      $this->resultexec = $resultexec;
  }


 function getParamtemp() {
      return $this->paramtemp;
  }

  function setParamtemp($paramtemp) {
      $this->paramtemp = $paramtemp;
  }
  
  public function setFormFieldList($formFieldList)
    {
        $this->formFieldList = $formFieldList;
	}
	
	public function getFormFieldList()
    {
        return $this->formFieldList;
    }	
}
