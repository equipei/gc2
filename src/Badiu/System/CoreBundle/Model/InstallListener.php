<?php

namespace Badiu\System\CoreBundle\Model;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class InstallListener
{
    /**
     * @var RouterInterface
     */
    private $router;

	private $container;
    /**
     * @var RouterInterface $router
     */
    public function __construct(RouterInterface $router, Container $container)
    {
        $this->router = $router;
		$this->container = $container;
    }

	public function onKernelRequest(GetResponseEvent $event) {
		$badiuSession = $this->container->get('badiu.system.access.session');
		if($badiuSession->exist()){return null;}
		
		$keychekinstall='badiu.system.core.core.badiunetinstalled';
		$isinstalled=$badiuSession->getValueOff($keychekinstall);
		if($isinstalled){return null;}
		
		$currentroute=$this->container->get('request')->get('_route');
		if($currentroute=='badiu.system.core.update.link'){return null;}
		
		$systemcheck = $this->container->get('badiu.system.core.lib.systemcheck');
		
		$existusertbl=$systemcheck->exitUserTable();;
		$connected=$systemcheck->isDbConnection();
		$installcompleted=0;
		
		if($connected && $existusertbl){$installcompleted=$systemcheck->isInstallationCompleted();}
		
	
		if(!$connected){
			$updatelink=$this->container->get('badiu.system.core.update.link');
			$mparam=array();
			$mparam['title']=$this->container->get('translator')->trans('badiu.system.core.failed');
			$message="<div class=\"alert alert-danger\" role=\"alert\">".$this->container->get('translator')->trans('badiu.system.core.install.connectiondbfailed')." </div>";
			$mparam['content']=$message;
			$msg=$updatelink->showMessage($mparam);
			echo $msg;
			exit;
		}
		else if($connected && $existusertbl && $installcompleted){
			$badiuSession->addValueOff($keychekinstall,1);
			return null;
		}else if($connected && !$existusertbl && !$installcompleted){
			
			$utilapp =$this->container->get('badiu.system.core.lib.util.app');
			$fparam=array('_function'=>'acceptTerm');
			$url=$utilapp->getUrlByRoute('badiu.system.core.update.link',$fparam);
			header('Location: '.$url);
			exit;
			return null;
		}else if($connected && !$existusertbl && !$installcompleted){
			$utilapp =$this->container->get('badiu.system.core.lib.util.app');
			$fparam=array('_operation'=>'update','_forceclearcache'=>1);
			$url=$utilapp->getUrlByRoute('badiu.system.core.update.link',$fparam);
			header('Location: '.$url);
			return null;
		}
		
	}
   
}