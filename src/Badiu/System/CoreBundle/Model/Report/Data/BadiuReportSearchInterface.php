<?php

namespace Badiu\System\CoreBundle\Model\Report\Data;
use Badiu\System\CoreBundle\Model\Report\Data\Sql\BadiuReportSqlParam;
use Doctrine\ORM\EntityManager;
interface BadiuReportSearchInterface {
    
   public  function searchCount(BadiuReportSqlParam $param);
   public function search(BadiuReportSqlParam $param);
   public function makeParamSql(BadiuReportSqlParam $param);
   public function getEm();
   public function setEm(EntityManager $em); 
     
}
