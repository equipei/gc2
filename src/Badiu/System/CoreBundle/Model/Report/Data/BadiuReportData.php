<?php

namespace Badiu\System\CoreBundle\Model\Report\Data;
class BadiuReportData {
   
    /**
     * @var integer
     */
    private $count;
   
    /**
     * @var array
     */
   private $rows;

   /**
     * @var integer
     */
    private $countschedulertask;


   public function getCount() {
       return $this->count;
   }

   public function getRows() {
       return $this->rows;
   }

   public function setCount($count) {
       $this->count = $count;
   }

   public function setRows($rows) {
       $this->rows = $rows;
   }

   public function getCountschedulertask() {
    return $this->countschedulertask;
}

public function setCountschedulertask($countschedulertask) {
    $this->countschedulertask = $countschedulertask;
}
}
