<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Sql;
use Doctrine\ORM\Query;
class SqlFilter {
  
    public static  $OPARATOR_MAT_EQUAL ="EQUAL";
    public static  $OPARATOR_MAT_DIFFERENT ="DIFFERENT";
    public static  $OPARATOR_MAT_LIKE ="LIKE";
    public static  $OPARATOR_MAT_LIKE_LEFT ="LIKE_LEFT";
    public static  $OPARATOR_MAT_LIKE_RIGHT ="LIKE_RIGHT";
    public static  $OPARATOR_MAT_IN ="IN";
    public static  $OPARATOR_MAT_NOT_IN ="NOT_IN";
    public static  $OPARATOR_LOGIC_AND ="AND";
    
    
    /**
     * @var array
     */
    private $list;
     
      /**
     * @var boolean
     */
     private $override;
   function __construct() {
                 $this->list=array();
                 $this->override=FALSE;
            }
 public function clean() {
     $this->list=array();
 } 
      
 public function add($key,$var,$tablealias,$tablecolumn,$operatorlogic=null,$operatormat=null) {
     if($operatorlogic==null){$operatorlogic=self::$OPARATOR_LOGIC_AND;}
     if($operatormat==null){$operatormat=self::$OPARATOR_MAT_EQUAL;}
     $this->list[$key]=array('var'=>$var,'tablealias'=>$tablealias,'tablecolumn'=>$tablecolumn,'operatorlogic'=>$operatorlogic,'operatormat'=>$operatormat);
 }
 public function addCollection($newlist) {
     $this->list=array_merge($this->list,$newlist);
  }
  
 public function get() {
      $v= "";
      $cont=0;
     foreach ($this->list as $key => $value){
      /*
       * if(isset($param->var->roleAssignments->idAssignments)){
               $param->filtro.= " AND rs.idAssignments=:roleAssignments_idAssignments "; 
            }
       */
    
       $commandwhere="";
       $var=$value['var'];
       $operatorlogic=$value['operatorlogic'];
       $operatormat=$value['operatormat'];
       $textOperator=$this->istTextOperator($operatormat);
       $operatormat=$this->getMatOperatorValue($operatormat);
       
       $tablealias=$value['tablealias'];
       $tablecolumn=$value['tablecolumn'];
       $tablecolumn=$tablealias.'.'.$tablecolumn; 
        if($textOperator){
                $tablecolumn=" UPPER ($tablecolumn) ";
        }
       
        $key=$this->matOperatorKey($value['operatormat'],$key);
       if($cont==0){
           $commandwhere=" WHERE ";
            $operatorlogic=" ";
        }
       
        if(isset($var)){
             $v.= ' '.$commandwhere .' '.$operatorlogic.' '.  $tablecolumn .' '.$operatormat.' '.$key ." ";
             $cont++;
         }
         
      }
        return $v;
 }  

 
  public function getQuery(Query $query) {
      
     
      foreach ($this->list as $key => $value){
          $var=$value['var'];
          $operatormat=$value['operatormat'];
          $textOperator=$this->istTextOperator($operatormat);
          
        if(isset($var)){
            if($textOperator){$var=mb_strtoupper($var, 'UTF-8');}
            $var=$this->matOperatorValue($operatormat,$var);
          // echo "<br >$key - $var";
            $query->setParameter($key, $var);
        }
         
      }
        return $query;
 }  
  public function matOperatorValue($operatormat,$var) {
         if($operatormat==self::$OPARATOR_MAT_LIKE){
              $var="%$var%";
          }else if($operatormat==self::$OPARATOR_MAT_LIKE_LEFT){
               $var="%$var";
          }else if($operatormat==self::$OPARATOR_MAT_LIKE_RIGHT){
               $var="$var%";
          }
          return $var;
 }
  public function matOperatorKey($operatormat,$key) {
    
            if($operatormat==self::$OPARATOR_MAT_IN){
               $key="(:$key)";
           }else if($operatormat==self::$OPARATOR_MAT_NOT_IN){
              $key="(:$key)";
          }else{
               $key=':'.$key;
          }
          return $key;
 } 
 public function getMatOperatorValue($txtoperator) {
     if($txtoperator==self::$OPARATOR_MAT_EQUAL) return " = ";
     if($txtoperator==self::$OPARATOR_MAT_DIFFERENT) return " != ";
     else if($txtoperator==self::$OPARATOR_MAT_LIKE) return " LIKE ";
     else if($txtoperator==self::$OPARATOR_MAT_LIKE_LEFT) return " LIKE ";
     else if($txtoperator==self::$OPARATOR_MAT_LIKE_RIGHT) return " LIKE ";
     else if($txtoperator==self::$OPARATOR_MAT_IN) return " IN ";
       else if($txtoperator==self::$OPARATOR_MAT_NOT_IN) return " NOT IN ";
 }
 
 
  public function istTextOperator($txtoperator) {
     if($txtoperator==self::$OPARATOR_MAT_EQUAL) return FALSE;
     else if($txtoperator==self::$OPARATOR_MAT_LIKE) return TRUE;
     else if($txtoperator==self::$OPARATOR_MAT_LIKE_LEFT) return TRUE;
     else if($txtoperator==self::$OPARATOR_MAT_LIKE_RIGHT) return TRUE;
 }
 public function getList() {
     return $this->list;
 }

 public function setList($list) {
     $this->list = $list;
 }

 public function getOverride() {
     return $this->override;
 }

 public function setOverride($override) {
     $this->override = $override;
 }


}
