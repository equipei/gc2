<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Sql;
class SqlColumn {
  
     /**
     * @var array
     */
    private $list;
    
      /**
     * @var string
     */
     private $command;
      /**
     * @var boolean
     */
     
     private  $aggregateFunctions;
      /**
     * @var boolean
     */
     private $override;
   function __construct() {
                 $this->list=array();
                 $this->command=null;
                 $this->override=FALSE;
                  $this->aggregateFunctions=FALSE;
            }
 public function clean() {
     $this->list=array();
 } 
      
 public function add($tblalias,$column) {
     $value=$tblalias.".".$column;
     array_push($this->list,$value);
 }

 public function get() {
     if(!empty($this->command)){ return $this->command;}
     
     $cont=0;
     $var="";
      foreach($this->list as $l){
          if($cont==0){
              $var=" $l ";
          }else{
              $var.=", $l ";
          }
          
          $cont++;
          
      }
        return $var;
 }   
 
 public function getList() {
     return $this->list;
 }

 public function getCommand() {
     return $this->command;
 }

 public function getOverride() {
     return $this->override;
 }

 public function setList($list) {
     $this->list = $list;
 }

 public function setCommand($command) {
     $this->command = $command;
 }

 public function setOverride($override) {
     $this->override = $override;
 }

 public function getAggregateFunctions() {
     return $this->aggregateFunctions;
 }

 public function setAggregateFunctions($aggregateFunctions) {
     $this->aggregateFunctions = $aggregateFunctions;
 }


}
