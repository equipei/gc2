<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Sql;
class SqlOrder {
  
     /**
     * @var array
     */
    private $list;

    /**
     * @var string
     */
    private $command;
     /**
     * @var boolean
     */
    private $desc;
      /**
     * @var boolean
     */
    private $override;
   function __construct() {
                 $this->list=array();
                 $this->override=FALSE;
                 $this->command=null;
            }
 public function clean() {
     $this->list=array();
 } 
      
 public function add($tblalias,$column) {
     $value=$tblalias.".".$column;
     array_push($this->list,$value);
 }

 public function get() {
     if(!empty($this->command)) return $this->command;
     
     $cont=0;
     $var="";
     $commandsort=" DESC ";
     if($this->desc){$commandsort=" DESC ";}
     else { $commandsort=" ASC ";}
      foreach($this->list as $l){
          if($cont==0){
              $var.=" ORDER BY $l ";
          }else{
              $var.=", $l ";
          }
          
          $cont++;
          
      }
    if($cont>0){$var.=$commandsort;}
       return $var;
 }   
 
 
 public function getList() {
     return $this->list;
 }

 public function setList($list) {
     $this->list = $list;
 }
 public function getDesc() {
     return $this->desc;
 }

 public function setDesc($desc) {
     $this->desc = $desc;
 }

 public function getOverride() {
     return $this->override;
 }

 public function setOverride($override) {
     $this->override = $override;
 }

 public function getCommand() {
     return $this->command;
 }

 public function setCommand($command) {
     $this->command = $command;
 }



}
