<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Sql;
class SqlGroup {
  
     /**
     * @var array
     */
    private $list;

      /**
     * @var boolean
     */
     private $override;
     
       /**
     * @var string
     */
     private $command;
     
   function __construct() {
                 $this->list=array();
                 $this->override=FALSE;
                  $this->command=null;
            }
 public function clean() {
     $this->list=array();
 } 
      
 public function add($tblalias,$column) {
     $value=$tblalias.".".$column;
     array_push($this->list,$value);
 }
  
 public function get() {
       if(!empty($this->command)){ return $this->command;}
     $cont=0;
     $var="";
     
      foreach($this->list as $l){
          if($cont==0){
              $var=" GROUP BY $l ";
          }else{
              $var.=", $l ";
          }
          
          $cont++;
          
      }
     
        return $var;
 }   
 
 
 public function getList() {
     return $this->list;
 }

 public function setList($list) {
     $this->list = $list;
 }

 public function getOverride() {
     return $this->override;
 }

 public function setOverride($override) {
     $this->override = $override;
 }

 public function getCommand() {
     return $this->command;
 }

 public function setCommand($command) {
     $this->command = $command;
 }



}
