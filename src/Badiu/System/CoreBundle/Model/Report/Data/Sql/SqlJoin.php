<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Sql;
class SqlJoin {
  
    public static  $INNER_JOIN ="JOIN";
    
    /**
     * @var array
     */
    private $list;
 
   function __construct() {
                 $this->list=array();
                
            }
 public function clean() {
     $this->list=array();
 } 
      
 public function add($tablealiasjoin,$tablecolumn,$tablealias,$jointype=null) {
      if($jointype==null){$jointype=self::$INNER_JOIN;}
      $this->list[$tablealias]=array('tablealiasjoin'=>$tablealiasjoin,'tablecolumn'=>$tablecolumn,'tablealias'=>$tablealias,'jointype'=>$jointype);
 }
  
 public function get() {
    
      $v= "";
    foreach ($this->list as $key => $value){
        
        $jointype=$value['jointype'];
        $tablealiasjoin= $value['tablealiasjoin'];
        $tablecolumn=$value['tablecolumn'];
        $tablealias=$value['tablealias'];
         $v.=$jointype . ' '. $tablealiasjoin.'.'.$tablecolumn . ' '. $tablealias. ' ';
      }
       
      return $v;
 }   

 public function getList() {
     return $this->list;
 }

 public function setList($list) {
     $this->list = $list;
 }




}
