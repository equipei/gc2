<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Sql;
use Badiu\System\CoreBundle\Model\Report\Data\Sql\SqlColumn;
use Badiu\System\CoreBundle\Model\Report\Data\Sql\SqlJoin;
use Badiu\System\CoreBundle\Model\Report\Data\Sql\SqlFilter;
use Badiu\System\CoreBundle\Model\Report\Data\Sql\SqlGroup;
use Badiu\System\CoreBundle\Model\Report\Data\Sql\SqlOrder;
class BadiuReportSqlParam {

     
      public $var;
      /**
     * @var SqlColumn
     */
     private $column;
    
      /**
     * @var SqlJoin
     */
     private $join; 
     
      /**
     * @var SqlFilter
     */
     private $filter;
      /**
     * @var SqlOrder
     */
     private $order;
     
      /**
     * @var SqlGroup
     */
     private $group;
     
        /**
     * @var string
     */
     private $table;
      /**
     * @var string
     */
     private $tablePrefix;
   
   
     
   function __construct($var,$override=FALSE,$aggregateFunctions=FALSE) {
                 $this->var= $var;
                 
                 $this->column= new SqlColumn();
                 $this->column->setOverride($override);
                 $this->column->setAggregateFunctions($aggregateFunctions);
                 
                 $this->filter=new SqlFilter();
                 
                 $this->join=new SqlJoin();
                 
                 $this->order=new SqlOrder();
                 $this->order->setOverride($override);
                 
                 $this->group=new SqlGroup();
                 $this->group->setOverride($override);
                
       }
    
        public function getSql() {
          $sql =" SELECT  ";
          $sql.=$this->getColumn()->get();
          $sql.=" FROM  ";
          $sql.=$this->table;
          $sql.=" ";
          $sql.=$this->tablePrefix;
          $sql.=" ";
          $sql.= $this->getJoin()->get();
          $sql.= $this->getFilter()->get();
                  ;
          $sql.=" ";
          $sql.= $this->getGroup()->get();
          $sql.=" ";
         // $sql.=$this->having;
          $sql.=" ";
          if(!$this->getColumn()->getAggregateFunctions()){
               $sql.=$this->order->get();
          }
         
          $sql.=" ";
         
        return $sql;
       }

    
       public function getColumn() {
           return $this->column;
       }

       public function getJoin() {
           return $this->join;
       }

       public function getFilter() {
           return $this->filter;
       }

       public function getOrder() {
           return $this->order;
       }

       public function getGroup() {
           return $this->group;
       }

       public function setColumn(SqlColumn $column) {
           $this->column = $column;
       }

       public function setJoin(SqlJoin $join) {
           $this->join = $join;
       }

       public function setFilter(SqlFilter $filter) {
           $this->filter = $filter;
       }

       public function setOrder(SqlOrder $order) {
           $this->order = $order;
       }

       public function setGroup(SqlGroup $group) {
           $this->group = $group;
       }

     

       public function getTable() {
           return $this->table;
       }

       public function getTablePrefix() {
           return $this->tablePrefix;
       }

       public function setTable($table) {
           $this->table = $table;
       }

       public function setTablePrefix($tablePrefix) {
           $this->tablePrefix = $tablePrefix;
       }

       public function getVar() {
           return $this->var;
       }

       public function setVar($var) {
           $this->var = $var;
       }


}
