<?php

namespace Badiu\System\CoreBundle\Model\Report\Data;
use Badiu\System\CoreBundle\Model\Report\Data\Table\BadiuReportDataTable;
use Badiu\System\CoreBundle\Model\Report\Data\BadiuReportData;
use Doctrine\ORM\EntityManager;
 use Symfony\Component\Translation\Translator;
interface BadiuReportDataInterface {
    
    public  function extractData($paramFilter);
    public  function makeTableColumnsTitle();
    public  function makeTableRows();
    public  function makeTableInfo();
    
    public function makeTable();
    public function getData();
    public function setData(BadiuReportData $em);
    
    public function getTable();
    public function setTable(BadiuReportDataTable $table);
   
    public function exportTable($format);
    
    public function getEm();
    public function setEm(EntityManager $em);
    
    public function getTranslator();
    public function setTranslator(Translator $translator);
}
