<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Table;
class TableColumn {
  
     /**
     * @var array
     */
    private $list;
     /**
     * @var array
     */
    private $types;
    /**
     * @var integer
     */
    private $count; 
    /**
     * @var integer
     */
    private $dynamicCount; 
     
   function __construct() {
                 $this->list=array();
                 $this->types=array();
                 $this->count=0;
                 $this->dynamicCount=0;
            }
 public function clean() {
     $this->list=array();
 } 
      
 public function add($key,$name,$type=null) {
     $this->list[$key]=array('name'=>$name,'type'=>$type);
     array_push($this->types,$type);
     $this->updateCount();
 }
  
 public function updateCount() {
     $this->count++;
 }
  public function updateDynamicCount() {
     $this->dynamicCount++;
 }
 public function getList() {
     return $this->list;
 }

 public function setList($list) {
     $this->list = $list;
 }
 public function getDynamicCount() {
     return $this->dynamicCount;
 }

 public function setDynamicCount($dynamicCount) {
     $this->dynamicCount = $dynamicCount;
 }
 public function getCount() {
     return $this->count;
 }

 public function setCount($count) {
     $this->count = $count;
 }




}
