<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Table;
class TableRow {
  
     /**
     * @var array
     */
    private $list;
     
     
   function __construct() {
                 $this->list=array();
            }
 public function clean() {
     $this->list=array();
 } 
      
 public function add($row) {
      array_push($this->list,$row);
 }
 
 public function getList() {
     return $this->list;
 }

 public function setList($list) {
     $this->list = $list;
 }


}
