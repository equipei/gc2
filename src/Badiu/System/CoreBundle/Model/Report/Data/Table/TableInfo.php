<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Table;
class TableInfo {
  
     /**
     * @var array
     */
    private $list;
     
     
   function __construct() {
                 $this->list=array();
            }
 public function clean() {
     $this->list=array();
 } 
      
 public function add($column) {
     array_push($this->list,$column);
 }
  
  public function get() {
     $cont=0;
     $var="";
      foreach($this->list as $l){
          if($cont==0){
              $var=" $l ";
          }else{
              $var.="\n $l ";
          }
          
          $cont++;
          
      }
        return $var;
 }   
 
 public function getList() {
     return $this->list;
 }

 public function setList($list) {
     $this->list = $list;
 }


}
