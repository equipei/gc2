<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Table;
use Badiu\System\CoreBundle\Model\Report\Data\Table\TableColumn;
use Badiu\System\CoreBundle\Model\Report\Data\Table\TableRow;
use Badiu\System\CoreBundle\Model\Report\Data\Table\TableInfo;
class BadiuReportDataTable {
   
    /**
     * @var TableInfo
     */
    private $info;
 
    /**
     * @var TableColumn
     */
   private $column;
   
    /**
     * @var array
     */
   private $columntype;
   /**
     * @var TableRow
     */
   private $row;
        /**
     * @var integer
     */
   private $countrow;

       /**
     * @var integer
     */
    private $countrowcurrent;
     /**
     * @var boolean
     */
    private $countrowcurrentforceset=false;
    /**
     * @var integer
     */
    private $pageindex=0;

        /**
     * @var integer
     */
    private $pagemaxrow=10; //for report client
    /**
     * @var array
     */
   private $columngroup;

   
   /**
     * @var integer
     */
    private $countschedulertask;

    /**
     * @var integer
     */
    private $limitrowshow=120;
   function __construct() {
                 $this->info=new TableInfo();
                 $this->column=new TableColumn();
				 $this->columntype=array();
                 $this->row=new TableRow();
                 
            }
            public function castToArray() {
                $countcurrentrow=0;
                if(isset($this->row)!=null && is_array($this->row->getList())){$countcurrentrow=sizeof($this->row->getList());}
                $data=array('countrow'=>$this->getCountrow(),'countcurrentrow'=>$countcurrentrow,'pageindex'=>$this->getPageindex(),'pagemaxrow'=>$this->getPagemaxrow(),'columns'=>$this->column->getList(),'rows'=>$this->row->getList());
                return $data;
            }
            public function makeColumngroup($groups,$fields) {
                $list=array();
              
               if(empty($groups) || empty($fields)){return null; }
               foreach ($groups as $g){
                    $childrem=array();
                    foreach ($fields as $fkey => $fvalue){
                            if($fvalue==$g){
                                array_push($childrem,$fkey);
                           }
                    }
                    $list[$g]=$childrem;
               }
            
                 $this->columngroup=$list;
            }

            public function getInfo() {
                return $this->info;
            }

            public function getColumn() {
                return $this->column;
            }

            public function getRow() {
                return $this->row;
            }

            public function setInfo(TableInfo $info) {
                $this->info = $info;
            }

            public function setColumn(TableColumn $column) {
                $this->column = $column;
            }

            public function setRow(TableRow $row) {
                $this->row = $row;
            }



            public function getCountrow() {
                  return $this->countrow;
            }

            public function setCountrow($countrow) {
                $this->countrow = $countrow;
            }


    /**
     * @return int
     */
    public function getPageindex()
    {
        return $this->pageindex;
    }

    /**
     * @param int $pageindex
     */
    public function setPageindex($pageindex)
    {
        $this->pageindex = $pageindex;
    }

    public function getColumngroup() {
        return $this->columngroup;
    }
    public function setColumngroup($columngroup) {
        $this->columngroup = $columngroup;
    }

    public function getCountschedulertask() {
        return $this->countschedulertask;
    }
    
    public function setCountschedulertask($countschedulertask) {
        $this->countschedulertask = $countschedulertask;
    }

    public function geLimitrowshow() {
        return $this->limitrowshow;
    }
    
    public function setLimitrowshow($limitrowshow) {
        $this->limitrowshow = $limitrowshow;
    }

    
    public function getCountrowcurrent() {
        if(is_array($this->row->getList()) && !$this->countrowcurrentforceset){$this->countrowcurrent=sizeof($this->row->getList());}
        return $this->countrowcurrent;
  }

  public function setCountrowcurrent($countrowcurrent) {
      $this->countrowcurrent = $countrowcurrent;
  }

  public function getCountrowcurrentforceset() {
    return $this->countrowcurrentforceset;
}

public function setCountrowcurrentforceset($countrowcurrentforceset) {
  $this->countrowcurrentforceset = $countrowcurrentforceset;
}
  public function getPagemaxrow() {
    return $this->pagemaxrow;
}

public function setPagemaxrow($pagemaxrow) {
    $this->pagemaxrow= $pagemaxrow;
}

public function getColumntype() {
         return $this->columntype;
    }
			
	public function setColumntype($columntype) {
         $this->columntype = $columntype;
   }
}
