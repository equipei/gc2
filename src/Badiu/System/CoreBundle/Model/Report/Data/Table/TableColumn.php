<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Table;
class TableColumn {
  
     /**
      * normal title
     * @var array
     */
    private $list;
    /**
     * title muster
     * @var array
     */
    private $listMuster;
    
     /**
     * @var array
     */
    private $info;
    /**
     * @var integer
     */
    private $count; 
    /**
     * @var integer
     */
    private $dynamicCount; 
     
   function __construct() {
                 $this->list=array();
                 $this->listMuster=array();
                 $this->info=array();
                 $this->count=0;
                 $this->dynamicCount=0;
            }
 public function clean() {
     $this->list=array();
     $this->info=array();
 } 
      
 public function add($key,$name,$info=array()) {
     $this->list[$key]=$name;
     $this->info[$key]=$info;
     $this->updateCount();
 }
  
 public function updateCount() {
     $this->count++;
 }
  public function updateDynamicCount() {
     $this->dynamicCount++;
 }
 public function getList() {
     return $this->list;
 }

 public function setList($list) {
     $this->list = $list;
 }
 public function getDynamicCount() {
     return $this->dynamicCount;
 }

 public function setDynamicCount($dynamicCount) {
     $this->dynamicCount = $dynamicCount;
 }
 public function getCount() {
     return $this->count;
 }

 public function setCount($count) {
     $this->count = $count;
 }

 public function getListMuster() {
     return $this->listMuster;
 }

 public function setListMuster($listMuster) {
     $this->listMuster = $listMuster;
 }

 public function getInfo() {
     return $this->info;
 }

 public function setInfo($info) {
     $this->info = $info;
 }






}
