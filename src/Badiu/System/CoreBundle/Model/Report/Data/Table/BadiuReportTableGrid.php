<?php

namespace Badiu\System\CoreBundle\Model\Report\Data\Table;
class BadiuReportTableGrid {
  
     /**
     * @var array
     */
    private $list;
     
     
   function __construct() {
                 $this->list=array();
            }
 public function clean() {
     $this->list=array();
 } 
      
 public function add($key,$value,$overridevalue=TRUE,$separator=", ") {
     if($overridevalue){$this->list[$key]=$value;}
     else {
         if(array_key_exists($key,$this->list)){
             $oldvalue=$this->list[$key];
             $newvalue=$oldvalue.$separator.$value;
             $this->list[$key]=$newvalue;
         } else{
             $this->list[$key]=$value;
         }
     }
 }
 
 public function get($key) {
     $value=null;
     if (array_key_exists($key,$this->list)){$value=$this->list[$key];}
     return $value;
 } 
 
 public function getList() {
     return $this->list;
 }

 public function setList($list) {
     $this->list = $list;
 }


}
