<?php

namespace Badiu\System\CoreBundle\Model\Report\Data;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class BadiuReportDataSelectRow {

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
   private $columns;

    /**
     * @var boolean
     */
    private $existControl;

    /**
     * @var string
     */
    private $format;

    function __construct(Container $container,$columns=array(),$format=null) {
        $this->container=$container;
        $this->columns=$columns;
        $this->existControl=FALSE;
        $this->format=$format;
        $this->init();

    }
    public function init() {

        if (array_key_exists("_ctrl", $this->columns)) {
            $this->existControl=TRUE;
        }else { $this->existControl=FALSE;}
        $this->existControl=FALSE;//review implements js

   }
    public function addColumns($columns) {

        if ($this->existControl && empty($this->format)) {
            $data= "<input type=\"checkbox\"  id =\"chckHead\"/>";
            $columns= array_merge(array('_checkbox'=>$data),$columns);
        }

        return $columns;
    }
    public function addData($data) {
        $data= "<input type=\"checkbox\" class = \"chcktbl\"  />";
        return $data;
    }
    public function getContainer() {
        return $this->container;
    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }





    /**
     * @return boolean
     */
    public function isExistControl()
    {
        return $this->existControl;
    }

    /**
     * @param boolean $existControl
     */
    public function setExistControl($existControl)
    {
        $this->existControl = $existControl;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }


}
