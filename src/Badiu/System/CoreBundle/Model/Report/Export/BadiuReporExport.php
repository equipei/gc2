<?php

namespace Badiu\System\CoreBundle\Model\Report\Export;
use Badiu\System\CoreBundle\Model\Report\Data\Table\BadiuReportDataTable;
use Badiu\System\CoreBundle\Model\Report\Export\BadiuReporExcelExport;

class BadiuReporExport {
      /*
    * @var string
    */
   private $format;
   
      /*
    * @var string
    */
   private $fileName;
 /*
    * @var string
    */
   private $extension;
   /**
     * @var BadiuReportDataTable
     */
   private $table;
  

      function __construct($table=null,$format,$fileName=null,$extension=null) {
                 $this->table=$table;
                 $this->format=$format;
                 $this->fileName=$fileName;
                 $this->extension=$extension;
                
                  
     }
     public function attachToMail() {
        if($this->format=='xls'){
           $export= new BadiuReporExcelExport($this->table,$this->fileName);
          return  $export->attachToMail();
        }
    }
     public function download() {
         if($this->format=='xls'){
            $export= new BadiuReporExcelExport($this->table,$this->fileName);
            $export->download();
         }
     }

    
     public function getFormat() {
         return $this->format;
     }

     public function getFileName() {
         return $this->fileName;
     }

     public function getExtension() {
         return $this->extension;
     }

     public function getTable() {
         return $this->table;
     }

     public function setFormat($format) {
         $this->format = $format;
     }

     public function setFileName($fileName) {
         $this->fileName = $fileName;
     }

     public function setExtension($extension) {
         $this->extension = $extension;
     }

     public function setTable(BadiuReportDataTable $table) {
         $this->table = $table;
     }



}
