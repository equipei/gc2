<?php

namespace Badiu\System\CoreBundle\Model\Report\Export;
use Symfony\Component\HttpFoundation\Response;
use Badiu\System\CoreBundle\Model\Report\Data\Table\BadiuReportDataTable;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Cell;
use PHPExcel_RichText;
class BadiuReporExcelExport {
      /*
    * @var string
    */
   private $fileName;
 /*
    * @var string
    */
   private $extension;
   /**
     * @var BadiuReportDataTable
     */
   private $table;
  

      function __construct($table=null,$fileName=null,$extension=null) {
                 $this->table=$table;
                 $this->fileName=$fileName;
                 $this->extension=$extension;
                 if(empty($this->extension)){$this->extension=".xls";}
                 if(empty($this->fileName)){$this->fileName="BadiuReport_".time();}
                  
     }
     
     public function getFileContent() {
       $objPHPExcel = new PHPExcel();
      $objPHPExcel->getProperties()->setCreator("Badiu Report")
                   ->setLastModifiedBy("Badiu Report")
                   ->setTitle("Badiu Report")
                   ->setSubject("Badiu Report");
       
        $y=1;
        $x=0;
       
        //cabecalho
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x,$y,$this->getTable()->getInfo()->get());
        $y++;
        $x=0;
        //mesclar primeira linha
        $rangeInfo=$this->getCellsRange(0, sizeof($this->getTable()->getColumn()->getList())-1,1);
        $rangeHead=$this->getCellsRange(0, sizeof($this->getTable()->getColumn()->getList())-1,2);
        $objPHPExcel->getActiveSheet()->mergeCells($rangeInfo);
        //tornar negrito o cabeçalho
        $objPHPExcel->getActiveSheet()->getStyle($rangeHead)->getFont()->setBold(true);
        
        foreach ($this->getTable()->getColumn()->getList()  as $value) {
             $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x,$y,$value);
            $x++;
         }
         $y++;
         
         foreach ($this->getTable()->getRow()->getList() as $row) {
              $x=0;
               foreach ($this->getTable()->getColumn()->getList() as  $ckey =>$value) {
                   $v = "";
                  if (array_key_exists($ckey,$row)){ $v = $row[$ckey];}
                  $v = $this->cleanHtml($v);
                  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x,$y,$v);
                   $x++;
               }
              
             $y++; 
         }
      
    //ajustar tamanho da coluna automaticamente     
    $cellIterator = $objPHPExcel->getActiveSheet()->getRowIterator()->current()->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells( true );
    
    foreach( $cellIterator as $cell ) {
        $objPHPExcel->getActiveSheet()->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
    }
      
       $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
       return $objWriter;
      
   }
  public function attachToMail() {
       $file=$this->getFileContent();
       ob_start();
       $file->save('php://output');
       $fdata = ob_get_contents();
       ob_end_clean();
        // $file->save('/home/dev1/badiunetdata/badiunet/teste7.xls');
        $attach= \Swift_Attachment::newInstance($fdata,$this->getFileName().$this->getExtension(),'application/vnd.ms-excel');
       return $attach;
    }
     public function download() {
         $file=$this->getFileContent();
         $response = new Response();
         $response->headers->set('Content-Type', 'application/vnd.ms-excel');
         $response->headers->set('Content-Disposition', 'attachment;filename="'.$this->getFileName().$this->getExtension());
         $response->headers->set('Cache-Control', 'max-age=0');
         //$response->prepare();
         $response->sendHeaders();
         $file->save('php://output');
         exit();
   }
   
   
function getCellsRange($start, $end, $row){
    $merge = 'A1:A1';
    if($start>=0 && $end>=0 && $row>=0){
        $start = PHPExcel_Cell::stringFromColumnIndex($start);
        $end = PHPExcel_Cell::stringFromColumnIndex($end);
        $merge = "$start{$row}:$end{$row}";

    }
  //echo $merge;exit;
    return $merge;
}

public function cleanHtml($txt) {
    if(empty($txt)){return $txt;}
    $txt=str_replace("<br />","\n",$txt);
    $txt=str_replace("<br  />","\n",$txt);
    $txt=str_replace("<br>","\n",$txt);
   $txt= strip_tags($txt);
    return $txt;
}

public function getFileName() {
    return $this->fileName;
}

public function getExtension() {
    return $this->extension;
}

public function getTable() {
    return $this->table;
}

public function setFileName($fileName) {
    $this->fileName = $fileName;
}

public function setExtension($extension) {
    $this->extension = $extension;
}

public function setTable(BadiuReportDataTable $table) {
    $this->table = $table;
}


}
