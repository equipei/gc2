<?php

namespace Badiu\System\CoreBundle\Model\General;
use Badiu\System\CoreBundle\Model\Report\Data\Sql\BadiuReportSqlParam;
use Doctrine\ORM\EntityManager;
interface BadiuGeneralDataInterface {
    
    public  function save();
    public  function edit();
    public  function delete();
    public  function checkDuplication();
    public function search(\stdClass $param);
    public function searchCount(\stdClass $param);
    public function getEm();
    public function setEm(EntityManager $em);
}
