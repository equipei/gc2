<?php
namespace Badiu\System\CoreBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSchedulerExpression;

class DefaultSchedulerExpression extends BadiuSchedulerExpression{

    function __construct($container) {
        parent::__construct($container);
     }
    
     public function initTblexpresion(){
      $this->add('userid','USER_ID');
      $this->add('name','USER_FULLNAME');
	  $this->add('userfullname','USER_FULLNAME');
      $this->add('firstname','USER_FIRSTNAME'); 
      $this->add('lastname','USER_LASTNAME');
	  $this->add('alternatename','USER_ALTERNATENAME');
      $this->add('username','USER_LOGIN');
      $this->add('email','USER_EMAIL'); 
      $this->add('idnumber','USER_IDNUMBER');
     

   }

}
