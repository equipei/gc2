<?php

namespace Badiu\System\CoreBundle\Model\Page;
use Badiu\System\CoreBundle\Model\Report\Data\Table\BadiuReportDataTable;
use Symfony\Component\Form\FormView; 
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;
class BadiuPage implements BadiuPageInterface{
   
 /**
     * @var Container
     */
    private $container;

   /**
     * @var Translator
     */
    private $translator;
     /**
     * @var FormView
     */
    private $form;
    
      /**
     * @var BadiuReportDataTable
     */
    private $table;
    
      /**
     * @var array
     */
    private $additionalContents;
    
      /**
     * @var array
     */
    private $links;
    
     /**
     * @var array
     */
    private $message;
    
    /**
     * @var array
     */
    private $data;

    /**
 * @var string
 */
    private $title;

 /**
 * @var string
 */
    private $menuTitle;

  

    /**
     * @var string
     */
    private $module;

    /**
     * @var string
     */
    private $operation;

    
     /**
     * @var string
     */
    private $formviewlayout; //delete 

     /**
     * @var string
     */
    private $layout='report'; //frontpage | report | form | view | dashboard | message | permissiondaned
 /**
     * @var BadiuPageTitle
     */
    private $ptitle;
    
     /**
     * @var array
     */
    private $themelayoutblockhidden; //review this variable is inside config
    
  /**
     * @var object
     */
    private $config;
    
    /**
     * @var object
     */
    private $formconfig;
     /**
     * @var object
     */
    private $crudroute; 
    
      /**
     * @var boolean
     */
    private $isexternalclient=FALSE; 
    
      /**
     * @var boolean
     */
    private $isschedule=FALSE; 
      /**
     * @var key
     */
    private $key; 
    
        /** its used for multiple entity cron to isolate session
     * @var integer 
     */
    private $sessionhashkey;
    private $viewsytemtype = 'bootstrap4';// bootstrap4 | bootstrap3 | bootstrap2
	private $baseurl;
	private $serviceurl;
	private $filebase;
	private $langpackages;
	private $currenturl;
	private $currenturlencode;
	private $cache;
	private $lang;
	private $systemdata;
 function __construct(Container $container) {
            $this->container=$container;
            $this->translator=$this->container->get('translator');

            $this->additionalContents=array();
            $this->links=array();
            $this->message=array();
                
            $this->title="Badiu.Net";
            $this->menuTitle="";
               

            $this->operation="";
             //   $this->module="";
            $this->data=array();
            //isolate if of cron schculer
            
			$apputil = $this->getContainer()->get('badiu.system.core.lib.util.app');
			$this->baseurl=$apputil->getBaseUrl();
			$this->filebase=$this->baseurl.'/system/file/get/';
			//print_R($this->fileurl);exit; 
            $this->init();
            $this->crudroute=null;
			$this->serviceurl=$apputil->getServiceUrl();
			$this->langpackages=array();
			$this->currenturl=$apputil->getCurrentUrl();
			$this->currenturlencode=urlencode($this->currenturl);
			
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$this->translator->setLocale($badiuSession->get()->getLang());
    }

    public function init() {
        $this->ptitle= new BadiuPageTitle($this->container);
		$this->ptitle->getTranslator()->setSystemdata($this->getSystemdata());
        $this->title=$this->ptitle->getSite();
        $this->menuTitle=$this->ptitle->getMenu(); 
        $this->operation=$this->ptitle->getContent();
		//if(!empty($this->menuTitle)){$this->title=$this->menuTitle;}
		
		//manage theme
		$permission=$this->getContainer()->get('badiu.system.access.permission');
		$utilapp =$this->getContainer()->get('badiu.system.core.lib.util.app');
		$permissionthemconfigurl=$permission->has_access('badiu.theme.core.config.edit',$this->getSessionhashkey());
		if($permissionthemconfigurl){
			$themconfigurl=$utilapp->getUrlByRoute('badiu.theme.core.config.edit');
			$this->addData('badiuthemecorecustomurl',$themconfigurl);
		}
		
		$isadmin=$permission->has_access('badiu.',$this->getSessionhashkey());
		if($isadmin){
			$themconfigurl=$utilapp->getUrlByRoute('badiu.theme.core.config.edit');
			$this->addData('isamdin',1);
		}
		
		//add head menu
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		$headcontentmenu=$badiuSession->getValue('badiu.theme.core.core.param.config.headcontentmenu');
		if(!empty($headcontentmenu)){
			$hedmenu=$this->getContainer()->get('badiu.system.core.lib.util.factorymenu')->make($headcontentmenu);
			$this->addData('headcontentmenu',$hedmenu);
		}else{$this->addData('headcontentmenu',array());}
		
    }
//review deleted
  /*  function __construct(FormView $formView=null,BadiuReportDataTable $table=null,$additionalContents=array(),$links=array()) {
                $this->form=$formView;
                $this->table=$table;
                $this->additionalContents=$additionalContents;
                $this->links=$links;
                $this->message=array();
               
                $this->title="Badiu.Net";
                $this->menuTitle="";
               

                $this->operation="";
                $this->module="";
                $this->data=array();

            }*/
            
            public function addData($key,$value) {
                $this->data[$key]=$value;
               
            }
            public function getForm() {
                return $this->form;
            }

            public function getTable() {
                return $this->table;
            }

        

            public function setForm(FormView $form) {
                $this->form = $form;
            }

            public function setTable(BadiuReportDataTable $table) {
                $this->table = $table;
            }

           
            public function getAdditionalContents() {
                return $this->additionalContents;
            }

            public function getLinks() {
                return $this->links;
            }

            public function setAdditionalContents($additionalContents) {
                $this->additionalContents = $additionalContents;
            }

            public function setLinks($links) {
                $this->links = $links;
            }

            public function getMessage() {
                return $this->message;
            }

            public function setMessage($message) {
                $this->message = $message;
            }


            public function getData() {
                return $this->data;
            }

            public function setData($data) {
                $this->data = $data;
            }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


 /**
     * @return string
     */
    public function getMenuTitle()
    {
        return $this->menuTitle;
    }

    /**
     * @param string $menuTitle
     */
    public function setMenuTitle($menuTitle)
    {
        $this->menuTitle = $menuTitle;
    }

 


    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param string $operation
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;
    }
    function getFormviewlayout() {
        return $this->formviewlayout;
    }

    function setFormviewlayout($formviewlayout) {
        $this->formviewlayout = $formviewlayout;
    }

    function getContainer() {
        return $this->container;
    }

    function getTranslator() {
		if(!empty($this->translator) && empty($this->translator->getSystemdata())){$this->translator->setSystemdata($this->getSystemdata());}
        return $this->translator;
    }

    function getPtitle() {
        return $this->ptitle;
    }

  

    function setContainer(Container $container) {
        $this->container = $container;
    }

    function setTranslator(Translator $translator) {
        $this->translator = $translator;
    }

    function setPtitle(BadiuPageTitle $ptitle) {
        $this->ptitle = $ptitle;
    }

    function getThemelayoutblockhidden() {
        return $this->themelayoutblockhidden;
    }

    function setThemelayoutblockhidden($themelayoutblockhidden) {
        $this->themelayoutblockhidden = $themelayoutblockhidden;
    }
    function getLayout() {
        return $this->layout;
    }

    function setLayout($layout) {
        $this->layout = $layout;
    }

    function getConfig() {
        return $this->config;
    }

    function setConfig($config) {
        $this->config = $config;
    }

    function getCrudroute() {
        return $this->crudroute;
    }

    function setCrudroute($crudroute) {
        $this->crudroute = $crudroute;
    }

    function getFormconfig() {
        return $this->formconfig;
    }

    function setFormconfig($formconfig) {
        $this->formconfig = $formconfig;
    }

    function getIsexternalclient() {
        return $this->isexternalclient;
    }

    function setIsexternalclient($isexternalclient) {
        $this->isexternalclient = $isexternalclient;
    }

    function getKey(){
        return $this->key;
    } 

    function setKey($key) {
        $this->key = $key;
    }

    function getIsschedule() {
        return $this->isschedule;
    }

    function setIsschedule($isschedule) {
        $this->isschedule = $isschedule;
    }

    function getViewsytemtype() {
        return $this->viewsytemtype;
    }

    function setViewsytemtype($viewsytemtype) {
        $this->viewsytemtype = $viewsytemtype;
    }



    public function getSessionhashkey() {
        return $this->sessionhashkey;
    }
    
    public function setSessionhashkey($sessionhashkey) {
       $this->sessionhashkey = $sessionhashkey;
    }
  
  
  	    public function getBaseurl() {
        return $this->baseurl;
    }
    
    public function setBaseurl($baseurl) {
       $this->baseurl = $baseurl;
    }
	
	public function getFilebase() {
        return $this->filebase;
    }
    
    public function setFilebase($filebase) {
       $this->filebase = $filebase;
    }
	
	public function getServiceurl() {
        return $this->serviceurl;
    }
    
    public function setServiceurl($serviceurl) {
       $this->serviceurl = $serviceurl;
    }
	
	public function getLangpackages() {
		if(!empty($this->langpackages) || sizeof($this->langpackages)==0){
			$this->langpackages=$this->getContainer()->get('badiu.system.module.translator.form.dataoptions')->getPackages();
		}
        return $this->langpackages;
    }
    
    public function setLangpackages($langpackages) {
       $this->langpackages = $langpackages;
    }
	
	public function getCurrenturl() {
        return $this->currenturl;
    }
    
    public function setCurrenturl($currenturl) {
       $this->currenturl = $currenturl;
    }
	
	public function getCurrenturlencode() {
        return $this->currenturlencode;
    }
    
    public function setCurrenturlencode($currenturlencode) {
       $this->currenturlencode = $currenturlencode;
    }
	
	/**
     * Gets the value of cachedata.
     *
     * @return array
     */
    public function getCache()
    {
		if(empty($this->cache)){
			//$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			//$this->cache=$badiuSession->getGlobaldata()->getParam();
			$this->cache=$this->getSystemdata()->getManagecache()->getParam();
			
		}
		
        return $this->cache;
    }

    /**
     * Sets the value of cache.
     *
     * @param array $cache The new value
     *
     * @return self
     */
    public function setCache(array $cache)
    {
        $this->cache = $cache;
        return $this;
    }
	
	public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }

// Getter for $lang
    public function getLang() {
		if(empty($this->lang)){
			$this->lang=$this->getSystemdata()->getManagelang()->getParam();
		}
		//print_r($this->lang);exit;
        return $this->lang;
    }

    // Setter for $lang
    public function setLang($lang) {
        $this->lang = $lang;
    }
}
