<?php

namespace Badiu\System\CoreBundle\Model\Page;
use Badiu\System\CoreBundle\Model\Report\Data\Table\BadiuReportDataTable;
use Symfony\Component\Form\FormView; 
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Translation\Translator;
//use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger; 
class BadiuPageTitle {
   
 /**
     * @var Container
     */
    private $container;

   /**
     * @var Translator
     */
    private $translator;
    
     /**
     * @var object
     */
    private $keymanger;
 function __construct(Container $container) {
            $this->container=$container;
            $this->translator=$this->container->get('translator');
            $this->keymanger=$this->getContainer()->get('badiu.system.core.functionality.keymanger');
           // $this->keymanger=new BadiuKeyManger();
            $currentRoute=$this->container->get('request')->get('_route'); 
            $dkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey');
            if(!empty($dkey)){$currentRoute=$dkey;}
             $key=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($currentRoute);
            $this->keymanger->setBaseKey($key);
			$this->keymanger->setContainer($container);

    }

  public function getSite() {
			$title=$this->getServiceTitle();
			if(!empty($title)){return $title;}
             $currentRoute=$this->container->get('request')->get('_route'); 
             $dkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey');
            if(!empty($dkey)){$currentRoute=$dkey;}
			
             $title=$this->translator->trans($currentRoute);
             return $title;
        }

         public function getMenu(){
             $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
			$cbundle->setKeymanger($this->keymanger);
			$cbundle->initKeymangerExtend($this->getKeymanger());
             $keyTitle=$this->getKeymanger()->menuTitle();
			$keyTitleExend=$cbundle->getKeymangerExtend()->menuTitle();
//echo $keyTitle ."<br>";
//echo $keyTitleExend;
             $keyValue=$cbundle->getValue($keyTitle,$keyTitleExend);
             
             $currentRoute=$this->container->get('request')->get('_route'); 
             $dkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey');
            if(!empty($dkey)){$currentRoute=$dkey;}
             $keyutil=$this->getContainer()->get('badiu.system.core.lib.config.keyutil');
             if(empty($keyValue)){
                 $currentRoute=$this->container->get('request')->get('_route'); 
                 if(!empty($dkey)){$currentRoute=$dkey;}
                 $currentRoute= $keyutil->removeLastItem($currentRoute);
                 $currentRoute= $keyutil->removeLastItem($currentRoute);
                 $currentRoute=$currentRoute.".menu";
                 $title=$this->translator->trans($currentRoute);
                return $title; 
             }else{

                  $label="no title";
                   $p = explode("|", $keyValue);

                   $type=null;
                   $service=null;
                   $function=null;
                   if(isset($p[0])) {$type=$p[0];}
                   if(isset($p[1])) {$service=$p[1];}
                   if(isset($p[2])) {$function=$p[2];}
                  if($type=='t'){
                         $paramRequest="parentid";
                         $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
                        //dasabled the param parenteid is set on edit and view
                         /* if($sysoperation->isEdit() || $sysoperation->isView()){
                              $paramRequest="id";
                          }*/
                          //$paramValue=$this->container->get('request')->get($paramRequest);
						  $paramValue=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter($paramRequest);
                          $execfuncionservice=$this->container->get('badiu.system.core.lib.util.execfuncionservice');
                        //  echo " $service | $function |$paramValue ";exit;
                          $result=$execfuncionservice->exec($service,$function,$paramValue);
       
                      if($result!=null){
                          if(is_array($result)){$label=$result['name'];} 
                          else{$label=$result; }
                      }
                  }

                return $label; 
             }
    }
        /*
        //delete
  public function getMenu() {
              $value=$this->getForIndex();

             return $value;
             $value=null;
             $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
             if($sysoperation->isEdit() || $sysoperation->isView()){
                   $value=$this->getForEdit();
              }else{
                  $value=$this->getForIndex();
              }
              return $value;
            
             $cbundle=$this->getContainer()->get('badiu.system.core.lib.config.bundle');
             $cbundle->setKeymanger($this->keymanger);
             $keyTitle=$this->getKeymanger()->menuTitle();
             $keyValue=$cbundle->getValue($keyTitle);
             
             if(empty($keyValue)){
                 $currentRoute=$this->container->get('request')->get('_route'); 
                 $currentRoute= $this->keymanger->removeLastItem($currentRoute);
                 $currentRoute= $this->keymanger->removeLastItem($currentRoute);
                 $currentRoute=$currentRoute.".menu";
                 $title=$this->translator->trans($currentRoute);
                return $title; 
             }else{

                  $p=array();
                  $pos=stripos($keyValue, "|");
                  if($pos=== false){
                      return "no title";
                  }else{
                      $p = explode("|", $keyValue);
                  }
                   $service=null;
                   $function=null;
                   if(isset($p[0])) {$service=$p[0];}
                   if(isset($p[1])) {$function=$p[1];}
                   $execfuncionservice=$this->container->get('badiu.system.core.lib.util.execfuncionservice');
                  $result=$service->$function();
                  return $result; 

                 $htmllink=$this->getContainer()->get('badiu.system.core.lib.html.link');
                 $plink=$htmllink->makeParamByConfig($keyValue);
                 
                 $function=$plink['function']; 
                 $service=$plink['service'];
                 $route=$plink['route'];
                 $paramRequest="parentid";
                 $paramRoute="parentid";
                 if($function=='d'){$function="getNameById";}
                 $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
                
                 $paramValue=$this->container->get('request')->get($paramRequest);
                 $label='no_value_label';
                 
                 $execfuncionservice=$this->container->get('badiu.system.core.lib.util.execfuncionservice');
                 $result=$execfuncionservice->exec($service,$function,$paramValue);
                 if($result!=null){
                    if(is_array($result)){$label=$result['name'];} 
                    else{$label=$result; }
                  }

                 
                return $label; 
             }
        }
*/
    
    /*
    //delete
    public function getForEdit() {
                $currentRoute=$this->container->get('request')->get('_route'); 
                 $currentRoute= $this->keymanger->removeLastItem($currentRoute);
                 $currentRoute= $this->keymanger->removeLastItem($currentRoute);
                 $currentRoute=$currentRoute.".menu";
                 $title=$this->translator->trans($currentRoute);
                return $title;
    }*/

  public function getServiceTitle() {
	  
            $currentRoute=$this->container->get('request')->get('_route'); 
            $dkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey');
            if(!empty($dkey)){$currentRoute=$dkey;}
			if(empty($currentRoute)){return null;}
			$key=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($currentRoute);
            $keyservice=null;
			$ckey=$key.'.config';
			if($this->getContainer()->has($ckey)){
				$ckey=$key.'.config';
				 $keyservice=$this->getContainer()->get($ckey);
			}
			if(empty($keyservice)){return null;}
			if(method_exists($keyservice,'getSiteTitle')){
				$tile=$keyservice->getSiteTitle();
				return $tile;
			}
			
            return null;    
        }
	
  public function getContent() {
             $currentRoute=$this->container->get('request')->get('_route'); 
             $title=$this->translator->trans($currentRoute);
             return $title;    
        }

        public function getContainer() {
          return $this->container;
      }

      public function setContainer(Container $container) {
          $this->container = $container;
      }

       public function getTranslator() {
        return $this->translator;
    }

    public function setTranslator(Translator $translator) {
        $this->translator = $translator;
    }

public function getKeymanger() {
          return $this->keymanger;
      }

       public function setKeymanger($keymanger) {
          $this->keymanger = $keymanger;
      }

}
