<?php

namespace Badiu\System\CoreBundle\Model\Page;
use Badiu\System\CoreBundle\Model\Report\Data\Table\BadiuReportDataTable;
use Symfony\Component\Form\FormView; 
interface BadiuPageInterface {
    
    //review deleted
    public function getForm();
    public function setForm(FormView $form);
    
     //review deleted  
    public function getTable();
    public function setTable(BadiuReportDataTable $table);
   
    //review deleted
    public function getAdditionalContents();
    public function setAdditionalContents($additionalContent);
    
     public function getLinks();
     public function setLinks($links);
     
     public function getMessage();
     public function setMessage($message);

    public function getTitle();
    public function setTitle($title);

    public function getModule();
    public function setModule($module);

    public function getOperation();
    public function setOperation($operation);


     
  
}
