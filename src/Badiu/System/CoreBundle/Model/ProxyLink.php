<?php

namespace Badiu\System\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ProxyLink extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
			  
	function exec() {
	}
	
	#/system/proxy/url?type=&shortname=ddc&service=badiu.admin.cms.repository.data&route=badiu.admin.cms.resourcecontentimage.index&_function=send&type=redirectbyshortname
    public function send() { 

			$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
			$type=$this->getUtildata()->getVaueOfArray($param,'type');
			$shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			$route=$this->getUtildata()->getVaueOfArray($param,'route');
			$service=$this->getUtildata()->getVaueOfArray($param,'service');
			$entity=$this->getEntity();
				
			if($type=='redirectbyshortname'){
				 if(empty($shortname)){echo "param shortname required";exit;}
				 if(empty($route)){echo "param route required";exit;}
				 if(empty($service)){echo "param service required";exit;}
					
				 //if ($this->getContainer()->get('request')->get('_route')->getRouteCollection()->get($route) === null) {echo "param route $route is not valid";exit;}
				 if(!$this->getContainer()->has($service)){echo "param $service is not valid";exit;}
				 $dservice=$this->getContainer()->get($service);
				 $serviceid=$dservice->getIdByShortname($entity,$shortname);
				 if(empty($serviceid)){echo "id of service not find";exit;}
				 
				 $lparam=array('parentid'=>$serviceid);
				 $mkey=$this->getUtildata()->getVaueOfArray($param,'_mkey');
				 if(!empty($mkey)){$lparam['_mkey']=$mkey;}
				 $url=$this->getUtilapp()->getUrlByRoute($route,$lparam);
				
				 header('Location: '.$url);
				 exit;
			}
			
			return null;
    }
	
	
}
			  