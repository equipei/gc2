<?php

namespace Badiu\System\CoreBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CalendarFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
              
              
      public function changeParamOnOpen() {
       
          if ($this->isEdit() || $this->isClone()) {
                $param = $this->getParam();
                $value=$this->getParamItem('value');
                $value  = $this->getJson()->decode($value,true);
          
				$param['monthholidaysdays']=$this->getUtildata()->getVaueOfArray($value,'monthholidaysdays');
                $param['yearholidaysdays']=$this->getUtildata()->getVaueOfArray($value,'yearholidaysdays');
                $param['weekworkingdays']=$this->getUtildata()->getVaueOfArray($value,'weekworkingdays');
				$param['monthworkingdays']=$this->getUtildata()->getVaueOfArray($value,'monthworkingdays');
				$param['yearworkingdays']=$this->getUtildata()->getVaueOfArray($value,'yearworkingdays');
				$param['dayworkinghours']=$this->getUtildata()->getVaueOfArray($value,'dayworkinghours');
				$param['monthworkinghours']=$this->getUtildata()->getVaueOfArray($value,'monthworkinghours');
				$param['yearworkinghours']=$this->getUtildata()->getVaueOfArray($value,'yearworkinghours');
                $this->setParam($param);
         }
     
     }      
    public function changeParam() {

        $param = $this->getParam();
        $valueconf=array();

        $valueconf['monthholidaysdays']=$this->getParamItem('monthholidaysdays');
        $valueconf['yearholidaysdays']=$this->getParamItem('yearholidaysdays');
        $valueconf['weekworkingdays']=$this->getParamItem('weekworkingdays');
		$valueconf['monthworkingdays']=$this->getParamItem('monthworkingdays');
		$valueconf['yearworkingdays']=$this->getParamItem('yearworkingdays');
		$valueconf['dayworkinghours']=$this->getParamItem('dayworkinghours');
		$valueconf['monthworkinghours']=$this->getParamItem('monthworkinghours');
		$valueconf['yearworkinghours']=$this->getParamItem('yearworkinghours');
		
        $value  = $this->getJson()->encode($valueconf);

        $param['value']=$value;
      
        unset($param['monthholidaysdays']);
        unset($param['yearholidaysdays']);
        unset($param['weekworkingdays']);
		unset($param['monthworkingdays']);
		unset($param['yearworkingdays']);
		unset($param['dayworkinghours']);
		unset($param['monthworkinghours']);
		unset($param['yearworkinghours']);

        $this->setParam($param);

      
     }
   

    
}
