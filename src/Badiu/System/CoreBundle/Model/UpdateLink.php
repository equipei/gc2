<?php

namespace Badiu\System\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class UpdateLink extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
			   
	function exec() {
		
		//$this->checkSecurity();
		$appservercommand=$this->getContainer()->get('badiu.system.core.lib.appservercommand');
	    $operation=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_operation');
		if($operation=='clearcache'){
			$result=$appservercommand->symfonyCommand('cache:clear');
			$mparam=array();
			$mparam['content']="<div class=\"alert alert-success\" role=\"alert\">$result </div>";
			$msg=$this->showMessage($mparam);
			echo $msg;
			exit;
		}
		
		if($operation=='deletecache'){
			$result=$appservercommand->deleteCache();
			$mparam=array();
			$mparam['content']="<div class=\"alert alert-success\" role=\"alert\">$result </div>";
			$msg=$this->showMessage($mparam);
			echo $msg;
			exit;
		}
		else if($operation=='updatedb'){
			$result=$appservercommand->symfonyCommand('doctrine:schema:update --force'); 
			$mparam=array();
			$mparam['content']="<div class=\"alert alert-success\" role=\"alert\">$result </div>";
			$msg=$this->showMessage($mparam);
			echo $msg;
			exit;
		}else if($operation=='clearcacheupdatedb'){
			$result=$appservercommand->symfonyCommand('cache:clear');
			$result.="<br />";
			$result.=$appservercommand->symfonyCommand('doctrine:schema:update --force'); 
			
			$mparam=array();
			$mparam['content']="<div class=\"alert alert-success\" role=\"alert\">$result </div>";
			$msg=$this->showMessage($mparam);
			echo $msg;
			exit;
		}
		else if($operation=='install' || $operation=='update'){

			$forceclearcache=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_forceclearcache');
			if($forceclearcache){
				$appservercommand->deleteCache();
				$fparam=array('_operation'=>$operation);
				$utilapp =$this->getContainer()->get('badiu.system.core.lib.util.app');
				$url=$utilapp->getUrlByRoute('badiu.system.core.update.link',$fparam);
				header('Location: '.$url);
			}
		
			//echo "<hr >-------update database -------";
			$resultdb=$appservercommand->symfonyCommand('doctrine:schema:update --force'); 
		
			//echo "<hr >-------update modules -------";
			$this->getContainer()->get('badiu.system.module.module.install')->setupsystem();
				
			$entity=$this->addEntity(); 
			$userid=$this->addUser($entity);
			$config=$this->installEntity($entity,$userid);
			
			$this->addSystemVersion();
			$url=$this->getUtilapp()->getUrlByRoute('badiu.system.core.core.frontpage');
		
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			
			if(!$badiuSession->exist()){$badiuSession->delete();}  
			
			$message=$this->getTranslator()->trans('badiu.system.core.install.execsucess');
			$mparam=array();
			if($operation=='install'){
				$message=$this->getTranslator()->trans('badiu.system.core.install.execsucess');
				$mparam['title']=$this->getTranslator()->trans('badiu.system.core.install.title');
			}else {
				$message=$this->getTranslator()->trans('badiu.system.core.update.execsucess');
				$mparam['title']=$this->getTranslator()->trans('badiu.system.core.update.title');
			}
			$labelcontinue=$this->getTranslator()->trans('badiu.system.core.button.labelcontinue');
				
			$content="<div class=\"alert alert-light\" role=\"alert\">$resultdb</div> 
					<div class=\"alert alert-success\" role=\"alert\">$message </div>
				  <a class=\"btn btn-primary\" href=\"$url\" role=\"button\">$labelcontinue</a>";
			$mparam['content']=$content;
			$msg=$this->showMessage($mparam);
			echo $msg;
			exit;	
		}
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		if(!$badiuSession->exist()){
				$url=$this->getUtilapp()->getUrlByRoute('badiu.system.core.core.frontpage');
				header('Location: '.$url); exit;
		}
		
       
		$message=$this->getTranslator()->trans('badiu.system.core.command.withoutcommand');
		$mparam=array();
		$mparam['title']=$this->getTranslator()->trans('badiu.system.core.command.exec');
		$mparam['content']="<div class=\"alert alert-danger\" role=\"alert\">$message </div>";
		$msg=$this->showMessage($mparam);
		echo $msg;exit;
		
			
	
	}
	function requestCommand() {
		$this->checkSecurity();
		$appservercommand=$this->getContainer()->get('badiu.system.core.lib.appservercommand');
		$operation=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_operation');
		$urlgoback=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urlgoback');
		$result="";
		if($operation=='clearcache'){
			$result=$appservercommand->symfonyCommand('cache:clear');
		}
		if($operation=='deletecache'){
			$result=$appservercommand->deleteCache();
		}
		else if($operation=='updatedb'){
			$result=$appservercommand->symfonyCommand('doctrine:schema:update --force'); 
		}else if($operation=='clearcacheupdatedb'){
			$result=$appservercommand->symfonyCommand('cache:clear');
			$result.=$appservercommand->symfonyCommand('doctrine:schema:update --force'); 
		}
		else if($operation=='update'){
			$result=$appservercommand->deleteCache();
			//echo "<hr >-------update database -------";
			$resultdb=$appservercommand->symfonyCommand('doctrine:schema:update --force'); 
		
			//echo "<hr >-------update modules -------";
			$this->getContainer()->get('badiu.system.module.module.install')->setupsystem();
				
			$entity=$this->addEntity(); 
			$userid=$this->addUser($entity);
			$config=$this->installEntity($entity,$userid);
		}
		
		if(!empty($urlgoback)){header('Location: '.urldecode($urlgoback)); exit;}
		if(empty($result)){
			$message=$this->getTranslator()->trans('badiu.system.core.command.withoutcommand');
			$mparam=array();
			$mparam['title']=$this->getTranslator()->trans('badiu.system.core.command.exec');
			$mparam['content']="<div class=\"alert alert-danger\" role=\"alert\">$message </div>";
			$msg=$this->showMessage($mparam);
			echo $msg;exit;
		}
		
        return $result;
	}
	
	 public function addEntity() { 
            $data= $this->getContainer()->get('badiu.system.entity.entity.data');
			$fparam=array('shortname'=>'systembadiunet');
			$entityid=$data->getGlobalColumnValue('id',$fparam);
			if(!empty($entityid)){ return $entityid;} 
			$param=array('name'=>'Plataforma Badiu.Net','dtype'=>'system','shortname'=>'systembadiunet','deleted'=>0,'timecreated'=> new \DateTime());
			$result=$data->insertNativeSql($param, false);
            return $result;
       }
       public function addUser($entity) { 
            $data= $this->getContainer()->get('badiu.system.user.user.data');
			$fparam=array('entity'=>$entity,'username'=>'admin');
			$userid=$data->getGlobalColumnValue('id',$fparam);
			if(!empty($userid)){ return $userid;}
            $email= 'admin@localhost.com';
            $firstname='Admin';
            $lastname= 'System';
            $password= md5('badiu');
            $doctype="EMAIL";
            $param=array('entity'=>$entity,'firstname'=>$firstname,'lastname'=>$lastname,'username'=>'admin','email'=>$email,'auth'=>'manual','password'=>$password,'doctype'=>$doctype,'docnumber'=>$email,'deleted'=>0,'confirmed'=>1,'timecreated'=> new \DateTime());
            $result=$data->insertNativeSql($param, false);
            return $result;
        } 
        public function installEntity($entity,$userid) { 
            $result=0;
            $dinstall= $this->getContainer()->get('badiu.system.module.install');
            $result= $dinstall->setupentity($entity);
            
            $datarole = $this->getContainer()->get('badiu.system.access.role.data');
            $roleadminid=  $datarole->getIdByShortname($entity,'admin');
            $datauseraccess = $this->getContainer()->get('badiu.system.access.user.data');
			
			$fparam=array('entity'=>$entity,'roleid'=>$roleadminid,'userid'=>$userid);
			$countupaccess=$datauseraccess->countGlobalRow($fparam);
			if(!empty($countupaccess)){ return $countupaccess;}
			
            $param=array('entity'=>$entity,'roleid'=>$roleadminid,'userid'=>$userid,'deleted'=>0,'timecreated'=> new \DateTime());
            $result+=$datauseraccess->insertNativeSql($param, false);
            return $result;
        }
        
        public function startSession($entity,$userid) { 
            $name= $this->getParamItem('name');
            $email= $this->getParamItem('email');
             $param=array('userid'=>$userid,'entity'=>$entity,'firstname'=>$name,'lastname'=>'','email'=>$email,'username'=>$email);
             $this->getContainer()->get('badiu.system.access.session')->start($param);
        }
	public function addSystemVersion() { 
            $data= $this->getContainer()->get('badiu.system.module.configglobal.data');
			$varsion=null;
			$dvarsion=null;
           
		   $vparamcheckexist=array('bundlekey'=>'badiu.system.packge','name'=>'badiu.system.packge.version');
		   $vparamadd=array('bundlekey'=>'badiu.system.packge','name'=>'badiu.system.packge.version','value'=>$varsion,'timecreated'=>new \DateTime(),'timemodified'=>new \DateTime());
		   $vparamedit=array('bundlekey'=>'badiu.system.packge','name'=>'badiu.system.packge.version','value'=>$varsion,'timemodified'=>new \DateTime());
           $vresult=$data->addNativeSql($vparamcheckexist,$vparamadd,$vparamedit);
        
		   $vdparamcheckexist=array('bundlekey'=>'badiu.system.packge','name'=>'badiu.system.packge.version');
		   $vdparamadd=array('bundlekey'=>'badiu.system.packge','name'=>'badiu.system.packge.version','value'=>$varsion,'timecreated'=>new \DateTime(),'timemodified'=>new \DateTime());
		   $vdparamedit=array('bundlekey'=>'badiu.system.packge','name'=>'badiu.system.packge.version','value'=>$varsion,'timemodified'=>new \DateTime());
           $vdresult=$data->addNativeSql($vdparamcheckexist,$vdparamadd,$vdparamedit);
        
           return $vdresult;
        } 
		
	public function checkSecurity() { 
			$operation=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_operation');
		   //check is logued
		   $badiuSession = $this->getContainer()->get('badiu.system.access.session');
		   if($badiuSession->exist()){
				$permission=$this->getContainer()->get('badiu.system.access.permission');
				$hasperm=$permission->has_access('badiu.system.core.command.dashboard');
				if(!$hasperm){
					$message=$this->getTranslator()->trans('badiu.system.core.command.withoutpermission',array('%command%'=>$operation));
					$mparam=array();
					$mparam['title']=$this->getTranslator()->trans('badiu.system.core.command.exec');
					$mparam['content']="<div class=\"alert alert-danger\" role=\"alert\">$result </div>";
					$msg=$this->showMessage($mparam);
					echo $msg;exit;
				}
		   }
			$systemcheck = $this->getContainer()->get('badiu.system.core.lib.systemcheck');
			$existusertbl=$systemcheck->exitUserTable();;
			$connected=$systemcheck->isDbConnection();
			$installcompleted=0;
		
			if($connected && $existusertbl){$installcompleted=$systemcheck->isInstallationCompleted();}
			if($connected && !$existusertbl && !$installcompleted){return null;}
			else if($connected && $existusertbl && !$installcompleted){return null;}
			else if($connected && $existusertbl && $installcompleted){
				if(!$badiuSession->exist()){
					$url=$this->getUtilapp()->getUrlByRoute('badiu.system.core.core.frontpage');
					header('Location: '.$url); exit;
				}
				$permission=$this->getContainer()->get('badiu.system.access.permission');
				$hasperm=$permission->has_access('badiu.system.core.command.dashboard');
				if(!$hasperm){
					$message=$this->getTranslator()->trans('badiu.system.core.command.withoutpermission',array('%command%'=>$operation));
					$mparam=array();
					$mparam['title']=$this->getTranslator()->trans('badiu.system.core.command.exec');
					$mparam['content']="<div class=\"alert alert-danger\" role=\"alert\">$message </div>";
					$msg=$this->showMessage($mparam);
					echo $msg;exit;
				}
			
				
		}
	}			
	public function showMessage($param) { 
		$title=$this->getUtildata()->getVaueOfArray($param,'title');
		$type=$this->getUtildata()->getVaueOfArray($param,'type');
		$content=$this->getUtildata()->getVaueOfArray($param,'content');
		$baseresoursepath = $this->getContainer()->get('templating.helper.assets')->getUrl('bundles/badiuthemecore'); 
	$out='
		<html>
		<head>
			<title>'.$title.'</title>
			 <link rel="stylesheet" href="'.$baseresoursepath.'/css/bootstrap.min.css" type="text/css">
		</head>
		
		<body>
		'.$content.'
		</body>
	  </html>
	
	';
	  return $out;
	}	
	 public function acceptTerm() { 
	 $utilapp =$this->getContainer()->get('badiu.system.core.lib.util.app');
			$url=$utilapp->getUrlByRoute('badiu.system.core.update.link',array('_operation'=>'install'));
	 $html='
			<br /><br /><div class="card">
  <div class="card-header">Instalação do Badiu GC2</div>
  <div class="card-body">
    <p class="card-text">
	
	BADIU GC2 - SISTEMA DE GESTÃO CORPORATIVO DE CURSO<br /><br />
Copyright (C) 2022  Lino Vaz Moniz e equipe Badiu (http://badiu.net)<br />
Badiu GC2 é um software livre, você pode distribuí-lo e/ou modificá-lo sob os termos da GNU General Public License como publicado pela Free Software Foundation,<br /> tanto na versão 2 da Licença, ou (a seu critério) qualquer versão posterior.
<br /><br />Veja informações mais  detalhado sobre a licença do Badiu GC2 em:<br />
<a href="http://www.badiu.net/license/gc2" target="_blank">http://www.badiu.net/license/gc2</a>

<br /><br />
Leia os termos especificados no link. Caso aceite, clique no link Continuar, para processar a instalação.

	</p>
    <a href="'.$url.'" class="btn btn-primary">Continuar</a>
  </div>
</div>';

		$mparam=array();
		$mparam['title']=$this->getTranslator()->trans('badiu.system.core.install.title');
				$mparam['content']=$html;
		$msg=$this->showMessage($mparam);
		echo $msg;
		exit;	
	
	 }
}			  