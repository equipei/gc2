<?php

namespace Badiu\System\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class Proxy404Link extends BadiuModelLib{
   
    function __construct(Container $container) {
            parent::__construct($container);
               
            }
    
          
         function exec() {
				$currentroute=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_currentroute');
             	$badiuSession = $this->getContainer()->get('badiu.system.access.session');
				$confredrect=$badiuSession->getValue('badiu.system.core.param.config.site.route.error404configtoredirect');
				$confredrect= $this->getJson()->decode($confredrect, true);
				$routeconfig=$this->getUtildata()->getVaueOfArray($confredrect,'paths.'.$currentroute,true);
				$routekey=$this->getUtildata()->getVaueOfArray($routeconfig,'route');
				$rparam=$this->getUtildata()->getVaueOfArray($routeconfig,'param');
				
				if(empty($rparam)){$rparam=array();}
				if(empty($routekey)){
					 $routekey=$this->getUtildata()->getVaueOfArray($confredrect,'default.route',true);
					 $rparam=$this->getUtildata()->getVaueOfArray($confredrect,'default.param',true);
					 if(empty($rparam)){$rparam=array();}
				}
				
				if(empty($routekey)){
					$routekey=$badiuSession->getValue('badiu.system.core.param.config.site.route.frontpage');
				}
				if(empty($routekey)){
					$routekey='badiu.system.core.param.config.site.route.frontpage';
				}
					
				$url=$this->getUtilapp()->getUrlByRoute($routekey,$rparam);
				
			   header('Location: '.$url);
               exit;
         }
		 
		
}
