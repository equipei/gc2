<?php

namespace Badiu\System\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuSystemCoreBundle:Default:index.html.twig', array('name' => $name));
    }
}
