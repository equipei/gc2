<?php

namespace Badiu\System\CoreBundle\Controller\Functionality;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Badiu\System\CoreBundle\Model\Page\BadiuPage;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger;  


class BadiuWithoutDataBaseController extends BadiuController
{


  function __construct($key=null) { 
            parent::__construct();
  }
  
  public function indexAction(Request $request) {

        //get key of module
        $this->initKeyManger($request);
        
        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
       
   
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionView())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        //translator
        $translator=$this->get('translator');
        
        //page
        $page=$this->get('badiu.system.core.page');
        $page->setOperation($translator->trans($this->get('request')->get('_route')));
         //menu 
        $menu=$this->get($kminherit->moduleMenu());
        $menu->setKeymanger($this->getKeymanger());
        
        //report
        $report=$this->get($kminherit->report());
        $report->setKeymanger($this->getKeymanger());
          
        //link
        $croute=$cbundle->getCrudRoute($this->getKeymanger());
        $report->setRouter($this->get("router"));
       
        $report->addLink('edit',$croute->getEdit());
        $report->addLink('copy',$croute->getCopy());
        $report->addLink('delete',$croute->getDelete());
        $report->addLink('remove',$croute->getRemove());
       
         //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
       //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
        //form config
         $cform=$cbundle->getFormConfig('filter',$this->getKeymanger());
         if($cform->getAction()==''){$cform->setAction($kminherit->routeIndex());}


        //form
        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction());
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
       
     // $filter = $this->get($this->getKeymanger()->filter());
        $filter = array();
        $type=$this->get($kminherit->formFilterType());
        $type->setKeymanger($this->getKeymanger());

        $form = $this->createForm($type,$filter,$formOption);
        $form->add('submit', 'submit', array('label' => $cform->getSubmitlabel(),'attr' => $cform->getSubmitattr()));

         
        $form->handleRequest($request);
        //$filter->setEntity($badiuSession->get()->getEntity());
        $filter =$form->getData();
        $filter['entity']=$badiuSession->get()->getEntity();
        if(!isset($filter['_page'])){$filter['_page']=0;}
         //check form
         $chekForm=$controller->searchCheckForm($form);
         if(!$chekForm){
               $page= new BadiuPage($form->createView());
               return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
         }

         $controller->setDefaultdata($cform->getDefaultdata());
         //change data
         $filter=$controller->setDefaultDataOnSearchForm($filter);
        
       // $filter->set_page($request->query->get('_page'));
        $report=$controller->extractData($filter,$report);
        $format=$request->query->get('_format');
        if(!empty($format))$report->exportTable($format);
        
        // $format=$request->query->get('_format');
       //  if(!empty($format))$report->exportTable($format);
 
  

        $additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

        $links=array(array('url'=>$this->generateUrl($kminherit->routeAdd()),'position'=>'beforeform','type'=>'button','name'=>$translator->trans('addnew')));
        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());


        $page->addData('badiu_form1',$form->createView());
        $page->addData('badiu_form1_group_list',$formGroupList);
        $page->addData('badiu_form1_field_list',$formFieldList);

		//$page->addData('badiu_list_data_row',$report->getDashboardData()['row']);
        //$page->addData('badiu_list_data_rows',$report->getDashboardData()['rows']);

         //$page->setTable($report->getTable());
         $page->addData('badiu_table1',$report->getTable());
         $page->setAdditionalContents($additionalContent);
         $page->setLinks($links);
         
		  $cpage=$cbundle->getPageConfig($this->getKeymanger());
		 $page->setConfig($cpage);
		 
		  $croute=$cbundle->getCrudRoute($this->getKeymanger());
		  $page->setCrudroute($croute);

         return $this->render($badiuSession->get()->getTheme().':Layout:default.html.twig', array('page'=>$page));
 
      
  
    } 
      
public function indexParentAction(Request $request,$parentid) {

        //get key of module
        $this->initKeyManger($request);
        
        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
       
   
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionView())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        //translator
        $translator=$this->get('translator');
        
        //page
        $page=$this->get('badiu.system.core.page');
        $page->setOperation($translator->trans($this->get('request')->get('_route')));
         //menu 
        $menu=$this->get($kminherit->moduleMenu());
        $menu->setKeymanger($this->getKeymanger());
        
        //report
        $report=$this->get($kminherit->report());
        $report->setKeymanger($this->getKeymanger());
          
        //link
        $croute=$cbundle->getCrudRoute($this->getKeymanger());
        $report->setRouter($this->get("router"));
       
        $report->addLink('edit',$croute->getEdit());
        $report->addLink('copy',$croute->getCopy());
        $report->addLink('delete',$croute->getDelete());
        $report->addLink('remove',$croute->getRemove());
       
         //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
       //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
        //form config
         $cform=$cbundle->getFormConfig('filter',$this->getKeymanger());
         if($cform->getAction()==''){$cform->setAction($kminherit->routeIndex());}


        //form
        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction(),array('parentid'=>$parentid));
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
       
     // $filter = $this->get($this->getKeymanger()->filter());
        $filter = array();
        $type=$this->get($kminherit->formFilterType());
        $type->setKeymanger($this->getKeymanger());

        $form = $this->createForm($type,$filter,$formOption);
        $form->add('submit', 'submit', array('label' => $cform->getSubmitlabel(),'attr' => $cform->getSubmitattr()));

         
        $form->handleRequest($request);
        //$filter->setEntity($badiuSession->get()->getEntity());
        $filter =$form->getData();
		$filter['parentid']=$parentid;
        $filter['entity']=$badiuSession->get()->getEntity();
        $filter['_page']=$request->query->get('_page');
        if(!isset($filter['_page'])){$filter['_page']=0;}
		
        $filter['entity']=$badiuSession->get()->getEntity();
        if(!isset($filter['_page'])){$filter['_page']=0;}
         //check form
         $chekForm=$controller->searchCheckForm($form);
         if(!$chekForm){
               $page= new BadiuPage($form->createView());
               return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
         }

         $controller->setDefaultdata($cform->getDefaultdata());
         //change data
         $filter=$controller->setDefaultDataOnSearchForm($filter);
         //$filter=$controller->changeDataOnSearchForm($filter);

       // $filter->set_page($request->query->get('_page'));
        $report=$controller->extractData($filter,$report);
        $format=$request->query->get('_format');
        if(!empty($format))$report->exportTable($format);
        
        // $format=$request->query->get('_format');
       //  if(!empty($format))$report->exportTable($format);
 
  

        $additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

        $links=array(array('url'=>$this->generateUrl($kminherit->routeAdd(),array('parentid'=>$parentid)),'position'=>'beforeform','type'=>'button','name'=>$translator->trans('addnew')));
        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());


        $page->addData('badiu_form1',$form->createView());
        $page->addData('badiu_form1_group_list',$formGroupList);
        $page->addData('badiu_form1_field_list',$formFieldList);

		//$page->addData('badiu_list_data_row',$report->getDashboardData()['row']);
        //$page->addData('badiu_list_data_rows',$report->getDashboardData()['rows']);

         //$page->setTable($report->getTable());
         $page->addData('badiu_table1',$report->getTable());
         $page->setAdditionalContents($additionalContent);
         $page->setLinks($links);
         
		  $cpage=$cbundle->getPageConfig($this->getKeymanger());
		 $page->setConfig($cpage);
		 
		  $croute=$cbundle->getCrudRoute($this->getKeymanger());
		  $page->setCrudroute($croute);
		  
         return $this->render($badiuSession->get()->getTheme().':Layout:default.html.twig', array('page'=>$page));
 
      


    }
   

    public function addAction(Request $request)
    {
       //get key of module
        $this->initKeyManger($request);
        
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionAdd())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        //translator
        $translator=$this->get('translator');
       
       //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');

       //page 
        $page=$this->get('badiu.system.core.page');
        $page->setOperation($translator->trans($this->get('request')->get('_route')));
         //menu 
        $menu=$this->get($kminherit->moduleMenu());
        $menu->setKeymanger($this->getKeymanger());
        
        //data
        #$data = $this->get($kminherit->data());
        

        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
        //form config
        $cform=$cbundle->getFormConfig('data',$this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($kminherit->routeAdd());}

        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction());
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
      
        #$dto = $this->get($kminherit->entity());
        $dto =  array();
        $type=$this->get($kminherit->formFilterType());
        //$type->setKeymanger($this->getKeymanger());
        $form = $this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans($cform->getSubmitlabel()),'attr' => $cform->getSubmitattr()));
      
         
        $form->handleRequest($request); 
        

        if ($form->isValid()) {
          
             $data =$form->getData();  
            //start default data
            
            $controller->setDatalayout($cform->getDatalayout());
             $dto=$controller->setDefaultDataOnSearchForm($dto); //review
            //$data=$controller->changeEntityOnAddForm($data,$dto) ;

            //check form
            $controller->setDatalayout($cform->getDatalayout());
            $chekForm=$controller->addCheckForm($form,$data);
            
            if(!$chekForm){
               $page= new BadiuPage($form->createView());
               return $this->render($badiuSession->get()->getTheme().':Layout:form.html.twig', array('page' =>$page));
            }

            $dto= $controller->save($data);

            //go to nex page
            $url=$controller->nextUrlAfterSave();
            if(empty($url)){$url=$this->redirect($this->generateUrl($kminherit->routeIndex()));}
            return $url;
         }
        $additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
       
     /*  print_r($formGroupList);
echo "-------------------------------<br>";
print_r($formFieldList);*/

        $page->addData('badiu_form1',$form->createView());
        $page->addData('badiu_form1_group_list',$formGroupList);
        $page->addData('badiu_form1_field_list',$formFieldList);
        // print_r($page->getData());
        //form filerole
        $filerole=$cform->getFilerole();
        if(!empty($filerole)){
            $formfrole=array();
            array_push($formfrole,array('position'=>'beforeform','file'=>$cform->getFilerole()));
            array_push($additionalContent,$formfrole);
        }


          $page->setForm($form->createView());
          $page->setAdditionalContents($additionalContent);
         
		 
        return $this->render($badiuSession->get()->getTheme().':Layout:form.html.twig', array('page' =>$page));
  
    }

public function editAction(Request $request,$id)
    {
       //get key of module
        $this->initKeyManger($request);
        
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionEdit())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }

        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        //translator
        $translator=$this->get('translator');
       
        //page 
        $page=$this->get('badiu.system.core.page');
        $page->setOperation($translator->trans($this->get('request')->get('_route')));

         //menu 
        $menu=$this->get($kminherit->moduleMenu());
        $menu->setKeymanger($this->getKeymanger());
      
        //data
       // $data =  $this->get($kminherit->data());

        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
        //form config
        $cform=$cbundle->getFormConfig('edit',$this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($kminherit->routeEdit());}


        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction(),array('id'=>$id));
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
       // $dto =$data->findById($id);
        $parentid=null;
        $dto=array();
         //parent
        $cparent=$cbundle->getEntityParent($this->getKeymanger());
         $functionFindParent=$cparent->getFunctionFind();
        if(!empty($functionFindParent)){
                $pservice=$this->get($cparent->getService());
                $parentid = $pservice->$functionFindParent($dto);
        }

       // if(method_exists ($dto,'findParent')) {$parentid = $dto->findParent();}
        
        $dto=$controller->setDefaultDataOnOpenEditForm($dto);
        $type=$this->get($kminherit->formType());
        $type->setKeymanger($this->getKeymanger());
        $form =$this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans($cform->getSubmitlabel()),'attr' => $cform->getSubmitattr()));
           
        $form->handleRequest($request); 
        
        if ($form->isValid()) {
            $dto=$form->getData();
             $data=$controller->changeDataOnEditForm($dto);

            //checkform
            $controller->setDatalayout($cform->getDatalayout());
            $chekForm=$controller->editCheckForm($form,$dto);

            if(!$chekForm){
               $page= new BadiuPage($form->createView());
                return $this->render($badiuSession->get()->getTheme().':Layout:form.html.twig', array('page' =>$page));
            }

            $dto= $controller->save($dto);

            if(!empty($parentid)){
                return $this->redirect($this->generateUrl($kminherit->routeIndex(),array('parentid'=>$parentid)));
            }else {
                return $this->redirect($this->generateUrl($kminherit->routeIndex()));
            }

         }
          $menu->setParentid($parentid);

        $additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

         $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
     

        $page->addData('badiu_form1',$form->createView());
        $page->addData('badiu_form1_group_list',$formGroupList);
        $page->addData('badiu_form1_field_list',$formFieldList);
        //   $page->setForm($form->createView());
          $page->setAdditionalContents($additionalContent);
          
        return $this->render($badiuSession->get()->getTheme().':Layout:form.html.twig', array('page' =>$page));
  
    }
    

}
