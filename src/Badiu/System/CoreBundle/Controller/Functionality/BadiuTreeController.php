<?php

namespace Badiu\System\CoreBundle\Controller\Functionality;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Badiu\System\CoreBundle\Model\Page\BadiuPage;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger;  


class BadiuTreeController extends BadiuController 
{
    
    private $keymanger;
  function __construct($key=null) { 
              $this->keymanger=new BadiuKeyManger();
              if($key!=null){$this->keymanger->setBaseKey($key);}
           
      }
     
      
     public function initKeyManger(Request $request){
          $key=$request->attributes->get('_route');
           $key=$this->keymanger->removeLastItem($key);
           $this->keymanger->setBaseKey($key);
      }
    
    
    
   
    public function addAction(Request $request)
    {
       //get key of module
        $this->initKeyManger($request);
        
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionAdd())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());
        
        //translator
        $translator=$this->get('translator');
       
        //data
        $data = $this->get($this->getKeymanger()->data());
          
      
        $formOption=array();
        $formOption['action']=$this->generateUrl($this->getKeymanger()->routeAdd());
        $formOption['method']='POST';
        $formOption['label']=$translator->trans('add.channel');
        $formOption['attr']=array( 'role'=>'form' );
      
       // $dto = new SupportTicketChannel();
         $dto = $this->get($this->getKeymanger()->entity());
        $type=$this->get($this->getKeymanger()->formType());
        $form = $this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans('add'),'attr' => array('class' => 'btn btn-default')));
        
         
        $form->handleRequest($request); 
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $dto->setDeleted(false);
            $dto->setEntity($badiuSession->get()->getEntity());
            $dto->setTimecreated(new \Datetime());
            $dto->setDeleted(FALSE);
            $data->setDto($dto);
            
            //check duplication
            $error=false;
            if($data->existAddIdnumber()){
                $form->get('idnumber')->addError(new FormError($translator->trans('duplication.record.name')));
                 $error=true;
            }
            if($data->existAdd()){
                $form->get('name')->addError(new FormError($translator->trans('duplication.record.name')));
                 $error=true;
            }
            if($error){
               $page= new BadiuPage($form->createView());
               return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
            }
            $data->save();
            return $this->redirect($this->generateUrl($this->getKeymanger()->routeIndex()));
         }
       $page= new BadiuPage($form->createView());
        return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
  
    }

    
    public function editAction(Request $request,$id)
    {
       //get key of module
        $this->initKeyManger($request);
        
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionEdit())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());
        
        //translator
        $translator=$this->get('translator');
       
        //data
        $data =  $this->get($this->getKeymanger()->data());
          
      
        $formOption=array();
        $formOption['action']=$this->generateUrl($this->getKeymanger()->routeEdit(),array('id'=>$id));
        $formOption['method']='POST';
        $formOption['label']=$translator->trans('edit.db');
        $formOption['attr']=array( 'role'=>'form' );
      
        $dto =$data->findById($id);
        $type=$this->get($this->getKeymanger()->formType());
        $form =$this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans('edit'),'attr' => array('class' => 'btn btn-default')));
           
        $form->handleRequest($request); 
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $dto->setDeleted(FALSE);
            $dto->setTimemodified(new \Datetime());
            $data->setDto($dto);
            
            //check duplication
            $error=false;
            if($data->existEdit()){
                $form->get('name')->addError(new FormError($translator->trans('duplication.record.name')));
               $error=true;
            }
             if($data->existEdit()){
                $form->get('name')->addError(new FormError($translator->trans('duplication.record.name')));
               $error=true;
            }
            if($data->existEditIdnumber()){
                $form->get('idnumber')->addError(new FormError($translator->trans('duplication.record.name')));
               $error=true;
            }
            if($error){
               $page= new BadiuPage($form->createView());
                return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
            }
            $data->save();
            return $this->redirect($this->generateUrl($this->getKeymanger()->routeIndex()));
         }
       $page= new BadiuPage($form->createView());
        return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
  
    }
    

    public function copyAction(Request $request,$id)
    {
        
        //get key of module
        $this->initKeyManger($request);
       
         //get session data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionAdd())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());
        
        //translator
        $translator=$this->get('translator');
       
        //data
        $data =  $this->get($this->getKeymanger()->data());
          
      
        $formOption=array();
        $formOption['action']=$this->generateUrl($this->getKeymanger()->routeAdd());
        $formOption['method']='POST';
        $formOption['label']=$translator->trans('add.db');
        $formOption['attr']=array( 'role'=>'form' );
      
        $dto =$data->findById($id);
         $type=$this->get($this->getKeymanger()->formType());
        $form = $this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans('add'),'attr' => array('class' => 'btn btn-default')));
        
         
        $form->handleRequest($request); 
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $dto->setDeleted(FALSE);
            $dto->setTimecreated(new \Datetime());
            $data->setDto($dto);
            
            //check duplication
            if($data->existEdit()){
                $form->get('name')->addError(new FormError($translator->trans('duplication.record.name')));
                $page= new BadiuPage($form->createView());
                return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
            }
            $data->save();
            return $this->redirect($this->generateUrl($this->getKeymanger()->routeIndex()));
         }
        $additionalContent=array();
        $page= new BadiuPage($form->createView());
        return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
  
    }

    public function deleteAction(Request $request,$id)
    {
       //get key of module
        $this->initKeyManger($request);
        
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionDelete())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());
        
        //data
        $data =  $this->get($this->getKeymanger()->data());
        
        $data->delete($id);
        return $this->redirect($this->generateUrl($this->getKeymanger()->routeIndex()));
        
        
    }
    

    public function removeAction(Request $request,$id)
    {
        
        //get key of module
        $this->initKeyManger($request);
       
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionRemove())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }
        
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());
        
        //data
        $data =  $this->get($this->getKeymanger()->data());
        
        $data->remove($id);
        return $this->redirect($this->generateUrl($this->getKeymanger()->routeIndex()));
        
        
    }
    public function getKeymanger() {
        return $this->keymanger;
    }

    public function setKeymanger(BadiuKeyManger $keymanger) {
        $this->keymanger = $keymanger;
    }



}
