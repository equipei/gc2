<?php

namespace Badiu\System\CoreBundle\Controller\Functionality;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Badiu\System\CoreBundle\Model\Page\BadiuPage;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger;  


class BadiuParentController extends Controller
{
    
    private $keymanger;
  function __construct($key=null) { 
              $this->keymanger=new BadiuKeyManger();
              if($key!=null){$this->keymanger->setBaseKey($key);}
           
      }
     
      
     public function initKeyManger(Request $request){
          $key=$request->attributes->get('_route');
           $key=$this->keymanger->removeLastItem($key);
           $this->keymanger->setBaseKey($key);
      }
    
    public function indexAction(Request $request,$parentid) {
       
        //get key of module
        $this->initKeyManger($request);
        
        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
       
   
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionView())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }
        
        //translator
        $translator=$this->get('translator');
        
        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');

         //page
        $page=$this->get('badiu.system.core.page');
        
         //menu 
        $menu=$this->get($this->getKeymanger()->moduleMenu());
        
        //report
        $report=$this->get($this->getKeymanger()->report());
        
          
        //link
        $croute=$cbundle->getCrudRoute($this->getKeymanger());
        $report->setRouter($this->get("router"));
       
        $report->addLink('edit',$croute->getEdit());
        $report->addLink('copy',$croute->getCopy());
        $report->addLink('delete',$croute->getDelete());
        $report->addLink('remove',$croute->getRemove());
        
         //controller
        if ($this->container->has($this->getKeymanger()->controller())){
            $controller=$this->get($this->getKeymanger()->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
       //controller
        if ($this->container->has($this->getKeymanger()->controller())){
            $controller=$this->get($this->getKeymanger()->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
        //form config
         $cform=$cbundle->getFormConfig('data',$this->getKeymanger());
         if($cform->getAction()==''){$cform->setAction($this->getKeymanger()->routeIndex());}


        //entity parent
        $pentity=$cbundle->getEntityParent($this->getKeymanger());
        print_r($pentity);
        //form
        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction(),array('parentid'=>$parentid));
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
       
        $filter = $this->get($this->getKeymanger()->filter());
        $type=$this->get($this->getKeymanger()->formFilterType());
        $form = $this->createForm($type,$filter,$formOption);
        $form->add('submit', 'submit', array('label' => $cform->getSubmitlabel(),'attr' => $cform->getSubmitattr()));
        
         
        $form->handleRequest($request); 
        
         //check form
         $chekForm=$controller->searchCheckForm($form);
         if(!$chekForm){
               $page= new BadiuPage($form->createView());
               return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
         }
        $filter->addParent($parentid);
        $report->extractData($filter);
        $report->makeTable();
        
         $format=$request->query->get('format');
         if(!empty($format))$report->exportTable($format);
    
         
         $additionalContent=$menu->get();
         $links=array(array('url'=>$this->generateUrl($this->getKeymanger()->routeAdd(),array('parentid'=>$parentid)),'position'=>'beforeform','type'=>'button','name'=>$translator->trans('addnew')));
         //$page= new BadiuPage($form->createView(),$report->getTable(),$additionalContent,$links);
         $page->setForm($form->createView());
         $page->setTable($report->getTable());
         $page->setAdditionalContents($additionalContent);
         $page->setLinks($links);
         return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page'=>$page));
 
      
  
    }
    
   
    public function addAction(Request $request,$parentid)
    {


       //get key of module
        $this->initKeyManger($request);
        
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionAdd())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }


        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');

        //parent
        $cparent=$cbundle->getEntityParent($this->getKeymanger());



        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());
        
        //translator
        $translator=$this->get('translator');
       



       //page 
        $page=$this->get('badiu.system.core.page');
      
         //menu 
        $menu=$this->get($this->getKeymanger()->moduleMenu());
        
        //data
        $data = $this->get($this->getKeymanger()->data());
        

        //controller
        if ($this->container->has($this->getKeymanger()->controller())){
            $controller=$this->get($this->getKeymanger()->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
        //form config
        $cform=$cbundle->getFormConfig('data',$this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($this->getKeymanger()->routeAdd());}

        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction());
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
      
        $dto = $this->get($this->getKeymanger()->entity());
        $type=$this->get($this->getKeymanger()->formType());
        $form = $this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans($cform->getSubmitlabel()),'attr' => $cform->getSubmitattr()));
      
         
        $form->handleRequest($request); 
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $dto->setDeleted(false);
            $dto->setEntity($badiuSession->get()->getEntity());
            $dto->setTimecreated(new \Datetime());
            $dto->setDeleted(FALSE);
            $data->setDto($dto);
            
            //check form
            $chekForm=$controller->addCheckForm($form,$data);
            
            if(!$chekForm){
               $page= new BadiuPage($form->createView());
               return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
            }
            $data->save();
            return $this->redirect($this->generateUrl($this->getKeymanger()->routeIndex()));
         }
          $additionalContent=$menu->get();
          $page->setForm($form->createView());
          $page->setAdditionalContents($additionalContent);
         
        return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
  
    }

    
    public function editAction(Request $request,$id)
    {
       //get key of module
        $this->initKeyManger($request);
        
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionEdit())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }

        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());
        
        //translator
        $translator=$this->get('translator');
       
        //page 
        $page=$this->get('badiu.system.core.page');

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');

         //menu 
        $menu=$this->get($this->getKeymanger()->moduleMenu());
      
        //data
        $data =  $this->get($this->getKeymanger()->data());

        //controller
        if ($this->container->has($this->getKeymanger()->controller())){
            $controller=$this->get($this->getKeymanger()->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
        //form config
        $cform=$cbundle->getFormConfig('data',$this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($this->getKeymanger()->routeEdit());}

        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction(),array('id'=>$id));
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
        $dto =$data->findById($id);
        $type=$this->get($this->getKeymanger()->formType());
        $form =$this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans($cform->getSubmitlabel()),'attr' => $cform->getSubmitattr()));
           
        $form->handleRequest($request); 
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $dto->setDeleted(FALSE);
            $dto->setTimemodified(new \Datetime());
            $data->setDto($dto);
            
            //checkform
            $chekForm=$controller->editCheckForm($form,$data);

            if(!$chekForm){
               $page= new BadiuPage($form->createView());
                return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
            }
            $data->save();
            return $this->redirect($this->generateUrl($this->getKeymanger()->routeIndex()));
         }
          $additionalContent=$menu->get();
           $page->setForm($form->createView());
          $page->setAdditionalContents($additionalContent);
          
        return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
  
    }
    

    public function copyAction(Request $request,$id)
    {
        
        //get key of module
        $this->initKeyManger($request);
       
         //get session data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionAdd())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());
        
        //translator
        $translator=$this->get('translator');
       
        //page 
        $page=$this->get('badiu.system.core.page');
      
         //menu 
        $menu=$this->get($this->getKeymanger()->moduleMenu());

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');

        //data
        $data =  $this->get($this->getKeymanger()->data());

        //controller
        if ($this->container->has($this->getKeymanger()->controller())){
            $controller=$this->get($this->getKeymanger()->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
        //form config
        $cform=$cbundle->getFormConfig('data',$this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($this->getKeymanger()->routeEdit());}


        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction());
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
        $dto =$data->findById($id);
         $type=$this->get($this->getKeymanger()->formType());
        $form = $this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans('add'),'attr' => array('class' => 'btn btn-default')));
        
         
        $form->handleRequest($request); 
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $dto->setDeleted(FALSE);
            $dto->setTimecreated(new \Datetime());
            $data->setDto($dto);

            //checkform
            $chekForm=$controller->addCheckForm($form,$data);

            if(!$chekForm){
                $page= new BadiuPage($form->createView());
                return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
            }
            $data->save();
            return $this->redirect($this->generateUrl($this->getKeymanger()->routeIndex()));
         }
           
           $additionalContent=$menu->get();
           $page->setForm($form->createView());
          $page->setAdditionalContents($additionalContent);
         
        return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
  
    }

    public function deleteAction(Request $request,$id)
    {
       //get key of module
        $this->initKeyManger($request);
        
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionDelete())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());
        
        //data
        $data =  $this->get($this->getKeymanger()->data());
        
        $data->delete($id);
        return $this->redirect($this->generateUrl($this->getKeymanger()->routeIndex()));
        
        
    }
    

    public function removeAction(Request $request,$id)
    {
        
        //get key of module
        $this->initKeyManger($request);
       
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionRemove())){
             return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }
        
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());
        
        //data
        $data =  $this->get($this->getKeymanger()->data());
        
        $data->remove($id);
        return $this->redirect($this->generateUrl($this->getKeymanger()->routeIndex()));
        
        
    }
    public function getKeymanger() {
        return $this->keymanger;
    }

    public function setKeymanger(BadiuKeyManger $keymanger) {
        $this->keymanger = $keymanger;
    }



}
