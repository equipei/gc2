<?php

namespace Badiu\System\CoreBundle\Controller\Functionality;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Badiu\System\CoreBundle\Model\Page\BadiuPage;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger;  
use  Badiu\System\CoreBundle\Model\Lib\Config\KeyUtil;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
class BadiuController extends Controller
{

    /**
     * @var BadiuKeyManger
     */
    private $keymanger;
	
	    /**
		* for situation of dynamic key
     * @var BadiuKeyManger
     */
    private $keymangerfilter;
    /**
     * @var Container
     */
    private $keymangerExtend;

    private $datalayout;
    private $logtimestart;
    private $log;
    private $currentkeyroute;
	private $currentkeyroutefilter;
    private $dynamicroute=false;
     /**
     * @var object
     */
    private $utildata = null;
	private $systemdata;
  function __construct($key=null) { 
              $this->keymanger=new BadiuKeyManger();
			  $this->keymangerfilter=null;
              if($key!=null){$this->keymanger->setBaseKey($key);}
             
		   }
     
 public function iniSystemdata(){
		$badiuSession=$this->get('badiu.system.access.session');
        $systemdata=$this->get('badiu.system.core.functionality.systemdata');
		$systemdata->setEntity($badiuSession->get()->getEntity());
		$systemdata->setLang($badiuSession->get()->getLang());
		$systemdata->init();
		$this->setSystemdata($systemdata);
		
 }	
      public function initDatetimezone(){
			$datetimezone=$this->get('badiu.system.access.session')->getValue('badiu.system.core.core.param.config.datetimezone');
			if(!empty($datetimezone)){date_default_timezone_set($datetimezone);}
			else {date_default_timezone_set('America/Sao_Paulo');}  
			
			setlocale(LC_ALL, 'pt_BR.utf-8', 'ptb', 'pt_BR', 'portuguese-brazil', 'portuguese-brazilian', 'bra', 'brazil', 'br');
			setlocale(LC_TIME, 'pt_BR.utf-8', 'ptb', 'pt_BR', 'portuguese-brazil', 'portuguese-brazilian', 'bra', 'brazil', 'br');

	  }   
     public function initKeyManger(Request $request){
			$this->initDatetimezone();
			 $this->utildata = $this->get('badiu.system.core.lib.util.data');  	
            $currentroute=$request->attributes->get('_route');
            $baseKeydyncoriginal=$request->attributes->get('_route');
            $this->dynamicroute=false;
              
           if($currentroute=='badiu.system.core.report.dynamic.index'){
                $this->setCurrentkeyroute($this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey'));
                $this->dynamicroute=true;
           }else if($currentroute=='badiu.system.core.report.dynamicp.index'){
                $this->setCurrentkeyroute($this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey'));
                $this->dynamicroute=true;
            }else if($currentroute=='badiu.system.core.report.dynamicd.dashboard'){
                $this->setCurrentkeyroute($this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey'));
                $this->dynamicroute=true;
            }else if($currentroute=='badiu.system.core.report.dynamicdp.dashboard'){
                $this->setCurrentkeyroute($this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey'));
                $this->dynamicroute=true;
            }
			else if($currentroute=='badiu.system.core.data.dynamic.add' || $currentroute=='badiu.system.core.data.dynamic.edit' || $currentroute=='badiu.system.core.data.dynamic.copy'){
                $this->setCurrentkeyroute($this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey'));
                $this->dynamicroute=true;
            }else if($currentroute=='badiu.system.core.data.dynamicp.add'  || $currentroute=='badiu.system.core.data.dynamicp.edit' || $currentroute=='badiu.system.core.data.dynamicp.copy'){
                $this->setCurrentkeyroute($this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_dkey'));
                $this->dynamicroute=true;
			}
            else{
                $this->setCurrentkeyroute($request->attributes->get('_route'));
                $this->dynamicroute=false;
           }
		   $changecurrentroute=null; 
		   $this->currentkeyroutefilter=$this->getCurrentkeyroute(); 
           if($this->dynamicroute){ 
				$nr=$this->initFreportChangeRoute();
				if(!empty($nr)){$changecurrentroute=$nr;}
		   }
			
           $key=$this->getCurrentkeyroute(); 
		  $keyutil=new KeyUtil();
          $key=$keyutil->removeLastItem($key) ;
          $this->keymanger->setBaseKey($key);
		  $this->keymangerfilter=$this->keymanger;
		  
		   if($this->dynamicroute){
                $baseKeydyncoriginal=$keyutil->removeLastItem($baseKeydyncoriginal) ;
                $this->keymanger->setBaseKeydyncoriginal($baseKeydyncoriginal);
           }
		   
           if($this->dynamicroute && !empty( $changecurrentroute)){
			   $this->currentkeyroutefilter=$changecurrentroute; 
			    $baseKeydyncoriginal=$keyutil->removeLastItem($baseKeydyncoriginal) ;
                $this->keymangerfilter->setBaseKeydyncoriginal($baseKeydyncoriginal);
				
           }
          
      } 
    
	public function forceLogin(Request $request){
		 $currentroute=$request->attributes->get('_route');
		 $isroutloginpage = FALSE;
		 if ($currentroute == "badiu.auth.core.login.add") {$isroutloginpage = TRUE;}
		 $permission=$this->get('badiu.system.access.permission');
		 $badiuSession = $this->get('badiu.system.access.session');
		 $badiuSession->setSystemdata($this->getSystemdata());
		 $hasperm=$permission->has_access($currentroute);
		 
		  if (!$badiuSession->exist() && !$isroutloginpage && !$hasperm) {
			  $url =$this->get('badiu.system.core.lib.util.app')->getUrlByRoute("badiu.auth.core.login.add");
			  header('Location: '.$url);
			  exit;
		  }
	}
       public function cronAction(Request $request) {
		$this->initDatetimezone();
        //get key of module
        #$this->initKeyManger($request);
        
        //translator
        #$translator=$this->get('translator');
        
        
        //get session data
     //   $badiuSession=$this->get('badiu.system.access.session');

       
   
        //permission review
       /* $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
             return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=> $page));
        }*/

        //bundle config
       # $cbundle=$this->get('badiu.system.core.lib.config.bundle');
       # $cbundle->initKeymangerExtend($this->getKeymanger());
       # $this->setKeymangerExtend($cbundle->getKeymangerExtend());
        
        
        //service of key maneger that do inherit key of service
        #$kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        #$kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        
       //cron
        $result=null;
       #$cronkey=$kminherit->cron();
       $cronkey='badiu.system.scheduler.task.cron'; //added
       if($this->has($cronkey)){
            $cron=$this->get($cronkey);
            if(method_exists($cron, 'exec')){$result=$cron->exec();}
        }
      
        $response = new Response('');
        return $response;
      
 
      
  
    } 
    
    public function indexAction(Request $request) {
        $this->iniSystemdata();
		$this->forceLogin($request);
        //get key of module
		
        $this->initKeyManger($request);
        $this->logtimestart=new \DateTime();
        //translator
        $translator=$this->get('translator');
        $translator->setSystemdata($this->getSystemdata());
        //page
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setKey($this->getCurrentkeyroute());
        $page->setLayout('report');
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
        
        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
		$badiuSession->setSystemdata($this->getSystemdata());
        
		
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
             return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=> $page));
        }
		$this->initFreportDynamic();
        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		 $cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());
        
        
        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        //
        
        
         //menu 
         $keymenu=$kminherit->moduleMenu();
         if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
         $menu=$this->get($keymenu);
		 $menu->setSystemdata($this->getSystemdata());
		 
        $menu->setKeymanger($this->getKeymanger());
        
         $additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);
       
        //get typeview
        $typeview='default';
        $functionality=$cbundle->getValueQueryString($this->getKeymanger()->functionality(),$cbundle->getKeymangerExtend()->functionality());
        if(isset($functionality['typeview'])){$typeview=$functionality['typeview'];}
 
        if($typeview=='richjs'){
             return $this->render($badiuSession->get()->getTheme().':Layout:defaultjs.html.twig', array('page'=>$page));
        }
        
        
        //report
        $report=$this->get($kminherit->report());
		$report->setSystemdata($this->getSystemdata());
        $report->setKeymanger($this->getKeymanger());
          
        //link
        $croute=$cbundle->getCrudRoute($this->getKeymanger());
        $page->setCrudroute($croute);
        $report->setRouter($this->get("router")); //review 
        
        //review
        $report->addLink('edit',$croute->getEdit());
        $report->addLink('copy',$croute->getCopy());
        $report->addLink('delete',$croute->getDelete());
        $report->addLink('remove',$croute->getRemove());
        
         //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
       //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
        //form config

         $cform=$cbundle->getFormConfig('filter',$this->getKeymanger());
         $cpage=$cbundle->getPageConfig($this->getKeymanger());
         if($cform->getAction()==''){$cform->setAction($kminherit->routeIndex());}

          $controller->setKeymanger($kminherit);

        //form
     /*   $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction());
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();*/
      
       

        $filter = array();
	$filter=$this->get('badiu.system.core.lib.http.querystringsystem')->updateParam($filter);

		
        //review form filter $type=$this->get($kminherit->formFilterType());
       //review form filter $type->setKeymanger($this->getKeymanger());

	$filter=$this->get('badiu.system.core.lib.http.querystringsystem')->updateParam($filter);
		
	
        $filter['entity']=$badiuSession->get()->getEntity();
        if(!isset($filter['_page'])){$filter['_page']=0;}
         $filter['_format']=$request->query->get('_format');
         $controller->setDatalayout($cform->getDatalayout());
       
         $report->setDefaultdatafilterconfig($cform->getDefaultdata()); 
         $report->setDefaultdatafilterconfigonopenform($cform->getDefaultdataonpenform()); 
         $report->setFconfig($cform);
         $filter['_page']=$request->query->get('_page');
        
          //$report->extractData($filter);
          //  $report->makeTable();
        // print_r($filter);exit; 
         $filter=$controller->serviceExecBeforeSubmit($filter,$cform->getServiceexecbeforesubmit());
         $report=$controller->extractData($filter,$report);
         $report=$controller->serviceExecAfterSubmit($report,$cform->getServiceexecaftersubmit());
         $format=$request->query->get('_format');
         if(!empty($format)){
			if($format=='json'){
				$json =json_encode($report->getListData());
				$response = new Response($json);
				$response->headers->set('Content-Type', 'application/json');
				return $response;
			}
			$report->exportTable($format);
			
		}



        
             $filerole=$cform->getFilerole();
        if(!empty($filerole)){
            $formfrole=array();
            foreach ($filerole as $frole) {
                 array_push($formfrole,array('position'=>'pagefooter','file'=>$frole));
            }
            array_push($additionalContent,$formfrole);
           
        }
        
           $filebeforereport=$cform->getFilebeforereport();
          
        if(!empty($filebeforereport)){
            $fbeforereport=array();
            foreach ($filebeforereport as $freport) {
                 array_push($fbeforereport,array('position'=>'beforereport','file'=>$freport));
            }
            array_push($additionalContent,$fbeforereport);
           
        }
        
        $fileafterreport=$cform->getFileafterreport();
        if(!empty($fileafterreport)){
            $fafterreport=array();
            foreach ($fileafterreport as $freport) {
                 array_push($fafterreport,array('position'=>'afterreport','file'=>$freport));
            }
            array_push($additionalContent,$fafterreport);
           
        }
          $filereport=$cform->getFilereport();
         $freport=array();
         array_push($freport,array('position'=>'afterreport','file'=>$filereport));
          array_push($additionalContent, $freport);
          
        $page->setAdditionalContents($additionalContent);
        $page->setThemelayoutblockhidden($cform->getThemelayoutblockhidden());//delete

        $links=array();
        $lroutes=$this->get('router')->getRouteCollection()->all();
        if (array_key_exists($kminherit->routeAdd(),$lroutes)){
            $links=array(array('url'=>$this->generateUrl($kminherit->routeAdd()),'position'=>'beforeform','type'=>'button','name'=>$translator->trans('addnew')));
          }
	foreach ($cform->getLinkimenu() as $imneu) {
            $imroute=$imneu['route'];
            $imlabel=$imneu['label'];
            $modal=null;
            if(isset($imneu['modal'])){ $modal=$imneu['modal'];}
            $limenu=array('url'=>$this->generateUrl($imroute),'position'=>'beforeform','type'=>'button','name'=>$translator->trans($imlabel),'modal'=>$modal);
            array_push($links,$limenu);
	}
      

         
        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());

        $dasheboardsqlsinlgleconfig=$cbundle->getValueQueryString($this->getKeymanger()->dashboardDbSqlSingleConfig(),$this->getKeymangerExtend()->dashboardDbSqlSingleConfig());
        $dasheboardsqlconfig=$cbundle->getValueQueryString($this->getKeymanger()->dashboardDbSqlConfig(),$this->getKeymangerExtend()->dashboardDbSqlConfig());

        //schedule config
      /*  $schedulekeymanger=$this->get('badiu.system.core.functionality.keymanger');
        $schedulekeymanger->setBaseKey('badiu.system.scheduler.tasksystem.add');
        $schedulecbundle=$this->get('badiu.system.core.lib.config.bundle');
        $schedulecbundle->setKeymanger($schedulekeymanger);
        $schedulecbundle->initKeymangerExtend($schedulekeymanger);
        $schedulecform=$schedulecbundle->getFormConfig('data',$schedulekeymanger);
        $schedulefc=$this->get('badiu.system.core.lib.form.factoryconfig');
        $schedulefc->setDefaultparam($schedulecform->getDefaultdataonpenform());
        $schedulefc->setType('data');
        $page->addData('badiu_scheduleformconfig',$schedulefc->get('badiu.system.scheduler.tasksystem.add'));
        */
	 $page->addData('badiu_form1_data',$filter);
         
         //form config
         $tkey=$this->getCurrentkeyroute();
         $fc=$this->get('badiu.system.core.lib.form.factoryconfig');
         $fc->setDefaultparam($cform->getDefaultdataonpenform());
         $page->addData('badiu_formconfig',$fc->get($tkey));
         
		 
         $page->addData('badiu_form1_group_list',$formGroupList);
         $page->addData('badiu_form1_field_list',$formFieldList);

         $page->addData('badiu_error_code',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.code'));
         $page->addData('badiu_error_message',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.message'));
         $page->addData('badiu_list_data_row',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row'));
         $page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows'));
        // $page->addData('badiu_report_content',$report->getDashboardData()['reportcontent']);
         
        
         
         $page->addData('badiu_list_data_row.config',$dasheboardsqlsinlgleconfig);
         $page->addData('badiu_list_data_rows.config',$dasheboardsqlconfig);

    
         $page->addData('badiu_table1',$report->getTable());
    
        $page->setThemelayoutblockhidden($cform->getThemelayoutblockhidden());
       
         $page->setLinks($links);
         $page->setConfig($cpage);
         $page->setFormconfig($cform);
         $themelayout=$cpage->getThemelayout(); 
         if(empty($themelayout)){$themelayout=$cform->getThemelayout(); }//review delete
         if(empty($themelayout)){$themelayout=$badiuSession->get()->getTheme().':Layout:column2.html.twig';}
         else{
            $pos=stripos($themelayout, ":");
            if($pos=== false){$themelayout=$badiuSession->get()->getTheme().':Layout:'.$themelayout;}
         }   
         
         
		 $this->addlog($request,'view') ;
        return $this->render($themelayout, array('page'=>$page));
      
 
      
  
    }
	
public function indexParentAction(Request $request,$parentid) {
	
		$this->iniSystemdata();
        $this->forceLogin($request);
        //get key of module
        $this->initKeyManger($request);
        $this->logtimestart=new \DateTime(); 
         //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());
        //page
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setKey($this->getCurrentkeyroute());
        $page->setLayout('report');
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
   
        
        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
		$badiuSession->setSystemdata($this->getSystemdata());
       
   
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
             return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }
		
		$this->initFreportDynamic();
        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        
        //menu 
        $keymenu=$kminherit->moduleMenu();
        if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
        $menu=$this->get($keymenu);
		$menu->setSystemdata($this->getSystemdata());
       
        $menu->setKeymanger($this->getKeymanger());
		
        //report
        $report=$this->get($kminherit->report());
		$report->setSystemdata($this->getSystemdata());
        $report->setKeymanger($this->getKeymanger());
      
        //link
        $croute=$cbundle->getCrudRoute($this->getKeymanger());
        $page->setCrudroute($croute);
        $report->setRouter($this->get("router"));
       
        $report->addLink('edit',$croute->getEdit());
        $report->addLink('copy',$croute->getCopy());
        $report->addLink('delete',$croute->getDelete());
        $report->addLink('remove',$croute->getRemove());
        
         //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
       //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
        //form config

         $cform=$cbundle->getFormConfig('filter',$this->getKeymanger());
          $cpage=$cbundle->getPageConfig($this->getKeymanger());
         if($cform->getAction()==''){$cform->setAction($kminherit->routeIndex());}
      
        $controller->setKeymanger($kminherit);
		 //entity parent
        $pentity=$cbundle->getEntityParent($this->getKeymanger());

        if($this->has($pentity->getEntity())){
            $entitydata=$this->get($pentity->getEntity());
            if(!$entitydata->exist($parentid)){
                $msg=$translator->trans($pentity->getNotFoundMessage());
                echo $msg; //fazer critica
                exit;
            }
        }
        

        
        //form
     /*   $formOption=array();
		
        
	$formOption['action']=$this->generateUrl($cform->getAction(),array('parentid'=>$parentid));
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();*/
      
       
     
        $filter = array();
	$filter=$this->get('badiu.system.core.lib.http.querystringsystem')->updateParam($filter);
        $filter=$controller->setDefaultDataOnOpenSearchForm($filter);
		
        //review form filter $type=$this->get($kminherit->formFilterType());
        //review form filter $type->setKeymanger($this->getKeymanger());

       //review form filter  $form = $this->createForm($type,$filter,$formOption);
       //review form filter  $form->add('submit', 'submit', array('label' => $cform->getSubmitlabel(),'attr' => $cform->getSubmitattr()));

         
       //review form filter  $form->handleRequest($request);
        
      //review form filter  $filter =$form->getData();
		
    $filter=$this->get('badiu.system.core.lib.http.querystringsystem')->updateParam($filter);
               	
		$filter['entity']=$badiuSession->get()->getEntity();
		$filter['parentid']=$parentid;
        if(!isset($filter['_page'])){$filter['_page']=0;}
		
		
         $filter['_format']=$request->query->get('_format');
         $controller->setDatalayout($cform->getDatalayout());
         
         $report->setDefaultdatafilterconfig($cform->getDefaultdata());
         $report->setDefaultdatafilterconfigonopenform($cform->getDefaultdataonpenform()); 
         $report->setFconfig($cform);
         
          //check form
        /* $chekForm=$controller->searchCheckForm($form);
         if(!$chekForm){
               $page= new BadiuPage($form->createView());
               return $this->render($badiuSession->get()->getTheme().':Layout/form.html.twig', array('page' =>$page));
         }*/
       
         $filter['_page']=$request->query->get('_page');
      
         $filter=$controller->serviceExecBeforeSubmit($filter,$cform->getServiceexecbeforesubmit());
		 $report=$controller->extractData($filter,$report);
	     $report=$controller->serviceExecAfterSubmit($report,$cform->getServiceexecaftersubmit());
         $format=$request->query->get('_format');
         if(!empty($format)){
			if($format=='json'){
				$json =json_encode($report->getListData());
				$response = new Response($json);
				$response->headers->set('Content-Type', 'application/json');
				return $response;
			}
			$report->exportTable($format);
			
		}



        $additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

           $filerole=$cform->getFilerole();
        if(!empty($filerole)){
            $formfrole=array();
            foreach ($filerole as $frole) {
                 array_push($formfrole,array('position'=>'pagefooter','file'=>$frole));
            }
            array_push($additionalContent,$formfrole);
           
        }
        
           $filebeforereport=$cform->getFilebeforereport();
        if(!empty($filebeforereport)){
            $fbeforereport=array();
            foreach ($filebeforereport as $freport) {
                 array_push($fbeforereport,array('position'=>'beforereport','file'=>$freport));
            }
            array_push($additionalContent,$fbeforereport);
           
        }
        
        $fileafterreport=$cform->getFileafterreport();
        if(!empty($fileafterreport)){
            $fafterreport=array();
            foreach ($fileafterreport as $freport) {
                 array_push($fafterreport,array('position'=>'afterreport','file'=>$freport));
            }
            array_push($additionalContent,$fafterreport);
           
        }
       
        $filereport=$cform->getFilereport();
       
         $freport=array();
         array_push($freport,array('position'=>'report','file'=>$filereport));
          array_push($additionalContent, $freport);
      

	 $links=array();
        $lroutes=$this->get('router')->getRouteCollection()->all();
        if (array_key_exists($kminherit->routeAdd(),$lroutes)){
            $links=array(array('url'=>$this->generateUrl($kminherit->routeAdd(),array('parentid'=>$parentid)),'position'=>'beforeform','type'=>'button','name'=>$translator->trans('addnew')));
         }
	foreach ($cform->getLinkimenu() as $imneu) {
		$imroute=$imneu['route'];
		$imlabel=$imneu['label'];
                $modal=null;
                if(isset($imneu['modal'])){ $modal=$imneu['modal'];}
		$limenu=array('url'=>$this->generateUrl($imroute,array('parentid'=>$parentid)),'position'=>'beforeform','type'=>'button','name'=>$translator->trans($imlabel),'modal'=>$modal);
		array_push($links,$limenu);
	}
        

         
     
        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());

        $dasheboardsqlsinlgleconfig=$cbundle->getValueQueryString($this->getKeymanger()->dashboardDbSqlSingleConfig(),$this->getKeymangerExtend()->dashboardDbSqlSingleConfig());
        $dasheboardsqlconfig=$cbundle->getValueQueryString($this->getKeymanger()->dashboardDbSqlConfig(),$this->getKeymangerExtend()->dashboardDbSqlConfig());


      //review form filter   $page->addData('badiu_form1',$form->createView());
	$page->addData('badiu_form1_data',$filter);
         $page->addData('badiu_form1_group_list',$formGroupList);
         $page->addData('badiu_form1_field_list',$formFieldList);

         //form config
         $tkey=$this->getCurrentkeyroute();
         $fc=$this->get('badiu.system.core.lib.form.factoryconfig');
          $fc->setDefaultparam($cform->getDefaultdataonpenform());
        $page->addData('badiu_formconfig',$fc->get($tkey));
        
        $page->addData('badiu_error_code',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.code'));
         $page->addData('badiu_error_message',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.message'));
         $page->addData('badiu_list_data_row',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row'));
         $page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows'));
        //$page->addData('badiu_report_content',$report->getDashboardData()['reportcontent']);
        
          $page->addData('badiu_list_data_row.config',$dasheboardsqlsinlgleconfig);
         $page->addData('badiu_list_data_rows.config',$dasheboardsqlconfig);
         
         

         $page->addData('badiu_table1',$report->getTable());
         $page->setAdditionalContents($additionalContent);
         $page->setThemelayoutblockhidden($cform->getThemelayoutblockhidden());
         $page->setLinks($links);
         $page->setConfig($cpage);
          $page->setFormconfig($cform);
          $themelayout=$cpage->getThemelayout(); 
         if(empty($themelayout)){$themelayout=$cform->getThemelayout(); }//review delete
         if(empty($themelayout)){$themelayout=$badiuSession->get()->getTheme().':Layout:column2.html.twig';}
         else{
            $pos=stripos($themelayout, ":");
            if($pos=== false){$themelayout=$badiuSession->get()->getTheme().':Layout:'.$themelayout;}
         }  
         
          $this->addlog($request,'view',$parentid) ;
         return $this->render($themelayout, array('page'=>$page));
 
      
  
    }

   
    public function addAction(Request $request)
    {
        $this->iniSystemdata();
	   $this->forceLogin($request);
       //get key of module
        $this->initKeyManger($request);
          $this->logtimestart=new \DateTime(); 
         //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());
     
        //page 
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setKey($this->getCurrentkeyroute());
        //$page->setLayout('form');
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
     
        
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
             return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

         
       //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());

           //menu 
           $keymenu=$kminherit->moduleMenu();
           if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
           $menu=$this->get($keymenu);
		   $menu->setSystemdata($this->getSystemdata());
        $menu->setKeymanger($this->getKeymanger());
        
        //data
        $data = $this->get($kminherit->data());
        

        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
        //form config
        $cform=$cbundle->getFormConfig('data',$this->getKeymanger());
         $cpage=$cbundle->getPageConfig($this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($kminherit->routeAdd());}
        
         $controller->setDefaultdataonpenform($cform->getDefaultdataonpenform());
        $controller->setKeymanger($kminherit);
        
        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction());
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
      
        $dto = null;
	if($cform->getDatalayout()=='dataforserveraltable'){ 
		$dto = array();
                $dto=$this->get('badiu.system.core.lib.http.querystringsystem')->updateParam($dto);
	}
        else {$dto=$this->get($kminherit->entity());}
         $controller->setDatalayout($cform->getDatalayout());
        $controller->setDefaultdata($cform->getDefaultdata());
	$dto=$controller->setDefaultDataOnOpenAddForm($dto);	
        $type=$this->get($kminherit->formType());
        $type->setKeymanger($this->getKeymanger());
        $form = $this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans($cform->getSubmitlabel()),'attr' => $cform->getSubmitattr()));
      
         
        $form->handleRequest($request); 
        
		$additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());


        $page->addData('badiu_form1',$form->createView());
	$page->addData('badiu_form1_data',$dto);
        $page->addData('badiu_form1_group_list',$formGroupList);
        $page->addData('badiu_form1_field_list',$formFieldList);
 
        //form config
         $tkey=$this->getCurrentkeyroute();
         $fc=$this->get('badiu.system.core.lib.form.factoryconfig');
         $fc->setType('data');
          $fc->setDefaultparam($cform->getDefaultdataonpenform());
        $page->addData('badiu_formconfig',$fc->get($tkey));
        
        $filerole=$cform->getFilerole();
        if(!empty($filerole)){
            $formfrole=array();
            foreach ($filerole as $frole) {
                 array_push($formfrole,array('position'=>'pagefooter','file'=>$frole));
            }
            array_push($additionalContent,$formfrole);
           
        }
 

          $page->setForm($form->createView());
          $page->setAdditionalContents($additionalContent);
	$page->setThemelayoutblockhidden($cform->getThemelayoutblockhidden());	
        
        if ($form->isValid()) {
           if($cform->getDatalayout()=='dataforserveraltable'){ $dto =$form->getData();}
           
         
            $dto=$controller->setDefaultDataOnAddForm($dto) ;
            
            $data->setDto($dto);
            //check form
	    $chekForm=null; 
	    $chekForm=$controller->addCheckForm($form,$data);
            if(!$chekForm){
                $page->addData('badiu_form1',$form->createView());
               return $this->render($badiuSession->get()->getTheme().':Layout:form.html.twig', array('page' =>$page));
            }
			$dto=$controller->serviceExecBeforeSubmit($dto,$cform->getServiceexecbeforesubmit());
			$dto=$controller->save($data);
                        $dto=$controller->serviceExecAfterSubmit($dto,$cform->getServiceexecaftersubmit());
            

           
            if(!empty($cform->getPageviewaftersubmit())){
                $page->addData('badiu_list_data_row',$dto);
                 return $this->render($cform->getPageviewaftersubmit(), array('page' =>$page));
            }else if(!empty($cform->getRoutetoredirectaftersubmit())){
                 return $this->redirect($this->generateUrl($cform->getRoutetoredirectaftersubmit()));
            }else{
                $url=$controller->nextUrlAfterSave();
                if(empty($url)){$url=$kminherit->routeIndex();}
                return  $this->redirect($this->generateUrl($url));
            }
            
         }
         $page->setConfig($cpage);
          $page->setFormconfig($cform);
          $themelayout=$cpage->getThemelayout(); 
          $themelayouttemp="BadiuThemeBaseBundle";
          if(empty($themelayout)){$themelayout=$cform->getThemelayout(); }
        
        // if(empty($themelayout)){$themelayout=$badiuSession->get()->getTheme().':Temp:form.html.twig';}
        if(empty($themelayout)){$themelayout=$themelayouttemp.':Layout:form.html.twig';}
         else{
            $pos=stripos($themelayout, ":");
            if($pos=== false){$themelayout=$badiuSession->get()->getTheme().':Layout:'.$themelayout;}
         } 
          $this->addlog($request,'create') ;
         
        return $this->render($themelayout, array('page' =>$page));
  
    }

  public function addserviceAction(Request $request)
    {
        $this->iniSystemdata();
	   $this->forceLogin($request);
        //get key of module
        $this->initKeyManger($request);
         $this->logtimestart=new \DateTime(); 
		 
         //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());
     
        //page 
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setKey($this->getCurrentkeyroute());
        $page->setLayout('form');
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
     
        
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
     
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){ 
             return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }
        
        
        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        $isedit=false;
        $dtoofdb=array();
        $foperation=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('foperation');
        $sysoperation=$this->get('badiu.system.core.lib.util.systemoperation');
        $isedit=$sysoperation->isEdit($this->getCurrentkeyroute());
       $id=null;
        if($isedit ||  $foperation=='clone'){
          
            $id=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('id');
           
            $data =  $this->get($kminherit->data());
            $dtoofdb =$data->findById($id);
          
            $objectutil=$this->get('badiu.system.core.lib.util.objectutil');
            $dtoofdb= $objectutil->castEntityToArrayForForm($dtoofdb);
 
            if($foperation=='clone'){
                if (array_key_exists("shortname",$dtoofdb)){$dtoofdb['shortname']=null;}
                if (array_key_exists("idnumber",$dtoofdb)){$dtoofdb['idnumber']=null;}
            }
            
            
          }
         
       //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());

           //menu 
           $keymenu=$kminherit->moduleMenu();
           if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
           $menu=$this->get($keymenu);
		   $menu->setSystemdata($this->getSystemdata());
            $menu->setKeymanger($this->getKeymanger());
        
        //data
        $data = $this->get($kminherit->data());
        

        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
        //form config
        $cform=$cbundle->getFormConfig('data',$this->getKeymanger());
         $cpage=$cbundle->getPageConfig($this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($kminherit->routeAdd());}
        
         $controller->setDefaultdataonpenform($cform->getDefaultdataonpenform());
        $controller->setKeymanger($kminherit);
        
        $formOption=array();
        $parentid=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
        
        
        //review and delete
        $lroutes=$this->get('router')->getRouteCollection()->all();
        if (array_key_exists($cform->getAction(),$lroutes)){
            if($parentid){$formOption['action']=$this->generateUrl($cform->getAction(),array('parentid'=>$parentid));}//revis
            else{ $formOption['action']=$this->generateUrl($cform->getAction());}
        }
        
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
      
        $dto = null;
	if($cform->getDatalayout()=='dataforserveraltable'){ 
		$dto = array();
                $dto=$this->get('badiu.system.core.lib.http.querystringsystem')->updateParam($dto);
	}
        else {$dto=$this->get($kminherit->entity());}
         $controller->setDatalayout($cform->getDatalayout());
        $controller->setDefaultdata($cform->getDefaultdata());
        
	
       //review form filter $type=$this->get($kminherit->formType());
       //review form filter  $type->setKeymanger($this->getKeymanger());
     
        
	$additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());

        $page->addData('badiu_form1_data',$dto);
        $page->addData('badiu_form1_group_list',$formGroupList);
        $page->addData('badiu_form1_field_list',$formFieldList);
 
        //form config
         $tkey=$this->getCurrentkeyroute();
         $fc=$this->get('badiu.system.core.lib.form.factoryconfig');
         $fc->setType('data');
         $fc->setDatalayout($cform->getDatalayout());
         //check if edit
         //get data for db
         //convert do array
         //set id hidden
       
        if($isedit || $foperation =='clone'){$fc->setDefaultparam($dtoofdb);}  
        else {$fc->setDefaultparam($cform->getDefaultdataonpenform());}
     
        $page->addData('badiu_formconfig',$fc->get($tkey));
       /* echo "<pre>";
		print_r($fc->get($tkey));
		echo "<pre>";exit;*/
        $filerole=$cform->getFilerole();
        if(!empty($filerole)){
            $formfrole=array();
            foreach ($filerole as $frole) {
                 array_push($formfrole,array('position'=>'pagefooter','file'=>$frole));
            }
            array_push($additionalContent,$formfrole);
           
        }
 

         $croute=$cbundle->getCrudRoute($this->getKeymanger());
        $page->setCrudroute($croute);
          $page->setAdditionalContents($additionalContent);
	$page->setThemelayoutblockhidden($cform->getThemelayoutblockhidden());	
        
  
         $page->setConfig($cpage);
         $page->setFormconfig($cform); 
         $page->setKey($this->getCurrentkeyroute());
        $themelayout=$cpage->getThemelayout(); 
       // echo "xxxd theme: ".$badiuSession->get()->getTheme();exit; 
         if(empty($themelayout)){$themelayout=$badiuSession->get()->getTheme().':Layout:column2.html.twig';}
         else{
            $pos=stripos($themelayout, ":");
            if($pos=== false){$themelayout=$badiuSession->get()->getTheme().':Layout:'.$themelayout;}
         } 
       
         if($isedit){$this->addlog($request,'edit',$id) ;}
         else{ $this->addlog($request,'create',$parentid) ;}
      
        return $this->render($themelayout, array('page' =>$page));
  
    }
    
// delete temp review     
     public function addModalAction(Request $request)
    {
		 $this->iniSystemdata();
       //get key of module
        $this->initKeyManger($request);
        
         //page 
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
        
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission'); 
        if(!$permission->has_access($this->getCurrentkeyroute())){
             return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());
       
       //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');

      
         //menu 
         $keymenu=$kminherit->moduleMenu();
         if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
        $menu->setKeymanger($this->getKeymanger());
        
        //data
        $data = $this->get($kminherit->data());
        

        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
        //form config
        $cform=$cbundle->getFormConfig('data',$this->getKeymanger());
        $cpage=$cbundle->getPageConfig($this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($kminherit->routeAdd());}
        
         $controller->setDefaultdataonpenform($cform->getDefaultdataonpenform());
        
        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction());
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
      
        $dto = null;
	if($cform->getDatalayout()=='dataforserveraltable'){ 
		$dto = array();
                $dto=$this->get('badiu.system.core.lib.http.querystringsystem')->updateParam($dto);
	}
        else {$dto=$this->get($kminherit->entity());}
         $controller->setDatalayout($cform->getDatalayout());
        $controller->setDefaultdata($cform->getDefaultdata());
	$dto=$controller->setDefaultDataOnOpenAddForm($dto);	
        $type=$this->get($kminherit->formType());
        $type->setKeymanger($this->getKeymanger());
        $form = $this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans($cform->getSubmitlabel()),'attr' => $cform->getSubmitattr()));
      
         
        $form->handleRequest($request); 
        
		$additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());


        $page->addData('badiu_form1',$form->createView());
	$page->addData('badiu_form1_data',$dto);
        $page->addData('badiu_form1_group_list',$formGroupList);
        $page->addData('badiu_form1_field_list',$formFieldList);
 
        //form config
         $tkey=$this->getCurrentkeyroute();
         $fc=$this->get('badiu.system.core.lib.form.factoryconfig');
          $fc->setDefaultparam($cform->getDefaultdataonpenform());
        $page->addData('badiu_formconfig',$fc->get($tkey));
        
         $filerole=$cform->getFilerole();
        if(!empty($filerole)){
            $formfrole=array();
            foreach ($filerole as $frole) {
                 array_push($formfrole,array('position'=>'pagefooter','file'=>$frole));
            }
            array_push($additionalContent,$formfrole);
        }


          $page->setForm($form->createView());
          $page->setAdditionalContents($additionalContent);
          
	$page->setThemelayoutblockhidden($cform->getThemelayoutblockhidden());	
        if ($form->isValid()) {
           if($cform->getDatalayout()=='dataforserveraltable'){ $dto =$form->getData();}
            
         
            $dto=$controller->setDefaultDataOnAddForm($dto) ;
            
            $data->setDto($dto);
            //check form
			 $chekForm=null; 
			$chekForm=$controller->addCheckForm($form,$data);
             
            
            if(!$chekForm){
             	   $page->addData('badiu_form1',$form->createView());
               return $this->render($badiuSession->get()->getTheme().':Layout:form.html.twig', array('page' =>$page));
            }
			$dto=$controller->serviceExecBeforeSubmit($dto,$cform->getServiceexecbeforesubmit());
			$dto=$controller->save($data);
                        $dto=$controller->serviceExecAfterSubmit($dto,$cform->getServiceexecaftersubmit());
                       
            
          /*   $result=array('process'=>'ok');
            $json =json_encode($result);
             $response = new Response($json);
            $response->headers->set('Content-Type', 'application/json');
            
            return $response;*/
                        return null;
         }
       
       $themelayout=$cform->getThemelayout();
         if(empty($themelayout)){$themelayout=$badiuSession->get()->getTheme().':Layout:formmodal.html.twig';}
        return $this->render($themelayout, array('page' =>$page));
  
    }
    public function addParentAction(Request $request,$parentid)
    {
         $this->iniSystemdata();
		$this->forceLogin($request);
        //get key of module
        $this->initKeyManger($request);
        $this->logtimestart=new \DateTime(); 
          //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());
        
        //page
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
        //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');

        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
            return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }


        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        //parent
        $cparent=$cbundle->getEntityParent($this->getKeymanger());



        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());

      




    
        //$page->setFormviewlayout($request->query->get('_formviewlayout'));
        //menu
        $keymenu=$kminherit->moduleMenu();
        if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
        $menu->setKeymanger($this->getKeymanger());

        //data parente entity

        $entitydata=$this->get($cparent->getEntity());
        if(!$entitydata->exist($parentid)){
            $msg=$translator->trans($cparent->getNotFoundMessage());
            echo $msg; //fazer critica
            exit;
        }else{

        }



        //data
        $data = $this->get($kminherit->data());


        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }

        //form config
        $cform=$cbundle->getFormConfig('data',$this->getKeymanger());
        $cpage=$cbundle->getPageConfig($this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($kminherit->routeAdd());}
         $controller->setDatalayout($cform->getDatalayout());
        $controller->setDefaultdataonpenform($cform->getDefaultdataonpenform());
        
        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction(),array('parentid'=>$parentid));
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();


        $dto = null;
		if($cform->getDatalayout()=='dataforserveraltable'){ 
			$dto = array();
			$dto=$this->get('badiu.system.core.lib.http.querystringsystem')->updateParam($dto);
		}
		else {$dto=$this->get($kminherit->entity());}
               
                 $controller->setDefaultdata($cform->getDefaultdata());
		$dto=$controller->setDefaultDataOnOpenAddForm($dto);
        $type=$this->get($kminherit->formType());
        $type->setKeymanger($this->getKeymanger());
        $form = $this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans($cform->getSubmitlabel()),'attr' => $cform->getSubmitattr()));


        $form->handleRequest($request);

		$additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
		
	 $filerole=$cform->getFilerole();
        if(!empty($filerole)){
            $formfrole=array();
            foreach ($filerole as $frole) {
                 array_push($formfrole,array('position'=>'pagefooter','file'=>$frole));
            }
            array_push($additionalContent,$formfrole);
        }

        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
     

        $page->addData('badiu_form1',$form->createView());
		$page->addData('badiu_form1_data',$dto);
        $page->addData('badiu_form1_group_list',$formGroupList);
        $page->addData('badiu_form1_field_list',$formFieldList);
        
        //form config
         $tkey=$this->getCurrentkeyroute();
         $fc=$this->get('badiu.system.core.lib.form.factoryconfig');
         $fc->setType('data');
          $fc->setDefaultparam($cform->getDefaultdataonpenform());
        $page->addData('badiu_formconfig',$fc->get($tkey));
        
        $page->setAdditionalContents($additionalContent);
        $page->setThemelayoutblockhidden($cform->getThemelayoutblockhidden());
       
	 $urlreferer=$request->query->get('_urlreferer');
         if(!empty($urlreferer)){$badiuSession->addValue('_urlreferer',$urlreferer);}
        //echo $urlreferer."<br>";
      //  $session1->set('_urlreferer', $urlreferer);
       
        if ($form->isValid()) {
            if($cform->getDatalayout()=='dataforserveraltable'){ $dto =$form->getData();}
            
           
            $dto=$controller->setDefaultDataOnAddForm($dto) ;
            
            $data->setDto($dto);
             
            $functionAddParent=$cparent->getFunctionAdd();
            $methodParent=$cparent->getMethodAdd();
           
            if(!empty($functionAddParent)){
                $pservice=$this->get($cparent->getService());
                $dtoParent=$entitydata->findById($parentid);
                $dto=$pservice->$functionAddParent($dto,$dtoParent);
             }  else if(!empty($methodParent)){
                 $dtoParent=$entitydata->findById($parentid);
                 $dto=$controller->addEntityToClass($dto,$dtoParent,$methodParent);
             }
            

            $data->setDto($dto);

            //check form
            
            $chekForm=true;//$controller->addCheckForm($form,$data);

            if(!$chekForm){
               $page->addData('badiu_form1',$form->createView());
               
                return $this->render($badiuSession->get()->getTheme().':Layout:form.html.twig', array('page' =>$page));
            }
	    $dto=$controller->serviceExecBeforeSubmit($dto,$cform->getServiceexecbeforesubmit());		
            $dto=$controller->save($data);
            $dto=$controller->serviceExecAfterSubmit($dto,$cform->getServiceexecaftersubmit());
            
            if(!empty($cform->getPageviewaftersubmit())){
                $page->addData('badiu_list_data_row',$dto);
                 return $this->render($cform->getPageviewaftersubmit(), array('page' =>$page));
            }else if(!empty($cform->getRoutetoredirectaftersubmit())){
                return  $this->redirect($this->generateUrl($cform->getRoutetoredirectaftersubmit()));
            }else{
                $url=$controller->nextUrlAfterSave();
                if(empty($url)){$url=$kminherit->routeIndex();}
                 return  $this->redirect($this->generateUrl($url,array('parentid'=>$parentid)));
            }
           // return $this->redirect($this->generateUrl($kminherit->routeIndex(),array('parentid'=>$parentid)));
        }
        $page->setConfig($cpage);
         $page->setFormconfig($cform);
         $themelayout=$cform->getThemelayout();
         if(empty($themelayout)){$themelayout=$badiuSession->get()->getTheme().':Layout:form.html.twig';}
          $this->addlog($request,'create',$parentid) ;
        return $this->render($themelayout, array('page' =>$page));
      

    }

    public function editAction(Request $request,$id)
    {
		 $this->iniSystemdata();
		$this->forceLogin($request);
       //get key of module
        $this->initKeyManger($request);
        
         //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());
     
        //page 
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));

     
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
             return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>new BadiuPage()));
        }

        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

           //menu 
           $keymenu=$kminherit->moduleMenu();
           if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
           $menu=$this->get($keymenu);
		   $menu->setSystemdata($this->getSystemdata());
            $menu->setKeymanger($this->getKeymanger());
      
        //data
        $data =  $this->get($kminherit->data());

        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
		
        //form config
        $cform=$cbundle->getFormConfig('edit',$this->getKeymanger());
         $cpage=$cbundle->getPageConfig($this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($kminherit->routeEdit());}
        

        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction(),array('id'=>$id));
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
	    $controller->setDatalayout($cform->getDatalayout());  
            
		$dto = null;
		if($cform->getDatalayout()=='dataforserveraltable'){ $dto = array();}
		else { $dto =$data->findById($id);}
		
       $dto=$controller->setDefaultDataOnOpenEditForm($dto);
	     
        $controller->setKeymanger($kminherit);
        $parentid=null;

         //parent
        $cparent=$cbundle->getEntityParent($this->getKeymanger());
         $functionFindParent=$cparent->getFunctionFind();
          $methodFindParent=$cparent->getMethodFind();
        if(!empty($functionFindParent)){
                $pservice=$this->get($cparent->getService());
                $parentid = $pservice->$functionFindParent($dto);
        }else if(!empty($methodFindParent)){
               $parentid = $controller->findEndityIdInClass($dto,$methodFindParent);
        }

       // if(method_exists ($dto,'findParent')) {$parentid = $dto->findParent();}

        $type=$this->get($kminherit->formType());
        $type->setKeymanger($this->getKeymanger());
        $form =$this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans($cform->getSubmitlabel()),'attr' => $cform->getSubmitattr()));
           
        $form->handleRequest($request); 
        
	 $menu->setParentid($parentid);

        $additionalContent=array();
		
	 $filerole=$cform->getFilerole();
        if(!empty($filerole)){
            $formfrole=array();
            foreach ($filerole as $frole) {
                 array_push($formfrole,array('position'=>'pagefooter','file'=>$frole));
            }
            array_push($additionalContent,$formfrole);
        }
	/* review problem on check error 
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);
*/
         $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
    
        $page->addData('badiu_form1',$form->createView());
	$page->addData('badiu_form1_data',$dto);
        $page->addData('badiu_form1_group_list',$formGroupList);
        $page->addData('badiu_form1_field_list',$formFieldList);
        
        //form config
         $tkey=$this->getCurrentkeyroute();
         $fc=$this->get('badiu.system.core.lib.form.factoryconfig');
          $fc->setDefaultparam($cform->getDefaultdataonpenform());
        $page->addData('badiu_formconfig',$fc->get($tkey));
        
        $page->setAdditionalContents($additionalContent);	
	$page->setThemelayoutblockhidden($cform->getThemelayoutblockhidden());	
        if ($form->isValid()) {
           if($cform->getDatalayout()=='dataforserveraltable'){ $dto =$form->getData();}
            $dto=$controller->setDefaultDataOnEditForm($dto);
            
            $data->setDto($dto);


            //checkform
            $chekForm=$controller->editCheckForm($form,$data);
          if(!$chekForm){
                 $page->addData('badiu_form1',$form->createView());
               return $this->render($badiuSession->get()->getTheme().':Layout:form.html.twig', array('page' =>$page));
            }

           $dto=$controller->serviceExecBeforeSubmit($dto,$cform->getServiceexecbeforesubmit());
            $dto= $controller->save($data);
            
            $dto=$controller->serviceExecAfterSubmit($dto,$cform->getServiceexecaftersubmit());
            
            if($cform->getDatalayout()=='dataforserveraltable' && empty($parentid) ){  $parentid=$controller->getParentid(); }
            
            $paramparentid=array();
            if(!empty($parentid)){ $paramparentid=array('parentid'=>$parentid);}
            
            if(!empty($cform->getPageviewaftersubmit())){
                $page->addData('badiu_list_data_row',$dto);
                return $this->render($cform->getPageviewaftersubmit(), array('page' =>$page));
            }else if(!empty($cform->getRoutetoredirectaftersubmit())){
                return  $this->redirect($this->generateUrl($cform->getRoutetoredirectaftersubmit()));
            }else{
                $url=$controller->nextUrlAfterSave();
                if(empty($url)){$url=$kminherit->routeIndex();}
                return  $this->redirect($this->generateUrl($url,$paramparentid));
            }
         }
       //start duplicate code rewiew it  
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);
        $page->setAdditionalContents($additionalContent);
        $page->setConfig($cpage);
         $page->setFormconfig($cform);
        $themelayout=$cpage->getThemelayout(); 
        $themelayouttemp="BadiuThemeBaseBundle";
          if(empty($themelayout)){$themelayout=$cform->getThemelayout(); }
        
        // if(empty($themelayout)){$themelayout=$badiuSession->get()->getTheme().':Temp:form.html.twig';}
         if(empty($themelayout)){$themelayout=$themelayouttemp.':Layout:form.html.twig';}
         else{
            $pos=stripos($themelayout, ":");
            if($pos=== false){$themelayout=$badiuSession->get()->getTheme().':Layout:'.$themelayout;}
         }  
        return $this->render($themelayout, array('page' =>$page));
  
    }
    

    public function copyAction(Request $request,$id)
    {
         $this->iniSystemdata();
		$this->forceLogin($request);
        //get key of module
        $this->initKeyManger($request);
		 $this->logtimestart=new \DateTime();
         //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());
      
       //page 
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
     
        
        
         //get session data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
         //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
             return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        
       //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());

           //menu 
           $keymenu=$kminherit->moduleMenu();
           if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
          $menu=$this->get($keymenu);
		  $menu->setSystemdata($this->getSystemdata());
        $menu->setKeymanger($this->getKeymanger());
        
        //data
        $data = $this->get($kminherit->data());
        

        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
      
        //form config
        $cform=$cbundle->getFormConfig('data',$this->getKeymanger());
        $cpage=$cbundle->getPageConfig($this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($kminherit->routeAdd());}
        
        $controller->setKeymanger($kminherit);
        
       $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction());
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();
      
        
        $dto=null;
        if($cform->getDatalayout()=='dataforserveraltable'){
             $dto=array();
        }else{
            $dtoold =$data->findById($id);
            $dto= clone $dtoold; 
        }
        $controller->setDatalayout($cform->getDatalayout());
        $controller->setDefaultdata($cform->getDefaultdata());
        $dto=$controller->setDefaultDataOnOpenEditForm($dto);
        
         $type=$this->get($kminherit->formType());
        $type->setKeymanger($this->getKeymanger());
        $form = $this->createForm($type,$dto,$formOption);
        $form->add('submit', 'submit', array('label' => $translator->trans('add'),'attr' => array('class' => 'btn btn-default')));
        
         
        $form->handleRequest($request); 
        
        $additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());


        $page->addData('badiu_form1',$form->createView());
	$page->addData('badiu_form1_data',$dto);
        $page->addData('badiu_form1_group_list',$formGroupList);
        $page->addData('badiu_form1_field_list',$formFieldList);
        
        //form config
         $tkey=$this->getCurrentkeyroute();
         $fc=$this->get('badiu.system.core.lib.form.factoryconfig');
          $fc->setDefaultparam($cform->getDefaultdataonpenform());
        $page->addData('badiu_formconfig',$fc->get($tkey));
        
        $filerole=$cform->getFilerole();
        if(!empty($filerole)){
            $formfrole=array();
            foreach ($filerole as $frole) {
                 array_push($formfrole,array('position'=>'pagefooter','file'=>$frole));
            }
            array_push($additionalContent,$formfrole);
           
        }
        $page->setForm($form->createView());
        $page->setAdditionalContents($additionalContent);
         $page->setConfig($cpage);
          $page->setFormconfig($cform);
        $page->setThemelayoutblockhidden($cform->getThemelayoutblockhidden());  
        if ($form->isValid()) {
            if($cform->getDatalayout()=='dataforserveraltable'){ $dto =$form->getData();}
            else{
                $em = $this->getDoctrine()->getManager();
                $dto->setDeleted(FALSE);
                $dto->setTimecreated(new \Datetime());
                $dto->setId(null);
            }
            $dto=$controller->setDefaultDataOnAddForm($dto) ;
            
            $data->setDto($dto);

            //checkform
            $chekForm=$controller->addCheckForm($form,$data);

            if(!$chekForm){
                 //$page= new BadiuPage($form->createView());
               $page->addData('badiu_form1',$form->createView());
               return $this->render($badiuSession->get()->getTheme().':Layout:form.html.twig', array('page' =>$page));
            }
           // $data->save();
            $dto= $controller->save($data);
             //go to nex page
            $url=$controller->nextUrlAfterSave();
			$this->addlog($request,'copy') ;
            if(empty($url)){$url=$this->redirect($this->generateUrl($kminherit->routeIndex()));}
            return $url;
         }

       
         $this->addlog($request,'copy') ;
        return $this->render($badiuSession->get()->getTheme().':Layout:form.html.twig', array('page' =>$page));
  
    }

    public function deleteAction(Request $request,$id)
    {
		 $this->iniSystemdata();
		$this->forceLogin($request);
       //get key of module
        $this->initKeyManger($request);
        $this->logtimestart=new \DateTime();
         //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());
      
       //page 
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
     
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
             return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=> $page));
        }
        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        //data
        $data =  $this->get($kminherit->data());
        
        $data->delete($id);
		 $this->addlog($request,'delete') ;
        return $this->redirect($this->generateUrl($kminherit->routeIndex()));
        
        
    }
    

    public function removeAction(Request $request,$id)
    {
		 $this->iniSystemdata();
        $this->forceLogin($request);
        //get key of module
        $this->initKeyManger($request);
       
        $this->logtimestart=new \DateTime();
         //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());
      
       //page 
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
     
         //get sessionn data
        $badiuSession=$this->container->get('badiu.system.access.session');
       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
             return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=> $page));
        }

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());


        //get entity manager
        $em =  $this->getDoctrine()->getManager($badiuSession->get()->getDbapp());
        
        //data
        $data =  $this->get($kminherit->data());
        
        $data->remove($id);
		 $this->addlog($request,'remove') ;
        return $this->redirect($this->generateUrl($kminherit->routeIndex()));
        
        
    }

    public function frontpageAction(Request $request) {
         $this->iniSystemdata();
		$this->forceLogin($request);
		//get key of module
        $this->initKeyManger($request);
		
		$this->logtimestart=new \DateTime();
         //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());

         //page
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
		  $page->setOperation($translator->trans($this->getCurrentkeyroute()));
        $page->setKey($this->getCurrentkeyroute());
        $page->setLayout('frontpage');
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
   
        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
		$badiuSession->setSystemdata($this->getSystemdata());

       
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
            return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=> $page));
        }

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        $cpage=$cbundle->getPageConfig($this->getKeymanger());
        
            //menu
        $keymenu=$kminherit->moduleMenu();
        if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
        $menu=$this->get($keymenu);
		$menu->setSystemdata($this->getSystemdata());
        $menu->setKeymanger($this->getKeymanger());

       // $menuBeforeContent=$menu->get('before.content');
       $additionalContent=array();
        $defaultmenu=$menu->get();
        $menuBeforeContent=$menu->get('content');
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);
        array_push($additionalContent,$menuBeforeContent);
        //$page->setForm($form->createView());
      // $page->setTable($report->getTable());
        $page->setAdditionalContents($additionalContent);
        $page->setConfig($cpage);
        //echo $badiuSession->get()->getTheme();exit;
		
		$this->addlog($request,'view') ;
        return $this->render($badiuSession->get()->getTheme().':Layout:frontpage.html.twig', array('page'=>$page));

    }

	 public function frontpageParentAction(Request $request,$parentid) {
		 $this->iniSystemdata();
		$this->forceLogin($request);
        //get key of module
        $this->initKeyManger($request);
        
		$this->logtimestart=new \DateTime();
         //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());
        
        //page
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
		  $page->setOperation($translator->trans($this->getCurrentkeyroute()));
        $page->setKey($this->getCurrentkeyroute());
        $page->setLayout('frontpage');
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
      
        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
		$badiuSession->setSystemdata($this->getSystemdata());
       
   
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
             return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

         $cpage=$cbundle->getPageConfig($this->getKeymanger());
          //menu 
          $keymenu=$kminherit->moduleMenu();
          if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
          $menu=$this->get($keymenu);
		  $menu->setSystemdata($this->getSystemdata());
        $menu->setKeymanger($this->getKeymanger());
       
        //entity parent
        $pentity=$cbundle->getEntityParent($this->getKeymanger());

	if($this->has($pentity->getEntity())){
        $entitydata=$this->get($pentity->getEntity());
		
        if(!$entitydata->exist($parentid)){
            $msg=$translator->trans($pentity->getNotFoundMessage());
            echo $msg; //fazer critica
            exit;
        }
	}	
       
      $additionalContent=array();
        $defaultmenu=$menu->get();
        $menuBeforeContent=$menu->get('content');
		
         $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);
        array_push($additionalContent,$menuBeforeContent);
        $page->setAdditionalContents($additionalContent);
       $page->setAdditionalContents($additionalContent);
         $page->setConfig($cpage);
	
		$this->addlog($request,'view') ;
        return $this->render($badiuSession->get()->getTheme().':Layout:frontpage.html.twig', array('page'=>$page));
   }
   //review delete
	public function serviceAction(Request $request) {
		 $this->iniSystemdata();
		$this->forceLogin($request);
		//get key of   
        $this->initKeyManger($request);

        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
		$badiuSession->setSystemdata($this->getSystemdata());

        //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());

        //permission review
      /*  $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
            return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>new BadiuPage()));
        }*/

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit'); 
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

		//controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
		
		 //report
        $report=$this->get($kminherit->report());
		$report->setSystemdata($this->getSystemdata());
        $report->setKeymanger($this->getKeymanger());
          
		  
		 $filter = array();
		 $filter['parentid']=$request->query->get('parentid');
		 $filter['gsearch']=$request->query->get('gsearch');
		 $filter=$controller->setDefaultDataOnSearchForm($filter);
         
		 
         $report=$controller->extractData($filter,$report,'dashboard');

        
		$json =json_encode($report->getDashboardData()['rows']);
             $response = new Response($json);
           $response->headers->set('Content-Type', 'application/json');
         return $response;

    } 
	
	public function serviceProcessAction(Request $request) {
		$clientsession = $this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_clientsession','ajaxws');
		
		if(!empty($clientsession)){ 
			$wsnavegation=$this->get('badiu.auth.core.webservice.navegation');
			$wsnavegation->setTyperequest("ajaxws");
			$autresult=$wsnavegation->authRequest();
			$this->utildata = $this->get('badiu.system.core.lib.util.data');
			$statusresponse=$this->getUtildata()->getVaueOfArray($autresult,'status');
			if($statusresponse=='danied'){
				$json =json_encode($autresult);
				$response = new Response($json);
				$response->headers->set('Content-Type', 'application/json');
				return $response;
				
			}
			/*else if($statusresponse=='accept'){
				
			}*/
			
		} 
		
		$stoken = $this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_stoken','ajaxws');
		
		if(!empty($stoken)){ 
			$loginsystemmoduletoken=$this->get('badiu.auth.core.login.systemmoduletoken');
			
			$autresult=$loginsystemmoduletoken->authRequest();
			if(!empty($autresult)){
				$json =json_encode($autresult);
				$response = new Response($json);
				$response->headers->set('Content-Type', 'application/json');
				return $response;
				
			}
		}
		
	$jsonsyncresponse=$this->get('badiu.system.core.lib.util.jsonsyncresponse');
       //$this->initDatetimezone();
	   date_default_timezone_set('America/Sao_Paulo');//review
        $manageremotesequest= $this->get('badiu.system.core.lib.util.manageremotesequest');
         $skipsession=$manageremotesequest->skipSession();
		
		
         if($skipsession){
            $json=null;
			
             $result=$manageremotesequest->processService();
              if (is_array($result) && array_key_exists("status",$result) && array_key_exists("info",$result) && array_key_exists("message",$result)){
                    $json =json_encode($result);
               }else{
                    $jsonsyncresponse=$this->get('badiu.system.core.lib.util.jsonsyncresponse');
                    $result=$jsonsyncresponse->accept($result);
                    $json =json_encode($result);
                }
             $response = new Response($json);
             $response->headers->set('Content-Type', 'application/json');
            return $response;
         } 
		
		//get key of
	
        $this->initKeyManger($request);

        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
		//$badiuSession->setSystemdata($this->getSystemdata());

        //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());

        //permission  reivew
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
			$serviceaddress=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_service');
           $function=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_function');
           if(empty($function)){$function="exec";}
			$json=$jsonsyncresponse->denied('badiu.system.service.permission.denied',"Without permission to access service $serviceaddress and execute function $function");
             $json =json_encode($json);
			 $response = new Response($json);
			$response->headers->set('Content-Type', 'application/json');
			return $response;
        }

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		//$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

		//controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }
		
		$result=$controller->serviceProcess();
                $json =null;
                if (is_array($result) && array_key_exists("status",$result) && array_key_exists("info",$result) && array_key_exists("message",$result)){
                     $json =json_encode($result);
                   
                }else{
                   
                     $result=$jsonsyncresponse->accept($result);
                     $json =json_encode($result);
                    
                }
         
        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

  
     public function viewAction(Request $request,$id) {
		 $this->iniSystemdata();
		$this->forceLogin($request);
        //get key of module
        $this->initKeyManger($request);
        
		$this->logtimestart=new \DateTime();
		
        //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());

        //page
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setLayout('view');
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
        $page->setKey($this->getCurrentkeyroute());
        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
		$badiuSession->setSystemdata($this->getSystemdata());


        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
            return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());
        
         $cpage=$cbundle->getPageConfig($this->getKeymanger());
         
        //menu
        $keymenu=$kminherit->moduleMenu();
        if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
        $menu=$this->get($keymenu);
		$menu->setSystemdata($this->getSystemdata());
        $menu->setKeymanger($this->getKeymanger());

        $cform=$cbundle->getFormConfig('filter',$this->getKeymanger());
        
        //report
        $report=$this->get($kminherit->report());
		$report->setSystemdata($this->getSystemdata());
        $report->setKeymanger($this->getKeymanger());
        $report->setDefaultdatafilterconfig($cform->getDefaultdata());
        $report->setDefaultdatafilterconfigonopenform($cform->getDefaultdataonpenform()); ////new shoud replace $controller->setDefaultdata($cform->getDefaultdata());
        $report->setFconfig($cform);
        //link
        $croute=$cbundle->getCrudRoute($this->getKeymanger());
        $page->setCrudroute($croute);
        $report->setRouter($this->get("router"));

        $report->addLink('edit',$croute->getEdit());
        $report->addLink('copy',$croute->getCopy());
        $report->addLink('delete',$croute->getDelete());
        $report->addLink('remove',$croute->getRemove());

        
        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }

        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }

        
        //check if id exist
        /*$pentity=$cbundle->getEntityParent($this->getKeymanger());
        $entitydata=$this->get($pentity->getEntity());
        if(!$entitydata->exist($parentid)){
            $msg=$translator->trans($pentity->getNotFoundMessage());
            echo $msg; //fazer critica
            exit;
        }
        */
          $filter = array();
        $filter['id']=$id;
        $filter['entity']=$badiuSession->get()->getEntity();
       //$report->extractData($filter,'viewDetail');
       // $report->makeTable('viewDetail');
        $report=$controller->extractData($filter,$report,'viewDetail');
        //print_r($report->getTable());
       // $format=$request->query->get('_format');
       // if(!empty($format))$report->exportTable($format);


        $additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

        //$links=array(array('url'=>$this->generateUrl($this->getKeymanger()->routeAdd(),array('parentid'=>$parentid)),'position'=>'beforeform','type'=>'button','name'=>$translator->trans('addnew')));
        
       // $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());
       // $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());

        
        $dasheboardsqlsinlgleconfig=$cbundle->getValueQueryString($this->getKeymanger()->dashboardDbSqlSingleConfig(),$this->getKeymangerExtend()->dashboardDbSqlSingleConfig());
        $dasheboardsqlconfig=$cbundle->getValueQueryString($this->getKeymanger()->dashboardDbSqlConfig(),$this->getKeymangerExtend()->dashboardDbSqlConfig());


       //  $page->addData('badiu_form1',$form->createView());
       //  $page->addData('badiu_form1_group_list',$formGroupList);
       //  $page->addData('badiu_form1_field_list',$formFieldList);

       $page->addData('badiu_error_code',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.code'));
       $page->addData('badiu_error_message',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.message'));
       $page->addData('badiu_list_data_row',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row'));
       $page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows'));
         //$page->addData('badiu_report_content',$report->getDashboardData()['reportcontent']);

         $page->addData('badiu_list_data_row.config',$dasheboardsqlsinlgleconfig);
         $page->addData('badiu_list_data_rows.config',$dasheboardsqlconfig);
         
         $page->addData('badiu_table1',$report->getTable());

        $page->setAdditionalContents($additionalContent);
         $page->setConfig($cpage);
       // $page->setLinks($links);
      
       $themelayout=$cpage->getThemelayout(); 
       if(empty($themelayout)){$themelayout=$cform->getThemelayout(); }//review delete
       if(empty($themelayout)){$themelayout=$badiuSession->get()->getTheme().':Layout:column2.html.twig';}
       else{
           $pos=stripos($themelayout, ":");
           if($pos=== false){$themelayout=$badiuSession->get()->getTheme().':Layout:'.$themelayout;}
       }  

		$this->addlog($request,'view') ;
       return $this->render($themelayout, array('page'=>$page));
    
    }

    public function dashboardParentAction(Request $request,$parentid) {
		 $this->iniSystemdata();
		$this->forceLogin($request);
        //get key of module
        $this->initKeyManger($request);
        
		$this->logtimestart=new \DateTime();
		
        //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());

        //page
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setLayout('dashboard');
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
		 $page->setKey($this->getCurrentkeyroute());
        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
		$badiuSession->setSystemdata($this->getSystemdata());


        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
            return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }
		
		$this->initFreportDynamic();
        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());
         $cpage=$cbundle->getPageConfig($this->getKeymanger());
        
   //menu
        $keymenu=$kminherit->moduleMenu();
        if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
        $menu=$this->get($keymenu);
		$menu->setSystemdata($this->getSystemdata());
        $menu->setKeymanger($this->getKeymanger());
		$menu->setTitle($page->getMenuTitle());
		
        //report
        $report=$this->get($kminherit->report());
		$report->setSystemdata($this->getSystemdata());
        $report->setKeymanger($this->getKeymanger());

        //link
      /*  $croute=$cbundle->getCrudRoute($this->getKeymanger());
       * $page->setCrudroute($croute);
        $report->setRouter($this->get("router"));

        $report->addLink('edit',$croute->getEdit());
        $report->addLink('copy',$croute->getCopy());
        $report->addLink('delete',$croute->getDelete());
        $report->addLink('remove',$croute->getRemove());
*/
        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }

       

        //form config
        $cform=$cbundle->getFormConfig('filter',$this->keymangerfilter);
        $cpage=$cbundle->getPageConfig($this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($kminherit->routeIndex());}


        //entity parent
        $pentity=$cbundle->getEntityParent($this->getKeymanger());
		if($this->has($pentity->getEntity())){
			$entitydata=$this->get($pentity->getEntity());
			if(!$entitydata->exist($parentid)){
				$msg=$translator->trans($pentity->getNotFoundMessage());
				echo $msg; //fazer critica
				exit;
			}
		}
        


        $filter =array();
        $filter=$this->get('badiu.system.core.lib.http.querystringsystem')->updateParam($filter);
        $filter['parentid']=$parentid;
        $filter['entity']=$badiuSession->get()->getEntity();
        if(!isset($filter['_page'])){$filter['_page']=0;}

		$fileContents=array();
        $hasNex=true;
        $contf=1;
		while ($hasNex){
			$fileContent=$cbundle->getValueInherit($this->getKeymanger()->dashboardFileContent($contf),$this->getKeymangerExtend()->dashboardFileContent($contf));
			if(!empty($fileContent)){$fileContents[$contf]=$fileContent;}
			else{break;}
			$contf++;
			
		}

        $dasheboardsqlsinlgleconfig=$cbundle->getValueQueryString($this->getKeymanger()->dashboardDbSqlSingleConfig(),$this->getKeymangerExtend()->dashboardDbSqlSingleConfig());
        $dasheboardsqlconfig=$cbundle->getValueQueryString($this->getKeymanger()->dashboardDbSqlConfig(),$this->getKeymangerExtend()->dashboardDbSqlConfig());

        $page->addData('badiu_list_data_row.config',$dasheboardsqlsinlgleconfig);
        $page->addData('badiu_list_data_rows.config',$dasheboardsqlconfig);
         
        //$filter->addParent($parentid);
       // $ldata=$report->extractData($filter,'dashboard');
        $report->setDefaultdatafilterconfig($cform->getDefaultdata());
        $report->setDefaultdatafilterconfigonopenform($cform->getDefaultdataonpenform()); ////new shoud replace $controller->setDefaultdata($cform->getDefaultdata());
        $report->setFconfig($cform);
        $report=$controller->extractData($filter,$report,'dashboard');
        $page->addData('badiu_error_code',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.code'));
         $page->addData('badiu_error_message',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.message'));
         $page->addData('badiu_list_data_row',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row'));
         $page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows'));
       // $page->addData('badiu_report_content',$report->getDashboardData()['reportcontent']);
		
		 
        //$page->addData('badiu_list_data_row',$ldata['row']);
        //$page->addData('badiu_list_data_rows',$ldata['rows']);
        $page->addData('badiu_files_content',$fileContents);
 
		

        //print_r($ldata['row']);
       // echo "--------------------------------------<br><br><br>";
       // print_r($ldata['rows']);
       // $format=$request->query->get('_format');
       // if(!empty($format))$report->exportTable($format);


        $additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

        $lroutes=$this->get('router')->getRouteCollection()->all();
        if (array_key_exists($kminherit->routeAdd(),$lroutes)){
            $links=array(array('url'=>$this->generateUrl($this->getKeymanger()->routeAdd(),array('parentid'=>$parentid)),'position'=>'beforeform','type'=>'button','name'=>$translator->trans('addnew')));
        }else{
            $links=array();
        }

        
        //form config -------------START ---
		 $cpage=$cbundle->getPageConfig($this->getKeymanger());
		 $cformd=$cbundle->getFormConfig('filter',$this->keymangerfilter);
		 
		 $controller->setDefaultdataonpenform($cformd->getDefaultdataonpenform());
         $controller->setKeymanger($kminherit);
		
       // if($cformd->getAction()==''){$cformd->setAction($kminherit->routeAdd());}
		
		
		// for filter
      //  $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());
      //  $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());

		$formGroupList=$cbundle->getGroupOfField($this->keymangerfilter->formFilterFieldsEnable(),$this->keymangerfilter->formFilterFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->keymangerfilter->formFilterFieldsEnable(),$this->keymangerfilter->formFilterFieldsEnable());

		// $tkey=$this->getCurrentkeyroute();
		 $tkey=$this->currentkeyroutefilter;
         $fc=$this->get('badiu.system.core.lib.form.factoryconfig');
         $fc->setType('filter');
         $fc->setDatalayout($cformd->getDatalayout());
		 $fc->setDefaultparam($cformd->getDefaultdataonpenform());
		 
		 $page->addData('badiu_form1_group_list',$formGroupList);
         $page->addData('badiu_form1_field_list',$formFieldList);
         $page->addData('badiu_formconfig1',$fc->get($tkey));
		 $page->addData('badiu_formconfig1_defaultdata',$cformd->getDatalayout());
		 $page->addData('badiu_formconfig1_defaultdataonpenform',$cformd->getDefaultdataonpenform());
		
		
        
		//for data
        $cformd=$cbundle->getFormConfig('data',$this->getKeymanger());
        $controller->setDefaultdataonpenform($cformd->getDefaultdataonpenform());
        $controller->setKeymanger($kminherit);
		
		
		$formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());

		$fc=$this->get('badiu.system.core.lib.form.factoryconfig');
        $fc->setType('data');
        $fc->setDatalayout($cformd->getDatalayout());
		$fc->setDefaultparam($cformd->getDefaultdataonpenform());
        
        $page->addData('badiu_form2_group_list',$formGroupList);
        $page->addData('badiu_form2_field_list',$formFieldList);
		$page->addData('badiu_formconfig2',$fc->get($tkey));
		 
		 //form config -------------END ---
        $page->setAdditionalContents($additionalContent);
        $page->setLinks($links);
         $page->setConfig($cpage);
        
         $themelayout=$cpage->getThemelayout(); 
         if(empty($themelayout)){$themelayout=$cform->getThemelayout(); }//review delete
         if(empty($themelayout)){$themelayout=$badiuSession->get()->getTheme().':Layout:column2.html.twig';}
         else{
             $pos=stripos($themelayout, ":");
             if($pos=== false){$themelayout=$badiuSession->get()->getTheme().':Layout:'.$themelayout;}
         }   
		 $this->addlog($request,'view') ;
		
		 if($this->has($this->getKeymanger()->outputchangedata())){
			 $outputschange=$this->get($this->getKeymanger()->outputchangedata());
			;
			if(method_exists($outputschange,'exec')){$page=$outputschange->exec($page);}
		 }
         return $this->render($themelayout, array('page'=>$page));
       }
    
  
 
 
     public function dashboardAction(Request $request) {
		 $this->iniSystemdata();
		$this->forceLogin($request);
        //get key of module
        $this->initKeyManger($request);
        
		$this->logtimestart=new \DateTime();
		
        //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());

        //page
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setLayout('dashboard');
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
		 $page->setKey($this->getCurrentkeyroute());
        //get session data
        $badiuSession=$this->get('badiu.system.access.session');
		$badiuSession->setSystemdata($this->getSystemdata());


        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
            return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }
		
		$this->initFreportDynamic();
		
        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
		$cbundle->setSystemdata($this->getSystemdata());
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());
        
         $cpage=$cbundle->getPageConfig($this->getKeymanger());
        //menu
        $keymenu=$kminherit->moduleMenu();
        if($this->dynamicroute){$keymenu='badiu.system.core.menu';}
        $menu=$this->get($keymenu);
		$menu->setSystemdata($this->getSystemdata());
        $menu->setKeymanger($this->getKeymanger());

        //report
        $report=$this->get($kminherit->report());
		$report->setSystemdata($this->getSystemdata());
        $report->setKeymanger($this->getKeymanger());

        //link
      /*  $croute=$cbundle->getCrudRoute($this->getKeymanger());
       * $page->setCrudroute($croute);
        $report->setRouter($this->get("router"));

        $report->addLink('edit',$croute->getEdit());
        $report->addLink('copy',$croute->getCopy());
        $report->addLink('delete',$croute->getDelete());
        $report->addLink('remove',$croute->getRemove());
*/
        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }

       

        //form config
        $cform=$cbundle->getFormConfig('filter',$this->getKeymanger());
        $cpage=$cbundle->getPageConfig($this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($kminherit->routeIndex());}
       
        $filter=array();
		$filter['entity']=$badiuSession->get()->getEntity();
        $filter=$this->get('badiu.system.core.lib.http.querystringsystem')->updateParam($filter);
        if(!isset($filter['_page'])){$filter['_page']=0;}

		$fileContents=array();
        $hasNex=true;
        $contf=1;
		while ($hasNex){
			$fileContent=$cbundle->getValueInherit($this->getKeymanger()->dashboardFileContent($contf),$this->getKeymangerExtend()->dashboardFileContent($contf));
			if(!empty($fileContent)){$fileContents[$contf]=$fileContent;}
			else{break;}
			$contf++;
			
		}

         $dasheboardsqlsinlgleconfig=$cbundle->getValueQueryString($this->getKeymanger()->dashboardDbSqlSingleConfig(),$this->getKeymangerExtend()->dashboardDbSqlSingleConfig());
        $dasheboardsqlconfig=$cbundle->getValueQueryString($this->getKeymanger()->dashboardDbSqlConfig(),$this->getKeymangerExtend()->dashboardDbSqlConfig());

        
       
        //$filter->addParent($parentid);
       // $ldata=$report->extractData($filter,'dashboard');
      // print_r($cform->getDefaultdata());exit;
        $report->setDefaultdatafilterconfig($cform->getDefaultdata());
        $report->setDefaultdatafilterconfigonopenform($cform->getDefaultdataonpenform()); ////new shoud replace $controller->setDefaultdata($cform->getDefaultdata());
        $report->setFconfig($cform);
        //$filter=$controller->setDefaultDataOnSearchForm($filter);
     //   print_r($filter);exit; 
        $report=$controller->extractData($filter,$report,'dashboard');
        $page->addData('badiu_error_code',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.code'));
        $page->addData('badiu_error_message',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'error.message'));
        $page->addData('badiu_list_data_row',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'row'));
        $page->addData('badiu_list_data_rows',$this->getUtildata()->getVaueOfArray($report->getDashboardData(),'rows'));
       //  $page->addData('badiu_report_content',$report->getDashboardData()['reportcontent']);
		
	
        $page->addData('badiu_list_data_row.config',$dasheboardsqlsinlgleconfig);
        $page->addData('badiu_list_data_rows.config',$dasheboardsqlconfig);
        
        //$page->addData('badiu_list_data_row',$ldata['row']);
        //$page->addData('badiu_list_data_rows',$ldata['rows']);
        $page->addData('badiu_files_content',$fileContents);
 

        //print_r($ldata['row']);
       // echo "--------------------------------------<br><br><br>";
       // print_r($ldata['rows']);
       // $format=$request->query->get('_format');
       // if(!empty($format))$report->exportTable($format);


 //form config -------------START ---
		 $cpage=$cbundle->getPageConfig($this->getKeymanger());
		 $cformd=$cbundle->getFormConfig('filter',$this->getKeymanger());
		 
		 $controller->setDefaultdataonpenform($cformd->getDefaultdataonpenform());
         $controller->setKeymanger($kminherit);
		
       // if($cformd->getAction()==''){$cformd->setAction($kminherit->routeAdd());}
		
		// for filter
        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());

		 //$tkey=$this->getCurrentkeyroute();
		  $tkey=$this->currentkeyroutefilter;
         $fc=$this->get('badiu.system.core.lib.form.factoryconfig');
         $fc->setType('filter');
         $fc->setDatalayout($cformd->getDatalayout());
		 $fc->setDefaultparam($cformd->getDefaultdataonpenform());
		 
		 $page->addData('badiu_form1_group_list',$formGroupList);
         $page->addData('badiu_form1_field_list',$formFieldList);
         $page->addData('badiu_formconfig1',$fc->get($tkey));
		 $page->addData('badiu_formconfig1_defaultdata',$cformd->getDatalayout());
		 $page->addData('badiu_formconfig1_defaultdataonpenform',$cformd->getDefaultdataonpenform());
		
		
        
		//for data
        $cformd=$cbundle->getFormConfig('data',$this->getKeymanger());
        $controller->setDefaultdataonpenform($cformd->getDefaultdataonpenform());
        $controller->setKeymanger($kminherit);
		
		
		$formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formDataFieldsEnable(),$this->getKeymangerExtend()->formDataFieldsEnable());

		$fc=$this->get('badiu.system.core.lib.form.factoryconfig');
        $fc->setType('data');
        $fc->setDatalayout($cformd->getDatalayout());
		$fc->setDefaultparam($cformd->getDefaultdataonpenform());
        
        $page->addData('badiu_form2_group_list',$formGroupList);
        $page->addData('badiu_form2_field_list',$formFieldList);
		$page->addData('badiu_formconfig2',$fc->get($tkey));
		 
		 //form config -------------END ---

        $additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);

        $lroutes=$this->get('router')->getRouteCollection()->all();
        if (array_key_exists($kminherit->routeAdd(),$lroutes)){
            $links=array(array('url'=>$this->generateUrl($this->getKeymanger()->routeAdd(),array('parentid'=>$parentid)),'position'=>'beforeform','type'=>'button','name'=>$translator->trans('addnew')));
        }else{
            $links=array();
        }

        
        
        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());


        // $page->addData('badiu_form1',$form->createView());
       //  $page->addData('badiu_form1_group_list',$formGroupList);
       ///    $page->addData('badiu_form1_field_list',$formFieldList);
        // $page->addData('badiu_table1',$report->getTable());
        $page->setAdditionalContents($additionalContent);
        $page->setLinks($links);
          $page->setConfig($cpage);

        
        $themelayout=$cpage->getThemelayout(); 
        if(empty($themelayout)){$themelayout=$cform->getThemelayout(); }//review delete
        if(empty($themelayout)){$themelayout=$badiuSession->get()->getTheme().':Layout:column2.html.twig';}
        else{
            $pos=stripos($themelayout, ":");
            if($pos=== false){$themelayout=$badiuSession->get()->getTheme().':Layout:'.$themelayout;}
        }  
		$this->addlog($request,'view') ;		
        return $this->render($themelayout, array('page'=>$page));



    }
   
       public function linkwithoutsessionAction(Request $request) {
		 $this->iniSystemdata();
        //get key of module
        $this->initKeyManger($request);
       
	   $this->logtimestart=new \DateTime();
	   
        //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());

       
        $servicekey=$this->getCurrentkeyroute();
       
        if($this->has($servicekey)){
            $service=$this->get($servicekey);
            $function=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_function');
            if(empty($function)){$function="exec";}
            if(!method_exists($service,$function)){echo "There is no function $function in object $servicekey ";exit;}
            $service->$function();
        }
     
        return null;
   }
     
     public function linkAction(Request $request) {
		 $this->iniSystemdata();
		$this->forceLogin($request);
        //get key of module
        $this->initKeyManger($request);
       
	   $this->logtimestart=new \DateTime();
	   
        //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());

        //page
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
      
         //get session data
         $badiuSession=$this->get('badiu.system.access.session');
		$badiuSession->setSystemdata($this->getSystemdata());
        
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
            return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }

        $servicekey=$this->getCurrentkeyroute();
       
        if($this->has($servicekey)){
            $service=$this->get($servicekey);
            $function=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_function');
            if(empty($function)){$function="exec";}
            if(!method_exists($service,$function)){echo "There is no function $function in object $servicekey ";exit;}
			$this->addlog($request,'view') ;
            $service->$function();
        }
       $this->addlog($request,'view') ;
        return null;
   }
	public function linkParentAction(Request $request,$parentid) {
		 $this->iniSystemdata();
		$this->forceLogin($request);
        //get key of module
        $this->initKeyManger($request);
        $this->logtimestart=new \DateTime();
        //translator
        $translator=$this->get('translator');
		$translator->setSystemdata($this->getSystemdata());

        //page
        $page=$this->get('badiu.system.core.page');
		$page->setSystemdata($this->getSystemdata());
        $page->setOperation($translator->trans($this->getCurrentkeyroute()));
      
         //get session data
         $badiuSession=$this->get('badiu.system.access.session');
		$badiuSession->setSystemdata($this->getSystemdata());
        
        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getCurrentkeyroute())){
            return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
        }

        $servicekey=$this->getCurrentkeyroute();
        if($this->has($servicekey)){
            $service=$this->get($servicekey);
            $function=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_function');
            if(empty($function)){$function="exec";}
            if(!method_exists($service,$function)){echo "There is no function $function in object $servicekey ";exit;}
            $service->$function();
        }
        $this->addlog($request,'view') ;
        return null;
   }
   public function uploadAction(Request $request,$type) {
	 $this->iniSystemdata();
    //get key of module
    $this->initKeyManger($request);
   $this->logtimestart=new \DateTime();
    //translator
    $translator=$this->get('translator');
	$translator->setSystemdata($this->getSystemdata());

    //page
    $page=$this->get('badiu.system.core.page');
	$page->setSystemdata($this->getSystemdata());
    $page->setOperation($translator->trans($this->getCurrentkeyroute()));
  
     //get session data
     $badiuSession=$this->get('badiu.system.access.session');
	$badiuSession->setSystemdata($this->getSystemdata());
    
    //permission
    $permission=$this->get('badiu.system.access.permission');
    if(!$permission->has_access($this->getCurrentkeyroute())){
      //  review
       // return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
    }
    $muploadlib=$this->get('badiu.system.file.file.manageupload');
    $result=$muploadlib->single();

    $jsonsyncresponse=$this->get('badiu.system.core.lib.util.jsonsyncresponse');
    $result=$jsonsyncresponse->accept($result);
    $json =json_encode($result);
    $response = new Response($json);
    $response->headers->set('Content-Type', 'application/json');
	// $this->addlog($request,'create') ;
    return $response;
   
}

   public function downloadAction(Request $request,$code,$name) {
	 //$this->iniSystemdata();
    //get key of module
    $this->initKeyManger($request);
   
    //translator
    $translator=$this->get('translator');

    //page
    $page=$this->get('badiu.system.core.page');
    $page->setOperation($translator->trans($this->getCurrentkeyroute()));
  
     //get session data
     $badiuSession=$this->get('badiu.system.access.session');
	 //$badiuSession->setSystemdata($this->getSystemdata());
    
    //permission
    $permission=$this->get('badiu.system.access.permission');
    if(!$permission->has_access($this->getCurrentkeyroute())){
      //  return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
    }

    $flib=$this->get('badiu.system.file.file.lib');
   
    $response=$flib->downlaod($code);
    
	
   
    return $response;
}

public function serviceFileAction(Request $request,$filefp) {
        $this->iniSystemdata();
    //get key of   
    $this->initKeyManger($request);

    //get session data
    $badiuSession=$this->get('badiu.system.access.session');
    $badiuSession->setSystemdata($this->getSystemdata());

    //translator
    $translator=$this->get('translator');
	$translator->setSystemdata($this->getSystemdata());

    //permission  reivew
   /* $permission=$this->get('badiu.system.access.permission');
    if(!$permission->has_access($this->getCurrentkeyroute())){
        return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>new BadiuPage()));
    }*/

    //bundle config
    $cbundle=$this->get('badiu.system.core.lib.config.bundle');
	$cbundle->setSystemdata($this->getSystemdata());
    $cbundle->initKeymangerExtend($this->getKeymanger());
    $this->setKeymangerExtend($cbundle->getKeymangerExtend());

    //service of key maneger that do inherit key of service
    $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
    $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

    //controller
    if ($this->container->has($kminherit->controller())){
        $controller=$this->get($kminherit->controller());
    }else{
        $controller=$this->get('badiu.system.core.functionality.controller');
    }
    
    $result=$controller->serviceProcess();
     
    $response = new Response($result);
  //  $response->headers->set('Content-Type', 'application/json');
    return $response;
}


 public function webviewAction(Request $request) {
	 $this->iniSystemdata();
    //get key of module
    $this->initKeyManger($request);
   
    //translator
    $translator=$this->get('translator');
	$translator->setSystemdata($this->getSystemdata());

    //page
    $page=$this->get('badiu.system.core.page');
	$page->setSystemdata($this->getSystemdata());
    $page->setOperation($translator->trans($this->getCurrentkeyroute()));
  
     //get session data
     $badiuSession=$this->get('badiu.system.access.session');
     $badiuSession->setSystemdata($this->getSystemdata());
    
    //permission
    $permission=$this->get('badiu.system.access.permission');
    if(!$permission->has_access($this->getCurrentkeyroute())){
      //  return $this->render($badiuSession->get()->getTheme().':Layout:nopermission.html.twig',array('page'=>$page));
    }
	$bfile=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameter('bfile');
    $flib=$this->get('badiu.system.file.file.lib');
    $fparam=array('bundlepath'=>$bfile); //check tipe of file 
    $response=$flib->getContent($fparam); 
    
	
   
    return $response;
}
     private function initNavegationLogParam() {
                 $badiuSession = $this->getContainer()->get('badiu.system.access.session');
                $logparam=$badiuSession->get()->getWebserviceinfo();
                unset($logparam['cripty']);
                unset($logparam['criptykey']);
                unset($logparam['sessiondna']);
                $logparam['response']['status']='accept';
                $logparam['response']['info']='';
                $logparam['response']['message']='data successul send';
                $logparam['query']=$this->param;$this->log['ip']=$this->getUtildata()->getVaueOfArray($logparam,'client.ip',true);
                $this->log['dtype']='webservice';
               
                
             
                $logparam=$this->getJson()->encode($logparam);
                $this->log['param']= $logparam;
                
               
        }
     
      private function addlog($request,$action,$parentid=null) {
         //  if(!$this->log){return null;}
           
           $dloglib=$this->get('badiu.system.log.core.lib');
           $logtimeend=new \DateTime();
           if(!empty($this->logtimestart)){
               $start=$this->logtimestart->getTimestamp();
               $end=$logtimeend->getTimestamp();
               $timeexec=$end-$start; 
               $this->log['timeexec']=$timeexec;
           }
           if(!isset($this->log['dtype'])){$this->log['dtype']='web';}
           if(!isset($this->log['clientdevice'])){$this->log['clientdevice']='server';}
           if(!isset($this->log['modulekey'])){$this->log['modulekey']=$this->keymanger->getBaseKey();}
           if(!isset($this->log['moduleinstance'])){$this->log['moduleinstance']=$parentid;}
          if(!isset($this->log['functionalitykey'])){ $this->log['functionalitykey']=$this->getCurrentkeyroute(); }
           
           if(!isset($this->log['action'])){$this->log['action']=$action;}
           if(!isset($this->log['rkey'])){ $this->log['rkey']=null;}
           if(!isset($this->log['ip'])){$this->log['ip']=$_SERVER["REMOTE_ADDR"];   }
           
           $badiuSession = $this->get('badiu.system.access.session');
           if(!isset($this->log['param'])){
               $param=array();
               $client=array('browser'=>$_SERVER['HTTP_USER_AGENT'],'ip'=>$_SERVER["REMOTE_ADDR"]);
               $param['client']=$client;
               $accessby=$badiuSession->get()->getAccessby();
               if(!empty($accessby)){$accessby=(array)$accessby;}
               $user=array('firstname'=>$badiuSession->get()->getUser()->getFirstname(),
                            'lastname'=>$badiuSession->get()->getUser()->getLastname(),
                            'id'=>$badiuSession->get()->getUser()->getId(),
                            'email'=>$badiuSession->get()->getUser()->getEmail(),
                            'roles'=>$badiuSession->get()->getRoles(),
                            'accessby'=>$accessby,
                        );
               $param['user']=$user;
             $sserver=array();
             
             $key=$this->log['functionalitykey']=$this->getCurrentkeyroute();
              $pos=stripos($key, "badiu.moodle.mreport");
               
              $serviceid=$request->query->get('_serviceid');
              if($pos!== false && $serviceid==$parentid && !empty($parentid) && $this->has('badiu.admin.server.service.data')){
                  $servicedata = $this->get('badiu.admin.server.service.data');
                  $info=$servicedata->getInfoById($serviceid);
                  $utildata= $this->get('badiu.system.core.lib.util.data');
                  $sserver['id']=$serviceid;
                  $sserver['url']=$utildata->getVaueOfArray($info,'url');
                  $sserver['name']=$utildata->getVaueOfArray($info,'name');
                  $sserver['entity']=$utildata->getVaueOfArray($info,'entity');
                 $param['sserver']=$sserver; 
            }
            
            
              $query=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
              if(!isset($query['parentid']) && !empty($parentid)){$query['parentid']=$parentid;}
              $param['query']=$query; 
             $json = $this->get('badiu.system.core.lib.util.json');
                $logparam=$json->encode($param);
                $this->log['param']= $logparam;
            
            }
           
            
           $this->log['entity']= $badiuSession->get()->getEntity();
           $this->log['userid']= $badiuSession->get()->getUser()->getId();
           
           $dloglib->add($this->log);
           
           
       }

    public function initFreportDynamic($fparam=null) {
		if(!$this->has('badiu.sync.freport.freport.lib.factoryservicereportdynamickey')){return null;}
		$factorysdk=$this->get('badiu.sync.freport.freport.lib.factoryservicereportdynamickey');
		 
		
		if(empty($fparam)){$fparam=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameters();}
		$freportid=$this->getUtildata()->getVaueOfArray($fparam, '_freportid');
		$force = $this->getUtildata()->getVaueOfArray($fparam, '_force');
		$dkey = $this->getUtildata()->getVaueOfArray($fparam, '_dkey');
		if(empty($dkey)){$dkey=$this->get('request')->get('_route');}
	   
		$faparam=array('_freportid'=>$freportid,'_dkey'=>$dkey,'_force'=>$force);
		$factorysdk->initSession($faparam);
    } 

	public function initFreportChangeRoute($fparam=null) {
		if(!$this->has('badiu.sync.freport.freport.lib.factoryservicereportdynamickey')){return null;}
		$factorysdk=$this->get('badiu.sync.freport.freport.lib.factoryservicereportdynamickey');
		 
		if(empty($fparam)){$fparam=$this->get('badiu.system.core.lib.http.querystringsystem')->getParameters();}
		$freportid=$this->getUtildata()->getVaueOfArray($fparam, '_freportid');
		$dkey = $this->getUtildata()->getVaueOfArray($fparam, '_dkey');
		$fdkey = $this->getUtildata()->getVaueOfArray($fparam, '_fdkey');
		$force = $this->getUtildata()->getVaueOfArray($fparam, '_force');
		if(empty($dkey)){return null;}
	    if(empty($fdkey)){return null;}
	   
		$faparam=array('_freportid'=>$freportid,'_dkey'=>$dkey,'_fdkey'=>1,'_force'=>$force);
		$nk=$factorysdk->initSessionOfDynmickey($faparam);
		return $nk;
    } 	
    public function getKeymanger() {
        return $this->keymanger;
    }

    public function setKeymanger(BadiuKeyManger $keymanger) {
        $this->keymanger = $keymanger;
    }

    /**
     * @return mixed
     */
    public function getDatalayout()
    {
        return $this->datalayout;
    }

    /**
     * @param mixed $datalayout
     */
    public function setDatalayout($datalayout)
    {
        $this->datalayout = $datalayout;
    }

    /**
     * @return mixed
     */
    public function getKeymangerExtend()
    {
        return $this->keymangerExtend;
    }

    /**
     * @param mixed $keymangerExtend
     */
    public function setKeymangerExtend(BadiuKeyManger $keymangerExtend)
    {
        $this->keymangerExtend = $keymangerExtend;
    }



    function getLogtimestart() {
        return $this->logtimestart;
    }

    function setLogtimestart($logtimestart) {
        $this->logtimestart = $logtimestart;
    }

    function getLog() {
        return $this->log;
    }

    function setLog($log) {
        $this->log = $log;
    }

    function getUtildata() {
        return $this->utildata;
    }

    function setUtildata($utildata) {
        $this->utildata = $utildata;
    }
    function getCurrentkeyroute() {
        return $this->currentkeyroute;
    }

    function setCurrentkeyroute($currentkeyroute) {
        $this->currentkeyroute = $currentkeyroute;
    }
    
    function getDynamicroute() {
        return $this->dynamicroute;
    }

    function setDynamicroute($dynamicroute) {
        $this->dynamicroute = $dynamicroute;
    }
   
public function getSystemdata()
    {
      	return $this->systemdata;
    }

    public function setSystemdata($systemdata)
    {
        $this->systemdata = $systemdata;
    }   
}
