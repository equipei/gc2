<?php
namespace Badiu\System\ComponentBundle\Controller;
use Badiu\System\ComponentBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Tbldate controller.
 *
 * @Route("/componente")
 */
class BadiudFilterHour2ChoiceController extends Controller {

/**
 * @Route("/hora")
 * @Template("BadiuSystemComponentBundle:Default:index.html.twig")
 */

	public function indexAction() {

		$user = new User();
		$form = $this->createFormBuilder($user)
			->add('Validade', 'badiudfilterhour2choice', array(
				//pode passar valores aqui de escolha dinamicamente
				'choices1' => array("1" => "00", '2' => "01", '3' => "02", '4' => "03", '5' => "05", '6' => "06", '7' => "07", "8" => "08", '9' => "09", '10' => "10", '11' => "11", '12' => "12", '13' => "13", '14' => "14", "15" => "15", '16' => "16", '17' => "17", '18' => "18", '19' => "19", '20' => "20", '21' => "21", '22' => "22", '23' => "23"),
				//opcoes do label, caso nao for definido existe por default
				'labelchoice1' => 'Seleciona hora',

				'choices2' => array("1" => "00", '2' => "01", '3' => "02", '4' => "03", '5' => "05", '6' => "06", '7' => "07", "8" => "08", '9' => "09", '10' => "10", '11' => "11", '12' => "12", '13' => "13", '14' => "14", "15" => "15", '16' => "16", '17' => "17", '18' => "18", '19' => "19", '20' => "20", '21' => "21", '22' => "22", '23' => "23"),
				//opcoes do label, caso nao for definido existe por default
				'labelchoice2' => 'Seleciona minuto',
			))

			->getForm();

		return array('form' => $form->createView());
	}
}
