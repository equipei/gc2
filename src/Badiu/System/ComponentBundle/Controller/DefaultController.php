<?php

namespace Badiu\System\ComponentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {
	public function indexAction() {
		$fbuilder = array();
		$form = $this->createFormBuilder($fbuilder)
		// ->add('cpf', 'text',array('label'=>$translator->trans('cpf'),'required'  => true))
			->add('lino', 'text', array('label' => 'Login', 'required' => true))
			->add('humberto', 'password', array('label' => 'Senha', 'required' => true))
		// ->add('save', 'submit')
			->add('d1', 'date')
		/*->add('taya', 'badiuchoice2text', array(

			                  //pode passar valores aqui de escolha dinamicamente
			                 'choices'=> array("cvt"=>"Cv Telecom ",'unit'=>"Unitel T+"),

			                 //opcoes do label, caso nao for definido existe por default
			                 'labelchoice'=>'Seleciona Operadora',
			                 'labeltext1'=>'DDD',
			                 'labeltext2'=>'Telefone'

		*/
			->getForm();

		return $this->render('BadiuSystemComponentBundle:Default:index.html.twig', array('form' => $form->createView()));
	}
}
