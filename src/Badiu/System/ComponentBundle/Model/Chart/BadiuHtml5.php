<?php

namespace Badiu\System\ComponentBundle\Model\Chart;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\ComponentBundle\Model\Chart\BadiuChart;

class BadiuHtml5 extends BadiuChart {

    function __construct(Container $container) {
        parent::__construct($container);
    }
// 
    public function progressbar($x,$y,$translatorkey=null) {
        $text=null;
        $perc=0;
        if($y > 0){$perc= $x*100/ $y;}
        $percbar=$perc;
        //format
        if($perc > 0){
             $k=$perc/2;
            if($k != round($k)){
               $perc=number_format($perc, 2, ',', '');
            }
        }
        if(!empty($translatorkey)){$text=$this->getTranslator()->trans($translatorkey,array('%currentvalue%'=>$x,'%total%'=>$y,'%percentage%'=>$perc));}
       
           $with=$this->width;
            if(!empty($text)){$text.="<br />";}
            $html = <<< eof
                $text
                <progress  style=" width: $with%" value="$percbar" max="100"></progress><br />

eof;
        return  $html;
      }

      
      public function templete1($data,$keytranslator) {
        
        $row = $this->getUtildata()->getVaueOfArray($data,'badiu_list_data_row');
        $y=$this->getUtildata()->getVaueOfArray($row,1);
        $y=$this->getUtildata()->getVaueOfArray($y,'countrecord');
        $x=$this->getUtildata()->getVaueOfArray($data,'badiu_table1_count');
    
        $progressbar=$this->progressbar($x,$y,$keytranslator);
        return  $progressbar;
      
      }
}
