<?php

namespace Badiu\System\ComponentBundle\Model\Chart\Template;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportContent;
class Template1Html5Progressbar extends BadiuReportContent {
private $keytranslator;
function __construct(Container $container,$keytranslator) {
    parent::__construct($container);
    $this->keytranslator=$keytranslator;
}

public function head() {
     $progressbar=$this->getContainer()->get('badiu.system.component.chart.html5')->templete1($this->getData(),$this->keytranslator);
     return  $progressbar;

      
      }
      function getKeytranslator() {
          return $this->keytranslator;
      }

      function setKeytranslator($keytranslator) {
          $this->keytranslator = $keytranslator;
      }

      
   
}

