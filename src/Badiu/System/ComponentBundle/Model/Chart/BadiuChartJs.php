<?php

namespace Badiu\System\ComponentBundle\Model\Chart;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\ComponentBundle\Model\Chart\BadiuChart;

class BadiuChartJs extends BadiuChart {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function horizontalbar() {
        $html = <<< eof
            <style type="text/css">
                .badiu-dashboard-cdreporter{
                    margin-bottom:  15px;
                }
            </style>
           <div class="badiu-dashboard-cdreporter">
                <canvas id="bar-chart-horizontal" width="150" height="20"></canvas>
                 <script>
                   new Chart(document.getElementById("bar-chart-horizontal"), {
    type: 'horizontalBar',
    data: {
      labels: [$this->labels],
      datasets: [
        {
          label: "$this->titleinfo",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: [$this->values]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: "$this->title"
      }
    }
});
                 </script>
                 </div>
                     <br />
eof;
        return $html;
    }

}
