<?php

namespace Badiu\System\ComponentBundle\Model\Chart;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class BadiuChart extends BadiuModelLib{

      /**
     * @var string
     */
    public $title;
    
      /**
     * @var string
     */
    public $titleinfo;
       /**
     * @var string
     */
    public $labels;
    
       /**
     * @var string
     */
    public $values;
    
       /**
     * @var string
     */
    public $colors="";
    
       /**
     * @var string
     */
    public $classid="";
    
       /**
     * @var integer
     */
    public $width;
    
  
         /**
     * @var integer
     */
   public $height;
   
         /**
     * @var string
     */
    private $defaultlib;
    
    function __construct(Container $container) {
            parent::__construct($container);
            $this->title="";
            $this->titleinfo="";
            $this->labels="";
            $this->values="";
            $this->colors ='"#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3cba9f", "#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f"';
            $this->classid=time();
            $this->width=100;
            $this->height=10;
            $this->defaultlib='badiu.system.component.chart.chartjs';
        }
   
        function getTitle() {
            return $this->title;
        }

        function getLabels() {
            return $this->labels;
        }

        function getValues() {
            return $this->values;
        }

        function setTitle($title) {
            $this->title = $title;
        }

        function setLabels($labels) {
            $this->labels = $labels;
        }

        function setValues($values) {
            $this->values = $values;
        }

        function getColors() {
            return $this->colors;
        }

        function getClassid() {
            return $this->classid;
        }

        function setColors($colors) {
            $this->colors = $colors;
        }

        function setClassid($classid) {
            $this->classid = $classid;
        }

        function getWidth() {
            return $this->width;
        }

        function getHeight() {
            return $this->height;
        }

        function setWidth($width) {
            $this->width = $width;
        }

        function setHeight($height) {
            $this->height = $height;
        }

        function getDefaultlib() {
            return $this->defaultlib;
        }

        function setDefaultlib($defaultlib) {
            $this->defaultlib = $defaultlib;
        }


        function getTitleinfo() {
            return $this->titleinfo;
        }

        function setTitleinfo($titleinfo) {
            $this->titleinfo = $titleinfo;
        }


}
