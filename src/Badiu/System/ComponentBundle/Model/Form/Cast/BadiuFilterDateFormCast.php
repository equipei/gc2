<?php

namespace Badiu\System\ComponentBundle\Model\Form\Cast;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Date\DOMAINTABLE;
class BadiuFilterDateFormCast {

    public function changeParam($filter) {
        $formcastdate=$this->getContainer()->get('badiu.system.core.lib.form.castdate');
        $newfilter=array();
        $isparamofhtmlform=true;
        foreach ($filter as $key => $value) {
           if(is_array($value)) {
               if(array_key_exists("badiudateoperator",$value) && array_key_exists("badiudate1",$value)&& array_key_exists("badiudate2",$value)){
                  $isparamofhtmlform=false;
                   $operator=$value['badiudateoperator'];
                   $operatoraux=null;
                   $badiudate1=$value['badiudate1'];
                   $badiudate2=$value['badiudate2'];
                   
                 /* if($operator==DOMAINTABLE::$OPERATOR_TEXT_ON){
                      $operator=DOMAINTABLE::$OPERATOR_TEXT_BETWEEN;
                      if(!empty($badiudate1)) {
                          $badiudate1->setTime(0, 0, 0);
                          $badiudate2=$value['badiudate1'];
                          $badiudate2->setTime(23, 59, 59);
                      }
                  }*/
                 if($operator !=DOMAINTABLE::$OPERATOR_TEXT_BETWEEN){$badiudate2=null;}
                   
                   $keyoperator="::".$key."operator";
                   $keybadiudate1=$key."1";
                   $keybadiudate2=$key."2";
                   $matoperator=$this->getOperator($operator);
                   
                   $newfilter[$keyoperator]=$matoperator;
                   $newfilter[$keybadiudate1]=$badiudate1;
                   $newfilter[$keybadiudate2]=$badiudate2;
                   
               }else{
                
                    $newfilter[$key]=$value;
               }
           }else{
               $newfilter[$key]=$value;
           }
        }
        //html form
        if($isparamofhtmlform){
           $newfilter=array();
           $operationf=null;
           $operationaux=null;
           $keydate1=null;
           $keydate2=null;
           
           
           foreach ($filter as $key => $value) {
                $pos=stripos($key, "_badiudateoperator");
                if($pos!== false){
                    $originalkey=$key;
                    $key="::".$key;
                    $operationaux=$value;
                    $value=$this->getOperator($value);
                    $operationf=$value;
                    //set value of date1 and date2 for star and end day
                     if($operationaux==DOMAINTABLE::$OPERATOR_TEXT_ON){
                         $p=explode("_",$originalkey);
                         if(sizeof($p)==2){
                             $bskey=$p[0];
                             $time1=$filter[$bskey.'_badiudate1'];
                             if(!empty($time1)){
                                 //$time1=date_create_from_format('d/m/Y', $time1);
                                 $time1=$formcastdate->convertToDbDate($time1);
                                 $time2=clone $time1;
                                 $time1->setTime(0, 0, 0);
                                 $time2->setTime(23, 59, 59);
                                 $newfilter[$bskey.'1']=$time1;
                                  $newfilter[$bskey.'2']=$time2;
                             }
                             
                         }
                         
                     }
                }
               
                //change date
                $posd1=stripos($key, "_badiudate1");
                $posd2=stripos($key, "_badiudate2");
                if($posd1!== false || $posd2!== false){
                  
                    if(!empty($value)){
                        //$value= date_create_from_format('d/m/Y', $value);
                        $value=$formcastdate->convertToDbDate($value);
                    }
                }
                $key= str_replace("_badiudate","",$key);
                 if(!array_key_exists($key,$newfilter)){$newfilter[$key]=$value;}
                
           }
           //clean date2
          // if( $operationf !=DOMAINTABLE::$OPERATOR_TEXT_BETWEEN){$newfilter[$keydate2]=null;}
        }
       
        
        return $newfilter;
    }
   
    public function changeSql($filter,$sql) {
        foreach ($filter as $key => $value) {
            $pos = stripos($key, "::");
            if ($pos !== false){
                $sql = str_replace($key, $value,$sql);
            }
        }
        return $sql;
    }
    
    public function getOperator($operator) {
        $op=" > ";
        if($operator==DOMAINTABLE::$OPERATOR_TEXT_FROM){$op=" >= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_AFTER){$op=" > ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_UNTIL){$op=" <= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_BEFORE){$op=" < ";}
       // else if($operator==DOMAINTABLE::$OPERATOR_TEXT_ON){$op=" = ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_ON){$op=" >= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_DIFFERENT){$op=" != ";}
        else if($operator==DOMAINTABLE::$OPERATOR_TEXT_BETWEEN){$op=" >= ";}
        return $op;
    }
    
}
