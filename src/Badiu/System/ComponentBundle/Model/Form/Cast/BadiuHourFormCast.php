<?php

namespace Badiu\System\ComponentBundle\Model\Form\Cast;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class BadiuHourFormCast {

   public function convertToForm($fminutes) {
   
       $hour=null;
       $minutes=null;
        if(!empty($fminutes)){
           $hour=$fminutes/60;
           $hour=floor($hour);
            $minutes=$fminutes % 60;
       }
        $formhour=array();
        $formhour['badiuhour']=$hour;
        $formhour['badiuminute']=$minutes;
        return $formhour;
   }

   public function convertFromForm($form) {
        $fminutes=null;
        $hour=null;
        $minutes=null;
        if(isset($form['badiuhour'])){$hour=$form['badiuhour'];}
        if(isset($form['badiuminute'])){$minutes=$form['badiuminute'];}
        $mtime=$hour*60;
        $mtime=$mtime+$minutes;
        $fminutes = $mtime;
        return $fminutes;
   }   
}
