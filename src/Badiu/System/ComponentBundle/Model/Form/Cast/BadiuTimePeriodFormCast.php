<?php

namespace Badiu\System\ComponentBundle\Model\Form\Cast;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Date\DOMAINTABLE;
class BadiuTimePeriodFormCast {

   public function convertToForm($value) {
   
        return null;
   }

   public function convertFromForm($form,$timeback=true) {
        $number=null;
        $unittime=null;
        $operator="-";
       
        if(!$timeback){$operator="+";}
        if(isset($form['number'])){$number=$form['number'];}
        if(isset($form['unittime'])){$unittime=$form['unittime'];}
        
        $now=null;
        if($number >0 && !empty($unittime)){
             $now =new \Datetime();
          
             if($unittime==DOMAINTABLE::$TIME_UNIT_SECOND){$now->modify("$operator $number seconds ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_MINUTE){$now->modify("$operator $number minutes ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_HOUR){$now->modify("$operator $number hours ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_DAY){$now->modify("$operator $number day ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_WEEK){ $number=$number*7;$now->modify("$operator $number day ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_MONTH){$now->modify("$operator $number month ");}
             else if($unittime==DOMAINTABLE::$TIME_UNIT_YEAR){$number=$number*12; $now->modify("$operator $number month ");}
        }
        
        return $now;
   }
 
public function changeParam($filter,$timeback=true) {

        $newfilter=array();
        $isparamofhtmlform=false;
        $newfilter=array();
      
           foreach ($filter as $key => $value) {
               
                $form=array();
                $pos=stripos($key, "_badiutimeperiodunittime");
                if($pos!== false){
                  
                    $isparamofhtmlform=true;
                   $p= explode("_", $key);
                   
                   $keynumber=null;
                   $number=null;
                   $keyfield=null;
                   if(isset($p[0])){
                       $keyfield=$p[0];
                       $keynumber=$keyfield.'_badiutimeperiodnumber';
                       if (array_key_exists($keynumber,$filter)){$number=$filter[$keynumber];}
                   }
                   $form['unittime']=$value;
                   $form['number']=$number;
                   $timeback=$this->isTimeBack($keyfield);
                   $value=$this->convertFromForm($form,$timeback);
                  // echo $keyfield;print_r($value);
                  
                    $newfilter[$keyfield]=$value;
                   if(!empty($value)){ $newfilter[$keyfield."now"]=new \Datetime();}
                }else{
                    if(!array_key_exists($key,$newfilter)){$newfilter[$key]=$value;}
            
                }
               
           }
        
     
         if(!$isparamofhtmlform){
            
             
             foreach ($filter as $key => $value) {
        
           if(!empty($value) && is_array($value)) {
               if(array_key_exists("unittime",$value) && array_key_exists("number",$value)){
                    $timeback=$this->isTimeBack($key);
                   
                   $newvalue=$this->convertFromForm($value,$timeback);
                     $newfilter[$key]=$newvalue;
                     if(!empty($newvalue)){ $newfilter[$key."now"]=new \Datetime();}
                 }
                 
           }else{
               $newfilter[$key]=$value;
           }
        }
         }
       
      
        return $newfilter;
    }  
    
public function isTimeBack($key){
    
    $pos=strrpos($key,"will");
    if ($pos !== false && strlen("will") + $pos == strlen($key)){
      
        return false;
    }
   
    return true;
}
            
}
