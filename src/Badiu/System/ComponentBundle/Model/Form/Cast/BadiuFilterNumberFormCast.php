<?php

namespace Badiu\System\ComponentBundle\Model\Form\Cast;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Math\DOMAINTABLE;
class BadiuFilterNumberFormCast {

    public function changeParam($filter) {
        $newfilter=array();
         $isparamofhtmlform=true;
        foreach ($filter as $key => $value) {
           if(is_array($value)) {
                if(array_key_exists("badiunumberoperator",$value) && array_key_exists("badiunumber1",$value)&& array_key_exists("badiunumber2",$value)){
                   $operator=$value['badiunumberoperator'];
                   $badiunumber1=$value['badiunumber1'];
                   $isparamofhtmlform=false;
                 if($operator !=DOMAINTABLE::$OPERATOR_MATH_BETWEEN){$badiunumber2=null;}
                   
                   $keyoperator="::".$key."operator";
                   $keybadiunumber1=$key."1";
                   $keybadiunumber2=$key."2";
                   $matoperator=$this->getOperator($operator);
                   
                   $newfilter[$keyoperator]=$matoperator;
                   $newfilter[$keybadiunumber1]=$badiunumber1;
                   $newfilter[$keybadiunumber2]=$badiunumber2;
                   
               }else{
                    $newfilter[$key]=$value;
               }
           }else{
               $newfilter[$key]=$value;
           }
        }
        
        //html form
        if($isparamofhtmlform){
            foreach ($filter as $key => $value) {
                 $pos=stripos($key, "_badiunumberoperator");
                if($pos!== false){
                    $key="::".$key;
                    $value=$this->getOperator($value);
               }else{
                    $newfilter[$key]=$value;
               }
            $key= str_replace("_badiunumber","",$key);
            $newfilter[$key]=$value;
            }
        }
   
        return $newfilter;
    }
   
    public function changeSql($filter,$sql) {
        foreach ($filter as $key => $value) {
            $pos = stripos($key, "::");
            if ($pos !== false){
                $sql = str_replace($key, $value,$sql);
            }
        }
        return $sql;
    }
    
    public function getOperator($operator) {
        $op=" > ";
        if($operator==DOMAINTABLE::$OPERATOR_MATH_BIGGER){$op=" > ";}
        else if($operator==DOMAINTABLE::$OPERATOR_MATH_BIGGEROREQUAL){$op=" >= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_MATH_LESS){$op=" < ";}
        else if($operator==DOMAINTABLE::$OPERATOR_MATH_LESSOREQUAL){$op=" <= ";}
        else if($operator==DOMAINTABLE::$OPERATOR_MATH_EQUAL){$op=" = ";}
        else if($operator==DOMAINTABLE::$OPERATOR_MATH_DIFFERENT){$op=" != ";}
        else if($operator==DOMAINTABLE::$OPERATOR_MATH_BETWEEN){$op=" >= ";}
        return $op;
    }
    
}
