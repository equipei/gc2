<?php

namespace Badiu\System\ComponentBundle\Model\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BadiudFilterHour2ChoiceFormType extends AbstractType {
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		$builder
		//trata o campo do tipo choice
		->add('labelchoice1', 'choice', array(
			'choices' => $options['choices1'],
			'empty_value' => $options['labelchoice1'],
		))
			->add('labelchoice2', 'choice', array(
				'choices' => $options['choices2'],
				'empty_value' => $options['labelchoice2'],
			));
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildView(FormView $view, FormInterface $form, array $options) {
		$view->vars['widget'] = $options['widget'];
	}

	//Aqui sao definidos As opcoes dos labels por default
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$compound = function (Options $options) {
			return $options['widget'] !== 'single_text';
		};

		$resolver->setDefaults(array(
			'input' => 'badiudfilterhour2choicetype',
			'widget' => null,
			'compound' => $compound,
			'labelchoice1' => "----",
			'labelchoice2' => "----",
		));

		// Don't add some defaults in order to preserve the defaults
		// set in DateType and TimeType
		$resolver->setOptional(array(
			'choices1',
			'choices2',
		));

		$resolver->setAllowedValues(array(
			'input' => array(
				'badiudfilterhour2choicetype',
			),
			// This option will overwrite "date_widget" and "time_widget" options
			'widget' => array(
				null, // default, don't overwrite options
				'text',
				'choice',
			),
		));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getName() {
		return 'badiudfilterhour2choice';
	}

}
