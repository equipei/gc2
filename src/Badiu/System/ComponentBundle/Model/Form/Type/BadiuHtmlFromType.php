<?php

namespace Badiu\System\ComponentBundle\Model\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BadiuHtmlFromType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
              
                ->add('badiuhtml', 'text', array(
                    'label' => $options['badiuhtml']
                        ) 
                );
             
        
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['widget'] = $options['widget'];
    }

    //Aqui sao definidos As opcoes dos labels por default
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $compound = function (Options $options) {
            return $options['widget'] !== 'single_text';
        };

        $resolver->setDefaults(array(
            'input' => 'badiuhtml',
            'widget' => null,
            'compound' => $compound,
            'badiuhtml' => "badiuhtml",
          
        ));

        // Don't add some defaults in order to preserve the defaults
        // set in DateType and TimeType
        $resolver->setOptional(array(
            'badiuhtal',
            'choices'
        ));

        $resolver->setAllowedValues(array(
            'input' => array(
                'badiuhtml',
            ),
            // This option will overwrite "date_widget" and "time_widget" options
            'widget' => array(
                null, // default, don't overwrite options
                'text',
                
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'badiuhtml';
    }

}
