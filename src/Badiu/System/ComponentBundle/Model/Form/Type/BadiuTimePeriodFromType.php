<?php

namespace Badiu\System\ComponentBundle\Model\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BadiuTimePeriodFromType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                //trata o campo do tipo choice
                
                //trata o campo do tipo text1
                ->add('number', 'text', array(
                    'label' => $options['number'],
                    //'data' =>  $options['number'],
                        ) 
                )
                ->add('unittime', 'choice', array(
                    'choices' => $options['choices'],
                   // 'data' =>  $options['unittime'],
                    //'empty_value' => $options['badiuminute']
                        )
                );
             
        
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['widget'] = $options['widget'];
    }

    //Aqui sao definidos As opcoes dos labels por default
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $compound = function (Options $options) {
            return $options['widget'] !== 'single_text';
        };

        $resolver->setDefaults(array(
            'input' => 'badiutimeperiod',
            'widget' => null,
            'compound' => $compound,
            'number' => "number",
            'unittime' => "unittime",
        ));

        // Don't add some defaults in order to preserve the defaults
        // set in DateType and TimeType
        $resolver->setOptional(array(
            'number',
            'nittime',
            'choices'
        ));

        $resolver->setAllowedValues(array(
            'input' => array(
                'badiutimeperiod',
            ),
            // This option will overwrite "date_widget" and "time_widget" options
            'widget' => array(
                null, // default, don't overwrite options
                'text',
                'choice',
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'badiutimeperiod';
    }

}
