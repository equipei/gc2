<?php

namespace Badiu\System\ComponentBundle\Model\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BadiuHourFromType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                //trata o campo do tipo choice
                
                //trata o campo do tipo text1
                ->add('badiuhour', 'text', array(
                    'label' => $options['badiuhour']
                        ) 
                )
                ->add('badiuminute', 'choice', array(
                    'choices' => $options['choices'],
                    //'empty_value' => $options['badiuminute']
                        )
                );
             
        
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['widget'] = $options['widget'];
    }

    //Aqui sao definidos As opcoes dos labels por default
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $compound = function (Options $options) {
            return $options['widget'] !== 'single_text';
        };

        $resolver->setDefaults(array(
            'input' => 'badiuhour',
            'widget' => null,
            'compound' => $compound,
            'badiuhour' => "badiuhour",
            'badiuminute' => "badiuminute2",
        ));

        // Don't add some defaults in order to preserve the defaults
        // set in DateType and TimeType
        $resolver->setOptional(array(
            'badiuhour',
            'badiuminute',
            'choices'
        ));

        $resolver->setAllowedValues(array(
            'input' => array(
                'badiuhour',
            ),
            // This option will overwrite "date_widget" and "time_widget" options
            'widget' => array(
                null, // default, don't overwrite options
                'text',
                'choice',
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'badiuhour';
    }

}
