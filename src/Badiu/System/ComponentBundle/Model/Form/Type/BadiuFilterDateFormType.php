<?php

namespace Badiu\System\ComponentBundle\Model\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BadiuFilterDateFormType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                //trata o campo do tipo choice
                ->add('badiudateoperator', 'choice', array(
                    'choices' => $options['choices'],
                     'required'=>false,
                    'data' => 'from',
                    'empty_value' => $options['labelbadiudateoperator']
                        )
                )
                //trata o campo do tipo date1
                ->add('badiudate1', 'date', array(
                    'label' => $options['labelbadiudate1'],
                     'required'=>false,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => $options['attr'],
                        )
                ) 
                //trata campo do tipo date2
                ->add('badiudate2',  'date', array(
                    'label' => $options['labelbadiudate2'],
                    'required'=>false,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => $options['attr'],
                        )
                )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['widget'] = $options['widget'];
    }

    //Aqui sao definidos As opcoes dos labels por default
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $compound = function (Options $options) {
            return $options['widget'] !== 'single_date';
        };

        $resolver->setDefaults(array(
            'input' => 'badiufilterdate',
            'widget' => null,
            'compound' => $compound,
            'labelbadiudateoperator' => "----",
            'labelbadiudate1' => "badiudate1",
            'labelbadiudate2' => "badiudate2",
        ));

        // Don't add some defaults in order to preserve the defaults
        // set in DateType and TimeType
        $resolver->setOptional(array(
            'badiudateoperator',
            'badiudate1',
            'badiudate2',
            'choices'
        ));

        $resolver->setAllowedValues(array(
            'input' => array(
                'badiufilterdate',
            ),
            // This option will overwrite "date_widget" and "time_widget" options
            'widget' => array(
                null, // default, don't overwrite options
                'choice',
                'date',
                'date'
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'badiufilterdate';
    }

}
