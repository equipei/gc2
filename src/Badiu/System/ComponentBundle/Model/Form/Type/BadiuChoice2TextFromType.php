<?php

namespace Badiu\System\ComponentBundle\Model\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BadiuChoice2TextFromType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                //trata o campo do tipo choice
                ->add('field1', 'choice', array(
                    'choices' => $options['choices'],
                    'empty_value' => $options['labelfield1']
                        )
                )
                //trata o campo do tipo text1
                ->add('field2', 'text', array(
                    'label' => $options['labelfield2']
                        )
                )
                //trata campo do tipo text2
                ->add('field3', 'text', array(
                    'label' => $options['labelfield3']
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['widget'] = $options['widget'];
    }

    //Aqui sao definidos As opcoes dos labels por default
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $compound = function (Options $options) {
            return $options['widget'] !== 'single_text';
        };

        $resolver->setDefaults(array(
            'input' => 'badiuchoice2texttype',
            'widget' => null,
            'compound' => $compound,
            'labelfield1' => "----",
            'labelfield2' => "field2",
            'labelfield3' => "field3",
        ));

        // Don't add some defaults in order to preserve the defaults
        // set in DateType and TimeType
        $resolver->setOptional(array(
            'field1',
            'field2',
            'field3',
            'choices'
        ));

        $resolver->setAllowedValues(array(
            'input' => array(
                'badiuchoice2texttype',
            ),
            // This option will overwrite "date_widget" and "time_widget" options
            'widget' => array(
                null, // default, don't overwrite options
                'text',
                'choice',
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'badiuchoice2text';
    }

}
