<?php

namespace Badiu\System\ComponentBundle\Model\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BadiuFilterNumberFormType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                //trata o campo do tipo choice
                ->add('badiunumberoperator', 'choice', array(
                    'choices' => $options['choices'],
                    'data'=>'biggerorequal',
                    'required'=>false,
                    'empty_value' => $options['labeloperator'],
                     'required'=>false,
                    'attr' => $options['attr']
                        )
                )
                 ->add('badiunumber1', 'text', array(
                    'label' => $options['labelnumber1'],
                     'attr' => $options['attr'],
                      'required'=>false
                        )
                )
                 ->add('badiunumber2', 'text', array(
                    'label' => $options['labelnumber2'],
                     'attr' => $options['attr'],
                      'required'=>false
                        )
                )
               
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['widget'] = $options['widget'];
    }

    //Aqui sao definidos As opcoes dos labels por default
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $compound = function (Options $options) {
            return $options['widget'] !== 'single_text';
        };

        $resolver->setDefaults(array(
            'input' => 'badiufilternumber',
            'widget' => null,
            'compound' => $compound,
            'labeloperator' => "----",
            'labelnumber1' => "badiunumber1",
            'labelnumber2' => "badiunumber2",
        ));

        // Don't add some defaults in order to preserve the defaults
        // set in DateType and TimeType
        $resolver->setOptional(array(
            'badiunumberoperator',
            'badiunumber1',
            'badiunumber2',
            'choices'
        ));

        $resolver->setAllowedValues(array(
            'input' => array(
                'badiufilternumber',
            ),
            // This option will overwrite "date_widget" and "time_widget" options
            'widget' => array(
                null, // default, don't overwrite options
                'text',
                'choice'
               
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'badiufilternumber';
    }

}
