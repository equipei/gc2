<?php

namespace Badiu\System\ComponentBundle\Model\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BadiuTimeHourFromType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                //trata o campo do tipo choice
                
                //trata o campo do tipo text1
                ->add('badiutimehour', 'choice', array(
                    'choices' => $options['choices']['hours'],
                    ) 
                )
                ->add('badiutimeminute', 'choice', array(
                    'choices' => $options['choices']['minutes'],
                   )
                );
             
        
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['widget'] = $options['widget'];
    }

    //Aqui sao definidos As opcoes dos labels por default
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $compound = function (Options $options) {
            return $options['widget'] !== 'single_text';
        };

        $resolver->setDefaults(array(
            'input' => 'badiutimehour',
            'widget' => null,
            'compound' => $compound,
            'badiutimehour' => "badiutimehour",
            'badiutimeminute' => "badiutimeminute",
        ));

        // Don't add some defaults in order to preserve the defaults
        // set in DateType and TimeType
        $resolver->setOptional(array(
            'badiutimehour',
            'badiutimeminute',
            'choices'
        ));

        $resolver->setAllowedValues(array(
            'input' => array(
                'badiutimehour',
            ),
            // This option will overwrite "date_widget" and "time_widget" options
            'widget' => array(
                null, // default, don't overwrite options
                'choice',
                'choice',
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'badiutimehour';
    }

}
