<?php

namespace Badiu\Tms\TrailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TmsCompetence
 *
 * @ORM\Table(name="tms_trail_competence", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="tms_trail_competence_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="tms_trail_competence_name_uix", columns={"entity", "name"})},
 *       indexes={@ORM\Index(name="tms_trail_competence_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="tms_trail_competence_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="tms_trail_competence_idnumber_ix", columns={"idnumber"}),
 *              @ORM\Index(name="tms_trail_competence_description_ix", columns={"description"}),
 *              @ORM\Index(name="tms_trail_competence_name_ix", columns={"name"}),
 *              @ORM\Index(name="tms_trail_competence_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="tms_trail_competence_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="tms_trail_competence_useridadd_ix", columns={"useridadd"})})
 *          @ORM\Entity
 */
class Competence {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

	/**
	 * @var CompetenceCategory
	 *
	 * @ORM\ManyToOne(targetEntity="CompetenceCategory")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
	 * })
	 */
	private $categoryid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string",length=255, nullable=true)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255,nullable=true)
	 */
	private $shortname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=255,nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="boolean", nullable=false)
	 */
	private $deleted;

	public function getId() {
		return $this->id;
	}

	public function getEntity() {
		return $this->entity;
	}

	public function getIdnumber() {
		return $this->idnumber;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getParam() {
		return $this->param;
	}

	public function getTimecreated() {
		return $this->timecreated;
	}

	public function getTimemodified() {
		return $this->timemodified;
	}

	public function getUseridadd() {
		return $this->useridadd;
	}

	public function getUseridedit() {
		return $this->useridedit;
	}

	public function getDeleted() {
		return $this->deleted;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setEntity($entity) {
		$this->entity = $entity;
	}

	public function setIdnumber($idnumber) {
		$this->idnumber = $idnumber;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setParam($param) {
		$this->param = $param;
	}

	public function setTimecreated(\DateTime $timecreated) {
		$this->timecreated = $timecreated;
	}

	public function setTimemodified(\DateTime $timemodified) {
		$this->timemodified = $timemodified;
	}

	public function setUseridadd($useridadd) {
		$this->useridadd = $useridadd;
	}

	public function setUseridedit($useridedit) {
		$this->useridedit = $useridedit;
	}

	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}

	public function getDtype() {
		return $this->dtype;
	}

	public function setDtype($dtype) {
		$this->dtype = $dtype;
	}

	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}

	public function getShortname() {
		return $this->name;
	}
	public function setShortname($shortname) {
		$this->shortname = $shortname;
	}

	function getCategoryid() {
        return $this->categoryid;
    }
    function setCategoryid($categoryid) {
        $this->categoryid = $categoryid;
    }

}
