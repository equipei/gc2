<?php

namespace Badiu\Tms\CoreBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Badiu\System\CoreBundle\Model\Page\BadiuPage;
use Badiu\System\CoreBundle\Model\Functionality\BadiuKeyManger;  
use Badiu\System\CoreBundle\Controller\Functionality\BadiuController;
use Symfony\Component\HttpFoundation\Response;
class UserreportController extends BadiuController
{


  function __construct($key=null) { 
            parent::__construct();
  }
  
      public function indexParentAction(Request $request,$parentid) {
        
        //get key of module
        $this->initKeyManger($request);

        //get session data
        $badiuSession=$this->get('badiu.system.access.session');


        //permission
        $permission=$this->get('badiu.system.access.permission');
        if(!$permission->has_access($this->getKeymanger()->permissionView())){
            return $this->render($badiuSession->get()->getTheme().':Layout:permission_denied.html.twig',array('page'=>new BadiuPage()));
        }

        //bundle config
        $cbundle=$this->get('badiu.system.core.lib.config.bundle');
        $cbundle->initKeymangerExtend($this->getKeymanger());
        $this->setKeymangerExtend($cbundle->getKeymangerExtend());

        //service of key maneger that do inherit key of service
        $kminherit=$this->get('badiu.system.core.functionality.keymangerinherit');
        $kminherit->init($this->getKeymanger(),$this->getKeymangerExtend());

        //translator
        $translator=$this->get('translator');

        //page
        $page=$this->get('badiu.system.core.page');
        $page->setOperation($translator->trans($this->get('request')->get('_route')));
        //menu
        $menu=$this->get($kminherit->moduleMenu());
        $menu->setKeymanger($this->getKeymanger());

        //report
        $report=$this->get($kminherit->report());
        $report->setKeymanger($this->getKeymanger());

        //link
        $croute=$cbundle->getCrudRoute($this->getKeymanger());
        $report->setRouter($this->get("router"));

        $report->addLink('edit',$croute->getEdit());
        $report->addLink('copy',$croute->getCopy());
        $report->addLink('delete',$croute->getDelete());
        $report->addLink('remove',$croute->getRemove());

        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }

        //controller
        if ($this->container->has($kminherit->controller())){
            $controller=$this->get($kminherit->controller());
        }else{
            $controller=$this->get('badiu.system.core.functionality.controller');
        }

        //form config
        $cform=$cbundle->getFormConfig('data',$this->getKeymanger());
        if($cform->getAction()==''){$cform->setAction($kminherit->routeIndex());}


        //entity parent
        $pentity=$cbundle->getEntityParent($this->getKeymanger());
        
        $entitydata=$this->get($pentity->getEntity());
        if(!$entitydata->exist($parentid)){
            $msg=$translator->trans($pentity->getNotFoundMessage());
            echo $msg; //fazer critica
            exit;
        }

        //form
        $formOption=array();
        $formOption['action']=$this->generateUrl($cform->getAction(),array('parentid'=>$parentid));
        $formOption['method']=$cform->getMethod();
        $formOption['label']=$translator->trans($cform->getLabel());
        $formOption['attr']=$cform->getAttr();

        //$filter =$this->get($this->getKeymanger()->filter());
        $filter = array();
        //$type=$this->get($this->getKeymanger()->formFilterType());
        $type=$this->get($kminherit->formFilterType());
        $type->setKeymanger($this->getKeymanger());
        $form = $this->createForm($type,$filter,$formOption);
        $form->add('submit', 'submit', array('label' => $cform->getSubmitlabel(),'attr' => $cform->getSubmitattr()));


        $form->handleRequest($request);
        //check form
        $chekForm=$controller->searchCheckForm($form);
        if(!$chekForm){
            $page= new BadiuPage($form->createView());
            return $this->render($badiuSession->get()->getTheme().':Layout/Crud:index.html.twig', array('page' =>$page));
        }
        $filter =$form->getData();
        $filter['parentid']=$parentid;
        $filter['entity']=$badiuSession->get()->getEntity();
        $filter['bpage']=$request->query->get('bpage');
        

          $controller->setDatalayout($cform->getDatalayout());
          $controller->setDefaultdata($cform->getDefaultdata());
         //change data
          
         $filter=$controller->setDefaultDataOnSearchForm($filter);
         $filter=$controller->changeDataOnSearchForm($filter);
         
        //$filter->addParent($parentid);
        $report->extractData($filter);
        $format=$request->query->get('format');
        if($format=='json'){
             /*$ptoken=$request->query->get('_btoken');
             $token=$this->container->getParameter('alog_ws_json_token');
             if($ptoken!=$token){
                 echo "token inválido";
                 exit;
             }*/
             $json =json_encode($report->getData()->getRows());
             $response = new Response($json);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        $report->makeTable();

        $format=$request->query->get('format');
        if(!empty($format))$report->exportTable($format);


        $additionalContent=array();
        $defaultmenu=$menu->get();
        $navbar=$menu->get('navbar');
        array_push($additionalContent,$defaultmenu);
        array_push($additionalContent,$navbar);
        $links=array(array('url'=>$this->generateUrl($this->getKeymanger()->routeAdd(),array('parentid'=>$parentid)),'position'=>'beforeform','type'=>'button','name'=>$translator->trans('addnew')));
        
        $formGroupList=$cbundle->getGroupOfField($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());
        $formFieldList=$cbundle->getFieldsInGroup($this->getKeymanger()->formFilterFieldsEnable(),$this->getKeymangerExtend()->formFilterFieldsEnable());


         $page->addData('badiu_form1',$form->createView());
         $page->addData('badiu_form1_group_list',$formGroupList);
         $page->addData('badiu_form1_field_list',$formFieldList);
         $page->addData('badiu_table1',$report->getTable());
        $page->setAdditionalContents($additionalContent);
        $page->setLinks($links);
        return $this->render($badiuSession->get()->getTheme().':Layout:index.html.twig', array('page'=>$page));



    }
 
}
