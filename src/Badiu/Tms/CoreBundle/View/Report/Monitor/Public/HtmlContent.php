<?php
$formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap4');
$formfactoryhtmllib->setDprefix('ffilter.');
$formfieldto = $this->getContainer()->get('badiu.system.core.lib.form.field');

 $datarow=$utildata->getVaueOfArray($page->getData(),'badiu_list_data_row.data',true);
// $titlerow=$utildata->getVaueOfArray($page->getData(),'badiu_list_data_row.title',true);
 $datarows=$utildata->getVaueOfArray($page->getData(),'badiu_list_data_rows.data',true);
 $dtyperows=$utildata->getVaueOfArray($page->getData(),'badiu_list_data_rows.dtype',true);
 $routerows=$utildata->getVaueOfArray($page->getData(),'badiu_list_data_rows.route',true);
 
$formconfig1 = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig1');
$formfields1 = $utildata->getVaueOfArray($formconfig1, 'formfields');
/*echo "<pre>";
 print_r($page->getData());
 echo "</pre>";exit;*/
 $baseresoursepath = $container->get('templating.helper.assets')->getUrl('bundles/badiuthemecore/tms'); 
 
 ?>
<link rel="stylesheet" href="<?php echo $baseresoursepath; ?>/badiu-tms-report-monitor-dashboard-default.css">

<div id="_badiu_theme_core_dashboard_vuejs">
	<!--<div class="d-flex">
		<div class="d-flex align-items-center mr-2 px-3 py-2 header-icon">
			<i class="fas fa-plus"></i>
		</div>
		<div class="d-flex align-items-center px-3 py-2 header-title w-100">INDICADORES</div>
	</div>-->

	<div class="d-flex justify-conten-between mt-4 mx-0 badiu-tms-report-monitor-dashboard-default">


		<div class="w-75 container-main mr-4">

			<div class="d-flex container-header-last-datetime align-items-center justify-content-end mb-3 px-3 py-2 w-100">
				<p class="m-0 header-last-datetime">
					<!--Última inscrição realizada em: <span>31/08/2021 00:09:27</span>-->
				</p>
			</div>

			<div class="grid-container">
			<?php 
				foreach ($datarow as $dk => $drow) {
					$countrecord=$utildata->getVaueOfArray($drow,'countrecord');
					$vuejsvar="dlist.rowdata$dk.countrecord";
					$vuejslabel="dlist.rowtitle$dk";
			 ?>
				<div class="grid-item">
					<div class="container-grid color-bg-<?php echo $dk;?> p-1 mb-1" v-html="<?php echo $vuejslabel;?>"></div>
					<div class="container-data-number text-data-number px-3 py-2" v-html="<?php echo $vuejsvar;?>"></div>
				</div>
				<?php }?>
			</div>

			
			<div class="d-flex align-items-center justify-content-end my-3 py-2 w-100">
				<p class="m-0 header-last-datetime">
					<!--mês atual = <span>ago/21</span> mês anterior = <span>jul/21</span>-->
				</p>
			</div>
			
			<div class="container-charts1 row my-4">
			
		
			<?php 
				foreach ($datarows as $dk => $drow) {
					$vjscdvar="apexchartdata.rowsdata$dk";
					$vjsrows="dlist.rowsdata$dk";
					$dtype= $utildata->getVaueOfArray($dtyperows,$dk);
					$vuejslabel="dlist.rowstitle$dk";
					$size="height=\"250\"";
					if($dtype=='pie'){$size="width=\"380\" height=\"250\"";}
					$size=$utildata->getVaueOfArray($dtyperows,$dk);
					
					 
					$link=$utildata->getVaueOfArray($routerows,$dk);
					if(!empty($link) && $router->getRouteCollection()->get($link) !== null) {$link=$utilapp->getUrlByRoute($link);}
					//if(!empty($link)){$link="<a style=\"text-align: right;\" href=\"$link\"><i class=\"fas fa-table\"></i></a>";}
					if(!empty($link)){$link="<a style=\"text-align: right;\" href=\"#\" @click.prevent=\"makeLintRedirect('".$link."')\" ><i class=\"fas fa-table\"></i></a>";}
					
					
			    ?>
				<div class="col-sm-6 col-md-6 mb-2" v-if="<?php echo $vjsrows;?>">
					<div class="container-chart1">
						<div class="container-chart-header chart-title "> {{<?php echo $vuejslabel;?>}}  &nbsp;<?php echo $link;?></div>
						<div >
							 <apexchart  type="<?php echo $dtype;?>" <?php echo $size;?> :options="<?php echo $vjscdvar;?>.chartOptions" :series="<?php echo $vjscdvar;?>.series"></apexchart>
						</div>

					</div>
				</div>
				<?php }?>
				

				
			</div>
		</div>

		<div class="w-25 container-sidebar p-3">
			<?php
		$factoryformfilter=$container->get('badiu.theme.core.lib.template.vuejs.factoryform');
		$factoryformfilter->setSessionhashkey($page->getSessionhashkey());
        $factoryformfilter->setPage($page);
		$formconfig=new stdClass();
		$formconfig->showfirstitem=false;
		$formconfig->onlyform=true;
		$formconfig->formnumber=1;
		$factoryformfilter->setConfig($formconfig);
       echo $factoryformfilter->exec();
	?>

		</div>

	</div>
</div>




<style></style>