<?php $baseresoursepath = $container->get('templating.helper.assets')->getUrl('bundles/badiuthemecore/tms'); ?>
<link rel="stylesheet" href="<?php echo $baseresoursepath; ?>/badiu-tms-report-monitor-dashboard-default.css">

<div id="_badiu_theme_core_dashboard_vuejs">
	<div class="d-flex">
		<div class="d-flex align-items-center mr-2 px-3 py-2 header-icon">
			<i class="fas fa-plus"></i>
		</div>
		<div class="d-flex align-items-center px-3 py-2 header-title w-100">INDICADORES</div>
	</div>

	<div class="d-flex justify-conten-between mt-4 mx-0 badiu-tms-report-monitor-dashboard-default">


		<div class="w-75 container-main mr-4">

			<div class="d-flex container-header-last-datetime align-items-center justify-content-end mb-3 px-3 py-2 w-100">
				<p class="m-0 header-last-datetime">
					Última inscrição realizada em: <span>31/08/2021 00:09:27</span>
				</p>
			</div>

			<div class="grid-container">
				<div class="grid-item">
					<div class="container-grid color-bg-1 p-1 mb-1">Total de inscrições</div>
					<div class="container-data-number text-data-number px-3 py-2">4.210.248</div>
				</div>
				<div class="grid-item">
					<div class="container-grid color-bg-2 p-1 mb-1">Inscrições neste ano</div>
					<div class="container-data-number text-data-number px-3 py-2">1.128.094</div>
				</div>
				<div class="grid-item">
					<div class="container-grid color-bg-3 p-1 mb-1">Inscrições (mês atual)</div>
					<div class="container-data-number text-data-number px-3 py-2">149.520</div>
				</div>
				<div class="grid-item">
					<div class="container-grid color-bg-4 p-1 mb-1">Inscrições (mês anterior)</div>
					<div class="container-data-number text-data-number px-3 py-2">142.360</div>
				</div>
				<div class="grid-item">
					<div class="container-grid color-bg-5 p-1 mb-1">Pessoas atendidas (inscrições)</div>
					<div class="container-data-number text-data-number px-3 py-2">1.659.358</div>
				</div>
				<div class="grid-item">
					<div class="container-grid color-bg-6 p-1 mb-1">Pessoas atendidas (conclusões)</div>
					<div class="container-data-number text-data-number px-3 py-2">831.340</div>
				</div>
				<div class="grid-item">
					<div class="container-grid color-bg-7 p-1 mb-1">Servidores públicos</div>
					<div class="container-data-number text-data-number px-3 py-2">841.756</div>
				</div>
				<div class="grid-item">
					<div class="container-grid color-bg-8 p-1 mb-1">Conclusões</div>
					<div class="container-data-number text-data-number px-3 py-2">1.925.533</div>
				</div>
			</div>

			<div class="d-flex align-items-center justify-content-end my-3 py-2 w-100">
				<p class="m-0 header-last-datetime">
					mês atual = <span>ago/21</span> mês anterior = <span>jul/21</span>
				</p>
			</div>

			<div class="container-charts">

				<div>
					<div class="container-chart">
						<div class="container-chart-header chart-title">Região de origem das inscrições</div>
						<div>
							<img src="/badiunet/web/bundles/badiuthemecore/tms/images/graficos/grafico1.png" alt="">
						</div>

						<div class="mt-2">
							<img src="/badiunet/web/bundles/badiuthemecore/tms/images/graficos/grafico6.png" alt="">
						</div>
					</div>
				</div>

				<div>
					<div class="container-chart">
						<div class="container-chart-header chart-title">Temática</div>
						<div>
							<img src="/badiunet/web/bundles/badiuthemecore/tms/images/graficos/grafico2.png" alt="">
						</div>
					</div>

					<div class="container-chart mt-2">
						<div class="container-chart-header chart-title">Número de inscrições por indivíduo</div>
						<div>
							<img src="/badiunet/web/bundles/badiuthemecore/tms/images/graficos/grafico7.png" alt="">
						</div>
					</div>

				</div>

				<div>
					<div class="container-chart">
						<div class="container-chart-header chart-title">Número de inscrições e inscritos</div>
						<div>
							<img src="/badiunet/web/bundles/badiuthemecore/tms/images/graficos/grafico3.png" alt="">
						</div>
					</div>
					<div class="container-chart mt-2">
						<div class="container-chart-header chart-title">Vínculo empregatício dos inscritos</div>
						<div>
							<img src="/badiunet/web/bundles/badiuthemecore/tms/images/graficos/grafico4.png" alt="">
						</div>
					</div>
					<div class="container-chart mt-2">
						<div class="container-chart-header chart-title">Situação das inscrições</div>
						<div>
							<img src="/badiunet/web/bundles/badiuthemecore/tms/images/graficos/grafico5.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="w-25 container-sidebar p-3">
			<form>
				<div class="form-group">
					<label for="periodo">Período</label>
					<select class="form-control" id="periodo">
						<option>2018 - 2019</option>
						<option>2019 - 2020</option>
						<option>2020 - 2021</option>
					</select>
				</div>

				<div class="form-group">
					<label for="ano_inscricao">Ano de inscrição</label>
					<select class="form-control" id="ano_inscricao">
						<option>2018</option>
						<option>2019</option>
						<option>2020</option>
						<option>2021</option>
					</select>
				</div>

				<div class="form-group">
					<label for="esfera_poder">Esfera de poder</label>
					<select class="form-control" id="esfera_poder">
						<option>2018</option>
						<option>2019</option>
						<option>2020</option>
						<option>2021</option>
					</select>
				</div>

				<div class="form-group">
					<label for="esfera_governo">Esfera de governo</label>
					<select class="form-control" id="esfera_governo">
						<option>(Tudo)</option>
					</select>
				</div>

				<div class="form-group">
					<label for="uf_nascimento">UF de nascimento</label>
					<select class="form-control" id="uf_nascimento">
						<option>(Tudo)</option>
					</select>
				</div>

				<div class="form-group">
					<label for="orgao">Órgão</label>
					<select class="form-control" id="orgao">
						<option>(Tudo)</option>
					</select>
				</div>

				<div class="form-group">
					<label for="tematica">Temática</label>
					<select class="form-control" id="tematica">
						<option>(Tudo)</option>
					</select>
				</div>

				<div class="form-group">
					<label for="curso">Curso</label>
					<select class="form-control" id="curso">
						<option>(Tudo)</option>
					</select>
				</div>

				<div class="form-group">
					<label for="situacao_oferta">Situação da oferta</label>
					<select class="form-control" id="situacao_oferta">
						<option>(Tudo)</option>
					</select>
				</div>

				<div class="form-group">
					<label for="oferta">Oferta</label>
					<select class="form-control" id="oferta">
						<option>(Tudo)</option>
					</select>
				</div>

				<div class="form-group">
					<label for="categoria_situacao">Categoria de situação</label>
					<select class="form-control" id="categoria_situacao">
						<option>(Tudo)</option>
					</select>
				</div>
			</form>

			<div class="badiu-tms-report-monitor-dashboard-text-semibold mt-4 mb-3">
				Situação das inscrições
			</div>

			<ul class="container-situations pl-0">
				<li><i class="fas fa-square-full mr-3" style="color: #95AFCA;"></i> Concluído</li>
				<li><i class="fas fa-square-full mr-3" style="color: #F7BB80;"></i> Desistente</li>
				<li><i class="fas fa-square-full mr-3" style="color: #ED9A9B;"></i> Em curso</li>
				<li><i class="fas fa-square-full mr-3" style="color: #ADD4D1;"></i> Reprovado</li>
				<li><i class="fas fa-square-full mr-3" style="color: #9BC795;"></i> Trancado</li>
			</ul>
		</div>

	</div>
</div>




<style></style>