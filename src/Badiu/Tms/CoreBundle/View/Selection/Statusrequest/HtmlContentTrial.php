<div class="row">
    <div class="col-12 col-sm-4">
     
         <img v-if="rowdata3.defaultimage" :src="rowdata3.defaultimage" class="card-img-top" alt="">
	
    </div>
   <div class="col-12 col-sm-8">
      <div class="row">
		
		<div class="col-12">
			<div class="card-info d-flex align-items-baseline">
           	 <h5 class="card-title mb-0" v-html="rowdata3.name"></h5>
            </div>
			
			
			<div class="d-flex align-items-center  justify-content-start">
                <div class="d-flex align-items-baseline mb-1">
                    <span class="text-tipo text-color-4">Quant. de curso: {{rowdata3.countdiscipline}}</span> 
                </div>
				<span class="d-flex align-items-center text-cargah text-color-4 pl-5">
                      <img src="<?php echo $baseresoursepath;?>/images/icons/clock_curso.svg" class="mr-1"> {{rowdata3.disciplinehour}}
                </span>
             </div>
		</div>
		<div class="col-12">
				<div class="card-desc">
					<p v-if="rowdata3.description"   v-html="rowdata3.description"></p>
					<p v-if="rowdata3.disciplinestable"  class="card-desc" v-html="rowdata3.disciplinestable"></p>
				</div>
				
				
				 
		</div>
	  </div>
    </div>
    
  </div>