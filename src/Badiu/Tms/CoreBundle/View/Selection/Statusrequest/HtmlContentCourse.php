
<div class="row">
      <div class="col-12 col-sm-4">

        <img v-if="rowdata2.defaultimage" :src="rowdata2.defaultimage" class="card-img-top" alt="">

        
      </div>

      <div class="col-12 col-sm-8">
	  
	  <div class="card-info d-flex align-items-baseline">
           <span class="text-categoria" v-html="rowdata2.disciplinecategoryname"></span>
        </div>

        <div class="d-flex align-items-baseline">
            <h5 class="card-title mb-0" v-html="rowdata2.disciplinename"></h5>
        </div>

        <div class="d-flex align-items-center  justify-content-start">
          <div class="d-flex align-items-baseline">
            <a v-if="rowdata2.classeadminlmsurl" :href="rowdata2.classeadminlmsurl" class="mr-1"><i class="fas fa-external-link-alt"></i></a>
            <a v-if="rowdata2.classeurlview" :href="rowdata2.classeurlview" class="mr-1"><i class="fas fa-link"></i></a>
            <a v-if="rowdata2.classeurledit" :href="rowdata2.classeurledit" class="mr-1"><i class="fas fa-edit"></i></a>
            <span class="text-tipo text-color-4" v-html="rowdata2.name"></span>
          </div>
          <span class="d-flex align-items-left text-cargah text-color-4 pl-5">
            <img src="<?php echo $baseresoursepath; ?>/images/icons/clock_curso.svg" class="mr-1"> {{rowdata2.disciplinehour}}
          </span>
        </div>
		
		
        <!-- DESCRICAO DA TURMA -->
        <div class="col px-0" v-if="rowdata2.description">
          <span class="text-tipo"><?php echo $translator->trans('badiu.tms.offer.classe.descriptiontitle'); ?></span>
          <div class="card-desc" v-html="rowdata2.description"></div>
        </div>
        <!-- EMENTA -->
        <div class="col px-0" v-if="rowdata2.disciplinesummary">
          <span class="text-tipo"><?php echo $translator->trans('badiu.ams.curriculum.discipline.summary'); ?></span>
          <div class="card-desc" v-html="rowdata2.disciplinesummary"></div>
        </div>
        <!-- PUBLICO-ALVO -->
        <div class="col px-0" v-if="rowdata2.disciplineusertarget">
          <span class="text-tipo"><?php echo $translator->trans('badiu.ams.core.usertargetoffice'); ?></span>
          <div class="card-desc">
            <p v-html="rowdata2.disciplineusertarget"></p>
          </div>
        </div>
		
		
      </div>

      
    </div>
