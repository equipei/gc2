<?php 

	$buttonlabel=$translator->trans('badiu.admin.selection.request.button.requestenroll');
	$modulekey=$utildata->getVaueOfArray($page->getData(),'badiu_list_data_row.data.1.modulekey',true);
	if(!empty($modulekey)){$buttonlabel=$translator->trans($modulekey.'.selectionprocessrequest');}
	
	$courselabel=$translator->trans('badiu.admin.selection.request.course.name');
	if($modulekey=='badiu.ams.offer.offer'){$courselabel=$translator->trans('badiu.tms.offer.offer.name');}
	$isuseranonymous = $container->get('badiu.system.access.session')->get()->getUser()->getAnonymous();
	$parentid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
	$prefixid="";
	if(!empty($parentid)){$prefixid="-".$parentid;}
	
	$ptahstepnotloguedin=$utilapp->getFilePath("BadiuAdminSelectionBundle:View/Inforequest/StepNotloguedin.php");
	$ptahsteploguedin=$utilapp->getFilePath("BadiuAdminSelectionBundle:View/Inforequest/StepLoguedin.php");
	
	$contentinfo="HtmlContentCourse.php";
	if($modulekey=='badiu.tms.offer.classe'){$contentinfo="HtmlContentCourse.php";}
	else if($modulekey=='badiu.tms.offer.offer'){$contentinfo="HtmlContentTrial.php";}
	
	$baseresoursepath = $container->get('templating.helper.assets')->getUrl('bundles/badiuthemecore/tms');
	/*echo "<pre>";
	print_r($page->getData());
	echo "</pre>";*/
?>
<br />

<div id="_badiu_theme_core_dashboard_vuejs<?php echo $prefixid;?>">
<?php 
if($isuseranonymous){require_once($ptahstepnotloguedin);}
else {require_once($ptahsteploguedin);}
?> 
<div class="card">
  <div class="card-header" ><center><b>
 <?php echo $translator->trans('badiu.admin.selection.inforequest.title'); ?></b></center>
  </div>
  <div class="card-body">
  
   <div class="alert alert-danger text-center" role="alert"   v-show="rowdata1.statusshortame=='inaccessible'"><?php echo $translator->trans('badiu.admin.selection.request.naccessible.message'); ?></div>
   <div class="alert alert-danger text-center" role="alert"   v-show="rowdata1.statusshortame!='inaccessible' && fparam.messagerestriction" v-html="fparam.messagerestriction"> </div>
    
	<?php require_once($contentinfo);?>
  
  <hr />
  
 
    
	
	<div class="row">
	  
	  <table class="table table-striped">
    
    <tbody>
     
      
	  <tr>
        <td  class="tblinfotdtitle" width="30%"><div class="tblinfotitle"><?php echo $translator->trans('badiu.admin.selection.project.limitaccept'); ?></div></td>
        <td class="tblinfotdcontent"><div class="tblinfocontent" v-html="rowdata1.limitaccept"></div></td>
      
      </tr>
	
	 
	  <tr>
        <td  class="tblinfotdtitle" width="30%"><div class="tblinfotitle"><?php echo $translator->trans('badiu.admin.selection.project.timestart'); ?></div></td>
        <td class="tblinfotdcontent"><div class="tblinfocontent" v-html="rowdata1.requesttimestart"></div></td>
      
      </tr>
	  
	   <tr>
        <td  class="tblinfotdtitle" width="30%"><div class="tblinfotitle"><?php echo $translator->trans('badiu.admin.selection.project.timeend'); ?></div></td>
        <td class="tblinfotdcontent"><div class="tblinfocontent" v-html="rowdata1.requesttimeend"></div></td>
      
      </tr>
	  
	     <tr v-show="fparam.showcoupon">
        <td  class="tblinfotdtitle" width="10%"><div class="tblinfotitle"><?php echo $translator->trans('badiu.admin.selection.inforequest.coupon'); ?></div></td>
        <td class="tblinfotdcontent">
		  <div class="alert alert-danger" role="alert" v-show="couponerror" v-html="couponerror"></div>
		  <div class="tblinfocontent">
			<input type="text" v-model="fparam.coupon" class="form-control badiu-admin-selection-inforequest-coupon" id="badiuadminselectioninforequestcoupon" aria-describedby="emailHelp" placeholder="<?php echo $translator->trans('badiu.admin.selection.inforequest.coupon.placeholder'); ?>">
		  </div>
		</td>
      
      </tr>
    </tbody>
  </table>
	</div>
	
	<hr />
	    <p class="card-text" v-html="rowdata1.msgpresentation"></p> 
	<div class="row">
		<div class="col"></div>
		<div class="col" v-show="rowdata1.statusshortame=='preregistration' && !fparam.messagerestriction"><a @click="requestProcess()" class="form-control btn btn-primary"><?php echo $buttonlabel; ?></a></div>
	</div>
  </div>
</div>

</div>
