

		<!-- start menu course / trail -->
	
		<div class="row">
			<div class="d-flex badiu-tms-my-student-dashboard-default-menu-horizontal">
				<a href="#"  @click.prevent="myactivetab('classe')" class="badiu-tms-my-student-dashboard-default-item  m-3" v-bind:class="{'badiu-tms-my-student-dashboard-default-item-active': fparam.my.show.classe}"><?php echo $translator->trans('badiu.tms.my.studentfviewdefault.course'); ?> ({{rowsdata3.length}}) </a> 
				<?php if($anebletrial){?>
				<a href="#"  @click.prevent="myactivetab('trail')" class="m-3" v-bind:class="{'badiu-tms-my-student-dashboard-default-item-active': fparam.my.show.trail}"><?php echo $translator->trans('badiu.tms.my.studentfviewdefault.trial'); ?> ({{rowsdata6.length}})</a>
				<?php } ?>
			</div>
		</div>
		
		 <?php  include('MyClasseSearch.php');?> 
		
         <div class="row" v-if="fparam.my.show.classe">
                <div class="d-flex badiu-tms-my-student-dashboard-default-menu-horizontal">
                    <a href="#"  @click.prevent="updatemyclasselist('inprogress')"  class="badiu-tms-my-student-dashboard-default-item m-3" v-bind:class="{'badiu-tms-my-student-dashboard-default-item-active': fparam.myclasses.show.inprogress}">Em andamento ({{fparam.myclasses.countstatus.inprogress}}) </a>
                    <a href="#"  @click.prevent="updatemyclasselist('completed')" class="m-3" v-bind:class="{'badiu-tms-my-student-dashboard-default-item-active': fparam.myclasses.show.completed}">Concluídos ({{fparam.myclasses.countstatus.completed}})</a>
                   <a href="#"  @click.prevent="updatemyclasselist('archived')"  class="m-3" v-bind:class="{'badiu-tms-my-student-dashboard-default-item-active': fparam.myclasses.show.archived}">Não concluídos ({{fparam.myclasses.countstatus.archived}})</a> 
                </div>
        </div>
		 <?php  include('MyTrialSearch.php');?> 
		<div class="row" v-if="fparam.my.show.trail">
                <div class="d-flex badiu-tms-my-student-dashboard-default-menu-horizontal">
                    <a href="#"  @click.prevent="updatemytriallist('inprogress')"  class="badiu-tms-my-student-dashboard-default-item m-3" v-bind:class="{'badiu-tms-my-student-dashboard-default-item-active': fparam.mytrial.show.inprogress}">Em andamento ({{fparam.mytrial.countstatus.inprogress}}) </a>
                    <a href="#"  @click.prevent="updatemytriallist('completed')" class="m-3" v-bind:class="{'badiu-tms-my-student-dashboard-default-item-active': fparam.mytrial.show.completed}">Concluídos ({{fparam.mytrial.countstatus.completed}})</a>
                   <a href="#"  @click.prevent="updatemytriallist('archived')"  class="m-3" v-bind:class="{'badiu-tms-my-student-dashboard-default-item-active': fparam.mytrial.show.archived}">Não concluídos ({{fparam.mytrial.countstatus.archived}})</a> 
                </div>
        </div>
		<!-- end menu course / trail -->
        <?php  include('MyCourses.php');?> 
		<?php  include('MyTrail.php');?> 
                  