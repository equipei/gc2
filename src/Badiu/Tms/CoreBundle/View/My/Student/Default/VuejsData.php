<?php 
$aneblesupplementarycontent	=$container->get('badiu.system.access.session')->getValue('badiu.tms.my.studentfviewdefault.param.config.aneblesupplementarycontent');
if(empty($aneblesupplementarycontent)){$aneblesupplementarycontent=0;}
?>
fparam:{
	init: 0, 
	aneblesupplementarycontent: <?php echo $aneblesupplementarycontent;?>,
	<?php 
	if($aneblesupplementarycontent){echo 'currentab: "supplementarycontent",';}
	else {echo 'currentab: "availableclasse",';}
	?>
	
	my:{
		show: {classe: true,trail: false,classerecommended: false},
	},
	
	myclasses:{
		currentsatus: "inprogress",
		countstatus: {inprogress: 0,completed: 0, archived: 0 },
		show: {inprogress: true,completed: false, archived: false },
		list: [],
	}, 
	mytrial:{
		currentsatus: "inprogress",
		countstatus: {inprogress: 0,completed: 0, archived: 0 },
		show: {inprogress: true,completed: false, archived: false },
		list: [],
		review: {
			showmodalcrtclass: "modal fade",
			showmodalcrt: "none",
			itemsected: "",
			itemindex: "",
		}
	}, 
	available:{
		show: {classe: true,trail: false},
		advancedsearch: {classe: false,trail: false},
	 }, 
	myclasseslist: "",
	carousel: {
		index: 0,
	},
	policyprivate: {
		showmodalcrtclass: "modal fade",
		showmodalcrt: "none",
		title: "",
		content: ""
	},
	classeinfo: {
		showmodalcrtclass: "modal fade",
		showmodalcrt: "none",
		title: "<?php echo $translator->trans('badiu.tms.discipline.discipline.info');?>",
		summary: "",
		usrtarget: ""
	},
	enrolcancel: {
		showmodalcrtclass: "modal fade",
		showmodalcrt: "none",
		type: "classe",
		title: "<?php echo $translator->trans('badiu.tms.enrol.classe.title.cancelenrol');?>",
		item: "",
	},
	_service: "badiu.tms.my.studentfview.defaultlib",
},