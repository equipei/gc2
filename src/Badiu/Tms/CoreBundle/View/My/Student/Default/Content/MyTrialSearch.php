<!-- start search -->
<div id="mytrialsearch"  class="row no-gutters"  v-if="fparam.my.show.trail">
            <div class="col">
                    <input class="form-control input-search" type="search" @keyup.enter="searchtrial('my')" v-model="ffilter.badiuform.trial" id="trialavalaiblefilter" placeholder="Pesquise as trilha que você está inscrito">
             </div>
            <div class="col-auto">
           <button class="btn btn-search" type="button" @click="searchtrial('my')">
                <img src="<?php echo $baseresoursepath;?>/images/icons/lupa.svg">
            </button>
        </div>
</div>

<!-- end search -->	
