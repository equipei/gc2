<?php
$baseresoursepath = $container->get('templating.helper.assets')->getUrl('bundles/badiuthemecore/tms');

?>
<div class="badiu-report-list">
  <h5 class="result-search">Resultado: <?php echo $infoResult; ?></h5>

  <div class="container-fluid" v-if="tabledata" v-for="(item, index) in tabledata">
    <div class="row">
      <div class="col-12 col-sm-4">

        <img v-if="item.defaultimage.value" :src="item.defaultimage.value" class="card-img-top" alt="">
 
		
      </div> 

      <div class="col-12 col-sm-8">
        <!-- DESCRICAO DA TURMA -->
      <div class="col px-0" v-if="item.abstractordescription.value">
	  
	  <div class="card-info d-flex align-items-baseline">
          <span class="text-categoria" v-html="item.categoryname.value"></span>
        </div>
		<div class="d-flex align-items-baseline">
         <h5 class="card-title mb-0" v-html="item.struturename.value"></h5>
        </div>
		
		<div class="d-flex align-items-center  justify-content-start">
          <div class="d-flex align-items-baseline">
            <a v-if="item.urledit.value" :href="item.urledit.value" class="mr-1"><i class="fas fa-edit"></i></a>
            <span class="text-tipo text-color-4" v-html="item.name.value"></span>
          </div>
         <span class="d-flex align-items-center ml-4 text-cargah text-color-4" v-html="item.typeshortname.value"></span>
        </div>
		
          <span class="text-tipo"><?php echo $translator->trans('badiu.admin.cms.resourcecontent.descriptiontitle'); ?></span>
           <div class="card-desc">
				<p v-html="item.abstractordescription.value"></p>
		   </div>
        </div>
		
		<!-- EMENTA -->
        <div class="col px-0" v-if="item.summary.value">
          <span class="text-tipo"><?php echo $translator->trans('badiu.ams.curriculum.discipline.summary'); ?></span>
          <div class="card-desc" v-html="item.summary.value"></div>
        </div>
		
		<!-- PUBLICO-ALVO -->
        <div class="col px-0" v-if="item.usertarget.value">
          <span class="text-tipo"><?php echo $translator->trans('badiu.ams.core.usertargetoffice'); ?></span>
          <div class="card-desc">
            <p v-html="item.usertarget.value"></p>
          </div>
        </div>
		
		 <div class="card-options">
          <div class="d-flex align-items-left"> 
              <a v-if="item.itemviewurl.value && item.itemviewurltarget.value=='_blank'" target="_blank" :href="item.itemviewurl.value" class="btn btn-card-blue">Acessar</a>
			  <a v-if="item.itemviewurl.value && item.itemviewurltarget.value!='_blank'"  :href="item.itemviewurl.value" class="btn btn-card-blue">Acessar</a>
                                       
          </div>
        </div>
      </div>

      
    </div>
    <hr />
  </div>

  <?php echo $pagingout; ?>
</div>