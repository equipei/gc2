
<!-- start search -->
<div class="row no-gutters"  v-if="fparam.available.show.trail">
            <div class="col">
                    <input class="form-control input-search" type="search" @keyup.enter="searchtrial('available')" v-model="ffilter.badiuform.trial" id="trialavalaiblefilter" placeholder="Pesquise as trilhas do seu interesse">
             </div>
            <div class="col-auto">
           <button class="btn btn-search" type="button" @click="searchtrial('available')">
                <img src="<?php echo $baseresoursepath;?>/images/icons/lupa.svg">
            </button>
        </div>
</div>

<!-- end search -->	
	<div class="badiu-grid-cards my-4" v-if="fparam.available.show.trail">
							<div v-for="(item, index) in rowsdata4">
                                <div class="card-content">
                                   <!-- <span class="curso-tipo-status">
                                        <p><img src="images/icons/unlock.svg" class="mr-2">Curso livre</p>
										</span>
									-->
                                    <div>
                                        <div class="card-img">
                                            <img v-if="item.defaultimage" :src="item.defaultimage" class="card-image" alt="">

                                        </div>

                                        

										
                                        <div class="card-desc">
										
                                            <div class="d-flex align-items-baseline mb-1">

                                                <a v-if="item.offerurlview" :href="item.offerurlview" class="mr-1"><i class="fas fa-link"></i></a>
                                                <a v-if="item.offerurledit" :href="item.offerurledit" class="mr-1"><i class="fas fa-edit"></i></a>

                                                <h5 class="card-title mb-0" v-html="item.name">
                                                 </h5>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-between mb-4">
                                                <div class="d-flex align-items-baseline mb-1">
                                                        <span class="text-tipo text-color-4">Quant. de curso: {{item.countdiscipline}}</span> 
                                                </div>

                                                <span class="d-flex align-items-center ml-4 text-cargah text-color-4">
                                                    <img src="<?php echo $baseresoursepath;?>/images/icons/clock_curso.svg" class="mr-1"> {{item.disciplinehour}}
                                                </span>
                                            </div>
                                            <p v-html="item.description"></p>
											<div v-html="item.disciplinestable"></div>
                                        </div>
                                    </div>

                                    <div class="card-options">
                                       
                                        <div class="d-flex align-items-center justify-content-between">
                                            <a v-if="item.requestenrolurl" :href="item.requestenrolurl" class="btn btn-card-blue">Solicitar inscrição</a>
                                         </div>
                                    </div>
                                </div>
                            
                    </div>
						   
	</div>
				<!-- end course list -->