<!-- start modal review trial-->

<div id="_badiu_tms_my_default_classe_info" style="visibility:hidden">
    <div id="_badiu_tms_my_default_classe_info_review_status" v-bind:class="[fparam.classeinfo.showmodalcrtclass]" v-bind:style="{ display: fparam.classeinfo.showmodalcrt }" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" v-html="fparam.classeinfo.title"></h5>
                    <button type="button" class="close" v-on:click="fparam.classeinfo.showmodalcrt = 'none'" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
				 <div class="modal-body">
					<h6><?php echo $translator->trans('badiu.ams.curriculum.discipline.summary');?></h6>
					<div v-html="fparam.classeinfo.summary"></div>
					<br />
					<h6><?php echo $translator->trans('badiu.ams.core.usertargetoffice');?></h6>
					<div  v-html="fparam.classeinfo.usertarget"></div>
				 </div>
				
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" v-on:click="fparam.classeinfo.showmodalcrt = 'none'">Fechar</button>
                   
                </div>
            </div>
        </div>
    </div>
</div>


<!--end modal -->