
<!-- start menu course / trail -->

<div class="row">
    <div class="d-flex badiu-tms-my-student-dashboard-default-menu-horizontal">
        <a href="#"  @click.prevent="availableactivetab('classe')" class="badiu-tms-my-student-dashboard-default-item  m-3" v-bind:class="{'badiu-tms-my-student-dashboard-default-item-active': fparam.available.show.classe}"><?php echo $translator->trans('badiu.tms.my.studentfviewdefault.course'); ?> ({{rowsdata2.length}}) </a>
		<?php if($anebletrial){?>
		<a v-if="isloggedin==1" href="#"  @click.prevent="availableactivetab('classerecommended')" class="badiu-tms-my-student-dashboard-default-item  m-3" v-bind:class="{'badiu-tms-my-student-dashboard-default-item-active': fparam.available.show.classerecommended}"><?php echo $translator->trans('badiu.tms.my.studentfviewdefault.courserecommended'); ?> ({{rowsdata8.length}}) </a>
        <a href="#"  @click.prevent="availableactivetab('trail')" class="m-3" v-bind:class="{'badiu-tms-my-student-dashboard-default-item-active': fparam.available.show.trail}"> <?php echo $translator->trans('badiu.tms.my.studentfviewdefault.trial'); ?>  ({{rowsdata4.length}})</a>
		<?php } ?>
   </div>
</div>
<!-- end menu course / trail -->	

					
<!-- start course list -->
 <?php  include('CoursesAvailable.php');?> 
 <?php  include('TrialAvailable.php');?> 
 <?php  include('CoursesRecommendedAvailable.php');?> 

<!-- end course list -->