<!-- start search -->
<div class="row no-gutters" v-if="fparam.available.show.classe">
    <div class="col">
        <!--<input class="form-control input-search" @keyup.enter="searchclasse('available')" type="search" v-model="ffilter.badiuform.gsearch" id="courseavalaiblefilter" placeholder="Pesquise curso do seu interesse"> -->
        <input class="form-control input-search" @keyup.enter="searchclasse('available')" type="search" @input="ffilter.badiuautocomplete.gsearch_istyping = true" placeholder="Pesquise os cursos de seu interesse" v-model="ffilter.badiuautocomplete.gsearch_query" @input="ffilter.badiuautocomplete.gsearch_istyping = true" id="courseavalaiblefilter" autocomplete="off" placeholder="Pesquise os cursos de seu interesse">

    </div>

    <div class="col-auto">
        <button class="btn btn-search mr-2" type="button" @click="searchclasse('available')">
            <img src="<?php echo $baseresoursepath; ?>/images/icons/lupa.svg">
        </button>
        <a class="pointer ml-2" @click.prevent="showHideAdvancedsearch()"><i class="fas fa-chevron-down"></i></a>
    </div>

</div>
<div align="center" v-if="ffilter.badiuautocomplete.gsearch_isloading"> <span>Procurando...</span> </div>
<div class="list-group" v-if="ffilter.badiuautocomplete.gsearch_isshowlist"> <a v-for="sitem in ffilter.badiuautocomplete.gsearch_searchresult" @click.prevent="gsearch_itemselected(sitem)" href="#" class="list-group-item"> {{sitem.name}}</a> </div>

<div class="row advancedsearch pt-4" v-if="fparam.available.advancedsearch.classe && fparam.available.show.classe">
    <?php
    $cont = 0;
    foreach ($formfields1 as $fk => $field) {
        $flabel = $utildata->getVaueOfArray($field, 'label');
		$type = $utildata->getVaueOfArray($field, 'type');
        $formfieldto->init($field);
        $fdata = $formfactoryhtmllib->get($formfieldto, false);
        $cont++;
        if ($cont > 2 && $type!='hidden' ) {
    ?>
            <div class="col-12 col-md-3 my-1">
                <label for="periodo"><?php echo $flabel; ?></label>
                <?php echo $fdata; ?>
            </div>
    <?php }
    } ?>
    <div class="form-group col-12 my-5"> <button type="button" @click="searchclasse('available')" class="btn btn-primary">Pesquisar</button></div>

</div>
<!-- end search -->
<div class="badiu-grid-cards row my-4" v-if="fparam.available.show.classe">
    <div v-for="(item, index) in rowsdata2">
        <div class="card-content">
            <!-- <span class="curso-tipo-status">
                                        <p><img src="images/icons/unlock.svg" class="mr-2">Curso livre</p>
										</span>
									-->
            <div>
                <div class="card-img">
                    <img v-if="item.defaultimage" :src="item.defaultimage" class="card-image" alt="">

                </div>

                <div class="card-info d-flex align-items-baseline">

                    <a v-if="item.disciplinecategoryurlview" :href="item.disciplinecategoryurlview" class="mr-1"><i class="fas fa-link"></i></a>
                    <a v-if="item.disciplinecategoryurledit" :href="item.disciplinecategoryurledit" class="mr-1"><i class="fas fa-edit"></i></a>

                    <span class="text-categoria" v-html="item.disciplinecategoryname"></span>
                </div>

                <div class="card-desc">
                    <div class="d-flex align-items-baseline mb-1">

                        <a v-if="item.disciplineurlview" :href="item.disciplineurlview" class="mr-1"><i class="fas fa-link"></i></a>
                        <a v-if="item.disciplineurledit" :href="item.disciplineurledit" class="mr-1"><i class="fas fa-edit"></i></a>

                        <h5 class="card-title mb-0">
                            <span v-html="item.disciplinename"></span>
                            <a href="#" @click.prevent="showModalClasseInfo('available',index)"><i class="fas fa-info-circle"></i></a>
                        </h5>

                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <div class="d-flex align-items-baseline mb-1">
                            <a v-if="item.classeadminlmsurl" :href="item.classeadminlmsurl" class="mr-1"><i class="fas fa-external-link-alt"></i></a>
                            <a v-if="item.classeurlview" :href="item.classeurlview" class="mr-1"><i class="fas fa-link"></i></a>
                            <a v-if="item.classeurledit" :href="item.classeurledit" class="mr-1"><i class="fas fa-edit"></i></a>

                            <span class="text-tipo text-color-4" v-html="item.name"></span>
                        </div>

                        <span class="d-flex align-items-center ml-4 text-cargah text-color-4">
                            <img src="<?php echo $baseresoursepath; ?>/images/icons/clock_curso.svg" class="mr-1"> {{item.disciplinehour}}
                        </span>


                    </div>

                    <p v-html="item.description"></p>

                </div>
            </div>

            <div class="card-options">


                <div class="d-flex align-items-center justify-content-between">
                    <a v-if="item.classerequestenrolurl" :href="item.classerequestenrolurl" class="btn btn-card-blue">Solicitar inscrição</a>

                </div>
            </div>
        </div>

    </div>

</div>
<!-- end course list -->