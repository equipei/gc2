<?php

$formfactoryhtmllib = $this->getContainer()->get('badiu.theme.core.lib.template.vuejs.formfactoryhtmlbootstrap4');
$formfactoryhtmllib->setDprefix('ffilter.');
$formfieldto = $this->getContainer()->get('badiu.system.core.lib.form.field');
$formconfig1 = $utildata->getVaueOfArray($page->getData(), 'badiu_formconfig1');
$formfields1 = $utildata->getVaueOfArray($formconfig1, 'formfields');
$privatepolicecookies=$utildata->getVaueOfArray($page->getData(), 'badiu_list_data_row.data.1.content',true);
$badiusession=$container->get('badiu.system.access.session');
$aneblecarouselnewscontent	=$badiusession->getValue('badiu.tms.my.studentfviewdefault.param.config.aneblecarouselnewscontent');
$aneblecardsnewscontent	=$badiusession->getValue('badiu.tms.my.studentfviewdefault.param.config.aneblecardsnewscontent');
$anebletrial	=$badiusession->getValue('badiu.tms.my.studentfviewdefault.param.config.anebletrial');

$labelmycoursetrial= $translator->trans('badiu.tms.my.studentfviewdefault.mycoursetrial');
$labelavailablecoursetrial= $translator->trans('badiu.tms.my.studentfviewdefault.availablecoursetrial');

$labelmycourse= $translator->trans('badiu.tms.my.studentfviewdefault.mycourse');
$labelavailablecourse= $translator->trans('badiu.tms.my.studentfviewdefault.availablecourse');

$labelmytrial= $translator->trans('badiu.tms.my.studentfviewdefault.mytrial');
$labelavailabletrial= $translator->trans('badiu.tms.my.studentfviewdefault.availabletrial');

$labelred= $translator->trans('badiu.tms.my.studentfviewdefault.red');

$labelmy=$labelmycourse;
$labelavailable=$labelavailablecourse;

if($anebletrial){
	$labelmy=$labelmycoursetrial;
	$labelavailable=$labelavailablecoursetrial;
}
$baseresoursepath = $container->get('templating.helper.assets')->getUrl('bundles/badiuthemecore/tms');

//link card edit
	$permission = $container->get('badiu.system.access.permission');
	$permissioncimg = $permission->has_access('badiu.admin.cms.resourcecontentcard.add', $page->getSessionhashkey());
	$linkcmscard = "";
	if ($permissioncimg) {
		$clparam = array('type' => 'redirectbyshortname', 'shortname' => 'badiutmscorestudentfviewdefault', '_function' => 'send', 'service' => 'badiu.admin.cms.repository.data', 'route' => 'badiu.admin.cms.resourcecontentcard.index');
		$url = $utilapp->getUrlByRoute('badiu.system.core.proxy.link', $clparam);

		$linkcmscard = "<a href=\"$url\"><i class=\"fa fa-edit\"></i></a>";
	}
?>

<div id="_badiu_theme_core_dashboard_vuejs">
	<div class="badiu-tms-my-student-dashboard-default">
	
		<!-- start carousel -->
		<?php if($aneblecarouselnewscontent){?>
		 <div class="row no-gutters my-4">
			<?php include('Content/Carousel.php'); ?>
		 </div> 
		<?php } ?>
		<!-- end carousel -->

		<!-- start news -->
		<?php if($aneblecardsnewscontent){?>
		 <?php echo $linkcmscard; ?>
		 <div class="row my-4 wrapper-news">
			<?php include('Content/News.php'); ?>
		 </div>
		<?php } ?>  
		<!-- end news -->

		

	<!-- start user not logedin -->
       
		<!-- without content -->
	   <div id="cursosdisponiviesnotloggedin" v-if="isloggedin==0  && fparam.aneblesupplementarycontent==0">
			<?php include('Content/AvailableIndex.php');?>
		</div>
	  <!-- with content -->
		<div id="withcontentwithoutlogin" class="row my-5" id="cursosdisponiviesnotloggedin" v-if="isloggedin==0  && fparam.aneblesupplementarycontent==1">
			<div id="cursos-tabs" class="col-12">

				<ul class="nav nav-tabs" id="myTabWithoutlogin" role="tablist">
					
					<li class="nav-item">
						<a class="nav-link  active" @click="changemaintab('available')" id="cursosdisponivies-tab" data-toggle="tab" href="#cursosdisponivies" role="tab" aria-controls="cursosdisponivies" aria-selected="true"><?php echo $labelavailable; ?> </a>
					</li>
					
					<li class="nav-item">
						<a class="nav-link" @click="changemaintab('supplementarycontent')" id="supplementarycontent-tab" data-toggle="tab" href="#supplementarycontent" role="tab" aria-controls="supplementarycontent" aria-selected="true"><?php echo $labelred; ?> </a>
					</li>
					
					
					
					
				</ul>

				<div class="tab-content" id="myTabContentWithoutlogin">
					

					<div class="tab-pane fade" id="cursosdisponivies" role="tabpanel" aria-labelledby="cursosdisponivies-tab">
						<?php include('Content/AvailableIndex.php'); ?>
					</div>
					
					<div class="tab-pane fade show active" id="supplementarycontent" role="tabpanel" aria-labelledby="supplementarycontent-tab">
						<?php include('Content/Supplementarycontent.php'); ?>
					</div>
				</div>
			</div>
		</div>
		
	<!-- end user not logedin -->


		<!-- start user logedin -->
		
		<!-- without content -->
		<div id="withoutcontentloguedin" class="row my-5" v-if="isloggedin==1 && fparam.aneblesupplementarycontent==0">
			<div id="cursos-tabs" class="col-12">

				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" @click="changemaintab('my')" id="meuscursos-tab" data-toggle="tab" href="#meuscursos" role="tab" aria-controls="meuscursos" aria-selected="true"><?php echo $labelmy; ?></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" @click="changemaintab('available')" id="cursosdisponivies-tab" data-toggle="tab" href="#cursosdisponivies" role="tab" aria-controls="cursosdisponivies" aria-selected="true"><?php echo $labelavailable; ?> </a>
					</li>
					
				</ul>

				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="meuscursos" role="tabpanel" aria-labelledby="meuscursos-tab">
						<?php include('Content/MyIndex.php'); ?>
					</div>

					<div class="tab-pane fade" id="cursosdisponivies" role="tabpanel" aria-labelledby="cursosdisponivies-tab">
						<?php include('Content/AvailableIndex.php'); ?>
					</div>
					
				</div>
			</div>
		</div>
		
		
		<!-- with content -->
		<div id="withcontentloguedin" class="row my-5" v-if="isloggedin==1  && fparam.aneblesupplementarycontent==1">
			<div id="cursos-tabs" class="col-12">

				<ul class="nav nav-tabs" id="myTab" role="tablist">
					
					
					<li class="nav-item">
						<a class="nav-link  active" @click="changemaintab('my')" id="meuscursos-tab" data-toggle="tab" href="#meuscursos" role="tab" aria-controls="meuscursos" aria-selected="true"><?php echo $labelmy; ?></a>
					</li>
					
					
					
					<li class="nav-item">
						<a class="nav-link" @click="changemaintab('available')" id="cursosdisponivies-tab" data-toggle="tab" href="#cursosdisponivies" role="tab" aria-controls="cursosdisponivies" aria-selected="true"><?php echo $labelavailable; ?> </a>
					</li>
					
					<li class="nav-item">
						<a class="nav-link" @click="changemaintab('supplementarycontent')" id="supplementarycontent-tab" data-toggle="tab" href="#supplementarycontent" role="tab" aria-controls="supplementarycontent" aria-selected="true"><?php echo $labelred;?> </a>
					</li>
					
				</ul>

				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="meuscursos" role="tabpanel" aria-labelledby="meuscursos-tab">
						<?php include('Content/MyIndex.php'); ?>
					</div>

					<div class="tab-pane fade" id="cursosdisponivies" role="tabpanel" aria-labelledby="cursosdisponivies-tab">
						<?php include('Content/AvailableIndex.php'); ?>
					</div>
					
					<div class="tab-pane fade" id="supplementarycontent" role="tabpanel" aria-labelledby="supplementarycontent-tab">
						<?php include('Content/Supplementarycontent.php'); ?>
					</div>
				</div>
			</div>
		</div>
		<!-- end user logedin -->
	</div> <!-- end class badiu-tms-my-student-dashboard-default -->

<?php if(!empty($privatepolicecookies)){include('Content/PolicyPrivateModal.php');} ?>
<?php include('Content/CourseInfoModal.php'); ?>
<?php include('Content/CancelEnrolModal.php'); ?>
</div><!-- end id _badiu_theme_core_dashboard_vuejs -->

