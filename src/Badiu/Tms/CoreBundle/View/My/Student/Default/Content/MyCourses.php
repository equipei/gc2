                        <div class="badiu-grid-cards my-4" v-if="fparam.my.show.classe">
							 <div v-for="(item, index) in fparam.myclasses.list">
                                <div class="card-content">
                                   <!-- <span class="curso-tipo-status">
                                        <p><img src="images/icons/unlock.svg" class="mr-2">Curso livre</p>
										</span>
									-->
                                    <div>
                                        <div class="card-img">
                                            <img v-if="item.defaultimage" :src="item.defaultimage" class="card-image" alt="">

                                        </div>

                                        <div class="card-info d-flex align-items-baseline">

                                            <a v-if="item.disciplinecategoryurlview" :href="item.disciplinecategoryurlview" class="mr-1"><i class="fas fa-link"></i></a>
                                            <a v-if="item.disciplinecategoryurledit" :href="item.disciplinecategoryurledit" class="mr-1"><i class="fas fa-edit"></i></a>

                                            <span class="text-categoria" v-html="item.disciplinecategoryname"></span>
                                        </div>

                                        <div class="card-desc">
                                            <div class="d-flex align-items-baseline mb-1">

                                                <a v-if="item.disciplineurlview" :href="item.disciplineurlview" class="mr-1"><i class="fas fa-link"></i></a>
                                                <a v-if="item.disciplineurledit" :href="item.disciplineurledit" class="mr-1"><i class="fas fa-edit"></i></a>

                                                <h5 class="card-title mb-0" v-html="item.disciplinename">
                                                 </h5>
												 <a  href="#" @click.prevent="showModalClasseInfo('my',index)"><i class="fas fa-info-circle"></i></a>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-between mb-4">
                                                <div class="d-flex align-items-baseline mb-1">
                                                    <a v-if="item.classeadminlmsurl" :href="item.classeadminlmsurl" class="mr-1"><i class="fas fa-external-link-alt"></i></a>
													<a v-if="item.classeurlview" :href="item.classelineurlview" class="mr-1"><i class="fas fa-link"></i></a>
                                                    <a v-if="item.classeurlview" :href="item.classeurlview" class="mr-1"><i class="fas fa-edit"></i></a>

                                                    <span class="text-tipo text-color-4" v-html="item.name"></span>
                                                </div>
												
                                                <span class="d-flex align-items-center ml-4 text-cargah text-color-4">
                                                    <img src="<?php echo $baseresoursepath;?>/images/icons/clock_curso.svg" class="mr-1"> {{item.disciplinehour}}
                                                </span>
                                            </div>
                                            <p v-html="item.description"></p>
                                        </div>
                                    </div>

                                    <div class="card-options">
                                        <div class="mb-3">
                                            <div v-if="item.progress" v-html="item.progress"> </div>
                                            <div class="d-flex align-items-center justify-content-between">
                                                <p class="text-previsao mb-0" v-if="item.finalgrade" v-html="item.finalgrade"></p>
                                                <small class="text-required" v-if="item.statusname" v-html="item.statusname"></small>
												<small class="text-required" v-if="item.statusinfo" v-html="item.statusinfo"></small>
                                            </div>
                                        </div>

                                        <div class="d-flex align-items-center justify-content-between">
											<div v-html="item.classelmslink"></div> 
											<a v-if="item.classecancancelenrol" @click="showModalEnrolCancel(item,'classe')" class="btn btn-card-red" title=""><?php echo $translator->trans('badiu.tms.enrol.cancelenrol');?></a>
                                            <a v-if="item.certificateurl" :href="item.certificateurl" class="btn btn-card-green" target="_blank"><?php echo $translator->trans('badiu.tms.enrol.getcertificate');?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>			

                            


                            
                        </div>
                  