<!-- start modal review trial-->
<div v-html="rowdata1.templatecustomcss"> </div>
<div v-html="rowdata1.customcss"> </div>
<div  v-if="this.rowdata1.content" id="_badiu_tms_my_default_policy_private" class="model_policy_private" style="visibility:hidden">
    <div id="_badiu_tms_my_default_policy_private_review_status" v-bind:class="[fparam.policyprivate.showmodalcrtclass]" v-bind:style="{ display: fparam.policyprivate.showmodalcrt }" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" v-html="fparam.policyprivate.title"></h5>
                    
                </div>
                <div class="modal-body" v-html="fparam.policyprivate.content">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" @click="acceptPrivatePolicy()">Aceito</button>
                   
                </div>
            </div>
        </div>
    </div>
</div>

<!--end modal -->