sysinit: function () {
	this.fparam.init=1;
	if(this.isloggedin==1){this.fparam.currentab='myclasse';}
	this.ffilter.badiuform.type=this.fparam.currentab;
},
searchclasse: function (type) {
		this.ffilter.badiuautocomplete.gsearch_isshowlist = false;
		var self = this;
			this.formcontrol.processing=true;
			
			this.ffilter.badiuform._service=this.fparam._service;
			this.ffilter.badiuform._function="searchClasse";
			if(type=='available' || type=='availableclasse'){
				this.ffilter.badiuform._sqlindex=2; 
			}else if(type=='my' || type=='myclasse'){
				this.ffilter.badiuform._sqlindex=3; 
			}
			
			//console.log(this.ffilter.badiuform);
			// return null;  
			   axios.post(this.serviceurl,this.ffilter.badiuform).then(function (response) {
				   console.log(response.data); 
				    if(response.data.status=='accept'){
						if(type=='available'  || type=='availableclasse'){						
							self.rowsdata2=response.data.message.data[2];
						}else if(type=='my'  || type=='myclasse'){
							self.rowsdata3=response.data.message.data[3];
							self.updatemyclasselist(self.fparam.myclasses.currentsatus);
							self.myclassecountstatus();
							self.myclassestatuactive();
						}
						self.formcontrol.processing=false;

					 }else if(response.data.status=='danied'){
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
						
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 	 
	 
  },
 
 searchsupplementarycontent: function (type) {
		this.ffilter.badiuautocomplete.gsearch_isshowlist = false;
		var self = this;
			this.formcontrol.processing=true;
			
			this.ffilter.badiuform._service=this.fparam._service;
			this.ffilter.badiuform._function="searchSupplementaryContent";
			this.ffilter.badiuform.type=type;
			this.ffilter.badiuform._sqlindex=10; 
			/*if(type=='availablecontent'){
				this.ffilter.badiuform._sqlindex=10; 
			}else if(type=='my' || type=='myclasse'){
				this.ffilter.badiuform._sqlindex=3; 
			}*/
			
			//console.log(this.fparam.currentab);
			// return null;  
			   axios.post(this.serviceurl,this.ffilter.badiuform).then(function (response) {
				   console.log(response.data); 
				    if(response.data.status=='accept'){
						self.rowsdata10=response.data.message.data[10];
						/*if(type=='available'){						
							self.rowsdata10=response.data.message.data[10];
						}else if(type=='my'  || type=='myclasse'){
							self.rowsdata3=response.data.message.data[3];
							self.updatemyclasselist(self.fparam.myclasses.currentsatus);
							self.myclassecountstatus();
							self.myclassestatuactive();
						}*/
						self.formcontrol.processing=false;

					 }else if(response.data.status=='danied'){
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
						
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 	 
	 
  },
  changemaintab: function(currentab){
	  if(currentab=='my'){this.fparam.currentab="myclasse";}
	  else if(currentab=='available'){this.fparam.currentab="availableclasse";}
	  else if(currentab=='supplementarycontent'){this.fparam.currentab="supplementarycontent";}
	  this.ffilter.badiuform.type=this.fparam.currentab;
	  this.ffilter.badiuautocomplete.gsearch_query="";
	  this.ffilter.badiuform.gsearch="";
	 //  console.log("bbb");
	   console.log(this.ffilter.badiuform.gsearch);
	  
	  this.ffilter.badiuform.gsearch_badiuautocompleteitemname="";

	  this.ffilter.badiuform.trial="";
	  this.ffilter.badiuform.disciplinecategoryid="";
	  this.ffilter.badiuform.typeid="";
	  this.ffilter.badiuform.typemanagerid="";
	  this.ffilter.badiuform.typeaccessid="";
	  this.ffilter.badiuform.dhour_badiunumberoperator="";
	  this.ffilter.badiuform.dhour_badiunumber1="";
	  this.ffilter.badiuform.dhour_badiunumber2="";
   },  
  searchtrial: function (type) {
	
		var self = this;
			this.formcontrol.processing=true;
			
			this.ffilter.badiuform._service=this.fparam._service;
			this.ffilter.badiuform._function="searchTrial";
			if(type=='available'){	this.ffilter.badiuform._sqlindex=4; }
			else if(type=='my'){
				this.ffilter.badiuform._sqlindex=6; 
			}
			//console.log(this.ffilter);
			// return null;  
			   axios.post(this.serviceurl,this.ffilter.badiuform).then(function (response) {
				  // console.log(response.data); 
				    if(response.data.status=='accept'){ 
						if(type=='available'){
							self.rowsdata4=response.data.message.data[4];
						}else if(type=='my'){
							self.rowsdata6=response.data.message.data[6];
							self.updatemytriallist(this.fparam.mytrial.currentsatus);
							self.mytrialcountstatus();
							self.mytrialcountstatus();
							self.mytrialstatuactive();
						}
						self.formcontrol.processing=false;

					 }else if(response.data.status=='danied'){
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
						
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 	 
	 
  },
  gsearch_exec_after_itemselected: function () {
	 
	 if(this.fparam.currentab=='supplementarycontent'){this.searchsupplementarycontent(this.fparam.currentab);}
	 else {this.searchclasse(this.fparam.currentab);}
  },
  reviewtrial: function () {
		
		var self = this;
			this.formcontrol.processing=true;
			var item=this.fparam.mytrial.review.itemsected;
			var index=this.fparam.mytrial.review.itemindex;
			this.ffilter.badiuform._service=this.fparam._service;
			this.ffilter.badiuform._function="reviewTrial";
			this.ffilter.badiuform.enrolid=item.enrolid; 
			 
			   axios.post(this.serviceurl,this.ffilter.badiuform).then(function (response) {
				   console.log(response.data); 
				   console.log("index: "+ index); 
				    if(response.data.status=='accept'){ 
					 if(response.data.message=='badiu.ams.enrol.offer.message.changestatuwithsucess'){
						 self.rowsdata6[index].statusshortname='studyingenrol';
						 self.rowsdata6[index].statusname='<?php echo $translator->trans('badiu.tms.enrol.status.studying');?>';
						 self.mytrialcountstatus();
						 self.fparam.mytrial.currentsatus ='inprogress';
						 self.mytrialstatuactive();
						 self.fparam.mytrial.review.showmodalcrt = 'none';
					 }
						//self.rowsdata4=response.data.message.data[4];
						//self.formcontrol.processing=false;

					 }else if(response.data.status=='danied'){
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
						
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 	 
	 
  },
  updatemyclasselist: function (status) {
	 this.fparam.myclasses.list=[];
	 var lstatus;
	 this.fparam.myclasses.currentsatus=status;
	 this.myclassecountstatus();
	 this.myclassestatuactive();
	for (i = 0; i < this.rowsdata3.length; i++) {
		 lstatus=this.rowsdata3[i].statusshortname;
		 
		if(status=='completed'){
			if(lstatus=='approvedenrol'){
				this.fparam.myclasses.list.push(this.rowsdata3[i]);
			}
		}else if(status=='inprogress'){
			if(lstatus=='preregistrationenrol' || lstatus=='activeenrol' || lstatus=='studyingenrol' ){
				this.fparam.myclasses.list.push(this.rowsdata3[i]);
				console.log("dddd");
			}
		}else if(status=='archived'){ 
			if(lstatus=='disapprovedenrol' || lstatus=='canceledenrol' || lstatus=='lockedenrol' || lstatus=='suspendedenrol' || lstatus=='inactiveenrol'  || lstatus=='retiredenrol' || lstatus=='quitterenrol' || lstatus=='evadedenrol' ){
				this.fparam.myclasses.list.push(this.rowsdata3[i]);
			}
		}
	}
	
  },
   updatemytriallist: function (status) {
	 this.fparam.mytrial.list=[];
	 var lstatus;
	 this.fparam.mytrial.currentsatus=status;
	 this.mytrialcountstatus();
	 this.mytrialstatuactive();
	for (i = 0; i < this.rowsdata6.length; i++) {
		 lstatus=this.rowsdata6[i].statusshortname;
		 
		if(status=='completed'){
			if(lstatus=='approvedenrol'){
				this.fparam.mytrial.list.push(this.rowsdata6[i]);
			}
		}else if(status=='inprogress'){
			if(lstatus=='preregistrationenrol' || lstatus=='activeenrol' || lstatus=='studyingenrol' ){
				this.fparam.mytrial.list.push(this.rowsdata6[i]);
				console.log("dddd");
			}
		}else if(status=='archived'){ 
			if(lstatus=='disapprovedenrol' || lstatus=='canceledenrol' || lstatus=='lockedenrol' || lstatus=='suspendedenrol' || lstatus=='inactiveenrol'  || lstatus=='retiredenrol' || lstatus=='quitterenrol' || lstatus=='evadedenrol' ){
				this.fparam.mytrial.list.push(this.rowsdata6[i]);
			}
		}
	}
	
  },
  myclassecountstatus: function () {
	 var lstatus;
	 this.fparam.myclasses.countstatus.completed=0;
	 this.fparam.myclasses.countstatus.inprogress=0;
	 this.fparam.myclasses.countstatus.archived=0;
	 for (i = 0; i < this.rowsdata3.length; i++) {
		 lstatus=this.rowsdata3[i].statusshortname;
		 if(lstatus=='approvedenrol'){this.fparam.myclasses.countstatus.completed++;}
		 else if(lstatus=='preregistrationenrol' || lstatus=='activeenrol' || lstatus=='studyingenrol' ){this.fparam.myclasses.countstatus.inprogress++;}
		 else if(lstatus=='disapprovedenrol' || lstatus=='canceledenrol' || lstatus=='lockedenrol' || lstatus=='suspendedenrol' || lstatus=='inactiveenrol'  || lstatus=='retiredenrol' || lstatus=='quitterenrol' || lstatus=='evadedenrol' ){this.fparam.myclasses.countstatus.archived++;}
		}		
  },
 
mytrialcountstatus: function () {
	 var lstatus;
	 this.fparam.mytrial.countstatus.completed=0;
	 this.fparam.mytrial.countstatus.inprogress=0;
	 this.fparam.mytrial.countstatus.archived=0;
	 for (i = 0; i < this.rowsdata6.length; i++) {
		 lstatus=this.rowsdata6[i].statusshortname;
		 if(lstatus=='approvedenrol'){this.fparam.mytrial.countstatus.completed++;}
		 else if(lstatus=='preregistrationenrol' || lstatus=='activeenrol' || lstatus=='studyingenrol' ){this.fparam.mytrial.countstatus.inprogress++;}
		 else if(lstatus=='disapprovedenrol' || lstatus=='canceledenrol' || lstatus=='lockedenrol' || lstatus=='suspendedenrol' || lstatus=='inactiveenrol'  || lstatus=='retiredenrol' || lstatus=='quitterenrol' || lstatus=='evadedenrol' ){this.fparam.mytrial.countstatus.archived++;}
		}		
  },
 myclassestatuactive: function () {

	 if(this.fparam.myclasses.currentsatus =='inprogress'){
		 this.fparam.myclasses.show.inprogress=true;
		 this.fparam.myclasses.show.completed=false;
		 this.fparam.myclasses.show.archived=false;
	 }
	 else if(this.fparam.myclasses.currentsatus =='completed'){
		 this.fparam.myclasses.show.inprogress=false;
		 this.fparam.myclasses.show.completed=true;
		 this.fparam.myclasses.show.archived=false;
	 }
	 else if(this.fparam.myclasses.currentsatus =='archived'){
		 this.fparam.myclasses.show.inprogress=false;
		 this.fparam.myclasses.show.completed=false;
		 this.fparam.myclasses.show.archived=true;
	 }
	 
	 console.log("inprogress: " +this.fparam.myclasses.show.inprogress);
	  console.log("completed: " +this.fparam.myclasses.show.completed);
	   console.log("archived: " +this.fparam.myclasses.show.archived);
  },
  
  mytrialstatuactive: function () {

	 if(this.fparam.mytrial.currentsatus =='inprogress'){
		 this.fparam.mytrial.show.inprogress=true;
		 this.fparam.mytrial.show.completed=false;
		 this.fparam.mytrial.show.archived=false;
	 }
	 else if(this.fparam.mytrial.currentsatus =='completed'){
		 this.fparam.mytrial.show.inprogress=false;
		 this.fparam.mytrial.show.completed=true;
		 this.fparam.mytrial.show.archived=false;
	 }
	 else if(this.fparam.mytrial.currentsatus =='archived'){
		 this.fparam.mytrial.show.inprogress=false;
		 this.fparam.mytrial.show.completed=false;
		 this.fparam.mytrial.show.archived=true;
	 }
	 
	 console.log("inprogress: " +this.fparam.mytrial.show.inprogress);
	  console.log("completed: " +this.fparam.mytrial.show.completed);
	   console.log("archived: " +this.fparam.mytrial.show.archived);
  },
   
   availableactivetab: function (tab) {
		if(tab=='classe'){
			this.fparam.currentab="availableclasse";
			this.fparam.available.show.classe=true;
			this.fparam.available.show.trail=false;
			this.fparam.available.show.classerecommended=false;
		}else if(tab=='trail'){
			this.fparam.currentab="availabletrail";
			this.fparam.available.show.classe=false;
			this.fparam.available.show.trail=true;
			this.fparam.available.show.classerecommended=false;
		}else if(tab=='classerecommended'){
			this.fparam.currentab="availableclasserecommended";
			this.fparam.available.show.classe=false;
			this.fparam.available.show.trail=false;
			this.fparam.available.show.classerecommended=true;
		}
	  this.ffilter.badiuform.type=this.fparam.currentab;
  },
  
  myactivetab: function (tab) {
		if(tab=='classe'){
			this.fparam.my.show.classe=true;
			this.fparam.my.show.trail=false;
			this.fparam.currentab="myclasse";
		}else if(tab=='trail'){
			this.fparam.my.show.classe=false;
			this.fparam.my.show.trail=true;
			this.fparam.currentab="mytrail";
		}
	  this.ffilter.badiuform.type=this.fparam.currentab;
  },
  
  carouselcleanimageurl: function () {
	  for (i = 0; i < this.rowsdata9.length; i++) {
		 var img=this.rowsdata9[i].image;
		img= img.split(' ').join('a');
		img= img.split('(').join('a');
		img= img.split(')').join('a');
		this.rowsdata9[i].image=img;
		
		}		
  },
  carouseloutbrowser: function  () {
   var self = this;
   var size=this.rowsdata9.length;
   
    if (this.fparam.carousel.index < size && size > 0 ) {
       //console.log( "index caroursel "+this.fparam.carousel.index );
      if(this.fparam.init==1){this.fparam.carousel.index++;}
      
	   if(this.fparam.carousel.index==(size)){this.fparam.carousel.index=0;}
       window.setTimeout( function() {
          self.carouseloutbrowser();
       }, 14000)
    }
},
  carouselnavegation: function  (operation) {
	  var size=this.rowsdata9.length;
	  if(operation=='next'){
		  this.fparam.carousel.index++;
		   if(this.fparam.carousel.index >= size){this.fparam.carousel.index=0;}
	  }
	  else if(operation=='back'){
		  this.fparam.carousel.index--;
		   if(this.fparam.carousel.index < 0 && size > 0  ){this.fparam.carousel.index=size-1;}
	  }else if(Number.isInteger(operation)){
		  this.fparam.carousel.index=operation;
	 }
	 console.log("linha atual");
	  console.log(this.rowsdata9[this.fparam.carousel.index]);
  },
showModalToReviewTrial: function (item,index) {
       this.fparam.mytrial.review.itemsected=item;
	   this.fparam.mytrial.review.itemindex=index;
       this.fparam.mytrial.review.showmodalcrt="block";
	   document.getElementById("_badiu_tms_my_default_trial_review").style.visibility = "visible";
	  this.fparam.mytrial.review.showmodalcrtclass	="modal fade show ";	
	  //reviewtrial(item,index)
},

showHideAdvancedsearch: function () {
	if(this.fparam.available.advancedsearch.classe){this.fparam.available.advancedsearch.classe=false;}
	else {this.fparam.available.advancedsearch.classe=true;}
	
	
},
 

enrolCancel: function (item,type) {
			//console.log("cancel enrol "+type); 
			var modulekey=null;
			if(type=='classe'){modulekey="badiu.tms.enrol.classe";}
			else if(type=='trial'){modulekey="badiu.tms.enrol.offer";}
		var self = this;
			this.formcontrol.processing=true;
			
			var pparam={_service: this.fparam._service,_function: "removeEnrol", moduleinstance: item.enrolid, modulekey: modulekey,objectid: item.id };  
			 axios.post(this.serviceurl,pparam).then(function (response) {
				   console.log(response.data); 
				  
				    if(response.data.status=='accept'){ 
						window.location.replace(self.currenturl);
					 }else if(response.data.status=='danied'){
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
						
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 	 
	 
  }, 
showModalPrivatePolicy: function () {
			var hasaccept=localStorage.getItem('badiu.sistem.core.policyprivate.accept');
			if(hasaccept){return null;}
			this.fparam.policyprivate.showmodalcrt="block";
			document.getElementById("_badiu_tms_my_default_policy_private").style.visibility = "visible";
			this.fparam.policyprivate.showmodalcrtclass	="modal fade show ";
			this.fparam.policyprivate.title=this.rowdata1.name;	   
			this.fparam.policyprivate.content=this.rowdata1.content;
	 
},
acceptPrivatePolicy: function () {
	localStorage.setItem('badiu.sistem.core.policyprivate.accept', 1);
	this.fparam.policyprivate.showmodalcrt="none";
},

showModalClasseInfo: function (type,index) {
			if(type=='available'){
				this.fparam.classeinfo.summary=this.rowsdata2[index].disciplinesummary;	   
				this.fparam.classeinfo.usertarget=this.rowsdata2[index].disciplineusertarget;
			}else if(type=='my'){
				this.fparam.classeinfo.summary=this.rowsdata3[index].disciplinesummary;	   
				this.fparam.classeinfo.usertarget=this.rowsdata3[index].disciplineusertarget;
			}else{
				this.fparam.classeinfo.summary="";	   
				this.fparam.classeinfo.usertarget="";
			}
			
			this.fparam.classeinfo.showmodalcrtclass="modal fade show ";
			this.fparam.classeinfo.showmodalcrt="block";
			document.getElementById("_badiu_tms_my_default_classe_info").style.visibility = "visible";
			
	 
},

showModalEnrolCancel: function (item,type) {
			this.fparam.enrolcancel.item=item;
			this.fparam.enrolcancel.type=type;
			this.fparam.enrolcancel.showmodalcrtclass="modal fade show ";
			this.fparam.enrolcancel.showmodalcrt="block";
			document.getElementById("_badiu_tms_my_default_enrol_cancel").style.visibility = "visible";
			
	 
},