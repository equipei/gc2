<?php 
$badiuSession =$container->get('badiu.system.access.session');
$entity=$badiuSession->get()->getEntity();
$repositoryid=$container->get('badiu.admin.cms.repository.data')->getIdByShortname($entity,'badiutmscorestudentfviewdefault');
$listitemadd=null;

if($repositoryid){
	$fmparam=array('repositoryid'=>$repositoryid,'enable'=>array('page'=>1,'file'=>1));
	$cmfactorytemplate=$container->get('badiu.admin.cms.resourece.factorytemplate');
	$listitemadd=$cmfactorytemplate->getListItemsAddUrl($fmparam);
}

?>

<!-- start search -->
<br />
<div class="row no-gutters" >
    <div class="col">
        <!--<input class="form-control input-search" @keyup.enter="searchclasse('available')" type="search" v-model="ffilter.badiuform.gsearch" id="courseavalaiblefilter" placeholder="Pesquise curso do seu interesse"> -->
        <input class="form-control input-search" @keyup.enter="searchsupplementarycontent('supplementarycontent')" type="search" @input="ffilter.badiuautocomplete.gsearch_istyping = true" placeholder="Pesquise os REDs de seu interesse" v-model="ffilter.badiuautocomplete.gsearch_query" @input="ffilter.badiuautocomplete.gsearch_istyping = true" id="courseavalaiblefilter" autocomplete="off" placeholder="Pesquise os REDs de seu interesse">

    </div>

    <div class="col-auto">
        <button class="btn btn-search mr-2" type="button" @click="searchsupplementarycontent('supplementarycontent')">
            <img src="<?php echo $baseresoursepath; ?>/images/icons/lupa.svg">
        </button>
       <!-- <a class="pointer ml-2" @click.prevent="showHideAdvancedsearch()"><i class="fas fa-chevron-down"></i></a> -->
    </div>

</div>
<div align="center" v-if="ffilter.badiuautocomplete.gsearch_isloading"> <span>Procurando...</span> </div>
<div class="list-group" v-if="ffilter.badiuautocomplete.gsearch_isshowlist"> <a v-for="sitem in ffilter.badiuautocomplete.gsearch_searchresult" @click.prevent="gsearch_itemselected(sitem)" href="#" class="list-group-item"> {{sitem.name}}</a> </div>

<!-- end search -->

<?php if(!empty($listitemadd)){?>
<div class="dropdown show">
  <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Adicionar contéudo
  </a>

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
   <?php foreach ($listitemadd as $lim) {
	   $label= $utildata->getVaueOfArray($lim, 'label');
	   $url= $utildata->getVaueOfArray($lim, 'url');
	   ?>
    <a class="dropdown-item" href="<?php echo $url; ?>"><?php echo $label; ?></a>
   <?php } ?>
  </div>
</div>
<?php } ?>
<div class="badiu-grid-cards my-4" v-if="rowsdata10">
							<div v-for="(item, index) in rowsdata10">
                                <div class="card-content">
                                   <!-- <span class="curso-tipo-status">
                                        <p><img src="images/icons/unlock.svg" class="mr-2">Curso livre</p>
										</span>
									-->
                                    <div>
                                        <div class="card-img">
                                            <img v-if="item.defaultimage" :src="item.defaultimage" class="card-image" alt="">

                                        </div>

                                        <div class="card-info d-flex align-items-baseline">

                                            <a v-if="item.disciplinecategoryurlview" :href="item.disciplinecategoryurlview" class="mr-1"><i class="fas fa-link"></i></a>
                                            <a v-if="item.disciplinecategoryurledit" :href="item.disciplinecategoryurledit" class="mr-1"><i class="fas fa-edit"></i></a>

                                            <span class="text-categoria" v-html="item.categoryname"></span>
                                        </div>

                                        <div class="card-desc">
                                            <div class="d-flex align-items-baseline mb-1">
												
                                                <a v-if="item.disciplineurlview" :href="item.disciplineurlview" class="mr-1"><i class="fas fa-link"></i></a>
                                                <a v-if="item.disciplineurledit" :href="item.disciplineurledit" class="mr-1"><i class="fas fa-edit"></i></a>

                                                <h5 class="card-title mb-0">
												 <span v-html="item.struturename"></span>
												</h5>
												 
                                            </div>
                                            <div class="d-flex align-items-center justify-content-between mb-4">
                                                <div class="d-flex align-items-baseline mb-1">
                                                    <a v-if="item.classeadminlmsurl" :href="item.classeadminlmsurl" class="mr-1"><i class="fas fa-external-link-alt"></i></a>
													<a v-if="item.classeurlview" :href="item.classeurlview" class="mr-1"><i class="fas fa-link"></i></a>
                                                    <a v-if="item.urledit" :href="item.urledit" class="mr-1"><i class="fas fa-edit"></i></a>

                                                    <span class="text-tipo text-color-4" v-html="item.name"></span>
                                                </div>
												 
                                                <span class="d-flex align-items-center ml-4 text-cargah text-color-4" v-html="item.typeshortname"></span>
												
												
                                            </div>
											
                                            <p v-html="item.abstractordescription"></p>
											
                                        </div>
                                    </div>

                                    <div class="card-options">
                                        

                                        <div class="d-flex align-items-center justify-content-between">
                                            <a v-if="item.itemviewurl && item.itemviewurltarget=='_blank'" target="_blank" :href="item.itemviewurl" class="btn btn-card-blue">Acessar</a>
											<a v-if="item.itemviewurl && item.itemviewurltarget!='_blank'" :href="item.itemviewurl" class="btn btn-card-blue">Acessar</a>
                                            
                                        </div>
                                    </div>
                                </div>
                            
                    </div>
						   
	</div>