<?php
$baseresoursepath = $container->get('templating.helper.assets')->getUrl('bundles/badiuthemecore/tms');
?>
<div class="badiu-report-list"> 
<h5 class="result-search">Resultado: <?php echo $infoResult; ?></h5>

<div class="container-fluid" v-if="tabledata"  v-for="(item, index) in tabledata">
  <div class="row">
    <div class="col-12 col-md-4">
      <div class="card-img">
         <img v-if="item.defaultimage.value" :src="item.defaultimage.value" class="card-img-top" alt="">
		</div>
		
		
			 
    </div>
	
    <div class="col-12 col-md-8">
     
	 <div class="card-info d-flex align-items-baseline">
                 <a v-if="item.disciplinecategoryurlview.value" :href="item.disciplinecategoryurlview.value" class="mr-1"><i class="fas fa-link"></i></a>
                 <a v-if="item.disciplinecategoryurledit.value" :href="item.disciplinecategoryurledit.value" class="mr-1"><i class="fas fa-edit"></i></a>
				 <span class="text-categoria" v-html="item.disciplinecategoryname.value"></span>
            </div>
			
			<div class="d-flex align-items-baseline">
				<a v-if="item.disciplineurlview.value" :href="item.disciplineurlview.value" class="mr-1"><i class="fas fa-link"></i></a>
                <a v-if="item.disciplineurledit.value" :href="item.disciplineurledit.value" class="mr-1"><i class="fas fa-edit"></i></a>
				<h5 class="card-title mb-0" v-html="item.disciplinename.value"></h5>
            </div>
			
			<div class="d-flex align-items-center  justify-content-start">
                <div class="d-flex align-items-baseline">
                   <a v-if="item.classeurlview.value" :href="item.classeurlview.value" class="mr-1"><i class="fas fa-link"></i></a>
                   <a v-if="item.classeurledit.value" :href="item.classeurledit.value" class="mr-1"><i class="fas fa-edit"></i></a>
					<span class="text-tipo text-color-4" v-html="item.name.value"></span>
                </div>
				<span class="d-flex align-items-center text-cargah text-color-4 pl-5">
                      <img src="<?php echo $baseresoursepath;?>/images/icons/clock_curso.svg" class="mr-1"> {{item.disciplinehour.value}}
                </span>
             </div>
			 
	 <div class="col" v-if="item.description.value">
					<span class="text-tipo"><?php echo $translator->trans('badiu.tms.offer.classe.descriptiontitle');?></span>
					 <div class="card-desc" v-html="item.description.value"></div>
				</div>
				<div class="col" v-if="item.disciplinesummary.value">
					<span class="text-tipo"><?php echo $translator->trans('badiu.ams.curriculum.discipline.summary');?></span>
					 <div class="card-desc" v-html="item.disciplinesummary.value"></div>
				</div>
				<div class="col" v-if="item.disciplineusertarget.value">
					<span class="text-tipo"><?php echo $translator->trans('badiu.ams.core.usertargetoffice');?></span>
					  <div class="card-desc"> <p  v-html="item.disciplineusertarget.value"></p></div>
				</div>
	
		<!-- button acccess progress -->
		<div class="card-options">
                      <div class="mb-3">
                          <div v-if="item.progress" v-html="item.progress.value"> </div>
                          <div class="d-flex align-items-center justify-content-between">
                           <p class="text-previsao mb-0" v-if="item.finalgrade.value" v-html="item.finalgrade.value"></p>
                             <small class="text-required" v-if="item.statusname.value" v-html="item.statusname.value"></small>
                           </div>
                         </div>
					<div class="d-flex align-items-center justify-content-between">
							<div v-html="item.classelmslink.value"></div> 
                            <a v-if="item.certificateurl.value" :href="item.certificateurl.value" class="btn btn-card-green" target="_blank">Emitir certificado</a>
       </div>
  </div>	
				 
    </div>
   

  </div>
  <hr />
</div>

<?php echo $pagingout; ?>
</div>