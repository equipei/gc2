<!-- start modal review trial-->

<div id="_badiu_tms_my_default_enrol_cancel" style="visibility:hidden">
    <div id="_badiu_tms_my_default_enrol_cancel_review_status" v-bind:class="[fparam.enrolcancel.showmodalcrtclass]" v-bind:style="{ display: fparam.enrolcancel.showmodalcrt }" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" v-html="fparam.enrolcancel.title"></h5>
                    <button type="button" class="close" v-on:click="fparam.enrolcancel.showmodalcrt = 'none'" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
				 <div class="modal-body">
					<div v-if="this.fparam.enrolcancel.type=='classe'"><?php echo $translator->trans('badiu.tms.enrol.classe.cancelenrolmessageconfirm');?></div>
					<div v-if="this.fparam.enrolcancel.type=='trial'"><?php echo $translator->trans('badiu.tms.enrol.offer.cancelenrolmessageconfirm');?></div>
				 </div>
				
                <div class="modal-footer">
				   <button type="button" class="btn btn-card-red" @click="enrolCancel(fparam.enrolcancel.item,fparam.enrolcancel.type)"><?php echo $translator->trans('badiu.tms.enrol.cancelenrol');?></button>
                    <button type="button" class="btn btn-secondary" v-on:click="fparam.enrolcancel.showmodalcrt = 'none'"><?php echo $translator->trans('badiu.system.action.close');?></button>
                   
                </div>
            </div>
        </div>
    </div>
</div>


<!--end modal -->