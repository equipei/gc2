<div class="badiu-grid-cards my-4" v-if="fparam.my.show.trail">
    <div v-for="(item, index) in fparam.mytrial.list">
        <div class="card-content">
            <!-- <span class="curso-tipo-status">
                                        <p><img src="images/icons/unlock.svg" class="mr-2">Curso livre</p>
										</span>
									-->
            <div>
                <div class="card-img">
                    <img v-if="item.defaultimage" :src="item.defaultimage" class="card-image" alt="">

                </div>

				
                <div class="card-desc">
					<p v-if="item.updateinfo" class="alert alert-info" v-html="item.updateinfo"></p>
                    <div class="d-flex align-items-baseline mb-1">

                        <a v-if="item.offerurlview" :href="item.offerurlview" class="mr-1"><i class="fas fa-link"></i></a>
                        <a v-if="item.offerurledit" :href="item.offerurledit" class="mr-1"><i class="fas fa-edit"></i></a>

                        <h5 class="card-title mb-0" v-html="item.name">
                        </h5>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <div class="d-flex align-items-baseline mb-1">

                            <span class="text-tipo text-color-4">Quant. de curso: {{item.countdiscipline}}</span>
                        </div>

                        <span class="d-flex align-items-center ml-4 text-cargah text-color-4">
                            <img src="<?php echo $baseresoursepath; ?>/images/icons/clock_curso.svg" class="mr-1"> {{item.disciplinehour}}
                        </span>
                    </div>
                    <p v-html="item.description"></p>
                    <div v-html="item.enroldisciplinestable"></div>
                </div>
            </div>

            <div class="card-options">
                <div class="mb-3">
                    <div v-if="item.progress" v-html="item.progress"> </div>
                    <div class="d-flex align-items-center justify-content-between">
                        <p class="text-previsao mb-0" v-if="item.progress"> Cursos obrigatórios concluídos: {{item.countdisciplinecompleted}} de {{item.countdiscipline}} </p>
                        <small class="text-required" v-if="item.progress" v-html="item.statusname"></small>
                    </div>
                </div>

                <div class="d-flex align-items-center justify-content-between">
					<a v-if="item.cancancelenrol" @click="showModalEnrolCancel(item,'trial')" class="btn btn-card-red" title=""><?php echo $translator->trans('badiu.tms.enrol.cancelenrol');?></a>
                    <a v-if="item.certificateurl" :href="item.certificateurl" class="btn btn-card-green" target="_blank"><?php echo $translator->trans('badiu.tms.enrol.getcertificate');?></a>
                    <a v-if="item.review" @click="showModalToReviewTrial(item,index)" class="btn btn-card-green" title="Foi adicionado novo curso na trilha. Ao clicar rem refazer, sua matrícula será alterada para inscrito e a trilha será levada para aba em andamento. Assim, poderá se realizar os novos cursos adicionados na trilha">Refazer a trilha</a>

                </div>


            </div>
        </div>

    </div>

</div>
<!-- end course list -->

<!-- start modal review trial-->

<div id="_badiu_tms_my_default_trial_review" style="visibility:hidden">
    <div id="_badiu_tms_my_default_trial_review_status" v-bind:class="[fparam.mytrial.review.showmodalcrtclass]" v-bind:style="{ display: fparam.mytrial.review.showmodalcrt }" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Reativar trilha</h5>
                    <button type="button" class="close" v-on:click="fparam.mytrial.review.showmodalcrt = 'none'" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Ao reativar a trilha, o status na trilha será alterado para cursando. A trilha será levada para a aba de trilhas em andamento. Um novo certificado poderá ser emitido ao realizar os novos cursos adicionados na trilha.

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" @click="reviewtrial()">Reativar</button>
                    <button type="button" class="btn btn-secondary" v-on:click="fparam.mytrial.review.showmodalcrt = 'none'">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!--end modal  delete /remove-->