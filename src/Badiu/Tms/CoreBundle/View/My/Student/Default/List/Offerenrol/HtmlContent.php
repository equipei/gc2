<?php
$baseresoursepath = $container->get('templating.helper.assets')->getUrl('bundles/badiuthemecore/tms');

?>
<div class="badiu-report-list"> 
<h5 class="result-search">Resultado: <?php echo $infoResult; ?></h5>

<div class="container-fluid" v-if="tabledata"  v-for="(item, index) in tabledata">
  <div class="row">
    <div class="col-12 col-sm-4">
   
         <img v-if="item.defaultimage.value" :src="item.defaultimage.value" class="card-img-top" alt="">
	
    </div>
    <div class="col-12 col-sm-8">
      <div class="row">
		
		<div class="col-12">
			<div class="card-info d-flex align-items-baseline">
                 <a v-if="item.offerurlview.value" :href="item.offerurlview.value" class="mr-1"><i class="fas fa-link"></i></a>
                 <a v-if="item.offerurledit.value" :href="item.offerurledit.value" class="mr-1"><i class="fas fa-edit"></i></a>
				 <h5 class="card-title mb-0" v-html="item.name.value"></h5>
            </div>
			
			
			<div class="d-flex align-items-center  justify-content-start">
                <div class="d-flex align-items-baseline mb-1">
                    <span class="text-tipo text-color-4">Quant. de curso: {{item.countdiscipline.value}}</span> 
                </div>
				<span class="d-flex align-items-center text-cargah text-color-4 pl-5">
                      <img src="<?php echo $baseresoursepath;?>/images/icons/clock_curso.svg" class="mr-1"> {{item.disciplinehour.value}}
                </span>
             </div>
		</div>
		<div class="col-12">
				<div class="card-desc">
					<p v-if="item.description.value"   v-html="item.description.value"></p>
					<p v-if="item.enroldisciplinestable.value"  class="card-desc" v-html="item.enroldisciplinestable.value"></p>
				</div>
				
				
				 <div class="card-options">
					<div class="mb-3">
                         <div v-if="item.progress.value" v-html="item.progress.value"> </div>
                            <div class="d-flex align-items-center justify-content-between">
                                 <p class="text-previsao mb-0" v-if="item.progress.value"> Cursos obrigatórios concluídos: {{item.countdisciplinecompleted.value}} de {{item.countdiscipline.value}} </p>
                                <small class="text-required" v-if="item.progress.value" v-html="item.statusname.value"></small>
                            </div>
                     </div>
											
                        <div class="d-flex align-items-center justify-content-start">
                                <a v-if="item.certificateurl.value" :href="item.certificateurl.value" class="btn btn-card-green" target="_blank">Emitir certificado</a>
								<a v-if="item.review.value" @click="showModalToReviewTrial(item,index)"  class="btn btn-card-green" title="Foi adicionado novo curso na trilha. Ao clicar rem refazer, sua matrícula será alterada para inscrito e a trilha será levada para aba em andamento. Assim, poderá se realizar os novos cursos adicionados na trilha">Refazer a trilha</a>
						</div>
                 </div>
				 
				 
			
		</div>
	  </div>
    </div>
    
  </div>
  <hr />
</div>

<?php echo $pagingout; ?>
</div>