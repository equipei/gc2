<?php

$permission = $container->get('badiu.system.access.permission');
$permissioncimg = $permission->has_access('badiu.admin.cms.resourcecontentimage.add', $page->getSessionhashkey());
$linkcourselmanage = "";
if ($permissioncimg) {
	$clparam = array('type' => 'redirectbyshortname', 'shortname' => 'badiutmscorestudentfviewdefault', '_function' => 'send', 'service' => 'badiu.admin.cms.repository.data', 'route' => 'badiu.admin.cms.resourcecontentimage.index');
	$url = $utilapp->getUrlByRoute('badiu.system.core.proxy.link', $clparam);

	$linkcourselmanage = "<a class=\"edit-carousel\" href=\"$url\"><i class=\"fa fa-edit\"></i></a>";
}

//print custom css
$datarows9 = $utildata->getVaueOfArray($page->getData(), 'badiu_list_data_rows.data.9', true);

$html9templatecustom = "";
$html9custom = "";
$templatelist = array();
foreach ($datarows9 as $drow) {
	$templateid = $utildata->getVaueOfArray($drow, 'templateid');
	//$templatecustomcss=$utildata->getVaueOfArray($drow,'templatecustomcss');
	$templatecustomjs = $utildata->getVaueOfArray($drow, 'templatecustomjs');
	//$templatelist[$templateid]=$templatecustomjs.$templatecustomcss;
	$templatelist[$templateid] = $templatecustomjs;

	//$customcss=$utildata->getVaueOfArray($drow,'customcss');
	$customjs = $utildata->getVaueOfArray($drow, 'customjs');

	$html9custom .= $customjs;
	//$html9custom.=$customcss;
}

foreach ($templatelist as $trow) {
	$html9templatecustom .= $trow;
}
$html = $html9templatecustom;
echo $html;

?>
<div v-if="rowsdata9[fparam.carousel.index]">
	<div v-html="rowsdata9[fparam.carousel.index].templatecustomcss"></div>
	<div v-html="rowsdata9[fparam.carousel.index].customcss"></div>
</div>
<?php echo $linkcourselmanage; ?>
<div v-if="rowsdata9[fparam.carousel.index]" class="badiu-carousel" v-bind:style="{ 'background-image': 'url(' + rowsdata9[fparam.carousel.index].image + ')' }">
	<div class="badiu-carousel-controle badiu-carousel-controle-left" @click.prevent="carouselnavegation('back')">
		<img src="<?php echo $baseresoursepath; ?>/images/icons/arrow_left.svg">
	</div>
	<div class="badiu-carousel-controle badiu-carousel-controle-right" @click.prevent="carouselnavegation('next')">
		<img src="<?php echo $baseresoursepath; ?>/images/icons/arrow_right.svg">
	</div>
	<div v-if="rowsdata9[fparam.carousel.index]" class="badiu-carousel-itens">
		<div class="badiu-carousel-item">
			<a class="badiu-carousel-link" v-bind:href="rowsdata9[fparam.carousel.index].defaulturl">
				<div class="badiu-carousel-item-titulo" v-html="rowsdata9[fparam.carousel.index].title"></div>
			</a>
			<div class="badiu-carousel-item-texto" v-html="rowsdata9[fparam.carousel.index].description"></div>
		</div>
	</div>
</div>

<div class="badiu-carousel-bullets my-2">
	<div class="badiu-carousel-bullet" v-for="(item, index) in rowsdata9">
		<div v-if="fparam.carousel.index==index" @click.prevent="carouselnavegation(index)" class="badiu-carousel-bullet-ativo">&#8226;</div>
		<div v-if="fparam.carousel.index!=index" @click.prevent="carouselnavegation(index)" class="badiu-carousel-bullet-normal">&#8226;</div>
	</div>
</div>