<div id="myclassesearch" v-if="fparam.my.show.classe">
	<div class="row no-gutters">
		<div class="col">
			<!--<input class="form-control input-search" @keyup.enter="searchclasse('my')" type="search" v-model="ffilter.badiuform.gsearch" id="courseavalaiblefilter" placeholder="Pesquise curso do seu interesse"> -->
			<input class="form-control input-search" @keyup.enter="searchclasse('my')" type="search" @input="ffilter.badiuautocomplete.gsearch_istyping = true" placeholder="Pesquise os cursos em que você está inscrito" v-model="ffilter.badiuautocomplete.gsearch_query" @input="ffilter.badiuautocomplete.gsearch_istyping = true" id="mycoursefilter" autocomplete="off" placeholder="Pesquise os cursos em que você está inscrito">

		</div>

		<div class="col-auto">
			<button class="btn btn-search" type="button" @click="searchclasse('my')">
				<img src="<?php echo $baseresoursepath; ?>/images/icons/lupa.svg">
			</button>
			<a class="pointer ml-2" @click.prevent="showHideAdvancedsearch()"><i class="fas fa-chevron-down"></i></a>
		</div>

	</div>
	<div v-if="ffilter.badiuautocomplete.gsearch_isloading"> <span>Procurando...</span> </div>
	<div class="list-group" v-if="ffilter.badiuautocomplete.gsearch_isshowlist"> <a v-for="sitem in ffilter.badiuautocomplete.gsearch_searchresult" @click.prevent="gsearch_itemselected(sitem)" href="#" class="list-group-item"> {{sitem.name}}</a> </div>

	<div class="row advancedsearch pt-4" v-if="fparam.available.advancedsearch.classe && fparam.available.show.classe">
		<?php
		$cont = 0;
		foreach ($formfields1 as $fk => $field) {
			$flabel = $utildata->getVaueOfArray($field, 'label');
			$type = $utildata->getVaueOfArray($field, 'type');
			$formfieldto->init($field);
			$fdata = $formfactoryhtmllib->get($formfieldto, false);
			$cont++;
			if ($cont > 2 && $type!='hidden' ) {

		?>
				<div class="col-12 col-md-3 my-1">
					<label for="periodo"><?php echo $flabel; ?></label>
					<?php echo $fdata; ?>
				</div>
		<?php }
		} ?>
		<div class="form-group col-12"> <button type="button" @click="searchclasse('my')" class="btn btn-primary">Pesquisar</button></div>

	</div>

</div>