<?php

namespace Badiu\Tms\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuTreeDataBase;
class TmsTreeDataBase  extends BadiuTreeDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
     
   public function getFormChoice($entity,$param=array(),$orderby="") {
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id,o.name FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.dtype = :dtype $wsql $orderby";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('dtype','training');
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return  $result;
    }
 

 public function findByEntity($entity,$orderby="",$deleted=false) {
             $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity  AND o.dtype = :dtype AND o.deleted=:deleted $orderby";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
             $query->setParameter('dtype','training');
             $query->setParameter('deleted',$deleted);
            $result= $query->getResult();
            return  $result;
        }

 public function getFormChoiceParent($param=array('dtype'=>'training')) {
        $result= parent::getFormChoiceParent($param);
        return  $result;
 }
 
  public function getFormChoiceParentKeyId($param=array('dtype'=>'training','typeofkey'=>'id')) {
	
        $result=parent::getFormChoiceParent($param);
		
        return $result;
}
}
