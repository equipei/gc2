<?php

namespace Badiu\Tms\CoreBundle\Model;
use Badiu\Ams\OfferBundle\Model\Classe\ClassePermissionAccessFilter;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CorePermissionAccessFilter extends  ClassePermissionAccessFilter {

	function __construct(Container $container) {
		parent::__construct($container);
	}
 
   
	
	public function checkInstanceAccessRow(){
		//check level of parentid
		$intancelevel=$this->getInstanceLevel();
		$result=0;
		$parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		$result=$this->checkDisiciplineCategoryByLevel($parentid,$intancelevel);
		
		return $result;
	}

	public function getInstanceLevel(){
		$result="";
		$routerlib = $this->getContainer()->get('badiu.system.core.lib.config.routerlib');
		$key=$routerlib->getKey();
	
		$pos=stripos($key, "badiu.tms.offer.vdiscipline");
		if($pos!== false){return 'discipline';}
		
		$pos=stripos($key, "badiu.tms.offer.classe.");
		if($pos!== false){ return 'odiscipline';}
		
		$pos=stripos($key, "badiu.tms.offer.classe");
		if($pos!== false){ return 'classe';}
		
		return null;
	}

	public function checkDisiciplineCategoryByLevel($parentid,$intancelevel){
		$userid=$this->getUserid();
		$entity=$this->getEntity();
		 $sql="";
		
		 if($intancelevel=='discipline'){
			 $sql = "SELECT COUNT(d.id) AS countrecord FROM BadiuAmsDisciplineBundle:AmsDiscipline d JOIN d.categoryid dct 	JOIN BadiuSystemAccessBundle:SystemAccessUserRecord aur WITH ( aur.userid =$userid AND  aur.moduleinstance=dct.id ) WHERE aur.modulekey=:modulekey AND aur.entity = dct.entity AND d.entity=:entity AND d.id=:parentid ";
		 }else if($intancelevel=='odiscipline'){
			 $sql = "SELECT COUNT(od.id) AS countrecord FROM BadiuAmsOfferBundle:AmsOfferDiscipline od JOIN od.disciplineid d JOIN d.categoryid dct 	JOIN BadiuSystemAccessBundle:SystemAccessUserRecord aur WITH ( aur.userid =$userid AND  aur.moduleinstance=dct.id ) WHERE aur.modulekey=:modulekey AND aur.entity = dct.entity AND d.entity=:entity AND od.id=:parentid ";
		 }else if($intancelevel=='classe'){
			 $sql = "SELECT COUNT(cl.id) AS countrecord FROM BadiuAmsOfferBundle:AmsOfferClasse cl JOIN cl.odisciplineid od JOIN od.disciplineid d JOIN d.categoryid dct 	JOIN BadiuSystemAccessBundle:SystemAccessUserRecord aur WITH ( aur.userid =$userid AND  aur.moduleinstance=dct.id ) WHERE aur.modulekey=:modulekey AND aur.entity = dct.entity AND d.entity=:entity AND cl.id=:parentid ";
		 }
		
		 $disciplinedata = $this->getContainer()->get('badiu.tms.discipline.discipline.data');
		 $query = $disciplinedata->getEm()->createQuery($sql);
		 $query->setParameter('entity',$entity);
		 $query->setParameter('parentid',$parentid);
		 $query->setParameter('modulekey','badiu.tms.discipline.category');
		 
		 $result= $query->getOneOrNullResult();
		 $countrecord=$this->getUtildata()->getVaueOfArray($result,'countrecord');
		
		 return $countrecord;
	}	
}


