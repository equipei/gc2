<?php
 
namespace Badiu\Tms\CoreBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
       $result=$this->initDbStatus() ;
          $result+=  $this->initDefaultCourse();
    	   $result+= $this->initDefaultCourseOffer();
           $result+= $this->initDbSystemAccessRole();
		   $result+= $this->initDbAccessRole();
		   $result+= $this->initDbRole();
           $result+= $this->initPermissions();
           $result+= $this->initDbType();
           $result+= $this->initDbTypemanager();
           $result+= $this->initDbTypeaccess();
           $result+= $this->initDbClasseStatus();
		   $result+=$this->initDbOfferStatus();
		   $result+=$this->initDbOfferDisciplineStatus();
		   $result+=$this->initDbEntityConfig();
		   $result+=$this->initDbSchedulerTask();
  		return $result;
	} 
	
    public function initDbSystemAccessRole() {
        $cont=0;
        $entity=$this->getEntity();
        $data = $this->getContainer()->get('badiu.system.access.role.data');
        $exist=$data->countGlobalRow(array('entity'=>$entity));
        if($exist && !$this->getForceupdate()){return 0;}
        
         $entity=$this->getEntity();
        
       
        $list=array(
           array('entity'=>$entity,'shortname'=>'studentcorporate','defaultroute'=>null,'name'=>$this->getTranslator()->trans('badiu.tms.role.role.student'),'defaultroute'=>'badiu.tms.my.studentfviewdefault.dashboard'),
           array('entity'=>$entity,'shortname'=>'teachearcorporate','defaultroute'=>null,'name'=>$this->getTranslator()->trans('badiu.tms.role.role.teachear'),'defaultroute'=>'badiu.tms.my.teachear.frontpage'),
           array('entity'=>$entity,'shortname'=>'coordenatorcorporate','defaultroute'=>null,'name'=>$this->getTranslator()->trans('badiu.tms.role.role.coordenator')),
           
        );
        
        foreach ($list as $param) {
            $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
            $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
            $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
            
            $paramedit=null;
            if($this->getForceupdate()){
                $paramedit= $param;
            }
            $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
            $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
            $r=$this->getUtildata()->getVaueOfArray($result,'id');
             if($r){$cont++;}
           }
           
      return $cont;
        
    }
	 public function initDbRole() {
         $cont=0;
         $dtype='training';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.role.role.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist && !$this->getForceupdate()){return 0;}
         
		  $entity=$this->getEntity();
		 $datarole = $this->getContainer()->get('badiu.system.access.role.data');
		 $studentroleid=$datarole->getIdByShortname($entity,'studentcorporate');
		 $teachearroleid=$datarole->getIdByShortname($entity,'teachearcorporate');
		 $coordenatorroleid=$datarole->getIdByShortname($entity,'coordenatorcorporate');
		
         $list=array(
			array('entity'=>$entity,'dtype'=>$dtype,'shortname'=>'studentcorporate','sysroleid'=>$studentroleid,'lmssynckey'=>'student','name'=>$this->getTranslator()->trans('badiu.ams.role.role.student')),
			array('entity'=>$entity,'dtype'=>$dtype,'shortname'=>'teachearcorporate','sysroleid'=>$teachearroleid,'lmssynckey'=>'editingteacher','name'=>$this->getTranslator()->trans('badiu.ams.role.role.teachear')),
			array('entity'=>$entity,'dtype'=>$dtype,'shortname'=>'coordenatorcorporate','sysroleid'=>$coordenatorroleid,'lmssynckey'=>'manager','name'=>$this->getTranslator()->trans('badiu.ams.role.role.coordenator')),
			
		 );
		 
		 foreach ($list as $param) {
			 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
			 
			 $paramedit=null;
			 if($this->getForceupdate()){
				 $paramedit= $param; 
			 }
			 $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname,'dtype'=>$dtype);
			 $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
			 $r=$this->getUtildata()->getVaueOfArray($result,'id');
              if($r){$cont++;}
            }
			
       return $cont;
         
     }
	 
	 public function initDbAccessRole() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.system.access.role.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
        // if($exist && !$this->getForceupdate()){return 0;}
         
		
         $list=array(
			array('entity'=>$entity,'shortname'=>'studentcorporate','defaultroute'=>'badiu.tms.my.studentfviewdefault.dashboard','name'=>$this->getTranslator()->trans('badiu.tms.role.role.student'),'deleted'=>0),
			array('entity'=>$entity,'shortname'=>'teachearcorporate','name'=>$this->getTranslator()->trans('badiu.tms.role.role.teachear'),'deleted'=>0),
			array('entity'=>$entity,'shortname'=>'coordenatorcorporate','name'=>$this->getTranslator()->trans('badiu.tms.role.role.coordenator'),'deleted'=>0),
			
		 );
		 
		 foreach ($list as $param) {
			 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 
			 $paramedit=null;
			 if($this->getForceupdate()){
				 $paramedit= $param; 
			 }
			 $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
			 $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
			 $r=$this->getUtildata()->getVaueOfArray($result,'id');
              if($r){$cont++;}
            }
			
       return $cont;
         
     }
	 
	 public function initDbStatus() {
         $cont=0;
         $dtype='training';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.enrol.status.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist && !$this->getForceupdate()){return 0;}
        
		$entity=$this->getEntity();
         $list=array(
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'preregistrationenrol','level'=>'offer,discipline,classe','crole'=>'student,teachear,coordenator','enable'=>0,'lmssyncenable'=>1,'enableaccesslms'=>1,'dtag'=>'','sortorder'=>1,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.preregistration'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.preregistration.abbreviation')),
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'activeenrol','level'=>'offer,discipline,classe','crole'=>'student,teachear,coordenator','enable'=>1,'lmssyncenable'=>1,'enableaccesslms'=>1,'dtag'=>'','sortorder'=>2,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.active'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.active.abbreviation')),
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'inactiveenrol','level'=>'offer,discipline,classe','crole'=>'student,teachear,coordenator','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>3,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.inactive'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.inactive.abbreviation')),
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'studyingenrol','level'=>'offer,discipline,classe','crole'=>'student','enable'=>1,'lmssyncenable'=>1,'enableaccesslms'=>1,'dtag'=>'absent,atriskofevasion','sortorder'=>4,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.studying'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.studying.abbreviation')),
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'suspendedenrol','level'=>'offer,discipline,classe','crole'=>'student,teachear,coordenator','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>5,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.suspended'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.suspended.abbreviation')),
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'lockedenrol','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>6,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.locked'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.locked.abbreviation')),
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'canceledenrol','level'=>'offer,discipline,classe','crole'=>'student,teachear,coordenator','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'adminprocess','sortorder'=>7,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.canceled'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.canceled.abbreviation')),
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'approvedenrol','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'bygrade,byequivalence,adminprocess','sortorder'=>8,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.approved'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.approved.abbreviation')),
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'disapprovedenrol','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'bygrade,byattendance','sortorder'=>9,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.disapproved'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.disapproved.abbreviation')),
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'retiredenrol','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>10,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.retired'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.retired.abbreviation')),
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'quitterenrol','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>11,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.quitter'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.quitter.abbreviation')),
			array('entity'=>$entity,'dtype'=>'training','shortname'=>'evadedenrol','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>12,'name'=>$this->getTranslator()->trans('badiu.tms.enrol.status.evaded'),'abbreviation'=>$this->getTranslator()->trans('badiu.tms.enrol.status.evaded.abbreviation')),
			
		 ); 
		 
         
         
         foreach ($list as $param) {
			 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
			 
			 $paramedit=null;
			 if($this->getForceupdate()){
				 $paramedit= $param;
			 }
			 $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname,'dtype'=>$dtype);
			 $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
			 $r=$this->getUtildata()->getVaueOfArray($result,'id');
              if($r){$cont++;}
            }
	return $cont;
    }

     public function initDefaultCourse() {
         $cont=0;
         $dtype='training';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.course.course.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'shortname'=>'tmsdefaultcourse'));
         if($exist){return 0;}
         
        $param=array();
        $param['entity']=$entity;
        $param['name']=$this->getTranslator()->trans('badiu.tms.course.default');
        $param['shortname']='tmsdefaultcourse';
        $param['timecreated']=new \DateTime();
         $param['deleted']=0;
        $param['dtype']=$dtype;
        if(!$data->existByShortname($entity,'tmsdefaultcourse')){
            $result = $data->insertNativeSql($param,false); 
			if( $result){return 1;}
        }
        return 0;
         
     }
	 
	 public function initDefaultCourseOffer() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.offer.offer.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'shortname'=>'tmsdefaultcourseoffer'));
         if($exist){return 0;}
         $dtype='training';
        $param=array();
        $param['entity']=$entity;
        $param['name']=$this->getTranslator()->trans('badiu.tms.course.defaultoffer'); 
		$param['courseid']=$this->getContainer()->get('badiu.ams.course.course.data')->getIdByShortname($entity,'tmsdefaultcourse');
        $param['shortname']='tmsdefaultcourseoffer'; 
		$param['dtype']=$dtype;
        $param['timecreated']=new \DateTime();
         $param['deleted']=0;
       
        if(!$data->existByShortname($entity,'tmsdefaultcourseoffer')){ 
            $result = $data->insertNativeSql($param,false); 
			if( $result){return 1;}
        }
        return 0;
         
     }


  

    public function initPermissions() {
        $cont=0;
		
        $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');

		//guest
		$param=array(
          'roleshortname'=>'guest',
          'permissions'=>array('badiu.tms.my.studentfviewdefault.dashboard',
          'badiu.tms.my.studentofferavailableflist.index',
          'badiu.tms.my.studentofferavailable.view',
          'badiu.tms.my.studentofferavailable.index',
          'badiu.tms.my.studentclasseavailableflist.index',
          'badiu.tms.my.studentclasseavailable.view',
		  'badiu.tms.my.studentclasseavailable.index',
		  'badiu.tms.my.studentclasseavailable.view',
		  'badiu.tms.my.studentclasseavailable.view',
		  'badiu.tms.my.studentclasseavailable.view',
		  'badiu.system.core.service.process/badiu.tms.my.studentfview.defaultlib'
		  )
        );
        $cont+=$libpermission->add($param);
		
		//authenticateduser
		$param=array(
          'roleshortname'=>'authenticateduser',
          'permissions'=>array('badiu.tms.my.studentfviewdefault.dashboard',
		  'badiu.tms.discipline.dependency.index',
          'badiu.tms.my.studentsupplementarycontent.index',
          'badiu.tms.my.studentofferavailableflist.index',
          'badiu.tms.my.studentofferavailable.view',
          'badiu.tms.my.studentofferavailable.index',
          'badiu.tms.my.studentclasseavailableflist.index',
          'badiu.tms.my.studentclasseavailable.view',
		  'badiu.tms.my.studentclasseavailable.index',
		  'badiu.tms.my.studentclasseavailable.view',
		  'badiu.tms.my.studentclasseavailable.view',
		  'badiu.tms.my.studentclasseavailable.view',
		  'badiu.admin.certificate.request.link',
		  'badiu.system.core.service.process/badiu.moodle.core.lib.remoteaccess/remoteAuth',
		  'badiu.system.core.service.process/badiu.tms.my.studentfview.defaultlib'
		  
		  )
        );
        $cont+=$libpermission->add($param);

        //student  
        $param=array(
          'roleshortname'=>'studentcorporate',
          'permissions'=>array('badiu.tms.my.student.frontpage',
          'badiu.tms.my.studentoffer.dashboard',
          'badiu.tms.my.studentoffercurriculum.dashboard',
          'badiu.tms.my.studentclasseenrol.index',
          'badiu.tms.my.studentclasseenrolactive.index',
          'badiu.tms.my.studentclasseenrolinactive.index',
          'badiu.tms.my.studentdmoodleenrol.index',
		  'badiu.admin.certificate.request.link',
		  
		  'badiu.tms.my.studentofferenrolflist.index:',
		  'badiu.tms.my.studentofferenrol.view',
		  'badiu.tms.my.studentofferenrol.index',
		  'badiu.tms.my.studentclasseenrolflist.index',
		  'badiu.tms.my.studentclasseenrol.view',
		  'badiu.tms.my.studentclasseenrol.index',
		  'badiu.admin.certificate.request.link'
		   
		  )
        );
        $cont+=$libpermission->add($param);

        //teachear
        $param=array(
            'roleshortname'=>'teachearcorporate',
            'permissions'=>array('badiu.tms.my.teachear.frontpage',
			'badiu.tms.my.teachearclasseenrol.index',
			'badiu.tms.offer.classe.moodle',
			'badiu.tms.offer.classeattendance',
			'badiu.tms.offer.classeenrol',
			'badiu.tms.offer.classegrade',
			'badiu.tms.offer.classeview',
            'badiu.tms.offer.classeview.frontpage')
		);
		
          $cont+=$libpermission->add($param);

          //coordenator
        $param=array(
            'roleshortname'=>'coordenatorcorporate',
            'permissions'=>array('badiu.tms.core.frontpage',
			'badiu.tms.offer.classe.',
			'badiu.tms.offer.classe.moodle',
			'badiu.tms.offer.classeattendance',
			'badiu.tms.offer.classeenrol',
			'badiu.tms.offer.classegrade',
			'badiu.tms.offer.classeview',
			'badiu.tms.offer.vdiscipline.',
			'badiu.tms.discipline.discipline.index')
          );
          $cont+=$libpermission->add($param);
    }
 
    
    public function initDbTypemanager() {
      $cont=0;
      $entity=$this->getEntity();
      $data = $this->getContainer()->get('badiu.tms.offer.typemanager.data');
      $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>'training'));
      if($exist && !$this->getForceupdate()){return 0;}
      
       $entity=$this->getEntity();
      
     
      $list=array(
         array('entity'=>$entity,'shortname'=>'trainingwithteacher','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.typemanager.trainingwithteacher')),
         array('entity'=>$entity,'shortname'=>'trainingwithoutteacher','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.typemanager.trainingwithoutteacher')),
         array('entity'=>$entity,'shortname'=>'trainingtutoriale','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.typemanager.trainingtutorial')),
         array('entity'=>$entity,'shortname'=>'trainingvideoconference','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.typemanager.trainingvideoconference')),
         
      );
      
      foreach ($list as $param) {
          $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
          $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
          $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
          
          $paramedit=null;
          if($this->getForceupdate()){
              $paramedit= $param;
          }
          $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
          $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
          $r=$this->getUtildata()->getVaueOfArray($result,'id');
           if($r){$cont++;}
         }
         
    return $cont;
      
  }

  public function initDbTypeaccess() {
    $cont=0;
    $entity=$this->getEntity();
    $data = $this->getContainer()->get('badiu.tms.offer.typeaccess.data');
    $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>'training'));
    if($exist && !$this->getForceupdate()){return 0;}
    
     $entity=$this->getEntity();
    
   
    $list=array(
       array('entity'=>$entity,'shortname'=>'trainingfree','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.typeaccess.trainingfree')),
       array('entity'=>$entity,'shortname'=>'trainingrestricted','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.typeaccess.trainingrestricted')),
      
    );
    
    foreach ($list as $param) {
        $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
        $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
        $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
        
        $paramedit=null;
        if($this->getForceupdate()){
            $paramedit= $param;
        }
        $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
        $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
        $r=$this->getUtildata()->getVaueOfArray($result,'id');
         if($r){$cont++;}
       }
       
  return $cont;
    
}


public function initDbType() {
  $cont=0;
  $entity=$this->getEntity();
  $data = $this->getContainer()->get('badiu.tms.offer.type.data');
  $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>'training'));
  if($exist && !$this->getForceupdate()){return 0;}
  
   $entity=$this->getEntity();
  
 
  $list=array(
     array('entity'=>$entity,'shortname'=>'trainingelearning','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.type.elearning')),
     array('entity'=>$entity,'shortname'=>'trainingpresential','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.type.presential')),
     array('entity'=>$entity,'shortname'=>'trainingblended','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.type.blended')),
    
  );
  
  foreach ($list as $param) {
      $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
      $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
      $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
      
      $paramedit=null;
      if($this->getForceupdate()){
          $paramedit= $param;
      }
      $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
      $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
      $r=$this->getUtildata()->getVaueOfArray($result,'id');
       if($r){$cont++;}
     }
     
return $cont;
  
}

public function initDbClasseStatus() {
  $cont=0;
  $entity=$this->getEntity();
  $data = $this->getContainer()->get('badiu.ams.offer.classestatus.data');
  $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>'training'));
  if($exist && !$this->getForceupdate()){return 0;}
  
   $entity=$this->getEntity();
  
 
  $list=array(
     array('entity'=>$entity,'shortname'=>'classeactive','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.classestatus.active')),
     array('entity'=>$entity,'shortname'=>'classecanceled','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.classestatus.canceled')),
     array('entity'=>$entity,'shortname'=>'classeongoing','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.classestatus.ongoing')),
     array('entity'=>$entity,'shortname'=>'classeclosed','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.classestatus.closed')),
    
  );
  
  foreach ($list as $param) {
      $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
      $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
      $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
      
      $paramedit=null;
      if($this->getForceupdate()){
          $paramedit= $param;
      }
      $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
      $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
      $r=$this->getUtildata()->getVaueOfArray($result,'id');
       if($r){$cont++;}
     }
     
return $cont;
  
}


public function initDbOfferStatus() {
  $cont=0;
  $entity=$this->getEntity();
  $data = $this->getContainer()->get('badiu.ams.offer.status.data');
  $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>'training'));
  if($exist && !$this->getForceupdate()){return 0;}
  
   $entity=$this->getEntity();
  
 
  $list=array(
     array('entity'=>$entity,'shortname'=>'offeractive','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.status.active')),
	 array('entity'=>$entity,'shortname'=>'offerinactive','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.status.inactive')),
     array('entity'=>$entity,'shortname'=>'offersuspense','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.status.suspense')),
      
  );

  foreach ($list as $param) {
      $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
      $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
      $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
      
      $paramedit=null;
      if($this->getForceupdate()){
          $paramedit= $param;
      }
      $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
      $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
      $r=$this->getUtildata()->getVaueOfArray($result,'id');
       if($r){$cont++;}
     }
     
return $cont;
  
}



public function initDbOfferDisciplineStatus() {
  $cont=0;
  $entity=$this->getEntity();
  $data = $this->getContainer()->get('badiu.ams.offer.disciplinestatus.data');
  $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>'training'));
  if($exist && !$this->getForceupdate()){return 0;}
  
   $entity=$this->getEntity();
  
 
  $list=array(
     array('entity'=>$entity,'shortname'=>'offeractive','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.disciplinestatus.active')),
     array('entity'=>$entity,'shortname'=>'offercanceled','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.disciplinestatus.canceled')),
     array('entity'=>$entity,'shortname'=>'offerongoing','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.disciplinestatus.ongoing')),
	 array('entity'=>$entity,'shortname'=>'offerclosed','dtype'=>'training','name'=>$this->getTranslator()->trans('badiu.tms.offer.disciplinestatus.closed')),
    
  );

  foreach ($list as $param) {
      $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
      $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
      $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
      
      $paramedit=null;
      if($this->getForceupdate()){
          $paramedit= $param;
      }
      $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
      $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
      $r=$this->getUtildata()->getVaueOfArray($result,'id');
       if($r){$cont++;}
     }
     
return $cont;
  
}


public function initDbEntityConfig() {
  $cont=0;
  $entity=$this->getEntity();
  $data = $this->getContainer()->get('badiu.system.module.configentity.data');
 
  
   $entity=$this->getEntity();
  
 
  $list=array(
     
	array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'badiu.system.core.param.config.site.route.frontpagewihtoulogin','value'=>'badiu.tms.my.studentfviewdefault.dashboard','enableinheritance'=>0),
     array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'badiu.system.core.param.config.site.route.frontpage','value'=>'badiu.tms.my.studentfviewdefault.dashboard','enableinheritance'=>0),
	 array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'badiu.system.core.param.config.site.route.error404','value'=>'badiu.tms.my.studentfviewdefault.dashboard','enableinheritance'=>0),
	      
      
	 array('entity'=>$entity,'bundlekey'=>'badiu.theme.core','name'=>'badiu.theme.core.core.param.config.headcontentmenu','value'=>'[
{"label": "Página inicial","route": {"key": "badiu.tms.my.studentfviewdefault.dashboard"}},
{"label": "Curso","route": {"key": "badiu.tms.discipline.discipline.index"},"ckeckperm": 1},
{"label": "Turma","route": {"key": "badiu.tms.offer.classeg.index"},"ckeckperm": 1},
{"label": "Trilha","route": {"key": "badiu.tms.offer.offer.index"},"ckeckperm": 1},
{"label": "Matrícula","route": {"key": "badiu.tms.enrol.classe.index"},"ckeckperm": 1},
{"label": "Administração","route": {"key": "badiu.system.core.core.frontpage"},"ckeckperm": 1}

]','enableinheritance'=>0),
	 array('entity'=>$entity,'bundlekey'=>'badiu.tms.core','name'=>'badiu.system.core.menu','value'=>'badiu.tms.menu=badiu.tms.core.frontpage&badiu.system.user.user.menu=badiu.system.user.default.index&badiu.system.access.menu=badiu.system.access.role.index&badiu.system.module.menu=badiu.system.module.module.index&badiu.system.scheduler.menu=badiu.system.scheduler.core.frontpage&badiu.util.menu=badiu.util.core.core.frontpage&badiu.admin.menu=badiu.admin.core.core.frontpage&badiu.admin.server.appclient.menu=r|badiu.admin.server.appclient.index|_mkey/badiu.system.core.frontpage&badiu.admin.selection.menu.content=r|badiu.admin.selection.project.index','enableinheritance'=>0),
	// array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'badiu.system.core.menu.content','value'=>'badiu.tms.menu.content=badiu.tms.core.frontpage&badiu.system.user.user.menu.content=badiu.system.user.default.index&badiu.system.access.menu.content=badiu.system.access.role.index&badiu.system.module.menu.content=badiu.system.module.module.index&badiu.system.scheduler.menu.content=badiu.system.scheduler.core.frontpage&badiu.util.menu.content=badiu.util.core.core.frontpage&badiu.admin.menu.content=badiu.admin.core.core.frontpage&badiu.admin.server.appclient.menu.content=r|badiu.admin.server.appclient.index|_mkey/badiu.system.core.frontpage&badiu.admin.selection.menu.content=r|badiu.admin.selection.project.index','enableinheritance'=>0),
	// array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'badiu.system.core.menu.category','value'=>'badiu.moodle=badiu.admin.server.appclient.menu&badiu.system=badiu.system.access.menu,badiu.system.module.menu,badiu.system.scheduler.menu&badiu.system.core.otheroptions=badiu.admin.menu,badiu.util.menu','enableinheritance'=>0),
	// array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'badiu.system.core.menu.content.category','badiu.moodle=badiu.admin.server.appclient.menu.content&badiu.system=badiu.system.access.menu.content,badiu.system.module.menu.content,badiu.system.scheduler.menu.content&badiu.system.core.otheroptions=badiu.admin.menu.content,badiu.util.menu.content','enableinheritance'=>0),
	 
	 array('entity'=>$entity,'bundlekey'=>'badiu.admin.core','name'=>'badiu.admin.core.menu','value'=>'badiu.admin.cms.menu=badiu.admin.cms.cms.frontpage&badiu.admin.selection.menu=badiu.admin.selection.core.frontpage&badiu.admin.form.menu=badiu.admin.form.core.frontpage&badiu.admin.project.menu=badiu.admin.project.core.frontpage&badiu.admin.enterprise.menu=badiu.admin.enterprise.enterprise.frontpage','enableinheritance'=>0),
	// array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'badiu.admin.core.menu.content','value'=>'badiu.admin.cms.menu.content=badiu.admin.cms.cms.frontpage&badiu.admin.selection.menu.content=badiu.admin.selection.core.frontpage&badiu.admin.form.menu.content=badiu.admin.form.core.frontpage&badiu.admin.project.menu.content=badiu.admin.project.core.frontpage&badiu.admin.enterprise.menu.content=badiu.admin.enterprise.enterprise.frontpage','enableinheritance'=>0),
	 
	 array('entity'=>$entity,'bundlekey'=>'badiu.util.core','name'=>'badiu.util.core.menu','value'=>'badiu.util.address.menu=badiu.util.address.address.frontpage&badiu.util.document.menu=badiu.util.document.document.frontpage','enableinheritance'=>0),
	 array('entity'=>$entity,'bundlekey'=>'badiu.util.core','name'=>'badiu.util.core.menu.content','value'=>'badiu.util.address.menu.content=badiu.util.address.address.frontpage&badiu.util.document.menu=badiu.util.document.document.frontpage','enableinheritance'=>0),
	
	array('entity'=>$entity,'bundlekey'=>'badiu.admin.client','name'=>'badiu.admin.client.menu','value'=>'frontpage=r|badiu.tms.my.studentfviewdefault.dashboard&badiu.admin.client.profile.menu=badiu.admin.client.defaultprofile.dashboard&badiu.admin.client.changepassword.menu=badiu.admin.client.changepassword.add&badiu.tms.my.studentfviewdefault.dashboard.menu=badiu.tms.my.studentfviewdefault.dashboard&badiu.tms.my.teachear.menu=r|badiu.tms.my.teachear.frontpage&badiu.tms.my.coordenator.menu=r|badiu.tms.discipline.discipline.index','enableinheritance'=>0),
	array('entity'=>$entity,'bundlekey'=>'badiu.admin.client','name'=>'badiu.admin.client.menu.content','value'=>'badiu.admin.client.profile.menu.content=badiu.admin.client.defaultprofile.dashboard&badiu.admin.client.changepassword.menu.content=badiu.admin.client.changepassword.add&badiu.tms.my.studentfviewdefault.dashboard.menu.content=badiu.tms.my.studentfviewdefault.dashboard&badiu.tms.my.teachear.menu.content=r|badiu.tms.my.teachear.frontpage&badiu.tms.my.coordenator.menu.content=r|badiu.tms.discipline.discipline.index','enableinheritance'=>0),
	
	/*array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'','value'=>'','enableinheritance'=>0),
	array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'','value'=>'','enableinheritance'=>0),
	array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'','value'=>'','enableinheritance'=>0),
	array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'','value'=>'','enableinheritance'=>0),
	array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'','value'=>'','enableinheritance'=>0),
	array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'','value'=>'','enableinheritance'=>0),
	array('entity'=>$entity,'bundlekey'=>'badiu.system.core','name'=>'','value'=>'','enableinheritance'=>0),*/	
    
  );
  
  foreach ($list as $param) {
      $name=$this->getUtildata()->getVaueOfArray($param,'name');
      $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
    
      
      $paramedit=null;
      if($this->getForceupdate()){
          $paramedit= $param;
      }
      $paramcheckexist=array('entity'=>$entity,'name'=>$name);
      $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
      $r=$this->getUtildata()->getVaueOfArray($result,'id');
       if($r){$cont++;}
     }
     
return $cont;
  
}
public function initDbSchedulerTask(){
	$cont=0;
  $entity=$this->getEntity();
  $data = $this->getContainer()->get('badiu.system.scheduler.task.data');
  //$exist=$data->countGlobalRow(array('entity'=>$entity));
  //if($exist && !$this->getForceupdate()){return 0;}
  
   $entity=$this->getEntity();
  
 
  $list=array(
     array('tcron'=>'cron1','lcron'=>'entity','lmodel'=>100,'entity'=>$entity,'shortname'=>'monitor_processing','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.system.scheduler.task.monitor.default.processing'),'address'=>'badiu.system.scheduler.task.monitor','status'=>'active','doperation'=>'update','sortorder'=>10,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"timeunit": "minute", "value": 5 }','param'=>'{"status": "processing"}'),
     array('tcron'=>'cron1','lcron'=>'entity','lmodel'=>100,'entity'=>$entity,'shortname'=>'monitor_failed','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.system.scheduler.task.monitor.default.failed'),'address'=>'badiu.system.scheduler.task.monitor','status'=>'active','doperation'=>'update','sortorder'=>20,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"timeunit": "minute", "value": 5 }','param'=>'{"status": "failed"}'),
     array('tcron'=>'cron1','lcron'=>'entity','lmodel'=>100,'entity'=>$entity,'shortname'=>'monitor_queueanalysis','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.system.scheduler.task.monitor.default.queueanalysis'),'address'=>'badiu.system.scheduler.task.monitor','status'=>'active','doperation'=>'update','sortorder'=>30,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"timeunit": "minute", "value": 5 }','param'=>'{"status": "queueanalysis"}'),
	 
	  //moodlesyncrecentdata
	 array('tcron'=>'cron1','lcron'=>'entity','lmodel'=>100,'entity'=>$entity,'shortname'=>'moodlesyncrecentdata','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.ams.core.scheduler.task.moodlesyncrecentdata'),'address'=>'badiu.ams.core.lib.lmsmoodlesynctask','status'=>'active','doperation'=>'update','sortorder'=>100,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"value":"1","timeunit":"minute"}','param'=>$this->getTaskCommandSyncMoodleRecent(300,5000)),
	 array('tcron'=>'cron1','lcron'=>'entity','lmodel'=>100,'entity'=>$entity,'shortname'=>'moodlesyncrecentdata1h','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.ams.core.scheduler.task.moodlesyncrecentdata1h'),'address'=>'badiu.ams.core.lib.lmsmoodlesynctask','status'=>'active','doperation'=>'update','sortorder'=>110,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"value":"60","timeunit":"minute"}','param'=>$this->getTaskCommandSyncMoodleRecent(3600,10000)),
	 array('tcron'=>'cron1','lcron'=>'entity','lmodel'=>100,'entity'=>$entity,'shortname'=>'moodlesyncrecentdata6h','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.ams.core.scheduler.task.moodlesyncrecentdata6h'),'address'=>'badiu.ams.core.lib.lmsmoodlesynctask','status'=>'active','doperation'=>'update','sortorder'=>120,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"value":"6","timeunit":"hour"}','param'=>$this->getTaskCommandSyncMoodleRecent(21600,20000)),
	 array('tcron'=>'cron1','lcron'=>'entity','lmodel'=>100,'entity'=>$entity,'shortname'=>'moodlesyncrecentdata24h','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.ams.core.scheduler.task.moodlesyncrecentdata24h'),'address'=>'badiu.ams.core.lib.lmsmoodlesynctask','status'=>'active','doperation'=>'update','sortorder'=>130,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"value":"24","timeunit":"hour"}','param'=>$this->getTaskCommandSyncMoodleRecent(86400,50000)),
	 array('tcron'=>'cron1','lcron'=>'entity','lmodel'=>100,'entity'=>$entity,'shortname'=>'moodlesyncrecentdata7dh','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.ams.core.scheduler.task.moodlesyncrecentdata7d'),'address'=>'badiu.ams.core.lib.lmsmoodlesynctask','status'=>'active','doperation'=>'update','sortorder'=>140,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"value":"7","timeunit":"day"}','param'=>$this->getTaskCommandSyncMoodleRecent(604800,80000)),
	 array('tcron'=>'cron1','lcron'=>'entity','lmodel'=>100,'entity'=>$entity,'shortname'=>'moodlesyncrecentdata30d','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.ams.core.scheduler.task.moodlesyncrecentdata30d'),'address'=>'badiu.ams.core.lib.lmsmoodlesynctask','status'=>'active','doperation'=>'update','sortorder'=>150,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"value":"30","timeunit":"day"}','param'=>$this->getTaskCommandSyncMoodleRecent(2592000,100000)),
	 
	 array('tcron'=>'cron1','lcron'=>'entity','lmodel'=>100,'entity'=>$entity,'shortname'=>'moodlesyncgeneraldata','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.ams.core.scheduler.task.moodlesyncgeneraldata'),'address'=>'badiu.ams.core.lib.lmsmoodlesynctask','status'=>'inactive','doperation'=>'update','sortorder'=>190,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"value":"30","timeunit":"day"}','param'=>$this->getTaskCommandSyncMoodleGeneral(100000)),
	  
    //trial
	 array('entity'=>$entity,'lcron'=>'entity','lmodel'=>100,'shortname'=>'trialodisciplineenrol','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.tms.core.scheduler.task.trialodisciplineenrol'),'address'=>'badiu.tms.enrol.lib.offersynctask','status'=>'active','doperation'=>'import','sortorder'=>200,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"value":"5","timeunit":"minute"}','param'=>'{"operation":"addnewenrol","maxrecord":500}'),
	 array('entity'=>$entity,'lcron'=>'entity','lmodel'=>100,'shortname'=>'trialupdateenolcompleted','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.tms.core.scheduler.task.trialupdateenolcompleted'),'address'=>'badiu.tms.enrol.lib.offersynctask','status'=>'active','doperation'=>'update','sortorder'=>210,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"value":"5","timeunit":"minute"}','param'=>'{"operation":"updatecompletedofferenrol","maxrecord":500}'),
	 array('entity'=>$entity,'lcron'=>'entity','lmodel'=>100,'shortname'=>'trialtrialodisciplineenrolcompleted','dtype'=>'service','name'=>$this->getTranslator()->trans('badiu.tms.core.scheduler.task.trialtrialodisciplineenrolcompleted'),'address'=>'badiu.tms.enrol.lib.offersynctask','status'=>'active','doperation'=>'update','sortorder'=>220,'simultaneousexec'=>0,'timeroutine'=>'system_timeinterval','timeroutineparam'=>'{"value":"5","timeunit":"minute"}','param'=>'{"operation":"updatecompleteddisciplineenrol","maxrecord":500}'),

    
  );

  foreach ($list as $param) {
      $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
      $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
     
      $paramedit=null;
      if($this->getForceupdate()){
          $paramedit= $param;
      }
      $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
      $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
      $r=$this->getUtildata()->getVaueOfArray($result,'id');
       if($r){$cont++;}
     }
     
	 return $cont;
	
}

	
	private function getTaskCommandSyncMoodleRecent($period=300,$maxrecord=5000){
	
	$commndstr='{"operation":"syncrecentdata", "lasttime": '.$period.', "maxrecord": '.$maxrecord.', "execafter": {"service": "badiu.ams.enrol.lib.managechangestatus/changeClasseEnrolStatus", "defaultroles": 0,"roles": [
		 {"criteria":"coursenotcompletedallactivitycompleted","statusshortname":"preregistrationenrol","newstatusshortname":"studyingenrol","activeclasse":1},
 {"criteria":"coursenotcompletednotallactivitycompleted","statusshortname":"preregistrationenrol","newstatusshortname":"studyingenrol","activeclasse":1},
                {"criteria":"coursenotcompletednotallactivitycompleted","statusshortname":"activeenrol","newstatusshortname":"studyingenrol","activeclasse":1},
				{"criteria":"coursenotcompletedallactivitycompleted","statusshortname":"studyingenrol","newstatusshortname":"disapprovedenrol","activeclasse":1,"waiting": {"time": 900, "itemtype": "finalgrade" }},
  
		 {"criteria":"coursecompleted","statusshortname":"preregistrationenrol","newstatusshortname":"approvedenrol","activeclasse":1},
		 {"criteria":"coursecompleted","statusshortname":"activeenrol","newstatusshortname":"approvedenrol","activeclasse":1},
		 {"criteria":"coursecompleted","statusshortname":"disapprovedenrol","newstatusshortname":"approvedenrol","activeclasse":1},
         {"criteria":"coursecompleted","statusshortname":"studyingenrol","newstatusshortname":"approvedenrol","activeclasse":1},
		 {"criteria":"coursecompleted","statusshortname":"evadedenrol","newstatusshortname":"approvedenrol","activeclasse":1},
		 {"criteria":"coursecompleted","statusshortname":"quitterenrol","newstatusshortname":"approvedenrol","activeclasse":1}
	   
	   ] }}';
	
	
	   return $commndstr;
	   
	   /*
	    
	     {"criteria":"coursenotcompletedallactivitycompleted","statusshortname":"studyingenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletednotallactivitycompleted","statusshortname":"studyingenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyactivitycompleted","statusshortname":"preregistrationenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingrestricted","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingrestricted","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"preregistrationenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingrestricted","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		
	   */
	}
	
	private function getTaskCommandSyncMoodleGeneral($maxrecord=5000){
	
	$commndstr='{"operation":"syncgeneraldata", "maxrecord":'.$maxrecord.', "execafter": {"service": "badiu.ams.enrol.lib.managechangestatus/changeClasseEnrolStatus", "defaultroles": 0,"roles": [
		 {"criteria":"coursenotcompletedallactivitycompleted","statusshortname":"preregistrationenrol","newstatusshortname":"studyingenrol","activeclasse":1},
 {"criteria":"coursenotcompletednotallactivitycompleted","statusshortname":"preregistrationenrol","newstatusshortname":"studyingenrol","activeclasse":1},
                {"criteria":"coursenotcompletednotallactivitycompleted","statusshortname":"activeenrol","newstatusshortname":"studyingenrol","activeclasse":1},
  {"criteria":"coursenotcompletedallactivitycompleted","statusshortname":"studyingenrol","newstatusshortname":"disapprovedenrol","activeclasse":1,"waiting": {"time": 900, "itemtype": "finalgrade" },"filter":{"service":"badiu.ams.enrol.lib.managechangestatusfilter/check","minfinalgrade": 60}},
	     
		 {"criteria":"coursecompleted","statusshortname":"disapprovedenrol","newstatusshortname":"approvedenrol","activeclasse":1},
                 {"criteria":"coursecompleted","statusshortname":"studyingenrol","newstatusshortname":"approvedenrol","activeclasse":1},
		 {"criteria":"coursecompleted","statusshortname":"evadedenrol","newstatusshortname":"approvedenrol","activeclasse":1},
		 {"criteria":"coursecompleted","statusshortname":"quitterenrol","newstatusshortname":"approvedenrol","activeclasse":1},
{"criteria":"coursecompleted","statusshortname":"preregistrationenrol","newstatusshortname":"approvedenrol","activeclasse":1},
 {"criteria":"coursecompleted","statusshortname":"activeenrol","newstatusshortname":"approvedenrol","activeclasse":1}
	   
	   ] }}';
	
	
	   return $commndstr;
	   
	   /*
	   {"criteria":"coursenotcompletedallactivitycompleted","statusshortname":"studyingenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletednotallactivitycompleted","statusshortname":"studyingenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyactivitycompleted","statusshortname":"preregistrationenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingrestricted","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingrestricted","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"preregistrationenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyactivitycompleted","classetypeaccessshortname":"trainingrestricted","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 
	   
	   */
	}
}
