<?php

namespace Badiu\Tms\CoreBundle\Model\My\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class StudentFviewdDefaultLib extends BadiuModelLib {

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
  
	public function searchClasse() {
		 $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$param['entity']=$this->getEntity();
		$param['timenow1']=new \DateTime();
		$param['timenow2']=new \DateTime();
		$dhor1=$this->getUtildata()->getVaueOfArray($param,'dhour_badiunumber1');
		$dhor2=$this->getUtildata()->getVaueOfArray($param,'dhour_badiunumber2');
		
		if($dhor1){$param['dhour_badiunumber1']=$dhor1=$dhor1*60;}
		if($dhor2){$param['dhour_badiunumber2']=$dhor2=$dhor2*60;}
		
		$sqlindex=$this->getUtildata()->getVaueOfArray($param,'_sqlindex');
		if($sqlindex==3){$param['userid']=$this->getUserid();}
		/* $fparam=array();
		 $fparam['entity']=$this->getEntity();
		 $fparam['_sqlindex']=$this->getUtildata()->getVaueOfArray($param,'_sqlindex');
		 $fparam['gsearch']=$this->getUtildata()->getVaueOfArray($param,'gsearch');*/
		 $this->getSearch()->getKeymanger()->setBaseKey('badiu.tms.my.studentfviewdefault');
		 $result=$this->getSearch()->searchList($param);	 
		 return $result; 
		  
	}
	
	public function searchTrial() {
		 $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		 $fparam=array();
		 $fparam['entity']=$this->getEntity();
		 $fparam['_sqlindex']=$this->getUtildata()->getVaueOfArray($param,'_sqlindex');
		 $fparam['trial']=$this->getUtildata()->getVaueOfArray($param,'trial');
		 $sqlindex=$this->getUtildata()->getVaueOfArray($param,'_sqlindex');
		 if($sqlindex==6){$fparam['userid']=$this->getUserid();}
		 $this->getSearch()->getKeymanger()->setBaseKey('badiu.tms.my.studentfviewdefault');
		 $result=$this->getSearch()->searchList($fparam);	 
		 return $result; 
		  
	}
	
	public function searchSupplementaryContent() {
		 $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$param['entity']=$this->getEntity();
		
		
		$sqlindex=$this->getUtildata()->getVaueOfArray($param,'_sqlindex');
		//if($sqlindex==3){$param['userid']=$this->getUserid();}
		
		 $this->getSearch()->getKeymanger()->setBaseKey('badiu.tms.my.studentfviewdefault');
		 $result=$this->getSearch()->searchList($param);	
		 
		 return $result; 
		  
	}
	public function reviewTrial() {
		 $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		 $fparam=array();
		 $fparam['entity']=$this->getEntity();
		 $fparam['id']=$this->getUtildata()->getVaueOfArray($param,'enrolid');
		 $enroldata=$this->getContainer()->get('badiu.ams.enrol.offer.data');
		 
		 $statusdata=$this->getContainer()->get('badiu.ams.enrol.status.data');
		 $statusapprovedenrolid=$statusdata->getIdByShortname($this->getEntity(),'approvedenrol'); 
		 $statusstudyingenrolid=$statusdata->getIdByShortname($this->getEntity(),'studyingenrol'); 
		 if(!$statusapprovedenrolid){return 'badiu.ams.enrol.sttus.error.shortnameapprovedenrolnotfound';}
		 if(!$statusstudyingenrolid){return 'badiu.ams.enrol.sttus.error.shortnamestudyingenrolnotfound';}
		 
		 $fparam['statusid']=$statusapprovedenrolid;
		 
		 $existenrol=$enroldata->countGlobalRow($fparam);
		 if(!$existenrol){return 'badiu.ams.enrol.offer.error.statusapprovednotfound';}
		 $fparam['statusid']=$statusstudyingenrolid;
		 $fparam['timefinish']=null;
		 $fparam['timemodified']=new \DateTime();
		 if(!$statusapprovedenrolid){return 'badiu.ams.enrol.offer.statusnisnotapprovedenrol';}
		 $r= $enroldata->updateNativeSql($fparam,false);
		 if($r){return 'badiu.ams.enrol.offer.message.changestatuwithsucess';}
		 return 'badiu.ams.enrol.offer.error.changestatusfailed';; 
		  
	}
	
	 public function getTagForAutocomplete() {
		 $gsearch=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
		 $type=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('type'); 
		 $badiuSession=$this->getContainer()->get('badiu.system.access.session');
       
		$badiuSession->setHashkey($this->getSessionhashkey());
		   
		$userid=$badiuSession->get()->getUser()->getId();
		if(empty($userid)){$userid=0; }
		$entity=$this->getEntity();
		$wsql="";
        $sql=""; 
		//echo "type: $type";exit;
        if(!empty($gsearch)){
           $gsearch=strtolower($gsearch);
		   $gsearch="'%".$gsearch."%'";
        }else{return null;}
		$tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
		$defaultsqlfilter=$this->getContainer()->get('badiu.tms.my.studentfviewdefault.defaultsqlfilter');
		
		if($type=='availableclasse'){
			$wsql=$defaultsqlfilter->adddfilteravailableclasse();
			$sql="SELECT DISTINCT tgaut.value AS name FROM BadiuSystemModuleBundle:SystemModuleTag tgaut WHERE tgaut.entity = $entity  AND (SELECT COUNT(o.id) FROM BadiuAmsOfferBundle:AmsOfferClasse o JOIN o.statusid s JOIN o.odisciplineid od JOIN od.disciplineid d JOIN d.categoryid dct JOIN od.offerid f LEFT JOIN f.courseid c   WHERE tgaut.entity=o.entity AND f.shortname='tmsdefaultcourseoffer' AND d.dtype='training' AND  o.entity=:entity AND o.deleted=:deleted  $wsql AND ((tgaut.modulekey='badiu.tms.offer.classe' AND o.id=tgaut.moduleinstance  AND LOWER(tgaut.value) LIKE $gsearch ) OR (tgaut.modulekey='badiu.tms.discipline.discipline' AND d.id=tgaut.moduleinstance   AND LOWER(tgaut.value) LIKE $gsearch ) OR (tgaut.modulekey='badiu.tms.discipline.category' AND dct.id=tgaut.moduleinstance   AND LOWER(tgaut.value) LIKE $gsearch )) ) > 0 ";
			
		}
		else if($type=='myclasse'){
			$sql="SELECT DISTINCT tgaut.value AS name FROM BadiuSystemModuleBundle:SystemModuleTag tgaut WHERE tgaut.entity = $entity  AND (SELECT COUNT(o.id) FROM BadiuAmsEnrolBundle:AmsEnrolClasse o JOIN o.roleid r JOIN o.statusid s JOIN o.classeid cl JOIN cl.statusid cls JOIN cl.odisciplineid od JOIN od.disciplineid d JOIN d.categoryid dct JOIN od.offerid f LEFT JOIN f.courseid c  WHERE tgaut.entity=o.entity AND f.shortname='tmsdefaultcourseoffer' AND d.dtype='training' AND  o.entity=:entity AND o.userid=:userid  AND o.deleted=:deleted $wsql AND ((tgaut.modulekey='badiu.tms.offer.classe' AND cl.id=tgaut.moduleinstance   AND LOWER(tgaut.value) LIKE $gsearch ) OR (tgaut.modulekey='badiu.tms.discipline.discipline' AND d.id=tgaut.moduleinstance   AND LOWER(tgaut.value) LIKE $gsearch )  OR (tgaut.modulekey='badiu.tms.discipline.category' AND dct.id=tgaut.moduleinstance   AND LOWER(tgaut.value) LIKE $gsearch )) ) > 0 ";
			}
		//content review type		
		else if($type=='supplementarycontent') {
			$sql="SELECT DISTINCT tgaut.value AS name FROM BadiuSystemModuleBundle:SystemModuleTag tgaut WHERE tgaut.entity = $entity  AND (SELECT COUNT(o.id) FROM  BadiuAdminCmsBundle:AdminCmsResource o JOIN o.repositoryid r JOIN o.statusid s LEFT JOIN o.templateid t LEFT JOIN o.listabstracttemplateid lat  LEFT JOIN o.structureid st LEFT JOIN o.categoryid ct JOIN o.typeid tp WHERE o.entity=:entity AND r.shortname='badiutmscorestudentfviewdefault' AND s.shortname='approval' AND o.typeid > 0 AND o.deleted=:deleted $wsql AND ((tgaut.modulekey='badiu.admin.cms.resourcecontent' AND o.id=tgaut.moduleinstance   AND LOWER(tgaut.value) LIKE $gsearch ) OR (tgaut.modulekey='badiu.admin.cms.resourcecategory' AND o.categoryid=tgaut.moduleinstance   AND LOWER(tgaut.value) LIKE $gsearch )  OR (tgaut.modulekey='badiu.admin.cms.resourcetype' AND o.typeid=tgaut.moduleinstance   AND LOWER(tgaut.value) LIKE $gsearch )  OR (tgaut.modulekey='badiu.admin.cms.resourcestructure' AND o.structureid=tgaut.moduleinstance   AND LOWER(tgaut.value) LIKE $gsearch )) ) > 0  ";
		}
	   $timenow=new \DateTime();
        $query = $tagdata->getEm()->createQuery($sql);
        $query->setParameter('entity',$this->getEntity());
		$query->setParameter('deleted',0);
		
		if($type=='availableclasse'){
			$query->setParameter('timenow1' ,$timenow);
			$query->setParameter('timenow2',$timenow);
		}else if($type=='myclasse'){
			$query->setParameter('userid' ,$userid);
		}

        $query->setMaxResults(10);
        $result= $query->getResult();
        return  $result;
    }
	
	
	public function removeEnrol() {
		 $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		 $userid=$this->getUserid();
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 
		 $lparam=array();
		 $lparam['modulekey']=$modulekey;
		 $lparam['moduleinstance']=$moduleinstance;
		 
		$result= $this->getContainer()->get('badiu.ams.enrol.lib.managedelete')->add($lparam);
		 
		 $fparam=array();
		 $fparam['entity']=$this->getEntity();
		 $fparam['id']=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $fparam['userid']=$userid;
		 
		 if($modulekey=='badiu.ams.enrol.classe' || $modulekey=='badiu.tms.enrol.classe'){
			 $enroldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
			 $result=$enroldata->removeNativeSql($fparam);
		 }
		 else if($modulekey=='badiu.ams.enrol.offer' || $modulekey=='badiu.tms.enrol.offer'){
			 $enroldata=$this->getContainer()->get('badiu.ams.enrol.offer.data');
			 $result=$enroldata->removeNativeSql($fparam);
		 }
		 
		 //remove from selection
		 $smoduleinstance=$this->getUtildata()->getVaueOfArray($param,'objectid');
		 $smodulekey=null;
		 if($modulekey=='badiu.ams.enrol.classe' ){ $smodulekey='badiu.ams.offer.classe';}
		 else if($modulekey=='badiu.tms.enrol.classe' ){ $smodulekey='badiu.tms.offer.classe';}
		 else if($modulekey=='badiu.ams.enrol.offer' ){ $smodulekey='badiu.ams.offer.offer';}
		 else if($modulekey=='badiu.tms.enrol.offer' ){ $smodulekey='badiu.tms.offer.offer';}
		
		 
		 
		 
		  $projectdata=$this->getContainer()->get('badiu.admin.selection.project.data');
		  $pparam=array('modulekey'=>$smodulekey,'moduleinstance'=>$smoduleinstance);
		  $projectid=$projectdata->getGlobalColumnValue('id', $pparam);
		 
		  if(!empty($projectid) && !empty($userid)){
			 $requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
			  $rparam=array('entity'=>$this->getEntity(),'userid'=>$userid,'projectid'=>$projectid);
			 $requestdata->removeBatchNativeSql($rparam,false);
		  }
		   
		 return $result; 
		  
	}
}
