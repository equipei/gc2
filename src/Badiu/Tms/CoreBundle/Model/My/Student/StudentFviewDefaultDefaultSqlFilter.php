<?php

namespace Badiu\Tms\CoreBundle\Model\My\Student;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class StudentFviewDefaultDefaultSqlFilter extends BadiuSqlFilter{
    /**
     * @var object
     */
  
    function __construct(Container $container) {
            parent::__construct($container);
       }

        function exec() {
           
           return null;
        }
        
        function adddfilteravailableclasse() {

			$wsql="";
			$wsql.=$this->addfilterwithoutenroledclasse();
			$wsql.=$this->addfilteractiveclasse();
			$wsql.=$this->addfilteravailableclassetag();
			$wsql.=$this->addsortorderavailableclasse();
			

			return $wsql;
		}
		
		
		function adddfiltermyclasse() {
			$wsql="";
			$wsql.=$this->addfiltermyclassetag();
			$wsql.=$this->addsortordermyclasse();
			return $wsql;
		}
		
		
		function addfilteravailableclassetag() {
			$wsql="";
			$gsearch=$this->getUtildata()->getVaueOfArray($this->getParam(),'gsearch');
			if(!empty($gsearch)){
				$gsearch=strtolower($gsearch);
				$gsearch="'%".$gsearch."%'";
				//$gswsql=" LOWER(CONCAT(o.id,o.name,d.name,dct.name)) LIKE $gsearch ";
				$tagwsql1=" (SELECT COUNT(tg1) FROM BadiuSystemModuleBundle:SystemModuleTag tg1 WHERE tg1.moduleinstance=o.id AND tg1.modulekey='badiu.tms.offer.classe' AND LOWER(tg1.value) LIKE $gsearch  ) > 0 ";
				$tagwsql2=" (SELECT COUNT(tg2) FROM BadiuSystemModuleBundle:SystemModuleTag tg2 WHERE tg2.moduleinstance=d.id AND tg2.modulekey='badiu.tms.discipline.discipline'  AND LOWER(tg2.value) LIKE $gsearch ) > 0 ";
				$tagwsql3=" (SELECT COUNT(tg3) FROM BadiuSystemModuleBundle:SystemModuleTag tg3 WHERE tg3.moduleinstance=dct.id AND tg3.modulekey='badiu.tms.discipline.category'  AND LOWER(tg3.value) LIKE $gsearch ) > 0 ";
				
				$wsql=" AND ( $tagwsql1 OR $tagwsql2 OR $tagwsql3 ) ";
			}
			
			return $wsql;
         }
		 
		
		 function addfiltermyclassetag() {
			$wsql="";
			$gsearch=$this->getUtildata()->getVaueOfArray($this->getParam(),'gsearch');
			if(!empty($gsearch)){
				$gsearch=strtolower($gsearch);
				$gsearch="'%".$gsearch."%'";
				//$gswsql=" LOWER(CONCAT(cl.id,cl.name,d.name,dct.name)) LIKE $gsearch ";
				$tagwsql1=" (SELECT COUNT(tg1) FROM BadiuSystemModuleBundle:SystemModuleTag tg1 WHERE tg1.moduleinstance=cl.id AND tg1.modulekey='badiu.tms.offer.classe' AND LOWER(tg1.value) LIKE $gsearch  ) > 0 ";
				$tagwsql2=" (SELECT COUNT(tg2) FROM BadiuSystemModuleBundle:SystemModuleTag tg2 WHERE tg2.moduleinstance=d.id AND tg2.modulekey='badiu.tms.discipline.discipline'  AND LOWER(tg2.value) LIKE $gsearch ) > 0 ";
				$tagwsql3=" (SELECT COUNT(tg3) FROM BadiuSystemModuleBundle:SystemModuleTag tg3 WHERE tg3.moduleinstance=dct.id AND tg3.modulekey='badiu.tms.discipline.category'  AND LOWER(tg3.value) LIKE $gsearch ) > 0 ";
				
				$wsql=" AND ( $tagwsql1 OR $tagwsql2 OR $tagwsql3 ) ";
			}
			
			return $wsql;
         }
		 
		 function addsortorderavailableclasse() {
			 $orderby=$this->getUtildata()->getVaueOfArray($this->getParam(),'_orderby');
			 $wsql="";
			 if($orderby=='bytimestartdesc'){$wsql=" ORDER BY o.timestart DESC ";}
			 else if($orderby=='bytimestartasc'){$wsql=" ORDER BY o.timestart ";}
			 else if($orderby=='byname'){$wsql=" ORDER BY d.name ";}
			 return $wsql;
		 }
		 
		 function addsortordermyclasse() {
			 $orderby=$this->getUtildata()->getVaueOfArray($this->getParam(),'_orderby');
			 $wsql="";
			 if($orderby=='bytimestartdesc'){$wsql=" ORDER BY cl.timestart DESC ";}
			 else if($orderby=='bytimestartasc'){$wsql=" ORDER BY cl.timestart ";}
			 else if($orderby=='byname'){$wsql=" ORDER BY d.name ";}
			 return $wsql;
		 }
        function addfilterwithoutenroledclasse() {
         
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			if($badiuSession->get()->getUser()->getAnonymous()){return " ";}
		    
			$userid=$badiuSession->get()->getUser()->getId();
			if(empty($userid)){$userid=0; }
			
			
            $sql=" AND (SELECT COUNT(o1.id) FROM BadiuAmsEnrolBundle:AmsEnrolClasse o1  WHERE o1.classeid=o.id AND o1.userid=$userid ) = 0 ";
            return $sql;
         }

	function addfilterwithoutenroledoffer() {
        	$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			if($badiuSession->get()->getUser()->getAnonymous()){return " ";}
		    
			$userid=$badiuSession->get()->getUser()->getId();
			if(empty($userid)){$userid=0; }
			
			
            $sql=" AND (SELECT COUNT(ef1.id) FROM BadiuAmsEnrolBundle:AmsEnrolOffer ef1   WHERE ef1.offerid=o.id AND ef1.userid=$userid ) = 0 ";
            return $sql;
         }
		
	   function addfiltertrailclasseavailable() {
         
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			if($badiuSession->get()->getUser()->getAnonymous()){return " ";}
		    
			$userid=$badiuSession->get()->getUser()->getId();
			if(empty($userid)){$userid=0; }
			
			//classe not enroled
            $sql=" AND (SELECT COUNT(d1.id) FROM BadiuAmsEnrolBundle:AmsEnrolClasse o1 JOIN o1.classeid cl1 JOIN cl1.odisciplineid od1 JOIN od1.disciplineid d1  WHERE d1.id=d.id AND o1.userid=$userid ) = 0 ";
            
			//classe of trail
			$sql.=" AND (SELECT COUNT(o2.id) FROM BadiuAmsEnrolBundle:AmsEnrolOffer o2 JOIN  o2.offerid f2  WHERE o2.offerid=ro.offerid AND  o2.userid=$userid AND f2.shortname != 'tmsdefaultcourseoffer' AND f2.dtype='training' ) >  0";
			
			//withou course approved
			$sql.=" AND (SELECT COUNT(o3.id) FROM BadiuAmsEnrolBundle:AmsEnrolDiscipline o3 JOIN o3.statusid s3 WHERE o3.odisciplineid=od.id AND o3.userid=$userid AND s3.shortname='approvedenrol') = 0 ";
			return $sql;
         }
		 
	
	 function addfilteractiveclasse(){
		 $param=array('_classealias'=>'o','_classestatusalias'=>'s');
		 $classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		 $wsql=$classedefaultsqlfilter->activefilter($param);
		 return $wsql;
		
	 }
	 
	 function addfilterclassetag(){
		 $param=array('_classealias'=>'o','_classestatusalias'=>'s');
		 
		 // "AND LOWER(CONCAT(o.id,o.name,d.name,dct.name)) LIKE :gsearch "
		 $classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		 $wsql=$classedefaultsqlfilter->activefilter($param);
		 return $wsql;
		
	 }
	  
	
} 
