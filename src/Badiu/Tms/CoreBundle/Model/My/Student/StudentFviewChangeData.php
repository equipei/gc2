<?php

namespace Badiu\Tms\CoreBundle\Model\My\Student;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportChangeData;
class StudentFviewChangeData extends BadiuReportChangeData
{

    function __construct(Container $container) {
            parent::__construct($container);
              }
              

public function all($data){
	
	$listenforloffer=$this->addColunsToMyOfferDiscipline($data);
	$data['badiu_list_data_rows']['data'][5]=$listenforloffer;
	//print_r($data['badiu_list_data_rows']['data'][5]);exit;
	return $data;
      
  }
  
   function addColunsToMyOfferDiscipline($data){
	  //get enrol classe by discipline key
	  $listclassenrol=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows.data.3',true); 
	  $listclassenrol=$this->makeEnrolClasseKeyByDiscipine($listclassenrol);
	  //get enrol discipline by key
	 $listdisciplineenrol=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows.data.7',true); 
	 $listdisciplineenrol=$this->makeEnrolDisciplineKeyByDiscipine($listdisciplineenrol);
	
	
	 //check enrol in classe and discipline add column status and acces link to list of offer discipline
	$listofferdiscipline=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows.data.5',true); 
	 //return new list
	 $fparam=array('offerdisciplines'=>$listofferdiscipline,'enroldisciplines'=>$listdisciplineenrol,'enrolclasses'=>$listclassenrol);
	 $listenforloffer=$this->makeListOfEnrolDisciplineOffer($fparam);
	return $listenforloffer;
   }
 function makeEnrolClasseKeyByDiscipine($listenrol){
	 $newlist=array();
	 if(empty($listenrol)){return $newlist;}
	 if(!is_array($listenrol)){return $newlist;}
	 
	 foreach ($listenrol as $row){
		$disciplineid=$this->getUtildata()->getVaueOfArray($row,'disciplineid'); 
		$newlist[$disciplineid]=$row;
	 }
	 return $newlist;
 }
 
 function makeEnrolDisciplineKeyByDiscipine($listenrol,$separateoffer=false){
	 $newlist=array();
	 if(empty($listenrol)){return $newlist;}
	 if(!is_array($listenrol)){return $newlist;}
	 
	 foreach ($listenrol as $row){
		$disciplineid=$this->getUtildata()->getVaueOfArray($row,'id'); 
		$offerid=$this->getUtildata()->getVaueOfArray($row,'offerid'); 
		$ofkey="$offerid/$disciplineid";
		$newlist[$disciplineid]=$row;
		$newlist[$ofkey]=$row;
	 }
	 return $newlist;
 }
  function makeListOfEnrolDisciplineOffer($fparam){
	 $newlist=array();
	 $listdisciplineofoffer=$this->getUtildata()->getVaueOfArray($fparam,'offerdisciplines'); 
	 if(empty($listdisciplineofoffer)){return $newlist;}
	 if(!is_array($listdisciplineofoffer)){return $newlist;}
	 
	 foreach ($listdisciplineofoffer as $row){
		$disciplineid=$this->getUtildata()->getVaueOfArray($row,'id');
		$discliplineenroldto=$this->getUtildata()->getVaueOfArray($row,$disciplineid);
		$confenroldto=$this->factoryEnrolOfferDiscipline($row,$fparam);
		//print_R($confenroldto);exit; 
		$row['statusshortname']=$this->getUtildata()->getVaueOfArray($confenroldto,'statusshortname');
		$row['statusname']=$this->getUtildata()->getVaueOfArray($confenroldto,'statusname');
		array_push($newlist,$row);
		
	 }
	 return $newlist;
 } 
  function factoryEnrolOfferDiscipline($offerdiscioplinedto,$fparam){
	  $newdto=array();
	//  if(empty($discliplineenroldto) && empty($classeenroldto)){return $newdto;}
	
	$offerid=$this->getUtildata()->getVaueOfArray($offerdiscioplinedto,'offerid');
	$disciplineid=$this->getUtildata()->getVaueOfArray($offerdiscioplinedto,'id');
	$ofkey="$offerid/$disciplineid";
	
	
	//chek enrol offer
	$offershortname=$this->getUtildata()->getVaueOfArray($fparam,'enroldisciplines.'.$ofkey.'.statusshortname',true);
	if(!empty($offershortname)){
		$newdto['statusshortname']=$offershortname;
		$newdto['statusname']=$this->getUtildata()->getVaueOfArray($fparam,'enroldisciplines.'.$ofkey.'.statusname',true);
		return $newdto;
	}
	$disciplineshortname=$this->getUtildata()->getVaueOfArray($fparam,'enroldisciplines.'.$disciplineid.'.statusshortname',true);
	if(!empty($disciplineshortname)){
		$newdto['statusshortname']=$disciplineshortname;
		$newdto['statusname']=$this->getUtildata()->getVaueOfArray($fparam,'enroldisciplines.'.$disciplineid.'.statusname',true);
		return $newdto;
	}
	/*
	$classeshortname=$this->getUtildata()->getVaueOfArray($fparam,'enrolclasses.'.$disciplineid.'.statusshortname',true);
	if(!empty($classeshortname)){
		$newdto['statusshortname']=$classeshortname;
		$newdto['statusname']=$this->getUtildata()->getVaueOfArray($fparam,'enrolclasses.'.$disciplineid.'.statusname',true);
		return $newdto;
	}*/
	$newdto['statusshortname']='notneroled';
	$newdto['statusname']='Não matriculado';
	return $newdto;
  }
}
