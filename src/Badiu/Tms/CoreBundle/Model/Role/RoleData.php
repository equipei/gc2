<?php

namespace Badiu\Tms\CoreBundle\Model\Role;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Tms\CoreBundle\Model\TmsDataBase;
class RoleData  extends TmsDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    	
	 public function getFormChoice($entity,$param=array()) {
		 if(!empty($orderby)){$orderby= "ORDER BY $orderby";}
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id,o.name FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.dtype = :dtype $wsql $orderby";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('dtype','training');
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return  $result;
    }
 
}
