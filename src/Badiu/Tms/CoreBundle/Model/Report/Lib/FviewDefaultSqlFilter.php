<?php

namespace Badiu\Tms\CoreBundle\Model\Report\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;
class FviewDefaultSqlFilter extends BadiuSqlFilter{

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
  
	 
 function addenrolfilter() {
	
	$fparam=$this->getParam();
	$year=  $this->getUtildata()->getVaueOfArray($fparam,'year');
    $month=  $this->getUtildata()->getVaueOfArray($fparam,'month');
	
    $govbresferaatuacao=$this->getUtildata()->getVaueOfArray($fparam,'govbresferaatuacao');
	$govbrocupacaoprincipal=$this->getUtildata()->getVaueOfArray($fparam,'govbrocupacaoprincipal');
	$govbruf=$this->getUtildata()->getVaueOfArray($fparam,'govbruf');
	$govbrtipodevinculo=$this->getUtildata()->getVaueOfArray($fparam,'govbrtipodevinculo');
	$govbrareadeatuacao=$this->getUtildata()->getVaueOfArray($fparam,'govbrareadeatuacao');
	
	$govbrcidade=$this->getUtildata()->getVaueOfArray($fparam,'govbrcidade');
	
  
  //get timestart timeend by year / month
    $tparam=array('year'=>$year,'month'=>$month);
    $dateperiod=$this->getContainer()->get('badiu.system.core.lib.date.periodutil')->startEndDatePeriod($tparam);
	$fparam['defultdate1']=$dateperiod->date1;
	$fparam['defultdate2']=$dateperiod->date2;
	$fparam['::defultdateoperator']=' >= ';

   $fqueryn=$this->getContainer()->get('badiu.system.core.lib.sql.factoryqueryselectnative');
   $wsql="";

	$wsql.=$fqueryn->period($fparam,'defultdate','o.timestart',false);
	$wsql.=$fqueryn->equal($this->getParam(),'enrolstatusid','o.statusid',true);
	$wsql.=$fqueryn->equal($this->getParam(),'classestatusid','cl.statusid',true);
	$wsql.=$fqueryn->equal($this->getParam(),'typeid','cl.typeid',true);
	$wsql.=$fqueryn->equal($this->getParam(),'typemanagerid','cl.typemanagerid',true);
	$wsql.=$fqueryn->equal($this->getParam(),'typeaccessid','cl.typeaccessid',true);
	
	//govbresferaatuacao
	$ffparam=array('fieldshortname'=>'govbresferaatuacao','fieldvalue'=>$govbresferaatuacao,'pfx'=>'f1');
	$wsql.=$this->getAdminFormFilter($ffparam);
	
	$ffparam=array('fieldshortname'=>'govbrocupacaoprincipal','fieldvalue'=>$govbrocupacaoprincipal,'pfx'=>'f2');
	$wsql.=$this->getAdminFormFilter($ffparam);
	
	$ffparam=array('fieldshortname'=>'govbruf','fieldvalue'=>$govbruf,'pfx'=>'f3');
	$wsql.=$this->getAdminFormFilter($ffparam);
	
	$ffparam=array('fieldshortname'=>'govbrtipodevinculo','fieldvalue'=>$govbrtipodevinculo,'pfx'=>'f4');
	$wsql.=$this->getAdminFormFilter($ffparam);
	
	$ffparam=array('fieldshortname'=>'govbrareadeatuacao','fieldvalue'=>$govbrareadeatuacao,'pfx'=>'f5');
	$wsql.=$this->getAdminFormFilter($ffparam);
	
	$ffparam=array('fieldshortname'=>'govbrcidade','fieldvalue'=>$govbrcidade,'pfx'=>'f6');
	$wsql.=$this->getAdminFormFilter($ffparam);
	
	//other
	//disciplinecategoryid,disciplineid,enterpriseid,projectid,departmentid,typedevid,typepartnershipid,authordevid
	
	$wsql.=$fqueryn->equal($this->getParam(),'disciplinecategoryid','dct.id',true);
	$wsql.=$fqueryn->equal($this->getParam(),'disciplineid','d.id',true);
	$wsql.=$fqueryn->equal($this->getParam(),'enterpriseid','cl.enterpriseid',true);
	$wsql.=$fqueryn->equal($this->getParam(),'departmentid','cl.departmentid',true);
	$wsql.=$fqueryn->equal($this->getParam(),'projectid','cl.projectid',true);
	$wsql.=$fqueryn->equal($this->getParam(),'typedevid','cl.typedevid',true);
	$wsql.=$fqueryn->equal($this->getParam(),'typepartnershipid','cl.typepartnershipid',true);
	$wsql.=$fqueryn->equal($this->getParam(),'authordevid','cl.authordevid',true);
	
	
    return $wsql;
}
/*
function addclassefilter() {
	$fparam=$this->getParam();
	$year=  $this->getUtildata()->getVaueOfArray($fparam,'year');
    $month=  $this->getUtildata()->getVaueOfArray($fparam,'month');
	
	//get timestart timeend by year / month
    $tparam=array('year'=>$year,'month'=>$month);
    $dateperiod=$this->getContainer()->get('badiu.system.core.lib.date.periodutil')->startEndDatePeriod($tparam);
	$fparam['defultdate1']=$dateperiod->date1;
	$fparam['defultdate2']=$dateperiod->date2;
	$fparam['::defultdateoperator']=' >= ';


    $fqueryn=$this->getContainer()->get('badiu.system.core.lib.sql.factoryqueryselectnative');
    $wsql="";

	$wsql.=$fqueryn->period($fparam,'defultdate','cl.timestart',false);
}*/
 function getAdminFormFilter($param) {
	 $fieldshortname=$this->getUtildata()->getVaueOfArray($param,'fieldshortname');
	 $fieldvalue=$this->getUtildata()->getVaueOfArray($param,'fieldvalue');
	 $pfx=$this->getUtildata()->getVaueOfArray($param,'pfx');
	 if(empty($fieldvalue)){return ""; }
	  if(empty($fieldshortname)){return ""; }
	  //$wsql.=" AND ( SELECT COUNT(sfr.id) FROM BadiuAdminFormBundle:AdminFormResponse sfr JOIN sfr.attemptid sfa JOIN sfa.formid sff JOIN sfr.formitemid sfi JOIN sfi.fieldid sffd WHERE sfa.moduleinstance=o.userid AND   sff.shortname='dadosprofissionais' AND sffd.shortname='govbresferaatuacao'  AND sfr.answer=$govbresferaatuacao ) > 0";
	 if($fieldshortname=='govbruf'){$fieldvalue="'".$fieldvalue."'";}
	 $wsql=" AND ( SELECT COUNT(sfr$pfx.id) FROM BadiuAdminFormBundle:AdminFormResponse sfr$pfx JOIN sfr$pfx.attemptid sfa$pfx JOIN sfa$pfx.formid sff$pfx JOIN sfr$pfx.formitemid sfi$pfx JOIN sfi$pfx.fieldid sffd$pfx WHERE sfa$pfx.moduleinstance=o.userid AND   sff$pfx.shortname='dadosprofissionais' AND sffd$pfx.shortname='".$fieldshortname."'  AND sfr$pfx.answer=$fieldvalue ) > 0";
	 return $wsql;
 }
}
