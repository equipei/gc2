<?php

namespace Badiu\Tms\CoreBundle\Model\Report\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FviewDefaultLib extends BadiuModelLib {

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
  
	public function searchAll() {
		 $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		 $param['entity']=$this->getEntity();
		 $modulekey= $this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $this->getSearch()->getKeymanger()->setBaseKey($modulekey);
		
		 
		 $resultrow=$this->getSearch()->searchListSingle($param);	
		 //print_r($resultrow);exit;
		 $resultdlist=$this->castRow($resultrow);
		 
		 $resultrows=$this->getSearch()->searchList($param);
		
		 $resultdlist=$this->castRows($resultdlist,$resultrows);
		 
		
		 $resultrowsapexchar=$this->castRowsApexchartdata($resultrows);  	
		 
		 $result=array('dlist'=>$resultdlist,'apexchartdata'=>$resultrowsapexchar);
		// print_r($resultrows);exit;
		// $result=array();
		 //$result=$this->getSearch()->searchList($param);
		//print_r( $result);exit;
		 return $result; 
		  
	}
	
	public function castRow($list) {
		$listdata = $this->getUtildata()->getVaueOfArray($list,'data');
		//$listlabel = $this->getUtildata()->getVaueOfArray($list,'label');
		//$listtitle = $this->getUtildata()->getVaueOfArray($list,'title');
		
		 $newlist=array();
		 foreach ( $listdata as $k=> $row) {
			 $datakey="rowdata$k";
			 $labelkey="rowlabel$k";
			 $titlekey="rowtitle$k";
			 $newlist[$datakey]=$row;
			 $newlist[$labelkey]=$this->getUtildata()->getVaueOfArray($list,"label.$k",true);
			 $newlist[$titlekey]=$this->getUtildata()->getVaueOfArray($list,"title.$k",true);
		 }
		 return $newlist;
	}
	public function castRows($resultdlist,$list) {
		
		$listdata = $this->getUtildata()->getVaueOfArray($list,'data');
		//$listlabel = $this->getUtildata()->getVaueOfArray($list,'label');
		//$listtitle = $this->getUtildata()->getVaueOfArray($list,'title');
		//$listdtype = $this->getUtildata()->getVaueOfArray($list,'dtype');
		
		 
		 foreach ($listdata as $k=> $row) {
			 $datakey="rowsdata$k";
			 $labelkey="rowslabel$k";
			 $titlekey="rowstitle$k";
			 $dtypekey="rowsdtype$k";
			 $resultdlist[$datakey]=$row;
			 $resultdlist[$labelkey]=$this->getUtildata()->getVaueOfArray($list,"label.$k",true);
			 $resultdlist[$titlekey]=$this->getUtildata()->getVaueOfArray($list,"title.$k",true);
			 $resultdlist[$dtypekey]=$this->getUtildata()->getVaueOfArray($list,"dtype.$k",true);
		 }
		 return $resultdlist;
	}
	public function castRowsApexchartdata($list) {
		$apexchartfactorydata=$this->getContainer()->get('badiu.theme.core.lib.template.vuejs.apexchartfactorydata');
		$acdrows=$apexchartfactorydata->makeSourceDataRows($list);
		return $acdrows;
	}

}
