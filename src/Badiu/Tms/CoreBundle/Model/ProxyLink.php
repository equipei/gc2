<?php

namespace Badiu\Tms\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ProxyLink extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
			  
	function exec() {return null;}
	
	private function getIdActiveVersionDisciplineOffer() {
		$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
			$disciplineid=$this->getUtildata()->getVaueOfArray($param,'disciplineid');
			$entity=$this->getEntity();
			$offerid=$this->getContainer()->get('badiu.ams.offer.offer.data')->getIdByShortname($this->getEntity(),'tmsdefaultcourseoffer'); 
			
			$odisciplinedata=$this->getContainer()->get('badiu.tms.offer.discipline.data');
			
			
			
			//get offer discipline status offerongoing
			$statusid=$this->getContainer()->get('badiu.tms.offer.disciplinestatus.data')->getIdByShortname($entity,'offerongoing');
		
			//get max version 
			$fparam=array('statusid'=>$statusid,'disciplineid'=>$disciplineid,'offerid'=>$offerid,'entity'=>$entity);
			$fversion=$odisciplinedata->getMaxGlobalColumnValue('fversion',$fparam);
			$fparam=array('statusid'=>$statusid,'disciplineid'=>$disciplineid,'offerid'=>$offerid,'fversion'=>$fversion,'entity'=>$entity);
			$countod=$odisciplinedata->countGlobalRow($fparam);
			$odisciplineid=null;
			if($countod==1){
				$odisciplineid=$odisciplinedata->getGlobalColumnValue('id',$fparam);
			}
			return $odisciplineid;
	}
	function gotoActiveVersionDisciplineOffer() {
			$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
			$disciplineid=$this->getUtildata()->getVaueOfArray($param,'disciplineid');
		
			$odisciplineid=$this->getIdActiveVersionDisciplineOffer();
			
			if($odisciplineid){
				$parammage=array('parentid'=>$odisciplineid);
				$url=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.classe.index',$parammage);
				header('Location: '.$url);
				 exit;
			}
			$parad=array('parentid'=>$disciplineid);
			$url=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.vdiscipline.index',$parad);
			header('Location: '.$url);
			exit; 
			return null;
		}	
	function gotoActiveVersionDisciplineOfferAddNewClase() {
			$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
			$disciplineid=$this->getUtildata()->getVaueOfArray($param,'disciplineid');
		
			$odisciplineid=$this->getIdActiveVersionDisciplineOffer();
			
			if($odisciplineid){
				$parammage=array('parentid'=>$odisciplineid);
				$url=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.classe.add',$parammage);
				header('Location: '.$url);
				 exit;
			}
			$parad=array('parentid'=>$disciplineid);
			$url=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.vdiscipline.index',$parad);
			header('Location: '.$url);
			exit; 
			return null;
		}	
}			  