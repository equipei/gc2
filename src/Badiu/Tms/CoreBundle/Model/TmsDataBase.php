<?php

namespace Badiu\Tms\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class TmsDataBase  extends BadiuDataBase {
    private $dtype ='training';
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
     
   public function getFormChoice($entity,$param=array(),$orderby="") {
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id,o.name,o.shortname FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.dtype = :dtype $wsql $orderby";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('dtype','training');
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return  $result;
    }
 

 public function findByEntity($entity,$orderby="",$deleted=false) {
             $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity  AND o.dtype = :dtype AND o.deleted=:deleted $orderby";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
             $query->setParameter('dtype','training');
             $query->setParameter('deleted',$deleted);
            $result= $query->getResult();
            return  $result;
        }

		
 public function existAdd() {
          $r=FALSE;
           $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity   AND o.dtype = :dtype AND UPPER(o.name) = :name ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('name',strtoupper($this->getDto()->getName()) );
			 $query->setParameter('dtype','training');
            $result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
            
            
       }
       public function existEdit() {
           $r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.id != :id AND o.entity = :entity   AND o.dtype = :dtype AND UPPER(o.name) = :name ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$this->getDto()->getId());
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('name',strtoupper($this->getDto()->getName()) );
			 $query->setParameter('dtype','training');
            $result= $query->getSingleResult();
            if($result['countrecord']>0){$r=TRUE;}
             return $r;
          
       }	
	   
	function getDtype() {
        return $this->dtype;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }
}
