<?php

namespace Badiu\Tms\CoreBundle\Model\Discipline\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class CheckDisciplineDependency extends BadiuModelLib {

    function __construct(Container $container) {
                parent::__construct($container);
           
       } 

	public function exec($param) {
		  $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 $entity=$this->getEntity();
		 $hasmodulekey=false;
		 if($modulekey=='badiu.ams.offer.classe'){ $hasmodulekey=true;}
		 else if($modulekey=='badiu.tms.offer.classe'){ $hasmodulekey=true;}
		 if(!$hasmodulekey){return null;}
		
		 if(empty($moduleinstance)){return null;}
		 if(empty($userid)){return null;}
		 //get lista discipline
		 $dependencydata=$this->getContainer()->get('badiu.tms.discipline.dependency.data');
		 $rows=$dependencydata->getTrial($entity,$moduleinstance,$userid);
		
		 //check approved
		 if(empty($rows)){return null;}
		 if(!is_array($rows)){return null;}
		 $count=0;
		
		 $countapproved=0;
		  foreach ($rows as $row) {
			$countclasseapprovedenrol=  $this->getUtildata()->getVaueOfArray($row,'countclasseapprovedenrol');
			if($countclasseapprovedenrol){$countapproved++;}
			$count++;
		  }
		
		  $message=null;
		  if($countapproved==$count){return null;}
		  else if($countapproved < $count){
			  $disciplineid=$this->getContainer()->get('badiu.ams.discipline.discipline.data')->getIdByClasseid($moduleinstance);
			   $link=$this->getUtilapp()->getUrlByRoute('badiu.tms.discipline.dependency.index',array('parentid'=>$disciplineid));
			   if($countapproved==0){ $message=$this->getTranslator()->trans('badiu.tms.discipline.dependency.message.withoutrequiredcourse',array('%countrequiredcourse%'=>$count,'%linkcourserequired%'=>$link));}
			 else if($countapproved > 0){ $message=$this->getTranslator()->trans('badiu.tms.discipline.dependency.message.withpartialrequiredcourse',array('%countrequiredcourse%'=>$count,'%countrequiredcoursecompleted%'=>$countapproved,'%linkcourserequired%'=>$link));}
			 return $message;
		  }
		 //return info
		 
		return null; 
		  
	}

}
