<?php
namespace Badiu\Tms\CoreBundle\Model\Discipline\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\DisciplineBundle\Model\Lib\SummarizeData as AmsSummarizeData;
class SummarizeData  extends AmsSummarizeData{

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
	function teste(){
		$fparam=array('_period'=>'allyears','hasclasseactive'=>1);
		$fparam=array('_period'=>'currentyear','hasclasseactive'=>1);
		$result=$this->countByYear($fparam);
		
		echo "<pre>"; 
		print_r($result);
		echo "</pre>";exit;
	}
	
	function countByYearSearch($param,$dresult){
		$operation=$this->getUtildata()->getVaueOfArray($param,'operation');
		$offerclassetypeshortname=$this->getUtildata()->getVaueOfArray($param,'offerclassetypeshortname');
		$ddate1=$this->getUtildata()->getVaueOfArray($param,'_date1');
		$ddate2=$this->getUtildata()->getVaueOfArray($param,'_date2');
		$year=$this->getUtildata()->getVaueOfArray($param,'_year');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		
		$hasclasseactive=$this->getUtildata()->getVaueOfArray($param,'hasclasseactive');
		
		if(empty($ntity)){$entity=$this->getEntity();}
		if(empty($operation)){$operation="countdiscipline";}
		
		$wsql="";
		if($hasclasseactive){
			 $fparam=array('_classealias'=>'o','_classestatusalias'=>'cls');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->activefilter($fparam);
		}
		if(!empty($offerclassetypeshortname)){$wsql.=" AND clt.shortname=:offerclassetypeshortname ";}
		
		$sql="SELECT COUNT(DISTINCT d.id) AS countrecord FROM BadiuAmsOfferBundle:AmsOfferClasse o JOIN o.statusid cls JOIN o.typeid clt JOIN o.odisciplineid od JOIN od.disciplineid d JOIN d.categoryid dct JOIN od.offerid f WHERE  o.entity=:entity AND f.shortname=:offershortname AND d.dtype='training' AND d.timecreated IS NOT NULL  AND d.dtype=:dtype AND d.deleted=:deleted AND d.timecreated >=:ddate1 AND d.timecreated <=:ddate2  $wsql ";
		
		$data=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
		 
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
		$query->setParameter('deleted', 0);
		$query->setParameter('offershortname', 'tmsdefaultcourseoffer');
		$query->setParameter('dtype', 'training');
		$query->setParameter('ddate1',$ddate1);
		$query->setParameter('ddate2',$ddate2);
		
		if($hasclasseactive){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		}
		if(!empty($offerclassetypeshortname)){$query->setParameter('offerclassetypeshortname', $offerclassetypeshortname);}
		$result= $query->getOneOrNullResult();
		
		$countrecord=$this->getUtildata()->getVaueOfArray($result,'countrecord');
		$dresult[$year]=$countrecord;
		return $dresult;
	}
	 
	 function getMinMaxYear($param){
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		$hasclasseactive=$this->getUtildata()->getVaueOfArray($param,'hasclasseactive');
		$operation=$this->getUtildata()->getVaueOfArray($param,'operation');
		$offerclassetypeshortname=$this->getUtildata()->getVaueOfArray($param,'offerclassetypeshortname');
		
		if(empty($operation)){$operation="countdiscipline";}
		
		if(empty($ntity)){$entity=$this->getEntity();}
		$timenullcontrol = new \DateTime();
		$timenullcontrol->setDate(1970, 1, 1);
		$wsql="";
		if($hasclasseactive){
			 $fparam=array('_classealias'=>'o','_classestatusalias'=>'cls');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->activefilter($fparam);
		}
		if(!empty($offerclassetypeshortname)){$wsql.=" AND clt.shortname=:offerclassetypeshortname ";}
		$sql="SELECT MIN(d.timecreated) AS firstdate, MAX(d.timecreated) AS lastdate  FROM  BadiuAmsOfferBundle:AmsOfferClasse o JOIN o.statusid cls JOIN o.typeid clt  JOIN o.odisciplineid od JOIN od.disciplineid d JOIN d.categoryid dct JOIN od.offerid f WHERE  o.entity=:entity AND f.shortname=:offershortname AND d.dtype='training' AND d.timecreated > :timenullcontrol  AND d.dtype=:dtype AND d.deleted=:deleted $wsql ";
		
		$data=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
		 
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
		$query->setParameter('deleted', 0);
		$query->setParameter('offershortname', 'tmsdefaultcourseoffer');
		$query->setParameter('dtype', 'training');
		$query->setParameter('timenullcontrol', $timenullcontrol);
		
		if($hasclasseactive){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		}
		if(!empty($offerclassetypeshortname)){$query->setParameter('offerclassetypeshortname', $offerclassetypeshortname);}
		$result= $query->getOneOrNullResult();
		
		return $result;
	}
}
