<?php
namespace Badiu\Tms\CoreBundle\Model\Discipline;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class DisciplineCreateOfferFilter extends BadiuFormFilter{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
   public function execBeforeSubmit() {
    
        }
        
   public function execAfterSubmit() {
	  
		 $entity=$this->getEntity();
		 $disciplineid = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
		 if(empty($disciplineid)){return null;}
		 
		 $dconfig= $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
		if(empty($dconfig)){$dconfig=array();}
		else {$dconfig=$this->getJson()->decode($dconfig,true);}
		
		 $offerid = $this->getContainer()->get('badiu.ams.offer.offer.data')->getIdByShortname($entity,'tmsdefaultcourseoffer');
		 //if(empty($offerid)){return null;}

		
		 $odisciplineid=null;
		 $disciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
		 $exist=$disciplinedata->existDiscipline($entity, $offerid, $disciplineid);
		 if($exist) {
			
			 $odisciplineid=$disciplinedata->getLastDisciplineid($entity, $offerid, $disciplineid);
			 $badiuSession = $this->getContainer()->get('badiu.system.access.session');
			 $badiuSession->setHashkey($this->getSessionhashkey());
			 $updatelastversionbychangediscipline=$badiuSession->getValue('badiu.tms.offer.discipline.updatelastversionbychangediscipline.param.config.default');
			  
			 if($updatelastversionbychangediscipline){$this->forceUpdateLastversionDiscipline($odisciplineid);}	
		}
		 else{

			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			
			$classestatus=$badiuSession->getValue('badiu.tms.offer.classestatus.param.config.default');
			$classestatusid=$this->getContainer()->get('badiu.tms.offer.classestatus.data')->getIdByShortname($this->getEntity(),$classestatus); 

			$typeid=$this->getUtildata()->getVaueOfArray($this->getParam(), 'typeid');
			if(empty($typeid)){
				$type=$badiuSession->getValue('badiu.tms.offer.type.param.config.default');
				$typeid=$this->getContainer()->get('badiu.tms.offer.type.data')->getIdByShortname($this->getEntity(),$type); 
			}
			
			$typemanagerid=$this->getUtildata()->getVaueOfArray($this->getParam(), 'typemanagerid');
			if(empty($typemanagerid)){
				$typemanager=$badiuSession->getValue('badiu.tms.offer.typemanager.param.config.default');
				$typemanagerid=$this->getContainer()->get('badiu.tms.offer.typemanager.data')->getIdByShortname($this->getEntity(),$typemanager); 
			}
			
			$typeaccess=$badiuSession->getValue('badiu.tms.offer.typeaccess.param.config.default');
			$typeaccessid=$this->getContainer()->get('badiu.tms.offer.typeaccess.data')->getIdByShortname($this->getEntity(),$typeaccess); 

			
			
			
			$param=array();
			$param['disciplineid']=$disciplineid;
			$param['offerid']=$offerid;
			$param['entity']=$entity;
			$param['disciplinename']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'name');
			$param['drequired']=FALSE;
			$param['statusid']=$this->getContainer()->get('badiu.tms.offer.disciplinestatus.data')->getIdByShortname($entity,'offerongoing');
			$param['typeid']=$typeid;
			$param['typemanagerid']=$typemanagerid;
			$param['typeaccessid']=$typeaccessid;
			
			//enrolconfig
			$enroldatetype=$badiuSession->getValue('badiu.tms.offer.discipline.enroldatetype.param.config.default');
			$enroldatedynamic=$badiuSession->getValue('badiu.tms.offer.discipline.enroldatedynamic.param.config.default');
			$enrolreplicate=$badiuSession->getValue('badiu.tms.offer.discipline.enrolreplicate.param.config.default');
			$enrolreplicateupadate=$badiuSession->getValue('badiu.tms.offer.discipline.enrolreplicateupadate.param.config.default');
			if(empty($enroldatetype)){$enroldatetype="dynamic";}
			if(empty($enroldatedynamic)){$enroldatedynamic='{"operator":"","number":"120","unittime":"system_time_unit_day"}';}
			if(empty($enrolreplicate)){$enrolreplicate="discipline,lmsmoodle";}
			if(empty($enrolreplicateupadate)){$enrolreplicateupadate="discipline,lmsmoodle";}
			$param['enroldatetype']=$enroldatetype;
			$param['enroldatedynamic']=$enroldatedynamic;
			$param['enrolreplicate']=$enrolreplicate;
			$param['enrolreplicateupadate']=$enrolreplicateupadate;
	
			//processenrol
			$enroltype=$badiuSession->getValue('badiu.tms.offer.enrol.type.param.config.default');
			$enrolrequesttimestart=$badiuSession->getValue('badiu.tms.offer.discipline.enroldatetype.param.config.default');
			$enrolrequesttimeend=$badiuSession->getValue('badiu.tms.offer.enrol.enrolrequesttimeend.param.config.default');
			$enrolanalysiscriteria=$badiuSession->getValue('badiu.tms.offer.enrol.analysiscriteria.param.config.default');
			$maxenrol=$badiuSession->getValue('badiu.tms.offer.enrol.max.param.config.default');
			$defaultrole=$badiuSession->getValue('badiu.tms.offer.enrol.role.param.config.default');
			$defaultstatus=$badiuSession->getValue('badiu.tms.offer.enrol.status.param.config.default');
		
			if(empty($enroltype)){$enroltype="select";}
			if(empty($enrolanalysiscriteria)){$enrolanalysiscriteria="automaticregistration";}
			if(empty($defaultrole)){$defaultrole="studentcorporate";}
			if(empty($defaultstatus)){$defaultstatus="activeenrol";}
		
			$enrolselection=array();
			$enrolselection['enroltype']=$enroltype;
			$enrolselection['analysiscriteria']=$enrolanalysiscriteria;
			$enrolselection['defaultrole']=$defaultrole;
			$enrolselection['defaultstatus']=$defaultstatus; 
			

			$param['enroltype']=$enroltype;
			
			
			//rolecompletation
			$roleconclusionenable=$badiuSession->getValue('badiu.tms.offer.enrol.roleconclusionenable.param.config.default');
			$roleconclusiondhour=$badiuSession->getValue('badiu.tms.offer.enrol.roleconclusiondhour.param.config.default');
			$roleconclusiongrade=$badiuSession->getValue('badiu.tms.offer.enrol.roleconclusiongrade.param.config.default');
			$lmscoursecompletation=$badiuSession->getValue('badiu.tms.offer.enrol.lmscoursecompletation.param.config.default');
			$roleconclusiontypeoperation=$badiuSession->getValue('badiu.tms.offer.enrol.roleconclusiontypeoperation.param.config.default');


			$param['roleconclusionenable']=$roleconclusionenable;
			$param['roleconclusiondhour']=$roleconclusiondhour;
			$param['roleconclusiongrade']=$roleconclusiongrade;
			$param['lmscoursecompletation']=$lmscoursecompletation;
			$param['roleconclusiontypeoperation']=$roleconclusiontypeoperation;
			$param=$this->getContainer()->get('badiu.ams.core.lib.roleconclusionform')->disciplineFormChangeParam($param);
			$dconfig=$this->getUtildata()->getVaueOfArray($param, 'dconfig');
			if(!empty($dconfig) && !is_array($dconfig)){$dconfig=$this->getJson()->decode($dconfig,true);}
			if(!is_array($dconfig)){$dconfig=array();}
			$dconfig['enrolselection']=$enrolselection;
			$param['dconfig']=$this->getJson()->encode($dconfig);

			$param['fversion']=1;
			$param['equivalencereqminfversion']=1;
	
			
			//teachingplan
			$param['dhour']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'dhour');
			$param['credit']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'credit');
			$param['summary']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'summary');
			$param['teachingplan']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'teachingplan');
			$param['objective']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'objective');
			$param['usertarget']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'usertarget');
			$param['competence']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'competence');
			$param['usertargetestimatedamout']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'usertargetestimatedamout');
			$param['costdev']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'costdev');
			$param['defaultimage']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'defaultimage');
			
		
			//dev authordevid
			$param['projectid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'projectid');
			$param['enterpriseid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'enterpriseid');
			$param['departmentid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'departmentid');
			$param['typedevid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'typedevid');
			$param['typepartnershipid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'typepartnershipid');
			$param['authordevid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'authordevid');
			$param['devinfo']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'devinfo');
			
			
			 
			//certificate
			$certificateupdateuser=$badiuSession->getValue('badiu.ams.classe.enrol.certificateupdateuser.param.config.default');
			$certificateupdateenrol=$badiuSession->getValue('badiu.ams.classe.enrol.certificateupdateenrol.param.config.default');$certificateupdateuser=$badiuSession->getValue('badiu.ams.classe.enrol.certificateupdateuser.param.config.default');
			$certificateupdateobjectinfo=$badiuSession->getValue('badiu.ams.classe.enrol.certificateupdateobjectinfo.param.config.default');
			$certificateupdateobjectdata=$badiuSession->getValue('badiu.ams.classe.enrol.certificateupdateobjectdata.param.config.default');
			
			$param['certificateupdateuser']=$certificateupdateuser;
			$param['certificateupdateenrol']=$certificateupdateenrol;
			$param['certificateupdateobjectinfo']=$certificateupdateobjectinfo;
			$param['certificateupdateobjectdata']=$certificateupdateobjectdata;
			
			$param['maxgrade']=null;
			$param['conclusiongrade']=null;
			$param['timecreated']=new \DateTime();
			
			//certificate
			$param['enablecertificate']=1;
			$param['certificatetempleid']=$this->getContainer()->get('badiu.admin.certificate.template.data')->getIdByShortname($entity,'default');;
			
			
			
			
			$odisciplinecustomkey=$badiuSession->getValue('badiu.tms.offer.discipline.custom');
			if(!empty($odisciplinecustomkey)){
				$param['_param']=$this->getParam();
				$param=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($odisciplinecustomkey,$param);
				
			}
			$param['deleted']=0;
			
			$odisciplineid=$disciplinedata->insertNativeSql($param,false);
		 }
		 
		
		 $dconfig['offer']['id']=$offerid;
		 $dconfig['offer']['discipline']['lastid']=$odisciplineid;
		 //$dconfig['enrolselection']=$enrolselection;
		 $dconfig=$this->getJson()->encode($dconfig);

		
		 $fparam=$this->getParam();
		 $disciplinedata=$this->getContainer()->get('badiu.tms.discipline.discipline.data');
		 $aparam=array('id'=>$disciplineid,'dconfig'=>$dconfig);
		 $disciplinedata->updateNativeSql($aparam,false);
		 $fparam['dconfig']=$dconfig;

			 //lms sync
		  $this->getLmsMoodleConfig();
       }
    

	   public function getLmsMoodleConfig() {
		
		$categoryid = $this->getUtildata()->getVaueOfArray($this->getParam(), 'categoryid');
		if(empty($categoryid)){return null;}
	   
		$disciplineid = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
		if(empty($disciplineid)){return null;}
	   
		//check sync exist
		$odisciplineid=null;
		$disciplinedata=$this->getContainer()->get('badiu.tms.discipline.discipline.data');
		$fparam=array('id'=>$disciplineid);
		$dconfig=$disciplinedata->getGlobalColumnValue('dconfig',$fparam);
		if(!empty($dconfig)){
			$dconfig = $this->getJson()->decode($dconfig,true);
			$odisciplineid=$this->getUtildata()->getVaueOfArray($dconfig, 'offer.discipline.lastid',true);
		}
		
		if(empty($odisciplineid)){return null;}
		$odisciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
		$fparam=array('id'=>$odisciplineid);
		$oddconfig=$odisciplinedata->getGlobalColumnValue('dconfig',$fparam);
		if(!empty($oddconfig)){
			$oddconfig = $this->getJson()->decode($oddconfig,true);
			$lmsintegrationstatus=$this->getUtildata()->getVaueOfArray($oddconfig, 'lmsintegration.status',true);
			if($lmsintegrationstatus=='active'){return null;}
		}
		
		//exec sync

		//get category sync info
		$disciplinecategorydata=$this->getContainer()->get('badiu.tms.discipline.category.data');
		$fparam=array('id'=>$categoryid);
		$dconfig=$disciplinecategorydata->getGlobalColumnValue('dconfig',$fparam);
		if(empty($dconfig)){return null;}
	   
		
	   $dconfig = $this->getJson()->decode($dconfig,true);
	 
	   $lmsintegrationstatus=$this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.status',true);
	   if($lmsintegrationstatus!='active'){return null;}
	
	   //add config on dconfig of discipline offer
	   $lmsintegration['status']='pending';
	   $lmsintegration['sserviceid']=$this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.sserviceid',true);
	   $lmsintegration['lmssynctype']='replicationdatainlms';
	   $lmsintegration['lmssynclevel']='coursecat';
	   $lmsintegration['lmscoursecatparent']=$this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscoursecatid',true);
	 
		
		$oddconfig['lmsintegration']=$lmsintegration;
		$oddconfig = $this->getJson()->encode($oddconfig);
		$upodparam=array('id'=>$odisciplineid,'dconfig'=>$oddconfig);
		
		$odisciplinedata->updateNativeSql($upodparam,false);

		//exec sync to lms moodle
		$fparam=array('id'=>$odisciplineid);
		$oparam=$odisciplinedata->getGlobalColumnsValue('o.id,o.disciplinename,o.shortname,o.idnumber,o.dconfig',$fparam);
		$lmsmoodleintegrationfilter=$this->getContainer()->get('badiu.ams.offer.discipline.lmsmoodleintegrationfilter');
		$lmsmoodleintegrationfilter->setParam($oparam);
		$lmsmoodleintegrationfilter->execAfterSubmit(); 
	 
	}
	
	  public function forceUpdateLastversionDiscipline($odisciplineid){
		  if(empty($odisciplineid)){return null;}
		  $fparam=array('id'=>$odisciplineid);
		  $odisciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
		  $oparam=$odisciplinedata->getGlobalColumnValue('dconfig',$fparam);
		
		 $param=array();
		 $param['id']=$odisciplineid;
		 $param['typeid']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'typeid');
		 $param['typemanagerid']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'typemanagerid');
		 
		 //teachingplan
			$param['dhour']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'dhour');
			$param['credit']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'credit');
			$param['summary']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'summary');
			$param['teachingplan']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'teachingplan');
			$param['objective']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'objective');
			$param['usertarget']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'usertarget');
			$param['competence']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'competence');
			$param['usertargetestimatedamout']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'usertargetestimatedamout');
			$param['costdev']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'costdev');
			$param['defaultimage']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'defaultimage');
			
		
			//dev authordevid
			$param['projectid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'projectid');
			$param['enterpriseid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'enterpriseid');
			$param['departmentid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'departmentid');
			$param['typedevid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'typedevid');
			$param['typepartnershipid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'typepartnershipid');
			$param['authordevid']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'authordevid');
			$param['devinfo']= $this->getUtildata()->getVaueOfArray($this->getParam(), 'devinfo');
			
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		$odisciplinecustomkey=$badiuSession->getValue('badiu.tms.offer.discipline.custom');
		
		if(!empty($odisciplinecustomkey)){
				$param['dconfig']=$oparam;
				$param['_param']=$this->getParam();
				$param=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($odisciplinecustomkey,$param);
		}
		
		$odisciplinedata->updateNativeSql($param,false);
	 }
		
}
