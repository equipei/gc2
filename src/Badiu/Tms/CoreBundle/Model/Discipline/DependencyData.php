<?php
 
namespace Badiu\Tms\CoreBundle\Model\Discipline;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class DependencyData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
 
   public function getDisciplineRequired($entity,$disciplineid) {
          $sql="SELECT  r.id,rd.name,rd.shortname  FROM ".$this->getBundleEntity()." o JOIN o.requredisciplineid rd WHERE  o.entity = :entity AND o.disciplineid = :disciplineid AND o.deleted=:deleted";
		 $query = $this->getEm()->createQuery($sql);
		 $query->setParameter('entity',$entity);
		 $query->setParameter('disciplineid',$disciplineid);
			$query->setParameter('deleted',0);
            $result= $query->getResult();
        return  $result; 
        
    }
	
	
	  public function getTrial($entity,$classeid,$userid) {
		 
		  if(empty($userid)){return null;}
		  if(empty($classeid)){return null;}
		   if(empty($entity)){return null;}
		 
          $sql="SELECT DISTINCT rd.id,rd.name,rd.shortname,(SELECT COUNT(o1.id) FROM BadiuAmsEnrolBundle:AmsEnrolClasse o1 JOIN o1.classeid cl1 JOIN cl1.odisciplineid od1 JOIN o1.statusid s1 WHERE od1.disciplineid=rd.id AND s1.shortname='approvedenrol' AND o1.userid=$userid) AS countclasseapprovedenrol   FROM ".$this->getBundleEntity()." o JOIN o.requredisciplineid rd JOIN BadiuAmsOfferBundle:AmsOfferDiscipline od WITH od.disciplineid=o.disciplineid  JOIN BadiuAmsOfferBundle:AmsOfferClasse cl WITH cl.odisciplineid=od.id WHERE  o.entity = :entity AND cl.id = :classeid AND o.deleted=:deleted";
		 
		  //$sql="SELECT  rd.id,rd.name,rd.shortname,(SELECT s1.name FROM BadiuAmsEnrolBundle:AmsEnrolDiscipline o1 JOIN o1.odisciplineid od1 JOIN o1.statusid s1 WHERE od1.disid=rd.id AND o1.userid=$userid) AS status   FROM ".$this->getBundleEntity()." o JOIN o.requredisciplineid rd WHERE  o.entity = :entity AND o.disciplineid = :disciplineid AND o.deleted=:deleted";
		  $query = $this->getEm()->createQuery($sql);
		  $query->setParameter('entity',$entity);
		  $query->setParameter('classeid',$classeid);
			$query->setParameter('deleted',0);
            $result= $query->getResult();
        return  $result; 
        
    }
	
	
}
