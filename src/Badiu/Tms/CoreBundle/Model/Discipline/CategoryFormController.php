<?php

namespace Badiu\Tms\CoreBundle\Model\Discipline;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\CoreBundle\Model\DOMAINTABLE;

class CategoryFormController extends BadiuFormController {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function changeParamOnOpen() {
			$this->getFieldProcessOnOpen();
            $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
            $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $param = $this->getParam();
            $lmsintegration = $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');

            $param['sserviceid'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'sserviceid');
            $param['lmssynctype'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynctype');
            $param['lmssynclevel'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynclevel');
            $param['lmscoursecat'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatid');
           
            if(!$this->isEdit()){$param=$this->getContainer()->get('badiu.tms.core.lib.lmsmoodlesyncdefaultform')->disciplineCategoryLevelFormChangeParamOnOpen($param);}
			else{$param=$this->getContainer()->get('badiu.tms.core.lib.lmsmoodlesyncdefaultform')->defaultChangeParamOnEdit($param);}
            
         /*   echo "<pre>" ;
            print_R($param);
            echo "</pre>" ;exit;*/
		    $this->setParam($param); 
       
    }

    public function changeParam() {
       
       $param = $this->getParam();
        $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
        if (empty($dconfig) || !is_array($dconfig)) {
            $dconfig = array();
        }
		/* if($this->isEdit()){
			 $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
			 $entity=$this->getEntity();
			 $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
			 $dconfig=$odata->getColumnValue($entity,'dconfig',$paramf=array('id'=>$id));
			 if(!empty($dconfig)){ $dconfig = $this->getJson()->decode($dconfig, true);;}
			
		 }*/
		$param['dconfig'] = $dconfig;
		
		
		$dconfig = $this->getUtildata()->getVaueOfArray($param, 'dconfig');
        $dconfig = $this->getJson()->encode($dconfig);
        $param['dconfig'] = $dconfig;
        $this->setParam($param);

       
    }

    

}
