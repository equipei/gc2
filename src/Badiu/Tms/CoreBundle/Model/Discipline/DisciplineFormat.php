<?php

namespace Badiu\Tms\CoreBundle\Model\Discipline;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\DisciplineBundle\Model\DisciplineFormat as AmsDisciplineFormat;
class DisciplineFormat extends AmsDisciplineFormat{
     private $permission;
     function __construct(Container $container) {
            parent::__construct($container);
			$this->permission=$this->getContainer()->get('badiu.system.access.permission');
       } 

         
     
       public  function name($data){
        $name=$this->getUtildata()->getVaueOfArray($data,'name');
        $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
        $dconfig=$this->getJson()->decode($dconfig, true);
        $id=$this->getUtildata()->getVaueOfArray($dconfig,'offer.discipline.lastid',true);
        if(empty($id)){return $name;}
       
        $parammage=array('parentid'=>$id);
        $manageclasseurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.classe.index',$parammage);
     
        $manageclasselink ="<a href=\"$manageclasseurl\">$name</a>";
      
    return $manageclasselink; 
} 

 /*public  function classelinkemanager($data){
            $value="";
            $disciplineid=$this->getUtildata()->getVaueOfArray($data,'id');
            if(empty($disciplineid)){return null;}
			$labelmanageclasselink=$this->getTranslator()->trans('badiu.tms.discipline.discipline.manageclasselink');
            $labeladdclasselink=$this->getTranslator()->trans('badiu.tms.discipline.discipline.addclasselink');
           
		   //badiu.tms.core.proxy.link|id AS disciplineid,_function/gotoActiveVersionDisciplineOffer
            $parammage=array('disciplineid'=>$disciplineid,'_function'=>'gotoActiveVersionDisciplineOffer');
            $manageclasseurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.core.proxy.link',$parammage);
			$parammage=array('disciplineid'=>$disciplineid,'_function'=>'gotoActiveVersionDisciplineOfferAddNewClase');
            $addclasseurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.core.proxy.link',$parammage);
            $manageclasselink ="<a href=\"$manageclasseurl\">$labelmanageclasselink</a>";
            $addclasselink ="<a href=\"$addclasseurl\">$labeladdclasselink</a>";

            $value="$manageclasselink <br />$addclasselink";
           // $manageclasselink  badiu.tms.offer.classe.index dconfig.offer.discipline.lastid AS parentid  
        return $value; 
    } */
    public  function classelinkemanager($data){
            $value="";
            $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
            $dconfig=$this->getJson()->decode($dconfig, true);
            $id=$this->getUtildata()->getVaueOfArray($dconfig,'offer.discipline.lastid',true);
            if(empty($id)){return null;}
			$labelmanageclasselink=$this->getTranslator()->trans('badiu.tms.discipline.discipline.manageclasselink');
            $labeladdclasselink=$this->getTranslator()->trans('badiu.tms.discipline.discipline.addclasselink');
			$labelmanagedisciplinedependencylink=$this->getTranslator()->trans('badiu.tms.discipline.dependency.link');
           
            $parammage=array('parentid'=>$id);
			
			$permimanageclasseurl=$this->permission->has_access('badiu.tms.offer.classe.index',$this->getSessionhashkey());
			$manageclasseurl="";
			if($permimanageclasseurl){
				 $manageclasseurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.classe.index',$parammage);
			}
		  
		    $permiaddclasseurl=$this->permission->has_access('badiu.tms.offer.classe.add',$this->getSessionhashkey());
            $addclasseurl=null;
			if($permiaddclasseurl){
				$addclasseurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.classe.add',$parammage);
			}
			$id=$this->getUtildata()->getVaueOfArray($data,'id');
			 $parammage=array('parentid'=>$id); 
			$permimagegedisciplinedependencyurl=$this->permission->has_access('badiu.tms.discipline.dependency.index',$this->getSessionhashkey());
            $magegedisciplinedependencyurl=null;
			if($permimagegedisciplinedependencyurl){
				$magegedisciplinedependencyurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.discipline.dependency.index',$parammage);
			}
			
            if(!empty($manageclasseurl)){$manageclasseurl="<a href=\"$manageclasseurl\">$labelmanageclasselink</a><br />";}
            if(!empty($addclasseurl)){$addclasseurl ="<a href=\"$addclasseurl\">$labeladdclasselink</a><br />";}
			 if(!empty($magegedisciplinedependencyurl)){$magegedisciplinedependencyurl ="<a href=\"$magegedisciplinedependencyurl\">$labelmanagedisciplinedependencylink</a><br />";}

            $value="$manageclasseurl $addclasseurl $magegedisciplinedependencyurl";
           // $manageclasselink  badiu.tms.offer.classe.index dconfig.offer.discipline.lastid AS parentid  
        return $value; 
    } 
    
	
	
 public  function categorylinkemanager($data){
            $value="";
            $id=$this->getUtildata()->getVaueOfArray($data,'id');
            
			$labelmanagedisciplinelink=$this->getTranslator()->trans('badiu.tms.discipline.category.mamagediscipline');
            $labeladddisciplinelink=$this->getTranslator()->trans('badiu.tms.discipline.category.addnewdiscipline');

           
            $parammage=array('parentid'=>$id);
			
			$permimanagedisciplineurl=$this->permission->has_access('badiu.tms.discipline.categorydiscipline.index',$this->getSessionhashkey());
			$managedisciplineurl="";
			if($permimanagedisciplineurl){
				 $managedisciplineurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.discipline.categorydiscipline.index',$parammage);
			}
		  
		    $permiadddisciplineurl=$this->permission->has_access('badiu.tms.discipline.categorydiscipline.add',$this->getSessionhashkey());
            $adddisciplineurl=null;
			if($permiadddisciplineurl){
				$adddisciplineurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.discipline.categorydiscipline.add',$parammage);
			}
			
			
            if(!empty($managedisciplineurl)){$managedisciplineurl="<a href=\"$managedisciplineurl\">$labelmanagedisciplinelink</a><br />";}
            if(!empty($adddisciplineurl)){$adddisciplineurl ="<a href=\"$adddisciplineurl\">$labeladddisciplinelink</a><br />";}
			
            $value="$managedisciplineurl $adddisciplineurl ";
         
        return $value; 
    }	
}
