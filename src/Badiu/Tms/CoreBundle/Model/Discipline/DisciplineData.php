<?php

namespace Badiu\Tms\CoreBundle\Model\Discipline;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\DisciplineBundle\Model\DisciplineData as AmsDisciplineData;
class DisciplineData  extends AmsDisciplineData {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
 
   public function getForAutocomplete() {
        
        $disciplinecategoryid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('disciplinecategoryid'); 
        $name=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
        
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $disciplineData=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
      
        $wsql="";
        if(!empty($name)){$wsql.=" AND CONCAT(o.id,LOWER(o.name))  LIKE :name ";}
		if(!empty($disciplinecategoryid)){$wsql=" AND o.categoryid=:categoryid ";}
        $sql="SELECT  o.id,o.name  FROM ".$disciplineData->getBundleEntity()." o  WHERE  o.entity = :entity AND o.dtype=:dtype $wsql ";
		
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
		$query->setParameter('dtype','training');
        if(!empty($name)){$query->setParameter('name','%'.strtolower($name).'%');}
		if(!empty($disciplinecategoryid)){$query->setParameter('categoryid',$disciplinecategoryid);}
        $result= $query->getResult();
        return  $result; 
        
    }
	
	
	  public function getNameByIdOnEditAutocomplete($id) {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT o.name FROM ".$this->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }
  
   public function getForAutocompleteNewsFordependance() {
        $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
        $disciplinecategoryid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('disciplinecategoryid'); 
        $name=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
        
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $disciplineData=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
      
        $wsql="";
        if(!empty($name)){$wsql.=" AND CONCAT(o.id,LOWER(o.name))  LIKE :name ";}
		if(!empty($disciplinecategoryid)){$wsql=" AND o.categoryid=:categoryid ";}
		 if(!empty($parentid)){$wsql.=" AND (SELECT COUNT(d.id) FROM  BadiuAmsDisciplineBundle:AmsDisciplineDependency d WHERE d.requredisciplineid=o.id AND d.disciplineid =$parentid) = 0";}
        $sql="SELECT  o.id,o.name  FROM ".$disciplineData->getBundleEntity()." o  WHERE  o.entity = :entity AND o.dtype=:dtype $wsql ";
		
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
		$query->setParameter('dtype','training');
        if(!empty($name)){$query->setParameter('name','%'.strtolower($name).'%');}
		if(!empty($disciplinecategoryid)){$query->setParameter('categoryid',$disciplinecategoryid);}
        $result= $query->getResult();
        return  $result; 
        
    }
}
