<?php

namespace Badiu\Tms\CoreBundle\Model\Discipline;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class CategoryDefaultSqlFilter extends BadiuSqlFilter{
    /**
     * @var object
     */
  
    function __construct(Container $container) {
            parent::__construct($container);
       }

        function exec() {
           
           return null;
        }
        
       
		
		
		function addfilterpermission() {
			$wsql="";
			
			$userid=$this->getUserid();
			
			$userrecorddata=$this->getContainer()->get('badiu.system.access.userrecord.data');
			$fparam=array('userid'=>$userid,'modulekey'=>'badiu.tms.discipline.category','entity'=>$this->getEntity());
			$countrecord=$userrecorddata->countGlobalRow($fparam);
			
			if(empty($countrecord)){return "";}
			$wsql=" AND (SELECT COUNT(aur1.id) FROM BadiuSystemAccessBundle:SystemAccessUserRecord aur1 WHERE aur1.userid =$userid AND aur1.moduleinstance=dct.id AND aur1.modulekey='badiu.tms.discipline.category') > 0 ";
			return $wsql;
         }
		 
} 
