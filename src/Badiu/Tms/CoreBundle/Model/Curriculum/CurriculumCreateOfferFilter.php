<?php
namespace Badiu\Tms\CoreBundle\Model\Curriculum;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class CurriculumCreateOfferFilter extends BadiuFormFilter{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
   public function execBeforeSubmit() {
    
        }
        
   public function execAfterSubmit() {
	  echo "fffs";exit;
		 $entity=$this->getEntity();
		 $curriculumid = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
		 if(empty($curriculumid)){return null;}
		 
		 $curriculumname = $this->getUtildata()->getVaueOfArray($this->getParam(), 'name');
		 if(empty($curriculumname)){return null;}
		 
		 $dconfig= $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
		 if(empty($dconfig)){$dconfig=array();}
		 else {$dconfig=$this->getJson()->decode($dconfig,true);}
		

		//get default course id
		$courseid=$this->getContainer()->get('badiu.ams.course.course.data')->getIdByShortname($entity,'tmsdefaultcourse');
		
		//get offerid
		 $offerid=null;
		 $offerdata = $this->getContainer()->get('badiu.ams.offer.offer.data');
		 $offershortname='tmsdefaultcurriculumoffer-'.$curriculumid;
		 $exist=$offerdata->countGlobalRow(array('entity'=>$entity,'shortname'=>$offershortname));
         if($exist){
			$offerid=$offerdata->getIdByShortname($entity,$offershortname);
		 }else{
			//add offer			
			$dtype='training';
				$param=array();
				$param['entity']=$entity;
				$param['name']=$curriculumname; 
				$param['courseid']=$courseid;
				$param['shortname']=$offershortname; 
				$param['dtype']=$dtype;
				$param['timecreated']=new \DateTime();
				$param['deleted']=FALSE;
				$offerid= $data->insertNativeSql($param,false);  
		 }
		
       
		 $dconfig['offer']['id']=$offerid;
		 $dconfig=$this->getJson()->encode($dconfig);

		 $fparam=$this->getParam();
		 $curriculumdata=$this->getContainer()->get('badiu.tms.curriculum.curriculum.data');
		 $aparam=array('id'=>$curriculumid,'dconfig'=>$dconfig);
		 $curriculumdata->updateNativeSql($aparam,false);
		 $fparam['dconfig']=$dconfig;
		 $this->setParam($fparam);
       }
    

}
