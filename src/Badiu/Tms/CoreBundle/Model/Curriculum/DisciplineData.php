<?php

namespace Badiu\Tms\CoreBundle\Model\Curriculum;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\CurriculumBundle\Model\DisciplineData as AmsDisciplineData;
class DisciplineData  extends   AmsDisciplineData {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
			$this->setDtype('training');
              }
   
}
