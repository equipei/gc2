<?php

namespace Badiu\Tms\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuWebService;
use Badiu\Ams\CoreBundle\Model\Lib\LmsMoodleSyncDefaultForm AS AmsLmsMoodleSyncDefaultForm;
class LmsMoodleSyncDefaultForm    extends AmsLmsMoodleSyncDefaultForm{

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
  
	public function initParamConfig($type='offer') {
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		$badiuSession->setHashkey($this->getSessionhashkey());
		
		$param=array();
		$defaultserverservice=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.defaultserverservice');
		$param['sserviceid'] = $defaultserverservice;

		$defaulttype=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.defaulttype');
		$param['lmssynctype'] = $defaulttype;
		
		$defaultcourselevel=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.defaultcourselevel');
		$param['lmssynclevel'] = $defaultcourselevel;

		$defaultcoursecategory=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.defaultcoursecategory');
		$param['lmscoursecatparent'] = $defaultcoursecategory;

		$defaultautomatic=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.automatic');
		$param['lmssyncautomatic'] = $defaultautomatic;

		$defaultforceupdate=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.forceupdate');
		$param['lmssyncforceupdate'] = $defaultforceupdate;

		return $param;
	   }
	

	public function disciplineCategoryLevelFormChangeParamOnOpen($param) {
			$cparam=$this->initParamConfig();
			$defaultserverservice=$this->getUtildata()->getVaueOfArray($cparam,'sserviceid');
			$param['sserviceid'] = $defaultserverservice;
			
			$defaulttype=$this->getUtildata()->getVaueOfArray($cparam,'lmssynctype');
			//$param['lmssynctype'] = $defaulttype;
			$param['lmssynctype'] = 'replicationdatainlms';

			$defaultcourselevel=$this->getUtildata()->getVaueOfArray($cparam,'lmssynclevel');
			$param['lmssynclevel'] = $defaultcourselevel;

			$defaultcoursecategory=$this->getUtildata()->getVaueOfArray($cparam,'lmscoursecatparent');
			$param['lmscoursecatparent'] = $defaultcoursecategory;

			$mdlcoursecatname=null;
			if(!empty($defaultserverservice) && !empty($defaultcoursecategory)){
				$mdldatautil=$this->getContainer()->get('badiu.moodle.core.lib.datautil');
				$mdlcoursecatname=$mdldatautil->getCoursecatname($defaultserverservice,$defaultcoursecategory);

				$dconfig=array();
				$dconfig['lmsintegration']['lmscoursecatname']=$mdlcoursecatname;
				$dconfig= $this->getJson()->encode($dconfig);
				$param['dconfig']=$dconfig;
			} 
			
			return $param;
	}
	
	public function disciplineFormChangeParam($param) {
		
		return $param;
		
	}

	public function disciplineFormChangeParamOnOpen($param) {
		
		return $param;
	}
}
