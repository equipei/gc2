<?php

namespace Badiu\Tms\CoreBundle\Model\Enrol;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\EnrolBundle\Model\CoreFormat AS AmsCoreFormat;

class CoreFormat extends AmsCoreFormat {

  	private $permission;
	 function __construct(Container $container) {
        parent::__construct($container);
		$this->permission=$this->getContainer()->get('badiu.system.access.permission');
	}
 
	
	public  function classeurledit($data){ 
			
			$permissionedit=$this->permission->has_access('badiu.tms.offer.classeenrol.edit',$this->getSessionhashkey());

			if(!$permissionedit){return null;}
			
			$id =$this->getUtildata()->getVaueOfArray($data,'id');
			$classeid =$this->getUtildata()->getVaueOfArray($data,'classeid');
			$urlgoback=$this->getUtilapp()->getCurrentUrl();
			//$urlgoback= str_replace('/system/service/process', '/tms/my/student/fview/default/index', $urlgoback);
			$parammage=array('id'=>$id=$id,'_urlgoback'=>urlencode($urlgoback),'parentid'=>$classeid);
			$enrolediturl=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.classeenrol.edit',$parammage);
			
			if(empty($enrolediturl)){return  null;}
			
			return  $enrolediturl;
		} 	
		
	public  function classeiconedit($data){ 
			
			$url=$this->classeurledit($data);
			if(empty($url)){return  null;}
			$link="<a href=\"$url\"><i class=\"fa fa-edit\"></i></a>";
			return  $link;
		} 
		
}
