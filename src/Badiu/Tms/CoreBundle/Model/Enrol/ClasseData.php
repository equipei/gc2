<?php

namespace Badiu\Tms\CoreBundle\Model\Enrol;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\EnrolBundle\Model\ClasseData as AmsClasseData;
class ClasseData  extends AmsClasseData {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    	
	 //get user just enroled in classe
    public function getEnroledUsers() {
         $classeid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
         $name=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
         
        if(empty($classeid)) return null;

        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $userData=$this->getContainer()->get('badiu.system.user.user.data');
        
        $wsql="";
        if(!empty($name)){
            $wsql=" AND LOWER(CONCAT(u.id,u.firstname,u.email))  LIKE :name ";
            $name=strtolower($name);
        }
         $sql="SELECT DISTINCT u.id,CONCAT(u.firstname,' ',u.lastname,' ',u.email) AS name FROM ".$this->getBundleEntity()." o JOIN o.userid u JOIN o.roleid r WHERE  o.entity = :entity  AND o.classeid=:classeid AND r.shortname=:roleshortname $wsql ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('classeid',$classeid);
		$query->setParameter('roleshortname','studentcorporate');
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(50);
        $result= $query->getResult();
        return  $result;
        }	
}
