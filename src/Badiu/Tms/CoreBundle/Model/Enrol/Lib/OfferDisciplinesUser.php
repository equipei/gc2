<?php

namespace Badiu\Tms\CoreBundle\Model\Enrol\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class OfferDisciplinesUser extends BadiuModelLib {

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
	   //http://54.156.96.187/badiunet/web/app_dev.php/system/service/process?_service=badiu.tms.enrol.offer.lib.disciplineuser&_function=teste
  /*  public function  teste(){
		
		$fparam=array('userid'=>1016,'offerid'=>99);
		$list=$this->getDisciplines($fparam);
		$tbl=$this->getDisciplinesTableFormat($list);
		echo $tbl;exit;
	}*/
	public function getDisciplines($param) {
		 $offerid=$this->getUtildata()->getVaueOfArray($param,'offerid');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 
		 if(empty($entity)){$entity=$this->getEntity();}
		 $edata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 $sql="SELECT d.id,o.drequired,d.name,d.dhour,d.shortname,(SELECT s1.name FROM BadiuAmsEnrolBundle:AmsEnrolDiscipline o1 JOIN o1.odisciplineid od1 JOIN o1.statusid s1 WHERE od1.id=o.id AND o1.userid=$userid) AS status FROM BadiuAmsOfferBundle:AmsOfferDiscipline o JOIN o.disciplineid d WHERE  o.entity=:entity AND o.offerid=:offerid ";

		$query = $edata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('offerid', $offerid);
		$result = $query->getResult();
		return $result; 
		  
	}

	public function getDisciplinesTableFormat($list) {
		if(!is_array($list)){return null;}
		$configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
		
		$html=''; 
		$html.="<table id=\"trialcourse\"  class=\"table\">
				<tr>
					<th>Código</th>
					<th>Curso</th>
					<th>Carga horária</th>
					<th>Obrigatório</th>
					<th>Status</th>
				</tr>";
		$notenrolled=$this->getTranslator()->trans('badiu.tms.enrol.notenrol');
		foreach ($list as $row) {
			$shortname=$this->getUtildata()->getVaueOfArray($row,'shortname');
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			$dhour=$this->getUtildata()->getVaueOfArray($row,'dhour');
			$dhour=$configformat->textHourRound($dhour);
			
			$drequired=$this->getUtildata()->getVaueOfArray($row,'drequired');
			$drequired=$configformat->defaultBoolean($drequired);
			
			$status=$this->getUtildata()->getVaueOfArray($row,'status');
			if(empty($status)){$status=$notenrolled;}
			$html.="
			<tr>
				<td>$shortname</td>
				<td>$name</td>
				<td>$dhour</td>
				<td>$drequired</td>
				<td>$status</td>
			</tr>";
		}
		$html.="</table>";
		return $html;
	

	}		
	
}
