<?php

namespace Badiu\Tms\CoreBundle\Model\Enrol\Lib;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\EnrolBundle\Model\DOMAINTABLE;
use Badiu\Tms\CoreBundle\Model\DOMAINTABLE as TMSDOMAINTABLE;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
use Badiu\Ams\EnrolBundle\Model\Lib\DisciplineEnrolChek;
class DisciplineDepartmentEnrolChek  extends DisciplineEnrolChek 
{

    /**
     * @var Container
     */
    private $container;

	private $response;
	function __construct(Container $container)
    {
        parent::__construct($container);
		$this->response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
      }

	
	public function exec()
    {	
		//get param
		$param=$this->getParam();
		
		$checkParam=$this->checkParam($param);
		if(!$checkParam){return $this->getResponse()->get();}
		
	
		$chekToken=$this->checkToken();
		if(!$chekToken){return $this->getResponse()->get();}
		
		//exec service
		$checkEnrol=$this->checkEnrol($param);
				
		return $this->getResponse()->get();
    }
	
	
	
	public function checkEnrol($param)
    { 
		$serviceid=$this->getServiceid();
		$data=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
		$entity=$this->getEntity();
		$userid=$param->userid;
		$lmscourseid=$param->courseid;
		$serviceid=1;
		$resul=false;
		//echo "entity: $entity userid: $userid lmscourseid: $lmscourseid";exit;
		//$resul=$data->existEnrolByLms($entity,$userid,$serviceid,$lmscourseid);
		$countDepartment=$this->countDepartments($entity,$serviceid,$lmscourseid);
		if($countDepartment){
			$listDepartmentsid=$this->getListDepartmentsid($entity,$serviceid,$lmscourseid);
			$ldepartement=$this->getListValueOfArray($listDepartmentsid);
			//print_r($ldepartement);exit;
			$isMemberOfDepartment= $this->isDepartamentMember($entity,$ldepartement,$userid);
			$resul=$isMemberOfDepartment;
		}else{
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo(TMSDOMAINTABLE::$REQUEST_DENIED_USER_NOT_ENROLED_IN_ANY_DEPARTMENT);
		
		}
		if($resul){
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		}else{
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo(TMSDOMAINTABLE::$REQUEST_DENIED_USER_NOT_ENROLED_IN_ANY_DEPARTMENT);
		}
		return $resul;
    }
	
	public function countDepartments($entity,$serviceid,$lmscourseid) {
    		$r=FALSE;
			$odiscplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			$sql="SELECT  COUNT(o.id) AS countrecord FROM ".$odiscplinedata->getBundleEntity()." o JOIN o.offerid f  WHERE o.entity = :entity AND o.sserviceid=:sserviceid  AND o.idnumberlms=:idnumberlms AND f.moduleinstance >0  ";
      		$query = $odiscplinedata->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('sserviceid',$serviceid);
			$query->setParameter('idnumberlms',$lmscourseid);
			
			
            $result= $query->getSingleResult();
			if($result['countrecord']>0){$r=TRUE;}
            return $r;
   }
   
  
	public function getListDepartmentsid($entity,$serviceid,$lmscourseid) {
    		$odiscplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			$sql="SELECT  DISTINCT f.moduleinstance AS id FROM ".$odiscplinedata->getBundleEntity()." o JOIN o.offerid f  WHERE o.entity = :entity AND o.sserviceid=:sserviceid  AND o.idnumberlms=:idnumberlms  AND f.moduleinstance >0  ";
      		$query = $odiscplinedata->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('sserviceid',$serviceid);
			$query->setParameter('idnumberlms',$lmscourseid);
			
			
            $result= $query->getResult();
			
            return $result;
   
	}
	public function isDepartamentMember($entity,$departmentsid,$userid) {
    		$r=FALSE;
			$dmembersdata=$this->getContainer()->get('badiu.admin.enterprise.dmembers.data');
			$sql="SELECT  COUNT(o.id) AS countrecord  FROM ".$dmembersdata->getBundleEntity()." o  WHERE o.entity = :entity AND o.userid= :userid AND o.departmentid IN (:departmentid) ";
      		$query = $dmembersdata->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('userid',$userid);
			$query->setParameter('departmentid',$departmentsid);
			$result= $query->getSingleResult();
			
			if($result['countrecord']>0){$r=TRUE;}
            return $r;
   
	}
	//levar isso para lib
	public function getListValueOfArray($list,$key='id'){
		$nlist=array();
		foreach($list as $l){
			$value=$l[$key];
			array_push($nlist,$value);
		}
		return $nlist;
	}
}
?>