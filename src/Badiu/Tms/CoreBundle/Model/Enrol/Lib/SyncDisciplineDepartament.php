<?php

namespace Badiu\Tms\CoreBundle\Model\Enrol\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Badiu\Ams\OfferBundle\Entity\AmsOfferDiscipline;
use Badiu\Tms\CoreBundle\Model\Enrol\Lib\SyncOfferDepartament;
class SyncDisciplineDepartament extends SyncOfferDepartament{
    
	 /**
     * @var AmsOfferDiscipline
     */
    private $odiscipline;

	
    function __construct(Container $container) {
            parent::__construct($container);
			$this->odiscipline=null;
        }
    

	public function enrold($odisciplineid,$departmentid) {
			$this->initOdisciplineid($odisciplineid);
		    $enrolStatu=null;
			$role=null;
		
			$members=$this->getDepartmentMembers($departmentid);
			if(empty($members) && sizeof($members)==0){
				return null;
			}
		
			$enrolStatus=$this->getEnrolStatus();
			$role=$this->getRole();
			
			$greadeItem=$this->createFinalGradeItem(); 
		
		//add list to offer
		foreach ($members as $member){
		   $user=$member->getUserid();
		   $result=$this->processEnrol($this->odiscipline,$user,$role,$enrolStatus);
		   $this->enrolGrade($greadeItem,$user);
		   
		}
		
	}
	
	public function enrolOffertedDiscipline($offerid) {
		$this->initOffer($offerid);
		$departmentid=$this->getOffer()->getModuleinstance();
		$listodisciplines=$this->getDisciplinesByOffer($offerid);
		if(empty($listodisciplines) && sizeof($listodisciplines)==0){
				return null;
		}
		//$x=10;
		//$y=12;
		$cont=0;
		foreach ($listodisciplines as $odiscipline){
			//if($cont >=$x && $cont <$y){
				$this->odiscipline=$odiscipline;
				$this->enrold($odiscipline->getId(),$departmentid);
				//echo "process disc $cont <br>";
			//}
			
			$cont++;
		}
		
	}
	
	public function processEnrol($odiscipline,$user,$role,$status) {
		
		//check if exist
		$roleData=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
		$exist=$roleData->existEnrol($this->getEntity(),$odiscipline->getId(),$user->getId(),$role->getId());
		//add 
		if(!$exist){
			$data=$this->getContainer()->get('badiu.ams.enrol.offer.data');
			$dto=$this->getContainer()->get('badiu.ams.enrol.discipline.entity');
			$dto=$this->initDefaultEntityData($dto);
			$dto->setOdisciplineid($odiscipline);
			$dto->setUserid($user);
			$dto->setRoleid($role);
			$dto->setStatusid($status);
			$data->setDto($dto);
			$data->save();
		}
		
	}
	
	
		
	public function initOdisciplineid($odisciplineid) {
			if(empty($odisciplineid)){
				$odisciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
				$this->offer=$offerdata->findById($odisciplineid);
			}
			
	}
	
	public function getDisciplinesByOffer($offerid) {
		$odisciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
		$listodisciplines=$odisciplinedata->getByOffer($offerid);
		return $listodisciplines;
	}
	
	public function createFinalGradeItem() {
		$dto=$this->getContainer()->get('badiu.ams.grade.item.entity');
		$data=$this->getContainer()->get('badiu.ams.grade.item.data');
		
		$exist=$data->existFinalItem($this->getEntity(),$this->odiscipline->getId());
		if($exist){
			$dto=$data->findFinalItem($this->getEntity(),$this->odiscipline->getId());
		}else{
			$dto=$this->initDefaultEntityData($dto);
			$dto->setOdisciplineid($this->odiscipline);
			$dto->setName('discipline');
			$dto->setItemtype('discipline');
			$dto->setMaxgrade(100);
			$data->setDto($dto);
			$data->save();
			$dto=$data->getDto();
		}
		
		return $dto;
	}
	
	
	public function enrolGrade($item,$user) {
		$dto=$this->getContainer()->get('badiu.ams.grade.grade.entity');
		$data=$this->getContainer()->get('badiu.ams.grade.grade.data');
		
		$exist=$data->existUser($this->getEntity(),$item->getId(),$user->getId());
		if(!$exist){
			$dto=$this->initDefaultEntityData($dto);
			$dto->setItemid($item);
			$dto->setUserid($user);
			$data->setDto($dto);
			$data->save();
			$dto=$data->getDto();
			return $dto;
		}
		return null;
	}
	 public function getOdiscipline() {
          return $this->odiscipline;
      }

	  
      public function setOdiscipline(AmsOfferDiscipline $odiscipline) {
          $this->odiscipline = $odiscipline;
      }

}
