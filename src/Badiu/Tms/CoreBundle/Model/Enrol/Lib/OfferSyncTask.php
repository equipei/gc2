<?php

namespace Badiu\Tms\CoreBundle\Model\Enrol\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

class OfferSyncTask  extends  TaskGeneralExec{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
				$this->cresult=array('datashouldexec' => 0, 'dataexec' => 0);
        } 

  public function import() {
   $result=  $this->addNewEnrol();
   return $this->cresult;

}

public function update() {
   $this->updateCompletedOfferEnrol();
   $this->updateCompletedDisciplineEnrol();
    return $this->cresult;
}

public function updateCompletedOfferEnrol(){
	$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
	if($operation!='updatecompletedofferenrol'){return null;}
	$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
	
	//get lista of enrol not completed
	$fparam=array('maxrecord'=>$maxrecord,'type'=>'completedalldiscipline');
	$listenrol=$this->getOfferEnrol($fparam);
	
	 if(!is_array($listenrol)){return  null;}
	$econtexec=0;
	$statusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($this->getEntity(),'approvedenrol');
	$data=$this->getContainer()->get('badiu.tms.enrol.offer.data');
	$certificaterlink=$this->getContainer()->get('badiu.admin.certificate.request.link');
	foreach ($listenrol as $row) {
		$userid= $this->getUtildata()->getVaueOfArray($row,'userid');
		$offerid= $this->getUtildata()->getVaueOfArray($row,'offerid');
		$enrolofferid= $this->getUtildata()->getVaueOfArray($row,'id');
		$iparam=array('id'=>$enrolofferid,'statusid'=>$statusid,'timefinish'=>new \DateTime(),'timemodified'=>new \DateTime());
		$result=$data->updateNativeSql($iparam,false);
		$cparam=array('modulekey'=>'badiu.ams.enrol.offer','moduleinstance'=>$offerid,'userid'=>$userid,'_operation'=>'savedraft','_source'=>'api');
		$certificaterlink->setParam($cparam);
		$crlresult= $certificaterlink->savedraft();
		
		$datashouldexec=1;
		$dataexec=0;
		if($result){$dataexec=1;$econtexec++;}
		 $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => null);
		$this->cresult = $this->addResult($this->cresult, $resultmdl);  
	}
	return $econtexec;	 
}
public function updateCompletedDisciplineEnrol(){
	$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
	if($operation!='updatecompleteddisciplineenrol'){return null;}
	$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
	
	//get lista of enrol not completed
	$fparam=array('maxrecord'=>$maxrecord,'type'=>'notcompletedalldiscipline');
	$listenrol=$this->getOfferEnrol($fparam);
	 if(!is_array($listenrol)){return  null;}
	
	$econtexec=0;
	$statusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($this->getEntity(),'approvedenrol');
	$data=$this->getContainer()->get('badiu.tms.enrol.offer.data');
	foreach ($listenrol as $row) {
		$userid= $this->getUtildata()->getVaueOfArray($row,'userid');
		$offerid= $this->getUtildata()->getVaueOfArray($row,'offerid');
		$enrolofferid= $this->getUtildata()->getVaueOfArray($row,'id');
		
		$dparam=array('userid'=>$userid,'offerid'=>$offerid);
		$listdisciplines=$this->getOfferDiscipline($dparam);
		$datashouldexec=sizeof($listdisciplines);
		$dataexec=0;
		
		 foreach ($listdisciplines as $ldrow) {
				$disciplineid=$this->getUtildata()->getVaueOfArray($ldrow,'disciplineid');
				$odisciplineid=$this->getUtildata()->getVaueOfArray($ldrow,'odisciplineid');
				$edfparam=array('userid'=>$userid,'odisciplineid'=>$odisciplineid,'disciplineid'=>$disciplineid,'statusid'=>$statusid);
				
				$r=$this->changeEnrolTofferDiscipline($edfparam); 
				if($r){ $econtexec++; $dataexec++;}
		 }
		$dataexec=0;
		
		 $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => null);
		$this->cresult = $this->addResult($this->cresult, $resultmdl);  
	}
	return $econtexec;	 
}
public function addNewEnrol(){
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='addnewenrol'){return null;}
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		
		//get lista of enrol not completed
		$fparam=array('maxrecord'=>$maxrecord,'type'=>'newsenroltodiscipline');
		$listenrol=$this->getOfferEnrol($fparam);
		 if(!is_array($listenrol)){return  null;}
		 $econtexec=0;
		
		 foreach ($listenrol as $row) {
			$userid= $this->getUtildata()->getVaueOfArray($row,'userid');
			$offerid= $this->getUtildata()->getVaueOfArray($row,'offerid');
			//get list of offer discipline not enroled by user
			$dparam=array('userid'=>$userid,'offerid'=>$offerid,'type'=>'usrnotenroled');
			$listdisciplines=$this->getOfferDiscipline($dparam);
			
			if(is_array($listdisciplines)){
				 $datashouldexec=sizeof($listdisciplines);
				  $dataexec=0;
				  foreach ($listdisciplines as $ldrow) {
					  $disciplineid=$this->getUtildata()->getVaueOfArray($ldrow,'disciplineid');
					  $odisciplineid=$this->getUtildata()->getVaueOfArray($ldrow,'odisciplineid');
					  $edfparam=array('userid'=>$userid,'odisciplineid'=>$odisciplineid,'disciplineid'=>$disciplineid);
						$r=$this->addEnrolTofferDiscipline($edfparam);
					  if($r){ $econtexec++; $dataexec++;}
				  }
				 $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => null);
				 $this->cresult = $this->addResult($this->cresult, $resultmdl);   
			 }
			
			
		 }
		
		  return $econtexec;
	}
	
  public function getOfferEnrol($param) {
	    $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		$type=$this->getUtildata()->getVaueOfArray($param,'type');
		$wsql="";
		$wsqlcountofferdiscipline="(SELECT COUNT(o2.id) FROM BadiuAmsOfferBundle:AmsOfferDiscipline o2 WHERE o2.offerid=f.id AND o2.deleted=0 ) ";
		$wsqlcountofferdisciplinerequired="(SELECT COUNT(o2.id) FROM BadiuAmsOfferBundle:AmsOfferDiscipline o2 WHERE o2.offerid=f.id AND o2.deleted=0 AND o2.drequired=1) ";
		 if($type=='newsenroltodiscipline'){
			$wsqlcountenroldiscipline="(SELECT COUNT(o1.id) FROM BadiuAmsEnrolBundle:AmsEnrolDiscipline o1 JOIN o1.statusid s1 JOIN o1.odisciplineid od1 WHERE od1.offerid=f.id AND o1.userid=o.userid AND o1.deleted=0  ) ";
			//count enrol in discipline  is less than count offer discipline
			$wsql.=" AND ($wsqlcountenroldiscipline < $wsqlcountofferdiscipline) ";
			$wsql.=" AND s.shortname != 'approvedenrol' ";
		 } else if($type=='completedalldiscipline' || $type=='notcompletedalldiscipline' ){
			 $wsqlcountenroldiscipline="(SELECT COUNT(o1.id) FROM BadiuAmsEnrolBundle:AmsEnrolDiscipline o1 JOIN o1.statusid s1 JOIN o1.odisciplineid od1 WHERE od1.offerid=f.id AND o1.userid=o.userid AND s1.shortname = 'approvedenrol' AND o1.deleted=0 AND od1.drequired=1) ";
			//count enrol in discipline  is less than count offer discipline
			if($type=='completedalldiscipline'){$wsql.=" AND ($wsqlcountenroldiscipline = $wsqlcountofferdisciplinerequired) ";}
			else if($type=='notcompletedalldiscipline'){$wsql.=" AND ($wsqlcountenroldiscipline < $wsqlcountofferdisciplinerequired) ";}
			$wsql.=" AND s.shortname != 'approvedenrol' ";
		 }
		$data=$this->getContainer()->get('badiu.tms.enrol.offer.data');
		$sql="SELECT o.id,u.id AS userid,f.id AS offerid FROM BadiuAmsEnrolBundle:AmsEnrolOffer o JOIN o.offerid f JOIN o.roleid r JOIN o.statusid s JOIN o.userid u WHERE f.shortname != :offershortname AND f.dtype=:dtype AND  o.entity=:entity  AND o.deleted=:deleted $wsql ORDER BY o.timestart ";
        
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $this->getEntity());
		$query->setParameter('offershortname', 'tmsdefaultcourseoffer');
		$query->setParameter('dtype', 'training');
		$query->setParameter('deleted', 0);
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		
		
        return $result;
    } 
	
	public function getOfferDiscipline($param) {
	   $type=$this->getUtildata()->getVaueOfArray($param,'type');
		$userid= $this->getUtildata()->getVaueOfArray($param,'userid');
		$offerid= $this->getUtildata()->getVaueOfArray($param,'offerid');
		
		$wsql="";
		//get list discipline user is not enroled
		 if($type=='usrnotenroled'){ 
			$wsqlcountenroldiscipline="SELECT COUNT(o1.id) FROM BadiuAmsEnrolBundle:AmsEnrolDiscipline o1 JOIN o1.odisciplineid od1 WHERE od1.id=o.id AND o1.userid=$userid AND o1.deleted=0 ";
			$wsql.=" AND ($wsqlcountenroldiscipline) = 0 ";
		 }
		
        $data=$this->getContainer()->get('badiu.tms.offer.discipline.data');
		$sql="SELECT o.id AS odisciplineid,d.id AS disciplineid FROM BadiuAmsOfferBundle:AmsOfferDiscipline o JOIN o.disciplineid d WHERE o.offerid= :offerid AND o.entity=:entity  $wsql  ";
		
        $query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $this->getEntity());
		$query->setParameter('offerid', $offerid);
		$result = $query->getResult();
        return $result; 
    } 
 
	public function addEnrolTofferDiscipline($param) {
	    $userid= $this->getUtildata()->getVaueOfArray($param,'userid');
		$odisciplineid= $this->getUtildata()->getVaueOfArray($param,'odisciplineid');
		$disciplineid= $this->getUtildata()->getVaueOfArray($param,'disciplineid');
		
		$data=$this->getContainer()->get('badiu.tms.offer.discipline.data');
		//count discipline enrol
		$sql="SELECT o.id,s.id AS statusid,r.id AS roleid FROM BadiuAmsEnrolBundle:AmsEnrolDiscipline o JOIN o.statusid s JOIN o.roleid r JOIN o.odisciplineid od JOIN od.offerid f WHERE o.entity=:entity  AND f.shortname = :offershortname AND od.disciplineid=:disciplineid AND o.userid=:userid AND s.enable=:statusenable ORDER BY o.timemodified DESC ";
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $this->getEntity());
		$query->setParameter('offershortname','tmsdefaultcourseoffer' );
		$query->setParameter('disciplineid', $disciplineid);
		$query->setParameter('userid', $userid);
		$query->setParameter('statusenable', 1);
		$odresult = $query->getResult();
		
		 $disciplinedatalib=$this->getContainer()->get('badiu.ams.enrol.discipline.lib');
		$countexec=0;
		 foreach ($odresult as $odrow) {
			 if($countexec==1){break;}
			  $roleid=$this->getUtildata()->getVaueOfArray($odrow,'roleid');
			  $statusid=$this->getUtildata()->getVaueOfArray($odrow,'statusid');
			  $fparam=array('odisciplineid'=>$odisciplineid,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid);
			  $disciplinedatalib->add($fparam);
			  $countexec++;
		 }
		 
		 if($countexec){return $countexec;}
		//count classe enrol
		
		//count discipline enrol
		$sql="SELECT o.id,s.id AS statusid,r.id AS roleid FROM BadiuAmsEnrolBundle:AmsEnrolClasse o JOIN o.statusid s JOIN o.roleid r JOIN o.classeid cl JOIN cl.odisciplineid od JOIN od.offerid f WHERE  o.entity=:entity  AND f.shortname = :offershortname AND od.disciplineid=:disciplineid AND o.userid=:userid AND s.enable=:statusenable ORDER BY o.timemodified DESC ";
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $this->getEntity());
		$query->setParameter('offershortname','tmsdefaultcourseoffer' );
		$query->setParameter('disciplineid', $disciplineid);
		$query->setParameter('userid', $userid);
		$query->setParameter('statusenable', 1);
		$odresult = $query->getResult();
		
		 
		$countexec=0;
		 foreach ($odresult as $odrow) {
			 if($countexec==1){break;}
			  $roleid= $this->getUtildata()->getVaueOfArray($odrow,'roleid');
			  $statusid=$this->getUtildata()->getVaueOfArray($odrow,'statusid');
			   $fparam=array('odisciplineid'=>$odisciplineid,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid);
			  
			  $disciplinedatalib->add($fparam);
			  $countexec++;
		 }
        return $countexec; 
    } 
	
	public function changeEnrolTofferDiscipline($param) {
	    $userid= $this->getUtildata()->getVaueOfArray($param,'userid');
		$odisciplineid= $this->getUtildata()->getVaueOfArray($param,'odisciplineid');
		$disciplineid= $this->getUtildata()->getVaueOfArray($param,'disciplineid');
		$statusid= $this->getUtildata()->getVaueOfArray($param,'statusid');
		
		$data=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
		//count discipline enrol
		$sql="SELECT o.id,r.id AS roleid FROM BadiuAmsEnrolBundle:AmsEnrolDiscipline o JOIN o.statusid s JOIN o.roleid r JOIN o.odisciplineid od JOIN od.offerid f WHERE o.entity=:entity  AND f.shortname = :offershortname AND od.disciplineid=:disciplineid AND o.userid=:userid AND s.id=:statusid ORDER BY o.timemodified DESC ";
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $this->getEntity());
		$query->setParameter('offershortname','tmsdefaultcourseoffer' );
		$query->setParameter('disciplineid', $disciplineid);
		$query->setParameter('userid', $userid);
		$query->setParameter('statusid', $statusid);
		$odresult = $query->getResult();
		
		 $disciplinedatalib=$this->getContainer()->get('badiu.ams.enrol.discipline.lib');
		$countexec=0;
		 foreach ($odresult as $odrow) {
			 if($countexec==1){break;}
			  $roleid=$this->getUtildata()->getVaueOfArray($odrow,'roleid');
			   $entity=$this->getEntity();
			  $entity=$this->getEntity();
			  $enrolid=$data->getEnrolid($entity,$odisciplineid,$userid,$roleid);
			  if($enrolid){
				   $iparam=array('id'=>$enrolid,'statusid'=>$statusid,'timemodified'=>new \DateTime());
				   $result=$data->updateNativeSql($iparam,false);	
			  }else{
				  $fparam=array('odisciplineid'=>$odisciplineid,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid);
				  $disciplinedatalib->add($fparam);
			  }
			  $countexec++;
		 }
		 
		 if($countexec){return $countexec;}
		//count classe enrol
		
		//count discipline enrol
		$sql="SELECT o.id,r.id AS roleid FROM BadiuAmsEnrolBundle:AmsEnrolClasse o JOIN o.statusid s JOIN o.roleid r JOIN o.classeid cl JOIN cl.odisciplineid od JOIN od.offerid f WHERE  o.entity=:entity  AND f.shortname = :offershortname AND od.disciplineid=:disciplineid AND o.userid=:userid AND s.id=:statusid ORDER BY o.timemodified DESC ";
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $this->getEntity());
		$query->setParameter('offershortname','tmsdefaultcourseoffer' );
		$query->setParameter('disciplineid', $disciplineid);
		$query->setParameter('userid', $userid);
		$query->setParameter('statusid', $statusid);
		$odresult = $query->getResult();
		
			$countexec=0;
		 foreach ($odresult as $odrow) {
			 if($countexec==1){break;}
			  $roleid= $this->getUtildata()->getVaueOfArray($odrow,'roleid');
			  $entity=$this->getEntity();
			  $enrolid=$data->getEnrolid($entity,$odisciplineid,$userid,$roleid);
			  if($enrolid){
				   $iparam=array('id'=>$enrolid,'statusid'=>$statusid,'timemodified'=>new \DateTime());
				   $result=$data->updateNativeSql($iparam,false);	
			  }else{
				  $fparam=array('odisciplineid'=>$odisciplineid,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid);
				  $disciplinedatalib->add($fparam);
			  }
			  $countexec++;
		 }
        return $countexec; 
    } 
}
?>