<?php

namespace Badiu\Tms\CoreBundle\Model\Enrol\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Badiu\Ams\OfferBundle\Entity\AmsOffer;
class SyncOfferDepartament extends BadiuModelLib{
    
	 /**
     * @var AmsOffer
     */
    private $offer;

	
    function __construct(Container $container) {
            parent::__construct($container);
			$this->offer=null;
        }
    

	
	public function enrol($offerid) {
			$this->initOffer($offerid);
		
		//get departament members list
		$departmentid=null;
		$enrolStatu=null;
		$role=null;
		
		//review
		//if($this->offer->getModulekey()=='badiu.admin.enterprise.department'){
			$departmentid=$this->offer->getModuleinstance();
		//}
		
		$members=$this->getDepartmentMembers($departmentid);
		if(empty($members) && sizeof($members)==0){
			return null;
		}
		
		$enrolStatus=$this->getEnrolStatus();
		$role=$this->getRole();
	
		//add list to offer
		foreach ($members as $member){
		   $user=$member->getUserid();
		   $result=$this->processEnrol($this->offer,$user,$role,$enrolStatus);
		   
		}
		
	}
	
	public function processEnrol($offer,$user,$role,$status) {
		
		//check if exist
		$roleData=$this->getContainer()->get('badiu.ams.enrol.offer.data');
		$exist=$roleData->existEnrol($this->getEntity(),$offer->getId(),$user->getId(),$role->getId());
		//add 
		if(!$exist){
			$data=$this->getContainer()->get('badiu.ams.enrol.offer.data');
			$dto=$this->getContainer()->get('badiu.ams.enrol.offer.entity');
			$dto=$this->initDefaultEntityData($dto);
			$dto->setOfferid($offer);
			$dto->setUserid($user);
			$dto->setRoleid($role);
			$dto->setStatusid($status);
			$data->setDto($dto);
			$data->save();
		}
		
	}
	
	public function getDepartmentMembers($departmentid) {
		
		//get statusDepartmentMembersid
		$statusDepartmentMembersData=$this->getContainer()->get('badiu.admin.enterprise.dmembersstatus.data');
		$statusDepartmentMembersid=$statusDepartmentMembersData->getIdByShortname($this->getEntity(),'active');
		
		//get list of members
		$departmentMembersData=$this->getContainer()->get('badiu.admin.enterprise.dmembers.data');
		$members=$departmentMembersData->geMembers($departmentid,$statusDepartmentMembersid);
		return $members;
	}
	
	public function getEnrolStatus() {
		$enrolStatusData=$this->getContainer()->get('badiu.ams.enrol.status.data');
		$enrolStatus=$enrolStatusData->findByShortname($this->getEntity(),'active');
		return $enrolStatus;
	}
	
	public function getRole() {
		$roleData=$this->getContainer()->get('badiu.ams.role.role.data');
		$role=$roleData->findByShortname($this->getEntity(),'student');
		return $role;
	}
	
	public function initOffer($offerid) {
			if(empty($offer)){
				$offerdata=$this->getContainer()->get('badiu.ams.offer.offer.data');
				$this->offer=$offerdata->findById($offerid);
			}
			
	}
	
	 public function getOffer() {
          return $this->offer;
      }

	  
      public function setOffer(AmsOffer $offer) {
          $this->offer = $offer;
      }

}
