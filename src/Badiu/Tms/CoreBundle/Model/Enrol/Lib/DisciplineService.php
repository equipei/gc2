<?php
namespace Badiu\Tms\CoreBundle\Model\Enrol\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
class DisciplineService extends  BadiuExternalService{
   
	public function __construct(Container $container) {
		parent::__construct($container);
	  
  }


 public function get(){
		//check token
                try {
                    $ckeckToken=$this->checkToken();
                    if(!$ckeckToken){return $this->getResponse()->get();}
		
                    $qparam=$this->getHttpQuerStringParam();
                    
                
                    $data=$this->getContainer()->get('badiu.system.user.user.data');
                    $entity=null;
                    $doctype=null;
                    $docnumber=null;
					$courseshortname=null;
                   
                    if(array_key_exists('entity',$qparam)){$entity=$qparam['entity'];}
                    if(array_key_exists('doctype',$qparam)){$doctype=$qparam['doctype'];}
                    if(array_key_exists('docnumber',$qparam)){$docnumber=$qparam['docnumber'];}
					if(array_key_exists('courseshortname',$qparam)){$courseshortname=$qparam['courseshortname'];}
                   
                    //doctype | docnumber
                    if(empty($doctype)){return $this->getResponse()->denied("badiu.system.user.user.doctype.is.empty");}
                    if(empty($docnumber)){return $this->getResponse()->denied("badiu.system.user.user.docnumber.is.empty");}
                    
					if($doctype=="CPF"){
						$cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
						$valid = $cpfdata->isValid($docnumber);
						if(!$valid){return $this->getResponse()->denied("badiu.system.user.user.docnumbercpf.is.notvalid");}
						$docnumber=$cpfdata->clean($docnumber);	
					}
					
					$ckparm=array('doctype'=>$doctype,'docnumber'=>$docnumber,'entity'=>$entity);
                    $exist=$data->countNativeSql($ckparm,false);
                    if(!$exist){return $this->getResponse()->denied("badiu.system.user.user.docnumber.not.exist");}
                    
					$userid=$data->getGlobalColumnValue('id',$ckparm);
					if(empty($userid)){return $this->getResponse()->denied("badiu.system.user.user.id.not.foundbydocument");}
					
					$result=$this->getDisciplines($userid,$courseshortname);
                    return $this->getResponse()->accept($result);
                   
                } catch (Exception $ex) {
                  
                    return $this->getResponse()->denied("badiu.system.error.general",  $ex);
                }
	}
	
	private function getDisciplines($userid,$courseshortname=null) {
		 $data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 $wsql= "";
		 if(!empty($courseshortname)){$wsql=" AND d.shortname=:courseshortname ";}
		 $sql="SELECT DISTINCT d.id, d.name AS coursename,d.shortname AS courseshortname, d.dhour AS coursehour,s.name AS stutsname,s.shortname AS statusshortname  FROM ".$data->getBundleEntity()." o JOIN o.statusid s JOIN o.classeid cl JOIN cl.odisciplineid od JOIN od.disciplineid d WHERE d.dtype=:dtype AND o.userid=:userid $wsql";
         $query = $data->getEm()->createQuery($sql);
         $query->setParameter('dtype','training');
		 $query->setParameter('userid',$userid);
		  if(!empty($courseshortname)){ $query->setParameter('courseshortname',$courseshortname);}
		 $result= $query->getResult();
        return $result;
  }  
}
