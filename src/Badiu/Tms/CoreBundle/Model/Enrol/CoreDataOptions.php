<?php

namespace Badiu\Tms\CoreBundle\Model\Enrol;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\EnrolBundle\Model\CoreDataOptions as  AmsCoreDataOptions;
class CoreDataOptions extends AmsCoreDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
   
	   public  function getDateType(){
		$list=array();
        $list['discipline']=$this->getDateTypeLabel('discipline');
		$list['classe']=$this->getDateTypeLabel('classe');
		$list['dynamic']=$this->getDateTypeLabel('dynamic');
	    return  $list;
       
    }
	public  function getDateTypeOffer(){
		$list=array();
       $list['dynamic']=$this->getDateTypeLabel('dynamic');
	    return  $list;
       
    }
	 public  function getLevelLabel($value){
        $label=null;
        if($value=='offer'){$label=$this->getTranslator()->trans('badiu.ams.enrol.level.offer');}
		else if($value=='discipline'){$label=$this->getTranslator()->trans('badiu.tms.enrol.level.discipline');}
		else if($value=='classe'){$label=$this->getTranslator()->trans('badiu.ams.enrol.level.classe');}
		else if($value=='lmsmoodle'){$label=$this->getTranslator()->trans('badiu.ams.enrol.level.lmsmoodle');}
	    return  $label;
    }
	
	public  function getDateTypeClasse(){
		 $list=array();
		$list['discipline']=$this->getDateTypeLabel('discipline');
		$list['dynamic']=$this->getDateTypeLabel('dynamic');
	    return  $list;
       
    }
	 public  function getDateTypeLabel($value){
        $label=null;
    	if($value=='classe'){$label=$this->getTranslator()->trans('badiu.ams.enrol.confenroldatetype.parentconfig.classe');}
		else if($value=='dynamic'){$label=$this->getTranslator()->trans('badiu.ams.enrol.confenroldatetype.dynamic');}
		
	    return  $label;
    }
	
	public  function getLevelReplicateClasse(){
		$list=array();
        $list['discipline']=$this->getLevelLabel('discipline');
		$list['lmsmoodle']=$this->getLevelLabel('lmsmoodle');
	    return  $list;
    }
	public  function getLevelReplicateUpdateClasse(){
		$list=array();
        $list['discipline']=$this->getLevelLabel('discipline');
		$list['lmsmoodle']=$this->getLevelLabel('lmsmoodle');
	    return  $list;
    }
	

	public  function getDefaultStatusSelection($keyconfig='badiu.tms.enrol.selection.param.config.defaultstatus',$default='activeenrol',$dtype='training'){
		$list=parent::getDefaultStatusSelection($keyconfig,$default,$dtype);
	    return  $list;
    }
	
	public  function getDefaultRoleSelection($keyconfig='badiu.tms.enrol.selection.param.config.defaultrole',$default='studentcorporate',$dtype='training'){
		$list=parent::getDefaultRoleSelection($keyconfig,$default,$dtype);
	    return  $list;
    }

}
