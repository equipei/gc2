<?php

namespace Badiu\Tms\CoreBundle\Model\Offer;

use Badiu\Ams\OfferBundle\Model\Classe\GradeAddBatchFormController AS AmsGradeAddBatchFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class GradeAddBatchFormController extends AmsGradeAddBatchFormController
{ 
    function __construct(Container $container) {
            parent::__construct($container);
				$this->setListstudentbasekey('badiu.tms.offer.classegradeaddbatchliststudent');
            }
    
     
}
