<?php
namespace Badiu\Tms\CoreBundle\Model\Offer\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\OfferBundle\Model\Classe\Lib\ClassePlannedAttendanceFilter as AmsClassePlannedAttendanceFilter;
class ClassePlannedAttendanceFilter extends AmsClassePlannedAttendanceFilter{
    function __construct(Container $container) {
            parent::__construct($container);
			$this->setEnrolroleshortname('studentcorporate');
			$this->setEnrolstatusshortname('activeenrol');
              } 
    
  
}
