<?php

namespace Badiu\Tms\CoreBundle\Model\Offer\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\OfferBundle\Model\Lib\EnrolSelectionIntegrationFilter;
class OfferEnrolSelectionIntegrationFilter extends EnrolSelectionIntegrationFilter{
    private $modulekey;

    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
        
   public function execAfterSubmit() {
	   $this->setModulekey('badiu.tms.offer.offer');
	    $id=  $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
	  
	   //get name
	  // $name=$this->getModulekey().'.data/'.$id;
	  
	   $data=$this->getContainer()->get($this->getModulekey().'.data');
	   $paname=$data->getNameParent($id);
	   $offername=$this->getUtildata()->getVaueOfArray($paname, 'name');
	  // $coursename=$this->getUtildata()->getVaueOfArray($paname, 'coursename');
	  // if(!empty($coursename) && !empty($offername)){$name= $coursename."/".$offername;}
	  // else  if(empty($coursename) && !empty($offername)){$name= $offername;}
		$name= $offername; 
	   $this->setName($name); 
	  
	   parent::execAfterSubmit();
	  
   } 
   
}
