<?php

namespace Badiu\Tms\CoreBundle\Model\Offer\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class OfferDisciplines extends BadiuModelLib {

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
	   // /system/service/process?_service=badiu.tms.offer.offer.lib.disciplines&_function=teste
  /*  public function  teste(){
		
		$fparam=array('offerid'=>99);
		$list=$this->getDisciplines($fparam);
		$tbl=$this->getDisciplinesTableFormat($list);
		echo $tbl;exit;
	}*/
	public function getDisciplines($param) {
		 $offerid=$this->getUtildata()->getVaueOfArray($param,'offerid');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 
		 if(empty($entity)){$entity=$this->getEntity();}
		 $edata=$this->getContainer()->get('badiu.ams.offer.offer.data');
		 $sql="SELECT d.id,o.drequired,d.name,d.dhour,d.shortname  FROM BadiuAmsOfferBundle:AmsOfferDiscipline o JOIN o.disciplineid d WHERE  o.entity=:entity AND o.offerid=:offerid ";

		$query = $edata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('offerid', $offerid);
		$result = $query->getResult();
		return $result; 
		  
	}

	public function getDisciplinesTableFormat($list) {
		if(!is_array($list)){return null;}
		$configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
		
		$html=''; 
		$html.="<table id=\"trialcourse\" class=\"table\">
				<tr>
					<th>Código</th>
					<th>Curso</th>
					<th>Carga horária</th>
					<th>Obrigatório</th>
				</tr>";
		$notenrolled=$this->getTranslator()->trans('badiu.tms.enrol.notenrol');
		foreach ($list as $row) {
			$shortname=$this->getUtildata()->getVaueOfArray($row,'shortname');
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			$dhour=$this->getUtildata()->getVaueOfArray($row,'dhour');
			$dhour=$configformat->textHourRound($dhour);
			
			$drequired=$this->getUtildata()->getVaueOfArray($row,'drequired');
			$drequired=$configformat->defaultBoolean($drequired);
		
		$html.="
			<tr>
				<td>$shortname</td>
				<td>$name</td>
				<td>$dhour</td>
				<td>$drequired</td>
		
			</tr>";
		}
		$html.="</table>";
		return $html;
	

	}		
	
}
