<?php

namespace Badiu\Tms\CoreBundle\Model\Offer\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\OfferBundle\Model\Lib\EnrolSelectionIntegrationFilter as AmsEnrolSelectionIntegrationFilter;
class VDisciplineEnrolSelectionConfigFilter extends AmsEnrolSelectionIntegrationFilter{
    private $modulekey;

    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
 
	   public function execAfterSubmit(){
	 	  
		} 
		
		 public function changeParamOnOpenEdit($param,$modulekey){
			
			 $dconfig = $this->getUtildata()->getVaueOfArray($param, 'dconfig');
			
             if(!is_array($dconfig)){$dconfig=$this->getJson()->decode($dconfig, true);}
           $rdata=$this->getUtildata()->getVaueOfArray($dconfig, 'enrolselection');
		
			$param['enrolrequesttimestart']=$this->getUtildata()->getVaueOfArray($rdata, 'requesttimestart');
			$param['enrolrequesttimeend']=$this->getUtildata()->getVaueOfArray($rdata, 'requesttimeend');
			$param['enrolanalysiscriteria']=$this->getUtildata()->getVaueOfArray($rdata, 'analysiscriteria');
			$param['enrolservicecheckcriteria']=$this->getUtildata()->getVaueOfArray($rdata, 'servicecheckcriteria');
			$param['listdocumentnumber']=$this->getUtildata()->getVaueOfArray($rdata, 'listdocumentnumber');
			$param['defaultstatus']=$this->getUtildata()->getVaueOfArray($rdata, 'defaultstatus');
			$param['defaultrole']=$this->getUtildata()->getVaueOfArray($rdata, 'defaultrole');
			return $param;
	}	
}

