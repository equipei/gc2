<?php
namespace Badiu\Tms\CoreBundle\Model\Offer\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class DisciplineEnrolSelectionConfigFilter extends EnrolSelectionIntegrationFilter{
    private $modulekey;

    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
        
   public function execAfterSubmit() {
	  
	   $this->setModulekey('badiu.ams.offer.discipline');
	    $id=  $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
	   
	   //get name
	   $name=$this->getModulekey().'.data/'.$id;
	  
	   $data=$this->getContainer()->get($this->getModulekey().'.data');
	   
	   $paname=$data->getNameParent($id);
	 
	   $disciplinename=$this->getUtildata()->getVaueOfArray($paname, 'name');
	   $offername=$this->getUtildata()->getVaueOfArray($paname, 'offername');
	   $coursename=$this->getUtildata()->getVaueOfArray($paname, 'coursename');
	   if(!empty($coursename) && !empty($offername)){$name= $coursename."/".$offername;}
	   else  if(empty($coursename) && !empty($offername)){$name= $offername;}

		$name.="/".$disciplinename;
	   $this->setName($name); 
	  
	   parent::execAfterSubmit();
	  
   } 
   
}
