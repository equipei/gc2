<?php

namespace Badiu\Tms\CoreBundle\Model\Offer;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class ClasseFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
   
    public  function orderResult(){
        $list=array();
        $list['date']='';
		$list['name']='';
        return $list;
    }

}
