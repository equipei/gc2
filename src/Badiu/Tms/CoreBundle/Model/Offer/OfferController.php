<?php

namespace Badiu\Tms\CoreBundle\Model\Offer;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Badiu\Ams\CurriculumBundle\Model\DOMAINTABLE;
use Badiu\Ams\OfferBundle\Model\OfferController as AmsOfferController;
class OfferController extends AmsOfferController{
    
     function __construct(Container $container) {
            parent::__construct($container);
       }
    

    public function save($data) {
		$result=$data->save();
		$this->copyCurriculum($data->getDto());
		$enrolofferdeptlib=$this->getContainer()->get('badiu.tms.enrol.lib.syncofferdepartament');
		$enrolofferdeptlib->enrol($data->getDto()->getId());
		$enroldisciplinedeptlib=$this->getContainer()->get('badiu.tms.enrol.lib.syncdisciplinedepartament');
		$enroldisciplinedeptlib->enrolOffertedDiscipline($data->getDto()->getId());
        return $data->getDto();
    }

}
