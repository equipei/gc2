<?php
namespace Badiu\Tms\CoreBundle\Model\My\Student;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ClasseReportController extends BadiuReportController {

	function __construct(Container $container) {
		parent::__construct($container);
	}
 
  public function exec() {
    
    $param=$this->getFparam();
    $param['timenow1']=new \DateTime();
    $param['timenow2']=new \DateTime();
	$this->setFparam($param);
  
    return null;
  }


}
