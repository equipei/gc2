<?php

namespace Badiu\Tms\CoreBundle\Model\Offer;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class VDisciplineFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     public function changeParamOnOpen() {
			$this->getFieldProcessOnOpen();
		 $param = $this->getParam();
		
            $this->inheritDconfig();
            $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
            $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $param = $this->getParam();
            $lmsintegration = $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');

            $param['sserviceid'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'sserviceid');
            $param['lmssynctype'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynctype');
            $param['lmssynclevel'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynclevel');
            $param['lmscoursecat'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatid');
            $param['lmscoursecatparent'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatparent');
            $param['lmscourse'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseid');
            $param['lmscoursegroup'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursegroupid');
            $param['lmscourseidtemplate'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseidtemplate');
			
			
            $param['productintegration'] = $this->getUtildata()->getVaueOfArray($dconfig, 'financintegration.productintegration',true);
			//$param=$this->getContainer()->get('badiu.ams.core.lib.roleconclusionform')->disciplineFormChangeParamOnOpen($param);
			if($this->isEdit()){$param=$this->getContainer()->get('badiu.tms.offer.vdiscipline.lib.enrolselectionfilter')->changeParamOnOpenEdit($param,'badiu.tms.offer.vdiscipline');}
			else{$param=$this->getContainer()->get('badiu.tms.core.lib.lmsmoodlesyncdefaultform')->defaultChangeParamOnEdit($param);}
			
            $this->setParam($param); 
			
     }   
     
    public function changeParam() {
		
		
		//$this->addParamItem('disciplineid',$parentid);
		
		$param = $this->getParam();
		
        //get name of discipline
        $disciplineid= $this->getParamItem('disciplineid');
        $disciplinename= $this->getParamItem('disciplinename');
        if(empty($disciplinename)){
            $ddata=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $entity = $badiuSession->get()->getEntity();
            $dname=$ddata->getColumnValue($entity,'name',$paramf=array('id'=>$disciplineid));
            $this->addParamItem('disciplinename',$dname);
         }
        
        $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
        if (empty($dconfig) || !is_array($dconfig)) {
            $dconfig = array();
        }
		 
		 //offerid
		 if(!$this->isEdit()){
			 $offerid=$this->getContainer()->get('badiu.ams.offer.offer.data')->getIdByShortname($this->getEntity(),'tmsdefaultcourseoffer'); 
			 $param['offerid'] = $offerid;
		 }
		  
		 $param['dconfig'] = $dconfig;
		$param=$this->getContainer()->get('badiu.ams.core.lib.roleconclusionform')->disciplineFormChangeParam($param);
		$dconfig = $this->getUtildata()->getVaueOfArray($param, 'dconfig');
        $dconfig = $this->getJson()->encode($dconfig);
       
		
        $param['dconfig'] = $dconfig;
		
        $this->setParam($param);
        
     }
     
      public function validation() {
        
        $isvalid = $this->isVersionValid();
        if ($isvalid != null) {
            return $isvalid;
        }
	  }
	  
	   public function isVersionValid() {
			$offerid=$this->getContainer()->get('badiu.ams.offer.offer.data')->getIdByShortname($this->getEntity(),'tmsdefaultcourseoffer'); 
			$param['offerid'] = $offerid;	
			$entity=$this->getEntity();
			$disciplineid= $this->getParamItem('disciplineid');
			$fversion= $this->getParamItem('fversion');
			
			$odisciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			$fapram=array('offerid'=>$offerid,'disciplineid'=>$disciplineid,'fversion'=>$fversion,'entity'=>$entity);
			$exitversion=$odisciplinedata->countGlobalRow($fapram);
			//print_r($fapram);
			//$checkexist=false;
			
			if(!$exitversion){$checkexist=false;}
			if($exitversion==1 && $this->isEdit()){$checkexist=false;}
			if($exitversion > 1 && $this->isEdit()){$checkexist=true;}
			if($exitversion && !$this->isEdit()){$checkexist=true;}
          if ($checkexist) {
                $message=array();
                 $message['generalerror'] = $this->getTranslator()->trans('badiu.tms.offer.vdiscipline.fversion.duplication');
                $message['fversion']=$this->getTranslator()->trans('badiu.tms.offer.vdiscipline.fversion.justexist');
                $info = 'badiu.tms.offer.vdiscipline.fversion.duplication';
                return $this->getResponse()->denied($info, $message);
            }
			return null;
    }
         
     public function inheritDconfig() {
          if($this->isEdit()){return null;}
          $dconfig=null;
           $param = $this->getParam();
		   $badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			
			$classestatus=$badiuSession->getValue('badiu.tms.offer.classestatus.param.config.default');
			$classestatusid=$this->getContainer()->get('badiu.tms.offer.classestatus.data')->getIdByShortname($this->getEntity(),$classestatus); 

			$type=$badiuSession->getValue('badiu.tms.offer.type.param.config.default');
			$typeid=$this->getContainer()->get('badiu.tms.offer.type.data')->getIdByShortname($this->getEntity(),$type); 

			$typemanager=$badiuSession->getValue('badiu.tms.offer.typemanager.param.config.default');
			$typemanagerid=$this->getContainer()->get('badiu.tms.offer.typemanager.data')->getIdByShortname($this->getEntity(),$typemanager); 

			$typeaccess=$badiuSession->getValue('badiu.tms.offer.typeaccess.param.config.default');
			$typeaccessid=$this->getContainer()->get('badiu.tms.offer.typeaccess.data')->getIdByShortname($this->getEntity(),$typeaccess); 

					
			
			
			$param['typeid']=$typeid;
			$param['typemanagerid']=$typemanagerid;
			$param['typeaccessid']=$typeaccessid;
			
			//enrolconfig
			$enroldatetype=$badiuSession->getValue('badiu.tms.offer.discipline.enroldatetype.param.config.default');
			$enroldatedynamic=$badiuSession->getValue('badiu.tms.offer.discipline.enroldatedynamic.param.config.default');
			$enrolreplicate=$badiuSession->getValue('badiu.tms.offer.discipline.enrolreplicate.param.config.default');
			$enrolreplicateupadate=$badiuSession->getValue('badiu.tms.offer.discipline.enrolreplicateupadate.param.config.default');
			$param['enroldatetype']=$enroldatetype;
			$param['enroldatedynamic']=$enroldatedynamic;
			$param['enrolreplicate']=$enrolreplicate;
			$param['enrolreplicateupadate']=$enrolreplicateupadate;
	
			//processenrol
			$enroltype=$badiuSession->getValue('badiu.tms.offer.enrol.type.param.config.default');
			
			$param['enroltype']=$enroltype;
			

			//rolecompletation
			$roleconclusionenable=$badiuSession->getValue('badiu.tms.offer.enrol.roleconclusionenable.param.config.default');
			$roleconclusiondhour=$badiuSession->getValue('badiu.tms.offer.enrol.roleconclusiondhour.param.config.default');
			$roleconclusiongrade=$badiuSession->getValue('badiu.tms.offer.enrol.roleconclusiongrade.param.config.default');
			$lmscoursecompletation=$badiuSession->getValue('badiu.tms.offer.enrol.lmscoursecompletation.param.config.default');
			$roleconclusiontypeoperation=$badiuSession->getValue('badiu.tms.offer.enrol.roleconclusiontypeoperation.param.config.default');

			$param['roleconclusionenable']=$roleconclusionenable;
			$param['roleconclusiondhour']=$roleconclusiondhour;
			$param['roleconclusiongrade']=$roleconclusiongrade;
			$param['lmscoursecompletation']=$lmscoursecompletation;
			$param['roleconclusiontypeoperation']=$roleconclusiontypeoperation;
			$param=$this->getContainer()->get('badiu.ams.core.lib.roleconclusionform')->disciplineFormChangeParam($param);
			$dconfig=$this->getUtildata()->getVaueOfArray($param, 'dconfig');
			//$param['dconfig']=$this->getJson()->encode($dconfig);

			//certificate
			$certificateupdateuser=$badiuSession->getValue('badiu.ams.classe.enrol.certificateupdateuser.param.config.default');
			$certificateupdateenrol=$badiuSession->getValue('badiu.ams.classe.enrol.certificateupdateenrol.param.config.default');$certificateupdateuser=$badiuSession->getValue('badiu.ams.classe.enrol.certificateupdateuser.param.config.default');
			$certificateupdateobjectinfo=$badiuSession->getValue('badiu.ams.classe.enrol.certificateupdateobjectinfo.param.config.default');
			$certificateupdateobjectdata=$badiuSession->getValue('badiu.ams.classe.enrol.certificateupdateobjectdata.param.config.default');
			
			$param['certificateupdateuser']=$certificateupdateuser;
			$param['certificateupdateenrol']=$certificateupdateenrol;
			$param['certificateupdateobjectinfo']=$certificateupdateobjectinfo;
			$param['certificateupdateobjectdata']=$certificateupdateobjectdata;
			
			//next 
			$param['fversion']=$this->nextVersion();
			$param['equivalencereqminfversion']=1;
			
			//sync lms
			$entity=$this->getEntity();
			$disciplineid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
			$categoryinfo=$this->getContainer()->get('badiu.tms.discipline.discipline.data')->getCategoryInfo($entity,$disciplineid);
			
			$ctdconfig=$this->getUtildata()->getVaueOfArray($categoryinfo, 'dconfig');
			$ctdconfig = $this->getJson()->decode($ctdconfig,true);
	
			$defaultforceupdate=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.forceupdate');
			//add config on dconfig of discipline offer
			$lmsintegration['status']='pending';
			$lmsintegration['sserviceid']=$this->getUtildata()->getVaueOfArray($ctdconfig, 'lmsintegration.sserviceid',true);
			$lmsintegration['lmssynctype']='replicationdatainlms';
			$lmsintegration['lmssynclevel']='coursecat';
			$lmsintegration['lmscoursecatparent']=$this->getUtildata()->getVaueOfArray($ctdconfig, 'lmsintegration.lmscoursecatid',true);
			$lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($ctdconfig, 'lmsintegration.lmscoursecatname',true);
			$lmsintegration['lmssyncforceupdate']=$defaultforceupdate;
			$dconfig['lmsintegration']=$lmsintegration;
			//$dconfig = $this->getJson()->encode($dconfig);
			$param['dconfig']=$this->getJson()->encode($dconfig);
			
		 
			//print_r($categoryinfo);exit;
			//get category id by disciplineid
			
			
           $this->setParam($param);
     }
       

	public function nextVersion() {
			$offerid=$this->getContainer()->get('badiu.ams.offer.offer.data')->getIdByShortname($this->getEntity(),'tmsdefaultcourseoffer'); 
			$param['offerid'] = $offerid;	
			$disciplineid= $this->getParamItem('disciplineid');
			
			$odisciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			$fapram=array('offerid'=>$offerid,'disciplineid'=>$disciplineid);
			$nextverion=$odisciplinedata->getMaxGlobalColumnValue('fversion',$param);
			if(empty($nextverion)){$nextverion=1;}
			else{$nextverion++;}
			return $nextverion;
	}
   
    
}
