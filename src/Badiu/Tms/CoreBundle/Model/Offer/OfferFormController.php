<?php
namespace Badiu\Tms\CoreBundle\Model\Offer;
use Badiu\Ams\CourseBundle\Model\OfferFormController as AmsOfferFormController;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\CoreBundle\Model\DOMAINTABLE;

class OfferFormController extends AmsOfferFormController {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function changeParamOnOpen() {
			parent::changeParamOnOpen();
			 
             if($this->isEdit()){
				 $param = $this->getParam();
				 $param=$this->getContainer()->get('badiu.ams.offer.lib.enrolselectionfilter')->changeParamOnOpenEdit($param,'badiu.tms.offer.offer');
				 $this->setParam($param); 
				}
			
            
       
    }


    

}
