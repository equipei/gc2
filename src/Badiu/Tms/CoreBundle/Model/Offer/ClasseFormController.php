<?php
namespace Badiu\Tms\CoreBundle\Model\Offer;
use Badiu\Ams\OfferBundle\Model\Discipline\ClasseFormController as AmsClasseFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ClasseFormController extends AmsClasseFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     public function changeParamOnOpen() {
		 parent::changeParamOnOpen();
		 if($this->isEdit()){
			  $param = $this->getParam();
			  $param=$this->getContainer()->get('badiu.ams.offer.lib.enrolselectionfilter')->changeParamOnOpenEdit($param,'badiu.tms.offer.classe');
			  $this->setParam($param); 
			 
		  }
		  $this->inheritDisciplineSelectionConfig();
            
     }   
 
 
  public function inheritDisciplineSelectionConfig(){
			 if($this->isEdit()){return null;}
			 
			  $entity= $this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
			  $disciplineid= $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
			 if(empty($disciplineid)){return null;}
			
			 $sdata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
             $dconfig=$sdata->getColumnValue($entity,'dconfig',$paramf=array('id'=>$disciplineid));
			
             if(!is_array($dconfig)){$dconfig=$this->getJson()->decode($dconfig, true);}
             $rdata=$this->getUtildata()->getVaueOfArray($dconfig, 'enrolselection');
		   
			$formcastdate=$this->getContainer()->get('badiu.system.core.lib.form.castdate');
		    $requesttimestart=$this->getUtildata()->getVaueOfArray($rdata, 'requesttimestart');
			if(!empty($requesttimestart)){
				$now1=new \DateTime();
				$now1->setTimestamp($requesttimestart);
				$requesttimestart= $formcastdate->convertToForm($now1);
			}
			
		    $requesttimeend= $this->getUtildata()->getVaueOfArray($rdata, 'requesttimeend');
			if(!empty($requesttimeend)){
				$now2=new \DateTime();
				$now2->setTimestamp($requesttimeend);
				$requesttimeend= $formcastdate->convertToForm($now2);
			}
			
			$param = $this->getParam();
			$param['enrolrequesttimestart']=$requesttimestart;
			$param['enrolrequesttimeend']=$requesttimeend;
			$param['enrolanalysiscriteria']=$this->getUtildata()->getVaueOfArray($rdata, 'analysiscriteria');
			$param['enrolservicecheckcriteria']=$this->getUtildata()->getVaueOfArray($rdata, 'servicecheckcriteria');
			$param['listdocumentnumber']=$this->getUtildata()->getVaueOfArray($rdata, 'listdocumentnumber');
			$param['defaultstatus']=$this->getUtildata()->getVaueOfArray($rdata, 'defaultstatus');
			$param['defaultrole']=$this->getUtildata()->getVaueOfArray($rdata, 'defaultrole');
			$this->setParam($param);
			
			return $param;
			
	}	
         
    
}
