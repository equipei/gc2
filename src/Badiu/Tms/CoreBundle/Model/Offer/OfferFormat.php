<?php

namespace Badiu\Tms\CoreBundle\Model\Offer;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;

class OfferFormat extends  BadiuFormat {

    private $urlbaseimg="";
	private $lmscoremoodleformat;
	private $permission;
	private $baseresoursepath;
	private $sessionuserid;
    function __construct(Container $container) {
        parent::__construct($container);
        $this->urlbase=$this->getUtilapp()->getBaseUrl();
		$this->urlbase.="/system/file/get/";
		$this->lmscoremoodleformat=$this->getContainer()->get('badiu.ams.core.lmsmoodle.format');
		$this->permission=$this->getContainer()->get('badiu.system.access.permission');
		$this->baseresoursepath= $this->getContainer()->get('templating.helper.assets')->getUrl('bundles/badiuthemecore');
   }
   public function defaultimage($data) {
        $result = null;
        $defaultimage=$this->getUtildata()->getVaueOfArray($data,'defaultimage'); 
		$disciplinedefaultimage=$this->getUtildata()->getVaueOfArray($data,'disciplinedefaultimage'); 
       
		if(!empty($defaultimage)){
			$result= $this->urlbase.$defaultimage;
		}else {$result=$this->baseresoursepath."/tms/images/default-card.jpg";}
		
		return $result;
    }
	
	public function dhour($data) {
			$value="";
			
			$labelmin=$this->getTranslator()->trans('badiu.system.time.minute.short');
			$labelhour=$this->getTranslator()->trans('badiu.system.time.hour.short');
			$value=$this->getUtildata()->getVaueOfArray($data,'disciplinehour'); 
			 if (empty($value)){$value=$this->getUtildata()->getVaueOfArray($data,'dhour'); }
            if (empty($value)){return null;}
		
            if($value > 0 && $value < 60) {$value=$value." $labelmin";}  
			else if($value >= 60) {
					$value=$value/60;
					$value=$value." $labelhour";
			}  
			
          return $value; 
		}
	
	public  function requestenrolurl($data){ 

			$url = null;
			$skey =$this->getUtildata()->getVaueOfArray($data,'enrolselectionskey');
			$enrolselectionanalysiscriteria =$this->getUtildata()->getVaueOfArray($data,'enrolselectionanalysiscriteria');
			if(empty($enrolselectionanalysiscriteria)){return  null;}
			if(!empty($skey)){$url=$this->getUtilapp()->getUrlByRoute('badiu.admin.selection.requestenroll.link',array('parentid'=>$skey));}
			return $url;
		}
		
	public  function progress($data){ 
	
			$url = null;
			$countdiscipline =$this->getUtildata()->getVaueOfArray($data,'countdiscipline');
			$countdisciplinecompleted =$this->getUtildata()->getVaueOfArray($data,'countdisciplinecompleted');
			$statusshortname =$this->getUtildata()->getVaueOfArray($data,'statusshortname');
			
			$x=$countdisciplinecompleted;
			$y=$countdiscipline;
			$progress=0;
			if($x!=null && $y!=null){
				if($y >=0 && $x >=0 && $y>= $x){
					if($y==0){$progress=0;} 
					 else{
						 $progress= $x*100/$y;
					}
				}					
			}
			
			//if(empty($progress)){return null;}
			
			 $profressformat = number_format($progress, 2, ',', '.');
			if($statusshortname=='approvedenrol'){$profressformat=100;$progress=100;} 
			$html="
			<p class=\"text-progresso mb-1\">$profressformat% concluído</p>
             <div class=\"progress mb-1\" style=\"height: 4px;\">
               <div class=\"progress-bar bg-progress\" role=\"progressbar\" style=\"width: $progress%\" aria-valuenow=\"$progress\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
             </div>";
			return  $html;
		} 
		
			
	 public  function certificateurl($data){ 
			
			$skeycertificate =$this->getUtildata()->getVaueOfArray($data,'skeycertificate');
			
			$enablecertificate =$this->getUtildata()->getVaueOfArray($data,'enablecertificate');
			if(empty($skeycertificate) && !$enablecertificate){return null;}
			
			$certificatetempleid =$this->getUtildata()->getVaueOfArray($data,'certificatetempleid');
			if(empty($skeycertificate) && empty($certificatetempleid)){return null;}
			
			$statusshortname =$this->getUtildata()->getVaueOfArray($data,'statusshortname');
			if(empty($skeycertificate) && $statusshortname!='approvedenrol'){return null;}
			
			$certificatetimecreated=$this->getUtildata()->getVaueOfArray($data,'certificatetimecreated');
			$enroltimefinish=$this->getUtildata()->getVaueOfArray($data,'enroltimefinish');
			
			$enrolid =$this->getUtildata()->getVaueOfArray($data,'enrolid');
			
			$generatenewcertificateversion=false;
			if (!is_a($certificatetimecreated, 'DateTime') && is_string($certificatetimecreated)) {
				$certificatetimecreated = \DateTime::createFromFormat('Y-m-d H:i:s', $certificatetimecreated);
			}
			
			if (!is_a($enroltimefinish, 'DateTime') && is_string($enroltimefinish)) {
				$enroltimefinish = \DateTime::createFromFormat('Y-m-d H:i:s', $enroltimefinish);
			}
			
			if (is_a($certificatetimecreated, 'DateTime') && is_a($enroltimefinish, 'DateTime')) {
				if($enroltimefinish->getTimestamp() > $certificatetimecreated->getTimestamp()){$generatenewcertificateversion=true;}
			}
			
			$fparam=array();
			if(empty($skeycertificate)){
				$moduleinstance =$this->getUtildata()->getVaueOfArray($data,'id');
				$userid=$this->getSessionuserid();
				$fparam=array('modulekey'=>'badiu.ams.enrol.offer','moduleinstance'=>$moduleinstance,'userid'=>$userid,'_operation'=>'create','skey'=>$skeycertificate,'enrolid'=>$enrolid);
			}else{
				$fparam=array('skey'=>$skeycertificate,'_operation'=>'view');
			}
			
			if($generatenewcertificateversion){
				$moduleinstance =$this->getUtildata()->getVaueOfArray($data,'id');
				$userid=$this->getSessionuserid();
				$fparam=array('modulekey'=>'badiu.ams.enrol.offer','moduleinstance'=>$moduleinstance,'userid'=>$userid,'_operation'=>'createnewversion','skey'=>$skeycertificate,'enrolid'=>$enrolid);
			}
			$url=$this->getUtilapp()->getUrlByRoute('badiu.admin.certificate.request.link',$fparam);
			return  $url;
		} 		
	
 public  function review($data){ 

			$statusshortname =$this->getUtildata()->getVaueOfArray($data,'statusshortname');
			if($statusshortname!='approvedenrol'){return null;}

			$timefinish =$this->getUtildata()->getVaueOfArray($data,'enroltimefinish');
			if(empty($timefinish)){return null;}
				
			$timelastdisciplineadd=$this->getUtildata()->getVaueOfArray($data,'timelastdisciplineadd');
			if (!is_a($timelastdisciplineadd, 'DateTime') && is_string($timelastdisciplineadd)) {
				$timelastdisciplineadd = \DateTime::createFromFormat('Y-m-d H:i:s', $timelastdisciplineadd);
			}else {return null;}
			if(empty($timelastdisciplineadd)){return null;}
				
			if($timelastdisciplineadd->getTimestamp() > $timefinish->getTimestamp()){
				return 1;
			}
		
			return  null;
		} 			
	
		public  function updateinfo($data){
				$info=null;			
				$review=$this->review($data);
				if($review){
					$timelastdisciplineadd=$this->getUtildata()->getVaueOfArray($data,'timelastdisciplineadd');
					$timelastdisciplineadd = \DateTime::createFromFormat('Y-m-d H:i:s', $timelastdisciplineadd);
					$dateup=$timelastdisciplineadd->format('d/m/Y H:i:s');
					$info=$this->getTranslator()->trans('badiu.tms.offer.offer.infoupdate',array('%timeupdate%'=>$dateup));
				}
			return $info;
		} 	
		
	public  function status($data){ 
	
			$url = null;
			$statusname =$this->getUtildata()->getVaueOfArray($data,'statusname');
			
			if(empty($statusname)){return null;}
			
			$html="Status: $statusname";
			return  $html;
		}		
	
	
	public  function offerurledit($data){ 
			
			$permissionedit=$this->permission->has_access('badiu.tms.offer.offer.edit',$this->getSessionhashkey());

			if(!$permissionedit){return null;}
			
			$id =$this->getUtildata()->getVaueOfArray($data,'id');
			$urlgoback=$this->getUtilapp()->getCurrentUrl();
			$parammage=array('id'=>$id=$id,'parentid'=>$id,'_urlgoback'=>$urlgoback);
			$urlgoback= str_replace('/system/service/process', '/tms/my/student/fview/default/index', $urlgoback);
			$offerediturl=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.offer.edit',$parammage);
			if(empty($offerediturl)){return  null;}
			
			return  $offerediturl;
		} 	
		
	
		
		public  function offerurlview($data){ 
			
			$permissionedit=$this->permission->has_access('badiu.tms.offer.discipline.index',$this->getSessionhashkey());

			if(!$permissionedit){return null;}
			
			$id =$this->getUtildata()->getVaueOfArray($data,'id');
			$urlgoback=$this->getUtilapp()->getCurrentUrl();
			$urlgoback= str_replace('/system/service/process', '/tms/my/student/fview/default/index', $urlgoback);
			$parammage=array('parentid'=>$id);
			$url=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.discipline.index',$parammage);
			if(empty($url)){return  null;}
			return  $url;
		}
		
		
	public  function disciplineurlview($data){  
			
			$permissionaccess=$this->permission->has_access('badiu.tms.offer.classe.index',$this->getSessionhashkey());

			if(!$permissionaccess){return null;}
			
			$odisciplineid =$this->getUtildata()->getVaueOfArray($data,'odisciplineid');
			$parammage=array('parentid'=>$odisciplineid);
			$disciplineurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.classe.index',$parammage);
			if(empty($disciplineurl)){return  null;} 
			return  $disciplineurl;
		}		
	public  function disciplineurledit($data){  
			
			return  null;
		}

	public  function disciplinecategoryurlview($data){  
			
			return  null;
		}
	public  function disciplinecategoryurledit($data){  
			
			return  null;
		}		

	

		
	public  function description($data){ 
			$description =$this->getUtildata()->getVaueOfArray($data,'description');
			//$description=mb_strimwidth($description, 0, 350, "...");
			return  $description;
		} 
	public  function coursecategorydescription($data){ 
			$description =$this->getUtildata()->getVaueOfArray($data,'description');
			//$description=mb_strimwidth($description, 0, 80, "...");
			return  $description;
		} 		

	public  function disciplinestable($data){ 
			$id =$this->getUtildata()->getVaueOfArray($data,'id');
			$tbldisciplines=$this->getContainer()->get('badiu.tms.offer.offer.lib.disciplines');
			$fparam=array('offerid'=>$id);
			$list=$tbldisciplines->getDisciplines($fparam);
			$tbl=$tbldisciplines->getDisciplinesTableFormat($list);
			return  $tbl;
		} 
	public  function enroldisciplinestable($data){ 
			$id =$this->getUtildata()->getVaueOfArray($data,'id');
			$userid=$this->getSessionuserid();
			$tbldisciplines=$this->getContainer()->get('badiu.tms.enrol.offer.lib.disciplineuser');
			$fparam=array('offerid'=>$id,'userid'=>$userid);
			$list=$tbldisciplines->getDisciplines($fparam);
			$tbl=$tbldisciplines->getDisciplinesTableFormat($list);
			return  $tbl;
		} 
	 public function getSessionuserid() {
		 if(!empty($this->sessionuserid)){return $this->sessionuserid;}
		 $badiuSession = $this->getContainer()->get('badiu.system.access.session');
         $badiuSession->setHashkey($this->getSessionhashkey());
		 $this->sessionuserid=$badiuSession->get()->getUser()->getId();
		 return $this->sessionuserid;
	 }
	 
	 public  function myenrolmanage($data){ 
			$html="";
			
			$enrolid =$this->getUtildata()->getVaueOfArray($data,'enrolid');
			$fparam=array('id'=>$enrolid,'parentid'=>$enrolid);
			$detailurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.my.studentofferenrol.view',$fparam);
			
			if(!empty($detailurl)){$html.="<a href=\"$detailurl\">Ver detalhes</a><br />";}
			
			return  $html;
		}

	public  function myavailablemanage($data){ 
			$requesturl=$this->requestenrolurl($data);
			$requestlink="";
			if(!empty($requesturl)){$requestlink="<a href=\"$requesturl\">Solicitar inscrição</a>";}
			return  $requestlink;
		}
		
		public  function availablemanage($data){ 
			$html="";
			$requesturl=$this->requestenrolurl($data);
			if(!empty($requesturl)){$html.="<a href=\"$requesturl\">Solicitar inscrição</a><br />";}
			
			/*$classeid =$this->getUtildata()->getVaueOfArray($data,'id');
			$fparam=array('id'=>$classeid,'parentid'=>$classeid);
			$detailurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.my.studentofferavailable.view',$fparam);
			*/
			if(!empty($detailurl)){$html.="<a href=\"$detailurl\">Ver detalhes</a><br />";}
			return  $html;
		}
		
		public  function cancancelenrol($data){ 
		
			$result=false;
			$statusshortname =$this->getUtildata()->getVaueOfArray($data,'statusshortname');
			$timestart =$this->getUtildata()->getVaueOfArray($data,'timestart');

			if($statusshortname=='preregistrationenrol'  || $statusshortname=='activeenrol'){
				$badiuSession = $this->getContainer()->get('badiu.system.access.session');
				$classestudentllimitminuteaftertimestart=$badiuSession->getValue('badiu.tms.enrol.cancel.param.config.classestudentllimitminuteaftertimestart');
				//echo $classestudentllimitminuteaftertimestart;exit;
				if(empty($classestudentllimitminuteaftertimestart)){return $result;}
				
				if (!is_a($timestart, 'DateTime') && is_string($timestart)) {
					$timestart = \DateTime::createFromFormat('Y-m-d H:i:s', $timestart);
				}
			    if(empty($timestart)){return null;}
				$now=new \DateTime();
					$lessdaylimit=$classestudentllimitminuteaftertimestart*60;
					
					$limitocancel=$timestart->getTimestamp()+$lessdaylimit;
				
					if($limitocancel > $now->getTimestamp() ){$result=true;}
			}
			return $result;
		}
}
