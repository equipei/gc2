<?php

namespace Badiu\Tms\CoreBundle\Model\Offer;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\OfferBundle\Model\DisciplineFormat as AmsDisciplineFormat;
class DisciplineFormat extends AmsDisciplineFormat{
     
     function __construct(Container $container) {
            parent::__construct($container);
       } 

       public  function classelinkemanager($data){
            $value="";
            $id=$this->getUtildata()->getVaueOfArray($data,'id');
            if(empty($id)){return null;}
			$labelmanageclasselink=$this->getTranslator()->trans('badiu.tms.discipline.discipline.manageclasselink');
            $labeladdclasselink=$this->getTranslator()->trans('badiu.tms.discipline.discipline.addclasselink');
           
            $parammage=array('parentid'=>$id);
            $manageclasseurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.classe.index',$parammage);
            $addclasseurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.classe.add',$parammage);
            $manageclasselink ="<a href=\"$manageclasseurl\">$labelmanageclasselink</a>";
            $addclasselink ="<a href=\"$addclasseurl\">$labeladdclasselink</a>";

            $value="$manageclasselink <br />$addclasselink";
           // $manageclasselink  badiu.tms.offer.classe.index dconfig.offer.discipline.lastid AS parentid  
        return $value; 
    } 
    
}
