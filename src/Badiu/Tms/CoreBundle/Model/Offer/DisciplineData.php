<?php
namespace Badiu\Tms\CoreBundle\Model\Offer;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\OfferBundle\Model\DisciplineData as AmsDisciplineData;
class DisciplineData extends AmsDisciplineData {
 
    function __construct(Container $container, $bundleEntity) {
        parent::__construct($container, $bundleEntity);
    }
	
	/**
	* The classe search discipline in autocomplete field to create new classe
	*/
	
	 public function getDisciplineInDefaultOfferForAutocomplete() {
        
        $name=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
		$categoryid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('categoryid'); 
        if(empty($name)) return null;

        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
        $wsql="";
        if(!empty($name)){$wsql=" AND CONCAT(d.id,LOWER(o.disciplinename),LOWER(d.name))  LIKE :name ";}
		 if(!empty($categoryid)){$wsql.=" AND d.categoryid =:categoryid ";}
        $sql="SELECT  o.id,d.name AS name  FROM ".$this->getBundleEntity()." o JOIN o.offerid f JOIN f.courseid  c JOIN o.disciplineid d WHERE  o.entity = :entity AND f.shortname=:offershortname AND c.dtype=:dtype $wsql  ";
       //CONCAT(o.fversion,' / ',o.disciplinename)
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('offershortname','tmsdefaultcourseoffer');
		$query->setParameter('dtype','training');
        if(!empty($name)){$query->setParameter('name','%'.strtolower($name).'%');}
		if(!empty($categoryid)){$query->setParameter('categoryid',$categoryid);}
        $result= $query->getResult();
        return  $result;
        
    }
	
	 //get discipline without current discipline in offer
    public function getNewsDisciplines() {
        
        $offerid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
        $name=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
        if(empty($offerid)) return null;

        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $disciplineData=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
      
        $wsql="";
        if(!empty($name)){$wsql=" AND CONCAT(o.id,LOWER(o.name))  LIKE :name ";}
        $sql="SELECT  o.id,o.name  FROM ".$disciplineData->getBundleEntity()." o  WHERE  o.entity = :entity AND o.dtype=:dtype $wsql AND o.id NOT IN (SELECT  d.id FROM " . $this->getBundleEntity() . " od JOIN od.disciplineid d   WHERE od.offerid=:offerid) ";
		//$sql="SELECT  o.id,o.name  FROM ".$disciplineData->getBundleEntity()." o  WHERE  o.entity = :entity   AND c.dtype=:dtype  $wsql  ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
		$query->setParameter('dtype','training');
        if(!empty($name)){$query->setParameter('name','%'.strtolower($name).'%');}
        $query->setParameter('offerid',$offerid);
        $result= $query->getResult();
        return  $result; 
        
    }
}
