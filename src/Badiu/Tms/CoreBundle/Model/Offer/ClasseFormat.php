<?php

namespace Badiu\Tms\CoreBundle\Model\Offer;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\OfferBundle\Model\Classe\ClasseFormat AS AmsClasseFormat;

class ClasseFormat extends AmsClasseFormat {

    private $urlbaseimg="";
	private $lmscoremoodleformat;
	private $permission;
	private $baseresoursepath;
	private $sessionuserid;
	private $classelibutil;
    function __construct(Container $container) {
        parent::__construct($container);
        $this->urlbase=$this->getUtilapp()->getBaseUrl();
		$this->urlbase.="/system/file/get/";
		$this->lmscoremoodleformat=$this->getContainer()->get('badiu.ams.core.lmsmoodle.format');
		$this->permission=$this->getContainer()->get('badiu.system.access.permission');
		$this->baseresoursepath= $this->getContainer()->get('templating.helper.assets')->getUrl('bundles/badiuthemecore');
		
		$this->classelibutil=$this->getContainer()->get('badiu.ams.offer.classe.lib.util');
   }
   public function defaultimage($data) {
        $result = null;
        $defaultimage=$this->getUtildata()->getVaueOfArray($data,'defaultimage'); 
		$disciplinedefaultimage=$this->getUtildata()->getVaueOfArray($data,'disciplinedefaultimage'); 
       
		if(!empty($defaultimage) && $defaultimage!='null'){
			$result= $this->urlbase.$defaultimage;
		}else {$result=$this->baseresoursepath."/image/default/ams-classe-default-card.jpg";}
		
		return $result;
    }
	
	public function defaultimagecoursecategory($data) {
        $result = null;
        $defaultimage=$this->getUtildata()->getVaueOfArray($data,'defaultimage'); 
		$disciplinedefaultimage=$this->getUtildata()->getVaueOfArray($data,'disciplinedefaultimage'); 
       
		if(!empty($defaultimage)){
			$result= $this->urlbase.$defaultimage;
		}else {$result=$this->baseresoursepath."/tms/images/destaque4.png";}
		
		return $result;
    }
	public function dhour($data) {
			$value="";
			
			$labelmin=$this->getTranslator()->trans('badiu.system.time.minute.short');
			$labelhour=$this->getTranslator()->trans('badiu.system.time.hour.short');
			$value=$this->getUtildata()->getVaueOfArray($data,'disciplinehour'); 
            if (empty($value)){return null;}
		
            if($value > 0 && $value < 60) {$value=$value." $labelmin";}  
			else if($value >= 60) {
					$value=$value/60;
					$value=$value." $labelhour";
			}  
			
          return $value; 
		}
   
   public  function classelmsurl($data){ 
			$url=$this->lmscoremoodleformat->url($data);
			return $url;
		} 
	public  function classelmslink($data){ 
			$url=$this->lmscoremoodleformat->url($data);
			if(empty($url)){return null;}
			
			$classetimestart=$this->getUtildata()->getVaueOfArray($data,'classetimestart'); 
			$classetimeend=$this->getUtildata()->getVaueOfArray($data,'classetimeend');
			$classestatusshortname=$this->getUtildata()->getVaueOfArray($data,'classestatusshortname');
			
			$fparam=array('timestart'=>$classetimestart,'timeend'=>$classetimeend,'statusshortname'=>$classestatusshortname);
			
			$isclasseactive=$this->classelibutil->isActive($fparam);
			 
			$statusshortname =$this->getUtildata()->getVaueOfArray($data,'statusshortname');
			$statusclasseshortname =$this->getUtildata()->getVaueOfArray($data,'statusshortname');
			$labelaccess=$this->getTranslator()->trans('badiu.ams.synclms.access');
			$cssclasse="btn btn-card-green";
			$label=$this->getTranslator()->trans('badiu.ams.synclms.start');
			if($statusshortname=='preregistrationenrol'  || $statusshortname=='activeenrol'){
				$label=$this->getTranslator()->trans('badiu.ams.synclms.start');
				$cssclasse="btn btn-card-yellow";
			}else if($statusshortname=='studyingenrol'){
				$label=$this->getTranslator()->trans('badiu.ams.synclms.continue');
				$cssclasse="btn btn-card-yellow";
				
			}else if($statusshortname=='approvedenrol'){
				$label=$labelaccess;
				$cssclasse="btn btn-card-green";
			}
			else if($statusshortname=='quitterenrol' || $statusshortname=='evadedenrol' || $statusshortname=='retiredenrol' || $statusshortname=='disapprovedenrol'){
				$label=$this->getTranslator()->trans('badiu.ams.synclms.tryagain');
				$cssclasse="btn btn-card-yellow";
				
				if(!$isclasseactive){
					$label=$labelaccess;
					$cssclasse="btn btn-card-red";
				}
			}
			
			
			$link=" <a href=\"$url\" class=\"$cssclasse\">$label</a>";
			
			return $link;
		} 			
	
	public  function classeadminlmsurl($data){ 
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
		$haspermission= $badiuSession->hasParamPermission('badiu.system.core.param.permission.access.as','exec');
		if(!$haspermission){return null;}
		
		$url=$this->lmscoremoodleformat->url($data);
		
		return $url; 
		
	}
	public  function classerequestenrolurl($data){ 

			$url = null;
			$skey =$this->getUtildata()->getVaueOfArray($data,'enrolselectionskey');
			$enrolselectionanalysiscriteria =$this->getUtildata()->getVaueOfArray($data,'enrolselectionanalysiscriteria');
			if(empty($enrolselectionanalysiscriteria)){return  null;}
			if(!empty($skey)){$url=$this->getUtilapp()->getUrlByRoute('badiu.admin.selection.requestenroll.link',array('parentid'=>$skey));}
			return $url;
		}
		
	public  function classecancancelenrol($data){ 
			$result=false;
			$statusshortname =$this->getUtildata()->getVaueOfArray($data,'statusshortname');
			$timestart =$this->getUtildata()->getVaueOfArray($data,'timestart');

			if($statusshortname=='preregistrationenrol'  || $statusshortname=='activeenrol'){
				$badiuSession = $this->getContainer()->get('badiu.system.access.session');
				$classestudentllimitminuteaftertimestart=$badiuSession->getValue('badiu.tms.enrol.cancel.param.config.classestudentllimitminuteaftertimestart');
				//echo $classestudentllimitminuteaftertimestart;exit;
				if(empty($classestudentllimitminuteaftertimestart)){return $result;}
				
				if (!is_a($timestart, 'DateTime') && is_string($timestart)) {
					$timestart = \DateTime::createFromFormat('Y-m-d H:i:s', $timestart);
				}
			    if(empty($timestart)){return null;}
				$now=new \DateTime();
					$lessdaylimit=$classestudentllimitminuteaftertimestart*60;
					
					$limitocancel=$timestart->getTimestamp()+$lessdaylimit;
				
					if($limitocancel > $now->getTimestamp() ){$result=true;}
			}
			return $result;
		}
	public  function progress($data){ 
	
			$url = null;
			$progress =$this->getUtildata()->getVaueOfArray($data,'progress');
			
			$progressinfo =$this->getUtildata()->getVaueOfArray($data,'progressinfo');
			$progressinfo = $this->getJson()->decode($progressinfo, true);
			
            $countactivityanebleprogress=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivityanebleprogress',true);
			$countactivitycompleted=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivitycompleted',true);
			
			$countactivityanebleprogressincoursecompletetionconfig=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivityanebleprogressincoursecompletetionconfig',true);
			$countactivitycompleincoursecompletetionconfig=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivitycompleincoursecompletetionconfig',true);
			
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$courseprogresscountonlyrequiredactivities=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.courseprogresscountonlyrequiredactivities');
				
			$statusshortname =$this->getUtildata()->getVaueOfArray($data,'statusshortname');
			if(!$progress && empty($countactivityanebleprogress)){return null;}
			$labelprogress="";
			if(!$courseprogresscountonlyrequiredactivities && $countactivityanebleprogress){$labelprogress =$this->getTranslator()->trans('badiu.tms.enrol.classe.activityprogessinfo',array('%x%'=>$countactivitycompleted,'%y%'=>$countactivityanebleprogress)); }
			else if($courseprogresscountonlyrequiredactivities && $countactivityanebleprogressincoursecompletetionconfig){$labelprogress =$this->getTranslator()->trans('badiu.tms.enrol.classe.activityprogessinfo',array('%x%'=>$countactivitycompleincoursecompletetionconfig,'%y%'=>$countactivityanebleprogressincoursecompletetionconfig));}
			 $profressformat = number_format($progress, 2, ',', '.');
			//if($statusshortname=='approvedenrol'){$profressformat=100;$progress=100;} 
			$html="
			<p class=\"text-progresso mb-1\">$profressformat% concluído $labelprogress</p>
             <div class=\"progress mb-1\" style=\"height: 4px;\">
               <div class=\"progress-bar bg-progress\" role=\"progressbar\" style=\"width: $progress%\" aria-valuenow=\"$progress\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
             </div>";
			return  $html;
		} 
	public  function statusinfo($data){ 
			$info="";
			$progress =$this->getUtildata()->getVaueOfArray($data,'progress');
			
			$progressinfo =$this->getUtildata()->getVaueOfArray($data,'progressinfo');
			$progressinfo = $this->getJson()->decode($progressinfo, true);
			
            $countactivityanebleprogress=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivityanebleprogress',true);
			$countactivitycompleted=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivitycompleted',true);
			
			$countactivityanebleprogressincoursecompletetionconfig=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivityanebleprogressincoursecompletetionconfig',true);
			$countactivitycompleincoursecompletetionconfig=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivitycompleincoursecompletetionconfig',true);
			
			
			$statusshortname =$this->getUtildata()->getVaueOfArray($data,'statusshortname');
			if(!$progress && empty($countactivityanebleprogress)){return null;}
			
			if($statusshortname=='studyingenrol' && $countactivityanebleprogressincoursecompletetionconfig >0 && $countactivityanebleprogressincoursecompletetionconfig ==$countactivitycompleincoursecompletetionconfig ){
				$info=$this->getTranslator()->trans('badiu.tms.offer.classegrade.gradeinprocess');
			} 
			
			return  $info;
	}	
	public  function finalgrade($data){ 
	
			$url = null;
			$finalgrade =$this->getUtildata()->getVaueOfArray($data,'finalgrade');
			
			if(empty($finalgrade)){return null;}
			
			 $finalgradeformat = number_format($finalgrade, 2, ',', '.');
			
			$html="Nota: $finalgradeformat";
			return  $html;
		}

	public  function status($data){ 
	
			$url = null;
			$statusname =$this->getUtildata()->getVaueOfArray($data,'statusname');
			
			if(empty($statusname)){return null;}
			
			$html="Status: $statusname";
			return  $html;
		}		
	
	
	public  function classeurledit($data){ 
			
			$permissionedit=$this->permission->has_access('badiu.tms.offer.classe.edit',$this->getSessionhashkey());

			if(!$permissionedit){return null;}
			
			$id =$this->getUtildata()->getVaueOfArray($data,'id');
			$odisciplineid =$this->getUtildata()->getVaueOfArray($data,'odisciplineid');
			$urlgoback=$this->getUtilapp()->getCurrentUrl();
			$urlgoback= str_replace('/system/service/process', '/tms/my/student/fview/default/index', $urlgoback);
			$parammage=array('id'=>$id=$id,'_urlgoback'=>urlencode($urlgoback),'parentid'=>$odisciplineid);
			$classeediturl=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.classe.edit',$parammage);
			if(empty($classeediturl)){return  null;}
			
			return  $classeediturl;
		} 	
		
	public  function classeiconedit($data){ 
			
			$url=$this->classeurledit($data);
			if(empty($url)){return  null;}
			$link="<a href=\"$url\"><i class=\"fa fa-edit\"></i></a>";
			return  $link;
		} 
		
		public  function classeurlview($data){ 
			
			$permissionedit=$this->permission->has_access('badiu.tms.offer.classeview.view',$this->getSessionhashkey());

			if(!$permissionedit){return null;}
			
			$id =$this->getUtildata()->getVaueOfArray($data,'id');
			$odisciplineid =$this->getUtildata()->getVaueOfArray($data,'odisciplineid');
			$urlgoback=$this->getUtilapp()->getCurrentUrl();
			$urlgoback= str_replace('/system/service/process', '/tms/my/student/fview/default/index', $urlgoback);
			$parammage=array('id'=>$id=$id,'parentid'=>$id);
			$classeurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.classeview.view',$parammage);
			if(empty($classeurl)){return  null;}
			return  $classeurl;
		}
		
		
	public  function disciplineurlview($data){  
			
			$permissionaccess=$this->permission->has_access('badiu.tms.offer.vdiscipline.index',$this->getSessionhashkey());

			if(!$permissionaccess){return null;}
			
			$disciplineid =$this->getUtildata()->getVaueOfArray($data,'disciplineid');
			$parammage=array('parentid'=>$disciplineid);
			$disciplineurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.offer.vdiscipline.index',$parammage);
			if(empty($disciplineurl)){return  null;} 
			return  $disciplineurl;
		}		
	public  function disciplineurledit($data){  
			
			$permissionaccess=$this->permission->has_access('badiu.tms.discipline.discipline.edit',$this->getSessionhashkey());

			if(!$permissionaccess){return null;}
			
			$disciplineid =$this->getUtildata()->getVaueOfArray($data,'disciplineid');
			$urlgoback=$this->getUtilapp()->getCurrentUrl();
			$urlgoback= str_replace('/system/service/process', '/tms/my/student/fview/default/index', $urlgoback);
			$parammage=array('id'=>$disciplineid,'_urlgoback'=>urlencode($urlgoback));
			$disciplineurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.discipline.discipline.edit',$parammage);
			if(empty($disciplineurl)){return  null;} 
			return  $disciplineurl;
			
			
		}

	public  function disciplinecategoryurlview($data){  
			
			return  null;
		}
	public  function disciplinecategoryurledit($data){  
			
			return  null;
		}		

	

			
	 public  function certificateurl($data){ 
			
			$skeycertificate =$this->getUtildata()->getVaueOfArray($data,'skeycertificate');
			
			$enablecertificate =$this->getUtildata()->getVaueOfArray($data,'enablecertificate');
			if(empty($skeycertificate) && !$enablecertificate){return null;}
			
			$certificatetempleid =$this->getUtildata()->getVaueOfArray($data,'certificatetempleid');
			if(empty($skeycertificate) && empty($certificatetempleid)){return null;}
			
			$statusshortname =$this->getUtildata()->getVaueOfArray($data,'statusshortname');
			if($statusshortname!='approvedenrol'){return null;}
			
			$enrolid =$this->getUtildata()->getVaueOfArray($data,'enrolid');
			
			$fparam=array();
			if(empty($skeycertificate)){
				$moduleinstance =$this->getUtildata()->getVaueOfArray($data,'id');
				$userid=$this->getSessionuserid();
				$fparam=array('modulekey'=>'badiu.ams.enrol.classe','moduleinstance'=>$moduleinstance,'userid'=>$userid,'_operation'=>'create','skey'=>$skeycertificate,'enrolid'=>$enrolid);
			}else{
				$fparam=array('skey'=>$skeycertificate,'_operation'=>'view');
			}
			$url=$this->getUtilapp()->getUrlByRoute('badiu.admin.certificate.request.link',$fparam);
			return  $url;
		} 		
	public  function description($data){ 
			$description =$this->getUtildata()->getVaueOfArray($data,'description');
			//$description=mb_strimwidth($description, 0, 350, "...");
			return  $description;
		} 
	public  function coursecategorydescription($data){ 
			$description =$this->getUtildata()->getVaueOfArray($data,'description');
			//$description=mb_strimwidth($description, 0, 80, "...");
			return  $description;
		} 		
	public  function myenrolmanage($data){ 
			$html="";
			$accessurl=$this->classelmsurl($data);
			if(!empty($accessurl)){$html.="<a href=\"$accessurl\">Acessar</a><br />";}
			
			$enrolid =$this->getUtildata()->getVaueOfArray($data,'enrolid');
			$fparam=array('id'=>$enrolid,'parentid'=>$enrolid);
			$detailurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.my.studentclasseenrol.view',$fparam);
			
			if(!empty($detailurl)){$html.="<a href=\"$detailurl\">Ver detalhes</a><br />";}
			
			//review
			$html="";
			$certicateurl=$this->certificateurl($data);
			if(!empty($certicateurl)){$html.="<a href=\"$certicateurl\" target=\"_blank\">Emitir certificado</a><br />";}
			
			return  $html;
		}
	public  function availablemanage($data){ 
			$html="";
			$requesturl=$this->classerequestenrolurl($data);
			if(!empty($requesturl)){$html.="<a href=\"$requesturl\">Solicitar inscrição</a><br />";}
			
			/*$classeid =$this->getUtildata()->getVaueOfArray($data,'id');
			$fparam=array('id'=>$classeid,'parentid'=>$classeid);
			$detailurl=$this->getUtilapp()->getUrlByRoute('badiu.tms.my.studentclasseavailable.view',$fparam);
			*/
			if(!empty($detailurl)){$html.="<a href=\"$detailurl\">Ver detalhes</a><br />";}
			return  $html;
		}
	 public function getSessionuserid() {
		 if(!empty($this->sessionuserid)){return $this->sessionuserid;}
		 $badiuSession = $this->getContainer()->get('badiu.system.access.session');
         $badiuSession->setHashkey($this->getSessionhashkey());
		 $this->sessionuserid=$badiuSession->get()->getUser()->getId();
		 return $this->sessionuserid;
	 }
}
