<?php
use Symfony\Component\Routing\RouteCollection;


$routes = new RouteCollection();
$badiusytemlocalcurrdir=getBadiuSystemLocalBundleDirForBundle();
$badiusytemlocalfoldes=getBadiuSystemLocalBundleByFolder($badiusytemlocalcurrdir);
$routes=addBadiuSystemLocalBundleRouting($badiusytemlocalcurrdir,$badiusytemlocalfoldes, $routes,$loader);
return $routes;

 
 
 function getBadiuSystemLocalBundleDirForBundle() {
	 $currdir=getcwd();
	$poslc=strrpos($currdir,'/');
	if($poslc > 0 ){ $currdir=substr($currdir,0,$poslc );}
	else{return "";}
	$currdir="$currdir/src/Badiu/Local";
	return $currdir;
 }

 function getBadiuSystemLocalBundleByFolder($currdir) {
	 $list=array();
		if(!is_dir($currdir)){return $list;}
        $files = scandir($currdir);
		
        foreach ( $files as $k => $v) {
          	$fpath="$currdir/$v";
			if($v!='.' &&  $v!='..'){
				if(is_dir($fpath)){
					$list[]=$v;
				}  
			}
			
			
        }
        return $list;
    }
	
  function addBadiuSystemLocalBundleRouting($badiusytemlocalcurrdir,$badiusytemlocalfoldes, $routes,$loader) {
		if(!is_array($badiusytemlocalfoldes)){return $routes;}
		foreach ($badiusytemlocalfoldes as $k => $v) {
			$badiusytemlocalbundleroutingfile=$badiusytemlocalcurrdir.'/'.$v.'/Resources/config/routing.yml';
			if(file_exists($badiusytemlocalbundleroutingfile)){
				$lbr='@BadiuLocal'.$v.'/Resources/config/routing.yml';
				$lbc='Badiu\\Local\\'.$v.'\\BadiuLocal'.$v;
			    if(class_exists($lbc)){
				 $routes->addCollection($loader->import($lbr));
				}
			}
		}
		return $routes;
	 }	