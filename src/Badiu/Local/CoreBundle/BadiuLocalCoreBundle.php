<?php

namespace Badiu\Local\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BadiuLocalCoreBundle extends Bundle
{
	
	public function addLocalBundles($bundles) { 
	
	$foldes=$this->getBundleByFolder();
	$bundles=$this->addLocalClassBundleByFolder($foldes, $bundles);
	
	  return $bundles;
	}
	
	

    public function getBundleByFolder() {
		$list=array();
       $currdir=getcwd();
	   $poslc=strrpos($currdir,'/');
	   if($poslc > 0 ){ $currdir=substr($currdir,0,$poslc );}
	   else{return $list;}
	   $currdir="$currdir/src/Badiu/Local";
       
		if(!is_dir($currdir)){return $list;}
        $files = scandir($currdir);
		
        foreach ( $files as $k => $v) {
          	$fpath="$currdir/$v";
			if($v!='.' &&  $v!='..'){
				if(is_dir($fpath)){
					$list[]=$v;
				}  
			}
			
			
        }
        return $list;
    }

     public function addLocalClassBundleByFolder($localfolders, $bundles) {
		 if(!is_array($localfolders)){return $bundles;}
		
		   foreach ( $localfolders as $k => $v) {
			
			   $lbc='Badiu\\Local\\'.$v.'\\BadiuLocal'.$v;
			   if(class_exists($lbc)){
				 $itembundel=new $lbc();
			     if(is_object($itembundel)){$bundles[]=$itembundel;}  
			   }
				
		   }
		return $bundles;
	 }		 
}
