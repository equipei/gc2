<?php

namespace Badiu\Ams\DisciplineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsDisciplineDependency
 *
 * @ORM\Table(name="ams_discipline_dependency", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_discipline_dependency_uix", columns={"entity", "disciplineid","requredisciplineid"}), 
 *      @ORM\UniqueConstraint(name="ams_discipline_dependency_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_discipline_dependency_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_discipline_dependency_disciplineid_ix", columns={"disciplineid"}),
 *              @ORM\Index(name="ams_discipline_dependency_requredisciplineid_ix", columns={"requredisciplineid"}),   
 *              @ORM\Index(name="ams_discipline_dependency_dtype_ix", columns={"dtype"}),   
 *              @ORM\Index(name="ams_discipline_dependency_deleted_ix", columns={"deleted"}), 
 *              @ORM\Index(name="ams_discipline_dependency_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsDisciplineDependency
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
  
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var AmsDiscipline
     *
     * @ORM\ManyToOne(targetEntity="AmsDiscipline")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disciplineid", referencedColumnName="id", nullable=false)
     * })
     */
    private $disciplineid;
   
     /**
     * @var AmsDiscipline
     *
     * @ORM\ManyToOne(targetEntity="AmsDiscipline")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="requredisciplineid", referencedColumnName="id", nullable=false)
     * })
     */
    private $requredisciplineid;
  
	  /**
     * @var integer
     *
     * @ORM\Column(name="equivalencereqminfversion", type="integer", nullable=true)
     */
    private $equivalencereqminfversion;  
    /** 
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;
    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

   
    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

   
    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getDisciplineid() {
        return $this->disciplineid;
    }

    public function getRequredisciplineid() {
        return $this->requredisciplineid;
    }

    public function setDisciplineid() {
        $this->disciplineid = $disciplineid;
    }

    public function setRequredisciplineid() {
        $this->requredisciplineid = $requredisciplineid;
    }

 /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }


 function getEquivalencereqminfversion() { 
        return $this->equivalencereqminfversion;
    }

    function setEquivalencereqminfversion($equivalencereqminfversion) {
        $this->equivalencereqminfversion = $equivalencereqminfversion;
    }
}
