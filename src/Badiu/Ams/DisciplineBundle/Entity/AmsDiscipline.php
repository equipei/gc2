<?php

namespace Badiu\Ams\DisciplineBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AmsDiscipline
 *
 * @ORM\Table(name="ams_discipline", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_discipline_name_uix", columns={"entity", "name"}), 
 *      @ORM\UniqueConstraint(name="ams_discipline_shortname_uix", columns={"entity", "shortname"}), 
 *      @ORM\UniqueConstraint(name="ams_discipline_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_discipline_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_discipline_name_ix", columns={"name"}), 
 *              @ORM\Index(name="ams_discipline_shortname_ix", columns={"shortname"}), 
 *              @ORM\Index(name="ams_discipline_categoryid_ix", columns={"categoryid"}),
 *              @ORM\Index(name="ams_discipline_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="ams_discipline_deleted_ix", columns={"deleted"}),
  *              @ORM\Index(name="ams_discipline_typeid_ix", columns={"typeid"}),
 *              @ORM\Index(name="ams_discipline_typeaccessid_ix", columns={"typeaccessid"}),
 *              @ORM\Index(name="ams_discipline_typemanagerid_ix", columns={"typemanagerid"}),
 *             @ORM\Index(name="ams_discipline_statusid_ix", columns={"statusid"}),
 *              @ORM\Index(name="ams_discipline_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsDiscipline
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var AmsDisciplineCategory
     *
     * @ORM\ManyToOne(targetEntity="AmsDisciplineCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
		 * @var string
		 *
		 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
		 */
		private $shortname;

    
    /**
      *Composition of the value if the value.        
     * @var integer
     *
     * @ORM\Column(name="credit", type="integer",  nullable=true)
     */
    private $credit;
    
	  /**
     *Composition of the value if the value.
     * @var integer
     *
     * @ORM\Column(name="dhour", type="integer", nullable=true)
     */
    private $dhour;
    

/**
     * @var string
     *
     * @ORM\Column(name="summary", type="text", nullable=true)
     */
    private $summary;
	/**
     * @var string
     *
     * @ORM\Column(name="teachingplan", type="text", nullable=true)
     */
    private $teachingplan;
	
	/**
     * @var string
     *
     * @ORM\Column(name="competence", type="text", nullable=true)
     */
    private $competence;
		/**
     * @var string
     *
     * @ORM\Column(name="defaultimage", type="string", length=255, nullable=true)
     */
    private $defaultimage;
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='discipline';
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="adminformattemptid", type="bigint", nullable=true)
	 */
	private $adminformattemptid;
	
	
	/**
	
     * @var \Badiu\Ams\OfferBundle\Entity\AmsOfferType
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\OfferBundle\Entity\AmsOfferType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typeid", referencedColumnName="id")
     * })
     */
    private $typeid; //default type of offer
	
		/**
     * @var \Badiu\Ams\OfferBundle\Entity\AmsOfferTypeManager
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\OfferBundle\Entity\AmsOfferTypeManager")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typemanagerid", referencedColumnName="id")
     * })
     */
    private $typemanagerid;
	     /**
     * @var \Badiu\Ams\OfferBundle\Entity\AmsOfferStatus
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\OfferBundle\Entity\AmsOfferStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;
	
	        	/**
     * @var \Badiu\Ams\OfferBundle\Entity\AmsOfferTypeAccess
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\OfferBundle\Entity\AmsOfferTypeAccess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typeaccessid", referencedColumnName="id")
     * })
     */
    private $typeaccessid;
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProject
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProject")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="projectid", referencedColumnName="id")
	 * })
	 */
	private $projectid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProjectTypePartnership
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProjectTypePartnership")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="typepartnershipid", referencedColumnName="id")
	 * })
	 */
	private $typepartnershipid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProjectTypeDev
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProjectTypeDev")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="typedevid", referencedColumnName="id")
	 * })
	 */
	private $typedevid;
	
	/**
	 *
	 * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="authordevid", referencedColumnName="id")
	 * })
	 */
	private $authordevid;
	
	
	/**
     * @var integer
     *
     * @ORM\Column(name="costdev", type="float", precision=10, scale=0, nullable=true)
     */
    private $costdev;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="devinfo", type="text", nullable=true)
     */
    private $devinfo;
	/**
	 *
	 * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="enterpriseid", referencedColumnName="id")
	 * })
	 */
	private $enterpriseid;
	
	 /**
     * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterpriseDepartment
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterpriseDepartment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="departmentid", referencedColumnName="id")
     * })
     */

    private $departmentid; 
	
	/**
     * @var string
     *
     * @ORM\Column(name="usertarget", type="text", nullable=true)
     */
    private $usertarget;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="usertargetestimatedamout", type="bigint", nullable=true)
     */
    private $usertargetestimatedamout;
	
	/**
     * @var string
     *
     * @ORM\Column(name="objective", type="text", nullable=true)
     */
    private $objective;
	
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timeupdate", type="datetime", nullable=true)
	 */
	private $timeupdate;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

              /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getCategoryid() {
        return $this->categoryid;
    }

    public function setCategoryid(AmsDisciplineCategory $categoryid) {
        $this->categoryid = $categoryid;
    }

public function getShortname() {
        return $this->shortname;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }


   

    public function getTeachingplan() {
        return $this->teachingplan;
    }
  

    public function setTeachingplan($teachingplan) {
        $this->teachingplan = $teachingplan;
    }
	
 
 
	public function getSummary() {
        return $this->summary;
    }

    public function setSummary($summary) {
        $this->summary = $summary;
    }
    
    function getCredit() {
        return $this->credit;
    }

    function setCredit($credit) {
        $this->credit = $credit;
    }

    function getDhour() {
        return $this->dhour;
    }

    function setDhour($dhour) {
        $this->dhour = $dhour;
    }


    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }
	
		   function getDefaultimage() {
        return $this->defaultimage;
    }

    function setDefaultimage($defaultimage) {
        $this->defaultimage = $defaultimage;
    }
	
	public function getProjectid() {
		return $this->projectid;
	}
	
	public function setProjectid($projectid) {
		$this->projectid = $projectid;
	}
	
	function getEnterpriseid() {
            return $this->enterpriseid;
        }
	function setEnterpriseid($enterpriseid) {
            $this->enterpriseid = $enterpriseid;
        }

	public function setDepartmentid($departmentid)
    {
        $this->departmentid = $departmentid;
	}
	
	public function getDepartmentid()
    {
        return $this->departmentid;
    }

public function setUsertarget($usertarget)
    {
        $this->usertarget = $usertarget;
	}
	
	public function getUsertarget()
    {
        return $this->usertarget;
    }

public function setAdminformattemptid($adminformattemptid)
    {
        $this->adminformattemptid = $adminformattemptid;
	}
	
	public function getAdminformattemptid()
    {
        return $this->adminformattemptid;
    }	
public function setTypepartnershipid($typepartnershipid)
    {
        $this->typepartnershipid = $typepartnershipid;
	}
	
	public function getTypepartnershipid()
    {
        return $this->typepartnershipid;
    }	

public function setTypedevid($typedevid)
    {
        $this->typedevid = $typedevid;
	}
	
	public function getTypedevid()
    {
        return $this->typedevid;
    }	

public function setObjective($objective)
    {
        $this->objective = $objective;
	}
	
	public function getObjective()
    {
        return $this->objective;
    }	



	public function setTimeupdate($timeupdate)
    {
        $this->timeupdate = $timeupdate;
	}
	
	public function getTimeupdate()
    {
        return $this->timeupdate;
    }


public function setAuthordevid($authordevid)
    {
        $this->authordevid = $authordevid;
	}
	
	public function getAuthordevid()
    {
        return $this->authordevid;
    }		
	
	
public function setCostdev($costdev)
    {
        $this->costdev = $costdev;
	}
	
	public function getCostdev()
    {
        return $this->costdev;
    }	


public function setDevinfo($devinfo)
    {
        $this->devinfo = $devinfo;
	}
	
	public function getDevinfo()
    {
        return $this->devinfo;
    }


function getCompetence() {
            return $this->competence;
        }
	function setCompetence($competence) {
            $this->competence = $competence;
        }	
		
public function setUsertargetestimatedamout($usertargetestimatedamout)
    {
        $this->usertargetestimatedamout = $usertargetestimatedamout;
	}
	
	public function getUsertargetestimatedamout()
    {
        return $this->usertargetestimatedamout;
    }
	
	public function getTypeid() {
        return $this->typeid;
    }
	public function setTypeid($typeid) {
        $this->typeid = $typeid;
    }
	
	public function setTypeaccessid($typeaccessid) {
        $this->typeaccessid = $typeaccessid;
    }

    function getTypeaccessid() {
        return  $this->typeaccessid ;
    }
	
	
    public function getStatusid() {
        return $this->statusid;
    }

 public function setStatusid($statusid) {
        $this->statusid = $statusid;
    }
	
	public function getTypemanagerid() {
        return $this->typemanagerid;
    }
	
	public function setTypemanagerid($typemanagerid) {
        $this->typemanagerid = $typemanagerid;
    }
	
	public function getTimestart() {
	  return $this->timestart;
    }

    public function getTimeend() {
        return $this->timeend;
    }
	
	  public function setTimestart($timestart) {
        $this->timestart = $timestart;
    }

    public function setTimeend($timeend) {
        $this->timeend = $timeend;
    }
}
