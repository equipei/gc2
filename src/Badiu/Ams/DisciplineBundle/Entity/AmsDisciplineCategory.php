<?php

namespace Badiu\Ams\DisciplineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsDisciplineCategory
 *
 * @ORM\Table(name="ams_discipline_cetegory", uniqueConstraints={
 *                                                  @ORM\UniqueConstraint(name="ams_discipline_cetegory_shortname_uix", columns={"entity", "shortname"}),
 *                                                  @ORM\UniqueConstraint(name="ams_discipline_cetegory_idnumber_uix", columns={"entity", "idnumber"})},
 *                                     indexes={@ORM\Index(name="ams_discipline_cetegory_entity_ix", columns={"entity"}),
 *                                              @ORM\Index(name="ams_discipline_cetegory_name_ix", columns={"name"}),
 *                                              @ORM\Index(name="ams_discipline_cetegory_shortname_ix", columns={"shortname"}),
 *                                              @ORM\Index(name="ams_discipline_cetegory_dtype_ix", columns={"dtype"}),
 *                                                  @ORM\Index(name="ams_discipline_cetegory_orderidpath_ix", columns={"orderidpath"}),
  *                                                  @ORM\Index(name="ams_discipline_cetegory_sortorder_ix", columns={"sortorder"}),
 *                                              @ORM\Index(name="ams_discipline_cetegory_deleted_ix", columns={"deleted"})})
 * 
 * @ORM\Entity
 */
class AmsDisciplineCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
		 * @var string
		 *
		 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
		 */
		private $shortname;


        /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='course'; // course | training | event

   /**
     * @var integer
     *
     * @ORM\Column(name="parent", type="bigint", nullable=true)
     */
    private $parent; //id of father
    
    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    private $level; //generation of idpath
    
    /**
     * @var string
     *
     * @ORM\Column(name="idpath", type="string", length=255, nullable=false)
     */
    private $idpath; //id tree example: 1, 1.1, 1.2,1.2.1

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path; //hierarchy of id father exemp 1, 1/2, 1/2/4
   
     /**
     * @var float
     *
     * @ORM\Column(name="orderidpath", type="float", precision=10, scale=0, nullable=false)
     */
    private $orderidpath; //order of id tree

    /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="bigint", nullable=true)
     */
    private $sortorder;

     /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

             /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

	/**
     * @var string
     *
     * @ORM\Column(name="defaultimage", type="string", length=255, nullable=true)
     */
    private $defaultimage;
     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
  
     /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    
    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getIdpath() {
        return $this->idpath;
    }

    public function getPath() {
        return $this->path;
    }

    public function getOrderidpath() {
        return $this->orderidpath;
    }

    public function getSortorder() {
        return $this->sortorder;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setIdpath($idpath) {
        $this->idpath = $idpath;
    }

    public function setPath($path) {
        $this->path = $path;
    }

    public function setOrderidpath($orderidpath) {
        $this->orderidpath = $orderidpath;
    }

    public function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

public function getShortname() {
        return $this->shortname;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }

    function getParent() {
        return $this->parent;
    }

    function getLevel() {
        return $this->level;
    }

    function setParent($parent) {
        $this->parent = $parent;
    }

    function setLevel($level) {
        $this->level = $level;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

	   function getDefaultimage() {
        return $this->defaultimage;
    }

    function setDefaultimage($defaultimage) {
        $this->defaultimage = $defaultimage;
    }
}
