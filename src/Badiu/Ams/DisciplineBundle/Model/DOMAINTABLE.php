<?php
namespace Badiu\Ams\DisciplineBundle\Model;
class DOMAINTABLE {

	//Discipline type
    public static  $DISCIPLINE   ="discipline";
    public static  $DISCIPLINE_LINK   ="discipline_link";
    public static  $EVENT   ="event";
    public static  $TRAINING   ="training";
	
	public static  $MEASURETYPE_MINUTE="minute";
    public static  $MEASURETYPE_HOUR   ="hour";
	public static  $MEASURETYPE_CREDIT   ="credit";

}
?>