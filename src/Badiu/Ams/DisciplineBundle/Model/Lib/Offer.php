<?php

namespace  Badiu\Ams\DisciplineBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Offer extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
	public function save($data) {
			$dto=$data->getDto();
			$disciplineid=$this->getContainer()->get('request')->get('parentid');
			$oddata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			
			if(isset($dto['offers']) && is_array($dto['offers'])){
				$listoffer=$dto['offers'];
				  foreach ($listoffer as $offerid){
						$existdiscipline=$oddata->existDiscipline($this->getEntity(),$offerid,$disciplineid);
						if(!$existdiscipline){
							$dodata=$this->getContainer()->get('badiu.ams.discipline.offer.data');
							$fdto=$this->getOfferForm($dto);
							$offer=$this->getOffer($offerid);
						//	echo "oferta: $offerid <br>";
							$fdto->setOfferid($offer);
							$dodata->setDto($fdto);
							$result=$dodata->save();
						}else{echo "$offerid ja existe";exit;}
						
				  }
				
			}
		
			return $data->getDto();
		}
		
	public function getOfferForm($fdata) {
			$dto=null;
			$dto=$this->getContainer()->get('badiu.ams.discipline.offer.entity');
			$dto=$this->initDefaultEntityData($dto);
			
			
			$disciplineid=$this->getContainer()->get('request')->get('parentid');
			$discipline=$this->getDiscipline($disciplineid);
			$dto->setDisciplineid($discipline);
			
			if(isset($fdata['disciplinename'])) {$dto->setDisciplinename($fdata['disciplinename']);}
			else {$dto->setDisciplinename($discipline->getName());}
			
			if(isset($fdata['drequired'])) {$dto->setDrequired($fdata['drequired']);}
			if(isset($fdata['statusid'])) {$dto->setStatusid($fdata['statusid']);}
			if(isset($fdata['groupid'])) {$dto->setGroupid($fdata['groupid']);}
			if(isset($fdata['typeid'])) {$dto->setTypeid($fdata['typeid']);}
			if(isset($fdata['categoryid'])) {$dto->setCategoryid($fdata['categoryid']);}
			if(isset($fdata['description'])) {$dto->setDescription($fdata['description']);}
			if(isset($fdata['periodid'])) {$dto->setPeriodid($fdata['periodid']);}
			if(isset($fdata['timestart'])) {$dto->setTimestart($fdata['timestart']);}
			if(isset($fdata['timeend'])) {$dto->setTimeend($fdata['timeend']);}
			if(isset($fdata['dhour'])) {$dto->setDhour($fdata['dhour']);}
			if(isset($fdata['dhour'])) {$dto->setMeasurevalue($fdata['dhour']);}//temp review
			if(isset($fdata['conclusiongrade'])) {$dto->setConclusiongrade($fdata['conclusiongrade']);}
			if(isset($fdata['sserviceid'])) {$dto->setSserviceid($fdata['sserviceid']);}
			if(isset($fdata['idnumberlms'])) {$dto->setIdnumberlms($fdata['idnumberlms']);}
			if(isset($fdata['summary'])) {$dto->setSummary($fdata['summary']);}
			if(isset($fdata['teachingplan'])) {$dto->setTeachingplan($fdata['teachingplan']);}
			if(isset($fdata['hroomid'])) {$dto->setHroomid($fdata['hroomid']);}
			//if(isset($fdata['shortname'])) {$dto->setShortname($fdata['shortname']);}
			if(isset($fdata['param'])) {$dto->setParam($fdata['param']);}
			
			return $dto;
		
	}
	
	public function getDiscipline($id) {
		$data=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
		$dto=$data->findById($id);
		return $dto;
	}
	
	public function getOffer($id) {
		$data=$this->getContainer()->get('badiu.ams.offer.offer.data');
		$dto=$data->findById($id);
		return $dto;
	}
}
