<?php

namespace  Badiu\Ams\DisciplineBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class SummarizeData  extends BadiuModelLib{

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
	/*function teste(){
		$fparam=array('_period'=>'currentyear');
		$result=$this->countByYear($fparam);
		
		echo "<pre>"; 
		print_r($result);
		echo "</pre>";exit;
	}*/
	 function countByYear($param){
		  $period=$this->getUtildata()->getVaueOfArray($param,'_period'); 
		  if(!empty($period)){unset($param['_period']);}
		  
		  $result=array();
		  $data=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
		  $periodutil=$this->getContainer()->get('badiu.system.core.lib.date.periodutil');
		  if($period=='allyears'){
			  $perioddto=$this->getMinMaxYear($param);
			
			  $firstdate=$this->getUtildata()->getVaueOfArray($perioddto,'firstdate'); 
			  $firstdate= $periodutil->castDbStringToDate($firstdate);
			  if(empty($firstdate)){return $result;}
			  $firstdate=$firstdate->format('Y');
			  
			  $lasdate=$this->getUtildata()->getVaueOfArray($perioddto,'lastdate'); 
			 
			  $lasdate= $periodutil->castDbStringToDate($lasdate);
			  if(empty( $lasdate)){return $result;}
			 
			  $lasdate=$lasdate->format('Y');
			
			 if(ctype_digit($firstdate) && ctype_digit($lasdate) && ($lasdate >= $firstdate)){
				$limit=50;
				$exec=true;
			 
				while ($exec){
						
					$tparam=array('year'=>$firstdate);
					$dateperiod= $periodutil->startEndDatePeriod($tparam);
					$param['_date1']=$dateperiod->date1;
					$param['_date2']=$dateperiod->date2;
					$param['_year']=$firstdate;
			  
					$result=$this->countByYearSearch($param,$result);
					$firstdate++;
					
					if($firstdate > $lasdate){$exec=false;break;}
				}
				
			 }
			   
			  
		  }else if($period=='currentyear'){
			  $now=new \DateTime();
			  $currentyear = $now->format('Y');
			  $tparam=array('year'=>$currentyear);
			  $dateperiod= $periodutil->startEndDatePeriod($tparam);
			  $param['_date1']=$dateperiod->date1;
			  $param['_date2']=$dateperiod->date2;
			  $param['_year']=$currentyear;
			  
			  $result=$this->countByYearSearch($param,$result);
		  }
		 return $result;
	 }
	 
	function countByYearSearch($param,$dresult){
		$dtype=$this->getUtildata()->getVaueOfArray($param,'dtype'); 
		$date1=$this->getUtildata()->getVaueOfArray($param,'_date1');
		$date2=$this->getUtildata()->getVaueOfArray($param,'_date2');
		$year=$this->getUtildata()->getVaueOfArray($param,'_year');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		if(empty($ntity)){$entity=$this->getEntity();}
		$wsql="";
		if(!empty($dtype)){$wsql.=" AND o.dtype=:dtype ";}
		$sql="SELECT COUNT(o.id) AS countrecord FROM BadiuAmsDisciplineBundle:AmsDiscipline o WHERE o.id > 0 AND o.entity=:entity AND o.timecreated >=:date1 AND o.timecreated <=:date2 AND o.deleted=:deleted $wsql ";
		
		$data=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
		 
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
		$query->setParameter('date1',$date1);
		$query->setParameter('date2',$date2);
		$query->setParameter('deleted', 0);
		if(!empty($dtype)){$query->setParameter('dtype', $dtype);}
		$result= $query->getOneOrNullResult();
		
		$countrecord=$this->getUtildata()->getVaueOfArray($result,'countrecord');
		$dresult[$year]=$countrecord;
		return $dresult;
	}
	 
	 function getMinMaxYear($param){
		$dtype=$this->getUtildata()->getVaueOfArray($param,'dtype'); 
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		if(empty($ntity)){$entity=$this->getEntity();}
		$wsql="";
		if(!empty($dtype)){$wsql.=" AND o.dtype=:dtype ";}
		$sql="SELECT MIN(o.timecreated) AS firstdate, MAX(o.timecreated) AS lastdate  FROM BadiuAmsDisciplineBundle:AmsDiscipline o WHERE o.id > 0 AND o.entity=:entity AND o.timecreated IS NOT NULL  AND o.deleted=:deleted $wsql ";
		
		$data=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
		 
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
		$query->setParameter('deleted', 0);
		if(!empty($dtype)){$query->setParameter('dtype', $dtype);}
		$result= $query->getOneOrNullResult();
		
		return $result;
	}
}
