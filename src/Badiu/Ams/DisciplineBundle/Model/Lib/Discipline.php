<?php

namespace  Badiu\Ams\DisciplineBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Discipline extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	/**
	*return list of all fied data for copy
	* cen be used as service 
	*/
	public function getDataFromDiscipline($param=array()) {
			$disciplineid=$this->getUtildata()->getVaueOfArray($param, 'disciplineid');
			$badiuSession=$this->getContainer()->get('badiu.system.access.session');
			$entity=$badiuSession->get()->getEntity();
			if(empty($disciplineid)){
				$qparams=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
				$disciplineid=$this->getUtildata()->getVaueOfArray($qparams, 'disciplineid');
				$clientsession=$this->getUtildata()->getVaueOfArray($qparams, '_clientsession');
				//get entity by $clientsession need to do it
			}
			if(empty($disciplineid) || empty($entity) ){return null;}
			
			$offerdiscipinedata=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
			$fparam=array('id'=>$disciplineid,'entity'=>$entity);
			$column=" o.id,o.name,o.shortname,o.credit,o.dhour,o.summary,o.teachingplan,o.description,o.idnumber ";
			$result=$offerdiscipinedata->getGlobalColumnsValue($column,$fparam);
			return $result;
		}
		
	
}
