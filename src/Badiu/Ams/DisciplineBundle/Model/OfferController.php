<?php

namespace Badiu\Ams\DisciplineBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormError;
class OfferController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
              
    public function addCheckForm($form,$data) {
           $check=true;
		 
         // if ($data->existEdit()) {
            //    $form->get('cpf')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.cpfnotvalid')));
//                $check= false; 
          //  }
           return  $check;
       }
	   
	 public function setDefaultDataOnAddForm($dto) {
            $defaultData=$this->getDefaultdata();
          if(!empty($defaultData)){
              foreach ($defaultData as $key => $value){
                  $dto[$key]=$value;
              }  
          }
         
        return $dto;
     }
	 
	
	 public function save($data) {
		$dto=$data->getDto();
		$offerlib=$this->getContainer()->get('badiu.ams.discipline.offer.lib');
		$result=$data->setDto($offerlib->save($data));
		return $result;
    } 
	
	   public function addDisciplineidToOffer($dto,$dtoParent) {
			//$dto->setModuleinstance($dtoParent->getId());
			return $dto;
    }
	public function findDisciplineidInOffer($dto) {
		//$disciplineid=null;
		//if (array_key_exists('disciplineid',$dto)){$disciplineid=$dto['disciplineid'];}
       // return $disciplineid;
	   return $dto->getDisciplineid()->getId();
    }	
	
	
}
