<?php

namespace Badiu\Ams\DisciplineBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class DisciplineFormat extends BadiuFormat{
       private $permission;
     function __construct(Container $container) {
            parent::__construct($container);
			$this->permission=$this->getContainer()->get('badiu.system.access.permission');
       } 

    /*public  function exec($data,$function=null){
            $result=null; 
            if($function=='name'){$result=$this->name($data);}

            return $result;
    }   */           
     
    public  function name($data){
            $value="";
            $iconRefDiscipline=$this->getContainer()->get('templating.helper.assets')->getUrl('bundles/badiuthemebase/image/icons/discipline_reference.png');
            $iconDiscipline=$this->getContainer()->get('templating.helper.assets')->getUrl('bundles/badiuthemebase/image/icons/discipline.png');

             $imgRefDiscipline="<img src=\"$iconRefDiscipline\" />";
             $imgDiscipline="<img src=\"$iconDiscipline\" />";
            if (array_key_exists("name",$data)){
                    $name=$data["name"];
                    $dtype=$data["dtype"];
                    $value=$data["name"];
                    if(!empty($name)){
                        if($dtype==DOMAINTABLE::$DISCIPLINE){
                            $value= "$imgDiscipline $name";
                        }
                       else if($dtype==DOMAINTABLE::$DISCIPLINE_LINK){
                            $value= "<em><span style=\"color: #837c7c\">$imgRefDiscipline $name</span></em>";
                        }
                    }else{
                        $value=$name;
                    }
                    
            }
            
        
        return $value; 
    } 
public  function hour($data){
            $value="";
			
			$labelmin=$this->getTranslator()->trans('badiu.system.time.minute.short');
			$labelhour=$this->getTranslator()->trans('badiu.system.time.hour.short');
             if (array_key_exists("dhour",$data)){
			 $value=$data['dhour'];
                if($value > 0 && $value < 60) {$value=$value." $labelmin";}  
				else if($value >= 60) {
					$value=$value/60;
					$value=$value." $labelhour";
				}  
			  }
          return $value; 
    } 
	

 public  function categorylinkemanager($data){
            $value="";
            $id=$this->getUtildata()->getVaueOfArray($data,'id');
            
			$labelmanagedisciplinelink=$this->getTranslator()->trans('badiu.ams.discipline.category.mamagediscipline');
            $labeladddisciplinelink=$this->getTranslator()->trans('badiu.ams.discipline.category.addnewdiscipline');

           
            $parammage=array('parentid'=>$id);
			
			$permimanagedisciplineurl=$this->permission->has_access('badiu.ams.discipline.categorydiscipline.index',$this->getSessionhashkey());
			$managedisciplineurl="";
			if($permimanagedisciplineurl){
				 $managedisciplineurl=$this->getUtilapp()->getUrlByRoute('badiu.ams.discipline.categorydiscipline.index',$parammage);
			}
		  
		    $permiadddisciplineurl=$this->permission->has_access('badiu.ams.discipline.categorydiscipline.add',$this->getSessionhashkey());
            $adddisciplineurl=null;
			if($permiadddisciplineurl){
				$adddisciplineurl=$this->getUtilapp()->getUrlByRoute('badiu.ams.discipline.categorydiscipline.add',$parammage);
			}
			
			
            if(!empty($managedisciplineurl)){$managedisciplineurl="<a href=\"$managedisciplineurl\">$labelmanagedisciplinelink</a><br />";}
            if(!empty($adddisciplineurl)){$adddisciplineurl ="<a href=\"$adddisciplineurl\">$labeladddisciplinelink</a><br />";}
			
            $value="$managedisciplineurl $adddisciplineurl ";
         
        return $value; 
    }	
}
