<?php

namespace Badiu\Ams\DisciplineBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\CoreBundle\Model\AmsDataBase;
class DisciplineData  extends AmsDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
     
   public function getFormChoice($entity,$param=array(),$orderby="") {
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id,o.name FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.dtype LIKE :dtype $wsql $orderby";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('dtype','%discipline%');
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return  $result;
    }
 

 public function findByEntity($entity,$orderby="",$deleted=false) {
             $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity  AND o.dtype LIKE :dtype AND o.deleted=:deleted $orderby";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
             $query->setParameter('dtype','%discipline%');
             $query->setParameter('deleted',$deleted);
            $result= $query->getResult();
            return  $result;
        }

		public function getNewOffers() {
			$offerdata=$this->getContainer()->get('badiu.ams.offer.offer.data');
			$offerdiscipinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			$disciplineid=$this->getContainer()->get('request')->get('parentid');
			$badiuSession=$this->getContainer()->get('badiu.system.access.session');
			$entity=$badiuSession->get()->getEntity();
            $sql="SELECT DISTINCT o.id,CONCAT(c.name, ' / ',o.name) AS name FROM ".$offerdata->getBundleEntity()." o JOIN o.curriculumid cr JOIN cr.courseid c WHERE o.entity=:entity AND o.id NOT IN (SELECT f.id FROM ".$offerdiscipinedata->getBundleEntity()." od JOIN od.offerid f WHERE od.entity=:odentity AND od.disciplineid=:disciplineid)";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			 $query->setParameter('odentity',$entity);
			  $query->setParameter('disciplineid',$disciplineid);
           $result= $query->getResult();
		   return  $result;
        }
	
	/**
	*return liste of all fied 
	* cen be used as service
	*/
	public function getGeneralInfo($param=array()) {
			$disciplineid=$this->getUtildata()->getVaueOfArray($param, 'disciplineid');
			$badiuSession=$this->getContainer()->get('badiu.system.access.session');
			$entity=$badiuSession->get()->getEntity();
			if(empty($disciplineid)){
				$qparams=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
				$disciplineid=$this->getUtildata()->getVaueOfArray($qparams, 'disciplineid');
				$clientsession=$this->getUtildata()->getVaueOfArray($qparams, '_clientsession');
				//get entity by $clientsession need to do it
			}
			
			if(empty($disciplineid) || empty($entity) ){return null;}
			
			
			$offerdiscipinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			$disciplineid=$this->getContainer()->get('request')->get('parentid');
			$badiuSession=$this->getContainer()->get('badiu.system.access.session');
			$entity=$badiuSession->get()->getEntity();
            $sql="SELECT DISTINCT o.id,CONCAT(c.name, ' / ',o.name) AS name FROM ".$offerdata->getBundleEntity()." o JOIN o.curriculumid cr JOIN cr.courseid c WHERE o.entity=:entity AND o.id NOT IN (SELECT f.id FROM ".$offerdiscipinedata->getBundleEntity()." od JOIN od.offerid f WHERE od.entity=:odentity AND od.disciplineid=:disciplineid)";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			 $query->setParameter('odentity',$entity);
			  $query->setParameter('disciplineid',$disciplineid);
           $result= $query->getResult();
		   return  $result;
        }
		

		public function getCategoryInfo($entity,$disciplineid) {
            $sql="SELECT  ct.id,ct.name,ct.dconfig FROM ".$this->getBundleEntity()." o  JOIN o.categoryid ct WHERE o.entity = :entity AND o.id = :id ";
            $query = $this->getEm()->createQuery($sql);
			 $query->setParameter('entity',$entity);
            $query->setParameter('id',$disciplineid);
            $result= $query->getOneOrNullResult();
            return  $result;
           
           
       }

 public function getForAutocomplete() {
        
        $disciplinecategoryid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('disciplinecategoryid'); 
        $name=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
        
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $disciplineData=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
      
        $wsql="";
        if(!empty($name)){$wsql.=" AND CONCAT(o.id,LOWER(o.name))  LIKE :name ";}
		if(!empty($disciplinecategoryid)){$wsql=" AND o.categoryid=:categoryid ";}
        $sql="SELECT  o.id,o.name  FROM ".$disciplineData->getBundleEntity()." o  WHERE  o.entity = :entity AND o.dtype!=:dtype $wsql ";
		
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
		$query->setParameter('dtype','training');
        if(!empty($name)){$query->setParameter('name','%'.strtolower($name).'%');}
		if(!empty($disciplinecategoryid)){$query->setParameter('categoryid',$disciplinecategoryid);}
        $result= $query->getResult();
        return  $result; 
        
    }
	
	  public function getNameByIdOnEditAutocomplete($id) {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT o.name FROM ".$this->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }	 

  public function getIdByClasseid($id) {
        $sql = "SELECT  d.id FROM BadiuAmsOfferBundle:AmsOfferClasse o JOIN o.odisciplineid od JOIN od.disciplineid d  WHERE o.id=:id";
       $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
		if(isset($result['id'])){$result=$result['id'];}
        return  $result;
    }  
}
