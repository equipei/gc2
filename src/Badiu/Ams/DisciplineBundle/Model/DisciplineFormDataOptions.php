<?php

namespace Badiu\Ams\DisciplineBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class DisciplineFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
   
    public  function getDisciplineType(){
        $list=array();
        $list[DOMAINTABLE::$DISCIPLINE]=$this->getTranslator()->trans('badiu.ams.discipline.dtype.discipline');
        $list[DOMAINTABLE::$DISCIPLINE_LINK]=$this->getTranslator()->trans('badiu.ams.discipline.dtype.link');
        return $list;
    }

}
