<?php

namespace Badiu\Ams\OfferBundle\Filter;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFilter;
class OfferFilter extends BadiuFilter {
    
  /**
     * @var integer
      */
    private $categoryid;

    /**
     * @var integer
      */
    private $curriculumid;
    /**
     * @var integer
      */    
    private $statusid;
    
    /**
     * @var integer
      */
    private $typeid;
    
  /**
   * @return int
   */
  public function getCategoryid()
  {
    return $this->categoryid;
  }

  /**
   * @param int $categoryid
   */
  public function setCategoryid($categoryid)
  {
    $this->categoryid = $categoryid;
  }

  /**
   * @return int
   */
  public function getCurriculumid()
  {
    return $this->curriculumid;
  }

  /**
   * @param int $curriculumid
   */
  public function setCurriculumid($curriculumid)
  {
    $this->curriculumid = $curriculumid;
  }

  /**
   * @return int
   */
  public function getStatusid()
  {
    return $this->statusid;
  }

  /**
   * @param int $statusid
   */
  public function setStatusid($statusid)
  {
    $this->statusid = $statusid;
  }

  /**
   * @return int
   */
  public function getTypeid()
  {
    return $this->typeid;
  }

  /**
   * @param int $typeid
   */
  public function setTypeid($typeid)
  {
    $this->typeid = $typeid;
  }




}
