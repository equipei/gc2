<?php

namespace Badiu\Ams\OfferBundle\Filter;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFilter;
class DisciplineFilter extends BadiuFilter {
  /**
   * @var integer
   */
  private $offerid;
    /**
     * @var integer
      */    
    private $statusid;
    
    /**
     * @var integer
      */
    private $groupid;

  /**
   * @var integer
   */
  private $drequired;

  /**
   * @var integer
   */
  private $periodid;

  public function addParent($parentid) {
    $this->offerid = $parentid;
  }



  /**
   * @return int
   */
  public function getStatusid()
  {
    return $this->statusid;
  }

  /**
   * @param int $statusid
   */
  public function setStatusid($statusid)
  {
    $this->statusid = $statusid;
  }

  /**
   * @return int
   */
  public function getGroupid()
  {
    return $this->groupid;
  }

  /**
   * @param int $groupid
   */
  public function setGroupid($groupid)
  {
    $this->groupid = $groupid;
  }

  /**
   * @return int
   */
  public function getDrequired()
  {
    return $this->drequired;
  }

  /**
   * @param int $drequired
   */
  public function setDrequired($drequired)
  {
    $this->drequired = $drequired;
  }

  /**
   * @return int
   */
  public function getPeriodid()
  {
    return $this->periodid;
  }

  /**
   * @param int $periodid
   */
  public function setPeriodid($periodid)
  {
    $this->periodid = $periodid;
  }

  /**
   * @return int
   */
  public function getOfferid()
  {
    return $this->offerid;
  }

  /**
   * @param int $offerid
   */
  public function setOfferid($offerid)
  {
    $this->offerid = $offerid;
  }





}
