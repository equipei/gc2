

<div class="report-container mb40"  id="_badiu_theme_base_index_vuejs">
    <div class="panel">

        <div class="row-fluid">
            <div class="col-xs-12">
                <h5 class="title pull-left">Resultado:<?php echo $infoResult; ?></h5>
                <a href=<?php echo $urltoextport; ?> class="pull-left btn-export-excel">Exportar</a>
            </div>
        </div>
        <div class="table-window" > 

            <table class="table table-condensed table-hover  bootgrid-table">
                <thead>
                    <tr>
                        <th  v-for="(item, index) in tablehead" v-html="item.name"></th>
                    </tr>
                </thead>
                <tbody> 
                    <tr v-for="(itemdata, index) in tabledata">
                        <td v-for="(item, index1) in tablehead">
                            <div class="form-group" v-if="item.key === 'grade'">  

	                              <span v-if="!itemdata.crtedit" :class="myclass"  v-on:mouseover= "change()"  @click="whenclick(itemdata)" v-html="itemdata[item.key]"></span>

			                    <span v-if="!itemdata.crtedit" @click="whenclick(itemdata)" :class="pincel" class="glyphicon glyphicon-pencil" aria-hidden="true">editar</span> 
			                    <input  v-model="itemdata.grade" v-on:keyup.enter="whenedited(itemdata)" v-on:blur="whenedited(itemdata)" v-if="itemdata.crtedit" type="text" class="fadeorm-control" id="exampleInputName2" placeholder="">
               
                               
                             </div>
                            <div  v-if="item.key !== 'grade'"> 
                               <div v-html="itemdata[item.key]" ></div>
                            </div>    
                        </td> 
                    </tr>
                </tbody> 
            </table>

        </div> 
        <?php echo $pagingout; ?>


    </div>


    <!-- start modal -->
    <div id="_badiu_theme_base_modal_delete" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Item Clicked!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Voce realmente deseja apagar esse registro?
                    <div v-for="(items, index) in itemsected">
                    <div v-html="items" > <br /></div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" @click="processDeleteService()">Remover</button> 
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <!--end modal-->
	
	<!-- start form control -->
	<div class="row">
		<div  class="col-8">
			<select class="form-control"  name="_badiu_core_report_menu"  v-model="gradebatchaddtype">
			<option v-for="item in gradebatchaddformoptions" :value="item.value">{{item.text }}</option>
			</select>
		</div>
		<div v-if="showgraderate"  class="col-2"> <input type="text" class="form-control" v-model="graderate" placeholder="Digite a nota"></div>
		<div v-if="showgraderate"  class="col-2"> <button type="button" @click="gradebatchaddexec()" class="form-control btn btn-primary">Processar</button> </div>
	</div>
	<!-- end form control -->
</div>

