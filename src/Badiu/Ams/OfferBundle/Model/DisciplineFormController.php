<?php

namespace Badiu\Ams\OfferBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class DisciplineFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     public function changeParamOnOpen() {
			$this->getFieldProcessOnOpen();
            $this->inheritDconfig();
             $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
            $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $param = $this->getParam();
            $lmsintegration = $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');

            $param['sserviceid'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'sserviceid');
            $param['lmssynctype'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynctype');
            $param['lmssynclevel'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynclevel');
            $param['lmscoursecat'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatid');
            $param['lmscoursecatparent'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatparent');
            $param['lmscourse'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseid');
            $param['lmscoursegroup'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursegroupid');
			$param['lmscourseidtemplate'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseidtemplate');
            
            $param['productintegration'] = $this->getUtildata()->getVaueOfArray($dconfig, 'financintegration.productintegration',true);
			//$param=$this->getContainer()->get('badiu.ams.core.lib.roleconclusionform')->disciplineFormChangeParamOnOpen($param);
			if($this->isEdit()){$param=$this->getContainer()->get('badiu.ams.offer.lib.enrolselectionfilter')->changeParamOnOpenEdit($param,'badiu.ams.offer.discipline');}
			else{$param=$this->getContainer()->get('badiu.tms.core.lib.lmsmoodlesyncdefaultform')->defaultChangeParamOnEdit($param);}
			
            $this->setParam($param); 
     }   
     
    public function changeParam() {
		$param = $this->getParam();
        //get name of discipline
        $disciplineid= $this->getParamItem('disciplineid');
        $disciplinename= $this->getParamItem('disciplinename');
        if(empty($disciplinename)){
            $ddata=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $entity = $badiuSession->get()->getEntity();
            $dname=$ddata->getColumnValue($entity,'name',$paramf=array('id'=>$disciplineid));
            $this->addParamItem('disciplinename',$dname);
         }
        
        $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
        if (empty($dconfig) || !is_array($dconfig)) {
            $dconfig = array();
        }
		 
		 $param['dconfig'] = $dconfig;
		$param=$this->getContainer()->get('badiu.ams.core.lib.roleconclusionform')->disciplineFormChangeParam($param);
		$dconfig = $this->getUtildata()->getVaueOfArray($param, 'dconfig');
        $dconfig = $this->getJson()->encode($dconfig);
       
        $param['dconfig'] = $dconfig;
        $this->setParam($param);
        
     }
     
     
         
     public function inheritDconfig() {
          if($this->isEdit()){return null;}
          $dconfig=null;
          $isinherit=false;
         //has dconfig if is edit
         $entity= $this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
        // $sectionid=$this->getParamItem('sectionid');
          $sectionid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('sectionid');
         //echo "sectionid: $sectionid";exit;
         if(!empty($sectionid)){
              $sdata=$this->getContainer()->get('badiu.ams.offer.section.data');
              $dconfig=$sdata->getColumnValue($entity,'dconfig',$paramf=array('id'=>$sectionid));
              
              $dconfig = $this->getJson()->decode($dconfig, true);
              $status= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.status',true);
              if($status=='active'){$isinherit=true;}
              $dconfig['lmsintegration']['lmscoursecatparent']= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscoursecatid',true);
              $dconfig['lmsintegration']['lmscoursecatparenid']= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscoursecatid',true);
              $dconfig['lmsintegration']['lmscoursecatparentname']= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscoursecatname',true);
              $lmssynctypeoriginal=$this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmssynctypeoriginal',true);
              if(!empty($lmssynctypeoriginal)){$dconfig['lmsintegration']['lmssynctype']= $lmssynctypeoriginal;}
         
              $dconfig['lmsintegration']['lmscoursecatid']= null;
              $dconfig['lmsintegration']['lmscoursecat']= null;
      
                  
           $dconfig = $this->getJson()->encode($dconfig);
           
              $this->addParamItem('dconfig',$dconfig);
        }
         // get config of offer
        if(!$isinherit){
            $parentid = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
            $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
            $dconfig=$odata->getColumnValue($entity,'dconfig',$paramf=array('id'=>$parentid));
            
              $dconfig = $this->getJson()->decode($dconfig, true);
              $dconfig['lmsintegration']['lmscoursecatparent']= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscoursecatid',true);
              $dconfig['lmsintegration']['lmscoursecatparenid']= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscoursecatid',true);
              $dconfig['lmsintegration']['lmscoursecatparentname']= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscoursecatname',true);
              $lmssynctypeoriginal=$this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmssynctypeoriginal',true);
              if(!empty($lmssynctypeoriginal)){$dconfig['lmsintegration']['lmssynctype']= $lmssynctypeoriginal;}
         
              $dconfig['lmsintegration']['lmscoursecatid']= null;
              $dconfig['lmsintegration']['lmscoursecat']= null;
              
              $dconfig = $this->getJson()->encode($dconfig);
              $this->addParamItem('dconfig',$dconfig);
            
        }
          
     }
         
    
}
