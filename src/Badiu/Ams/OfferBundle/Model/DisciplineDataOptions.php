<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use  Badiu\Ams\OfferBundle\Model\DOMAINTABLE;
class DisciplineDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
     public function get($key){ 
        
         if($key=='ctype') return $this->getCalssType();
         return array();
     }
    public  function getCalssType(){
        $list = array();
        $list['offer']=$this->getTranslator()->trans('badiu.ams.offer.class.type.offer');
        $list['discipline']=$this->getTranslator()->trans('badiu.ams.offer.class.type.discipline');
        return $list;
    }

    public  function getCalssePatterName(){
        $list = array();
        $list[DOMAINTABLE::$DISCIPLINE_CLAESS_PATTER_NAME_ABC]=$this->getTranslator()->trans('badiu.ams.offer.discipline.classpatternnameabc');
        $list[DOMAINTABLE::$DISCIPLINE_CLAESS_PATTER_NAME_III]=$this->getTranslator()->trans('badiu.ams.offer.discipline.classpatternnameiii');
        $list[DOMAINTABLE::$DISCIPLINE_CLAESS_PATTER_NAME_123]=$this->getTranslator()->trans('badiu.ams.offer.discipline.classpatternname123');
        return $list;
    }
    
}
