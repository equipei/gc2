<?php

namespace Badiu\Ams\OfferBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnrolClasseBatchgenerateFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
    private $countclasse=0;
	private $countcupon=0;
    public function saveExec() {
		$listclasse=$this->getClassList();
		
		$couponmanagerlib=$this->getContainer()->get('badiu.admin.coupon.core.managerlib');
		$param=$this->getParam();
		
		if(is_array($listclasse)){
			foreach ($listclasse as $row) {
				$classeid=	$this->getUtildata()->getVaueOfArray($row,'id');
				$param['moduleinstance']=$classeid;
				$this->countcupon+=$couponmanagerlib->generate($param);
				$this->countclasse++;
			}
		}

		
		return $this->countcupon;
	}
	
public function getClassList() {
		$param=$this->getParam();
		$entity=$this->getEntity();
		
		$wsql="";
		$isactiveclasse= $this->getUtildata()->getVaueOfArray($param,'enableclasse');
		$statusid= $this->getUtildata()->getVaueOfArray($param,'statusid');
		$typeid= $this->getUtildata()->getVaueOfArray($param,'typeid');
		$typemanagerid= $this->getUtildata()->getVaueOfArray($param,'typemanagerid');
		$shortname= $this->getUtildata()->getVaueOfArray($param,'shortname');
		$offerid= $this->getUtildata()->getVaueOfArray($param,'offerid');
		$modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		
		if($modulekey=='badiu.tms.offer.classe'){
			$wsql.=" AND f.shortname='tmsdefaultcourseoffer' ";
		}else {$wsql.=" AND f.id=$offerid  ";}
		
		$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		$classedefaultsqlfilter->setModuleKey($modulekey);
		$fparam=array('_classealias'=>'o','_classestatusalias'=>'s');
	   if($isactiveclasse){
		   $wsql.=$classedefaultsqlfilter->activefilter($fparam);
	   }else if ($isactiveclasse=="0"){
		   $wsql.=$classedefaultsqlfilter->inactivefilter($fparam);
	   }
	    if($statusid){$wsql.=" AND o.statusid = $statusid ";}
		if($typeid){$wsql.=" AND o.typeid = $typeid ";}
		if($typemanagerid){$wsql.=" AND o.typemanagerid = $typemanagerid ";}
		
		
		$classedata=$this->getContainer()->get('badiu.ams.offer.classe.data');
	    $sql = " SELECT o.id FROM BadiuAmsOfferBundle:AmsOfferClasse o JOIN o.statusid s  JOIN o.odisciplineid od JOIN od.offerid f JOIN BadiuAdminSelectionBundle:AdminSelectionProject ps WITH (o.id=ps.moduleinstance AND ps.modulekey='".$modulekey."') WHERE o.entity=:entity AND ps.analysiscriteria=:analysiscriteria $wsql ";
		
		
		$query = $classedata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity );
		$query->setParameter('analysiscriteria','badiuadmincoupon' );
		
		if($isactiveclasse || $isactiveclasse=="0"){ 
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		 }
				 
		$result = $query->getResult();
		return $result ;
	}

public function execResponse() {
		$this->setSuccessmessage($this->getTranslator()->trans('badiu.ams.offer.enrolclassecouponbatchgenerate.couponsgenarated',array('%amount%'=>$this->countcupon,'%classess%'=>$this->countclasse)));
		
		
		$outrsult=array('result'=>$this->getResultexec(),'message'=>$this->getSuccessmessage(),'urlgoback'=>$this->getUrlgoback());
		$this->getResponse()->setStatus("accept");
		$this->getResponse()->setMessage($outrsult);
		return $this->getResponse()->get();
	}
}
