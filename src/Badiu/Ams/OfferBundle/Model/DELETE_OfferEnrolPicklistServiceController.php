<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Badiu\Ams\CurriculumBundle\Model\DOMAINTABLE;
class OfferEnrolPicklistServiceController extends BadiuController{
    
     function __construct(Container $container) {
            parent::__construct($container);
       }
    
   public function extractData($filter,$report,$layout='indexCrud') {
          if($layout=='dashboard' ){
				$report->extractData($filter,'dashboard');
				
           }
         $list=array();
		 $list['registered']=array();
		 $list['unregistered']=array();
		 
		 $registered=array();
		foreach ($report->getDashboardData()['rows'][1]  as $row) {
			 $registered[$row['id']]=$row['firstname'];
		 }
		 $list['registered']=$registered;
		$unregistered=array();
		foreach ($report->getDashboardData()['rows'][2]  as $row) {
			 $unregistered[$row['id']]=$row['firstname'];
		 }
		 $list['unregistered']=$unregistered;
		 $rows=array('rows'=>array($list));
		$report->setDashboardData($rows);
          return $report;
    }
}
