<?php

namespace Badiu\Ams\OfferBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class DisciplineController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
   /*   
    public function setDefaultDataOnAddForm($dto) {
        
        $dto=parent::setDefaultDataOnAddForm($dto);
        $name=$dto->getDisciplinename();
        if(empty($name)){ 
            $dto->setDisciplinename($dto->getDisciplineid()->getName());
        }
   

    return $dto;
}
    */

 public function setDefaultDataOnOpenEditForm($dto) {
     
                   $lib=$this->getContainer()->get('badiu.ams.offer.discipline.libform');
		   $id=$this->getContainer()->get('request')->get('id');
		   $dto=$lib->getDataFormEdit($id);
                  
		  return $dto;
     }
	 public function save($data) {
                $lib=$this->getContainer()->get('badiu.ams.offer.discipline.libform');
		$data->setDto($lib->save($data));
                $this->setParentid($data->getDto()->getOfferid()->getId());
              
                return $data->getDto();
    }   
    /*
    public function addOfferidToDiscipline($dto,$dtoParent) {
      $dto->setOfferid($dtoParent);
        return $dto;
    }
     public function findOfferidInDiscipline($dto) {

        return $dto->getOfferid()->getId();
    }
	*/
	
	
}


