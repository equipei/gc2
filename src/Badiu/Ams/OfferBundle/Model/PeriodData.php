<?php

namespace Badiu\Ams\OfferBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class PeriodData extends BadiuDataBase {

    function __construct(Container $container, $bundleEntity) {
        parent::__construct($container, $bundleEntity);
    }

    public function findBySection($offerid, $section) {

        $sql = "SELECT  o FROM " . $this->getBundleEntity() . " o WHERE o.offerid=:offerid AND o.section =:section ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('offerid', $offerid);
        $query->setParameter('section', $section);
        $result = $query->getSingleResult();
        return $result;
    }

    public function existSection($offerid, $section) {
        $r = FALSE;
        $sql = "SELECT  COUNT(o.id) AS countrecord  FROM " . $this->getBundleEntity() . " o WHERE o.offerid=:offerid AND o.section =:section ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('offerid', $offerid);
        $query->setParameter('section', $section);
        $result = $query->getSingleResult();
        if ($result['countrecord'] > 0) {
            $r = TRUE;
        }
        return $r;
    }

    public function findByOffer() {

        //check if is client

        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit = $sysoperation->isEdit();
        if ($isEdit) {
            $id = $this->getContainer()->get('request')->get('id');
            if (empty($id))return null;
             $odisciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
             $sql = "SELECT  o FROM " . $this->getBundleEntity() . " o JOIN ".$odisciplinedata->getBundleEntity()." df WITH df.offerid=o.offerid WHERE df.id=:id ORDER BY o.section ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id', $id);

            $result = $query->getResult();
            return $result;
        } else {
            $externalclinet = $this->getContainer()->get('request')->get('_externalclinet');

            if ($externalclinet == 1) {
                //return $this->getFormChoiceByOffer();
                return $this->getFormChoiceByOfferMdlDiscipinesingleTemp();
            }

            $offerid = $this->getContainer()->get('request')->get('parentid');
            if (empty($offerid))
                return null;


            $sql = "SELECT  o FROM " . $this->getBundleEntity() . " o WHERE o.offerid=:offerid ORDER BY o.section ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('offerid', $offerid);

            $result = $query->getResult();
            return $result;
        }
    }
 /* review or delete*/
  /*  public function getFormChoiceByOffer() {

        $offerid = $this->getContainer()->get('request')->get('parentid');

        if (empty($offerid))
            return null;
        $sql = "SELECT  o.id,o.name FROM " . $this->getBundleEntity() . " o WHERE o.offerid=:offerid ORDER BY o.section ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('offerid', $offerid);

        $result = $query->getResult();

        return $result;
    }*/

    //review and delete after
        public function getFormChoiceByOfferMdlDiscipinesingleTemp() {

        $offerid = $this->getContainer()->get('request')->get('parentid');

        if (empty($offerid))
            return null;
        $sql = "SELECT  o.id,o.name FROM " . $this->getBundleEntity() . " o WHERE o.offerid=:offerid ORDER BY o.section ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('offerid', $offerid);

        $result = $query->getResult();

        return $result;
    }

    //review 
   public function getFormChoiceByOffer() {

        //check if is client

        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit = $sysoperation->isEdit();
        if ($isEdit) {
            $id = $this->getContainer()->get('request')->get('id');
            if (empty($id))return null;
             $odisciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
             $sql = "SELECT o.id,o.name FROM " . $this->getBundleEntity() . " o JOIN ".$odisciplinedata->getBundleEntity()." df WITH df.offerid=o.offerid WHERE df.id=:id ORDER BY o.section ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id', $id);

            $result = $query->getResult();
            return $result;
        } else {
            $externalclinet = $this->getContainer()->get('request')->get('_externalclinet');

            if ($externalclinet == 1) {
                return $this->getFormChoiceByOffer();
            }

            $offerid = $this->getContainer()->get('request')->get('parentid');
            if (empty($offerid))
                return null;


            $sql = "SELECT  o FROM " . $this->getBundleEntity() . " o WHERE o.offerid=:offerid ORDER BY o.section ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('offerid', $offerid);

            $result = $query->getResult();
            return $result;
        }
    } 

}
