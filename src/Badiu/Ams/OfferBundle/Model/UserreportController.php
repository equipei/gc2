<?php

namespace Badiu\Ams\OfferBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class UserreportController extends BadiuController
{ 
    function __construct(Container $container) {
            parent::__construct($container);
              }
      
         public function setDefaultDataOnSearchForm($dto) {
          $defaultData=$this->getDefaultdata();

          if(!empty($defaultData)){
              foreach ($defaultData as $key => $value){
                  $dto[$key]=$value;
              }  
          }
          $listdep=$this->getEnterpriseDepartment();
          $dto['moduleinstance']=$listdep;
          $dto['drequired']=TRUE;
          
        return $dto;
    }

    public function getEnterpriseDepartment() {
          
            $userid=$this->getContainer()->get('request')->get('parentid'); 
            $dmembersdata=$this->getContainer()->get('badiu.admin.enterprise.dmembers.data');
            $sql="SELECT d.id FROM ".$dmembersdata->getBundleEntity()." o JOIN o.departmentid d WHERE o.userid=:parentid";
            $query = $dmembersdata->getEm()->createQuery($sql);
            $query->setParameter('parentid',$userid);
            $result= $query->getResult();
            
            $list=array();
            if(!empty($result)){
              $cont=0;
              foreach ($result as $row) {
                  $list[$cont]=$row['id'];
                  $cont++;
              }
            }
          
            return  $list;
      
    }

}
