<?php

namespace Badiu\Ams\OfferBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class OfferImporCurriculum extends BadiuModelLib{
    
     private $param ;
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
    public function exec() {
               if(empty($this->param)){return null;}
               $disciplinelist=$this->getDisciplinesFromCurriculum();
               if(empty($disciplinelist)){return null;}
               $cont=0;
			   $this->copySection();
               $perioddto=$this->getPeriodOffer();
               $offerid=$this->getUtildata()->getVaueOfArray($this->param, 'offerid');
               $periodtype = $this->getUtildata()->getVaueOfArray($this->param, 'periodoffer');
              
               foreach ($disciplinelist as $row) {
                     $sectionsequence=$this->getUtildata()->getVaueOfArray($this->param, 'sectionsequence');
                    if($periodtype=='section' && !empty($sectionsequence)){
                        $perioddto=$this->getPeriodSection();
                    }
                    $result=$this->addDiscipline($row,$perioddto);
                    if($result){$cont++;}
               }
              return  $cont;
             }
     public function getDisciplinesFromCurriculum() {
        $param=$this->param;
     
        $data=$this->getContainer()->get('badiu.ams.curriculum.discipline.data');
        $offerid=$this->getUtildata()->getVaueOfArray($this->param, 'offerid');
        $curriculumid=$this->getContainer()->get('badiu.ams.offer.offer.data')->getCurriculumid($offerid);
        
        if(empty($curriculumid)){return 0;}
        $wsql = "";
        
                     
        $sectionsids = $this->getUtildata()->getVaueOfArray($param, 'sectionsids');
        $groupids = $this->getUtildata()->getVaueOfArray($param, 'groupids');
        $drequired = $this->getUtildata()->getVaueOfArray($param, 'drequired');
     
        if (!empty($sectionsids)) {
            $wsql .= " AND st.id IN ($sectionsids) ";
        }
        if (!empty($groupids)) {
            $wsql .= " AND g.id IN ($groupids) ";
        }
        if ($drequired =="0") {$wsql .= " AND o.drequired = FALSE";}
        else  if($drequired =="1") {$wsql .= " AND o.drequired = TRUE ";}
        $sql = "SELECT o.id,o.entity,o.disciplinename,o.sortorder,o.drequired,o.dhour,o.credit,o.description,o.summary,o.teachingplan,d.id AS originaldiscipineid,d.name AS originaldisciplinename,st.section AS sectionsequence,st.name AS sectionname  FROM " . $data->getBundleEntity() . " o JOIN o.disciplineid d  LEFT JOIN o.sectionid st LEFT JOIN o.groupid g WHERE  o.entity=:entity AND o.curriculumid= :curriculumid AND o.deleted=:deleted $wsql  ";
        $query = $data->getEm()->createQuery($sql); 
        $query->setParameter('entity', $this->getEntity());
        $query->setParameter('curriculumid', $curriculumid);
        $query->setParameter('deleted', false);
        $result = $query->getResult();
        return $result;
    }
    function addDiscipline($dcrow,$perioddto) {
        $data=$this->getContainer()->get('badiu.ams.offer.discipline.data');
        $offerid=$this->getUtildata()->getVaueOfArray($this->param, 'offerid');
        $disciplineid=$this->getUtildata()->getVaueOfArray($dcrow, 'originaldiscipineid');
		$sectionsequence=$this->getUtildata()->getVaueOfArray($dcrow, 'sectionsequence');
        $exist=$data->existDiscipline($this->getEntity(), $offerid, $disciplineid);
       
        if($exist){
          
            $edto=$data->findDiscipline($this->getEntity(), $offerid, $disciplineid);
            $setup=$this->getContainer()->get('badiu.ams.offer.discipline.setup');
            $setup->exec($edto);
            return 0;
        }
        
	    $osectiondata=$this->getContainer()->get('badiu.ams.offer.section.data');
		
		$pfilter=array('entity'=>$this->getEntity(),'offerid'=>$offerid,'section'=>$sectionsequence);
		$sectionid=$osectiondata->getGlobalColumnValue("id",$pfilter);
		
        $param=array();
        $param['entity']=$this->getEntity();
        $param['offerid']=$this->getUtildata()->getVaueOfArray($this->param, 'offerid');
        $param['sectionid']=$sectionid;
        $param['sortorder']=$this->getUtildata()->getVaueOfArray($this->param, 'sortorder');
        $param['typeid']=$this->getUtildata()->getVaueOfArray($this->param, 'typeid');
        $param['disciplineid']=$this->getUtildata()->getVaueOfArray($dcrow, 'originaldiscipineid');
        $param['disciplinename']=$this->getUtildata()->getVaueOfArray($dcrow, 'disciplinename');
        $param['shortname']=null;
        $param['statusid']=$this->getContainer()->get('badiu.ams.offer.disciplinestatus.data')->getIdByShortname($this->getEntity(),'active');
        $param['categoryid']=$this->getUtildata()->getVaueOfArray($this->param, 'categoryid');
        $param['timestart']=$perioddto->timestart;
        $param['timeend']=$perioddto->timeend;
        $param['categoryid']=$this->getUtildata()->getVaueOfArray($this->param, 'categoryid');
        $param['drequired']=$this->getUtildata()->getVaueOfArray($dcrow, 'drequired');
        $param['maxenrol']=$this->getUtildata()->getVaueOfArray($this->param, 'maxenrol');
        $param['maxenrolperclass']=$this->getUtildata()->getVaueOfArray($this->param, 'maxenrolperclass');
        $param['maxclass']=$this->getUtildata()->getVaueOfArray($this->param, 'maxclass');
        $param['classpatternname']=$this->getUtildata()->getVaueOfArray($this->param, 'classpatternname');
        $param['maxgrade']=$this->getUtildata()->getVaueOfArray($this->param, 'maxgrade');
        $param['conclusiongrade']=$this->getUtildata()->getVaueOfArray($this->param, 'conclusiongrade');
        $param['numbergrade']=$this->getUtildata()->getVaueOfArray($this->param, 'numbergrade');
        $param['credit']=$this->getUtildata()->getVaueOfArray($dcrow, 'credit');
        $param['dhour']=$this->getUtildata()->getVaueOfArray($dcrow, 'dhour');
        $param['teachingplan']=$this->getUtildata()->getVaueOfArray($dcrow, 'teachingplan');
        $param['summary']=$this->getUtildata()->getVaueOfArray($dcrow, 'summary');
		$param['description']=$this->getUtildata()->getVaueOfArray($dcrow, 'description');
		$param['fversion']=1;
        
        $newdisciplineid = $data->insertNativeSql($param);
        
        $newdto=$data->findById($newdisciplineid);
        $setup=$this->getContainer()->get('badiu.ams.offer.discipline.setup');
        $setup->exec($newdto);
        return $newdisciplineid; 
    }
    function getPeriodOffer() {
        $dto=new \stdClass();
        $dto->timestart=null;
        $dto->timeend=null;
        $periodtype = $this->getUtildata()->getVaueOfArray($this->param, 'periodoffer');
        $offerid=$this->getUtildata()->getVaueOfArray($this->param, 'offerid');
        if(!empty($offerid)){return $dto;}
        if($periodtype=='offer'){
            $perioddto=$this->getContainer()->get('badiu.ams.offer.offer.data')->getPeriod($offerid);
             $dto->timestart=$this->getUtildata()->getVaueOfArray($perioddto, 'timestart');
            $dto->timeend=$this->getUtildata()->getVaueOfArray($perioddto, 'timeend');
        }
        return $dto;
    }
	function getPeriodSection() {
        $dto=new \stdClass();
        $dto->timestart=null;
        $dto->timeend=null;
        $periodtype = $this->getUtildata()->getVaueOfArray($this->param, 'periodoffer');
        $offerid=$this->getUtildata()->getVaueOfArray($this->param, 'offerid');
        if(!empty($offerid)){return $dto;}
        if($periodtype=='offer'){
            $perioddto=$this->getContainer()->get('badiu.ams.offer.offer.data')->getPeriod($offerid);
             $dto->timestart=$this->getUtildata()->getVaueOfArray($perioddto, 'timestart');
            $dto->timeend=$this->getUtildata()->getVaueOfArray($perioddto, 'timeend');
        }/*else if($periodtype=='section'){
            
        }*/
        return $dto;
    }
    
	function copySection(){
		 
		//get curriculumid
		 $offerid=$this->getUtildata()->getVaueOfArray($this->param, 'offerid');
         $curriculumid=$this->getContainer()->get('badiu.ams.offer.offer.data')->getCurriculumid($offerid);
		 
		 //get section by curriculum
		$csectiondata=$this->getContainer()->get('badiu.ams.curriculum.section.data');
		$osectiondata=$this->getContainer()->get('badiu.ams.offer.section.data');
		$coluns="o.section,o.name";
		$pfilter=array('entity'=>$this->getEntity(),'curriculumid'=>$curriculumid);
		$sections=$csectiondata->getGlobalColumnsValues($coluns,$pfilter);
		if(!is_array($sections)){return null;} 
		foreach ($sections as $row) {
			$section=$this->getUtildata()->getVaueOfArray($row, 'section');
			$name=$this->getUtildata()->getVaueOfArray($row, 'name');
			$paramcheckexist=array('entity'=>$this->getEntity(),'offerid'=>$offerid,'section'=>$section);
			$paramadd=array('entity'=>$this->getEntity(),'offerid'=>$offerid,'section'=>$section,'name'=>$name,'dtype'=>'course','deleted'=>0,'timecreated'=>new \DateTime());
			$paramedit=array('entity'=>$this->getEntity(),'offerid'=>$offerid,'section'=>$section,'name'=>$name,'dtype'=>'course','deleted'=>0,'timemodified'=>new \DateTime());
			$resultp=$osectiondata->addNativeSql($paramcheckexist,$paramadd,$paramedit);	
		}
		
		
	}
    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }


     
}
