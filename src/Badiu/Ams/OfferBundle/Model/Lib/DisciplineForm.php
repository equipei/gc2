<?php

namespace Badiu\Ams\OfferBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Badiu\Ams\CoreBundle\Model\DOMAINTABLE;
class DisciplineForm extends BadiuModelLib{
     
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
          	public function getDataFormEdit($disciplineid) {
                   
			$fdata=array();
                    	 $fdata=$this->getOdisciplineDb($disciplineid,$fdata);
			return $fdata;
	}
	public function save($data) {
         
			$sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
			$isEdit=$sysoperation->isEdit();
			$disciplineid=null;
			if($isEdit){
				$disciplineid=$this->getContainer()->get('request')->get('id');
			}
			$dto=$data->getDto();
			
			$data->setDto($this->getDisciplineForm($disciplineid,$dto));
			$data->save();
		
			return $data->getDto();
		}
	public function getOdisciplineDb($disciplineid,$fdata) {
			
                      
			$data=$this->getContainer()->get('badiu.ams.offer.discipline.data');
                        $dto=$data->findById($disciplineid);
                        //general=disciplineid,disciplinename,drequired,statusid,groupid,typeid,categoryid,maxenrol,description&
                        //
                        if(!empty($dto->getDisciplineid())){ $fdata['disciplineid']=$dto->getDisciplineid()->getId();}
                        $fdata['disciplinename']=$dto->getDisciplinename();
                        $fdata['drequired']=$dto->getDrequired();
                        if(!empty($dto->getStatusid())){ $fdata['statusid']=$dto->getStatusid()->getId();}
                        if(!empty($dto->getGroupid())){ $fdata['groupid']=$dto->getGroupid()->getId();}
                        if(!empty($dto->getCategoryid())){ $fdata['categoryid']=$dto->getCategoryid()->getId();}
                        $fdata['maxenrol']=$dto->getMaxenrol();
                        
                        //hourperiodinfo=periodid,timestart,timeend,dhour,credit
                         if(!empty($dto->getPeriodid())){ $fdata['periodid']=$dto->getPeriodid()->getId();}
                         $fdata['timestart']=$dto->getTimestart();
                         $fdata['timeend']=$dto->getTimeend();
                         $fdata['dhour']=$dto->getDhour();
                         $fdata['credit']=$dto->getCredit();
                         
                         //&disciplinegrade=numbergrade,maxgrade,conclusiongrade
                          $fdata['numbergrade']=$dto->getNumbergrade();
                          $fdata['maxgrade']=$dto->getMaxgrade();
                          $fdata['conclusiongrade']=$dto->getConclusiongrade();
                           
                         //&disciplineclass=maxclass,templateclass,classpatternname,maxenrolperclass
                         $fdata['maxclass']=$dto->getMaxclass();
                         $fdata['templateclass']=$dto->getTemplateclass();
                         $fdata['classpatternname']=$dto->getClasspatternname();
                         $fdata['maxenrolperclass']=$dto->getMaxenrolperclass();
                         
                         //&lmsmoodleintegration=sserviceid,lmssynctype,lmscontextid
                         if(!empty($dto->getSserviceid())){ $fdata['sserviceid']=$dto->getSserviceid()->getId();}
                         $fdata['lmssynctype']=$dto->getLmssynctype();
                         
                         $fdata['lmscontextidparent']=array();
                         $fdata['lmscontextidparent']['field1']=$dto->getLmscontexttypeparent();
                         $fdata['lmscontextidparent']['field2']=$this->getRemoteLmscontextidparent($dto);
                        // $fdata['lmscontextidparent']['field2']=$dto->getLmscontextidparent();
                         
                         $fdata['lmscontextid']=array();
                         $fdata['lmscontextid']['field1']=$dto->getLmscontexttype();
                         $fdata['lmscontextid']['field2']=$this->getRemoteLmscontextid($dto);
                         //$fdata['lmscontextid']['field2']=$dto->getLmscontextid();
                         
                         //&teachingplaninfo=summary
                          $fdata['summary']=$dto->getSummary();
                          $fdata['teachingplan']=$dto->getTeachingplan();
                          
                          //&otherconf=productintegration,shortname,idnumber,param
                          $fdata['productintegration']=$dto->getProductintegration();
                        
                          $fdata['shortname']=$dto->getShortname();
                          $fdata['idnumber']=$dto->getIdnumber();
			  $fdata['param']=$dto->getParam();
			  $fdata['description']=$dto->getDescription();
                        
                        
			return $fdata;
		
	}
	
	public function getDisciplineForm($disciplineid,$fdata) {
                     
                        $dto=null;
                        if(!empty($disciplineid)){
				$data=$this->getContainer()->get('badiu.ams.offer.discipline.data');
				$dto=$data->findById($disciplineid);
                               
			}else{
				$dto=$this->getContainer()->get('badiu.ams.offer.discipline.entity');
				$dto=$this->initDefaultEntityData($dto);
                                $offerid=$this->getContainer()->get('request')->get('parentid');
                                $dto->setOfferid($this->getContainer()->get('badiu.ams.offer.offer.data')->findById($offerid));
                        }
		
                        
                        //general=disciplineid,disciplinename,drequired,statusid,groupid,typeid,categoryid,maxenrol,description&
                        
                         if(isset($fdata['disciplineid'])) {$dto->setDisciplineid($this->getContainer()->get('badiu.ams.discipline.discipline.data')->findById($fdata['disciplineid']));}
                         if(isset($fdata['disciplinename'])) {$dto->setDisciplinename($fdata['disciplinename']);}
                         if(empty($dto->getDisciplinename())){$dto->setDisciplinename($dto->getDisciplineid()->getName());}
                         if(isset($fdata['drequired'])) {$dto->setDrequired($fdata['drequired']);}
                        
                        if(isset($fdata['statusid'])) {$dto->setStatusid($this->getContainer()->get('badiu.ams.offer.disciplinestatus.data')->findById($fdata['statusid']));}
                        if(isset($fdata['groupid'])) {$dto->setGroupid($this->getContainer()->get('badiu.ams.curriculum.group.data')->findById($fdata['groupid']));}
                        if(isset($fdata['categoryid'])) {$dto->setCategoryid($this->getContainer()->get('badiu.ams.offer.disciplinecategory.data')->findById($fdata['categoryid']));}
                        if(isset($fdata['maxenrol'])) {$dto->setMaxenrol($fdata['maxenrol']);}
                       
                        
                        //hourperiodinfo=periodid,timestart,timeend,dhour,credit
                        if(isset($fdata['periodid']))  {$dto->setPeriodid($this->getContainer()->get('badiu.ams.offer.period.data')->findById($fdata['periodid']));}
                         if(isset($fdata['timestart'])) {$dto->setTimestart($fdata['timestart']);}
                         if(isset($fdata['timeend'])) {$dto->setTimeend($fdata['timeend']);}
                         if(isset($fdata['dhour'])) {$dto->setDhour($fdata['dhour']);} 
                         if(isset($fdata['credit'])) {$dto->setCredit($fdata['credit']);} 
                     
                         //&disciplinegrade=numbergrade,maxgrade,conclusiongrade
                         if(isset($fdata['numbergrade'])) {$dto->setNumbergrade($fdata['numbergrade']);}
                         if(isset($fdata['maxgrade'])) {$dto->setMaxgrade($fdata['maxgrade']);}
                         if(isset($fdata['conclusiongrade'])) {$dto->setConclusiongrade($fdata['conclusiongrade']);}
                        
                           
                         //&disciplineclass=maxclass,templateclass,classpatternname,maxenrolperclass
                          if(isset($fdata['maxclass'])) {$dto->setMaxclass($fdata['maxclass']);}
                          if(isset($fdata['templateclass'])) {$dto->setTemplateclass($fdata['templateclass']);}
                          if(isset($fdata['classpatternname'])) {$dto->setClasspatternname($fdata['classpatternname']);}
                          if(isset($fdata['maxenrolperclass'])) {$dto->setMaxenrolperclass($fdata['maxenrolperclass']);}
                        
                         
                         //&lmsmoodleintegration=sserviceid,lmssynctype,lmscontextid
                          if(isset($fdata['sserviceid']))  {$dto->setSserviceid($this->getContainer()->get('badiu.admin.server.service.data')->findById($fdata['sserviceid']));}
                          if(isset($fdata['lmssynctype'])) {$dto->setLmssynctype($fdata['lmssynctype']);}
                        
                       
                          if(isset($fdata['lmscontextidparent']['field1'])) {$dto->setLmscontexttypeparent($fdata['lmscontextidparent']['field1']);}
                          if(isset($fdata['lmscontextidparent']['field2'])) {$dto->setLmscontextidparent($fdata['lmscontextidparent']['field2']);}
                          if(empty($dto->getLmscontexttypeparent())){$dto->setLmscontexttypeparent(DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY); }
                          
                          if(isset($fdata['lmscontextid']['field1'])) {$dto->setLmscontexttype($fdata['lmscontextid']['field1']);}
                          if(isset($fdata['lmscontextid']['field2'])) {$dto->setLmscontextid($fdata['lmscontextid']['field2']);}
                          if(empty($dto->getLmscontexttype())){$dto->setLmscontexttype(DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY); }
                         
                         //&teachingplaninfo=summary
                          if(isset($fdata['summary'])) {$dto->setSummary($fdata['summary']);}
                          if(isset($fdata['teachingplan'])) {$dto->setTeachingplan($fdata['teachingplan']);}
                          
                          
                          //&otherconf=productintegration,shortname,idnumber,param
                          if(isset($fdata['productintegration'])) {$dto->setProductintegration($fdata['productintegration']);}
                         
                        if(isset($fdata['param'])) {$dto->setParam($fdata['param']);}
			if(isset($fdata['shortname'])) {$dto->setShortname($fdata['shortname']);}
			if(isset($fdata['description'])) {$dto->setDescription($fdata['description']);}
                        if(isset($fdata['idnumber'])) {$dto->setIdnumber($fdata['idnumber']);}
                        
                       
			return $dto;
		
	} 
        
   public function getRemoteLmscontextid($dto) {
       if(empty($dto->getSserviceid())){return $dto->getLmscontextid();}
       if($dto->getLmscontexttype()==DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY && !empty($dto->getLmscontextid()) ){
           
              $param=array('_serviceid'=>$dto->getSserviceid(),'id'=>$dto->getLmscontextid(),'key'=>'course.category.getnametree');
              $result=  $this->getSearch()->searchWebService($param);
              if($result['status']=='accept'){
                  return $dto->getLmscontextid().' - '.$result['message'];
              }
             
       }else if($dto->getLmscontexttype()==DOMAINTABLE::$LMS_CONTEXT_COURSE && !empty($dto->getLmscontextid()) ){
           
              $param=array('_serviceid'=>$dto->getSserviceid(),'id'=>$dto->getLmscontextid(),'key'=>'course.course.getname');
              $result=  $this->getSearch()->searchWebService($param);
              if($result['status']=='accept'){
                  return $dto->getLmscontextid().' - '.$result['message'];
              }
             
       }
       return $dto->getLmscontextid();
   }     
   public function getRemoteLmscontextidparent($dto) {
          
       if(empty($dto->getSserviceid())){return $dto->getLmscontextidparent();}
     
       if($dto->getLmscontexttypeparent()==DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY && !empty($dto->getLmscontextidparent()) ){
       
              $param=array('_serviceid'=>$dto->getSserviceid(),'id'=>$dto->getLmscontextidparent(),'key'=>'course.category.getnametree');
              $result=  $this->getSearch()->searchWebService($param);
              if($result['status']=='accept'){
                  return $dto->getLmscontextidparent().' - '.$result['message'];
              }
             
       }else if($dto->getLmscontexttypeparent()==DOMAINTABLE::$LMS_CONTEXT_COURSE && !empty($dto->getLmscontextidparent()) ){
           
              $param=array('_serviceid'=>$dto->getSserviceid(),'id'=>$dto->getLmscontextidparent(),'key'=>'course.course.getname');
              $result=  $this->getSearch()->searchWebService($param);
              if($result['status']=='accept'){
                  return $dto->getLmscontextidparent().' - '.$result['message'];
              }
             
       }
       return $dto->getLmscontextidparent();
   }         
}
