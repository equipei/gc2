<?php

namespace Badiu\Ams\OfferBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class GradeItemIntegrationFilter extends BadiuFormFilter{
    private $modulekey;
	private $name;
	private $dtype='course';
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
   public function execBeforeSubmit() {

        }
        
   public function execAfterSubmit() {
	
	    if(empty($this->getModulekey())){return null;}
	    $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
		 if(empty($id)){return null;}
		 
		 $data=$this->getContainer()->get($this->getModulekey().'.data');
		 $dconfig=  $data->getGlobalColumnValue('dconfig',array('id'=>$id));
         $dconfig=$this->getJson()->decode($dconfig,true);
		
		 
		 $enable= $this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.enable',true);
		 $grademinperc= $this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.defaultroles.grade.minperc',true);
		 $lmscoursecompletation= $this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.defaultroles.lmscoursecompletation',true);
		
		$gradelibmanager= $this->getContainer()->get('badiu.admin.grade.lib.manager');
		$itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		
		 if($enable && $grademinperc){
			$fparam=array('entity'=>$this->getEntity(),'modulekey'=>$this->getModulekey(),'moduleinstance'=>$id,'itemtype'=>'finalgrade');
			$itemid=$itemdata->getGlobalColumnValue('id',$fparam); 
			if(empty($itemid)){
				$fiparam=array();
				$itemid=$gradelibmanager->addItem($fparam);
			}
		 }
		if	($enable && $lmscoursecompletation){
			//add finalgrade
			$fparam=array('entity'=>$this->getEntity(),'modulekey'=>$this->getModulekey(),'moduleinstance'=>$id,'itemtype'=>'finalgrade');
			$itemid=$itemdata->getGlobalColumnValue('id',$fparam); 
			if(empty($itemid)){
				$fiparam=array();
				$itemid=$gradelibmanager->addItem($fparam);
			}
		
			//add finalcopletation
			$fparam=array('entity'=>$this->getEntity(),'modulekey'=>$this->getModulekey(),'moduleinstance'=>$id,'itemtype'=>'finalcompletation');
			$itemid=$itemdata->getGlobalColumnValue('id',$fparam); 
			if(empty($itemid)){
				$fiparam=array();
				$itemid=$gradelibmanager->addItem($fparam);
			}
			
			//add progress
			$fparam=array('entity'=>$this->getEntity(),'modulekey'=>$this->getModulekey(),'moduleinstance'=>$id,'itemtype'=>'progress');
			$itemid=$itemdata->getGlobalColumnValue('id',$fparam); 
			if(empty($itemid)){
				$fiparam=array();
				$itemid=$gradelibmanager->addItem($fparam);
			}
			
			//add lmscourseaccess
			$fparam=array('entity'=>$this->getEntity(),'modulekey'=>$this->getModulekey(),'moduleinstance'=>$id,'itemtype'=>'lmscourseaccess');
			$itemid=$itemdata->getGlobalColumnValue('id',$fparam); 
			if(empty($itemid)){
				$fiparam=array();
				$itemid=$gradelibmanager->addItem($fparam);
			}
		 }
		
         
   }  
   public function addItem($param) {
	   
   }
	 function getModulekey() {
        return $this->modulekey;
    }

	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

 function getName() {
        return $this->name;
    }

	
	function setName($name) {
        $this->name = $name;
    }

	  /**
     * @return string
    */
	public function getDtype()
	{
		return $this->dtype;
	}
 
	/**
	 * @param string $dtype
	 */
	public function setDtype($dtype)
	{
		$this->dtype = $dtype;
	}
}
