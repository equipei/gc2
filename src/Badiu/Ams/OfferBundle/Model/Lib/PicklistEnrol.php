<?php

namespace Badiu\Ams\OfferBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Badiu\Ams\CurriculumBundle\Model\DOMAINTABLE; 
class PicklistEnrol extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function offer(){
            $add=$this->getContainer()->get('request')->get('add');  
            $remove=$this->getContainer()->get('request')->get('rmv');  
	 
            if (isset($add)) {
		//echo 'ADD';
		//print_r($add);
		$this->addOnOffer();
            }else if (isset($remove)) {
		//echo 'REMOVE';
		//print_r($remove);
                $this->removeOnOffer();
            }
	 		
    }
	
    
    public function discipline(){
            $add=$this->getContainer()->get('request')->get('add');  
            $remove=$this->getContainer()->get('request')->get('rmv');  
	 
            if (isset($add)) {
		//echo 'ADD discipline';
		
		$this->addOnDiscipline();
            }else if (isset($remove)) {
		//echo 'REMOVE';
		//print_r($remove);
                $this->removeOnDiscipline();
            }
	 		
    }
    
     public function classe(){
            $add=$this->getContainer()->get('request')->get('add');  
            $remove=$this->getContainer()->get('request')->get('rmv');  
	 
            if (isset($add)) {
		//echo 'ADD discipline';
		
		$this->addOnClasse();
            }else if (isset($remove)) {
		//echo 'REMOVE';
		//print_r($remove);
                $this->removeOnClasse();
            }
	 		
    }
    public function addOnOffer(){
                        $fdata=$_GET['add'];
			$offerid=$this->getContainer()->get('request')->get('parentid'); 
			$enroldata=$this->getContainer()->get('badiu.ams.enrol.offer.lib');
			foreach ($fdata as $data){
				$userid=null;
				$roleid=null;
				$statusid=null;
				$timestart=null;
				$timeend=null;
				if(isset($data['id'])){$userid=$data['id'];}
				if(isset($data['field1'])){$roleid=$data['field1'];}
				if(isset($data['field2'])){$statusid=$data['field2'];}
				
				if(!empty($userid) && !empty($roleid) && !empty($statusid) ){
					$enroldata->add($offerid,$userid,$roleid,$statusid,$timestart,$timeend);
					//echo "xuser $userid saved<br>";
				}
			}
			
			
	}
      public function removeOnOffer(){
                        $fdata=$_GET['rmv'];
			$enroldata=$this->getContainer()->get('badiu.ams.enrol.offer.data');
			foreach ($fdata as $id){
                               if(!empty($id)){
                                    $enroldata->remove($id);
                                }
				
			}
			
			
	}    
      public function addOnDiscipline(){
                        $fdata=$_GET['add'];
			$odisciplineid=$this->getContainer()->get('request')->get('parentid'); 
			$enroldata=$this->getContainer()->get('badiu.ams.enrol.discipline.lib');
			foreach ($fdata as $data){
				$userid=null;
				$roleid=null;
				$statusid=null;
				$timestart=null;
				$timeend=null;
				if(isset($data['id'])){$userid=$data['id'];}
				if(isset($data['field1'])){$roleid=$data['field1'];}
				if(isset($data['field2'])){$statusid=$data['field2'];}
				
				if(!empty($userid) && !empty($roleid) && !empty($statusid) ){
					$enroldata->add($odisciplineid,$userid,$roleid,$statusid,$timestart,$timeend);
					//echo "xuser $userid saved<br>";
				}
			}
			
			
	}
   public function removeOnDiscipline(){
                        $fdata=$_GET['rmv'];
			$enroldata=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
			foreach ($fdata as $id){
                               if(!empty($id)){
                                    $enroldata->remove($id);
                                }
				
			}
			
			
	}     
    public function addOnClasse(){
                        $fdata=$_GET['add'];
			$classid=$this->getContainer()->get('request')->get('parentid'); 
			$enroldata=$this->getContainer()->get('badiu.ams.enrol.classe.lib');
			foreach ($fdata as $data){
				$userid=null;
				$roleid=null;
				$statusid=null;
				$timestart=null;
				$timeend=null;
				if(isset($data['id'])){$userid=$data['id'];}
				if(isset($data['field1'])){$roleid=$data['field1'];}
				if(isset($data['field2'])){$statusid=$data['field2'];}
				
				if(!empty($userid) && !empty($roleid) && !empty($statusid) ){
					$enroldata->add($classid,$userid,$roleid,$statusid,$timestart,$timeend);
					//echo "xuser $userid saved<br>";
				}
			}
			
			
	}
       public function removeOnClasse(){
                        $fdata=$_GET['rmv'];
			$enroldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
			foreach ($fdata as $id){
                               if(!empty($id)){
                                    $enroldata->remove($id);
                                }
				
			}
			
			
	}     
        
}
