<?php

namespace Badiu\Ams\OfferBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class DisciplineMoodlePicklist extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function course(){
            $add=$this->getContainer()->get('request')->get('add');  
            $remove=$this->getContainer()->get('request')->get('rmv');  
	
            if (isset($add)) {
               
		$this->addCourse();
            }else if (isset($remove)) {
                foreach ($remove as $item){
                   $this->removeData($item);
                }   
		
            }
	 		
    }
	
    
    public function cohort(){
            $add=$this->getContainer()->get('request')->get('add');  
            $remove=$this->getContainer()->get('request')->get('rmv');  
	 
            if (isset($add)) {
		$this->addCohort();
            }else if (isset($remove)) {
		 foreach ($remove as $item){
                   $this->removeData($item);
                }
		
            }
	 		
    }
    
        public function exceptuser(){
            $add=$this->getContainer()->get('request')->get('add');  
            $remove=$this->getContainer()->get('request')->get('rmv');  
	 
            if (isset($add)) {
		$this->addExceptuser();
            }else if (isset($remove)) {
		 foreach ($remove as $item){
                   $this->removeData($item);
                }
		
            }
	 		
    }
    
    public function cohortnoaccompanying(){
            $add=$this->getContainer()->get('request')->get('add');  
            $remove=$this->getContainer()->get('request')->get('rmv');  
	 
            if (isset($add)) {
		$this->addCohortNoAccompanying();
            }else if (isset($remove)) {
		 foreach ($remove as $item){
                   $this->removeData($item);
                }
		
            }
	 		
    }
    
            public function globalexceptuser(){
            $add=$this->getContainer()->get('request')->get('add');  
            $remove=$this->getContainer()->get('request')->get('rmv');  
	 
            if (isset($add)) {
		$this->addGlobalExceptuser();
            }else if (isset($remove)) {
		 foreach ($remove as $item){
                   $this->removeData($item);
                }
		
            }
	 		
    }
    public function addCourse(){ 
                        $fdata=$this->getContainer()->get('request')->get('add');
			$disciplineid=$this->getContainer()->get('request')->get('parentid'); 
			$modueledata=$this->getContainer()->get('badiu.system.module.data.data');
			foreach ($fdata as $data){
                                $dto=$this->getContainer()->get('badiu.system.module.data.entity');
                                $dto=$this->initDefaultEntityData($dto);
				
                                
				$couresecategoryid=null;
                                $courseid=null;
				$coursename=null;
				$couresecategoryname=null;
                                 $courseid=$data['id'];
				if(isset($courseid)){
                                    $courseifo=$this->getMdlCourseInfo($courseid);
                                   // print_r($courseifo);
                                    if(isset($courseifo['course'])){$coursename=$courseifo['course'];}
                                    if(isset($courseifo['categoryid'])){$couresecategoryid=$courseifo['categoryid'];}
                                    if(isset($courseifo['category'])){$couresecategoryname=$courseifo['category'];}
                                }
				
				$dto->setCustomint1($couresecategoryid);
                                $dto->setCustomint2($courseid);
                                $dto->setCustomchar1($couresecategoryname);
                                $dto->setCustomchar2($coursename);
				$dto->setBkey('badiu.ams.offer');
                                $dto->setModulekey('badiu.ams.offer.discipline');
                                $dto->setModuleinstance($disciplineid);
                                $dto->setDtype('moodle.course');
                                $modueledata->setDto($dto);
				$modueledata->save();
                                
			}
			
			
	}
        
      public function addCohort(){
                    $fdata=$this->getContainer()->get('request')->get('add');
			$disciplineid=$this->getContainer()->get('request')->get('parentid'); 
			$modueledata=$this->getContainer()->get('badiu.system.module.data.data');
			foreach ($fdata as $data){
                                $dto=$this->getContainer()->get('badiu.system.module.data.entity');
                                $dto=$this->initDefaultEntityData($dto);
				
                                $cohortname=null;
				$cohortid=$data['id'];
				if(isset($cohortid)){
                                    $cohortname=$this->getMdlCohortInfo($cohortid);
                                  }
			
				$dto->setCustomint1($cohortid);
                                $dto->setCustomchar1($cohortname);
                                $dto->setBkey('badiu.ams.offer');
                                $dto->setModulekey('badiu.ams.offer.discipline');
                                $dto->setModuleinstance($disciplineid);
                                $dto->setDtype('moodle.cohort');
                                $modueledata->setDto($dto);
				$modueledata->save();
                                
			}
			    
	}

public function addCohortNoAccompanying(){
                    $fdata=$this->getContainer()->get('request')->get('add');
			$disciplineid=$this->getContainer()->get('request')->get('parentid'); 
			$modueledata=$this->getContainer()->get('badiu.system.module.data.data');
			foreach ($fdata as $data){
                                $dto=$this->getContainer()->get('badiu.system.module.data.entity');
                                $dto=$this->initDefaultEntityData($dto);
				
                                $cohortname=null;
				$cohortid=$data['id'];
				if(isset($cohortid)){
                                    $cohortname=$this->getMdlCohortInfo($cohortid);
                                  }
			
				$dto->setCustomint1($cohortid);
                                $dto->setCustomchar1($cohortname);
                                $dto->setBkey('badiu.ams.offer');
                                $dto->setModulekey('badiu.ams.offer.discipline');
                                $dto->setModuleinstance($disciplineid);
                                $dto->setDtype('moodle.cohort.noaccompanying');
                                $modueledata->setDto($dto);
				$modueledata->save();
                                
			}
			    
	}        
  public function addExceptuser(){
                    $fdata=$this->getContainer()->get('request')->get('add');
			$disciplineid=$this->getContainer()->get('request')->get('parentid'); 
			$modueledata=$this->getContainer()->get('badiu.system.module.data.data');
			foreach ($fdata as $data){
                                $dto=$this->getContainer()->get('badiu.system.module.data.entity');
                                $dto=$this->initDefaultEntityData($dto);
				
                                $user=null;
				$userid=$data['id'];
				if(isset($userid)){
                                    $user=$this->getMdlExceptuserInfo($userid);
                                  }
				
				$dto->setCustomint1($userid);
                                $dto->setCustomchar1($user);
                                $dto->setBkey('badiu.ams.offer');
                                $dto->setModulekey('badiu.ams.offer.discipline');
                                $dto->setModuleinstance($disciplineid);
                                $dto->setDtype('moodle.exceptuser');
                                $modueledata->setDto($dto);
				$modueledata->save();
                                
			}
			    
	}
   public function addGlobalExceptuser(){
                    $fdata=$this->getContainer()->get('request')->get('add');
			$disciplineid=$this->getContainer()->get('request')->get('parentid'); 
			$modueledata=$this->getContainer()->get('badiu.system.module.data.data');
			foreach ($fdata as $data){
                                $dto=$this->getContainer()->get('badiu.system.module.data.entity');
                                $dto=$this->initDefaultEntityData($dto);
				
                                $user=null;
				$userid=$data['id'];
				if(isset($userid)){
                                    $user=$this->getMdlExceptuserInfo($userid);
                                  }
				
				$dto->setCustomint1($userid);
                                $dto->setCustomchar1($user);
                                $dto->setBkey('badiu.ams.offer');
                                $dto->setModulekey('badiu.ams.offer.offer');
                                $dto->setModuleinstance($disciplineid);
                                $dto->setDtype('moodle.exceptuser');
                                $modueledata->setDto($dto);
				$modueledata->save();
                                
			}
			    
	}
  
        public function getMdlCourseInfo($courseid=11){
        $param = array();
        $param['courseid']=$courseid;
        $this->getSearch()->getKeymanger()->setBaseKey('badiu.ems.offer.disciplinemdlcourseinfo');
        $result = $this->getSearch()->searchListSingle($param);
       //  print_r($result[1]);exit;
        if(isset($result[1])){$result=$result[1];}
        return $result;
    }  
    
     public function getMdlCohortInfo($cohortid){
        $param = array();
        $param['cohortid']=$cohortid;
        $this->getSearch()->getKeymanger()->setBaseKey('badiu.ems.offer.disciplinemdlcohortinfo');
        $result = $this->getSearch()->searchListSingle($param);
      
        if(isset($result[1]['name'])){$result=$result[1]['name'];}
        return $result;
    }  
     public function getMdlExceptuserInfo($userid){
        $param = array();
        $param['userid']=$userid;
        $this->getSearch()->getKeymanger()->setBaseKey('badiu.ems.offer.disciplinemdlexceptuserinfo');
        $result = $this->getSearch()->searchListSingle($param);
      
        if(isset($result[1]['name'])){$result=$result[1]['name'];}
        return $result;
    }  
    
    public function removeData($id){
        $data=$this->getContainer()->get('badiu.system.module.data.data');
        $data->remove($id);
    }
}
