<?php

namespace Badiu\Ams\OfferBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class DisciplineSetup extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function exec($odisciplineDto) {
        if(empty($odisciplineDto)){return null;}
        //add grade
        $this->addGradeItem($odisciplineDto);
        //add classe
        $this->addClass($odisciplineDto);
        return $odisciplineDto;
    }

    /**
     * On create offer discipline add the gradeitem of discipline offer
     */
    public function addGradeItem($odisciplineDto) {
        
        //check if exist create 
            $itemdata = $this->getContainer()->get('badiu.ams.grade.item.data');
            $existfinalgradeitem = $itemdata->existFinalItem($this->getEntity(), $odisciplineDto->getId());
           
            if (!$existfinalgradeitem) {
                 $this->creadGradeItem($odisciplineDto, 'discipline');
            }
            $existactivitygradeitems = $itemdata->existItem($this->getEntity(), $odisciplineDto->getId(), 'activity');
           
            if (!$existactivitygradeitems) {
              
                if (!empty($odisciplineDto->getNumbergrade())) {
                    $cont = 0;
                    for ($i = 0; $i < $odisciplineDto->getNumbergrade(); $i++) {
                        $cont++;
                       $this->creadGradeItem($odisciplineDto, 'activity', $cont);
                    }
                }
            }
     
    }

    public function creadGradeItem($odisciplineDto, $type, $seq = null) {
     
        $name = 'finalgrade';
        if ($type == 'finalgrade') {
            $name = $this->getTranslator()->trans('badiu.ams.offer.discipline.defaulnameofinalgradeitem');
        } else if ($type == 'activity') {
            $name = $this->getTranslator()->trans('badiu.ams.offer.discipline.defaulnameseqofgradeitem', array('%sequence%' => $seq));
        }

        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $entity = $badiuSession->get()->getEntity();
        //$scaleData=$this->getContainer()->get('badiu.ams.grade.scale.data');
        //$scaleDto=$scaledate->findByShortname($entity,'numeric');

        $gradeItemDto = $this->getContainer()->get('badiu.ams.grade.item.entity');
        $gradeItemDto = $this->initDefaultEntityData($gradeItemDto);
        $gradeItemData = $this->getContainer()->get('badiu.ams.grade.item.data');
        //$gradeItemDto->setScaleid($scaleDto);
        $gradeItemDto->setModuleinstance($odisciplineDto->getId());
		$gradeItemDto->setModulekey('badiu.ams.offer.discipline');
        $gradeItemDto->setName($name);
        $gradeItemDto->setItemtype($type);
        $gradeItemDto->setGradetype(1);
        $gradeItemDto->setMaxgrade($odisciplineDto->getMaxgrade());
        $gradeItemDto->setMingrade(0.00);
        $gradeItemData->setDto($gradeItemDto);

        $gradeItemData->save();
    }

    public function addClass($odisciplineDto) {
        
            $classdata = $this->getContainer()->get('badiu.ams.offer.classe.data');
            if ($odisciplineDto->getTemplateclass()) {
                $existemplate=$classdata->existTemplate($this->getEntity(), $odisciplineDto->getId(), true);
                if (!$existemplate) {
                    $this->createClass($odisciplineDto, true);
                }
            }
            
            if (!empty($odisciplineDto->getMaxclass())) {
                $cont = 0;
                $existclass = $classdata->existTemplate($this->getEntity(), $odisciplineDto->getId(), false);
                if (!$existclass) {
                 
                    for ($i = 0; $i < $odisciplineDto->getMaxclass(); $i++) {
                         $cont++;
                         $this->createClass($odisciplineDto, false, $cont);
                        if($cont > 30){break;}
                    }
                }
            }

           
       
    }

    public function createClass($odisciplineDto, $template = false, $seq = null) {
        $name = null;
        if ($template) {
            $name = $this->getTranslator()->trans('badiu.ams.offer.discipline.defaulnameseqofclassteplate');
        } else {
            $typeseq = "ABC";
            $typeseq = $odisciplineDto->getClasspatternname();
            if (empty($typeseq)) {
                $typeseq = "ABC";
            }
            $seqlib = $this->getContainer()->get('badiu.system.core.lib.util.sequence');
            $nexseq = $seqlib->next($seq, $odisciplineDto->getClasspatternname());
            $name = $this->getTranslator()->trans('badiu.ams.offer.discipline.defaulnameseqofclass', array('%sequence%' => $nexseq));
        }

        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $entity = $badiuSession->get()->getEntity();

        $classDto = $this->getContainer()->get('badiu.ams.offer.classe.entity');
        $classDto = $this->initDefaultEntityData($classDto);
        $classData = $this->getContainer()->get('badiu.ams.offer.classe.data');
        $classDto->setOdisciplineid($odisciplineDto);
        $classDto->setName($name);
        $classDto->setCtemplate($template);
        $classData->setDto($classDto);
        $classData->save();

        return $classData->getDto();
    }

}
 