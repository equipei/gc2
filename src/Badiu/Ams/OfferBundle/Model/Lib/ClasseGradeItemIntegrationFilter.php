<?php

namespace Badiu\Ams\OfferBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class ClasseGradeItemIntegrationFilter extends GradeItemIntegrationFilter{
    private $modulekey;

    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
        
   public function execAfterSubmit() {
	 
	   $this->setModulekey('badiu.ams.offer.classe');
	 
	   parent::execAfterSubmit();
	  
   } 
   
}
