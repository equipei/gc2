<?php

namespace Badiu\Ams\OfferBundle\Model\Lib\Service;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
class Discipline extends BadiuExternalService{

    /**
     * @var Container
     */
    private $container;

	private $response;
	function __construct(Container $container)
    {
        parent::__construct($container);
		$this->response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
      }

	public function exec()
    {	
			return 'no functon defined';
    }
	
	
	public function isSyncWithLms()
	{ 
		//check token
		$ckeckToken=$this->checkToken();
		if(!$ckeckToken){return $this->getResponse()->get();}
		
		//get param
		$param=$this->getParamSyncWithLms();
		$chekParam=$this->checkParamSyncWithLms($param);
		if(!$chekParam){return $this->getResponse()->get();}
		
		$serviceid=$this->getServiceid();
		$dtypelms='course';
		$data=$this->getContainer()->get('badiu.ams.offer.discipline.data');
		$entity=$this->getEntity();
		$idnumberlms=$param->courseid;
		$resul=$data->isSyncWithLms($entity,$serviceid,$dtypelms,$idnumberlms);
		$resp="false";
		if($resul){$resp="true";}
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($resp);
		
		return $this->getResponse()->get();
    }
	public function isSyncWithLmsByOffer()
	{ 
		//check token
		$ckeckToken=$this->checkToken();
		if(!$ckeckToken){return $this->getResponse()->get();}
		
		//get param
		$param=$this->getParamSyncWithLms();
		$chekParam=$this->checkParamSyncWithLms($param,true);
		if(!$chekParam){return $this->getResponse()->get();}
		
		$serviceid=$this->getServiceid();
		$dtypelms='course';
		$data=$this->getContainer()->get('badiu.ams.offer.discipline.data');
		$entity=$this->getEntity();
		$idnumberlms=$param->courseid;
		$offerid=$param->offerid;
		$resul=$data->isSyncWithLmsByOffer($entity,$offerid,$serviceid,$dtypelms,$idnumberlms);
		$resp="false";
		if($resul){$resp="true";}
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($resp);
		
		return $this->getResponse()->get();
    }
	public function getIdByLmsByOffer()
	{ 
		//check token
		$ckeckToken=$this->checkToken();
		if(!$ckeckToken){return $this->getResponse()->get();}
		
		//get param
		$param=$this->getParamSyncWithLms();
		$chekParam=$this->checkParamSyncWithLms($param,true);
		if(!$chekParam){return $this->getResponse()->get();}
		
		$serviceid=$this->getServiceid();
		$dtypelms='course';
		$data=$this->getContainer()->get('badiu.ams.offer.discipline.data');
		$entity=$this->getEntity();
		$idnumberlms=$param->courseid;
		$offerid=$param->offerid;
		$resul=$data->getIdByLmsByOffer($entity,$offerid,$serviceid,$dtypelms,$idnumberlms);
		
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($resul);
		
		return $this->getResponse()->get();
    }
	public function getParamSyncWithLms(){
		$param= new \stdClass();
		$param->courseid=$this->getContainer()->get('request')->get('courseid');
		$param->offerid=$this->getContainer()->get('request')->get('offerid');
		return $param;
	}
	public function checkParamSyncWithLms($param,$checkOffer=false){
		$resp=TRUE;
		
		if(empty($param->courseid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.offer.discipline.courseid.is.empty');
			return $resp;
		}
		if($checkOffer && empty($param->offerid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.offer.discipline.offerid.is.empty');
			return $resp;
		}
		
		return $resp;
	}
	
        
        public function save(){
		//check token
		$ckeckToken=$this->checkToken();
		if(!$ckeckToken){return $this->getResponse()->get();}
		
		//get param
		$dto=$this->getSaveParam();
		if(!is_object($dto))return $this->getResponse()->get();
		
		$data=$this->getContainer()->get('badiu.ams.offer.discipline.data');
		$data->setDto($dto);
		$result=$data->save();
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($data->getDto()->getId());
		
		return $this->getResponse()->get();	
	}
        
        public function getSaveParam(){
		$dto=null;
                $isedit=false;
                $id=$this->getContainer()->get('request')->get('id');
                if(!empty($id)){
                    $data=$this->getContainer()->get('badiu.ams.offer.discipline.data');
                    $dto=$data->findById($id);
                    $isedit=true;
                }
                if(!$isedit){
                   $dto=$this->getContainer()->get('badiu.ams.offer.discipline.entity');
		$dto=$this->initDefaultEntityData($dto); 
                }
                
		
                //offerid
           	$offerid=$this->getContainer()->get('request')->get('parentid');
		if(!empty($offerid)) {$dto->setOfferid($this->getContainer()->get('badiu.ams.offer.offer.data')->findById($offerid));}
		else{
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.offer.offer.offerid.is.empty');
			return TRUE;
		}
                //periodid
		$periodid=$this->getContainer()->get('request')->get('periodid');
		if(!empty($periodid)) {$dto->setPeriodid=($this->getContainer()->get('badiu.ams.offer.period.data')->findById($periodid));}
		
		 //groupid
		$groupid=$this->getContainer()->get('request')->get('groupid');
		if(!empty($groupid)) {$dto->setGroupid($this->getContainer()->get('badiu.ams.curriculum.group.data')->findById($groupid));}
		
                 //typeid
		$typeid=$this->getContainer()->get('request')->get('typeid');
		if(!empty($typeid)) {$dto->setTypeid($this->getContainer()->get('badiu.ams.offer.type.data')->findById($typeid));}
		
                 //disciplineid
		$disciplineid=$this->getContainer()->get('request')->get('disciplineid');
		if(!empty($disciplineid)) {$dto->setDisciplineid($this->getContainer()->get('badiu.ams.discipline.discipline.data')->findById($disciplineid));}
		
                //disciplinename
		$disciplinename=$this->getContainer()->get('request')->get('disciplinename');
		if(!empty($disciplinename)){$dto->setDisciplinename($disciplinename);}
                else{
                    $disciplinename=$dto->setDisciplineid()->getName();
                    $dto->setDisciplinename($disciplinename);
                }
                
                $maxenrol=$this->getContainer()->get('request')->get('maxenrol');
                if($maxenrol>=0){$dto->setMaxenrol($maxenrol);}
                
                $dto->setShortname($this->getContainer()->get('request')->get('shortname'));
                
                //hroomid
		$hroomid=$this->getContainer()->get('request')->get('hroomid');
		if(!empty($hroomid)) {$dto->setHroomid($this->getContainer()->get('badiu.util.housing.room.data')->findById($hroomid));}
		
                //hroomid
		$sserviceid=$this->getContainer()->get('request')->get('sserviceid');
		if(!empty($sserviceid)) {$dto->setSserviceid($this->getContainer()->get('badiu.admin.server.service.data')->findById($sserviceid));}
		
                  //statusid
		$statusid=$this->getContainer()->get('request')->get('statusid');
		if(!empty($statusid)) {$dto->setStatusid($this->getContainer()->get('badiu.ams.offer.disciplinestatus.data')->findById($statusid));}
		
                //categoryid
		$categoryid=$this->getContainer()->get('request')->get('categoryid');
		if(!empty($categoryid)) {$dto->setCategoryid($this->getContainer()->get('badiu.ams.offer.disciplinecategory.data')->findById($categoryid));}
		
                //timestart
                $timestart=$this->getContainer()->get('request')->get('timestart');
                if($timestart > 0 ){
                    $dto->setTimestart(new \Datetime());
                    $dto->getTimestart()->setTimestamp($timestart);
                }
		
                //timeend
                $timeend=$this->getContainer()->get('request')->get('timeend');
                if($timeend > 0 ){
                    $dto->setTimeend(new \Datetime());
                    $dto->getTimeend()->setTimestamp($timeend);
                }
                
                //drequired
                $drequired=$this->getContainer()->get('request')->get('drequired');
                if($drequired!=null){$dto->setDrequired($drequired);}
                
                //drequired
                $drequired=$this->getContainer()->get('request')->get('drequired');
                if($drequired!=null){$dto->setDrequired($drequired);}
                
                $conclusiongrade=$this->getContainer()->get('request')->get('conclusiongrade');
                if(!empty($conclusiongrade) && is_numeric($conclusiongrade)){$dto->setConclusiongrade($conclusiongrade);}
                
               //$dto->setMeasuretype($this->getContainer()->get('request')->get('measuretype'));
                
                $measurevalue=$this->getContainer()->get('request')->get('measurevalue');
                if(!empty($measurevalue) && is_numeric($measurevalue)){$dto->setMeasurevalue($measurevalue);}
                
                $dto->setDhour($this->getContainer()->get('request')->get('dhour'));
		$dto->setTeachingplan($this->getContainer()->get('request')->get('teachingplan'));
		$dto->setSummary($this->getContainer()->get('request')->get('summary'));
		$dto->setModulekey($this->getContainer()->get('request')->get('modulekey'));
                
                $moduleinstance=$this->getContainer()->get('request')->get('moduleinstance');
                if(!empty($moduleinstance) && is_numeric($moduleinstance)){$dto->setModuleinstance($moduleinstance);}
                
                
		//$dto->setIdnumber($this->getContainer()->get('request')->get('idnumber')); //problem with cache navegator
                $dto->setRoomaddress($this->getContainer()->get('request')->get('roomaddress'));
                $dto->setDescription($this->getContainer()->get('request')->get('description'));
                $dto->setParam($this->getContainer()->get('request')->get('param'));
                if(!$isedit){
                    $dto->setTimecreated(new \Datetime());
                    $dto->setdeleted(FALSE);
                }
                
				
		return $dto;
	}
}
?>