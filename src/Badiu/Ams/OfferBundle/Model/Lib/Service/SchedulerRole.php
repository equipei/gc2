<?php

namespace Badiu\Ams\OfferBundle\Model\Lib\Service;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
class SchedulerRole extends BadiuExternalService{

    /**
     * @var Container
     */
    private $container;

	private $response;
	function __construct(Container $container)
    {
        parent::__construct($container);
	 }

	public function exec()
    {	
		$contr=0;
		$conte=0;
		$roles=$this->getListOfRole();
		foreach ($roles as $role) {
			$date=time();
			if(!empty($role->getCustomint1())){
				$timeday=$role->getCustomint1()*86400;
				$date=$date-$timeday;
				$fdate = new \DateTime();
				$fdate->setTimestamp($date);
				$enrols=$this->getListDisciplineEnrol($role->getModuleinstance(),$fdate);
				
				$contr++;
				foreach ($enrols as $enrol) {
					$this->sendMessage($role,$enrol);
					$conte++;
				}
				
			}
			
		}
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($contr."/".$conte);
		return $this->getResponse()->get();
    }
	
	
	public function getListOfRole()
	{ 
		$reporttype='badiu.ams.offer.enrol.discipline.report';
		$data=$this->getContainer()->get('badiu.system.scheduler.role.data');
		$roles=$data->getByReporttype($this->getEntity(),$reporttype);
		return $roles;
    }
	public function getListDisciplineEnrol($offerid,$fdate)
	{ 
		$data=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
		$enrols=$data->getListByDate($offerid,$fdate);
		return $enrols;
    }
	public function sendMessage($role,$enrol)
	{ 
			$message = \Swift_Message::newInstance()
             ->setContentType("text/html")
             ->setSubject($role->getSendsubject())
             ->setFrom($this->getContainer()->getParameter('mail.address.from'))
             ->setTo($enrol->getUserid()->getEmail())
             ->setBody($role->getSendmessage()); 
             $this->getContainer()->get('mailer')->send($message);
    }
	
	
}
?>