<?php

namespace Badiu\Ams\OfferBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Badiu\Ams\CurriculumBundle\Model\DOMAINTABLE; 
class Offer extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    

	
     public function copyCurriculum($offerDto) {

            //create period
            $this->criatePeriod($offerDto);
     		//get curriculum id
     		$curriculumid=$offerDto->getCurriculumid()->getId();
            //echo "<br>curriculumid: $curriculumid";
     		//get disciplines of curriculum
     		$curriculumdata=$this->getContainer()->get('badiu.ams.curriculum.discipline.data');
     		$disciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
     		$perioddata=$this->getContainer()->get('badiu.ams.offer.period.data');
            $disciplines=$curriculumdata->getAllDisciplinesByCurriculum($curriculumid);
     		
            //add disciplines in offer
     		 foreach ($disciplines as $discipline) {
     		 	$period= null;
                $section=$discipline->getSection();
               if(!empty($section)){
                   $period=  $perioddata->findBySection($offerDto->getId(),$discipline->getSection()); 
               }
                $dto=$this->getContainer()->get('badiu.ams.offer.discipline.entity');
     		 	$dto->setEntity($discipline->getEntity());
     		 	$dto->setDeleted($discipline->getDeleted());
     		 	$dto->setTimecreated(new \Datetime());
     		 	$dto->setDisciplineid($discipline->getDisciplineid());
                $dto->setOfferid($offerDto);
                $dto->setDisciplinename($discipline->getDisciplineid()->getName());
     		 	$dto->setTeachingplan($discipline->getTeachingplan());
                        $dto->setSummary($discipline->getSummary());
                         $dto->setDescription($discipline->getDescription());
                         $dto->setDhour($discipline->getDhour());
                         $dto->setCredit($discipline->getCredit()); 
     		 	$dto->setDrequired($discipline->getDrequired());
               if(!empty($period)) $dto->setPeriodid($period);
                $groupid=$discipline->getGroupid();
                if(!empty($groupid))
                                    $dto->setGroupid($discipline->getGroupid());


            // echo "<br> -->".$discipline->getDisciplineid()->getName();
                /*summury)
     		 	getStatusid()
     		 	getGroupid()
     		 	getOfferid
     		 	getDrequired()*/
     		 	$disciplinedata->setDto($dto);
     		 	$disciplinedata->save();
				
				//add grade item
				$odisciplinelib=$this->getContainer()->get('badiu.ams.offer.discipline.lib');
				$odisciplinelib->addGradeItem($disciplinedata->getDto());
                
     		 }
            // exit;
     }

     public function criatePeriod($offerDto) {
        
        //get numsections config
         $cdata=$this->getContainer()->get('badiu.ams.curriculum.curriculum.data');
         $numsections= $cdata->getNumsections($offerDto->getCurriculumid()->getId());
         $typesections= $cdata->getTypesections($offerDto->getCurriculumid()->getId());
        
        // create period
        $pdata=$this->getContainer()->get('badiu.ams.offer.period.data');
         for($i=1;$i<=$numsections;$i++){

            $key='badiu.ams.curriculum.sections.period.list';
            if($typesections==DOMAINTABLE::$TYPE_SECTION_PERIOD){$key='badiu.ams.curriculum.sections.period.list';}
            else if($typesections==DOMAINTABLE::$TYPE_SECTION_MODULE){$key='badiu.ams.curriculum.sections.module.list';}
            else if($typesections==DOMAINTABLE::$TYPE_SECTION_UNIT){$key='badiu.ams.curriculum.sections.unit.list';}
            else if($typesections==DOMAINTABLE::$TYPE_SECTION_LEVEL){$key='badiu.ams.curriculum.sections.level.list';}
            $name= $this->getTranslator()->trans($key,array('%number%'=>$i));
            $dto=$this->getContainer()->get('badiu.ams.offer.period.entity');

            $dto->setEntity($offerDto->getEntity());
            $dto->setDeleted($offerDto->getDeleted());
            $dto->setTimecreated(new \Datetime());
            $dto->setOfferid($offerDto);
            $dto->setSection($i);
            $dto->setName($name);
            $pdata->setDto($dto);
            $pdata->save();
			
			
			
        } 
     }
}
