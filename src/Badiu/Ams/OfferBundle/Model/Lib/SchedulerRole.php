<?php

namespace Badiu\Ams\OfferBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class SchedulerRole extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function getDataFormEdit($roleid) {
			 $fdata=array();
			 $fdata=$this->getRoleDb($roleid,$fdata);
			return $fdata;
	}
	public function save($data) {
			$sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
			$isEdit=$sysoperation->isEdit();
			$roleid=null;
			if($isEdit){
				$roleid=$this->getContainer()->get('request')->get('id');
			}
			$dto=$data->getDto();
			
			
		 	$data->setDto($this->getRoleForm($roleid,$dto));
			$result=$data->save();
			
				
			return $data->getDto();
		}
	public function getRoleDb($roleid,$fdata) {
			 
			$data=$this->getContainer()->get('badiu.ams.offer.schedulerrole.data');
			$dto=$data->findById($roleid);
			
			$fdata['name']=$dto->getName();
			$fdata['daction']=$dto->getDaction();
			$fdata['reporttype']=$dto->getReporttype();
			$fdata['message']=$dto->getSendmessage();
			$fdata['subject']=$dto->getSendsubject();
			$fdata['countdayenrol']=$dto->getCustomint1();
					
			$fdata['idnumber']=$dto->getIdnumber();
			$fdata['param']=$dto->getParam();
			$fdata['description']=$dto->getDescription();
			return $fdata;
		
	}
	
	public function getRoleForm($roleid,$fdata) {
			$dto=null;
			
			if(!empty($roleid)){
				$data=$this->getContainer()->get('badiu.ams.offer.schedulerrole.data');
				$dto=$data->findById($roleid);
			}
			if(empty($dto)){
				$dto=$this->getContainer()->get('badiu.ams.offer.schedulerrole.entity');
				$dto=$this->initDefaultEntityData($dto);
				}
			
			if(isset($fdata['name'])) {$dto->setName($fdata['name']);}
			if(isset($fdata['daction'])) {$dto->setDaction($fdata['daction']);}
			if(isset($fdata['reporttype'])) {$dto->setReporttype($fdata['reporttype']);}
			if(isset($fdata['message'])) {$dto->setSendmessage($fdata['message']);}
			if(isset($fdata['subject'])) {$dto->setSendsubject($fdata['subject']);}
			if(isset($fdata['countdayenrol'])) {$dto->setCustomint1($fdata['countdayenrol']);}
	
			if(isset($fdata['idnumber'])) {$dto->setIdnumber($fdata['idnumber']);}
			if(isset($fdata['param'])) {$dto->setParam($fdata['param']);}
			if(isset($fdata['description'])) {$dto->setDescription($fdata['description']);}
			
			$sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
			$isEdit=$sysoperation->isEdit();
			//if(!$isEdit){
				$offerid=$this->getContainer()->get('request')->get('parentid');
				$dto->setModulekey('badiu.ams.offer.schedulerrole');
				$dto->setModuleinstance($offerid);
			//}
			
        
			return $dto;
		
	}
	
	

}
