<?php

namespace Badiu\Ams\OfferBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class EnrolSelectionIntegrationFilter extends BadiuFormFilter{
    private $modulekey;
	private $name;
	private $dtype='course';
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
   public function execBeforeSubmit() {

          $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
          if(!is_array($dconfig)){$dconfig=$this->getJson()->decode($dconfig, true);}
          
		  $param=$this->getParam();
 
          $selectionintegration=array();
             
          $requesttimestart=$this->getUtildata()->getVaueOfArray($this->getParam(), 'enrolrequesttimestart');
		  if (is_a($requesttimestart, 'DateTime')) {
				$selectionintegration['requesttimestart']=$requesttimestart->getTimestamp();
		  }else {$selectionintegration['requesttimestart']="";}
		  unset($param['enrolrequesttimestart']);
          
          $requesttimeend=$this->getUtildata()->getVaueOfArray($this->getParam(), 'enrolrequesttimeend');
		  if (is_a($requesttimeend, 'DateTime')) {
				$selectionintegration['requesttimeend']=$requesttimeend->getTimestamp();
		  }else {$selectionintegration['requesttimeend']="";}
          unset($param['enrolrequesttimeend']);
          
          $selectionintegration['analysiscriteria']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'enrolanalysiscriteria');
          unset($param['enrolanalysiscriteria']);
          
          $selectionintegration['servicecheckcriteria']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'enrolservicecheckcriteria');
          unset($param['enrolservicecheckcriteria']);
         
		  $selectionintegration['defaultrole']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'defaultrole');
          unset($param['defaultrole']);
         
		  $selectionintegration['defaultstatus']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'defaultstatus');
          unset($param['defaultstatus']);
         
		 $selectionintegration['listdocumentnumber']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'listdocumentnumber');
          unset($param['listdocumentnumber']);
           
          if(empty($dconfig) || !is_array($dconfig)){$dconfig=array();}
          
          $dconfig['enrolselection']=$selectionintegration;
          $dconfig = $this->getJson()->encode($dconfig);
          $param['dconfig']=$dconfig;
        
          $this->setParam($param);
		 
        }
        
   public function execAfterSubmit() {
	
	   if(empty($this->getModulekey())){return null;}
	 
	   $enroltype=$this->getUtildata()->getVaueOfArray($this->getParam(), 'enroltype');
	    if($enroltype!='select'){return null;}
	    $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
		$maxenrol = $this->getUtildata()->getVaueOfArray($this->getParam(), 'maxenrol');
		
		 if(empty($id)){return null;}
		 
		 $data=$this->getContainer()->get($this->getModulekey().'.data');
		 $dconfig=  $data->getGlobalColumnValue('dconfig',array('id'=>$id));
         $dconfig=$this->getJson()->decode($dconfig,true);
		
		 $slectionparam= $this->getUtildata()->getVaueOfArray($dconfig, 'enrolselection');
		 $slectionparam['modulekey']=$this->getModulekey();
		 $slectionparam['moduleinstance']=$id;
		 $slectionparam['name']= $this->getName();
         $slectionparam['limitaccept']= $maxenrol;
		 $slectionparam['dtype']= $this->getDtype();
		 $slectionparam['ttype']= 'default';
		 
	
		//cast date
		 $requesttimestart=$this->getUtildata()->getVaueOfArray($slectionparam, 'requesttimestart');
	
		 if(!empty($requesttimestart)){
			 $now1=new \DateTime();
			 $now1->setTimestamp($requesttimestart);
			 $slectionparam['requesttimestart']=$now1;
		 }
		 $requesttimeend=$this->getUtildata()->getVaueOfArray($slectionparam, 'requesttimeend');
		 if(!empty($requesttimeend)){
			 $now2=new \DateTime();
			 $now2->setTimestamp($requesttimeend);
			 $slectionparam['requesttimeend']=$now2;
		 }
		  
         $selectionprojetlib=$this->getContainer()->get('badiu.admin.selection.project.lib');
		
         $enrolselectionid=$selectionprojetlib->add($slectionparam);
		
         $slectionparam['id']=$enrolselectionid;
         $dconfig['enrolselection']=$slectionparam;
		 $dconfig=$this->getJson()->encode($dconfig);
		 $param=$this->getParam();
		 $param['dconfig']=$dconfig;
	
		 $paramup=array('id'=>$id,'dconfig'=>$dconfig);
		 $data->updateNativeSql($paramup,false);
         $this->setParam($param);
         
         
   } 
    public function changeParamOnOpenEdit($param,$modulekey){
			if(empty($modulekey)){return $param; }
			$moduleinstance=$this->getUtildata()->getVaueOfArray($param, 'id');
			if(empty($moduleinstance)){return $param; }
			$data=$this->getContainer()->get('badiu.admin.selection.project.data');
			$fparam=array('modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'entity'=>$this->getEntity(),'ttype'=>'default');
			$rdata=$data->getGlobalColumnsValue('o.id,o.requesttimestart,o.requesttimeend,o.analysiscriteria,o.listdocumentnumber,o.servicecheckcriteria,o.defaultstatus,o.defaultrole',$fparam);
			
			$requesttimestart=$this->getUtildata()->getVaueOfArray($rdata, 'requesttimestart');
			if(!empty($requesttimestart)){$requesttimestart= $requesttimestart->getTimestamp();};
			
			$requesttimeend=$this->getUtildata()->getVaueOfArray($rdata, 'requesttimeend');
			if(!empty($requesttimeend)){$requesttimeend= $requesttimeend->getTimestamp();};
			
			$param['enrolrequesttimestart']=$requesttimestart;
			$param['enrolrequesttimeend']=$requesttimeend;
			$param['enrolanalysiscriteria']=$this->getUtildata()->getVaueOfArray($rdata, 'analysiscriteria');
			$param['enrolservicecheckcriteria']=$this->getUtildata()->getVaueOfArray($rdata, 'servicecheckcriteria');
			$param['listdocumentnumber']=$this->getUtildata()->getVaueOfArray($rdata, 'listdocumentnumber');
			$param['defaultstatus']=$this->getUtildata()->getVaueOfArray($rdata, 'defaultstatus');
			$param['defaultrole']=$this->getUtildata()->getVaueOfArray($rdata, 'defaultrole');
			return $param;
	}		
   
	 function getModulekey() {
        return $this->modulekey;
    }

	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

 function getName() {
        return $this->name;
    }

	
	function setName($name) {
        $this->name = $name;
    }

	  /**
     * @return string
    */
	public function getDtype()
	{
		return $this->dtype;
	}
 
	/**
	 * @param string $dtype
	 */
	public function setDtype($dtype)
	{
		$this->dtype = $dtype;
	}
}
