<?php

namespace Badiu\Ams\OfferBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ClassePlanned extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
          	public function getDataFormEdit($plannedid) {
			$fdata=array();
                    	 $fdata=$this->getPlannedDb($plannedid,$fdata);
			return $fdata;
	}
	public function save($data) {
         
			$sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
			$isEdit=$sysoperation->isEdit();
			$plannedid=null;
			if($isEdit){
				$plannedid=$this->getContainer()->get('request')->get('id');
			}
			$dto=$data->getDto();
			
			$data->setDto($this->getPlannedForm($plannedid,$dto));
			$data->save();
		
			return $data->getDto();
		}
	public function getPlannedDb($plannedid,$fdata) {
			
                   
			$data=$this->getContainer()->get('badiu.ams.offer.classeplanned.data');
                       $dto=$data->findById($plannedid);
                   
			if($dto->getTimestart()!=null){
                            $fdata['day']=$dto->getTimestart();
                            $fdata['hourstart']=array('badiutimehour'=>$dto->getTimestart()->format('H'),'badiutimeminute'=>$fdata['minute']=$dto->getTimestart()->format('i'));
                          }
                        if($dto->getTimeend()!=null){
                             $fdata['hourend']=array('badiutimehour'=>$dto->getTimeend()->format('H'),'badiutimeminute'=>$fdata['minute']=$dto->getTimeend()->format('i'));
                        }
                        
                        $fdata['hourduration']=$dto->getHourduration();
                        
                        if($dto->getHroomid()!=null){$fdata['hroomid']=$dto->getHroomid()->getId();}
                        if($dto->getStatusid()!=null) {$fdata['statusid']=$dto->getStatusid()->getId();}
                        
                        $fdata['idnumber']=$dto->getIdnumber();
			$fdata['param']=$dto->getParam();
			$fdata['description']=$dto->getDescription();
                        
                        // print_r($fdata);
                        //  echo "xxx6";exit;
			return $fdata;
		
	}
	
	public function getPlannedForm($plannedid,$fdata) {
          
			$dto=null;
                        if(!empty($plannedid)){
				$data=$this->getContainer()->get('badiu.ams.offer.classeplanned.data');
				$dto=$data->findById($plannedid);
                               
			}else{
				$dto=$this->getContainer()->get('badiu.ams.offer.classeplanned.entity');
				$dto=$this->initDefaultEntityData($dto);
                                $dto->setClasseid($this->getContainer()->get('badiu.ams.offer.classe.data')->findById($this->getContainer()->get('request')->get('parentid')));
			}
                        
                        if(isset($fdata['day'])) {
                            $date=$fdata['day'];
                             $date1 = new \DateTime();
                             $date1->setDate($date->format('y'),$date->format('m'),$date->format('d'));
                             $dto->setTimestart($date1);
                             
                            $date2 = new \DateTime();
                            $date2->setDate($date->format('y'),$date->format('m'),$date->format('d'));
                            $dto->setTimeend($date2);
                            
                            $hourstart=$fdata['hourstart']['badiutimehour']; 
                            $minstart=$fdata['hourstart']['badiutimeminute']; 
                             $dto->getTimestart()->setTime(0,0);
                            $dto->getTimestart()->setTime($hourstart,$minstart);
                           
                            $hourend=$fdata['hourend']['badiutimehour']; 
                            $minend=$fdata['hourend']['badiutimeminute']; 
                              $dto->getTimeend()->setTime(0,0);
                             $dto->getTimeend()->setTime($hourend,$minend);
                            
                       }
                    
                    
                        $dto->setHourduration($fdata['hourduration']);
                        
                        if(isset($fdata['timeexec'])) {$dto->setTimeexec($fdata['timeexec']);}
			if(isset($fdata['hroomid'])) {$dto->setHroomid($this->getContainer()->get('badiu.util.housing.room.data')->findById($fdata['hroomid']));}
                        if(isset($fdata['statusid'])) {$dto->setStatusid($this->getContainer()->get('badiu.ams.offer.classetaughtstatus.data')->findById($fdata['statusid']));}
                        
                        
                         
                        if(isset($fdata['idnumber'])) {$dto->setIdnumber($fdata['idnumber']);}
			if(isset($fdata['param'])) {$dto->setParam($fdata['param']);}
			if(isset($fdata['description'])) {$dto->setDescription($fdata['description']);}
			
		
			return $dto;
		
	}   
}
