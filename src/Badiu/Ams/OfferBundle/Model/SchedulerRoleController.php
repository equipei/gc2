<?php

namespace Badiu\Ams\OfferBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormError;
class SchedulerRoleController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
              
    public function addCheckForm($form,$data) {
           $check=true;
		 
           return  $check;
       }
	   

	 public function setDefaultDataOnOpenEditForm($dto) {
          $lib=$this->getContainer()->get('badiu.ams.offer.schedulerrole.lib');
		  $id=$this->getContainer()->get('request')->get('id');
		  $dto=$lib->getDataFormEdit($id);
		  return $dto;
     }
    
	 public function save($data) {
	 
		$dto=$data->getDto();
		$lib=$this->getContainer()->get('badiu.ams.offer.schedulerrole.lib');
		$result=$data->setDto($lib->save($data));
		return $result;
    }   
	
	 public function findOfferidInSchedulerrole($dto) {

        return $dto->getModulekey();
    }
}
