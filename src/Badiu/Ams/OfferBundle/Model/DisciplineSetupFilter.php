<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class DisciplineSetupFilter extends BadiuFormFilter{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
   public function execBeforeSubmit() {
    
        }
        
   public function execAfterSubmit() {
         $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
         if(empty($id)){return null;}
         $data=$this->getContainer()->get('badiu.ams.offer.discipline.data');
         $dto=$data->findById($id);
        
         $setup=$this->getContainer()->get('badiu.ams.offer.discipline.setup');
         $setup->exec($dto);
         
   } 
       
    
}
