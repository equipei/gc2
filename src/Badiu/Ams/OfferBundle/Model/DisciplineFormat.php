<?php

namespace Badiu\Ams\OfferBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;

class DisciplineFormat extends BadiuFormat {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function measure($data) {
        $result = null;
        $measuretype = null;
        $measurevalue = null;

        if (array_key_exists("measurevalue", $data)) {

            $measurevalue = $data["measurevalue"];
            if ($measurevalue < 60) {
                $result = "$measurevalue min ";
            } else if ($measurevalue >= 60) {
                $hour = $measurevalue / 60;
                $result = "$hour h ";
                $result = "$hour h ";
            }
        }


        return $result;
    }

    public function period($data) {
        $result = null;
        $timestart = null;
        $timeend = null;
        if (array_key_exists("timestart", $data)) { $timestart=$data["timestart"];}
        if (array_key_exists("timeend", $data)) { $timeend=$data["timeend"];}
        
        
        if ($timestart!=null && $timeend!=null ) {
            $dateformat=$this->getContainer()->get('badiu.system.core.lib.format.dateformat');
             $result= $dateformat->period($timestart,$timeend);
        }
            



        return $result;
    }

    
    public function grade($data) {
        $grade = null;
        if (array_key_exists("grade", $data)) { $grade=$data["grade"];}
      
        $html="<div>{{badiubasedefaulttablehead}}</div>"; 
        /* $html.=' <div>
                              <div  inline-edit="token" on-save="updateTodo(token)" on-cancel="cancelEdit(token)" ng-hide="token == null || token.length == 0">{{token}}</div>

                            <div ng-show="token == null || token.length == 0">
                              <input type="text" class="form-control" id="inputtoken" ng-change="addFunc(token)" ng-model="token" placeholder="token">
                            </div>

                            </div>';*/
        //return $grade;
         return $html;
    }
	
	 public  function classelinkemanager($data){
            $value="";
            $id=$this->getUtildata()->getVaueOfArray($data,'id');
            
			$labelmanageclasselink=$this->getTranslator()->trans('badiu.ams.discipline.discipline.manageclasselink');
            $labeladdclasselink=$this->getTranslator()->trans('badiu.ams.discipline.discipline.addclasselink');
           
            $parammage=array('parentid'=>$id);
            $manageclasseurl=$this->getUtilapp()->getUrlByRoute('badiu.ams.offer.classe.index',$parammage);
            $addclasseurl=$this->getUtilapp()->getUrlByRoute('badiu.ams.offer.classe.add',$parammage);
            $manageclasselink ="<a href=\"$manageclasseurl\">$labelmanageclasselink</a>";
            $addclasselink ="<a href=\"$addclasseurl\">$labeladdclasselink</a>";

            $value="$manageclasselink <br />$addclasselink";
           
        return $value; 
    } 
}
