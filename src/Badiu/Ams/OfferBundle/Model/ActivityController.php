<?php

namespace Badiu\Ams\OfferBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ActivityController extends BadiuController {
	function __construct(Container $container) {
		parent::__construct($container);
	}

	public function addCheckForm($form, $data) {
		$check = true;

		return $check;
	}

	public function addTopicidToActivity($dto, $dtoParent) {
		$dto->setTopicid($dtoParent);
		return $dto;
	}

	public function findTopicidInActivity($dto) {
		return $dto->getTopicid()->getId();
	}

}
