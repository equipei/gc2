<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Badiu\Ams\CurriculumBundle\Model\DOMAINTABLE;
class OfferController extends BadiuController{
    
     function __construct(Container $container) {
            parent::__construct($container);
       }
    

    public function save($data) {
        $result=$data->save();
		$sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
			$isEdit=$sysoperation->isEdit();
			if(!$isEdit){
                            $offerlib=$this->getContainer()->get('badiu.ams.offer.offer.lib');
                            $offerlib->copyCurriculum($data->getDto());
                        }
		
        return $data->getDto();
    }
   
    /*
   public function copyCurriculum($data) {
			
            //create period
            $this->criatePeriod($data);
     		//get curriculum id
     		$curriculumid=$data->getCurriculumid()->getId();
            //echo "<br>curriculumid: $curriculumid";
     		//get disciplines of curriculum
     		$curriculumdata=$this->getContainer()->get('badiu.ams.curriculum.discipline.data');
     		$disciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
     		$perioddata=$this->getContainer()->get('badiu.ams.offer.period.data');
            $disciplines=$curriculumdata->getAllDisciplinesByCurriculum($curriculumid);
     		
            //add disciplines in offer
			$x=10;
			$y=12;
			$cont=0;
     		 foreach ($disciplines as $discipline) {
			 //if($cont >=$x && $cont <$y){

				 $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                 $entity=$badiuSession->get()->getEntity();
				 
     		 	$period= null;
                $section=$discipline->getSection();
               if(!empty($section)){
                   $period=  $perioddata->findBySection($data->getId(),$discipline->getSection()); 
               }
			   $exist=$disciplinedata->existDiscipline($entity,$data->getId(),$discipline->getDisciplineid()->getId());
			 
			   if($exist){
					$dto=$disciplinedata->findDiscipline($entity,$data->getId(),$discipline->getDisciplineid()->getId());
					$dto->setTimemodified(new \Datetime());
			   }else{
					$dto=$this->getContainer()->get('badiu.ams.offer.discipline.entity');
					$dto->setEntity($discipline->getEntity());
					$dto->setDeleted($discipline->getDeleted());
					$dto->setTimecreated(new \Datetime());
					$dto->setDisciplineid($discipline->getDisciplineid());
					$dto->setOfferid($data);
			   }
                
     		 	
                $dto->setDisciplinename($discipline->getDisciplineid()->getName());
				$dto->setIdnumberlms($discipline->getDisciplineid()->getIdnumber()); 
     		 	$dto->setTeachingplan($discipline->getTeachingplan());
     		 	$dto->setDrequired($discipline->getDrequired());
				
				//review it
				if($discipline->getDisciplineid()->getParam()=='SCORM'){
					$dto->setConclusiongrade(1);
				}else{
					$dto->setConclusiongrade(70);
				}
				
               if(!empty($period)) $dto->setPeriodid($period);
                $groupid=$discipline->getGroupid();
                if(!empty($groupid))$dto->setGroupid($discipline->getGroupid());


     		 	// echo "<br> -->".$discipline->getDisciplineid()->getName();
                //summury)
     		// 	getStatusid()
     		// 	getGroupid()
     		// 	getOfferid
     		// 	getDrequired()
     	 	$disciplinedata->setDto($dto);
     		 	$disciplinedata->save();
                
			//	echo "process offer $cont <br>";
			 // }
			  $cont++;

     		 }
            // exit;
     }
*/
 /*    public function criatePeriod($offer) {
        
        //get numsections config
         $cdata=$this->getContainer()->get('badiu.ams.curriculum.curriculum.data');
         $numsections= $cdata->getNumsections($offer->getCurriculumid()->getId());
         $typesections= $cdata->getTypesections($offer->getCurriculumid()->getId());
        
        // create period
        $pdata=$this->getContainer()->get('badiu.ams.offer.period.data');
		
         for($i=1;$i<=$numsections;$i++){
			$exist=$pdata->existSection($offer->getId(),$i);
			
			if(!$exist){
				$key='badiu.ams.curriculum.sections.period.list';
				if($typesections==DOMAINTABLE::$TYPE_SECTION_PERIOD){$key='badiu.ams.curriculum.sections.period.list';}
				else if($typesections==DOMAINTABLE::$TYPE_SECTION_MODULE){$key='badiu.ams.curriculum.sections.module.list';}
				else if($typesections==DOMAINTABLE::$TYPE_SECTION_UNIT){$key='badiu.ams.curriculum.sections.unit.list';}
				else if($typesections==DOMAINTABLE::$TYPE_SECTION_LEVEL){$key='badiu.ams.curriculum.sections.level.list';}
				$name= $this->getTranslator()->trans($key,array('%number%'=>$i));
				$dto=$this->getContainer()->get('badiu.ams.offer.period.entity');
						
				$dto->setEntity($offer->getEntity());
				$dto->setDeleted($offer->getDeleted());
				$dto->setTimecreated(new \Datetime());
				$dto->setOfferid($offer);
				$dto->setSection($i);
				$dto->setName($name);
				$pdata->setDto($dto);
				$pdata->save();
			}
            
        } 
     }*/
}
