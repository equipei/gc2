<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class SchedulerRoleFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
    
    public  function getReportType(){
        $list=array();
		// comentado só para atender sistuaçao do titanio temp
       // $list[DOMAINTABLE::$REPORT_OFFER_ENROL]=$this->getTranslator()->trans('badiu.ams.offer.enrol.report');
        $list[DOMAINTABLE::$REPORT_OFFER_DISCIPLINE_ENROL]=$this->getTranslator()->trans('badiu.ams.offer.enrol.discipline.report');
		return $list;
    }

}
