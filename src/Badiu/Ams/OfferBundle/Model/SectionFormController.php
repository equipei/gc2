<?php

namespace Badiu\Ams\OfferBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class SectionFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     public function changeParamOnOpen() {
        
            $this->inheritDconfig(); 
            $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
            $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $param = $this->getParam();
           
            $lmsintegration = $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');

            $param['sserviceid'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'sserviceid');
            $param['lmssynctype'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynctype');
            $param['lmssynclevel'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynclevel');
            $param['lmscoursecat'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatid');
            $param['lmscourse'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseid');
            $param['lmscoursecatparent'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatparent');
          
            $param['productintegration'] = $this->getUtildata()->getVaueOfArray($dconfig, 'financintegration.productintegration',true);

            $this->setParam($param); 
           
     }   
     
    public function changeParam() {

        $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
        if (empty($dconfig) || !is_array($dconfig)) {
            $dconfig = array();
        }

        $dconfig = $this->getJson()->encode($dconfig);
        $param = $this->getParam();
        $param['dconfig'] = $dconfig;
        $this->setParam($param);
        
     }
     
     public function inheritDconfig() {
      
          if($this->isEdit()){return null;}
          
          $dconfig=null;
          $entity= $this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
          $parentid = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
          $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
          $dconfig=$odata->getColumnValue($entity,'dconfig',$paramf=array('id'=>$parentid));
          $dconfig = $this->getJson()->decode($dconfig, true);
        
          $dconfig['lmsintegration']['lmscoursecatparent']= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscoursecatid',true);
          $dconfig['lmsintegration']['lmscoursecatparenid']= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscoursecatid',true);
          $dconfig['lmsintegration']['lmscoursecatparentname']= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscoursecatname',true);
         
          $lmssynctypeoriginal=$this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmssynctypeoriginal',true);
          if(!empty($lmssynctypeoriginal)){$dconfig['lmsintegration']['lmssynctype']= $lmssynctypeoriginal;}
         
          $dconfig['lmsintegration']['lmscoursecatid']= null;
          $dconfig['lmsintegration']['lmscoursecat']= null;
      
                  
           $dconfig = $this->getJson()->encode($dconfig);
    
          $this->addParamItem('dconfig',$dconfig);
      }
         
    
}
