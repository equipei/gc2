<?php

namespace Badiu\Ams\OfferBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnrolExportToAllDisciplineFormController extends BadiuFormController
{ 
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     
     public function save() {
          $param = $this->getParam();
       
          $offerid=$this->getParamItem('offerid');
          $addenrolinclasse=$this->getParamItem('addenrolinclasse');
          $odisciplines=$this->getListOfDisciplines($offerid);
          $filterclassbymarker = $this->getParamItem('filterclassbymarker');
          $contdisciplines=0;
          $contenrol=0;
          $contenrolclasse=0;
         foreach ($odisciplines as $row) {
               $param = $this->getParam();
               $odisciplineid=$this->getUtildata()->getVaueOfArray($row, 'id');
               $tdata= $this->getContainer()->get('badiu.ams.enrol.lib.transfer');
               $tdata->init('offer',$offerid,'discipline',$odisciplineid,$param);
               $result=$tdata->exec();
               $contenrol+=$result;
               $contdisciplines++;
                if($addenrolinclasse){
                    if(empty($filterclassbymarker)){
                      $fistclasseid=$this->getContainer()->get('badiu.ams.offer.classe.data')->geFirstClasseByDisciplineid($odisciplineid);
                      if(!empty($fistclasseid)){
                          $param['enroldate']='discipline';
                           $tdata->init('discipline',$odisciplineid,'classe',$fistclasseid,$param);
                           $contenrolclasse+=$tdata->exec();
                     }
                    }else{
                    
                        $listclasseid=$this->getContainer()->get('badiu.ams.offer.classe.data')->getListidByDisciplineidMarker($odisciplineid,$filterclassbymarker); 
                       
                        foreach ($listclasseid as $lcid) {
                          $lclassid=$this->getUtildata()->getVaueOfArray($lcid, 'id');
                           $param['enroldate']='discipline'; 
                           $tdata->init('discipline',$odisciplineid,'classe',$lclassid,$param);
                           $contenrolclasse+=$tdata->exec();
                        }
                    }
                  
                 
               }
          }
          
         $this->setSuccessmessage($this->getTranslator()->trans('badiu.ams.enrol.exportoffertoalldiscipline.info',array('%countenrol%'=>$contenrol,'%countdiscipilne%'=>$contdisciplines))); 
          $outrsult=array('result'=>$contdisciplines,'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
          $this->getResponse()->setStatus("accept");
          $this->getResponse()->setMessage($outrsult);
         return $this->getResponse()->get();
         
     }
    
      public function getListOfDisciplines($offerid) {
           $statusid=$this->getContainer()->get('badiu.ams.offer.disciplinestatus.data')->getIdByShortname($this->getEntity(), 'active');
          $listids= $this->getContainer()->get('badiu.ams.offer.discipline.data')->getColumnValues($this->getEntity(),'id',array('o.offerid'=>$offerid,'o.statusid'=>$statusid));
          return $listids;
      }
}
