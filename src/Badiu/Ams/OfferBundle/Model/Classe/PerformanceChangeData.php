<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportChangeData;
class PerformanceChangeData extends BadiuReportChangeData
{

    function __construct(Container $container) {
            parent::__construct($container);
              }
              

public function all($data){
		$trows=$this->getUtildata()->getVaueOfArray($data,'badiu_table1_rows'); 
		$trows=$this->addColumns($trows);
		$data['badiu_table1_rows']=$trows;
	
	/*echo "<pre>"; 
	print_r($data); 
	echo "</pre>";exit;
	*/
     return $data;
      
  }
 function addColumns($rows){
	 $newlist=array();
	 if(empty($rows)){return $newlist;}
	 if(!is_array($rows)){return $newlist;}
	 $roleconclusionlib=$this->getContainer()->get('badiu.ams.core.lib.roleconclusionlib');
	 foreach ($rows as $row){
		 $dconfigclasse=$this->getUtildata()->getVaueOfArray($row, 'classedconfig');
		 $dconfigdiscipline=$this->getUtildata()->getVaueOfArray($row, 'disclipinedconfig');
		 $dconfigoffer=$this->getUtildata()->getVaueOfArray($row, 'offerdconfig');
		 
		 $dconfig=array('level'=>'classe','source'=>'array','config'=>array('classe'=>$dconfigclasse,'discipline'=>$dconfigdiscipline,'offer'=>$dconfigoffer));
		 $crole=$roleconclusionlib->config($dconfig);
		$row['roleconclusion']= $crole;
		$row=$this->checkCompletedCriteria($row);
		/*echo "<pre>";
	 print_r($row);
	  echo "</pre>";exit;*/
		array_push($newlist,$row);
	 }
	 return $newlist;
 }
 
 function checkCompletedCriteria($row){
	 
	 $hourplanned=$this->getUtildata()->getVaueOfArray($row, 'disciplinedhour');
	 $hourshouldexecpercent=$this->getUtildata()->getVaueOfArray($row, 'roleconclusion.conclusion.discipline.defaultroles.dhour.minperc',true);
	
	 $hourshouldexec=null;
	 if($hourshouldexecpercent > 0 && $hourplanned > 0){$hourshouldexec=$hourshouldexecpercent * 0.01*$hourplanned;}
	 $hourexec=$this->getUtildata()->getVaueOfArray($row, 'countexecutedhour');
	 
	 $hourcompleted=0;
	 if($hourexec > 0 && $hourshouldexec > 0 && $hourexec >=$hourshouldexec ){$hourcompleted=1;}
	
	$row['roleconclusion']['result']['dhour']=array('planned'=>$hourplanned,'shouldexecpercent'=>$hourshouldexecpercent,'shouldexec'=>$hourshouldexec,'exec'=>$hourexec,'completed'=>$hourcompleted);	
	
	$gradeplanned=$this->getUtildata()->getVaueOfArray($row, 'finalgrade');
	$gradeshouldexecpercent=$this->getUtildata()->getVaueOfArray($row, 'roleconclusion.conclusion.discipline.defaultroles.grade.minperc',true);
	
	 $gradeshouldexec=null;
	 if($gradeshouldexecpercent > 0 && $gradeplanned > 0){$gradeshouldexec=$gradeshouldexecpercent * 0.01*$gradeplanned;}
	 $gradeexec=$this->getUtildata()->getVaueOfArray($row, 'countexecutedhour');
	 
	 $gradecompleted=0;
	 if($gradeexec > 0 && $gradeshouldexec > 0 && $gradeexec >=$gradeshouldexec ){$gradecompleted=1;}
	
	$row['roleconclusion']['result']['grade']=array('planned'=>$gradeplanned,'shouldexecpercent'=>$gradeshouldexecpercent,'shouldexec'=>$gradeshouldexec,'exec'=>$gradeexec,'completed'=>$gradecompleted);	
	
	
	$finalcompleted=0;
	if($hourshouldexecpercent > 0 && $gradeshouldexecpercent > 0 && $hourcompleted > 0 && $gradecompleted > 0 ){$finalcompleted=1;}
	else if(empty($hourshouldexecpercent) && $gradeshouldexecpercent > 0 && $gradecompleted > 0 ){$finalcompleted=1;}
	if($hourshouldexecpercent > 0 && empty($gradeshouldexecpercent) && $hourcompleted > 0){$finalcompleted=1;}
	$row['completedcriteria']=$finalcompleted;
	return $row;
 }
}
