<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;
use Badiu\System\CoreBundle\Model\Functionality\BadiuAccessFilter;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ClassePermissionAccessFilter extends  BadiuAccessFilter {

	function __construct(Container $container) {
		parent::__construct($container);
	}
 
       public function exec(){
		   $hasexception=$this->isRoutException();
		   if($hasexception){return true;}
		   
		   $routerlib = $this->getContainer()->get('badiu.system.core.lib.config.routerlib');
		   $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');
		  // $key=$routerlib->getKey();
		  // echo $key;exit;
		  
		  //key has parent
		  $keyhasparent=$routerlib->hasParentParameter();
		  if(!$keyhasparent){return true;}
		  
		  //has access role with parentkey
		  $iskeyforrolewithrestrictparent=$libpermission->isKeyOfRoleWithEnablerestrictParentKey();
		  if(!$iskeyforrolewithrestrictparent){return true;}
		  
		  //has enrol
		  $isenrol=$this->checkInstanceEnrol();
		
		   //if(!$isenrol){return false;}
		  if(!$isenrol){
			  echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">';
			  echo "<div class=\"alert alert-danger\" role=\"alert\">Você não tem permissão para acessar essa funcionalidade</div></hr>";
			  exit;
		  }
		
		  return true;
         } 
   
     
	public function checkInstanceEnrol(){
		  $enrolclassedata = $this->getContainer()->get('badiu.ams.enrol.classe.data');
		  $listinstance=$enrolclassedata->getInstancesidByRole($this->getUserid());
		  $routerlib = $this->getContainer()->get('badiu.system.core.lib.config.routerlib');
		  $key=$routerlib->getKey();
		  $dtype='course';
		  $pos=stripos($key, "badiu.tms.");
		  if($pos!== false){$dtype='training';}
		  
		  $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		  $result=false;
		  if(!empty($listinstance) && is_array($listinstance)){
			  foreach ($listinstance as $row){
				 $id=$this->getUtildata()->getVaueOfArray($row,'id');
				 $rdtype=$this->getUtildata()->getVaueOfArray($row,'dtype');
				 if($dtype==$rdtype && $id==$parentid){$result=true;break;}
				 
			  }
		  }
		  
		  if(!$result){
			 $result=$this->checkInstanceAccessRow();
		  }
		 return $result;
	}	
	
	public function isRoutException(){
		$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$service=$this->getUtildata()->getVaueOfArray($param,'_service');
		$routekey=$this->getUtildata()->getVaueOfArray($param,'_key');
		if($service=='badiu.system.core.functionality.form.service' && !empty($routekey)){return true;}
		return false;
	}
	
	public function checkInstanceAccessRow(){
		return false;
	}

	public function getInstanceLevel(){
		
		return null;
	}
		
}


