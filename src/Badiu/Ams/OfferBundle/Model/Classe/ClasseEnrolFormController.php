<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ClasseEnrolFormController extends BadiuFormController
{ 
	private $replication=array();
    function __construct(Container $container) {
            parent::__construct($container);
              }
     
	  public function changeParamOnOpen() {
		   $param = $this->getParam();
		   if(!$this->isEdit()){
			  $offerid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
			  $etparam=array('modulekey'=>'badiu.ams.offer.classe','moduleinstance'=>$offerid);
			  $param=$this->getContainer()->get('badiu.ams.enrol.lib.time')->changeParamOnAddForm($etparam,$param);
			  $param=$this->getContainer()->get('badiu.ams.enrol.lib.replicateconfig')->changeParamOnAddForm($etparam,$param);
			  
			   $this->setParam($param); 
		   }
		   
		
	   }
      
     
    
     public function execAfter(){
		  $enrolid=$this->getParamItem('id');
		  $classeid=$this->getParamItem('classeid');
		  $userid=$this->getParamItem('userid');
		  $roleid=$this->getParamItem('roleid');
		  $statusid=$this->getParamItem('statusid');
		  $enrolreplicate=$this->getParamItem('enrolreplicate');
		  $enrolreplicateupadate=$this->getParamItem('enrolreplicateupadate');
		  $timestart=$this->getParamItem('timestart');
		  $timeend=$this->getParamItem('timeend');
		 
		  $isedit=$this->isEdit();
		  $fparam=array('id'=>$enrolid,'classeid'=>$classeid,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid,'isedit'=>$isedit,'enrolreplicate'=>$enrolreplicate,'enrolreplicateupadate'=>$enrolreplicateupadate,'timestart'=>$timestart,'timeend'=>$timeend);
		 
		  $this->getContainer()->get('badiu.ams.enrol.classe.lib')->replicate($fparam);
		  parent::execAfter();
	 }
    
}
