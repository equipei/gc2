<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Admin\AttendanceBundle\Model\PlannedData;

class ClassePlannedData  extends PlannedData {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
  

  public function getTimesByClasseid($entity,$classeid) {
		$r=FALSE;
            $sql="SELECT  o.id,o.timestart,o.timeend FROM ".$this->getBundleEntity()." o  WHERE o.entity= :entity AND o.modulekey = :modulekey AND o.moduleinstance = :moduleinstance AND o.deleted=:deleted";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('modulekey','badiu.ams.offer.classeattendance');
			$query->setParameter('moduleinstance',$classeid);
			$query->setParameter('deleted',false);
			$result= $query->getResult();
            return $result;
   }

}
