<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class ClassePlannedDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
    
    public  function getFormChoice(){
        $data=$this->getContainer()->get('badiu.ams.offer.classeplanned.data');
        $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $entity = $badiuSession->get()->getEntity();
		
        $list=$data->getTimesByClasseid($entity,$parentid);
		
        $newlist=array();
        $dateformat=$this->getContainer()->get('badiu.system.core.lib.format.dateformat');
        $cont=0;
        foreach ($list as $row) {
            $cont++;
            $timestart =$this->getUtildata()->getVaueOfArray($row,'timestart');
            $timeend = $this->getUtildata()->getVaueOfArray($row,'timeend');
            $id = $this->getUtildata()->getVaueOfArray($row,'id');
            $seq=$this->getTranslator()->trans('badiu.ams.offer.classeplanned.seq',array('%number%'=>$cont));
            if ($timestart!=null && $timeend!=null ) {
                $result= $dateformat->period($timestart,$timeend);
                $newlist[$id]=$seq.' - '.$result;
            }
        
        }
        return  $newlist;
    }
    
    public  function getClasseTeacher(){
		 $list=array();
        $enroldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
        $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		if(empty($parentid)){ return $list;}
        $list=$enroldata->getUserFormChoice($parentid);
       
        return $list;
    }
	

}
