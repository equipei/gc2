<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
use Badiu\Admin\AttendanceBundle\Model\AttendanceFormat;
class ClasseAttendanceFormat extends AttendanceFormat {
  private $attfomdataoptions;

    function __construct(Container $container) {
        parent::__construct($container);
    	$this->attfomdataoptions=$this->getContainer()->get('badiu.ams.offer.classeattendance.form.dataoptions');
  }
   
  public function criteria($data) {
      
      $result = null;
      $criteria =$this->getUtildata()->getVaueOfArray($data,'criteria');
	  $result = $this->attfomdataoptions->getCriteriaLabel($criteria);
      return $result;
  }
}
