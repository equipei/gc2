<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class SyncLmsManagerFormController extends BadiuFormController
{ 
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     
     public function save() {
          $param = $this->getParam();
        
		 $classeid=$this->getParamItem('classeid');
		 $typeoperation=$this->getParamItem('typeoperation');
		 $importfromlmsoptions=$this->getParamItem('importfromlmsoptions');
		 $exporttolmsoptions=$this->getParamItem('exporttolmsoptions');
		 $entity=$this->getEntity();
		 
		
		  $classedata=$this->getContainer()->get('badiu.ams.offer.classe.data');
		  $dconfig=$classedata->getColumnValue($entity,'dconfig',$paramf=array('id'=>$classeid));
		  $dconfig = $this->getJson()->decode($dconfig, true);	
		  $param['dconfig']=$dconfig;
		  
		 
		 if($typeoperation=='import' && $importfromlmsoptions=='coursefinalgrade'){
			  $lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesync');
			  $result=$lmsmoodlesync->importGradesFromMoodle($param);
			  $coutexec=$this->getUtildata()->getVaueOfArray($result,'executed');
			  $coutshouldprocess=$this->getUtildata()->getVaueOfArray($result,'shouldprocess');
			  $this->setSuccessmessage($this->getTranslator()->trans('badiu.ams.offer.classesynclmsmanager.messagegradeimport',array('%coutexec%'=>$coutexec,'%coutshouldprocess%'=>$coutshouldprocess))); 
		 }else if($typeoperation=='import' && $importfromlmsoptions=='coursecompletation'){ 
			  $lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesync');
			  $result=$lmsmoodlesync->importCourseEnrolCompletationFromMoodle($param);
			  $coutexec=$this->getUtildata()->getVaueOfArray($result,'executed');
			  $coutshouldprocess=$this->getUtildata()->getVaueOfArray($result,'shouldprocess');
			  $this->setSuccessmessage($this->getTranslator()->trans('badiu.ams.offer.classesynclmsmanager.messagecoursecompletationimport',array('%coutexec%'=>$coutexec,'%coutshouldprocess%'=>$coutshouldprocess))); 

		 }else if($typeoperation=='import' && $importfromlmsoptions=='courseprogress'){ 
			  $lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesync');
			  $result=$lmsmoodlesync->importCourseEnrolProgressFromMoodle($param);
			  $coutexec=$this->getUtildata()->getVaueOfArray($result,'executed');
			  $coutshouldprocess=$this->getUtildata()->getVaueOfArray($result,'shouldprocess');
			  $this->setSuccessmessage($this->getTranslator()->trans('badiu.ams.offer.classesynclmsmanager.messagecoursecompletationimport',array('%coutexec%'=>$coutexec,'%coutshouldprocess%'=>$coutshouldprocess))); 

		 }
		 
		  
        
          //$result=$tdata->exec();
          $outrsult=array('result'=>$result,'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
          $this->getResponse()->setStatus("accept");
          $this->getResponse()->setMessage($outrsult);
         return $this->getResponse()->get();
         
     }
    
}
