<?php

namespace Badiu\Ams\OfferBundle\Model\Classe\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ClasseAttendanceManager extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

	 public function teste() {
		 $param=array();
		 $param['roleshortname']='student';
		 $param['plannedid']=6;
		 $param['statusshortname']='active';
		 $param['classeid']=109;
	
		$result=$this->addUser($param);
		 
	 }
    public function addUser($param) {
         
		 $entity= $this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		 
		 $marker=$this->getUtildata()->getVaueOfArray($param,'marker');
		 
		 //get user enrol profile
		 $enrolroleid=$this->getUtildata()->getVaueOfArray($param,'roleid');
		 if(empty($enrolroleid)){
			 $enrolroleshortname= $this->getUtildata()->getVaueOfArray($param,'roleshortname');
			 if(empty($enrolroleshortname)){$enrolroleshortname='student';}
			  $enrolroleid=$this->getContainer()->get('badiu.ams.role.role.data')->getIdByShortname($entity,$enrolroleshortname);
		 }
		
		//get entrol status 
		 $enrolstatusid=$this->getUtildata()->getVaueOfArray($param,'statusid');
		 if(empty($enrolstatusid)){
			 $enrolstatusshortname= $this->getUtildata()->getVaueOfArray($param,'statusshortname');
			 if(empty($enrolstatusshortname)){$enrolroleshortname='active';}
			  $enrolstatusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,$enrolstatusshortname);
		 }		
		 //get planned id
		 $plannedid= $this->getUtildata()->getVaueOfArray($param,'plannedid');
		 
		 //get classeid
		 $classeid= $this->getUtildata()->getVaueOfArray($param,'classeid');
		 
		 $hroomid=$this->getUtildata()->getVaueOfArray($param, 'hroomid');
		 $timestart=$this->getUtildata()->getVaueOfArray($param, 'timestart');
		 $timeend=$this->getUtildata()->getVaueOfArray($param, 'timeend');
		 $dhour=$this->getUtildata()->getVaueOfArray($param, 'dhour');
		 $criteria=$this->getUtildata()->getVaueOfArray($param, 'criteria'); 
		 $statusinfo=$this->getUtildata()->getVaueOfArray($param, 'statusinfo'); 
		
		 $daykey=null;
		 if(!empty($timestart)){$daykey=$timestart->format('Y_m_d');}
		
		 
		 //get list enrol
		 $enrolclassedata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 //$efparam=array('roleid'=>$enrolroleid,'statusid'=>$enrolstatusid);
		 $efparam=array('roleid'=>$enrolroleid);
		 if(!empty($marker)){$efparam['marker']=$marker;}
		
		$listuser=$enrolclassedata->getUsersidStatusenable($classeid,$efparam);
		
		 $cont=0;
		
		 if(is_array($listuser)){
			$attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
			foreach ($listuser as $rowe) {
				$userid=$this->getUtildata()->getVaueOfArray($rowe,'id');
				 //loop
				$paramcheckexist=array('entity'=>$entity,'plannedid'=>$plannedid,'userid'=>$userid,'modulekey'=>'badiu.ams.offer.classeattendance','moduleinstance'=>$classeid);
				$paramadd=array('entity'=>$entity,'plannedid'=>$plannedid,'statusinfo'=>$statusinfo,'hroomid'=>$hroomid,'timestart'=>$timestart,'timeend'=>$timeend,'daykey'=>$daykey,'dhour'=>$dhour,'criteria'=>$criteria,'userid'=>$userid,'modulekey'=>'badiu.ams.offer.classeattendance','moduleinstance'=>$classeid,'deleted'=>0,'timecreated'=>new \DateTime());
				$paramedit=null;
				
				$dresult=$attendancedata->addNativeSql($paramcheckexist,$paramadd,$paramedit);
				$rid=$this->getUtildata()->getVaueOfArray($dresult,'id');
				if($rid){ $cont++;} 
        	}
		 }
		
		 
		
		return  $cont;
    }
// /system/service/process?_service=badiu.ams.offer.classeattendance.attendancemanager&_function=fillNewUserInPlanned
     public function fillNewUserInPlanned() {
		  $planneddata=$this->getContainer()->get('badiu.admin.attendance.planned.data');
		  $entity=$this->getEntity();
		  $modulekey='badiu.ams.offer.classeattendance';
		  $listplannedrow= $planneddata->getGlobalColumnsValues('o.id,o.timestart,o.timeend,o.hourduration,o.moduleinstance,o.criteria', array('entity'=>$entity,'modulekey'=> $modulekey));
			
		 foreach ($listplannedrow as $row) {
			 $param=array();
			 $param['roleshortname']='student';
			 $param['plannedid']=$this->getUtildata()->getVaueOfArray($row,'id');
			 $param['statusshortname']='active';
			 $param['classeid']=$this->getUtildata()->getVaueOfArray($row,'moduleinstance');
			 $param['statusinfo']='planned';
			 $param['hroomid']=null; 
			 $param['criteria']=$this->getUtildata()->getVaueOfArray($row,'criteria');
			 $param['dhour']=$this->getUtildata()->getVaueOfArray($row,'hourduration');
			 $param['timestart']=$this->getUtildata()->getVaueOfArray($row,'timestart');
			 $param['timeend']=$this->getUtildata()->getVaueOfArray($row,'timeend');
			 $this->addUser($param); 
		 }
		 
	 }

}
