<?php
namespace Badiu\Ams\OfferBundle\Model\Classe\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class ClasseUpdateEnrolFilter extends BadiuFormFilter{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
   public function execBeforeSubmit() {
    
        }
        
   public function execAfterSubmit() {
		 $enroldatetype=$this->getUtildata()->getVaueOfArray($this->getParam(),'enroldatetype');//classe |discipline |offer | dynamic
		 $enroldatedynamic=$this->getUtildata()->getVaueOfArray($this->getParam(),'enroldatedynamic');
		 $enrolreplicate=$this->getUtildata()->getVaueOfArray($this->getParam(),'enrolreplicate');
		 $enrolreplicateupadate=$this->getUtildata()->getVaueOfArray($this->getParam(),'enrolreplicateupadate');
		 $entity=$this->getEntity();
		 $classeid=$this->getUtildata()->getVaueOfArray($this->getParam(),'id');
		 $timestart=null;
		 $timeend=null;
		 $mdltimestart=0;
		 $mdltimeend=0;
		
		 if($enroldatetype=='classe'){
			$timestart= $this->getUtildata()->getVaueOfArray($this->getParam(),'timestart');
			$timeend= $this->getUtildata()->getVaueOfArray($this->getParam(),'timeend');
			
			if (is_a($timestart, 'DateTime')) {$mdltimestart=$timestart->getTimestamp();}
			if (is_a($timeend, 'DateTime')) {$mdltimeend=$timeend->getTimestamp();}
		 }else if($enroldatetype=='discipline'){}
		 else if($enroldatetype=='offer'){}
		 else if($enroldatetype=='dynamic'){}
		
		$replicateenrolclasse=$this->getUtildata()->existStringInTextList('classe',$enrolreplicate);
		$replicateenroldiscipline=$this->getUtildata()->existStringInTextList('discipline',$enrolreplicate);
		$replicateenroloffer=$this->getUtildata()->existStringInTextList('offer',$enrolreplicate);
		$replicateenrollmsmoodle=$this->getUtildata()->existStringInTextList('lmsmoodle',$enrolreplicate);
		
		$upadateenrolclasse=$this->getUtildata()->existStringInTextList('classe',$enrolreplicateupadate);
		$upadateenroldiscipline=$this->getUtildata()->existStringInTextList('discipline',$enrolreplicateupadate);
		$upadateenroloffer=$this->getUtildata()->existStringInTextList('offer',$enrolreplicateupadate);
		$upadateenrollmsmoodle=$this->getUtildata()->existStringInTextList('lmsmoodle',$enrolreplicateupadate);
			
		 
		 $classeenroldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 
		 $disciplineenrollib=$this->getContainer()->get('badiu.ams.enrol.discipline.lib');
		 $offerenrollib=$this->getContainer()->get('badiu.ams.enrol.offer.lib');
		 
		 $enrollist= $classeenroldata->getList($entity,$classeid);
		 
		  $classedata=$this->getContainer()->get('badiu.ams.offer.classe.data');
		 $classenparent=$classedata->getNameParent($classeid);
		 
		 $odisciplineid=$this->getUtildata()->getVaueOfArray($classenparent,'odisciplineid');
		 $offerid=$this->getUtildata()->getVaueOfArray($classenparent,'offerid');
		 
		 if(!is_array($enrollist)){return null;}
		 
		 $dconfig=$this->getUtildata()->getVaueOfArray($this->getParam(),'dconfig');
		 if(!is_array($dconfig)){ $dconfig = $this->getJson()->decode($dconfig, true); }
		
		 $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		 $serviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
		 $lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynclevel',true);
		 $lmscoursecatid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatid',true);
		 $groupid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursegroupid',true);		
		 $lmscourseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);				
	     $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		
						
		 foreach ($enrollist as $row) {
			$id=$this->getUtildata()->getVaueOfArray($row,'id');	
			$userid=$this->getUtildata()->getVaueOfArray($row,'userid');
			$roleid=$this->getUtildata()->getVaueOfArray($row,'roleid');
			$statusid=$this->getUtildata()->getVaueOfArray($row,'statusid');
			$marker=$this->getUtildata()->getVaueOfArray($row,'marker');
			
			if($upadateenrolclasse){
				$cuparam=array('id'=>$id,'timestart'=>$timestart,'timeend'=>$timeend,'timemodified'=>new \DateTime());
				$result=$classeenroldata->updateNativeSql($cuparam,false);
			}
			
			if($replicateenroldiscipline){
				$disciplineenrollib->add($odisciplineid,$userid,$roleid,$statusid,$timestart,$timeend,$marker,$upadateenroldiscipline);
			}
			
			if($replicateenroloffer){
				$offerenrollib->add($offerid,$userid,$roleid,$statusid,$timestart,$timeend,$marker,$upadateenroloffer);
			}
			
			if($replicateenrollmsmoodle && ($lmssynclevel=='course' || $lmssynclevel=='group') &&  $lmscourseid > 0 && $serviceid > 0){
				$mmelparam=array();
				$mmelparam['userid']=$userid;		
				$mmelparam['courseid']=$lmscourseid;
				$mmelparam['groupid']=$groupid;
				$mmelparam['sserviceid']=$serviceid;
				if($upadateenrollmsmoodle){$mmelparam['forceupdate']=1;}
				$mmelparam['timestart']=$mdltimestart;
				$mmelparam['timeend']=$mdltimeend;
				$managesyncmoodle= $this->getContainer()->get('badiu.ams.enrol.lib.managesyncmoodle');
				$managesyncmoodle->process($mmelparam);
			 
			}
			
		 }
		 
   }
}
