<?php

namespace Badiu\Ams\OfferBundle\Model\Classe\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ClasseAttendanceMenu extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function addfrequenceforallstudent() {
         $classeid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
         $plannedid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('classeplannedid');
         $urlback=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urlback');
         $urlback=urldecode($urlback);
         if(empty($plannedid)){ header("Location: $urlback");}
         if(empty($classeid)){ header("Location: $urlback");}
         //get list of enrol
         $list=$this->getContainer()->get('badiu.ams.enrol.classe.data')->getUsersid($classeid);
         $badiuSession=$this->getContainer()->get('badiu.system.access.session');
         $entity=$badiuSession->get()->getEntity();
         $dcattendance=$this->getContainer()->get('badiu.ams.offer.classeattendance.data');
         $phour=100;
         foreach ($list as $l) {
             $userid=$this->getUtildata()->getVaueOfArray($l,'id');
             $dcattendance->add($entity,$plannedid,$userid,$phour,$classeid);
         }
     
          //echo $urlback;exit;
         header("Location: $urlback");
        exit;
        return null;
    }

    

}
