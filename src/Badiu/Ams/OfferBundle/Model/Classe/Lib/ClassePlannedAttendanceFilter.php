<?php
namespace Badiu\Ams\OfferBundle\Model\Classe\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class ClassePlannedAttendanceFilter extends BadiuFormFilter{
    private $enrolstatusshortname;
	private $enrolroleshortname;
    function __construct(Container $container) {
            parent::__construct($container);
				$this->enrolstatusshortname='active';
				$this->enrolroleshortname='student';
              }
    
   public function execBeforeSubmit() {
    
        }
        
   public function execAfterSubmit() {
		$targettype=$this->getUtildata()->getVaueOfArray($this->getParam(),'targettype');
		 if($targettype=='oneuser'){return null;}
		
	     $this->execAfterSubmitForAutomaticfill();
		 $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
		 
		
		 if(empty($id)){return null;}
		 $classeid = $this->getUtildata()->getVaueOfArray($this->getParam(), 'moduleinstance');
		 
		  
		 $param=array();
		 $param['roleshortname']=$this->getEnrolroleshortname();
		 $param['plannedid']=$id;
		 $param['statusshortname']=$this->getEnrolstatusshortname(); 
		 $param['classeid']=$classeid;
		 $param['hroomid']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'hroomid');
		 $param['timestart']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'timestart');
		 $param['timeend']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'timeend');
		 $param['dhour']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'hourduration');
		 $param['criteria']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'criteria'); 
		 $param['statusinfo']='planned';
		 $attendancemanager=$this->getContainer()->get('badiu.ams.offer.classeattendance.attendancemanager');
		 
		 $attendancemanager->addUser($param);
		
       }
    
	public function execAfterSubmitForAutomaticfill() {
         $automaticfillexec = $this->getUtildata()->getVaueOfArray($this->getParam(), 'automaticfillexec');
		
		 if(empty($automaticfillexec)){return null;}
		 if(!is_array($automaticfillexec)){return null;}
		 
		 $classeid = $this->getUtildata()->getVaueOfArray($this->getParam(), 'moduleinstance');
		 $listid = $this->getUtildata()->getVaueOfArray($automaticfillexec, 'listidsexec');
		  
		 if(empty($listid)){return null;}
		  if(!is_array($listid)){return null;}
		  $attendancemanager=$this->getContainer()->get('badiu.ams.offer.classeattendance.attendancemanager');
		  
		   $planneddata=$this->getContainer()->get('badiu.admin.attendance.planned.data');
		 foreach ($listid as $id) {
			 $plannedrow= $planneddata->getGlobalColumnsValue('o.timestart,o.timeend,o.hourduration', array('id'=>$id));
			 $param=array();
			 $param['roleshortname']=$this->getEnrolroleshortname();
			 $param['plannedid']=$id;
			 $param['statusshortname']=$this->getEnrolstatusshortname(); 
			 $param['classeid']=$classeid;
			 $param['statusinfo']='planned';
			 $param['hroomid']=$this->getUtildata()->getVaueOfArray($this->getParam(),'hroomid');
			 $param['criteria']=$this->getUtildata()->getVaueOfArray($this->getParam(),'criteria');
			 $param['dhour']=$this->getUtildata()->getVaueOfArray($plannedrow,'hourduration');
			 $param['timestart']=$this->getUtildata()->getVaueOfArray($plannedrow,'timestart');
			 $param['timeend']=$this->getUtildata()->getVaueOfArray($plannedrow,'timeend');
			 $attendancemanager->addUser($param); 
		 }
		 
		 
       }	
	   
	   
	    /**
     * @return string
     */
    public function getEnrolstatusshortname()
    {
        return $this->enrolstatusshortname;
    }

    /**
     * @param string $enrolstatusshortname
     */
    public function setEnrolstatusshortname($enrolstatusshortname)
    {
        $this->enrolstatusshortname = $enrolstatusshortname;
    }
	
	/**
     * @return string
     */
    public function getEnrolroleshortname()
    {
        return $this->enrolroleshortname;
    }

    /**
     * @param string $enrolroleshortname
     */
    public function setEnrolroleshortname($enrolroleshortname)
    {
        $this->enrolroleshortname = $enrolroleshortname;
    }
}
