<?php

namespace Badiu\Ams\OfferBundle\Model\Classe\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ClasseUtil extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

	
    public function isActive($param) {
         $timestart=$this->getUtildata()->getVaueOfArray($param,'timestart');
		 $timeend=$this->getUtildata()->getVaueOfArray($param,'timeend');
		 $statusshortname=$this->getUtildata()->getVaueOfArray($param,'statusshortname');
		
		 if($statusshortname != 'classeongoing' && $statusshortname != 'ongoing') {return false;}
	    
		$now=new \DateTime();
		//timestart
		if(!empty($timestart)  && is_a($timestart, 'DateTime')){
			if($timestart->getTimestamp() > $now->getTimestamp()){return false;}
		}
		
		//timeend
		if(!empty($timeend)  && is_a($timeend, 'DateTime')){
			if($timeend->getTimestamp() < $now->getTimestamp()){return false;}
		}
		return  true;
    }


}
