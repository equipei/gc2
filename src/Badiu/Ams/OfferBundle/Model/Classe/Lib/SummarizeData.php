<?php
namespace Badiu\Ams\OfferBundle\Model\Classe\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Report\SummarizeData as ReportSummarizeData;
class SummarizeData  extends ReportSummarizeData{

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
	function teste(){
		//$fparam=array('_period'=>'allyears','classetypeshortname'=>'trainingpresential');
		$fparam=array('_period'=>'allyears');
		$result=$this->countByYear($fparam); 
		
		echo "<pre>"; 
		print_r($result);
		echo "</pre>";exit;
	}
	
	function countByYearSearch($param,$dresult){
		$operation=$this->getUtildata()->getVaueOfArray($param,'operation');
		$statusshortname=$this->getUtildata()->getVaueOfArray($param,'statusshortname');
		$typeaccesshortname=$this->getUtildata()->getVaueOfArray($param,'typeaccessshortname');
		$typeshortname=$this->getUtildata()->getVaueOfArray($param,'typeshortname');
		$disciplinedtype=$this->getUtildata()->getVaueOfArray($param,'disciplinedtype');
		$offershortname=$this->getUtildata()->getVaueOfArray($param,'offershortname');
		$isclasseactive=$this->getUtildata()->getVaueOfArray($param,'isclasseactive');
		
		
		$ddate1=$this->getUtildata()->getVaueOfArray($param,'_date1');
		$ddate2=$this->getUtildata()->getVaueOfArray($param,'_date2');
		$year=$this->getUtildata()->getVaueOfArray($param,'_year');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		
		
		
		if(empty($ntity)){$entity=$this->getEntity();}
		if(empty($operation)){$operation="countclasse";}
		
		$wsql="";
		if($isclasseactive){
			 $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->activefilter($fparam);
		}
		if(!empty($statusshortname)){$wsql.=" AND cls.shortname=:statusshortname ";}
		if(!empty($typeaccesshortname)){$wsql.=" AND clta.shortname=:typeaccesshortname ";}
		if(!empty($typeshortname)){$wsql.=" AND clt.shortname=:typeshortname ";}
		if(!empty($disciplinedtype)){$wsql.=" AND d.dtype=:disciplinedtype ";}
		if(!empty($offershortname)){$wsql.=" AND f.shortname=:offershortname ";}
		
		$column="cl.id";
		if($operation=='countdiscipline'){$column=" DISTINCT d.id ";}
		$sql="SELECT COUNT($column) AS countrecord FROM BadiuAmsOfferBundle:AmsOfferClasse cl LEFT JOIN cl.statusid cls LEFT JOIN cl.typeid clt LEFT JOIN cl.typeaccessid clta JOIN cl.odisciplineid od JOIN od.disciplineid d LEFT JOIN d.categoryid dct JOIN od.offerid f WHERE  cl.entity=:entity AND cl.deleted=:deleted  AND cl.timestart >=:ddate1 AND cl.timestart <=:ddate2  $wsql ";
		
		$data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
		$query->setParameter('deleted', 0);

		$query->setParameter('ddate1',$ddate1);
		$query->setParameter('ddate2',$ddate2);
		
		if(!empty($statusshortname)){$query->setParameter('statusshortname', $statusshortname);}
		if(!empty($typeaccesshortname)){$query->setParameter('typeaccesshortname', $typeaccesshortname);}
		if(!empty($typeshortname)){$query->setParameter('typeshortname', $typeshortname);}
		if(!empty($disciplinedtype)){$query->setParameter('disciplinedtype', $disciplinedtype);}
		if(!empty($offershortname)){$query->setParameter('offershortname',$offershortname);}
		
		if($isclasseactive){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		}
		
		$result= $query->getOneOrNullResult();
		
		$countrecord=$this->getUtildata()->getVaueOfArray($result,'countrecord');
		$dresult[$year]=$countrecord;
		return $dresult;
	}
	 
	 function getMinMaxYear($param){
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		$statusshortname=$this->getUtildata()->getVaueOfArray($param,'statusshortname');
		$typeaccesshortname=$this->getUtildata()->getVaueOfArray($param,'typeaccessshortname');
		$typeshortname=$this->getUtildata()->getVaueOfArray($param,'typeshortname');
		$disciplinedtype=$this->getUtildata()->getVaueOfArray($param,'disciplinedtype');
		$offershortname=$this->getUtildata()->getVaueOfArray($param,'offershortname');
		$isclasseactive=$this->getUtildata()->getVaueOfArray($param,'isclasseactive');
		
		
		if(empty($ntity)){$entity=$this->getEntity();}
		
		
		$wsql="";
		if($isclasseactive){
			 $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->activefilter($fparam);
			
		}
		
		if(!empty($statusshortname)){$wsql.=" AND cls.shortname=:statusshortname ";}
		if(!empty($typeaccesshortname)){$wsql.=" AND clta.shortname=:typeaccesshortname ";}
		if(!empty($typeshortname)){$wsql.=" AND clt.shortname=:typeshortname ";}
		if(!empty($disciplinedtype)){$wsql.=" AND d.dtype=:disciplinedtype ";}
		if(!empty($offershortname)){$wsql.=" AND f.shortname=:offershortname ";}
		
		$timenullcontrol = new \DateTime();
		$timenullcontrol->setDate(1970, 1, 1);
		
		
		$sql="SELECT MIN(cl.timestart) AS firstdate, MAX(cl.timestart) AS lastdate  FROM BadiuAmsOfferBundle:AmsOfferClasse cl LEFT JOIN cl.statusid cls LEFT JOIN cl.typeid clt LEFT JOIN cl.typeaccessid clta JOIN cl.odisciplineid od JOIN od.disciplineid d LEFT JOIN d.categoryid dct JOIN od.offerid f WHERE  cl.entity=:entity AND cl.timestart > :timenullcontrol AND cl.deleted=:deleted   $wsql ";
		
		$data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
		$query->setParameter('deleted', 0);
		$query->setParameter('timenullcontrol', $timenullcontrol);	
		
		if(!empty($statusshortname)){$query->setParameter('statusshortname', $statusshortname);}
		if(!empty($typeaccesshortname)){$query->setParameter('typeaccesshortname', $typeaccesshortname);}
		if(!empty($typeshortname)){$query->setParameter('typeshortname', $typeshortname);}
		if(!empty($disciplinedtype)){$query->setParameter('disciplinedtype', $disciplinedtype);}
		if(!empty($offershortname)){$query->setParameter('offershortname',$offershortname);}
		
		if($isclasseactive){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
			
		}
		
		
		$result= $query->getOneOrNullResult();
		
		return $result;
	}
}
