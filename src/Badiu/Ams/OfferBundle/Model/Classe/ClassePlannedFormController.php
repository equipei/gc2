<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ClassePlannedFormController extends BadiuFormController
{ 
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     public function changeParamOnOpen() {
         if(!$this->isEdit()){return null;}
        $param = $this->getParam();
        $timestart=$this->getParamItem('timestart');
        if(!empty($timestart)){
            $t1 = new \DateTime();
            $t1->setTimestamp($timestart);
            $param['hourstart']=($t1->format('H')*60)+$t1->format('i');
        }
        
        $timeend=$this->getParamItem('timeend');
        if(!empty($timeend)){
            $t2 = new \DateTime();
            $t2->setTimestamp($timeend);
            $param['hourend']=($t2->format('H')*60)+$t2->format('i');
        }

        $dconfig=$this->getParamItem('dconfig');
        $dconfig = $this->getJson()->decode($dconfig, true);
      
        $param['responsibleteacher']=$this->getUtildata()->getVaueOfArray( $dconfig, 'responsibleteachers');
      
         $this->setParam($param);
    
     }   
     
    public function changeParam() {

  
        $param = $this->getParam();

        $datestart=$this->getParamItem('timestart');
        $hourstart=$this->getParamItem('hourstart');
        $hourend=$this->getParamItem('hourend');
        $hourduration=null;
        if(!empty($hourstart) && !empty($hourend)){$hourduration=$hourend-$hourstart;}
        if(!empty($datestart)){
             $dateend=clone $datestart;
             $hourstart=$this->getContainer()->get('badiu.system.core.lib.form.casthour')->convertToForm($hourstart);
             if(is_array($hourstart)){ $datestart->setTime($hourstart['hour'],$hourstart['minute']);}
             
             $hourend=$this->getContainer()->get('badiu.system.core.lib.form.casthour')->convertToForm($hourend);
             if(is_array($hourend)) {$dateend->setTime($hourend['hour'],$hourend['minute']);}
             
            
             $param['timestart']=$datestart;   
             $param['timeend']=$dateend;   
             $param['hourduration']=$hourduration;   
			
        } 
		
		
        $responsibleteacher=$this->getParamItem('responsibleteacher');
        $teachers=array();
        if(!empty($responsibleteacher)){
            $pos=stripos($responsibleteacher, ",");
            if($pos=== false){
                $teachers=array($responsibleteacher);
            }else{
                $teachers= explode(",", $responsibleteacher);
            }
        }
        $cdbdconfig=array();
        if($this->isEdit()){
            $id=$this->getParamItem('id');
            $data=$this->getContainer()->get($this->getKminherit()->data());
            $cdbdconfig=$data->getColumnValue($this->getEntity(),'dconfig',$paramf=array('id'=>$id));
            $cdbdconfig = $this->getJson()->decode($cdbdconfig, true);
            if(empty($cdbdconfig)){$cdbdconfig=array();}
        }
         $cdbdconfig['responsibleteachers']=$teachers;

         $cdbdconfig = $this->getJson()->encode($cdbdconfig);
         $param['dconfig']=$cdbdconfig;
        
        $this->setParam($param);
         $this->removeParamItem('hourstart');
         $this->removeParamItem('hourend');
         $this->removeParamItem('responsibleteacher');
       }
     
     
    
}
