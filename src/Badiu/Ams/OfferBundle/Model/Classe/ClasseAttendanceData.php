<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ClasseAttendanceData  extends BadiuDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
  
   public function existUser($entity,$plannedid,$userid) {
		$r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.plannedid=:plannedid AND o.userid=:userid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('plannedid',$plannedid);
			$query->setParameter('userid',$userid);
			$result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
   }
    public function getIdByUser($entity,$plannedid,$userid) {
		$r=FALSE;
            $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.plannedid=:plannedid AND o.userid=:userid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
	    $query->setParameter('plannedid',$plannedid);
	    $query->setParameter('userid',$userid);
	    $result= $query->getSingleResult();
            if(isset($result['id'])){$result=$result['id'];}
            return $result;
   }
    
    public function add($entity,$plannedid,$userid,$phour,$classeid,$customparam=array()) {
                $exist=$this->existUser($entity,$plannedid,$userid);
                $result=null;
                $dhour=$this->getContainer()->get('badiu.ams.offer.classeplanned.data')->getGlobalColumnValue('hourduration',array('id'=>$plannedid));
                //if (array_key_exists("dhour",$customparam)&& !empty($customparam['dhour'])){$dhour=$customparam['dhour'];}
                
                if(empty($dhour)){return null;}
                $thour=0.01*$phour*$dhour;
              
                 if($exist){
                     $id=$this->getIdByUser($entity,$plannedid,$userid);
                    // if (!array_key_exists("dhour",$customparam)){$dhour=$this->getGlobalColumnValue('dhour',array('id'=>$id));}
                     $uparam=array('id'=>$id,'thour'=>$thour,'dhour'=>$dhour,'timemodified'=>new \DateTime());
                     $uparam= array_merge($uparam,$customparam);
                     $result=$this->updateNativeSql($uparam,false);
                 }else{
                     $iparam=array('entity'=>$entity,'userid'=>$userid,'plannedid'=>$plannedid,'dhour'=>$dhour,'thour'=>$thour,'classeid'=>$classeid,'deleted'=>0,'timecreated'=>new \DateTime());
                     $iparam= array_merge($iparam,$customparam);
                     $result=$this->insertNativeSql($iparam,false);
                     
                 }
               
            return $result;
   }
  
   //http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_function=addByParam&_service=badiu.ams.offer.classeattendance.data&classeid=1&plannedid=20&userid=143&dhour=8
   public function addByParam() {
               $plannedid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('plannedid');
               $userid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('userid'); 
               $phour=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('phour'); 
               $classeid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('classeid'); 
              
               $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                $entity=$badiuSession->get()->getEntity();
                
		$result=$this->add($entity,$plannedid,$userid,$phour,$classeid);
            return $result;
   }

   public function getRoomById($id) {
  
    $sql="SELECT  r.id,r.name FROM ".$this->getBundleEntity()." o JOIN o.hroomid r WHERE o.id = :id";
    $query = $this->getEm()->createQuery($sql);
    $query->setParameter('id',$id);
    $result= $query->getSingleResult();
    return $result;
}
}
