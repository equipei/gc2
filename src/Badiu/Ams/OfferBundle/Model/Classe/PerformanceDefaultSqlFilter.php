<?php
namespace Badiu\Ams\OfferBundle\Model\Classe;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class PerformanceDefaultSqlFilter extends BadiuSqlFilter{
    /**
     * @var object
     */
   private  $sqlservice;
   

    function __construct(Container $container) {
            parent::__construct($container);
     }

        function addfields() {
			$entity=$this->getEntity();
			$classeid=$this->getUtildata()->getVaueOfArray($this->getParam(), 'parentid');
			$rparam=array('instanceid'=>$classeid,'level'=>'classe','source'=>'db');
			$roleconclusionlib=$this->getContainer()->get('badiu.ams.core.lib.roleconclusionlib');
			$conclusionconfig=$roleconclusionlib->config($rparam);
			
			
			$hasdhour=$this->getUtildata()->getVaueOfArray($conclusionconfig, 'conclusion.discipline.defaultroles.dhour.minperc',true);
			$hasdgrade=$this->getUtildata()->getVaueOfArray($conclusionconfig, 'conclusion.discipline.defaultroles.grade.minperc',true);
			
			
			$attendance=$this->getContainer()->get('badiu.admin.attendance.attendance.defaultsqlfilter');
			$attendance->setSessionhashkey($this->getSessionhashkey());
			$attendance->setParam($this->getParam());
			
			$sql="";
			if($hasdhour){
				$param1=array('_prefix'=>2,'_aliasuser'=>'o.userid','_aliasmodulekey'=>"'badiu.ams.offer.classeattendance'",'_aliasmoduleinstance'=>$classeid,'_column'=>'thour','_operation'=>'SUM','_columnalias'=>'countexecutedhour','_statusinfo'=>'executed');
				$sql.=$attendance->getCommandsqlUserAgrgegate($param1);
			
			}
			if($hasdgrade){
				$param1=array('_prefix'=>3,'_aliasuser'=>'o.userid','_columnalias'=>'finalgrade');
				$sql.=$this->getCommandsqlFinalGrade($param1);
			}
            return $sql;
        }
     

	 function getCommandsqlFinalGrade($param) {
          $fqueryn=$this->getContainer()->get('badiu.system.core.lib.sql.factoryqueryselectnative');
          $prefix= $this->getUtildata()->getVaueOfArray($param,'_prefix');
          $columnalias= $this->getUtildata()->getVaueOfArray($param,'_columnalias');
		  $useralias= $this->getUtildata()->getVaueOfArray($param,'_aliasuser');
		  $classeid=$this->getUtildata()->getVaueOfArray($this->getParam(), 'parentid');
		  
		  $wsql="";
		    
          $csql=" SELECT o$prefix.grade FROM BadiuAmsGradeBundle:AmsGrade o$prefix JOIN  o$prefix.itemid i$prefix WHERE o$prefix.userid=$useralias AND i$prefix.modulekey='badiu.ams.offer.classe' AND i$prefix.moduleinstance=$classeid AND i$prefix.itemtype='finalgrade'";
		  $sql=",($csql) AS $columnalias  ";
		 
		  return $sql;
	  } 
		  
          
}
