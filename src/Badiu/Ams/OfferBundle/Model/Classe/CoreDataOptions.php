<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class CoreDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }

    public  function gradeItem(){
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
         $entity=$badiuSession->get()->getEntity();
		$itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		$classeid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		$fparam=array('modulekey'=>'badiu.ams.offer.classe','moduleinstance'=>$classeid,'dtype'=>'grade');
		$list=$itemdata->getFormChoice($entity,$fparam);
       return $list;
    }

    
}
