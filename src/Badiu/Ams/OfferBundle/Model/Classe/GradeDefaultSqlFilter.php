<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class GradeDefaultSqlFilter extends BadiuSqlFilter{
  
  
    function __construct(Container $container) {
            parent::__construct($container);
       }

        function exec() {
           
           return null;
        }
         function addcolumnsoverview(){
			 $wsql="";
			 $fparam=array('pfx'=>1,'itemtype'=>'progress','moduleinstncealias'=>'o.classeid','modulekey'=>'badiu.ams.offer.classe','useralias'=>'o.userid','selectcolumn'=>'dconfig');
			 $wsql.=$this->addColumn($fparam);
			
			$fparam=array('pfx'=>2,'itemtype'=>'finalgrade','moduleinstncealias'=>'o.classeid','modulekey'=>'badiu.ams.offer.classe','useralias'=>'o.userid');
			 $wsql.=$this->addColumn($fparam);
			 
			 $fparam=array('pfx'=>3,'itemtype'=>'finalcompletation','moduleinstncealias'=>'o.classeid','modulekey'=>'badiu.ams.offer.classe','useralias'=>'o.userid');
			 $wsql.=$this->addColumn($fparam);
			
			 
			return  $wsql; 
		 }
       
	  function addfilteroverview(){
		  
			 $wsql="";
			 $fparam=array('pfx'=>11,'itemtype'=>'progress','moduleinstncealias'=>'o.classeid','modulekey'=>'badiu.ams.offer.classe','useralias'=>'o.userid');
			 $wsql.=$this->addColumnFilter($fparam);
			
			$fparam=array('pfx'=>12,'itemtype'=>'finalgrade','moduleinstncealias'=>'o.classeid','modulekey'=>'badiu.ams.offer.classe','useralias'=>'o.userid');
			 $wsql.=$this->addColumnFilter($fparam);
			 
			 $fparam=array('pfx'=>13,'itemtype'=>'finalcompletation','moduleinstncealias'=>'o.classeid','modulekey'=>'badiu.ams.offer.classe','useralias'=>'o.userid');
			 $wsql.=$this->addColumnFilter($fparam);
			
			
			return  $wsql; 
		 } 
 function addColumn($param) {
	 $itemtype=$this->getUtildata()->getVaueOfArray($param,'itemtype');
	 $pfx=$this->getUtildata()->getVaueOfArray($param,'pfx');
	 $moduleinstncealias=$this->getUtildata()->getVaueOfArray($param,'moduleinstncealias');
	 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
	 $useralias=$this->getUtildata()->getVaueOfArray($param,'useralias');
	 $selectcolumn=$this->getUtildata()->getVaueOfArray($param,'selectcolumn');
	
	 if(empty($itemtype)){return ""; }
	 if(empty($pfx)){return ""; }
	 if(empty($moduleinstncealias)){return ""; }
	 if(empty($modulekey)){return ""; }
	 if(empty($useralias)){return ""; }
	 $column="grade";
	 if(!empty($selectcolumn)){ $column=$selectcolumn;}
	 $wsql=" ,(SELECT g$pfx.$column FROM BadiuAdminGradeBundle:AdminGrade g$pfx JOIN g$pfx.itemid i$pfx WHERE i$pfx.moduleinstance=$moduleinstncealias AND i$pfx.modulekey='".$modulekey."' AND i$pfx.itemtype='".$itemtype."' AND g$pfx.userid=$useralias ) AS $itemtype ";
	 return $wsql;
  }

 function addColumnFilter($param) {
	
	 $itemtype=$this->getUtildata()->getVaueOfArray($param,'itemtype');
	 $pfx=$this->getUtildata()->getVaueOfArray($param,'pfx');
	 $moduleinstncealias=$this->getUtildata()->getVaueOfArray($param,'moduleinstncealias');
	 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
	 $useralias=$this->getUtildata()->getVaueOfArray($param,'useralias');
	
	
	 if(empty($itemtype)){return ""; }
	 if(empty($pfx)){return ""; }
	 if(empty($moduleinstncealias)){return ""; }
	 if(empty($modulekey)){return ""; }
	 if(empty($useralias)){return ""; }
	
	$cwsql="";
	$fqueryn=$this->getContainer()->get('badiu.system.core.lib.sql.factoryqueryselectnative');
	
	if($itemtype=='progress' || $itemtype=='finalgrade'){ 
		$cwsql.=$fqueryn->number($this->getParam(),$itemtype,"g$pfx.grade");
		
	}else if($itemtype=='finalcompletation'){
		$cwsql.=$fqueryn->equal($this->getParam(),$itemtype,"g$pfx.grade",true,true);
		
	}
	$cwsql=trim($cwsql);
	if(empty($cwsql)){return "";}
	
	 $wsql=" AND (SELECT COUNT(g$pfx.id) FROM BadiuAdminGradeBundle:AdminGrade g$pfx JOIN g$pfx.itemid i$pfx WHERE i$pfx.moduleinstance=$moduleinstncealias AND i$pfx.modulekey='".$modulekey."' AND i$pfx.itemtype='".$itemtype."' AND g$pfx.userid=$useralias  $cwsql ) > 0 ";

	 return $wsql;
	}	  
	
} 
