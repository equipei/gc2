<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Admin\AttendanceBundle\Model\AttendanceDataOptions;
class ClasseAttendanceDataOptions extends AttendanceDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
    
  
	public  function getTargetBatchType(){
			$list=array();
			$list['defaultplanned']=$this->getTranslator()->trans('badiu.ams.offer.classeplannedbytimedefault.batchtype.defaultplanned');
			//$list['customuser']=$this->getTranslator()->trans('badiu.ams.offer.classeplannedbytimedefault.batchtype.customuser');
			$list['oneuser']=$this->getTranslator()->trans('badiu.ams.offer.classeplannedbytimedefault.batchtype.oneuser');
			return $list;
	 }
	 public  function getCriteria(){
        
        $list=array();
        $list['manual']=$this->getCriteriaLabel('manual');
	    $list['automatic_lmsclasroomaccessintimeplanned']=$this->getCriteriaLabel('automatic_lmsclasroomaccessintimeplanned');
	    $list['automatic_lmslmsactivitycompleted']=$this->getCriteriaLabel('automatic_lmslmsactivitycompleted');
        return  $list;
    }
	
	 public  function getCriteriaLabel($value){
        $label=null;
        if($value=='manual'){$label=$this->getTranslator()->trans('badiu.ams.offer.classeattendance.criteria.manual');}
	    else if($value=='automatic_lmsclasroomaccessintimeplanned'){$label=$this->getTranslator()->trans('badiu.ams.offer.classeattendance.criteria.automaticlmsclasroomaccessintimeplanned');}
	    else if($value=='automatic_lmslmsactivitycompleted'){$label=$this->getTranslator()->trans('badiu.ams.offer.classeattendance.criteria.automaticlmslmsactivitycompleted');}
        return  $label;
    }
	
	public  function getDaysPlanned(){
		$list=array();
		$classeid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		if(empty($classeid)){return $list;}
		
		$attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
		$dblist= $attendancedata->getGlobalColumnsValues('DISTINCT o.daykey', array('modulekey'=>'badiu.ams.offer.classeattendance','moduleinstance'=>$classeid,'deleted'=>0));
		if(!is_array($dblist)){return $list;}
		$dataoptions=$this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
		foreach ($dblist as $row) {
			$daykey=$this->getUtildata()->getVaueOfArray($row,'daykey');
			$name=$daykey;
			$p=explode("_",$daykey);
			$d=$this->getUtildata()->getVaueOfArray($p,2);
			$m=$this->getUtildata()->getVaueOfArray($p,1);
			$y=$this->getUtildata()->getVaueOfArray($p,0);
					
			$ml=$dataoptions->getMonthLabel($m);
			$now=new \DateTime();
			$currentyear=true;
			if($now->format('Y')!=$y){$currentyear=false;};
			if($currentyear){$name=$this->getTranslator()->trans('badiu.system.time.format.daymonth',array('%day%'=>$d,'%month%'=>$ml));}
			else{$name=$this->getTranslator()->trans('badiu.system.time.format.date',array('%day%'=>$d,'%month%'=>$ml,'%year%'=>$y));}
					
			$list[$daykey]=$name;
		}
		return $list;
	 }
	
}
