<?php
namespace Badiu\Ams\OfferBundle\Model\Classe;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ClasseMoodleReportController extends BadiuReportController {

	function __construct(Container $container) {
		parent::__construct($container);
	}
 
  public function exec() {
    
    //get classeid
     $parentid= $this->getUtildata()->getVaueOfArray($this->getFparam(),'parentid');
   
    if(empty($parentid)){return 'Require param parentid';}
    $data=$this->getContainer()->get('badiu.ams.offer.classe.data');
    $dconfig =$data->getGlobalColumnValue('dconfig',array('id'=>$parentid));
    $dconfig = $this->getJson()->decode($dconfig,true);
    $sserviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
    $courseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);

    if(empty($sserviceid)){return 'Require param sserviceid in classe moodle syn config';}
    if(empty($courseid)){return 'Require param courseid in classe moodle syn config';}
   
    $param=$this->getFparam();
    $param['_datasource']='servicesql';
    $param['_serviceid']=$sserviceid;
	$param['_serviceidfocerupdate']=1;
	$param['courseid']=$courseid;
	
    $this->setFparam($param);
   
    return null;
  }


}
