<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnrolImportFromDisciplineFormController extends BadiuFormController
{ 
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     
     public function save() {
          $param = $this->getParam();
        
          $classeid=$this->getParamItem('classeid');
          $odisciplineid=$this->getContainer()->get('badiu.ams.offer.classe.data')->geDisciplineidById($classeid);
          $tdata= $this->getContainer()->get('badiu.ams.enrol.lib.transfer');
          $tdata->init('discipline',$odisciplineid,'classe',$classeid,$param);
          $result=$tdata->exec();
        
         $this->setSuccessmessage($this->getTranslator()->trans('badiu.ams.enrol.import.info',array('%countrecord%'=>$result))); 
          $outrsult=array('result'=>$result,'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
          $this->getResponse()->setStatus("accept");
          $this->getResponse()->setMessage($outrsult);
         return $this->getResponse()->get();
         
     }
    
}
