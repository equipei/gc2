<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class EnrolDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }

    public  function sourcetoimport(){
        $list = array();
        $list['discipline']=$this->getTranslator()->trans('badiu.ams.offer.classeenrollimportfromdiscipline.enroldatediscipline');
        $list['classe']=$this->getTranslator()->trans('badiu.ams.offer.classeenrollimportfromdiscipline.enroldateclasse');
        return $list;
    }

    
}
