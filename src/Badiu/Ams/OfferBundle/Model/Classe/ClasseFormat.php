<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;

class ClasseFormat extends BadiuFormat {

    
    function __construct(Container $container) {
        parent::__construct($container);
        
    }
    public function summary($data) {
        $result = null;
        $summary =$this->getUtildata()->getVaueOfArray($data,'summary');
		$odsummary =$this->getUtildata()->getVaueOfArray($data,'odsummary');
		$crdsummary =$this->getUtildata()->getVaueOfArray($data,'crdsummary');
		$dsummary =$this->getUtildata()->getVaueOfArray($data,'dsummary');
      
	   if(!empty($summary)){$result=$summary;}
	   else if(!empty($odsummary)){$result=$odsummary;}
	   else if(!empty($crdsummary)){$result=$crdsummary;}
	   else if(!empty($dsummary)){$result=$dsummary;}
        
        return $result;
    }
	
	 public function teachingplan($data) {
        $result = null;
        $teachingplan =$this->getUtildata()->getVaueOfArray($data,'teachingplan');
		$odteachingplan =$this->getUtildata()->getVaueOfArray($data,'odteachingplan');
		$crdteachingplan =$this->getUtildata()->getVaueOfArray($data,'crdteachingplan');
		$dteachingplan =$this->getUtildata()->getVaueOfArray($data,'dteachingplan');
      
	   if(!empty($teachingplan)){$result=$teachingplan;}
	   else if(!empty($odteachingplan)){$result=$odteachingplan;}
	   else if(!empty($crdteachingplan)){$result=$crdteachingplan;}
	   else if(!empty($dteachingplan)){$result=$dteachingplan;}
        
        return $result;
    }
    
	 public function defaultimage($data) {
        $result = null;
        
        return $result;
    }
	
	
}
