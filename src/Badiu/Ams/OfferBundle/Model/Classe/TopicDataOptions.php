<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use  Badiu\Ams\OfferBundle\Model\DOMAINTABLE;
class TopicDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
    
    public  function getFormChoice(){
        $data=$this->getContainer()->get('badiu.ams.offer.topic.data');
        $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $param=array('classeid'=>$parentid);
        $entity=$badiuSession->get()->getEntity();
        $list=$data->getFormChoice( $entity,$param);
        
        return $list;
    }
    
}
