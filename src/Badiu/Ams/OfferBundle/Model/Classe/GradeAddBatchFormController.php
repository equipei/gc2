<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class GradeAddBatchFormController extends BadiuFormController
{ 
	private $liststudentbasekey;
    function __construct(Container $container) {
            parent::__construct($container);
			$this->liststudentbasekey='badiu.ams.offer.classegradeaddbatchliststudent';
              }
    
     
     public function save() {
		
          $param = $this->getParam();
			$check=$this->validation();
			if($check != null){return $check;}
			
			$grade=$this->getParamItem('grade');
			$itemid=$this->getParamItem('gradeitemid');
			$entity=$this->getEntity();
			$listusers=$this->getUsersid();
			$shoudexec=sizeof($listusers);
			$exec=0;
			$gradedata=$this->getContainer()->get('badiu.admin.grade.grade.data');
			
			foreach ($listusers as $row) {
				 $userid=$this->getUtildata()->getVaueOfArray($row,'id');
				 $paramcheckexist=array('entity'=>$entity,'userid'=>$userid,'itemid'=>$itemid);
				 $paramadd=array('entity'=>$entity,'userid'=>$userid,'itemid'=>$itemid,'grade'=>$grade,'timecreated'=>new \DateTime(),'timecreatedgrade'=>new \DateTime(),'deleted'=>0);
				 $presult=$gradedata->addNativeSql($paramcheckexist,$paramadd,null);
				 $r=$this->getUtildata()->getVaueOfArray($presult,'id');
				 if($r){$exec++;}
			}
			
		   $message=$this->getTranslator()->trans('badiu.ams.offer.classegradeaddbatch.result',array('%countgradeadd%'=>$exec,'%countenrol%'=>$shoudexec));
           $this->setSuccessmessage($message);
		   $outrsult=array('result'=>$exec,'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
           $this->getResponse()->setStatus("accept");
           $this->getResponse()->setMessage($outrsult);
           return $this->getResponse()->get();
         
     }
      public function getUsersid() {
		 $classeid=$this->getParamItem('classeid');
		 $this->getSearch()->getKeymanger()->setBaseKey($this->getListstudentbasekey());
		 $fparam=array('classeid'=>$classeid,'entity'=>$this->getEntity(),'deleted'=>0); 
		 $result = $this->getSearch()->searchList($fparam);
		 $list=$this->getUtildata()->getVaueOfArray($result,'data.1',true);
		 return $list;
  }
	 public function validation() {
		 $isemailvalid = $this->isClasseidValid();
       if ($isemailvalid != null) {
            return $isemailvalid;
        }
		$isemailvalid = $this->isGradeValid();
        
		if ($isemailvalid != null) {
            return $isemailvalid;
        }
	 }
	 public function isGradeValid() {
		$grade=$this->getParamItem('grade');
		$itemid=$this->getParamItem('gradeitemid');
		$grade=str_replace(",",".",$grade);
		
		if(!is_numeric($grade) || $grade < 0){
			$message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.grade.notvalid');
            $info='badiu.admin.grade.notvalid';
            return $this->getResponse()->denied($info, $message);
		}
		
		//check max grade
		$itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		$paramfilter=array('entity'=>$this->getEntity(),'id'=>$itemid);
		
		$maxgrade=$itemdata->getGlobalColumnValue('maxgrade',$paramfilter); 
		
		if($grade > $maxgrade){
			$message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.grade.gradeisupperthanmaxgrade',array('%maxgrade%'=>$maxgrade));
            $info='badiu.admin.grade.gradeisupperthanmaxgrade';
            return $this->getResponse()->denied($info, $message);
		}
		$this->addParamItem('grade',$grade);
		return null;
	 }
	  public function isClasseidValid() {
		$classeid=$this->getParamItem('classeid');
		
		if(empty($classeid)){
			$message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.ams.offer.classe.message.paramclasseidrequired');
            $info='badiu.ams.offer.classe.message.paramclasseidrequired';
            return $this->getResponse()->denied($info, $message);
		}
		
		//check max grade
		$classedata=$this->getContainer()->get('badiu.ams.offer.classe.data');
		$paramfilter=array('entity'=>$this->getEntity(),'id'=>$classeid);
		
		$countdata=$classedata->countGlobalRow($paramfilter); 
		
		if(empty($countdata)){
			$message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.ams.offer.classe.message.classenotexit',array('%classeid%'=>$classeid));
            $info='badiu.admin.grade.gradeisupperthanmaxgrade';
            return $this->getResponse()->denied($info, $message);
		}
	
		return null;
	 }
	 public function  setListstudentbasekey($liststudentbasekey) {
        $this->liststudentbasekey=$liststudentbasekey;
    }
	
	 public function getListstudentbasekey() {
        return $this->liststudentbasekey;
    }
}
