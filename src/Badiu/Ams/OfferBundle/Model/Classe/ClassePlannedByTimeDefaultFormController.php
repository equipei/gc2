<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ClassePlannedByTimeDefaultFormController extends BadiuFormController
{ 
    function __construct(Container $container) {
            parent::__construct($container);
              }
       
      
    public function changeParam() {
           $param = $this->getParam();
		   $weekday = $this->getUtildata()->getVaueOfArray($param,'weekday');
		   $weekday= $this->getUtildata()->castStringToArray($weekday,",");
		   $param['weekday']=$weekday;
		   $this->setParam($param);
       }
     
     public function save() {
		
		 $this->addDefaultData();
		$this->castData();
		$check=$this->validation();
		if($check != null){return $check;}
       
		$this->changeParam();
    
		$this->removeColumns();
		$this->execBefore();
     
		//process exec
		$param = $this->getParam();
		
		$plannedautomaticfill=$this->getContainer()->get('badiu.admin.attendance.planned.lib.automaticfill');
		
	    $plannedautomaticfill->setParam($param);
	    $result= $plannedautomaticfill->exec();
		$countprocess=$this->getUtildata()->getVaueOfArray($result,'message.countexec',true);
		$param['automaticfillexec']=$this->getUtildata()->getVaueOfArray($result,'message');
		$this->setParam($param);
		
		$presult=$this->execAfter();
		
		$moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance'); 
		$urlgoback=$this->getUtilapp()->getUrlByRoute('badiu.ams.offer.classeplanned.index',array('parentid'=>$moduleinstance));
		
        $this->setSuccessmessage($this->getTranslator()->trans('badiu.ams.offer.classeplannedbytimedefault.resultprocess',array('%countprocess%'=>$countprocess)));
        $outrsult=array('result'=>$result,'message'=>$this->getSuccessmessage(),'urlgoback'=>$urlgoback);
        $this->getResponse()->setStatus("accept");
        $this->getResponse()->setMessage($outrsult);
        return $this->getResponse()->get();
	  
     }
    
}
