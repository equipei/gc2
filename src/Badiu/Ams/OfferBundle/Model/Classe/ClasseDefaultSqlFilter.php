<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class ClasseDefaultSqlFilter extends BadiuSqlFilter{
   
    function __construct(Container $container) {
            parent::__construct($container);
           
       }
		 private $modulekey;
		 private $shortnameenableclasse;
		 
        function exec() {
           
           return null;
        }
        
     function initShortnameenableclasse() {
           if($this->modulekey=='badiu.ams.offer.classe'){
			   $this->shortnameenableclasse='ongoing';
		   }else if($this->modulekey=='badiu.tms.offer.classe'){
			    $this->shortnameenableclasse='classeongoing';
		   }else {
			   //review default should be 'ongoing'
			  $this->shortnameenableclasse= 'classeongoing';
		   }
           
        }  
	function addfilteractive() {
           $enableclasse= $this->getUtildata()->getVaueOfArray($this->getParam(),'enableclasse');
		   $sql="";
		   if($enableclasse==1){$sql=$this->activefilter();}
           if($enableclasse=="0"){$sql=$this->inactivefilter();}
		   
			return  $sql;
	}			
        function activefilter($param=array()) {
			if(empty($this->shortnameenableclasse)){$this->initShortnameenableclasse();}
			$classealias=$this->getUtildata()->getVaueOfArray($param,'_classealias');
			$classestatusalias=$this->getUtildata()->getVaueOfArray($param,'_classestatusalias');
			$shortnameenableclasse=$this->getUtildata()->getVaueOfArray($param,'_classeshortnameenable');
			if(empty($classealias)){$classealias="o";}
			if(empty($classestatusalias)){$classestatusalias="s";}
			if(!empty($shortnameenableclasse)){$this->shortnameenableclasse=$shortnameenableclasse;}
			
            $sql=" AND $classestatusalias.shortname='".$this->shortnameenableclasse."' AND ( $classealias.timestart IS NULL OR $classealias.timestart <= :timenow1 )  AND ( $classealias.timeend IS NULL OR $classealias.timeend >= :timenow2 )  ";
            return $sql;
         }
		function inactivefilter($param=array()) {
			if(empty($this->shortnameenableclasse)){$this->initShortnameenableclasse();}
			$classealias=$this->getUtildata()->getVaueOfArray($param,'_classealias');
			$classestatusalias=$this->getUtildata()->getVaueOfArray($param,'_classestatusalias');
			$shortnameenableclasse=$this->getUtildata()->getVaueOfArray($param,'_classeshortnameenable');
			if(empty($classealias)){$classealias="o";}
			if(empty($classestatusalias)){$classestatusalias="s";}
			if(!empty($shortnameenableclasse)){$this->shortnameenableclasse=$shortnameenableclasse;}
			
            $sql=" AND  (( $classestatusalias.shortname!='".$this->shortnameenableclasse."' ) OR ($classestatusalias.shortname='".$this->shortnameenableclasse."' AND $classealias.timestart > :timenow1 ) OR  ($classestatusalias.shortname='".$this->shortnameenableclasse."' AND $classealias.timeend < :timenow2 ) ) ";
		    return $sql;
         }
		 
		 
		 // Getter for $modulekey
    public function getModuleKey() {
        return $this->modulekey;
    }

    // Setter for $modulekey
    public function setModuleKey($modulekey) {
        $this->modulekey = $modulekey;
    }
	
	
    // Getter for $shortnameenableclasse
    public function getShortNameEnableClasse() {
        return $this->shortnameenableclasse;
    }

    // Setter for $shortnameenableclasse
    public function setShortNameEnableClasse($shortnameenableclasse) {
        $this->shortnameenableclasse = $shortnameenableclasse;
    }
} 
