<?php

namespace Badiu\Ams\OfferBundle\Model\Classe;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CleanLmsDataFormController extends BadiuFormController
{ 
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     
     public function save() {
          $param = $this->getParam();
        
		
		 $courseprogressactivityconsolidate=$this->getParamItem('courseprogressactivityconsolidate');
		 $coursefinalgrade=$this->getParamItem('coursefinalgrade');
		 $coursecompletation=$this->getParamItem('coursecompletation');
		 $coursedataaccess=$this->getParamItem('coursedataaccess');
		 $changestatusenrolto=$this->getParamItem('changestatusenrolto');
		 $entity=$this->getEntity();
		
		
	
		
		$msg="";
		$cont=0;
		if($courseprogressactivityconsolidate){$msg.=$this->updateGrade('progress');$cont++;}
		if($coursefinalgrade){$msg.=$this->updateGrade('finalgrade');$cont++;}
		if($coursecompletation){$msg.=$this->updateGrade('finalcompletation');$cont++;}
		if($coursedataaccess){$msg.=$this->updateGrade('lmscourseaccess');$cont++;}
		if($changestatusenrolto){$msg.=$this->updateEnrolStatus();$cont++;}
		
		if($cont==0){
			$outrsult=array('result'=>null,'generalerror'=>$this->getTranslator()->trans('badiu.ams.offer.classecleanlmsdata.selectatleastone'),'urlgoback'=>null);
		    $this->getResponse()->setStatus('danied');
			$this->getResponse()->setInfo('badiu.ams.offer.classecleanlmsdata.selectatleastone');
            $this->getResponse()->setMessage($outrsult);
           return $this->getResponse()->get();
		}
		
		
		  $this->setSuccessmessage($msg); 
	
          $outrsult=array('result'=>null,'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
          $this->getResponse()->setStatus("accept");
          $this->getResponse()->setMessage($outrsult);
         return $this->getResponse()->get();
         
     }

	 public function updateGrade($itemtype) {
		 if(empty($itemtype)){return null;}
		 $classeid=$this->getParamItem('classeid');
		 $entity=$this->getEntity();
		
		 if(empty($classeid)){return null;}
		  
		  $itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		  $paramfilter=array('entity'=>$entity,'modulekey'=>'badiu.ams.offer.classe','moduleinstance'=>$classeid,'itemtype'=>$itemtype);
		  $itemid=$itemdata->getGlobalColumnValue('id',$paramfilter); 
		 
		  if(empty($itemid)){return null;}
		  $fparam=array('itemid'=>$itemid);
		  
		  $gradelibmanager= $this->getContainer()->get('badiu.admin.grade.lib.manager');
		  $result=$gradelibmanager->deleteGradeByItemid($fparam);
		  if(empty($result)){$result=0;}
		  $msg=null;
		 if ($itemtype=='finalcompletation'){$msg= $this->getTranslator()->trans('badiu.ams.offer.classecleanlmsdata.coursecompletation.result',array('%result%'=>$result));}
		 else if ($itemtype=='finalgrade'){$msg= $this->getTranslator()->trans('badiu.ams.offer.classecleanlmsdata.coursefinalgrade.result',array('%result%'=>$result));}
		 else if ($itemtype=='progress'){$msg= $this->getTranslator()->trans('badiu.ams.offer.classecleanlmsdata.courseprogressactivityconsolidate.result',array('%result%'=>$result));}
		 else if ($itemtype=='lmscourseaccess'){$msg= $this->getTranslator()->trans('badiu.ams.offer.classecleanlmsdata.coursedataaccess.result',array('%result%'=>$result));}
		 else if ($itemtype=='finalgrade'){$msg= $this->getTranslator()->trans('badiu.ams.offer.classecleanlmsdata.coursefinalgrade.result',array('%result%'=>$result));}
		 if(!empty($msg)){$msg.="<br />";}
		 return $msg;
		 }
	 
	  public function updateEnrolStatus() {
		   
			$classeid=$this->getParamItem('classeid');
			$entity=$this->getEntity();
		   if(empty($classeid)){return null;}
		   
		   $changestatusenrolto=$this->getParamItem('changestatusenrolto');
		   $defaultrole=$this->getParamItem('defaultrole');
		   $approvalstatus=$this->getParamItem('approvalstatus');
		   $disapprovedstatus=$this->getParamItem('disapprovedstatus');
		 
		   if(empty($changestatusenrolto)){return null;}
		   if(empty($defaultrole)){return null;}
		   if(empty($approvalstatus)){return null;}
		   if(empty($disapprovedstatus)){return null;}
		   
		   
		
		   $changestatusenroltoid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,$changestatusenrolto);
		   $approvalstatusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,$approvalstatus);
		   $disapprovedstatusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,$disapprovedstatus);
		   $defaultroleid=$this->getContainer()->get('badiu.ams.role.role.data')->getIdByShortname($entity,$defaultrole);
		   
		   
		   if(empty($changestatusenroltoid)){return null;}
		   if(empty($defaultroleid)){return null;}
		   	 $cont=0;
		 if($approvalstatusid){
			 $paramfilter=array('entity'=>$entity,'classeid'=>$classeid,'statusid'=>$approvalstatusid);
		     $enroldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		     $list=$enroldata->getGlobalColumnValues('id',$paramfilter,array('limit'=>20000));
			
		    if(is_array($list)){
			    foreach ($list as $row) {
					$id=$this->getUtildata()->getVaueOfArray($row,'id');
					$uparam=array('id'=>$id,'statusid'=>$changestatusenroltoid);
					$r= $enroldata->updateNativeSql($uparam,false);
					if($r){$cont++;}
				}
		   }
		 }
		   
		 if($disapprovedstatusid){
			 $paramfilter=array('entity'=>$entity,'classeid'=>$classeid,'statusid'=>$disapprovedstatusid);
		     $enroldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		     $list=$enroldata->getGlobalColumnValues('id',$paramfilter,array('limit'=>20000));
			
		    if(is_array($list)){
			    foreach ($list as $row) {
					$id=$this->getUtildata()->getVaueOfArray($row,'id');
					$uparam=array('id'=>$id,'statusid'=>$changestatusenroltoid);
				
					$r= $enroldata->updateNativeSql($uparam,false);
					if($r){$cont++;}
				}
		   }
		 }
		 $msg= $this->getTranslator()->trans('badiu.ams.offer.classecleanlmsdata.changestatusenrolto.result',array('%result%'=>$cont));
		  return $msg;
	  }
	
}
