<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class OfferFinancIntegrationFilter extends BadiuFormFilter{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
   public function execBeforeSubmit() {

          $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
         
          $param=$this->getParam();
 
          $financintegration=array();
          $financintegration['status']='pending';
          
          $financintegration['productintegration']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'productintegration');
          unset($param['productintegration']);
          
          $financintegration['costcenterintegration']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'costcenterintegration');
          unset($param['costcenterintegration']);
          
          $financintegration['projectintegration']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'projectintegration');
          unset($param['projectintegration']);
          
          $financintegration['contracttemplateintegration']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'contracttemplateintegration');
          unset($param['contracttemplateintegration']);
         
           
          if(empty($dconfig)){$dconfig=array();}
          else if (!empty($dconfig) && !is_array($dconfig)){$dconfig = $this->getJson()->decode($dconfig,true);}

          
          $dconfig['financintegration']=$financintegration;
          $dconfig = $this->getJson()->encode($dconfig);
          $param['dconfig']=$dconfig;
          
          $this->setParam($param);
          
        }
        
   public function execAfterSubmit() {
         $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
         $financintegration=$this->getContainer()->get('badiu.ams.core.lib.financtintegration');
         $financintegration->setId($id);
         $financintegration->setDatakey('badiu.ams.offer.offer.data');
         $financintegration->setField('dconfig');
         $financintegration->init();
         $financintegration->offer();
         
         $param=$this->getParam();   
         $param['dconfig']=$financintegration->getParam();
         $this->setParam($param);
         
   } 
       
    
}
