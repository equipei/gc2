<?php

namespace Badiu\Ams\OfferBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class DisciplineData extends BadiuDataBase {

    function __construct(Container $container, $bundleEntity) {
        parent::__construct($container, $bundleEntity);
    }
	
		public function getNameParent($id) {
		if(empty($id)){return null;} 
		 $sql="SELECT o.id,o.disciplinename AS name,f.id AS offerid,f.name AS offername,c.id AS courseid, c.name AS coursename FROM ".$this->getBundleEntity()." o JOIN o.offerid f LEFT JOIN f.courseid c WHERE o.id=:id";
         $query = $this->getEm()->createQuery($sql);
         $query->setParameter('id',$id);
         $result= $query->getOneOrNullResult();
		 return  $result;
    }
/*
    public function getMenuOffer($id) {
        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit = $sysoperation->isEdit();

        if ($isEdit) {
            $sql = "SELECT f.id, f.name FROM " . $this->getBundleEntity() . " o JOIN o.offerid f WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id', $id);
            $result = $query->getSingleResult();
            return $result;
        } else {
            $curriculumdata = $this->getContainer()->get('badiu.ams.offer.offer.data');
            $value = $curriculumdata->getNameById($id);
            return $value;
        }
    }
*/
    //review 
    //get course name by curriculumid
    public function getCourseNameById($id) {

        $sql = "SELECT  c.name FROM " . $this->getBundleEntity() . " o JOIN o.offerid f JOIN f.courseid c WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
         if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
    }

//review
//get offer name by curriculumid
    public function getOfferById($id) {

        $sql = "SELECT  f.name FROM " . $this->getBundleEntity() . " o JOIN o.offerid f  WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
         $result = $query->getOneOrNullResult();
         if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
    }
    public function getOfferidById($id) { 

        $sql = "SELECT  f.id FROM " . $this->getBundleEntity() . " o JOIN o.offerid f  WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('id',$result)){$result=$result['id'];}
        return $result;
    }
    //review
    public function getNameById($id) {

        $sql = "SELECT  o.disciplinename AS name FROM " . $this->getBundleEntity() . " o  WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
         if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
    }

     public function getOriginalNameById($id) {
        $data=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
        $sql = "SELECT  o.name FROM " . $data->getBundleEntity() . " o   WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
         if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
    }
    //get all disciplines by offerid
    public function getByOffer($offerid) {

        $sql = "SELECT  o FROM " . $this->getBundleEntity() . " o  WHERE o.offerid=:offerid";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('offerid', $offerid);
        $result = $query->getResult();
        return $result;
    }

    //get discipline without current discipline in offer
    public function getNewsDisciplines() {
        
        $offerid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
        $name=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
        if(empty($offerid)) return null;

        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $disciplineData=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
      
        $wsql="";
        if(!empty($name)){$wsql=" AND CONCAT(o.id,LOWER(o.name))  LIKE :name ";}
        //$sql="SELECT  o.id,o.name  FROM ".$disciplineData->getBundleEntity()." o  WHERE  o.entity = :entity $wsql AND o.id NOT IN (SELECT  d.id FROM " . $this->getBundleEntity() . " od JOIN od.disciplineid d   WHERE od.offerid=:offerid) ";
		$sql="SELECT  o.id,o.name  FROM ".$disciplineData->getBundleEntity()." o  WHERE  o.entity = :entity $wsql  ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        if(!empty($name)){$query->setParameter('name','%'.strtolower($name).'%');}
     //   $query->setParameter('offerid',$offerid);
        $result= $query->getResult();
        return  $result;
        
    }
	
	/**
	* The classe search discipline in autocomplete field to create new classe
	*/
	 public function getDisciplineForAutocomplete() {
        
        $name=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
        if(empty($name)) return null;

        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
        $wsql="";
        if(!empty($name)){$wsql=" AND CONCAT(o.id,LOWER(o.disciplinename),LOWER(d.name))  LIKE :name ";}
        $sql="SELECT  o.id,CONCAT(c.name,'/',f.name,'/',o.disciplinename) AS name  FROM ".$this->getBundleEntity()." o JOIN o.offerid f JOIN f.courseid  c JOIN o.disciplineid d WHERE  o.entity = :entity AND c.dtype=:dtype $wsql  ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('dtype','course');
        if(!empty($name)){$query->setParameter('name','%'.strtolower($name).'%');}
        $result= $query->getResult();
        return  $result;
        
    }
	
	 public function getDisciplineNameByIdOnEditAutocomplete($id) {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
        $sql="SELECT  CONCAT(c.name,'/',f.name,'/',o.disciplinename) AS name  FROM ".$this->getBundleEntity()." o JOIN o.offerid f JOIN f.courseid  c JOIN o.disciplineid d WHERE  o.entity = :entity AND o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }
	
//get discipline without current discipline in offer 
    //review delete
    /*
    public function getFormChoiceNewsDisciplines() {
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit = $sysoperation->isEdit();
        $offerid = null;

        if ($isEdit) {
            $id = $this->getContainer()->get('request')->get('id');
            $offerid = $this->getOfferidById($id);
        } else {
            $offerid = $this->getContainer()->get('request')->get('parentid');
        }

        if (empty($offerid))
            return null;


        $disciplineData = $this->getContainer()->get('badiu.ams.discipline.discipline.data');
        $sql = null;
        if ($isEdit) {
            $sql = "SELECT  o.id,o.name FROM " . $disciplineData->getBundleEntity() . " o  WHERE  o.entity = :entity ";
        } else {
             $sql = "SELECT  o.id,o.name FROM " . $disciplineData->getBundleEntity() . " o  WHERE  o.entity = :entity AND o.id NOT IN (SELECT  d.id FROM " . $this->getBundleEntity() . " od JOIN od.disciplineid d   WHERE od.offerid=:offerid) ";
        }


        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity', $badiuSession->get()->getEntity());
        if (!$isEdit) {$query->setParameter('offerid', $offerid);}
        $result = $query->getResult();
        return $result; 
    }
    public function getOfferidById($id) {

        $sql = "SELECT  f.id FROM " . $this->getBundleEntity() . " o JOIN o.offerid f  WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getSingleResult();
        $result = $result['id'];
        return $result;
    }*/

    public function existDiscipline($entity, $offerid, $disciplineid) {
        $r = FALSE;
        $sql = "SELECT  COUNT(o.id) AS countrecord  FROM " . $this->getBundleEntity() . " o WHERE  o.entity=:entity AND o.offerid=:offerid AND o.disciplineid =:disciplineid";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
        $query->setParameter('offerid', $offerid);
        $query->setParameter('disciplineid', $disciplineid);
        $result = $query->getOneOrNullResult();
        if ($result['countrecord'] > 0) {
            $r = TRUE;
        }
        return $r;
    }
    public function getLastDisciplineid($entity, $offerid, $disciplineid) {
        $r = FALSE;
        $sql = "SELECT  MAX(o.id) AS id  FROM " . $this->getBundleEntity() . " o WHERE  o.entity=:entity AND o.offerid=:offerid AND o.disciplineid =:disciplineid";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
        $query->setParameter('offerid', $offerid);
        $query->setParameter('disciplineid', $disciplineid);
        $result = $query->getOneOrNullResult();
        if(isset($result['id'])){$result=$result['id'];}
        return $result;
    }
    public function findDiscipline($entity, $offerid, $disciplineid) {
        $r = FALSE;
        $sql = "SELECT  o FROM " . $this->getBundleEntity() . " o WHERE  o.entity=:entity AND  o.offerid=:offerid AND o.disciplineid =:disciplineid";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
        $query->setParameter('offerid', $offerid);
        $query->setParameter('disciplineid', $disciplineid);
        $result = $query->getOneOrNullResult();

        return $result;
    }

     public function getLmsConfigById($id) {
        $r = FALSE;
        $sql = "SELECT  sv.id AS sserviceid,sv.servicetoken AS token,sv.dtype,sv.servicerurl,o.lmscontexttype,o.lmscontextid  FROM " . $this->getBundleEntity() . " o JOIN o.sserviceid sv  WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
        return  $result;
    }
    public function isSyncWithLms($entity, $sserviceid, $dtypelms, $idnumberlms) {
        $r = FALSE;
        $sql = "SELECT  COUNT(o.id) AS countrecord  FROM " . $this->getBundleEntity() . " o WHERE o.entity=:entity AND o.sserviceid=:sserviceid AND o.dtypelms =:dtypelms AND o.idnumberlms=:idnumberlms ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
        $query->setParameter('sserviceid', $sserviceid);
        $query->setParameter('dtypelms', $dtypelms);
        $query->setParameter('idnumberlms', $idnumberlms);
        $result = $query->getOneOrNullResult();
        if ($result['countrecord'] > 0) {
            $r = TRUE;
        }
        return $r;
    }

    public function isSyncWithLmsByOffer($entity, $offerid, $sserviceid, $dtypelms, $idnumberlms) {
        $r = FALSE;
        $sql = "SELECT  COUNT(o.id) AS countrecord  FROM " . $this->getBundleEntity() . " o WHERE o.entity=:entity AND o.offerid=:offerid AND o.sserviceid=:sserviceid AND o.dtypelms =:dtypelms AND o.idnumberlms=:idnumberlms ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
        $query->setParameter('offerid', $offerid);
        $query->setParameter('sserviceid', $sserviceid);
        $query->setParameter('dtypelms', $dtypelms);
        $query->setParameter('idnumberlms', $idnumberlms);
        $result = $query->getOneOrNullResult();
        if ($result['countrecord'] > 0) {
            $r = TRUE;
        }
        return $r;
    }

    public function getIdByLmsByOffer($entity, $offerid, $sserviceid, $dtypelms, $idnumberlms) {
        $r = FALSE;
        $sql = "SELECT  o.id  FROM " . $this->getBundleEntity() . " o WHERE o.entity=:entity AND o.offerid=:offerid AND o.sserviceid=:sserviceid AND o.dtypelms =:dtypelms AND o.idnumberlms=:idnumberlms ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
        $query->setParameter('offerid', $offerid);
        $query->setParameter('sserviceid', $sserviceid);
        $query->setParameter('dtypelms', $dtypelms);
        $query->setParameter('idnumberlms', $idnumberlms);
        $result = $query->getOneOrNullResult();
        $result = $result['id'];
        return $result;
    }

    
     public function getMenuCourse($id) {
        
            $offerdiscipline=$this->getContainer()->get('badiu.ams.offer.discipline.data');
            $sql="SELECT c.id, c.name FROM ".$offerdiscipline->getBundleEntity()." o  JOIN o.offerid f JOIN f.courseid c WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
            return  $result;
        
    }


    public function getMenuOffer($id) {
       
              $offerdiscipline=$this->getContainer()->get('badiu.ams.offer.discipline.data');
            $sql="SELECT f.id, f.name FROM ".$offerdiscipline->getBundleEntity()." o JOIN  o.offerid f WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id); 
            $result= $query->getOneOrNullResult();
            return  $result; 
        
    }
   

    public function getMenu($id) {
       
            $offerdiscipline=$this->getContainer()->get('badiu.ams.offer.discipline.data');
            $sql="SELECT o.id,o.disciplinename AS name FROM ".$offerdiscipline->getBundleEntity()." o  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql); 
            $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
            return  $result;
        
    }  
    
       public function getDayToStart($id) {
            $sql="SELECT o.timestart  FROM ".$this->getBundleEntity()." o  WHERE o.id=:id ";
            $query = $this->getEm()->createQuery($sql); 
            $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
             $timestart=$result['timestart'];
             if(empty($timestart)) {return null;}
            
             $now=time();
             $start=$timestart->getTimestamp();
             $day=($start-$now)/86400;
             
            return  $day;
        
    } 

    public function hasVacancy($id) {
            $sql="SELECT o.maxenrol, COUNT(ed.id) AS countenrol  FROM ".$this->getBundleEntity()." o  LEFT JOIN  BadiuAmsEnrolBundle:AmsEnrolDiscipline ed WITH o.id=ed.odisciplineid WHERE o.id=:id  GROUP BY o.maxenrol ";
            $query = $this->getEm()->createQuery($sql); 
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
             $maxenrol=$result['maxenrol'];
             $countenrol=$result['countenrol'];
             if($maxenrol<$countenrol){return true;}
            return  false;
        
    } 
    public function getStatusShortname($id) {
            $sql="SELECT s.shortname AS statusshortname FROM ".$this->getBundleEntity()." o  LEFT JOIN  o.statusid s  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql); 
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
            $result=$result['statusshortname'];
          return $result;
        
    }
    
    public function geSectionidById($id) {
        $r = FALSE;
        $sql = "SELECT  s.id FROM " . $this->getBundleEntity() . " o JOIN o.sectionid s  WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
        if(isset($result['id'])){$result=$result['id'];}
        return  $result;
    }
       public function geOfferidById($id) {
        
        $sql = "SELECT  f.id FROM " . $this->getBundleEntity() . " o JOIN o.offerid f  WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
        if(isset($result['id'])){$result=$result['id'];}
        return  $result;
    }
   public function getPeriod($id) {
            $sql="SELECT o.timestart,o.timeend FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
	    return  $result;
        }
		
	   public function getDconfig($id) {
            $sql="SELECT o.dconfig AS disclipinedconfig,f.dconfig AS offerdconfig FROM ".$this->getBundleEntity()." o JOIN o.offerid f  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
			return  $result;
        }
}
