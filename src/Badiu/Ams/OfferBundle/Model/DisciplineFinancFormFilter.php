<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;

class DisciplineFinancFormFilter extends BadiuFormFilter{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
     

	

     public function execAfterSubmit() {
         
         $disciplineid=$this->getParamItem('id');
         if(empty($disciplineid)){return null;}
        
        $dto=$this->getContainer()->get('badiu.ams.offer.discipline.data')->findById($disciplineid);
             if(!$dto->getProductintegration()){return $dto;}
         
             $course=$dto->getOfferid()->getCurriculumid()->getCourseid()->getName();
             $offer=$dto->getOfferid()->getName();
             $discipline=$dto->getDisciplinename();
             $name=$course.'/'.$offer.'/'.$discipline;
             
            
             
             $pdata=$this->getContainer()->get('badiu.financ.product.product.data');
             $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
            if($sysoperation->isEdit() && !empty($dto->getProductid()) && $pdata->exist($dto->getProductid())){
                 $param=array('name'=>$name,'id'=>$dto->getProductid(),'shortname'=>$dto->getShortname());
                 if(!empty($dto->getOfferid()->getEnterpriseid())){$param['enterpriseid']=$dto->getOfferid()->getEnterpriseid()->getId();}
                  $pdata->updateNativeSql($param);
            }else{
                $pdto=$this->getContainer()->get('badiu.financ.product.product.entity');
                $pdto=$this->initDefaultEntityData($pdto);
                $pdto->setName($name);
                $pdto->setTservice(true);
                $pdto->setShortname($dto->getShortname());
                 $pdto->setModulekey('badiu.ams.offer.discipline');
                $pdto->setModuleinstance($dto->getId());
                if(!empty($dto->getOfferid()->getEnterpriseid())){$pdto->setEnterpriseid($dto->getOfferid()->getEnterpriseid());}
                $pdata->setDto($pdto);
                $pdata->save();
                $dto->setProductid($pdata->getDto()->getId());
                
                $odata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
                $param=array('productid'=>$pdata->getDto()->getId(),'id'=>$dto->getId());
                $odata->updateNativeSql($param);
            }
           
           return $dto;
        
     }
     
     
     public function addProductOfOffer($dto) {
             $course=$dto->getCurriculumid()->getCourseid()->getName();
             $offer=$dto->getName();
             $name=$course.'/'.$offer;
             
             $pdata=$this->getContainer()->get('badiu.financ.product.product.data');
             $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
            if($sysoperation->isEdit() && !empty($dto->getProductid()) && $pdata->exist($dto->getProductid())){
                 $param=array('name'=>$name,'id'=>$dto->getProductid());
                 if(!empty($dto->getEnterpriseid())){$param['enterpriseid']=$dto->getEnterpriseid()->getId();}
                  $pdata->updateNativeSql($param);
            }else{
                $pdto=$this->getContainer()->get('badiu.financ.product.product.entity');
                $pdto=$this->initDefaultEntityData($pdto);
                $pdto->setName($name);
                $pdto->setTservice(true);
                $pdto->setShortname($dto->getShortname());
                $pdto->setModulekey('badiu.ams.offer.offer');
                $pdto->setModuleinstance($dto->getId());
                 if(!empty($dto->getEnterpriseid())){$pdto->setEnterpriseid($dto->getEnterpriseid());}
                $pdata->setDto($pdto);
                $pdata->save();
                $dto->setProductid($pdata->getDto()->getId());
               
                $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
                $param=array('productid'=>$pdata->getDto()->getId(),'id'=>$dto->getId());
                $odata->updateNativeSql($param);
            }
           
           return $dto;
     }

      public function addProjectOfOffer($dto) {
             $course=$dto->getCurriculumid()->getCourseid()->getName();
             $offer=$dto->getName();
             $name=$course.'/'.$offer;
             
             $pdata=$this->getContainer()->get('badiu.admin.project.project.data');
             $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
            if($sysoperation->isEdit() && !empty($dto->getProjectid()) && $pdata->exist($dto->getProjectid())){
                 $param=array('name'=>$name,'id'=>$dto->getProjectid());
                $pdata->updateNativeSql($param);
            }else{
                $pdto=$this->getContainer()->get('badiu.admin.project.project.entity');
                $pdto=$this->initDefaultEntityData($pdto);
                $pdto->setName($name);
                $pdto->setShortname($dto->getShortname());
                $pdto->setModulekey('badiu.ams.offer.offer');
                $pdto->setModuleinstance($dto->getId());
                $pdata->setDto($pdto);
                $pdata->save();
                $dto->setProjectid($pdata->getDto()->getId());
               
                $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
                $param=array('projectid'=>$pdata->getDto()->getId(),'id'=>$dto->getId());
                $odata->updateNativeSql($param);
            }
           
           return $dto;
     }

         public function addCostcenterOfOffer($dto) {
             $course=$dto->getCurriculumid()->getCourseid()->getName();
             $offer=$dto->getName();
             $name=$course.'/'.$offer;
             
             $pdata=$this->getContainer()->get('badiu.financ.maccount.costcenter.data');
             $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
            if($sysoperation->isEdit() && !empty($dto->getCostcenterid()) && $pdata->exist($dto->getCostcenterid())){
                 $param=array('name'=>$name,'id'=>$dto->getCostcenterid());
                 $pdata->updateNativeSql($param);
            }else{
                $pdto=$this->getContainer()->get('badiu.financ.maccount.costcenter.entity');
                $pdto=$this->initDefaultEntityData($pdto);
                $pdto->setName($name);
                $pdto->setShortname($dto->getShortname());
                $pdto->setModulekey('badiu.ams.offer.offer');
                $pdto->setModuleinstance($dto->getId());
                 
                //tree
                $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
                 $factorykeytree->init('badiu.financ.maccount.costcenter.data');

                $idpath = null;
                $dtype = null;
                $entity = $dto->getEntity();

                $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
                $level = $factorykeytree->getLevel($newidtree);
                $order = $factorykeytree->getOrder($newidtree);
                $pdto->setIdpath($newidtree);
                $pdto->setLevel($level);
                $pdto->setOrderidpath($order);
                
                $pdata->setDto($pdto);
                $pdata->save();
                $dto->setCostcenterid($pdata->getDto()->getId());
               
                $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
                
                 
            
                
                $param=array('costcenterid'=>$pdata->getDto()->getId(),'id'=>$dto->getId());
                $odata->updateNativeSql($param);
            }
           
           return $dto;
     }

      public function addContractTemplateOfOffer($dto) {
             $course=$dto->getCurriculumid()->getCourseid()->getName();
             $offer=$dto->getName();
             $name=$course.'/'.$offer;
             
             $pdata=$this->getContainer()->get('badiu.admin.contract.template.data');
             $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
            if($sysoperation->isEdit() && !empty($dto->getContracttemplateid()) && $pdata->exist($dto->getContracttemplateid())){
                 
                 $param=$this->addContratTemplateConfig($dto,null,true);
                 
                if(!empty($dto->getEnterpriseid())){$param['contractedenterprise']=$dto->getEnterpriseid()->getId();}
                $param['contractedtype']='enterprise';
                $param['name']=$name;
                $param['id']=$dto->getContracttemplateid();
           
                 $pdata->updateNativeSql($param);
            }else{
                $pdto=$this->getContainer()->get('badiu.admin.contract.template.entity');
                $pdto=$this->initDefaultEntityData($pdto);
                $pdto->setName($name);
                $pdto->setShortname($dto->getShortname());
                $pdto->setModulekey('badiu.ams.offer.offer');
                $pdto->setModuleinstance($dto->getId());
                $pdto=$this->addContratTemplateConfig($dto,$pdto,false);
                $pdto->setContractedtype('enterprise');
                 if(!empty($dto->getEnterpriseid())){$pdto->setContractedenterprise($dto->getEnterpriseid());}
                $pdata->setDto($pdto);
                $pdata->save();
                $dto->setContracttemplateid($pdata->getDto()->getId());
               
                $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
                $param=array('contracttemplateid'=>$pdata->getDto()->getId(),'id'=>$dto->getId());
                $odata->updateNativeSql($param);
            }
           
           return $dto;
     }
     
      public function addContratTemplateConfig($dto,$pdto,$isedit=false) {
          $dconfig=$dto->getContracttemplateconfig();
           if(empty($dconfig)){return $pdto;}
            $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
            $dconfig=$json->decode($dconfig, true);
            $finalvalue=0;
            if(isset($dconfig['totalvalue']) && !empty($dconfig['totalvalue'])){ $finalvalue=$dconfig['totalvalue'];}
            if(isset($dconfig['discountvalue']) && !empty($dconfig['totalvalue'])){ $finalvalue=$finalvalue-$dconfig['discountvalue'];}
            $dconfig['finalvalue']=$finalvalue;
            if($isedit){ 
                return $dconfig;
            }
            
           
            if(!sset($dconfig['totalvalue'])){$pdto->setTotalvalue($dconfig['totalvalue']);}
            if(!sset($dconfig['discountvalue'])){$pdto->setDiscountvalue($dconfig['discountvalue']);}
            $pdto->setFinalvalue($finalvalue);
            
            if(!sset($dconfig['billingrecurrent'])){$pdto->setBillingrecurrent($dconfig['billingrecurrent']);}
            if(!sset($dconfig['billingportion'])){$pdto->setBillingportion($dconfig['billingportion']);}
            if(!sset($dconfig['paygateway'])){$pdto->setPaygateway($dconfig['paygateway']);}
            if(!sset($dconfig['paymethod'])){$pdto->setPpaymethod($dconfig['paymethod']);}
            if(!sset($dconfig['billingday'])){$pdto->setBillingday($dconfig['billingday']);}
           return $pdto;
      }
}
