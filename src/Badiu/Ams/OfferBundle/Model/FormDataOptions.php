<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class FormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
     public function get($key){ 
        
         if($key=='ctype') return $this->getCalssType();
         return array();
     }
    public  function getCalssType(){
        $list = array();
        $list['offer']=$this->getTranslator()->trans('badiu.ams.offer.class.type.offer');
        $list['discipline']=$this->getTranslator()->trans('badiu.ams.offer.class.type.discipline');
        return $list;
    }

    
}
