<?php

namespace Badiu\Ams\OfferBundle\Model\Discipline;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class EnrolDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }

    public  function sourcetoimport(){
        $list = array();
        $list['offer']=$this->getTranslator()->trans('badiu.ams.offer.disciplineenrolimportfromoffer.enroldateoffer');
        $list['discipline']=$this->getTranslator()->trans('badiu.ams.offer.disciplineenrolimportfromoffer.enroldatediscipline');
        return $list;
    }

    
}
