<?php

namespace Badiu\Ams\OfferBundle\Model\Discipline;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ClasseData extends BadiuDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
   		public function getNameParent($id) {
		if(empty($id)){return null;} 
         $sql="SELECT o.id,o.name,od.id AS odisciplineid,od.disciplinename,f.id AS offerid,f.name AS offername,c.id AS courseid, c.name AS coursename FROM ".$this->getBundleEntity()." o JOIN o.odisciplineid od JOIN od.offerid f LEFT JOIN f.courseid c WHERE o.id=:id";
         $query = $this->getEm()->createQuery($sql);
         $query->setParameter('id',$id);
         $result= $query->getOneOrNullResult();
         return  $result;
    }
    public function existTemplate($entity,$odisciplineid,$ctemplate=true) {
	    $r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.odisciplineid=:odisciplineid AND o.ctemplate=:ctemplate";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('odisciplineid',$odisciplineid);
			$query->setParameter('ctemplate',$ctemplate);
			$result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
   }
   
     public function getMenuCourse($id) {
        
            $data=$this->getContainer()->get('badiu.ams.offer.classe.data');
            $sql="SELECT c.id, c.name FROM ".$data->getBundleEntity()." o JOIN o.odisciplineid od JOIN od.offerid f JOIN f.courseid c WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
            return  $result;
        
       
    }


    public function getMenuOffer($id) {
        
            $data=$this->getContainer()->get('badiu.ams.offer.classe.data');
            $sql="SELECT f.id, f.name FROM ".$data->getBundleEntity()." o JOIN o.odisciplineid od JOIN od.offerid f  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id); 
            $result= $query->getSingleResult();
            return  $result; 
        
    }
   

    public function getMenuDiscipline($id) {
        
             $data=$this->getContainer()->get('badiu.ams.offer.classe.data');
             $sql="SELECT od.id,od.disciplinename AS name FROM ".$data->getBundleEntity()." o JOIN o.odisciplineid od  WHERE o.id=:id";
           
            $query = $this->getEm()->createQuery($sql); 
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
            return  $result;
       
    }  
   
     public function getMenuClass($id) {
        
             $data=$this->getContainer()->get('badiu.ams.offer.classe.data');
             $sql="SELECT  o.id,o.name FROM ".$data->getBundleEntity()." o   WHERE o.id=:id";
           
            $query = $this->getEm()->createQuery($sql); 
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
            return  $result;
        
    }  
  
   

public function getInfoById($id) {
	    $sql="SELECT  o.timestart,o.timeend,d.dhour FROM ".$this->getBundleEntity()." o JOIN o.odisciplineid d WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
            return $result;
   }
public function getHourDuration($id) {
	    $sql="SELECT  d.dhour FROM ".$this->getBundleEntity()." o JOIN o.odisciplineid d WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
            if(isset($result['dhour'])){$result=$result['dhour'];}
            return $result;
   }
   
  public function geDisciplineidById($id) {
       
        $sql = "SELECT  d.id FROM " . $this->getBundleEntity() . " o JOIN o.odisciplineid d  WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
        if(isset($result['id'])){$result=$result['id'];}
        return  $result;
    }
 public function getPeriod($id) {
            $sql="SELECT o.timestart,o.timeend FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
	    return  $result;
        }
      
        
        
     public function geFirstClasseByDisciplineid($odisciplineid) {
          
            $sql = "SELECT MIN(o.id) AS id FROM " . $this->getBundleEntity() . " o  WHERE o.odisciplineid =:odisciplineid AND o.ctemplate =:ctemplate";
           $query = $this->getEm()->createQuery($sql);
          $query->setParameter('odisciplineid', $odisciplineid);
           $query->setParameter('ctemplate',FALSE);
          $result = $query->getOneOrNullResult();
          if(isset($result['id'])){$result=$result['id'];}
          return  $result;
       }

       public function getListidByDisciplineidMarker($odisciplineid,$marker) {
        $marker=strtolower($marker);
        $sql = "SELECT o.id FROM " . $this->getBundleEntity() . " o  WHERE o.odisciplineid =:odisciplineid AND o.ctemplate =:ctemplate AND LOWER(o.marker) LIKE :marker ";
       $query = $this->getEm()->createQuery($sql);
      $query->setParameter('odisciplineid', $odisciplineid); 
       $query->setParameter('ctemplate',FALSE);
       $query->setParameter('marker', '%'.$marker.'%'); 
      $result = $query->getResult();
     
      return  $result;
   }
   
   
   public function getDconfig($id) {
            $sql="SELECT o.dconfig AS classedconfig,od.dconfig AS disclipinedconfig,f.dconfig AS offerdconfig FROM ".$this->getBundleEntity()." o JOIN o.odisciplineid od JOIN od.offerid f  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
			return  $result;
        }
		
			
}
