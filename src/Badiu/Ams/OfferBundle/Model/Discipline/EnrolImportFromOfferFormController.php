<?php

namespace Badiu\Ams\OfferBundle\Model\Discipline;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnrolImportFromOfferFormController extends BadiuFormController
{ 
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     
     public function save() {
          $param = $this->getParam();
        
          $odisciplineid=$this->getParamItem('odisciplineid');
          $offerid=$this->getContainer()->get('badiu.ams.offer.discipline.data')->geOfferidById($odisciplineid);
          $tdata= $this->getContainer()->get('badiu.ams.enrol.lib.transfer');
          $tdata->init('offer',$offerid,'discipline',$odisciplineid,$param);
          $result=$tdata->exec();
         $this->setSuccessmessage($this->getTranslator()->trans('badiu.ams.enrol.import.info',array('%countrecord%'=>$result))); 
          $outrsult=array('result'=>$result,'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
          $this->getResponse()->setStatus("accept");
          $this->getResponse()->setMessage($outrsult);
         return $this->getResponse()->get();
         
     }
    
}
