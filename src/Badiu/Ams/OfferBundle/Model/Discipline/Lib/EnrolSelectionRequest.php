<?php

namespace Badiu\Ams\OfferBundle\Model\Discipline\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class EnrolSelectionRequest extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

	
    public function exec($param) {
         $entity= $this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		 
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 
		  //date enrol
		 $mdltimestart=0;
		 $mdltimeend=0;
		 $timestart=null;;
		 $timeend=null;
		 $etparam=array('modulekey'=>'badiu.ams.offer.discipline','moduleinstance'=>$moduleinstance);
		 $enroltime=$this->getContainer()->get('badiu.ams.enrol.lib.time');
		 $resulenroltime=$enroltime->getConfigPeriodValidate($etparam);
		
		if(!empty($resulenroltime)){
			$mdltimestart=$resulenroltime->timestarttp;
			$mdltimeend=$resulenroltime->timeendtp;
			$timestart=$resulenroltime->timestart;
			$timeend=$resulenroltime->timeend;
		}
		
		 
		 $disciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
		 
		$disciplineinfo= $disciplinedata->getNameParent($moduleinstance);
		$disciplineid=$moduleinstance;
		$offerid=$this->getUtildata()->getVaueOfArray($classeinfo,'offerid');
		 
		 $statusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,'active');
		 $roleid=$this->getContainer()->get('badiu.ams.role.role.data')->getIdByShortname($entity,'student');
		  
		 $disciplinelib=$this->getContainer()->get('badiu.ams.enrol.discipline.lib');
		 $disciplinelib->add($disciplineid,$userid,$roleid,$statusid,$timestart,$timeend);
				 	
		 $offerlib=$this->getContainer()->get('badiu.ams.enrol.offer.lib');
		 $offerlib->add($offerid,$userid,$roleid,$statusid,$timestart,$timeend);
		
		 $fparamdiscipline=array('id'=>$moduleinstance);
		 $dconfig=$disciplinedata->getGlobalColumnValue('dconfig',$fparamdiscipline);
		 
		
		 
		 $dconfig = $this->getJson()->decode($dconfig, true); 
		 
		 $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		 $serviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
		 $lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynclevel',true);
		 $lmscoursecatid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatid',true);
		 $lmscourseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);		
		 $groupid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursegroupid',true);			 
	     $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		 $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		
		if(($lmssynclevel=='course' || $lmssynclevel=='group')  && $lmscourseid > 0 && $serviceid > 0){
			$mmelparam=array();
			$mmelparam['userid']=$userid;		
			$mmelparam['courseid']=$lmscourseid;
			$mmelparam['groupid']=$groupid;
			$mmelparam['sserviceid']=$serviceid;
			$mmelparam['timestart']=$mdltimestart;
			$mmelparam['timeend']=$mdltimeend;
		 	$managesyncmoodle= $this->getContainer()->get('badiu.ams.enrol.lib.managesyncmoodle');
			$managesyncmoodle->process($mmelparam);
			 
		}
		return  null;
    }


 public function getUrl($param) {
         $entity= $this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		 
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 
		 $disciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
		 $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
		 
		 $fparamdiscipline=array('id'=>$moduleinstance);
		 $dconfig=$disciplinedata->getGlobalColumnValue('dconfig',$fparamdiscipline);
		 
		 
		 $dconfig = $this->getJson()->decode($dconfig, true); 
		 
		 $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		 $serviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
		 $lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynclevel',true);
		 $lmscoursecatid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatid',true);
		 $lmscourseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);				
	     $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		 $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		$url=null;
		if(($lmssynclevel=='course' || $lmssynclevel=='group') &&  $lmscourseid > 0 && $serviceid > 0){
			$urltarget="/course/view.php?id=$lmscourseid";
			$url=$utilapp->getUrlByRoute('badiu.system.core.service.process',array('_service'=>'badiu.moodle.core.lib.remoteaccess','_function'=>'remoteAuth','_serviceid'=>$serviceid,'_urltarget'=>$urltarget));
		}
		return  $url;
    }

}
