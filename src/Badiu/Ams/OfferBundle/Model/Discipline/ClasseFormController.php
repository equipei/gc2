<?php

namespace Badiu\Ams\OfferBundle\Model\Discipline;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ClasseFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     public function changeParamOnOpen() {
			$this->getFieldProcessOnOpen();
           $this->inheritDconfig();
           $this->inheritDisciplineConfig();
            $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
            $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $param = $this->getParam();
            $lmsintegration = $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');

            $param['sserviceid'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'sserviceid');
            $param['lmssynctype'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynctype');
            $param['lmssynclevel'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynclevel');
            $param['lmscoursecat'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatid');
            $param['lmscoursecatparent'] =$this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatparentid');
            $param['lmscourse'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseid');
            $param['lmscoursegroup'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursegroupid');
            $param['lmscourseidtemplate'] = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseidtemplate');
			
            $param['productintegration'] = $this->getUtildata()->getVaueOfArray($dconfig, 'financintegration.productintegration',true);
           
            
			if($this->isEdit()){$param=$this->getContainer()->get('badiu.ams.core.lib.roleconclusionform')->disciplineFormChangeParamOnOpen($param);}
			
			if($this->isEdit()){$param=$this->getContainer()->get('badiu.ams.offer.lib.enrolselectionfilter')->changeParamOnOpenEdit($param,'badiu.ams.offer.classe');}
			
            
            if(!$this->isEdit()){
                $lmsmoodlesyncdefaultform=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesyncdefaultform');
                $parentid= $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
                $lmsmoodlesyncdefaultform->setParentid($parentid);
                 $param=$lmsmoodlesyncdefaultform->classeFormChangeParamOnOpen($param);
            }else{$param=$this->getContainer()->get('badiu.tms.core.lib.lmsmoodlesyncdefaultform')->defaultChangeParamOnEdit($param);}
            $this->setParam($param); 
			
			
     }   
     
    public function changeParam() {
		$param = $this->getParam();
      
        $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
        if (empty($dconfig) || !is_array($dconfig)) {
            $dconfig = array();
        }

		$param['dconfig'] = $dconfig;
		$param=$this->getContainer()->get('badiu.ams.core.lib.roleconclusionform')->disciplineFormChangeParam($param);
		$dconfig = $this->getUtildata()->getVaueOfArray($param, 'dconfig');
        $dconfig = $this->getJson()->encode($dconfig);
		
        $param['dconfig'] = $dconfig;
		
        $this->setParam($param);
        
		
     }
      
     
      public function inheritDconfig() {
         //return null;
          if($this->isEdit()){return null;}
          $dconfig=null;
          $isinherit=false;
         //has dconfig if is edit
         $entity= $this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
        
         $disciplineid= $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
 
      
         if(!empty($disciplineid)){
         
              $sdata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
              $dconfig=$sdata->getColumnValue($entity,'dconfig',$paramf=array('id'=>$disciplineid));
              
              $dconfig = $this->getJson()->decode($dconfig, true);
              $status= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.status',true);
              if($status=='active'){$isinherit=true;}
              $dconfig['lmsintegration']['lmscoursecatparent']= null;
              $dconfig['lmsintegration']['lmscoursecatparenid']= null;
              $dconfig['lmsintegration']['lmscoursecatparentname']= null;
              $lmssynctypeoriginal=$this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmssynctypeoriginal',true);
              if(!empty($lmssynctypeoriginal)){$dconfig['lmsintegration']['lmssynctype']= $lmssynctypeoriginal;}
         
              $dconfig['lmsintegration']['lmscoursegroupid']= null;
              $dconfig['lmsintegration']['lmscoursegroup']= null;
              
              $lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmssynclevel',true);
              if($lmssynclevel=='course'){
                   $dconfig['lmsintegration']['lmssynclevel']='group';
              }
			  $param['lmscourseidtemplate'] = $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscourseidtemplate');
			  $param['lmscoursenametemplate'] = $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmscoursenametemplate');
              $dconfig = $this->getJson()->encode($dconfig);
              $this->addParamItem('dconfig',$dconfig);
        }
        
        
         //echo "sectionid: $sectionid";exit;
         if(!$isinherit){
             
               $sectionid=$this->getContainer()->get('badiu.ams.offer.discipline.data')->geSectionidById($disciplineid);
               $sdata=$this->getContainer()->get('badiu.ams.offer.section.data');
               $dconfig=$sdata->getColumnValue($entity,'dconfig',$paramf=array('id'=>$sectionid));
              
              $dconfig = $this->getJson()->decode($dconfig, true);
              $status= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.status',true);
              if($status=='active'){$isinherit=true;}
              $dconfig['lmsintegration']['lmscoursecatparent']= null;
              $dconfig['lmsintegration']['lmscoursecatparenid']= null;
              $dconfig['lmsintegration']['lmscoursecatparentname']= null;
              $lmssynctypeoriginal=$this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmssynctypeoriginal',true);
              if(!empty($lmssynctypeoriginal)){$dconfig['lmsintegration']['lmssynctype']= $lmssynctypeoriginal;}
         
              $dconfig = $this->getJson()->encode($dconfig);
           
              $this->addParamItem('dconfig',$dconfig);
        }
         // get config of offer
        if(!$isinherit){
            $offerid=$this->getContainer()->get('badiu.ams.offer.discipline.data')->geOfferidById($disciplineid);
             
            $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
            $dconfig=$odata->getColumnValue($entity,'dconfig',$paramf=array('id'=>$offerid));
            
              $dconfig = $this->getJson()->decode($dconfig, true);
              $dconfig['lmsintegration']['lmscoursecatparent']= null;
              $dconfig['lmsintegration']['lmscoursecatparenid']= null;
              $dconfig['lmsintegration']['lmscoursecatparentname']= null;
              $lmssynctypeoriginal=$this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration.lmssynctypeoriginal',true);
              if(!empty($lmssynctypeoriginal)){$dconfig['lmsintegration']['lmssynctype']= $lmssynctypeoriginal;}
              $dconfig = $this->getJson()->encode($dconfig);
              $this->addParamItem('dconfig',$dconfig);
            
        }
          
     }
    
     public function inheritDisciplineConfig() {
        if($this->isEdit()){return null;}
        $entity= $this->getEntity();
        $disciplineid= $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
        if(empty($disciplineid)){return null;}
        $ddata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
        $dto= $ddata->findById($disciplineid);
        $objectutil=$this->getContainer()->get('badiu.system.core.lib.util.objectutil');
        $dto= $objectutil->castEntityToArrayForForm($dto);

        $typeaccessid= $this->getUtildata()->getVaueOfArray($dto, 'typeaccessid');
        $this->addParamItem('typeaccessid',$typeaccessid);

        $typemanagerid= $this->getUtildata()->getVaueOfArray($dto, 'typemanagerid');
        $this->addParamItem('typemanagerid',$typemanagerid);

        $typeid = $this->getUtildata()->getVaueOfArray($dto, 'typeid');
        $this->addParamItem('typeid',$typeid);
		
		
		//enrol
		$this->addParamItem('enroldatetype',$this->getUtildata()->getVaueOfArray($dto, 'enroldatetype'));
		$this->addParamItem('enroldatedynamic',$this->getUtildata()->getVaueOfArray($dto, 'enroldatedynamic'));
		$this->addParamItem('enrolreplicate',$this->getUtildata()->getVaueOfArray($dto, 'enrolreplicate'));
		$this->addParamItem('enrolreplicateupadate',$this->getUtildata()->getVaueOfArray($dto, 'enrolreplicateupadate'));
		$this->addParamItem('maxenrol',$this->getUtildata()->getVaueOfArray($dto, 'maxenrol'));
					
				
		$dtype=$this->getParamItem('dtype');
		
		//enable it to limit tms
		//if($dtype=='training'){
			 $enablecertificate = $this->getUtildata()->getVaueOfArray($dto, 'enablecertificate');
			 $certificatetempleid = $this->getUtildata()->getVaueOfArray($dto, 'certificatetempleid');
			 $this->addParamItem('enablecertificate',$enablecertificate);
			 $this->addParamItem('certificatetempleid',$certificatetempleid);
		//}
       
		$certificateupdateuser = $this->getUtildata()->getVaueOfArray($dto, 'certificateupdateuser');
		$certificateupdateenrol = $this->getUtildata()->getVaueOfArray($dto, 'certificateupdateenrol');
		$certificateupdateobjectinfo = $this->getUtildata()->getVaueOfArray($dto, 'certificateupdateobjectinfo');
		$certificateupdateobjectdata = $this->getUtildata()->getVaueOfArray($dto, 'certificateupdateobjectdata');
		
		$this->addParamItem('certificateupdateuser',$certificateupdateuser);
		$this->addParamItem('certificateupdateenrol',$certificateupdateenrol);
		$this->addParamItem('certificateupdateobjectinfo',$certificateupdateobjectinfo);
		$this->addParamItem('certificateupdateobjectdata',$certificateupdateobjectdata);
	   
		//teachingplan
		$this->addParamItem('dhour', $this->getUtildata()->getVaueOfArray($dto, 'dhour'));
		$this->addParamItem('credit', $this->getUtildata()->getVaueOfArray($dto, 'credit'));
		 $this->addParamItem('objective',$this->getUtildata()->getVaueOfArray($dto, 'objective'));
		 $this->addParamItem('usrtarget',$this->getUtildata()->getVaueOfArray($dto, 'usrtarget'));
		 $this->addParamItem('summary',$this->getUtildata()->getVaueOfArray($dto, 'summary'));
		 $this->addParamItem('teachingplan',$this->getUtildata()->getVaueOfArray($dto, 'teachingplan'));
		 $this->addParamItem('competence',$this->getUtildata()->getVaueOfArray($dto, 'competence'));
		 $this->addParamItem('usertargetestimatedamout',$this->getUtildata()->getVaueOfArray($dto, 'usertargetestimatedamout'));
		 $this->addParamItem('usertarget',$this->getUtildata()->getVaueOfArray($dto, 'usertarget'));
		 $this->addParamItem('costdev',$this->getUtildata()->getVaueOfArray($dto, 'costdev'));
		 $this->addParamItem('defaultimage',$this->getUtildata()->getVaueOfArray($dto, 'defaultimage'));
		 
		 //dev authordevid
		  $this->addParamItem('projectid',$this->getUtildata()->getVaueOfArray($dto, 'projectid'));
		  $this->addParamItem('enterpriseid',$this->getUtildata()->getVaueOfArray($dto, 'enterpriseid'));
		  $this->addParamItem('departmentid',$this->getUtildata()->getVaueOfArray($dto, 'departmentid'));
		  $this->addParamItem('typedevid',$this->getUtildata()->getVaueOfArray($dto, 'typedevid'));
		  $this->addParamItem('typepartnershipid',$this->getUtildata()->getVaueOfArray($dto, 'typepartnershipid'));
		  $this->addParamItem('authordevid',$this->getUtildata()->getVaueOfArray($dto, 'authordevid'));
		  $this->addParamItem('devinfo',$this->getUtildata()->getVaueOfArray($dto, 'devinfo'));
		 
		 
		 
		 
     }  
    /* public function inheritDconfig() {
          if($this->isEdit()){return null;}
          $dconfig=null;
         //has dconfig if is edit
         $entity= $this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
         $disciplineid=$this->getParamItem('disciplineid');
         if(!empty($disciplineid)){
              $sdata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
              $dconfig=$sdata->getColumnValue($entity,'dconfig',$paramf=array('id'=>$disciplineid));
              $this->addParamItem('dconfig',$dconfig);
        }
         // get config of offer
        if(empty($dconfig)){
            $parentid = $this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
            $odata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
            $dconfig=$odata->getColumnValue($entity,'dconfig',$paramf=array('id'=>$parentid));
            $this->addParamItem('dconfig',$dconfig);
            
        }
          
     }*/
         
    
}
