<?php

namespace Badiu\Ams\OfferBundle\Model\Discipline;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;

//move this classe to folder classe
class ClasseFormat extends BadiuFormat {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function measure($data) {
        $result = null;
        $measuretype = null;
        $measurevalue = null;

        if (array_key_exists("measurevalue", $data)) {

            $measurevalue = $data["measurevalue"];
            if ($measurevalue < 60) {
                $result = "$measurevalue min ";
            } else if ($measurevalue >= 60) {
                $hour = $measurevalue / 60;
                $result = "$hour h ";
                $result = "$hour h ";
            }
        }


        return $result;
    }

    public function period($data) {
        $result = null;
        $timestart = null;
        $timeend = null;
        if (array_key_exists("timestart", $data)) { $timestart=$data["timestart"];}
        if (array_key_exists("timeend", $data)) { $timeend=$data["timeend"];}
        
        
        if ($timestart!=null && $timeend!=null ) {
            $dateformat=$this->getContainer()->get('badiu.system.core.lib.format.dateformat');
             $result= $dateformat->period($timestart,$timeend);
        }
            



        return $result;
    }

    
    public function grade($data) {
        $grade = null;
        if (array_key_exists("grade", $data)) { $grade=$data["grade"];}
      
        $html="<div>{{badiubasedefaulttablehead}}</div>"; 
        /* $html.=' <div>
                              <div  inline-edit="token" on-save="updateTodo(token)" on-cancel="cancelEdit(token)" ng-hide="token == null || token.length == 0">{{token}}</div>

                            <div ng-show="token == null || token.length == 0">
                              <input type="text" class="form-control" id="inputtoken" ng-change="addFunc(token)" ng-model="token" placeholder="token">
                            </div>

                            </div>';*/
        //return $grade;
         return $html;
    }
    
    public function teacher($data) {
        $classeid = null;
         
        $value="";
        if (array_key_exists("id", $data)) { $classeid=$data["id"];}
        if(empty($classeid)){return  $value;}
        $session =$this->getContainer()->get('session');
        $key="badiu.ams.offer.classe.dbsearch.fields.table.view.dashboard.db.sql.1";
        $rows=$session->get($key);
        $cont=0;
    if(empty($rows)) {return $value;}
        foreach ($rows as $row) {
           
            $rclasseid=null;
            $user=null;
           if (array_key_exists("classid", $row)){$rclasseid=$row["classid"];}
           if (array_key_exists("user", $row)){$user=$row["user"];}
           if($classeid==$rclasseid){
                if($cont==0){$value.=$user;}
                else{$value.="<br />".$user;}
                $cont++;
           }
        }
        
        return $value;
    }
	
	public function summary($data) {
        $result = null;
        $summary =$this->getUtildata()->getVaueOfArray($data,'summary');
		$odsummary =$this->getUtildata()->getVaueOfArray($data,'odsummary');
		$crdsummary =$this->getUtildata()->getVaueOfArray($data,'crdsummary');
		$dsummary =$this->getUtildata()->getVaueOfArray($data,'dsummary');
      
	   if(!empty($summary)){$result=$summary;}
	   else if(!empty($odsummary)){$result=$odsummary;}
	   else if(!empty($crdsummary)){$result=$crdsummary;}
	   else if(!empty($dsummary)){$result=$dsummary;}
        
        return $result;
    }
	
	 public function teachingplan($data) {
        $result = null;
        $teachingplan =$this->getUtildata()->getVaueOfArray($data,'teachingplan');
		$odteachingplan =$this->getUtildata()->getVaueOfArray($data,'odteachingplan');
		$crdteachingplan =$this->getUtildata()->getVaueOfArray($data,'crdteachingplan');
		$dteachingplan =$this->getUtildata()->getVaueOfArray($data,'dteachingplan');
      
	   if(!empty($teachingplan)){$result=$teachingplan;}
	   else if(!empty($odteachingplan)){$result=$odteachingplan;}
	   else if(!empty($crdteachingplan)){$result=$crdteachingplan;}
	   else if(!empty($dteachingplan)){$result=$dteachingplan;}
        
        return $result;
    }
   
 public function progressactivities($data) {
       
			$progressinfo =$this->getUtildata()->getVaueOfArray($data,'progress');
			
			$progressinfo = $this->getJson()->decode($progressinfo, true);
			
            $countactivityanebleprogress=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivityanebleprogress',true);
			$countactivitycompleted=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivitycompleted',true);
			
			$countactivityanebleprogressincoursecompletetionconfig=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivityanebleprogressincoursecompletetionconfig',true);
			$countactivitycompleincoursecompletetionconfig=$this->getUtildata()->getVaueOfArray($progressinfo,'progress.countactivitycompleincoursecompletetionconfig',true);
			
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$courseprogresscountonlyrequiredactivities=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.courseprogresscountonlyrequiredactivities');
				
			
			$x=0;
			$y=0;
			if(!$courseprogresscountonlyrequiredactivities){$x=$countactivitycompleted;$y=$countactivityanebleprogress; }
			else if($courseprogresscountonlyrequiredactivities){$x=$countactivitycompleincoursecompletetionconfig;$y=$countactivityanebleprogressincoursecompletetionconfig;}
			
			 $percn=0;
			if($x!=null && $y!=null){
				if($y >=0 && $x >=0 && $y>= $x){
					
					if($y==0){$perc=0;}
					else{
						$percn= $x*100/$y;
						$perc=number_format($percn, 2, ',', '.');
					}
				$perc= $this->getTranslator()->trans('badiu.system.percentage.shownumber',array('%x%'=>$x,'%y%'=>$y,'%percent%'=>$perc)); 
				$perc="$perc <br/><progress  value=\"$percn\" max=\"100\"></progress>  ";
              
			 }
			}else{$perc="";}
			return  $perc;
}   
}
