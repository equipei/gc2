<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class OfferDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }

    public  function sourcePeriodImportFromCurriculum(){
        $list = array();
        $list['offer']=$this->getTranslator()->trans('badiu.ams.offer.disciplineimportfromcurriculum.dateofoffer');
        $list['section']=$this->getTranslator()->trans('badiu.ams.offer.disciplineenrolimportfromoffer.dateofsection');
        return $list;
    }

    
}
