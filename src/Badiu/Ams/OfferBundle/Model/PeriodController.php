<?php

namespace Badiu\Ams\OfferBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class PeriodController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
      


    public function addOfferidToPeriod($dto,$dtoParent) {
      $dto->setOfferid($dtoParent);
        return $dto;
    }
     public function findOfferidInPeriod($dto) {

        return $dto->getOfferid()->getId();
    }
	
	
	
}


