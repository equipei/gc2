<?php

namespace Badiu\Ams\OfferBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
       
           $result= $this->initDbCategory();
           $result+= $this->initDbStatus();
           $result+= $this->initDbLevel();
           $result+= $this->initDbType();
           $result+= $this->initDbDisciplineStatus();
           $result+= $this->initDbDisciplineCategory();
           $result+= $this->initDbActivityStatus();
           $result+= $this->initDbClasseStatus();
           $result+= $this->initDbClasseTaughtStatus();
           $result+= $this->initDbTypemanager();
           $result+= $this->initDbTypeaccess();
           return $result;
    } 

     public function initDbCategory() {
         $cont=0; 
         $dtype='course';
         $data = $this->getContainer()->get('badiu.ams.offer.category.data');
         $entity=$this->getEntity();
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist){return 0;}
         $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
         $factorykeytree->init('badiu.ams.offer.category.data');
         
        
         
         //category add  systematic
         $param=array();
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.ams.offer.category.systematic');
         $param['shortname']='systematic';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         $param['dtype']=$dtype;
         $idpath=null;
          $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
          $level = $factorykeytree->getLevel($newidtree);
          $order = $factorykeytree->getOrder($newidtree);
          
          $parentinfo = $factorykeytree->getParentInfo($entity, $idpath, $dtype);
          $dtotree=array();
          $dtotree = $factorykeytree->makePath($parentinfo, $dtotree);
          
           $param['idpath']=$newidtree;
           $param['level']=$level;
           $param['orderidpath']=$order;
           $param['path']=$this->getUtildata()->getVaueOfArray($dtotree, 'path');
           $param['parent']=$this->getUtildata()->getVaueOfArray($dtotree, 'parent');
         if(! $data->existByShortname($entity,'systematic')){
              $result =  $data->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         //category add free
         $param=array();
         $idpath=null;
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.ams.offer.category.free');
         $param['shortname']='free';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         $param['dtype']=$dtype;
         
          $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
          $level = $factorykeytree->getLevel($newidtree);
          $order = $factorykeytree->getOrder($newidtree);
          
          $parentinfo = $factorykeytree->getParentInfo($entity, $idpath, $dtype);
          $dtotree=array();
          $dtotree = $factorykeytree->makePath($parentinfo, $dtotree);
       
          
           $param['idpath']=$newidtree;
           $param['level']=$level;
           $param['orderidpath']=$order;
           $param['path']=$this->getUtildata()->getVaueOfArray($dtotree, 'path');
           $param['parent']=$this->getUtildata()->getVaueOfArray($dtotree, 'parent');
         if(! $data->existByShortname($entity,'free')){
              $result =  $data->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         
     }
     
     public function initDbStatus() {
         $cont=0;
         $dtype='course';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.offer.status.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist){return 0;}
         
         $list=array();
         $list['active']=$this->getTranslator()->trans('badiu.ams.offer.status.active');
         $list['canceled']=$this->getTranslator()->trans('badiu.ams.offer.status.canceled');
         $list['ongoing']=$this->getTranslator()->trans('badiu.ams.offer.status.ongoing');
         $list['closed']=$this->getTranslator()->trans('badiu.ams.offer.status.closed');
         $list['suspense']=$this->getTranslator()->trans('badiu.ams.offer.status.suspense');
         
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
            $param['dtype']=$dtype;
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
         
     }
     
     public function initDbLevel() {
         $cont=0;
         $dtype='course';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.offer.level.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist){return 0;}
         
         $list=array();
         $list['basiceducation']=$this->getTranslator()->trans('badiu.ams.offer.level.basiceducation');
         $list['medioumeducation']=$this->getTranslator()->trans('badiu.ams.offer.level.medioumeducation');
         $list['oprofessionaleducation']=$this->getTranslator()->trans('badiu.ams.offer.level.professionaleducation');
         $list['highereducation']=$this->getTranslator()->trans('badiu.ams.offer.level.highereducation');
         $list['mastereducation']=$this->getTranslator()->trans('badiu.ams.offer.level.mastereducation');
         
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
            $param['dtype']=$dtype;
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
         
     }
     
      public function initDbType() {
         $cont=0;
         $dtype='course';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.offer.type.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist){return 0;}
         
         $list=array();
         $list['elearning']=$this->getTranslator()->trans('badiu.ams.offer.type.elearning');
         $list['presential']=$this->getTranslator()->trans('badiu.ams.offer.type.presential');
         $list['blended']=$this->getTranslator()->trans('badiu.ams.offer.type.blended');
        
         
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
            $param['dtype']=$dtype;
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
         
     }
     
     public function initDbDisciplineStatus() {
         $cont=0;
         $dtype='course';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.offer.disciplinestatus.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist){return 0;}
         
         $list=array();
         $list['active']=$this->getTranslator()->trans('badiu.ams.offer.disciplinestatus.active');
         $list['canceled']=$this->getTranslator()->trans('badiu.ams.offer.disciplinestatus.canceled');
         $list['ongoing']=$this->getTranslator()->trans('badiu.ams.offer.disciplinestatus.ongoing');
         $list['closed']=$this->getTranslator()->trans('badiu.ams.offer.disciplinestatus.closed');
                  
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
            $param['dtype']=$dtype;
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
         
     }
     
      public function initDbDisciplineCategory() {
         $cont=0; 
         $dtype='course';
         $data = $this->getContainer()->get('badiu.ams.offer.disciplinecategory.data');
         $entity=$this->getEntity();
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist){return 0;}
         $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
         $factorykeytree->init('badiu.ams.offer.disciplinecategory.data');
         
        $list=array();
         $list['regular']=$this->getTranslator()->trans('badiu.ams.offer.disciplinecategory.regular');
         $list['extracurricular']=$this->getTranslator()->trans('badiu.ams.offer.disciplinecategory.extracurricular');
         
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
             $param['dtype']=$dtype;
            $idpath=null;
            $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
            $level = $factorykeytree->getLevel($newidtree);
            $order = $factorykeytree->getOrder($newidtree);
          
            $parentinfo = $factorykeytree->getParentInfo($entity, $idpath, $dtype);
            $dtotree=array();
            $dtotree = $factorykeytree->makePath($parentinfo, $dtotree);
          
            $param['idpath']=$newidtree;
            $param['level']=$level;
             $param['orderidpath']=$order;
             $param['path']=$this->getUtildata()->getVaueOfArray($dtotree, 'path');
            $param['parent']=$this->getUtildata()->getVaueOfArray($dtotree, 'parent');
            if(! $data->existByShortname($entity,$key)){
                $result =  $data->insertNativeSql($param,false); 
                if($result){$cont++;}
            }
         }
      }
      
      
      public function initDbActivityStatus() {
         $cont=0;
         $dtype='course';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.offer.activitystatus.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist){return 0;}
         
         $list=array();
         $list['planned']=$this->getTranslator()->trans('badiu.ams.offer.activitystatus.planned');
         $list['executed']=$this->getTranslator()->trans('badiu.ams.offer.activitystatus.executed');

         
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
            $param['dtype']=$dtype;
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
         
     }
     
      public function initDbClasseStatus() {
         $cont=0;
         $dtype='course';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.offer.classestatus.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist){return 0;}
         
          $list=array();
         $list['active']=$this->getTranslator()->trans('badiu.ams.offer.classestatus.active');
         $list['canceled']=$this->getTranslator()->trans('badiu.ams.offer.classestatus.canceled');
         $list['ongoing']=$this->getTranslator()->trans('badiu.ams.offer.classestatus.ongoing');
         $list['closed']=$this->getTranslator()->trans('badiu.ams.offer.classestatus.closed');

         
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
            $param['dtype']=$dtype;
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
         
     }
     
      public function initDbClasseTaughtStatus() {
         $cont=0;
         $dtype='course';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.offer.classetaughtstatus.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist){return 0;}
         
         $list=array();
         $list['planned']=$this->getTranslator()->trans('badiu.ams.offer.classetaughtstatus.planned');
         $list['executed']=$this->getTranslator()->trans('badiu.ams.offer.classetaughtstatus.executed');

         
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
            $param['dtype']=$dtype;
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
         
     }


     public function initDbTypemanager() {
      $cont=0;
      $entity=$this->getEntity();
      $data = $this->getContainer()->get('badiu.ams.offer.typemanager.data');
      $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>'course'));
      if($exist && !$this->getForceupdate()){return 0;}
      
       $entity=$this->getEntity();
      
     
      $list=array(
         array('entity'=>$entity,'shortname'=>'coursewithteacher','dtype'=>'course','name'=>$this->getTranslator()->trans('badiu.ams.offer.typemanager.coursewithteacher')),
         array('entity'=>$entity,'shortname'=>'coursewithoutteacher','dtype'=>'course','name'=>$this->getTranslator()->trans('badiu.ams.offer.typemanager.coursewithoutteacher')),
         array('entity'=>$entity,'shortname'=>'coursetutoriale','dtype'=>'course','name'=>$this->getTranslator()->trans('badiu.ams.offer.typemanager.coursetutorial')),
         array('entity'=>$entity,'shortname'=>'coursevideoconference','dtype'=>'course','name'=>$this->getTranslator()->trans('badiu.ams.offer.typemanager.coursevideoconference')),
         
      );
      
      foreach ($list as $param) {
          $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
          $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
          $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
          
          $paramedit=null;
          if($this->getForceupdate()){
              $paramedit= $param;
          }
          $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
          $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
          $r=$this->getUtildata()->getVaueOfArray($result,'id');
           if($r){$cont++;}
         }
         
    return $cont;
      
  }

  public function initDbTypeaccess() {
    $cont=0;
    $entity=$this->getEntity();
    $data = $this->getContainer()->get('badiu.ams.offer.typeaccess.data');
    $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>'course'));
    if($exist && !$this->getForceupdate()){return 0;}
    
     $entity=$this->getEntity();
    
   
    $list=array(
       array('entity'=>$entity,'shortname'=>'coursefree','dtype'=>'course','name'=>$this->getTranslator()->trans('badiu.ams.offer.typeaccess.coursefree')),
       array('entity'=>$entity,'shortname'=>'couserestricted','dtype'=>'course','name'=>$this->getTranslator()->trans('badiu.ams.offer.typeaccess.couserestricted')),
      
    );
    
    foreach ($list as $param) {
        $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
        $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
        $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
        
        $paramedit=null;
        if($this->getForceupdate()){
            $paramedit= $param;
        }
        $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
        $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
        $r=$this->getUtildata()->getVaueOfArray($result,'id');
         if($r){$cont++;}
       }
       
  return $cont;
    
}
}
