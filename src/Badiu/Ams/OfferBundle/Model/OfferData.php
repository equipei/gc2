<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\CoreBundle\Model\AmsDataBase;
class OfferData  extends AmsDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
	public function getNameParent($id) {
		if(empty($id)){return null;} 
         $sql="SELECT o.id,o.name,c.id AS courseid, c.name AS coursename FROM ".$this->getBundleEntity()." o LEFT JOIN o.courseid c WHERE o.id=:id";
         $query = $this->getEm()->createQuery($sql);
         $query->setParameter('id',$id);
         $result= $query->getOneOrNullResult();
         return  $result;
    }
   
   public function getMenuCourse($id) {
         $sql="SELECT c.id, c.name FROM ".$this->getBundleEntity()." o JOIN o.courseid c WHERE o.id=:id";
         $query = $this->getEm()->createQuery($sql);
         $query->setParameter('id',$id);
         $result= $query->getSingleResult();
         return  $result;
    }
public function getCourseid($id) {
            $sql="SELECT c.id FROM ".$this->getBundleEntity()." o JOIN o.courseid c WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
			if(!empty($result)){$result=$result['id'];}
            return  $result;
        }
       
    public function getCourseName($id) {
            $sql="SELECT c.name FROM ".$this->getBundleEntity()." o JOIN o.courseid c WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
	   if(!empty($result)){$result=$result['name'];}
            return  $result;
        }

    public function getCurriculumid($id) {
            $sql="SELECT cr.id FROM ".$this->getBundleEntity()." o JOIN o.curriculumid cr WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
			if(!empty($result)){$result=$result['id'];}
            return  $result;
        }

    public function getPeriod($id) {
            $sql="SELECT o.timestart,o.timeend FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
	    return  $result;
        }
		
	public function getDconfig($id) {
            $sql="SELECT o.dconfig AS offerdconfig FROM ".$this->getBundleEntity()." o  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getSingleResult();
			return  $result;
        }
}
