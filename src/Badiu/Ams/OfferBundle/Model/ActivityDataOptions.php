<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class ActivityDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
              
    
    public  function getTopicsEntiy(){
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
        $classeid=$this->getContainer()->get('request')->get('parentid');
        $param=  array('classeid'=>$classeid);
        $data = $this->getContainer()->get('badiu.ams.offer.topic.data');
       $list= $data->findByParam($entity,$param);
        return $list;
    }
    
}
