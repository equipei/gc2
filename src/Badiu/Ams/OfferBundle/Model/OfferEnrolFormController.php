<?php

namespace Badiu\Ams\OfferBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class OfferEnrolFormController extends BadiuFormController
{ 
	private $replication=array();
    function __construct(Container $container) {
            parent::__construct($container);
              }
     
       public function changeParamOnOpen() {
		   $param = $this->getParam();
		   if(!$this->isEdit()){
			  $offerid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
			  $etparam=array('modulekey'=>'badiu.ams.offer.offer','moduleinstance'=>$offerid);
			  $param=$this->getContainer()->get('badiu.ams.enrol.lib.time')->changeParamOnAddForm($etparam,$param);
			  $param=$this->getContainer()->get('badiu.ams.enrol.lib.replicateconfig')->changeParamOnAddForm($etparam,$param);
			  
			   $this->setParam($param); 
		   }
	   }
  
     public function execAfter(){
		  $enrolid=$this->getParamItem('id');
		  $offerid=$this->getParamItem('offerid');
		  $userid=$this->getParamItem('userid');
		  $enrolreplicate=$this->getParamItem('enrolreplicate');
		  $enrolreplicateupadate=$this->getParamItem('enrolreplicateupadate');
		  $timestart=$this->getParamItem('timestart');
		  $timeend=$this->getParamItem('timeend');
		 
		  $isedit=$this->isEdit();
		  $fparam=array('id'=>$enrolid,'offerid'=>$offerid,'userid'=>$userid,'isedit'=>$isedit,'enrolreplicate'=>$enrolreplicate,'enrolreplicateupadate'=>$enrolreplicateupadate,'timestart'=>$timestart,'timeend'=>$timeend);
		  $this->getContainer()->get('badiu.ams.enrol.offer.lib')->replicate($fparam);
		  parent::execAfter();
	 }
    
}
