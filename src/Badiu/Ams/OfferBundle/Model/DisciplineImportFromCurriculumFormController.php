<?php

namespace Badiu\Ams\OfferBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class DisciplineImportFromCurriculumFormController extends BadiuFormController
{ 
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     
     public function save() {
          $param = $this->getParam();
          $lib=$this->getContainer()->get('badiu.ams.offer.disciplineimportfromcurriculum.lib');
          $lib->setParam($param);
          $result=$lib->exec();
          $this->setSuccessmessage($this->getTranslator()->trans('badiu.ams.offer.disciplineenrolimportfromoffer.messageresult',array('%count%'=>$result))); 
          $outrsult=array('result'=>$result,'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
          $this->getResponse()->setStatus("accept");
          $this->getResponse()->setMessage($outrsult);
         return $this->getResponse()->get();
         
     }
    
      public function getListOfDisciplines($offerid) {
          $statusid=$this->getContainer()->get('badiu.ams.offer.disciplinestatus.data')->getIdByShortname($this->getEntity(), 'active');
          $listids= $this->getContainer()->get('badiu.ams.offer.discipline.data')->getColumnValues($this->getEntity(),'id',array('o.offerid'=>$offerid,'o.statusid'=>$statusid));
          return $listids;
      }
}
