<?php

namespace Badiu\Ams\OfferBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class DisciplineLmsMoodleIntegrationFilter extends BadiuFormFilter{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
   public function execBeforeSubmit() {
   
          $dconfig = $this->getUtildata()->getVaueOfArray($this->getParam(), 'dconfig');
        
          $param=$this->getParam();
 
          $lmsintegration=array();
          $lmsintegration['status']='pending';
          
          $lmsintegration['sserviceid']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'sserviceid');
          unset($param['sserviceid']);
          
          $lmsintegration['lmssynctype']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'lmssynctype');
          unset($param['lmssynctype']);
          
          $lmsintegration['lmssynclevel']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'lmssynclevel');
          unset($param['lmssynclevel']);
          
		  $lmsintegration['lmssyncforceupdate']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'lmssyncforceupdate');
          unset($param['lmssyncforceupdate']);
		  
           $lmsintegration['lmscoursecatparent']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'lmscoursecatparent');
          unset($param['lmscoursecatparent']);
          
          $lmsintegration['lmscoursecatid']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'lmscoursecat');
          unset($param['lmscoursecat']);
          
          $lmsintegration['lmscourseid']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'lmscourse');
          unset($param['lmscourse']);
           
          $lmsintegration['lmscoursegroupid']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'lmscoursegroup');
          unset($param['lmscoursegroup']);
          
          $lmsintegration['lmscourseidtemplate']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'lmscourseidtemplate');
          unset($param['lmscourseidtemplate']);
		  
          if(empty($dconfig)){$dconfig=array();}
          else if (!empty($dconfig) && !is_array($dconfig)){$dconfig = $this->getJson()->decode($dconfig,true);}
          $dconfig['lmsintegration']=$lmsintegration;
        
          $dconfig = $this->getJson()->encode($dconfig);
          $param['dconfig']=$dconfig;
          
          $this->setParam($param);
          
        }
        
   public function execAfterSubmit() {
         $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
         $lmsmoodleintegration=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodleintegration');
         $lmsmoodleintegration->setId($id);
         $lmsmoodleintegration->setDatakey('badiu.ams.offer.discipline.data');
         $lmsmoodleintegration->setField('dconfig');
         $lmsmoodleintegration->init();
         $lmsmoodleintegration->discipline();
         $param=$this->getParam();   
       
         $param['dconfig']=$lmsmoodleintegration->getParam();
        
         $this->setParam($param);
       }
       
}
