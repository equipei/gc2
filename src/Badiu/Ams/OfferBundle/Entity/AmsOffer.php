<?php
namespace Badiu\Ams\OfferBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * AmsOffer
 *
 * @ORM\Table(name="ams_offer", uniqueConstraints={
 *
 *      @ORM\UniqueConstraint(name="ams_offer_name_uix", columns={"entity", "dtype","name","courseid"}),
 *      @ORM\UniqueConstraint(name="ams_offer_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="ams_offer_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_offer_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_offer_name_ix", columns={"name"}), 
 *              @ORM\Index(name="ams_offer_curriculumid_ix", columns={"curriculumid"}),
 *              @ORM\Index(name="ams_offer_courseid_ix", columns={"courseid"}),
 *              @ORM\Index(name="ams_offer_categoryid_ix", columns={"categoryid"}),
 *              @ORM\Index(name="ams_offer_statusid_ix", columns={"statusid"}),
 *              @ORM\Index(name="ams_offer_typeid_ix", columns={"typeid"}),
 *              @ORM\Index(name="ams_offer_levelid_ix", columns={"levelid"}),
 *              @ORM\Index(name="ams_offer_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="ams_offer_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="ams_offer_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="ams_offer_enterpriseid_ix", columns={"enterpriseid"}),
 *              @ORM\Index(name="ams_offer_idnumber_ix", columns={"idnumber"})})
 * 
 * @ORM\Entity
 */
class AmsOffer   {
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
   
    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

     /**
     * @var AmsOfferCategory
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;
    
         /**
     * @var AmsOfferLevel
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferLevel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="levelid", referencedColumnName="id")
     * })
     */
    private $levelid;
    
        /**
     * @var \Badiu\Ams\CourseBundle\Entity\AmsCourse
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\CourseBundle\Entity\AmsCourse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="courseid", referencedColumnName="id")
     * })
     */
    private $courseid;
    
    	/**
     * @var boolean
     *
     * @ORM\Column(name="hascurriculum", type="integer", nullable=true)
     */
    private $hascurriculum=1;
    
     /**
     * @var \Badiu\Ams\CurriculumBundle\Entity\AmsCurriculum
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\CurriculumBundle\Entity\AmsCurriculum")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curriculumid", referencedColumnName="id")
     * })
     */
    private $curriculumid;
    
    
     /**
     * @var AmsOfferStatus
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;
    
	/**
     * @var AmsOfferTypeManager
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferTypeManager")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typemanager", referencedColumnName="id")
     * })
     */
    private $typemanager;
	
	
	 /**
     * @var AmsOfferPeriodDay
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferPeriodDay")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="periodday", referencedColumnName="id")
     * })
     */
    private $periodday;
	
	/**
     * @var string
     *
     * @ORM\Column(name="labelinfo", type="string", length=255, nullable=true)
     */
    private $labelinfo;
     /**
     * @var integer
     *
     * @ORM\Column(name="maxenrol", type="bigint", nullable=true)
     */
    private $maxenrol;
	
	/**
     * @var string
     *
     * @ORM\Column(name="enroldatetype", type="string", length=50, nullable=true)
     */
    private $enroldatetype; //periodofparent | perioddynamic
	
	/**
     * @var string
     *
     * @ORM\Column(name="enroldatedynamic", type="string", length=255, nullable=true)
     */
    private $enroldatedynamic; 
	
	/**
     * @var string
     *
     * @ORM\Column(name="enrolreplicate", type="string", length=255, nullable=true)
     */
    private $enrolreplicate;
	
	/**
     * @var string
     *
     * @ORM\Column(name="enrolreplicateupadate", type="string", length=255, nullable=true)
     */
    private $enrolreplicateupadate;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="enroltype", type="string", length=50, nullable=true)
     */
    private $enroltype; //manual | selectprocess
	
	
    /**
     * @var AmsOfferType
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typeid", referencedColumnName="id")
     * })
     */
    private $typeid;
    
 
    /**
	 *
	 * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="enterpriseid", referencedColumnName="id")
	 * })
	 */
	private $enterpriseid;
        
         /**
     * @var \Badiu\Util\HousingBundle\Entity\UtilHousingEdifice
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Util\HousingBundle\Entity\UtilHousingEdifice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hedificeid", referencedColumnName="id")
     * })
     */
    private $hedificeid;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
   
     /**
     * @var \Badiu\Admin\ServerBundle\Entity\AdminServerService
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ServerBundle\Entity\AdminServerService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sserviceid", referencedColumnName="id")
     * })
     */
    private $sserviceid;
    
         /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var string
     *
     * @ORM\Column(name="lmssynctype", type="string", length=255, nullable=true)
     */
    private $lmssynctype='syncwithcreateddatainlms';// replicationdatainlms | syncwithcreateddatainlms
    
    /**
     * @var string
     *
     * @ORM\Column(name="lmscontexttypeparent", type="string", length=255, nullable=true)
     */
    private $lmscontexttypeparent; //LMS context type: coursecategory|course|group
  
    /**
   * @var integer
   *
   * @ORM\Column(name="lmscontextidparent", type="bigint", nullable=true)
     */
    private $lmscontextidparent; //id of  context instance. If context is course is id of course
	 /**
     * @var string
     *
     * @ORM\Column(name="lmscontexttype", type="string", length=255, nullable=true)
     */
    private $lmscontexttype; //LMS context data: coursecategory|course|group
	
/**
   * @var integer
   *
   * @ORM\Column(name="lmscontextid", type="bigint", nullable=true)
     */
    private $lmscontextid; //id of instance row in lms context
   
 /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='course'; //course | event
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="adminformattemptid", type="bigint", nullable=true)
	 */
	private $adminformattemptid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProject
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProject")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="projectid", referencedColumnName="id")
	 * })
	 */
	private $projectid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProjectTypePartnership
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProjectTypePartnership")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="typepartnershipid", referencedColumnName="id")
	 * })
	 */
	private $typepartnershipid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProjectTypeDev
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProjectTypeDev")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="typedevid", referencedColumnName="id")
	 * })
	 */
	private $typedevid;
	
	/**
	 *
	 * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="authordevid", referencedColumnName="id")
	 * })
	 */
	private $authordevid;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="devinfo", type="text", nullable=true)
     */
    private $devinfo;
	/**
     * @var integer
     *
     * @ORM\Column(name="costdev", type="float", precision=10, scale=0, nullable=true)
     */
    private $costdev;
	 /**
     * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterpriseDepartment
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterpriseDepartment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="departmentid", referencedColumnName="id")
     * })
     */

    private $departmentid; 
	
	/**
     * @var string
     *
     * @ORM\Column(name="usertarget", type="text", nullable=true)
     */
    private $usertarget;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="usertargetestimatedamout", type="bigint", nullable=true)
     */
    private $usertargetestimatedamout;
	/**
     * @var string
     *
     * @ORM\Column(name="objective", type="text", nullable=true)
     */
    private $objective;
	
	/**
     * @var string
     *
     * @ORM\Column(name="competence", type="text", nullable=true)
     */
    private $competence;

    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey; 

  /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;

    
	/**
     * @var boolean
     *
     * @ORM\Column(name="haspartnner", type="integer", nullable=true)
     */
    private $haspartnner=0; //delete it
	
	
	/**
     * @var boolean
     *
     * @ORM\Column(name="requireenrolpayment", type="integer", nullable=true)
     */
    private $requireenrolpayment=0;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="enablecertificate", type="integer", nullable=true)
     */
    private $enablecertificate;
	
		/**
     * @var integer
     *
     * @ORM\Column(name="certificatetempleid", type="bigint", nullable=true)
     */
    private $certificatetempleid;
	
	/**
     * @var string
     *
     * @ORM\Column(name="certificatecontent", type="text", nullable=true)
     */
    private $certificatecontent;


/**
     * @var string
     *
     * @ORM\Column(name="certificatetimestart",  type="datetime", nullable=true)
     */
    private $certificatetimestart;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="certificatetimeend",  type="datetime", nullable=true)
     */
    private $certificatetimeend;
	
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateuser", type="integer", nullable=true)
     */
    private $certificateupdateuser;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateenrol", type="integer", nullable=true)
     */
    private $certificateupdateenrol;
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateobjectinfo", type="integer", nullable=true)
     */
    private $certificateupdateobjectinfo;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateobjectdata", type="integer", nullable=true)
     */
    private $certificateupdateobjectdata;
    /**
     * @var boolean
     *
     * @ORM\Column(name="productintegration", type="integer", nullable=true)
     */
    private $productintegration=0;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="productid", type="bigint", nullable=true)
     */
    private $productid;
    
     /**
     * @var boolean
     *
     * @ORM\Column(name="costcenterintegration", type="integer", nullable=true)
     */
    private $costcenterintegration=0;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="costcenterid", type="bigint", nullable=true)
     */
    private $costcenterid;
    
      /**
     * @var boolean
     *
     * @ORM\Column(name="projectintegration", type="integer", nullable=true)
     */
    private $projectintegration=0;
    
   

          /**
     * @var boolean
     *
     * @ORM\Column(name="contracttemplateintegration", type="integer", nullable=true)
     */
    private $contracttemplateintegration=0;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="contracttemplateid", type="bigint", nullable=true)
     */
    private $contracttemplateid;
    
         /**
     * @var string
     *
     * @ORM\Column(name="contracttemplateconfig", type="text", nullable=true)
     */
    private $contracttemplateconfig;
    
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timeupdate", type="datetime", nullable=true)
	 */
	private $timeupdate;
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="enrolrequesttimestart", type="datetime", nullable=true)
     */
    private $enrolrequesttimestart;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="enrolrequesttimeend", type="datetime", nullable=true)
     */
    private $enrolrequesttimeend;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="lmsaccesstimestart", type="datetime", nullable=true)
     */
    private $lmsaccesstimestart;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="lmsaccesstimeend", type="datetime", nullable=true)
     */
    private $lmsaccesstimeend;

      /**
     * @var integer
     *
     * @ORM\Column(name="customint1", type="bigint",  nullable=true)
     */
    private $customint1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint2", type="bigint",  nullable=true)
     */
    private $customint2;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint3", type="bigint",  nullable=true)
     */
    private $customint3;
	
	    /**
     * @var integer
     *
     * @ORM\Column(name="customint4", type="bigint",  nullable=true)
     */
    private $customint4;
     /**
     * @var float
     *
     * @ORM\Column(name="customdec1", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec1;
    
    /**
     * @var float
     *
     * @ORM\Column(name="customdec2", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec2;
    /**
     * @var float
     *
     * @ORM\Column(name="customdec3", type="float",  precision=10, scale=0, nullable=true)
     */
    private $customdec3;
     /**
     * @var string
     *
     * @ORM\Column(name="customchar1", type="string", length=255, nullable=true)
     */
    private $customchar1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="customchar2", type="string", length=255, nullable=true)
     */
    private $customchar2;
    
     
    
    /**
     * @var string
     *
     * @ORM\Column(name="customchar3", type="string", length=255, nullable=true)
     */
    private $customchar3;
    
	 /**
     * @var string
     *
     * @ORM\Column(name="customchar4", type="string", length=255, nullable=true)
     */
    private $customchar4;
    
    /**
     * @var string
     *
     * @ORM\Column(name="customtext1", type="text", nullable=true)
     */
    private $customtext1;
    
     /**
     * @var string
     *
     * @ORM\Column(name="customtext2", type="text", nullable=true)
     */
    private $customtext2;
    
     /**
     * @var string
     *
     * @ORM\Column(name="customtext3", type="text", nullable=true)
     */
    private $customtext3;
   
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

	/**
     * @var string
     *
     * @ORM\Column(name="defaultimage", type="string", length=255, nullable=true)
     */
    private $defaultimage;
     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getCategoryid() {
        return $this->categoryid;
    }

    public function getCurriculumid() {
        return $this->curriculumid;
    }

    public function getStatusid() {
        return $this->statusid;
    }

    public function getTypeid() {
        return $this->typeid;
    }

    public function getName() {
        return $this->name;
    }

    public function getShortname() {
        return $this->shortname;
    }

    public function getTimestart() {
	  return $this->timestart;
    }

    public function getTimeend() {
        return $this->timeend;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setCategoryid(AmsOfferCategory $categoryid) {
        $this->categoryid = $categoryid;
    }

    public function setCurriculumid(\Badiu\Ams\CurriculumBundle\Entity\AmsCurriculum $curriculumid) {
        $this->curriculumid = $curriculumid;
    }

    public function setStatusid(AmsOfferStatus $statusid) {
        $this->statusid = $statusid;
    }

    public function setTypeid(AmsOfferType $typeid) {
        $this->typeid = $typeid;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    public function setTimestart($timestart) {
        $this->timestart = $timestart;
    }

    public function setTimeend($timeend) {
        $this->timeend = $timeend;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


    /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }

         /**
     * @return string
     */
    public function getModulekey()
    {
        return $this->modulekey;
    }

    /**
     * @param string $modulekey
     */
    public function setModulekey($modulekey)
    {
        $this->modulekey = $modulekey;
    }

     /**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string $moduleinstance
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }
	
	  public function setHaspartnner($haspartnner) {
        $this->haspartnner = $haspartnner;
    }
  public function getHaspartnner() {
        return $this->haspartnner;
    }


	  public function setRequireenrolpayment($requireenrolpayment) {
        $this->requireenrolpayment = $requireenrolpayment;
    }
  public function getRequireenrolpayment() {
        return $this->requireenrolpayment;
    }
	


	
    function getMaxenrol() {
        return $this->maxenrol;
    }

    function setMaxenrol($maxenrol) {
        $this->maxenrol = $maxenrol;
    }

    function getProductintegration() {
        return $this->productintegration;
    }

    function setProductintegration($productintegration) {
        $this->productintegration = $productintegration;
    }

    function getProductid() {
        return $this->productid;
    }

    function setProductid($productid) {
        $this->productid = $productid;
    }

    function getEnterpriseid() {
        return $this->enterpriseid;
    }

    function setEnterpriseid(\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise $enterpriseid) {
        $this->enterpriseid = $enterpriseid;
    }

    function getHedificeid() {
        return $this->hedificeid;
    }

    function getCustomint1() {
        return $this->customint1;
    }

    function getCustomint2() {
        return $this->customint2;
    }

    function getCustomint3() {
        return $this->customint3;
    }

    function getCustomint4() {
        return $this->customint4;
    }

    function getCustomdec1() {
        return $this->customdec1;
    }

    function getCustomdec2() {
        return $this->customdec2;
    }

    function getCustomdec3() {
        return $this->customdec3;
    }

    function getCustomchar1() {
        return $this->customchar1;
    }

    function getCustomchar2() {
        return $this->customchar2;
    }

    function getCustomchar3() {
        return $this->customchar3;
    }

    function getCustomchar4() {
        return $this->customchar4;
    }

    function getCustomtext1() {
        return $this->customtext1;
    }

    function getCustomtext2() {
        return $this->customtext2;
    }

    function getCustomtext3() {
        return $this->customtext3;
    }

    function setHedificeid(\Badiu\Util\HousingBundle\Entity\UtilHousingEdifice $hedificeid) {
        $this->hedificeid = $hedificeid;
    }

    function setCustomint1($customint1) {
        $this->customint1 = $customint1;
    }

    function setCustomint2($customint2) {
        $this->customint2 = $customint2;
    }

    function setCustomint3($customint3) {
        $this->customint3 = $customint3;
    }

    function setCustomint4($customint4) {
        $this->customint4 = $customint4;
    }

    function setCustomdec1($customdec1) {
        $this->customdec1 = $customdec1;
    }

    function setCustomdec2($customdec2) {
        $this->customdec2 = $customdec2;
    }

    function setCustomdec3($customdec3) {
        $this->customdec3 = $customdec3;
    }

    function setCustomchar1($customchar1) {
        $this->customchar1 = $customchar1;
    }

    function setCustomchar2($customchar2) {
        $this->customchar2 = $customchar2;
    }

    function setCustomchar3($customchar3) {
        $this->customchar3 = $customchar3;
    }

    function setCustomchar4($customchar4) {
        $this->customchar4 = $customchar4;
    }

    function setCustomtext1($customtext1) {
        $this->customtext1 = $customtext1;
    }

    function setCustomtext2($customtext2) {
        $this->customtext2 = $customtext2;
    }

    function setCustomtext3($customtext3) {
        $this->customtext3 = $customtext3;
    }

    function getSserviceid() {
        return $this->sserviceid;
    }

    function getLmssynctype() {
        return $this->lmssynctype;
    }

    function getLmscontexttypeparent() {
        return $this->lmscontexttypeparent;
    }

    function getLmscontextidparent() {
        return $this->lmscontextidparent;
    }

    function getLmscontexttype() {
        return $this->lmscontexttype;
    }

    function setSserviceid(\Badiu\Admin\ServerBundle\Entity\AdminServerService $sserviceid) {
        $this->sserviceid = $sserviceid;
    }

    function setLmssynctype($lmssynctype) {
        $this->lmssynctype = $lmssynctype;
    }

    function setLmscontexttypeparent($lmscontexttypeparent) {
        $this->lmscontexttypeparent = $lmscontexttypeparent;
    }

    function setLmscontextidparent($lmscontextidparent) {
        $this->lmscontextidparent = $lmscontextidparent;
    }

    function setLmscontexttype($lmscontexttype) {
        $this->lmscontexttype = $lmscontexttype;
    }

    function getLmscontextid() {
        return $this->lmscontextid;
    }

    function setLmscontextid($lmscontextid) {
        $this->lmscontextid = $lmscontextid;
    }

    function getLevelid() {
        return $this->levelid;
    }

    function setLevelid(AmsOfferLevel $levelid) {
        $this->levelid = $levelid;
    }

    function getCostcenterintegration() {
        return $this->costcenterintegration;
    }

    function getCostcenterid() {
        return $this->costcenterid;
    }

    function setCostcenterintegration($costcenterintegration) {
        $this->costcenterintegration = $costcenterintegration;
    }

    function setCostcenterid($costcenterid) {
        $this->costcenterid = $costcenterid;
    }

    function getProjectintegration() {
        return $this->projectintegration;
    }

    

    function setProjectintegration($projectintegration) {
        $this->projectintegration = $projectintegration;
    }

    function getContracttemplateintegration() {
        return $this->contracttemplateintegration;
    }

    function getContracttemplateid() {
        return $this->contracttemplateid;
    }

    function setContracttemplateintegration($contracttemplateintegration) {
        $this->contracttemplateintegration = $contracttemplateintegration;
    }

    function setContracttemplateid($contracttemplateid) {
        $this->contracttemplateid = $contracttemplateid;
    }

    function getContracttemplateconfig() {
        return $this->contracttemplateconfig;
    }

    function setContracttemplateconfig($contracttemplateconfig) {
        $this->contracttemplateconfig = $contracttemplateconfig;
    }


    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function getCourseid() {
        return $this->courseid;
    }

   
    function setCourseid($courseid) {
        $this->courseid = $courseid;
    }
 
  
	
	function getEnroltype() {
        return $this->enroltype;
    }

    function setEnroltype($enroltype) {
        $this->enroltype = $enroltype;
    }
	
	  function getEnroldatetype() {
        return $this->enroldatetype;
    }
    function setEnroldatetype($enroldatetype) {
        $this->enroldatetype = $enroldatetype;
    }


	  function getEnroldatedynamic() {
        return $this->enroldatedynamic;
    }
    function setEnroldatedynamic($enroldatedynamic) {
        $this->enroldatedynamic = $enroldatedynamic;
    }	
	
		  function getEnrolreplicate() {
        return $this->enrolreplicate;
    }
    function setEnrolreplicate($enrolreplicate) {
        $this->enrolreplicate = $enrolreplicate;
    }
	
		  function getEnrolreplicateupadate() {
        return $this->enrolreplicateupadate;
    }
    function setEnrolreplicateupadate($enrolreplicateupadate) {
        $this->enrolreplicateupadate = $enrolreplicateupadate;
    }
	
	public function getLabelinfo() {
        return $this->labelinfo;
    }
	
	public function setLabelinfo($labelinfo) {
        $this->labelinfo = $labelinfo;
    }
	
	public function getTypemanager() {
        return $this->typemanager;
    }
	
	public function setTypemanager($typemanager) {
        $this->typemanager = $typemanager;
    }
	
	public function getPeriodday() {
        return $this->periodday;
    }
	
	public function setPeriodday($periodday) {
        $this->periodday = $periodday;
    }
	
	   function getDefaultimage() {
        return $this->defaultimage;
    }

    function setDefaultimage($defaultimage) {
        $this->defaultimage = $defaultimage;
    }
	
		function getCertificatetempleid() {
        return $this->certificatetempleid;
    }

   
    function setCertificatetempleid($certificatetempleid) {
        $this->certificatetempleid = $certificatetempleid;
    }
	
	 public function  setEnablecertificate($enablecertificate) {
        $this->enablecertificate=$enablecertificate;
    }
	
	 public function getEnablecertificate() {
        return $this->enablecertificate;
    }
	
	 public function getCertificatecontent() {
	    return $this->certificatecontent;
    }

   
    function setCertificatecontent($certificatecontent) {
        $this->certificatecontent = $certificatecontent;
    }
	
	 public function getCertificatetimestart() {
	    return $this->certificatetimestart;
    }

   
    function setCertificatetimestart($certificatetimestart) {
        $this->certificatetimestart = $certificatetimestart;
    }
	
	
	
	 public function getCertificatetimeend() {
	    return $this->certificatetimeend;
    }

   
    function setCertificatetimeend($certificatetimeend) {
        $this->certificatetimeend = $certificatetimeend;
    }
	
	
    function getLmsaccesstimestart() {
        return $this->lmsaccesstimestart;
    }

    function getLmsaccesstimeend() {
        return $this->lmsaccesstimeend;
    }


 function setLmsaccesstimestart(\DateTime $lmsaccesstimestart) {
        $this->lmsaccesstimestart = $lmsaccesstimestart;
    }

    function setLmsaccesstimeend(\DateTime $lmsaccesstimeend) {
        $this->lmsaccesstimeend = $lmsaccesstimeend;
    }
	
	
	
	
    function getEnrolrequesttimestart() {
        return $this->enrolrequesttimestart;
    }

    function getEnrolrequesttimeend() {
        return $this->enrolrequesttimeend;
    }


 function setEnrolrequesttimestart(\DateTime $enrolrequesttimestart) {
        $this->enrolrequesttimestart = $enrolrequesttimestart;
    }

    function setEnrolrequesttimeend(\DateTime $enrolrequesttimeend) {
        $this->enrolrequesttimeend = $enrolrequesttimeend;
    }
	
	
	public function getProjectid() {
		return $this->projectid;
	}
	
	public function setProjectid($projectid) {
		$this->projectid = $projectid;
	}
	


	public function setDepartmentid($departmentid)
    {
        $this->departmentid = $departmentid;
	}
	
	public function getDepartmentid()
    {
        return $this->departmentid;
    }

public function setUsertarget($usertarget)
    {
        $this->usertarget = $usertarget;
	}
	
	public function getUsertarget()
    {
        return $this->usertarget;
    }

public function setAdminformattemptid($adminformattemptid)
    {
        $this->adminformattemptid = $adminformattemptid;
	}
	
	public function getAdminformattemptid()
    {
        return $this->adminformattemptid;
    }	
public function setTypepartnershipid($typepartnershipid)
    {
        $this->typepartnershipid = $typepartnershipid;
	}
	
	public function getTypepartnershipid()
    {
        return $this->typepartnershipid;
    }	

public function setTypedevid($typedevid)
    {
        $this->typedevid = $typedevid;
	}
	
	public function getTypedevid()
    {
        return $this->typedevid;
    }	

public function setObjective($objective)
    {
        $this->objective = $objective;
	}
	
	public function getObjective()
    {
        return $this->objective;
    }



	public function setTimeupdate($timeupdate)
    {
        $this->timeupdate = $timeupdate;
	}
	
	public function getTimeupdate()
    {
        return $this->timeupdate;
    }


public function setAuthordevid($authordevid)
    {
        $this->authordevid = $authordevid;
	}
	
	public function getAuthordevid()
    {
        return $this->authordevid;
    }		
	
	
public function setCostdev($costdev)
    {
        $this->costdev = $costdev;
	}
	
	public function getCostdev()
    {
        return $this->costdev;
    }	


public function setDevinfo($devinfo)
    {
        $this->devinfo = $devinfo;
	}
	
	public function getDevinfo()
    {
        return $this->devinfo;
    }	


function getCompetence() {
            return $this->competence;
        }
	function setCompetence($competence) {
            $this->competence = $competence;
        }	
		
		
        function setCertificateupdateuser($certificateupdateuser) {
            $this->certificateupdateuser = $certificateupdateuser;
        }

        function getCertificateupdateuser() {
            return $this->certificateupdateuser;
        }

	function setCertificateupdateobjectinfo($certificateupdateobjectinfo) {
            $this->certificateupdateobjectinfo = $certificateupdateobjectinfo;
        }

        function getCertificateupdateobjectinfo() {
            return $this->certificateupdateobjectinfo;
        }	

function setCertificateupdateobjectdata($certificateupdateobjectdata) {
            $this->certificateupdateobjectdata = $certificateupdateobjectdata;
        }

        function getCertificateupdateobjectdata() {
            return $this->certificateupdateobjectdata;
        }

function setCertificateupdateenrol($certificateupdateenrol) {
            $this->certificateupdateenrol = $certificateupdateenrol;
        }

        function getCertificateupdateenrol() {
            return $this->certificateupdateenrol;
        }	
		public function setUsertargetestimatedamout($usertargetestimatedamout)
    {
        $this->usertargetestimatedamout = $usertargetestimatedamout;
	}
	
	public function getUsertargetestimatedamout()
    {
        return $this->usertargetestimatedamout;
    }
}
