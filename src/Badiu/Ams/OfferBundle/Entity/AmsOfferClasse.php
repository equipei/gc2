<?php

namespace Badiu\Ams\OfferBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsOfferClasse
 *  
 * @ORM\Table(name="ams_offer_classe", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_offer_classe_idnumber_uix", columns={"entity", "idnumber"}),
 *      @ORM\UniqueConstraint(name="ams_offer_classe_shortname_uix", columns={"entity", "shortname"})},
 *       indexes={@ORM\Index(name="ams_offer_classe_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_offer_classe_odisciplineid_ix", columns={"odisciplineid"}), 
 *              @ORM\Index(name="ams_offer_classe_name_ix", columns={"name"}), 
 *              @ORM\Index(name="ams_offer_classe_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="ams_offer_classe_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="ams_offer_classe_ctype_ix", columns={"ctype"}),
 *              @ORM\Index(name="ams_offer_classe_dtype_ix", columns={"dtype"}), 
 *              @ORM\Index(name="ams_offer_classe_typemanagerid_ix", columns={"typemanagerid"}),
 *              @ORM\Index(name="ams_offer_classe_typeaccessid_ix", columns={"typeaccessid"}),
 *              @ORM\Index(name="ams_offer_classe_ctemplate_ix", columns={"ctemplate"}), 
 *              @ORM\Index(name="ams_offer_classe_marker_ix", columns={"marker"}), 
 *              @ORM\Index(name="ams_offer_classe_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsOfferClasse
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var AmsOfferDiscipline
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferDiscipline")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="odisciplineid", referencedColumnName="id")
     * })
     */
    private $odisciplineid;

    
     /**
     * @var AmsOfferClasseStatus
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferClasseStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;
	
	/**
     * @var AmsOfferTypeManager
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferTypeManager")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typemanagerid", referencedColumnName="id")
     * })
     */
    private $typemanagerid;

    	/**
     * @var AmsOfferTypeAccess
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferTypeAccess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typeaccessid", referencedColumnName="id")
     * })
     */
    private $typeaccessid;
	
	
	 /**
     * @var AmsOfferPeriodDay
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferPeriodDay")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="periodday", referencedColumnName="id")
     * })
     */
    private $periodday;
	
	
	 /**
     * @var AmsOfferType
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typeid", referencedColumnName="id")
     * })
     */
    private $typeid;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="labelinfo", type="string", length=255, nullable=true)
     */
    private $labelinfo;
      /**
     * @var integer
     *
     * @ORM\Column(name="maxenrol", type="bigint", nullable=true)
     */
    private $maxenrol;
	

/**
     * @var string
     *
     * @ORM\Column(name="enroldatetype", type="string", length=50, nullable=true)
     */
    private $enroldatetype; //periodofparent | perioddynamic
	
	/**
     * @var string
     *
     * @ORM\Column(name="enroldatedynamic", type="string", length=255, nullable=true)
     */
    private $enroldatedynamic; 
	
	/**
     * @var string
     *
     * @ORM\Column(name="enrolreplicate", type="string", length=255, nullable=true)
     */
    private $enrolreplicate;
	
	/**
     * @var string
     *
     * @ORM\Column(name="enrolreplicateupadate", type="string", length=255, nullable=true)
     */
    private $enrolreplicateupadate;
	
		/**
     * @var string
     *
     * @ORM\Column(name="enroltype", type="string", length=50, nullable=true)
     */
    private $enroltype; //manual | selectprocess
	
	
    /**
     * @var string
     *
     * @ORM\Column(name="ctype", type="string", length=50, nullable=false)
     */
    private $ctype='single'; // single | shared review srared and create table control
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    
    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;

     /**
     * @var boolean
     *
     * @ORM\Column(name="ctemplate", type="integer", nullable=true)
     */
    private $ctemplate;
    
      /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=255, nullable=true)
     */
    private $dtype;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="enablecertificate", type="integer", nullable=true)
     */
    private $enablecertificate;
	
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificatetempleid", type="bigint", nullable=true)
     */
    private $certificatetempleid;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="certificatecontent", type="text", nullable=true)
     */
    private $certificatecontent;
    
	/**
     * @var string
     *
     * @ORM\Column(name="certificatetimestart",  type="datetime", nullable=true)
     */
    private $certificatetimestart;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="certificatetimeend",  type="datetime", nullable=true)
     */
    private $certificatetimeend;
	
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateuser", type="integer", nullable=true)
     */
    private $certificateupdateuser;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateenrol", type="integer", nullable=true)
     */
    private $certificateupdateenrol;
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateobjectinfo", type="integer", nullable=true)
     */
    private $certificateupdateobjectinfo;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateobjectdata", type="integer", nullable=true)
     */
    private $certificateupdateobjectdata;
       /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;

	

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */

    private $timeend;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="enrolrequesttimestart", type="datetime", nullable=true)
     */
    private $enrolrequesttimestart;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="enrolrequesttimeend", type="datetime", nullable=true)
     */
    private $enrolrequesttimeend;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="lmsaccesstimestart", type="datetime", nullable=true)
     */
    private $lmsaccesstimestart;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="lmsaccesstimeend", type="datetime", nullable=true)
     */
    private $lmsaccesstimeend;
	
	/**
     * @var string
     *
     * @ORM\Column(name="summary", type="text", nullable=true)
     */
    private $summary;
	/**
     * @var string
     *
     * @ORM\Column(name="teachingplan", type="text", nullable=true)
     */
    private $teachingplan;
   /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;

    
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

      /**
     * @var string
     *
     * @ORM\Column(name="marker", type="string", length=255, nullable=true)
     */
    private $marker;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

/**
     * @var string
     *
     * @ORM\Column(name="defaultimage", type="string", length=255, nullable=true)
     */
    private $defaultimage;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="adminformattemptid", type="bigint", nullable=true)
	 */
	private $adminformattemptid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProject
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProject")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="projectid", referencedColumnName="id")
	 * })
	 */
	private $projectid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProjectTypePartnership
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProjectTypePartnership")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="typepartnershipid", referencedColumnName="id")
	 * })
	 */
	private $typepartnershipid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProjectTypeDev
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProjectTypeDev")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="typedevid", referencedColumnName="id")
	 * })
	 */
	private $typedevid;
	
	/**
	 *
	 * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="authordevid", referencedColumnName="id")
	 * })
	 */
	private $authordevid;
	
	
	/**
     * @var integer
     *
     * @ORM\Column(name="costdev", type="float", precision=10, scale=0, nullable=true)
     */
    private $costdev;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="devinfo", type="text", nullable=true)
     */
    private $devinfo;
	/**
	 *
	 * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="enterpriseid", referencedColumnName="id")
	 * })
	 */
	private $enterpriseid;
	
	 /**
     * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterpriseDepartment
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterpriseDepartment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="departmentid", referencedColumnName="id")
     * })
     */

    private $departmentid; 
	
	/**
     * @var string
     *
     * @ORM\Column(name="usertarget", type="text", nullable=true)
     */
    private $usertarget;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="usertargetestimatedamout", type="bigint", nullable=true)
     */
    private $usertargetestimatedamout;
	/**
     * @var string
     *
     * @ORM\Column(name="objective", type="text", nullable=true)
     */
    private $objective;
	
	/**
     * @var string
     *
     * @ORM\Column(name="competence", type="text", nullable=true)
     */
    private $competence;
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timeupdate", type="datetime", nullable=true)
	 */
	private $timeupdate;
     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    
         /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;


   
    
    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getOdisciplineid() {
        return $this->odisciplineid;
    }


    public function getName() {
        return $this->name;
    }

    public function getShortname() {
        return $this->shortname;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setOdisciplineid(AmsOfferDiscipline $odisciplineid) {
        $this->odisciplineid = $odisciplineid;
    }



    public function setName($name) {
        $this->name = $name;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    

    /**
     * @return string
     */
    public function getCtype()
    {
        return $this->ctype;
    }

    /**
     * @param string $ctype
     */
    public function setCtype($ctype)
    {
        $this->ctype = $ctype;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    function getMaxenrol() {
        return $this->maxenrol;
    }

    function setMaxenrol($maxenrol) {
        $this->maxenrol = $maxenrol;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function getStatusid() {
        return $this->statusid;
    }

    function setStatusid(AmsOfferClasseStatus $statusid) {
        $this->statusid = $statusid;
    }

    function getCtemplate() {
        return $this->ctemplate;
    }

    function getDtype() {
        return $this->dtype;
    }

    function setCtemplate($ctemplate) {
        $this->ctemplate = $ctemplate;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function getMarker() {
        return $this->marker;
    }
    function setMarker($marker) {
        $this->marker = $marker;
    }

	

public function getSummary() {
        return $this->summary;
    }

    public function setSummary($summary) {
        $this->summary = $summary;
    }
	 public function getTeachingplan() {
        return $this->teachingplan;
    }
	
	
    public function setTeachingplan($teachingplan) {
        $this->teachingplan = $teachingplan;
    }
	
	function getEnroltype() {
        return $this->enroltype;
    }

    function setEnroltype($enroltype) {
        $this->enroltype = $enroltype;
    }
	
	  function getEnroldatetype() {
        return $this->enroldatetype;
    }
    function setEnroldatetype($enroldatetype) {
        $this->enroldatetype = $enroldatetype;
    }


	  function getEnroldatedynamic() {
        return $this->enroldatedynamic;
    }
    function setEnroldatedynamic($enroldatedynamic) {
        $this->enroldatedynamic = $enroldatedynamic;
    }	
	
		  function getEnrolreplicate() {
        return $this->enrolreplicate;
    }
    function setEnrolreplicate($enrolreplicate) {
        $this->enrolreplicate = $enrolreplicate;
    }
	
		  function getEnrolreplicateupadate() {
        return $this->enrolreplicateupadate;
    }
    function setEnrolreplicateupadate($enrolreplicateupadate) {
        $this->enrolreplicateupadate = $enrolreplicateupadate;
    }
	
	
	public function getTypeid() {
        return $this->typeid;
    }
	
	public function setTypeid($typeid) {
        $this->typeid = $typeid;
    }
	
	public function getPeriodday() {
        return $this->periodday;
    }
	
	public function setPeriodday($periodday) {
        $this->periodday = $periodday;
    }
	
	public function getLabelinfo() {
        return $this->labelinfo;
    }
	
	public function setLabelinfo($labelinfo) {
        $this->labelinfo = $labelinfo;
    }
	
	public function getTypeaccessid() {
        return $this->typeaccessid;
    }
	
	public function setTypeaccessid($typeaccessid) {
        $this->typeaccessid = $typeaccessid;
    }

    public function getTypemanagerid() {
        return $this->typemanagerid;
    }
	
	public function setTypemanagerid($typemanagerid) {
        $this->typemanagerid = $typemanagerid;
    }
	
	
	   function getDefaultimage() {
        return $this->defaultimage;
    }

    function setDefaultimage($defaultimage) {
        $this->defaultimage = $defaultimage;
    }
	
	function getCertificatetempleid() {
        return $this->certificatetempleid;
    }

   
    function setCertificatetempleid($certificatetempleid) {
        $this->certificatetempleid = $certificatetempleid;
    }
 
   public function  setEnablecertificate($enablecertificate) {
        $this->enablecertificate=$enablecertificate;
    }
	
	 public function getEnablecertificate() {
        return $this->enablecertificate;
    }
	
	 public function getCertificatecontent() {
	    return $this->certificatecontent;
    }

   
    function setCertificatecontent($certificatecontent) {
        $this->certificatecontent = $certificatecontent;
    }
	
	
	 public function getCertificatetimestart() {
	    return $this->certificatetimestart;
    }

   
    function setCertificatetimestart($certificatetimestart) {
        $this->certificatetimestart = $certificatetimestart;
    }
	
	
	
	 public function getCertificatetimeend() {
	    return $this->certificatetimeend;
    }

   
    function setCertificatetimeend($certificatetimeend) {
        $this->certificatetimeend = $certificatetimeend;
    }
	
	
    function getLmsaccesstimestart() {
        return $this->lmsaccesstimestart;
    }

    function getLmsaccesstimeend() {
        return $this->lmsaccesstimeend;
    }


 function setLmsaccesstimestart(\DateTime $lmsaccesstimestart) {
        $this->lmsaccesstimestart = $lmsaccesstimestart;
    }

    function setLmsaccesstimeend(\DateTime $lmsaccesstimeend) {
        $this->lmsaccesstimeend = $lmsaccesstimeend;
    }
	
	
	
	
    function getEnrolrequesttimestart() {
        return $this->enrolrequesttimestart;
    }

    function getEnrolrequesttimeend() {
        return $this->enrolrequesttimeend;
    }


 function setEnrolrequesttimestart(\DateTime $enrolrequesttimestart) {
        $this->enrolrequesttimestart = $enrolrequesttimestart;
    }

    function setEnrolrequesttimeend(\DateTime $enrolrequesttimeend) {
        $this->enrolrequesttimeend = $enrolrequesttimeend;
    }
	
	
	public function getProjectid() {
		return $this->projectid;
	}
	
	public function setProjectid($projectid) {
		$this->projectid = $projectid;
	}
	
	function getEnterpriseid() {
            return $this->enterpriseid;
        }
	function setEnterpriseid($enterpriseid) {
            $this->enterpriseid = $enterpriseid;
        }

	public function setDepartmentid($departmentid)
    {
        $this->departmentid = $departmentid;
	}
	
	public function getDepartmentid()
    {
        return $this->departmentid;
    }

public function setUsertarget($usertarget)
    {
        $this->usertarget = $usertarget;
	}
	
	public function getUsertarget()
    {
        return $this->usertarget;
    }

public function setAdminformattemptid($adminformattemptid)
    {
        $this->adminformattemptid = $adminformattemptid;
	}
	
	public function getAdminformattemptid()
    {
        return $this->adminformattemptid;
    }	
public function setTypepartnershipid($typepartnershipid)
    {
        $this->typepartnershipid = $typepartnershipid;
	}
	
	public function getTypepartnershipid()
    {
        return $this->typepartnershipid;
    }	

public function setTypedevid($typedevid)
    {
        $this->typedevid = $typedevid;
	}
	
	public function getTypedevid()
    {
        return $this->typedevid;
    }	

public function setObjective($objective)
    {
        $this->objective = $objective;
	}
	
	public function getObjective()
    {
        return $this->objective;
    }


	public function setTimeupdate($timeupdate)
    {
        $this->timeupdate = $timeupdate;
	}
	
	public function getTimeupdate()
    {
        return $this->timeupdate;
    }


public function setAuthordevid($authordevid)
    {
        $this->authordevid = $authordevid;
	}
	
	public function getAuthordevid()
    {
        return $this->authordevid;
    }		
	
	
public function setCostdev($costdev)
    {
        $this->costdev = $costdev;
	}
	
	public function getCostdev()
    {
        return $this->costdev;
    }	


public function setDevinfo($devinfo)
    {
        $this->devinfo = $devinfo;
	}
	
	public function getDevinfo()
    {
        return $this->devinfo;
    }	


function getCompetence() {
            return $this->competence;
        }
	function setCompetence($competence) {
            $this->competence = $competence;
        }	


        function setCertificateupdateuser($certificateupdateuser) {
            $this->certificateupdateuser = $certificateupdateuser;
        }

        function getCertificateupdateuser() {
            return $this->certificateupdateuser;
        }

	function setCertificateupdateobjectinfo($certificateupdateobjectinfo) {
            $this->certificateupdateobjectinfo = $certificateupdateobjectinfo;
        }

        function getCertificateupdateobjectinfo() {
            return $this->certificateupdateobjectinfo;
        }	

function setCertificateupdateobjectdata($certificateupdateobjectdata) {
            $this->certificateupdateobjectdata = $certificateupdateobjectdata;
        }

        function getCertificateupdateobjectdata() {
            return $this->certificateupdateobjectdata;
        }	
		function setCertificateupdateenrol($certificateupdateenrol) {
            $this->certificateupdateenrol = $certificateupdateenrol;
        }

        function getCertificateupdateenrol() {
            return $this->certificateupdateenrol;
        }
		
public function setUsertargetestimatedamout($usertargetestimatedamout)
    {
        $this->usertargetestimatedamout = $usertargetestimatedamout;
	}
	
	public function getUsertargetestimatedamout()
    {
        return $this->usertargetestimatedamout;
    }		
}
