<?php

namespace Badiu\Ams\OfferBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsOfferClasseExecuted
 *  
 * @ORM\Table(name="ams_offer_classe_planned_detail", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_offer_classe_planned_detail_idnumber_uix", columns={"entity", "idnumber"})},
 *      indexes={@ORM\Index(name="ams_offer_classe_planned_detail_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="ams_offer_classe_planned_detail_plannedid_ix", columns={"plannedid"}),
 *              @ORM\Index(name="ams_offer_classe_planned_detail_hroomid_ix", columns={"hroomid"}),
 *              @ORM\Index(name="ams_offer_classe_planned_detail_timestart_ix", columns={"timestart"}),
 *              @ORM\Index(name="ams_offer_classe_planned_detail_timeend_ix", columns={"timeend"}),
 *              @ORM\Index(name="ams_offer_classe_planned_detail_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="ams_offer_classe_planned_detail_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsOfferClassePlannedDetail
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var AmsOfferClassePlanned
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferClassePlanned")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plannedid", referencedColumnName="id")
     * })
     */
    private $plannedid;
    

  	/**
     * @var \Badiu\Util\HousingBundle\Entity\UtilHousingRoom
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Util\HousingBundle\Entity\UtilHousingRoom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hroomid", referencedColumnName="id")
     * })
     */
    
    private $hroomid;

        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=false)
     */
    private $timeend;
        
     /**
     * @var integer
     *
     * @ORM\Column(name="hourduration", type="integer", nullable=false)
     */
    private $hourduration;

    
     /**
     * @var string
     *
     * @ORM\Column(name="contenttaught", type="text", nullable=true)
     */
    private $contenttaught;
    
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getPlannedid() {
        return $this->plannedid;
    }

    function getHroomid() {
        return $this->hroomid;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getHourduration() {
        return $this->hourduration;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setPlannedid(AmsOfferClassePlanned $plannedid) {
        $this->plannedid = $plannedid;
    }

    function setHroomid(\Badiu\Util\HousingBundle\Entity\UtilHousingRoom $hroomid) {
        $this->hroomid = $hroomid;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setHourduration($hourduration) {
        $this->hourduration = $hourduration;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


}
