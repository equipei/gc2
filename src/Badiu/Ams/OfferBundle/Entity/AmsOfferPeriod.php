<?php

namespace Badiu\Ams\OfferBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsOfferPeriod
 *
 * @ORM\Table(name="ams_offer_period", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_offer_period_name_uix", columns={"entity", "offerid","section"}),
 *      @ORM\UniqueConstraint(name="ams_offer_period_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="ams_offer_period_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_offer_period_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_offer_period_name_ix", columns={"name"}),
 *              @ORM\Index(name="ams_offer_period_offerid_ix", columns={"offerid"}),
 *              @ORM\Index(name="ams_offer_period_section_ix", columns={"section"}),
 *              @ORM\Index(name="ams_offer_period_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="ams_offer_period_deleted_ix", columns={"deleted"}), 
 *              @ORM\Index(name="ams_offer_period_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsOfferPeriod
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;


 /**
     * @var AmsOffer
     *
     * @ORM\ManyToOne(targetEntity="AmsOffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offerid", referencedColumnName="id")
     * })
     */
    private $offerid;

     /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
    
      /**
     * @var integer
     *
     * @ORM\Column(name="section", type="integer", nullable=true)
     */
    private $section;

    	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
     
    private $timeend;
   
  
     /**
     * @var \Badiu\Admin\ServerBundle\Entity\AdminServerService
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ServerBundle\Entity\AdminServerService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sserviceid", referencedColumnName="id")
     * })
     */
    private $sserviceid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="lmssynctype", type="string", length=255, nullable=true)
     */
    private $lmssynctype;// replicationdatainlms | syncwithcreateddatainlms
    
    /**
     * @var string
     *
     * @ORM\Column(name="lmscontexttypeparent", type="string", length=255, nullable=true)
     */
    private $lmscontexttypeparent; //LMS context type: coursecategory|course|group
  
    /**
   * @var integer
   *
   * @ORM\Column(name="lmscontextidparent", type="bigint", nullable=true)
     */
    private $lmscontextidparent; //id of  context instance. If context is course is id of course
	 /**
     * @var string
     *
     * @ORM\Column(name="lmscontexttype", type="string", length=255, nullable=true)
     */
    private $lmscontexttype; //LMS context data: coursecategory|course|group
	
		 /**
   * @var integer
   *
   * @ORM\Column(name="lmscontextid", type="bigint", nullable=true)
     */
    private $lmscontextid; //id of instance row in lms context
    
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

public function addParent(AmsOffer $offerid) {
        $this->offerid = $offerid;
    }
    public function findParent() {
        return $this->offerid->getId();
    }

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getShortname() {
        return $this->shortname;
    }

 

    public function getTimestart() {
        return $this->timestart;
    }

    public function getTimeend() {
        return $this->timeend;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }


    public function setTimestart($timestart) {
        $this->timestart = $timestart;
    }

    public function setTimeend($timeend) {
        $this->timeend = $timeend;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

public function getSection() {
        return $this->section;
    }

    public function setSection($section) {
        $this->section = $section;
    }

      public function getOfferid() {
        return $this->offerid;
    }

     public function setOfferid(AmsOffer $offerid) {
        $this->offerid = $offerid;
    }
    function getSserviceid() {
        return $this->sserviceid;
    }

    function getLmssynctype() {
        return $this->lmssynctype;
    }

    function getLmscontexttypeparent() {
        return $this->lmscontexttypeparent;
    }

    function getLmscontextidparent() {
        return $this->lmscontextidparent;
    }

    function getLmscontexttype() {
        return $this->lmscontexttype;
    }

    function setSserviceid(\Badiu\Admin\ServerBundle\Entity\AdminServerService $sserviceid) {
        $this->sserviceid = $sserviceid;
    }

    function setLmssynctype($lmssynctype) {
        $this->lmssynctype = $lmssynctype;
    }

    function setLmscontexttypeparent($lmscontexttypeparent) {
        $this->lmscontexttypeparent = $lmscontexttypeparent;
    }

    function setLmscontextidparent($lmscontextidparent) {
        $this->lmscontextidparent = $lmscontextidparent;
    }

    function setLmscontexttype($lmscontexttype) {
        $this->lmscontexttype = $lmscontexttype;
    }

    function getLmscontextid() {
        return $this->lmscontextid;
    }

    function setLmscontextid($lmscontextid) {
        $this->lmscontextid = $lmscontextid;
    }


}
