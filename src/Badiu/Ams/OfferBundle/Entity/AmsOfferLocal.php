<?php

namespace Badiu\Ams\OfferBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsOfferLocal
 *  
 * @ORM\Table(name="ams_offer_local", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_offer_local_shortname_uix", columns={"entity", "shortname"})},
 *       indexes={@ORM\Index(name="ams_offer_local_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_offer_local_offerid_ix", columns={"offerid"}), 
  *              @ORM\Index(name="ams_offer_local_hedificeid_ix", columns={"hedificeid"}),
 *              @ORM\Index(name="ams_offer_local_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="ams_offer_local_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="ams_offer_local_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsOfferLocal
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var AmsOffer
     *
     * @ORM\ManyToOne(targetEntity="AmsOffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offerid", referencedColumnName="id")
     * })
     */
    private $offerid;

    
    /**
     * @var \Badiu\Util\HousingBundle\Entity\UtilHousingEdifice
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Util\HousingBundle\Entity\UtilHousingEdifice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hedificeid", referencedColumnName="id")
     * })
     */
    private $hedificeid;
       
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    
    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;

    
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getOfferid() {
        return $this->offerid;
    }

    function getHedificeid() {
        return $this->hedificeid;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setOfferid(AmsOffer $offerid) {
        $this->offerid = $offerid;
    }

    function setHedificeid(\Badiu\Util\HousingBundle\Entity\UtilHousingEdifice $hedificeid) {
        $this->hedificeid = $hedificeid;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


}
