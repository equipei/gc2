<?php

namespace Badiu\Ams\OfferBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsOfferActivity
 *
 * @ORM\Table(name="ams_offer_activity", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_offer_activity_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="ams_offer_activity_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_offer_activity_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="ams_offer_activity_name_ix", columns={"name"}),
 *              @ORM\Index(name="ams_offer_activity_topicid_ix", columns={"topicid"}),
  *              @ORM\Index(name="ams_offer_activity_statusid_ix", columns={"statusid"}),
 *              @ORM\Index(name="ams_offer_activity_classeid_ix", columns={"classeid"}),
 *              @ORM\Index(name="ams_offer_activity_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="ams_offer_activity_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="ams_offer_activity_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="ams_offer_activity_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsOfferActivity {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var AmsOfferTopic
	 *
	 * @ORM\ManyToOne(targetEntity="AmsOfferTopic")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="topicid", referencedColumnName="id")
	 * })
	 */
	private $topicid;

       
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

         /**
     * @var AmsOfferActivityStatus
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferActivityStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;
    
        /**
     * @var AmsOfferClassePlanned
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferClassePlanned")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plannedid", referencedColumnName="id")
     * })
     */
    private $plannedid;
    

    /**
	 * @var AmsOfferClasse
	 *
	 * @ORM\ManyToOne(targetEntity="AmsOfferClasse")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="classeid", referencedColumnName="id")
	 * })
	 */
	private $classeid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
	 */
	private $shortname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype;

           /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */

    private $timeend;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted;

	public function getId() {
		return $this->id;
	}

	public function getEntity() {
		return $this->entity;
	}

	public function getName() {
		return $this->name;
	}

	public function getShortname() {
		return $this->shortname;
	}

	public function getIdnumber() {
		return $this->idnumber;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getParam() {
		return $this->param;
	}

	public function getTimecreated() {
		return $this->timecreated;
	}

	public function getTimemodified() {
		return $this->timemodified;
	}

	public function getUseridadd() {
		return $this->useridadd;
	}

	public function getUseridedit() {
		return $this->useridedit;
	}

	public function getDeleted() {
		return $this->deleted;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setEntity($entity) {
		$this->entity = $entity;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setShortname($shortname) {
		$this->shortname = $shortname;
	}

	public function setIdnumber($idnumber) {
		$this->idnumber = $idnumber;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setParam($param) {
		$this->param = $param;
	}

	public function setTimecreated(\DateTime $timecreated) {
		$this->timecreated = $timecreated;
	}

	public function setTimemodified(\DateTime $timemodified) {
		$this->timemodified = $timemodified;
	}

	public function setUseridadd($useridadd) {
		$this->useridadd = $useridadd;
	}

	public function setUseridedit($useridedit) {
		$this->useridedit = $useridedit;
	}

	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}

	/**
	 * @return string
	 */
	public function getDtype() {
		return $this->dtype;
	}

	/**
	 * @param string $dtype
	 */
	public function setDtype($dtype) {
		$this->dtype = $dtype;
	}

	public function getTopicid() {
		return $this->topicid;
	}
	public function setTopicid(AmsOfferTopic $topicid) {
		$this->topicid = $topicid;
	}

        function getStatusid() {
            return $this->statusid;
        }

        function getTimestart() {
            return $this->timestart;
        }

        function getTimeend() {
            return $this->timeend;
        }

        function setStatusid(AmsOfferActivityStatus $statusid) {
            $this->statusid = $statusid;
        }

        function setTimestart(\DateTime $timestart) {
            $this->timestart = $timestart;
        }

        function setTimeend(\DateTime $timeend) {
            $this->timeend = $timeend;
        }

        function getPlannedid() {
            return $this->plannedid;
        }

        
        function setPlannedid(AmsOfferClassePlanned $plannedid) {
            $this->plannedid = $plannedid;
        }

        function getClasseid() {
            return $this->classeid;
        }

        function setClasseid(AmsOfferClasse $classeid) {
            $this->classeid = $classeid;
        }




}
