<?php

namespace Badiu\Ams\OfferBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsOfferClasseDiscipline
 *  
 * @ORM\Table(name="ams_offer_classe_discipline", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_offer_classe_discipline_name_uix", columns={"entity", "classeid", "disciplineid"}),
 *      @ORM\UniqueConstraint(name="ams_offer_classe_discipline_idnumber_uix", columns={"entity", "idnumber"})},
 *      indexes={@ORM\Index(name="ams_offer_classe_discipline_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="ams_offer_classe_discipline_classeid_ix", columns={"classeid"}),
 *              @ORM\Index(name="ams_offer_classe_discipline_disciplineid_ix", columns={"disciplineid"}),
 *              @ORM\Index(name="ams_offer_classe_discipline_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="ams_offer_classe_discipline_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsOfferClasseDiscipline
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var AmsOfferClasse
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferClasse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="classeid", referencedColumnName="id")
     * })
     */
    private $classeid;

    /**
     * @var Badiu\Ams\DisciplineBundle\Entity\AmsDiscipline
     *
     * @ORM\ManyToOne(targetEntity="Badiu\Ams\DisciplineBundle\Entity\AmsDiscipline")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disciplineid", referencedColumnName="id")
     * })
     */
    private $disciplineid;


    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getClasseid() {
        return $this->classeid;
    }

    public function getDisciplineid() {
        return $this->disciplineid;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setClasseid(AmsOfferClasse $classeid) {
        $this->classeid = $classeid;
    }

    public function setDisciplineid(\Badiu\Ams\DisciplineBundle\Entity\AmsDiscipline $disciplineid) {
        $this->disciplineid = $disciplineid;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


}
