<?php
//REVISAR
namespace Badiu\Ams\OfferBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsOfferDiscipline
 *
 * @ORM\Table(name="ams_offer_discipline", uniqueConstraints={
  *      @ORM\UniqueConstraint(name="ams_offer_discipline_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_offer_discipline_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_offer_discipline_disciplineid_ix", columns={"disciplineid"}), 
 *              @ORM\Index(name="ams_offer_discipline_disciplinename_ix", columns={"disciplinename"}), 
 *              @ORM\Index(name="ams_offer_discipline_offerid_ix", columns={"offerid"}),
 *             @ORM\Index(name="ams_offer_discipline_statusid_ix", columns={"statusid"}),
 *              @ORM\Index(name="ams_offer_discipline_deleted_ix", columns={"deleted"}), 
 *              @ORM\Index(name="ams_offer_discipline_groupid_ix", columns={"groupid"}),
 *              @ORM\Index(name="ams_offer_discipline_typeid_ix", columns={"typeid"}),
 *              @ORM\Index(name="ams_offer_discipline_typeaccessid_ix", columns={"typeaccessid"}),
 *              @ORM\Index(name="ams_offer_discipline_typemanagerid_ix", columns={"typemanagerid"}),
  *             @ORM\Index(name="ams_offer_discipline_sectionid_ix", columns={"sectionid"}),
  *              @ORM\Index(name="ams_offer_discipline_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsOfferDiscipline
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;


   
	/**
     * @var AmsOffer
     *
     * @ORM\ManyToOne(targetEntity="AmsOffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offerid", referencedColumnName="id")
     * })
     */
    private $offerid;
	
  

    
        /**
     * @var AmsOfferSection
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferSection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sectionid", referencedColumnName="id")
     * })
     */
    private $sectionid;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="float", precision=10, scale=0, nullable=true)
     */
    private $sortorder;
    
    /**
     * @var \Badiu\Ams\CurriculumBundle\Entity\AmsCurriculumGroup
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\CurriculumBundle\Entity\AmsCurriculumGroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="groupid", referencedColumnName="id")
     * })
     */
    private $groupid; //review deleted

	/**
     * @var AmsOfferType
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typeid", referencedColumnName="id")
     * })
     */
    private $typeid;
	
	/**
	 *
	 * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="authordevid", referencedColumnName="id")
	 * })
	 */
	private $authordevid;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="devinfo", type="text", nullable=true)
     */
    private $devinfo;
	/**
     * @var integer
     *
     * @ORM\Column(name="costdev", type="float", precision=10, scale=0, nullable=true)
     */
    private $costdev;
	
	/**
     * @var AmsOfferTypeManager
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferTypeManager")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typemanagerid", referencedColumnName="id")
     * })
     */
    private $typemanagerid;
	
        	/**
     * @var AmsOfferTypeAccess
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferTypeAccess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typeaccessid", referencedColumnName="id")
     * })
     */
    private $typeaccessid;
	
	 /**
     * @var AmsOfferPeriodDay
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferPeriodDay")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="periodday", referencedColumnName="id")
     * })
     */
    private $periodday;
    
    	/**
     * @var string
     *
     * @ORM\Column(name="labelinfo", type="string", length=255, nullable=true)
     */
    private $labelinfo;
   
   
   /**
     * @var \Badiu\Ams\DisciplineBundle\Entity\AmsDiscipline
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\DisciplineBundle\Entity\AmsDiscipline")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disciplineid", referencedColumnName="id")
     * })
     */
  
    private $disciplineid;

     
    /**
     * @var string
     *
     * @ORM\Column(name="disciplinename", type="string", length=255, nullable=false)
     */
    private $disciplinename;

	/**
     * @var \Badiu\Util\HousingBundle\Entity\UtilHousingRoom
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Util\HousingBundle\Entity\UtilHousingRoom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hroomid", referencedColumnName="id")
     * })
     */
    private $hroomid;
	
	
	
         /**
         * @var string
         * 
         * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
         */
    private $shortname;
     /**
     * @var AmsOfferDisciplineStatus
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferDisciplineStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;
	
	/**
     * @var AmsOfferDisciplineCategory
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferDisciplineCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;

	
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
     
    private $timeend;
   
   /**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timeupdate", type="datetime", nullable=true)
	 */
	private $timeupdate;
	
   /**
     * @var \DateTime
     *
     * @ORM\Column(name="enrolrequesttimestart", type="datetime", nullable=true)
     */
    private $enrolrequesttimestart;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="enrolrequesttimeend", type="datetime", nullable=true)
     */
    private $enrolrequesttimeend;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="lmsaccesstimestart", type="datetime", nullable=true)
     */
    private $lmsaccesstimestart;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="lmsaccesstimeend", type="datetime", nullable=true)
     */
    private $lmsaccesstimeend;
    /**
     * @var boolean
     *
     * @ORM\Column(name="drequired", type="integer", nullable=true)
     */
    private $drequired=0; 
   
      /**
     * @var integer
     *
     * @ORM\Column(name="maxenrol", type="integer", nullable=true)
     */
    private $maxenrol;
    
	/**
     * @var string
     *
     * @ORM\Column(name="enroldatetype", type="string", length=50, nullable=true)
     */
    private $enroldatetype; //periodofparent | perioddynamic
	
	/**
     * @var string
     *
     * @ORM\Column(name="enroldatedynamic", type="string", length=255, nullable=true)
     */
    private $enroldatedynamic; 
	
	/**
     * @var string
     *
     * @ORM\Column(name="enrolreplicate", type="string", length=255, nullable=true)
     */
    private $enrolreplicate;
	
	/**
     * @var string
     *
     * @ORM\Column(name="enrolreplicateupadate", type="string", length=255, nullable=true)
     */
    private $enrolreplicateupadate;
	
		/**
     * @var string
     *
     * @ORM\Column(name="enroltype", type="string", length=50, nullable=true)
     */
    private $enroltype; //manual | selectprocess
	
       /**
     * @var integer
     *
     * @ORM\Column(name="maxenrolperclass", type="integer", nullable=true)
     */
    private $maxenrolperclass;
    
          /**
     * @var integer
     *
     * @ORM\Column(name="maxclass", type="integer", nullable=true)
     */
    private $maxclass=1;
    
   /**
     * @var boolean
     *
     * @ORM\Column(name="templateclass", type="integer", nullable=true)
     */
    private $templateclass=1;
    
   /**
     * @var string
     *
     * @ORM\Column(name="classpatternname", type="string", length=255, nullable=true)
     */
    private $classpatternname;
    
    /**
     * @var float
     *
     * @ORM\Column(name="maxgrade", type="float", precision=10, scale=0, nullable=true)
     */
    private $maxgrade;
   /**
     * @var float
     *
     * @ORM\Column(name="conclusiongrade", type="float", precision=10, scale=0, nullable=true)
     */
    private $conclusiongrade;
  
       /**
     * @var integer
     *
     * @ORM\Column(name="numbergrade", type="integer", nullable=true)
     */
    private $numbergrade;
    
    /**
     *
     * @var integer
     *
     * @ORM\Column(name="credit", type="integer",  nullable=true)
     */
    private $credit;
	/**
     *Composition of the value if the value.
     * @var integer
     *
     * @ORM\Column(name="dhour", type="integer",  nullable=true)
     */
    private $dhour;
     /**
     * @var string
     *
     * @ORM\Column(name="teachingplan", type="text", nullable=true)
     */
    private $teachingplan;
    
	/**
     * @var string
     *
     * @ORM\Column(name="summary", type="text", nullable=true)
     */
    private $summary;

	/**
     * @var integer
     *
     * @ORM\Column(name="fversion", type="integer", nullable=true)
     */
    private $fversion; //version of offer
	  /**
     * @var integer
     *
     * @ORM\Column(name="equivalencereqminfversion", type="integer", nullable=true)
     */
    private $equivalencereqminfversion;  //mininum version of offer required to equivalence
	
   /**
     * @var integer
     *
     * @ORM\Column(name="enablecertificate", type="integer", nullable=true)
     */
    private $enablecertificate;
	
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificatetempleid", type="bigint", nullable=true)
     */
    private $certificatetempleid;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="certificatecontent", type="text", nullable=true)
     */
    private $certificatecontent;

	/**
     * @var string
     *
     * @ORM\Column(name="certificatetimestart",  type="datetime", nullable=true)
     */
    private $certificatetimestart;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="certificatetimeend",  type="datetime", nullable=true)
     */
    private $certificatetimeend;
	
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateuser", type="integer", nullable=true)
     */
    private $certificateupdateuser;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateenrol", type="integer", nullable=true)
     */
    private $certificateupdateenrol;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateobjectinfo", type="integer", nullable=true)
     */
    private $certificateupdateobjectinfo;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="certificateupdateobjectdata", type="integer", nullable=true)
     */
    private $certificateupdateobjectdata;
	 /**
     * @var boolean
     *
     * @ORM\Column(name="productintegration", type="integer", nullable=true)
     */
    private $productintegration=1;
    
        /**
     * @var integer
     *
     * @ORM\Column(name="productid", type="bigint", nullable=true)
     */
    private $productid;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="adminformattemptid", type="bigint", nullable=true)
	 */
	private $adminformattemptid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProject
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProject")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="projectid", referencedColumnName="id")
	 * })
	 */
	private $projectid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProjectTypePartnership
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProjectTypePartnership")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="typepartnershipid", referencedColumnName="id")
	 * })
	 */
	private $typepartnershipid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProjectTypeDev
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProjectTypeDev")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="typedevid", referencedColumnName="id")
	 * })
	 */
	private $typedevid;
	/**
	 *
	 * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="enterpriseid", referencedColumnName="id")
	 * })
	 */
	private $enterpriseid;
	
	 /**
     * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterpriseDepartment
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterpriseDepartment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="departmentid", referencedColumnName="id")
     * })
     */

    private $departmentid; 
	
	/**
     * @var string
     *
     * @ORM\Column(name="usertarget", type="text", nullable=true)
     */
    private $usertarget;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="usertargetestimatedamout", type="bigint", nullable=true)
     */
    private $usertargetestimatedamout;
	/**
     * @var string
     *
     * @ORM\Column(name="objective", type="text", nullable=true)
     */
    private $objective;
	
	/**
     * @var string
     *
     * @ORM\Column(name="competence", type="text", nullable=true)
     */
    private $competence;
     /**
	    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;


    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;

    
       /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
   /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="roomaddress", type="text", nullable=true)
     */
    private $roomaddress;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

	/**
     * @var string
     *
     * @ORM\Column(name="defaultimage", type="string", length=255, nullable=true)
     */
    private $defaultimage;
     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;


    public function addParent(AmsOffer $offerid) {
        $this->offerid = $offerid;
    }
    public function findParent() {
        return $this->offerid->getId();
    }


    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getOfferid() {
        return $this->offerid;
    }

    public function getGroupid() {
        return $this->groupid;
    }

    public function getDisciplineid() {
        return $this->disciplineid;
    }

    public function getStatusid() {
        return $this->statusid;
    }

    public function getTimestart() {
        return $this->timestart;
    }

    public function getTimeend() {
        return $this->timeend;
    }

    public function getDrequired() {
        return $this->drequired;
    }

   

    public function getTeachingplan() {
        return $this->teachingplan;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }
  

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setOfferid(AmsOffer $offerid) {
        $this->offerid = $offerid;
    }

   
    public function setGroupid(\Badiu\Ams\CurriculumBundle\Entity\AmsCurriculumGroup $groupid) {
        $this->groupid = $groupid;
    }

    public function setDisciplineid(\Badiu\Ams\DisciplineBundle\Entity\AmsDiscipline $disciplineid) {
        $this->disciplineid = $disciplineid;
    }

    public function setStatusid(AmsOfferDisciplineStatus $statusid) {
        $this->statusid = $statusid;
    }

	   
    public function setTimestart($timestart) {
        $this->timestart = $timestart;
    }

    public function setTimeend($timeend) {
        $this->timeend = $timeend;
    }

    public function setDrequired($drequired) {
        $this->drequired = $drequired;
    }

    

    public function setTeachingplan($teachingplan) {
        $this->teachingplan = $teachingplan;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

 public function getDisciplinename() {
        return $this->disciplinename;
    }
    public function setDisciplinename($disciplinename) {
        $this->disciplinename = $disciplinename;
    }
public function getShortname() {
        return $this->shortname;
    }
    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    public function getConclusiongrade() {
        return $this->conclusiongrade;
    }

	 public function setConclusiongrade($conclusiongrade) {
        $this->conclusiongrade = $conclusiongrade;
    }
	
	
   

    public function getModulekey()
    {
        return $this->modulekey;
    }

  
    public function setModulekey($modulekey)
    {
        $this->modulekey = $modulekey;
    }


 
 
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

   

    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }

	  /**
     * @return int
     */
    public function getDhour()
    {
        return $this->dhour;
    }

    /**
     * @param int $dhour
     */
    public function setDhour($dhour)
    {
        $this->dhour = $dhour;
    }
public function getSummary() {
        return $this->summary;
    }

    public function setSummary($summary) {
        $this->summary = $summary;
    }
	
	
	
	public function getTypeid() {
        return $this->typeid;
    }
	public function setTypeid(AmsOfferType $typeid) {
        $this->typeid = $typeid;
    }
	

	  
	public function getCategoryid() {
        return $this->categoryid;
    }
	public function setCategoryid(AmsOfferDisciplineCategory $categoryid) {
        $this->categoryid = $categoryid;
    }
    function getRoomaddress() {
        return $this->roomaddress;
    }

    function setRoomaddress($roomaddress) {
        $this->roomaddress = $roomaddress;
    }

    function getMaxenrol() {
        return $this->maxenrol;
    }

    function setMaxenrol($maxenrol) {
        $this->maxenrol = $maxenrol;
    }

    function getMaxenrolperclass() {
        return $this->maxenrolperclass;
    }

    function getMaxclass() {
        return $this->maxclass;
    }

    function getCredit() {
        return $this->credit;
    }

    function setMaxenrolperclass($maxenrolperclass) {
        $this->maxenrolperclass = $maxenrolperclass;
    }

    function setMaxclass($maxclass) {
        $this->maxclass = $maxclass;
    }

    function setCredit($credit) {
        $this->credit = $credit;
    }

    function getNumbergrade() {
        return $this->numbergrade;
    }

    function setNumbergrade($numbergrade) {
        $this->numbergrade = $numbergrade;
    }

    function getProductintegration() {
        return $this->productintegration;
    }

    function setProductintegration($productintegration) {
        $this->productintegration = $productintegration;
    }

    function getProductid() {
        return $this->productid;
    }

    function setProductid($productid) {
        $this->productid = $productid;
    }

    function getHroomid() {
        return $this->hroomid;
    }

    function setHroomid(\Badiu\Util\HousingBundle\Entity\UtilHousingRoom $hroomid) {
        $this->hroomid = $hroomid;
    }

    function getTemplateclass() {
        return $this->templateclass;
    }

    function setTemplateclass($templateclass) {
        $this->templateclass = $templateclass;
    }

    function getClasspatternname() {
        return $this->classpatternname;
    }

    function setClasspatternname($classpatternname) {
        $this->classpatternname = $classpatternname;
    }

    function getMaxgrade() {
        return $this->maxgrade;
    }

    function setMaxgrade($maxgrade) {
        $this->maxgrade = $maxgrade;
    }

   
    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }
    function getSectionid() {
        return $this->sectionid;
    }

    function setSectionid(AmsOfferSection $sectionid) {
        $this->sectionid = $sectionid;
    }

    function getSortorder() {
        return $this->sortorder;
    }

    function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }


	
		function getEnroltype() {
        return $this->enroltype;
    }

    function setEnroltype($enroltype) {
        $this->enroltype = $enroltype;
    }

  function getEnroldatetype() {
        return $this->enroldatetype;
    }
    function setEnroldatetype($enroldatetype) {
        $this->enroldatetype = $enroldatetype;
    }


	  function getEnroldatedynamic() {
        return $this->enroldatedynamic;
    }
    function setEnroldatedynamic($enroldatedynamic) {
        $this->enroldatedynamic = $enroldatedynamic;
    }	
	
		  function getEnrolreplicate() {
        return $this->enrolreplicate;
    }
    function setEnrolreplicate($enrolreplicate) {
        $this->enrolreplicate = $enrolreplicate;
    }
	
		  function getEnrolreplicateupadate() {
        return $this->enrolreplicateupadate;
    }
    function setEnrolreplicateupadate($enrolreplicateupadate) {
        $this->enrolreplicateupadate = $enrolreplicateupadate;
    }
	
	
	public function getPeriodday() {
        return $this->periodday;
    }
	
	public function setPeriodday($periodday) {
        $this->periodday = $periodday;
    }
	
	public function getLabelinfo() {
        return $this->labelinfo;
    }
	
	public function setLabelinfo($labelinfo) {
        $this->labelinfo = $labelinfo;
    }
	
    public function getTypemanagerid() {
        return $this->typemanagerid;
    }
	
	public function setTypemanagerid($typemanagerid) {
        $this->typemanagerid = $typemanagerid;
    }
	
	function getEquivalencereqminfversion() { 
        return $this->equivalencereqminfversion;
    }

    function setEquivalencereqminfversion($equivalencereqminfversion) {
        $this->equivalencereqminfversion = $equivalencereqminfversion;
    }
	
	 function getFversion() { 
        return $this->fversion;
    }

    function setFversion($fversion) {
        $this->fversion = $fversion;
    }

    	
	public function setTypeaccessid($typeaccessid) {
        $this->typeaccessid = $typeaccessid;
    }

    function getTypeaccessid() {
        return  $this->typeaccessid ;
    }
	
	
	function getDefaultimage() {
        return $this->defaultimage;
    }

    function setDefaultimage($defaultimage) {
        $this->defaultimage = $defaultimage;
    }
	
	
	function getCertificatetempleid() {
        return $this->certificatetempleid;
    }

   
    function setCertificatetempleid($certificatetempleid) {
        $this->certificatetempleid = $certificatetempleid;
    }
 
   public function  setEnablecertificate($enablecertificate) {
        $this->enablecertificate=$enablecertificate;
    }
	
	 public function getEnablecertificate() {
        return $this->enablecertificate;
    }
	
 public function getCertificatecontent() {
	    return $this->certificatecontent;
    }

   
    function setCertificatecontent($certificatecontent) {
        $this->certificatecontent = $certificatecontent;
    }
	
	
	 public function getCertificatetimestart() {
	    return $this->certificatetimestart;
    }

   
    function setCertificatetimestart($certificatetimestart) {
        $this->certificatetimestart = $certificatetimestart;
    }
	
	
	
	 public function getCertificatetimeend() {
	    return $this->certificatetimeend;
    }

   
    function setCertificatetimeend($certificatetimeend) {
        $this->certificatetimeend = $certificatetimeend;
    }
	
	
    function getLmsaccesstimestart() {
        return $this->lmsaccesstimestart;
    }

    function getLmsaccesstimeend() {
        return $this->lmsaccesstimeend;
    }


 function setLmsaccesstimestart(\DateTime $lmsaccesstimestart) {
        $this->lmsaccesstimestart = $lmsaccesstimestart;
    }

    function setLmsaccesstimeend(\DateTime $lmsaccesstimeend) {
        $this->lmsaccesstimeend = $lmsaccesstimeend;
    }
	
	
	
	
    function getEnrolrequesttimestart() {
        return $this->enrolrequesttimestart;
    }

    function getEnrolrequesttimeend() {
        return $this->enrolrequesttimeend;
    }


 function setEnrolrequesttimestart(\DateTime $enrolrequesttimestart) {
        $this->enrolrequesttimestart = $enrolrequesttimestart;
    }

    function setEnrolrequesttimeend(\DateTime $enrolrequesttimeend) {
        $this->enrolrequesttimeend = $enrolrequesttimeend;
    }
	
	
	public function getProjectid() {
		return $this->projectid;
	}
	
	public function setProjectid($projectid) {
		$this->projectid = $projectid;
	}
	
	function getEnterpriseid() {
            return $this->enterpriseid;
        }
	function setEnterpriseid($enterpriseid) {
            $this->enterpriseid = $enterpriseid;
        }

	public function setDepartmentid($departmentid)
    {
        $this->departmentid = $departmentid;
	}
	
	public function getDepartmentid()
    {
        return $this->departmentid;
    }

public function setUsertarget($usertarget)
    {
        $this->usertarget = $usertarget;
	}
	
	public function getUsertarget()
    {
        return $this->usertarget;
    }

public function setAdminformattemptid($adminformattemptid)
    {
        $this->adminformattemptid = $adminformattemptid;
	}
	
	public function getAdminformattemptid()
    {
        return $this->adminformattemptid;
    }	
public function setTypepartnershipid($typepartnershipid)
    {
        $this->typepartnershipid = $typepartnershipid;
	}
	
	public function getTypepartnershipid()
    {
        return $this->typepartnershipid;
    }	

public function setTypedevid($typedevid)
    {
        $this->typedevid = $typedevid;
	}
	
	public function getTypedevid()
    {
        return $this->typedevid;
    }	

public function setObjective($objective)
    {
        $this->objective = $objective;
	}
	
	public function getObjective()
    {
        return $this->objective;
    }	
	
	
	public function setTimeupdate($timeupdate)
    {
        $this->timeupdate = $timeupdate;
	}
	
	public function getTimeupdate()
    {
        return $this->timeupdate;
    }


public function setAuthordevid($authordevid)
    {
        $this->authordevid = $authordevid;
	}
	
	public function getAuthordevid()
    {
        return $this->authordevid;
    }		
	
	
public function setCostdev($costdev)
    {
        $this->costdev = $costdev;
	}
	
	public function getCostdev()
    {
        return $this->costdev;
    }	


public function setDevinfo($devinfo)
    {
        $this->devinfo = $devinfo;
	}
	
	public function getDevinfo()
    {
        return $this->devinfo;
    }


function getCompetence() {
            return $this->competence;
        }
	function setCompetence($competence) {
            $this->competence = $competence;
        }	


        function setCertificateupdateuser($certificateupdateuser) {
            $this->certificateupdateuser = $certificateupdateuser;
        }

        function getCertificateupdateuser() {
            return $this->certificateupdateuser;
        }

	function setCertificateupdateobjectinfo($certificateupdateobjectinfo) {
            $this->certificateupdateobjectinfo = $certificateupdateobjectinfo;
        }

        function getCertificateupdateobjectinfo() {
            return $this->certificateupdateobjectinfo;
        }	

function setCertificateupdateobjectdata($certificateupdateobjectdata) {
            $this->certificateupdateobjectdata = $certificateupdateobjectdata;
        }

        function getCertificateupdateobjectdata() {
            return $this->certificateupdateobjectdata;
        }	

function setCertificateupdateenrol($certificateupdateenrol) {
            $this->certificateupdateenrol = $certificateupdateenrol;
        }

        function getCertificateupdateenrol() {
            return $this->certificateupdateenrol;
        }	

public function setUsertargetestimatedamout($usertargetestimatedamout)
    {
        $this->usertargetestimatedamout = $usertargetestimatedamout;
	}
	
	public function getUsertargetestimatedamout()
    {
        return $this->usertargetestimatedamout;
    }		
}
