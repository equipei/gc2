<?php

namespace Badiu\Ams\OfferBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * AmsOfferCategory
 *
 * @ORM\Table(name="ams_offer_cetegory", uniqueConstraints={
 *                                                  @ORM\UniqueConstraint(name="ams_offer_cetegory_shortname_uix", columns={"entity", "shortname"}),
 *                                                  @ORM\UniqueConstraint(name="ams_offer_cetegory_idnumber_uix", columns={"entity", "idnumber"})},
 *                                     indexes={@ORM\Index(name="ams_offer_cetegory_entity_ix", columns={"entity"}),
 *                                              @ORM\Index(name="ams_offer_cetegory_name_ix", columns={"name"}),
 *                                              @ORM\Index(name="ams_offer_cetegory_shortname_ix", columns={"shortname"}),
 *                                              @ORM\Index(name="ams_offer_cetegory_dtype_ix", columns={"dtype"}),
 *                                              @ORM\Index(name="ams_offer_cetegory_idpath_ix", columns={"idpath"}),
 *                                              @ORM\Index(name="ams_offer_cetegory_orderidpath_ix", columns={"orderidpath"}),
 *                                              @ORM\Index(name="ams_offer_cetegory_sortorder_ix", columns={"sortorder"}),
 *                                              @ORM\Index(name="ams_offer_cetegory_deleted_ix", columns={"deleted"}),
 *                                              @ORM\Index(name="ams_offer_cetegory_idnumber_ix", columns={"idnumber"})})
 * 
 * @ORM\Entity
 */

class AmsOfferCategory
{
      /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
    
            /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='course'; // course | training | event
    /**
     * @var integer
     *
     * @ORM\Column(name="parent", type="bigint", nullable=true)
     */
    private $parent; //id of father
    
    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    private $level; //generation of idpath
    
    /**
     * @var string
     *
     * @ORM\Column(name="idpath", type="string", length=255, nullable=false)
     */
    private $idpath; //id tree example: 1, 1.1, 1.2,1.2.1

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path; //hierarchy of id father exemp 1, 1/2, 1/2/4
   
     /**
     * @var float
     *
     * @ORM\Column(name="orderidpath", type="float", precision=10, scale=0, nullable=false)
     */
    private $orderidpath; //order of id tree
    
    /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="bigint", nullable=true)
     */
    private $sortorder;

     /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
  
     /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    
   
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function getDtype() {
        return $this->dtype;
    }

    function getParent() {
        return $this->parent;
    }

    function getLevel() {
        return $this->level;
    }

    function getIdpath() {
        return $this->idpath;
    }

    function getPath() {
        return $this->path;
    }

    function getOrderidpath() {
        return $this->orderidpath;
    }

    function getSortorder() {
        return $this->sortorder;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function setParent($parent) {
        $this->parent = $parent;
    }

    function setLevel($level) {
        $this->level = $level;
    }

    function setIdpath($idpath) {
        $this->idpath = $idpath;
    }

    function setPath($path) {
        $this->path = $path;
    }

    function setOrderidpath($orderidpath) {
        $this->orderidpath = $orderidpath;
    }

    function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


}
