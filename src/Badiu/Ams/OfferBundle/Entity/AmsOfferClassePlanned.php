<?php

namespace Badiu\Ams\OfferBundle\Entity;
use Badiu\System\ComponentBundle\Model\Form\Cast\BadiuHourFormCast;
use Doctrine\ORM\Mapping as ORM;

/**
 * AmsOfferClassePlanned
 *  
 * @ORM\Table(name="ams_offer_classe_planned", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_offer_classe_planned_idnumber_uix", columns={"entity", "idnumber"})},
 *      indexes={@ORM\Index(name="ams_offer_classe_planned_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="ams_offer_classe_planned_classeid_ix", columns={"classeid"}),
 *              @ORM\Index(name="ams_offer_classe_planned_hroomid_ix", columns={"hroomid"}),
 *              @ORM\Index(name="ams_offer_classe_planned_timestart_ix", columns={"timestart"}),
 *              @ORM\Index(name="ams_offer_classe_planned_timeend_ix", columns={"timeend"}),
 *              @ORM\Index(name="ams_offer_classe_planned_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="ams_offer_classe_planned_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsOfferClassePlanned
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var AmsOfferClasse
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferClasse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="classeid", referencedColumnName="id")
     * })
     */
    private $classeid;
    
     /**
     * @var AmsOfferClasseTaughtStatus
     *
     * @ORM\ManyToOne(targetEntity="AmsOfferClasseTaughtStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;

  	/**
     * @var \Badiu\Util\HousingBundle\Entity\UtilHousingRoom
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Util\HousingBundle\Entity\UtilHousingRoom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hroomid", referencedColumnName="id")
     * })
     */
    
    
    private $hroomid;
/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string",length=255, nullable=true)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255,nullable=true)
	 */
	private $shortname;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=false)
     */
    private $timeend;
        
     /**
     * @var integer
     *
     * @ORM\Column(name="hourduration", type="integer", nullable=false)
     */
    private $hourduration;

     /**
     * @var integer
     *
     * @ORM\Column(name="hourdurationtaught", type="integer", nullable=true)
     */
    private $hourdurationtaught;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="dsequence", type="integer", nullable=true)
     */
    private $dsequence;
    
     /**
     * @var string
     *
     * @ORM\Column(name="contenttaught", type="text", nullable=true)
     */
    private $contenttaught;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;

    
         /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getClasseid() {
        return $this->classeid;
    }

    function getHroomid() {
        return $this->hroomid;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getHourduration() {
         $fhour=new BadiuHourFormCast();
         $dhour= $fhour->convertToForm($this->hourduration);
         return $dhour;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setClasseid(AmsOfferClasse $classeid) {
        $this->classeid = $classeid;
    }

    function setHroomid(\Badiu\Util\HousingBundle\Entity\UtilHousingRoom $hroomid) {
        $this->hroomid = $hroomid;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setHourduration($hourduration) {
          $fhour=new BadiuHourFormCast();
        $dhour= $fhour->convertFromForm($hourduration);
        $this->hourduration = $dhour;
        
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getStatusid() {
        return $this->statusid;
    }

    function getDsequence() {
        return $this->dsequence;
    }

    function setStatusid(AmsOfferClasseTaughtStatus $statusid) {
        $this->statusid = $statusid;
    }

    function setDsequence($dsequence) {
        $this->dsequence = $dsequence;
    }


    function getHourdurationtaught() {
        return $this->hourdurationtaught;
    }

    function getContenttaught() {
        return $this->contenttaught;
    }

    function setHourdurationtaught($hourdurationtaught) {
        $this->hourdurationtaught = $hourdurationtaught;
    }

    function setContenttaught($contenttaught) {
        $this->contenttaught = $contenttaught;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }
    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

}
