<?php

namespace  Badiu\Ams\MyBundle\Model\Student;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class LmsFormat extends BadiuFormat{
      private $router;
     function __construct(Container $container) {
            parent::__construct($container);
             $this->router=$this->getContainer()->get("router");
       } 

   
    public  function lmscourseaccess($data){
       $value=null;
                 $fullname=null;
                 $id=null;
                 if(isset($data['fullname'])){$fullname=$data['fullname'];}
                 $value=$fullname;
                 
                 if(isset($data['id'])){$id=$data['id'];}
                
                 if(!empty($fullname) && !empty($id)){
                    $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
                     $urltarget="/course/view.php?id=$id";
                     
                    $lroute=$this->router->generate('badiu.system.core.service.process',array('_service'=>'badiu.ams.core.lib.lmsmoodledatautil','_function'=>'remoteAuth','_serviceid'=>$serviceid,'_urltarget'=>$urltarget));
                    $value="<a target=\"_blank\" href=\"$lroute\">$fullname</a>";
                    
                 }
		return $value;  
    } 
 
    
  
}
