<?php

namespace  Badiu\Ams\MyBundle\Model\Student;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class AmsFormat extends BadiuFormat{
      private $router;
	  private $moodleremoteaccess;
     function __construct(Container $container) {
            parent::__construct($container);
             $this->router=$this->getContainer()->get("router");
			 $this->moodleremoteaccess=$this->getContainer()->get('badiu.moodle.core.lib.remoteaccess');
       } 

	   
     public  function moodleaccess($data){ 
	
			$dconfig =$this->getUtildata()->getVaueOfArray($data,'dconfig');
			$classename=$this->getUtildata()->getVaueOfArray($data,'classename');
			$classedescription=$this->getUtildata()->getVaueOfArray($data,'classedescription');
			$classename.=" - ".$classedescription;
			$dconfig = $this->getJson()->decode($dconfig, true); 
		    $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
			$serviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
			$lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynclevel',true);
		    $lmscoursecatid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatid',true);
			$lmscourseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);				
			$status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
			$status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		    if(empty($serviceid)){return null;}
			$urltarget=null;
			$link=null;
			if(empty($classename)){$classename="Acessar";}
			if($lmssynclevel=='course' && $lmscourseid > 0 ){
					$urltarget="/course/view.php?id=$lmscourseid";
					$externalclient=false;
					$badiuSession=$this->getContainer()->get('badiu.system.access.session');
					$badiuSession->setHashkey($this->getSessionhashkey());
					$clienttype=$badiuSession->get()->getType();
					if($clienttype=='webservice' || $clienttype=='webservicesynceduser'){$externalclient=true;}
					if($externalclient){
						$this->moodleremoteaccess->setSessionhashkey($this->getSessionhashkey());
						$moodleurl=$this->moodleremoteaccess->getSessionUrl($serviceid);
						$url=$moodleurl.$urltarget;
						$link="<a  href=\"$url\">$classename</a><br />";
						return $link;
					}
						
					
					$url=$this->getRouter()->generate('badiu.system.core.service.process',array('_service'=>'badiu.moodle.core.lib.remoteaccess','_function'=>'remoteAuth','_serviceid'=>$serviceid,'_urltarget'=>$urltarget));
					$link="<a target=\"_blank\" href=\"$url\">$classename</a> <br />";
			}
			
			return  $link;
		}			
    
 
    public  function offernamewithcourse($data){
      
               $value=null;
               $offername= $this->getUtildata()->getVaueOfArray($data,'offername');
               $coursename= $this->getUtildata()->getVaueOfArray($data,'coursename');
               $value=$offername;
               if(!empty($coursename)){$value="$coursename / $offername";}
               return $value;  
      } 
	   public  function enrolstatus($data){
			
               $value=null;
               $offername= $this->getUtildata()->getVaueOfArray($data,'offername');
			   $offername= $this->getUtildata()->getVaueOfArray($data,'offername');
               $coursename= $this->getUtildata()->getVaueOfArray($data,'coursename');
               $value=$offername;
               if(!empty($coursename)){$value="$coursename / $offername";}
               return $value;  
      }  
  
}
