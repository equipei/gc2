<?php

namespace   Badiu\Ams\MyBundle\Model\Student;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportChangeData;
class AmsChangeData extends BadiuReportChangeData
{

    function __construct(Container $container) {
            parent::__construct($container);
              }
               

public function all($data){
	
	
	$disciplineenrolist=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows.data.2',true); 
	$classeenrolist=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows.data.3',true); 
	
	$classeesnerol=$this->makeEnrolclasseKeyByByDiscipine($classeenrolist);
	
	$newcrdiscipline=$this->addEnrolClasseToDiscipline($disciplineenrolist,$classeesnerol);
	
	$data['badiu_list_data_rows']['data'][2]=$newcrdiscipline;
	
	/*echo "<pre>";
	print_r($newcrdiscipline);
	echo "</pre>";exit; */
     return $data;
      
  }
 
 
  function makeEnrolclasseKeyByByDiscipine($listenrol){
	 $newlist=array();
	 if(empty($listenrol)){return $newlist;}
	 if(!is_array($listenrol)){return $newlist;}
	 
	 foreach ($listenrol as $row){
		$classeid=$this->getUtildata()->getVaueOfArray($row,'classeid');
		$odisciplineid=$this->getUtildata()->getVaueOfArray($row,'odisciplineid'); 
		$dlist=$this->getUtildata()->getVaueOfArray($newlist,$odisciplineid); 	
		if(empty($dlist)){$dlist=array();}
		$dlist[$classeid]=$row;
		$newlist[$odisciplineid]=$dlist;
	 }
	 return $newlist;
 } 
  function addEnrolClasseToDiscipline($disciplineenrolist,$classeenrollist){
	  $newlist=array();
	 if(empty($disciplineenrolist)){return $newlist;}
	 if(!is_array($disciplineenrolist)){return $newlist;}
	 
	 foreach ($disciplineenrolist as $row){
		$id=$this->getUtildata()->getVaueOfArray($row,'id'); 
		$odisciplineid=$this->getUtildata()->getVaueOfArray($row,'odisciplineid'); 
		$lclasse=$this->getUtildata()->getVaueOfArray($classeenrollist,$odisciplineid); 
		$row['classe']=$lclasse; 
		
		//get link of moodle
		$linkclasses=null;
		if(!empty($lclasse) && is_array($lclasse)){
			$cont=0;
			foreach ($lclasse as $lcrow){
				$separtor="";
				if($cont > 0){$separtor=" | ";}
				$link=$this->getUtildata()->getVaueOfArray($lcrow,'moodleaccess');
				if(!empty($link)){
					$linkclasses.="$separtor $link";
					$cont++;
				}
				
			}
		}
		if(!empty($linkclasses)){$row['moodleaccess']=$linkclasses;}
		
		$newlist[$id]=$row;
	 }
	 
	 return $newlist;
  }
}
