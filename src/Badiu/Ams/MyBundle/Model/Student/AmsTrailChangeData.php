<?php

namespace  Badiu\Ams\MyBundle\Model\Student;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportChangeData;
class AmsTrailChangeData extends BadiuReportChangeData
{

    function __construct(Container $container) {
            parent::__construct($container);
              }
              

public function all($data){
	
	
	$disciplineenrolist=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows.data.3',true); 
	$classeenrolist=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows.data.4',true); 
	$crdisciplinet=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows.data.2',true); 
	
	$discliplinesnerol=$this->makeEnrolKeyByDiscipine($disciplineenrolist);
	$classeesnerol=$this->makeEnrolclasseKeyByByDiscipine($classeenrolist);
	
	$newcrdiscipline=$this->addEnrolToCurriculumDiscipline($crdisciplinet,$discliplinesnerol,$classeesnerol);
	
	$data['badiu_list_data_rows']['data'][2]=$newcrdiscipline;
	
	/*echo "<pre>";
	print_r($newcrdiscipline);
	echo "</pre>";exit;*/
     return $data;
      
  }
 function makeEnrolKeyByDiscipine($listenrol){
	 $newlist=array();
	 if(empty($listenrol)){return $newlist;}
	 if(!is_array($listenrol)){return $newlist;}
	 
	 foreach ($listenrol as $row){
		$disciplineid=$this->getUtildata()->getVaueOfArray($row,'disciplineid'); 
		$offerid=$this->getUtildata()->getVaueOfArray($row,'offerid');
		$key="$offerid/$disciplineid";
		$newlist[$key]=$row;
	 }
	 return $newlist;
 }
 
  function makeEnrolclasseKeyByByDiscipine($listenrol){
	 $newlist=array();
	 if(empty($listenrol)){return $newlist;}
	 if(!is_array($listenrol)){return $newlist;}
	 
	 foreach ($listenrol as $row){
		$classeid=$this->getUtildata()->getVaueOfArray($row,'classeid');
		$odisciplineid=$this->getUtildata()->getVaueOfArray($row,'odisciplineid'); 
		$dlist=$this->getUtildata()->getVaueOfArray($newlist,$odisciplineid); 	
		if(empty($dlist)){$dlist=array();}
		$dlist[$classeid]=$row;
		$newlist[$odisciplineid]=$dlist;
	 }
	 return $newlist;
 } 
  function addEnrolToCurriculumDiscipline($currilumdisciplines,$disciplinelistenrol,$classelistenrol){
	  $newlist=array();
	 if(empty($currilumdisciplines)){return $newlist;}
	 if(!is_array($currilumdisciplines)){return $newlist;}
	 
	 if(empty($disciplinelistenrol)){return $currilumdisciplines;}
	 if(!is_array($disciplinelistenrol)){return $currilumdisciplines;}

	 foreach ($currilumdisciplines as $row){
		$disciplineid=$this->getUtildata()->getVaueOfArray($row,'disciplineid'); 
		$offerid=$this->getUtildata()->getVaueOfArray($row,'offerid'); 
		$dkey="$offerid/$disciplineid";
		$rowenrol=$this->getUtildata()->getVaueOfArray($disciplinelistenrol,$dkey); 
		$odisciplineid=$this->getUtildata()->getVaueOfArray($rowenrol,'odisciplineid'); 
		$row['statusname']=$this->getUtildata()->getVaueOfArray($rowenrol,'statusname'); 
		$row['odisciplineid']=$this->getUtildata()->getVaueOfArray($rowenrol,'odisciplineid'); 
		$row['statusshortname']=$this->getUtildata()->getVaueOfArray($rowenrol,'statusshortname'); 
		$row['rolename']=$this->getUtildata()->getVaueOfArray($rowenrol,'rolename');
		$row['moodleaccess']=$this->getUtildata()->getVaueOfArray($rowenrol,'moodleaccess'); 
		$row['dconfig']=$this->getUtildata()->getVaueOfArray($rowenrol,'disciplinedconfg');
		$row['moodleaccess']=$this->getUtildata()->getVaueOfArray($rowenrol,'moodleaccess'); 	
		$lclasse=$this->getUtildata()->getVaueOfArray($classelistenrol,$odisciplineid); 
		$row['classe']=$lclasse; 
		
		if(empty($row['statusname'])){
			$row['moodleaccess']=null;
			$row['statusname']=$this->getTranslator()->trans('badiu.ams.enrol.notenroled');
			$row['statusshortname']='notenroled';
			}
		
		//get link of moodle
		$linkclasses=null;
		if(!empty($lclasse) && is_array($lclasse)){
			$cont=0;
			foreach ($lclasse as $lcrow){
				$separtor="";
				if($cont > 0){$separtor=" | ";}
				$link=$this->getUtildata()->getVaueOfArray($lcrow,'moodleaccess');
				if(!empty($link)){
					$linkclasses.="$separtor $link";
					$cont++;
				}
				
			}
		}
		if(!empty($linkclasses)){$row['moodleaccess']=$linkclasses;}
		$this->getUtildata()->getVaueOfArray($classelistenrol,$odisciplineid);
		$key="$offerid/$disciplineid";
		$newlist[$key]=$row;
	 }
	 
	 return $newlist;
  }
}
