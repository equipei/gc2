<?php

namespace Badiu\Ams\MyBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Install extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    } 


    public function exec() {
       
             $result= $this->initPermissions();
           return $result;  
    }  
 
    public function initPermissions() {
        $cont=0;
        $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');

        //student  
        $param=array(
          'roleshortname'=>'student',
          'permissions'=>array('badiu.ams.my.student.frontpage',
          'badiu.ams.my.student.frontpage',
          'badiu.ams.my.studentoffer.dashboard',
          'badiu.ams.my.studentoffercurriculum.dashboard',
          'badiu.ams.my.studentdmoodleenrol.index',
          'badiu.ams.my.studentdmoodleenrol.index',
		  'badiu.admin.certificate.request.link')
        );
        $cont+=$libpermission->add($param);

        //teachear
        $param=array(
            'roleshortname'=>'teachear',
            'permissions'=>array('badiu.ams.my.teachear',
			'badiu.ams.offer.classeattendance.',
			'badiu.ams.offer.classeattendanceconsolidate.index',
			'badiu.ams.offer.classeattendancefullplanned.index',
			'badiu.ams.offer.classeenrol.index',
			'badiu.ams.offer.classeview',
			'badiu.ams.offer.classeview.frontpage'
			)
          );
          $cont+=$libpermission->add($param);

          //coordenator
        $param=array(
            'roleshortname'=>'coordenator',
            'permissions'=>array(
			'badiu.ams.my.coordenator.',
			'badiu.ams.my.coordenatoroffer.',
			'badiu.ams.offer.classe',
			'badiu.ams.offer.discipline',
			'badiu.ams.offer.enrol.',
			'badiu.ams.offer.manage.',
			'badiu.ams.offer.offer.view',
			'badiu.ams.offer.offerattendance.',
			'badiu.ams.offer.period.',
			'badiu.ams.offer.section.')
          );
          $cont+=$libpermission->add($param);
    }
  
}
