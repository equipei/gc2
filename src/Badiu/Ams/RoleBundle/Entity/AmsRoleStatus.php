<?php

namespace Badiu\Ams\RoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsEnrolStatus
 *  
 * @ORM\Table(name="ams_role_status", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_role_status_shortname_uix", columns={"entity", "roleid", "sysroleid", "enrolstatusid"})},
 *       indexes={@ORM\Index(name="ams_role_status_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="ams_role_status_sysroleid_ix", columns={"sysroleid"}),
 *              @ORM\Index(name="ams_role_status_roleid_ix", columns={"roleid"}),
 *              @ORM\Index(name="ams_role_status_enrolstatusid_ix", columns={"enrolstatusid"}),
 *              @ORM\Index(name="ams_role_status_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="ams_role_status_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="ams_role_status_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsRoleStatus
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var AmsRole
     *
     * @ORM\ManyToOne(targetEntity="AmsRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roleid", referencedColumnName="id")
     * })
     */
    private $roleid;

    /**
     * @var \Badiu\System\AccessBundle\Entity\SystemAccessRole
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\AccessBundle\Entity\SystemAccessRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sysroleid", referencedColumnName="id")
     * })
     */
    private $sysroleid;
    /**
     * @var \Badiu\Ams\EnrolBundle\Entity\AmsEnrolStatus
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\EnrolBundle\Entity\AmsEnrolStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="enrolstatusid", referencedColumnName="id")
     * })
     */
    private $enrolstatusid;


/**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='course'; //course | event
   
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param int $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return AmsRole
     */
    public function getRoleid()
    {
        return $this->roleid;
    }

    /**
     * @param AmsRole $roleid
     */
    public function setRoleid($roleid)
    {
        $this->roleid = $roleid;
    }

    /**
     * @return \Badiu\System\AccessBundle\Entity\SystemAccessRole
     */
    public function getSysroleid()
    {
        return $this->sysroleid;
    }

    /**
     * @param \Badiu\System\AccessBundle\Entity\SystemAccessRole $sysroleid
     */
    public function setSysroleid($sysroleid)
    {
        $this->sysroleid = $sysroleid;
    }

    /**
     * @return AmsEnrolStatus
     */
    public function getEnrolstatusid()
    {
        return $this->enrolstatusid;
    }

    /**
     * @param AmsEnrolStatus $enrolstatusid
     */
    public function setEnrolstatusid($enrolstatusid)
    {
        $this->enrolstatusid = $enrolstatusid;
    }

    /**
     * @return string
     */
    public function getIdnumber()
    {
        return $this->idnumber;
    }

    /**
     * @param string $idnumber
     */
    public function setIdnumber($idnumber)
    {
        $this->idnumber = $idnumber;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * @param string $param
     */
    public function setParam($param)
    {
        $this->param = $param;
    }

    /**
     * @return \DateTime
     */
    public function getTimecreated()
    {
        return $this->timecreated;
    }

    /**
     * @param \DateTime $timecreated
     */
    public function setTimecreated($timecreated)
    {
        $this->timecreated = $timecreated;
    }

    /**
     * @return \DateTime
     */
    public function getTimemodified()
    {
        return $this->timemodified;
    }

    /**
     * @param \DateTime $timemodified
     */
    public function setTimemodified($timemodified)
    {
        $this->timemodified = $timemodified;
    }

    /**
     * @return int
     */
    public function getUseridadd()
    {
        return $this->useridadd;
    }

    /**
     * @param int $useridadd
     */
    public function setUseridadd($useridadd)
    {
        $this->useridadd = $useridadd;
    }

    /**
     * @return int
     */
    public function getUseridedit()
    {
        return $this->useridedit;
    }

    /**
     * @param int $useridedit
     */
    public function setUseridedit($useridedit)
    {
        $this->useridedit = $useridedit;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param boolean $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }


     /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }


}
