<?php

namespace Badiu\Ams\RoleBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\CoreBundle\Model\AmsDataBase;
class RoleData  extends AmsDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
	
		
	public function getSysroleid($id) {
            $sql="SELECT sr.id AS sysroleid FROM ".$this->getBundleEntity()." o JOIN o.sysroleid sr  WHERE o.id=:id";
          
            $query = $this->getEm()->createQuery($sql);
			
            $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
            if(isset($result['sysroleid'])){$result=$result['sysroleid'];}
         
			  return  $result;
        }
}
