<?php

namespace Badiu\Ams\RoleBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
      
		   $result= $this->initDbRoleCore();
           $result+= $this->initDbRole();
		   return $result;
    } 

      public function initDbRole() {
         $cont=0;
         $dtype='course';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.role.role.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist && !$this->getForceupdate()){return 0;}
         
		  $entity=$this->getEntity();
		 $datarole = $this->getContainer()->get('badiu.system.access.role.data');
		 $studentroleid=$datarole->getIdByShortname($entity,'student');
		 $teachearroleid=$datarole->getIdByShortname($entity,'teachear');
		 $coordenatorroleid=$datarole->getIdByShortname($entity,'coordenator');
		
         $list=array(
			array('entity'=>$entity,'dtype'=>$dtype,'shortname'=>'student','sysroleid'=>$studentroleid,'lmssynckey'=>'student','name'=>$this->getTranslator()->trans('badiu.ams.role.role.student')),
			array('entity'=>$entity,'dtype'=>$dtype,'shortname'=>'teachear','sysroleid'=>$teachearroleid,'lmssynckey'=>'editingteacher','name'=>$this->getTranslator()->trans('badiu.ams.role.role.teachear')),
			array('entity'=>$entity,'dtype'=>$dtype,'shortname'=>'coordenator','sysroleid'=>$coordenatorroleid,'lmssynckey'=>'manager','name'=>$this->getTranslator()->trans('badiu.ams.role.role.coordenator')),
			
		 );
		 
		 foreach ($list as $param) {
			 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
			 
			 $paramedit=null;
			 if($this->getForceupdate()){
				 $paramedit= $param;
			 }
			 $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname,'dtype'=>$dtype);
			 $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
			 $r=$this->getUtildata()->getVaueOfArray($result,'id');
              if($r){$cont++;}
            }
			
       return $cont;
         
     }
	 
	 public function initDbRoleCore() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.system.access.role.data');
         //$exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         //if($exist){return 0;}
         
         $list=array();
         $list['student']=$this->getTranslator()->trans('badiu.ams.role.role.student');
         $list['coordenator']=$this->getTranslator()->trans('badiu.ams.role.role.coordenator');
         $list['teachear']=$this->getTranslator()->trans('badiu.ams.role.role.teachear');
       
         
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
           	
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
			}
         }
         
     }

     public function initPermissions() {
        $cont=0;
        $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');

        //student
        $param=array(
          'roleshortname'=>'student',
          'permissions'=>array('badiu.ams.my.student.frontpage')
        );
        $cont+=$libpermission->add($param);
    }
}
