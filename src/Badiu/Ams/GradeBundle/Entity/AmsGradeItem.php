<?php

namespace Badiu\Ams\GradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsGradeItem
 *  
 * @ORM\Table(name="ams_grade_item", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_grade_item_shortname_uix", columns={"entity","shortname"}),
 *      @ORM\UniqueConstraint(name="ams_grade_item_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_grade_item_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_grade_item_name_ix", columns={"name"}), 
 *              @ORM\Index(name="ams_grade_item_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="ams_grade_item_itemtype_ix", columns={"itemtype"}),
 *              @ORM\Index(name="ams_grade_item_gradetype_ix", columns={"gradetype"}),
 *              @ORM\Index(name="ams_grade_item_scaleid_ix", columns={"scaleid"}),
 *              @ORM\Index(name="ams_grade_item_itemsource_ix", columns={"itemsource"}),
 *              @ORM\Index(name="ams_grade_item_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="ams_grade_item_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsGradeItem
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

	

     /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
    
	    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;


    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;
/**
     * @var string
     *
     *@ORM\Column(name="itemtype", type="string", length=255, nullable=true)
     */
    private $itemtype='activity'; // finalgrade | activity 
    
    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='discipline'; // discipline| classe
    
/**

     * @var integer
     *
     * @ORM\Column(name="gradetype", type="integer", nullable=false)
     */

    private $gradetype=1; // 1-numeric | 2- scale

/**
     * @var AmsGradeScale
     *
     * @ORM\ManyToOne(targetEntity="AmsGradeScale")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="scaleid", referencedColumnName="id")
     * })
     */
    private $scaleid;
    /**
     * @var float
     *
     * @ORM\Column(name="maxgrade", type="float", precision=10, scale=10, nullable=false)
     */
    private $maxgrade;


    /**
     * @var float
     *
     * @ORM\Column(name="mingrade", type="float", precision=10, scale=10, nullable=false)
     */
    private $mingrade=0;


/**
     * @var string
     *
     *@ORM\Column(name="itemsource", type="string", length=255, nullable=true)
     */
    private $itemsource="lms"; //lms | manual 

	  /**
     * @var boolean
     *
     * @ORM\Column(name="overridden", type="integer", nullable=false)
     */
    private $overridden=0;
	  /**
     * @var boolean
     *
     * @ORM\Column(name="loked", type="integer", nullable=false)
     */
    private $loked=0;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
   
 
  
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getShortname() {
        return $this->shortname;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getItemtype() {
        return $this->itemtype;
    }

    public function getGradetype() {
        return $this->gradetype;
    }

    

    public function getMaxgrade() {
        return $this->maxgrade;
    }

    public function getMingrade() {
        return $this->mingrade;
    }

    public function getItemsource() {
        return $this->itemsource;
    }

    public function setItemtype($itemtype) {
        $this->itemtype = $itemtype;
    }

    public function setGradetype($gradetype) {
        $this->gradetype = $gradetype;
    }

    

    public function setMaxgrade($maxgrade) {
        $this->maxgrade = $maxgrade;
    }

    public function setMingrade($mingrade) {
        $this->mingrade = $mingrade;
    }

    public function setItemsource($itemsource) {
        $this->itemsource = $itemsource;
    }



    public function getScaleid() {
        return $this->scaleid;
    }

    public function setScaleid(AmsGradeScale $scaleid) {
        $this->scaleid = $scaleid;
    }



	  
  /**
     * @return string
     */
    public function getModulekey()
    {
        return $this->modulekey;
    }

    /**
     * @param string $dtype
     */
    public function setModulekey($modulekey)
    {
        $this->modulekey = $modulekey;
    }


     /**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string $dtype
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }

	
    public function setOverridden($overridden) {
        $this->overridden = $overridden;
    }

	
    public function getOverridden() {
        return $this->overridden;
    }

	
	    public function setLoked($loked) {
        $this->loked = $loked;
    }

	
    public function getLoked() {
        return $this->loked;
    }
 

    function getDtype() {
        return $this->dtype;
    }

  

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }


}
