<?php

namespace Badiu\Ams\GradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsGrade
 *  
 * @ORM\Table(name="ams_grade", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_grade_uix", columns={"entity", "itemid", "userid"}),
 *      @ORM\UniqueConstraint(name="ams_grade_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_grade_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_grade_itemid_ix", columns={"itemid"}), 
 *              @ORM\Index(name="ams_grade_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="ams_grade_grade_ix", columns={"grade"}),
 *              @ORM\Index(name="ams_grade_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="ams_grade_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsGrade
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

	/**
     * @var AmsGradeItem
     *
     * @ORM\ManyToOne(targetEntity="AmsGradeItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="itemid", referencedColumnName="id")
     * })
     */
    private $itemid;

	
	/**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id", nullable=false)
     * })
     */
    private $userid;

    /**
     * @var float
     *
     * @ORM\Column(name="grade", type="float", precision=10, scale=0, nullable=true)
     */
    private $grade=null;

	  /**
     * @var boolean
     *
     * @ORM\Column(name="overridden", type="integer", nullable=false)
     */
    private $overridden=0;
	  /**
     * @var boolean
     *
     * @ORM\Column(name="loked", type="integer", nullable=false)
     */
    private $loked=0;

    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    
  
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

  
    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getGrade() {
        return $this->gradet;
    }
  

    public function setGrade($grade) {
        $this->grade = $grade;
    }


	  
    public function getItemid() {
        return $this->itemid;
    }

    public function setItemid(AmsGradeItem $itemid) {
        $this->itemid = $itemid;
    }
 
 
    /**
     * @return \Badiu\System\UserBundle\Entity\SystemUser
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param \Badiu\System\UserBundle\Entity\SystemUser $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    public function setOverridden($overridden) {
        $this->overridden = $overridden;
    }

	
    public function getOverridden() {
        return $this->overridden;
    }

	
	    public function setLoked($loked) {
        $this->loked = $loked;
    }

	
    public function getLoked() {
        return $this->loked;
    }

}
