<?php

namespace Badiu\Ams\GradeBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class GradeData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }


	
   public function existUser($entity,$itemid,$userid) {
		$r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.itemid=:itemid AND o.userid=:userid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('itemid',$itemid);
			$query->setParameter('userid',$userid);
			$result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
   }
   
    public function findByUser($entity,$itemid,$userid) {
		$r=FALSE;
            $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.itemid=:itemid AND o.userid=:userid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('itemid',$itemid);
			$query->setParameter('userid',$userid);
			$result= $query->getSingleResult();
            return $result;
   }
   public function getIdByUser($entity,$itemid,$userid) {
		$r=FALSE;
            $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.itemid=:itemid AND o.userid=:userid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
	    $query->setParameter('itemid',$itemid);
	    $query->setParameter('userid',$userid);
	    $result= $query->getSingleResult();
            if(isset($result['id'])){$result=$result['id'];}
            return $result;
   }
    
    public function add($entity,$itemid,$userid,$grade,$forceupadate=true) {
                $exist=$this->existUser($entity,$itemid,$userid);
                $result=null;
                 if($exist){
                     $id=$this->getIdByUser($entity,$itemid,$userid);
                     $uparam=array('id'=>$id,'grade'=>$grade,'timemodified'=>new \DateTime());
                    if($forceupadate){ $result=$this->updateNativeSql($uparam,false);}
                 }else{
                     $iparam=array('entity'=>$entity,'userid'=>$userid,'itemid'=>$itemid,'grade'=>$grade,'overridden'=>0,'loked'=>0,'deleted'=>0,'timecreated'=>new \DateTime());
                     $result=$this->insertNativeSql($iparam,false);
                 }
               
            return $result;
   }
   //http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_function=addByParam&_service=badiu.ams.grade.grade.data&itemid=20&userid=143&grade=8
    public function addByParam() {
               $itemid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('itemid');
               $userid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('userid'); 
               $grade=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('grade'); 
              
               $badiuSession=$this->getContainer()->get('badiu.system.access.session');
                $entity=$badiuSession->get()->getEntity();
                
		$result=$this->add($entity,$itemid,$userid,$grade);
            return $result;
   }
}
