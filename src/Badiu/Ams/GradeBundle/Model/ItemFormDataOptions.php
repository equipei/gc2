<?php

namespace Badiu\Ams\GradeBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Badiu\Ams\GradeBundle\Model\DOMAINTABLE;
class ItemFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }

    public  function getItemtype(){
        $list=array();
        $list[DOMAINTABLE::$GRADE_ITEMTYPE_FINALGRADE]=$this->getTranslator()->trans('badiu.ams.grade.item.itemtype.finalgrade');
        $list[DOMAINTABLE::$GRADE_ITEMTYPE_ACTIVITY]=$this->getTranslator()->trans('badiu.ams.grade.item.itemtype.activity');
        return $list;
    }

     public  function getItemByOdisciplineid(){
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
        
        $odisciplineid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
       
        $data=$this->getContainer()->get('badiu.ams.grade.item.data');
        $param=array('moduleinstance'=>$odisciplineid,'modulekey'=>'badiu.ams.offer.discipline');
        $list=$data->getFormChoice($entity,$param);
       
        return $list;
    }
    public  function getItemByClasseid(){
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
        
        $classeid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
       
        $data=$this->getContainer()->get('badiu.ams.grade.item.data');
        $param=array('moduleinstance'=>$classeid,'modulekey'=>'badiu.ams.offer.classe');
        $list=$data->getFormChoice($entity,$param);
       
        return $list;
    }
}
