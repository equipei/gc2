<?php

namespace Badiu\Ams\GradeBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class GradeManagerLib extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function createItemFinalgrade($param) {
		return null; 
	}
    
	public function addGrade($param) {
		$classeid=$this->getUtildata()->getVaueOfArray($param,'classeid');
		$entity=$this->getEntity();
		$itemdata=$this->getContainer()->get('badiu.ams.grade.item.data');
		$gradedata=$this->getContainer()->get('badiu.ams.grade.grade.data');
		$paramfilter=array('entity'=>$entity,'modulekey'=>'badiu.ams.offer.classe','moduleinstance'=>$classeid,'itemtype'=>'finalgrade');
		
		$itemid=$itemdata->getGlobalColumnValue('id',$paramfilter); 
		if(empty($itemid)){return null;}
		

		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		$grade=$this->getUtildata()->getVaueOfArray($param,'finalgrade');
		$timecreated=$this->getUtildata()->getVaueOfArray($param,'timecreated');
		$timemodified=$this->getUtildata()->getVaueOfArray($param,'timemodified');
		
		$gparamcheckexist=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid);
		$gparamadd=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid,'grade'=>$grade,'deleted'=>0,'timecreated'=>new \DateTime());
		$gparamedit=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid,'grade'=>$grade,'timemodified'=>new \DateTime());
		$dresult=$gradedata->addNativeSql($gparamcheckexist,$gparamadd,$gparamedit);
		$rid=$this->getUtildata()->getVaueOfArray($dresult,'id');
		
		return $rid; 
	}
}
