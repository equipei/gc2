<?php

namespace Badiu\Ams\GradeBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
use Badiu\Ams\GradeBundle\Model\DOMAINTABLE;
class GradeFormat extends BadiuFormat{
     
     function __construct(Container $container) {
            parent::__construct($container);
       } 

public  function itemtype($data){
            $value="";
            $type=null;
             if (array_key_exists("itemtype",$data)){ $type=$data["itemtype"]; }
             if($type==DOMAINTABLE::$GRADE_ITEMTYPE_FINALGRADE){$value=$this->getTranslator()->trans('badiu.ams.grade.item.itemtype.finalgrade');}
              if($type==DOMAINTABLE::$GRADE_ITEMTYPE_ACTIVITY){$value=$this->getTranslator()->trans('badiu.ams.grade.item.itemtype.activity');}
	 return $value; 
    } 
}
