<?php

namespace Badiu\Ams\CurriculumBundle\Entity;
use Badiu\System\ComponentBundle\Model\Form\Cast\BadiuHourFormCast;
use Doctrine\ORM\Mapping as ORM;

/**
 * AmsCurriculumDiscipline
 *
 * @ORM\Table(name="ams_curriculum_discipline", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_curriculum_discipline_name_uix", columns={"entity", "curriculumid","disciplineid"}), 
 *      @ORM\UniqueConstraint(name="ams_curriculum_discipline_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_curriculum_discipline_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_curriculum_discipline_curriculumid_ix", columns={"curriculumid"}),
 *              @ORM\Index(name="ams_curriculum_discipline_disciplinename_ix", columns={"disciplinename"}), 
 *              @ORM\Index(name="ams_curriculum_discipline_disciplineid_ix", columns={"disciplineid"}),  
 *              @ORM\Index(name="ams_curriculum_discipline_sectionid_ix", columns={"sectionid"}),  
 *              @ORM\Index(name="ams_curriculum_discipline_deleted_ix", columns={"deleted"}), 
 *              @ORM\Index(name="ams_curriculum_discipline_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsCurriculumDiscipline
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

  
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

     /**
     * @var AmsCurriculum
     *
     * @ORM\ManyToOne(targetEntity="AmsCurriculum")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curriculumid", referencedColumnName="id")
     * })
     */
    private $curriculumid;
     /**
     * @var \Badiu\Ams\CourseBundle\Entity\AmsDiscipline
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\DisciplineBundle\Entity\AmsDiscipline")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disciplineid", referencedColumnName="id")
     * })
     */
    private $disciplineid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="disciplinename", type="string", length=255, nullable=true)
     */
    private $disciplinename;
    
    /**
     * @var AmsCurriculumSection
     *
     * @ORM\ManyToOne(targetEntity="AmsCurriculumSection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sectionid", referencedColumnName="id")
     * })
     */
    private $sectionid;
    

    /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="float", precision=10, scale=0, nullable=true)
     */
    private $sortorder;

       /**
     * @var AmsCurriculumGroup
     *
     * @ORM\ManyToOne(targetEntity="AmsCurriculumGroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="groupid", referencedColumnName="id")
     * })
     */
    private $groupid;
      /**
 * @var boolean
 *
 * @ORM\Column(name="drequired", type="integer", nullable=false)
 */
    private $drequired;

   

    /**
     *Composition of the value if the value.
     * @var integer
     *
     * @ORM\Column(name="dhour", type="integer",  nullable=true)
     */
    private $dhour;
    
      /**
      *Composition of the value if the value.        
     * @var integer
     *
     * @ORM\Column(name="credit", type="integer",  nullable=true)
     */
    private $credit;
/**
     * @var string
     *
     * @ORM\Column(name="summary", type="text", nullable=true)
     */
    private $summary;
    /**
     * @var string
     *
     * @ORM\Column(name="teachingplan", type="text", nullable=true)
     */
    private $teachingplan;
	
	
	  /**
     * @var integer
     *
     * @ORM\Column(name="equivalencereqminfversion", type="integer", nullable=true)
     */
    private $equivalencereqminfversion;  //mininum version of offer required to equivalence
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
     
     
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;


   
    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

   
    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

      public function getCategoryid() {
        return $this->curriculumid;
    }

    public function setCurriculumid(AmsCurriculum $curriculumid) {
        $this->curriculumid = $curriculumid;
    }
  
     public function getDisciplineid() {
        return $this->disciplineid;
    }

    public function setDisciplineid(\Badiu\Ams\DisciplineBundle\Entity\AmsDiscipline $disciplineid) {
        $this->disciplineid = $disciplineid;
    }

    
    
      public function getDrequired() {
        return $this->drequired;
    }

   public function setDrequired($drequired) {
        $this->drequired = $drequired;
    }
    public function getGroupid() {
        return $this->groupid;
    }

    public function setGroupid(AmsCurriculumGroup $groupid) {
        $this->groupid = $groupid;
    }

    public function getTeachingplan() {
        return $this->teachingplan;
    }

    public function setTeachingplan($teachingplan) {
        $this->teachingplan = $teachingplan;
    }

    public function getCurriculumid() {
        return $this->curriculumid;
    }

     public function getDhour()
    {
        return  $this->dhour;
     }
	
   public function setDhour($dhour)
    {
  
        $this->dhour = $dhour;
    }


public function getSummary() {
        return $this->summary;
    }

    public function setSummary($summary) {
        $this->summary = $summary;
    }
    function getCredit() {
        return $this->credit;
    }

    function setCredit($credit) {
        $this->credit = $credit;
    }

   
    function getDisciplinename() {
        return $this->disciplinename;
    }

    function setDisciplinename($disciplinename) {
        $this->disciplinename = $disciplinename;
    }

    function getSectionid(){
        return $this->sectionid;
    }

    function getSortorder() {
        return $this->sortorder;
    }

    function setSectionid(AmsCurriculumSection $sectionid) {
        $this->sectionid = $sectionid;
    }

    function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }

 function getEquivalencereqminfversion() { 
        return $this->equivalencereqminfversion;
    }

    function setEquivalencereqminfversion($equivalencereqminfversion) {
        $this->equivalencereqminfversion = $equivalencereqminfversion;
    }

}
