<?php

namespace Badiu\Ams\CurriculumBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AmsCurriculum
 *
 * @ORM\Table(name="ams_curriculum", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_curriculum_name_uix", columns={"entity", "courseid", "name"}), 
 *      @ORM\UniqueConstraint(name="ams_curriculum_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="ams_curriculum_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_curriculum_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_curriculum_name_ix", columns={"name"}), 
 *              @ORM\Index(name="ams_curriculum_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="ams_curriculum_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="ams_curriculum_courseid_ix", columns={"courseid"}),
 *              @ORM\Index(name="ams_curriculum_categoryid_ix", columns={"categoryid"}),
 *              @ORM\Index(name="ams_curriculum_courseid_ix", columns={"courseid"}),
 *              @ORM\Index(name="ams_curriculum_deleted_ix", columns={"deleted"}), 
 *              @ORM\Index(name="ams_curriculum_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsCurriculum
{
    /**  
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
       
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;


     /**
     * @var \Badiu\Ams\CourseBundle\Entity\AmsCourse
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\CourseBundle\Entity\AmsCourse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="courseid", referencedColumnName="id")
     * })
     */
    private $courseid;
    
    /**
     * @var AmsCurriculumCategory
     *
     * @ORM\ManyToOne(targetEntity="AmsCurriculumCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;
    
   
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
    
/**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='course'; //course | event
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="typesection", type="string", length=255, nullable=true)
     */
    private $typesection; //delete it
  
    
    /**
     * @var integer
     *
     * @ORM\Column(name="numbersection", type="integer", nullable=true)
     */
    private $numbersection; //delete it

 /**
     * @var boolean
     *
     * @ORM\Column(name="requiredsequence", type="integer", nullable=false)
     */
    private $requiredsequence=0;
    
     /**
     * @var boolean
     *
     * @ORM\Column(name="hasdependency", type="integer", nullable=true)
     */
    private $hasdependency=0;

  
    /**
      *Composition of the value if the value.        
     * @var integer
     *
     * @ORM\Column(name="credit", type="integer",  nullable=true)
     */
    private $credit;
    
    	  /**
     *Composition of the value if the value.
     * @var integer
     *
     * @ORM\Column(name="dhour", type="integer", nullable=true)
     */
    private $dhour;
    
	
	/**
     * @var boolean
     *
     * @ORM\Column(name="equivalencyenable", type="integer", nullable=true)
     */
    private $equivalencyenable;
	
	/**
     * @var string
     *
    * @ORM\Column(name="equivalencytype", type="string", length=50, nullable=true)
     */
    private $equivalencytype; //manual | automatic
	
	/**
     * @var string
     *
    * @ORM\Column(name="equivalencyrequiredperiod", type="string", length=255, nullable=true)
     */
    private $equivalencyrequiredperiod; 
	
	/**
     * @var boolean
     *
     * @ORM\Column(name="equivalencyenablefversion", type="integer", nullable=true)
     */
    private $equivalencyenablefversion;  // enable equivalence by offer version of discipline
	
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

	/**
     * @var string
     *
     * @ORM\Column(name="defaultimage", type="string", length=255, nullable=true)
     */
    private $defaultimage;
     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
	
	/**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
	
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function addParent(\Badiu\Ams\CourseBundle\Entity\AmsCourse $courseid) {
        $this->courseid = $courseid;
    }
    public function findParent() {
        return $this->courseid->getId();
    }
    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getCategoryid() {
        return $this->categoryid;
    }

    public function setCategoryid(AmsCurriculumCategory $categoryid) {
        $this->categoryid = $categoryid;
    }


    
     public function getCourseid() {
        return $this->courseid;
    }

    public function setCourseid(\Badiu\Ams\CourseBundle\Entity\AmsCourse $courseid) {
        $this->courseid = $courseid;
    }
    

    public function getShortname() {
        return $this->shortname;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }


    

     /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }


    public function getRequiredsequence() {
        return $this->requiredsequence;
    }

     public function setRequiredsequence($requiredsequence) {
        return $this->requiredsequence=$requiredsequence;
    }
  
   
 public function getDhour()
    {
     return  $this->dhour;
   }
	
   public function setDhour($dhour)
    {
         $this->dhour = $dhour;
   }
 

    function getCredit() {
        return $this->credit;
    }

    function setCredit($credit) {
        $this->credit = $credit;
    }

    function getHasdependency() {
        return $this->hasdependency;
    }

  

    function setHasdependency($hasdependency) {
        $this->hasdependency = $hasdependency;
    }

    function getTypesection() {
        return $this->typesection;
    }

    function getNumbersection() {
        return $this->numbersection;
    }

    function setTypesection($typesection) {
        $this->typesection = $typesection;
    }

    function setNumbersection($numbersection) {
        $this->numbersection = $numbersection;
    }

	
	    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

  function getEquivalencyenable() { 
        return $this->equivalencyenable;
    }

    function setEquivalencyenable($equivalencyenable) {
        $this->equivalencyenable = $equivalencyenable;
    }

 
  function getEquivalencytype() {
        return $this->equivalencytype;
    }

    function setEquivalencytype($equivalencytype) {
        $this->equivalencytype = $equivalencytype;
    }
	
	  function getEquivalencyrequiredperiod() {
        return $this->equivalencyrequiredperiod;
    }

    function setEquivalencyrequiredperiod($equivalencyrequiredperiod) {
        $this->equivalencyrequiredperiod = $equivalencyrequiredperiod;
    }

 function getEquivalencyenablefversion() { 
        return $this->equivalencyenablefversion;
    }

    function setEquivalencyenablefversion($equivalencyenablefversion) {
        $this->equivalencyenablefversion = $equivalencyenablefversion;
    }
	
	
	
	   function getDefaultimage() {
        return $this->defaultimage;
    }

    function setDefaultimage($defaultimage) {
        $this->defaultimage = $defaultimage;
    }
}
