<?php

namespace Badiu\Ams\CurriculumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuAmsCurriculumBundle:Default:index.html.twig', array('name' => $name));
    }
}
