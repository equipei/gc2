<?php

namespace Badiu\Ams\CurriculumBundle\Filter;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFilter;
class CurriculumFilter extends BadiuFilter {

  /**
     * @var integer
      */
    private $categoryid;

  /**
   * @return int
   */
  public function getCategoryid()
  {
    return $this->categoryid;
  }

  /**
   * @param int $categoryid
   */
  public function setCategoryid($categoryid)
  {
    $this->categoryid = $categoryid;
  }




}
