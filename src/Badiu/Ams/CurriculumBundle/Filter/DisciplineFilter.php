<?php

namespace Badiu\Ams\CurriculumBundle\Filter;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFilter;
class DisciplineFilter extends BadiuFilter {
    
  /**
     * @var integer
      */
    private $curriculumid;

    /**
     * @var string
     */
    private $disciplinename;

    /**
     * @var integer
     */
    private $groupid;


    /**
     * @var integer
     */
    private $drequired;

    /**
   * @var integer
   */
    private $dhour;

  /**
   * @var integer
   */
private $measurevalue;
    public function addParent($parentid) {
        $this->curriculumid = $parentid;
    }

    /**
     * @return int
     */
    public function getCurriculumid()
    {
        return $this->curriculumid;
    }

    /**
     * @param int $curriculumid
     */
    public function setCurriculumid($curriculumid)
    {
        $this->curriculumid = $curriculumid;
    }

    /**
     * @return string
     */
    public function getDisciplinename()
    {
        return $this->disciplinename;
    }

    /**
     * @param string $disciplinename
     */
    public function setDisciplinename($disciplinename)
    {
        $this->disciplinename = $disciplinename;
    }

    /**
     * @return int
     */
    public function getGroupid()
    {
        return $this->groupid;
    }

    /**
     * @param int $groupid
     */
    public function setGroupid($groupid)
    {
        $this->groupid = $groupid;
    }

    /**
     * @return int
     */
    public function getDrequired()
    {
        return $this->drequired;
    }

    /**
     * @param int $drequired
     */
    public function setDrequired($drequired)
    {
        $this->drequired = $drequired;
    }

  /**
   * @return int
   */
  public function getDhour()
  {
    return $this->dhour;
  }

  /**
   * @param int $dhour
   */
  public function setDhour($dhour)
  {
    $this->dhour = $dhour;
  }

  /**
   * @return int
   */
  public function getMeasurevalue()
  {
    return $this->measurevalue;
  }

  /**
   * @param int $measurevalue
   */
  public function setMeasurevalue($measurevalue)
  {
    $this->measurevalue = $measurevalue;
  }




}
