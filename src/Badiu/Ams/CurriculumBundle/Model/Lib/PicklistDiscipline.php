<?php

namespace Badiu\Ams\CurriculumBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class PicklistDiscipline extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	public function discipline(){
            $add=$this->getContainer()->get('request')->get('add');  
            $remove=$this->getContainer()->get('request')->get('rmv');  
	 
            if (isset($add)) {
		//echo 'ADD';
		print_r($add);
		$this->addDiscipline();
            }else if (isset($remove)) {
		//echo 'REMOVE';
		//print_r($remove);
                $this->removeDiscipline();
            }
	 		
    }
	
    
    public function addDiscipline(){
                        $fdata=$_GET['add'];
			$curriculumid=$this->getContainer()->get('request')->get('parentid'); 
                        $curriculum=$this->getContainer()->get('badiu.ams.curriculum.curriculum.data')->findById($curriculumid);
			$cdisciplinelib=$this->getContainer()->get('badiu.ams.curriculum.discipline.lib');
                        $cdisciplinedata=$this->getContainer()->get('badiu.ams.curriculum.discipline.data');
			foreach ($fdata as $data){
                                $disciplineid=null;
				$groupid=null;
				$required=false;
				$period=null;
				
				if(isset($data['id'])){$disciplineid=$data['id'];}
				if(isset($data['field1'])){$groupid=$data['field1'];}
				if(isset($data['field2'])){$required=$data['field2'];}
                                if(isset($data['field3'])){$period=$data['field3'];}
				
				if(!empty($disciplineid)){
					$cdiscipline=$cdisciplinelib->copyToCurriculum($disciplineid);
                                        $cdiscipline->setCurriculumid($curriculum);
                                        if(!empty($groupid)){$cdiscipline->setGroupid($this->getContainer()->get('badiu.ams.curriculum.group.data')->findById($groupid));}
                                        $cdiscipline->setDrequired($required);
                                        if(!empty($period)){$cdiscipline->setSection($period);}
					$cdisciplinedata->setDto($cdiscipline);
                                        $cdisciplinedata->save();
                                        echo "result: ".$cdisciplinedata->getDto()->getId();
                                        
				}
			}
			
			
	}
      public function removeDiscipline(){
                        $fdata=$_GET['rmv'];
			$cdisciplinedata=$this->getContainer()->get('badiu.ams.curriculum.discipline.data');
                        $curriculumid=$this->getContainer()->get('request')->get('parentid'); 
			foreach ($fdata as $disciplineid){
                               if(!empty($disciplineid)){
                                    $cdisciplinedata->removeByDiscipline($curriculumid,$disciplineid);
                                }
				
			}
			
			
	}    
      
        
}
