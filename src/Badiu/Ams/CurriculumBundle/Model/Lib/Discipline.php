<?php

namespace Badiu\Ams\CurriculumBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Discipline   extends BadiuModelLib{
   function __construct(Container $container) {
            parent::__construct($container);
              }
     
    public function copyToCurriculum($disciplineid) {
            $discipline=$this->getContainer()->get('badiu.ams.discipline.discipline.data')->findById($disciplineid);
            $dto=$this->getContainer()->get('badiu.ams.curriculum.discipline.entity');
	    $dto=$this->initDefaultEntityData($dto);
            $dto->setDisciplineid($discipline);
            $dto->setDhour($discipline->getDhour());
            $dto->setCredit($discipline->getCredit());
            $dto->setSummary($discipline->getSummary());
            $dto->setTeachingplan($discipline->getTeachingplan());
            $dto->setDescription($discipline->getDescription());
            
        return  $dto;
    }


}
