<?php

namespace Badiu\Ams\CurriculumBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class CurriculumData extends BadiuDataBase {

    function __construct(Container $container, $bundleEntity) {
        parent::__construct($container, $bundleEntity);
    }

    public function getNumsections($id) {
        $sql = "SELECT  o.numbersection FROM " . $this->getBundleEntity() . " o  WHERE o.id = :id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getSingleResult();
        $result = $result['numbersection'];

        return $result;
    }

    //change name to getTypesection
    public function getTypesections($id) {
        $sql = "SELECT  o.typesection FROM " . $this->getBundleEntity() . " o   WHERE o.id = :id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getSingleResult();
        $result = $result['typesection'];

        return $result;
    }

    //get curriculum name for menu
    public function getMenuDiscipline($id) {
        
            $curriculumdata = $this->getContainer()->get('badiu.ams.curriculum.curriculum.data');
            $value = $curriculumdata->getNameById($id);
            return $value;
       
    }

    public function getMenuCourse($id) {
       
            $disciplineData = $this->getContainer()->get('badiu.ams.curriculum.discipline.data');
            $sql = "SELECT c.id, c.name FROM " . $this->getBundleEntity() . " o  JOIN o.courseid c  WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id', $id);
            $result = $query->getSingleResult();
            return $result;
        
    }

    public function getListInsideCourse() {
        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit = $sysoperation->isEdit();
        $courseid = null;

        if ($isEdit) {
            $offeid = $this->getContainer()->get('request')->get('id');
            $offerData = $this->getContainer()->get('badiu.ams.offer.offer.data');
            $courseid = $offerData->getCourseid($offeid);
        } else {
            $courseid = $this->getContainer()->get('request')->get('parentid');
        }
        $curriculumData = $this->getContainer()->get('badiu.ams.curriculum.curriculum.data');
        $sql = "SELECT o FROM " . $curriculumData->getBundleEntity() . " o   WHERE o.courseid=:courseid AND o.deleted=:deleted";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('courseid', $courseid);
		$query->setParameter('deleted', FALSE);
        $result = $query->getResult();
        return $result;
    }

    public function getFormChoiceInsideCourse() {
        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit = $sysoperation->isEdit();
        $courseid = null;

        if ($isEdit) {
            $offeid = $this->getContainer()->get('request')->get('id');
            $offerData = $this->getContainer()->get('badiu.ams.offer.offer.data');
            $courseid = $offerData->getCourseid($offeid);
        } else {
            $courseid = $this->getContainer()->get('request')->get('parentid');
        }
        $curriculumData = $this->getContainer()->get('badiu.ams.curriculum.curriculum.data');
        $sql = "SELECT o.id,o.name FROM " . $curriculumData->getBundleEntity() . " o   WHERE o.courseid=:courseid  AND  o.deleted=:deleted";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('courseid', $courseid);
		$query->setParameter('deleted', FALSE);
        $result = $query->getResult();
        return $result;
    }

    public function existByCourse($courseid) {
        $sql = "SELECT COUNT(o.id) AS countrecord  FROM " . $this->getBundleEntity() . " o  WHERE o.courseid = :courseid";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('courseid', $courseid);
        $result = $query->getSingleResult();
        $result = $result['countrecord'];
        return $result;
    }

    public function getLastByCourse($courseid) {
        $sql = "SELECT o FROM " . $this->getBundleEntity() . " o  WHERE o.courseid = :courseid";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('courseid', $courseid);
        $result = $query->getSingleResult();
        return $result;
    }

}
