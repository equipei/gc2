<?php

namespace Badiu\Ams\CurriculumBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class DisciplineFormat extends BadiuFormat{
     
     function __construct(Container $container) {
            parent::__construct($container);
       } 

    public  function exec($data,$function=null){
            $result=null;
            if($function=='section'){$result=$this->section($data);}

            return $result;
    }              
     
    public  function section($data){
            $value="";
            if (array_key_exists("section",$data)){
                    $value=$data["section"];
                    if(!empty($value)){
                        $value=$this->getTranslator()->trans('badiu.ams.curriculum.sections.name.list',array('%number%'=>$value));
                    }
                    
            }
            
        
        return $value; 
    } 

}
