<?php

namespace Badiu\Ams\CurriculumBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class DisciplineController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
              
    public function addCheckForm($form,$data) {
           $check=true;
           if($data->existAddIdnumber()){
                $form->get('idnumber')->addError(new FormError($this->getTranslator()->trans('duplication.record.name')));
                  $check=false;
            }
           
           return  $check;
       }
 

    public function addCurriculumidToDiscipline($dto,$dtoParent) {
      $dto->setCurriculumid($dtoParent);
        return $dto;
    }
     public function findCurriculumidInDiscipline($dto) {

        return $dto->getCurriculumid()->getId();
    }
}
