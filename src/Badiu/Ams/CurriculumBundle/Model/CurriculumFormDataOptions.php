<?php

namespace Badiu\Ams\CurriculumBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Badiu\Ams\CurriculumBundle\Model\DOMAINTABLE;
class CurriculumFormDataOptions extends BadiuFormDataOptions{
     
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       } 
              
    
    public  function getTypesection(){
         $list=array();
        $list[DOMAINTABLE::$TYPE_SECTION_PERIOD]=$this->getTranslator()->trans('badiu.ams.curriculum.typesection.period');
        $list[DOMAINTABLE::$TYPE_SECTION_MODULE]=$this->getTranslator()->trans('badiu.ams.curriculum.typesection.module');
        $list[DOMAINTABLE::$TYPE_SECTION_UNIT]=$this->getTranslator()->trans('badiu.ams.curriculum.typesection.unit');
        $list[DOMAINTABLE::$TYPE_SECTION_LEVEL]=$this->getTranslator()->trans('badiu.ams.curriculum.typesection.level');
        return $list;
    } 

}
