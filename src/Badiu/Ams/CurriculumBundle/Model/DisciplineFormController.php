<?php

namespace Badiu\Ams\CurriculumBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class DisciplineFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     public function changeParamOnOpen() {
        
     }   
     
    public function changeParam() {

        //get name of discipline
        $disciplineid= $this->getParamItem('disciplineid');
        $disciplinename= $this->getParamItem('disciplinename');
        if(empty($disciplinename)){ 
            $ddata=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
            $badiuSession = $this->getContainer()->get('badiu.system.access.session');
            $entity = $badiuSession->get()->getEntity();
            $dname=$ddata->getColumnValue($entity,'name',$paramf=array('id'=>$disciplineid));
            $this->addParamItem('disciplinename',$dname);
         }
       
     }
    
}
