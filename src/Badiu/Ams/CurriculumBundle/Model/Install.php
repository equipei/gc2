<?php

namespace Badiu\Ams\CurriculumBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Install extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
       
           $result= $this->initDbCategory();
           $result+= $this->initDbGroup();
           return $result;
    } 

     public function initDbCategory() {
         $cont=0; 
         $datacategory = $this->getContainer()->get('badiu.ams.curriculum.category.data');
         $entity=$this->getEntity();
         $exist=$datacategory->countGlobalRow(array('entity'=>$entity));
         if($exist){return 0;}
         $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
         $factorykeytree->init('badiu.util.document.category.data');
         
         $dtype='course';
         
         //category add  systematic
         $param=array();
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.ams.curriculum.category.academic');
         $param['shortname']='academic';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         $param['dtype']=$dtype;
         $idpath=null;
          $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
          $level = $factorykeytree->getLevel($newidtree);
          $order = $factorykeytree->getOrder($newidtree);
          
          $parentinfo = $factorykeytree->getParentInfo($entity, $idpath, $dtype);
          $dtotree=array();
          $dtotree = $factorykeytree->makePath($parentinfo, $dtotree);
          
           $param['idpath']=$newidtree;
           $param['level']=$level;
           $param['orderidpath']=$order;
           $param['path']=$this->getUtildata()->getVaueOfArray($dtotree, 'path');
           $param['parent']=$this->getUtildata()->getVaueOfArray($dtotree, 'parent');
         if(! $datacategory->existByShortname($entity,'academic')){
              $result =  $datacategory->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         //category add free
         $param=array();
         $idpath=null;
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.ams.curriculum.category.corporate');
         $param['shortname']='corporate';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         $param['dtype']=$dtype;
         
          $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
          $level = $factorykeytree->getLevel($newidtree);
          $order = $factorykeytree->getOrder($newidtree);
          
          $parentinfo = $factorykeytree->getParentInfo($entity, $idpath, $dtype);
          $dtotree=array();
          $dtotree = $factorykeytree->makePath($parentinfo, $dtotree);
       
          
           $param['idpath']=$newidtree;
           $param['level']=$level;
           $param['orderidpath']=$order;
           $param['path']=$this->getUtildata()->getVaueOfArray($dtotree, 'path');
           $param['parent']=$this->getUtildata()->getVaueOfArray($dtotree, 'parent');
         if(! $datacategory->existByShortname($entity,'corporate')){
              $result =  $datacategory->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         
     }
     
     public function initDbGroup() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.curriculum.group.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist){return 0;}
         
         //add base
         $param=array();
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.ams.curriculum.group.base');
         $param['shortname']='base';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         $param['dtype']='course';
         if(!$data->existByShortname($entity,'base')){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
      
          //add free
         $param=array();
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.ams.curriculum.group.free');
         $param['shortname']='free';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         $param['dtype']='course';
         if(!$data->existByShortname($entity,'free')){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
     }
}
