<?php

namespace Badiu\Ams\CurriculumBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class SectionFilter extends BadiuFormFilter{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
   public function execBeforeSubmit() {

           
        } 
          
   public function execAfterSubmit() {
	   
         $id = $this->getUtildata()->getVaueOfArray($this->getParam(), 'id');
		 $numbersection = $this->getUtildata()->getVaueOfArray($this->getParam(), 'numbersection');
		 if(empty($numbersection)){return null;}
		 if($numbersection > 30){$numbersection=30;}
        $sectiondata=$this->getContainer()->get('badiu.ams.curriculum.section.data');
		for ($i = 1; $i <= $numbersection; $i++) {
			$paramcheckexist=array('entity'=>$this->getEntity(),'curriculumid'=>$id,'section'=>$i);
			$paramadd=array('entity'=>$this->getEntity(),'curriculumid'=>$id,'section'=>$i,'name'=>$i,'dtype'=>'course','deleted'=>0,'timecreated'=>new \DateTime());
			$paramedit=array('entity'=>$this->getEntity(),'curriculumid'=>$id,'section'=>$i,'name'=>$i,'dtype'=>'course','deleted'=>0,'timemodified'=>new \DateTime());
			$resultp=$sectiondata->addNativeSql($paramcheckexist,$paramadd,$paramedit);	
		}
		
         
   } 
       
    
}
