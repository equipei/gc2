<?php

namespace Badiu\Ams\CurriculumBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class
 DisciplineFormDataOptions extends BadiuFormDataOptions{
     
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       } 
              
   
     //get numsections config
    public  function getSection(){
        
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit=$sysoperation->isEdit();
      
        $numsections=0;
        $typesections='period';
        if($isEdit){
             $data=$this->getContainer()->get('badiu.ams.curriculum.discipline.data');
             $id=$this->getContainer()->get('request')->get("id");
             $numsections= $data->getCurriculumNumsections($id);
             $typesections= $data->getCurriculumTypesections($id);
        }else{
            $data=$this->getContainer()->get('badiu.ams.curriculum.curriculum.data');
            $id=$this->getContainer()->get('request')->get("parentid");
            $numsections= $data->getNumsections($id);
            $typesections=$data->getTypesections($id);
        }
        
         
        $key='badiu.ams.curriculum.sections.period.list';
            if($typesections==DOMAINTABLE::$TYPE_SECTION_PERIOD){$key='badiu.ams.curriculum.sections.period.list';}
            else if($typesections==DOMAINTABLE::$TYPE_SECTION_MODULE){$key='badiu.ams.curriculum.sections.module.list';}
            else if($typesections==DOMAINTABLE::$TYPE_SECTION_UNIT){$key='badiu.ams.curriculum.sections.unit.list';}
            else if($typesections==DOMAINTABLE::$TYPE_SECTION_LEVEL){$key='badiu.ams.curriculum.sections.level.list';}
           
         
        $list=array();
        for($i=1;$i<=$numsections;$i++){
            $list[$i]= $this->getTranslator()->trans($key,array('%number%'=>$i));
        } 
        return $list; 
    } 

}
