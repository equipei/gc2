<?php

namespace Badiu\Ams\CurriculumBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\CoreBundle\Model\AmsDataBase;
class DisciplineData  extends   AmsDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
			$this->setDtype('discipline');
              }
    
 //delte
    //get discipline without current discipline in curriculum
  /*  public function getNewsDisciplines() {
        $curriculumid=$this->getContainer()->get('request')->get('parentid'); 
        if(empty($curriculumid)) return null;

        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $disciplineData=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
      
        $sql="SELECT  o FROM ".$disciplineData->getBundleEntity()." o  WHERE  o.entity = :entity ";//AND o.id NOT IN (SELECT  d.id FROM ".$this->getBundleEntity()." cd JOIN cd.disciplineid d JOIN cd.curriculumid c WHERE c.id=:curriculumid) ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        //$query->setParameter('curriculumid',$curriculumid);
        $result= $query->getResult();
        return  $result;
    }*/

    public function getNewsDisciplines() {
        $curriculumid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
        $name=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
        if(empty($curriculumid)) return null;

        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $disciplineData=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
      
        $wsql="";
        if(!empty($name)){
            $wsql=" AND CONCAT(o.id,LOWER(o.name))  LIKE :name ";
             $name=strtolower($name);
        }
        $sql="SELECT  DISTINCT o.id,o.name  FROM ".$disciplineData->getBundleEntity()." o LEFT JOIN ".$this->getBundleEntity()." cd WITH o.id=cd.disciplineid   WHERE  o.entity = :entity AND o.dtype=:dtype $wsql  AND  (cd.curriculumid != :curriculumid OR cd.id IS NULL) "; 
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
		$query->setParameter('dtype',$this->getDtype());
        $query->setParameter('curriculumid',$curriculumid);
        if(!empty($name)){$query->setParameter('name','%'.strtolower($name).'%');}
        
        $query->setMaxResults(50);
        $result= $query->getResult();
        return  $result;
    }
    
      public function getOriginalNameById($id) {
        $data=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
        $sql = "SELECT  o.name FROM " . $data->getBundleEntity() . " o   WHERE o.id=:id";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result = $query->getOneOrNullResult();
        $result = $result['name'];
        return $result;
    }
    
    //get discipline without current discipline in curriculum
    public function getAllDisciplinesByCurriculum($curriculumid) {
        
        $sql="SELECT  o FROM ".$this->getBundleEntity()." o JOIN  o.curriculumid c WHERE c.id=:curriculumid ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('curriculumid',$curriculumid);
        $result= $query->getResult();
        return  $result;
    }

    //get curriculum name for menu
    public function getMenuCurriculum($id) {
        $curriculumdata=$this->getContainer()->get('badiu.ams.curriculum.curriculum.data');
        $value=$curriculumdata->getNameById($id);
       return  $value;
       
    }



    public function getCurriculumNumsections($id) {
        
        $sql="SELECT  c.numsections FROM ".$this->getBundleEntity()." o JOIN  o.curriculumid c WHERE o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id',$id);
        $result= $query->getSingleResult();
        return  $result['numsections'];
    }

    public function getCurriculumTypesections($id) {
        
        $sql="SELECT  ts.shortname FROM ".$this->getBundleEntity()." o JOIN  o.curriculumid c JOIN c.typesectionsid  ts WHERE o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('id',$id);
        $result= $query->getSingleResult();
        return  $result['shortname'];
    }
    
    public function removeByDiscipline($currulumid,$disciplineid) {
         $sql="DELETE  FROM ".$this->getBundleEntity()." o  WHERE o.curriculumid=:curriculumid AND o.disciplineid=:disciplineid";
         $query = $this->getEm()->createQuery($sql);
         $query->setParameter('curriculumid',$currulumid);
         $query->setParameter('disciplineid', $disciplineid);
         $result=$query->execute();
         return $result;
     
    }
}
