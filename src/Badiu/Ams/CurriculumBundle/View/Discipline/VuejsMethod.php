  curriculumCopyDisciplineDataToForm: function (disciplineid) {
			var self = this;
			var fparam={
					"_service":  "badiu.ams.discipline.discipline.lib.discipline",
					"_function":  "getDataFromDiscipline",
					"disciplineid":  disciplineid,
					<?php if($page->getIsexternalclient()){?> this.fparam._clientsession=this.clientsession;<?php }?>
				};
				
			  
			  axios.post(this.serviceurl,fparam).then(function (response) {
				    if(response.data.status=='accept'){
						console.log(response.data.message);
						self.badiuform.disciplinename=response.data.message.name;
						
						self.badiuform.description=response.data.message.description;
						if(!self.isparamempty(self.badiuform.description)){tinyMCE.get('description').setContent(response.data.message.description);}
						
						self.badiuform.summary=response.data.message.summary;
						if(!self.isparamempty(response.data.message.summary)){tinyMCE.get('summary').setContent(response.data.message.summary); }
						
						self.badiuform.teachingplan=response.data.message.teachingplan;
						if(!self.isparamempty(self.badiuform.teachingplan)){tinyMCE.get('teachingplan').setContent(response.data.message.teachingplan);}
						
						self.badiuform.credit=response.data.message.credit;
						
						//hour
						var dhour=response.data.message.dhour;
						dhour=parseInt(dhour);
						if(dhour > 0 ){
							var h=dhour/60; 
							 h=Math.floor(h);
							 var m=dhour%60;
							 
							 self.badiuform.dhour_badiutimehour=h;
							 self.badiuform.dhour_badiutimeminute=m;
						}else {
							 self.badiuform.dhour_badiutimehour='';
							 self.badiuform.dhour_badiutimeminute='';
						}
						
					
					 }else if(response.data.status=='danied'){
						
						

					 }
                }).catch(function (error) {
						
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 
  }