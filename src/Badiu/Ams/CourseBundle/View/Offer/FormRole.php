<script>
    //review return id and not shortname
//onReadCategoryid(); 
//onChangeCategoryid();

onReadServerService();
onChangeServerService();
onReadLmssynclevel();
onChangeLmssynclevel();
onChangeLmssynctype();

function onReadServerService(){
 
  $(document).ready(function() {
          var sserviceid = $("#sserviceid").val();
            if(sserviceid > 0){
            $("#lmscoursecat").attr('disabled',false);
             $("#lmscoursecatparent").attr('disabled',false);
            $("#lmscourse").attr('disabled',false);
            
          }else{
               $("#item_lmscourse").hide();
               $("#item_lmscoursecat").hide();
               $("#item_lmscoursecatparent").hide();
           
             $("#lmscoursecat").attr('disabled',true);
             $("#lmscoursecatparent").attr('disabled',true);
             $("#lmscourse").attr('disabled',true);
           }
     
    });
} 
 
function onChangeServerService(){
         $('#sserviceid').change(function() {
         var sserviceid = $("#sserviceid").val();
         
         if(sserviceid > 0){
             $("#lmscoursecat").attr('disabled',false);
             $("#lmscoursecatparent").attr('disabled',false);
            $("#lmscourse").attr('disabled',false);
          
               $("#lmscoursecat").val('');
               $("#lmscoursecatparebt").val('');
               $("#lmscourse").val('');
               
               $("#item_lmscourse").show();
               $("#item_lmscoursecat").show();
               $("#item_lmscoursecatparent").show();
           
          }else{
               $("#item_lmscourse").hide();
               $("#item_lmscoursecat").hide();
               $("#item_lmscoursecatparent").hide();
           
           
             $("#lmscoursecat").attr('disabled',true);
              $("#lmscourse").attr('disabled',true);
          
               $("#lmscoursecat").val('');
               $("#lmscourse").val('');
        }
      
      }); 
}

function onReadLmssynclevel(){
         $('#lmssynclevel').ready(function() {
         var lmssynclevel = $("#lmssynclevel").val();
         var lmssynctype = $("#lmssynctype").val();
         
         //for syncwithcreateddatainlms
         if(lmssynctype=='syncwithcreateddatainlms' && lmssynclevel=='coursecat'){
              $("#item_lmscourse").hide();
              $("#item_lmscoursecat").show();
              $("#item_lmscoursecatparent").hide();
              $("#item_lmscoursegroup").hide();
         }else  if(lmssynctype=='syncwithcreateddatainlms' && lmssynclevel=='course'){
              $("#item_lmscourse").show();
              $("#item_lmscoursecat").show();
              $("#item_lmscoursecatparent").hide();
              $("#item_lmscoursegroup").hide();
              $("#item_lmscoursegroup").hide();
         }else  if(lmssynctype=='syncwithcreateddatainlms' && lmssynclevel=='group'){
              $("#item_lmscourse").show();
              $("#item_lmscoursecat").show();
              $("#item_lmscoursecatparent").hide();
              $("#item_lmscoursegroup").hide();
              $("#item_lmscoursegroup").show();
         } 
         
         //for replicationdatainlms
          if(lmssynctype=='replicationdatainlms' && lmssynclevel=='coursecat'){
                $("#item_lmscourse").hide();
                $("#item_lmscoursecat").hide();
                $("#item_lmscoursecatparent").show();
                $("#item_lmscoursegroup").hide();
                $("#item_lmscoursegroup").hide();
         }else  if(lmssynctype=='replicationdatainlms' && lmssynclevel=='course'){
               $("#item_lmscourse").hide();
               $("#item_lmscoursecat").hide();
               $("#item_lmscoursecatparent").show();
               $("#item_lmscoursegroup").hide();
         } else  if(lmssynctype=='replicationdatainlms' && lmssynclevel=='group'){
               $("#item_lmscourse").show();
               $("#item_lmscoursecat").show();
               $("#item_lmscoursecatparent").hide();
               $("#item_lmscoursegroup").hide();
         }
 }); 

}

function onChangeLmssynclevel(){
         $('#lmssynclevel').change(function() {
         var lmssynclevel = $("#lmssynclevel").val();
         var lmssynctype = $("#lmssynctype").val();
         
         //for syncwithcreateddatainlms
         if(lmssynctype=='syncwithcreateddatainlms' && lmssynclevel=='coursecat'){
              $("#item_lmscourse").hide();
              $("#item_lmscoursecat").show();
              $("#item_lmscoursecatparent").hide();
              $("#item_lmscoursegroup").hide();
         }else  if(lmssynctype=='syncwithcreateddatainlms' && lmssynclevel=='course'){
              $("#item_lmscourse").show();
              $("#item_lmscoursecat").show();
              $("#item_lmscoursecatparent").hide();
              $("#item_lmscoursegroup").hide();
         }else  if(lmssynctype=='syncwithcreateddatainlms' && lmssynclevel=='group'){
              $("#item_lmscourse").show();
              $("#item_lmscoursecat").show();
              $("#item_lmscoursecatparent").hide();
              $("#item_lmscoursegroup").show();
         }  
         //for replicationdatainlms
          if(lmssynctype=='replicationdatainlms' && lmssynclevel=='coursecat'){
                $("#item_lmscourse").hide();
                $("#item_lmscoursecat").hide();
                $("#item_lmscoursecatparent").show();
                $("#item_lmscoursegroup").hide();
         }else  if(lmssynctype=='replicationdatainlms' && lmssynclevel=='course'){
               $("#item_lmscourse").hide();
               $("#item_lmscoursecat").hide();
               $("#item_lmscoursecatparent").show();
               $("#item_lmscoursegroup").hide();
         }else  if(lmssynctype=='replicationdatainlms' && lmssynclevel=='course'){
               $("#item_lmscourse").show();
               $("#item_lmscoursecat").show();
               $("#item_lmscoursecatparent").hide();
               $("#item_lmscoursegroup").hide();
         } 
 }); 

}

function onReadCategoryid(){
 
  $(document).ready(function() {
          var categoryid = $("#categoryid").val();
           if(categoryid=='free'){
             $("#item_curriculumid").hide();
         
          }else{
            $("#item_curriculumid").show();
          }
     
    });
} 


function onChangeCategoryid(){
         $('#categoryid').change(function() {
         var categoryid = $("#categoryid").val();
         alert(categoryid);
          if(categoryid=='free'){
             $("#item_curriculumid").hide();
         
          }else{
            $("#item_curriculumid").show();
          }
      
      }); 
}


function onChangeLmssynctype(){
         $('#lmssynctype').change(function() {
         var lmssynclevel = $("#lmssynclevel").val();
         var lmssynctype = $("#lmssynctype").val();
         
         //for syncwithcreateddatainlms
         if(lmssynctype=='syncwithcreateddatainlms' && lmssynclevel=='coursecat'){
              $("#item_lmscourse").hide();
                $("#item_lmscoursecat").show();
                 $("#item_lmscoursecatparent").hide();
         }else  if(lmssynctype=='syncwithcreateddatainlms' && lmssynclevel=='course'){
              $("#item_lmscourse").show();
              $("#item_lmscoursecat").hide();
               $("#item_lmscoursecatparent").hide();
         } 
         //for replicationdatainlms
          if(lmssynctype=='replicationdatainlms' && lmssynclevel=='coursecat'){
                $("#item_lmscourse").hide();
                $("#item_lmscoursecat").hide();
               $("#item_lmscoursecatparent").show();
         }else  if(lmssynctype=='replicationdatainlms' && lmssynclevel=='course'){
               $("#item_lmscourse").hide();
               $("#item_lmscoursecat").hide();
               $("#item_lmscoursecatparent").show();
         } 
 }); 

}
</script>