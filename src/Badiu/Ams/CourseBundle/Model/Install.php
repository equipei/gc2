<?php

namespace Badiu\Ams\CourseBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Install extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
       
           $result= $this->initDbCategory();
    } 

     public function initDbCategory() {
         $cont=0; 
         $datacategory = $this->getContainer()->get('badiu.ams.course.category.data');
         $entity=$this->getEntity();
        
         $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
         $factorykeytree->init('badiu.util.document.category.data');
         
         $dtype='course';
         
         //category add  systematic
         $param=array();
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.ams.course.category.systematic');
         $param['shortname']='systematic';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         $param['dtype']=$dtype;
         $idpath=null;
          $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
          $level = $factorykeytree->getLevel($newidtree);
          $order = $factorykeytree->getOrder($newidtree);
          
          $parentinfo = $factorykeytree->getParentInfo($entity, $idpath, $dtype);
          $dtotree=array();
          $dtotree = $factorykeytree->makePath($parentinfo, $dtotree);
          
           $param['idpath']=$newidtree;
           $param['level']=$level;
           $param['orderidpath']=$order;
           $param['path']=$this->getUtildata()->getVaueOfArray($dtotree, 'path');
           $param['parent']=$this->getUtildata()->getVaueOfArray($dtotree, 'parent');
         if(! $datacategory->existByShortname($entity,'systematic')){
              $result =  $datacategory->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         //category add free
         $param=array();
         $idpath=null;
         $param['entity']=$entity;
         $param['name']=$this->getTranslator()->trans('badiu.ams.course.category.free');
         $param['shortname']='free';
         $param['timecreated']=new \DateTime();
         $param['deleted']=0;
         $param['dtype']=$dtype;
         
          $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
          $level = $factorykeytree->getLevel($newidtree);
          $order = $factorykeytree->getOrder($newidtree);
          
          $parentinfo = $factorykeytree->getParentInfo($entity, $idpath, $dtype);
          $dtotree=array();
          $dtotree = $factorykeytree->makePath($parentinfo, $dtotree);
       
          
           $param['idpath']=$newidtree;
           $param['level']=$level;
           $param['orderidpath']=$order;
           $param['path']=$this->getUtildata()->getVaueOfArray($dtotree, 'path');
           $param['parent']=$this->getUtildata()->getVaueOfArray($dtotree, 'parent');
         if(! $datacategory->existByShortname($entity,'free')){
              $result =  $datacategory->insertNativeSql($param,false); 
              if($result){$cont++;}
         }
         
     }
}
