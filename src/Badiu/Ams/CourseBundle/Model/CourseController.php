<?php

namespace Badiu\Ams\CourseBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class CourseController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
              

     public function addCurseidToCurriculum($dto,$dtoParent) {
        $dto->setCourseid($dtoParent);
        return $dto;
    }


     public function findCurseidInCurriculum($dto) {

        return $dto->getCourseid()->getId();
    }


    public function findCurseidInOffer($dto) {
        return $dto->getCurriculumid()->getCourseid()->getId();
    } 

    public function addCurseidToOffer($dto,$dtoParent) {
        $dto->getCurriculumid()->setCourseid($dtoParent);
        return $dto;
    }

}
