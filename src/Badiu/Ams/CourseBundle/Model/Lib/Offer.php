<?php

namespace Badiu\Ams\CourseBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Badiu\Ams\CoreBundle\Model\DOMAINTABLE;
class Offer extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
          	public function getDataFormEdit($offerid) {
			$fdata=array();
                    	 $fdata=$this->getOfferDb($offerid,$fdata);
			return $fdata;
	}
	public function save($data) {
         
			$sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
			$isEdit=$sysoperation->isEdit();
			$offerid=null;
			if($isEdit){
				$offerid=$this->getContainer()->get('request')->get('id');
			}
			$dto=$data->getDto();
			
			$data->setDto($this->getOfferForm($offerid,$dto));
			$data->save();
		
			return $data->getDto();
		}
	public function getOfferDb($offerid,$fdata) {
			
                   
			$data=$this->getContainer()->get('badiu.ams.offer.offer.data');
                        $dto=$data->findById($offerid);
                   
			
                        $fdata['name']=$dto->getName();
                        $fdata['shortname']=$dto->getShortname();
                        $fdata['idnumber']=$dto->getIdnumber();
			$fdata['param']=$dto->getParam();
			$fdata['description']=$dto->getDescription();
                        
                        if(!empty($dto->getCurriculumid())) {$fdata['curriculumid']=$dto->getCurriculumid()->getId();}
                        if(!empty($dto->getCategoryid())) {$fdata['categoryid']=$dto->getCategoryid()->getId();}
                        if(!empty($dto->getTypeid())) {$fdata['typeid']=$dto->getTypeid()->getId();}
                        if(!empty($dto->getStatusid())) {$fdata['statusid']=$dto->getStatusid()->getId();}
                        
                        $fdata['timestart']=$dto->getTimestart();
                        $fdata['timeend']=$dto->getTimeend();
                        
                        if(!empty($dto->getEnterpriseid())) {$fdata['enterpriseid']=$dto->getEnterpriseid()->getId();}
                         if(!empty($dto->getHedificeid())){$fdata['hedificeid']=$dto->getHedificeid()->getId();}
                        if(!empty($dto->getSserviceid())) {$fdata['sserviceid']=$dto->getSserviceid()->getId();}
                        
                        $fdata['lmssynctype']=$dto->getLmssynctype();
                        $fdata['lmscontextid']=$dto->getLmscontextid();
                        $mdlcontextname=$this->getRemoteLmscontextid($dto);
                        if(!empty($mdlcontextname)){$fdata['lmscontextid']=$mdlcontextname;}
                        
                        //fianc
                        $fdata['productintegration']=$dto->getProductintegration();
                        $fdata['costcenterintegration']=$dto->getCostcenterintegration();
                        $fdata['projectintegration']=$dto->getProjectintegration();
                        $fdata['contracttemplateintegration']=$dto->getContracttemplateintegration();
                      
                       $fdata= $this->getContratTemplate($fdata,$dto);
                     // print_r($fdata);exit;
			return $fdata;
		
	}
	
	public function getOfferForm($offerid,$fdata) {
                     
                        $dto=null;
                        if(!empty($offerid)){
				$data=$this->getContainer()->get('badiu.ams.offer.offer.data');
				$dto=$data->findById($offerid);
                               
			}else{
				$dto=$this->getContainer()->get('badiu.ams.offer.offer.entity');
				$dto=$this->initDefaultEntityData($dto);
                        }
		
                         if(isset($fdata['name'])) {$dto->setName($fdata['name']);}
			if(isset($fdata['shortname'])) {$dto->setShortname($fdata['shortname']);}
			if(isset($fdata['description'])) {$dto->setDescription($fdata['description']);}
                       
                        if(isset($fdata['curriculumid'])) {$dto->setCurriculumid($this->getContainer()->get('badiu.ams.curriculum.curriculum.data')->findById($fdata['curriculumid']));}
                        if(isset($fdata['categoryid'])) {$dto->setCategoryid($this->getContainer()->get('badiu.ams.offer.category.data')->findById($fdata['categoryid']));}
                        if(isset($fdata['typeid'])) {$dto->setTypeid($this->getContainer()->get('badiu.ams.offer.type.data')->findById($fdata['typeid']));}
                        if(isset($fdata['statusid'])) {$dto->setStatusid($this->getContainer()->get('badiu.ams.offer.status.data')->findById($fdata['statusid']));}
                        
                        if(!empty($fdata['timestart'])){$dto->setTimestart($fdata['timestart']);}
                        if(!empty($fdata['timeend'])){$dto->setTimeend($fdata['timeend']);}
			
                        if(isset($fdata['enterpriseid'])) {$dto->setEnterpriseid($this->getContainer()->get('badiu.admin.enterprise.enterprise.data')->findById($fdata['enterpriseid']));}
                        if(isset($fdata['hedificeid'])) {$dto->setHedificeid($this->getContainer()->get('badiu.util.housing.edifice.data')->findById($fdata['hedificeid']));}
                        if(isset($fdata['sserviceid'])) {$dto->setSserviceid($this->getContainer()->get('badiu.admin.server.service.data')->findById($fdata['sserviceid']));}
                        if(!empty($fdata['lmssynctype'])){$dto->setLmssynctype($fdata['lmssynctype']);}
                        if(!empty($fdata['lmscontextid'])){$dto->setLmscontextid($fdata['lmscontextid']);}
                        
                        $dto->setLmscontexttypeparent(DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY);
                        $dto->setLmscontexttype(DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY);
                       //badiu.system.core.util.autocomplete
                        
                        if($fdata['productintegration']>=0){$dto->setProductintegration($fdata['productintegration']);}
                        if($fdata['costcenterintegration']>=0){$dto->setCostcenterintegration($fdata['costcenterintegration']);}
                        if($fdata['projectintegration']>=0){$dto->setProjectintegration($fdata['projectintegration']);}
                        if($fdata['contracttemplateintegration']>=0){$dto->setContracttemplateintegration($fdata['contracttemplateintegration']);}
                         
                         
                        if(isset($fdata['idnumber'])) {$dto->setIdnumber($fdata['idnumber']);}
			if(isset($fdata['param'])) {$dto->setParam($fdata['param']);}
			
                        
                        $param=array();
                        //integration financ
                        $contract=array();
                        $contract['totalvalue']=$fdata['contracttotalvalue'];
                        $contract['discountvalue']=$fdata['contractdiscountvalue'];
                        $contract['billingrecurrent']=$fdata['contractbillingrecurrent'];
                        $contract['billingportion']=$fdata['contractbillingportion'];
                        $contract['paygateway']=$fdata['contractpaygateway'];
                        $contract['paymethod']=$fdata['contractpaymethod'];
                        $contract['billingday']=$fdata['contractbillingday'];
                       
                        
                        $crontacttemplateconf=json_encode($contract);
                        $dto->setContracttemplateconfig($crontacttemplateconf);
                        
			return $dto;
		
	} 
        
   public function getRemoteLmscontextid($dto) {
       if(empty($dto->getSserviceid())){return $dto->getLmscontextid();}
       if($dto->getLmscontexttype()==DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY && !empty($dto->getLmscontextid()) ){
           
              $param=array('_serviceid'=>$dto->getSserviceid(),'id'=>$dto->getLmscontextid(),'key'=>'course.category.getname');
              $result=  $this->getSearch()->searchWebService($param);
              if($result['status']=='accept'){
                  return $dto->getLmscontextid().' - '.$result['message'];
              }
             
       }
       return null;
   }     
     
   
      public function getContratTemplate($fdata,$dto) {
          
          if(empty($dto->getContracttemplateid())){return $fdata;}
          $pdata=$this->getContainer()->get('badiu.admin.contract.template.data');
          $pdto=$pdata->findById($dto->getContracttemplateid());
          if(!empty($pdto)){
              
               $fdata['contracttotalvalue']=$pdto->getTotalvalue();
               $fdata['contractdiscountvalue']=$pdto->getDiscountvalue();
               $fdata['contractbillingrecurrent']=$pdto->getBillingrecurrent();
               $fdata['contractbillingportion']=$pdto->getBillingportion();
               $fdata['contractpaygateway']=$pdto->getPaygateway();
               $fdata['contractpaymethod']=$pdto->getPaymethod();
               $fdata['contractbillingday']=$pdto->getBillingday();
               
          }
        
          return $fdata;
      }
}
