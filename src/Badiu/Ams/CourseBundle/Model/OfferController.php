<?php

namespace Badiu\Ams\CourseBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormError;
class OfferController extends BadiuController
{

    function __construct(Container $container) {
            parent::__construct($container);
              }
              
  

       public function setDefaultDataOnOpenEditForm($dto) {
                   $lib=$this->getContainer()->get('badiu.ams.course.offer.lib');
		   $id=$this->getContainer()->get('request')->get('id');
		   $dto=$lib->getDataFormEdit($id);
                  
		  return $dto;
     }
	 public function save($data) {
                $lib=$this->getContainer()->get('badiu.ams.course.offer.lib');
		$data->setDto($lib->save($data));
                $this->setParentid($data->getDto()->getCurriculumid()->getCourseid()->getId());
                
                $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
		/*
                $isEdit=$sysoperation->isEdit();
		if(!$isEdit){
                    $offerlib=$this->getContainer()->get('badiu.ams.offer.offer.lib');
                     $offerlib->copyCurriculum($data->getDto());
                }*/
                return $data->getDto();
    }   

 


	
}
