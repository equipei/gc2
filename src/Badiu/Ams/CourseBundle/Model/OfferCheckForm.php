<?php

namespace Badiu\Ams\CourseBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuCheckForm;

class OfferCheckForm extends BadiuCheckForm {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function validation() {
        
        return null;
    }
}
