<?php

namespace Badiu\Ams\CourseBundle\Filter;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFilter;
class CourseFilter extends BadiuFilter {
    
  /**
     * @var integer
      */
    private $categoryid;

  /**
   * @var integer
   */
  private $levelid;
  /**
   * @return int
   */
  public function getCategoryid()
  {
    return $this->categoryid;
  }

  /**
   * @param int $categoryid
   */
  public function setCategoryid($categoryid)
  {
    $this->categoryid = $categoryid;
  }

  /**
   * @return int
   */
  public function getLevelid()
  {
    return $this->levelid;
  }

  /**
   * @param int $levelid
   */
  public function setLevelid($levelid)
  {
    $this->levelid = $levelid;
  }




}
