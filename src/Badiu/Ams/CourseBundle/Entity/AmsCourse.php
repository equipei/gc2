<?php

namespace Badiu\Ams\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsCourse
 *
 * @ORM\Table(name="ams_course", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_course_name_uix", columns={"entity", "name"}), 
 *      @ORM\UniqueConstraint(name="ams_course_shortname_uix", columns={"entity", "shortname"}), 
 *      @ORM\UniqueConstraint(name="ams_course_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_course_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_course_name_ix", columns={"name"}), 
 *              @ORM\Index(name="ams_course_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="ams_course_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="ams_course_deleted_ix", columns={"deleted"}), 
 *              @ORM\Index(name="ams_course_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsCourse
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
    /**
     * @var AmsCourseCategory
     *
     * @ORM\ManyToOne(targetEntity="AmsCourseCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

      /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;



    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='course'; // course |training| event

   /**
     * @var boolean
     *
     * @ORM\Column(name="withcurriculum", type="integer", nullable=true)
     */
    private $withcurriculum=true;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getCategoryid() {
        return $this->categoryid;
    }

    public function setCategoryid(AmsCourseCategory $categoryid) {
        $this->categoryid = $categoryid;
    }


    public function getShortname() {
        return $this->shortname;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }

/**
     * @return boolean
     */
     function getWithcurriculum()
    {
        return $this->withcurriculum;
    }

    /**
     * @param boolean $withcurriculum
     */
    public function setWithcurriculum($withcurriculum)
    {
        $this->withcurriculum = $withcurriculum;
    }
}
