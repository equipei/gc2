<?php

namespace Badiu\Ams\CourseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuAmsCourseBundle:Default:index.html.twig', array('name' => $name));
    }
}
