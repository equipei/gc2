<?php

namespace Badiu\Ams\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BadiuAmsCoreBundle:Default:index.html.twig');
    }
}
