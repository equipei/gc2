<?php

namespace Badiu\Ams\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuTreeDataBase;
class AmsTreeDataBase  extends BadiuTreeDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
     
   public function getFormChoice($entity,$param=array(),$orderby="") {
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id,o.name FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.dtype = :dtype $wsql $orderby";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('dtype','course');
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return  $result;
    }
 

 public function findByEntity($entity,$orderby="",$deleted=false) {
             $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity  AND o.dtype = :dtype AND o.deleted=:deleted $orderby";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
             $query->setParameter('dtype','course');
             $query->setParameter('deleted',$deleted);
            $result= $query->getResult();
            return  $result;
        }
        
        
 public function getFormChoiceParent($param=array('dtype'=>'course')) {
        $result= parent::getFormChoiceParent($param);
        return  $result;
 }
 public function getFormChoiceParentKeyId($param=array('dtype'=>'course','typeofkey'=>'id')) {
        $result=parent::getFormChoiceParent($param);
        return $result;
}
 public function getFormChoiceParentOnly($param=array('dtype'=>'course','fullpath'=>false,'outdata'=>'pkwithparent')) {
        $result= parent::getFormChoiceParent($param);
        return  $result;
 }
 public function getFormChoiceParentLevel1($param=array('dtype'=>'course','level'=>0)) {
        $result= parent::getFormChoiceParent($param);
        return  $result;
 }
  public function  getFormChoiceDefault($param=array('dtype'=>'course')) {
        $result= parent::getFormChoiceDefault($param);
        return  $result;
 }
 public function  getFormChoiceDefaultEntity($param=array('dtype'=>'course','outdata'=>'entity')) {
        $result= parent::getFormChoiceDefault($param);
        return  $result;
 }
  public function  getFormChoiceLastKeyFirst($param=array('dtype'=>'course')) {
        $result= parent::getFormChoiceDefault($param);
        return  $result;
 }
 
  public function getFormChoiceParentFirstLevel($param=array('dtype'=>'course','level'=>0)) {
        $result=parent::getFormChoiceParent($param);
        return $result;
}

  public function getFormChoiceDefaultFirstLevel($param=array('dtype'=>'course','level'=>0)) {
        $result=parent::getFormChoiceDefault($param);
        return $result;
}
}
