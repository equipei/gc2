<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBaseJson;

class LmsMoodleIntegration extends BadiuDataBaseJson{
    private $dto;
	private $systemshorname;
    function __construct(Container $container) {
            parent::__construct($container);
			
			$this->systemshorname="";
				if($this->getContainer()->hasParameter('badiu.system.core.instance.shortname')){
						$systemshorname=$this->getContainer()->getParameter('badiu.system.core.instance.shortname');
						if(!empty($systemshorname)){$this->systemshorname=$systemshorname.'.';}
					
					}
              }
    
       public function init() {
          parent::init();
       
         if(!empty($this->getId())){
              $this->dto=$this->getData()->findById($this->getId());
            }
       
     }      
        function offer() {
            $this->syncCourse();
            $this->syncCoursecat();
          
        }
        function section() {
            $this->syncCourse('section');
            $this->syncCoursecat('section');
          
        }
        function discipline() {
            $this->syncCourse('discipline');
            $this->syncCoursecat('discipline');
            $this->syncGroup('discipline');
        }
           function classe() {
            $this->syncCourse('classe');
            $this->syncGroup('classe');
        }
        function getServerInfo($lmsintegration) {
                $sserverdata=$this->getContainer()->get('badiu.admin.server.service.data');
                $sserviceid = $this->getUtildata()->getVaueOfArray($lmsintegration, 'sserviceid');
                $sserverinfo=$sserverdata->getInfoOfServiceForDs($sserviceid);
                $lmsintegration['sservicetype']=$this->getUtildata()->getVaueOfArray($sserverinfo, 'dtype');
                $lmsintegration['sservicename']=$this->getUtildata()->getVaueOfArray($sserverinfo, 'name');
                $lmsintegration['sserviceurl']=$this->getUtildata()->getVaueOfArray($sserverinfo, 'url');
                return $lmsintegration;
        }
        
        
         function syncCoursecat($amslevel='offer') {
            $this->findParam();
                       
            $lmsintegration=$this->getUtildata()->getVaueOfArray($this->getParam(), 'lmsintegration');
            $lmssynclevel = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynclevel');
        
            if($lmssynclevel!='coursecat'){return null;}
          
            $sserviceid = $this->getUtildata()->getVaueOfArray($lmsintegration, 'sserviceid');
            if(empty($sserviceid)){return null;}
            
            $lmssynctype = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynctype');
            $lmscoursecatparent = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatparent');
           
		   
           
           $mdldatautil=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodledatautil');
           $mdldatautil->getWebservice()->setServiceid($sserviceid);

			$lmscourseidtemplate=$this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseidtemplate');
				if(!empty($lmscourseidtemplate)){
					$coursetemplateinfo=$mdldatautil->getCourse($lmscourseidtemplate);
					$lmsintegration['lmscoursenametemplate']=$this->getUtildata()->getVaueOfArray($coursetemplateinfo, 'name');
				}
         
            if($lmssynctype=='syncwithcreateddatainlms'){
            
                $lmscoursecat = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatid');
                if(empty($lmscoursecat)){return null;}
                
                $coursecatinfo=$mdldatautil->getCoursecat($lmscoursecat);
                $lmsintegration['lmscoursecatid']=$lmscoursecat;
                $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($coursecatinfo, 'name');
                $lmsintegration['lmscoursecatparent']=$lmscoursecatparent;
                $lmsintegration['status']='active';
				
				$lmssyncforceupdate=$this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssyncforceupdate');
				$lmsintegration['lmssyncforceupdate']=$lmssyncforceupdate;
				
				
				
				
				
				if($lmssyncforceupdate){
					$this->init();
					$newname=$this->getDto()->getName();
					if($amslevel=='offer'){ $newname=$this->getDto()->getCourseid()->getName()."/".$this->getDto()->getName();}
					else  if($amslevel=='discipline'){$newname=$this->getDto()->getDisciplinename();}
					else  if($amslevel=='section'){$newname=$this->getDto()->getName();}
					else  if($amslevel=='tmsdiscipinecategory'){$newname=$this->getDto()->getName();}
					$lmscoursecatname=$this->getUtildata()->getVaueOfArray($coursecatinfo, 'name');
					if($lmscoursecatname!=$newname){
						//change name
						$paramu=array('_serviceid'=>$sserviceid,'_key'=>'course.category.update','id'=>$lmscoursecat,'name'=>$newname);
						$uresult=$this->processUpdate($paramu);
						$ustatus=$this->getUtildata()->getVaueOfArray($uresult, 'status');
						if($ustatus=='accept'){$lmsintegration['lmscoursecatname']=$newname;}
					}
					
				}
				
				//print_r($lmsintegration);exit;
             }
          else if($lmssynctype=='replicationdatainlms'){
               $search=$this->getContainer()->get('badiu.system.core.functionality.search');
			  
                   $this->init();
                   $name=null;
				   $idnumber=$this->getDto()->getId();
                   if($amslevel=='offer'){
					            $name=$this->getDto()->getCourseid()->getName()."/".$this->getDto()->getName();
					            $idnumber='100.'.$this->getDto()->getId();
					          }
                   else  if($amslevel=='discipline'){
					            $name=$this->getDto()->getDisciplinename();
					            $idnumber='300.'.$this->getDto()->getId();
					          }
                   else  if($amslevel=='section'){
					            $name=$this->getDto()->getName();
					            $idnumber='200.'.$this->getDto()->getId();
					         }
                   else  if($amslevel=='tmsdiscipinecategory'){
                    $name=$this->getDto()->getName();
                    $idnumber='500.'.$this->getDto()->getId();
                  
                 }
				 $idnumber.=$this->systemshorname;
                    //$shortname=$this->getDto()->getShortname();
					//$shortname=$this->getDto()->getId();
					 $cidnumber=$this->getDto()->getIdnumber();
				    if(!empty($cidnumber)){$idnumber=$cidnumber;}
					
                    $categoryid=0;
                   if(!empty($lmscoursecatparent)){$categoryid=$lmscoursecatparent;}
                   
                   //check if course exist
                     $parami=array('_serviceid'=>$sserviceid,'_key'=>'course.category.create','parentid'=>$categoryid,'name'=>$name,'idnumber'=>$idnumber);
                 
                    $iresult=$search->execWebService($parami);
                   
                    $istatus=$this->getUtildata()->getVaueOfArray($iresult, 'status');
                    $imessage=$this->getUtildata()->getVaueOfArray($iresult, 'message');
                    $info=$this->getUtildata()->getVaueOfArray($iresult, 'info');
               
                    if($istatus=='accept' && !empty($imessage) ){
                        $coursecatinfo=$mdldatautil->getCoursecat($imessage);
                        $coursecatparentinfo=null;
                        
                        $lmsintegration['lmscoursecatid']=$this->getUtildata()->getVaueOfArray($coursecatinfo, 'id');
                        $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($coursecatinfo, 'name');
                        $lmsintegration['lmscoursecatshortname']=$this->getUtildata()->getVaueOfArray($coursecatinfo, 'idnumber');
                        $lmsintegration['status']='active';
                        $lmsintegration['lmssynctype']='syncwithcreateddatainlms';
                        $lmsintegration['lmssynctypeoriginal']='replicationdatainlms';
                     
               } else if($istatus=='danied' && $info=='badiu.moodle.ws.error.param.idnumberjustexist' && !empty($shortname) ){
                  
                        $coursecatinfo=$mdldatautil->getCoursecat($shortname,true);
                        $coursecatparentinfo=null;
                   
                        
                        $lmsintegration['lmscoursecatid']=$this->getUtildata()->getVaueOfArray($coursecatinfo, 'id');
                        $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($coursecatinfo, 'name');
                        $lmsintegration['lmscoursecatshortname']=$this->getUtildata()->getVaueOfArray($coursecatinfo, 'idnumber');
                        $lmsintegration['status']='active';
                        $lmsintegration['lmssynctype']='syncwithcreateddatainlms';
                        $lmsintegration['lmssynctypeoriginal']='replicationdatainlms';
                        
               } 
          } 
       
          $param=$this->getParam();
          $param['lmsintegration']=$lmsintegration;
          
          $this->setParam($param);
          $this->updateParam();
		  $this->updateServiceSyncData($param);
         
        }
       
        
        function syncCourse($amslevel='offer') {
            $this->findParam();
           
            $lmsintegration=$this->getUtildata()->getVaueOfArray($this->getParam(), 'lmsintegration');
            $lmssynclevel = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynclevel');
            if($lmssynclevel!='course'){return null;}
            
            $sserviceid = $this->getUtildata()->getVaueOfArray($lmsintegration, 'sserviceid');
            if(empty($sserviceid)){return null;}
            
            $lmssynctype = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynctype');
            //$lmscoursecatparent = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatparent');
			$lmscoursecatparent = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatparentid');
           
		   $mdldatautil=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodledatautil');
           $mdldatautil->getWebservice()->setServiceid($sserviceid);

			$lmscourseidtemplate=$this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseidtemplate');
			if(!empty($lmscourseidtemplate)){
					$coursetemplateinfo=$mdldatautil->getCourse($lmscourseidtemplate);
					$lmsintegration['lmscoursenametemplate']=$this->getUtildata()->getVaueOfArray($coursetemplateinfo, 'name');
			}else if($lmssynctype=='replicationdatabycoursetemplateinlms'){
				$lmssynctype='replicationdatainlms';
			}
				
           
            if($lmssynctype=='syncwithcreateddatainlms'){
                $lmscourse = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseid');
                if(empty($lmscourse)){return null;}
                
                $courseinfo=$mdldatautil->getCourse($lmscourse);
				
                $lmsintegration['lmscoursecatid']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatid');
                $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatname');
                $lmsintegration['lmscoursecatparent']=$lmscoursecatparent;
                $lmsintegration['lmscourseid']=$lmscourse;
                $lmsintegration['lmscoursename']=$this->getUtildata()->getVaueOfArray($courseinfo, 'name');
                $lmsintegration['lmscourseshortname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'shortname');
                $lmsintegration['status']='active';
				
				$lmssyncforceupdate=$this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssyncforceupdate');
				$lmsintegration['lmssyncforceupdate']=$lmssyncforceupdate;
				
				
				if($lmssyncforceupdate){
					$this->init();
					$newname=$this->getDto()->getName();
					if($amslevel=='offer'){ $newname=$this->getDto()->getCourseid()->getName()."/".$this->getDto()->getName();}
					else  if($amslevel=='discipline'){$newname=$this->getDto()->getDisciplinename();}
					else  if($amslevel=='section'){$newname=$this->getDto()->getName();}
					else  if($amslevel=='classe'){
						$newname=$this->getDto()->getName();
						$joindisciplinenameinclassname=$this->getUtildata()->getVaueOfArray($lmsintegration, 'joindisciplinenameinclassname');
						if($joindisciplinenameinclassname){$newname=$this->getDto()->getOdisciplineid()->getDisciplinename()." - ".$newname;}
					}
					$lmscoursename=$this->getUtildata()->getVaueOfArray($courseinfo, 'name');;
					if($lmscoursename!=$newname){
						//change name
						$paramu=array('_serviceid'=>$sserviceid,'_key'=>'course.course.update','id'=>$lmscourse,'name'=>$newname);
						$uresult=$this->processUpdate($paramu);
						$ustatus=$this->getUtildata()->getVaueOfArray($uresult, 'status');
						if($ustatus=='accept'){$lmsintegration['lmscoursename']=$newname;}
					}
					
				}
				
             }
          else if($lmssynctype=='replicationdatainlms' || $lmssynctype=='replicationdatabycoursetemplateinlms'){
			      
               $search=$this->getContainer()->get('badiu.system.core.functionality.search');
			    $specialcharactere=$this->getContainer()->get('badiu.system.core.lib.util.specialcharactere');
                   $this->init();
                   $name=null;
				   $idnumber=$this->getDto()->getId();
                   if($amslevel=='offer'){
					   $name=$this->getDto()->getCourseid()->getName()."/".$this->getDto()->getName();
					    $idnumber='100.'.$this->getDto()->getId();
					  }
                   else  if($amslevel=='discipline'){
					   $name=$this->getDto()->getDisciplinename();
					   $idnumber='300.'.$this->getDto()->getId();
					   }
                   else  if($amslevel=='section'){
					   $name=$this->getDto()->getName();
					    $idnumber='200.'.$this->getDto()->getId();
					   } 
                   else  if($amslevel=='classe'){
					   $name=$this->getDto()->getName();
					    $namefull=$this->getDto()->getName();
					     $idnumber='400.'.$this->getDto()->getId();
					} 
					$idnumber.=$this->systemshorname;
                    $cidnumber=$this->getDto()->getIdnumber();
				   if(!empty($cidnumber)){$idnumber=$cidnumber;}
					
				   if(empty($shortname)){
					  //$shortname=$specialcharactere->removeSpecialChar($name);
					  //$shortname= $shortname.'/'.$this->getDto()->getId().' / '.$this->getDto()->getName();
					  $shortname= strtolower($name).'.'.$this->getDto()->getId();
					  $shortname=str_replace(' ', '', $shortname);
				   }
				   
				   //change name for class
				    if($amslevel=='classe'){
						$joindisciplinenameinclassname=$this->getUtildata()->getVaueOfArray($lmsintegration, 'joindisciplinenameinclassname');
						if($joindisciplinenameinclassname){$name=$this->getDto()->getOdisciplineid()->getDisciplinename()." - ".$name;}
					}
				   
                  // $categoryid=1;
				   $categoryid= $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatparentid');
				   if(empty($categoryid)){$categoryid= $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseid');}
				   if(empty($categoryid)){$categoryid= $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatparent');}
                   
				   $lmscourseidtemplate=$this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseidtemplate');
					if(!empty($lmscourseidtemplate)){
						$coursetemplateinfo=$mdldatautil->getCourse($lmscourseidtemplate);
						$lmsintegration['lmscoursenametemplate']=$this->getUtildata()->getVaueOfArray($coursetemplateinfo, 'name');
					}
				
                   //check if course exist
                   
                  if($lmssynctype=='replicationdatainlms'){
						$parami=array('_serviceid'=>$sserviceid,'_key'=>'course.course.create','categoryid'=>$categoryid,'name'=>$name,'shortname'=>$shortname,'idnumber'=>$idnumber);
                
                    $iresult=$search->execWebService($parami);
					
                    $istatus=$this->getUtildata()->getVaueOfArray($iresult, 'status');
                    $imessage=$this->getUtildata()->getVaueOfArray($iresult, 'message');
                    $info=$this->getUtildata()->getVaueOfArray($iresult, 'info');
               
                    if($istatus=='accept' && !empty($imessage) ){
                        $courseinfo=$mdldatautil->getCourse($imessage);
                        $lmsintegration['lmscoursecatid']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatid');
                        $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatname');
                        $lmsintegration['lmscourseid']=$this->getUtildata()->getVaueOfArray($courseinfo, 'id');
                        $lmsintegration['lmscoursename']=$this->getUtildata()->getVaueOfArray($courseinfo, 'name');
                         $lmsintegration['lmscourseshortname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'shortname');
                        $lmsintegration['status']='active';
                        $lmsintegration['lmssynctype']='syncwithcreateddatainlms';
                        $lmsintegration['lmssynctypeoriginal']='replicationdatainlms';
                     
					} else if($istatus=='danied' && $info=='badiu.moodle.ws.error.param.shortnamejustexist' && !empty($shortname) ){
                  
                        $courseinfo=$mdldatautil->getCourse($shortname,true);
                      
                        $lmsintegration['lmscoursecatid']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatid');
                        $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatname');
                        $lmsintegration['lmscourseid']=$this->getUtildata()->getVaueOfArray($courseinfo, 'id');
                        $lmsintegration['lmscoursename']=$this->getUtildata()->getVaueOfArray($courseinfo, 'name');
                        $lmsintegration['lmscourseshortname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'shortname');
                        $lmsintegration['status']='active';
                        $lmsintegration['lmssynctype']='syncwithcreateddatainlms';
                        $lmsintegration['lmssynctypeoriginal']='replicationdatainlms';
					} 
				 }else  if($lmssynctype=='replicationdatabycoursetemplateinlms'){
						$parami=array('_serviceid'=>$sserviceid,'_key'=>'course.course.duplicate','categoryid'=>$categoryid,'name'=>$name,'shortname'=>$shortname,'idnumber'=>$idnumber,'sourcecourseid'=>$lmscourseidtemplate);
            
                    $iresult=$search->execWebService($parami);
					// print_r($iresult);exit;
                    $istatus=$this->getUtildata()->getVaueOfArray($iresult, 'status');
                    $imessage=$this->getUtildata()->getVaueOfArray($iresult, 'message');
                    $info=$this->getUtildata()->getVaueOfArray($iresult, 'info');
               
                    if($istatus=='accept' && !empty($imessage) ){
                        $courseinfo=$mdldatautil->getCourse($imessage);
                        $lmsintegration['lmscoursecatid']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatid');
                        $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatname');
                        $lmsintegration['lmscourseid']=$this->getUtildata()->getVaueOfArray($courseinfo, 'id');
                        $lmsintegration['lmscoursename']=$this->getUtildata()->getVaueOfArray($courseinfo, 'name');
                         $lmsintegration['lmscourseshortname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'shortname');
                        $lmsintegration['status']='active';
                        $lmsintegration['lmssynctype']='syncwithcreateddatainlms';
                        $lmsintegration['lmssynctypeoriginal']='replicationdatainlms';
                     
					} else if($istatus=='danied' && $info=='badiu.moodle.ws.error.param.shortnamejustexist' && !empty($shortname) ){
                  
                        $courseinfo=$mdldatautil->getCourse($shortname,true);
                      
                        $lmsintegration['lmscoursecatid']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatid');
                        $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatname');
                        $lmsintegration['lmscourseid']=$this->getUtildata()->getVaueOfArray($courseinfo, 'id');
                        $lmsintegration['lmscoursename']=$this->getUtildata()->getVaueOfArray($courseinfo, 'name');
                        $lmsintegration['lmscourseshortname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'shortname');
                        $lmsintegration['status']='active';
                        $lmsintegration['lmssynctype']='syncwithcreateddatainlms';
                        $lmsintegration['lmssynctypeoriginal']='replicationdatainlms';
					} 
				 }
                   
          }
       
          $param=$this->getParam();
          $param['lmsintegration']=$lmsintegration;
          
          $this->setParam($param);
          $this->updateParam();
		  $this->updateServiceSyncData($param);
         
        }
        
       
          function syncGroup($amslevel='discipline') {
            $this->findParam();
           
            $lmsintegration=$this->getUtildata()->getVaueOfArray($this->getParam(), 'lmsintegration');
            
            $lmssynclevel = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynclevel');
            if($lmssynclevel!='group'){return null;}
            
            $sserviceid = $this->getUtildata()->getVaueOfArray($lmsintegration, 'sserviceid');
            if(empty($sserviceid)){return null;}
            
            $lmssynctype = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynctype');
            $lmscoursecatparent = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecatparent');
            
           
           $mdldatautil=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodledatautil');
           $mdldatautil->getWebservice()->setServiceid($sserviceid);
            if($lmssynctype=='syncwithcreateddatainlms'){
                $lmscoursegroup = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursegroupid');
                if(empty($lmscoursegroup)){return null;}
                
                $coursegroupinfo=$mdldatautil->getCoursegroup($lmscoursegroup);
                $lmsintegration['lmscoursecatid']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'coursecatid');
                $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'coursecatname');
                $lmsintegration['lmscourseid']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'courseid');
                $lmsintegration['lmscoursename']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'coursename');
                $lmsintegration['lmscourseshortname']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'courseshortname');
                $lmsintegration['lmscoursegroupid']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'id');
                $lmsintegration['lmscoursegroupname']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'name');
                $lmsintegration['status']='active';
               
             }
          else if($lmssynctype=='replicationdatainlms'){
               $courseid = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourseid');
               if(empty($sserviceid)){return null;}
               $search=$this->getContainer()->get('badiu.system.core.functionality.search');
                   $this->init();
                   $name=null;
				   $idnumber=$this->getDto()->getId();
                   if($amslevel=='discipline'){
					   $name=$this->getDto()->getDisciplinename();
					     $idnumber='300.'.$this->getDto()->getId();
					   }
                   else  if($amslevel=='classe'){
					   $name=$this->getDto()->getName();
					   $idnumber='400.'.$this->getDto()->getId();
					   } 
                    $shortname=$this->getDto()->getShortname();
                 
					$idnumber.=$this->systemshorname;
					 $cidnumber=$this->getDto()->getIdnumber();
				   if(!empty($cidnumber)){$idnumber=$cidnumber;}
				   
                    //check if course exist
                   $parami=array('_serviceid'=>$sserviceid,'_key'=>'course.group.create','courseid'=>$courseid,'name'=>$name,'idnumber'=>$idnumber);
                 
                    $iresult=$search->execWebService($parami);
                    $istatus=$this->getUtildata()->getVaueOfArray($iresult, 'status');
                    $imessage=$this->getUtildata()->getVaueOfArray($iresult, 'message');
                    $info=$this->getUtildata()->getVaueOfArray($iresult, 'info');
               
                    if($istatus=='accept' && !empty($imessage) ){
                        $coursegroupinfo=$mdldatautil->getCoursegroup($imessage);
                        $lmsintegration['lmscoursecatid']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'coursecatid');
                        $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'coursecatname');
                        $lmsintegration['lmscourseid']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'courseid');
                        $lmsintegration['lmscoursename']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'coursename');
                        $lmsintegration['lmscourseshortname']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'courseshortname');
                        $lmsintegration['lmscoursegroupid']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'id');
                        $lmsintegration['lmscoursegroupname']=$this->getUtildata()->getVaueOfArray($coursegroupinfo, 'name');
                        $lmsintegration['status']='active';
                        $lmsintegration['lmssynctype']='syncwithcreateddatainlms';
                        $lmsintegration['lmssynctypeoriginal']='replicationdatainlms';
                     
                
               } else if($istatus=='danied' ){
                   //show erro message 
               } 
          }
       
          $param=$this->getParam();
          $param['lmsintegration']=$lmsintegration;
          
          $this->setParam($param);
          $this->updateParam();
          $this->updateServiceSyncData($param);
        }
		
	function processUpdate($param){
		$search=$this->getContainer()->get('badiu.system.core.functionality.search');
		
		$iresult=$search->execWebService($param);
		return $iresult;
	}
	
	function updateServiceSyncData($param){
		$modulekey=$this->getDatakey();
		$modulekey=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($modulekey);
		$dparam=array('modulekey'=>$modulekey,'moduleinstance'=>$this->getId());
		$param['_sysconfig']=$dparam;
		if(!$this->getContainer()->has('badiu.admin.server.syncdata.lib.manage')){return null;}
		$syncdatamanagelib=$this->getContainer()->get('badiu.admin.server.syncdata.lib.manage');
		$syncdatamanagelib->process($param);
	}
       function getDto() {
          return $this->dto;
      }

      function setDto($dto) {
          $this->dto = $dto;
      }

      
     function getOfferName($lmsintegration) {
         
     }   
      
	   function getSystemshorname() {
          return $this->systemshorname;
      }

      function setSystemshorname($systemshorname) {
          $this->systemshorname = $systemshorname;
      }
}
