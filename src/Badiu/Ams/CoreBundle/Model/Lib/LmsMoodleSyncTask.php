<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

class LmsMoodleSyncTask  extends  TaskGeneralExec{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
				$this->cresult=array('datashouldexec' => 0, 'dataexec' => 0);
        } 

  public function import() {
    $result = array('datashouldexec' => 0, 'dataexec' => 0);
    
    return $result;
}

public function update() {
   $this->updateRecentDataFromLms();
   $this->updateGeneralDataFromLms();
   
   $this->changeEnrolStatusClasse();
   $this->updateClasseLastaccessFromLms(); //review
   $this->updateClasseFinalGradeFromLms();//review
   $this->updateClasseFinalCompletationFromLms();//review
   $this->updateClasseProgressFromLms();//review
   $this->updateEnrolStatusnStudyingenrol(); //review
   $this->updateEnrolStatusCompleted();//review
   $this->changeClasseEnrolStatus();//review
   
   
    return $this->cresult;
}
public function updateRecentDataFromLms() {
	
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
	
		if($operation!='syncrecentdata'){return null;}
	
		
		$itemtypes=$this->getUtildata()->getVaueOfArray($this->getParam(),'itemtypes');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		$lasttime=$this->getUtildata()->getVaueOfArray($this->getParam(),'lasttime');
		
		$execafterservice=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.service',true);
		//$execafterfunction=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.function',true);
		$execafterdefaultroles=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.defaultroles',true);
		$execafterroles=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.roles',true);
		
		if(empty($itemtypes)){$itemtypes=array('courseaccess','coursefinalgrade','courseprogress','coursecompletation');}
		if(empty($maxrecord)){$maxrecord=50;}
		if(empty($lasttime)){$lasttime=300;}
		
		$listlms=$this->getLmsEnabled($this->getParam());
		
		 if(!is_array($listlms)){return  null;}
		 $lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesyncrecentdata');
		 $count=0;
		 
		 $listrecoredprocessed=array();
		  foreach ($listlms as $lrow) {
			  $sserviceid =$this->getUtildata()->getVaueOfArray($lrow,'sserviceid');
			  $entity =$this->getUtildata()->getVaueOfArray($lrow,'entity');
			  $fparam=array('lasttime'=>$lasttime,'sserviceid'=> $sserviceid,'entity'=>$entity);
			 
			  //courseaccess
			  if (in_array("courseaccess",$itemtypes)){
				  $lmsresult=$lmsmoodlesync->importLastaccessCourseFromMoodle($fparam);
				  $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
				  $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			      $message=null;
			      $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				  $this->cresult = $this->addResult($this->cresult, $resultmdl); 
				  
				  $rpd=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed');
				  $rpkey=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed.key',true);
				  $listrecoredprocessed[$rpkey]=$rpd;
				 
			 }
			  
			  if (in_array("coursefinalgrade",$itemtypes)){
				  $lmsresult=$lmsmoodlesync->importGradesFromMoodle($fparam);
				  $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
				  $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			      $message=null;
			      $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				  $this->cresult = $this->addResult($this->cresult, $resultmdl); 
				  
				  $rpd=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed');
				  $rpkey=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed.key',true);
				  $listrecoredprocessed[$rpkey]=$rpd;
			 }
			 if (in_array("coursecompletation",$itemtypes)){
				 
				  $lmsresult=$lmsmoodlesync->importCourseEnrolCompletationFromMoodle($fparam);
				  $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
				  $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			      $message=null;
			      $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				  $this->cresult = $this->addResult($this->cresult, $resultmdl); 
				  
				  $rpd=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed');
				  $rpkey=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed.key',true);
				  $listrecoredprocessed[$rpkey]=$rpd;
			 }
			if (in_array("courseprogress",$itemtypes)){
				  $lmsresult=$lmsmoodlesync->importCourseEnrolProgressFromMoodle($fparam);
				  $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
				  $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			      $message=null;
			      $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				  $this->cresult = $this->addResult($this->cresult, $resultmdl);

				  $rpd=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed');
				  $rpkey=$this->getUtildata()->getVaueOfArray($lmsresult,'recoredprocessed.key',true);
				  $listrecoredprocessed[$rpkey]=$rpd;				  
			 }
			
			  $count++;
		  }
		
		  $sparam=array('defaultroles'=>$execafterdefaultroles,'roles'=>$execafterroles);
		  //exec string service/function
		
		   foreach ($listrecoredprocessed as $prow) {
			   if(!empty($prow) && sizeof($prow) >0){
			    $classeid=$this->getUtildata()->getVaueOfArray($prow,'classeid');
				$userid=$this->getUtildata()->getVaueOfArray($prow,'userid');
			    $sparam['classeid']=$classeid;
				$sparam['userid']=$userid;
			 
			     $spresult=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($execafterservice,$sparam);
				 if(is_array($spresult)){
					foreach ($spresult as $prrow) { $this->addResult($this->cresult,$prrow); }
				}
			  }
		   }
		 
		 
		  return $count;
	}

public function updateGeneralDataFromLms() {
	
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');

		if($operation!='syncgeneraldata'){return null;}
			
		$itemtypes=$this->getUtildata()->getVaueOfArray($this->getParam(),'itemtypes');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		$lasttime=$this->getUtildata()->getVaueOfArray($this->getParam(),'lasttime');
		
		$execafterservice=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.service',true);
		//$execafterfunction=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.function',true);
		$execafterdefaultroles=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.defaultroles',true);
		$execafterroles=$this->getUtildata()->getVaueOfArray($this->getParam(),'execafter.roles',true);
		
		if(empty($itemtypes)){$itemtypes=array('courseaccess','coursefinalgrade','courseprogress','coursecompletation');}
		if(empty($maxrecord)){$maxrecord=500;}
	
		$lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesync');
		 $count=0;
		 
		//courseaccess
		if (in_array("courseaccess",$itemtypes)){
			$itemtype='lmscourseaccess';
			$items=$this->getClasseGradeItemList($itemtype,$maxrecord);
			foreach ($items as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $dconfig =$this->getUtildata()->getVaueOfArray($row,'dconfig');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  $dconfig = $this->getJson()->decode($dconfig, true);

			  $fparam=array('dconfig'=> $dconfig,'classeid'=>$classeid ,'entity'=>$entity);
			  
			   
			   $lmsresult=$lmsmoodlesync->importLastaccessCourseFromMoodle($fparam);
			   $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
			   $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			    $message=$itemtype;
			    $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				$this->cresult = $this->addResult($this->cresult, $resultmdl); 
			   $count++;
		  }	
				 
		 }
			  
		 if (in_array("coursefinalgrade",$itemtypes)){
				$itemtype='finalgrade';
				$items=$this->getClasseGradeItemList($itemtype,$maxrecord);
			foreach ($items as $row) {
				$classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $dconfig =$this->getUtildata()->getVaueOfArray($row,'dconfig');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  $dconfig = $this->getJson()->decode($dconfig, true);
			   $fparam=array('dconfig'=> $dconfig,'classeid'=>$classeid ,'entity'=>$entity);
			   $lmsresult=$lmsmoodlesync->importGradesFromMoodle($fparam);
			   $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
			   $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			    $message=$itemtype;
			    $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				$this->cresult = $this->addResult($this->cresult, $resultmdl); 
			   $count++;
		  }
		}
		if (in_array("coursecompletation",$itemtypes)){
			$itemtype='finalcompletation';
			$items=$this->getClasseGradeItemList($itemtype,$maxrecord);

			foreach ($items as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $dconfig =$this->getUtildata()->getVaueOfArray($row,'dconfig');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  $dconfig = $this->getJson()->decode($dconfig, true);
			   $fparam=array('dconfig'=> $dconfig,'classeid'=>$classeid,'entity'=>$entity);
			   $lmsresult=$lmsmoodlesync->importCourseEnrolCompletationFromMoodle($fparam);
			   $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
			   $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			    $message=$itemtype;
			    $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				$this->cresult = $this->addResult($this->cresult, $resultmdl); 
			  $count++;
		  }
		}
		if (in_array("courseprogress",$itemtypes)){
			$itemtype='progress';
			$items=$this->getClasseGradeItemList($itemtype,$maxrecord);

			foreach ($items as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $dconfig =$this->getUtildata()->getVaueOfArray($row,'dconfig');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  $dconfig = $this->getJson()->decode($dconfig, true);
			   $fparam=array('dconfig'=> $dconfig,'classeid'=>$classeid,'entity'=>$entity);
			   $lmsresult=$lmsmoodlesync->importCourseEnrolProgressFromMoodle($fparam);
			   $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
			   $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			    $message=$itemtype;
			    $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				$this->cresult = $this->addResult($this->cresult, $resultmdl); 
			    $count++;
		  }
		} 
		
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		$sparam=array('defaultroles'=>$execafterdefaultroles,'roles'=>$execafterroles,'maxrecord'=>$maxrecord);
		$spresult=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($execafterservice,$sparam);
		 if(is_array($spresult)){
			foreach ($spresult as $prrow) { $this->addResult($this->cresult,$prrow); }
		 }
		return $count;
	}
	//should teste
	public function changeEnrolStatusClasse() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='changeenrolstatusclasse'){return null;}
		$execservice=$this->getUtildata()->getVaueOfArray($this->getParam(),'exec.service',true);
		//$execfunction=$this->getUtildata()->getVaueOfArray($this->getParam(),'exec.function',true);
		$execdefaultroles=$this->getUtildata()->getVaueOfArray($this->getParam(),'exec.defaultroles',true);
		$execroles=$this->getUtildata()->getVaueOfArray($this->getParam(),'exec.roles',true);
		
		
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		$sparam=array('defaultroles'=>$execdefaultroles,'roles'=>$execroles,'maxrecord'=>$maxrecord);
		$result=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($execservice,$sparam);
		//  $message=null;
		  //$resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
		 //$this->cresult = $this->addResult($this->cresult, $resultmdl); 
		  return $count;
	}

public function updateClasseLastaccessFromLms() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='synclastaccess'){return null;}
		
		$itemtype=$this->getUtildata()->getVaueOfArray($this->getParam(),'itemtype');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		if(empty($itemtype)){$itemtype='lmscourseaccess';}
		if(empty($maxrecord)){$maxrecord=50;}
	
		$items=$this->getClasseGradeItemList($itemtype,$maxrecord);
		
		 if(!is_array($items)){return  null;}
		 $lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesync');
		 $count=0;
		 
		
		  foreach ($items as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $dconfig =$this->getUtildata()->getVaueOfArray($row,'dconfig');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  $dconfig = $this->getJson()->decode($dconfig, true);
			   $fparam=array('dconfig'=> $dconfig,'classeid'=>$classeid,'entity'=>$entity);
			   
			   $lmsresult=$lmsmoodlesync->importLastaccessCourseFromMoodle($fparam);
			   $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
			   $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			    $message=null;
			    $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				$this->cresult = $this->addResult($this->cresult, $resultmdl); 
			   /* print_r($fparam);
				echo "<hr>";
			   print_r($lmsresult);exit;*/
			   $count++;
		  }
		  return $count;
	}
	
	public function updateClasseFinalGradeFromLms() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='syncgrade'){return null;}
		
		//finalgrade | finalcompletation
		$itemtype=$this->getUtildata()->getVaueOfArray($this->getParam(),'itemtype');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		if(empty($itemtype)){$itemtype='finalgrade';}
		if(empty($maxrecord)){$maxrecord=50;}
	
		$items=$this->getClasseGradeItemList($itemtype,$maxrecord);
		 if(!is_array($items)){return  null;}
		 $lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesync');
		 $count=0;
		 //print_r($items);exit; 
		  foreach ($items as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $dconfig =$this->getUtildata()->getVaueOfArray($row,'dconfig');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  $dconfig = $this->getJson()->decode($dconfig, true);
			   $fparam=array('dconfig'=> $dconfig,'classeid'=>$classeid,'entity'=>$entity);
			   $lmsresult=$lmsmoodlesync->importGradesFromMoodle($fparam);
			   $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
			   $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			    $message=null;
			    $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				$this->cresult = $this->addResult($this->cresult, $resultmdl); 
			   /* print_r($fparam);
				echo "<hr>";
			   print_r($lmsresult);exit;*/
			   $count++;
		  }
		  return $count;
	}
	
	public function updateClasseFinalCompletationFromLms() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='synccompletation'){return null;}
		
		//finalgrade | finalcompletation
		$itemtype=$this->getUtildata()->getVaueOfArray($this->getParam(),'itemtype');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		if(empty($itemtype)){$itemtype='finalcompletation';}
		if(empty($maxrecord)){$maxrecord=50;}
	
		$items=$this->getClasseGradeItemList($itemtype,$maxrecord);
		 if(!is_array($items)){return  null;}
		 $lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesync');
		 $count=0;
		 
		  foreach ($items as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $dconfig =$this->getUtildata()->getVaueOfArray($row,'dconfig');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  $dconfig = $this->getJson()->decode($dconfig, true);
			   $fparam=array('dconfig'=> $dconfig,'classeid'=>$classeid,'entity'=>$entity);
			   $lmsresult=$lmsmoodlesync->importCourseEnrolCompletationFromMoodle($fparam);
			   $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
			   $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			    $message=null;
			    $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				$this->cresult = $this->addResult($this->cresult, $resultmdl); 
			   /* print_r($fparam);
				echo "<hr>";
			   print_r($lmsresult);exit;*/
			   $count++;
		  }
		  return $count;
	}
	
	public function updateClasseProgressFromLms() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='syncprogress'){return null;}
		
		//progress | activityprogress
		$itemtype=$this->getUtildata()->getVaueOfArray($this->getParam(),'itemtype');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		if(empty($itemtype)){$itemtype='progress';}
		if(empty($maxrecord)){$maxrecord=50;}
	
		$items=$this->getClasseGradeItemList($itemtype,$maxrecord);
		
		 if(!is_array($items)){return  null;}
		 $lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesync');
		 $count=0;
		  
		  foreach ($items as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $dconfig =$this->getUtildata()->getVaueOfArray($row,'dconfig');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  $dconfig = $this->getJson()->decode($dconfig, true);
			   $fparam=array('dconfig'=> $dconfig,'classeid'=>$classeid,'entity'=>$entity);
			   
			   $lmsresult=$lmsmoodlesync->importCourseEnrolProgressFromMoodle($fparam);
			   $datashouldexec=$this->getUtildata()->getVaueOfArray($lmsresult,'shouldprocess');
			   $dataexec=$this->getUtildata()->getVaueOfArray($lmsresult,'executed');
			    $message=null;
			    $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				$this->cresult = $this->addResult($this->cresult, $resultmdl); 
			   /* print_r($fparam);
				echo "<hr>";
			   print_r($lmsresult);exit;*/
			   $count++;
		  }
		  return $count;
	} 
	public function updateEnrolStatusCompleted() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='syncenorolstatuscompletation'){return null;}
		
		$statusshortnamediff=$this->getUtildata()->getVaueOfArray($this->getParam(),'statusshortnamediff');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		if(empty($statusshortnamediff)){$statusshortnamediff='approvedenrol';}
		if(empty($maxrecord)){$maxrecord=50;}
		
	
		$fparam=array('maxrecord'=>$maxrecord,'statusshortnamediff'=>$statusshortnamediff,'itemtype'=>'finalcompletation','grade'=>1);
		$enrols=$this->getEnrolClasseGradeList($fparam);
		
		 if(!is_array($enrols)){return  null;}
		 $count=0;
		 
		  $enorldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 
		  foreach ($enrols as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $userid =$this->getUtildata()->getVaueOfArray($row,'userid');
			  $enrolid=$this->getUtildata()->getVaueOfArray($row,'id');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  $newstatusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,'approvedenrol'); //review add session
			  $fparam=array('id'=>$enrolid,'statusid'=>$newstatusid,'timefinish'=>new \DateTime());
			  $r= $enorldata->updateNativeSql($fparam,false);
			   $datashouldexec=1;
			   if($r){$r=1;}
			   $dataexec=$r;
			   $message=null;
			   $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
			   $this->cresult = $this->addResult($this->cresult, $resultmdl); 
			  $count++;
		  }
		  return $count;
	}
	
	public function changeClasseEnrolStatus() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='changeclasseenrolstatus'){return null;}
		
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		$classetypeaccessshortname=$this->getUtildata()->getVaueOfArray($this->getParam(),'classetypeaccessshortname');
		$statusshortname=$this->getUtildata()->getVaueOfArray($this->getParam(),'statusshortname');
		$newstatusshortname=$this->getUtildata()->getVaueOfArray($this->getParam(),'newstatusshortname');
		$criteria=$this->getUtildata()->getVaueOfArray($this->getParam(),'criteria'); //coursecompleted | coursenotcompletedallrequiredactivitycompleted |  coursenotcompletedanyrequiredactivitycompleted  |  coursenotcompletednotallrequiredactivitycompleted     ----| notcompletedallrequireactivitywhenerolend | notcompletedanyrequireactivitywhenenrolend | notcoursecompletedwhenenrolend | notcoursecompletedwhenofferend | withoutaccesswhenenrolend | withoutaccesswhenofferend
		$activenrol=$this->getUtildata()->getVaueOfArray($this->getParam(),'activenrol');
		$activeclasse=$this->getUtildata()->getVaueOfArray($this->getParam(),'activeclasse');
		$waitingtime=$this->getUtildata()->getVaueOfArray($this->getParam(),'waitingtime');
		
		//if(empty($itemtype)){$itemtype='classe';}
		if(empty($maxrecord)){$maxrecord=500;}
	
		$fparam=array('activenrol'=>$activenrol,'activeclasse'=>$activeclasse,'criteria'=>$criteria,'classetypeaccessshortname'=>$classetypeaccessshortname,'statusshortname'=>$statusshortname,'newstatusshortname'=>$newstatusshortname,'maxrecord'=>$maxrecord);
		
		$enrols=$this->getEnrolClasseToChangeStatus($fparam);

		 if(!is_array($enrols)){return  null;}
		 $count=0;
		 
		  $enorldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 	foreach ($enrols as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $userid =$this->getUtildata()->getVaueOfArray($row,'userid');
			  $enrolid=$this->getUtildata()->getVaueOfArray($row,'id');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  
			  $timemodified=$this->getUtildata()->getVaueOfArray($row,'timemodified');
			
			  $process=true;
			  if(!empty($waitingtime) && !empty($timemodified)){
				  
				  if (!is_a($timemodified, 'DateTime') && is_string($timemodified)) {
					$timemodified = \DateTime::createFromFormat('Y-m-d H:i:s', $timemodified);
				 }
				
				 if (is_a($timemodified, 'DateTime')){
					 
					 $timeaddwait=$timemodified->getTimestamp()+$waitingtime;
					 $tnow=new \DateTime();
					 if($tnow->getTimestamp() < $timeaddwait){$process=false;}
					
				 }
				
			  }  
			  
			   if($process){
				   $newstatusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,$newstatusshortname); //review add session
			  
					$fparam=array('id'=>$enrolid,'statusid'=>$newstatusid,'timefinish'=>new \DateTime());
					$r= $enorldata->updateNativeSql($fparam,false);
					$datashouldexec=1;
					if($r){$r=1;}
					$dataexec=$r;
					$message=null;
					$resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
					$this->cresult = $this->addResult($this->cresult, $resultmdl); 
					$count++;
				   
			   }
			  
		  }
		  return $count;
	}
	
	public function updateEnrolStatusnStudyingenrol() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='syncenorolstatusstudying'){return null;}
		
		//finalgrade | finalcompletation
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		$statusshortname=$this->getUtildata()->getVaueOfArray($this->getParam(),'statusshortname');
		$statusshortname1=$this->getUtildata()->getVaueOfArray($this->getParam(),'statusshortname1');
		$criteria=$this->getUtildata()->getVaueOfArray($this->getParam(),'criteria'); //mdlcourseaccess | hasactivityrequiretocoursecomplete
		if(empty($itemtype)){$itemtype='lmscourseaccess';}
		if(empty($statusshortname)){$statusshortname='activeenrol';}
		if(empty($statusshortname1)){$statusshortname1='preregistrationenrol';}
		if(empty($criteria)){$criteria='mdlcourseaccess';}
		if(empty($maxrecord)){$maxrecord=50;}
		$itemtype='lmscourseaccess';
		if($criteria=='hasactivityrequiretocoursecomplete'){$itemtype='progress';}
		else if($criteria=='mdlcourseaccess'){$itemtype='lmscourseaccess';}
		
		$fparam=array('maxrecord'=>$maxrecord,'statusshortname'=>$statusshortname,'statusshortname1'=>$statusshortname1,'itemtype'=>$itemtype,'grade'=>1,'criteria'=>$criteria);
		$enrols=$this->getEnrolClasseGradeList($fparam);
		
		
		 if(!is_array($enrols)){return  null;}
		 $count=0;
		 
		  $enorldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 
		  foreach ($enrols as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $userid =$this->getUtildata()->getVaueOfArray($row,'userid');
			  $enrolid=$this->getUtildata()->getVaueOfArray($row,'id');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  $newstatusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,'studyingenrol'); //review add session
			  $fparam=array('id'=>$enrolid,'statusid'=>$newstatusid);
			  $r= $enorldata->updateNativeSql($fparam,false);
			   $datashouldexec=1;
			   if($r){$r=1;}
			   $dataexec=$r;
			   $message=null;
			   $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
			   $this->cresult = $this->addResult($this->cresult, $resultmdl); 
			  $count++;
		  }
		  return $count;
	}
	
	
  public function getClasseGradeItemList($itemtype,$maxrecord) {
	   $itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
	   $isactiveclasse=1;
	   $wsql="";
	   if($isactiveclasse){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		  $wsql.=$classedefaultsqlfilter->activefilter($fparam);
	   }
		if($this->getTaskdto()->getTcron()=='entity'){$wsql.=" AND cl.entity =:entity ";}	
        $sql = "SELECT cl.id AS classeid,cl.dconfig,cl.entity FROM ".$itemdata->getBundleEntity()." o INNER JOIN BadiuAmsOfferBundle:AmsOfferClasse cl WITH (cl.id=o.moduleinstance) JOIN cl.statusid cls WHERE  o.modulekey=:modulekey AND o.itemtype=:itemtype AND cl.dconfig IS NOT NULL $wsql ";
        $query = $itemdata->getEm()->createQuery($sql);
        $query->setParameter('modulekey', 'badiu.ams.offer.classe');
		$query->setParameter('itemtype', $itemtype);
		if($isactiveclasse){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		 }
		 if($this->getTaskdto()->getTcron()=='entity'){ $query->setParameter('entity', $this->getTaskdto()->getEntity());}	
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
        return $result;
    } 
	
	 public function getEnrolClasseGradeList($param) {
		 $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		 $statusshortname=$this->getUtildata()->getVaueOfArray($param,'statusshortname');
		 $statusshortname1=$this->getUtildata()->getVaueOfArray($param,'statusshortname1');
		 $statusshortnamediff=$this->getUtildata()->getVaueOfArray($param,'statusshortnamediff');
		 $itemtype=$this->getUtildata()->getVaueOfArray($param,'itemtype');
		 $grade=$this->getUtildata()->getVaueOfArray($param,'grade');
		 $criteria=$this->getUtildata()->getVaueOfArray($param,'criteria');
		  
        $itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		$wsql="";
		if(!empty($statusshortname) && empty($statusshortname1)){ $wsql.=" AND s.shortname=:statusshortname ";}
		else if(!empty($statusshortname) &&!empty($statusshortname1)){
			$wsql.=" AND (s.shortname=:statusshortname  OR s.shortname=:statusshortname1) "; //review
		}
		if(!empty($statusshortnamediff)){ $wsql.=" AND s.shortname !=:statusshortnamediff ";}
		
		if($criteria=='hasactivityrequiretocoursecomplete'){$wsql.=" AND g.customint4 > 0 ";}
		
		//grade 
		if($itemtype=='progress'){
			$wsql.="  AND g.grade > :grade  ";
			$grade=0;
		}else{ $wsql.="  AND g.grade = :grade  ";} 
		
		$isactiveclasse=1;
	   if($isactiveclasse){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		  $wsql.=$classedefaultsqlfilter->activefilter($fparam);
	   }
	   if($this->getTaskdto()->getTcron()=='entity'){$wsql.=" AND o.entity =:entity ";}	
		 $sql = " SELECT o.id,u.id AS userid,i.moduleinstance AS classeid,o.entity FROM BadiuAmsEnrolBundle:AmsEnrolClasse o JOIN o.classeid cl JOIN cl.statusid cls JOIN o.userid u  LEFT JOIN o.roleid r LEFT JOIN o.statusid s JOIN BadiuAdminGradeBundle:AdminGradeItem i WITH (o.classeid=i.moduleinstance AND i.modulekey='badiu.ams.offer.classe') JOIN  BadiuAdminGradeBundle:AdminGrade g WITH (g.itemid=i.id AND o.userid=g.userid) WHERE i.itemtype=:itemtype $wsql ";
		
	
        
		$query = $itemdata->getEm()->createQuery($sql);
        $query->setParameter('itemtype',$itemtype );
		if(!empty($statusshortname)){$query->setParameter('statusshortname', $statusshortname);}
		if(!empty($statusshortname1)){$query->setParameter('statusshortname1', $statusshortname1);}
		if(!empty($statusshortnamediff)){$query->setParameter('statusshortnamediff', $statusshortnamediff);}
		$query->setParameter('grade', $grade);
		if($isactiveclasse){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		 }
		if($this->getTaskdto()->getTcron()=='entity'){ $query->setParameter('entity', $this->getTaskdto()->getEntity());}			 
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		return $result;
    } 	

 public function getEnrolClasseToChangeStatus($param) {
		 $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		 $statusshortname=$this->getUtildata()->getVaueOfArray($param,'statusshortname');
		 $classetypeaccessshortname=$this->getUtildata()->getVaueOfArray($param,'classetypeaccessshortname');
		 $this->getUtildata()->getVaueOfArray($param,'itemtype');
		 $criteria=$this->getUtildata()->getVaueOfArray($param,'criteria'); //coursecompleted  | coursenotcompletedallrequiredactivitycompleted   | coursenotcompletedanyrequiredactivitycompleted      -- rever | completedallrequireactivity | notcompletedallrequireactivitywhenerolend | notcompletedanyrequireactivitywhenenrolend | notcoursecompletedwhenenrolend | notcoursecompletedwhenofferend | withoutaccesswhenenrolend | withoutaccesswhenofferend
		 $isactivenrol=$this->getUtildata()->getVaueOfArray($param,'activenrol');
		 $isactiveclasse=$this->getUtildata()->getVaueOfArray($param,'activeclasse');
		 
		 $edata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 
		 
		 $itemtype=null;
		 $wsql="";
		 if($criteria=='coursecompleted'){
			 $wsql.=" AND g.grade = :grade  ";	
			 $grade=1;
			 $itemtype='finalcompletation';
			
		 }
		 
		 else if($criteria=='coursenotcompletedallrequiredactivitycompleted'){
			 $wsql.=" AND g.customint4 > 0 AND g.customint3 > 0 AND g.customint4=g.customint3 ";
			 $wsql.=" AND (SELECT COUNT(g1.id) FROM BadiuAdminGradeBundle:AdminGrade g1 JOIN g1.itemid i1 WHERE i1.moduleinstance=o.classeid AND i1.modulekey='badiu.ams.offer.classe' AND i1.itemtype= 'progress' AND  g1.userid=o.userid AND g1.grade=1) = 0";
			 $wsql.="  AND g.grade > :grade  ";	
			 $grade=0;
			 $itemtype='progress';
		 }
		 else if($criteria=='coursenotcompletedanyrequiredactivitycompleted'){
			 $wsql.=" AND g.customint4 = 0 AND g.customint3 > 0 ";
			 $wsql.=" AND (SELECT COUNT(g1.id) FROM BadiuAdminGradeBundle:AdminGrade g1 JOIN g1.itemid i1 WHERE i1.moduleinstance=o.classeid AND i1.modulekey='badiu.ams.offer.classe' AND i1.itemtype= 'progress' AND  g1.userid=o.userid AND g1.grade=1) = 0";
			 $wsql.="  AND g.grade >= :grade  ";	
			 $grade=0;
			 $itemtype='progress';
		 } else if($criteria=='coursenotcompletednotallrequiredactivitycompleted'){
			 $wsql.=" AND g.customint4 > 0 AND g.customint3 > 0 AND g.customint4  < g.customint3 ";
			 $wsql.=" AND (SELECT COUNT(g1.id) FROM BadiuAdminGradeBundle:AdminGrade g1 JOIN g1.itemid i1 WHERE i1.moduleinstance=o.classeid AND i1.modulekey='badiu.ams.offer.classe' AND i1.itemtype= 'progress' AND  g1.userid=o.userid AND g1.grade=1) = 0";
			 $wsql.="  AND g.grade > :grade  ";	
			 $grade=0;
			 $itemtype='progress';
		 }
		 
		 
		 if(!empty($classetypeaccessshortname)){ $wsql.=" AND clta.shortname=:classetypeaccessshortname ";}
		 if(!empty($statusshortname)){$wsql.=" AND s.shortname=:statusshortname ";}
       
		
		
	   if($isactiveclasse==1){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		   $wsql.=$classedefaultsqlfilter->activefilter($fparam);
	   }else if($isactiveclasse===0){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		   $wsql.=$classedefaultsqlfilter->inactivefilter($fparam);
	   }
	   
	   
	   if($isactivenrol==1){
			$fparam=array('_enrolalias'=>'o');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.enrol.core.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->activedatefilter($fparam);
			
		}else if($isactivenrol===0){
			$fparam=array('_enrolalias'=>'o');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.enrol.core.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->inactivedatefilter($fparam);
		}
		
		 if($this->getTaskdto()->getTcron()=='entity'){$wsql.=" AND o.entity =:entity ";}	
		 
		 $sql = " SELECT o.id,u.id AS userid,i.moduleinstance AS classeid,o.entity ,g.timemodifiedgrade AS timemodified  FROM BadiuAmsEnrolBundle:AmsEnrolClasse o JOIN o.statusid s JOIN o.roleid r JOIN o.userid u  JOIN o.classeid cl JOIN cl.statusid cls LEFT JOIN cl.typeaccessid clta JOIN BadiuAdminGradeBundle:AdminGradeItem i WITH (o.classeid=i.moduleinstance AND i.modulekey='badiu.ams.offer.classe') JOIN  BadiuAdminGradeBundle:AdminGrade g WITH (g.itemid=i.id AND o.userid=g.userid) WHERE i.itemtype=:itemtype $wsql ";
		
		
		$query = $edata->getEm()->createQuery($sql);
        $query->setParameter('itemtype',$itemtype );
		if(!empty($classetypeaccessshortname)){$query->setParameter('classetypeaccessshortname', $classetypeaccessshortname);}
		if(!empty($statusshortname)){$query->setParameter('statusshortname', $statusshortname);}
		if($grade!==null){$query->setParameter('grade', $grade);}
		
		if($isactiveclasse==1 || $isactiveclasse===0){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		 }	
		 
		 if($isactivenrol==1 || $isactivenrol===0){
			$tnow=new \DateTime();
		    $query->setParameter('enroltimenow1', $tnow);
		    $query->setParameter('enroltimenow2', $tnow);
		}
		if($this->getTaskdto()->getTcron()=='entity'){ $query->setParameter('entity', $this->getTaskdto()->getEntity());}	
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		
		return $result;
    } 	


 public function getLmsEnabled($param) {
	  $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
	  if(empty($maxrecord)){$maxrecord=50;}
       $ssyncdata=$this->getContainer()->get('badiu.admin.server.syncdata.data');
	  $wsql="";
		  if($this->getTaskdto()->getTcron()=='entity'){$wsql.=" AND o.entity =:entity ";}		
        $sql = "SELECT DISTINCT ss.id AS sserviceid,ss.entity  FROM ".$ssyncdata->getBundleEntity()." o JOIN o.serviceid ss  WHERE  ss.syncdb=:sssyncdb AND ss.deleted=:ssdeleted $wsql ";
        $query =  $ssyncdata->getEm()->createQuery($sql);
        $query->setParameter('sssyncdb', 1);
		$query->setParameter('ssdeleted', 0);
		if($this->getTaskdto()->getTcron()=='entity'){ $query->setParameter('entity', $this->getTaskdto()->getEntity());}	
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		
        return $result;
    } 	
}
?>