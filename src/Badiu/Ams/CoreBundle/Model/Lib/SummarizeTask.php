<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

class SummarizeTask  extends  TaskGeneralExec{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
				$this->cresult=array('datashouldexec' => 0, 'dataexec' => 0);
        } 

  public function import() {
    $result = array('datashouldexec' => 0, 'dataexec' => 0);
    
    return $result;
}

public function update() {
   $this->classeEnrolStatusPercentage();
   $this->classeMoodleQuestionnaireRankOneToFiveGradePercent();
   $this->classeMoodleQuestionnaireRankOneToFiveQuestionAmoutResponse();
   return $this->cresult;
}
/**
operation: classeenrolstatuspercentage
datatype:  new|update
modulekey: badiu.ams.offer.classe | badiu.tms.offer.classe
enrolstatusshorname - shortname of status enrol
enrolroleshorname - shortname of role enrol
activeclasse 0|1
maxrecord - number of max rows class to exec
*/
public function classeEnrolStatusPercentage() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='classeenrolstatuspercentage'){return null;}
			
		$list=$this->getClasseEnrolStatusPercentageList($this->getParam());
		
		 if(!is_array($list)){return  null;}
		 $lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesync');
		 $count=0;
		 
		 $moduledatanumb=$this->getContainer()->get('badiu.system.module.datanumb.data');
		
		 $modulekey=$this->getUtildata()->getVaueOfArray($this->getParam(),'modulekey');
		 if(empty($modulekey)){$modulekey="badiu.ams.offer.classe";}
		 
		 $statusshorname=$this->getUtildata()->getVaueOfArray($this->getParam(),'enrolstatusshorname');
		 if(empty($statusshorname)){$statusshorname="approved";}
		 
		 $name="enrolstatus$statusshorname";
		  $contexec=0;
		  if(!is_array($list)){return null;}
		  if(empty($list)){return null;}
		  $datashouldexec=sizeof($list);
		  $dataexec=0;
		  foreach ($list as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'id');
			  $entity =$this->getUtildata()->getVaueOfArray($row,'entity');
			  $referenceid =$this->getUtildata()->getVaueOfArray($row,'referenceid');
			  $countenrol = $this->getUtildata()->getVaueOfArray($row,'countenrol');
			  $countstatus = $this->getUtildata()->getVaueOfArray($row,'countstatus');
			  
			  $value=0;
			  if($countenrol > 0 && $countstatus > 0 && $countstatus <=$countenrol){$value=$countstatus*100/$countenrol;}
			  if($referenceid){
				  $iparam=array('id'=>$referenceid,'value'=>$value,'timemodified'=>new \DateTime());
				  $resultp=$moduledatanumb->updateNativeSql($iparam,false);
			   }else{
				   $aparam=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$classeid,'name'=>$name,'value'=>$value,'timemodified'=>new \DateTime(),'timecreated'=>new \DateTime());
				   $resultp=$moduledatanumb->insertNativeSql($aparam,false);
			   }
			    if($resultp){$dataexec++;}
			    
			   $count++;
		  }
		  
				$message=null;
			   $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => "");
			   $this->cresult = $this->addResult($this->cresult, $resultmdl); 
			  
		  return $count;
	}
	
	/**
operation: classemoodlequestionnairerankonetofivegradepercent
datatype:  new|update
modulekey: badiu.ams.offer.classe | badiu.tms.offer.classe
activeclasse 0|1
maxrecord - number of max rows class to exec
*/
public function classeMoodleQuestionnaireRankOneToFiveGradePercent() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='classemoodlequestionnairerankonetofivegradepercent'){return null;}
			
		$list=$this->getClasseMoodleQuestionnaireRankOneToFiveGradePercent($this->getParam());
		
		 if(!is_array($list)){return  null;}
		 $lmsmoodlesync=$this->getContainer()->get('badiu.ams.core.lib.lmsmoodlesync');
		 $count=0;
		 
		 $moduledatanumb=$this->getContainer()->get('badiu.system.module.datanumb.data');
		
		 $modulekey=$this->getUtildata()->getVaueOfArray($this->getParam(),'modulekey');
		 if(empty($modulekey)){$modulekey="badiu.ams.offer.classe";}
		 
		  $name="moodlequestionnairerankonetofivegradepercent";
		  $contexec=0;
		  if(!is_array($list)){return null;}
		  if(empty($list)){return null;}
		  $datashouldexec=sizeof($list);
		  $dataexec=0;
		  foreach ($list as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'id');
			  $entity =$this->getUtildata()->getVaueOfArray($row,'entity');
			  $referenceid =$this->getUtildata()->getVaueOfArray($row,'referenceid');
			  
			  $value=$this->makeGradePercentOfRankOneToFive($row);
			 /* if($value > 0){
				   print_r($row);
					echo "Valor: $value";exit;
			  }*/
			 
			 if($referenceid){
				  $iparam=array('id'=>$referenceid,'value'=>$value,'timemodified'=>new \DateTime());
				  $resultp=$moduledatanumb->updateNativeSql($iparam,false);
			   }else{
				   $aparam=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$classeid,'name'=>$name,'value'=>$value,'timemodified'=>new \DateTime(),'timecreated'=>new \DateTime());
				   $resultp=$moduledatanumb->insertNativeSql($aparam,false);
			   }
			    if($resultp){$dataexec++;}
			    
			   $count++;
		  }
		  
				$message=null;
			   $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => "");
			   $this->cresult = $this->addResult($this->cresult, $resultmdl); 
			  
		  return $count;
	}
	

	/**
operation: classemoodlequestionnairerankonetofivequestionamoutresponse
datatype:  new|update
modulekey: badiu.ams.offer.classe | badiu.tms.offer.classe
activeclasse 0|1
maxrecord - number of max rows class to exec

*/
public function classeMoodleQuestionnaireRankOneToFiveQuestionAmoutResponse() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='classemoodlequestionnairerankonetofivequestionamoutresponse'){return null;}
			
		$listcourse=$this->getClasseMoodleQuestionnaireHasRankQuestionList($this->getParam());
		
		
		  $moduledatanumb=$this->getContainer()->get('badiu.system.module.datanumb.data');
		  $modulekey=$this->getUtildata()->getVaueOfArray($this->getParam(),'modulekey');

		 if(empty($modulekey)){$modulekey="badiu.ams.offer.classe";}
		  $name="moodlequestionnairerankonetofivegradequestionamoutresponse";
		 $count=0;
		 $contexec=0;
		 $dataexec=0;
		 $datashouldexec=0;
		 $moduledata=$this->getContainer()->get('badiu.system.module.data.data');
		 $utilstring=$this->getContainer()->get('badiu.system.core.lib.util.string');
		/*echo "<pre>";
		 print_r($listcourse);
		 echo "</pre>";
		 exit;*/
		  foreach ($listcourse as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $entity =$this->getUtildata()->getVaueOfArray($row,'entity');
			  $row['maxrecord']=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
			  //get list of rak question
			  
			  $listqueston=$this->getQuestionMoodleQuestionnaireRankTypeList($row);
			
			  // if(!is_array( $listqueston)){return  null;}
			  // if(sizeof($listqueston)==0){return  null;}
			  // print_r($listqueston);exit;
			  $datashouldexec+=sizeof($listqueston);
			  foreach ($listqueston as $qrow) {
				$questionid =$this->getUtildata()->getVaueOfArray($qrow,'id');
				$questioncontent =$this->getUtildata()->getVaueOfArray($qrow,'question');
				$serviceid =$this->getUtildata()->getVaueOfArray($qrow,'serviceid');
				$moodlecourseid =$this->getUtildata()->getVaueOfArray($qrow,'moodlecourseid');
				
				$questioncontentkey=$utilstring->castToKey($questioncontent);
				if(strlen($questioncontentkey)>255){$questioncontentkey=substr($questioncontentkey, 0, 254);} 
				
				$pinsert=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$classeid,'name'=>$name,'shortname'=>$questioncontentkey,'customint7'=>$questionid,'customtext1'=>$questioncontent);
				$frparam=array('entity'=>$entity,'serviceid'=>$serviceid,'moodlecourseid'=>$moodlecourseid,'activityquestionid'=>$questionid);
				
				$frparam['rankvalue']=1;
				$pinsert['customint1']= $this->getQuestionMoodleQuestionnaireRankSumResponseRate($frparam);
				if(empty($pinsert['customint1'])){$pinsert['customint1']=0;}
				
				$frparam['rankvalue']=2;
				$pinsert['customint2']= $this->getQuestionMoodleQuestionnaireRankSumResponseRate($frparam);
				if(empty($pinsert['customint2'])){$pinsert['customint2']=0;}
				
				$frparam['rankvalue']=3;
				$pinsert['customint3']= $this->getQuestionMoodleQuestionnaireRankSumResponseRate($frparam);
				if(empty($pinsert['customint3'])){$pinsert['customint3']=0;}
				
			
				$frparam['rankvalue']=4;
				$pinsert['customint4']= $this->getQuestionMoodleQuestionnaireRankSumResponseRate($frparam);
				if(empty($pinsert['customint4'])){$pinsert['customint4']=0;}
				
				$frparam['rankvalue']=5;
				$pinsert['customint5']= $this->getQuestionMoodleQuestionnaireRankSumResponseRate($frparam);
				if(empty($pinsert['customint5'])){$pinsert['customint5']=0;}
				
				$pinsert['customint6']=$pinsert['customint1']+$pinsert['customint2']+$pinsert['customint3']+$pinsert['customint4']+$pinsert['customint5'];
				$paramadd=$pinsert;
				$paramadd['timecreated']=new \DateTime();
				$paramedit=$pinsert;
				$paramedit['timemodified']=new \DateTime();;
				
				//if($classeid==116){echo "sssrr $classeid<pre>"; print_r($pinsert);echo "<pre>";exit;} 
				$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$classeid,'name'=>$name,'customint7'=>$questionid);
				 $presult=$moduledata->addNativeSql($paramcheckexist,$paramadd,$paramedit);
				$r=$this->getUtildata()->getVaueOfArray($presult,'id');
				 if($r){$dataexec++;}
				 $count++;
			  }
			  
			  
		  }
		
		  
				$message=null;
			   $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => "");
			   $this->cresult = $this->addResult($this->cresult, $resultmdl); 
			  
		  return $count;
	}	
	public function makeGradePercentOfRankOneToFive($row) {
		$value=0;
			$countrankrespose = $this->getUtildata()->getVaueOfArray($row,'countrankrespose');
			  $countrank1respose = $this->getUtildata()->getVaueOfArray($row,'countrank1respose');
			  $countrank2respose = $this->getUtildata()->getVaueOfArray($row,'countrank2respose');
			  $countrank3respose = $this->getUtildata()->getVaueOfArray($row,'countrank3respose');
			  $countrank4respose = $this->getUtildata()->getVaueOfArray($row,'countrank4respose');
			  $countrank5respose = $this->getUtildata()->getVaueOfArray($row,'countrank5respose');
			 
			 
			  $countresponsecheck=$countrank1respose+$countrank2respose+$countrank3respose+$countrank4respose+$countrank5respose;
			  if($countrankrespose!=$countresponsecheck){$countrankrespose=$countresponsecheck;}
			  if($countrankrespose==0){return 0;}
			  $r1=$countrank1respose*1;
			  $r2=$countrank2respose*2;
			  $r3=$countrank3respose*3;
			  $r4=$countrank4respose*4;
			  $r5=$countrank5respose*5;
			  $y=$countrankrespose*5;
			  $x=$r1+$r2+$r3+$r4+$r5;
			 
			   if($x > 0 && $y > 0 && $y >=$x){$value=$x*100/$y;}
			   
			  //  if($countrankrespose > 0){ echo "$x | $y ";print_r($row); echo "valr: $value";exit;}
		return 	$value;
	}
	 public function getClasseEnrolStatusPercentageList($param) {
		 $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		 if(empty($maxrecord)){$maxrecord=50;}
		 
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 if(empty($modulekey)){$modulekey="badiu.ams.offer.classe";}
		 
		 $datatype=$this->getUtildata()->getVaueOfArray($this->getParam(),'datatype');
		
		 
		 $isactiveclasse=$this->getUtildata()->getVaueOfArray($param,'activeclasse');
		 
		 $roleshorname=$this->getUtildata()->getVaueOfArray($param,'enrolroleshorname');
		 if(empty($roleshorname)){$roleshorname="student";}	

		 $statusshorname=$this->getUtildata()->getVaueOfArray($param,'enrolstatusshorname');
		 if(empty($statusshorname)){$statusshorname="approved";}	
		 
		 $name="enrolstatus$statusshorname";
		 
		 if($isactiveclasse==null || $isactiveclasse==""){$isactiveclasse=1;}
         
		
		 $countenrols="SELECT COUNT(e1.id) FROM BadiuAmsEnrolBundle:AmsEnrolClasse e1 JOIN e1.roleid r1 JOIN e1.statusid s1 JOIN e1.classeid cl1 WHERE cl1.id=cl.id AND r1.shortname='".$roleshorname."'";
		 $countenrolsbystatus ="SELECT COUNT(e2.id) FROM BadiuAmsEnrolBundle:AmsEnrolClasse e2 JOIN e2.roleid r2 JOIN e2.statusid s2 JOIN e2.classeid cl2 WHERE cl2.id=cl.id AND r2.shortname='".$roleshorname."'  AND s2.shortname='".$statusshorname."'";
		 
		 $columns=",($countenrols) AS countenrol, ($countenrolsbystatus) AS countstatus ";
		 $classedata=$this->getContainer()->get('badiu.tms.offer.classe.data');
		 $classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		 $wsql="";
		
		if($modulekey=="badiu.ams.offer.classe"){$wsql.=" AND f.shortname!=:offershortname ";}
		else if($modulekey=="badiu.tms.offer.classe"){$wsql.=" AND  f.shortname=:offershortname ";}
		
		 if($isactiveclasse==1){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $wsql.=$classedefaultsqlfilter->activefilter($fparam);
		 }else if($isactiveclasse===0){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $wsql.=$classedefaultsqlfilter->inactivefilter($fparam);
		 }
	    
		 if($datatype=='new'){$wsql.=" AND dn.id IS NULL";}
		 else{$wsql.=" AND dn.id IS NOT NULL ORDER BY dn.timemodified "; }
		 
		 $sql = " SELECT cl.id,dn.id AS referenceid,cl.entity $columns FROM BadiuAmsOfferBundle:AmsOfferClasse cl JOIN cl.statusid cls JOIN cl.odisciplineid od JOIN od.offerid f LEFT JOIN BadiuSystemModuleBundle:SystemModuleDataNumb dn WITH (cl.id=dn.moduleinstance AND dn.modulekey=:modulekey AND dn.name=:name)  WHERE cl.id > 0 $wsql ";
		
		$query = $classedata->getEm()->createQuery($sql);
		$query->setParameter('offershortname','tmsdefaultcourseoffer');
        $query->setParameter('modulekey',$modulekey);
		$query->setParameter('name',$name);
		if($isactiveclasse==1 || $isactiveclasse===0){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		 }	
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		return $result;
    } 	


 public function getClasseMoodleQuestionnaireRankOneToFiveGradePercent($param) {
		 $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		 if(empty($maxrecord)){$maxrecord=50;}
		 
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 if(empty($modulekey)){$modulekey="badiu.ams.offer.classe";}
		 
		 $datatype=$this->getUtildata()->getVaueOfArray($this->getParam(),'datatype');
		
		 
		 $isactiveclasse=$this->getUtildata()->getVaueOfArray($param,'activeclasse');
		 
		 
		 $name="moodlequestionnairerankonetofivegradepercent";
		 
		 if($isactiveclasse==null || $isactiveclasse==""){$isactiveclasse=1;}
         
		 $sparam=array('_prefix'=>1,'_operation'=>'countresponse');
		 $countresponse=$this->getCsqlClasseMoodleQuestionnaireRankOneToFiveSum($sparam);
		 
		 $sparam=array('_prefix'=>2,'_operation'=>'countresponserank','rank'=>1);
		 $sumrank1=$this->getCsqlClasseMoodleQuestionnaireRankOneToFiveSum($sparam);
		 
		 $sparam=array('_prefix'=>3,'_operation'=>'countresponserank','rank'=>2);
		 $sumrank2=$this->getCsqlClasseMoodleQuestionnaireRankOneToFiveSum($sparam);
		 
		 $sparam=array('_prefix'=>4,'_operation'=>'countresponserank','rank'=>3);
		 $sumrank3=$this->getCsqlClasseMoodleQuestionnaireRankOneToFiveSum($sparam);
		 
		 $sparam=array('_prefix'=>5,'_operation'=>'countresponserank','rank'=>4);
		 $sumrank4=$this->getCsqlClasseMoodleQuestionnaireRankOneToFiveSum($sparam);
		 
		 $sparam=array('_prefix'=>6,'_operation'=>'countresponserank','rank'=>5);
		 $sumrank5=$this->getCsqlClasseMoodleQuestionnaireRankOneToFiveSum($sparam);
		 
		 $columns=",($countresponse) AS countrankrespose, ($sumrank1) AS countrank1respose , ($sumrank2) AS countrank2respose , ($sumrank3) AS countrank3respose , ($sumrank4) AS countrank4respose , ($sumrank5) AS countrank5respose ";
		 $classedata=$this->getContainer()->get('badiu.tms.offer.classe.data');
		 $classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		 $wsql="";
		
		if($modulekey=="badiu.ams.offer.classe"){$wsql.=" AND f.shortname!=:offershortname ";}
		else if($modulekey=="badiu.tms.offer.classe"){$wsql.=" AND  f.shortname=:offershortname ";}
		
		 if($isactiveclasse==1){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $wsql.=$classedefaultsqlfilter->activefilter($fparam);
		 }else if($isactiveclasse===0){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $wsql.=$classedefaultsqlfilter->inactivefilter($fparam);
		 }
	    
		 if($datatype=='new'){$wsql.=" AND dn.id IS NULL";}
		 else{$wsql.=" AND dn.id IS NOT NULL ORDER BY dn.timemodified "; }
		
		 $sql = " SELECT DISTINCT cl.id,dn.id AS referenceid,cl.entity $columns FROM BadiuAmsOfferBundle:AmsOfferClasse cl JOIN cl.statusid cls JOIN cl.odisciplineid od JOIN od.offerid f LEFT JOIN BadiuSystemModuleBundle:SystemModuleDataNumb dn WITH (cl.id=dn.moduleinstance AND dn.modulekey=:modulekey AND dn.name=:name)  WHERE cl.id > 0 $wsql ";
		
		$query = $classedata->getEm()->createQuery($sql);
		$query->setParameter('offershortname','tmsdefaultcourseoffer');
        $query->setParameter('modulekey',$modulekey);
		$query->setParameter('name',$name);
		if($isactiveclasse==1 || $isactiveclasse===0){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		 }	
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		return $result;
    } 
	
	 public function getCsqlClasseMoodleQuestionnaireRankOneToFiveSum($param) {
		 $pfx=$this->getUtildata()->getVaueOfArray($param,'_prefix');
		 $operation=$this->getUtildata()->getVaueOfArray($param,'_operation');
		 $rank=$this->getUtildata()->getVaueOfArray($param,'rank');
		
		 $wsql="";
		 if($operation=="countresponse"){$wsql=" AND mms$pfx.responseint > 0 ";};
		 if($operation=="countresponserank"){$wsql=" AND mms$pfx.responseint =  $rank ";};
		 $csql="SELECT COUNT(mms$pfx.id) FROM BadiuMoodleCoreBundle:MoodleCoreModIsummarize mms$pfx JOIN BadiuAdminServerBundle:AdminServerSyncData ssd$pfx WITH (mms$pfx.serviceid=ssd$pfx.serviceid AND mms$pfx.courseid=ssd$pfx.remotedataid) WHERE ssd$pfx.remotedatatype='course' AND ssd$pfx.modulekey='badiu.ams.offer.classe' AND ssd$pfx.moduleinstance=cl.id AND mms$pfx.modulekey='badiu.moodle.core.sync.questionnaire.response.rank' AND mms$pfx.activitymodule='questionnaire' $wsql ";
		 return $csql;
	 }
	 
	 
	 
	 public function getClasseMoodleQuestionnaireHasRankQuestionList($param) {
		 $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		 if(empty($maxrecord)){$maxrecord=200;}
		 
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 if(empty($modulekey)){$modulekey="badiu.ams.offer.classe";}
		 
		 $datatype=$this->getUtildata()->getVaueOfArray($this->getParam(),'datatype');
		
		 
		 $isactiveclasse=$this->getUtildata()->getVaueOfArray($param,'activeclasse');
		 
		 
		 $name="moodlequestionnairerankonetofivegradequestionamoutresponse";
				
		 
		 if($isactiveclasse==null || $isactiveclasse==""){$isactiveclasse=1;}
        
		$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		$wsql="";
		if($modulekey=="badiu.ams.offer.classe"){$wsql.=" AND f.shortname!=:offershortname ";}
		else if($modulekey=="badiu.tms.offer.classe"){$wsql.=" AND  f.shortname=:offershortname ";}
		
		 if($isactiveclasse==1){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $wsql.=$classedefaultsqlfilter->activefilter($fparam);
		 }else if($isactiveclasse===0){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $wsql.=$classedefaultsqlfilter->inactivefilter($fparam);
		 }
	    
		 if($datatype=='new'){$wsql.=" AND dn.id IS NULL";}
		 else{$wsql.=" AND dn.id IS NOT NULL ORDER BY dn.timemodified "; }
		
		 $sql = " SELECT DISTINCT cl.id AS classeid,cl.entity FROM BadiuAmsOfferBundle:AmsOfferClasse cl JOIN cl.statusid cls JOIN cl.odisciplineid od JOIN od.offerid f LEFT JOIN BadiuSystemModuleBundle:SystemModuleData dn WITH (cl.id=dn.moduleinstance AND dn.modulekey=:modulekey AND dn.name=:name)  WHERE cl.id > 0 $wsql ";
		$classedata=$this->getContainer()->get('badiu.tms.offer.classe.data');
		$query = $classedata->getEm()->createQuery($sql);
		$query->setParameter('offershortname','tmsdefaultcourseoffer');
        $query->setParameter('modulekey',$modulekey);
		$query->setParameter('name',$name);
		if($isactiveclasse==1 || $isactiveclasse===0){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		 }	
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		return $result;
    } 
	
  public function getQuestionMoodleQuestionnaireRankTypeList($param) {
		 $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		 $classeid=$this->getUtildata()->getVaueOfArray($param,'classeid');
		 $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($dtype)){$dtype="training";}
		 if(empty($maxrecord)){$maxrecord=500;}
		 if($maxrecord < 500){$maxrecord=500;}
		 
		
		 //review to same questions with different id
		 $sql = " SELECT DISTINCT mms.activityquestionid AS id, mms.activityquestioncontent AS question, ss.id AS serviceid, mms.courseid AS moodlecourseid FROM BadiuMoodleCoreBundle:MoodleCoreModIsummarize mms JOIN BadiuAdminServerBundle:AdminServerSyncData ssd WITH (mms.serviceid=ssd.serviceid AND mms.courseid=ssd.remotedataid) LEFT JOIN BadiuAdminServerBundle:AdminServerSyncUser ssu WITH (mms.serviceid=ssu.serviceid AND mms.userid=ssu.userid) LEFT JOIN ssu.sysuserid u JOIN mms.serviceid ss JOIN BadiuAmsOfferBundle:AmsOfferClasse cl  WITH (ssd.moduleinstance=cl.id AND ssd.modulekey='badiu.ams.offer.classe')  WHERE ssd.remotedatatype='course' AND ssd.modulekey='badiu.ams.offer.classe'  AND mms.activitymodule='questionnaire' AND mms.entity=:entity AND cl.id=:classeid  AND mms.modulekey=:modulekey   ORDER BY   mms.activityquestionid";
		
		$classedata=$this->getContainer()->get('badiu.tms.offer.classe.data');
		$query = $classedata->getEm()->createQuery($sql);
		$query->setParameter('classeid',$classeid);
		$query->setParameter('entity',$entity);
        $query->setParameter('modulekey','badiu.moodle.core.sync.questionnaire.response.rank');
		
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		return $result;
    } 
	
 public function getQuestionMoodleQuestionnaireRankSumResponseRate($param) {
		 $serviceid=$this->getUtildata()->getVaueOfArray($param,'serviceid');
		 $moodlecourseid=$this->getUtildata()->getVaueOfArray($param,'moodlecourseid');
		 $activityquestionid=$this->getUtildata()->getVaueOfArray($param,'activityquestionid');
		 $rankvalue=$this->getUtildata()->getVaueOfArray($param,'rankvalue');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 
		 $sql = "SELECT COUNT(s.id) AS countrecord FROM BadiuMoodleCoreBundle:MoodleCoreModIsummarize s   WHERE s.entity=:entity AND s.serviceid=:serviceid AND s.courseid=:moodlecourseid AND s.modulekey=:modulekey AND s.activityquestionid=:activityquestionid AND s.responseint= :rankvalue ";
		 $classedata=$this->getContainer()->get('badiu.tms.offer.classe.data');
		$query = $classedata->getEm()->createQuery($sql);
		$query->setParameter('serviceid',$serviceid);
		$query->setParameter('moodlecourseid',$moodlecourseid);
		$query->setParameter('activityquestionid',$activityquestionid);
		$query->setParameter('rankvalue',$rankvalue);
		$query->setParameter('modulekey','badiu.moodle.core.sync.questionnaire.response.rank');
		
		$query->setParameter('entity',$entity);
        
        $result = $query->getOneOrNullResult();
		$result=$this->getUtildata()->getVaueOfArray($result,'countrecord');
		return $result;
    }  
		
	
}
?>