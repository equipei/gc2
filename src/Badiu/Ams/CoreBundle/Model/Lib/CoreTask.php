<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

class CoreTask  extends  TaskGeneralExec{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
				$this->cresult=array('datashouldexec' => 0, 'dataexec' => 0);
        } 

  public function import() {
    $result = array('datashouldexec' => 0, 'dataexec' => 0);
    
    return $result;
}

public function update() {
   $this->addGradeItems();
   $this->terminationEnrolOfActiveClasse();
   $this->terminationEnrolOfInactiveClasse();
   return $this->cresult;
}


public function addGradeItems() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='setupgrade'){return null;}
		
		$itemtype=$this->getUtildata()->getVaueOfArray($this->getParam(),'itemtype');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		//if(empty($itemtype)){$itemtype='lmscourseaccess';}
		if(empty($maxrecord)){$maxrecord=50;}
		
		$fgparam=array('itemtype'=>$itemtype,'maxrecord'=>$maxrecord,'isactive'=>1);
		$classelist=$this->getClasses($fgparam);
		 if(!is_array($classelist)){return  null;}
		$gradelibmanager= $this->getContainer()->get('badiu.admin.grade.lib.manager');
		$itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		 $count=0;
		  
		  $modulekey='badiu.ams.offer.classe';
		  foreach ($classelist as $row) {
			  $id =$this->getUtildata()->getVaueOfArray($row,'id');
			  $dconfig =$this->getUtildata()->getVaueOfArray($row,'dconfig');
			  $dconfig = $this->getJson()->decode($dconfig, true);
			  $presult=array('shouldprocess'=>4,'executed'=>0);
				//add finalgrade
				$fparam=array('entity'=>$this->getEntity(),'modulekey'=>$modulekey,'moduleinstance'=>$id,'itemtype'=>'finalgrade');
				$itemid=$itemdata->getGlobalColumnValue('id',$fparam); 
				if(empty($itemid)){
					$fiparam=array();
					$itemid=$gradelibmanager->addItem($fparam);
					if($itemid){$presult['executed']++;}
				}
			
				//add finalcopletation
				$fparam=array('entity'=>$this->getEntity(),'modulekey'=>$modulekey,'moduleinstance'=>$id,'itemtype'=>'finalcompletation');
				$itemid=$itemdata->getGlobalColumnValue('id',$fparam); 
				if(empty($itemid)){
					$fiparam=array();
					$itemid=$gradelibmanager->addItem($fparam);
					if($itemid){$presult['executed']++;}
				}
				
				//add progress
				$fparam=array('entity'=>$this->getEntity(),'modulekey'=>$modulekey,'moduleinstance'=>$id,'itemtype'=>'progress');
				$itemid=$itemdata->getGlobalColumnValue('id',$fparam); 
				if(empty($itemid)){
					$fiparam=array();
					$itemid=$gradelibmanager->addItem($fparam);
					if($itemid){$presult['executed']++;}
				}
				//add lmscourseaccess
				$fparam=array('entity'=>$this->getEntity(),'modulekey'=>$modulekey,'moduleinstance'=>$id,'itemtype'=>'lmscourseaccess');
				$itemid=$itemdata->getGlobalColumnValue('id',$fparam); 
				if(empty($itemid)){
					$fiparam=array();
					$itemid=$gradelibmanager->addItem($fparam);
					if($itemid){$presult['executed']++;}
				}
				
			   
			   $datashouldexec=$this->getUtildata()->getVaueOfArray($presult,'shouldprocess');
			   $dataexec=$this->getUtildata()->getVaueOfArray($presult,'executed');
			    $message=null;
			    $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				$this->cresult = $this->addResult($this->cresult, $resultmdl); 
			  
			   $count++;
		  }
		  return $count;
	}
	
	/*
	* change enrol status for not active classe
	*/
	public function terminationEnrolOfActiveClasse() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='terminationenrolactiveclasse'){return null;}
		$criteria=$this->getUtildata()->getVaueOfArray($this->getParam(),'criteria'); // notcompletedallrequireactivitywhenerolend |  notcompletedanyrequireactivitywhenerolend |  
		$classetypeaccessshortname=$this->getUtildata()->getVaueOfArray($this->getParam(),'classetypeaccessshortname');
		$statusshortname=$this->getUtildata()->getVaueOfArray($this->getParam(),'statusshortname');
		$newstatusshortname=$this->getUtildata()->getVaueOfArray($this->getParam(),'newstatusshortname');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		
		if(empty($maxrecord)){$maxrecord=50;}
		
		//get list of enrol
		$fparam=array('criteria'=>$criteria,'classetypeaccessshortname'=>$classetypeaccessshortname,'isactive'=>1,'maxrecord'=>$maxrecord);
		$listenrols=$this->getClasseInfoEnrols($param);
		
		 if(!is_array($listenrols)){return  null;}
		
		 $count=0;
		  $entity=$this->getEntity();
		  $newstatusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,$newstatusshortname); 
		  $enorldata=$this->getContainer()->get('badiu.ams.enrol.classe.data'); 
		  $presult=array('shouldprocess'=>4,'executed'=>sizeof($listenrols));
		 foreach ($listenrols as $row) {
			  $enrolid =$this->getUtildata()->getVaueOfArray($erow,'id');
			  $fparam=array('id'=>$enrolid,'statusid'=>$newstatusid,'timefinish'=>new \DateTime());
			  $r= $enorldata->updateNativeSql($fparam,false);
			  $presult['executed']++;
			   $count++;
		  }		
		
		  return $count;
	}
	
	/*
	* change enrol status for not active classe
	*/
	public function terminationEnrolOfInactiveClasse() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='terminationenrolofinactiveclasse'){return null;}
		$criteria=$this->getUtildata()->getVaueOfArray($this->getParam(),'criteria'); //notcompletedwhenclasseclose | notcompletedallrequireactivitywhenerolend |  notcompletedanyrequireactivitywhenerolend |  
		$classetypeaccessshortname=$this->getUtildata()->getVaueOfArray($this->getParam(),'classetypeaccessshortname');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		
		if(empty($maxrecord)){$maxrecord=50;}
		
		$fgparam=array('maxrecord'=>$maxrecord,'isactive'=>0);
		$classelist=$this->getClasses($fgparam);
		 if(!is_array($classelist)){return  null;}
		
		 $count=0;
		  $entity=$this->getEntity();
		  $quitterenroltatusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,'quitterenrol'); 
		  $evadedenrolstatusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,'evadedenrol'); 
			$enorldata=$this->getContainer()->get('badiu.ams.enrol.classe.data'); 
;		 
		 foreach ($classelist as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'id');
			  
			   //get active enrol
			  $fparam=array('itemtype'=>'lmscourseaccess','classeid'=>$classeid,'enrolenable'=>1,'roleshortname'=>'studentcorporate');
			  $listenrol=$this->getClasseInfoEnrolsByClasseid($fparam);
			   $presult=array('shouldprocess'=>4,'executed'=>sizeof($listenrol));
			  foreach ($listenrol as $erow) {
				  $timecreatedgrade =$this->getUtildata()->getVaueOfArray($erow,'timecreatedgrade');
				  $timemodifiedgrade =$this->getUtildata()->getVaueOfArray($erow,'timemodifiedgrade');
				  $grade =$this->getUtildata()->getVaueOfArray($erow,'grade');
				  $enrolid =$this->getUtildata()->getVaueOfArray($erow,'id');
				   if(!empty($grade)){
						$newstatusid=$quitterenroltatusid;
				   }else{$newstatusid=$evadedenrolstatusid;}
				   
				   $fparam=array('id'=>$enrolid,'statusid'=>$newstatusid,'timefinish'=>new \DateTime());
				   $r= $enorldata->updateNativeSql($fparam,false);
				   $presult['executed']++;
			   }
			 
			   $datashouldexec=$this->getUtildata()->getVaueOfArray($presult,'shouldprocess');
			   $dataexec=$this->getUtildata()->getVaueOfArray($presult,'executed');
			    $message=null;
			    $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				$this->cresult = $this->addResult($this->cresult, $resultmdl); 
			  
			   $count++;
		  }
		  return $count;
	}
	// revew set incremental data
  public function getClasses($param) {
		$maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		$isactive=$this->getUtildata()->getVaueOfArray($param,'isactive');
		
		$wsql="";
		if($isactive==1){
			$fparam=array('_classealias'=>'o','_classestatusalias'=>'s');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
			$wsql=$classedefaultsqlfilter->activefilter($fparam);
		}else if($isactive==0){
			$fparam=array('_classealias'=>'o','_classestatusalias'=>'s');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
			$wsql=$classedefaultsqlfilter->inactivefilter($fparam);
		}
		
		$data=$this->getContainer()->get('badiu.ams.offer.classe.data');
        $sql = "SELECT o.id,o.dconfig FROM  BadiuAmsOfferBundle:AmsOfferClasse o JOIN o.statusid s  WHERE  o.id > 0  $wsql";
       
 	   $query = $data->getEm()->createQuery($sql);
		if($isactive==1 || $isactive==0){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		}
		
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		return $result;
    } 
	
	// revew set incremental data
  public function getClasseInfoEnrolsByClasseid($param) {
		$classeid=$this->getUtildata()->getVaueOfArray($param,'classeid');
		$itemtype=$this->getUtildata()->getVaueOfArray($param,'itemtype');
		$enrolenable=$this->getUtildata()->getVaueOfArray($param,'enrolenable');
		$maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		$roleshortname=$this->getUtildata()->getVaueOfArray($param,'roleshortname');;
		$data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
	     $itemtype="'".$itemtype."'";
	   $sql = "SELECT o.id,u.id AS userid,g.grade,g.timecreatedgrade,g.timemodifiedgrade,s.id AS statusid,s.shortname AS statushortname FROM  ".$data->getBundleEntity()." o JOIN o.statusid s JOIN o.userid u LEFT JOIN o.roleid r  JOIN BadiuAdminGradeBundle:AdminGradeItem i WITH (o.classeid=i.moduleinstance AND i.modulekey='badiu.ams.offer.classe' AND i.itemtype= $itemtype) LEFT JOIN  BadiuAdminGradeBundle:AdminGrade g WITH (g.itemid=i.id AND o.userid=g.userid) WHERE  o.classeid=:classeid AND s.enable=:enrolenable AND r.shortname=:roleshortname";
        $query = $data->getEm()->createQuery($sql);
		$query->setParameter('classeid', $classeid);
		$query->setParameter('enrolenable', $enrolenable);
		$query->setParameter('roleshortname', $roleshortname);
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		return $result;
    } 
	

	// revew set incremental data
  public function getClasseInfoEnrols($param) {
		$itemtype=$this->getUtildata()->getVaueOfArray($param,'itemtype');
		$enrolenable=$this->getUtildata()->getVaueOfArray($param,'enrolenable');
		$maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		$roleshortname=$this->getUtildata()->getVaueOfArray($param,'roleshortname');
		$criteria=$this->getUtildata()->getVaueOfArray($param,'criteria');
		$isactive=$this->getUtildata()->getVaueOfArray($param,'isactive');
		$classetypeaccessshortname=$this->getUtildata()->getVaueOfArray($param,'classetypeaccessshortname');
		
		//notcompletedanyrequireactivitywhenerolend |  
		
		 $itemtype="'".$itemtype."'";
		 
		 $wsql="";
		 if($criteria=='notcompletedanyrequireactivitywhenerolend'){
			 $wsql.=" AND g.customint4 > 0 AND g.customint3 > 0 AND g.customint4=g.customint3 ";
			 $wsql.=" AND (SELECT COUNT(g1.id) FROM BadiuAdminGradeBundle:AdminGrade g1 JOIN g1.itemid i1 WHERE i1.moduleinstance=o.classeid AND i1.modulekey='badiu.ams.offer.classe' AND i1.itemtype= 'progress' AND  g1.userid=o.userid AND g1.grade=1) = 0";
		 }
		 
		 if($isactive==1){
			$fparam=array('__enrolalias'=>'o','_statusenrolalias'=>'s');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.enrol.core.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->activefilter($fparam);
		}else if($isactive==0){
			$fparam=array('_enrolalias'=>'o','_statusenrolalias'=>'s');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.enrol.core.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->inactivefilter($fparam);
		}
		
		if(!empty($classetypeaccessshortname)){
			 $wsql.=" AND clta.shortname=:classetypeaccessshortname ";
		 }
		
		$data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
	    
	    $sql = "SELECT o.id,u.id AS userid,g.grade,g.timecreatedgrade,g.timemodifiedgrade,s.id AS statusid,s.shortname AS statushortname FROM  ".$data->getBundleEntity()." o JOIN o.statusid s JOIN o.userid u LEFT JOIN o.roleid r JOIN o.classeid cl LEFT JOIN cl.typeaccessid clta JOIN BadiuAdminGradeBundle:AdminGradeItem i WITH (o.classeid=i.moduleinstance AND i.modulekey='badiu.ams.offer.classe' AND i.itemtype= $itemtype) LEFT JOIN  BadiuAdminGradeBundle:AdminGrade g WITH (g.itemid=i.id AND o.userid=g.userid) WHERE  o.classeid=:classeid AND r.shortname=:roleshortname $wsql";
        $query = $data->getEm()->createQuery($sql);
		if(!empty($classetypeaccessshortname)){$query->setParameter('classetypeaccessshortname', $classetypeaccessshortname);}
		$query->setParameter('roleshortname', $roleshortname);
		$query->setMaxResults($maxrecord);
		if($isactive==1 || $isactive==0){
			$tnow=new \DateTime();
		    $query->setParameter('enroltimenow1', $tnow);
		    $query->setParameter('enroltimenow2', $tnow);
		}
        $result = $query->getResult();
		return $result;
		
    } 	
}
?>