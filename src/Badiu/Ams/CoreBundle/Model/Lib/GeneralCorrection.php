<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class GeneralCorrection extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
    
     //http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_service=badiu.ams.core.lib.generalcorrection&_function=dhour
     public function dhour() {
		 
			$cdisciplinedata=$this->getContainer()->get('badiu.ams.curriculum.discipline.data');
			$odisciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			
			$presult=array('curriculum'=>0,'offer'=>0);
					
			//get list curriculum discipline
			 $sql = "SELECT  o.id,d.dhour FROM " . $cdisciplinedata->getBundleEntity() . " o JOIN o.disciplineid d  WHERE o.entity=:entity AND ( o.dhour IS NULL OR o.dhour=0 ) ";
			$query = $cdisciplinedata->getEm()->createQuery($sql);
			 $query->setParameter('entity', $this->getEntity());
			$dresult = $query->getResult();
			foreach ($dresult as $drow) {
				$id=$this->getUtildata()->getVaueOfArray($drow,'id');
				$dhour=$this->getUtildata()->getVaueOfArray($drow,'dhour');
				$uparam=array('id'=>$id,'dhour'=>$dhour);
				$r=$cdisciplinedata->updateNativeSql($uparam,false);
				if($r){$presult['curriculum']++;}
			}
			//get list offer discipline
			 $sql = "SELECT  o.id,d.dhour FROM " . $odisciplinedata->getBundleEntity() . " o JOIN o.disciplineid d  WHERE o.entity=:entity AND ( o.dhour IS NULL OR o.dhour=0 ) ";
			$query = $odisciplinedata->getEm()->createQuery($sql);
			 $query->setParameter('entity', $this->getEntity());
			$dresult = $query->getResult();
			foreach ($dresult as $drow) {
				$id=$this->getUtildata()->getVaueOfArray($drow,'id');
				$dhour=$this->getUtildata()->getVaueOfArray($drow,'dhour');
				$uparam=array('id'=>$id,'dhour'=>$dhour);
				$r=$odisciplinedata->updateNativeSql($uparam,false);
				if($r){$presult['offer']++;}
			}
          return $presult; 
        }
}
