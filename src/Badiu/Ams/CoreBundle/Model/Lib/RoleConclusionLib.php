<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuWebService;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class RoleConclusionLib    extends BadiuModelLib{

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
  
	public function config($param) {
		$source = $this->getUtildata()->getVaueOfArray($param, 'source');
		$level = $this->getUtildata()->getVaueOfArray($param, 'level');
		$instanceid = $this->getUtildata()->getVaueOfArray($param, 'instanceid');
		
		if($level=='classe'){
			$config=$this->configClasse($param);
			return $config;
		}else if($level=='discipline'){
			$config=$this->configDiscipline($param);
			return $config;
		}else if($level=='offer'){
			$config=$this->configOffer($param);
			return $config;
		}
		return null;

	}
  
  public function configClasse($param) {
		$instanceid = $this->getUtildata()->getVaueOfArray($param, 'instanceid');
		$source = $this->getUtildata()->getVaueOfArray($param, 'source');
		
		$dconfigclasse=null;
		$dconfigdiscipline=null;
		$dconfigoffer=null;
		
		
		if($source=='db'){
			if(empty($instanceid)){return null;}
			
			$classedata=$this->getContainer()->get('badiu.ams.offer.classe.data');
			$gconfig=$classedata->getDconfig($instanceid);
			
			$dconfigclasse=$this->getUtildata()->getVaueOfArray($gconfig, 'classedconfig');
			$dconfigdiscipline=$this->getUtildata()->getVaueOfArray($gconfig, 'disclipinedconfig');
			$dconfigoffer=$this->getUtildata()->getVaueOfArray($gconfig, 'offerdconfig');
			
		
		}else if($source=='array'){
			$dconfigclasse=$this->getUtildata()->getVaueOfArray($param, 'config.classe',true);
			$dconfigdiscipline=$this->getUtildata()->getVaueOfArray($param, 'config.discipline',true);
			$dconfigoffer=$this->getUtildata()->getVaueOfArray($param, 'config.offer',true);
		}
		
		if(!is_array($dconfigclasse)){$dconfigclasse=$this->getJson()->decode($dconfigclasse,true);}
		if(!is_array($dconfigdiscipline)){$dconfigdiscipline=$this->getJson()->decode($dconfigdiscipline,true);}
		if(!is_array($dconfigoffer)){$dconfigoffer=$this->getJson()->decode($dconfigoffer,true);}
		
		
		$enable=$this->getUtildata()->getVaueOfArray($dconfigclasse, 'conclusion.discipline.enable',true);
		$dconfigclasse['conclusion']['level']='classe';
		if($enable){return $dconfigclasse;}
				
		$enable=$this->getUtildata()->getVaueOfArray($dconfigdiscipline, 'conclusion.discipline.enable',true);
		$dconfigdiscipline['conclusion']['level']='discipline';
		if($enable){return $dconfigdiscipline;}
			
		
		$enable=$this->getUtildata()->getVaueOfArray($dconfigoffer, 'conclusion.enable',true);
		$dconfigoffer['conclusion']['level']='offer';
		if($enable){return $dconfigoffer;}
		
		return null;
  }
  public function configDiscipline($param) {
	   	$instanceid = $this->getUtildata()->getVaueOfArray($param, 'instanceid');
		$source = $this->getUtildata()->getVaueOfArray($param, 'source');
		
		$dconfigdiscipline=null;
		$dconfigoffer=null;
		
		
		if($source=='db'){
			if(empty($instanceid)){return null;}
			
			$disciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			$gconfig=$disciplinedata->getDconfig($instanceid);
			
			$dconfigdiscipline=$this->getUtildata()->getVaueOfArray($gconfig, 'disclipinedconfig');
			$dconfigoffer=$this->getUtildata()->getVaueOfArray($gconfig, 'offerdconfig');
			
		
		}else if($source=='array'){
			$dconfigdiscipline=$this->getUtildata()->getVaueOfArray($param, 'config.discipline',true);
			$dconfigoffer=$this->getUtildata()->getVaueOfArray($param, 'config.offer',true);
		}
		
		if(!is_array($dconfigdiscipline)){$dconfigdiscipline=$this->getJson()->decode($dconfigdiscipline,true);}
		if(!is_array($dconfigoffer)){$dconfigoffer=$this->getJson()->decode($dconfigoffer,true);}
		
		$enable=$this->getUtildata()->getVaueOfArray($dconfigdiscipline, 'conclusion.discipline.enable',true);
		$dconfigdiscipline['conclusion']['level']='discipline';
		if($enable){return $dconfigdiscipline;}
			
		
		$enable=$this->getUtildata()->getVaueOfArray($dconfigoffer, 'conclusion.enable',true);
		$dconfigoffer['conclusion']['level']='offer';
		if($enable){return $dconfigoffer;}
		
		return null;		
  }
  public function configOffer($param) {
		$instanceid = $this->getUtildata()->getVaueOfArray($param, 'instanceid');
		$source = $this->getUtildata()->getVaueOfArray($param, 'source');
		
		$dconfigoffer=null;
		if($source=='db'){ 
			if(empty($instanceid)){return null;}
			
			$offerdata=$this->getContainer()->get('badiu.ams.offer.offer.data');
			$gconfig=$offerdata->getDconfig($instanceid);
			
			$dconfigoffer=$this->getUtildata()->getVaueOfArray($gconfig, 'offerdconfig');
			
		
		}else if($source=='array'){
			$dconfigoffer=$this->getUtildata()->getVaueOfArray($param, 'config.offer',true);
		}
		
		if(!is_array($dconfigoffer)){$dconfigoffer=$this->getJson()->decode($dconfigoffer,true);}
				
		$enable=$this->getUtildata()->getVaueOfArray($dconfigoffer, 'conclusion.enable',true);
		$dconfigoffer['conclusion']['level']='offer';
		if($enable){return $dconfigoffer;}
		
		return null;	
  }
}
