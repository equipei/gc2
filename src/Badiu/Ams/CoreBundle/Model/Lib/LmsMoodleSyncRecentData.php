<?php
namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class LmsMoodleSyncRecentData  extends BadiuModelLib {
	private $maxrecord=10000;
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	 function exec() {
		 
		 }
	
	
	//Lastaccess
	
	 function importLastaccessCourseFromMoodle($param) {
		 $serviceid=$this->getUtildata()->getVaueOfArray($param,'sserviceid');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
				
		 //get lisf of  lastaccess
		 $list=$this->getLastaccessCourseSource($param);
		 
		 $gradelibmanager= $this->getContainer()->get('badiu.admin.grade.lib.manager');
		 
		 //synuser
		  $paramdto=array();
		  $paramdto['sserver']['entity']=$entity;
		  $paramdto['sserver']['id']=$serviceid;
		  $syncusermlib=$this->getContainer()->get('badiu.admin.server.syncuser.managelib');
		  
		  $ssyncdata=$this->getContainer()->get('badiu.admin.server.syncdata.data');
		  $sparam=array('serviceid'=>$serviceid,'dtype'=>'frombadiunetremoteservice','modulekey'=>'badiu.ams.offer.classe');
		 
		 if(!is_array( $list)){return null;}
		  $gradessync=array();
		  
		  $presult=array('shouldprocess'=>sizeof($list),'executed'=>0);
		 
		 foreach ($list as $row) {
				$ruser=array();
				$ruser['id']=$this->getUtildata()->getVaueOfArray($row,'userid');
				$ruser['username']=$this->getUtildata()->getVaueOfArray($row,'username');
				$ruser['idnumber']=$this->getUtildata()->getVaueOfArray($row,'idnumber');
				$ruser['firstname']=$this->getUtildata()->getVaueOfArray($row,'firstname');
				$ruser['lastname']=$this->getUtildata()->getVaueOfArray($row,'lastname');
				$ruser['auth']=$this->getUtildata()->getVaueOfArray($row,'auth');
				
				$timeaccess=$this->getUtildata()->getVaueOfArray($row,'timeaccess');
				$courseid=$this->getUtildata()->getVaueOfArray($row,'courseid');
				$sparam['remotedataid']=$courseid;
				
				$sdto=$ssyncdata->getGlobalColumnsValue(' DISTINCT o.moduleinstance AS classeid ',$sparam);
				$classeid=$this->getUtildata()->getVaueOfArray($sdto,'classeid');
				
				if(!empty($timeaccess)){
					$tnow=new \DateTime();
					$tnow->setTimestamp($timeaccess);
					$timeaccess=$tnow;
				}
				
				$ruser['keysync']='username';
				$ruser['autosync']=1;
				$ruser['anonymousaccess']=0;
				$paramdto['user']=$ruser;
				$synuser=$syncusermlib->process($paramdto);

				$sysuserid=$this->getUtildata()->getVaueOfArray($synuser,'message');
				$sysuserstatus=$this->getUtildata()->getVaueOfArray($synuser,'status');
				
				if($sysuserstatus=='acept' && $sysuserid > 0 && $classeid > 0){
					$gradessync['userid']=$sysuserid;
					$gradessync['grade']=1;
					$gradessync['timeaccess']=$timeaccess;
					$gradessync['classeid']=$classeid;
					$gradessync['entity']=$entity;	
					$psyngrade=$gradelibmanager->addLastaccess($gradessync);
					$presult['recoredprocessed']=array('userid'=>$sysuserid,'classeid'=>$classeid,'key'=>$sysuserid.'|'.$classeid);
					if($psyngrade){$presult['executed']++;}
				
				}
			}
			
		 return $presult;
		 
		 
	 }
    function getLastaccessCourseSource($param) { 
		$serviceid=$this->getUtildata()->getVaueOfArray($param,'sserviceid');
		$lasttime=$this->getUtildata()->getVaueOfArray($param,'lasttime');
		$maxrows=$this->getUtildata()->getVaueOfArray($param,'maxrows');
		
		if(empty($maxrows)){$maxrows=$this->maxrecord;}
		if(empty($lasttime)){$lasttime=300;}
		
		$timeaccess =new \DateTime();
		$timeaccess=$timeaccess->getTimestamp();
		$timeaccess=$timeaccess-$lasttime;
		
		$sql="SELECT DISTINCT u.id AS userid,u.username,u.idnumber,u.firstname,u.lastname,u.email,u.auth,ul.courseid,ul.timeaccess FROM {_pfx}user u INNER JOIN {_pfx}user_lastaccess ul ON u.id=ul.userid  WHERE ul.timeaccess >= $timeaccess ";
		$webservice=$this->getContainer()->get('badiu.system.core.lib.webservice');
		$webservice->setServiceid($serviceid);
		$result=$webservice->search($sql,0,$maxrows);
		
		return $result;
	}
	
	
	//review set param to specify is is finalcoursegrade or ativicty grade
	 function importGradesFromMoodle($param) {
		$serviceid=$this->getUtildata()->getVaueOfArray($param,'sserviceid');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 
		 //get lisf of  grade
		 $listgrade=$this->getGradeCourseSource($param);
		 
		 $gradelibmanager= $this->getContainer()->get('badiu.admin.grade.lib.manager');
		 
		 //synuser
		  $paramdto=array();
		  $paramdto['sserver']['entity']=$entity;
		  $paramdto['sserver']['id']=$serviceid;
		  $syncusermlib=$this->getContainer()->get('badiu.admin.server.syncuser.managelib');
		  
		   $ssyncdata=$this->getContainer()->get('badiu.admin.server.syncdata.data');
		  $sparam=array('serviceid'=>$serviceid,'dtype'=>'frombadiunetremoteservice','modulekey'=>'badiu.ams.offer.classe');
		 
		 
		 if(!is_array( $listgrade)){return null;}
		  $gradessync=array();
		  
		  $presult=array('shouldprocess'=>sizeof($listgrade),'executed'=>0);
		  
		 foreach ($listgrade as $row) {
				$ruser=array();
				$ruser['id']=$this->getUtildata()->getVaueOfArray($row,'userid');
				$ruser['username']=$this->getUtildata()->getVaueOfArray($row,'username');
				$ruser['idnumber']=$this->getUtildata()->getVaueOfArray($row,'idnumber');
				$ruser['firstname']=$this->getUtildata()->getVaueOfArray($row,'firstname');
				$ruser['lastname']=$this->getUtildata()->getVaueOfArray($row,'lastname');
				$ruser['auth']=$this->getUtildata()->getVaueOfArray($row,'auth');
				
				$finalgrade=$this->getUtildata()->getVaueOfArray($row,'finalgrade');
				$timecreatedgrade=$this->getUtildata()->getVaueOfArray($row,'timecreated');
				$timemodifiedgrade=$this->getUtildata()->getVaueOfArray($row,'timemodified');
				
				$courseid=$this->getUtildata()->getVaueOfArray($row,'courseid');
				$sparam['remotedataid']=$courseid;
				
				$sdto=$ssyncdata->getGlobalColumnsValue(' DISTINCT o.moduleinstance AS classeid ',$sparam);
				$classeid=$this->getUtildata()->getVaueOfArray($sdto,'classeid');
				
				$ruser['keysync']='username';
				$ruser['autosync']=1;
				$ruser['anonymousaccess']=0;
				$paramdto['user']=$ruser;
				$synuser=$syncusermlib->process($paramdto);

				$sysuserid=$this->getUtildata()->getVaueOfArray($synuser,'message');
				$sysuserstatus=$this->getUtildata()->getVaueOfArray($synuser,'status');
				if($sysuserstatus=='acept' && $sysuserid > 0  && $classeid > 0){
					$gradessync['userid']=$sysuserid;
					$gradessync['finalgrade']=$finalgrade;
					$gradessync['timecreated']=$timecreatedgrade;
					$gradessync['timemodified']=$timemodifiedgrade;
					$gradessync['classeid']=$classeid;
					$gradessync['entity']=$entity;
					$psyngrade=$gradelibmanager->addGrade($gradessync);
					$presult['recoredprocessed']=array('userid'=>$sysuserid,'classeid'=>$classeid,'key'=>$sysuserid.'|'.$classeid);
					if($psyngrade){$presult['executed']++;}
				
				}
			}
			
		 return $presult;
		 
		 
	 }
    function getGradeCourseSource($param) { 
		$serviceid=$this->getUtildata()->getVaueOfArray($param,'sserviceid');
		$lasttime=$this->getUtildata()->getVaueOfArray($param,'lasttime');
		$maxrows=$this->getUtildata()->getVaueOfArray($param,'maxrows');
		
		if(empty($maxrows)){$maxrows=$this->maxrecord;}
		if(empty($lasttime)){$lasttime=300;}
		$timeaccess =new \DateTime();
		$timeaccess=$timeaccess->getTimestamp();
		$timeaccess=$timeaccess-$lasttime;
		
		
		$sql="SELECT g.id,g.finalgrade,g.timecreated,g.timemodified,g.userid,u.username,u.idnumber,u.firstname,u.lastname,u.email,u.auth,i.courseid FROM {_pfx}grade_items i INNER JOIN {_pfx}grade_grades g ON i.id=g.itemid INNER JOIN {_pfx}user u ON u.id=g.userid WHERE i.itemtype = 'course' AND  g.timemodified >= $timeaccess ";
		$webservice=$this->getContainer()->get('badiu.system.core.lib.webservice');
		$webservice->setServiceid($serviceid);
		$result=$webservice->search($sql,0,$maxrows);
		return $result;
	}
	
	
	//coursecompletation 
	
	 function importCourseEnrolCompletationFromMoodle($param) {
		$serviceid=$this->getUtildata()->getVaueOfArray($param,'sserviceid');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 
		 //get lisf of  completation
		 $listgrade=$this->getCourseEnrolCompletation($param);
		
		 $gradelibmanager= $this->getContainer()->get('badiu.admin.grade.lib.manager');
		 
		 //synuser
		  $paramdto=array();
		  $paramdto['sserver']['entity']=$entity;
		  $paramdto['sserver']['id']=$serviceid;
		  $syncusermlib=$this->getContainer()->get('badiu.admin.server.syncuser.managelib');
		  
		    $ssyncdata=$this->getContainer()->get('badiu.admin.server.syncdata.data');
		  $sparam=array('serviceid'=>$serviceid,'dtype'=>'frombadiunetremoteservice','modulekey'=>'badiu.ams.offer.classe');
		 
		 
		 if(!is_array( $listgrade)){return null;}
		  $gradessync=array();
		 
		  $presult=array('shouldprocess'=>sizeof($listgrade),'executed'=>0);
		  
		 foreach ($listgrade as $row) {
				$ruser=array();
				$ruser['id']=$this->getUtildata()->getVaueOfArray($row,'userid');
				$ruser['username']=$this->getUtildata()->getVaueOfArray($row,'username');
				$ruser['idnumber']=$this->getUtildata()->getVaueOfArray($row,'idnumber');
				$ruser['firstname']=$this->getUtildata()->getVaueOfArray($row,'firstname');
				$ruser['lastname']=$this->getUtildata()->getVaueOfArray($row,'lastname');
				$ruser['auth']=$this->getUtildata()->getVaueOfArray($row,'auth');
				
				$timecompleted=$this->getUtildata()->getVaueOfArray($row,'timecompleted');
				
				$courseid=$this->getUtildata()->getVaueOfArray($row,'courseid');
				$sparam['remotedataid']=$courseid;
				
				$sdto=$ssyncdata->getGlobalColumnsValue(' DISTINCT o.moduleinstance AS classeid ',$sparam);
				$classeid=$this->getUtildata()->getVaueOfArray($sdto,'classeid');
				
				$ruser['keysync']='username';
				$ruser['autosync']=1;
				$ruser['anonymousaccess']=0;
				$paramdto['user']=$ruser;
				$synuser=$syncusermlib->process($paramdto);

				$sysuserid=$this->getUtildata()->getVaueOfArray($synuser,'message');
				$sysuserstatus=$this->getUtildata()->getVaueOfArray($synuser,'status');
				if($sysuserstatus=='acept' && $sysuserid > 0  && $classeid > 0){
					$gradessync['userid']=$sysuserid;
					$gradessync['finalgrade']=1;
					$gradessync['timecreated']=$timecompleted;
					$gradessync['classeid']=$classeid;
					$gradessync['entity']=$entity;
					$psyngrade=$gradelibmanager->addFinalCompletation($gradessync);
					$presult['recoredprocessed']=array('userid'=>$sysuserid,'classeid'=>$classeid,'key'=>$sysuserid.'|'.$classeid);
					if($psyngrade){$presult['executed']++;}
				
				}
			} 
			
		 return $presult;
		 
		 
	 }
	 
	  function getCourseEnrolCompletation($param) { 
		$serviceid=$this->getUtildata()->getVaueOfArray($param,'sserviceid');
		$lasttime=$this->getUtildata()->getVaueOfArray($param,'lasttime');
		$maxrows=$this->getUtildata()->getVaueOfArray($param,'maxrows');
		
		if(empty($maxrows)){$maxrows=$this->maxrecord;}
		if(empty($lasttime)){$lasttime=300;}
		$timeaccess =new \DateTime();
		$timeaccess=$timeaccess->getTimestamp();
		$timeaccess=$timeaccess-$lasttime;
		$sql="SELECT cp.userid,cp.timecompleted,u.username,u.idnumber,u.firstname,u.lastname,u.email,u.auth,cp.course AS courseid FROM {_pfx}course_completions cp INNER JOIN {_pfx}user u ON u.id=cp.userid WHERE  cp.timecompleted >= $timeaccess ";

		$webservice=$this->getContainer()->get('badiu.system.core.lib.webservice');
		$webservice->setServiceid($serviceid);
		$result=$webservice->search($sql,0,$maxrows);
		return $result;
	}
	
	//courseprogress
	
	 function importCourseEnrolProgressFromMoodle($param) {
		$serviceid=$this->getUtildata()->getVaueOfArray($param,'sserviceid');
		
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 
		 //get lisf of  grade
		 
		 $listgrade=$this->getCourseEnrolProgress($param);
		
		 $gradelibmanager= $this->getContainer()->get('badiu.admin.grade.lib.manager');
		 
		 $ssyncdata=$this->getContainer()->get('badiu.admin.server.syncdata.data');
		  $sparam=array('serviceid'=>$serviceid,'dtype'=>'frombadiunetremoteservice','modulekey'=>'badiu.ams.offer.classe');
		
		 //synuser
		  $paramdto=array();
		  $paramdto['sserver']['entity']=$entity;
		  $paramdto['sserver']['id']=$serviceid;
		  $syncusermlib=$this->getContainer()->get('badiu.admin.server.syncuser.managelib');
		  
		 if(!is_array( $listgrade)){return null;}
		  $gradessync=array();
		  $presult=array('shouldprocess'=>sizeof($listgrade),'executed'=>0);
		 
		 foreach ($listgrade as $row) {
				$ruser=array();
				$ruser['id']=$this->getUtildata()->getVaueOfArray($row,'userid');
				$ruser['username']=$this->getUtildata()->getVaueOfArray($row,'username');
				$ruser['idnumber']=$this->getUtildata()->getVaueOfArray($row,'idnumber');
				$ruser['firstname']=$this->getUtildata()->getVaueOfArray($row,'firstname');
				$ruser['lastname']=$this->getUtildata()->getVaueOfArray($row,'lastname');
				$ruser['auth']=$this->getUtildata()->getVaueOfArray($row,'auth');
				
				$countactivityanebleprogress=$this->getUtildata()->getVaueOfArray($row,'countactivityanebleprogress');
				$countactivitycompleted=$this->getUtildata()->getVaueOfArray($row,'countactivitycompleted');
				$countactivityrequiretocoursecomplete=$this->getUtildata()->getVaueOfArray($row,'countactivityrequiretocoursecomplete');
				$countactivitycomletedrequiretocoursecomplete=$this->getUtildata()->getVaueOfArray($row,'countactivitycomletedrequiretocoursecomplete');
				
				$courseid=$this->getUtildata()->getVaueOfArray($row,'courseid');
				$sparam['remotedataid']=$courseid;
				
				$sdto=$ssyncdata->getGlobalColumnsValue(' DISTINCT o.moduleinstance AS classeid ',$sparam);
				$classeid=$this->getUtildata()->getVaueOfArray($sdto,'classeid');
				
				 
				$ruser['keysync']='username';
				$ruser['autosync']=1;
				$ruser['anonymousaccess']=0;
				$paramdto['user']=$ruser;
				$synuser=$syncusermlib->process($paramdto);

				$sysuserid=$this->getUtildata()->getVaueOfArray($synuser,'message');
				$sysuserstatus=$this->getUtildata()->getVaueOfArray($synuser,'status');
				if($sysuserstatus=='acept' && $sysuserid > 0 && $classeid > 0){
					$gradessync['userid']=$sysuserid;
					$gradessync['finalgrade']=1;
					$gradessync['countactivityanebleprogress']=$countactivityanebleprogress;
					$gradessync['countactivitycompleted']=$countactivitycompleted;
					$gradessync['countactivityrequiretocoursecomplete']=$countactivityrequiretocoursecomplete;
					$gradessync['countactivitycomletedrequiretocoursecomplete']=$countactivitycomletedrequiretocoursecomplete;
					$gradessync['classeid']=$classeid;
					$gradessync['entity']=$entity;
					$psyngrade=$gradelibmanager->addProgress($gradessync);
					$presult['recoredprocessed']=array('userid'=>$sysuserid,'classeid'=>$classeid,'key'=>$sysuserid.'|'.$classeid);
					if($psyngrade){$presult['executed']++;}
				
				}
			}
			
		 return $presult;
		 
		 
	 }
	 
	  function getCourseEnrolProgress($param) { 
		$serviceid=$this->getUtildata()->getVaueOfArray($param,'sserviceid');
		$lasttime=$this->getUtildata()->getVaueOfArray($param,'lasttime');
		$maxrows=$this->getUtildata()->getVaueOfArray($param,'maxrows');
		
		if(empty($maxrows)){$maxrows=$this->maxrecord;}
		if(empty($lasttime)){$lasttime=300;}
		$timeaccess =new \DateTime();
		$timeaccess=$timeaccess->getTimestamp();
		$timeaccess=$timeaccess-$lasttime;
		
		$sql="SELECT DISTINCT u.id AS userid,u.username,u.idnumber,u.firstname,u.lastname,u.email,u.auth,cm.course AS courseid, (SELECT COUNT(DISTINCT cm1.id) FROM {_pfx}course_modules cm1 WHERE cm1.course=cm.course AND cm1.completion > 0  AND cm1.visibleold=1 AND cm1.deletioninprogress=0) AS countactivityanebleprogress, (SELECT COUNT(DISTINCT cmp2.id) FROM {_pfx}course_modules_completion cmp2 INNER JOIN {_pfx}course_modules cm2 ON cmp2.coursemoduleid = cm2.id WHERE  cm2.course=cm.course AND cmp2.userid=cmp.userid AND cm2.completion > 0 AND cmp2.completionstate > 0 AND cmp2.completionstate < 3 AND cm2.visibleold=1 AND cm2.deletioninprogress=0) AS countactivitycompleted,(SELECT COUNT(DISTINCT cm3.id) FROM {_pfx}course_modules cm3 INNER JOIN {_pfx}course_completion_criteria ccc3 ON cm3.id=ccc3.moduleinstance  WHERE cm3.course=ccc3.course AND ccc3.criteriatype=4 AND cm3.course=cm.course AND cm3.completion > 0  AND cm3.visibleold=1 AND cm3.deletioninprogress=0 ) AS countactivityrequiretocoursecomplete, (SELECT COUNT(DISTINCT cmp4.id)  FROM {_pfx}course_modules_completion cmp4 INNER JOIN {_pfx}course_modules cm4 ON cmp4.coursemoduleid = cm4.id INNER JOIN {_pfx}course_completion_criteria ccc4 ON cm4.id=ccc4.moduleinstance WHERE cm4.course=ccc4.course AND ccc4.criteriatype=4 AND cmp4.coursemoduleid=ccc4.moduleinstance AND cm4.course=cm.course AND cmp4.userid=cmp.userid AND cm4.completion > 0 AND cmp4.completionstate > 0 AND cmp4.completionstate < 3 AND cm4.visibleold=1 AND cm4.deletioninprogress=0) AS countactivitycomletedrequiretocoursecomplete FROM {_pfx}course_modules_completion cmp INNER JOIN {_pfx}course_modules cm ON cmp.coursemoduleid = cm.id INNER JOIN {_pfx}user u ON cmp.userid=u.id  WHERE cmp.timemodified >= $timeaccess ";

		$webservice=$this->getContainer()->get('badiu.system.core.lib.webservice');
		$webservice->setServiceid($serviceid);
		$result=$webservice->search($sql,0,$maxrows);
		
		return $result;
	}
    function getMaxrecord(){
        return $this->maxrecord;
    }

    function setMaxrecord($maxrecord) {
        $this->maxrecord = $maxrecord;
    }		
}
