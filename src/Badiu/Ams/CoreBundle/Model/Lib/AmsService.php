<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Badiu\Ams\CoreBundle\Model\DOMAINTABLE;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;

class AmsService extends BadiuExternalService
{
/**
     * @var Container
     */
    private $container;

	private $response;
	function __construct(Container $container)
    {
        parent::__construct($container);
		$this->response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
      }

	public function exec()
    {	
		//get param
		$param=$this->getParam();
		
		$checkParam=$this->checkParam($param);
		if(empty($chkParam)){return $this->getResponse()->get();}
		
	
		$chekToken=$this->checkToken($param->token);
		if(!$chekToken){return $this->getResponse()->get();}
		
		//exec service
						
		return $response;
    }
	
	public function getParam(){
		$param= new \stdClass();
		$param->token=$this->getContainer()->get('request')->get('_token');
		$param->courseid=$this->getContainer()->get('request')->get('courseid');
		$param->userid=$this->getContainer()->get('request')->get('userid');
		
		return $param;
	}
	
	public function checkParam($param){
		$resp=TRUE;
		if(empty($param->token)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo(SERVICEDOMAINTABLE::$ERROR_TOKEN_EMPTY);
		}
		else if(empty($param->courseid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo(DOMAINTABLE::$ERROR_COURSEID_EMPTY);
		}
		else if(empty($param->userid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo(SERVICEDOMAINTABLE::$ERROR_USERID_EMPTY);
		}
		return $resp;
	}
	
	//extende of BadiuExternalService
	/* 
	public function getServiceid($token){
		$servicedata=$this->getContainer()->get('badiu.admin.server.service.data');
		$result=$servicedata->getIdByToken($token);
		return $result;
	}*/
	 public function getResponse() {
          return $this->response;
      }


      public function setResponse($response) {
          $this->response = $response;
      }
}
?>