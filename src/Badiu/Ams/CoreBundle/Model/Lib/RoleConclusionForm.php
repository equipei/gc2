<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuWebService;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class RoleConclusionForm    extends BadiuModelLib{

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
  
   /*
	{		
		conclusion:{
			enable: 1
			defaultroles: {
				dhour: {minperc: 70},
				credit: {minperc: 70},
				requireddiscipline: {minperc: 70},
				roleconclusionnonrequireddiscipline: {minperc: 70},
				}
			apply: "manual | automtic ",
			
			discipline: {
				enable: 1
				defaultroles: {
					dhour: {minperc: 70},
					grade: {minperc: 70},
					lmscoursecompletation: ',
				}
				apply: "manual | automtic "
			}
		}
		*/
	public function offerFormChangeParam($param) {
		 $dconfig = $this->getUtildata()->getVaueOfArray($param, 'dconfig');
		 if(!is_array($dconfig)){ $dconfig=$this->getJson()->decode($dconfig,true);}
		 
		
	
		$dconfig['conclusion']['enable']=$this->getUtildata()->getVaueOfArray($param, 'roleconclusionenable');
		unset($param['roleconclusionenable']);
		
		$dconfig['conclusion']['defaultroles']['dhour']['minperc']=$this->getUtildata()->getVaueOfArray($param, 'roleconclusiondhour');
		unset($param['roleconclusiondhour']);
		
		$dconfig['conclusion']['defaultroles']['credit']['minperc']=$this->getUtildata()->getVaueOfArray($param, 'roleconclusioncredit');
		unset($param['roleconclusioncredit']);
		
		$dconfig['conclusion']['defaultroles']['requireddiscipline']['minperc']=$this->getUtildata()->getVaueOfArray($param, 'roleconclusionrequireddiscipline');
		unset($param['roleconclusionrequireddiscipline']);
		
		$dconfig['conclusion']['defaultroles']['nonrequireddiscipline']['minperc']=$this->getUtildata()->getVaueOfArray($param, 'roleconclusionnonrequireddiscipline');
		unset($param['roleconclusionnonrequireddiscipline']);
		
		$dconfig['conclusion']['apply']=$this->getUtildata()->getVaueOfArray($param, 'roleconclusiontypeoperation');
		unset($param['roleconclusiontypeoperation']);
		
		//default discipline
		$dconfig['conclusion']['discipline']['enable']=$this->getUtildata()->getVaueOfArray($param, 'droleconclusionenable');
		unset($param['droleconclusionenable']);
		
		$dconfig['conclusion']['discipline']['defaultroles']['dhour']['minperc']=$this->getUtildata()->getVaueOfArray($param, 'droleconclusiondhour');
		unset($param['droleconclusiondhour']);
		
		$dconfig['conclusion']['discipline']['defaultroles']['grade']['minperc']=$this->getUtildata()->getVaueOfArray($param, 'droleconclusiongrade');
		unset($param['droleconclusiongrade']);
		
		$dconfig['conclusion']['discipline']['defaultroles']['lmscoursecompletation']=$this->getUtildata()->getVaueOfArray($param, 'lmscoursecompletation');
		unset($param['lmscoursecompletation']);
		
		$dconfig['conclusion']['discipline']['apply']=$this->getUtildata()->getVaueOfArray($param, 'droleconclusiontypeoperation');
		unset($param['droleconclusiontypeoperation']);
		
		
		$param['dconfig'] = $dconfig;
		return $param;
		
	}

	public function offerFormChangeParamOnOpen($param) {
		$dconfig = $this->getUtildata()->getVaueOfArray($param, 'dconfig');
		if(!is_array($dconfig)){ $dconfig=$this->getJson()->decode($dconfig,true);}
		
		$param['roleconclusionenable']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.enable',true);
		$param['roleconclusiondhour']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.defaultroles.dhour.minperc',true);
		$param['roleconclusioncredit']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.defaultroles.credit.minperc',true);
		$param['roleconclusionrequireddiscipline']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.defaultroles.requireddiscipline.minperc',true);
		$param['roleconclusionnonrequireddiscipline']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.defaultroles.nonrequireddiscipline.minperc',true);
		$param['roleconclusiontypeoperation']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.apply',true);
		
		
		//default discipline
		$param['droleconclusionenable']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.enable',true);
		$param['droleconclusiondhour']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.defaultroles.dhour.minperc',true);
		$param['droleconclusiongrade']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.defaultroles.grade.minperc',true);
		$param['lmscoursecompletation']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.defaultroles.lmscoursecompletation',true);
		$param['droleconclusiontypeoperation']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.apply',true);
		
		return $param;
	}
	
	public function disciplineFormChangeParam($param) {
		 $dconfig = $this->getUtildata()->getVaueOfArray($param, 'dconfig');
		 if(!is_array($dconfig)){ $dconfig=$this->getJson()->decode($dconfig,true);}
		
		$dconfig['conclusion']['discipline']['enable']=$this->getUtildata()->getVaueOfArray($param, 'roleconclusionenable');
		unset($param['roleconclusionenable']);
		
		
		$dconfig['conclusion']['discipline']['defaultroles']['dhour']['minperc']=$this->getUtildata()->getVaueOfArray($param, 'roleconclusiondhour');
		unset($param['roleconclusiondhour']);
		
		$dconfig['conclusion']['discipline']['defaultroles']['grade']['minperc']=$this->getUtildata()->getVaueOfArray($param, 'roleconclusiongrade');
		unset($param['roleconclusiongrade']);
		
		$dconfig['conclusion']['discipline']['defaultroles']['lmscoursecompletation']=$this->getUtildata()->getVaueOfArray($param, 'lmscoursecompletation');
		unset($param['lmscoursecompletation']);
		
		$dconfig['conclusion']['discipline']['apply']=$this->getUtildata()->getVaueOfArray($param, 'roleconclusiontypeoperation');
		unset($param['roleconclusiontypeoperation']);
		
		
		$param['dconfig'] = $dconfig;
		return $param;
		
	}

	public function disciplineFormChangeParamOnOpen($param) {
		$dconfig = $this->getUtildata()->getVaueOfArray($param, 'dconfig');
		if(!is_array($dconfig)){ $dconfig=$this->getJson()->decode($dconfig,true);}
		
		$param['roleconclusionenable']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.enable',true);
		$param['roleconclusiondhour']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.defaultroles.dhour.minperc',true);
		$param['roleconclusiongrade']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.defaultroles.grade.minperc',true);
		$param['lmscoursecompletation']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.defaultroles.lmscoursecompletation',true);
		$param['roleconclusiontypeoperation']=$this->getUtildata()->getVaueOfArray($dconfig, 'conclusion.discipline.apply',true);
		
		return $param;
	}
}
