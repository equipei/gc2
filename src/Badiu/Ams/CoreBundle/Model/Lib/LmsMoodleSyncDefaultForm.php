<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuWebService;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class LmsMoodleSyncDefaultForm    extends BadiuModelLib{ 
	private $parentid;
    function __construct(Container $container) {
                parent::__construct($container);
           
       }
    
	   public function initParamConfig($type='offer') {
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		$badiuSession->setHashkey($this->getSessionhashkey());
		
		$param=array();
		$defaultserverservice=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.defaultserverservice');
		$param['sserviceid'] = $defaultserverservice;

		$defaulttype=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.defaulttype');
		$param['lmssynctype'] = $defaulttype;
		
		$defaultcourselevel=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.defaultcourselevel');
		if($type=='classe'){$defaultcourselevel=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.defaultclasselevel');}
		$param['lmssynclevel'] = $defaultcourselevel;

		$defaultcoursecategory=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.defaultcoursecategory');
		$param['lmscoursecatparent'] = $defaultcoursecategory;

		$defaultautomatic=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.automatic');
		$param['lmssyncautomatic'] = $defaultautomatic;

		$defaultforceupdate=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.forceupdate');
		$param['lmssyncforceupdate'] = $defaultforceupdate;
				
		
		return $param;
	   }
	   
	   public function defaultChangeParamOnEdit($param) {
			$cparam=$this->initParamConfig();
			$lmssyncforceupdate=$this->getUtildata()->getVaueOfArray($cparam,'lmssyncforceupdate');
			$param['lmssyncforceupdate'] = $lmssyncforceupdate;
			return $param;
		
		}
	
	public function offerFormChangeParam($param) {
		
		return $param;
		
	}

	public function offerFormChangeParamOnOpen($param) {
		$cparam=$this->initParamConfig();
		$defaultserverservice=$this->getUtildata()->getVaueOfArray($cparam,'sserviceid');
		$param['sserviceid'] = $defaultserverservice;
		
		$defaulttype=$this->getUtildata()->getVaueOfArray($cparam,'lmssynctype');
		$param['lmssynctype'] = $defaulttype;

		$defaultcourselevel=$this->getUtildata()->getVaueOfArray($cparam,'lmssynclevel');
		$param['lmssynclevel'] = $defaultcourselevel;

		$defaultcoursecategory=$this->getUtildata()->getVaueOfArray($cparam,'lmscoursecatparent');
		$param['lmscoursecatparent'] = $defaultcoursecategory;

		$lmssyncforceupdate=$this->getUtildata()->getVaueOfArray($cparam,'lmssyncforceupdate');
		$param['lmssyncforceupdate'] = $lmssyncforceupdate;
		
		$mdlcoursecatname=null;
		if(!empty($defaultserverservice) && !empty($defaultcoursecategory)){
			$mdldatautil=$this->getContainer()->get('badiu.moodle.core.lib.datautil');
			$mdlcoursecatname=$mdldatautil->getCoursecatname($defaultserverservice,$defaultcoursecategory);

			$dconfig=array();
			$dconfig['lmsintegration']['lmscoursecatname']=$mdlcoursecatname;
			$dconfig= $this->getJson()->encode($dconfig);
			$param['dconfig']=$dconfig;
		} 
			return $param;
	}
	
	public function disciplineFormChangeParam($param) {
		
		return $param;
		
	}

	public function disciplineFormChangeParamOnOpen($param) {
		
		return $param;
	}

	public function classeFormChangeParam($param) {
		$cparam=$this->initParamConfig('classe');
		
		return $param;
		
	}

	public function classeFormChangeParamOnOpen($param) {
		if(empty($this->parentid)){return $param;}
		$cparam=$this->initParamConfig('classe');

		$param['lmssynctype'] =$this->getUtildata()->getVaueOfArray($cparam, 'lmssynctype');
		$param['lmssynclevel'] = $this->getUtildata()->getVaueOfArray($cparam, 'lmssynclevel');
	    
		$lmscoursecatparent = $this->getUtildata()->getVaueOfArray($param, 'lmscoursecatparent');
		if(empty($lmscoursecatparent)){$param['lmscoursecatparent']=$this->getUtildata()->getVaueOfArray($param, 'lmscoursecat');}

		$lmssyncforceupdate=$this->getUtildata()->getVaueOfArray($cparam,'lmssyncforceupdate');
		$param['lmssyncforceupdate'] = $lmssyncforceupdate;
		return $param;
	}

	public function mergeCofig($defaultparam,$inheritparam,$targetparam,$level='classe') {
	/*	$status= $this->getUtildata()->getVaueOfArray($oddconfig, 'lmsintegration.status',true);
	
		$sserviceid=$this->getUtildata()->getVaueOfArray($inheritparam, 'lmsintegration.sserviceid',true);

		if(empty($sserviceid)){
			$sserviceid=$this->getUtildata()->getVaueOfArray($defaultparam, 'lmsintegration.sserviceid',true);
			if(empty($sserviceid)){return $targetparam;}
			$param['sserviceid'] = $sserviceid;
			$param['lmssynctype'] =$this->getUtildata()->getVaueOfArray($defaultparam, 'lmssynctype');
			$param['lmssynclevel'] = $this->getUtildata()->getVaueOfArray($defaultparam, 'lmssynclevel');
			return $param;
		}
	*/

		return $targetparam;
	}
	/**
     * @return integer
     */
	function getParentid()
    {
        return $this->parentid;
    }

    /**
     * @param integer $parentid
     */
    public function setParentid($parentid)
    { 
		        $this->parentid = $parentid;
    }
}
