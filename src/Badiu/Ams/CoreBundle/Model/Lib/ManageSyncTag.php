<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ManageSyncTag extends BadiuModelLib {

	function __construct(Container $container) {
                parent::__construct($container);
           
       }
	// system/service/process?_service=badiu.ams.core.lib.managesynctag&_function=addTagNameDiscipline
	public function  addTagNameDiscipline($dtype='training'){
		//list discipline
		$disciplinedata=$this->getContainer()->get('badiu.ams.discipline.discipline.data');
		$fparam=array('dtype'=>$dtype);
		$listd=$disciplinedata->getGlobalColumnsValues('o.id,o.name',$fparam);
		if(empty($listd)){return null;}
		if(!is_array($listd)){return null;}
		$modulekey='badiu.tms.discipline.discipline';
		if($dtype=='course'){$modulekey='badiu.ams.discipline.discipline';}
		$tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
		$entity=$this->getEntity();
		$cont=0;
		foreach ($listd as $row) {
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			if(!empty($id ) && !empty($name )&& !empty($modulekey )){
				$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'dtype'=>'name');
				$paramadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','timecreated'=>new \DateTime());
				$paramedit=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','timemodified'=>new \DateTime());
				$tagdata->addNativeSql($paramcheckexist,$paramadd,$paramedit);
				$cont++;
			}
		}
		
		return $cont;
	}
	// system/service/process?_service=badiu.ams.core.lib.managesynctag&_function=addTagNameTmsClasse
	public function  addTagNameTmsClasse(){
		//list discipline
		$entity=$this->getEntity();
		$listd=$this->getTmsClasse();
		if(empty($listd)){return null;}
		if(!is_array($listd)){return null;}
		$modulekey='badiu.tms.offer.classe';
		$tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
		
		$cont=0;
		foreach ($listd as $row) {
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			if(!empty($id ) && !empty($name )&& !empty($modulekey )){
				$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'dtype'=>'name');
				$paramadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','timecreated'=>new \DateTime());
				$paramedit=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','timemodified'=>new \DateTime());
				$tagdata->addNativeSql($paramcheckexist,$paramadd,$paramedit);
				$cont++;
			}
		}
		
		return $cont;
	}
	
	// system/service/process?_service=badiu.ams.core.lib.managesynctag&_function=addTagNameTmsDisciplineCategory
	public function  addTagNameTmsDisciplineCategory(){
		//list discipline
		$entity=$this->getEntity();
		$listd=$this->getTmsDisciplineCategory();
		if(empty($listd)){return null;}
		if(!is_array($listd)){return null;}
		$modulekey='badiu.tms.discipline.category';
		$tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
		
		$cont=0;
		foreach ($listd as $row) {
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			if(!empty($id ) && !empty($name )&& !empty($modulekey )){
				$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'dtype'=>'name');
				$paramadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','timecreated'=>new \DateTime());
				$paramedit=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','timemodified'=>new \DateTime());
				$tagdata->addNativeSql($paramcheckexist,$paramadd,$paramedit);
				$cont++;
			}
		}
		
		return $cont;
	}
	// system/service/process?_service=badiu.ams.core.lib.managesynctag&_function=addTagNameTmsOffer
	public function  addTagNameTmsOffer(){
		//list discipline
		$entity=$this->getEntity();
		$listd=$this->getTmsOffer();
		if(empty($listd)){return null;}
		if(!is_array($listd)){return null;}
		$modulekey='badiu.tms.offer.offer';
		$tagdata=$this->getContainer()->get('badiu.system.module.tag.data');
		
		$cont=0;
		foreach ($listd as $row) {
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			if(!empty($id ) && !empty($name )&& !empty($modulekey )){
				$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'dtype'=>'name');
				$paramadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','timecreated'=>new \DateTime());
				$paramedit=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$id,'value'=>$name,'dtype'=>'name','timemodified'=>new \DateTime());
				$tagdata->addNativeSql($paramcheckexist,$paramadd,$paramedit);
				$cont++;
			}
		}
		
		return $cont;
	}
	public function  getTmsClasse(){
		 $cdata=$this->getContainer()->get('badiu.ams.offer.classe.data');
		 $sql="SELECT o.id,o.name  FROM BadiuAmsOfferBundle:AmsOfferClasse o JOIN o.odisciplineid od JOIN od.disciplineid d WHERE  o.entity=:entity AND d.dtype='training' ";
		$entity=$this->getEntity();
		$query = $cdata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$result = $query->getResult();
		return $result; 
		  
	}
	
	public function  getTmsDisciplineCategory(){
		 $cdata=$this->getContainer()->get('badiu.ams.offer.classe.data');
		 $sql="SELECT dct.id,dct.name  FROM BadiuAmsOfferBundle:AmsOfferClasse o JOIN o.odisciplineid od JOIN od.disciplineid d  JOIN d.categoryid dct WHERE  o.entity=:entity AND d.dtype='training' ";
		$entity=$this->getEntity();
		$query = $cdata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$result = $query->getResult();
		return $result; 
		  
	}
	
	public function  getTmsOffer(){
		 $cdata=$this->getContainer()->get('badiu.ams.offer.classe.data');
		 $sql="SELECT o.id,o.name  FROM BadiuAmsOfferBundle:AmsOffer o   WHERE o.entity=:entity AND  o.shortname!='tmsdefaultcourseoffer' AND o.dtype='training' ";
		$entity=$this->getEntity();
		$query = $cdata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$result = $query->getResult();
		return $result; 
		  
	}
}
?>