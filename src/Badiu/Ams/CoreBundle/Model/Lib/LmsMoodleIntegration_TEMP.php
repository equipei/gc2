<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Badiu\Ams\CoreBundle\Model\DOMAINTABLE;
class LmsMoodleIntegration extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
    
              //review
     public function offer($dto) {
       
          $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
           
            if(empty($dto->getSserviceid())){return $dto;}
            if(empty($dto->getSserviceid()) && empty($dto->getSserviceid()->getId())){return $dto;}
            // $dto->setLmscontextid($this->getId($dto->getLmscontextid()));
            // echo $dto->getLmscontextid();exit;
            //sserviceid,lmssynctype,lmscontexttypeparent,lmscontextidparent,lmscontexttype,lmscontextid
                if($dto->getLmssynctype()==DOMAINTABLE::$LMS_SYNC_WITHCREATED_DATA){
                    
                }else if($dto->getLmssynctype()==DOMAINTABLE::$LMS_SYNC_REPLICATION_DATA){
                  
                   // if($sysoperation->isEdit() &&  !empty($dto->getLmscontexttype()) &&  !empty($dto->getLmscontextid())){return $dto;} 
                   
                     $dto->setLmscontexttypeparent(DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY);
                     $dto->setLmscontextidparent($dto->getLmscontextid());
                     
                    $dto->setLmscontexttype(DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY);
                   
                     $mdlcoursecatname=$dto->getName();
                    //get course name
                    $offerdata=$this->getContainer()->get('badiu.ams.offer.offer.data');
                    $coursename=$offerdata->getCourseName($dto->getId());
                    if(!empty($coursename)){$mdlcoursecatname =$coursename. " - ".$dto->getName(); }
                   // echo $dto->getLmscontextid();
               //   echo "xx";
                    //crete category
                    $search=$this->getContainer()->get('badiu.system.core.functionality.search');
                    $param=array('_serviceid'=>$dto->getSserviceid()->getId(),'key'=>'course.category.create','parentid'=>$dto->getLmscontextid(),'name'=>$mdlcoursecatname);
                   // print_r($param);echo "<hr>";
                    $result=$search->searchWebService($param);
                   // print_r($result);
                    $categoryid=null;
                    if(isset($result['status']) && $result['status']=='accept'){$categoryid=$result['message'];}
                    $dto->setLmscontextid($categoryid);
                    
                     $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
                    $param=array('lmscontextid'=>$dto->getLmscontextid(),'lmscontexttype'=>$dto->getLmscontexttype(),'lmscontextidparent'=>$dto->getLmscontextidparent(),'lmscontexttypeparent'=>$dto->getLmscontexttypeparent(),'id'=>$dto->getId());
                   //echo "<hr>";
                   // print_r($param);
                   $resultup= $odata->updateNativeSql($param);
                 
                }
             
           
           return $dto;
           
           
     }
      //review
      function period($dto) {
         $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
          if(empty($dto->getSserviceid())){return $dto;}
            if(empty($dto->getSserviceid()) && empty($dto->getSserviceid()->getId())){return $dto;}
            // $dto->setLmscontextid($this->getId($dto->getLmscontextid()));
            // echo $dto->getLmscontextid();exit;
            //sserviceid,lmssynctype,lmscontexttypeparent,lmscontextidparent,lmscontexttype,lmscontextid
                if($dto->getLmssynctype()==DOMAINTABLE::$LMS_SYNC_WITHCREATED_DATA){
                    
                }else if($dto->getLmssynctype()==DOMAINTABLE::$LMS_SYNC_REPLICATION_DATA){
                  
                    if($sysoperation->isEdit() &&  !empty($dto->getLmscontexttype()) &&  !empty($dto->getLmscontextid())){return $dto;} 
                     $dto->setLmscontexttypeparent(DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY);
                     $dto->setLmscontextidparent($dto->getLmscontextid());
                     
                    $dto->setLmscontexttype(DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY);
                   
                     $mdlcoursecatname=$dto->getName();
                    
                  
                    //crete category
                    $search=$this->getContainer()->get('badiu.system.core.functionality.search');
                    $param=array('_serviceid'=>$dto->getSserviceid()->getId(),'key'=>'course.category.create','parentid'=>$dto->getLmscontextid(),'name'=>$mdlcoursecatname);
                   // print_r($param);echo "<hr>";
                    $result=$search->searchWebService($param);
                   // print_r($result);
                    $categoryid=null;
                    if(isset($result['status']) && $result['status']=='accept'){$categoryid=$result['message'];}
                    $dto->setLmscontextid($categoryid);
                    
                     $odata=$this->getContainer()->get('badiu.ams.offer.period.data');
                    $param=array('lmscontextid'=>$dto->getLmscontextid(),'lmscontexttype'=>$dto->getLmscontexttype(),'lmscontextidparent'=>$dto->getLmscontextidparent(),'lmscontexttypeparent'=>$dto->getLmscontexttypeparent(),'id'=>$dto->getId());
                   //echo "<hr>";
                   // print_r($param);
                   $resultup= $odata->updateNativeSql($param);
                 
                }
             
           
           return $dto;
           
           
     }
      //review
     public function discipline($dto) {
         
          $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
      
            if(empty($dto->getSserviceid())){return $dto;}
            if(empty($dto->getSserviceid()) && empty($dto->getSserviceid()->getId())){return $dto;}
           //  $dto->setLmscontextid($this->getId($dto->getLmscontextid()));
            // echo $dto->getLmscontextid();exit;
            //sserviceid,lmssynctype,lmscontexttypeparent,lmscontextidparent,lmscontexttype,lmscontextid
                if($dto->getLmssynctype()==DOMAINTABLE::$LMS_SYNC_WITHCREATED_DATA){
                    
                }else if($dto->getLmssynctype()==DOMAINTABLE::$LMS_SYNC_REPLICATION_DATA){
                  
                   // if($sysoperation->isEdit() &&  !empty($dto->getLmscontexttype()) &&  !empty($dto->getLmscontextid())){return $dto;} 
                   
                    $mdlname=$dto->getDisciplinename();
                    $mdlshotname=$dto->getShortname();
                    
                    //sync category
                    if($dto->getLmscontexttype()==DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY){
                         $search=$this->getContainer()->get('badiu.system.core.functionality.search');
                         $param=array('_serviceid'=>$dto->getSserviceid()->getId(),'key'=>'course.category.create','parentid'=>$dto->getLmscontextidparent(),'name'=>$mdlname);
                         $result=$search->searchWebService($param);
                         $categoryid=null;
                        if(isset($result['status']) && $result['status']=='accept'){$categoryid=$result['message'];}
                        $dto->setLmscontextid($categoryid);
                    
                        $odata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
                        $param=array('lmscontextid'=>$dto->getLmscontextid(),'id'=>$dto->getId());
                        $resultup= $odata->updateNativeSql($param);
                    }
                    //sync course
                    else if($dto->getLmscontexttype()==DOMAINTABLE::$LMS_CONTEXT_COURSE){
                         $search=$this->getContainer()->get('badiu.system.core.functionality.search');
                         $param=array('_serviceid'=>$dto->getSserviceid()->getId(),'key'=>'course.course.create','categoryid'=>$dto->getLmscontextidparent(),'name'=>$mdlname,'shortname'=>$mdlshotname);
                         $result=$search->searchWebService($param);
                         $courseid=null;
                        if(isset($result['status']) && $result['status']=='accept'){$courseid=$result['message'];}
                        $dto->setLmscontextid($courseid);
                    
                        $odata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
                        $param=array('lmscontextid'=>$dto->getLmscontextid(),'id'=>$dto->getId());
                        $resultup= $odata->updateNativeSql($param);
                    }
                   
                  }
             
           
           return $dto;
        
     }
     
     //reivew delete
     public function getId($text) {
         $p=explode(" - ",$text);
         if(isset($p[0])) {return $p[0];}
         return 0;
     }
     
     
     
     
    
        function getServerInfo($lmsintegration) {
                $sserverdata=$this->getContainer()->get('badiu.admin.server.service.data');
                $sserviceid = $this->getUtildata()->getVaueOfArray($lmsintegration, 'sserviceid');
                $sserverinfo=$sserverdata->getInfoOfServiceForDs($sserviceid);
                $lmsintegration['sservicetype']=$this->getUtildata()->getVaueOfArray($sserverinfo, 'dtype');
                $lmsintegration['sservicename']=$this->getUtildata()->getVaueOfArray($sserverinfo, 'name');
                $lmsintegration['sserviceurl']=$this->getUtildata()->getVaueOfArray($sserverinfo, 'url');
                return $lmsintegration;
        }
        
        
        function syncCourse($lmsintegration) {
            $sserviceid = $this->getUtildata()->getVaueOfArray($lmsintegration, 'sserviceid');
            $lmssynctype = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynctype');
            $lmssynclevel = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmssynclevel');
            $lmscoursecat = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscoursecat');
            $lmscourse = $this->getUtildata()->getVaueOfArray($lmsintegration, 'lmscourse');
        
           $mdldatautil=$this->getContainer()->get('badiu.moodle.core.lib.datautil');
           $mdldatautil->getSqlservice()->setServiceid($sserviceid);
            if($lmssynctype=='syncwithcreateddatainlms'){
              if($lmssynclevel=='coursecat' && !empty($lmscoursecat)){
                   $coursecatinfo=$mdldatautil->getCoursecat($lmscoursecat);
                   $lmsintegration['lmscoursecatid']=$lmscoursecat;
                  $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($coursecatinfo, 'name');
                  $lmsintegration['status']='active';
              } else  if($lmssynclevel=='course' && !empty($lmscourse)){
                   $courseinfo=$mdldatautil->getCourse($lmscourse);
                    $lmsintegration['lmscoursecatid']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatid');
                   $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'coursecatname');
                   $lmsintegration['lmscourseid']=$lmscourse;
                   $lmsintegration['lmscoursename']=$this->getUtildata()->getVaueOfArray($courseinfo, 'name');
                   $lmsintegration['lmscourseshortname']=$this->getUtildata()->getVaueOfArray($courseinfo, 'shortname');
                   $lmsintegration['status']='active';
              }
              
          }
          else if($lmssynctype=='replicationdatainlms'){
               $search=$this->getContainer()->get('badiu.system.core.functionality.search');
               $name=$this->getUtildata()->getVaueOfArray($courseinfo, 'name');
              if($lmssynclevel=='coursecat' && !empty($lmscoursecat)){
                   $parami=array('_serviceid'=>$sserviceid,'_key'=>'course.category.create','parentid'=>$lmscoursecat,'name'=>$name);
                    $iresult=$search->execWebService($parami);
                    $istatus=$this->getUtildata()->getVaueOfArray($iresult, 'status');
                    $imessage=$this->getUtildata()->getVaueOfArray($iresult, 'message');
                    if($istatus=='accept' && !empty($imessage)){
                        $coursecatinfo=$mdldatautil->getCoursecat($imessage);
                        $lmsintegration['lmscoursecatid']=$imessage;
                        $lmsintegration['lmscoursecatname']=$this->getUtildata()->getVaueOfArray($coursecatinfo, 'name');
                        $lmsintegration['status']='active';
                    }
                   
                  
                  
              } else  if($lmssynclevel=='course' && !empty($lmscourse)){
                  
                }
          }
          return $lmsintegration; 
        }
}
