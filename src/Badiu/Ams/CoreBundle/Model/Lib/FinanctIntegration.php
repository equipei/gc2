<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBaseJson;

class FinanctIntegration extends BadiuDataBaseJson{
    
    private $dto;
    function __construct(Container $container) {
            parent::__construct($container);
            $this->dto=null;
              }
     
     public function init() { 
          parent::init();
         if(!empty($this->getId())){
              $this->dto=$this->getData()->findById($this->getId());
          }
        
     }         
     public function offer() {
            $this->findParam();
            $this->addProduct();
         /*
           if($this->dto->getCostcenterintegration()){
               //add tree data
                 $this->dto=$this->addCostcenterOfOffer(); 
             } 
           if($this->dto->getProjectintegration()){
               $this->dto=$this->addProjectOfOffer();
             }
           if($this->dto->getContracttemplateintegration()){
              $this->dto=$this->addContractTemplateOfOffer();
             }
           return $this->dto;*/
           
           
     }

     public function discipline() {
         $this->findParam();
          $this->addProduct('discipline');
                 
     }
     
      
     public function addProduct($amslevel='offer') {
   
             $productintegration=$this->getUtildata()->getVaueOfArray($this->getParam(), 'financintegration.productintegration',true);
             
             if(!$productintegration){ return null;}
             $productid=$this->getUtildata()->getVaueOfArray($this->getParam(), 'financintegration.productid',true);
             $productname=$this->getUtildata()->getVaueOfArray($this->getParam(), 'financintegration.productname',true);
             $productshotname=$this->getUtildata()->getVaueOfArray($this->getParam(), 'financintegration.productshortname',true);
             
             $name=null;
             $enterpriseid=null;
             $enterprise=null;
             if($amslevel=='offer'){
                 $name=$this->getDto()->getCourseid()->getName()."/".$this->getDto()->getName();
				 if(!empty($this->dto->getEnterpriseid())){
					 $enterpriseid=$this->dto->getEnterpriseid()->getId();
					$enterprise=$this->dto->getEnterpriseid();
				 }
                 
             }
             else  if($amslevel=='discipline'){
                 $name=$this->getDto()->getOfferid()->getCourseid()->getName()."/".$this->getDto()->getOfferid()->getName()."/".$this->getDto()->getDisciplinename();
                 if(!empty($this->dto->getOfferid()->getEnterpriseid())){
					 $enterpriseid=$this->dto->getOfferid()->getEnterpriseid()->getId();
					$enterprise=$this->dto->getOfferid()->getEnterpriseid();
				 }
             }
             
             $pdata=$this->getContainer()->get('badiu.financ.product.product.data');
             //$sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
             $exist=false;
			$entity =$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
             if($productid){
                $exist=$pdata->exist($productid);
             } 
             else if (!empty($this->dto->getShortname())){
                     $entity =$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
					 $productid=$pdata->getGlobalColumnValue('id',array('entity'=>$entity,'shortname'=>$this->dto->getShortname()));
                    
                     if($productid){$exist=true; }  
              
             }    
              
            if($exist){
                 $param=array('name'=>$name,'id'=>$productid,'shortname'=>$this->dto->getShortname());
                 if(!empty($enterpriseid)){$param['enterpriseid']=$enterpriseid;}
                  $pdata->updateNativeSql($param);
                  
                 $param=$this->getParam();
                 $param['financintegration']['productid']=$productid;
                 $param['financintegration']['productshotname']=$this->dto->getShortname();
                 $param['financintegration']['productname']=$name;
                $param['financintegration']['status']='active';
                $this->setParam($param);
            }else{
				$fparam=array();
				$fparam['entity']=$entity;
				$fparam['name']=$name;
				$fparam['tservice']=1;
				$fparam['shortname']=$this->dto->getShortname();
				$fparam['modulekey']='badiu.ams.offer.'.$amslevel;
				$fparam['moduleinstance']=$this->dto->getId();
				$fparam['enterpriseid']=$enterpriseid;
				$fparam['deleted']=0;
				$fparam['timecreated']=new \DateTime();
                $rid=$pdata->insertNativeSql($fparam,false);
				/*$pdto=$this->getContainer()->get('badiu.financ.product.product.entity');
                $pdto=$this->initDefaultEntityData($pdto);
                $pdto->setName($name);
                $pdto->setTservice(true);
                $pdto->setShortname($this->dto->getShortname());
                $pdto->setModulekey('badiu.ams.offer.offer');
                $pdto->setModuleinstance($this->dto->getId());
				//$pdto->setModuleinstance($this->dto->getId());
                 if(!empty($enterpriseid)){$pdto->setEnterpriseid($enterprise);}
                $pdata->setDto($pdto);
                $pdata->save();*/
				
                $param=$this->getParam();
                //$param['financintegration']['productid']=$pdata->getDto()->getId();
				$param['financintegration']['productid']=$rid;
                 $param['financintegration']['productshotname']=$this->dto->getShortname();
                 $param['financintegration']['productname']=$name;
                $param['financintegration']['status']='active';
                $this->setParam($param);
                
                
             }
            $this->updateParam();
           
     }

      public function addProjectOfOffer() {
             $course=$this->dto->getCourseid()->getName();
             $offer=$this->dto->getName();
             $name=$course.'/'.$offer;
             
             $pdata=$this->getContainer()->get('badiu.admin.project.project.data');
             $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
            if($sysoperation->isEdit() && !empty($this->dto->getProjectid()) && $pdata->exist($this->dto->getProjectid())){
                 $param=array('name'=>$name,'id'=>$this->dto->getProjectid());
                $pdata->updateNativeSql($param);
            }else{
                $pdto=$this->getContainer()->get('badiu.admin.project.project.entity');
                $pdto=$this->initDefaultEntityData($pdto);
                $pdto->setName($name);
                $pdto->setShortname($this->dto->getShortname());
                $pdto->setModulekey('badiu.ams.offer.offer');
                $pdto->setModuleinstance($this->dto->getId());
                $pdata->setDto($pdto);
                $pdata->save();
                $this->dto->setProjectid($pdata->getDto()->getId());
               
                $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
                $param=array('projectid'=>$pdata->getDto()->getId(),'id'=>$this->dto->getId());
                $odata->updateNativeSql($param);
            }
           
           return $this->dto;
     }

         public function addCostcenterOfOffer() {
             $course=$this->dto->getCurriculumid()->getCourseid()->getName();
             $offer=$this->dto->getName();
             $name=$course.'/'.$offer;
             
             $pdata=$this->getContainer()->get('badiu.financ.maccount.costcenter.data');
             $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
            if($sysoperation->isEdit() && !empty($this->dto->getCostcenterid()) && $pdata->exist($this->dto->getCostcenterid())){
                 $param=array('name'=>$name,'id'=>$this->dto->getCostcenterid());
                 $pdata->updateNativeSql($param);
            }else{
                $pdto=$this->getContainer()->get('badiu.financ.maccount.costcenter.entity');
                $pdto=$this->initDefaultEntityData($pdto);
                $pdto->setName($name);
                $pdto->setShortname($this->dto->getShortname());
                $pdto->setModulekey('badiu.ams.offer.offer');
                $pdto->setModuleinstance($this->dto->getId());
                 
                //tree
                $factorykeytree = $this->getContainer()->get('badiu.system.core.lib.keytree.factorykeytree');
                 $factorykeytree->init('badiu.financ.maccount.costcenter.data');

                $idpath = null;
                $dtype = null;
                $entity = $this->dto->getEntity();

                $newidtree = $factorykeytree->getNext($entity, $idpath, $dtype);
                $level = $factorykeytree->getLevel($newidtree);
                $order = $factorykeytree->getOrder($newidtree);
                $pdto->setIdpath($newidtree);
                $pdto->setLevel($level);
                $pdto->setOrderidpath($order);
                
                $pdata->setDto($pdto);
                $pdata->save();
                $this->dto->setCostcenterid($pdata->getDto()->getId());
               
                $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
                
                 
            
                
                $param=array('costcenterid'=>$pdata->getDto()->getId(),'id'=>$this->dto->getId());
                $odata->updateNativeSql($param);
            }
           
           return $this->dto;
     }

      public function addContractTemplateOfOffer() {
             $course=$this->dto->getCurriculumid()->getCourseid()->getName();
             $offer=$this->dto->getName();
             $name=$course.'/'.$offer;
             
             $pdata=$this->getContainer()->get('badiu.admin.contract.template.data');
             $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
            if($sysoperation->isEdit() && !empty($this->dto->getContracttemplateid()) && $pdata->exist($this->dto->getContracttemplateid())){
                 
                 $param=$this->addContratTemplateConfig($this->dto,null,true);
                 
                if(!empty($this->dto->getEnterpriseid())){$param['contractedenterprise']=$this->dto->getEnterpriseid()->getId();}
                $param['contractedtype']='enterprise';
                $param['name']=$name;
                $param['id']=$this->dto->getContracttemplateid();
           
                 $pdata->updateNativeSql($param);
            }else{
                $pdto=$this->getContainer()->get('badiu.admin.contract.template.entity');
                $pdto=$this->initDefaultEntityData($pdto);
                $pdto->setName($name);
                $pdto->setShortname($this->dto->getShortname());
                $pdto->setModulekey('badiu.ams.offer.offer');
                $pdto->setModuleinstance($this->dto->getId());
                $pdto=$this->addContratTemplateConfig($this->dto,$pdto,false);
                $pdto->setContractedtype('enterprise');
                 if(!empty($this->dto->getEnterpriseid())){$pdto->setContractedenterprise($this->dto->getEnterpriseid());}
                $pdata->setDto($pdto);
                $pdata->save();
                $this->dto->setContracttemplateid($pdata->getDto()->getId());
               
                $odata=$this->getContainer()->get('badiu.ams.offer.offer.data');
                $param=array('contracttemplateid'=>$pdata->getDto()->getId(),'id'=>$this->dto->getId());
                $odata->updateNativeSql($param);
            }
           
           return $this->dto;
     }
     
      public function addContratTemplateConfig($pdto,$isedit=false) {
          $dconfig=$this->dto->getContracttemplateconfig();
           if(empty($dconfig)){return $pdto;}
            $json=$this->getContainer()->get('badiu.system.core.lib.util.json');
            $dconfig=$json->decode($dconfig, true);
            $finalvalue=0;
            if(isset($dconfig['totalvalue']) && !empty($dconfig['totalvalue'])){ $finalvalue=$dconfig['totalvalue'];}
            if(isset($dconfig['discountvalue']) && !empty($dconfig['totalvalue'])){ $finalvalue=$finalvalue-$dconfig['discountvalue'];}
            $dconfig['finalvalue']=$finalvalue;
            if($isedit){ 
                return $dconfig;
            }
            
           
            if(!sset($dconfig['totalvalue'])){$pdto->setTotalvalue($dconfig['totalvalue']);}
            if(!sset($dconfig['discountvalue'])){$pdto->setDiscountvalue($dconfig['discountvalue']);}
            $pdto->setFinalvalue($finalvalue);
            
            if(!sset($dconfig['billingrecurrent'])){$pdto->setBillingrecurrent($dconfig['billingrecurrent']);}
            if(!sset($dconfig['billingportion'])){$pdto->setBillingportion($dconfig['billingportion']);}
            if(!sset($dconfig['paygateway'])){$pdto->setPaygateway($dconfig['paygateway']);}
            if(!sset($dconfig['paymethod'])){$pdto->setPpaymethod($dconfig['paymethod']);}
            if(!sset($dconfig['billingday'])){$pdto->setBillingday($dconfig['billingday']);}
           return $pdto;
      }
    
     
      
      function getDto() {
          return $this->dto;
      }

      function setDto($dto) {
          $this->dto = $dto;
      }


}
