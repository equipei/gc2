<?php

namespace Badiu\Ams\CoreBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuWebService;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class LmsMoodleDataUtil    extends BadiuModelLib{
    /**
     * @var object
     */
   private  $webservice;
   
   private $container;
    function __construct(Container $container) {
                parent::__construct($container);
            $this->webservice=$this->getContainer()->get('badiu.system.core.lib.webservice');
       }
  
        function getCoursecat($coursecatid,$isshotname=false) {
            $wsql=" WHERE id= $coursecatid ";
            if($isshotname){ $wsql=" WHERE idnumber = '".$coursecatid."'";}
            $sql="SELECT id,name,idnumber  FROM {_pfx}course_categories $wsql";
            $result=$this->webservice->searchSingle($sql);
            return $result;
        }
        function getCourse($courseid,$isshotname=false) {
            $wsql=" WHERE  c.id= $courseid ";
            if($isshotname){ $wsql=" WHERE c.shortname = '".$courseid."'";}
            
            $sql="SELECT c.id,c.fullname AS name,c.shortname,ct.name AS coursecatname,ct.id AS coursecatid  FROM {_pfx}course c INNER JOIN {_pfx}course_categories ct ON c.category=ct.id $wsql";
            $result=$this->webservice->searchSingle($sql);
            return $result;
        }
        function getCoursegroup($groupid,$isshotname=false) {
            $wsql=" WHERE  g.id= $groupid ";
            if($isshotname){ $wsql=" WHERE g.idnumber = '".$groupid."'";}
            
            $sql="SELECT g.id,g.name,c.id AS courseid,c.fullname AS coursename,c.shortname AS courseshortname,ct.name AS coursecatname,ct.id AS coursecatid  FROM {_pfx}groups g INNER JOIN {_pfx}course c ON g.courseid=c.id INNER JOIN {_pfx}course_categories ct ON c.category=ct.id $wsql";
            $result=$this->webservice->searchSingle($sql);
            return $result;
        }
        function getSessionDefualtlmsUserid() {
           
            $badiusession=$this->getContainer()->get('badiu.system.access.session');
           
            //get is session if exist
            if($badiusession->existValue('badiu.admin.servervice.defaultlmsuserid')){
                $defaultlmsuserid= $badiusession->getValue('badiu.admin.servervice.defaultlmsuserid');
                return $defaultlmsuserid;
            } 
            
            //set is session if not exist
           $entity=$badiusession->get()->getEntity();
            $sserviceid=$this->getSessionDefualtlms();
            $username=$badiusession->get()->getUser()->getUsername();
            $this->webservice->setServiceid($sserviceid);
            $sql="SELECT id FROM {_pfx}user WHERE username= '".$username."'";
            $result=$this->webservice->searchSingle($sql);
       
            $mdluserid=$this->getContainer()->get('badiu.system.core.lib.util.data')->getVaueOfArray($result,'id');
            if(empty($mdluserid)){$mdluserid=0;}
            $badiusession->addValue('badiu.admin.servervice.defaultlmsuserid',$mdluserid);
          
            return $mdluserid;
        }
        function getSessionDefualtlms() {
           
            $badiusession=$this->getContainer()->get('badiu.system.access.session');
           
            //get is session if exist
            if($badiusession->existValue('badiu.admin.servervice.defaultlms')){
                $defaultlms= $badiusession->getValue('badiu.admin.servervice.defaultlms');
                return $defaultlms;
            } 
           // echo "defaultlms $defaultlms";exit;
            //set is session if not exist
           $entity=$badiusession->get()->getEntity();
           $sserviceid=$this->getContainer()->get('badiu.admin.server.service.data')->getIdByShortname($entity,'defaultlms');
            
            if(empty($sserviceid)){$sserviceid=0;}
            $badiusession->addValue('badiu.admin.servervice.defaultlms',$sserviceid);
          
            return $sserviceid;
        }
        
        function remoteAuth() {
            $urltarget=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urltarget');
            $urltarget=urldecode($urltarget);
            $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
            if(empty($serviceid)){echo 'param _serviceid is empty';exit;}
            
            $badiusession=$this->getContainer()->get('badiu.system.access.session');
            $entity=$badiusession->get()->getEntity();
            $userid=$badiusession->get()->getUser()->getId();
            $username=$badiusession->get()->getUser()->getUsername();
            $userfullname=$badiusession->get()->getUser()->getFullname();
            $moodleurl=$this->getSessionUrl($serviceid);
            $now=time();
            $basekey = "|$userid|$entity|$now";
            $lenth = strlen($basekey);
            $hash = $this->getContainer()->get('badiu.system.core.lib.util.hash');
            $lenth = 120 - $lenth;
            $authtokenkey = $hash->make($lenth);
            $authtoken = $authtokenkey . $basekey;
            $param = array('bkey' => 'badiu.moodle.user', 'modulekey' => 'badiu.system.user.authmoodleremote', 'moduleinstance' => $userid, 'name' => $userfullname, 'shortname' => $authtoken, 'customint1' => 1, 'customchar1' => $username,'customchar2' => $moodleurl,'customchar3' => $urltarget, 'entity' => $entity, 'timecreated' => new \DateTime(), 'deleted' => 0);
            $data = $this->getContainer()->get('badiu.system.module.data.data');
            $result = $data->insertNativeSql($param, false);
             $tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
          
            $authtoken=$tcript->encode('k2',$authtoken,$serviceid);
            $authtoken=base64_encode($authtoken);
          
            $url="$moodleurl/local/badiunet/rauth.php?_tokenauth=$authtoken";
            
            header("Location: $url"); 
            exit;
          }
       function remoteAccessAuth($param) {
           
           $tokenauth=$this->getUtildata()->getVaueOfArray($param,'_tokenauth');
           $tcript= $this->getContainer()->get('badiu.system.core.lib.util.templetecript');
           $tokenauth=$tcript->decode($tokenauth);
          
           $moduledata=$this->getContainer()->get('badiu.system.module.data.data');
           $param=array('modulekey'=>'badiu.system.user.recoverpwd','customint1'=>1);
           $id=$moduledata->getIdByShortname($tokenauth,array('customint1'=>1));
           $response = $this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
           if(empty($id)){return $response->denied('badiu.system.user.authmoodleremote.failed.tokenauthnotvalid', 'Token of remote auth not valid');}
          // $resp=$moduledata->findById($id); 
           
           $resultdesable=$moduledata->updateNativeSql(array('id'=>$id,'customint1'=>0),false);
           $resp= $moduledata->getGlobalColumnsValue('o.customchar1,o.customchar3',array('id'=>$id));  
           $result=array();
           $result['username']=$this->getUtildata()->getVaueOfArray($resp,'customchar1');
           $result['urltarget']=$this->getUtildata()->getVaueOfArray($resp,'customchar3');
           return $result;
       }   
          
       function getSessionUrl($serviceid=null) {   
          if(empty($serviceid)){
              $serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
          }
          
          if(empty($serviceid)){return null;}
          $badiuSession=$this->getContainer()->get('badiu.system.access.session');
          $sessionkey="badiu.moodle.mreport.site_url_$serviceid";
          if($badiuSession->existValue($sessionkey)){
            $url=$badiuSession->getValue($sessionkey);   
             return $url;
           }  
           
         $sservicedata=$this->getContainer()->get('badiu.admin.server.service.data');
         $url=$sservicedata->getGlobalColumnValue('url',array('id'=>$serviceid));
         $badiuSession->addValue($sessionkey,$url);
       
         return $url;
     }
     
     function getUserInfoFields() {
            $sql ="SELECT id,shortname,name,categoryid FROM {_pfx}user_info_field  ORDER BY categoryid,sortorder";
            $result=$this->webservice->search($sql,0,500);
            return $result;
        }
        function getWebservice() {
            return $this->webservice;
        }

        function setWebservice($webservice) {
            $this->webservice = $webservice;
        }



}
