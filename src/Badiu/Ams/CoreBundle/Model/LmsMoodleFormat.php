<?php

namespace Badiu\Ams\CoreBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\CoreBundle\Model\DOMAINTABLE;
class LmsMoodleFormat extends BadiuFormat {
	  private $moodleremoteaccess;
	   private $managesyncmoodle;
	function __construct(Container $container) {
		 parent::__construct($container);
		  $this->moodleremoteaccess=$this->getContainer()->get('badiu.moodle.core.lib.remoteaccess');
		  $this->managesyncmoodle=$this->getContainer()->get('badiu.ams.enrol.lib.managesyncmoodle');
	}

        public function name($data) {
            $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $sserviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
            $sserverinfo=$this->getServerServiceInfo($sserviceid);
			if(empty($sserverinfo)){return null;} 
            $name=$this->getUtildata()->getVaueOfArray($sserverinfo,'name');
            $url=$this->getUtildata()->getVaueOfArray($sserverinfo,'url');
            $link="<a href=\"$url\"  target=\"_blank\">$name</a>";
           return $link;
        }
       public function system($data) {
            $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $sserviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
            $sserverinfo=$this->getServerServiceInfo($sserviceid);
			if(empty($sserverinfo)){return null;}
            $name=$this->getUtildata()->getVaueOfArray($sserverinfo,'name');
            $url=$this->getUtildata()->getVaueOfArray($sserverinfo,'url');
            
            $value="";
            if(!empty($name)){$value.=$name;}
            if(!empty($url)){$value.=" - $url" ;}
           return $value;
        }
         public function infoabstract($data) {
             $value=""; 
            $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $sserviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
            $sserverinfo=$this->getServerServiceInfo($sserviceid);
			if(empty($sserverinfo)){return null;}
            $mdlname=$this->getUtildata()->getVaueOfArray($sserverinfo,'name');
            $mdlurl=$this->getUtildata()->getVaueOfArray($sserverinfo,'url');
            
            $lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynclevel',true);
            $synclabeltitle=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynclevel');
            $synclabel=$this->getSynclevelLabel($lmssynclevel);
            
            $value.="$mdlname - $mdlurl <br />";
            $value.="$synclabel: ";
            
			$url=$this->url($data);
            if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY){
               $id=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatid',true);
               $name=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatname',true);
               
               $mdlurlparam="$mdlurl/course/index.php?categoryid=$id";
              // $mdllink="<a href=\"$mdlurlparam\"  target=\"_blank\">$mdlurlparam</a>";
			   $mdllink="<a href=\"$url\"  target=\"_blank\">$mdlurlparam</a>";
             // $bmdllink="<a href=\"$mdlurl\"  target=\"_blank\">$mdlname</a>";
			   $bmdllink="<a href=\"$url\"  target=\"_blank\">$mdlname</a>";
               
               $value="Moodle: $bmdllink<br />";
               $value.="Categoria de curso: $name <br />";
               $value.="$mdllink  <br />";
            }
            else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_COURSE){
                $id=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);
                $name=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursename',true);
               
                $mdlurlparam="$mdlurl/course/view.php?id=$id";
               // $mdllink="<a href=\"$mdlurlparam\"  target=\"_blank\">$mdlurlparam</a>";
			    $mdllink="<a href=\"$url\"  target=\"_blank\">$mdlurlparam</a>";
               
               // $bmdllink="<a href=\"$mdlurl\"  target=\"_blank\">$mdlname</a>";
				 $bmdllink="<a href=\"$url\"  target=\"_blank\">$mdlname</a>";
                
                $value="Moodle: $bmdllink<br />";
                $value.="Curso: $name <br />";
                 $value.="$mdllink  <br />";
                }
            else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_GROUP){
                $coursename=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursename',true);
                $courseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);
                $id=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursegroupid',true);
                $name=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursegroupname',true);
             
                $mdlurlparam="$mdlurl/course/view.php?id=$courseid&group=$id";
              //  $mdllink="<a href=\"$mdlurlparam\"  target=\"_blank\">$mdlurlparam</a>";
			    $mdllink="<a href=\"$url\"  target=\"_blank\">$mdlurlparam</a>";
               // $bmdllink="<a href=\"$mdlurl\"  target=\"_blank\">$mdlname</a>";
			    $bmdllink="<a href=\"$url\"  target=\"_blank\">$mdlname</a>";
                
                $value="Moodle: $bmdllink<br />";
                $value.="Curso/Grupo: $coursename /  $name <br />";
                 $value.="$mdllink  <br />";
               
              }
            else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_ACTIVITY){
                }
           
                
            
           return $value;
        }
        
        public function synctype($data) {
            $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $lmssynctype=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynctypeoriginal',true);
            if(!empty($lmssynctype)){$lmssynctype=$lmssynctype=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynctype',true);}
            
            $value=""; 
            if($lmssynctype==DOMAINTABLE::$LMS_SYNC_WITHCREATED_DATA){$value=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynctype.syncwithcreateddatainlms');}
            else if($lmssynctype==DOMAINTABLE::$LMS_SYNC_REPLICATION_DATA){$value=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynctype.replicationdatainlms');}
            return $value;
            
   }
   public function synctlevel($data) {
            $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynclevel',true);
            $value=""; 
            if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY){$value=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccoursecat');}
            else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_COURSE){$value=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccourse');}
            else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_GROUP){$value=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccoursegroup');}
            else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_ACTIVITY){$value=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccourseactivity');}
             return $value;
            
   }
   public function sync($data) {
            $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynclevel',true);
            
             $sserviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
            $sserverinfo=$this->getServerServiceInfo($sserviceid);
			if(empty($sserverinfo)){return null;}
            $mdlname=$this->getUtildata()->getVaueOfArray($sserverinfo,'name');
            $mdlurl=$this->getUtildata()->getVaueOfArray($sserverinfo,'url');
            
            
            $value=""; 
            if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY){
                $labelcoursecat=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmscontextcoursecat');
                $coursecatid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatid',true);
                $coursecatname=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatname',true);
                
                $mdlurlparam="$mdlurl/course/index.php?categoryid=$coursecatid";
                $mdllink="<a href=\"$mdlurlparam\"  target=\"_blank\">$mdlurlparam</a>";
                $value="$coursecatname <br />";
                $value.="$mdllink  <br />";
                
            }
            else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_COURSE){
                $coursename=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursename',true);
                $courseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);
               
                $mdlurlparam="$mdlurl/course/view.php?id=$courseid";
                $mdllink="<a href=\"$mdlurlparam\"  target=\"_blank\">$mdlurlparam</a>";
                $value="$coursename <br />";
                 $value.="$mdllink  <br />";
                }
            else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_GROUP){
                $coursename=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursename',true);
                $courseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);
                $id=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursegroupid',true);
                $name=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursegroupname',true);
                
               
                $mdlurlparam="$mdlurl/course/view.php?id=$courseid&group=$id";
                $mdllink="<a href=\"$mdlurlparam\"  target=\"_blank\">$mdlurlparam</a>";
                $value="$coursename /  $name <br />";
                 $value.="$mdllink  <br />";
                }
            else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_ACTIVITY){
                }
           
        return $value;
            
   }
   
        private function getServerServiceInfo($serviceid) {   
         
         if(empty($serviceid)){return null;}
          $badiuSession=$this->getContainer()->get('badiu.system.access.session');
          $sessionkey="badiu.ams.core.synclms.serverserivicedata_$serviceid";
          if($badiuSession->existValue($sessionkey)){
            $data=$badiuSession->getValue($sessionkey);   
             return $data;
           }  
          
           
         $sservicedata=$this->getContainer()->get('badiu.admin.server.service.data');
		 if(!$sservicedata->exist($serviceid)){return null;}
         $data=$sservicedata->getInfoById($serviceid);
         $badiuSession->addValue($sessionkey,$data);
         return $data;
     }
     
     private function getSynclevelLabel($lmssynclevel) {  
         $label=$lmssynclevel;
        if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY){$label=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccoursecat');}
        else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_COURSE){$label=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccourse');}
        else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_GROUP){$label=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccoursegroup');}
        else if($lmssynclevel==DOMAINTABLE::$LMS_CONTEXT_ACTIVITY){$label=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccourseactivity');}
        return $label;
     }
	 
	    public  function access($data){ 
            $statusenableaccesslms =$this->getUtildata()->getVaueOfArray($data,'statusenableaccesslms');
            if(!$statusenableaccesslms){return null;}
			$url=$this->url($data);
			
			if(empty($url)){return null;}
			
			$classename=$this->getUtildata()->getVaueOfArray($data,'classename');
			$classedescription=$this->getUtildata()->getVaueOfArray($data,'classedescription');
			if(!empty($classedescription)){$classename.=" - ".$classedescription;}
			if(empty($classename)){$classename="Acessar";}
			$link= "<a href=\"$url\">$classename</a> <br />";
			
			return  $link;
		}
	
  public  function url($data){ 
			
			
           $dconfig =$this->getUtildata()->getVaueOfArray($data,'dconfig');
			$classename=$this->getUtildata()->getVaueOfArray($data,'classename');
			$classedescription=$this->getUtildata()->getVaueOfArray($data,'classedescription');
			if(!empty($classedescription)){$classename.=" - ".$classedescription;}
			$dconfig = $this->getJson()->decode($dconfig, true); 
		    $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
			$serviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
			$lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynclevel',true);
		    $lmscoursecatid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatid',true);
			$lmscourseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);	
			$groupid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursegroupid',true);								
			$status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		   if(empty($serviceid)){return null;}
		   
		   //check can access
		   $canaccess=$this->managesyncmoodle->canAccess($data);
		   if(!$canaccess){return null;}
			$urltarget=null;
			$url=null;
		
			if( ($lmssynclevel=='course' || $lmssynclevel=='group') && $lmscourseid > 0 ){
					$urltarget="/course/view.php?id=$lmscourseid";
					$externalclient=false;
					$badiuSession=$this->getContainer()->get('badiu.system.access.session');
					$badiuSession->setHashkey($this->getSessionhashkey());
					$clienttype=$badiuSession->get()->getType();
					if($clienttype=='webservice' || $clienttype=='webservicesynceduser'){$externalclient=true;}
					if($externalclient){
						$this->moodleremoteaccess->setSessionhashkey($this->getSessionhashkey());
						$moodleurl=$this->moodleremoteaccess->getSessionUrl($serviceid);
						$url=$moodleurl.$urltarget;
						
						return $url;
					}
						
				
					$url=$this->getRouter()->generate('badiu.system.core.service.process',array('_service'=>'badiu.moodle.core.lib.remoteaccess','_function'=>'remoteAuth','_serviceid'=>$serviceid,'_urltarget'=>$urltarget));
				
			}else if( $lmssynclevel=='coursecat' && $lmscoursecatid > 0 ){
					$urltarget="/course/index.php?categoryid=$lmscoursecatid";
					$externalclient=false;
					$badiuSession=$this->getContainer()->get('badiu.system.access.session');
					$badiuSession->setHashkey($this->getSessionhashkey());
					$clienttype=$badiuSession->get()->getType();
					if($clienttype=='webservice' || $clienttype=='webservicesynceduser'){$externalclient=true;}
					if($externalclient){
						$this->moodleremoteaccess->setSessionhashkey($this->getSessionhashkey());
						$moodleurl=$this->moodleremoteaccess->getSessionUrl($serviceid);
						$url=$moodleurl.$urltarget;
						
						return $url;
					}
						
				
					$url=$this->getRouter()->generate('badiu.system.core.service.process',array('_service'=>'badiu.moodle.core.lib.remoteaccess','_function'=>'remoteAuth','_serviceid'=>$serviceid,'_urltarget'=>$urltarget));
				
			}
			
			return  $url;
		}


		public  function admindaccess($data){ 
            $serviceid =$this->getUtildata()->getVaueOfArray($data,'id');
            $moodleurl=$this->moodleremoteaccess->getSessionUrl($serviceid);
			$url=$moodleurl."/course";
			$url=$this->getRouter()->generate('badiu.system.core.service.process',array('_service'=>'badiu.moodle.core.lib.remoteaccess','_function'=>'remoteAuth','_serviceid'=>$serviceid,'_urltarget'=>'/course/'));
			$link= "<a href=\"$url\" target=\"_blank\">Acessar</a>";
			return  $link;
		}	

		public  function daccess($data){ 
           	$url=$this->url($data);
			
			if(empty($url)){return null;}
			$link= "<a href=\"$url\">Acessar</a> <br />";
			
			return  $link;
		}		
}
