<?php

namespace Badiu\Ams\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Badiu\Ams\CoreBundle\Model\DOMAINTABLE;
class CoreFormDataOptions extends BadiuFormDataOptions{
       private  $webservice;
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
             $this->webservice=$this->getContainer()->get('badiu.system.core.lib.webservice');
       }

   public  function offerclasseorderby(){
        $list=array();
        $list['bytimestartdesc']=$this->getTranslator()->trans('badiu.ams.core.classe.sortorder.bytimestartdesc');
        $list['bytimestartasc']=$this->getTranslator()->trans('badiu.ams.core.sortorder.bytimestartasc');
        $list['byname']=$this->getTranslator()->trans('badiu.ams.core.sortorder.bydiscipinename');
          return $list;
    }

	public  function offerenrolorderby(){	
	
	}
}
