<?php

namespace Badiu\Ams\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class AmsDataBase  extends BadiuDataBase {
   
   private $dtype ='course';
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
     
   public function getFormChoice($entity,$param=array(),$orderby="") {
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id,o.name FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.dtype = :dtype $wsql $orderby";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('dtype','course');
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return  $result;
    }
 
 public function findByEntity($entity,$orderby="",$deleted=false) {
             $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity  AND o.dtype = :dtype AND o.deleted=:deleted $orderby";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
             $query->setParameter('dtype','course');
             $query->setParameter('deleted',$deleted);
            $result= $query->getResult();
            return  $result;
        }
		

 public function existAdd() {
  
          $r=FALSE;
           $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity   AND o.dtype = :dtype AND UPPER(o.name) = :name ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('name',strtoupper($this->getDto()->getName()) );
			 $query->setParameter('dtype','course');
            $result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
            
            
       }
       public function existEdit() {
           $r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.id != :id AND o.entity = :entity   AND o.dtype = :dtype AND UPPER(o.name) = :name ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$this->getDto()->getId());
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('name',strtoupper($this->getDto()->getName()) );
 	    $query->setParameter('dtype','course');
            $result= $query->getSingleResult();
           if($result['countrecord']>0){$r=TRUE;}
            return $r;
          
       }
       
        public function existAddShortname() {
           $shortname=$this->getDto()->getShortname();
            if(empty($shortname)) return false;
          $r=FALSE;
           $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity   AND o.dtype = :dtype AND UPPER(o.shortname) = :shortname ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('shortname',strtoupper($this->getDto()->getShortname()) );
            $query->setParameter('dtype','course');
            $result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
            
            
       }
       public function existEditShortname() {
           $shortname=$this->getDto()->getShortname();
            if(empty($shortname)) return false;
           $r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.id != :id AND o.entity = :entity   AND o.dtype = :dtype AND UPPER(o.shortname) = :shortname ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$this->getDto()->getId());
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('shortname',strtoupper($this->getDto()->getShortname()) );
            $query->setParameter('dtype','course');
            $result= $query->getSingleResult();
            if($result['countrecord']>0){$r=TRUE;}
             return $r;
          
       }
       
       public function existAddIdnumber() {
           $idnumber=$this->getDto()->getIdnumber();
            if(empty($idnumber)) return false;
          $r=FALSE;
           $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity  AND o.dtype = :dtype AND UPPER(o.idnumber) = :idnumber ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('idnumber',strtoupper($this->getDto()->getIdnumber()) );
             $query->setParameter('dtype','course');
            $result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
            
            
       }
       public function existEditIdnumber() {
           $idnumber=$this->getDto()->getIdnumber();
            if(empty($idnumber)) return false;
           $r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.id != :id  AND o.dtype = :dtype AND o.entity = :entity AND UPPER(o.idnumber) = :idnumber ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$this->getDto()->getId());
            $query->setParameter('entity',$this->getDto()->getEntity());
            $query->setParameter('idnumber',strtoupper($this->getDto()->getIdnumber()) );
             $query->setParameter('dtype','course');
            $result= $query->getSingleResult();
            if($result['countrecord']>0){$r=TRUE;}
             return $r;
          
       }
	   
	   
	   function getDtype() {
        return $this->dtype;
    }

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }
}
