<?php
namespace Badiu\Ams\CoreBundle\Model;
class DOMAINTABLE {
 	public static  $ERROR_COURSEID_EMPTY ="badiu.ams.service.courseid.is.empty";
	public static  $ERROR_COURSEID_NOT_VALID ="badiu.ams.service.courseid.is.not.valid";
        
        public static  $LMS_SYNC_REPLICATION_DATA ="replicationdatainlms";
        public static  $LMS_SYNC_WITHCREATED_DATA ="syncwithcreateddatainlms";
		public static  $LMS_SYNC_REPLICATION_DATA_BYTEMPLATE ="replicationdatabycoursetemplateinlms";
        
        public static  $LMS_CONTEXT_COURSECATEGORY ="coursecat";
        public static  $LMS_CONTEXT_COURSE ="course";
        public static  $LMS_CONTEXT_GROUP ="group";
        public static  $LMS_CONTEXT_ACTIVITY ="activity";
	
}
?>