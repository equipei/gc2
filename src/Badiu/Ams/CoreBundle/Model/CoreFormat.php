<?php

namespace Badiu\Ams\CoreBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CoreFormat extends BadiuFormat {

	private  $configformat;
	  private $moodleremoteaccess;
	function __construct(Container $container) {
		 parent::__construct($container);
		 $this->configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
		 $this->moodleremoteaccess=$this->getContainer()->get('badiu.moodle.core.lib.remoteaccess');
	}

		public function roleconclusionclasse($data) {
			$config=$this->getUtildata()->getVaueOfArray($data,'roleconclusion');
			$type=$this->getUtildata()->getVaueOfArray($config,'conclusion.level',true);
			$value=$this->roleconclusion($config,$type,'classe');
			
			return $value;
		}
	 
        public function roleconclusion($config,$type,$levelconfig) {
			if(!is_array($config)){$config=$this->getJson()->decode($config, true);}
			$value="";
			$levelname="";
			if($levelconfig=='offer'){$levelname=$this->getTranslator()->trans('badiu.ams.core.offer');}
			else if($levelconfig=='discipline'){$levelname=$this->getTranslator()->trans('badiu.ams.core.discipline');}
			else if($levelconfig=='classe'){$levelname=$this->getTranslator()->trans('badiu.ams.core.classe');}
			
			$totalhour=$this->getUtildata()->getVaueOfArray($config,'result.dhour.planned',true);
			$minhour=$this->getUtildata()->getVaueOfArray($config,'result.dhour.shouldexec',true);
			$percenthour=$this->getUtildata()->getVaueOfArray($config,'result.dhour.shouldexecpercent',true);
			//$studenthour=$this->getUtildata()->getVaueOfArray($config,'result.dhour.exec',true);
			
			
			 $totalhour=$this->configformat->defaultHour($totalhour);
			 $minhour=$this->configformat->defaultHour($minhour);
			
			$totalgrade=$this->getUtildata()->getVaueOfArray($config,'result.grade.planned',true);
			$mingrade=$this->getUtildata()->getVaueOfArray($config,'result.grade.shouldexec',true);
			$percentgrade=$this->getUtildata()->getVaueOfArray($config,'result.grade.shouldexecpercent',true);
			//$studentgrade=$this->getUtildata()->getVaueOfArray($config,'result.grade.exec',true);
			
			if($type=='classe' || $type=='discipline'){
				//$dhour=$this->getUtildata()->getVaueOfArray($config,'conclusion.discipline.defaultroles.dhour.minperc',true);
				//$grade=$this->getUtildata()->getVaueOfArray($config,'conclusion.discipline.defaultroles.grade.minperc',true);
				$lmscoursecompletation=$this->getUtildata()->getVaueOfArray($config,'conclusion.defaultroles.lmscoursecompletation',true);
				
				$value.=$this->getTranslator()->trans('badiu.ams.core.roleconclusion.label.levelconfig',array('%levelconfig%'=>$levelname));
				$value.=$this->getCommandBreakLine();
				
				if(!empty($percenthour)){
					$value.=$this->getTranslator()->trans('badiu.ams.core.roleconclusion.label.dhour',array('%percent%'=>$percenthour,'%total%'=>$totalhour,'%min%'=>$minhour));
					$value.=$this->getCommandBreakLine();
				}
				if(!empty($percentgrade)){
					$value.=$this->getTranslator()->trans('badiu.ams.core.roleconclusion.label.grade',array('%percent%'=>$percentgrade,'%total%'=>$totalgrade,'%min%'=>$mingrade));
					$value.=$this->getCommandBreakLine();
				}
				if(!empty($lmscoursecompletation)){
					$value.=$this->getTranslator()->trans('badiu.ams.core.roleconclusion.label.lmscoursecompletation');
					$value.=$this->getCommandBreakLine();
				}
				/*if(!empty($dhour)){
					$value.=$this->getTranslator()->trans('badiu.ams.core.roleconclusion.label.dhour',array('%percent%'=>$dhour,'%total%'=>$totalhour));
					$value.=$this->getCommandBreakLine();
				}
				if(!empty($grade)){
					$value.=$this->getTranslator()->trans('badiu.ams.core.roleconclusion.label.grade',array('%percent%'=>$grade,'%total%'=>$totalgrade));
					$value.=$this->getCommandBreakLine();
				}
				if(!empty($lmscoursecompletation)){
					$value.=$this->getTranslator()->trans('badiu.ams.core.roleconclusion.label.lmscoursecompletation');
					$value.=$this->getCommandBreakLine();
				}*/
			}else if($type=='offer'){
				$dhour=$this->getUtildata()->getVaueOfArray($config,'conclusion.defaultroles.dhour.minperc',true);
				$credit=$this->getUtildata()->getVaueOfArray($config,'conclusion.defaultroles.credit.minperc',true);
				$requireddiscipline=$this->getUtildata()->getVaueOfArray($config,'conclusion.defaultroles.requireddiscipline.minperc',true);
				$nonrequireddiscipline=$this->getUtildata()->getVaueOfArray($config,'conclusion.discipline.defaultroles.lmscoursecompletation',true);
				
				if(!empty($dhour)){
					$totalhour=600;
					$value.=$this->getTranslator()->trans('badiu.ams.core.roleconclusion.label.dhour',array('%percent%'=>$dhour,'%total%'=>$totalhour));
					$value.=$this->getCommandBreakLine();
				}
				
				if(!empty($requireddiscipline)){
					$totalrq=40;
					$value.=$this->getTranslator()->trans('badiu.ams.core.roleconclusion.label.requireddiscipline',array('%percent%'=>$requireddiscipline,'%total%'=>$totalrq));
					$value.=$this->getCommandBreakLine();
				}
				if(!empty($nonrequireddiscipline)){
					$totalnrq=7;
					$value.=$this->getTranslator()->trans('badiu.ams.core.roleconclusion.label.nonrequireddiscipline',array('%percent%'=>$nonrequireddiscipline,'%total%'=>$totalnrq));
					$value.=$this->getCommandBreakLine();
				}
			}
			
			
			if($this->getOutputtype()=='html' && !empty($value)){
				
				$value="<div style=\"font-size:11px\">$value</div>";
			}
			
            
           return $value;
        }
		
		
		
		 public  function offernamewithcourse($data){
      
               $value=null;
               $offername= $this->getUtildata()->getVaueOfArray($data,'offername');
               $coursename= $this->getUtildata()->getVaueOfArray($data,'coursename');
               $value=$offername;
               if(!empty($coursename)){$value="$coursename / $offername";}
               return $value;  
      } 
	 
	 			
     
}
