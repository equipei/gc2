<?php
 
namespace Badiu\Ams\CoreBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Badiu\Ams\CoreBundle\Model\DOMAINTABLE;
class LmsMoodleFormDataOptions extends BadiuFormDataOptions{
       private  $webservice;
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
             $this->webservice=$this->getContainer()->get('badiu.system.core.lib.webservice');
       }

    public  function getSites(){
        $list=array();
      
        return $list;
    }

	public  function getCourses(){
        $list=array();
        
		
        return $list;
    }

    public  function getSynctype(){
        $list=array();
        $list[DOMAINTABLE::$LMS_SYNC_WITHCREATED_DATA]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynctype.syncwithcreateddatainlms');
        $list[DOMAINTABLE::$LMS_SYNC_REPLICATION_DATA]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynctype.replicationdatainlms');
		$list[DOMAINTABLE::$LMS_SYNC_REPLICATION_DATA_BYTEMPLATE]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynctype.replicationdatabycoursetemplateinlms');
		
        return $list;
    }
	public  function getSynctypeDefault(){
        $list=array();
        $list[DOMAINTABLE::$LMS_SYNC_WITHCREATED_DATA]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynctype.syncwithcreateddatainlms');
        $list[DOMAINTABLE::$LMS_SYNC_REPLICATION_DATA]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynctype.replicationdatainlms');
		
        return $list;
    }
     //delete
    public  function getSyncDefaultLevel(){
        $list=array();
        $list[DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccoursecat');
        $list[DOMAINTABLE::$LMS_CONTEXT_COURSE]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccourse');
        $list[DOMAINTABLE::$LMS_CONTEXT_GROUP]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccoursegroup');
        $list[DOMAINTABLE::$LMS_CONTEXT_ACTIVITY]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccourseactivity');
          return $list;
    }

    public  function getSyncLevel1(){
        $list=array();
          $list[DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccoursecat');
        $list[DOMAINTABLE::$LMS_CONTEXT_COURSE]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccourse');
		
        return $list;
    }

     public  function getSyncLevel2(){
        $list=array();
        $list[DOMAINTABLE::$LMS_CONTEXT_COURSECATEGORY]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccoursecat');
        $list[DOMAINTABLE::$LMS_CONTEXT_COURSE]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccourse');
        $list[DOMAINTABLE::$LMS_CONTEXT_GROUP]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccoursegroup');
          return $list;
    } 
    
    public  function getSyncLevel3(){
        $list=array();
        $list[DOMAINTABLE::$LMS_CONTEXT_COURSE]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccourse');
        $list[DOMAINTABLE::$LMS_CONTEXT_GROUP]=$this->getTranslator()->trans('badiu.ams.synclms.moodle.lmssynccoursegroup');
          return $list;
    }
   
     public function getCoursecatForAutocomplete() { 
        $gsearch=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch');
		$sserviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_serviceid');
        $wsql="";
       
	    $sparam=array();
        $sparam['_serviceid']=$sserviceid;
        $param['name']=$gsearch;
		$sparam['_key']='course.category.getlisttree';
        //$sparam['_key']=null;
        //print_r($sparam);
      // echo "<hr>";
        $result=$this->webservice->exeCommand($sparam);
		
      
       return $result;
     }
     
     public function getCourseForAutocomplete() { 
         $lmscoursecat=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('lmscoursecat');
        $gsearch=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch');
       $wsql="";
         if(!empty($lmscoursecat)){ $wsql.=" AND category=$lmscoursecat ";}
        if(!empty($gsearch)){ $wsql.=" AND LOWER(CONCAT(id,fullname,shortname)) LIKE '%".strtolower($gsearch)."%' ";}
        $sql="SELECT id,fullname as name,shortname  FROM {_pfx}course WHERE id > 0  $wsql ";
        
         $result=$this->webservice->search($sql);
		
       return $result;
     }
     public function getCourseForAutocompleteOnEdit($param) { 
         
            $dconfig = $this->getJson()->decode($param, true);
            $lms= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');
            $coursename=$this->getUtildata()->getVaueOfArray($lms, 'lmscoursename');
            return $coursename;
     }
     
	  public function getCourseTemplateForAutocomplete() { 
        $gsearch=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch');
       $wsql="";
        if(!empty($gsearch)){ $wsql.=" AND LOWER(CONCAT(id,fullname,shortname)) LIKE '%".strtolower($gsearch)."%' ";}
        $sql="SELECT id,fullname as name,shortname  FROM {_pfx}course WHERE id > 0  $wsql ";
        
         $result=$this->webservice->search($sql);
		
       return $result;
     }
	 
	 public function getCourseTemplateForAutocompleteOnEdit($param) { 
         
            $dconfig = $this->getJson()->decode($param, true);
            $lms= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');
            $coursename=$this->getUtildata()->getVaueOfArray($lms, 'lmscoursenametemplate');
            return $coursename;
     }
     public function getCoursecatForAutocompleteOnEdit($param) { 
            $dconfig = $this->getJson()->decode($param, true);
            $lms= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');
            $coursename=$this->getUtildata()->getVaueOfArray($lms, 'lmscoursecatname');
            return $coursename;
     }
     public function getCoursecatparentForAutocompleteOnEdit($param) { 
        
            $dconfig = $this->getJson()->decode($param, true);
            $lms= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');
            $coursecatparentname=$this->getUtildata()->getVaueOfArray($lms, 'lmscoursecatparentname');
            return $coursecatparentname;
     }
     public function getCoursegroupForAutocomplete() { 
         $lmscourse=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('lmscourse');
        $gsearch=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch');
        $wsql="";
         if(!empty($lmscourse)){ $wsql.=" AND courseid=$lmscourse ";}
        if(!empty($gsearch)){ $wsql.=" AND LOWER(CONCAT(id,name)) LIKE '%".strtolower($gsearch)."%' ";}
        $sql="SELECT id,name  FROM {_pfx}groups WHERE id > 0  $wsql ";
         $result=$this->webservice->search($sql);
      
       return $result;
     }
     public function getCoursegroupForAutocompleteOnEdit($param) { 
         
            $dconfig = $this->getJson()->decode($param, true);
            $lms= $this->getUtildata()->getVaueOfArray($dconfig, 'lmsintegration');
            $coursegroupname=$this->getUtildata()->getVaueOfArray($lms, 'lmscoursegroupname');
            return $coursegroupname;
     }
	 
	  public  function getTypeoperation(){
        $list=array();
        $list['import']=$this->getTranslator()->trans('badiu.ams.synclms.moodle.import');
       // $list['export']=$this->getTranslator()->trans('badiu.ams.synclms.moodle.export');
        return $list;
    }
	
	public  function getClasseDataImport(){
        $list=array();
        $list['coursefinalgrade']=$this->getTranslator()->trans('badiu.ams.synclms.moodle.import.coursefinalgrade');
		$list['coursecompletation']=$this->getTranslator()->trans('badiu.ams.synclms.moodle.import.coursecompletation');
		$list['courseprogress']=$this->getTranslator()->trans('badiu.ams.synclms.moodle.import.courseprogress');
        
        return $list;
    }
	
}
