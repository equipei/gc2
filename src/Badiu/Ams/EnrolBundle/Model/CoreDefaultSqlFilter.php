<?php

namespace Badiu\Ams\EnrolBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class CoreDefaultSqlFilter extends BadiuSqlFilter{
   
    function __construct(Container $container) {
            parent::__construct($container);
           
       }

        function exec() {
           
           return null;
        }
        
       
	function addfilteractive() {
           $enableenrol= $this->getUtildata()->getVaueOfArray($this->getParam(),'enableenrol');
		   $sql="";
		   if($enableenrol==1){$sql=$this->activefilter();}
           if($enableenrol=="0"){$sql=$this->inactivefilter();}
		   
			return  $sql;
	}			
        function activefilter($param=array()) {
			$statusenrolalias=$this->getUtildata()->getVaueOfArray($param,'_statusenrolalias');
			$enrolalias=$this->getUtildata()->getVaueOfArray($param,'_enrolalias');
			if(empty($statusenrolalias)){$statusenrolalias="s";}
			if(empty($enrolalias)){$enrolalias="o";}
			//s.enable=:enrolenable
            $sql=" AND $statusenrolalias.enable=1 AND ( $enrolalias.timestart IS NULL OR $enrolalias.timestart <= :enroltimenow1 )  AND ( $enrolalias.timeend IS NULL OR $enrolalias.timeend >= :enroltimenow2 )  ";
            return $sql;
         }
		function inactivefilter($param=array()) {
			$statusenrolalias=$this->getUtildata()->getVaueOfArray($param,'_statusenrolalias');
			$enrolalias=$this->getUtildata()->getVaueOfArray($param,'_enrolalias');
			if(empty($statusenrolalias)){$statusenrolalias="s";}
			if(empty($enrolalias)){$enrolalias="o";}
			
            $sql=" AND ( $statusenrolalias.enable=0 OR  $enrolalias.timestart > :enroltimenow1 OR $enrolalias.timeend < :enroltimenow2 )  ";
            return $sql;
         }
		 
		function activedatefilter($param=array()) {
			$enrolalias=$this->getUtildata()->getVaueOfArray($param,'_enrolalias');
			if(empty($enrolalias)){$enrolalias="o";}
			 $sql=" AND ( $enrolalias.timestart IS NULL OR $enrolalias.timestart <= :enroltimenow1 )  AND ( $enrolalias.timeend IS NULL OR $enrolalias.timeend >= :enroltimenow2 )  ";
            return $sql;
         }
		function inactivedatefilter($param=array()) {
			$enrolalias=$this->getUtildata()->getVaueOfArray($param,'_enrolalias');
			if(empty($enrolalias)){$enrolalias="o";}
			
            $sql=" AND ($enrolalias.timestart > :enroltimenow1 OR $enrolalias.timeend < :enroltimenow2 )  ";
            return $sql;
         }
} 
