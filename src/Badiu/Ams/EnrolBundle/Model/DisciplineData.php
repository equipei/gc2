<?php

namespace Badiu\Ams\EnrolBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class DisciplineData  extends BadiuDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
    
    //get new user user not enrol on offer
    public function getNewUsers() {
         $odisciplineid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
         $name=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
         
        if(empty($odisciplineid)) return null;

        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $userData=$this->getContainer()->get('badiu.system.user.user.data');
        
        $wsql="";
        if(!empty($name)){
            $wsql=" AND LOWER(CONCAT(o.id,o.firstname,o.email))  LIKE :name ";
            $name=strtolower($name);
        }
         $sql="SELECT DISTINCT o.id,CONCAT(o.firstname,' ',o.lastname,' ',o.email) AS name FROM ".$userData->getBundleEntity()." o LEFT JOIN ".$this->getBundleEntity()." ed WITH o.id=ed.userid  WHERE  o.entity = :entity  $wsql  AND  (ed.odisciplineid != :odisciplineid OR ed.id IS NULL) ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('odisciplineid',$odisciplineid);
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(50);
        $result= $query->getResult();
        return  $result;
        }

public function getUserNameByIdOnEditAutocomplete($id) {
       $userData=$this->getContainer()->get('badiu.system.user.user.data');
       $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT CONCAT(o.firstname,' ',o.lastname,' ',o.email) AS name FROM ".$userData->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }
    public function getListByDate($offerid,$date) {
	
        $timestart=new \DateTime();
		$timestart->setTimestamp($date->getTimestamp());
		
		 $timeend=new \DateTime();
		$timeend->setTimestamp($date->getTimestamp());
		
		$timestart->setTime(0,0,0);
		$timeend->setTime(23,59, 59);
		
		 $badiuSession=$this->getContainer()->get('badiu.system.access.session');
       
        $sql="SELECT  o FROM ".$this->getBundleEntity()." o JOIN o.odisciplineid od WHERE  o.entity = :entity AND od.offerid = :offerid AND o.timestart >= :timestart AND o.timestart <= :timeend ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
		$query->setParameter('offerid',$offerid);
        $query->setParameter('timestart',$timestart);
		$query->setParameter('timeend',$timeend);
        $result= $query->getResult();
        return  $result;
    }
      public function existEnrol($entity,$odisciplineid,$userid,$roleid) {
		$r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.odisciplineid=:odisciplineid AND o.userid=:userid AND o.roleid=:roleid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('odisciplineid',$odisciplineid);
			$query->setParameter('userid',$userid);
			$query->setParameter('roleid',$roleid);
            $result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
	}
    public function getEnrolid($entity,$odisciplineid,$userid,$roleid) {
		$r=FALSE;
            $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.odisciplineid=:odisciplineid AND o.userid=:userid AND o.roleid=:roleid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('odisciplineid',$odisciplineid);
			$query->setParameter('userid',$userid);
			$query->setParameter('roleid',$roleid);
            $result= $query->getOneOrNullResult();
            if(isset($result['id'])){$result=$result['id'];}
            else {$result=null;}
            return $result;
    }
   public function existEnrolByLms($entity,$userid,$serviceid,$lmscourseid) {
		$r=FALSE;
           $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o JOIN o.odisciplineid od  WHERE o.entity = :entity AND o.userid=:userid  AND od.sserviceid=:sserviceid  AND od.idnumberlms=:idnumberlms";
      
			$query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('userid',$userid);
			$query->setParameter('sserviceid',$serviceid);
			$query->setParameter('idnumberlms',$lmscourseid);
			$result= $query->getSingleResult();
			 if($result['countrecord']>0){$r=TRUE;}
             return $r;
   }
  
   public function getUsersid($odisciplineid,$param=array()) {
        $wsql=$this->makeSqlWhere($param);
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT u.id FROM ".$this->getBundleEntity()." o JOIN o.userid u WHERE   o.entity = :entity AND o.odisciplineid=:odisciplineid  $wsql";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('odisciplineid',$odisciplineid);
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return $result;
}

}
