<?php

namespace Badiu\Ams\EnrolBundle\Model\Lib\Service;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\EnrolBundle\Model\DOMAINTABLE;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
use Badiu\Ams\CoreBundle\Model\Lib\AmsService;
class Offer extends AmsService 
{

    /**
     * @var Container
     */
    private $container;

	private $response;
	function __construct(Container $container)
    {
        parent::__construct($container);
		$this->response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
      }

	public function exec()
    {	
			return 'no functon defined';
    }
		
	
	public function existEnrolByLms()
	{ 
		$param=$this->getParam();
		$serviceid=$this->getServiceid($param->token);
		$data=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
		$entity=$this->getEntity();
		$userid=$param->userid;
		$lmscourseid=$param->courseid;
		$resul=$data->existEnrolByLms($entity,$userid,$serviceid,$lmscourseid);
		if($resul){
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		}else{
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo(DOMAINTABLE::$REQUEST_DENIED_USER_NOT_ENROLED_IN_DISCIPLINE);
		}
		return $resul;
    }

	public function save(){ 
		//check token
		$ckeckToken=$this->checkToken();
		if(!$ckeckToken){return $this->getResponse()->get();}
		
		//get param
		$dto=$this->getParam();
		$chekParam=$this->checkParam($dto);
		if(!$chekParam){return $this->getResponse()->get();}
		
		$data=$this->getContainer()->get('badiu.ams.enrol.offer.data');
		$add=$data->add($dto->offerid,$dto->userid,$dto->roleid,$dto->statusid,$dto->timestart,$dto->timeend);
		if($add==-1){
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.offer.already.exists');
			return $this->getResponse()->get();
		}else{
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
			$this->getResponse()->setMessage($add);
			return $this->getResponse()->get();	
		}
		return $this->getResponse()->get();	
	}
	public function getParam(){
		$param= new \stdClass();
		$param->offerid=$this->getContainer()->get('request')->get('offerid');
		$param->userid=$this->getContainer()->get('request')->get('userid');
		$param->roleid=$this->getContainer()->get('request')->get('roleid');
		$param->statusid=$this->getContainer()->get('request')->get('statusid');
		$param->timestart=$this->getContainer()->get('request')->get('timestart');
		$param->timeend=$this->getContainer()->get('request')->get('timeend');
		return $param;
	}
	
	
	public function checkParam($param){
		$resp=TRUE;
		
		if(empty($param->offerid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.offer.offerid.is.empty');
			return $resp;
		}else if(empty($param->userid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.offer.userid.is.empty');
			return $resp;
		}else if(empty($param->roleid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.offer.roleid.is.empty');
			return $resp;
		}else if(empty($param->statusid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.offer.statusid.is.empty');
			return $resp;
		}
		return $resp;
	}
	 public function getResponse() {
          return $this->response;
      }


      public function setResponse($response) {
          $this->response = $response;
      }
}
?>