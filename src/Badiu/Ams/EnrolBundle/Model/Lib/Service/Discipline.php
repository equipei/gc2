<?php

namespace Badiu\Ams\EnrolBundle\Model\Lib\Service;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuExternalService;
use Badiu\Ams\EnrolBundle\Model\DOMAINTABLE;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
class Discipline  extends BadiuExternalService{

    /**
     * @var Container
     */
    private $container;

	private $response;
	function __construct(Container $container)
    {
        parent::__construct($container);
		$this->response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
      }

	public function exec()
    {	
			return 'no functon defined';
    }
		
	/* review  only param
	public function existEnrolByLms()
	{ 
		//check token
		$ckeckToken=$this->checkToken();
		if(!$ckeckToken){return $this->getResponse()->get();}
		
		//get param
		$param=$this->getParamExistEnrolByLms();
		$chekParam=$this->checkParamExistEnrolByLms($param,true);
		if(!$chekParam){return $this->getResponse()->get();}
		
		$serviceid=$this->getServiceid();
		$data=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
		$entity=$this->getEntity();
		$userid=$param->userid;
		$lmscourseid=$param->courseid;
		$resul=$data->existEnrolByLms($entity,$userid,$serviceid,$lmscourseid);
		$resp="false";
		if($resul){$resp="true";}
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($resp);
		
		return $this->getResponse()->get();
    }
	*/
	public function existEnrol()
	{ 
		//check token
		$ckeckToken=$this->checkToken();
		if(!$ckeckToken){return $this->getResponse()->get();}
		
		//get param
		$param=$this->getParamExistEnrol();
		$chekParam=$this->checkParamExistEnrol($param);
		if(!$chekParam){return $this->getResponse()->get();}
		
		$data=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
		$resul=$data->existEnrol($this->getEntity(),$param->odisciplineid,$param->userid,$param->roleid);
		$resp="false";
		if($resul){$resp="true";}
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($resp);
		
		return $this->getResponse()->get();
    }
	
	public function save()
	{ 
		//check token
		$ckeckToken=$this->checkToken();
		if(!$ckeckToken){return $this->getResponse()->get();}
		
		//get param
		$param=$this->getParamSaveEnrol();
		$chekParam=$this->checkParamSaveEnrol($param,true);
		if(!$chekParam){return $this->getResponse()->get();}
		
		$data=$this->getContainer()->get('badiu.ams.enrol.discipline.lib');
		$result=$data->add($param->odisciplineid,$param->userid,$param->roleid,$param->statusid,$param->timestart,$param->timeend);
		$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		$this->getResponse()->setMessage($result);
		return $this->getResponse()->get();
    }
	public function getParamExistEnrol(){
		$param= new \stdClass();
		$param->odisciplineid=$this->getContainer()->get('request')->get('odisciplineid');
		$param->userid=$this->getContainer()->get('request')->get('userid');
		$param->roleid=$this->getContainer()->get('request')->get('roleid');
		$param->lmscourseid=$this->getContainer()->get('request')->get('lmscourseid');//for lms
		
		return $param;
	}
	public function checkParamExistEnrol($param){
		$resp=TRUE;
		if(empty($param->odisciplineid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.discipline.odisciplineid.is.empty');
			return $resp;
		}
		if(empty($param->userid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.discipline.userid.is.empty');
			return $resp;
		}
		if(empty($param->roleid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.discipline.roleid.is.empty');
			return $resp;
		}
		return $resp;
	}
	
	public function getParamSaveEnrol(){
		$param= new \stdClass();
		$param->odisciplineid=$this->getContainer()->get('request')->get('odisciplineid');
		$param->userid=$this->getContainer()->get('request')->get('userid');
		$param->roleid=$this->getContainer()->get('request')->get('roleid');
		$param->statusid=$this->getContainer()->get('request')->get('statusid');
		
		$timestart=$this->getContainer()->get('request')->get('timestart');
		$param->timestart = new \DateTime();
		$param->timestart->setTimestamp($timestart);
		
		$timeend=$this->getContainer()->get('request')->get('timeend');
		$param->timeend = new \DateTime();
		$param->timeend->setTimestamp($timeend);
		
		return $param;
	}
	public function checkParamSaveEnrol($param){
		$resp=TRUE;
		if(empty($param->odisciplineid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.discipline.odisciplineid.is.empty');
			return $resp;
		}
		
		if(empty($param->userid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.discipline.userid.is.empty');
			return $resp;
		}
		if(empty($param->roleid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.discipline.roleid.is.empty');
			return $resp;
		}if(empty($param->statusid)) {
			$resp=FALSE;
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo('badiu.ams.enrol.discipline.statusid.is.empty');
			return $resp;
		}
		return $resp;
	}
	 public function getResponse() {
          return $this->response;
      }


      public function setResponse($response) {
          $this->response = $response;
      }
}
?>