<?php

namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormFilter;
class AccessRoleFilter extends BadiuFormFilter{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
   public function execBeforeSubmit() {

           
        }
        
   public function execAfterSubmit() {
	 
        $userid = $this->getUtildata()->getVaueOfArray($this->getParam(), 'userid');
		$roleid = $this->getUtildata()->getVaueOfArray($this->getParam(), 'roleid');
        $accessmanagelib=$this->getContainer()->get('badiu.system.access.manage.lib');
		$amparam=array('userid'=>$userid,'roleid'=>$roleid,'entity'=>$this->getEntity());
		
		$accessmanagelib->addUserRole($amparam);
         
   } 
       
    
}
