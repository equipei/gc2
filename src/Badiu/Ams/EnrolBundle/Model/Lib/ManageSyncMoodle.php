<?php
namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ManageSyncMoodle    extends BadiuModelLib{

   private $param;
    function __construct(Container $container) {
                parent::__construct($container);
       } 
  
   public function processWParmConf($dconfig) {
	   
	    $timestart=$this->getUtildata()->getVaueOfArray($dconfig,'enrol.timestart',true);
		$timeend=$this->getUtildata()->getVaueOfArray($dconfig,'enrol.timeend',true);
		$userid=$this->getUtildata()->getVaueOfArray($dconfig,'enrol.userid',true);
		$replicationupadatelms=$this->getUtildata()->getVaueOfArray($dconfig,'enrol.replicationupadatelms',true);
        $roleshorname=$this->getUtildata()->getVaueOfArray($dconfig,'enrol.roleshorname',true);
        $enable=$this->getUtildata()->getVaueOfArray($dconfig,'enrol.enable',true);
		 
		 $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		 $serviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
		 $lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynclevel',true);
		 $lmscoursecatid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatid',true);
		 $lmscourseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);				
		 $groupid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursegroupid',true);				
	     $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		

		if(($lmssynclevel=='course' || $lmssynclevel=='group') && $lmscourseid > 0 && $serviceid > 0){
			$mmelparam=array();
			$mmelparam['userid']=$userid;		
			$mmelparam['courseid']=$lmscourseid;
			$mmelparam['groupid']=$groupid;
			$mmelparam['sserviceid']=$serviceid;
			$mmelparam['forceupdate']=$replicationupadatelms;
			$mmelparam['timestart']=$timestart;
			$mmelparam['timeend']=$timeend;
            $mmelparam['roleshorname']=$roleshorname;
            $mmelparam['enable']=$enable;
			
			$this->process($mmelparam);
		}
		else if($lmssynclevel=='coursecat' && $lmscoursecatid > 0){
			$mmelparam=array();
			$mmelparam['userid']=$userid;		
			$mmelparam['sserviceid']=$serviceid;
			
			$this->process($mmelparam);
		}
   }
   public function process($param) {
	    $this->param=$param;
		
	    if(!empty($result)){return $result;}
	    $sresult=0;
		$mdluserid=$this->addUser($param);
		if(empty($mdluserid)){return null;}
		
		$param['mdluserid']=$mdluserid;
		$syncenrol=$this->addEnrol($param);
		$status=$this->getUtildata()->getVaueOfArray($syncenrol,'status');
		if($status== 'accept'){return 1;}
		
        return 0;
    }
	
	function addUser($param) { 
		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		$serviceid=$this->getUtildata()->getVaueOfArray($param,'sserviceid');
		$entity=$this->getEntity();
		
		$syncuserdata=$this->getContainer()->get('badiu.admin.server.syncuser.data');
		//check user is sync
		$fparam=array('sysuserid'=>$userid,'serviceid'=>$serviceid,'entity'=>$entity);
		$mdluserid=$syncuserdata->getGlobalColumnValue('userid',$fparam);
		if(!empty($mdluserid)){return $mdluserid;}
		
		
		$userdata=$this->getContainer()->get('badiu.system.user.user.data');
		$syncusermlib=$this->getContainer()->get('badiu.admin.server.syncuser.managelib');
		
		//add user
		
		$fparamuser=array('id'=>$userid);
		$sysuserdto= $userdata->getGlobalColumnsValue('o.username,o.ukey, o.firstname,o.lastname,o.email,o.doctype,o.docnumber,o.alternatename,o.enablealternatename', $fparamuser);
		
		 $ukey = $this->getUtildata()->getVaueOfArray($sysuserdto,'ukey');
		 $username = $this->getUtildata()->getVaueOfArray($sysuserdto,'username');
		 $email= $this->getUtildata()->getVaueOfArray($sysuserdto,'email');
		 $email=trim($email);
		 $firstname = $this->getUtildata()->getVaueOfArray($sysuserdto,'firstname');
		 $lastname = $this->getUtildata()->getVaueOfArray($sysuserdto,'lastname');
		 $idnumber = $this->getUtildata()->getVaueOfArray($sysuserdto,'idnumber');
		 $enablealternatename = $this->getUtildata()->getVaueOfArray($sysuserdto,'enablealternatename');
		 $alternatename = $this->getUtildata()->getVaueOfArray($sysuserdto,'alternatename');
		 
         if (empty($ukey)) {$ukey = "''";}
		 
		 $param = array('_serviceid' => $serviceid, '_key' => 'user.user.add', 'username' => $username, 'email' => $email, 'password' => "not cached", 'auth' => "badiuauth", 'firstname' => $firstname, 'lastname' => $lastname,'alternatename'=>$alternatename, 'idnumber' => $ukey, 'lang' =>"pt_br");
        
		 $result = $this->getSearch()->execWebService($param);
          
		 $rstatus=$this->getUtildata()->getVaueOfArray($result,'status');
		 $mdluserid =null;
		 $rstatus=$this->getUtildata()->getVaueOfArray($result,'status');
		 $rinfo=$this->getUtildata()->getVaueOfArray($result,'info');
		 if($rstatus == 'accept'){$mdluserid =$this->getUtildata()->getVaueOfArray($result,'message');}
		 else if ($rstatus == 'danied' &&  $rinfo == 'badiu.moodle.ws.error.dulicateusername') {
            $param = array('_serviceid' => $serviceid, '_key' => 'user.user.getidbyusername', 'username' =>$username);
            $result2 = $this->getSearch()->execWebService($param);
			$rstatus2=$this->getUtildata()->getVaueOfArray($result2,'status');
			$rinfo2=$this->getUtildata()->getVaueOfArray($result2,'info');
			if ($rstatus2 == 'accept') {$mdluserid =$this->getUtildata()->getVaueOfArray($result2,'message');}
		}			
		
		 if(empty($mdluserid)){/*echo "syn mdl user failure";exit;*/;return null;}
		 //sync user
		 $paramdto=array();
		 $paramdto['sserver']['entity']=$entity;
		 $paramdto['sserver']['id']=$serviceid;
		 
		 $skrow['id']=$mdluserid;
		 $skrow['username']=$username;
		 $skrow['idnumber']=$ukey;
		 $skrow['firstname']=$firstname;
		 $skrow['lastname']=$lastname;
		 $skrow['email']=$email;
		 $skrow['auth']="badiuauth";
		 $skrow['keysync']='username';
		 $skrow['autosync']=1;
		 $skrow['_forceupdate']=0; 
		 $skrow['anonymousaccess']=0;
		 $paramdto['user']=$skrow;
		 $synuser=$syncusermlib->process($paramdto);
		 
		return $mdluserid;
         
	}
   function addEnrol($param) { 
		 $mdluserid=$this->getUtildata()->getVaueOfArray($param,'mdluserid',true);
		 $sserviceid=$this->getUtildata()->getVaueOfArray($param,'sserviceid');
		 $courseid=$this->getUtildata()->getVaueOfArray($param,'courseid');
		 $groupid=$this->getUtildata()->getVaueOfArray($param,'groupid');
		 $timeend=$this->getUtildata()->getVaueOfArray($param,'timeend');
		 $timestart=$this->getUtildata()->getVaueOfArray($param,'timestart');
		 $forceupdate=$this->getUtildata()->getVaueOfArray($param,'forceupdate');
         $roleshorname=$this->getUtildata()->getVaueOfArray($param,'roleshorname');
         $enable=$this->getUtildata()->getVaueOfArray($param,'enable');
		 
		 $param = array('_serviceid' => $sserviceid, '_key' => 'enrol.enrol.add', 'userid' => $mdluserid, 'courseid' => $courseid,'groupid' => $groupid,'timestart'=>$timestart,'timeend'=>$timeend,'forceupdate'=>$forceupdate,'roleshorname'=>$roleshorname,'enable'=>$enable);
        $result = $this->getSearch()->execWebService($param);
		return $result;
   }
    function canAccess($param) { 
		
		$timestart=null;
		$timeend=null;
		
		$enrollmsaccesstimestart=$this->getUtildata()->getVaueOfArray($param,'enrollmsaccesstimestart');
	    $enrollmsaccesstimeend=$this->getUtildata()->getVaueOfArray($param,'enrollmsaccesstimeend');
		
		if(!empty($enrollmsaccesstimestart)){$timestart=$enrollmsaccesstimestart;}
		if(!empty($enrollmsaccesstimeend)){$timeend=$enrollmsaccesstimeend;}
		
		$classelmsaccesstimestart=$this->getUtildata()->getVaueOfArray($param,'classelmsaccesstimestart');
		$classelmsaccesstimeend=$this->getUtildata()->getVaueOfArray($param,'classelmsaccesstimeend');
		
		if(!empty($classelmsaccesstimestart) && empty($timestart)){$timestart=$classelmsaccesstimestart;}
		if(!empty($classelmsaccesstimeend) && empty($timeend)){$timeend=$classelmsaccesstimeend;}
		
		
		$odisciplinelmsaccesstimestart=$this->getUtildata()->getVaueOfArray($param,'odisciplinelmsaccesstimestart');
		$odisciplinelmsaccesstimeend=$this->getUtildata()->getVaueOfArray($param,'odisciplinelmsaccesstimeend');
		
		if(!empty($odisciplinelmsaccesstimestart) && empty($timestart)){$timestart=$odisciplinelmsaccesstimestart;}
		if(!empty($odisciplinelmsaccesstimeend) && empty($timeend)){$timeend=$odisciplinelmsaccesstimeend;}
		
		$now=new \DateTime();
		//timestart
		if(!empty($timestart)  && is_a($timestart, 'DateTime')){
			if($timestart->getTimestamp() > $now->getTimestamp()){return false;}
		}
		
		//timeend
		if(!empty($timeend)  && is_a($timeend, 'DateTime')){
			if($timeend->getTimestamp() < $now->getTimestamp()){return false;}
		}
			
       return true;
    }
   
   
   function getParam() { 
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }
}
