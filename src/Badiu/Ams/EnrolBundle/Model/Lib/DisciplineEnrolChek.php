<?php

namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\EnrolBundle\Model\DOMAINTABLE;
use Badiu\System\CoreBundle\Model\Lib\Util\SERVICEDOMAINTABLE;
use Badiu\Ams\CoreBundle\Model\Lib\AmsService;
class DisciplineEnrolChek extends AmsService 
{

    /**
     * @var Container
     */
    private $container;

	private $response;
	function __construct(Container $container)
    {
        parent::__construct($container);
		$this->response=$this->getContainer()->get('badiu.system.core.lib.util.jsonsyncresponse');
      }

	public function exec()
    {	
		//get param
		$param=$this->getParam();
		
		$checkParam=$this->checkParam($param);
		if(!$checkParam){return $this->getResponse()->get();}
		
	
		$chekToken=$this->checkToken($param->token);
		if(!$chekToken){return $this->getResponse()->get();}
		
		//exec service
		$checkEnrol=$this->checkEnrol($param);
				
		return $this->getResponse()->get();
    }
		
	
	public function checkEnrol($param)
    { 
		$serviceid=$this->getServiceid();
		$data=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
		$entity=$this->getEntity();
		$userid=$param->userid;
		$lmscourseid=$param->courseid;
		//$serviceid=1;
		//echo "entity: $entity userid: $userid lmscourseid: $lmscourseid";exit;
		$resul=$data->existEnrolByLms($entity,$userid,$serviceid,$lmscourseid);
		if($resul){
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_ACCEPT);
		}else{
			$this->getResponse()->setStatus(SERVICEDOMAINTABLE::$REQUEST_DENIED);
			$this->getResponse()->setInfo(DOMAINTABLE::$REQUEST_DENIED_USER_NOT_ENROLED_IN_DISCIPLINE);
		}
		return $resul;
    }
	
	 public function getResponse() {
          return $this->response;
      }


      public function setResponse($response) {
          $this->response = $response;
      }
}
?>