<?php
namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ManageChangeStatus    extends BadiuModelLib{

   private $param;
    function __construct(Container $container) {
                parent::__construct($container);
       } 
  

   
   public function initDefaultRoles() {
	   $roles='[
		 {"criteria":"coursenotcompletednotallrequiredactivitycompleted","statusshortname":"preregistrationenrol","newstatusshortname":"studyingenrol","activeclasse":1},
		 {"criteria":"coursenotcompletednotallrequiredactivitycompleted","statusshortname":"activeenrol","newstatusshortname":"studyingenrol","activeclasse":1},
	     
		 {"criteria":"coursenotcompletedallrequiredactivitycompleted","statusshortname":"studyingenrol","newstatusshortname":"disapprovedenrol","activeclasse":1,"waiting": {"time": 300, "itemtype": "finalgrade" }},
	     {"criteria":"coursenotcompletednotallrequiredactivitycompleted","statusshortname":"studyingenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletednotallrequiredactivitycompleted","statusshortname":"studyingenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyrequiredactivitycompleted","statusshortname":"preregistrationenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyrequiredactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyrequiredactivitycompleted","classetypeaccessshortname":"trainingrestricted","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyrequiredactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyrequiredactivitycompleted","classetypeaccessshortname":"trainingrestricted","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyrequiredactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 {"criteria":"coursenotcompletedanyrequiredactivitycompleted","classetypeaccessshortname":"trainingfree","statusshortname":"preregistrationenrol","newstatusshortname":"evadedenrol","activeclasse":0},
		 {"criteria":"coursenotcompletedanyrequiredactivitycompleted","classetypeaccessshortname":"trainingrestricted","statusshortname":"activeenrol","newstatusshortname":"evadedenrol","activenrol":0,"activeclasse":1},
		 
		 {"criteria":"coursecompleted","statusshortname":"disapprovedenrol","newstatusshortname":"approvedenrol","activeclasse":1},
		 {"criteria":"coursecompleted","statusshortname":"evadedenrol","newstatusshortname":"approvedenrol","activeclasse":1},
		 {"criteria":"coursecompleted","statusshortname":"quitterenrol","newstatusshortname":"approvedenrol","activeclasse":1}
	   
	   ]';
	   
	   $roles = $this->getJson()->decode($roles, true);
		return $roles;
   }
   public function changeClasseEnrolStatus($scparam) {
	   
	    $roles=null;
		$defaultroles=$this->getUtildata()->getVaueOfArray($scparam,'defaultroles');
		if($defaultroles){$roles=$this->initDefaultRoles();}
		else{$roles=$this->getUtildata()->getVaueOfArray($scparam,'roles');}
		
		
		if(!is_array($roles)){return null;}
		
		$scclasseid=$this->getUtildata()->getVaueOfArray($scparam,'classeid');
		$scuserid=$this->getUtildata()->getVaueOfArray($scparam,'userid');
		$cresult=array();
		
		foreach ($roles as $role) {
			
			$maxrecord=$this->getUtildata()->getVaueOfArray($role,'maxrecord');
			$classetypeaccessshortname=$this->getUtildata()->getVaueOfArray($role,'classetypeaccessshortname');
			$statusshortname=$this->getUtildata()->getVaueOfArray($role,'statusshortname');
			$newstatusshortname=$this->getUtildata()->getVaueOfArray($role,'newstatusshortname');
			$criteria=$this->getUtildata()->getVaueOfArray($role,'criteria'); //coursecompleted | coursenotcompletedallrequiredactivitycompleted |  coursenotcompletedanyrequiredactivitycompleted  |  coursenotcompletednotallrequiredactivitycompleted     ----| notcompletedallrequireactivitywhenerolend | notcompletedanyrequireactivitywhenenrolend | notcoursecompletedwhenenrolend | notcoursecompletedwhenofferend | withoutaccesswhenenrolend | withoutaccesswhenofferend
			$activenrol=$this->getUtildata()->getVaueOfArray($role,'activenrol');
			$activeclasse=$this->getUtildata()->getVaueOfArray($role,'activeclasse');
			$waiting=$this->getUtildata()->getVaueOfArray($role,'waiting');
			$filter=$this->getUtildata()->getVaueOfArray($role,'filter');
		
			if(empty($maxrecord)){$maxrecord=10000;}
	
			$fpparam=array('activenrol'=>$activenrol,'activeclasse'=>$activeclasse,'criteria'=>$criteria,'classetypeaccessshortname'=>$classetypeaccessshortname,'statusshortname'=>$statusshortname,'newstatusshortname'=>$newstatusshortname,'maxrecord'=>$maxrecord,'classeid'=>$scclasseid,'userid'=>$scuserid);
		
			$enrols=$this->getEnrolClasseToChangeStatus($fpparam);
			
			//if(!is_array($enrols)){return  null;}
			$count=0;
		 
		  $enorldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		  $datashouldexec=0;
		  $dataexec=0;
		  $message=$criteria;
		 
		  if(is_array($enrols)){
			$datashouldexec=sizeof($enrols);
		 	foreach ($enrols as $row) {
			  $classeid =$this->getUtildata()->getVaueOfArray($row,'classeid');
			  $userid =$this->getUtildata()->getVaueOfArray($row,'userid');
			  $enrolid=$this->getUtildata()->getVaueOfArray($row,'id');
			  $entity=$this->getUtildata()->getVaueOfArray($row,'entity');
			  
			  $timemodified=$this->getUtildata()->getVaueOfArray($row,'timemodified');
			  $row['_waiting']=$waiting;
			  $row['_filter']=$filter;
			 
			 
			  $process=$this->isWaiting($row);
			  if($process){
				  $hasfilter=$this->checkFilter($row);
				  if($hasfilter){$process=false;}
			  }
			  if($process){
				   $newstatusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,$newstatusshortname); //review add session
			  
					$fparam=array('id'=>$enrolid,'statusid'=>$newstatusid,'timefinish'=>new \DateTime());
					$r= $enorldata->updateNativeSql($fparam,false);
					$datashouldexec=1;
					if($r){$r=1;}
					$dataexec=$r;
					$message=null;
					
					//$this->cresult = $this->addResult($this->cresult, $resultmdl); 
					$count++;
					$dataexec++;
				   
			   }
			  
		  }
		  
		  } //end if(is_array($enrols))
			  
		  $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
		  array_push($cresult,$resultmdl);
		}
		  return $cresult;
	}
	
   public function getEnrolClasseToChangeStatus($param) {

		 $maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		 $statusshortname=$this->getUtildata()->getVaueOfArray($param,'statusshortname');
		 $classetypeaccessshortname=$this->getUtildata()->getVaueOfArray($param,'classetypeaccessshortname');
		 $criteria=$this->getUtildata()->getVaueOfArray($param,'criteria'); //coursecompleted  | coursenotcompletedallrequiredactivitycompleted   | coursenotcompletedanyrequiredactivitycompleted      -- rever | completedallrequireactivity | notcompletedallrequireactivitywhenerolend | notcompletedanyrequireactivitywhenenrolend | notcoursecompletedwhenenrolend | notcoursecompletedwhenofferend | withoutaccesswhenenrolend | withoutaccesswhenofferend
		 $isactivenrol=$this->getUtildata()->getVaueOfArray($param,'activenrol');
		 $isactiveclasse=$this->getUtildata()->getVaueOfArray($param,'activeclasse');
		 
		 $classeid=$this->getUtildata()->getVaueOfArray($param,'classeid');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 
		
		 if(empty($maxrecord)){$maxrecord=10000;}
		 $edata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 
		 $itemtype=null;
		 $wsql="";
		 
		 if(!empty($classeid)){$wsql.=" AND o.classeid = $classeid "; }
		if(!empty($userid)){$wsql.=" AND o.userid = $userid "; }
		 
		//if(!empty($classeid)){echo " dd $classeid | $userid"; print_R($param);exit;}
		 if($criteria=='coursecompleted'){
			 $wsql.=" AND g.grade = :grade  ";	
			 $grade=1;
			 $itemtype='finalcompletation';
			
		 }
		 //requiredactivitycompleted
		 else if($criteria=='coursenotcompletedallrequiredactivitycompleted'){
			 $wsql.=" AND g.customint4 > 0 AND g.customint3 > 0 AND g.customint4=g.customint3 ";
			 $wsql.=" AND (SELECT COUNT(g1.id) FROM BadiuAdminGradeBundle:AdminGrade g1 JOIN g1.itemid i1 WHERE i1.moduleinstance=o.classeid AND i1.modulekey='badiu.ams.offer.classe' AND i1.itemtype= 'progress' AND  g1.userid=o.userid AND g1.grade=1) = 0";
			 $wsql.="  AND g.grade > :grade  ";	
			 $grade=0;
			 $itemtype='progress';
		 }
		 else if($criteria=='coursenotcompletedanyrequiredactivitycompleted'){
			 $wsql.=" AND g.customint4 = 0 AND g.customint3 > 0 ";
			 $wsql.=" AND (SELECT COUNT(g1.id) FROM BadiuAdminGradeBundle:AdminGrade g1 JOIN g1.itemid i1 WHERE i1.moduleinstance=o.classeid AND i1.modulekey='badiu.ams.offer.classe' AND i1.itemtype= 'progress' AND  g1.userid=o.userid AND g1.grade=1) = 0";
			 $wsql.="  AND g.grade >= :grade  ";	
			 $grade=0;
			 $itemtype='progress';
		 } else if($criteria=='coursenotcompletednotallrequiredactivitycompleted'){
			 $wsql.=" AND g.customint4 > 0 AND g.customint3 > 0 AND g.customint4  < g.customint3 ";
			 $wsql.=" AND (SELECT COUNT(g1.id) FROM BadiuAdminGradeBundle:AdminGrade g1 JOIN g1.itemid i1 WHERE i1.moduleinstance=o.classeid AND i1.modulekey='badiu.ams.offer.classe' AND i1.itemtype= 'progress' AND  g1.userid=o.userid AND g1.grade=1) = 0";
			 $wsql.="  AND g.grade > :grade  ";	
			 $grade=0;
			 $itemtype='progress';
		 }
		 
		  //not required activitycompleted
		 else if($criteria=='coursenotcompletedallactivitycompleted'){
			 $wsql.=" AND g.customint2 > 0 AND g.customint1 > 0 AND g.customint2=g.customint1 ";
			 $wsql.=" AND (SELECT COUNT(g1.id) FROM BadiuAdminGradeBundle:AdminGrade g1 JOIN g1.itemid i1 WHERE i1.moduleinstance=o.classeid AND i1.modulekey='badiu.ams.offer.classe' AND i1.itemtype= 'progress' AND  g1.userid=o.userid AND g1.grade=1) = 0";
			 $wsql.="  AND g.grade > :grade  ";	
			 $grade=0;
			 $itemtype='progress';
		 }
		 else if($criteria=='coursenotcompletedanyactivitycompleted'){
			 $wsql.=" AND g.customint2 = 0 AND g.customint1 > 0 ";
			 $wsql.=" AND (SELECT COUNT(g1.id) FROM BadiuAdminGradeBundle:AdminGrade g1 JOIN g1.itemid i1 WHERE i1.moduleinstance=o.classeid AND i1.modulekey='badiu.ams.offer.classe' AND i1.itemtype= 'progress' AND  g1.userid=o.userid AND g1.grade=1) = 0";
			 $wsql.="  AND g.grade >= :grade  ";	
			 $grade=0;
			 $itemtype='progress';
		 } else if($criteria=='coursenotcompletednotallactivitycompleted'){
			 $wsql.=" AND g.customint2 > 0 AND g.customint1 > 0 AND g.customint2  < g.customint1 ";
			 $wsql.=" AND (SELECT COUNT(g1.id) FROM BadiuAdminGradeBundle:AdminGrade g1 JOIN g1.itemid i1 WHERE i1.moduleinstance=o.classeid AND i1.modulekey='badiu.ams.offer.classe' AND i1.itemtype= 'progress' AND  g1.userid=o.userid AND g1.grade=1) = 0";
			 $wsql.="  AND g.grade > :grade  ";	
			 $grade=0;
			 $itemtype='progress';
		 }
		 
		 
		 
		 if(!empty($classetypeaccessshortname)){ $wsql.=" AND clta.shortname=:classetypeaccessshortname ";}
		 if(!empty($statusshortname)){$wsql.=" AND s.shortname=:statusshortname ";}
       
		
		
	   if($isactiveclasse==1){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		   $wsql.=$classedefaultsqlfilter->activefilter($fparam);
	   }else if($isactiveclasse===0){
		   $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
		   $classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
		   $wsql.=$classedefaultsqlfilter->inactivefilter($fparam);
	   }
	   
	   
	   if($isactivenrol==1){
			$fparam=array('_enrolalias'=>'o');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.enrol.core.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->activedatefilter($fparam);
			
		}else if($isactivenrol===0){
			$fparam=array('_enrolalias'=>'o');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.enrol.core.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->inactivedatefilter($fparam);
		}
		
		
		
		 $sql = " SELECT o.id,u.id AS userid,i.moduleinstance AS classeid,o.entity  FROM BadiuAmsEnrolBundle:AmsEnrolClasse o JOIN o.statusid s JOIN o.roleid r JOIN o.userid u  JOIN o.classeid cl JOIN cl.statusid cls LEFT JOIN cl.typeaccessid clta JOIN BadiuAdminGradeBundle:AdminGradeItem i WITH (o.classeid=i.moduleinstance AND i.modulekey='badiu.ams.offer.classe') JOIN  BadiuAdminGradeBundle:AdminGrade g WITH (g.itemid=i.id AND o.userid=g.userid) WHERE i.itemtype=:itemtype $wsql ";
		
		// if($criteria=='coursecompleted'){echo $sql;exit;}
		$query = $edata->getEm()->createQuery($sql);
        $query->setParameter('itemtype',$itemtype );
		if(!empty($classetypeaccessshortname)){$query->setParameter('classetypeaccessshortname', $classetypeaccessshortname);}
		if(!empty($statusshortname)){$query->setParameter('statusshortname', $statusshortname);}
		if($grade!==null){$query->setParameter('grade', $grade);}
		
		if($isactiveclasse==1 || $isactiveclasse===0){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		 }	
		 
		 if($isactivenrol==1 || $isactivenrol===0){
			$tnow=new \DateTime();
		    $query->setParameter('enroltimenow1', $tnow);
		    $query->setParameter('enroltimenow2', $tnow);
		}
		
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
	
		return $result;
    } 

	public function isWaiting($param) {
		
		 //"waiting": {"time": 300, "itemtype": "finalgrade" }
		$waiting=$this->getUtildata()->getVaueOfArray($param,'_waiting');
		if(empty($waiting)){return true;}
		if(!is_array($waiting)){return true;}
		
		$time=$this->getUtildata()->getVaueOfArray($waiting,'time');
		$itemtype=$this->getUtildata()->getVaueOfArray($waiting,'itemtype');
		
		if(empty($time)){return true;}
		if(empty($itemtype)){return true;}
		
		$timemodified=$this->getTimemodifiedgrade($param);
	
		if(empty($timemodified)){return true;}
	
     	
		if (!is_a($timemodified, 'DateTime') && is_string($timemodified)) {
			$timemodified = \DateTime::createFromFormat('Y-m-d H:i:s', $timemodified);
		}
				
		if (is_a($timemodified, 'DateTime')){
			$timeaddwait=$timemodified->getTimestamp()+$time;
			$tnow=new \DateTime();
			if($tnow->getTimestamp() < $timeaddwait){return false;}
					
		}
		
	return true;		
			  
	}	
	
	public function checkFilter($param) {
		$filter=$this->getUtildata()->getVaueOfArray($param,'_filter');
		if(empty($filter)){return false;}
		
		$service=$this->getUtildata()->getVaueOfArray($filter,'service');
		
		$result=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice')->execsrt($service,$param);
		
		return $result;
		
		return false;
	}
	public function getTimemodifiedgrade($param) {
		 $classeid=$this->getUtildata()->getVaueOfArray($param,'classeid');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 $itemtype=$this->getUtildata()->getVaueOfArray($param,'_waiting.itemtype',true);
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		
		 $itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		$gradedata=$this->getContainer()->get('badiu.admin.grade.grade.data');
		$paramfilter=array('entity'=>$entity,'modulekey'=>'badiu.ams.offer.classe','moduleinstance'=>$classeid,'itemtype'=>$itemtype);
		$itemid=$itemdata->getGlobalColumnValue('id',$paramfilter);
		
		if(empty($itemid)){return $itemid;}
		$gparam=array('itemid'=>$itemid,'userid'=>$userid);		
		$timemodifiedgrade=$gradedata->getGlobalColumnValue('timemodifiedgrade',$gparam); 
		
		return $timemodifiedgrade;
		
	}
   function getParam() { 
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }
}
