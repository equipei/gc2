<?php
namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

//http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_function=offer&_service=badiu.ams.enrol.lib.enrolusers&_function=offer&offerid=367&marker=ead
class EnrolUsers  extends BadiuModelLib {

    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	 
	 function offer() {
		 $offerid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('offerid');
		 $marker=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('marker');
		 
		  if(empty($offerid)){return null;}
		  if(empty($marker)){return null;}
		  
		 $userdata=$this->getContainer()->get('badiu.system.user.user.data');
		 $listuser=$userdata->getIdsByMarker($this->getEntity(),$marker);
		 if(empty($listuser)){return null;}
		 if(!is_array($listuser)){return null;}
		 
		 //data to enrol
		  $statusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($this->getEntity(),'active');
		  $roleid=$this->getContainer()->get('badiu.ams.role.role.data')->getIdByShortname($this->getEntity(),'student'); //teachear
		  
		  $offerdata=$this->getContainer()->get('badiu.ams.offer.offer.data');
		  $offerinfo=$offerdata->getGlobalColumnsValue('o.timestart,o.timeend',array('entity'=>$this->getEntity(),'id'=>$offerid));
		  
		  $timestart=$this->getUtildata()->getVaueOfArray($offerinfo,'timestart');
		  $timeend=$this->getUtildata()->getVaueOfArray($offerinfo,'timeend');
		  $offerlib=$this->getContainer()->get('badiu.ams.enrol.offer.lib');
		  $count=0;
		  foreach ($listuser as $row) {
			  $userid=$this->getUtildata()->getVaueOfArray($row,'id');
			  $r= $offerlib->add($offerid,$userid,$roleid,$statusid,$timestart,$timeend,$marker);
			  if($r){$count++;}
		  }
		   
		return $count;
		 
	 }
    
}
