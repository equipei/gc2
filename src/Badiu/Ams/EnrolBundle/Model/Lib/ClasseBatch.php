<?php

namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

//d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?_service=badiu.ams.enrol.classe.lib.batch&classeid=134&dayenrolduration=30&dayenroldurationoffer=30&mdlusrids=9,98
class ClasseBatch extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
		public function exec() {
			$resultp=array('classe'=>0,'discipline'=>0,'offer'=>0,'moodle'=>0);
			$classeid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('classeid');
			$mdlusrids=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('mdlusrids');
			$dayenrolduration=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('dayenrolduration');
			$dayenroldurationoffer=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('dayenroldurationoffer');
			$timestart=new \DateTime(); 
            $timeend= new \DateTime(); 
			$timeendoffer= new \DateTime();
			$timeendmdl=0;
            if(!empty($dayenrolduration)){
               $timeend->modify('+'.$dayenrolduration.' day');
			   $timeendmdl=$timeend->getTimestamp();
		   }else{$timeend=null;}

		    if(!empty($dayenroldurationoffer)){
                $timeendoffer->modify('+'.$dayenroldurationoffer.' day');
            }else{$timeendoffer=null;}
			$listusers=$this->getUtildata()->castStringToArray($mdlusrids,",");
			
			if(empty($classeid)){return null;}
			if(empty($listusers)){return null;}
			if(sizeof($listusers)==0){return null;}
			
			$statusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($this->getEntity(),'active');
			$roleid=$this->getContainer()->get('badiu.ams.role.role.data')->getIdByShortname($this->getEntity(),'student'); //teachear
			$classedata=$this->getContainer()->get('badiu.ams.offer.classe.data');
			$disciplineid=$classedata->geDisciplineidById($classeid);
			
			$disciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			$offerid=$disciplinedata->getOfferidById($disciplineid) ;
			
			$classelib=$this->getContainer()->get('badiu.ams.enrol.classe.lib');
			$disciplinelib=$this->getContainer()->get('badiu.ams.enrol.discipline.lib');
			$offerlib=$this->getContainer()->get('badiu.ams.enrol.offer.lib');
			$managesyncmoodle= $this->getContainer()->get('badiu.ams.enrol.lib.managesyncmoodle');
			
			$fparamclasse=array('id'=>$classeid);
			$dconfig=$classedata->getGlobalColumnValue('dconfig',$fparamclasse);
		 
		 
			$dconfig = $this->getJson()->decode($dconfig, true); 
		 
			$status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
			$serviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
			$lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynclevel',true);
			$lmscoursecatid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatid',true);
			$lmscourseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);				
			$status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
			$status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
			
		
			$syncuserdata=$this->getContainer()->get('badiu.admin.server.syncuser.data');
			
			foreach ($listusers as $mdluserid) {
				
				$paramcheck=array('userid'=>$mdluserid,'serviceid'=>$serviceid,'entity'=>$this->getEntity());
				$userid=$syncuserdata->getSysuserid($paramcheck);
				
				if(!empty($userid)){
					$addclasse=$classelib->add($classeid,$userid,$roleid,$statusid,$timestart,$timeend);
					$adddiscipline=$disciplinelib->add($disciplineid,$userid,$roleid,$statusid,$timestart,$timeend);
					$addoffer=$offerlib->add($offerid,$userid,$roleid,$statusid,$timestart,$timeendoffer);
					$addmdl=0;
					if($lmssynclevel=='course' && $lmscourseid > 0 && $serviceid > 0){
						$mmelparam=array();
						$mmelparam['userid']=$userid;		
						$mmelparam['courseid']=$lmscourseid;
						$mmelparam['sserviceid']=$serviceid;
						$mmelparam['timeend']=$timeendmdl;
					
						$addmdl=$managesyncmoodle->process($mmelparam);
					}
					if($addclasse){$resultp['classe']++;}
					if($adddiscipline){$resultp['discipline']++;}
					if($addoffer){$resultp['offer']++;}
					if($addmdl){$resultp['moodle']++;}
				}
				
				
			}
			return $resultp;
		}
		

			
		

}
