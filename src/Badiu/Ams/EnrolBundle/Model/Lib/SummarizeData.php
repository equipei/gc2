<?php
namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Report\SummarizeData as ReportSummarizeData;
class SummarizeData  extends ReportSummarizeData{

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
	function teste(){
		//$fparam=array('_period'=>'allyears','classetypeshortname'=>'trainingpresential');
		$fparam=array('_period'=>'allyears');
		$result=$this->countByYear($fparam); 
		
		echo "<pre>"; 
		print_r($result);
		echo "</pre>";exit;
	}
	
	function countByYearSearch($param,$dresult){
		$operation=$this->getUtildata()->getVaueOfArray($param,'operation');
		$statusshortname=$this->getUtildata()->getVaueOfArray($param,'statusshortname');
		$roleshortname=$this->getUtildata()->getVaueOfArray($param,'roleshortname');
		$classetypeshortname=$this->getUtildata()->getVaueOfArray($param,'classetypeshortname');
		$disciplinedtype=$this->getUtildata()->getVaueOfArray($param,'disciplinedtype');
		$offershortname=$this->getUtildata()->getVaueOfArray($param,'offershortname');
		$isclasseactive=$this->getUtildata()->getVaueOfArray($param,'isclasseactive');
		
		
		$ddate1=$this->getUtildata()->getVaueOfArray($param,'_date1');
		$ddate2=$this->getUtildata()->getVaueOfArray($param,'_date2');
		$year=$this->getUtildata()->getVaueOfArray($param,'_year');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		
		
		
		if(empty($ntity)){$entity=$this->getEntity();}
		if(empty($operation)){$operation="countclasseenrol";}
		
		$wsql="";
		if($isclasseactive){
			 $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->activefilter($fparam);
		}
		if(!empty($statusshortname)){$wsql.=" AND s.shortname=:statusshortname ";}
		if(!empty($roleshortname)){$wsql.=" AND r.shortname=:roleshortname ";}
		if(!empty($classetypeshortname)){$wsql.=" AND clt.shortname=:classetypeshortname ";}
		if(!empty($disciplinedtype)){$wsql.=" AND d.dtype=:disciplinedtype ";}
		if(!empty($offershortname)){$wsql.=" AND f.shortname=:offershortname ";}
		
		$sql="SELECT COUNT(o.id) AS countrecord FROM BadiuAmsEnrolBundle:AmsEnrolClasse o JOIN o.statusid s JOIN o.roleid r  JOIN o.classeid cl LEFT JOIN cl.statusid cls LEFT JOIN cl.typeid clt JOIN cl.odisciplineid od JOIN od.disciplineid d LEFT JOIN d.categoryid dct JOIN od.offerid f WHERE  o.entity=:entity AND o.deleted=:deleted  AND o.timestart >=:ddate1 AND o.timestart <=:ddate2  $wsql ";
		
		$data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
		$query->setParameter('deleted', 0);

		$query->setParameter('ddate1',$ddate1);
		$query->setParameter('ddate2',$ddate2);
		
		if(!empty($statusshortname)){$query->setParameter('statusshortname', $statusshortname);}
		if(!empty($roleshortname)){$query->setParameter('roleshortname', $roleshortname);}
		if(!empty($classetypeshortname)){$query->setParameter('classetypeshortname', $classetypeshortname);}
		if(!empty($disciplinedtype)){$query->setParameter('disciplinedtype', $disciplinedtype);}
		if(!empty($offershortname)){$query->setParameter('offershortname',$offershortname);}
		
		if($isclasseactive){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		}
		
		$result= $query->getOneOrNullResult();
		
		$countrecord=$this->getUtildata()->getVaueOfArray($result,'countrecord');
		$dresult[$year]=$countrecord;
		return $dresult;
	}
	 
	 function getMinMaxYear($param){
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		$statusshortname=$this->getUtildata()->getVaueOfArray($param,'statusshortname');
		$roleshortname=$this->getUtildata()->getVaueOfArray($param,'roleshortname');
		$classetypeshortname=$this->getUtildata()->getVaueOfArray($param,'classetypeshortname');
		$disciplinedtype=$this->getUtildata()->getVaueOfArray($param,'disciplinedtype');
		$offershortname=$this->getUtildata()->getVaueOfArray($param,'offershortname');
		$isclasseactive=$this->getUtildata()->getVaueOfArray($param,'isclasseactive');
		
		
		if(empty($ntity)){$entity=$this->getEntity();}
		
		if(empty($operation)){$operation="countclasseenrol";}
		
		$wsql="";
		if($isclasseactive){
			 $fparam=array('_classealias'=>'cl','_classestatusalias'=>'cls');
			$classedefaultsqlfilter = $this->getContainer()->get('badiu.ams.offer.classe.defaultsqlfilter');
			$wsql.=$classedefaultsqlfilter->activefilter($fparam);
		}
		
		$timenullcontrol = new \DateTime();
		$timenullcontrol->setDate(1970, 1, 1);
		
		if(!empty($statusshortname)){$wsql.=" AND s.shortname=:statusshortname ";}
		if(!empty($roleshortname)){$wsql.=" AND r.shortname=:roleshortname ";}
		if(!empty($classetypeshortname)){$wsql.=" AND clt.shortname=:classetypeshortname ";}
		if(!empty($disciplinedtype)){$wsql.=" AND d.dtype=:disciplinedtype ";}
		if(!empty($offershortname)){$wsql.=" AND f.shortname=:offershortname ";}
	
		$sql="SELECT MIN(o.timestart) AS firstdate, MAX(o.timestart) AS lastdate  FROM BadiuAmsEnrolBundle:AmsEnrolClasse o JOIN o.statusid s JOIN o.roleid r  JOIN o.classeid cl LEFT JOIN cl.statusid cls LEFT JOIN cl.typeid clt JOIN cl.odisciplineid od JOIN od.disciplineid d LEFT JOIN d.categoryid dct JOIN od.offerid f WHERE  o.entity=:entity  AND o.timestart > :timenullcontrol AND o.deleted=:deleted $wsql ";
		
		$data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		 
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
		$query->setParameter('deleted', 0);
		$query->setParameter('timenullcontrol', $timenullcontrol);	
		
		if(!empty($statusshortname)){$query->setParameter('statusshortname', $statusshortname);}
		if(!empty($roleshortname)){$query->setParameter('roleshortname', $roleshortname);}
		if(!empty($classetypeshortname)){$query->setParameter('classetypeshortname', $classetypeshortname);}
		if(!empty($disciplinedtype)){$query->setParameter('disciplinedtype', $disciplinedtype);}
		if(!empty($offershortname)){$query->setParameter('offershortname',$offershortname);}
		
		if($isclasseactive){
			$tnow=new \DateTime();
		    $query->setParameter('timenow1', $tnow);
		    $query->setParameter('timenow2', $tnow);
		}
		
		
		$result= $query->getOneOrNullResult();
		
		return $result;
	}
}
