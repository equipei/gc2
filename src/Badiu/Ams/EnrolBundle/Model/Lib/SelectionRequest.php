<?php
namespace Badiu\Ams\EnrolBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class SelectionRequest extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }
 

    public function exec($param) {
         $entity= $this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 
		 $defaultstatus=$this->getUtildata()->getVaueOfArray($param, 'defaultstatus');
		 $defaultrole=$this->getUtildata()->getVaueOfArray($param, 'defaultrole');
		 
		 if(empty($defaultstatus)){$defaultstatus="active";}
		 if(empty($defaultrole)){$defaultrole="student";}
		 
		 if($modulekey=='badiu.tms.offer.classe' || $modulekey=='badiu.tms.offer.discipline' || $modulekey=='badiu.tms.offer.offer' ){
			$defaultstatus="activeenrol";
			$defaultrole="studentcorporate";
		}
		  //date enrol
		 $mdltimestart=0;
		 $mdltimeend=0;
		 $timestart=new \DateTime();
		 $timeend=null;
		 $etparam=array('modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance);
		 $enroltime=$this->getContainer()->get('badiu.ams.enrol.lib.time');
		 $resulenroltime=$enroltime->getConfigPeriodValidate($etparam);
		
		if(!empty($resulenroltime)){
			$mdltimestart=$resulenroltime->timestarttp;
			$mdltimeend=$resulenroltime->timeendtp;
			$timestart=$resulenroltime->timestart;
			$timeend=$resulenroltime->timeend;
		}
		
		$dconfig=null;
		$statusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,$defaultstatus);
		$roleid=$this->getContainer()->get('badiu.ams.role.role.data')->getIdByShortname($entity,$defaultrole);
				
		//change to use enrolclassemage
		 if($modulekey=='badiu.ams.offer.classe' || $modulekey=='badiu.tms.offer.classe'){
			$classelib=$this->getContainer()->get('badiu.ams.enrol.classe.lib');
			$faddparam=array('classeid'=>$moduleinstance,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid,'timestart'=>$timestart,'timeend'=>$timeend);
			$classelib->add($faddparam);
			$frplparam=array('classeid'=>$moduleinstance,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid);
			$classelib->replicate($frplparam);
		
		 
		 }else  if($modulekey=='badiu.ams.offer.discipline' || $modulekey=='badiu.tms.offer.discipline'){
			$disciplinelib=$this->getContainer()->get('badiu.ams.enrol.discipline.lib');
			$faddparam=array('odisciplineid'=>$moduleinstance,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid,'timestart'=>$timestart,'timeend'=>$timeend);
			$disciplinelib->add($faddparam);
			$frplparam=array('odisciplineid'=>$moduleinstance,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid);
			$disciplinelib->replicate($frplparam);
			
		 }else  if($modulekey=='badiu.ams.offer.offer' || $modulekey=='badiu.tms.offer.offer'){
			
			 $offerlib=$this->getContainer()->get('badiu.ams.enrol.offer.lib');
			 $faddparam=array('offerid'=>$moduleinstance,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid,'timestart'=>$timestart,'timeend'=>$timeend);
			 $offerlib->add($faddparam);
			 $frplparam=array('offerid'=>$moduleinstance,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid);
			 $offerlib->replicate($frplparam);
		 }
    }


 public function getUrl($param) {
	
         $entity= $this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		 
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 
		 $defaultstatus=$this->getUtildata()->getVaueOfArray($param, 'defaultstatus');
		 $defaultrole=$this->getUtildata()->getVaueOfArray($param, 'defaultrole');
		 
		 if(empty($defaultstatus)){$defaultstatus="active";}
		 if(empty($defaultrole)){$defaultrole="student";}
		 
		 $moduledata=$this->getContainer()->get($modulekey.'.data');
		 $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
		 
		 //get module dto
		 $fparamdata=array('id'=>$moduleinstance);
		 $moduledto=$moduledata->getGlobalColumnsValue('o.dconfig,o.lmsaccesstimestart,o.lmsaccesstimeend',$fparamdata);
		 
		 
		 //get enrol dto
		 $enroldto=null;
		 $odisciplinedto=null;
		 $statusid=$this->getContainer()->get('badiu.ams.enrol.status.data')->getIdByShortname($entity,$defaultstatus);
		 $roleid=$this->getContainer()->get('badiu.ams.role.role.data')->getIdByShortname($entity,$defaultrole);
			
		//change to use enrolclassemage
		 if($modulekey=='badiu.ams.offer.classe' || $modulekey=='badiu.tms.offer.classe'){
			$mdata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
			$enrolid=$mdata->getEnrolid($entity,$moduleinstance,$userid,$roleid);
			$fparam=array('id'=>$enrolid);
			$enroldto=$mdata->getGlobalColumnsValue('o.lmsaccesstimestart,o.lmsaccesstimeend',$fparam);
				
			$classedata=$this->getContainer()->get('badiu.ams.offer.classe.data');
			$disciplinedto=$classedata->getNameParent($moduleinstance);
			$odisciplineid=$this->getUtildata()->getVaueOfArray($disciplinedto, 'odisciplineid');
			
			$disciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			$odisciplinedto=$disciplinedata->getGlobalColumnsValue('o.lmsaccesstimestart,o.lmsaccesstimeend',array('id'=>$odisciplineid));
		 }else  if($modulekey=='badiu.ams.offer.discipline' || $modulekey=='badiu.tms.offer.discipline'){
			$mdata=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
			$enrolid=$mdata->getEnrolid($entity,$moduleinstance,$userid,$roleid);
			$fparam=array('id'=>$enrolid);
			$enroldto=$mdata->getGlobalColumnsValue('o.lmsaccesstimestart,o.lmsaccesstimeend',$fparam);
				 
				
		 }else  if($modulekey=='badiu.ams.offer.offer' || $modulekey=='badiu.tms.offer.offer'){
			$mdata=$this->getContainer()->get('badiu.ams.enrol.offer.data');
			$enrolid=$mdata->getEnrolid($entity,$moduleinstance,$userid,$roleid);
			$fparam=array('id'=>$enrolid);
			$enroldto=$mdata->getGlobalColumnsValue('o.lmsaccesstimestart,o.lmsaccesstimeend',$fparam);
		 }
		 
		 $accesparam=array();
		 $accesparam['dconfig']=$this->getUtildata()->getVaueOfArray($moduledto,'dconfig');
		 $accesparam['classelmsaccesstimestart']=$this->getUtildata()->getVaueOfArray($moduledto,'lmsaccesstimestart');
		 $accesparam['classelmsaccesstimeend']=$this->getUtildata()->getVaueOfArray($moduledto,'lmsaccesstimeend');
		 
		 $accesparam['odisciplinelmsaccesstimestart']=$this->getUtildata()->getVaueOfArray($odisciplinedto,'lmsaccesstimestart');
		 $accesparam['odisciplinelmsaccesstimeend']=$this->getUtildata()->getVaueOfArray($odisciplinedto,'lmsaccesstimeend');
		 
		 $accesparam['enrollmsaccesstimestart']=$this->getUtildata()->getVaueOfArray($enroldto,'lmsaccesstimestart');
		 $accesparam['enrollmsaccesstimeend']=$this->getUtildata()->getVaueOfArray($enroldto,'lmsaccesstimeend');
		 
		 $lmscoremoodleformat=$this->getContainer()->get('badiu.ams.core.lmsmoodle.format');
		 $url=$lmscoremoodleformat->url($accesparam);
		 
		return  $url;
    }

/*
 public function getUrl($param) {
         $entity= $this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		 
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 
		 $moduledata=$this->getContainer()->get($modulekey.'.data');
		 $utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
		 
		 $fparamdata=array('id'=>$moduleinstance);
		 $dconfig=$moduledata->getGlobalColumnValue('dconfig',$fparamdata);
		 
		 
		 $dconfig = $this->getJson()->decode($dconfig, true); 
		 
		 $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		 $serviceid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.sserviceid',true);
		 $lmssynclevel=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmssynclevel',true);
		 $lmscoursecatid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscoursecatid',true);
		 $lmscourseid=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.lmscourseid',true);				
	     $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		 $status=$this->getUtildata()->getVaueOfArray($dconfig,'lmsintegration.status',true);
		$url=null;
		if(($lmssynclevel=='course' || $lmssynclevel=='group') &&  $lmscourseid > 0 && $serviceid > 0){
			$urltarget="/course/view.php?id=$lmscourseid";
			$url=$utilapp->getUrlByRoute('badiu.system.core.service.process',array('_service'=>'badiu.moodle.core.lib.remoteaccess','_function'=>'remoteAuth','_serviceid'=>$serviceid,'_urltarget'=>$urltarget));
		}else if($lmssynclevel=='coursecat' && $lmscoursecatid > 0){
			$urltarget="/course/index.php?categoryid=$lmscoursecatid";
			$url=$utilapp->getUrlByRoute('badiu.system.core.service.process',array('_service'=>'badiu.moodle.core.lib.remoteaccess','_function'=>'remoteAuth','_serviceid'=>$serviceid,'_urltarget'=>$urltarget));
		}
		return  $url;
    }*/
}
