<?php
namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ReplicateConfig  extends BadiuModelLib {

    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	 function getConfig($param) {
		$result=new \stdClass();
		$result->enrolreplicate=null;
		$result->enrolreplicateupadate=null;
		
		
		$modulekey =$this->enroldate=$this->getUtildata()->getVaueOfArray($param, 'modulekey');
		$moduleinstance= $this->enroldate=$this->getUtildata()->getVaueOfArray($param, 'moduleinstance');
		$entity =$this->enroldate=$this->getUtildata()->getVaueOfArray($param, 'entity');
		if(empty($entity)){$entity=$this->getEntity();}
		
		if(empty($modulekey)){return null;}
		if(empty($moduleinstance)){return null;}
		
		$moduledatakey=$modulekey.'.data';
		$moduledata=null;
		if($this->getContainer()->has($moduledatakey)){
			$moduledata=$this->getContainer()->get($moduledatakey);
		}else{return null;}
		$fparam=array('id'=>$moduleinstance,'entity'=>$entity);
		
		$enrolreplicateconfig=$moduledata->getGlobalColumnsValue('o.enrolreplicate,o.enrolreplicateupadate',$fparam);
		$result->enrolreplicate=$this->getUtildata()->getVaueOfArray($enrolreplicateconfig,'enrolreplicate');
		$result->enrolreplicateupadate=$this->getUtildata()->getVaueOfArray($enrolreplicateconfig,'enrolreplicateupadate');
		
		return $result;
		 
	 }
    
	function changeParamOnAddForm($param,$formparam){
		$result=$this->getConfig($param);
		//$badiutdate=$this->getContainer()->get('badiu.system.core.lib.form.castdate');
		//$timestart=$badiutdate->convertToForm($resulenroltime->timestart);
		//$timeend=$badiutdate->convertToForm($resulenroltime->timeend);
		
		$formparam['enrolreplicate']=$result->enrolreplicate;
		$formparam['enrolreplicateupadate']=$result->enrolreplicateupadate;
		return $formparam;
	}
}
