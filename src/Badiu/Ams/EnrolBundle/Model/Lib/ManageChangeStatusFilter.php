<?php

namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ManageChangeStatusFilter extends BadiuModelLib {

    function __construct(Container $container) {
                parent::__construct($container);
           
       }
  
	
	 public function check($param) {
		
		 $minprogress=$this->getUtildata()->getVaueOfArray($param,'_filter.minprogress',true);
		 $minfinalgrade=$this->getUtildata()->getVaueOfArray($param,'_filter.minfinalgrade',true);
		 $classeid =$this->getUtildata()->getVaueOfArray($param,'classeid');
		 $userid =$this->getUtildata()->getVaueOfArray($param,'userid');
		
		 if(!empty($minfinalgrade) && is_numeric($minfinalgrade)){
			$fparam=array('moduleinstance'=>$classeid,'modulekey'=>'badiu.ams.offer.classe','itemtype'=>'finalgrade','userid'=>$userid,'grade'=>$minfinalgrade); 
			$isgetcriteria=$this->hasGrade($fparam);
			if($isgetcriteria){return true;}
		 }
		 if(!empty($minprogress) && is_numeric($minprogress)){
			$fparam=array('moduleinstance'=>$classeid,'modulekey'=>'badiu.ams.offer.classe','itemtype'=>'progress','userid'=>$userid,'grade'=>$minprogress); 
			$isgetcriteria=$this->hasGrade($fparam);
			if($isgetcriteria){return true;}
		 }
		 return false;
	 }
	 
	 public function hasGrade($param) {
		 
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $itemtype=$this->getUtildata()->getVaueOfArray($param,'itemtype');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 $grade=$this->getUtildata()->getVaueOfArray($param,'grade');
		 $result=null;
		 
		 $data=$this->getContainer()->get('badiu.admin.grade.grade.data');
		 $sql="SELECT COUNT(g.id) AS countrecord FROM BadiuAdminGradeBundle:AdminGrade g JOIN g.itemid i WHERE i.moduleinstance=:moduleinstance AND i.modulekey=:modulekey AND i.itemtype=:itemtype AND g.userid=:userid AND g.grade >= :grade ";
		 $query = $data->getEm()->createQuery($sql);
		 $query->setParameter('moduleinstance',$moduleinstance );
		 $query->setParameter('modulekey',$modulekey );
		 $query->setParameter('itemtype',$itemtype );
		 $query->setParameter('userid',$userid );
		 $query->setParameter('grade',$grade );
		 $result= $query->getOneOrNullResult();
         if(isset($result['countrecord'])){ $result=$result['countrecord'];}
         else {return null;}
		 return $result;
	 }
	
	
}
