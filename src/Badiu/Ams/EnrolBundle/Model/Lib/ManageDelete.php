<?php
namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ManageDelete    extends BadiuModelLib{

    function __construct(Container $container) {
                parent::__construct($container);
       } 
  

	 public function add($param) {
		 $dto=$this->copyData($param);
		 $result=$this->addLog($dto);
		 return $result;
	 }
	 
	 public function copyData($param) {
		 $data=null;
		 $modulekey= $this->getUtildata()->getVaueOfArray($param, 'modulekey');
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param, 'moduleinstance');
		 if(empty($moduleinstance)){$moduleinstance=$this->getUtildata()->getVaueOfArray($param, 'id');}
		 
		  if($modulekey=='badiu.ams.enrol.classe' || $modulekey=='badiu.tms.enrol.classe'){$data='badiu.ams.enrol.classe.data';}
		  else if($modulekey=='badiu.ams.enrol.discipline' || $modulekey=='badiu.tms.enrol.discipline'){$data='badiu.ams.enrol.discipline.data';}
		  else if($modulekey=='badiu.ams.enrol.offer' || $modulekey=='badiu.tms.enrol.offer'){$data='badiu.ams.enrol.offer.data';}
		  if(empty($data)){return null;}
		  if(empty($moduleinstance)){return null;}
		   
		  $data= $this->getContainer()->get($data);
		  $dtoob= $data->findById($moduleinstance);
		  $objectutil=$this->getContainer()->get('badiu.system.core.lib.util.objectutil');
          $dto= $objectutil->castEntityToArrayForForm($dtoob);
		
		$iparam=array();
		$iparam['entity']=$this->getUtildata()->getVaueOfArray($dto, 'entity');
		$iparam['modulekey']=$modulekey;
		$iparam['moduleinstance']=$moduleinstance;
		$iparam['userid']=$this->getUtildata()->getVaueOfArray($dto, 'userid');
		$iparam['roleid']=$this->getUtildata()->getVaueOfArray($dto, 'roleid');
		$iparam['statusid']=$this->getUtildata()->getVaueOfArray($dto, 'statusid');
		$iparam['statusinfo']=$this->getUtildata()->getVaueOfArray($dto, 'statusinfo');
		$iparam['timestart']=$dtoob->getTimestart();
		$iparam['timeend']=$dtoob->getTimeend();
		$iparam['timefinish']=$dtoob->getTimefinish();
		$iparam['lmsaccesstimestart']=$dtoob->getLmsaccesstimestart();
		$iparam['lmsaccesstimeend']=$dtoob->getLmsaccesstimeend();
		$iparam['enrolreplicate']=$this->getUtildata()->getVaueOfArray($dto, 'enrolreplicate');
		$iparam['enrolreplicateupadate']=$this->getUtildata()->getVaueOfArray($dto, 'enrolreplicateupadate');
		$iparam['lmssync']=$this->getUtildata()->getVaueOfArray($dto, 'lmssync');
		$iparam['idnumber']=$this->getUtildata()->getVaueOfArray($dto, 'idnumber');
		$iparam['marker']=$this->getUtildata()->getVaueOfArray($dto, 'marker');
		$iparam['description']=$this->getUtildata()->getVaueOfArray($dto, 'description');
		$iparam['param']=$this->getUtildata()->getVaueOfArray($dto, 'param');
		$iparam['timecreatedclone']=$dtoob->getTimecreated();
		$iparam['timemodifiedclone']=$dtoob->getTimemodified();
		$iparam['timecreated']=new \DateTime();
		
		
		return $iparam;
	 }
	 
	 public function addLog($param) {
		 if(empty($param)){return null;}
		 
		 $logdata=$this->getContainer()->get('badiu.ams.enrol.deleted.data');
		 $result=$logdata->insertNativeSql($param,false);
		 return $result;
	 }
}
