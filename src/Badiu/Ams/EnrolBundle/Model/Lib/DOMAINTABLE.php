<?php
namespace Badiu\Ams\CoreBundle\Model\Lib\Moodle;
class DOMAINTABLE {


    public static  $REQUEST_ACCEPT   ="accept";
    public static  $REQUEST_DENIED   ="danied";
    
	public static  $ERROR_TOKEN_EMPTY ="param.token.is.empty";
	public static  $ERROR_TOKEN_NOT_VALID ="param.token.is.not.valid";
	
	public static  $ERROR_SERVICE_EMPTY ="param.service.is.empty";
	public static  $ERROR_SERVICE_NOT_VALID ="param.service.is.not.valid";
	
	public static  $ERROR_USER_EMPTY ="param.user.is.empty";
	public static  $ERROR_USER_NOT_VALID ="param.user.is.not.valid";
	
	public static  $ERROR_COURSE_EMPTY ="param.course.is.empty";
	public static  $ERROR_COURSE_NOT_VALID ="param.course.is.not.valid";
	
}
?>