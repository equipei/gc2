<?php

namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class Classe extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
	public function add($classid,$userid,$roleid,$statusid,$timestart=null,$timeend=null,$marker=null,$makerupdateforce=false) {
		if(empty($classid)){return 0;}
		if(!is_numeric($classid)){return 0;} 
		//check if exist
		$data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		$exist=$data->existEnrol($this->getEntity(),$classid,$userid,$roleid);
	 
		//add 
		if(!$exist){
			$param=array();
			$param['classeid']=$classid;
			$param['userid']=$userid;
			$param['roleid']=$roleid;
			$param['statusid']=$statusid;
			$param['timestart']=$timestart;
			$param['timeend']=$timeend;
			$param['marker']=$marker;
			$result=$data->insertNativeSql($param);
			return $result;
		}else{
			if($makerupdateforce){
				$enrolid=$data->getEnrolid($this->getEntity(),$classid,$userid,$roleid);
				if(empty($enrolid)){return -2;}
				$paramue=array('id'=>$enrolid,'marker'=>$marker);
				$r= $data->updateNativeSql(	$paramue,false);
			}
			return -1;
		}
		
	}
	
	public function getUser($id) {
		$data=$this->getContainer()->get('badiu.system.user.user.data');
		$dto=$data->findById($id);
		return $dto;
	}
	
	public function getStatus($id) {
		$data=$this->getContainer()->get('badiu.ams.enrol.status.data');
		$dto=$data->findById($id);
		return $dto;
	}
	
	public function getRole($id) {
		$data=$this->getContainer()->get('badiu.ams.role.role.data');
		$dto=$data->findById($id);
		return $dto;
	}
	public function getOdiscipline($id) {
		$data=$this->getContainer()->get('badiu.ams.offer.discipline.data');
		$dto=$data->findById($id);
		return $dto;
	}
	

}
