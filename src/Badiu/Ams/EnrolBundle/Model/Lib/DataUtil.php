<?php

namespace Badiu\Ams\EnrolBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class DataUtil extends BadiuModelLib {
private $permission;
     function __construct(Container $container) {
        parent::__construct($container);
		$this->permission=$this->getContainer()->get('badiu.system.access.permission');
       
    }
	#system/service/process?_service=badiu.ams.enrol.lib.datautil&_function=changeSelectionDefaultstatus&currentdefaultstatus=preregistrationenrol&newdefaultstatus=activeenrol
public function changeSelectionDefaultstatus() {
	  
	  $perm= $this->permission->has_access('badiu.admin.selection.project.',$this->getSessionhashkey());
	  if(!$perm){return null;}
	
	  $currentdefaultstatus=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('currentdefaultstatus');
	  $newdefaultstatus=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('newdefaultstatus');
	   if(empty($currentdefaultstatus)){return null;}
	   if(empty($newdefaultstatus)){return null;}
	
 	  $data=$this->getContainer()->get('badiu.admin.selection.project.data');
	  $fparam=array('entity'=>$this->getEntity(),'defaultstatus'=>$currentdefaultstatus);
	  $list=$data->getGlobalColumnsValues('o.id',$fparam);
		$cont=0;	
		
		foreach ($list as $row) {
			  $id=$this->getUtildata()->getVaueOfArray($row,'id');
			  $uparam=array('id'=>$id,'defaultstatus'=>$newdefaultstatus);
			  $r= $data->updateNativeSql($uparam,false);
			$cont++;
		 }
	  return $cont;
    } 
     
	
	#system/service/process?_service=badiu.ams.enrol.lib.datautil&_function=changeClasseStatus&currentstatusid=0&newstatusid=0
public function changeClasseStatus() {
	  
	  $perm= $this->permission->has_access('badiu.ams.enrol.classe.',$this->getSessionhashkey());
	  if(!$perm){return null;}
	
	  $currentstatusid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('currentstatusid');
	  $newstatusid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('newstatusid');
	   if(empty($currentstatusid)){return null;}
	   if(empty($newstatusid)){return null;}
	
 	  $data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
	  $fparam=array('entity'=>$this->getEntity(),'statusid'=>$currentstatusid);
	  $list=$data->getGlobalColumnsValues('o.id',$fparam);
		$cont=0;	
		
		foreach ($list as $row) {
			  $id=$this->getUtildata()->getVaueOfArray($row,'id');
			  $uparam=array('id'=>$id,'statusid'=>$newstatusid);
			  $r= $data->updateNativeSql($uparam,false);
			$cont++;
		 }
	  return $cont;
    }
	#system/service/process?_service=badiu.ams.enrol.lib.datautil&_function=fillClasseTimestartWithTimecreated
	public function fillClasseTimestartWithTimecreated() {
	  
	  $perm= $this->permission->has_access('badiu.ams.enrol.classe.',$this->getSessionhashkey());
	  if(!$perm){return 'you have no permission';}
	  $entity=$this->getEntity();
	  
	
 	  $data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
	 
	 $sql="SELECT o.id,o.timecreated  FROM ".$data->getBundleEntity()." o WHERE o.entity=:entity AND o.timestart IS NULL";
	 $query =   $data->getEm()->createQuery($sql);
	 $query->setParameter('entity',$entity);
	 $list= $query->getResult();
	
		$cont=0;	
		
		foreach ($list as $row) {
			  $id=$this->getUtildata()->getVaueOfArray($row,'id');
			  $timecreated=$this->getUtildata()->getVaueOfArray($row,'timecreated');
			  $uparam=array('id'=>$id,'timestart'=>$timecreated,'timemodified'=> new \DateTime());
			  $r= $data->updateNativeSql($uparam,false);
			$cont++;
		 }
	  return $cont;
    }

}
