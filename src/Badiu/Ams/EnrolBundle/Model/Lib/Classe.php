<?php

namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class Classe extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	
	//public function add($classeid,$userid,$roleid,$statusid,$timestart=null,$timeend=null,$marker=null,$makerupdateforce=false) {
	public function add($param) {
		$classeid=$this->getUtildata()->getVaueOfArray($param,'classeid');
		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		$roleid=$this->getUtildata()->getVaueOfArray($param,'roleid');
		$statusid=$this->getUtildata()->getVaueOfArray($param,'statusid');
		$timestart=$this->getUtildata()->getVaueOfArray($param,'timestart');
		$timeend=$this->getUtildata()->getVaueOfArray($param,'timeend');
		$marker=$this->getUtildata()->getVaueOfArray($param,'marker');
		$makerupdateforce=$this->getUtildata()->getVaueOfArray($param,'makerupdateforce');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		if(empty($entity)){$entity=$this->getEntity();}
		
		if(empty($classeid)){return 0;}
		if(!is_numeric($classeid)){return 0;} 
		//check if exist
		$data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		$exist=$data->existEnrol($entity,$classeid,$userid,$roleid);
	 
		//add 
		if(!$exist){
			if(empty($timestart)){$timestart=new \DateTime();}
			$param=array();
			$param['classeid']=$classeid;
			$param['userid']=$userid;
			$param['roleid']=$roleid;
			$param['statusid']=$statusid;
			$param['timestart']=$timestart;
			$param['timeend']=$timeend;
			$param['marker']=$marker;
			$result=$data->insertNativeSql($param);
			
			$accessmanagelib=$this->getContainer()->get('badiu.system.access.manage.lib');
			$amparam=array('userid'=>$userid,'roleid'=>$roleid,'entity'=>$entity);
			$accessmanagelib->addUserRole($amparam);
		
			return $result;
		}else{
			if($makerupdateforce){
				$enrolid=$data->getEnrolid($this->getEntity(),$classeid,$userid,$roleid);
				if(empty($enrolid)){return -2;}
				if(empty($timestart)){
					$timestart=$data->getGlobalColumnValue('timecreated',array('id'=>$enrolid));
				}
				$paramue=array('id'=>$enrolid,'marker'=>$marker,'statusid'=>$statusid,'timestart'=>$timestart,'timeend'=>$timeend,'timemodified'=>new \DateTime());
				$r= $data->updateNativeSql(	$paramue,false);
				$accessmanagelib=$this->getContainer()->get('badiu.system.access.manage.lib');
				$amparam=array('userid'=>$userid,'roleid'=>$roleid,'entity'=>$entity);
				$accessmanagelib->addUserRole($amparam);
			}
			return -1;
		}
		
	}
	
	public function replicate($param) {
		 $enrolid=$this->getUtildata()->getVaueOfArray($param,'id');
		 $classeid=$this->getUtildata()->getVaueOfArray($param,'classeid');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 $roleid=$this->getUtildata()->getVaueOfArray($param,'roleid');
		 $statusid=$this->getUtildata()->getVaueOfArray($param,'statusid');
		 $isedit=$this->getUtildata()->getVaueOfArray($param,'isedit');
		 $enrolreplicate=$this->getUtildata()->getVaueOfArray($param,'enrolreplicate');
		 $enrolreplicateupadate=$this->getUtildata()->getVaueOfArray($param,'enrolreplicateupadate');
		 $getconfig=0;
		 $timestart=$this->getUtildata()->getVaueOfArray($param,'timestart');
		 $timeend=$this->getUtildata()->getVaueOfArray($param,'timeend');
		 $mdltimestart=0;
		 $mdltimeend=0;
		   
		 if(!array_key_exists('enrolreplicate',$param)){$getconfig++;}
		 if(!array_key_exists('enrolreplicateupadate',$param)){$getconfig++;}
		 if(!array_key_exists('timestart',$param)){$getconfig++;}
		 if(!array_key_exists('timeend',$param)){$getconfig++;}
		 $classedata=$this->getContainer()->get('badiu.ams.offer.classe.data');
		 $enrolclassedata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		
		 if($getconfig ==4){
			 $fparamenrol=null;
			if(!empty($enrolid)){
				$fparamenrol=array('id'=>$enrolid);
				$enrolreplicateconfig=$enrolclassedata->getGlobalColumnsValue('o.timestart,o.timeend,o.enrolreplicate,o.enrolreplicateupadate',$fparamenrol);
			}else if(!empty($classeid)){
				$fparamenrol=array('id'=>$classeid);
				$enrolreplicateconfig=$classedata->getGlobalColumnsValue('o.timestart,o.timeend,o.enrolreplicate,o.enrolreplicateupadate',$fparamenrol);
			}
			
			$enrolreplicate=$this->getUtildata()->getVaueOfArray($enrolreplicateconfig,'enrolreplicate');
			$enrolreplicateupadate=$this->getUtildata()->getVaueOfArray($enrolreplicateconfig,'enrolreplicateupadate');
			
			$timestart=$this->getUtildata()->getVaueOfArray($enrolreplicateconfig,'timestart');
			$timeend=$this->getUtildata()->getVaueOfArray($enrolreplicateconfig,'timeend');
			 
		
		 }
		  
		 if (is_a($timestart, 'DateTime')) {$mdltimestart=$timestart->getTimestamp();}
		 if (is_a($timeend, 'DateTime')) {$mdltimeend=$timeend->getTimestamp();}
			 
		  $replicationlms=$this->getUtildata()->existStringInTextList('lmsmoodle',$enrolreplicate);
		  $replicationdiscipline=$this->getUtildata()->existStringInTextList('discipline',$enrolreplicate);
		  $replicationoffer=$this->getUtildata()->existStringInTextList('offer',$enrolreplicate);
		  
		  $replicationupadatelms=$this->getUtildata()->existStringInTextList('lmsmoodle',$enrolreplicateupadate);
		  $replicationupadatediscipline=$this->getUtildata()->existStringInTextList('discipline',$enrolreplicateupadate);
		  $replicationupadateoffer=$this->getUtildata()->existStringInTextList('offer',$enrolreplicateupadate);
		  
		  
		 $disciplineid=$classedata->geDisciplineidById($classeid);
	
		if( $replicationdiscipline || $replicationupadatediscipline){ 
			$disciplinelib=$this->getContainer()->get('badiu.ams.enrol.discipline.lib');
			$faddparam=array('odisciplineid'=>$disciplineid,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid,'timestart'=>$timestart,'timeend'=>$timeend,'makerupdateforce'=>$replicationupadatediscipline);
		
			$disciplinelib->add($faddparam);
		}		
		
		if($replicationoffer || $replicationupadateoffer){
			$disciplinedata=$this->getContainer()->get('badiu.ams.offer.discipline.data');
			$offerid=$disciplinedata->getOfferidById($disciplineid) ;
			
			$offerlib=$this->getContainer()->get('badiu.ams.enrol.offer.lib');
			 $faddparam=array('offerid'=>$offerid,'userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid,'timestart'=>$timestart,'timeend'=>$timeend,'makerupdateforce'=>$replicationupadateoffer);
			$offerlib->add($faddparam);
			
			
		}		
		
	  if( $replicationlms || $replicationupadatelms){

		$roledata=$this->getContainer()->get('badiu.ams.role.role.data');
		$lmssynckey=$roledata->getGlobalColumnValue('lmssynckey',array('id'=>$roleid)); 
		

		$statusdata=$this->getContainer()->get('badiu.ams.enrol.status.data');
		$lmssyncenable=$statusdata->getGlobalColumnValue('lmssyncenable',array('id'=>$statusid)); 

		 $fcfgparam=array('id'=>$classeid);
		 $dconfig=$classedata->getGlobalColumnValue('dconfig',$fcfgparam);
		 $dconfig = $this->getJson()->decode($dconfig, true); 
		
		 $dconfig['enrol']['timestart']=$mdltimestart;
		 $dconfig['enrol']['timeend']=$mdltimeend;
		 $dconfig['enrol']['replicationupadatelms']=$replicationupadatelms;
		 $dconfig['enrol']['userid']=$userid;
		 $dconfig['enrol']['roleshorname']=$lmssynckey;
		 $dconfig['enrol']['enable']=$lmssyncenable;
		 
		 $managesyncmoodle= $this->getContainer()->get('badiu.ams.enrol.lib.managesyncmoodle');
		 $managesyncmoodle->processWParmConf($dconfig);
	 } 
	  
	  	
	}
	

}
