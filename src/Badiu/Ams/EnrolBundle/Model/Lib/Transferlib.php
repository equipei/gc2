<?php

namespace Badiu\Ams\EnrolBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class Transferlib extends BadiuModelLib {

    private $from = 'offer';
    private $to = 'discipline';
    private $frominstance;
    private $toinstance;
    private $enroldate;
    private $param ;
    private $list;


    function __construct(Container $container) {
        parent::__construct($container);
    }

    function exec() {
       $this->getSourceList();
      $result=$this->add();
       return $result;
         
    }
    function init($from,$frominstance,$to,$toinstance,$param) {
       
        $this->from=$from;
        $this->frominstance=$frominstance;
        $this->to=$to;
        $this->toinstance=$toinstance;
        $this->param=$param;
        $this->enroldate=$this->getUtildata()->getVaueOfArray($param, 'enroldate');
        $this->list=null;
    }
    public function getSourceList() {
        $param=$this->param;
        $wsql = "";
        
        $data="";
        if($this->from=='offer'){
            $data=$this->getContainer()->get('badiu.ams.enrol.offer.data');
             $wsql .= " AND o.offerid = $this->frominstance";
        }
        else if($this->from=='discipline'){
            $data=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
            $wsql .= " AND o.odisciplineid = $this->frominstance";
        }
        else if($this->from=='classe'){
            $data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
            $wsql .= " AND o.classeid = $this->frominstance";
        }
        else {return null;}
      
              
        $roleids = $this->getUtildata()->getVaueOfArray($param, 'roleids');
        $statusids = $this->getUtildata()->getVaueOfArray($param, 'statusids');
        $marker = $this->getUtildata()->getVaueOfArray($param, 'marker');
        
        if (!empty($roleids)) {
            $wsql .= " AND r.id IN ($roleids) ";
        }
        if (!empty($statusids)) {
            $wsql .= " AND s.id IN ($statusids) ";
        }

        if (!empty($marker)) {
            $marker=strtolower($marker);
            $wsql .= " AND LOWER(o.marker) LIKE '%".$marker."%' ";
        }
        $sql = "SELECT o.id,u.id AS userid,r.id AS roleid,s.id AS statusid,o.timestart,o.timeend,o.marker FROM " . $data->getBundleEntity() . " o JOIN o.userid u JOIN o.roleid r JOIN o.statusid s WHERE  o.entity = :entity $wsql ";
       
        $query = $data->getEm()->createQuery($sql); 
        $query->setParameter('entity', $this->getEntity());

        $result = $query->getResult();
        $this->list=$result;
        return $result;
    }

    public function add() {
        $list=$this->list;
        if(empty($list)){return null;}
        $datalib="";
        $data="";
        $param=$this->param;
        $makerupdateforce = $this->getUtildata()->getVaueOfArray($param, 'makerupdateforce');
        $statusupdateforce = $this->getUtildata()->getVaueOfArray($param, 'statusupdateforce');
		
        if($this->to=='offer'){
            $datalib=$this->getContainer()->get('badiu.ams.enrol.offer.lib');
            $data=$this->getContainer()->get('badiu.ams.enrol.offer.data');
        }
        else if($this->to=='discipline'){
            $datalib=$this->getContainer()->get('badiu.ams.enrol.discipline.lib');
            $data=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
        }
        else if($this->to=='classe'){
            $datalib=$this->getContainer()->get('badiu.ams.enrol.classe.lib');
            $data=$this->getContainer()->get('badiu.ams.enrol.classe.data');
        }
        else {return null;} 
     
        $cont=0;
		
		$accessmanagelib=$this->getContainer()->get('badiu.system.access.manage.lib');
       foreach ($list as $row) {
           $userid=$this->getUtildata()->getVaueOfArray($row, 'userid');
           $roleid=$this->getUtildata()->getVaueOfArray($row, 'roleid');
           $statusid=$this->getUtildata()->getVaueOfArray($row, 'statusid');
           $timestart=$this->getUtildata()->getVaueOfArray($row, 'timestart');
           $timeend=$this->getUtildata()->getVaueOfArray($row, 'timeend');
           $timeenrol=$this->getEnrolTime($timestart,$timeend);
           $timestart=$timeenrol->timestart;
           $timeend=$timeenrol->timeend;
           $marker = $this->getUtildata()->getVaueOfArray($row, 'marker');
           
           $faddparam=array('userid'=>$userid,'roleid'=>$roleid,'statusid'=>$statusid,'timestart'=>$timestart,'timeend'=>$timeend,'marker'=>$marker,'makerupdateforce'=>$makerupdateforce);
		    if($this->to=='offer'){$faddparam['offerid']=$this->toinstance;}
			else if($this->to=='discipline'){$faddparam['odisciplineid']=$this->toinstance;}
			 else if($this->to=='classe'){$faddparam['classeid']=$this->toinstance;}
           //review change param to array
           if(!empty($this->toinstance)){
               $result=$datalib->add($this->toinstance,$userid,$roleid,$statusid,$timestart,$timeend,$marker,$makerupdateforce);
                if($statusupdateforce &&  $result==-1){
                    $enrolid=$data->getEnrolid($this->getEntity(),$this->toinstance,$userid,$roleid);
                    if(empty($enrolid)){return -2;}
                    $paramue=array('id'=>$enrolid,'statusid'=>$statusid);
                    $result= $data->updateNativeSql($paramue,false); 
					$amparam=array('userid'=>$userid,'roleid'=>$roleid,'entity'=>$this->getEntity());
					$accessmanagelib->addUserRole($amparam);
					
                }
                if($result > 0){$cont++;}
           }
       }
      
       return $cont;
    }
    public function getEnrolTime($timestart,$timeend) {
        $result=new \stdClass;
        $result->timestart=null;
        $result->timeend=null;
         
        if($this->from=='offer' && $this->to=='discipline'){
           if($this->enroldate=='offer'){
               $result->timestart=$timestart;
               $result->timeend=$timeend;
            
           }else if($this->enroldate=='discipline'){
               $data=$this->getContainer()->get('badiu.ams.offer.discipline.data');
                $result->timestart=$data->getColumnValue($this->getEntity(),'timestart',array('id'=>$this->toinstance));
                $result->timeend=$data->getColumnValue($this->getEntity(),'timeend',array('id'=>$this->toinstance));
                
           }
        }else  if($this->from=='discipline' && $this->to=='classe'){
           if($this->enroldate=='discipline'){
               $result->timestart=$timestart;
               $result->timeend=$timeend;
            
           }else if($this->enroldate=='classe'){
               $data=$this->getContainer()->get('badiu.ams.offer.classe.data');
                $result->timestart=$data->getColumnValue($this->getEntity(),'timestart',array('id'=>$this->toinstance));
                $result->timeend=$data->getColumnValue($this->getEntity(),'timeend',array('id'=>$this->toinstance));
                
           }
        }
        return $result;
    }
    function getFrom() {
        return $this->from;
    }

    function getTo() {
        return $this->to;
    }

    function setFrom($from) {
        $this->from = $from;
    }

    function setTo($to) {
        $this->to = $to;
    }

    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function getList() {
        return $this->list;
    }

    function setList($list) {
        $this->list = $list;
    }

    function getFrominstance() {
        return $this->frominstance;
    }

    function getToinstance() {
        return $this->toinstance;
    }

    function setFrominstance($frominstance) {
        $this->frominstance = $frominstance;
    }

    function setToinstance($toinstance) {
        $this->toinstance = $toinstance;
    }
    function getEnroldate() {
        return $this->enroldate;
    }

    function setEnroldate($enroldate) {
        $this->enroldate = $enroldate;
    }



}
