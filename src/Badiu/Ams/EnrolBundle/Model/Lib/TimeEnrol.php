<?php
namespace Badiu\Ams\EnrolBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class TimeEnrol  extends BadiuModelLib {

    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	/* function teste(){
		 $param=array('modulekey'=>'badiu.ams.offer.classe','moduleinstance'=>22);
		 $rsult=$this->getConfigPeriodValidate($param);
		 print_r($rsult);exit;
	 }*/
	 function getConfigPeriodValidate($param) {
		$result=new \stdClass();
		$result->timestart=null;
		$result->timeend=null;
		$result->timestarttp=0;
		$result->timeendtp=0;
		
		$modulekey =$this->enroldate=$this->getUtildata()->getVaueOfArray($param, 'modulekey');
		$moduleinstance= $this->enroldate=$this->getUtildata()->getVaueOfArray($param, 'moduleinstance');
		$entity =$this->enroldate=$this->getUtildata()->getVaueOfArray($param, 'entity');
		if(empty($entity)){$entity=$this->getEntity();}
		
		if(empty($modulekey)){return null;}
		if(empty($moduleinstance)){return null;}
		
		$moduledatakey=$modulekey.'.data';
		$moduledata=null;
		if($this->getContainer()->has($moduledatakey)){
			$moduledata=$this->getContainer()->get($moduledatakey);
		}else{return null;}
		$fparam=array('id'=>$moduleinstance,'entity'=>$entity);
		$econfig=$moduledata->getGlobalColumnsValue('o.timestart,o.timeend,o.enroldatetype,o.enroldatedynamic',$fparam);
		
		// get datestart dateend if enroldatetype is different of modulekey 
		
		$enroldatetype=$this->getUtildata()->getVaueOfArray($econfig,'enroldatetype');
		
		
		//if($modulekey=='badiu.ams.offer.classe'){
			
		if($enroldatetype=='classe' || $enroldatetype=='discipline' || $enroldatetype=='offer'){
			$result->timestart=$this->getUtildata()->getVaueOfArray($econfig,'timestart');
			$result->timeend=$this->getUtildata()->getVaueOfArray($econfig,'timeend');
				
		}else if($enroldatetype=='dynamic'){
			$enroldatedynamic=$this->getUtildata()->getVaueOfArray($econfig,'enroldatedynamic');
			$enroldatedynamic=$this->getJson()->decode($enroldatedynamic,true);
			if(is_array($enroldatedynamic)){
				$date1=new \DateTime();
				$date1->modify("-10 minutes"); //review
				$date2=new \DateTime();
				$unittime=$this->getUtildata()->getVaueOfArray($enroldatedynamic,'unittime');
				$number=$this->getUtildata()->getVaueOfArray($enroldatedynamic,'number');
				if(empty($unittime)){return null;}
				if(empty($number)){return null;}
				$date2=$this->getContainer()->get('badiu.system.core.lib.date.periodutil')->modifyDate($date2, $unittime,$number,"+");
				$result->timestart=$date1;
				$result->timeend=$date2;
					
			}
		}
			
			
		/*}else if($modulekey=='badiu.ams.offer.discipline'){
			
		}else if($modulekey=='badiu.ams.offer.offer'){
			
		}*/
		
		if (is_a($result->timestart, 'DateTime')) {$result->timestarttp=$result->timestart->getTimestamp();}
		if (is_a($result->timeend, 'DateTime')) {$result->timeendtp=$result->timeend->getTimestamp();}
		
		return $result;
		 
	 }
    
	function changeParamOnAddForm($param,$formparam){
		$resulenroltime=$this->getConfigPeriodValidate($param);
		$badiutdate=$this->getContainer()->get('badiu.system.core.lib.form.castdate');
		$timestart=null;
		$timeend=null;
		if(is_object($resulenroltime)){
			$timestart=$badiutdate->convertToForm($resulenroltime->timestart);
			$timeend=$badiutdate->convertToForm($resulenroltime->timeend);
		}
		
		
		$formparam['timestart']=$timestart;
		$formparam['timeend']=$timeend;
		return $formparam;
	}
}
