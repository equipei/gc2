<?php

namespace Badiu\Ams\EnrolBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuCheckForm;

class ClasseCheckForm extends BadiuCheckForm {

    function __construct(Container $container) {
        parent::__construct($container);
    }
    //revisar falta deferenciar se é edição ou novo cadastro
    public function validation() {
        return null;

       $classeid=$this->getUtildata()->getVaueOfArray($this->getParam(), 'classeid');
       $userid=$this->getUtildata()->getVaueOfArray($this->getParam(), 'userid');
       $roleid=$this->getUtildata()->getVaueOfArray($this->getParam(), 'roleid');
       $entity =$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
       
       $exist=$this->getContainer()->get('badiu.ams.enrol.classe.data')->existEnrol($entity,$classeid,$userid,$roleid);
       if($exist){
       
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.duplication.message');
                $info = 'badiu.ams.enrol.classe.duplication';
                return $this->getResponse()->denied($info, $message);
       }
     
        return null;
    }

  
}
