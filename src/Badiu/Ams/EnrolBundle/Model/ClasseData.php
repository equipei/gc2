<?php

namespace Badiu\Ams\EnrolBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ClasseData   extends BadiuDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
    
      public function existEnrol($entity,$classeid,$userid,$roleid) {
		$r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.classeid=:classeid AND o.userid=:userid AND o.roleid=:roleid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('classeid',$classeid);
			$query->setParameter('userid',$userid);
			$query->setParameter('roleid',$roleid);
            $result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
	}
    
	public function checkSatus($entity,$classeid,$userid,$statusshortname) {
			$r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o JOIN o.statusid s WHERE o.entity = :entity AND o.classeid=:classeid AND o.userid=:userid AND s.shortname=:statusshortname";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('classeid',$classeid);
			$query->setParameter('userid',$userid);
			$query->setParameter('statusshortname',$statusshortname);
            $result= $query->getOneOrNullResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
	}
	
    public function getEnrolid($entity,$classeid,$userid,$roleid) {
		$r=FALSE;
            $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.classeid=:classeid AND o.userid=:userid AND o.roleid=:roleid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('classeid',$classeid);
			$query->setParameter('userid',$userid);
			$query->setParameter('roleid',$roleid);
            $result= $query->getOneOrNullResult();
            if(isset($result['id'])){$result=$result['id'];}
            else {$result=null;}
            return $result;
    }
    
    //get new user user not enrol on offer
    public function getNewUsers() {
         $classeid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
         $name=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
         
        if(empty($classeid)) return null;

        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $userData=$this->getContainer()->get('badiu.system.user.user.data');
        
        $wsql="";
        if(!empty($name)){
            $wsql=" AND LOWER(CONCAT(o.id,o.firstname,o.email))  LIKE :name ";
            $name=strtolower($name);
        }
         $sql="SELECT DISTINCT o.id,CONCAT(o.firstname,' ',o.lastname,' ',o.email) AS name FROM ".$userData->getBundleEntity()." o LEFT JOIN ".$this->getBundleEntity()." ec WITH o.id=ec.userid  WHERE  o.entity = :entity  $wsql  AND  (ec.classeid != :classeid OR ec.id IS NULL) ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('classeid',$classeid);
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(50);
        $result= $query->getResult();
        return  $result;
        }

 //get user just enroled in classe
    public function getEnroledUsers() {
         $classeid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
         $name=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
         
        if(empty($classeid)) return null;

        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $userData=$this->getContainer()->get('badiu.system.user.user.data');
        
        $wsql="";
        if(!empty($name)){
            $wsql=" AND LOWER(CONCAT(u.id,u.firstname,u.email))  LIKE :name ";
            $name=strtolower($name);
        }
         $sql="SELECT DISTINCT u.id,CONCAT(u.firstname,' ',u.lastname,' ',u.email) AS name FROM ".$this->getBundleEntity()." o JOIN o.userid u JOIN o.roleid r WHERE  o.entity = :entity  AND o.classeid=:classeid AND r.shortname=:roleshortname $wsql ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('classeid',$classeid);
		$query->setParameter('roleshortname','student');
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(50);
        $result= $query->getResult();
        return  $result;
        }		
public function getUserNameByIdOnEditAutocomplete($id) {
       $userData=$this->getContainer()->get('badiu.system.user.user.data');
       $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT CONCAT(o.firstname,' ',o.lastname,' ',o.email) AS name FROM ".$userData->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }
  
  public function getUsersid($classeid,$param=array()) {
        $wsql=$this->makeSqlWhere($param);
       $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT u.id FROM ".$this->getBundleEntity()." o JOIN o.userid u WHERE   o.entity = :entity AND o.classeid=:classeid  $wsql";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('classeid',$classeid);
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return $result;
  }
 
  
  public function getUsersidStatusenable($classeid,$param=array()) {
        $wsql=$this->makeSqlWhere($param);
       $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT u.id FROM ".$this->getBundleEntity()." o JOIN o.userid u JOIN o.statusid s WHERE   o.entity = :entity AND o.classeid=:classeid AND s.enable=:statusenable $wsql";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('classeid',$classeid);
		$query->setParameter('statusenable',1);
		$query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return $result;
  }
public function getUserFormChoice($classeid,$roleshortname='teachear',$statusshortname='active') {
     $badiuSession=$this->getContainer()->get('badiu.system.access.session');
     $sql="SELECT DISTINCT u.id,CONCAT(u.firstname,' ',u.lastname) AS name FROM ".$this->getBundleEntity()." o JOIN o.userid  u JOIN o.roleid r JOIN o.statusid s WHERE  o.entity = :entity AND o.classeid=:classeid AND r.shortname=:roleshortname  AND s.shortname=:statusshortname ";
     $query = $this->getEm()->createQuery($sql);
     $query->setParameter('entity',$badiuSession->get()->getEntity());
     $query->setParameter('classeid',$classeid);
     $query->setParameter('roleshortname',$roleshortname);
     $query->setParameter('statusshortname',$statusshortname);
     $result= $query->getResult();
   
     return $result;
}

public function getList($entity,$classeid,$param=array()) {
		$wsql=$this->makeSqlWhere($param);
        $sql="SELECT o.id,u.id AS userid,o.timestart,o.timeend,r.id AS roleid,s.id AS statusid,o.marker FROM ".$this->getBundleEntity()." o JOIN o.userid u JOIN  o.roleid r JOIN o.statusid s WHERE   o.entity = :entity AND o.classeid=:classeid  $wsql";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('classeid',$classeid);
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return $result;
  }
  
	public function geInfo($entity,$id) {
		 $sql="SELECT o.id,u.id AS userid,u.firstname,u.lastname,u.email,u.doctype,u.docnumber,u.enablealternatename,u.alternatename,o.timecreated, o.timestart,o.timeend,o.timefinish,r.id AS roleid,r.name AS rolename,r.shortname AS roleshortname,s.id AS statusid,s.name AS stutsname,s.shortname AS statusshortname,  o.marker, cl.name AS classename, cl.timestart AS classetimestart, cl.timeend AS classetimeend,cl.enablecertificate AS classeenablecertificate, cl.certificatetempleid AS classecertificatetempleid, cl.certificatecontent AS classecertificatecontent,cl.certificatetimestart AS classecertificatetimestart,cl.certificatetimeend AS classecertificatetimeend,cl.certificateupdateuser AS classecertificateupdateuser,cl.certificateupdateenrol AS  classecertificateupdateenrol,cl.certificateupdateobjectinfo  AS  classecertificateupdateobjectinfo,cl.certificateupdateobjectdata  AS  classecertificateupdateobjectdata,od.disciplinename AS odisciplinename,od.dhour AS odisciplinehour,d.name AS disciplinename, d.dhour AS disciplinehour  FROM ".$this->getBundleEntity()." o JOIN o.userid u JOIN  o.roleid r JOIN o.statusid s JOIN o.classeid cl JOIN cl.odisciplineid od JOIN od.disciplineid d WHERE   o.entity = :entity AND o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        return $result;
  } 

	public function getInstancesidByRole($userid) {
            $sql="SELECT cl.id,ar.dtype FROM ".$this->getBundleEntity()." o JOIN o.classeid cl JOIN o.roleid r JOIN BadiuAmsRoleBundle:AmsRole ar WITH r.shortname=ar.shortname  WHERE o.userid=:userid ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('userid',$userid);
            $result= $query->getResult();
			return  $result;
   }  
}
