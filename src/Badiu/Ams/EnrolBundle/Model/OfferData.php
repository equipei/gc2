<?php

namespace Badiu\Ams\EnrolBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class OfferData  extends BadiuDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
    
    //get new user user not enrol on offer
    public function getNewUsers() {
          $offerid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid'); 
         $name=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
         
        if(empty($offerid)) return null;

        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $userData=$this->getContainer()->get('badiu.system.user.user.data');
        
        $wsql="";
        if(!empty($name)){
            $wsql=" AND LOWER(CONCAT(o.id,o.firstname,o.email))  LIKE :name ";
            $name=strtolower($name);
        }
         $sql="SELECT DISTINCT o.id,CONCAT(o.firstname,' ',o.lastname,' ',o.email) AS name FROM ".$userData->getBundleEntity()." o LEFT JOIN ".$this->getBundleEntity()." ef WITH o.id=ef.userid  WHERE  o.entity = :entity  $wsql  AND  (ef.offerid != :offerid OR ef.id IS NULL) ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('offerid',$offerid);
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(50);
        $result= $query->getResult();
        return  $result;
    }
  public function getUserNameByIdOnEditAutocomplete($id) {
       $userData=$this->getContainer()->get('badiu.system.user.user.data');
       $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT CONCAT(o.firstname,' ',o.lastname,' ',o.email) AS name FROM ".$userData->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }
   public function existEnrol($entity,$offerid,$userid,$roleid) {
		$r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.offerid=:offerid AND o.userid=:userid AND o.roleid=:roleid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('offerid',$offerid);
			$query->setParameter('userid',$userid);
			$query->setParameter('roleid',$roleid);
            $result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
   }
   public function getEnrolid($entity,$offerid,$userid,$roleid) {
    $r=FALSE;
        $sql="SELECT  o.id FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.offerid=:offerid AND o.userid=:userid AND o.roleid=:roleid";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('offerid',$offerid);
        $query->setParameter('userid',$userid);
        $query->setParameter('roleid',$roleid);
        $result= $query->getOneOrNullResult();
        if(isset($result['id'])){$result=$result['id'];}
        else {$result=null;}
        return $result; 
}

public function checkSatus($entity,$offerid,$userid,$statusshortname) {
			$r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o JOIN o.statusid s WHERE o.entity = :entity AND o.offerid=:offerid AND o.userid=:userid AND s.shortname=:statusshortname";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('offerid',$offerid);
			$query->setParameter('userid',$userid);
			$query->setParameter('statusshortname',$statusshortname);
            $result= $query->getOneOrNullResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
	}
   public function getList($entity,$offerid,$roleshortname,$statusshortname) {
           $sql="SELECT  o.id,u.id AS userid,CONCAT(u.firstname, ' ',u.lastname) AS name,u.email,r.name AS rolename,r.shortname AS roleshortname,o.timestart,o.timeend  FROM ".$this->getBundleEntity()." o JOIN o.userid u JOIN o.roleid r JOIN o.statusid s  WHERE o.entity = :entity AND o.offerid=:offerid   AND r.shortname=:roleshortname AND s.shortname=:statusshortname ORDER BY u.firstname,u.lastname ";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
	    $query->setParameter('offerid',$offerid);
            $query->setParameter('roleshortname',$roleshortname);
            $query->setParameter('statusshortname',$statusshortname);
            
	    $result= $query->getResult();
           return $result;
   }

   public function getUsersid($offerid,$param=array()) {
        $wsql=$this->makeSqlWhere($param);
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT u.id FROM ".$this->getBundleEntity()." o JOIN o.userid u WHERE   o.entity = :entity AND o.offerid=:offerid  $wsql";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('offerid',$offerid);
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return $result;
}
/*
review count dhour its cound discipline for certificate gc2. This function should move to tms
*/

public function geInfo($entity,$id) {
		
		$sql="SELECT o.id,u.id AS userid,u.firstname,u.lastname,u.email,u.doctype,u.docnumber,u.enablealternatename,u.alternatename,o.timecreated, o.timestart,o.timeend,o.timefinish,r.id AS roleid,r.name AS rolename,r.shortname AS roleshortname,s.id AS statusid,s.name AS stutsname,s.shortname AS statusshortname,  o.marker, f.id AS offerid, f.name AS offername, f.timestart AS offertimestart, f.timeend AS offertimeend,f.enablecertificate AS offerenablecertificate, f.certificatetempleid AS offercertificatetempleid, f.certificatecontent AS offercertificatecontent,f.certificatetimestart AS offercertificatetimestart,f.certificatetimeend AS offercertificatetimeend,f.certificateupdateuser AS offercertificateupdateuser,f.certificateupdateenrol AS offercertificateupdateenrol,f.certificateupdateobjectinfo AS offercertificateupdateobjectinfo,f.certificateupdateobjectdata AS offercertificateupdateobjectdata,(SELECT SUM(d2.dhour) FROM BadiuAmsOfferBundle:AmsOfferDiscipline o2 JOIN o2.disciplineid d2  WHERE o2.offerid=o.offerid AND o2.deleted=0) AS offerhour,(SELECT SUM(d3.dhour) FROM BadiuAmsEnrolBundle:AmsEnrolDiscipline o3 JOIN o3.odisciplineid od3 JOIN od3.disciplineid d3 JOIN o3.statusid s3  WHERE od3.offerid=o.offerid AND o3.userid=o.userid AND s3.id= s.id AND od3.deleted=0) AS offerhourcompleted FROM ".$this->getBundleEntity()." o JOIN o.offerid f JOIN o.userid u JOIN  o.roleid r JOIN o.statusid s WHERE   o.entity = :entity AND o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        return $result;
  }  
}
