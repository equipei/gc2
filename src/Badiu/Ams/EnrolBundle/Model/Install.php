<?php

namespace Badiu\Ams\EnrolBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
       
           $result= $this->initDbStatus();
    } 

     public function initDbStatus() {
         $cont=0;
         $dtype='course';
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.ams.enrol.status.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity,'dtype'=>$dtype));
         if($exist && !$this->getForceupdate()){return 0;}
        
		$entity=$this->getEntity();
         $list=array(
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'preregistration','level'=>'offer,discipline,classe','crole'=>'student,teachear,coordenator','enable'=>0,'lmssyncenable'=>1,'enableaccesslms'=>1,'dtag'=>'','sortorder'=>1,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.preregistration'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.preregistration.abbreviation')),
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'active','level'=>'offer,discipline,classe','crole'=>'student,teachear,coordenator','enable'=>1,'lmssyncenable'=>1,'enableaccesslms'=>1,'dtag'=>'','sortorder'=>2,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.active'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.active.abbreviation')),
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'inactive','level'=>'offer,discipline,classe','crole'=>'student,teachear,coordenator','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>3,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.inactive'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.inactive.abbreviation')),
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'studying','level'=>'offer,discipline,classe','crole'=>'student','enable'=>1,'lmssyncenable'=>1,'enableaccesslms'=>1,'dtag'=>'absent,atriskofevasion','sortorder'=>4,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.studying'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.studying.abbreviation')),
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'suspended','level'=>'offer,discipline,classe','crole'=>'student,teachear,coordenator','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>5,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.suspended'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.suspended.abbreviation')),
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'locked','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>6,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.locked'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.locked.abbreviation')),
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'canceled','level'=>'offer,discipline,classe','crole'=>'student,teachear,coordenator','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'adminprocess','sortorder'=>7,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.canceled'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.canceled.abbreviation')),
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'approved','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'bygrade,byequivalence,adminprocess','sortorder'=>8,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.approved'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.approved.abbreviation')),
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'disapproved','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'bygrade,byattendance','sortorder'=>9,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.disapproved'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.disapproved.abbreviation')),
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'retired','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>10,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.retired'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.retired.abbreviation')),
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'quitter','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>11,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.quitter'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.quitter.abbreviation')),
			array('entity'=>$entity,'dtype'=>'course','shortname'=>'evaded','level'=>'offer,discipline,classe','crole'=>'student','enable'=>0,'lmssyncenable'=>0,'enableaccesslms'=>0,'dtag'=>'','sortorder'=>12,'name'=>$this->getTranslator()->trans('badiu.ams.enrol.status.evaded'),'abbreviation'=>$this->getTranslator()->trans('badiu.ams.enrol.status.evaded.abbreviation')),
			
		 ); 
		 
         
         
         foreach ($list as $param) {
			 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
			 
			 $paramedit=null;
			 if($this->getForceupdate()){
				 $paramedit= $param;
			 }
			 $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname,'dtype'=>$dtype);
			 $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
			 $r=$this->getUtildata()->getVaueOfArray($result,'id');
              if($r){$cont++;}
            }
	return $cont;
    }
         
     
}
