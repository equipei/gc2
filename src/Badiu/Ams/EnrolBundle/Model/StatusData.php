<?php

namespace Badiu\Ams\EnrolBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Ams\CoreBundle\Model\AmsDataBase;
class StatusData  extends AmsDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
	  public function getFormChoice($entity,$param=array(),$orderby="o.sortorder") {
		if(!empty($orderby)){$orderby= "ORDER BY $orderby";}
        $wsql=$this->makeSqlWhere($param);
        $sql="SELECT  o.id,o.name FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.dtype = :dtype $wsql $orderby";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('dtype','course');
        $query=$this->makeSqlFilter($query, $param);
        $result= $query->getResult();
        return  $result;
    }
	
	 public function getFormChoiceEnable() {
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
		$param=array('enable'=>1);
		$result=$this->getFormChoice($entity,$param);
        return  $result;
    }
}
