<?php

namespace Badiu\Ams\EnrolBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class OfferController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
      
    public function addOfferidToEnrol($dto,$dtoParent) {
      $dto->setOfferid($dtoParent);
        return $dto;
    }
     public function findOfferidInEnrol($dto) {

        return $dto->getOfferid()->getId();
    }
	
	
	
}


