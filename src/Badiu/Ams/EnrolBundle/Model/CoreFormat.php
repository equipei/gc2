<?php

namespace Badiu\Ams\EnrolBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class CoreFormat extends BadiuFormat{
    private  $statusdata=null;
	private  $statuslist=null;
	private  $dataoptions=null;
	
     function __construct(Container $container) {
            parent::__construct($container);
            $this->statusdata=$this->getContainer()->get('badiu.ams.enrol.status.data');
			$this->dataoptions=$this->getContainer()->get('badiu.ams.enrol.core.form.dataoptions');
       } 

              
    
    public  function ptype($data){
            $ptype=$this->getUtildata()->getVaueOfArray($data,'ptype');
			$ptype=$this->getTranslator()->trans('badiu.ams.enrol.ptype.'.$ptype);
			
		    return $ptype; 
    } 
     
 public  function level($data){
           $level=$this->getUtildata()->getVaueOfArray($data,'level');
		   $level=$this->getUtildata()->castStringToArray($level,",");
		   if(!is_array($level)){return null;}
		
		   $rvalue="";
	       $cont=0;
	   
			foreach ($level as $v) {
				$value=$this->dataoptions->getLevelLabel($v);
				$separator=", ";
				if($cont==0){$separator="";}
				$rvalue.=$separator.$value;
				$cont++;
			}
		    return $rvalue; 
    } 
 
}
