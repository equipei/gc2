<?php

namespace Badiu\Ams\EnrolBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
class DisciplineController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
      
    public function addOfferdisciplineidToEnrol($dto,$dtoParent) {
      $dto->setOdisciplineid($dtoParent);
        return $dto;
    }
     public function findOfferdisciplineidInEnrol($dto) {

        return $dto->getOdisciplineid()->getId();
    }
	
	
	
}


