<?php

namespace Badiu\Ams\EnrolBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class CoreDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
   
   
   public  function getSynclms(){
        $list=array();
        $list['manual']=$this->getSynclmsLabel('manual');
		 $list['automatic']=$this->getSynclmsLabel('automatic');
	    return  $list;
    }
	
	 public  function getSynclmsLabel($value){
        $label=null;
        if($value=='manual'){$label=$this->getTranslator()->trans('badiu.ams.enrol.conf.lmssyncenrol.manual');}
		else if($value=='automatic'){$label=$this->getTranslator()->trans('badiu.ams.enrol.conf.lmssyncenrol.automatic');}
	    return  $label;
    }
	
	
	public  function getStype(){
        $list=array();
        $list['manual']=$this->getStypeLabel('manual');
		$list['select']=$this->getStypeLabel('select');
		
	    return  $list;
    }
	
	 public  function getStypeLabel($value){
        $label=null;
        if($value=='manual'){$label=$this->getTranslator()->trans('badiu.ams.enrol.type.manual');}
		else if($value=='select'){$label=$this->getTranslator()->trans('badiu.ams.enrol.type.select');}
	    return  $label;
    }
	
	public  function getPtype(){
        $list=array();
        $list['in']=$this->getPtypeLabel('in');
		$list['out']=$this->getPtypeLabel('out');
		$list['exec']=$this->getPtypeLabel('exec');
	    return  $list;
    }
	
	 public  function getPtypeLabel($value){
        $label=null;
        if($value=='in'){$label=$this->getTranslator()->trans('badiu.ams.enrol.ptype.in');}
		else if($value=='out'){$label=$this->getTranslator()->trans('badiu.ams.enrol.ptype.out');}
		else if($value=='exec'){$label=$this->getTranslator()->trans('badiu.ams.enrol.ptype.exec');}
	    return  $label;
    }
	
	public  function getLevel(){
        $list=array();
        $list['offer']=$this->getLevelLabel('offer');
		$list['discipline']=$this->getLevelLabel('discipline');
		$list['classe']=$this->getLevelLabel('classe');
	    return  $list;
    }
	
	 public  function getLevelLabel($value){
        $label=null;
        if($value=='offer'){$label=$this->getTranslator()->trans('badiu.ams.enrol.level.offer');}
		else if($value=='discipline'){$label=$this->getTranslator()->trans('badiu.ams.enrol.level.discipline');}
		else if($value=='classe'){$label=$this->getTranslator()->trans('badiu.ams.enrol.level.classe');}
		else if($value=='lmsmoodle'){$label=$this->getTranslator()->trans('badiu.ams.enrol.level.lmsmoodle');}
	    return  $label;
    }
	
	public  function getRoles(){
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $entity=$badiuSession->get()->getEntity();
		$datarole=$this->getContainer()->get('badiu.ams.role.role.data');
        $list=$datarole->getFormChoiceShortname($entity);
        return $list;
    }
	
	public  function getDateType(){
		$list=array();
        $list['offer']=$this->getDateTypeLabel('offer');
		$list['discipline']=$this->getDateTypeLabel('discipline');
		$list['classe']=$this->getDateTypeLabel('classe');
		$list['dynamic']=$this->getDateTypeLabel('dynamic');
	    return  $list;
       
    }
	public  function getDateTypeOffer(){
		 $list=array();
        $list['offer']=$this->getDateTypeLabel('offer');
		$list['dynamic']=$this->getDateTypeLabel('dynamic');
	    return  $list;
       
    }
	public  function getDateTypeDiscipline(){
		 $list=array();
        $list['offer']=$this->getDateTypeLabel('offer');
		$list['discipline']=$this->getDateTypeLabel('discipline');
		$list['dynamic']=$this->getDateTypeLabel('dynamic');
	    return  $list;
       
    }
	public  function getDateTypeClasse(){
		 $list=array();
        $list['offer']=$this->getDateTypeLabel('offer');
		$list['discipline']=$this->getDateTypeLabel('discipline');
		$list['dynamic']=$this->getDateTypeLabel('dynamic');
	    return  $list;
       
    }
	 public  function getDateTypeLabel($value){
        $label=null;
        if($value=='offer'){$label=$this->getTranslator()->trans('badiu.ams.enrol.confenroldatetype.parentconfig.offer');}
		else if($value=='discipline'){$label=$this->getTranslator()->trans('badiu.ams.enrol.confenroldatetype.parentconfig.discipline');}
		else if($value=='classe'){$label=$this->getTranslator()->trans('badiu.ams.enrol.confenroldatetype.parentconfig.classe');}
		else if($value=='dynamic'){$label=$this->getTranslator()->trans('badiu.ams.enrol.confenroldatetype.dynamic');}
		
	    return  $label;
    }
	
	public  function getLevelReplicateOffer(){
		$list=array();
		//$list['discipline']=$this->getLevelLabel('discipline');
		//$list['classe']=$this->getLevelLabel('classe');
		$list['lmsmoodle']=$this->getLevelLabel('lmsmoodle');
	    return  $list;
    }
	public  function getLevelReplicateUpdateOffer(){
		$list=array();
		//$list['discipline']=$this->getLevelLabel('discipline');
		//$list['classe']=$this->getLevelLabel('classe');
		$list['lmsmoodle']=$this->getLevelLabel('lmsmoodle');
        return $list;
    }
	public  function getLevelReplicateDiscipline(){
		$list=array();
		$list['offer']=$this->getLevelLabel('offer');
		//$list['classe']=$this->getLevelLabel('classe');
		$list['lmsmoodle']=$this->getLevelLabel('lmsmoodle');
		return  $list;
    }
		public  function getLevelReplicateUpdateDiscipline(){
		$list=array();
        $list['offer']=$this->getLevelLabel('offer');
		//$list['classe']=$this->getLevelLabel('classe');
		$list['lmsmoodle']=$this->getLevelLabel('lmsmoodle');
	    return  $list;
    }
	public  function getLevelReplicateClasse(){
		$list=array();
        $list['offer']=$this->getLevelLabel('offer');
		$list['discipline']=$this->getLevelLabel('discipline');
		$list['lmsmoodle']=$this->getLevelLabel('lmsmoodle');
	    return  $list;
    }
	public  function getLevelReplicateUpdateClasse(){
		$list=array();
        $list['offer']=$this->getLevelLabel('offer');
		$list['discipline']=$this->getLevelLabel('discipline');
		$list['lmsmoodle']=$this->getLevelLabel('lmsmoodle');
	    return  $list;
    }
	
	//badiu.ams.enrol.selection.param.config.defaultstatus: preregistration,active
	//badiu.ams.enrol.selection.param.config.defaultrole: student
	
	public  function getDefaultStatusSelection($keyconfig='badiu.ams.enrol.selection.param.config.defaultstatus',$default='active',$dtype='course'){
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
		$entity=$badiuSession->get()->getEntity();
		$defautloptions=$badiuSession->getValue($keyconfig);
		if(empty($defautloptions)){$defautloptions=$default;}
		$defautloptions=$this->getUtildata()->castStringToArray($defautloptions) ;
		$statusdata=$this->getContainer()->get('badiu.ams.enrol.status.data');
		$fparam=array('dtype'=>$dtype,'_shortnames'=>$defautloptions);
		$list=$statusdata->getFormChoiceShortname($entity,$fparam);
		
	    return  $list;
    }
	
	public  function getDefaultRoleSelection($keyconfig='badiu.ams.enrol.selection.param.config.defaultrole',$default='student',$dtype='course'){
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
		$entity=$badiuSession->get()->getEntity();
		$defautloptions=$badiuSession->getValue($keyconfig);
		if(empty($defautloptions)){$defautloptions=$default;}
		$defautloptions=$this->getUtildata()->castStringToArray($defautloptions) ;
		$statusdata=$this->getContainer()->get('badiu.ams.role.role.data');
		$fparam=array('dtype'=>$dtype,'_shortnames'=>$defautloptions);
		$list=$statusdata->getFormChoiceShortname($entity,$fparam);
		
	    return  $list;
    }
}
