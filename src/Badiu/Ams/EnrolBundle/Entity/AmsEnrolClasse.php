<?php

namespace Badiu\Ams\EnrolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsEnrolClasse
 *  
 * @ORM\Table(name="ams_enrol_classe", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_enrol_classe_idnumber_uix", columns={"entity", "idnumber"})},
 *      indexes={@ORM\Index(name="ams_enrol_classe_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="ams_enrol_classe_classeid_ix", columns={"classeid"}),
 *              @ORM\Index(name="ams_enrol_classe_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="ams_enrol_classe_roleid_ix", columns={"roleid"}),
 *              @ORM\Index(name="ams_enrol_classe_statusid_ix", columns={"statusid"}),
 *              @ORM\Index(name="ams_enrol_classe_deleted_ix", columns={"deleted"}), 
 *              @ORM\Index(name="ams_enrol_classe_marker_ix", columns={"marker"}),
 *              @ORM\Index(name="ams_enrol_classe_lmssync_ix", columns={"lmssync"}),
 *              @ORM\Index(name="ams_enrol_classe_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsEnrolClasse
{
    /** 
     * @var integer
     * 
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;



    /**
     * @var \Badiu\Ams\OfferBundle\Entity\AmsOfferClasse
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\OfferBundle\Entity\AmsOfferClasse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="classeid", referencedColumnName="id",nullable=false)
     * })
     */
    private $classeid;

    /**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id", nullable=false)
     * })
     */
    private $userid;


    /**
     * @var \Badiu\Ams\RoleBundle\Entity\AmsRole
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\RoleBundle\Entity\AmsRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roleid", referencedColumnName="id", nullable=false)
     * })
     */
    private $roleid;

    /**
     * @var AmsEnrolStatus
     *
     * @ORM\ManyToOne(targetEntity="AmsEnrolStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id", nullable=false)
     * })
     */
    private $statusid;

	      /**
	 * @var string
	 *
	 * @ORM\Column(name="statusinfo", type="string", length=50, nullable=true)
	 */
	private $statusinfo;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */

    private $timeend;

 /**
     * @var \DateTime
     *
     * @ORM\Column(name="timefinish", type="datetime", nullable=true)
     */
     
    private $timefinish;
	
		/**
     * @var \DateTime
     *
     * @ORM\Column(name="lmsaccesstimestart", type="datetime", nullable=true)
     */
    private $lmsaccesstimestart;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="lmsaccesstimeend", type="datetime", nullable=true)
     */
    private $lmsaccesstimeend;
	
	/**
     * @var string
     *
     * @ORM\Column(name="enrolreplicate", type="string", length=255, nullable=true)
     */
    private $enrolreplicate;
	
	/**
     * @var string
     *
     * @ORM\Column(name="enrolreplicateupadate", type="string", length=255, nullable=true)
     */
    private $enrolreplicateupadate;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="lmssync", type="string", length=50, nullable=true)
     */
    private $lmssync; //syncimported | syncexported | syncbylmsplugin
	
      /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    	 
	      /**
     * @var string
     *
     * @ORM\Column(name="marker", type="string", length=255, nullable=true)
     */
    private $marker;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
  

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param int $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

 

    /**
     * @return \Badiu\Ams\OfferBundle\Entity\AmsOfferClasse
     */
    public function getClasseid()
    {
        return $this->classeid;
    }

    /**
     * @param \Badiu\Ams\OfferBundle\Entity\AmsOfferClasse $classeid
     */
    public function setClasseid($classeid)
    {
        $this->classeid = $classeid;
    }

    /**
     * @return \Badiu\System\UserBundle\Entity\SystemUser
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param \Badiu\System\UserBundle\Entity\SystemUser $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    /**
     * @return \Badiu\Ams\RoleBundle\Entity\AmsRole
     */
    public function getRoleid()
    {
        return $this->roleid;
    }

    /**
     * @param \Badiu\Ams\RoleBundle\Entity\AmsRole $roleid
     */
    public function setRoleid($roleid)
    {
        $this->roleid = $roleid;
    }

    /**
     * @return AmsEnrolStatus
     */
    public function getStatusid()
    {
        return $this->statusid;
    }

    /**
     * @param AmsEnrolStatus $statusid
     */
    public function setStatusid($statusid)
    {
        $this->statusid = $statusid;
    }

    /**
     * @return \DateTime
     */
    public function getTimestart()
    {
        return $this->timestart;
    }

    /**
     * @param \DateTime $timestart
     */
    public function setTimestart($timestart)
    {
        $this->timestart = $timestart;
    }

    /**
     * @return \DateTime
     */
    public function getTimeend()
    {
        return $this->timeend;
    }

    /**
     * @param \DateTime $timeend
     */
    public function setTimeend($timeend)
    {
        $this->timeend = $timeend;
    }

    /**
     * @return string
     */
    public function getIdnumber()
    {
        return $this->idnumber;
    }

    /**
     * @param string $idnumber
     */
    public function setIdnumber($idnumber)
    {
        $this->idnumber = $idnumber;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * @param string $param
     */
    public function setParam($param)
    {
        $this->param = $param;
    }

    /**
     * @return \DateTime
     */
    public function getTimecreated()
    {
        return $this->timecreated;
    }

    /**
     * @param \DateTime $timecreated
     */
    public function setTimecreated($timecreated)
    {
        $this->timecreated = $timecreated;
    }

    /**
     * @return \DateTime
     */
    public function getTimemodified()
    {
        return $this->timemodified;
    }

    /**
     * @param \DateTime $timemodified
     */
    public function setTimemodified($timemodified)
    {
        $this->timemodified = $timemodified;
    }

    /**
     * @return int
     */
    public function getUseridadd()
    {
        return $this->useridadd;
    }

    /**
     * @param int $useridadd
     */
    public function setUseridadd($useridadd)
    {
        $this->useridadd = $useridadd;
    }

    /**
     * @return int
     */
    public function getUseridedit()
    {
        return $this->useridedit;
    }

    /**
     * @param int $useridedit
     */
    public function setUseridedit($useridedit)
    {
        $this->useridedit = $useridedit;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param boolean $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }


    function getMarker() {
        return $this->marker;
    }
    function setMarker($marker) {
        $this->marker = $marker;
    }
	
	  function getLmssync() {
        return $this->lmssync;
    }
    function setLmssync($lmssync) {
        $this->lmssync = $lmssync;
    }
	
	
	function getStatusinfo() {
            return $this->statusinfo;
        }
	
	
        function setStatusinfo($statusinfo) {
            $this->statusinfo = $statusinfo;
        }
		
	  function getEnrolreplicate() {
        return $this->enrolreplicate;
    }
    function setEnrolreplicate($enrolreplicate) {
        $this->enrolreplicate = $enrolreplicate;
    }
	
		  function getEnrolreplicateupadate() {
        return $this->enrolreplicateupadate;
    }
    function setEnrolreplicateupadate($enrolreplicateupadate) {
        $this->enrolreplicateupadate = $enrolreplicateupadate;
    }
	

	 public function getTimefinish() {
        return $this->timefinish;
    }
	
	   public function setTimefinish($timefinish) {
        $this->timefinish = $timefinish;
    }	
	
	
    function getLmsaccesstimestart() {
        return $this->lmsaccesstimestart;
    }

    function getLmsaccesstimeend() {
        return $this->lmsaccesstimeend;
    }


 function setLmsaccesstimestart(\DateTime $lmsaccesstimestart) {
        $this->lmsaccesstimestart = $lmsaccesstimestart;
    }

    function setLmsaccesstimeend(\DateTime $lmsaccesstimeend) {
        $this->lmsaccesstimeend = $lmsaccesstimeend;
    }
	
	
}
