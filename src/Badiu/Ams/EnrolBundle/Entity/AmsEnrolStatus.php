<?php

namespace Badiu\Ams\EnrolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsEnrolStatus
 *  
 * @ORM\Table(name="ams_enrol_status", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_enrol_status_name_uix", columns={"entity","dtype","name"}), 
 *      @ORM\UniqueConstraint(name="ams_enrol_status_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="ams_enrol_status_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_enrol_status_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_enrol_status_name_ix", columns={"name"}), 
 *              @ORM\Index(name="ams_enrol_status_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="ams_enrol_status_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="ams_enrol_status_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="ams_enrol_status_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsEnrolStatus
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;

    /**
     * @var string
     *
     * @ORM\Column(name="abbreviation", type="string", length=50, nullable=true)
     */
    private $abbreviation;	
	
	/**
     * @var string
     *
     * @ORM\Column(name="dtag", type="string", length=255, nullable=true)
     */
    private $dtag;

 /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype='course'; // course | event


    /**
     * @var AmsEnrolStatusCategory
     *
     * @ORM\ManyToOne(targetEntity="AmsEnrolStatusCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;

 /**
     * @var string
     *
     * @ORM\Column(name="level", type="string", length=255, nullable=true)
     */
    private $level;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="crole", type="string", length=255, nullable=true)
     */
    private $crole;
	
	    /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="bigint", nullable=true)
     */
    private $sortorder;
	
	 /**
     * @var integer
     *
     * @ORM\Column(name="enable", type="integer", nullable=true)
     */
    private $enable;
	
		/**
     * @var string
     *
     * @ORM\Column(name="lmssyncenable", type="integer", nullable=true)
     */
    private $lmssyncenable;
	
	 /**
     * @var integer
     *
     * @ORM\Column(name="enableaccesslms", type="integer", nullable=true)
     */
    private $enableaccesslms;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
	
	        /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getShortname() {
        return $this->shortname;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }



 /**
     * @return string
     */
    public function getDtype()
    {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype)
    {
        $this->dtype = $dtype;
    }
	
	function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

function getLevel() {
        return $this->level;
    }

    function setLevel($level) {
        $this->level = $level;
    }
	
	/**
     * @return string
     */
    public function getCrole()
    {
        return $this->crole;
    }

    /**
     * @param string $crole
     */
    public function setCrole($crole)
    {
        $this->crole = $crole;
    }
	
	
	  /**
     * @return string
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * @param string $abbreviation
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;
    }
	
	    public function getCategoryid() {
        return $this->categoryid;
    }

    public function setCategoryid(AmsEnrolStatusCategory $categoryid) {
        $this->categoryid = $categoryid;
    }
	
	
		
	  /**
     * @return string
     */
    public function getDtag()
    {
        return $this->dtag;
    }

    /**
     * @param string $dtag
     */
    public function setDtag($dtag)
    {
        $this->dtag = $dtag;
    }
	
	 public function getSortorder() {
        return $this->sortorder;
    }
	
	  public function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }


/**
     * @return integer
     */
    public function getLmssyncenable() {
        return $this->lmssyncenable;
    }

    /**
     * @param integer $lmssyncenable
     */
    public function setLmssyncenable($lmssyncenable) {
        $this->lmssyncenable = $lmssyncenable;
    }
	
		/**
     * @return integer
     */
    public function getEnableaccesslms() {
        return $this->enableaccesslms;
    }

    /**
     * @param integer $enableaccesslms
     */
    public function setEnableaccesslms($enableaccesslms) {
        $this->enableaccesslms = $enableaccesslms;
    }
	
		/**
     * @return integer
     */
    public function getEnable() {
        return $this->enable;
    }

    /**
     * @param integer $enable
     */
    public function setEnable($enable) {
        $this->enable = $enable;
    }
}
