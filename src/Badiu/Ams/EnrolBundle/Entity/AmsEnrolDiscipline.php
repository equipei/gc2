<?php

namespace Badiu\Ams\EnrolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmsEnrolDiscipline
 *
 * @ORM\Table(name="ams_enrol_discipline", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="ams_enrol_discipline_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="ams_enrol_discipline_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="ams_enrol_discipline_odisciplineid_ix", columns={"odisciplineid"}),
 *              @ORM\Index(name="ams_enrol_discipline_statusid_ix", columns={"statusid"}),
 *              @ORM\Index(name="ams_enrol_discipline_roleid_ix", columns={"roleid"}),
 *              @ORM\Index(name="ams_enrol_discipline_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="ams_enrol_discipline_deleted_ix", columns={"deleted"}), 
 *              @ORM\Index(name="ams_enrol_discipline_marker_ix", columns={"marker"}),
 *              @ORM\Index(name="ams_enrol_discipline_lmssync_ix", columns={"lmssync"}), 
 *              @ORM\Index(name="ams_enrol_discipline_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AmsEnrolDiscipline
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var \Badiu\Ams\OfferBundle\Entity\AmsOfferDiscipline
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\OfferBundle\Entity\AmsOfferDiscipline")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="odisciplineid", referencedColumnName="id")
     * })
     */
    private $odisciplineid;

   

    /**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id")
     * })
     */
    private $userid;


    /**
     * @var \Badiu\Ams\RoleBundle\Entity\AmsRole
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Ams\RoleBundle\Entity\AmsRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roleid", referencedColumnName="id")
     * })
     */
    private $roleid;


    /**
     * @var AmsEnrolStatus
     *
     * @ORM\ManyToOne(targetEntity="AmsEnrolStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;
	
		      /**
	 * @var string
	 *
	 * @ORM\Column(name="statusinfo", type="string", length=50, nullable=true)
	 */
	private $statusinfo;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
     
    private $timeend;
	
	 /**
     * @var \DateTime
     *
     * @ORM\Column(name="timefinish", type="datetime", nullable=true)
     */
     
    private $timefinish;
	
		/**
     * @var \DateTime
     *
     * @ORM\Column(name="lmsaccesstimestart", type="datetime", nullable=true)
     */
    private $lmsaccesstimestart;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="lmsaccesstimeend", type="datetime", nullable=true)
     */
    private $lmsaccesstimeend;
	
	/**
     * @var string
     *
     * @ORM\Column(name="enrolreplicate", type="string", length=255, nullable=true)
     */
    private $enrolreplicate;
	
	/**
     * @var string
     *
     * @ORM\Column(name="enrolreplicateupadate", type="string", length=255, nullable=true)
     */
    private $enrolreplicateupadate;
   
/**
     * @var string
     *
     * @ORM\Column(name="lmssync", type="string", length=50, nullable=true)
     */
    private $lmssync; //syncimported | syncexported | syncbylmsplugin
	
     /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="marker", type="string", length=255, nullable=true)
     */
    private $marker;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getStatusid() {
        return $this->statusid;
    }

    public function getRoleid() {
        return $this->roleid;
    }

    public function getTimestart() {
        return $this->timestart;
    }

    public function getTimeend() {
        return $this->timeend;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }



    public function setOdisciplineid(\Badiu\Ams\OfferBundle\Entity\AmsOfferDiscipline $odisciplineid) {
        $this->odisciplineid = $odisciplineid;
    }

  public function getOdisciplineid() {
        return $this->odisciplineid;
    }
    public function setStatusid(AmsEnrolStatus $statusid) {
        $this->statusid = $statusid;
    }

    public function setRoleid(\Badiu\Ams\RoleBundle\Entity\AmsRole $roleid) {
        $this->roleid = $roleid;
    }

    public function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    public function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

       
    /**
     * @return \Badiu\System\UserBundle\Entity\SystemUser
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param \Badiu\System\UserBundle\Entity\SystemUser $userid
     */
    public function setUserid(\Badiu\System\UserBundle\Entity\SystemUser $userid)
    {
        $this->userid = $userid;
    }


    function getMarker() {
        return $this->marker;
    }
    function setMarker($marker) {
        $this->marker = $marker;
    }
  function getLmssync() {
        return $this->lmssync;
    }
    function setLmssync($lmssync) {
        $this->lmssync = $lmssync;
    }
	
	
	function getStatusinfo() {
            return $this->statusinfo;
        }
	
	
        function setStatusinfo($statusinfo) {
            $this->statusinfo = $statusinfo;
        }
		
	function getEnrolreplicate() {
        return $this->enrolreplicate;
    }
    function setEnrolreplicate($enrolreplicate) {
        $this->enrolreplicate = $enrolreplicate;
    }
	
		  function getEnrolreplicateupadate() {
        return $this->enrolreplicateupadate;
    }
    function setEnrolreplicateupadate($enrolreplicateupadate) {
        $this->enrolreplicateupadate = $enrolreplicateupadate;
    }
	
	
	
	 public function getTimefinish() {
        return $this->timefinish;
    }
	
	   public function setTimefinish($timefinish) {
        $this->timefinish = $timefinish;
    }
	
	
    function getLmsaccesstimestart() {
        return $this->lmsaccesstimestart;
    }

    function getLmsaccesstimeend() {
        return $this->lmsaccesstimeend;
    }


 function setLmsaccesstimestart(\DateTime $lmsaccesstimestart) {
        $this->lmsaccesstimestart = $lmsaccesstimestart;
    }

    function setLmsaccesstimeend(\DateTime $lmsaccesstimeend) {
        $this->lmsaccesstimeend = $lmsaccesstimeend;
    }
	
	
}
