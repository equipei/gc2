<?php

namespace Badiu\Ams\EnrolBundle\Filter;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFilter;
class StatusFilter extends BadiuFilter {
    
  /**
     * @var integer
      */
    private $categoryid;

  /**
   * @return int
   */
  public function getCategoryid()
  {
    return $this->categoryid;
  }

  /**
   * @param int $categoryid
   */
  public function setCategoryid($categoryid)
  {
    $this->categoryid = $categoryid;
  }




}
