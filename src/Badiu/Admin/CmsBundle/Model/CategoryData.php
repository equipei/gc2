<?php
namespace Badiu\Admin\CmsBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuTreeDataBase;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CategoryData extends BadiuTreeDataBase
{

    public function __construct(Container $container, $bundleEntity)
    {
        parent::__construct($container, $bundleEntity);
    }

    
 public function getTemplateidByShortname($entity,$shortname) {
    $sql="SELECT  t.id FROM ".$this->getBundleEntity()." o JOIN o.templateid t   WHERE o.entity=:entity AND o.shortname=:shortname";
    $query = $this->getEm()->createQuery($sql);
	$query->setParameter('entity',$entity);
    $query->setParameter('shortname',$shortname);
    $result= $query->getOneOrNullResult();
    
	if(!empty($result)){$result=$result['id'];}
    return  $result;
}


}
