<?php

namespace Badiu\Admin\CmsBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class ResourceDefaultDefaultSqlFilter extends BadiuSqlFilter{
    /**
     * @var object
     */
  
    function __construct(Container $container) {
            parent::__construct($container);
       }

        function exec() {
           
           return null;
        }
        
        
		function addfiltertag() {
			$wsql="";
			$gsearch=$this->getUtildata()->getVaueOfArray($this->getParam(),'gsearch');
			$widhtypeid=$this->getUtildata()->getVaueOfArray($this->getParam(),'widhtypeid');
			if(!empty($gsearch)){
				$gsearch=strtolower($gsearch);
				$gsearch="'%".$gsearch."%'"; 
				$tagwsql1=" (SELECT COUNT(tg1) FROM BadiuSystemModuleBundle:SystemModuleTag tg1 WHERE tg1.moduleinstance=o.id AND tg1.modulekey='badiu.admin.cms.resourcecontent' AND LOWER(tg1.value) LIKE $gsearch  ) > 0 ";
				$tagwsql2=" (SELECT COUNT(tg2) FROM BadiuSystemModuleBundle:SystemModuleTag tg2 WHERE tg2.moduleinstance=o.categoryid AND tg2.modulekey='badiu.admin.cms.resourcecategory'  AND LOWER(tg2.value) LIKE $gsearch ) > 0 ";
				$tagwsql3=" (SELECT COUNT(tg3) FROM BadiuSystemModuleBundle:SystemModuleTag tg3 WHERE tg3.moduleinstance=o.typeid AND tg3.modulekey='badiu.admin.cms.resourcetype'  AND LOWER(tg3.value) LIKE $gsearch ) > 0 ";
				$tagwsql4=" (SELECT COUNT(tg4) FROM BadiuSystemModuleBundle:SystemModuleTag tg4 WHERE tg4.moduleinstance=o.structureid AND tg4.modulekey='badiu.admin.cms.resourcestructure'  AND LOWER(tg4.value) LIKE $gsearch ) > 0 ";
				$wsql=" AND ( $tagwsql1 OR $tagwsql2 OR $tagwsql3 OR $tagwsql4 ) ";
			}
			
			if($widhtypeid=='1'){$wsql.=" AND o.typeid IS NOT NULL";}
			else if($widhtypeid==='0'){$wsql.=" AND o.typeid IS NULL";}
			
			return $wsql;
         }
		 
	
} 
