<?php

namespace Badiu\Admin\CmsBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuAccessFilter;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class FviewitemAccessFilter extends  BadiuAccessFilter {

	function __construct(Container $container) {
		parent::__construct($container);
	}

       public function exec(){
         $filter=true;
		 $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		 $entity=$this->getEntity();
		 
		 $rdata=$this->getContainer()->get('badiu.admin.cms.resource.data');
		 $dto=$rdata->getInfoStatus($entity,$parentid);
		 $statusshortname=$this->getUtildata()->getVaueOfArray($dto,'statusshortname');
		 $deleted=$this->getUtildata()->getVaueOfArray($dto,'deleted');
		
		if($statusshortname !='approval' || $deleted){
			$permission=$this->getContainer()->get('badiu.system.access.permission');
			$hasperm=$permission->has_access('badiu.admin.cms.',$this->getSessionhashkey());
			if(!$hasperm){echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">';
						echo "<div class=\"alert alert-danger\" role=\"alert\">Sem permissão de acesso</div></hr>";exit;}
			}		 
      /*
          $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
          $param=array('resourseid'=>$parentid);
          $managelib=$this->getContainer()->get('badiu.admin.cms.resourcelog.lib.manage');
		  $managelib->add($param);
		 */
        return $filter;
         } 
   
         
       
}
