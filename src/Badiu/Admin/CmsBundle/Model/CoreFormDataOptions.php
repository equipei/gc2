<?php
namespace Badiu\Admin\CmsBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class CoreFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
    
    public  function getRepositoryStrutureParent(){
        $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
        $list=array();
        if(empty($parentid)){return  $list;}
       $param=array('fparam'=>array('repositoryid'=>$parentid));
       $list= $this->getContainer()->get('badiu.admin.cms.resourcestructure.data')->getFormChoiceParent($param);
       
       return $list;
    }
    public  function getRepositoryStrutureParentKeyId(){
      $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
      $list=array();
      if(empty($parentid)){return  $list;}
     $param=array('fparam'=>array('repositoryid'=>$parentid),'typeofkey'=>'id');
     $list= $this->getContainer()->get('badiu.admin.cms.resourcestructure.data')->getFormChoiceParent($param);
     
     return $list;
  }
  
      public  function getRepositoryCategoryParent(){
        $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
        $list=array();
        if(empty($parentid)){return  $list;}
       $param=array('fparam'=>array('repositoryid'=>$parentid));
       $list= $this->getContainer()->get('badiu.admin.cms.resourcecategory.data')->getFormChoiceParent($param);
       
       return $list;
    }
   public  function getRepositoryCategoryParentKeyId(){
      $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
      $list=array();
      if(empty($parentid)){return  $list;}
     $param=array('fparam'=>array('repositoryid'=>$parentid),'typeofkey'=>'id');
     $list= $this->getContainer()->get('badiu.admin.cms.resourcecategory.data')->getFormChoiceParent($param);
     
     return $list;
  }
  
	 public  function getResourceUpdate(){
        $list=array();
        $list['update']=$this->getTranslator()->trans('badiu.admin.cms.resource.update');
        $list['correction']=$this->getTranslator()->trans('badiu.admin.cms.resource.correction');
		return $list;
    }

    public  function getQuestionBoolean(){
      $list=array();
      $list[1]=$this->getTranslator()->trans('badiu.admin.cms.resourcequestion.true');
      $list[0]=$this->getTranslator()->trans('badiu.admin.cms.resourcequestion.false');
  return $list;
  }

  public  function getAnsweramountchoose(){
    $list=array();
    $list['one']=$this->getTranslator()->trans('badiu.admin.cms.resourcequestion.chooseoption.one');
    $list['multiple']=$this->getTranslator()->trans('badiu.admin.cms.resourcequestion.chooseoption.multiple');
    return $list;
}

public  function getLayoutviewmultiple(){
    $list=array();
    $list['radio']=$this->getTranslator()->trans('badiu.admin.cms.resourcequestion.layoutview.radio');
    $list['checkbox']=$this->getTranslator()->trans('badiu.admin.cms.resourcequestion.layoutview.checkbox');
    $list['select']=$this->getTranslator()->trans('badiu.admin.cms.resourcequestion.layoutview.select');
    return $list;
}

public  function getType(){
    $list=array();
    $list['page']=$this->getTypeLabel('page');
    $list['file']=$this->getTypeLabel('file');
    $list['image']=$this->getTypeLabel('image');
	$list['card']=$this->getTypeLabel('card');
	$list['videolibrary']=$this->getTypeLabel('playlist');
	$list['faq']=$this->getTypeLabel('faq');
    return $list;
}
 public  function getTypeLabel($type){
        $label=null;
        $label=$this->getTranslator()->trans('badiu.admin.cms.resourcetype.'.$type);
		
	    return  $label;
    }
}
