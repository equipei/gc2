<?php
 
 namespace Badiu\Admin\CmsBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
        $result=$this->initDbRepositorystatus() ;
        $result+=$this->initDbResourcestructurestatus();
		 $result+=$this->initDbResourcestatus();
  		return $result;
	} 
	
	 public function initDbRepositorystatus() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.cms.repositorystatus.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist && !$this->getForceupdate()){return 0;}
        
		$entity=$this->getEntity();
         $list=array(
			array('entity'=>$entity,'shortname'=>'active','name'=>$this->getTranslator()->trans('badiu.admin.cms.repositorystatus.active')),
			array('entity'=>$entity,'shortname'=>'inactive','name'=>$this->getTranslator()->trans('badiu.admin.cms.repositorystatus.inactive')),
	 	 ); 
		 
        foreach ($list as $param) {
			 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 
			 $paramedit=null;
			 if($this->getForceupdate()){
				 $paramedit= $param;
			 }
			 $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
			 $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
			 $r=$this->getUtildata()->getVaueOfArray($result,'id');
              if($r){$cont++;}
            }
	return $cont;
    }

public function initDbResourcestructurestatus() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.cms.resourcestructurestatus.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist && !$this->getForceupdate()){return 0;}
        
		$entity=$this->getEntity();
         $list=array(
			array('entity'=>$entity,'shortname'=>'active','name'=>$this->getTranslator()->trans('badiu.admin.cms.resourcestructurestatus.active')),
			array('entity'=>$entity,'shortname'=>'inactive','name'=>$this->getTranslator()->trans('badiu.admin.cms.resourcestructurestatus.inactive')),
	 	 ); 
		 
        foreach ($list as $param) {
			 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 
			 $paramedit=null;
			 if($this->getForceupdate()){
				 $paramedit= $param;
			 }
			 $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
			 $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
			 $r=$this->getUtildata()->getVaueOfArray($result,'id');
              if($r){$cont++;}
            }
	return $cont;
    }
  
  

public function initDbResourcestatus() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.cms.resourcestatus.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist && !$this->getForceupdate()){return 0;}
        
		$entity=$this->getEntity();
         $list=array(
			array('entity'=>$entity,'shortname'=>'inediting','name'=>$this->getTranslator()->trans('badiu.admin.cms.resourcestatus.inediting')),
			array('entity'=>$entity,'shortname'=>'underapproval','name'=>$this->getTranslator()->trans('badiu.admin.cms.resourcestatus.underapproval')),
			array('entity'=>$entity,'shortname'=>'approval','name'=>$this->getTranslator()->trans('badiu.admin.cms.resourcestatus.approval')),
			array('entity'=>$entity,'shortname'=>'cancelled','name'=>$this->getTranslator()->trans('badiu.admin.cms.resourcestatus.cancelled')),
	 	 ); 
		 
        foreach ($list as $param) {
			 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 
			 $paramedit=null;
			 if($this->getForceupdate()){
				 $paramedit= $param;
			 }
			 $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
			 $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
			 $r=$this->getUtildata()->getVaueOfArray($result,'id');
              if($r){$cont++;}
            }
	return $cont;
    }	 
}
