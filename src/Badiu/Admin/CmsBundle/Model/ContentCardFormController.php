<?php

namespace Badiu\Admin\CmsBundle\Model;
use Badiu\Admin\CmsBundle\Model\ResourceFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ContentCardFormController extends ResourceFormController
{

    function __construct(Container $container) {
            parent::__construct($container);
            $this->resourcemanagelib=$this->getContainer()->get('badiu.admin.cms.resourece.manage');
          }
    

      public function changeParamOnOpen() {
            parent::changeParamOnOpen();
           $this->getParamContent();
        }
   
     public function changeParam() {
        parent::changeParam();
        $this->setParamContent();
     }
    
     
      public function setParamContent() {
        
         $cada=array();
         $cada['header']=$this->getParamItem('cheader');
         $cada['body']=$this->getParamItem('cbody');
         $cada['footer']=$this->getParamItem('cfooter');
         $content=$this->getJson()->encode($cada);
         $this->addParamItem('content',$content);
         
         $this->removeParamItem('cheader');
         $this->removeParamItem('cbody');
         $this->removeParamItem('cfooter');

      }
      public function getParamContent() {
        
         $content=$this->getParamItem('content');
         $content = $this->getJson()->decode($content, true);
        
         $this->addParamItem('cheader',$this->getUtildata()->getVaueOfArray($content,'header'));
         $this->addParamItem('cbody',$this->getUtildata()->getVaueOfArray($content,'body'));
         $this->addParamItem('cfooter',$this->getUtildata()->getVaueOfArray($content,'footer'));
     }
    
}
