<?php

namespace Badiu\Admin\CmsBundle\Model;
use Badiu\Admin\CmsBundle\Model\ResourceFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class QuestionBooleanFormController extends ResourceFormController
{

    function __construct(Container $container) {
            parent::__construct($container);
            $this->resourcemanagelib=$this->getContainer()->get('badiu.admin.cms.resourece.manage');
          }
    
    /*  public function changeParamOnOpen() {
            parent::changeParamOnOpen();
            $this->getParamDconfig();
         }
   */
     public function changeParam() {
        parent::changeParam();
        $this->setParamDconfig();
     }
     
     public function setParamDconfig() {
        $core=array();
        $core['correctanswer']=$this->getParamItem('correctanswer');
        $core['generalfeedback']=$this->getParamItem('generalfeedback');
        $core['feedbacktrue']=$this->getParamItem('feedbacktrue');
        $core['feedbackfalse']=$this->getParamItem('feedbackfalse');
        $dconfig=array();
        if($this->isEdit()){
          $dconfig=$this->getDconfigdb();
        }
        $dconfig['core']=$core;
        $dconfig = $this->getJson()->encode($dconfig);
        $param = $this->getParam();
        $param['dconfig'] = $dconfig;
        $this->addParamItem('dconfig',$dconfig);
       
        $this->removeParamItem('correctanswer');
        $this->removeParamItem('generalfeedback');
        $this->removeParamItem('feedbacktrue');
        $this->removeParamItem('feedbackfalse');

     }
     /*public function getParamDconfig() {

    }*/
}
