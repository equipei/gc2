<?php

namespace Badiu\Admin\CmsBundle\Model;
use Badiu\Admin\CmsBundle\Model\ResourceFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class QuestionMultiplechoiceFormController extends ResourceFormController
{

    function __construct(Container $container) {
            parent::__construct($container);
            $this->resourcemanagelib=$this->getContainer()->get('badiu.admin.cms.resourece.manage');
          }
    
      public function changeParamOnOpen() {
            parent::changeParamOnOpen();
            $this->getParamDconfig();
         }
  
     public function changeParam() {
         parent::changeParam();
        $this->setParamDconfig();
     
     }
     
     public function setParamDconfig() {
        $core=array();
        $core['answeramountchoose']=$this->getParamItem('answeramountchoose');
        $core['layoutview']=$this->getParamItem('layoutview');
        $core['generalfeedback']=$this->getParamItem('generalfeedback');
        $core['feedbackanycorrectanswer']=$this->getParamItem('feedbackanycorrectanswer');
        $core['feedbackanywronganswer']=$this->getParamItem('feedbackanywronganswer');
        $dconfig=array();
        if($this->isEdit()){
          $dconfig=$this->getDconfigdb();
        
        }
        $core=$this->getAlternativeResponses($core);
        $dconfig['core']=$core;
        $dconfig = $this->getJson()->encode($dconfig);
        $param = $this->getParam();
        $param['dconfig'] = $dconfig;
        $this->addParamItem('dconfig',$dconfig);
       
        $this->removeParamItem('correctanswer');
        $this->removeParamItem('generalfeedback');
        $this->removeParamItem('feedbackanycorrectanswer');
        $this->removeParamItem('feedbackanywronganswer');
        $this->removeParamItem('answeramountchoose');
        $this->removeParamItem('layoutview');
       // print_r($this->getParam());exit;
     }
     public function getParamDconfig() {
            $dconfig = $this->getParamItem('dconfig');
            $dconfig = $this->getJson()->decode($dconfig, true);
            $dconfig=$this->getUtildata()->getVaueOfArray($dconfig,'core');

            $answeroptions=$this->getUtildata()->getVaueOfArray($dconfig,'answeroptions');
            if(is_array($answeroptions)){
              $cont=0;
              foreach ($answeroptions as $opt){
                $cont++;
                $answer=$this->getUtildata()->getVaueOfArray($opt,'answer');
                $this->addParamItem('answer'.$cont,$answer);
                
                $grade=$this->getUtildata()->getVaueOfArray($opt,'grade');
                $this->addParamItem('grade'.$cont,$grade);

                $feedback=$this->getUtildata()->getVaueOfArray($opt,'feedback');
                $this->addParamItem('feedback'.$cont,$feedback);
              }
            }
    }

    public function getAlternativeResponses($param) {
      
      $hasNex=true;
      $cont=1;
      $options=array();
      while ($hasNex){
         $keyresponse="answer$cont";
         $keygrade="grade$cont";
         $keyfeedback="feedback$cont";
         $keyoption="option$cont";

        
         $answer=$this->getParamItem( $keyresponse);
         $grade=$this->getParamItem($keygrade);
         $feedback=$this->getParamItem($keyfeedback);

         if(!empty($answer)){
            $options[$keyoption]=array('answer'=>$answer,'grade'=>$grade,'feedback'=>$feedback);
         }else{ $hasNex=false;}
          $cont++;
      }
      $param['answeroptions']=$options;

       //remove 
       $rhasNex=true;
      $rcont=1;
      while ($rhasNex){
          $keyresponse="answer$rcont";
          $keygrade="grade$rcont";
          $keyfeedback="feedback$rcont";
          
          $existparam=$this->existParamItem($keyresponse);
          if($existparam){
            $this->removeParamItem($keyresponse);
            $this->removeParamItem($keygrade);
            $this->removeParamItem($keyfeedback);
          }else{ $rhasNex=false; } 
         $rcont++;
      }
      return $param;
    }
}
