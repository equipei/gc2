<?php

namespace Badiu\Admin\CmsBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class CoreConfig extends BadiuModelLib {

	  function __construct(Container $container) {
        parent::__construct($container);
     
    } 


	function getSiteTitle() {
		$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$parentid=$this->getUtildata()->getVaueOfArray($param,'parentid');
		if(empty($parentid)){$parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');}
		$vkey=$this->getUtildata()->getVaueOfArray($param,'vkey');
		$vrelease=$this->getUtildata()->getVaueOfArray($param,'vrelease');
		
		if(empty($parentid)){return null;}
		if(empty($vkey)){return null;}
		if(empty($vrelease)){return null;}
		
		$resourcedata=$this->getContainer()->get('badiu.admin.cms.resource.data');
		$fcparam=array('repositoryid'=>$parentid,'vkey'=>$vkey,'vrelease'=>$vrelease);
		
		
		$title=$resourcedata->getGlobalColumnValue('name',$fcparam);
		return $title;
		
	}

}