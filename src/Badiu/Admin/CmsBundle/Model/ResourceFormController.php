<?php

namespace Badiu\Admin\CmsBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ResourceFormController extends BadiuFormController
{

  private $resourcemanagelib;
    function __construct(Container $container) {
            parent::__construct($container);
            $this->resourcemanagelib=$this->getContainer()->get('badiu.admin.cms.resourece.manage');
          }
    
     public function changeParamOnOpen() {
		parent::getFieldProcessOnOpen();
		$this->defaultNextSortorder();
         if($this->isEdit()){
            $this->addParamItem('vrelease','correction');
            $dconfig = $this->getParamItem('dconfig');
           
            $dconfig = $this->getJson()->decode($dconfig, true);
            $dconfig=$this->getUtildata()->getVaueOfArray($dconfig,'core');
            if(is_array($dconfig)){
              foreach ($dconfig as $key => $value){
                $this->addParamItem($key,$value);
              }
            }
            $this->addParamItem('dkey',null);
         }
		 
		
     }   
     
     public function changeParam() {
        //if new record add vkey, add skey set  release  true
        $newverison=TRUE;

        if(!$this->isEdit()){
          $param=$this->getResourcemanagelib()->startNewResource($this->getParam());
          $this->setParam($param);
          } 
     //print_r($this->getParam());exit;
     }
     
      public function execBefore(){
        parent::execBefore();
        if($this->isEdit()){
           $vrelease=$this->getParamItem('vrelease');
          if($vrelease=='correction'){
                $this->removeParamItem('vrelease');
                if(empty($this->getParamItem('dkey'))){$this->removeParamItem('dkey');}
          }else if($vrelease=='update'){
            $currentid=$this->getParamItem('id');

            $servicedb=$this->getContainer()->get($this->getKminherit()->data());
            $param= $this->getParam();
            $param=$this->getResourcemanagelib()->newVersion($servicedb,$param);
            $currentparam=array('id'=>$currentid,'vrelease'=>FALSE);
            $this->setParam($currentparam);
          }
        } 
   }
     public function execAfter(){
      parent::execAfter();
      if(!$this->isEdit()){
          $servicedb=$this->getContainer()->get($this->getKminherit()->data());
          $param= $this->getParam();
          $resourcemanage=$this->getContainer()->get('badiu.admin.cms.resourece.manage');
          $param=$resourcemanage->updateKeys($servicedb,$param);
          $this->setParam($param);
      }
     
 }
 function getDconfigdb($id=null) {
  $dconfig=array();
   if(empty($id)){
      $id=$this->getParamItem('id');
   }
   if(empty($id)){return $dconfig;}

    $servicedb=$this->getContainer()->get($this->getKminherit()->data());
    $dconfig=$servicedb->getMaxGlobalColumnValue('dconfig',array('id'=>$id));
    $dconfig = $this->getJson()->decode($dconfig, true);
    return $dconfig;
 }
  
  function defaultNextSortorder() {
	  if($this->isEdit()){return null;}
	  $nextorder=100;
	  
	  $dtype= $this->getParamItem('dtype');
	  $btype= $this->getParamItem('btype');
	  $parentid= $this->getParamItem('parentid');
	  $entity=$this->getEntity();
	  $repositoryid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
	  $resourcedata=$this->getContainer()->get('badiu.admin.cms.resource.data');
	  $fapram=array('entity'=>$entity,'repositoryid'=>$repositoryid,'dtype'=>$dtype,'btype'=>$btype);
	  
	  $lastvalue=$resourcedata->getMaxGlobalColumnValue('sortorder',$fapram);
	
	  if(!empty($lastvalue)){$nextorder=$lastvalue+100;}
	  $this->addParamItem('sortorder',$nextorder);
	 
	  return $nextorder;
  }

 function getResourcemanagelib() {
  return $this->resourcemanagelib;
}

function setResourcemanagelib($resourcemanagelib) {
  $this->resourcemanagelib = $resourcemanagelib;
}


}
