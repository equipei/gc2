<?php

namespace Badiu\Admin\CmsBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ItemOutputChangedata extends  BadiuModelLib {
 
	  function __construct(Container $container) {
        parent::__construct($container);
     
    } 
	public function exec($page){
		
		$repositorydata=$this->getContainer()->get('badiu.admin.cms.repository.data');
		$repositoryid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		$repositorydto= $repositorydata->getGlobalColumnsValue('o.name,o.param',array('id'=>$repositoryid));
		
		$repositoryname=$this->getUtildata()->getVaueOfArray($repositorydto,'name');
		$repositorydconfig=$this->getUtildata()->getVaueOfArray($repositorydto,'param');
	
		
		$repositorydconfig=$this->getJson()->decode($repositorydconfig,true);
		$vlmenu=$this->getUtildata()->getVaueOfArray($repositorydconfig,'vlmenu');
		if($vlmenu=='folder'){
			$fparam=array('repositoryid'=>$repositoryid);
			$strturelibcontent=$this->getContainer()->get('badiu.admin.cms.resourcestructure.lib.content');
			$menulist=$strturelibcontent->getTree($fparam);
			$addcontent=$page->getAdditionalContents();
			$addcontent[0]=$menulist;
			$page->setAdditionalContents($addcontent);
		}
		
		
		return $page;
	
	}
	
}

