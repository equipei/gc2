<?php
namespace Badiu\Admin\CmsBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class VideoListManageLib extends  BadiuModelLib {
    
	// /system/service/process?_service=badiu.admin.cms.repository.lib.manage&_function=teste
      
	public function __construct(Container $container) {
		parent::__construct($container);
	  
  }

  public function addComment(){
		$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$messagedata=$this->getContainer()->get('badiu.admin.message.lib.manager');
		$msgresult=$messagedata->addMessage($param);
		$result=$this->searchComment();
		return $result; 
  }
public function addDiscussion(){
		$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$messagedata=$this->getContainer()->get('badiu.admin.message.lib.manager');
		$msgresult=$messagedata->addDiscussion($param);
		$result=$this->searchDiscussion();
		return $result; 
  }

public function addActionLike(){
		$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		$gradelibmanager=$this->getContainer()->get('badiu.admin.grade.lib.manager');
		$msgresult=$gradelibmanager->addGradeSingle($param);
		$result=$this->searchActionLike();
		return $result; 
  }
  
	public function searchComment() {
		 $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		 $fparam=array();
		 $fparam['entity']=$this->getEntity();
		 $fparam['parentid']=$this->getUtildata()->getVaueOfArray($param,'resoureceid');
		 $fparam['userid']=$this->getUtildata()->getVaueOfArray($param,'userid');
		 $fparam['_sqlindex']=$this->getUtildata()->getVaueOfArray($param,'_sqlindex');
		 $this->getSearch()->getKeymanger()->setBaseKey('badiu.admin.cms.resourcecontentvideolist');
		 $result=$this->getSearch()->searchList($fparam);	
		 return $result;
		  
	}
	
	public function searchDiscussion() {
		 $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		 $fparam=array();
		 $fparam['entity']=$this->getEntity();
		 $fparam['parentid']=$this->getUtildata()->getVaueOfArray($param,'resoureceid');
		 $fparam['userid']=$this->getUtildata()->getVaueOfArray($param,'userid');
		 $fparam['_sqlindex']=$this->getUtildata()->getVaueOfArray($param,'_sqlindex');
		
		 $this->getSearch()->getKeymanger()->setBaseKey('badiu.admin.cms.resourcecontentvideolist');
		 $result=$this->getSearch()->searchList($fparam);	
		 return $result;
		  
	}
	
	public function searchActionLike() {
		 $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		 $fparam=array();
		 $fparam['entity']=$this->getEntity();
		 $fparam['parentid']=$this->getUtildata()->getVaueOfArray($param,'referencekey');
		 $fparam['userid']=$this->getUtildata()->getVaueOfArray($param,'userid');
		 $fparam['_sqlindex']=$this->getUtildata()->getVaueOfArray($param,'_sqlindex');
		
		 $this->getSearch()->getKeymanger()->setBaseKey('badiu.admin.cms.resourcecontentvideolist');
		 $result=$this->getSearch()->searchList($fparam);	
		 return $result;
		  
	}
	
	public function deleteMessage() {
		 $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 if(empty($modulekey) && empty($moduleinstance)){return null;}
		 
		 $datakey="";
		 if($modulekey=='badiu.admin.message.discussion'){$datakey='badiu.admin.message.discussion.data';}
		 else if($modulekey=='badiu.admin.message.message'){$datakey='badiu.admin.message.message.data';}
		
		 
		 $data=$this->getContainer()->get($datakey);
		 $fparam=array('id'=>$moduleinstance,'deleted'=>1);
		 $result=$data->updateNativeSql($fparam,false);
		
		 return $result;
		  
	}
	
	
	
}
