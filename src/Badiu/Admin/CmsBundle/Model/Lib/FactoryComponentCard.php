<?php

namespace Badiu\Admin\CmsBundle\Model\Lib;
 
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FactoryComponentCard extends BadiuModelLib {

	  function __construct(Container $container) {
        parent::__construct($container);
     
    } 
 
	public function default($param){
		$componentshortname=$this->getUtildata()->getVaueOfArray($param,0);
		$ukidentify=$this->getUtildata()->getVaueOfArray($param,3);
		if(empty($componentshortname)){return null;}
		$content=$this->getContent($param);

		
		//get component content
		$entity=$this->getEntity();
		$compparam=array('entity'=>$entity,'shortname'=>$componentshortname,'deleted'=>0);
		$contentcomp=$this->getContainer()->get('badiu.admin.cms.component.data')->getGlobalColumnValue('content',$compparam);
		if(empty($contentcomp)){return null;}
		
		$cparam=array('component'=>$contentcomp,'content'=>$content,'identify'=>$ukidentify);
		$content=$this->buildContent($cparam);
		
		return $content;
    }	
 
	public function getContent($param){
		$repositoryshortname=$this->getUtildata()->getVaueOfArray($param,1);
		$resourceshortname=$this->getUtildata()->getVaueOfArray($param,2);
		if(empty($repositoryshortname)){return null;}
		if(empty($resourceshortname)){return null;}
		$entity=$this->getEntity();
		
		$resourcedata=$this->getContainer()->get('badiu.admin.cms.resource.data');
		$sql="SELECT o.id,o.name,o.content,o.defaulturl,o.defaultimage,o.description,o.dconfig,o.param  FROM BadiuAdminCmsBundle:AdminCmsResource o JOIN o.repositoryid r JOIN o.statusid s WHERE o.entity=:entity AND r.shortname =:repositoryshortname AND o.shortname =:resourceshortname AND s.shortname =:statusshortname AND o.deleted=:deleted AND o.vrelease=:vrelease ";
		$query = $resourcedata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity );
		$query->setParameter('repositoryshortname',$repositoryshortname);
		$query->setParameter('resourceshortname',$resourceshortname);
		$query->setParameter('statusshortname','approval');
		$query->setParameter('deleted',0);
		$query->setParameter('vrelease',1);
		$result = $query->getOneOrNullResult();
		return $result;
	}

	public function buildContent($param){
		$component=$this->getUtildata()->getVaueOfArray($param,'component');
		$images=$this->getUtildata()->getVaueOfArray($param,'images');
		$identify=$this->getUtildata()->getVaueOfArray($param,'identify');
		
		
		
		$managestring=$this->getContainer()->get('badiu.system.core.lib.util.managestring');

		$content=$this->getUtildata()->getVaueOfArray($param,'content');
		
		$dconfig=$this->getUtildata()->getVaueOfArray($content,'dconfig');
		
		$dconfig=$this->getJson()->decode($dconfig,true);
		
		$link1url="";
		$link1label="";
		
		$link2url="";
		$link2label="";
		
		$category="";
		$info1="";
		$info2="";
		
		 if(is_array($dconfig)){
			$link1url=$this->getUtildata()->getVaueOfArray($dconfig,"link1.url",true);
			$link1label=$this->getUtildata()->getVaueOfArray($dconfig,"link1.label",true);
			
			$link2url=$this->getUtildata()->getVaueOfArray($dconfig,"link2.url",true);
			$link2label=$this->getUtildata()->getVaueOfArray($dconfig,"link2.label",true);
			
			$category=$this->getUtildata()->getVaueOfArray($dconfig,"category");
			$info1=$this->getUtildata()->getVaueOfArray($dconfig,"info1");
			$info2=$this->getUtildata()->getVaueOfArray($dconfig,"info2");
		 }
		$cardid=$this->getUtildata()->getVaueOfArray($content,'id');
		$cardconent=$this->getUtildata()->getVaueOfArray($content,'content');
		$title=$this->getUtildata()->getVaueOfArray($content,'name');
		$defaultimage=$this->getUtildata()->getVaueOfArray($content,'defaultimage');
		$description=$this->getUtildata()->getVaueOfArray($content,'description');
			
		$imageurl=$this->getUtilapp()->getFileUrl($defaultimage);
		
		if(empty($identify)){$identify="badiucard-".$cardid;}
		$component=str_replace("{ID}",$identify,$component);
		$component=str_replace("{IMAGE_URL}",$imageurl,$component);
		$component=str_replace("{IMAGE_ALT}",$description,$component);
		$component=str_replace("{TITLE}",$identify,$component);
		$component=str_replace("{CONTENT}",$cardconent,$component);
		$component=str_replace("{CATEGORY}",$category,$component);
		$component=str_replace("{INFOI}",$info1,$component);
		$component=str_replace("{INFOII}",$info2,$component);
		$component=str_replace("{BUTTON_URL}",$link1url,$component);
		$component=str_replace("{BUTTON_LABEL}",$link1label,$component);
		
		
		return $component;
		
	}
}