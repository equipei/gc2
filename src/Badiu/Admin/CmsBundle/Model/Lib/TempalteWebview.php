<?php
namespace Badiu\Admin\CmsBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class TemplateWebview extends  BadiuModelLib {
    
      
	public function __construct(Container $container) {
		parent::__construct($container);
	  
  }

  public function exec(){
		$file=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('file');
		if(empty($file)){echo "";exit;}
		$path="BadiuAdminCmsBundle:View/$file";
		$path=$this->getContainer()->get('badiu.system.core.lib.util.app')->getFilePath($path);
		if(!file_exists ($path)){echo "";exit;}
		$fcontent=""; 
		ob_start();
		include_once($path);
  	$fcontent= ob_get_contents();
		ob_end_clean();
		echo $fcontent;
		exit;
	return null;
 }


}
