<?php

namespace Badiu\Admin\CmsBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FactoryComponentCardList extends BadiuModelLib {

	  function __construct(Container $container) {
        parent::__construct($container);
     
    } 

	public function default($param){
		$componentshortname=$this->getUtildata()->getVaueOfArray($param,0);
		$ukidentify=$this->getUtildata()->getVaueOfArray($param,3);
		if(empty($componentshortname)){return null;}
		$listcard=$this->getListCard($param);
		
		
		//get component content
		$entity=$this->getEntity();
		$compparam=array('entity'=>$entity,'shortname'=>$componentshortname,'deleted'=>0);
		$contentcomp=$this->getContainer()->get('badiu.admin.cms.component.data')->getGlobalColumnValue('content',$compparam);
		if(empty($contentcomp)){return null;}
		
		$cparam=array('component'=>$contentcomp,'listcard'=>$listcard,'identify'=>$ukidentify,'configparam'=>$param);
		$content=$this->buildContent($cparam);
		
		return $content;
    }	

 
	public function getListCard($param){
		$repositoryshortname=$this->getUtildata()->getVaueOfArray($param,1);
		$foldershortname=$this->getUtildata()->getVaueOfArray($param,2);
		if(empty($repositoryshortname)){return null;}
		if(empty($foldershortname)){return null;}
		$entity=$this->getEntity();
		$maxrecord=50;
		$resourcedata=$this->getContainer()->get('badiu.admin.cms.resource.data');
		$sql="SELECT o.id,o.shortname  FROM BadiuAdminCmsBundle:AdminCmsResource o JOIN o.repositoryid r JOIN o.structureid st JOIN o.statusid s WHERE o.entity=:entity AND r.shortname =:repositoryshortname AND st.shortname =:structureshortname AND s.shortname =:statusshortname AND o.deleted=:deleted AND o.vrelease=:vrelease ORDER BY o.sortorder";
		$query = $resourcedata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity );
		$query->setParameter('repositoryshortname',$repositoryshortname);
		$query->setParameter('structureshortname',$foldershortname);
		$query->setParameter('statusshortname','approval');
		$query->setParameter('deleted',0);
		$query->setParameter('vrelease',1);
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		return $result;
	}

	public function buildContent($param){
		$component=$this->getUtildata()->getVaueOfArray($param,'component');
		$listcard=$this->getUtildata()->getVaueOfArray($param,'listcard');
		$identify=$this->getUtildata()->getVaueOfArray($param,'identify');
		
		if(empty($identify)){$identify="badiucardlist-".time();}
		$component=str_replace("{ID}",$identify,$component);
		
		$listcard=$this->getUtildata()->getVaueOfArray($param,'listcard');
		
		$componentshortname=$this->getUtildata()->getVaueOfArray($param,"configparam.0",true);
		$repositoryshortname=$this->getUtildata()->getVaueOfArray($param,"configparam.1",true);
		$cardcomponentshortname=$this->getUtildata()->getVaueOfArray($param,"configparam.4",true);
		if(empty($cardcomponentshortname)){$cardcomponentshortname="defaultcard";}
		$factoryserviceexpression=$this->getContainer()->get('badiu.system.core.lib.util.factoryserviceexpression');
		$content="";
		foreach($listcard as $row) {
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			$shortname=$this->getUtildata()->getVaueOfArray($row,'shortname');
			$expression="{badiu.admin.cms.resourece.factorycomponentcard|default|$cardcomponentshortname|$repositoryshortname|$shortname|$id}";

			$content.=$factoryserviceexpression->change($expression);
			
		}
		
		$component=str_replace("{LOOP_CARD}",$content,$component);
		return $component;
		
	}
}