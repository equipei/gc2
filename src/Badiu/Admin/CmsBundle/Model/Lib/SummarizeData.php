<?php

namespace Badiu\Admin\CmsBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Lib\Report\SummarizeData as ReportSummarizeData;
class SummarizeData  extends ReportSummarizeData{

    function __construct(Container $container) {
                parent::__construct($container);
           
       }

	function countByYearSearch($param,$dresult){
		$operation=$this->getUtildata()->getVaueOfArray($param,'operation');  
		$dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');  
		$btype=$this->getUtildata()->getVaueOfArray($param,'btype');
		$statusshortname=$this->getUtildata()->getVaueOfArray($param,'statusshortname');
		$widhtypeid=$this->getUtildata()->getVaueOfArray($param,'widhtypeid');
		$typeshortname=$this->getUtildata()->getVaueOfArray($param,'typeshortname');

		$date1=$this->getUtildata()->getVaueOfArray($param,'_date1');
		$date2=$this->getUtildata()->getVaueOfArray($param,'_date2');
		$year=$this->getUtildata()->getVaueOfArray($param,'_year');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		if(empty($ntity)){$entity=$this->getEntity();}
		$wsql="";
		$wjon="";
		if(!empty($dtype)){$wsql.=" AND o.dtype=:dtype ";}
		if(!empty($btype)){$wsql.=" AND o.btype=:btype ";}
		if(!empty($statusshortname)){$wsql.=" AND s.shortname=:statusshortname ";}
		if($widhtypeid==1){$wsql.=" AND o.typeid > :hastypeid ";}
		if(!empty($typeshortname)){$wsql.=" AND t.shortname=:typeshortname ";}
		if(empty($operation)){$operation='countresource';}
		
		
		$sql="SELECT COUNT(o.id) AS countrecord FROM BadiuAdminCmsBundle:AdminCmsResource o JOIN o.statusid s LEFT JOIN o.typeid t WHERE o.id > 0 AND o.entity=:entity AND o.timecreated >=:date1 AND o.timecreated <=:date2 AND o.deleted=:deleted $wsql ";
		if($operation=='countresourceaccesed'){
			$sql="SELECT COUNT(DISTINCT o.id ) AS countrecord FROM BadiuAdminCmsBundle:AdminCmsResourceLog l JOIN l.resourceid o JOIN o.statusid s LEFT JOIN o.typeid t WHERE o.id > 0 AND o.entity=:entity AND l.timecreated >=:date1 AND l.timecreated <=:date2 AND o.deleted=:deleted $wsql ";
		 }
		 if($operation=='countresourcelog'){
			$sql="SELECT COUNT(l.id ) AS countrecord FROM BadiuAdminCmsBundle:AdminCmsResourceLog l JOIN l.resourceid o JOIN o.statusid s LEFT JOIN o.typeid t WHERE o.id > 0 AND o.entity=:entity AND l.timecreated >=:date1 AND l.timecreated <=:date2 AND o.deleted=:deleted $wsql ";
		 }
		$data=$this->getContainer()->get('badiu.admin.cms.resource.data');
		 
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
		$query->setParameter('date1',$date1);
		$query->setParameter('date2',$date2);
		$query->setParameter('deleted', 0);
		if(!empty($dtype)){$query->setParameter('dtype', $dtype);}
		if(!empty($btype)){$query->setParameter('btype', $btype);}
		if(!empty($statusshortname)){$query->setParameter('statusshortname', $statusshortname);}
		if($widhtypeid==1){$query->setParameter('hastypeid', 0);}
		if(!empty($typeshortname)){$query->setParameter('typeshortname', $typeshortname);}
		$result= $query->getOneOrNullResult();
		
		$countrecord=$this->getUtildata()->getVaueOfArray($result,'countrecord');
		$dresult[$year]=$countrecord;
		return $dresult;
	}
	 
	 function getMinMaxYear($param){
		$operation=$this->getUtildata()->getVaueOfArray($param,'operation');  
		$dtype=$this->getUtildata()->getVaueOfArray($param,'dtype'); 
		$btype=$this->getUtildata()->getVaueOfArray($param,'btype');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		
		$statusshortname=$this->getUtildata()->getVaueOfArray($param,'statusshortname');
		$widhtypeid=$this->getUtildata()->getVaueOfArray($param,'widhtypeid');
		$typeshortname=$this->getUtildata()->getVaueOfArray($param,'typeshortname');
		
		if(empty($ntity)){$entity=$this->getEntity();}
		$timenullcontrol = new \DateTime();
		$timenullcontrol->setDate(1970, 1, 1);
		$wsql="";
		if(!empty($dtype)){$wsql.=" AND o.dtype=:dtype ";}
		if(!empty($btype)){$wsql.=" AND o.btype=:btype ";}
		if(!empty($statusshortname)){$wsql.=" AND s.shortname=:statusshortname ";}
		if($widhtypeid==1){$wsql.=" AND o.typeid > :hastypeid ";}
		if(!empty($typeshortname)){$wsql.=" AND t.shortname=:typeshortname ";}
		
		$sql="SELECT MIN(o.timecreated) AS firstdate, MAX(o.timecreated) AS lastdate  FROM BadiuAdminCmsBundle:AdminCmsResource o  JOIN o.statusid s LEFT JOIN o.typeid t WHERE o.id > 0 AND o.entity=:entity AND o.timecreated > :timenullcontrol  AND o.deleted=:deleted $wsql ";
		
		if($operation=='countresourceaccesed'){
			$sql="SELECT MIN(l.timecreated) AS firstdate, MAX(l.timecreated) AS lastdate  FROM BadiuAdminCmsBundle:AdminCmsResourceLog l JOIN l.resourceid o JOIN o.statusid s LEFT JOIN o.typeid t  WHERE o.id > 0 AND o.entity=:entity AND l.timecreated > :timenullcontrol  AND o.deleted=:deleted $wsql ";
		 }
		$data=$this->getContainer()->get('badiu.admin.cms.resource.data');
		 
		$query = $data->getEm()->createQuery($sql);
        $query->setParameter('entity', $entity);
		$query->setParameter('deleted', 0);
		$query->setParameter('timenullcontrol', $timenullcontrol);
		if(!empty($dtype)){$query->setParameter('dtype', $dtype);}
		if(!empty($btype)){$query->setParameter('btype', $btype);}
		if(!empty($statusshortname)){$query->setParameter('statusshortname', $statusshortname);}
		if($widhtypeid==1){$query->setParameter('hastypeid', 0);}
		if(!empty($typeshortname)){$query->setParameter('typeshortname', $typeshortname);}
		$result= $query->getOneOrNullResult();
		
		return $result;
	}
}
