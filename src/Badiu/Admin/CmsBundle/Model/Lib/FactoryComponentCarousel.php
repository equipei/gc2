<?php

namespace Badiu\Admin\CmsBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FactoryComponentCarousel extends BadiuModelLib {

	  function __construct(Container $container) {
        parent::__construct($container);
     
    } 
	public function teste(){
		$param=array();
		$this->exec($param);
		//{badiu.moodle.mreport.lib.menu|freport}
		//badiu.admin.cms.resourece.factorycomponentcarousel
		/*
		 $factoryserviceexpression=$this->getContainer()->get('badiu.system.core.lib.util.factoryserviceexpression');
		  $confmenu=$factoryserviceexpression->change($confmenu);
		*/
	}
	public function default($param){
		$componentshortname=$this->getUtildata()->getVaueOfArray($param,0);
		$ukidentify=$this->getUtildata()->getVaueOfArray($param,3);
		if(empty($componentshortname)){return null;}
		$listimg=$this->getListImages($param);
		
		
		
		//get component content
		$entity=$this->getEntity();
		$compparam=array('entity'=>$entity,'shortname'=>$componentshortname,'deleted'=>0);
		$contentcomp=$this->getContainer()->get('badiu.admin.cms.component.data')->getGlobalColumnValue('content',$compparam);
		if(empty($contentcomp)){return null;}
		
		$cparam=array('component'=>$contentcomp,'images'=>$listimg,'identify'=>$ukidentify);
		$content=$this->buildContent($cparam);
		
		return $content;
    }	

 
	public function getListImages($param){
		$repositoryshortname=$this->getUtildata()->getVaueOfArray($param,1);
		$foldershortname=$this->getUtildata()->getVaueOfArray($param,2);
		if(empty($repositoryshortname)){return null;}
		if(empty($foldershortname)){return null;}
		$entity=$this->getEntity();
		$maxrecord=50;
		$resourcedata=$this->getContainer()->get('badiu.admin.cms.resource.data');
		$sql="SELECT o.id,o.name,o.content,o.defaulturl,o.description  FROM BadiuAdminCmsBundle:AdminCmsResource o JOIN o.repositoryid r JOIN o.structureid st JOIN o.statusid s WHERE o.entity=:entity AND r.shortname =:repositoryshortname AND st.shortname =:structureshortname AND s.shortname =:statusshortname AND o.deleted=:deleted AND o.vrelease=:vrelease ORDER BY o.sortorder";
		$query = $resourcedata->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity );
		$query->setParameter('repositoryshortname',$repositoryshortname);
		$query->setParameter('structureshortname',$foldershortname);
		$query->setParameter('statusshortname','approval');
		$query->setParameter('deleted',0);
		$query->setParameter('vrelease',1);
		$query->setMaxResults($maxrecord);
        $result = $query->getResult();
		return $result;
	}

	public function buildContent($param){
		$component=$this->getUtildata()->getVaueOfArray($param,'component');
		$images=$this->getUtildata()->getVaueOfArray($param,'images');
		$identify=$this->getUtildata()->getVaueOfArray($param,'identify');
		
		if(empty($identify)){$identify="badiucarrousel-".time();}
		$component=str_replace("{ID}",$identify,$component);
		
		
		$managestring=$this->getContainer()->get('badiu.system.core.lib.util.managestring');

		$images=$this->getUtildata()->getVaueOfArray($param,'images');
		
		//target
		$targetcodeoriginal=$managestring->getFirstPartialText($component,'{LOOP_TARGET_START}','{LOOP_TARGET_END}',17);
		$targetcode=str_replace("{LOOP_TARGET_START}","",$targetcodeoriginal);
		$targetcode=str_replace("{LOOP_TARGET_END}","",$targetcode);
		$cont=0;
		
		$targetlist="";
		foreach($images as $row) {
			$classactive="";
			if($cont==0){$classactive=' class="active" ';}
			$targetitem=str_replace("{CLASS_ACTIVE}",$classactive,$targetcode);
			$targetitem=str_replace("{SEQ}",$cont,$targetitem);
			$targetlist.=$targetitem;
			$cont++;
		}
		$component=str_replace($targetcodeoriginal,$targetlist,$component);
		
		//replace item
		$itemcodeoriginal=$managestring->getFirstPartialText($component,'{LOOP_ITEM_START}','{LOOP_ITEM_END}',15);
		$itemcode=str_replace("{LOOP_ITEM_START}","",$itemcodeoriginal);
		$itemcode=str_replace("{LOOP_ITEM_END}","",$itemcode);
		$cont=0;
		
		$itemtlist="";
		foreach($images as $row) {  
			$fileimage=$this->getUtildata()->getVaueOfArray($row,'content');
			$title=$this->getUtildata()->getVaueOfArray($row,'name');
			$defaulturl=$this->getUtildata()->getVaueOfArray($row,'defaulturl');
			$description=$this->getUtildata()->getVaueOfArray($row,'description');
			
			$imageurl=$this->getUtilapp()->getFileUrl($fileimage);
			
			$classactive="";
			if($cont==0){$classactive=' active';}
			$citem=str_replace("{CLASS_ACTIVE_NAME}",$classactive,$itemcode);
			$citem=str_replace("{IMAGE_URL}",$imageurl,$citem);
			$citem=str_replace("{TITLE}",$title,$citem);
			$citem=str_replace("{IMAGE_ALT}",$title,$citem);
			$citem=str_replace("{DESCRIPTION}",$description,$citem);
			$itemtlist.=$citem;
			$cont++;
			
		}
		$component=str_replace($itemcodeoriginal,$itemtlist,$component);
		return $component;
		
	}
}