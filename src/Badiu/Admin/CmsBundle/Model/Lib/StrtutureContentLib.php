<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoggedInUserListener
 *
 * @author lino
 */

namespace Badiu\Admin\CmsBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class StrtutureContentLib extends BadiuModelLib {
    
      
	public function __construct(Container $container) {
		parent::__construct($container);
	  
  }


public function getTree($param){
			$menulinks=$this->makeMenuByFolder($param);
			$format=$this->getUtildata()->getVaueOfArray($param,'format');
			if($format=='defaulthtmllink'){$menulinks=$this->makeLinsByFolder($menulinks);}
			$partial=$this->getUtildata()->getVaueOfArray($param,'partial');
			
			return $menulinks;
	}
	
	private function makeMenuByFolder($param){
		$repositoryid=$this->getUtildata()->getVaueOfArray($param,'repositoryid');
		$partial=$this->getUtildata()->getVaueOfArray($param,'partial');
		$onlychidrens=$this->getUtildata()->getVaueOfArray($param,'onlychidrens');
		$currentvkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('vkey');
		$parentstrutureid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentstrutureid');
		$currentstrutureid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('currentstrutureid');
		if($partial){$currentstrutureid=$this->getUtildata()->getVaueOfArray($param,'currentstrutureid');}
		
		$resourcestructuredata=$this->getContainer()->get('badiu.admin.cms.resourcestructure.data');
		
		$ffparam=array('repositoryid'=>$repositoryid,'deleted'=>0,'level'=>0,'_orderby'=>'ORDER BY o.orderidpath');
		if($partial==1 && !empty($currentstrutureid)){
			$ffparam=array('repositoryid'=>$repositoryid,'deleted'=>0,'parent'=>$currentstrutureid,'_orderby'=>'ORDER BY o.orderidpath');
		}
		$folderlist= $resourcestructuredata->getGlobalColumnsValues('o.id,o.name',$ffparam);
		
		$resourcecontentdata=$this->getContainer()->get('badiu.admin.cms.resource.data');
		$fparam=array('repositoryid'=>$repositoryid,'vrelease'=>1,'deleted'=>0,'s.shortname'=>'approval');
		if($partial==1 && !empty($currentstrutureid)){
			$struturepath=$resourcestructuredata->getGlobalColumnValue('idpath',array('id'=>$currentstrutureid));
			$fparam=array('repositoryid'=>$repositoryid,'vrelease'=>1,'deleted'=>0,'s.shortname'=>'approval','_struturepath'=>$struturepath,'_onlychidrens'=>$onlychidrens);
		}
		$entity=$this->getEntity();
		$contentlist= $resourcecontentdata->getShortInfoList($entity,$fparam);
		
		$resourceindex= $resourcecontentdata->getGlobalColumnsValue('o.id,o.vkey,o.vrelease',array('repositoryid'=>$repositoryid,'shortname'=>'index','deleted'=>0));
		$resourceindexvkey=$this->getUtildata()->getVaueOfArray($resourceindex,'vkey');
		$resourceindexvvrelease=$this->getUtildata()->getVaueOfArray($resourceindex,'vrelease');
		$currentfolderid=null;
		//make links
		$contentlinks=array();
		$contentlinks['withoutfolder']=array();
		
		
		foreach ($contentlist as $row) {
			$strutureid=$this->getUtildata()->getVaueOfArray($row,'strutureid');
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			$vkey=$this->getUtildata()->getVaueOfArray($row,'vkey');
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			$current=0;
			if($vkey==$currentvkey && empty($currentstrutureid)){$current=1;$currentfolderid=$strutureid;}
			$rparam=array('parentid'=>$repositoryid,'vkey'=>$vkey,'vrelease'=>1);
			$url=$this->getUtilapp()->getUrlByRoute('badiu.admin.cms.resourcecontentfviewitemversion.dashboard',$rparam);
			$mlink=array();
			$mlink['type']='link';
			$mlink['position']='menu';
			$mlink['link']=array('id'=>"repositore-menu-$id",'url'=>$url,'target'=>'','name'=>$name,'description'=>'');
			$mlink['current']=$current;
			
			if(!empty($strutureid)){$contentlinks[$strutureid][$id]=$mlink;}
			else{
				array_push($contentlinks['withoutfolder'],$mlink);
			}
		}
		//if($partial==1){print_r($contentlinks);exit;}
		//make links
		$folderlinks=array();
		if(!$partial){$folderlinks=$this->getUtildata()->getVaueOfArray($contentlinks,'withoutfolder');}
		else{$folderlinks=$this->getUtildata()->getVaueOfArray($contentlinks,$currentstrutureid);}
		if(!is_array($folderlinks)){$folderlinks=array();}
		//array_push($folderlinks,$withoufolder);
		
		
		foreach ($folderlist as $row) {
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			$current=0;
			if($id==$currentfolderid && empty($parentstrutureid)){$current=1;}
			else if($id==$parentstrutureid){$current=1;}
			
			$links=$this->getUtildata()->getVaueOfArray($contentlinks,$id);
			$subfolderlist= $resourcestructuredata->getGlobalColumnsValues('o.id,o.name',array('repositoryid'=>$repositoryid,'deleted'=>0,'parent'=>$id,'_orderby'=>'ORDER BY o.orderidpath'));
			
			
			foreach ($subfolderlist as $sfl) {
				$subcurrent=0;
				$subfid=$this->getUtildata()->getVaueOfArray($sfl,'id');
				$subfname=$this->getUtildata()->getVaueOfArray($sfl,'name');
				if($subfid==$currentstrutureid){$subcurrent=1;}
				$rparam=array('parentid'=>$repositoryid,'vkey'=>$resourceindexvkey,'vrelease'=>$resourceindexvvrelease,'currentstrutureid'=>$subfid,'parentstrutureid'=>$id);
				$url=$this->getUtilapp()->getUrlByRoute('badiu.admin.cms.resourcecontentfviewitemversion.dashboard',$rparam);
				$mlink=array();
				$mlink['type']='link';
				$mlink['position']='menu';
				$mlink['link']=array('id'=>"repositore-menu-subitem-$subfid",'url'=>$url,'target'=>'','name'=>$subfname,'description'=>'');
				$mlink['current']=$subcurrent;
				array_push($links,$mlink);
			}
			
			$mlinkcat=array();
			$mlinkcat['type']='category';
			$mlinkcat['name']=$name;
			$mlinkcat['position']='menu';
			$mlinkcat['items']=$links;
			$mlinkcat['current']=$current;
			array_push($folderlinks,$mlinkcat);
		}
		/*echo "<pre>";
		print_r( $folderlinks);
		echo "</pre>";exit;*/
		return $folderlinks;
	}
	
	private function makeLinsByFolder($param){
		$html="";
		if(!is_array($param)){return "";}
		$html.="<div class=\"meucontentstrure\" >";
		foreach ($param as $row) {
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			$items=$this->getUtildata()->getVaueOfArray($row,'items');
			$type=$this->getUtildata()->getVaueOfArray($row,'type');
			if($type=='category'){
				$html.="<h5>$name</h5>";
				$html.="<ul>";
				if(is_array($items)){
					foreach ($items as $item) {
						$label=$this->getUtildata()->getVaueOfArray($item,'link.name',true);
						$url=$this->getUtildata()->getVaueOfArray($item,'link.url',true);
						$html.=" <li><a href=\"$url\">$label</a></li>";
					}	
				
				}
				$html.="</ul>";
			}else if($type=='link'){
				$label=$this->getUtildata()->getVaueOfArray($row,'link.name',true);
				$url=$this->getUtildata()->getVaueOfArray($row,'link.url',true);
				$html.=" <li><a href=\"$url\">$label</a></li>";
			}
			
		}
		$html.="</div>";
		return $html;
	}
}
