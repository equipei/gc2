<?php
namespace Badiu\Admin\CmsBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ResourceLogManage extends  BadiuModelLib {
    
	
      
	public function __construct(Container $container) {
		parent::__construct($container);
	  
  }

  public function add($param){
	 
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $dsession = $badiuSession->get();
        $accessby=$dsession->getAccessby();
		
		$iparam=array();
		$iparam['timecreated']=new \DateTime();
		$iparam['entity']=$this->getEntity();
		$iparam['resourceid']= $this->getUtildata()->getVaueOfArray($param, 'resourseid');
		$iparam['action']='view';
		$iparam['ip']=$_SERVER["REMOTE_ADDR"];
		$iparam['url']=null;
		if(isset($_SERVER['HTTP_REFERER'])){$iparam['urlreferer']=$_SERVER['HTTP_REFERER'];}
		$iparam['userid']=$this->getUserid();
		$iparam['dtype']='user';
		$iparam['urlreferer']=null;
		 if(isset($accessby->userid) && !empty($accessby->userid)){ $iparam['useridaccessedby']=$accessby->userid;}
		
		$logdata=$this->getContainer()->get('badiu.admin.cms.resourcelog.data');
		$r=$logdata->insertNativeSql($iparam,false);
		return $r;
  }
	
	
}
