<?php
namespace Badiu\Admin\CmsBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ReposistoryManageLib extends  BadiuModelLib {
    
	//http://54.156.96.187/badiunet/web/app_dev.php/system/service/process?_service=badiu.admin.cms.repository.lib.manage&_function=teste
      
	public function __construct(Container $container) {
		parent::__construct($container);
	  
  }

  public function teste(){
	  $param=array('repository'=>array('idnumber'=>121,'name'=>'GMoodle antigo','shortname'=>'GM'),
	  			'resource'=>array('idnumber'=>121,'name'=>'O que é','shortname'=>'OQ1','dtype'=>'content','btype'=>'page')
	  );
	  $this->syncLms($param);
  }

  public function syncLmsService(){

	$param=array('repository'=>array('idnumber'=>120,'name'=>'GMoodle antigo','shortname'=>'GM'),
				'resource'=>array('idnumber'=>121,'name'=>'O que é','shortname'=>'OQ','dtype'=>'content','btype'=>'page')
	);
	$this->syncLms($param);
}
  public function syncLms($param){
		$kparam=$this->getUtildata()->getVaueOfArray($param,'_kparam');
		if(!empty($kparam) && is_array($kparam)){$param=$kparam;}
		
		//create repository
		$param['repository']['id']=$this->createRepository($param);
		//create resource
		$repositoryid=$this->createResource($param);
		echo $repositoryid;exit;
		

	return null;
 }

 public function createRepository($param){
		$repositoryidnumber=$this->getUtildata()->getVaueOfArray($param,'repository.idnumber',true);
		$repositoryname=$this->getUtildata()->getVaueOfArray($param,'repository.name',true);
		$repositoryshortname=$this->getUtildata()->getVaueOfArray($param,'repository.shortname',true);
		$entity=$this->getEntity();

		$statusid=$this->getContainer()->get('badiu.admin.cms.repositorystatus.data')->getIdByShortname($entity,'active');
		$repositorydata=$this->getContainer()->get('badiu.admin.cms.repository.data');
		$paramcheckexist=array('entity'=>$entity,'idnumber'=>$repositoryidnumber);
		$paramadd=array('entity'=>$entity,'idnumber'=>$repositoryidnumber,'statusid'=>$statusid,'name'=>$repositoryname,'deleted'=>0,'timecreated'=>new \DateTime());
		$paramedit=array('entity'=>$entity,'idnumber'=>$repositoryidnumber,'statusid'=>$statusid,'name'=>$repositoryname,'deleted'=>0,'timemodified'=>new \DateTime());
		$result=$repositorydata->addNativeSql($paramcheckexist,$paramadd,$paramedit); 
		$repositoryid=$this->getUtildata()->getVaueOfArray($result,'id');
		return $repositoryid;

}

public function createResource($param){
		$idnumber=$this->getUtildata()->getVaueOfArray($param,'resource.idnumber',true);
		$name=$this->getUtildata()->getVaueOfArray($param,'resource.name',true);
		$shortname=$this->getUtildata()->getVaueOfArray($param,'resource.shortname',true);
		$dtype=$this->getUtildata()->getVaueOfArray($param,'resource.dtype',true);
		$btype=$this->getUtildata()->getVaueOfArray($param,'resource.btype',true);
		$modulekey=$this->getUtildata()->getVaueOfArray($param,'resource.modulekey',true);
		$moduleinstance=$this->getUtildata()->getVaueOfArray($param,'resource.moduleinstance',true);
		$repositoryid=$this->getUtildata()->getVaueOfArray($param,'repository.id',true);
		$entity=$this->getEntity();
		$statusid=$this->getContainer()->get('badiu.admin.cms.resourcestatus.data')->getIdByShortname($entity,'active');
		
		$resourcemanage=$this->getContainer()->get('badiu.admin.cms.resourece.manage');
		$resourcedata=$this->getContainer()->get('badiu.admin.cms.resource.data');
		
		$paramcheckexist=array('entity'=>$entity,'repositoryid'=>$repositoryid,'idnumber'=>$idnumber);
		
		$paramadd=array('entity'=>$entity,'repositoryid'=>$repositoryid,'idnumber'=>$idnumber,'statusid'=>$statusid,'name'=>$name,'dtype'=>$dtype,'btype'=>$btype,'deleted'=>0,'timecreated'=>new \DateTime());
		$paramadd=$resourcemanage->startNewResource($paramadd);

		$paramedit=array('entity'=>$entity,'repositoryid'=>$repositoryid,'idnumber'=>$idnumber,'statusid'=>$statusid,'name'=>$name,'dtype'=>$dtype,'btype'=>$btype,'deleted'=>0,'timemodified'=>new \DateTime());
		$result=$resourcedata->addNativeSql($paramcheckexist,$paramadd,$paramedit); 
		$resoureceid=$this->getUtildata()->getVaueOfArray($result,'id');
		$paramadd['id']=$resoureceid;

		$resourcemanage->updateKeys($resourcedata,$paramadd);
		return $resoureceid;

}
}
