<?php

namespace Badiu\Admin\CmsBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSchedulerExpression;

class FactoryExpression extends BadiuSchedulerExpression {
 
	  function __construct(Container $container) {
        parent::__construct($container);
     
    } 
	
	public function initTblexpresion(){
     $this->add('title','RESOURCE_TITLE');
     $this->add('abstract','RESOURCE_ABSTRACT');
	 $this->add('abstractordescription','RESOURCE_ABSTRACT_OR_DESCRIPTION');
     $this->add('header','RESOURCE_HEADER');
	 $this->add('content','RESOURCE_CONTENT');
     $this->add('footer','RESOURCE_FOOTER');
	 $this->add('description','RESOURCE_DESCRIPTION');
     $this->add('defaultimageurl','RESOURCE_DEFAULTIMAGEURL');
	 $this->add('contentimageurl','RESOURCE_CONTENTIMAGEURL');
	 $this->add('defaultimagesrc','RESOURCE_DEFAULTIMAGESRC');
	 $this->add('defaultimageview','RESOURCE_DEFAULTIMAGEVIEW');
	 //review
	 $this->add('contentvideo','RESOURCE_CONTENTVIDEO');
	 
	 $this->add('defaulturl','RESOURCE_DEFAULTURL');
	 
	 $this->add('struturemenu','RESOURCE_STRUTURE_MENU');
	 $this->add('struturemenupartial','RESOURCE_STRUTURE_MENU_PARTIAL');
	 $this->add('struturemenu','RESOURCE_SUBSTRUTURE_MENU');
    
	$this->add('templatecustomcss','RESOURCE_TEMPALTE_CSS');
    $this->add('templatecustomjs','RESOURCE_TEMPALTE_JS');
	 
	 $this->add('customcss','RESOURCE_CSS');
     $this->add('customjs','RESOURCE_JS');
	 $this->add('itemmager','RESOURCE_ITEMMANAGER');
	 $this->add('itemedit','RESOURCE_ITEM_EDIT');
	 
	  $this->add('titlelinkviewitem','RESOURCE_TITLE_LINK_VIEW_ITEM');
	  $this->add('linkviewitem','RESOURCE_LINK_VIEW_ITEM');	 
	  $this->add('urlviewitem','RESOURCE_URL_VIEW_ITEM');

	 $this->add('titlelinkviewitemversionrealiase','RESOURCE_TITLE_LINK_VIEW_ITEM_VERSION_REALEASE');
	 $this->add('linkviewitemversionrealiase','RESOURCE_LINK_VIEW_ITEM_VERSION_REALEASE');	 
	 $this->add('urlviewitemversionrealiase','RESOURCE_URL_VIEW_ITEM_VERSION_REALEASE');		  
	 
	 $this->add('fileurl','RESOURCE_FILE_URL');
	 $this->add('filename','RESOURCE_FILE_NAME');
	 $this->add('filelink','RESOURCE_FILE_LINK');
	 
	  $this->add('filepackagewebviewurl','RESOURCE_FILE_PACKAGE_WEVIEW_URL');
	  $this->add('filepackagewebviewlink','RESOURCE_FILE_PACKAGE_WEVIEW_LINK');
	  $this->add('filepackageiframe','RESOURCE_FILE_PACKAGE_IFRAME');
	
	
	  $this->add('faqaccordioncontent','RESOURCE_FAQ_VIEW_ACCORDION');
	
  }

}

