<?php

namespace Badiu\Admin\CmsBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSchedulerExpression;

class FactoryTemplate extends BadiuSchedulerExpression {
 
	  function __construct(Container $container) {
        parent::__construct($container);
     
    } 
	
	public function makeContent($param){
		$routerlib = $this->getContainer()->get('badiu.system.core.lib.config.routerlib');
		$key=$routerlib->getKey();
		$detailview=false;
		if($key=='badiu.admin.cms.resourcecontentfviewitem.dashboard'){$detailview=true;}
		$contentdetail=$this->getUtildata()->getVaueOfArray($param,'templatecontent');
		$listabostratcontent=$this->getUtildata()->getVaueOfArray($param,'listabstracttemplatecontent');
		$content=$contentdetail;
		if(!empty($listabostratcontent) && !$detailview){$content=$listabostratcontent;}
		$content=$this->cleanHtmlContent($content);
	
     $factoryexpression=$this->getContainer()->get('badiu.admin.cms.resourece.factoryexpression');
	 $factoryexpression->init($content,$param);
	 $factoryexpression->replace();
     $content=$factoryexpression->getMessage();
	
	return $content;
  }

public function cleanHtmlContent($htmlcontent){
	$urlbase=$this->getUtilapp()->getBaseUrl();
	$tagchange=$urlbase."/system/file/get/{RESOURCE_DEFAULTIMAGEURL}";
	$htmlcontent=str_replace($tagchange,"{RESOURCE_DEFAULTIMAGEURL}",$htmlcontent);
	
	$tagchange=$urlbase."/system/file/get/{RESOURCE_DEFAULTURL}";
	$htmlcontent=str_replace($tagchange,"{RESOURCE_DEFAULTURL}",$htmlcontent);
	return $htmlcontent;

}

 public function getItemAddUrl($dtype,$param) {
		if(empty($dtype)){return null;}
		$repositoryid=$this->getUtildata()->getVaueOfArray($param,'repositoryid');
		$mkey=$this->getUtildata()->getVaueOfArray($param,'_mkey');
		$labeladdnew=$this->getTranslator()->trans('badiu.admin.cms.resourcetype.'.$dtype.'addnew');
		$routekeyaddnew="badiu.admin.cms.resourcecontent$dtype.add";
		
		$linkaddnew=null;
		$permission=$this->getContainer()->get('badiu.system.access.permission');
		$urlgoback=$this->getUtilapp()->getCurrentUrl();
		$fparam=array('parentid'=>$repositoryid,'_mkey'=>$mkey,'_urlgoback'=>urlencode($urlgoback));
		if($permission->has_access($routekeyaddnew,$this->getSessionhashkey())){$linkaddnew=$this->getUtilapp()->getUrlByRoute($routekeyaddnew,$fparam);}
		
		if(empty($linkaddnew)){return null;}
		
		$item=array('label'=>$labeladdnew,'url'=>$linkaddnew);
		return $item;
 }
 public function getListItemsAddUrl($param) {
	  $list=array();
	  $page=$this->getItemAddUrl('page',$param);
	  $file=$this->getItemAddUrl('file',$param);
	  $image=$this->getItemAddUrl('image',$param);
	  $card=$this->getItemAddUrl('card',$param);
	  $videoplaylist=$this->getItemAddUrl('videoplaylist',$param);
	  $faq=$this->getItemAddUrl('faq',$param);
	  
	  $enablepage=$this->getUtildata()->getVaueOfArray($param,'enable.page',true);
	  $enablefile=$this->getUtildata()->getVaueOfArray($param,'enable.file',true);
	  $enableimage=$this->getUtildata()->getVaueOfArray($param,'enable.image',true);
	  $enablecard=$this->getUtildata()->getVaueOfArray($param,'enable.card',true);
	  $enablevideoplaylist=$this->getUtildata()->getVaueOfArray($param,'enable.videoplaylist',true);
	  $enablefaq=$this->getUtildata()->getVaueOfArray($param,'enable.faq',true);
	   
	  
	  if(!empty($page) && $enablepage){array_push($list,$page);}
	  if(!empty($file) && $enablefile){array_push($list,$file);}
	  if(!empty($image) && $enableimage){array_push($list,$image);}
	  if(!empty($card) && $enablecard){array_push($list,$card);}
	  if(!empty($videoplaylist) && $enablevideoplaylist){array_push($list,$videoplaylist);}
	  if(!empty($faq) && $enablefaq){array_push($list,$faq);}
	 return $list;
	  
  }
  
  
}