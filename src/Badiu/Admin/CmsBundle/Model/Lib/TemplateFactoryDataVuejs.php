<?php
namespace Badiu\Admin\CmsBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class TemplateFactoryDataVuejs extends  BadiuModelLib {
    
	private $list;   
	private $struture;
	private $out;     
	public function __construct(Container $container) {
		parent::__construct($container);
		$this->out=array();
		$this->list=array();
		$this->struture=array();
  }


	public function makeData() {
		$jsdata=array();
		$this->out['formoptions']['boolean']=$this->getDefaultBoolean();
		if(!is_array($this->list)){return null;}

		foreach ($this->getList()  as $row){
				$dkey = $this->getUtildata()->getVaueOfArray($row, 'dkey');
				$dconfig=$this->getUtildata()->getVaueOfArray($row,'dconfig');
				$dconfig = $this->getJson()->decode($dconfig, true);
				$row['dconfig']=$dconfig;
				$jsdata[$dkey]=$row;
		}
		

			  $this->out['struture']=$this->getStruture();
			 $this->out['resources']=$jsdata;
			$js=	$dconfig = $this->getJson()->encode($this->out);
			return $js;
	}
	 
	public function getDefaultBoolean() {
			$list=array();
			$option1=array('text'=>'Verdadeiro','value'=>1);
			$option2=array('text'=>'Falso','value'=>0);
			array_push($list,	$option1);
			array_push($list,	$option2);
			return $list;
			
	}

	public function getList() {
		return $this->list;
	}
	public function setList($list) {
		$this->list = $list;
	}

	public function getOut() {
		return $this->out;
	}
	public function setOut($out) {
		$this->out = $out;
	}

	public function getStruture() {
		return $this->struture;
	}
	public function setStruture($struture) {
		$this->struture = $struture;
	}
}
