<?php

namespace Badiu\Admin\CmsBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FactoryFolderContent extends BadiuModelLib {

	  function __construct(Container $container) {
        parent::__construct($container);
     
    } 
	
	public function makeContent($listexpression,$content){
		if(!is_array($listexpression)){return null;}
		$newlist=array();
	
		foreach ($listexpression as $key => $value) {
			$expression=$this->getUtildata()->getVaueOfArray($value,'currentexpression');
			$shortname=$this->getShortnameInExpression($expression);
			$listcontent=$this->getContentByShortname($shortname);
			$htmlcontent=$this->formtContent($listcontent,$shortname);
			$value['content']=$htmlcontent;
			$newlist[$key]=$value;
			
		}
	$content=$this->chengeContent($newlist,$content);
	return $content;
  }
  public function chengeContent($listexpression,$content){
		if(!is_array($listexpression)){return $content;}
		foreach ($listexpression as $key => $value) {
			$expression=$this->getUtildata()->getVaueOfArray($value,'tempexpression');
			$contentmaked=$this->getUtildata()->getVaueOfArray($value,'content');
			$content=str_replace($expression,$contentmaked,$content);
		}
		return $content;
  }
	public function getShortnameInExpression($value){
		$key=null;
		$value=str_replace("{","",$value);
		$value=str_replace("}","",$value);
		$p=explode("_",$value);
		if(!is_array($p)){return null;}
		$lastelem=end($p);
		return $lastelem;
	}
		

	public function getContentByShortname($shortname){
		$entity=$this->getEntity();
		//get id by shortname
		$folderid=$this->getContainer()->get('badiu.admin.cms.resourcestructure.data')->getIdByShortname($entity,$shortname);
		if(empty($folderid)){return null;}
		$resourcedata=$this->getContainer()->get('badiu.admin.cms.resource.data');
		$result=$resourcedata->getByStrutureid($entity,$folderid);
		return $result;
	}
	
	public function formtContent($list,$shortname){
		$result=$this->formtContentTemplate($list,$shortname);
		if(empty($result)){$result=$this->formtContentDefault($list,$shortname);}
		return $result;
	}
	
	public function formtContentDefault($contents,$shortname){
		
		if(empty($contents)){return null;}
		if(!is_array($contents)){return null;}
		$html="";
		$html.="<div class=\"$shortname row my-4\" >"; //row my-4 for card
		
		
		foreach ($contents as $row) {
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			$title=$this->getUtildata()->getVaueOfArray($row,'name');
			$abstract=$this->getUtildata()->getVaueOfArray($row,'abstract');
			$description=$this->getUtildata()->getVaueOfArray($row,'description');
			$dtype=$this->getUtildata()->getVaueOfArray($row,'dtype');
			$btype=$this->getUtildata()->getVaueOfArray($row,'btype');
			$customchar1=$this->getUtildata()->getVaueOfArray($row,'customchar1');
			$customint1=$this->getUtildata()->getVaueOfArray($row,'customint1');
			$content=$this->getUtildata()->getVaueOfArray($row,'content');
		
		 $fparam=array('parentid'=>$id);
		 $link=$this->getUtilapp()->getUrlByRoute('badiu.admin.cms.resourcecontentfviewitem.dashboard',$fparam);
		 
		  if($dtype=='content' && $btype=='file' && $customint1==1 && !empty($customchar1)){
			$p=explode("/",$content);
			$authkey=$this->getUtildata()->getVaueOfArray($p,0);
			$filename=$this->getUtildata()->getVaueOfArray($p,1);
			$filedata=$this->getContainer()->get('badiu.system.file.file.data');
			$info=$filedata->getInfoByAuthkey($authkey);
			$fpath=$this->getUtildata()->getVaueOfArray($info,'fpath');
			$filepath=$fpath."/".$authkey."_packge/$customchar1";
			$webview=$this->getUtilapp()->getWebviewUrl($filepath);
			//$webviewlink="<a href=\"$webview\"  target=\"_blank\">$filename</a>";
			 $html.="<b><a href=\"$webview\"  target=\"_blank\">$title</a></b>";
		  }else if($dtype=='content' && $btype=='faq'){
			  
			   $resourceid=$this->getUtildata()->getVaueOfArray($row,'id');
			    $name=$this->getUtildata()->getVaueOfArray($row,'name');
				$fparam=array('resourceid'=>$resourceid,'templatelist'=>1,'name'=>$name);
				$faqcontent=$this->getContainer()->get('badiu.admin.cms.resourcecontentfaq.lib.factorycontent')->viewAccordion($fparam);
				
				$html.="<div class=\"col\">$faqcontent</div>";
		  }else if($dtype=='content' && $btype=='image'){
			   $content=$this->getUtildata()->getVaueOfArray($row,'content');
			   $url= $this->getUtilapp()->getFileUrl($content);
			   $resourceid=$this->getUtildata()->getVaueOfArray($row,'id');
			   $name=$this->getUtildata()->getVaueOfArray($row,'name');
			   $description=$this->getUtildata()->getVaueOfArray($row,'description');
			   $defaulturl=$this->getUtildata()->getVaueOfArray($row,'defaulturl');
			  
				$html.="<div class=\"col-sm-6 col-md-3 mb-2\">";
                $html.="<div class=\"card-content\">";
				  $html.="<div>";
                   $html.="<div class=\"card-img\">";
                     $html.="<img src=\"$url\" class=\"card-image\" alt=''>";
				   $html.="</div>";
				    $html.="<div class=\"card-desc\"></div>";
			    $html.="</div>";
				$html.="</div>";
				$html.="</div>";
				
		  }
		 else{$html.="<b><a href=\"$link\">$title</a></b>";}
		 
		 //review
		 if($btype!='image'){
			 if(empty($abstract)){$abstract=$description;}
			 $html.="<div class=\"abstract\">$abstract</div>";
		 }
		
		}
		$html.="</div>";
	
		return $html;
	}
	 
	public function formtContentTemplate($contents,$shortname){
		if(empty($contents)){return null;}
		if(!is_array($contents)){return null;}
		$entity=$this->getEntity();
		$templateid=$this->getContainer()->get('badiu.admin.cms.resourcestructure.data')->getTemplateidByShortname($entity,$shortname);
		$templatedata=$this->getContainer()->get('badiu.admin.cms.template.data');
		
		$fparam=array('id'=>$templateid);
		$templateinfo=$templatedata->getGlobalColumnsValue('o.content,o.customcss,o.customjs',$fparam);
		
		$templatecontent=$this->getUtildata()->getVaueOfArray($templateinfo,'content');
		$templatecustomcss=$this->getUtildata()->getVaueOfArray($templateinfo,'customcss');
		$templatecustomjs=$this->getUtildata()->getVaueOfArray($templateinfo,'customjs');
		if(empty($templatecontent)){return null;}
		$resoureceformat=$this->getContainer()->get('badiu.admin.cms.resourece.format');
		
		$html="";
		$html.="<div class=\"$shortname\">";
		foreach ($contents as $row) {
			$row['templatecontent']=$templatecontent;
			$row['templatecustomcss']=$templatecustomcss;
			$row['templatecustomjs']=$templatecustomjs;
			$content=$resoureceformat->content($row);
			$html.=$content;
			
		}
		$html.="</div>";
		
		return $html;
	}
	

}