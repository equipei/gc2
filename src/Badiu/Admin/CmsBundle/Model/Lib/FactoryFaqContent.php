<?php

namespace Badiu\Admin\CmsBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FactoryFaqContent extends BadiuModelLib {

	  function __construct(Container $container) {
        parent::__construct($container);
     
    } 


	function viewAccordion($param) {
		$resourceid=$this->getUtildata()->getVaueOfArray($param,'resourceid');
		$templatelist=$this->getUtildata()->getVaueOfArray($param,'templatelist');
		$faqname=$this->getUtildata()->getVaueOfArray($param,'name');
		$mkey=$this->getUtildata()->getVaueOfArray($param,'mkey');
		if(empty($resourceid)){
			$resourceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
			$vkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('vkey');
			$vrelease=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('vrelease');
			
			if(!empty($vkey) && !empty($vrelease)){
				$vparam=array();
				$vparam['repositoryid']=$resourceid;
				$vparam['vkey']=$vkey;
				$vparam['vrelease']=$vrelease;
				$resourcedata=$this->getContainer()->get('badiu.admin.cms.resource.data');
				$resourceid=$resourcedata->getGlobalColumnValue('id',$vparam);
			}
			
		}
		if(empty($mkey)){$mkey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_mkey');}
		$fitemdata=$this->getContainer()->get('badiu.admin.cms.resourcecontentfaqitem.data');
		
		$fparam=array('resourceid'=>$resourceid,'deleted'=>0,'_orderby'=>' ORDER BY o.sortorder ');
		$list=$fitemdata->getGlobalColumnsValues('o.id,o.name,o.content',$fparam);
		
		 if(!is_array($list)){return null;}
		
		$linkaddnew=null;
		$canedit=false;
		$permission=$this->getContainer()->get('badiu.system.access.permission');
		$urlgoback=$this->getUtilapp()->getCurrentUrl();
		$fparam=array('parentid'=>$resourceid,'_mkey'=>$mkey,'_urlgoback'=>urlencode($urlgoback));
		if($permission->has_access('badiu.admin.cms.resourcecontentfaqitem.add',$this->getSessionhashkey())){$linkaddnew=$this->getUtilapp()->getUrlByRoute('badiu.admin.cms.resourcecontentfaqitem.add',$fparam);}
		if(!empty($linkaddnew)){
			$label=$this->getTranslator()->trans('badiu.admin.cms.resourcecontentfaqitem.add');
			$linkaddnew="<a href=\"$linkaddnew\" class=\"btn btn-primary faqdd\"  role=\"button\">$label</a>";
		}
		if($permission->has_access('badiu.admin.cms.resourcecontentfaqitem.edit',$this->getSessionhashkey())){$canedit=true;}
		 $html="";
		 if($templatelist){$html.="<h3 class=\"faqtitle\">$faqname</h3>";}
		 $html.=$linkaddnew;
		 $html.="<div id=\"accordionfaq$resourceid\" class=\"accordion accordion-faq\">";
		
		$cont=0;
		$open='false';
		$show='';
		 foreach ($list as $row) {
			 $id=$this->getUtildata()->getVaueOfArray($row,'id');
			 $name=$this->getUtildata()->getVaueOfArray($row,'name');
			 $content=$this->getUtildata()->getVaueOfArray($row,'content');
			 if($cont==0){$open='true';$show= " show ";}
			 else{$open='false';$show= " ";}
			 if($templatelist){$open='false';$show= " ";}
			 $linkedit="";
			 if($canedit){
				 $fparam['id']=$id;
				 $linkedit=$this->getUtilapp()->getUrlByRoute('badiu.admin.cms.resourcecontentfaqitem.edit',$fparam);
				 $linkedit="<a href=\"$linkedit\"><i class=\"fa fa-edit\"></i></a>";
			 }
			 $html.=" <div class=\"card\">";
			 $html.="  <div id=\"faq$id\" class=\"card-header\">";
			 $html.="<h2 class=\"mb-0 acoordionquestion\" data-toggle=\"collapse\" data-target=\"#collapse$id\" aria-expanded=\"$open\" aria-controls=\"collapse$id\"> $name</h2>";
			 $html.=" </div>";
			 
			$html.=" <div id=\"collapse$id\" class=\"collapse $show\" aria-labelledby=\"faq$id\" data-parent=\"#accordionfaq$resourceid\">";
			$html.="<div class=\"card-body\">$content $linkedit </div>";
			$html.="</div>";
			$html.="</div> ";
			 $cont++;
		 }
		 $html.="</div>";
		return $html;
		
	}

}