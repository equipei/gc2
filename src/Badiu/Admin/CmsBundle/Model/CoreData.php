<?php
namespace Badiu\Admin\CmsBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CoreData extends BadiuDataBase
{

    public function __construct(Container $container, $bundleEntity)
    {
        parent::__construct($container, $bundleEntity);
    }

    
 public function desableRelease($id,$vkey) {
    $sql="UPDATE  ".$this->getBundleEntity()." o SET o.vrelease=:vrelease  WHERE o.id!=:id AND o.vkey=:vkey";
    $query = $this->getEm()->createQuery($sql);
    $query->setParameter('id', $id);
    $query->setParameter('vkey', $vkey);
    $query->setParameter('vrelease', FALSE);
    $result=$query->execute();
    return $result;
}


public function getByStrutureid($entity,$structureid) {
    $sql="SELECT o.id,o.name,o.content,o.defaultimage,o.abstract,o.shortname,o.edition,o.vrelease,o.vkey,o.dkey,o.skey,o.idnumber,o.description,o.defaultimage,o.defaulturl,o.sortorder,o.customcss,o.customjs,t.name AS templatename,t.content AS templatecontent,t.customcss AS templatecustomcss,t.customjs AS templatecustomjs,lat.content AS listabstracttemplatecontent,lat.customcss AS listabstracttemplatecustomcss,lat.customjs AS listabstracttemplatecustomjs,o.dtype,o.btype ,o.param,o.timecreated,o.timemodified,o.deleted,st.id AS strutureid,r.id AS repositoryid,o.customint1,o.customchar1  FROM ".$this->getBundleEntity()." o JOIN o.repositoryid r LEFT JOIN o.templateid t LEFT JOIN o.listabstracttemplateid lat  LEFT JOIN o.structureid st   WHERE o.entity=:entity AND o.structureid=:structureid  ORDER BY o.sortorder";
    $query = $this->getEm()->createQuery($sql);
	$query->setParameter('entity',$entity);
    $query->setParameter('structureid',$structureid);
    $result= $query->getResult();
    return  $result;
}

public function getInfoStatus($entity,$id) {
    $sql="SELECT o.id,s.shortname AS statusshortname,o.deleted  FROM ".$this->getBundleEntity()." o JOIN o.statusid s   WHERE o.entity=:entity AND o.id=:id";
    $query = $this->getEm()->createQuery($sql);
	$query->setParameter('entity',$entity);
    $query->setParameter('id',$id);
    $result= $query->getOneOrNullResult();
    return  $result;
}

public function getShortInfoList($entity,$param) {
	$struturepath=null;
	$onlychidrens=null;
	if(array_key_exists('_struturepath', $param)){$struturepath=$param['_struturepath'];unset($param['_struturepath']);}
	if(array_key_exists('_onlychidrens', $param)){$onlychidrens=$param['_onlychidrens'];unset($param['_onlychidrens']);}
	$wsql=$this->makeSqlWhere($param);
	if(!empty($struturepath) && empty($onlychidrens)){$wsql.= " AND ( st.idpath = '".$struturepath."'  OR st.idpath LIKE '".$struturepath.".%') ";}
	else if(!empty($struturepath) && !empty($onlychidrens)){$wsql.= " AND st.idpath LIKE '".$struturepath.".%' ";}
    $sql="SELECT o.id,o.name,o.vkey,st.id AS strutureid  FROM ".$this->getBundleEntity()." o JOIN o.repositoryid r  JOIN o.statusid s LEFT JOIN o.structureid st   WHERE o.entity=:entity  $wsql ORDER BY o.sortorder ";
    $query = $this->getEm()->createQuery($sql);
	$query->setParameter('entity',$entity);
    $query=$this->makeSqlFilter($query, $param);
    $query->setMaxResults(2500);
    $result= $query->getResult();
    return  $result;
}
/*
public function getShortInfoListWitoutStruture($entity,$param) {
	$wsql=$this->makeSqlWhere($param);
    $sql="SELECT o.id,o.name,o.vkey  FROM ".$this->getBundleEntity()." o JOIN o.repositoryid r  JOIN o.statusid s WHERE o.entity=:entity  $wsql AND o.structureid IS NULL ORDER BY o.sortorder ";
    $query = $this->getEm()->createQuery($sql);
	$query->setParameter('entity',$entity);
    $query=$this->makeSqlFilter($query, $param);
    $query->setMaxResults(2500);
    $result= $query->getResult();
    return  $result;
}*/
}
