<?php

namespace Badiu\Admin\CmsBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class ResourceFormat extends BadiuFormat{
		private $factorytemplate;
		private $configformat;
		private $permission;
		private  $filelib;
		private  $formdataoptions;
		private $baseresoursepath;
		private $factorysreviceexpression;
		private $factorymarkdown;
      function __construct(Container $container) {
            parent::__construct($container);
			$this->factorytemplate=$this->getContainer()->get('badiu.admin.cms.resourece.factorytemplate');
			$this->configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
			$this->permission=$this->getContainer()->get('badiu.system.access.permission');
			$this->filelib=$this->getContainer()->get('badiu.system.file.file.lib');
			$this->formdataoptions=$this->getContainer()->get('badiu.admin.cms.core.formdataoptions');
			$this->baseresoursepath= $this->getContainer()->get('templating.helper.assets')->getUrl('bundles/badiuthemecore');
			$this->factoryserviceexpression=$this->getContainer()->get('badiu.system.core.lib.util.factoryserviceexpression');
			$this->factorymarkdown=$this->getContainer()->get('badiu.system.core.lib.markdow.factorycontent');
       } 


	
    public  function itemmager($data){

			$strutureid=$this->getUtildata()->getVaueOfArray($data,'strutureid');
			$folderpath=$this->configformat->treepathname($strutureid);
			$data['folderpath']=$folderpath;
			
          $htmlcontent="";
          $container=$this->getContainer();
          $furl=$this->getUtilapp()->getFilePath("BadiuAdminCmsBundle:View/Template/Default/ItemManager.php");
          ob_start();
            include($furl);
            $htmlcontent = ob_get_contents();
          ob_end_clean();
          return $htmlcontent;
   }
   
    public  function info($data){
          $htmlcontent="";
          $container=$this->getContainer();
          $furl=$this->getUtilapp()->getFilePath("BadiuAdminCmsBundle:View/Template/Admin/GeneralInfo.php");
          ob_start();
            include($furl);
            $htmlcontent = ob_get_contents();
          ob_end_clean();
          return $htmlcontent;
   }

	public  function replacefolderitems($data){
		$content=$this->getUtildata()->getVaueOfArray($data,'content');
		$managestring=$this->getContainer()->get('badiu.system.core.lib.util.managestring');
		$content=$managestring->getExpressions($content,'{RESOURCE_FOLDER_ITEMS_','}');
		
		$factoryfoldercontent=$this->getContainer()->get('badiu.admin.cms.resourcestructure.lib.factoryfoldercontent');
		$content=$factoryfoldercontent->makeContent($managestring->getPcontent(),$content);
		$data['content']=$content;
		return  $data;
		
	}

	public  function webviewurl($data){
		
		 $dtype=$this->getUtildata()->getVaueOfArray($data,'dtype');
		 $shortname=$this->getUtildata()->getVaueOfArray($data,'shortname');
		 $btype=$this->getUtildata()->getVaueOfArray($data,'btype');
		 $customchar1=$this->getUtildata()->getVaueOfArray($data,'customchar1');
		 $customint1=$this->getUtildata()->getVaueOfArray($data,'customint1');
		 $content=$this->getUtildata()->getVaueOfArray($data,'content');
		 $name=$this->getUtildata()->getVaueOfArray($data,'name');
		 
		 if($dtype=='content' && $btype=='file' && $customint1==1 && !empty($customchar1)){
			$content=$this->getUtildata()->getVaueOfArray($data,'content');
			$p=explode("/",$content);
			$authkey=$this->getUtildata()->getVaueOfArray($p,0);
			$filename=$this->getUtildata()->getVaueOfArray($p,1);
			$filedata=$this->getContainer()->get('badiu.system.file.file.data');
			$info=$filedata->getInfoByAuthkey($authkey);
			//$fpath=$this->getUtildata()->getVaueOfArray($info,'fpath');
			
			$entity=$this->getUtildata()->getVaueOfArray($info,'entity');
			$fparam=array('entity'=>$entity);
			//$defaultpath =  $this->getContainer()->getParameter('badiu.system.file.defaultpath');
			
			$basepath = $this->getContainer()->get('badiu.system.file.file.lib')->getBasePath($fparam);
			$filepath=$basepath.'/'.$authkey."_packge/$customchar1";
			$webview=$this->getUtilapp()->getWebviewUrl($filepath);
			$webviewlink="<a href=\"$webview\"  target=\"_blank\">$filename</a>";
			$data['filepackagewebviewurl']=$webview;
			$data['filepackagewebviewlink']=$webviewlink;
			$iframe="<iframe id=\"packagefilecontent-$shortname\" class=\"packagefilecontent\" src=\"$webview\" ></iframe>";
			$data['filepackageiframe']=$iframe;
			
		 }
		 	
		return  $data;
		
	}	
public  function content($data){
	$data=$this->replacefolderitems($data);
	
     $dtype=$this->getUtildata()->getVaueOfArray($data,'dtype');
     $btype=$this->getUtildata()->getVaueOfArray($data,'btype');
	
	 $title=$this->getUtildata()->getVaueOfArray($data,'name');
	 $data['title']=$title;
	 $currentstrutureid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('currentstrutureid');
	 if(!empty($currentstrutureid)){
		 $resourcestructuredata=$this->getContainer()->get('badiu.admin.cms.resourcestructure.data');
		 $data['title']=$resourcestructuredata->getGlobalColumnValue('name',array('id'=>$currentstrutureid));
	 }
	 
	 $id=$this->getUtildata()->getVaueOfArray($data,'id');
	 $url=$this->itemviewurl($data);
	 if(!empty($title) && !empty($id)){
		$titlelink="<a class=\"resource-title-link\" href=\"$url\">$title</a>";
		$data['titlelinkviewitem']=$titlelink;
	 }
	 $data['linkviewitem']="<a class=\"resource-title-link\" href=\"$url\">$url</a>";
	 $data['urlviewitem']=$url;
	 
	 $urlversionrealease=$this->itemviewurl($data,'versionrealease');
	 $titleversionrealease="<a class=\"resource-title-link\" href=\"$urlversionrealease\">$title</a>";
	 $linkversionrealease="<a class=\"resource-url-link\" href=\"$urlversionrealease\">$urlversionrealease</a>";
	 $data['titlelinkviewitemversionrealiase']=$titleversionrealease;
	 $data['linkviewitemversionrealiase']=$linkversionrealease;
	 $data['urlviewitemversionrealiase']=$urlversionrealease;
	 //replace file
	  $content=$this->getUtildata()->getVaueOfArray($data,'content');
	  $description=$this->getUtildata()->getVaueOfArray($data,'description');
	  $templatecontent=$this->getUtildata()->getVaueOfArray($data,'templatecontent');
	  $content=$this->filelib->addUrl($content);
	  $description=$this->filelib->addUrl($description);
	  $templatecontent=$this->filelib->addUrl($templatecontent);
	  
	  $content=$this->factoryserviceexpression->change($content);
	  $content=$this->factorymarkdown->change($content);
	  
	  $data['content']=$content;	
	  $data['description']=$description;
	  $data['templatecontent']=$templatecontent;
	  
	 //abstractordescription
	 $data['abstractordescription']=$this->abstractordescription($data);
	  
	 $defaultimageurl=$this->defaultimageurl($data);
	 $data['defaultimageurl']=$defaultimageurl;
	 $data['defaultimagesrc']=" src =\"$defaultimageurl\" ";
	 $data['defaultimageview']="<img src =\"$defaultimageurl\" class=\"defaultimageview\" />";
	 $data['defaulturl']=$this->defaulturl($data);
	
	//page video revie after
	 $data['contentvideo']="";
	if($dtype=='content' && $btype=='page'){
		$subtype=$this->getUtildata()->getVaueOfArray($data,'customchar1');
		$subtypedata=$this->getUtildata()->getVaueOfArray($data,'customchar2');
		$subtypecontent=$this->getUtildata()->getVaueOfArray($data,'customchar3');
		if(!empty($subtypecontent)){
			//use template to get this content
			if (strpos($subtypecontent, "player.vimeo.com") !== false) {$data['contentvideo']="<iframe src=\"$subtypecontent\" webkitallowfullscreen=\"\" mozallowfullscreen=\"\" allowfullscreen=\"\" frameborder=\"0\" height=\"338\" width=\"600\"></iframe>";}
			else if (strpos($subtypecontent, "awesomescreenshot.com") !== false) {$data['contentvideo']="<iframe src=\"$subtypecontent&info=false\" width=\"640\" height=\"360\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe>";}
			else if (strpos($subtypecontent, "youtube.com") !== false) {$data['contentvideo']="<iframe src=\"$subtypecontent\" width=\"560\" height=\"360\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe>";}
			else  { $data['contentvideo']="<iframe src=\"$subtypeconten\" width=\"640\" height=\"360\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe>";}
		}
	}
	
	
	$this->configformat->setColumn1('badiu.admin.cms.resourcestructure.data');
	$strutureid=$this->getUtildata()->getVaueOfArray($data,'strutureid');
	$folderpath=$this->configformat->treepathname($strutureid);
	$data['folderpath']=$folderpath;
	if($dtype=='content' && $btype=='card'){
		 $content=$this->getUtildata()->getVaueOfArray($data,'content');
		$content=$this->getJson()->decode($content,true);
		
		$data['header']=$this->getUtildata()->getVaueOfArray($content,'header');
		$data['content']=$this->getUtildata()->getVaueOfArray($content,'body');
		$data['footer']=$this->getUtildata()->getVaueOfArray($content,'footer');
	}else if($dtype=='content' && $btype=='image'){
		$defaultimageurl=$this->contentimageurl($data);
		if(empty($defaultimageurl)){$defaultimageurl=$this->defaultimageurl($data);}
		 $data['defaultimageurl']=$defaultimageurl;
		
	}else if($dtype=='content' && $btype=='file'){
		$defaultfileurl=$this->defaultfileurl($data);
		$data['fileurl']=$defaultfileurl;
		$content=$this->getUtildata()->getVaueOfArray($data,'content');
		$p=explode("/",$content);
		$filename=$this->getUtildata()->getVaueOfArray($p,1);
		$data['filename']=$filename;
		$data['filelink']="<a href=\"$defaultfileurl\">$filename</a>";
		
		$data=$this->webviewurl($data);
	} 
	else if($dtype=='content' && $btype=='faq'){
		$resourceid=$this->getUtildata()->getVaueOfArray($data,'resourceid');
		$fparam=array('resourceid'=>$resourceid);
		$faqcontent=$this->getContainer()->get('badiu.admin.cms.resourcecontentfaq.lib.factorycontent')->viewAccordion($fparam);
		$data['faqaccordioncontent']=$faqcontent;
		
	} 
	
	 $data['itemmager']=$this->itemmager($data);
	 $urledit=$this->urledit($data);
	 if(!empty( $urledit)){
		$linkdit="<a href=\"$urledit\"><i class=\"fa fa-edit\"></i></a>";
		$data['itemedit']=$linkdit;
	}	
	$data=$this->struturemenu($data);
	$data=$this->struturemenupartial($data);
	$data=$this->substruturemenu($data); 
	$content=$this->factorytemplate->makeContent($data);
	
    return $content;
}
 
public  function urledit($data){ 
			$btype=$this->getUtildata()->getVaueOfArray($data,'btype');
			$key="badiu.admin.cms.resourcecontent".$btype.".edit";
			$permissionedit=$this->permission->has_access($key,$this->getSessionhashkey());
			if(!$permissionedit){return null;}
			
			$id =$this->getUtildata()->getVaueOfArray($data,'id');
			$parentid =$this->getUtildata()->getVaueOfArray($data,'repositoryid');
			$urlgoback=$this->getUtilapp()->getCurrentUrl();
			$parammage=array('id'=>$id,'parentid'=>$parentid,'_urlgoback'=>urlencode($urlgoback));
			$editurl=$this->getUtilapp()->getUrlByRoute($key,$parammage);
			if(empty($editurl)){return  null;}
			
			return  $editurl;
} 

 public  function contentimage($data){
		$content=$this->getUtildata()->getVaueOfArray($data,'content');
		$imgurl=$this->getUtildata()->getVaueOfArray($data,'url');
		$url= $this->getUtilapp()->getFileUrl($content);
		$img="<img src=\"$url\" class=\"img-responsive\"  style=\"max-height: 400px;\" >";
		if(!empty($imgurl)){$img="<a href=\"$imgurl\">$img</a><br / >$imgurl";}
      return $img;
}

public  function contentimageurl($data){
		$content=$this->getUtildata()->getVaueOfArray($data,'content');
		$url= $this->getUtilapp()->getFileUrl($content);
     
      return $url;
}

 public  function defaultimage($data){
		$content=$this->getUtildata()->getVaueOfArray($data,'defaultimage');
		$url= $this->getUtilapp()->getFileUrl($content);
		$img="<img src=\"$url\" class=\"img-responsive\"  style=\"max-height: 150px;\" >";
	   return $img;
}
 public  function defaulturl($data){
		$defaulturl=$this->getUtildata()->getVaueOfArray($data,'defaulturl');
		if(empty($defaulturl)){return null;}
		$pos=stripos($defaulturl, "http");
        if($pos===0){return $defaulturl;}
		
		$base= $this->getUtilapp()->getBaseUrl();
		$url=$base.$defaulturl;
		
		return $url;
}
public  function defaultimageurl($data){
	
		$dtype=$this->getUtildata()->getVaueOfArray($data,'dtype');
		$btype=$this->getUtildata()->getVaueOfArray($data,'btype');
		$content=$this->getUtildata()->getVaueOfArray($data,'defaultimage');
		if($dtype=='content' &&  $btype=='image'){$content=$this->getUtildata()->getVaueOfArray($data,'content');}
		$url=null;
		if(empty($content)){$content=$this->getUtildata()->getVaueOfArray($data,'typedefaultimage');}
		if(empty($content)){$content=$this->getUtildata()->getVaueOfArray($data,'categorydefaultimage');}
		if(empty($content)){$content=$this->getUtildata()->getVaueOfArray($data,'struturedefaultimage');}
		
		if(empty($content) || $content=='null'){$url=$this->baseresoursepath."/image/default/admin-cms-resource-default-card.jpg"; }
		else{$url= $this->getUtilapp()->getFileUrl($content);}
	
		return $url;
}
public  function defaultfileurl($data){
		$content=$this->getUtildata()->getVaueOfArray($data,'content');
		$url= $this->getUtilapp()->getFileUrl($content);
      return $url;
}

public  function itemviewurl($data,$type='versionrealease'){
		$dtype=$this->getUtildata()->getVaueOfArray($data,'dtype');
		$btype=$this->getUtildata()->getVaueOfArray($data,'btype');
	 
		$id=$this->getUtildata()->getVaueOfArray($data,'id');
		$edition=$this->getUtildata()->getVaueOfArray($data,'edition');
		$vkey=$this->getUtildata()->getVaueOfArray($data,'vkey');
		$repositoryid=$this->getUtildata()->getVaueOfArray($data,'repositoryid');
		
		//$rparam=array('parentid'=>$id,'_mkey'=>'badiu.tms.my.studentfviewdefault.index');
		$rparam=array('parentid'=>$id);
		if($type=='versionrealease' && !empty($repositoryid)){
			$rparam['parentid']=$repositoryid;
			$rparam['vkey']=$vkey;
			$rparam['vrelease']=1;
		}
		$customchar1=$this->getUtildata()->getVaueOfArray($data,'customchar1');
		$customint1=$this->getUtildata()->getVaueOfArray($data,'customint1');
		
		
		$url= $this->getUtilapp()->getUrlByRoute('badiu.admin.cms.resourcecontentfviewitem.dashboard',$rparam);
		if($type=='versionrealease' && !empty($repositoryid)){
			$url= $this->getUtilapp()->getUrlByRoute('badiu.admin.cms.resourcecontentfviewitemversion.dashboard',$rparam);
		}
		if($dtype=='content' &&  $btype=='videoplaylist'){$url= $this->getUtilapp()->getUrlByRoute('badiu.admin.cms.resourcecontentvideolist.dashboard',$rparam);}
		
		if($dtype=='content' && $btype=='file' && $customint1==1 && !empty($customchar1)){
			 $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig'); 
			 $dconfig=$this->getJson()->decode($dconfig,true);
			 $confitemviewurl=$this->getUtildata()->getVaueOfArray($dconfig,'itemviewurl');
			 if( $confitemviewurl=='newwindowspackage'){
				 $dataf=$this->webviewurl($data);
				 $filepackagewebviewurl=$this->getUtildata()->getVaueOfArray($dataf,'filepackagewebviewurl');
				 if(!empty($filepackagewebviewurl)){$url=$filepackagewebviewurl;}
			 }
			  
		}
		return $url;
}

public  function itemviewurltarget($data){
		$dtype=$this->getUtildata()->getVaueOfArray($data,'dtype');
		$btype=$this->getUtildata()->getVaueOfArray($data,'btype');
		
		$customchar1=$this->getUtildata()->getVaueOfArray($data,'customchar1');
		$customint1=$this->getUtildata()->getVaueOfArray($data,'customint1');
		$response="";
		if($dtype=='content' && $btype=='file' && $customint1==1 && !empty($customchar1)){
			 $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig'); 
			 $dconfig=$this->getJson()->decode($dconfig,true);
			 $confitemviewurl=$this->getUtildata()->getVaueOfArray($dconfig,'itemviewurl');
			 if( $confitemviewurl=='newwindowspackage'){
				 $dataf=$this->webviewurl($data);
				 $filepackagewebviewurl=$this->getUtildata()->getVaueOfArray($dataf,'filepackagewebviewurl');
				 if(!empty($filepackagewebviewurl)){$response="_blank";}
			 }
			  
		}
		return $response;
}
public  function abstractordescription($data){
		$dtype=$this->getUtildata()->getVaueOfArray($data,'dtype');
		$btype=$this->getUtildata()->getVaueOfArray($data,'btype');
		
		 $abstractordescription=$this->getUtildata()->getVaueOfArray($data,'abstract');
		 if(empty($abstractordescription)){$abstractordescription=$this->getUtildata()->getVaueOfArray($data,'description');}
		 
		 if(empty($abstractordescription) && $dtype=='content' &&  $btype=='card'){
			  $content=$this->getUtildata()->getVaueOfArray($data,'content');
			  $content=$this->getJson()->decode($content,true);
			  $abstractordescription=$this->getUtildata()->getVaueOfArray($content,'body');
		}
		return $abstractordescription;
}

public  function abstractofcontent($data){
		$defaultimageurl=$this->defaultimageurl($data);
		$text=null;
		$content=$this->getUtildata()->getVaueOfArray($data,'content');
		$abstract=$this->getUtildata()->getVaueOfArray($data,'abstract');
		if(!empty($abstract)){$text=$abstract;}
		else {
			$text=$this->truncate($content, 250);
		}
		
		if(empty($defaultimageurl)){return $text;}

		$html="<div><img src=\"$defaultimageurl\" class=\"img-responsive\"  style=\"max-height: 150px; float: left;\">$text></div>";
		
      return $html;
}

public  function type($data){
		$dtype=$this->getUtildata()->getVaueOfArray($data,'dtype');
		$label=$this->formdataoptions->getTypeLabel($dtype);
       return $label;
		
}

public  function struturemenu($data){
	
		$content=$this->getUtildata()->getVaueOfArray($data,'content');
		$repositoryid=$this->getUtildata()->getVaueOfArray($data,'repositoryid');
		$pos=stripos($content, "{RESOURCE_STRUTURE_MENU}");
		
		if($pos=== false){return $data;}
		if(empty($repositoryid)){return $data;}
		
		$fparam=array('repositoryid'=>$repositoryid,'format'=>'defaulthtmllink');
		$strturelibcontent=$this->getContainer()->get('badiu.admin.cms.resourcestructure.lib.content');
		$menulist=$strturelibcontent->getTree($fparam);
		
		$content=str_replace("{RESOURCE_STRUTURE_MENU}",$menulist,$content);
		$data['content']=$content;
       return $data;
		
}

public  function struturemenupartial($data){
		$currentstrutureid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('currentstrutureid');
		if(empty($currentstrutureid)){return $data;}
		
		$content=$this->getUtildata()->getVaueOfArray($data,'content');
		$repositoryid=$this->getUtildata()->getVaueOfArray($data,'repositoryid');
		$pos=stripos($content, "{RESOURCE_STRUTURE_MENU_PARTIAL}");
		
		if($pos=== false){return $data;}
		if(empty($repositoryid)){return $data;}
		
		$fparam=array('repositoryid'=>$repositoryid,'format'=>'defaulthtmllink','partial'=>1,'currentstrutureid'=>$currentstrutureid);
		$strturelibcontent=$this->getContainer()->get('badiu.admin.cms.resourcestructure.lib.content');
		$menulist=$strturelibcontent->getTree($fparam);
	
		$content=str_replace("{RESOURCE_STRUTURE_MENU_PARTIAL}",$menulist,$content);
		$data['content']=$content;
       return $data;
		
}
public  function substruturemenu($data){
	
		$currentstrutureid=$this->getUtildata()->getVaueOfArray($data,'strutureid');
		
		if(empty($currentstrutureid)){return $data;}
		
		$content=$this->getUtildata()->getVaueOfArray($data,'content');
		$repositoryid=$this->getUtildata()->getVaueOfArray($data,'repositoryid');
		$pos=stripos($content, "{RESOURCE_SUBSTRUTURE_MENU}");
		
		if($pos=== false){return $data;}
		if(empty($repositoryid)){return $data;}
		
		$fparam=array('repositoryid'=>$repositoryid,'format'=>'defaulthtmllink','partial'=>1,'currentstrutureid'=>$currentstrutureid,'onlychidrens'=>1);
		$strturelibcontent=$this->getContainer()->get('badiu.admin.cms.resourcestructure.lib.content');
		$menulist=$strturelibcontent->getTree($fparam);
	
		$content=str_replace("{RESOURCE_SUBSTRUTURE_MENU}",$menulist,$content);
		$data['content']=$content;
       return $data;
		
}
public  function typemanager($data){
		$dtype=$this->getUtildata()->getVaueOfArray($data,'dtype');
		$repositoryid=$this->getUtildata()->getVaueOfArray($data,'repositoryid');
		$typeid=$this->getUtildata()->getVaueOfArray($data,'id');
		
		$labelmanager=$this->getTranslator()->trans('badiu.admin.cms.resourcetype.'.$dtype.'manager');
		$labeladdnew=$this->getTranslator()->trans('badiu.admin.cms.resourcetype.'.$dtype.'addnew');
		
		$routekeymanager="badiu.admin.cms.resourcecontent$dtype.index";
		$routekeyaddnew="badiu.admin.cms.resourcecontent$dtype.add";
		
		$linkmanager=null;
		$linkaddnew=null;
		
		$urlgoback=$this->getUtilapp()->getCurrentUrl();
		$fparamg=array('parentid'=>$repositoryid,'typeid'=>$typeid);
		$fparama=array('parentid'=>$repositoryid,'typeid'=>$typeid,'_urlgoback'=>urlencode($urlgoback));
		if($this->permission->has_access($routekeymanager,$this->getSessionhashkey())){$linkmanager=$this->getUtilapp()->getUrlByRoute($routekeymanager,$fparamg);}
		if($this->permission->has_access($routekeyaddnew,$this->getSessionhashkey())){$linkaddnew=$this->getUtilapp()->getUrlByRoute($routekeyaddnew,$fparama);}
		
		$html="";
		
		//if(!empty($linkmanager)){$linkmanager="<a href=\"$linkmanager\">$labelmanager</a><br />";}
	//	$html.=$linkmanager;
		
		if(!empty($linkaddnew)){$linkaddnew="<a href=\"$linkaddnew\">$labeladdnew</a><br />";}
		$html.=$linkaddnew;
		
      return $html;
}

}
