<template>
    
   <div v-if="resource">
            <h4>{{resource.name}}</h4>
		   <div v-html="resource.content"> </div>
       <a v-bind:href="linkEdit(resource)">editar</a><hr />     
        </div>
    
</template> 
<script> 

  module.exports = {
    props: ['resource'],

    created: function () {
            
        },
        watch: {
          
        },
    methods: {
      linkEdit: function (resource) {
              var link=badiuthembasedashboardapp.linkEdit(resource);
              return link;
            },
        }
					
  }
</script>