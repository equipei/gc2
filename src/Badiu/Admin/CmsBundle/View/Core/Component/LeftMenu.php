<template>
    
   <div v-if="elements">
      <div v-for="fitem in elements">
	    <a v-bind:href="fitem.url">{{fitem.name}}</a> | <a v-bind:href="fitem.urledit">[editar]</a> <br />
      </div>
  </div>
     
</template>  
<script> 

  module.exports = {
    props: ['elements'],

    created: function () {
            
        },
        watch: {
          
        },
    methods: {
      linkEdit: function (id) {
              var link=badiuthembasedashboardapp.linkFolderEdit(id);
              return link;
            },
	  linkView: function (id) {
              var link=badiuthembasedashboardapp.linkFolderView(id);
			  return link;
            },
        }
					
  }
</script>