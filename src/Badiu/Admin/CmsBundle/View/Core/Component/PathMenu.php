<template>
    
   <div v-if="struturepath">
      <div v-for="fitem in struturepath" class="d-inline">
	  
        <a v-bind:href="fitem.url">{{fitem.name}}</a> / 
      </div>
  </div>
     
</template>  
<script> 

  module.exports = {
    props: ['struturepath'],

    created: function () {
            
        },
        watch: {
          
        },
    methods: {
   
        }
					
  }
</script>