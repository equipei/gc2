<template>
    
   <div v-if="resource">
            <h4>{{resource.name}}</h4>
		   <div v-html="resource.content"> </div>
           
        </div>
    
</template> 
<script> 

  module.exports = {
    props: ['resource'],

    created: function () {
            
        },
        watch: {
          
        },
    methods: {
           
        }
					
  }
</script>