
<div id="_badiu_theme_core_dashboard_vuejs" >

<div v-for="item in cms.resources">
		
            <div v-if="item.dtype =='content' && item.btype =='page'" ><badiucmspage v-bind:resource="item"></badiuacmspage></div>
           
            <div v-if="item.dtype =='question' && item.btype =='boolean'"> <badiucmsquestionboolean v-bind:resource="item"  v-bind:formoptionsboolean="cms.formoptions.boolean"></badiucmsquestionboolean></div>
			
			<div v-if="item.dtype =='question' && item.btype =='multiplechoice'"> <badiucmsquestionmultiplechoice v-bind:resource="item"  v-bind:formoptionsboolean="cms.formoptions.boolean"></badiucmsquestionmultiplechoice></div>
			<hr />
       
</div>

</div>
