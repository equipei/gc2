<?php 

$utildata = $container->get('badiu.system.core.lib.util.data');
$utilapp=$container->get('badiu.system.core.lib.util.app');
$vkey=$utildata->getVaueOfArray($data,'vkey');
$dkey=$utildata->getVaueOfArray($data,'dkey');
$skey=$utildata->getVaueOfArray($data,'skey');
$vrelease=$utildata->getVaueOfArray($data,'vrelease');
$edition=$utildata->getVaueOfArray($data,'edition');

$currenturl=$utilapp->getCurrentUrl();
$currenturl=urlencode($currenturl);

$id=$utildata->getVaueOfArray($data,'id');
$repositoryid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');

$urledit=$utilapp->getUrlByRoute('badiu.admin.cms.resourcecontentcard.edit',array('parentid'=>$repositoryid,'id'=>$id,'_urlgoback'=>$currenturl));
$urleclone=$utilapp->getUrlByRoute('badiu.admin.cms.resourcecontentcard.add',array('parentid'=>$repositoryid,'foperation'=>'clone','id'=>$id,'_urlgoback'=>$currenturl));

$urlpageadd=$utilapp->getUrlByRoute('badiu.admin.cms.resourcecontentpage.add',array('parentid'=>$repositoryid,'_urlgoback'=>$currenturl));
$urlcardadd=$utilapp->getUrlByRoute('badiu.admin.cms.resourcecontentcard.add',array('parentid'=>$repositoryid,'_urlgoback'=>$currenturl));
$urlquestionbooleanadd=$utilapp->getUrlByRoute('badiu.admin.cms.resourcequestionboolean.add',array('parentid'=>$repositoryid,'_urlgoback'=>$currenturl));
$urlquestionmultiplechoiceadd=$utilapp->getUrlByRoute('badiu.admin.cms.resourcequestionmultiplechoice.add',array('parentid'=>$repositoryid,'_urlgoback'=>$currenturl));

?>
 
Versao: <?php  echo $edition;?><br />
Versao final: <?php  echo $vrelease;?><br />
Chave da versao: <?php  echo $vkey;?><br />
Chave de identificacao: <?php  echo $skey;?><br />
Chave de dev: <?php  echo $dkey;?><br />

<a href="<?php  echo $urledit;?>"><i class="fa fa-edit"></i></a>
<a href="<?php  echo $urleclone;?>"><i class="fa fa-copy"></i></a>
<a href=""><i class="fa fa-trash"></i></a>
<a href=""><i class="fas fa-times-circle"></i></a><br />

<a href="<?php  echo $urlpageadd;?>">page | </i></a>
<a href="<?php  echo $urlcardadd;?>">card | </i></a>
<a href="<?php  echo $urlquestionbooleanadd;?>">question boolen | </i></a>
<a href="<?php  echo $urlquestionmultiplechoiceadd;?>">question choice | </i></a>