<?php 

$utildata = $container->get('badiu.system.core.lib.util.data');
$json = $container->get('badiu.system.core.lib.util.json');
$content=$utildata->getVaueOfArray($data,'content');
$content=$json->decode($content,true);

$header=$utildata->getVaueOfArray($content,'header');
$body=$utildata->getVaueOfArray($content,'body');
$footer=$utildata->getVaueOfArray($content,'footer');

?>

<div class="card">
 <img class="card-img-top" src="<?php echo $defaultimageurl;?>" alt="Card image" >
  <div class="card-header">
  <?php echo $header;?>
  </div>
  <div class="card-body">
     
     <?php echo $body;?>
  </div>
  <?php if(!empty($footer)){;?>
  <div class="card-footer">
   <?php echo $footer;?>
  </div>
  <?php };?>
</div>


