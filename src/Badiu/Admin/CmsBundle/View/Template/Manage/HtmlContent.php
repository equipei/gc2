
<div id="_badiu_theme_core_dashboard_vuejs" >

<select   class="form-control" id="menulink" @change="goTomenu()"  v-model="menuchoose"  options="optionsmenu"  name="menulink">
                                    <option v-for="item in optionsmenu" :value="item.value">{{item.text }}</option>
</select>

<div class="row">
<div class="col-12"><badiucmspathmenu v-bind:struturepath="cms.struture.path"></badiucmspathmenu></div>
  <div class="col-3">
  <badiucmsleftmenu v-bind:elements="cms.struture.elements"></badiucmsleftmenu>
  <!--
  exemplo de menu
  https://docs.adobe.com/content/help/pt-BR/analytics/analyze/reports-analytics/getting-started.translate.html
  -->
  </div>
  <div class="col-9">
	<div v-for="item in cms.resources">
	
            <div v-if="item.dtype =='content' && item.btype =='page'" ><badiucmspage v-bind:resource="item"></badiuacmspage></div>
           
            <div v-if="item.dtype =='question' && item.btype =='boolean'"> <badiucmsquestionboolean v-bind:resource="item"  v-bind:formoptionsboolean="cms.formoptions.boolean"></badiucmsquestionboolean></div>
			
            <div v-if="item.dtype =='question' && item.btype =='multiplechoice'"> <badiucmsquestionmultiplechoice v-bind:resource="item" ></badiucmsquestionmultiplechoice></div>
    </div>
  </div>
</div>


</div>
