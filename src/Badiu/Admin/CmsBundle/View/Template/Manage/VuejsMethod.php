goTomenu: function () {
    if(!this.isparamempty(this.menuchoose)){
        window.location.replace(this.menuchoose);
    }
},
linkEdit: function (resource) {
	var url=null;
	var type=resource.btype;
    if(type=='page'){url=this.menuedit.resourcepage;}
	else if(type=='boolean'){url=this.menuedit.resourcequestionboolean;}
	else if(type=='multiplechoice'){url=this.menuedit.resourcequestionmultiplechoice;}
	url = url.replace("_badiu_empty_param_",resource.id );
	return url;
},
linkFolderEdit: function (id) { 
	var url=this.menuedit.resourcefolder;
	url = url.replace("_badiu_empty_param_",id);
	return url;
},
linkFolderView: function (id) { 
	var url=this.menueview.resourcefolder;
	url = url.replace("_badiu_empty_param_",id);
	return url;
},