<?php
/*$list=$utildata->getVaueOfArray($page->getData(), 'badiu_table1');
$list=$list->castToArray();
$utildata->getVaueOfArray($list, 'columns');*/
$list=$utildata->getVaueOfArray($page->getData(), 'badiu_list_data_rows.data.1',true);
$repositoryid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
$structureid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('structureid');
$currenturl=$utilapp->getCurrentUrl();
$currenturl=urlencode($currenturl);

 $strutureurlhome=$utilapp->getUrlByRoute('badiu.admin.cms.resourcemanage.dashboard',array('parentid'=>$repositoryid));
 $liststruture=array(0=>array('id'=>null,'name'=>'Inicio','url'=>$strutureurlhome));
if(!empty($structureid)){
	$factorykeytreepath=$container->get('badiu.system.core.lib.keytree.factorykeytreepath');
	$factorykeytreepath->init($structureid,"badiu.admin.cms.resourcestructure.data",true);
	$listpath=$factorykeytreepath->getPath();
	
	 foreach ($listpath as $k => $v) {
		 $itemstruteid=$utildata->getVaueOfArray($v, 'id');
		 $itemstrutename=$utildata->getVaueOfArray($v, 'name');
		 $strutureurlparam=array();
		 $strutureurlparam['parentid']=$repositoryid;
		 if(!empty($itemstruteid)){$strutureurlparam['structureid']=$itemstruteid;}
		 
		 $strutureurl=$utilapp->getUrlByRoute('badiu.admin.cms.resourcemanage.dashboard',$strutureurlparam);
		 $v['url']=$strutureurl;
		$liststruture[$k]=$v;
	 }
}

//subelements of struture

$struturedata=$container->get('badiu.admin.cms.resourcestructure.data');
$paramssefilter=array('id'=>$structureid,'repositoryid'=>$repositoryid);
$struturesubelements=$struturedata->geChildrens($paramssefilter);

//add link to $struturesubelements
$struturesubelementswithlink=array();
$currentidpath=$struturedata->getGlobalColumnValue('idpath',array('id'=>$structureid));
 $urlnewstruture=$utilapp->getUrlByRoute('badiu.admin.cms.resourcestructure.add',array('parentid'=>$repositoryid,'idpath'=>$currentidpath,'_urlgoback'=>$currenturl));
 foreach ($struturesubelements as $row) {
	 $itemstructureid=$utildata->getVaueOfArray($row, 'id');
	 $strutureurlparam['parentid']=$repositoryid;
	 $strutureurlparam['structureid']=$itemstructureid;
	 $row['url']=$utilapp->getUrlByRoute('badiu.admin.cms.resourcemanage.dashboard',$strutureurlparam);
	 $row['urledit']=$utilapp->getUrlByRoute('badiu.admin.cms.resourcestructure.edit',array('id'=>$itemstructureid,'parentid'=>$repositoryid,'_urlgoback'=>$currenturl));
	  array_push($struturesubelementswithlink,$row);
	 
 }
 
 //last add new
  $addnewstrture=array('id'=>null,'name'=> 'Cadastrar novo','url'=>$urlnewstruture);
  array_push($struturesubelementswithlink, $addnewstrture);
 
 
$templatefactorydatavuejs=$container->get('badiu.admin.cms.resourece.templatefactorydatavuejs');
$templatefactorydatavuejs->setList($list);
$dataestruture=array('path'=>$liststruture,'elements'=>$struturesubelementswithlink);
$templatefactorydatavuejs->setStruture($dataestruture);

$jsdata=$templatefactorydatavuejs->makeData();


$paramurladd=array('parentid'=>$repositoryid,'structureid'=>$structureid,'_urlgoback'=>$currenturl);
$paramurledit=array('parentid'=>$repositoryid,'_urlgoback'=>$currenturl,'id'=>'_badiu_empty_param_');
$paramurlview=array('parentid'=>$repositoryid,'structureid'=>'_badiu_empty_param_');

$pageurladd=$utilapp->getUrlByRoute('badiu.admin.cms.resourcecontentpage.add',$paramurladd);
$pageurledit=$utilapp->getUrlByRoute('badiu.admin.cms.resourcecontentpage.edit',$paramurledit);

$questionbooleanurladd=$utilapp->getUrlByRoute('badiu.admin.cms.resourcequestionboolean.add',$paramurladd);
$questionbooleanurledit=$utilapp->getUrlByRoute('badiu.admin.cms.resourcequestionboolean.edit',$paramurledit);

$questionmultiplechoiceurladd=$utilapp->getUrlByRoute('badiu.admin.cms.resourcequestionmultiplechoice.add',$paramurladd);
$questionmultiplechoiceurledit=$utilapp->getUrlByRoute('badiu.admin.cms.resourcequestionmultiplechoice.edit',$paramurledit);

$folderurladd=$utilapp->getUrlByRoute('badiu.admin.cms.resourcestructure.add',$paramurladd);
$folderurledit=$utilapp->getUrlByRoute('badiu.admin.cms.resourcestructure.edit',$paramurledit);
$folderurlview=$utilapp->getUrlByRoute('badiu.admin.cms.resourcemanage.dashboard',$paramurlview);
?>

cms: <?php echo $jsdata;  ?>,
menuchoose: "", 
menuedit: {
	"resourcepage": "<?php echo $pageurledit; ?>",
	"resourcequestionboolean": "<?php echo $questionbooleanurledit; ?>",
	"resourcequestionmultiplechoice": "<?php echo $questionmultiplechoiceurledit; ?>",
	"resourcefolder": "<?php echo $folderurledit; ?>"
},
menueview: {
	"resourcefolder": "<?php echo $folderurlview; ?>"
},
optionsmenu: [
    {"text":"Criar nova pagina","value":"<?php echo $pageurladd; ?>"},
    {"text":"Criar nova questao verdadeira / falso","value":"<?php echo $questionbooleanurladd; ?>"},
    {"text":"Criar nova questao de múltipla escolha","value":"<?php echo $questionmultiplechoiceurladd; ?>"},
	{"text":"Criar nova pasta","value":"<?php echo $folderurladd; ?>"},
    ],