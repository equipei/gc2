<?php
$content = $utildata->getVaueOfArray($page->getData(), 'badiu_list_data_row.data.1.content', true);
$title=$utildata->getVaueOfArray($page->getData(), 'badiu_list_data_row.data.1.name',true);


$parentid = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
$vkey = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('vkey');
$vrelease = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('vrelease');

$newparentid=null;
if(!empty($vkey) && $vrelease==1){
	$resourcedata= $container->get('badiu.admin.cms.resource.data');
	$fcparam=array('repositoryid'=>$parentid,'vkey'=>$vkey,'vrelease'=>$vrelease);
	$newparentid=$resourcedata->getGlobalColumnValue('id',$fcparam);
	
}
$prefixid = "";
if (!empty($parentid)) {
	$prefixid = "-" . $parentid;
}
$param = array('resourseid' => $parentid);
if(!empty($newparentid)){$param = array('resourseid' => $newparentid);}
$managelib = $container->get('badiu.admin.cms.resourcelog.lib.manage');
$managelib->add($param);
$title=$utildata->getVaueOfArray($page->getData(), 'badiu_list_data_row.data.1.name',true);


//print_R($page->getData());
?>

<div id="_badiu_theme_core_dashboard_vuejs<?php echo $prefixid; ?>">

	<div v-if="rowdata1.listabstracttemplatecustomcss" v-html="rowdata1.listabstracttemplatecustomcss"></div>
	<div v-if="!rowdata1.listabstracttemplatecustomcss" v-html="rowdata1.templatecustomcss"></div>

	<div v-html="rowdata1.customcss"></div>
	<div v-html="rowdata1.customcss"></div>

<!--
	<div class="badiu-barra-social-container">
		<div class="badiu-barra-social border">
			<div class="d-flex align-items-center container-social-item px-1 px-md-2">
				<div class="d-flex align-items-center">
					<a class="icon-btn color-like" @click.prevent="alert('Like')" data-toggle="tooltip" data-placement="top" title="Gostei">
						<i class="fa fa-thumbs-up"></i>
					</a><span class="like-numero ml-1">(99)</span>
				</div>
				<div class="d-flex align-items-center mx-2">
					<a class="icon-btn color-dislike" @click.prevent="alert('Dislike')" data-toggle="tooltip" data-placement="top" title="Não gostei">
						<i class="fa fa-thumbs-down"></i>
					</a><span class="like-numero ml-1">(5)</span>
				</div>
			</div>

			<div class="d-flex align-items-center container-social-item border-left px-1 px-md-2">
				<a class="icon-btn font-weight-bold" @click.prevent="handleFavoritar" data-toggle="tooltip" data-placement="top" title="Favoritar">
					<i class="far fa-bookmark" v-bind:class="vparam.isFavoritado ? 'fa fa-bookmark' : 'far fa-bookmark'"></i>
				</a>
			</div>

			<div class="d-flex align-items-center container-social-item border-left px-1 px-md-2">
				<span class="switch switch-xs mt-2">
					<input type="checkbox" value="cmp1" class="switch" id="badiu-check-finish-lido">
					<label for="badiu-check-finish-lido" class="font-weight-bold">Lido</label>
				</span>
			</div>

			<div class="d-flex align-items-start container-social-item border-left px-1 px-md-2">
				<div class="rating">
					<input type="radio" name="rating" value="5" id="5-favoritar">
					<label for="5-favoritar">☆</label>

					<input type="radio" name="rating" value="4" id="4-favoritar">
					<label for="4-favoritar">☆</label>

					<input type="radio" name="rating" value="3" id="3-favoritar">
					<label for="3-favoritar">☆</label>

					<input type="radio" name="rating" value="2" id="2-favoritar">
					<label for="2-favoritar">☆</label>

					<input type="radio" name="rating" value="1" id="1-favoritar">
					<label for="1-favoritar">☆</label>
				</div>
			</div>

			<div class="position-relative d-flex align-items-center justify-content-end container-social-item border-left px-3">
				<div class="social-links-share right" :class="vparam.isToggleShare ? 'open' : ''">
					<a title="Whatsapp" class="whatsapp" href="https://api.whatsapp.com/send?text=Curso sobre Moodle 3.11 - https://www.google.com.br" target="_blank"><i class="fab fa-whatsapp fa-1x"></i></a>
					<a title="Facebook" href="http://www.facebook.com/sharer.php?display=page&amp;u=https://www.google.com.br;" target="_blank"><i class="fab fa-facebook-f"></i></a>
					<a title="Twitter" class="twitter" href="https://twitter.com/share?text=Curso sobre Moodle 3.11 - &amp;url=https://www.google.com.br&amp;hashtags=badiu" target="_blank"><i class="fab fa-twitter"></i></a>
					<a title="Copiar link" class="link-clipboard" onclick="event.preventDefault()" href="#"><i class="fas fa-link"></i></a>

					<a class="toggle-social-links" @click="vparam.isToggleShare = !vparam.isToggleShare">
						<i class="fas fa-share-alt"></i>
						<i class="fas fa-times"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
-->
	<div v-html="rowdata1.content "></div>
</div>

<script>
	//carregar tooltip bootstrap
	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	})
</script>

<style>
	.badiu-barra-social-container {
		display: flex;
		align-items: center;
		justify-content: end;
	}

	.badiu-barra-social {
		display: flex;
		align-items: center;
		width: fit-content;
	}

	.container-social-item {
		height: 48px;
	}

	.social-links-share.right a:hover {
		color: #1cc2d4;
	}

	.social-links-share {
		position: absolute;
		width: 25px;
		height: 25px;
		line-height: 30px;
		border: 1px solid #999;
		border-radius: 18px;
		-webkit-transition: max-width 0.2s linear, padding 0.2s linear;
		-o-transition: max-width 0.2s linear, padding 0.2s linear;
		transition: max-width 0.2s linear, padding 0.2s linear;
		overflow: hidden;
		padding-left: 25px;
		right: 3px;
	}

	@media (min-width: 576px) {
		.social-links-share {
			position: relative;
			width: 25px;
			right: unset;
		}
	}

	.social-links-share a {
		opacity: 0;
		-webkit-transition: opacity 0s linear 0.2s;
		-o-transition: opacity 0s 0.2s linear;
		transition: opacity 0s linear 0.2s;
		padding: 0 4px;
		text-decoration: none;
		border: none;
		position: relative;
		top: -4px;
	}

	.social-links-share a.whatsapp i {
		font-weight: 600;
		font-size: 1.1em;
	}

	.social-links-share .toggle-social-links {
		opacity: 1;
		display: block;
		position: absolute;
		left: -2px;
		top: -4px;
		width: 25px;
		height: 25px;
		line-height: 30px;
		text-align: center;
		border-radius: 50px;
		cursor: pointer;
	}

	.social-links-share .toggle-social-links i {
		opacity: 1;
		position: absolute;
		width: 100%;
		text-align: center;
		top: 0;
		left: 0;
		height: 100%;
		line-height: 30px;
		-webkit-transition: opacity 0.2s linear 0s;
		-o-transition: opacity 0.2s 0s linear;
		transition: opacity 0.2s linear 0s;
		color: #999;
		font-size: 16px;
	}

	.social-links-share .toggle-social-links .fa-times {
		opacity: 0;
	}

	.social-links-share.open {
		max-width: 320px;
		border: 1px solid #999;
		padding-right: 8px;
	}

	.social-links-share.open a {
		opacity: 1;
		-webkit-transition: opacity 0 linear 0;
		-o-transition: opacity 0 0 linear;
		transition: opacity 0 linear 0;
	}

	.social-links-share.open a i {
		color: #999;
	}

	.social-links-share.open a.whatsapp i {
		font-weight: 600;
		font-size: 1.1em;
	}

	.social-links-share.open .toggle-social-links .fa-share-alt {
		opacity: 0;
	}

	.social-links-share.open .toggle-social-links .fa-times {
		opacity: 1;
		color: #999;
	}

	.social-links-share.right .toggle-social-links {
		right: 1px;
		left: unset;
	}

	.social-links-share.right.open {
		background-color: #FFF;
		width: 146px;
		border: 1px solid #999;
		padding-left: 8px;
		padding-right: 28px;
	}

	.social-links-share.right.open a {
		opacity: 1;
		-webkit-transition: opacity 0 linear 0;
		-o-transition: opacity 0 0 linear;
		transition: opacity 0 linear 0;
	}

	.social-links-share.right.open .toggle-social-links .fa-share-alt {
		opacity: 0;
	}

	.social-links-share.right.open .toggle-social-links .fa-times {
		opacity: 1;
		color: #999;
	}

	.social-links {
		display: flex;
		justify-content: flex-end;
	}

	.social-links .links-shares p {
		color: #555555;
		font-size: 0.75rem;
		line-height: 17px;
		font-weight: normal;
		margin-bottom: unset;
	}

	.social-links .links-shares .sociais {
		display: flex;
	}

	.social-links .links-shares .sociais a {
		margin-bottom: 0;
		margin-right: 10px;
		border: 1px solid #555555;
		border-radius: 50%;
		width: 32px;
		height: 32px;
		display: flex;
		justify-content: center;
		align-items: center;
		color: #555555;
		font-size: 1.10rem;
	}

	.social-links .links-shares .sociais a a {
		color: #555555;
		font-size: 1.10rem;
		display: block;
	}

	/* SOCIAL SHARE */


	/* LIKE --- UNLIKE */
	.icon-btn {
		cursor: pointer;
	}

	.color-like i {
		color: #63734A;
	}

	.color-dislike i {
		color: #ED0024;
	}

	.like-numero {
		font-size: 0.8rem;
	}

	/* LIKE --- UNLIKE */

	/* LIDO */
	.switch input[type=checkbox] {
		display: none;
	}

	.switch input[type=checkbox]+label {
		position: relative;
		min-width: calc(calc(2.375rem * .8) * 2);
		border-radius: calc(2.375rem * .8);
		height: calc(2.375rem * .8);
		line-height: calc(2.375rem * .8);
		display: inline-block;
		cursor: pointer;
		outline: none;
		user-select: none;
		vertical-align: middle;
		text-indent: calc(calc(calc(2.375rem * .8) * 2) + .5rem);
	}

	.switch input[type=checkbox]+label::before,
	.switch input[type=checkbox]+label::after {
		content: '';
		position: absolute;
		top: 0;
		left: 0;
		width: calc(calc(2.375rem * .8) * 2);
		bottom: 0;
		display: block;
	}

	.switch input[type=checkbox]+label::before {
		right: 0;
		background-color: #dee2e6;
		border-radius: calc(2.375rem * .8);
		transition: .2s all;
	}

	.switch input[type=checkbox]+label::after {
		top: 2px;
		left: 2px;
		width: calc(calc(2.375rem * .8) - calc(2px * 2));
		height: calc(calc(2.375rem * .8) - calc(2px * 2));
		border-radius: 50%;
		background-color: #fff;
		transition: all 0.3s ease-in 0s;
		;
	}

	.switch input[type=checkbox]:checked+label::before {
		background-color: #08d;
	}

	.switch input[type=checkbox]:checked+label::after {
		margin-left: calc(2.375rem * .8);
	}

	.switch input[type=checkbox]:focus+label::before {
		outline: none;
		box-shadow: 0 0 0 .2rem rgba(0, 136, 221, .25);
	}

	.switch input[type=checkbox]:disabled+label {
		color: #868e96;
		cursor: not-allowed;
	}

	.switch input[type=checkbox]:disabled+label::before {
		background-color: #e9ecef;
	}

	.switch.switch-xs {
		font-size: .8rem;
	}

	.switch.switch-xs input[type=checkbox]+label {
		min-width: calc(calc(1.5375rem * .8) * 2);
		height: calc(1.5375rem * .8);
		line-height: calc(1.5375rem * .8);
		text-indent: calc(calc(calc(1.5375rem * .8) * 2) + .5rem);
	}

	.switch.switch-xs input[type=checkbox]+label::before {
		width: calc(calc(1.5375rem * .8) * 2);
	}

	.switch.switch-xs input[type=checkbox]+label::after {
		width: calc(calc(1.5375rem * .8) - calc(2px * 2));
		height: calc(calc(1.5375rem * .8) - calc(2px * 2));
	}

	.switch.switch-xs input[type=checkbox]:checked+label::after {
		margin-left: calc(1.5375rem * .8);
	}

	.switch+.switch {
		margin-left: 1rem;
	}

	/* LIDO */

	/* ESTRELAS RATING */
	.rating {
		display: flex;
		flex-direction: row-reverse;
		justify-content: center
	}

	.rating>input {
		display: none
	}

	.rating>label {
		position: relative;
		width: 1em;
		font-size: 28px;
		color: rgba(0, 0, 0, 0.5);
		cursor: pointer;
		margin: 0;
	}

	.rating>label::before {
		content: "\2605";
		position: absolute;
		opacity: 0
	}

	.rating>label:hover:before,
	.rating>label:hover~label:before {
		opacity: 1 !important;
		color: #1351b4;
	}

	.rating>input:checked~label:before {
		opacity: 1;
		color: #1351b4;
	}

	.rating:hover>input:checked~label:before {
		opacity: 0.4
	}

	/* ESTRELAS RATING */
</style>