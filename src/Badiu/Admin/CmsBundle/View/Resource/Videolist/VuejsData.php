<?php 
$parentid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
if(empty($parentid)){$parentid=0;}

$userid="";
 $badiuSession = $container->get('badiu.system.access.session');
 $badiuSession->setHashkey($page->getSessionhashkey());
 $userid=$badiuSession->get()->getUser()->getId();

$permission=$container->get('badiu.system.access.permission');
$canmamager=$permission->has_access("badiu.admin.cms.resourcecontentvideolist",$page->getSessionhashkey());
if($canmamager){$canmamager=1;}
else {$canmamager=0;}
?>
vparam:{
	_service: "badiu.admin.cms.resourcecontentvideolist.lib.manage",
	index: 0, 
	currentdata: "",
	contentcompleted: [],
	contentevaluate: [],
	discussion: "",
	response: "",
	comment: "",
	discussionlike: [],
	commentlike: [],
	resourceid: <?php echo $parentid;?>,
	userid: <?php echo $userid;?>,
	ismanager: <?php echo $canmamager;?>,
	deleteconfirm: {
		showmodalcrtitem: "modal fade",
		showmodalcrt: "none",
		title: "",
		description: "",
		index: "",
		item: "",
	},
	
},