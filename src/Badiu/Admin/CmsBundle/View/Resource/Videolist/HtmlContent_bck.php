<?php
$parentid = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
$prefixid = "";
if (!empty($parentid)) {
	$prefixid = "-" . $parentid;
}
?>
<div id="_badiu_theme_core_dashboard_vuejs<?php echo $prefixid; ?>">

	<div class="badiu-panel-view-mview">
		<div class="badiu-panel-video" v-if="rowsdata1.length">
			<a @click.prevent="navegation('back')" href="#" class="leftnavegation"><i class="fas fa-chevron-circle-left"></i></a>&nbsp;&nbsp;
			<div class="iframevideo"><iframe :src="rowsdata1[vparam.index].content" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" height="338" width="580"></iframe></div>
			&nbsp;<a @click.prevent="navegation('forword')" href="#" class="rightnavegation"><i class="fas fa-chevron-circle-right"></i></a>
			<div class="pagenav">
				<ul class="pagination   justify-content-center">
					<li class="page-item" v-for="(item, index) in rowsdata1">
						<a class="page-link" :class="activePaging(index)" @click.prevent="navegationPaging(index)" href="#">{{labelPaging(index)}} </a>
					</li>
				</ul>
			</div>

		</div>
		<div v-if="!rowsdata1.length">Nenhum vídeo foi cadastrado</div>
	</div>


	<div class="badiu-panel-view-manager" v-if="badiupermission.manage==1">

		<div>
			<table class="table table-striped">
				<tr v-for="(item, index) in rowsdata1">
					<td v-html="item.content"></td>
					<td> <a href="#" @click.prevent="removeVideo(item.id,'remove')"> <i class="far fa-times-circle"></i></a></td>
				</tr>
			</table>
			<div class="form-group" v-if="badiupermission.add==1">
				<input type="text" class="form-control" id="cotent" placeholder="Digite url do vídeo" v-model="fdata.badiuform.content" name="content">

				<button type="submit" class="btn btn-default" @click="send()">Salvar</button>
			</div>
		</div>



	</div>
	<!-- config completed -->
	<div class="mt-5 container-tabs">
		<ul class="nav nav-tabs active" id="tabVideos" role="tablist">
			<li class="nav-item">
				<a class="nav-link" id="visaogeral-tab" data-toggle="tab" href="#visaogeral" role="tab" aria-controls="visaogeral" aria-selected="true">Visão geral</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="comentarios-tab" data-toggle="tab" href="#comentarios" role="tab" aria-controls="comentarios" aria-selected="false">Comentários</a>
			</li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content badiu-tabs-content my-4">
			<div class="tab-pane fade show active" id="visaogeral" role="tabpanel" aria-labelledby="visaogeral-tab">
				<p>Descrição</p>
				<p class="video-title">Vídeo aula 2 - Parte 2</p>

				<hr>

				<div class="row d-flex mt-4">
					<div class="col-6 d-flex flex-column">
						<h3>Concluído</h3>
							
						<span class="switch switch-xs mt-2">
							<input type="checkbox" value="cmp1"  class="switch" v-model="vparam.contentcompleted[vparam.index]" id="badiu-check-finish"> 
							<label for="badiu-check-finish">Eu já concluí esta aula</label>
						</span>
					</div>

					<div class="col-6 d-flex flex-column align-items-start">
						<h3>Avalie esta aula</h3>
						<div class="rating">
							<input type="radio" name="rating" value="4" id="4"> 
							<label for="4">☆</label> <input type="radio" name="rating" value="4" id="4" v-model="vparam.contentevaluate[vparam.index]">
							<label for="3">☆</label> <input type="radio" name="rating" value="3" id="3" v-model="vparam.contentevaluate[vparam.index]">
							<label for="2">☆</label> <input type="radio" name="rating" value="2" id="2" v-model="vparam.contentevaluate[vparam.index]">
							<label for="1">☆</label><input type="radio" name="rating" value="1" id="1" v-model="vparam.contentevaluate[vparam.index]">
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="comentarios" role="tabpanel" aria-labelledby="comentarios-tab">
				<div class="container-comments">
					<div class="container-comment d-flex mb-4">
						<img src="https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg.jpg?resize=760,506" class="rounded-circle mr-4" alt="avatar">

						<div class="comment-card d-flex flex-column px-4 py-2">
							<div class="d-flex">
								<p class="comment-name mb-0">Aluna</p>
							</div>
							<div class="d-flex">
								<span class="comment-date mr-4">10/08/2021</span>
								<span class="comment-time">11:24:36</span>
							</div>
							<div class="d-flex mt-4">
								<p class="comment-text">Muito bom este vídeo, esclarecedor.</p>
							</div>
						</div>
					</div>

					<!-- usuario logado - aplicar classe active-user -->
					<div class="container-comment active-user d-flex mb-4">
						<img src="https://organicthemes.com/demo/profile/files/2018/05/profile-pic.jpg" class="rounded-circle mr-4" alt="avatar">

						<div class="comment-card d-flex flex-column px-4 py-2">
							<div class="d-flex">
								<p class="comment-name mb-0">Meu Perfil</p>
							</div>
							<div class="d-flex">
								<span class="comment-date mr-4">10/08/2021</span>
								<span class="comment-time">14:44:23</span>
							</div>
							<div class="d-flex mt-4">
								<p class="comment-text">Gostei bastante da aula.</p>
							</div>
						</div>
					</div>

					<div class="container-comment d-flex mb-4">
						<img src="https://st.depositphotos.com/1008939/i/600/depositphotos_22408839-stock-photo-serious.jpg" class="rounded-circle mr-4" alt="avatar">

						<div class="comment-card d-flex flex-column px-4 py-2">
							<div class="d-flex">
								<p class="comment-name mb-0">Aluno</p>
							</div>
							<div class="d-flex">
								<span class="comment-date mr-4">10/08/2021</span>
								<span class="comment-time">18:54:36</span>
							</div>
							<div class="d-flex mt-4">
								<p class="comment-text">Que aula interessante, gostei muito.</p>
							</div>
						</div>
					</div>

					<div class="container-comment d-flex mb-4">
						<img src="https://cdn.fastly.picmonkey.com/contentful/h6goo9gw1hh6/2sNZtFAWOdP1lmQ33VwRN3/24e953b920a9cd0ff2e1d587742a2472/1-intro-photo-final.jpg?w=800&q=70" class="rounded-circle mr-4" alt="avatar">

						<div class="comment-card d-flex flex-column px-4 py-2">
							<div class="d-flex">
								<p class="comment-name mb-0">Aluno 2</p>
							</div>
							<div class="d-flex">
								<span class="comment-date mr-4">10/08/2021</span>
								<span class="comment-time">21:21:00</span>
							</div>
							<div class="d-flex mt-4">
								<p class="comment-text">Aula legal.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="container-comment-sender my-4 mr-3">
					<div class="form-group">
						<div class="input-group input-group-send">
							<input type="text" class="form-control" placeholder="Deixe seu comentário" id="message" />
							<span class="input-group-addon ml-4" onclick="alert('enviar msg')">
								<i class="fas fa-paper-plane"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<style>
	.badiu-panel-view-mview {
		/*background-color: red;*/
		/*position: absolute;*/
		width: 100%;
		overflow: hidden;
		display: flex;
		flex-direction: row;
		justify-content: center;
	}

	.badiu-panel-video {
		/*background-color: blue;*/
		width: 670px;
		height: 390px;
		position: relative;
		margin: 0 auto;

	}

	.badiu-panel-video .iframevideo {
		/*background-color: yellow;*/

		width: 600px;
		height: 338px;
		margin: 0 auto;
		display: inline;
		padding-left: 30px;
	}

	.badiu-panel-video .leftnavegation {
		position: absolute;
		padding-top: 135px;
		padding-left: 1px;
		color: white;
		font-size: 35px;
		color: blue;

	}

	.badiu-panel-video .rightnavegation {
		position: absolute;
		padding-top: 135px;
		padding-right: 1px;
		color: white;
		font-size: 35px;
		color: blue;
	}

	.badiu-panel-video .pagenav {
		/*background-color: green;*/
		height: 30px;
		position: relative;
		width: 100%;
		text-align: center;
	}

	.page-item .active {
		background-color: blue;
		color: white;
	}

	.badiu-panel-video li {
		list-style-type: none;
	}

	/*.badiu-panel-video .pagenav .pagination{
	margin: 0 auto ;
	
}*/

	.badiu-panel-view-manager {
		/*background-color: red;*/
		/*position: absolute;*/
		width: 100%;
		overflow: hidden;
		display: flex;
		flex-direction: row;
		justify-content: center;
	}

	/* TABS */
	.container-tabs .nav-tabs {
		border-bottom: 1px solid #CED4DA;
		padding-left: 36px;
	}

	.container-tabs .nav-tabs .nav-item {
		list-style: none;
	}

	.container-tabs .nav-tabs .nav-item .nav-link {
		padding: .5rem 2.5rem;
	}

	.container-tabs .nav-tabs .nav-item .nav-link {
		font-family: Roboto;
		font-weight: 400;
		font-size: 28px;
		color: #202020;
	}

	.badiu-tabs-content h3 {
		font-family: Roboto;
		font-weight: 500;
		line-height: 28px;
		color: #202020;
	}

	.badiu-tabs-content p {
		font-family: Roboto;
		font-weight: 400;
		font-size: 16px;
		color: #202020;
	}

	.badiu-tabs-content .video-title {
		font-family: Roboto;
		font-weight: 500;
		font-size: 18px;
		color: #202020;
	}

	/* TABS */

	/* ESTRELAS RATING */
	.rating {
		display: flex;
		flex-direction: row-reverse;
		justify-content: center
	}

	.rating>input {
		display: none
	}

	.rating>label {
		position: relative;
		width: 1em;
		font-size: 40px;
		color: rgba(0, 0, 0, 0.5);
		cursor: pointer;
		margin: 0;
	}

	.rating>label::before {
		content: "\2605";
		position: absolute;
		opacity: 0
	}

	.rating>label:hover:before,
	.rating>label:hover~label:before {
		opacity: 1 !important;
		color: #FBC60D;
	}

	.rating>input:checked~label:before {
		opacity: 1;
		color: #FBC60D;
	}

	.rating:hover>input:checked~label:before {
		opacity: 0.4
	}

	/* ESTRELAS RATING */

	/* SWTICH INPUT */
	.switch input[type=checkbox] {
		display: none;
	}

	.switch input[type=checkbox]+label {
		position: relative;
		min-width: calc(calc(2.375rem * .8) * 2);
		border-radius: calc(2.375rem * .8);
		height: calc(2.375rem * .8);
		line-height: calc(2.375rem * .8);
		display: inline-block;
		cursor: pointer;
		outline: none;
		user-select: none;
		vertical-align: middle;
		text-indent: calc(calc(calc(2.375rem * .8) * 2) + .5rem);
	}

	.switch input[type=checkbox]+label::before,
	.switch input[type=checkbox]+label::after {
		content: '';
		position: absolute;
		top: 0;
		left: 0;
		width: calc(calc(2.375rem * .8) * 2);
		bottom: 0;
		display: block;
	}

	.switch input[type=checkbox]+label::before {
		right: 0;
		background-color: #dee2e6;
		border-radius: calc(2.375rem * .8);
		transition: .2s all;
	}

	.switch input[type=checkbox]+label::after {
		top: 2px;
		left: 2px;
		width: calc(calc(2.375rem * .8) - calc(2px * 2));
		height: calc(calc(2.375rem * .8) - calc(2px * 2));
		border-radius: 50%;
		background-color: #fff;
		transition: all 0.3s ease-in 0s;
		;
	}

	.switch input[type=checkbox]:checked+label::before {
		background-color: #08d;
	}

	.switch input[type=checkbox]:checked+label::after {
		margin-left: calc(2.375rem * .8);
	}

	.switch input[type=checkbox]:focus+label::before {
		outline: none;
		box-shadow: 0 0 0 .2rem rgba(0, 136, 221, .25);
	}

	.switch input[type=checkbox]:disabled+label {
		color: #868e96;
		cursor: not-allowed;
	}

	.switch input[type=checkbox]:disabled+label::before {
		background-color: #e9ecef;
	}

	.switch.switch-xs {
		font-size: .8rem;
	}

	.switch.switch-xs input[type=checkbox]+label {
		min-width: calc(calc(1.5375rem * .8) * 2);
		height: calc(1.5375rem * .8);
		line-height: calc(1.5375rem * .8);
		text-indent: calc(calc(calc(1.5375rem * .8) * 2) + .5rem);
	}

	.switch.switch-xs input[type=checkbox]+label::before {
		width: calc(calc(1.5375rem * .8) * 2);
	}

	.switch.switch-xs input[type=checkbox]+label::after {
		width: calc(calc(1.5375rem * .8) - calc(2px * 2));
		height: calc(calc(1.5375rem * .8) - calc(2px * 2));
	}

	.switch.switch-xs input[type=checkbox]:checked+label::after {
		margin-left: calc(1.5375rem * .8);
	}

	.switch+.switch {
		margin-left: 1rem;
	}

	/* SWTICH INPUT */

	/* TAB COMENTARIOS */
	.container-comments {
		overflow-y: auto;
		width: 100%;
		height: 350px;
		padding: 0 40px 0 0;
	}

	.container-comments::-webkit-scrollbar {
		width: 20px;
	}

	.container-comments::-webkit-scrollbar-track {
		border-radius: 100px;
	}

	.container-comments::-webkit-scrollbar-thumb {
		border-radius: 100px;
		border: 4px solid transparent;
		background-clip: content-box;
		background-color: #C4C4C4;
	}

	.container-comment .comment-card {
		background: rgba(196, 196, 196, 0.1);
		border: 1px solid rgba(0, 0, 0, 0.15);
		border-radius: 0px 30px 30px 30px;
		width: 100%;
	}

	.container-comment.active-user .comment-card {
		background: rgba(106, 201, 178, 0.3);
		border: none;
	}

	.container-comment img {
		width: 66px;
		height: 66px;
		object-fit: cover;
	}

	.container-comment .comment-name {
		font-family: Roboto;
		font-weight: 500;
		font-size: 18px;
		color: #202020;
	}

	.container-comment .comment-date,
	.container-comment .comment-time {
		font-family: Roboto;
		font-weight: 400;
		font-size: 12px;
		color: #202020;
	}

	.container-comment .comment-text {
		font-family: Roboto;
		font-style: italic;
		font-weight: 400;
		font-size: 16px;
		color: #202020;
	}

	.container-comment-sender .input-group {
		display: flex;
		align-items: center;
	}

	.input-group-send .form-control {
		background: rgba(206, 212, 218, 0.5) !important;
		border-radius: 10px !important;
		border: none !important;
		padding-left: 34px;
	}

	.input-group-send .form-control::placeholder {
		font-family: Roboto;
		font-weight: 500;
		font-size: 18px;
		color: rgba(32, 32, 32, 0.6);
		opacity: 1;
	}

	.input-group-send .form-control:-ms-input-placeholder {
		font-family: Roboto;
		font-weight: 500;
		font-size: 18px;
		color: rgba(32, 32, 32, 0.6);
	}

	.input-group-send .form-control::-ms-input-placeholder {
		font-family: Roboto;
		font-weight: 500;
		font-size: 18px;
		color: rgba(32, 32, 32, 0.6);
	}

	.input-group-send .input-group-addon {
		cursor: pointer;
		opacity: 1;
		transition: all .2s;
	}

	.input-group-send .input-group-addon:hover {
		opacity: 0.6;
	}

	/* TAB COMENTARIOS */
</style>