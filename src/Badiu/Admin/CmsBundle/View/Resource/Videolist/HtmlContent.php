<?php
$parentid = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
$prefixid = "";
if (!empty($parentid)) {
	$prefixid = "-" . $parentid;
}
?>
<div id="_badiu_theme_core_dashboard_vuejs<?php echo $prefixid; ?>">

	<div class="badiu-panel-view-mview">

		<div class="badiu-panel-video" v-if="rowsdata1.length">

			<a @click.prevent="navegation('back')" href="#" class="leftnavegation"><i class="fas fa-chevron-circle-left"></i></a>
			<a @click.prevent="navegation('forword')" href="#" class="rightnavegation"><i class="fas fa-chevron-circle-right"></i></a>

			<div class="iframe-container">
				<iframe class="responsive-iframe" :src="rowsdata1[vparam.index].content" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0"></iframe>
			</div>

			<div class="pagenav mt-2">
				<ul class="pagination justify-content-center">
					<li class="page-item" v-for="(item, index) in rowsdata1">
						<a class="page-link" :class="activePaging(index)" @click.prevent="navegationPaging(index)" href="#">{{labelPaging(index)}} </a>
					</li>
				</ul>
			</div>

		</div>
		<div v-if="!rowsdata1.length">Nenhum vídeo foi cadastrado</div>
	</div>


	<div class="badiu-panel-view-manager my-5" v-if="badiupermission.manage==1">
		<div class="">
			<table class="table table-striped">
				<tr v-for="(item, index) in rowsdata1">
					<td>
						<p v-html="item.content" class="youtube-link mb-0"></p>
					</td>
					<td> <a href="#" @click.prevent="removeVideo(item.id,'remove')"> <i class="far fa-times-circle"></i></a></td>
				</tr>
			</table>
			<div class="form-group" v-if="badiupermission.add==1">
				<input type="text" class="form-control" id="cotent" @keyup.enter="send()" placeholder="Digite url do vídeo" v-model="fdata.badiuform.content" name="content">
				<button type="submit" class="btn btn-default" @click="send()">Salvar</button>
			</div>
		</div>
	</div>
	<!-- config completed -->
	<div class="mt-5 container-tabs">
		<ul class="nav nav-tabs active" id="tabVideos<?php echo $prefixid; ?>" role="tablist">
			<li class="nav-item">
				<a class="nav-link" id="visaogeral-tab<?php echo $prefixid; ?>" data-toggle="tab" href="#visaogeral<?php echo $prefixid; ?>" role="tab" aria-controls="visaogeral<?php echo $prefixid; ?>" aria-selected="true">Visão geral</a>
			</li>
			<!--<li class="nav-item">
				<a class="nav-link" id="comentarios-tab<?php echo $prefixid; ?>" data-toggle="tab" href="#comentarios<?php echo $prefixid; ?>" role="tab" aria-controls="comentarios<?php echo $prefixid; ?>" aria-selected="false">Comentários</a>
			</li>-->

			<li class="nav-item">
				<a class="nav-link" id="discussions-tab<?php echo $prefixid; ?>" data-toggle="tab" href="#discussions<?php echo $prefixid; ?>" role="tab" aria-controls="comentarios<?php echo $prefixid; ?>" aria-selected="false">Discussão</a>
			</li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content badiu-tabs-content my-4">
			<div class="tab-pane fade show active" id="visaogeral<?php echo $prefixid; ?>" role="tabpanel" aria-labelledby="visaogeral-tab<?php echo $prefixid; ?>">
				<!--<p>Descrição</p>
				<p class="video-title">Vídeo aula 2 - Parte 2</p>

				<hr>
					-->
				<div class="row d-flex mt-4">
					<div class="col-6 d-flex flex-column">
						<h3>Concluído</h3>

						<span class="switch switch-xs mt-2">
							<input type="checkbox" value="cmp1" class="switch" v-model="vparam.contentcompleted[vparam.index]" id="badiu-check-finish-<?php echo $prefixid; ?>">
							<label for="badiu-check-finish-<?php echo $prefixid; ?>">Eu já assisti este vídeo</label>
						</span>
					</div>

					<div class="col-6 d-flex flex-column align-items-start">
						<h3>Avalie o vídeo</h3>

						<div class="rating">
							<input type="radio" name="rating" value="5" id="5-<?php echo $prefixid; ?>" v-model="vparam.contentevaluate[vparam.index]">
							<label for="5-<?php echo $prefixid; ?>">☆</label>

							<input type="radio" name="rating" value="4" id="4-<?php echo $prefixid; ?>" v-model="vparam.contentevaluate[vparam.index]">
							<label for="4-<?php echo $prefixid; ?>">☆</label>

							<input type="radio" name="rating" value="3" id="3-<?php echo $prefixid; ?>" v-model="vparam.contentevaluate[vparam.index]">
							<label for="3-<?php echo $prefixid; ?>">☆</label>

							<input type="radio" name="rating" value="2" id="2-<?php echo $prefixid; ?>" v-model="vparam.contentevaluate[vparam.index]">
							<label for="2-<?php echo $prefixid; ?>">☆</label>

							<input type="radio" name="rating" value="1" id="1-<?php echo $prefixid; ?>" v-model="vparam.contentevaluate[vparam.index]">
							<label for="1-<?php echo $prefixid; ?>">☆</label>
						</div>

						<!--
						<div class="rating">
							<label for="5-<?php echo $prefixid; ?>">☆</label> <input type="radio" name="rating" value="5" id="5-<?php echo $prefixid; ?>" v-model="vparam.contentevaluate[vparam.index]">
							<label for="4-<?php echo $prefixid; ?>">☆</label> <input type="radio" name="rating" value="4" id="4-<?php echo $prefixid; ?>" v-model="vparam.contentevaluate[vparam.index]">
							<label for="3-<?php echo $prefixid; ?>">☆</label> <input type="radio" name="rating" value="3" id="3-<?php echo $prefixid; ?>" v-model="vparam.contentevaluate[vparam.index]">
							<label for="2-<?php echo $prefixid; ?>">☆</label> <input type="radio" name="rating" value="2" id="2-<?php echo $prefixid; ?>" v-model="vparam.contentevaluate[vparam.index]">
							<label for="1-<?php echo $prefixid; ?>">☆</label><input type="radio" name="rating" value="1" id="1-<?php echo $prefixid; ?>" v-model="vparam.contentevaluate[vparam.index]">
						</div>
						
						-->
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="comentarios<?php echo $prefixid; ?>" role="tabpanel" aria-labelledby="comentarios-tab<?php echo $prefixid; ?>">
				<div class="container-comment-sender my-4 mr-3">
					<div class="form-group">
						<div class="input-group input-group-send">
							<input type="text" class="form-control" placeholder="Deixe seu comentário" id="message" @keyup.enter="addContentComment()" v-model="vparam.comment" />
							<span class="input-group-addon ml-4" @click.prevent="addContentComment()">
								<i class="fas fa-paper-plane"></i>
							</span>
						</div>
					</div>
				</div>

				<div class="container-comments" v-if="rowsdata4.length">
					<div class="container-comment d-flex mb-4" v-for="(item, index) in rowsdata4" v-if="item.moduleinstance==vparam.currentdata.id">
						<!--<img src="https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg.jpg?resize=760,506" class="rounded-circle mr-4" alt="avatar">-->

						<div class="comment-card d-flex flex-column px-4 py-2">
							<div class="d-flex">
								<p class="comment-name mb-0" v-html="item.user"></p>
							</div>
							<div class="d-flex">
								<span class="comment-date mr-4" v-html="item.cdate"></span>
								<span class="comment-time" v-html="item.ctime"></span>
							</div>
							<div class="d-flex mt-4">
								<p class="comment-text" v-html="item.content"></p>
								<a v-if="canDeleteMessage(item)" @click.prevent="showModalToConfirmDelete('comment',index)"><i class="delete fas fa-trash-alt"></i></a>
							</div>
						</div>
					</div>

				</div>


			</div>

			<div class="tab-pane fade" id="discussions<?php echo $prefixid; ?>" role="tabpanel" aria-labelledby="discussions-tab<?php echo $prefixid; ?>">
				<div class="container-comment-sender my-4 mr-3">
					<div class="form-group">
						<div class="input-group input-group-send">
							<input type="text" class="form-control" placeholder="Evie uma mensagem" id="message" @keyup.enter="addContentDiscussion()" v-model="vparam.discussion" />
							<span class="input-group-addon ml-4" @click.prevent="addContentDiscussion()">
								<i class="fas fa-paper-plane"></i>
							</span>
						</div>
					</div>
				</div>

				<div class="container-comments" v-if="rowsdata5.length">
					<div class="container-comment d-flex mb-4" v-for="(item, index) in rowsdata5" v-if="item.moduleinstance==vparam.currentdata.id">
						<!--<img src="https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg.jpg?resize=760,506" class="rounded-circle mr-4" alt="avatar">-->

						<div class="comment-card d-flex flex-column px-4 py-2">
							<div class="comment-user-wrapper">
								<div class="d-flex align-items-center justify-content-between">
									<p class="comment-name mb-0" v-html="item.user"></p>
									<div class="d-flex">
										<ul class="list-inline d-sm-flex my-0">
											<li class="list-inline-item">
												<a class="color-like" @click.prevent="addLikeAction('badiu.admin.message.discussion',item.id,1)">
													<i class="fa fa-thumbs-up"></i>

												</a><span v-if="vparam.discussionlike[item.id]">({{vparam.discussionlike[item.id].countlike}})</span>
												<span v-if="!vparam.discussionlike[item.id]">(0)</span>
											</li>
											<li class="list-inline-item">
												<a class="color-dislike" @click.prevent="addLikeAction('badiu.admin.message.discussion',item.id,0)">
													<i class="far fa-heart"></i>

												</a><span v-if="vparam.discussionlike[item.id]">({{vparam.discussionlike[item.id].countunlike}})</span>
												<span v-if="!vparam.discussionlike[item.id]">(0)</span>
											</li>
										</ul>
									</div>
								</div>
								<div class="d-flex comment-date-wrapper align-items-center">
									<i class="far fa-clock fa-sm mr-2"></i>
									<span class="comment-date mr-4" v-html="item.cdate"></span>
									<span class="comment-time" v-html="item.ctime"></span>
								</div>

								<div class="d-flex my-4 comment-text-wrapper">
									<p class="comment-text" v-html="item.content"></p>
									<a v-if="canDeleteMessage(item)" @click.prevent="showModalToConfirmDelete('discussion',index)"><i class="delete fas fa-trash-alt"></i></a>
								</div>

								<div class="d-flex">
									<div class="input-group input-group-send align-items-center">
										<input type="text" class="form-control" placeholder="Comente essa mensagem" id="message" @keyup.enter="addResponseDiscussion(item.id)" v-model="vparam.response" />
										<button type="button" @click.prevent="addResponseDiscussion(item.id)" class="btn btn-secondary btn-sm ml-2">Responder</button>
									</div>
								</div>
							</div>

							<div class="commentdiscussion">
								<div class="d-flex mt-4 ml-5">
									<h3><b>Comentários</b></h3>
									<hr />
								</div>

								<!-- respostas comentario -->
								<div class="comment-answer ml-5" v-for="(citem, cindex) in rowsdata6" v-if="citem.discussionid==item.id">
									<div class="d-flex align-items-center justify-content-between">
										<p class="comment-name mb-0" v-html="citem.user"></p>

										<div class="d-flex">
											<ul class="list-inline d-sm-flex my-0">
												<li class="list-inline-item">
													<a class="color-like" @click.prevent="addLikeAction('badiu.admin.message.message',citem.id,1)">
														<i class="fa fa-thumbs-up"></i>

													</a><span v-if="vparam.commentlike[citem.id]">({{vparam.commentlike[citem.id].countlike}})</span>
													<span v-if="!vparam.commentlike[citem.id]">(0)</span>
												</li>
												<li class="list-inline-item">
													<a class="color-dislike" @click.prevent="addLikeAction('badiu.admin.message.message',citem.id,0)">
														<i class="far fa-heart"></i>

													</a><span v-if="vparam.commentlike[citem.id]">({{vparam.commentlike[citem.id].countunlike}})</span>
													<span v-if="!vparam.commentlike[citem.id]">(0)</span>
												</li>
											</ul>
										</div>
									</div>

									<div class="d-flex comment-date-wrapper align-items-center">
										<i class="far fa-clock fa-sm mr-2"></i>
										<span class="comment-date mr-4" v-html="citem.cdate"></span>
										<span class="comment-time" v-html="citem.ctime"></span>
									</div>

									<div class="d-flex mt-4 comment-answer-text">
										<p class="comment-text" v-html="citem.content"></p>
										<a v-if="canDeleteMessage(citem)" @click.prevent="showModalToConfirmDelete('discussioncomment',cindex)"><i class="delete fas fa-trash-alt"></i></a>
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>


			</div>
		</div>
	</div>



	<!-- start modal review trial-->

	<div id="_badiu_admin_cms_playlist_listvideo_mdelete_panel" style="visibility:hidden">
		<div id="_badiu_admin_cms_playlist_listvideo_mdelete_panel_status" v-bind:class="[vparam.deleteconfirm.showmodalcrtitem]" v-bind:style="{ display: vparam.deleteconfirm.showmodalcrt }" role="dialog" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" v-html="vparam.deleteconfirm.title"></h5>
						<button type="button" class="close" v-on:click="vparam.deleteconfirm.showmodalcrt = 'none'" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<b>Texto: </b>
						<p v-html="vparam.deleteconfirm.description"></p>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-primary" @click="deleteMessage(vparam.deleteconfirm.item,vparam.deleteconfirm.index)">Apagar</button>
						<button type="button" class="btn btn-secondary" v-on:click="vparam.deleteconfirm.showmodalcrt = 'none'">Cancelar</button>

					</div>
				</div>
			</div>
		</div>
	</div>


	<!--end modal -->

</div>
<style>
	.badiu-panel-view-mview {
		width: 100%;
		overflow: hidden;
		display: flex;
		flex-direction: row;
		justify-content: center;
	}

	.badiu-panel-video {
		max-width: 740px;
		min-width: 250px;

		width: 100%;
		height: 100%;
		position: relative;
		margin: 0 auto;

		display: flex;
		flex-direction: column;
		align-items: center;
	}

	.badiu-panel-video .leftnavegation {
		position: absolute;
		top: 40%;
		left: 0;
		font-size: 35px;
		color: blue;
		z-index: 10;
	}

	.badiu-panel-video .rightnavegation {
		position: absolute;
		top: 40%;
		right: 0;
		font-size: 35px;
		color: blue;
		z-index: 10;
	}

	.badiu-panel-video .pagenav {
		height: 50px;
		position: relative;
		width: 100%;
		text-align: center;
	}

	.page-item .active {
		background-color: blue;
		color: white;
	}

	.badiu-panel-video li {
		list-style-type: none;
	}

	/*.badiu-panel-video .pagenav .pagination{
	margin: 0 auto ;
	
}*/

	.badiu-panel-view-manager {
		/*background-color: red;*/
		/*position: absolute;*/
		width: 100%;
		overflow: hidden;
		display: flex;
		flex-direction: row;
		justify-content: center;
	}

	/* TABS */
	.container-tabs .nav-tabs {
		border-bottom: 1px solid #CED4DA;
		padding-left: 36px;
	}

	.container-tabs .nav-tabs .nav-item {
		list-style: none;
	}

	.container-tabs .nav-tabs .nav-item .nav-link {
		padding: .5rem 2.5rem;
	}

	.container-tabs .nav-tabs .nav-item .nav-link {
		font-family: Roboto;
		font-weight: 400;
		font-size: 16px;
		color: #202020;
	}

	.badiu-tabs-content h3 {
		font-family: Roboto;
		font-weight: 500;
		line-height: 28px;
		color: #202020;
		font-size: 14px;
	}

	.badiu-tabs-content p {
		font-family: Roboto;
		font-weight: 400;
		font-size: 16px;
		color: #202020;
	}

	.badiu-tabs-content .video-title {
		font-family: Roboto;
		font-weight: 500;
		font-size: 18px;
		color: #202020;
	}

	/* TABS */

	/* ESTRELAS RATING */
	.rating {
		display: flex;
		flex-direction: row-reverse;
		justify-content: center
	}

	.rating>input {
		display: none
	}

	.rating>label {
		position: relative;
		width: 1em;
		font-size: 32px;
		color: rgba(0, 0, 0, 0.5);
		cursor: pointer;
		margin: 0;
	}

	.rating>label::before {
		content: "\2605";
		position: absolute;
		opacity: 0
	}

	.rating>label:hover:before,
	.rating>label:hover~label:before {
		opacity: 1 !important;
		color: #FBC60D;
	}

	.rating>input:checked~label:before {
		opacity: 1;
		color: #FBC60D;
	}

	.rating:hover>input:checked~label:before {
		opacity: 0.4
	}

	/* ESTRELAS RATING */

	/* SWTICH INPUT */
	.switch input[type=checkbox] {
		display: none;
	}

	.switch input[type=checkbox]+label {
		position: relative;
		min-width: calc(calc(2.375rem * .8) * 2);
		border-radius: calc(2.375rem * .8);
		height: calc(2.375rem * .8);
		line-height: calc(2.375rem * .8);
		display: inline-block;
		cursor: pointer;
		outline: none;
		user-select: none;
		vertical-align: middle;
		text-indent: calc(calc(calc(2.375rem * .8) * 2) + .5rem);
	}

	.switch input[type=checkbox]+label::before,
	.switch input[type=checkbox]+label::after {
		content: '';
		position: absolute;
		top: 0;
		left: 0;
		width: calc(calc(2.375rem * .8) * 2);
		bottom: 0;
		display: block;
	}

	.switch input[type=checkbox]+label::before {
		right: 0;
		background-color: #dee2e6;
		border-radius: calc(2.375rem * .8);
		transition: .2s all;
	}

	.switch input[type=checkbox]+label::after {
		top: 2px;
		left: 2px;
		width: calc(calc(2.375rem * .8) - calc(2px * 2));
		height: calc(calc(2.375rem * .8) - calc(2px * 2));
		border-radius: 50%;
		background-color: #fff;
		transition: all 0.3s ease-in 0s;
		;
	}

	.switch input[type=checkbox]:checked+label::before {
		background-color: #08d;
	}

	.switch input[type=checkbox]:checked+label::after {
		margin-left: calc(2.375rem * .8);
	}

	.switch input[type=checkbox]:focus+label::before {
		outline: none;
		box-shadow: 0 0 0 .2rem rgba(0, 136, 221, .25);
	}

	.switch input[type=checkbox]:disabled+label {
		color: #868e96;
		cursor: not-allowed;
	}

	.switch input[type=checkbox]:disabled+label::before {
		background-color: #e9ecef;
	}

	.switch.switch-xs {
		font-size: .8rem;
	}

	.switch.switch-xs input[type=checkbox]+label {
		min-width: calc(calc(1.5375rem * .8) * 2);
		height: calc(1.5375rem * .8);
		line-height: calc(1.5375rem * .8);
		text-indent: calc(calc(calc(1.5375rem * .8) * 2) + .5rem);
	}

	.switch.switch-xs input[type=checkbox]+label::before {
		width: calc(calc(1.5375rem * .8) * 2);
	}

	.switch.switch-xs input[type=checkbox]+label::after {
		width: calc(calc(1.5375rem * .8) - calc(2px * 2));
		height: calc(calc(1.5375rem * .8) - calc(2px * 2));
	}

	.switch.switch-xs input[type=checkbox]:checked+label::after {
		margin-left: calc(1.5375rem * .8);
	}

	.switch+.switch {
		margin-left: 1rem;
	}

	/* SWTICH INPUT */

	/* TAB COMENTARIOS */
	.container-comments {
		overflow-y: auto;
		width: 100%;
		height: 350px;
		padding: 0 40px 0 0;
	}

	.container-comments::-webkit-scrollbar {
		width: 20px;
	}

	.container-comments::-webkit-scrollbar-track {
		border-radius: 100px;
	}

	.container-comments::-webkit-scrollbar-thumb {
		border-radius: 100px;
		border: 4px solid transparent;
		background-clip: content-box;
		background-color: #C4C4C4;
	}

	.container-comment .comment-card {
		background: rgba(196, 196, 196, 0.1);
		border: 1px solid rgba(0, 0, 0, 0.15);
		border-radius: 0px 30px 30px 30px;
		width: 100%;
	}

	.container-comment.active-user .comment-card {
		background: rgba(106, 201, 178, 0.3);
		border: none;
	}

	.container-comment img {
		width: 66px;
		height: 66px;
		object-fit: cover;
	}

	.container-comment .comment-name {
		font-family: Roboto;
		font-weight: 500;
		font-size: 18px;
		color: #202020;
	}

	.container-comment .comment-date,
	.container-comment .comment-time {
		font-family: Roboto;
		font-weight: 400;
		font-size: 12px;
		color: #87878b;
	}

	.container-comment .comment-text {
		font-family: Roboto;
		font-style: italic;
		font-weight: 400;
		font-size: 16px;
		color: #202020;
		margin: 0;
	}

	.container-comment-sender .input-group {
		display: flex;
		align-items: center;
	}

	.input-group-send .form-control {
		background: rgba(206, 212, 218, 0.5) !important;
		border-radius: 4px !important;
		border: none !important;
		padding-left: 16px;
	}

	.input-group-send .form-control::placeholder {
		font-family: Roboto;
		font-weight: 500;
		font-size: 18px;
		color: rgba(32, 32, 32, 0.2);
		opacity: 1;
	}

	.input-group-send .form-control:-ms-input-placeholder {
		font-family: Roboto;
		font-weight: 500;
		font-size: 18px;
		color: rgba(32, 32, 32, 0.6);
	}

	.input-group-send .form-control::-ms-input-placeholder {
		font-family: Roboto;
		font-weight: 500;
		font-size: 18px;
		color: rgba(32, 32, 32, 0.6);
	}

	.input-group-send .input-group-addon {
		cursor: pointer;
		opacity: 1;
		transition: all .2s;
	}

	.input-group-send .input-group-addon:hover {
		opacity: 0.6;
	}

	/* TAB COMENTARIOS */




	.comment-card .btn-sm {
		height: 32px;
	}

	.comment-user-wrapper {}

	.color-like i {
		color: #63734A;
	}

	.color-dislike i {
		color: #ED0024;
	}

	.comment-text-wrapper {
		background: #f6f6f7;
		border: 1px solid #edeff2;
		border-radius: 8px;
		padding: 16px;
		margin: 12px 0;
	}

	.comment-date-wrapper i {
		color: #87878b;
	}

	.comment-answer {
		background-color: #FFFFFF;
		border: 1px solid #e5e6eb;
		margin: 8px 0;
		border-radius: 8px;
		padding: 8px;
	}

	.delete {
		color: red;
		padding-left: 10px;
	}

	.iframe-container {
		position: relative;
		max-width: 650px;
		width: 100%;
		overflow: hidden;
		padding-top: 56.25%;
		/* 16:9 Aspect Ratio */
	}

	.responsive-iframe {
		position: absolute;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
		width: 100%;
		height: 100%;
		border: none;
	}

	.youtube-link {
		font-size: 16px;
		width: 100%;
		padding: 0;
		word-break: break-all;
	}

	@media (max-width: 767.98px) {
		iframe[src*="vimeo.com"] {
			width: 100%!important;
			height: 100%!important;
		}
	}
</style>