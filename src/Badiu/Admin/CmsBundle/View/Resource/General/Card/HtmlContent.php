<?php
$parentid = $container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
$prefixid = "";
if (!empty($parentid)) {
	$prefixid = "-" . $parentid;
}
$baseresoursepath = $container->get('templating.helper.assets')->getUrl('bundles/badiuthemecore/tms');

$fmparam=array('repositoryid'=>$parentid,'enable'=>array('page'=>1,'file'=>1,'image'=>1,'card'=>1,'videoplaylist'=>1,'faq'=>1));

$cmfactorytemplate=$container->get('badiu.admin.cms.resourece.factorytemplate');
$listitemadd=$cmfactorytemplate->getListItemsAddUrl($fmparam);

?>
<div id="_badiu_theme_core_dashboard_vuejs<?php echo $prefixid; ?>">
 	<div class="badiu-tms-my-student-dashboard-default">
<div class="dropdown show">
  <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Adicionar contéudo
  </a>

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
   <?php foreach ($listitemadd as $lim) {
	   $label= $utildata->getVaueOfArray($lim, 'label');
	   $url= $utildata->getVaueOfArray($lim, 'url');
	   ?>
    <a class="dropdown-item" href="<?php echo $url; ?>"><?php echo $label; ?></a>
   <?php } ?>
  </div>
</div>
<h5 class="result-search">Resultado: <?php echo $infoResult; ?></h5>
 <div class="row my-4" v-if="tabledata">
							<div class="col-sm-6 col-md-3 mb-2" v-for="(item, index) in tabledata">
                                <div class="card-content">
                                   <!-- <span class="curso-tipo-status">
                                        <p><img src="images/icons/unlock.svg" class="mr-2">Curso livre</p>
										</span>
									-->
                                    <div>
                                        <div class="card-img">
                                            <img v-if="item.defaultimage.value" :src="item.defaultimage.value" class="card-image" alt="">

                                        </div>

                                        <div class="card-info d-flex align-items-baseline">

                                            <a v-if="item.disciplinecategoryurlview" :href="item.disciplinecategoryurlview" class="mr-1"><i class="fas fa-link"></i></a>
                                            <a v-if="item.disciplinecategoryurledit" :href="item.disciplinecategoryurledit" class="mr-1"><i class="fas fa-edit"></i></a>

                                            <span class="text-categoria" v-html="item.category.value"></span>
                                        </div>

                                        <div class="card-desc">
                                            <div class="d-flex align-items-baseline mb-1">
												
                                                <a v-if="item.disciplineurlview" :href="item.disciplineurlview" class="mr-1"><i class="fas fa-link"></i></a>
                                                <a v-if="item.disciplineurledit" :href="item.disciplineurledit" class="mr-1"><i class="fas fa-edit"></i></a>

                                                <h5 class="card-title mb-0">
												 <span v-html="item.struture.value"></span>
												</h5>
												 
                                            </div>
                                            <div class="d-flex align-items-center justify-content-between mb-4">
                                                <div class="d-flex align-items-baseline mb-1">
                                                    <a v-if="item.classeadminlmsurl" :href="item.classeadminlmsurl" class="mr-1"><i class="fas fa-external-link-alt"></i></a>
													<a v-if="item.classeurlview" :href="item.classeurlview" class="mr-1"><i class="fas fa-link"></i></a>
                                                    <a v-if="item.urledit.value" :href="item.urledit.value" class="mr-1"><i class="fas fa-edit"></i></a>

                                                    <span class="text-tipo text-color-4" v-html="item.name.value"></span>
                                                </div>
												 
                                                <span class="d-flex align-items-center ml-4 text-cargah text-color-4" v-html="item.typeshortname.value"></span>
												
												
                                            </div>
											
                                            <p v-html="item.abstractordescription.value"></p>
											
                                        </div>
                                    </div>

                                    <div class="card-options">
                                        

                                        <div class="d-flex align-items-center justify-content-between">
                                            <a v-if="item.itemviewurl.value && item.itemviewurltarget.value=='_blank'" target="_blank" :href="item.itemviewurl.value" class="btn btn-card-blue">Acessar</a>
											<a v-if="item.itemviewurl.value && item.itemviewurltarget.value!='_blank'"  :href="item.itemviewurl.value" class="btn btn-card-blue">Acessar</a>
                                            
                                        </div>
                                    </div>
                                </div>
                            
                    </div>
						   
	</div>
  </div>
    <?php echo $pagingout; ?>
</div>
<style>
	
</style>