<?php

namespace Badiu\Admin\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuAdminCmsBundle:Default:index.html.twig', array('name' => $name));
    }
}
