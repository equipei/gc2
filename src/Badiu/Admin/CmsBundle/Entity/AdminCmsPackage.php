<?php

namespace Badiu\Admin\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminCmsPackage 
 *
 * @ORM\Table(name="admin_cms_package", uniqueConstraints={
*      @ORM\UniqueConstraint(name="admin_cms_package_shortname_uix", columns={"entity", "shortname"})},
*       indexes={@ORM\Index(name="admin_cms_package_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_cms_package_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_cms_package_idnumber_ix", columns={"idnumber"}),
 *              @ORM\Index(name="admin_cms_package_description_ix", columns={"description"}),
 *              @ORM\Index(name="admin_cms_package_statusid", columns={"statusid"}),
 *              @ORM\Index(name="admin_cms_package_sortorder_ix", columns={"sortorder"}),
 *              @ORM\Index(name="admin_cms_package_edition_ix", columns={"edition"}),
 *              @ORM\Index(name="admin_cms_package_vkey_ix", columns={"vkey"}),
 *              @ORM\Index(name="admin_cms_package_categoryid_ix", columns={"categoryid"}),
 *              @ORM\Index(name="admin_cms_package_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="admin_cms_package_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_cms_package_marker_ix", columns={"marker"}), 
 *              @ORM\Index(name="admin_cms_package_useridadd_ix", columns={"useridadd"})})
 *          @ORM\Entity
 */
class AdminCmsPackage {

	/**
	 * @var integer 
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var AdminCmsPackageStatus
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsPackageStatus")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
	 * })
	 */
	private $statusid;



	/**
	 * @var AdminCmsPackageCategory
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsPackageCategory")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
	 * })
	 */
	private $categoryid;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

		/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255,nullable=true)
	 */
	private $shortname;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="edition", type="integer",length=255, nullable=false)
	 */
	private $edition = 1;

	/**
         * this is the version key
         * 
	 * @var string 
	 *
	 * @ORM\Column(name="vkey", type="string",length=255, nullable=false)
	 */
	private $vkey; 

		/**
	 * @var integer
	 *
	 * @ORM\Column(name="sortorder", type="float", precision=10, scale=0, nullable=false)
	 */
	private $sortorder;

       
    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey; 

  /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;
        
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=255,nullable=true)
	 */
	private $description;

        /**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;

		 	      /**
     * @var string
     *
     * @ORM\Column(name="marker", type="string", length=255, nullable=true)
     */
		private $marker;
		
        /**
	 * @var string
	 *
	 * @ORM\Column(name="dconfig", type="text", nullable=true)
	 */
	private $dconfig;
        
	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted;

        function getId() {
            return $this->id;
        }

        function getStatusid(): AdminCmsPackageStatus {
            return $this->statusid;
        }

        function getCategoryid(): AdminCmsPackageCategory {
            return $this->categoryid;
        }

        function getEntity() {
            return $this->entity;
        }

        function getShortname() {
            return $this->shortname;
        }

        function getEdition() {
            return $this->edition;
        }

        function getVkey() {
            return $this->vkey;
        }

        function getSortorder() {
            return $this->sortorder;
        }

        function getModulekey() {
            return $this->modulekey;
        }

        function getModuleinstance() {
            return $this->moduleinstance;
        }

        function getDescription() {
            return $this->description;
        }

        function getIdnumber() {
            return $this->idnumber;
        }

        function getMarker() {
            return $this->marker;
        }

        function getDconfig() {
            return $this->dconfig;
        }

        function getParam() {
            return $this->param;
        }

        function getTimecreated(): \DateTime {
            return $this->timecreated;
        }

        function getTimemodified(): \DateTime {
            return $this->timemodified;
        }

        function getUseridadd() {
            return $this->useridadd;
        }

        function getUseridedit() {
            return $this->useridedit;
        }

        function getDeleted() {
            return $this->deleted;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setStatusid(AdminCmsPackageStatus $statusid) {
            $this->statusid = $statusid;
        }

        function setCategoryid(AdminCmsPackageCategory $categoryid) {
            $this->categoryid = $categoryid;
        }

        function setEntity($entity) {
            $this->entity = $entity;
        }

        function setShortname($shortname) {
            $this->shortname = $shortname;
        }

        function setEdition($edition) {
            $this->edition = $edition;
        }

        function setVkey($vkey) {
            $this->vkey = $vkey;
        }

        function setSortorder($sortorder) {
            $this->sortorder = $sortorder;
        }

        function setModulekey($modulekey) {
            $this->modulekey = $modulekey;
        }

        function setModuleinstance($moduleinstance) {
            $this->moduleinstance = $moduleinstance;
        }

        function setDescription($description) {
            $this->description = $description;
        }

        function setIdnumber($idnumber) {
            $this->idnumber = $idnumber;
        }

        function setMarker($marker) {
            $this->marker = $marker;
        }

        function setDconfig($dconfig) {
            $this->dconfig = $dconfig;
        }

        function setParam($param) {
            $this->param = $param;
        }

        function setTimecreated(\DateTime $timecreated) {
            $this->timecreated = $timecreated;
        }

        function setTimemodified(\DateTime $timemodified) {
            $this->timemodified = $timemodified;
        }

        function setUseridadd($useridadd) {
            $this->useridadd = $useridadd;
        }

        function setUseridedit($useridedit) {
            $this->useridedit = $useridedit;
        }

        function setDeleted($deleted) {
            $this->deleted = $deleted;
        }


        function getDtype() {
            return $this->dtype;
        }
        function setDtype($dtype) {
            $this->dtype = $dtype;
        }

}
