<?php

namespace Badiu\Admin\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminCmsResourceLog
 *
 * @ORM\Table(name="admin_cms_resource_log",

 *       indexes={@ORM\Index(name="admin_cms_resource_log_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_cms_resource_log_dtype_ix", columns={"dtype"}),
  *               @ORM\Index(name="admin_cms_resource_log_action", columns={"action"}),
 *              @ORM\Index(name="admin_cms_resource_log_ip", columns={"ip"}),
 *                @ORM\Index(name="admin_cms_resource_log_url", columns={"url"}),
 *                @ORM\Index(name="admin_cms_resource_log_urlreferer", columns={"urlreferer"}),
 *                @ORM\Index(name="admin_cms_resource_log_timecreated", columns={"timecreated"}),
 *                @ORM\Index(name="admin_cms_resource_log_useridaccessedby", columns={"useridaccessedby"}),
 *              @ORM\Index(name="admin_cms_resource_log_userid_ix", columns={"userid"})})
 *          @ORM\Entity
 */
class AdminCmsResourceLog {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

/**
	 * @var AdminCmsResource
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsResource")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="resourceid", referencedColumnName="id", nullable=false)
	 * })
	 */
	private $resourceid;
	
	/**
	 * @var AdminCmsResource
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsResourceContent")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="contentid", referencedColumnName="id", nullable=true)
	 * })
	 */
	private $contentid;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="action", type="string", length=255, nullable=true)
	 */
	private $action;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ip", type="string", length=255, nullable=true)
	 */
	private $ip;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="url", type="string", length=255, nullable=true)
	 */
	private $url;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="urlreferer", type="string", length=255, nullable=true)
	 */
	private $urlreferer;
	
	/**
	 * @var \Badiu\System\UserBundle\Entity\SystemUser
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="userid", referencedColumnName="id")
	 * })
	 */
	private $userid;
	
	
	/**
	 * @var \Badiu\System\UserBundle\Entity\SystemUser
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="useridaccessedby", referencedColumnName="id")
	 * })
	 */
	private $useridaccessedby;
	

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype;  //user|cron|auto|webservice
	
	
        /**
	 * @var string
	 *
	 * @ORM\Column(name="clientdevice", type="string", length=255, nullable=true)
	 */
	private $clientdevice; //pc | tablet | mobile
	
	     /**
	 * @var string
	 *
	 * @ORM\Column(name="dinfo", type="text", nullable=true)
	 */
	private $dinfo;
        
        /**
	 * @var string
	 *
	 * @ORM\Column(name="dnavegation", type="text", nullable=true)
	 */
	private $dnavegation;
    
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=255,nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	

	public function getId() {
		return $this->id;
	}

	public function getEntity() {
		return $this->entity;
	}

	

	public function getDescription() {
		return $this->description;
	}

	public function getParam() {
		return $this->param;
	}

	public function getTimecreated() {
		return $this->timecreated;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setEntity($entity) {
		$this->entity = $entity;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setParam($param) {
		$this->param = $param;
	}

	public function setTimecreated(\DateTime $timecreated) {
		$this->timecreated = $timecreated;
	}



	

	public function getDtype() {
		return $this->dtype;
	}

	public function setDtype($dtype) {
		$this->dtype = $dtype;
	}

	
		function getResourceid() {
            return $this->resourceid;
        }
		function setResourceid($resourceid) {
            $this->resourceid = $resourceid;
        }
		
	 function getContentid() {
            return $this->contentid;
        }
		function setContentid($contentid) {
            $this->contentid = $contentid;
        }


public function setUserid($userid) {
		$this->userid = $userid;
	}

	function getUserid() {
		return $this->userid;
	}

	function getAction() {
		return $this->action;
	}

	function getIp() {
		return $this->ip;
	}

	function getUrl() {
		return $this->url;
	}

	function getUrlreferer() {
		return $this->urlreferer;
	}

	function setAction($action) {
		$this->action = $action;
	}

	function setIp($ip) {
		$this->ip = $ip;
	}

	function setUrl($url) {
		$this->url = $url;
	}

	function setUrlreferer($urlreferer) {
		$this->urlreferer = $urlreferer;
	}

        function getClientdevice() {
            return $this->clientdevice;
        }

       

        function setClientdevice($clientdevice) {
            $this->clientdevice = $clientdevice;
        }

        

        function getDinfo() {
            return $this->dinfo;
        }

        function getDnavegation() {
            return $this->dnavegation;
        }

        function setDinfo($dinfo) {
            $this->dinfo = $dinfo;
        }

        function setDnavegation($dnavegation) {
            $this->dnavegation = $dnavegation;
        }	

		function getUseridaccessedby() {
            return $this->useridaccessedby;
        }

        function setUseridaccessedby($useridaccessedby) {
            $this->useridaccessedby = $useridaccessedby;
        }
		
}
