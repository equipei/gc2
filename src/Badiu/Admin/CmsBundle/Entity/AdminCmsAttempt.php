<?php

namespace Badiu\Admin\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminCmsAttempt 
 *
 * @ORM\Table(name="admin_cms_attempt",
 *       indexes={@ORM\Index(name="admin_cms_attempt_status_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_cms_attempt_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_cms_attempt_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="admin_cms_attempt_packageid_ix", columns={"packageid"}),
 *              @ORM\Index(name="admin_cms_attempt_status", columns={"status"}),
 *              @ORM\Index(name="admin_cms_attempt_grade_ix", columns={"grade"}),
 *              @ORM\Index(name="admin_cms_attempt_percentage_ix", columns={"percentage"}),
 *              @ORM\Index(name="admin_cms_attempt_timestart_ix", columns={"timestart"}),
 *              @ORM\Index(name="admin_cms_attempt_timeend_ix", columns={"timeend"}),
 *              @ORM\Index(name="admin_cms_attempt_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_cms_attempt_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="admin_cms_attempt_marker_ix", columns={"marker"}), 
 *              @ORM\Index(name="admin_cms_attempt_useridadd_ix", columns={"useridadd"})})
 *          @ORM\Entity
 */
class AdminCmsAttempt {

	/**
	 * @var integer 
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var AdminCmsPackage
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsPackage")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="packageid", referencedColumnName="id")
	 * })
	 */
	private $packageid;

    /**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id")
     * })
     */
    private $userid;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="status", type="string", length=50, nullable=false)
	 */
	private $status;

    /**
	 * @var float
	 *
	 * @ORM\Column(name="grade", type="float", precision=10, scale=0, nullable=true)
	 */
    private $grade;
    
    /**
	 * @var float
	 *
	 * @ORM\Column(name="percentage", type="float", precision=10, scale=0, nullable=true)
	 */
    private $percentage;
    
    /**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timestart", type="datetime", nullable=true)
	 */
    private $timestart;
    
    /**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timeend", type="datetime", nullable=true)
	 */
	private $timeend;
	
		 /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey; 

  /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;
        
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=255,nullable=true)
	 */
	private $description;

        /**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;

		 	      /**
     * @var string
     *
     * @ORM\Column(name="marker", type="string", length=255, nullable=true)
     */
	private $marker;
		
        /**
	 * @var string
	 *
	 * @ORM\Column(name="dconfig", type="text", nullable=true)
	 */
	private $dconfig;
        
	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted;


}
