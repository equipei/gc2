<?php

namespace Badiu\Admin\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *AdminCmsResourceMenu
 *
 * @ORM\Table(name="admin_cms_resource_menu", uniqueConstraints={
 *                                                  @ORM\UniqueConstraint(name="admin_cms_resource_menu_shortname_uix", columns={"entity", "shortname"})},
 *                                              indexes={@ORM\Index(name="admin_cms_resource_menu_entity_ix", columns={"entity"}),
 *                                              @ORM\Index(name="admin_cms_resource_menu_name_ix", columns={"name"}),
 *                                              @ORM\Index(name="admin_cms_resource_menu_shortname_ix", columns={"shortname"}),
 *                                              @ORM\Index(name="admin_cms_resource_menu_dtype_ix", columns={"dtype"}),
 *                                              @ORM\Index(name="admin_cms_resource_menu_idpath_ix", columns={"idpath"}),
 *                                              @ORM\Index(name="admin_cms_resource_menu_orderidpath_ix", columns={"orderidpath"}),
 *                                              @ORM\Index(name="admin_cms_resource_menu_sortorder_ix", columns={"sortorder"}),
 *                                              @ORM\Index(name="admin_cms_resource_menu_deleted_ix", columns={"deleted"}),
 *                                              @ORM\Index(name="admin_cms_resource_menu_marker_ix", columns={"marker"}),
 *                                              @ORM\Index(name="admin_cms_resource_menu_statusid_ix", columns={"statusid"}),
*                                              @ORM\Index(name="admin_cms_resource_menu_repositoryid_ix", columns={"repositoryid"}),
 *                                              @ORM\Index(name="admin_cms_resource_menu_idnumber_ix", columns={"idnumber"})})
 *
 * @ORM\Entity
 */
class AdminCmsResourceMenu {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

        	/**
	 * @var AdminCmsResourceStructureStatus
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsResourceStructureStatus")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
	 * })
	 */
	private $statusid;

		/**
	 * @var AdminCmsRepository
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsRepository")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="repositoryid", referencedColumnName="id")
	 * })
	 */
	private $repositoryid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
	 */
	private $shortname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype; //is shortname of repositoy
        
   /**
     * @var string
     *
     * @ORM\Column(name="defaulturl", type="string", length=255, nullable=true)
     */
    private $defaulturl;

        
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="parent", type="bigint", nullable=true)
	 */
	private $parent; //id of father

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="level", type="integer", nullable=true)
	 */
	private $level; //generation of idpath

	/**
	 * @var string
	 *
	 * @ORM\Column(name="idpath", type="string", length=255, nullable=false)
	 */
	private $idpath; //id tree example: 1, 1.1, 1.2,1.2.1

	/**
	 * @var string
	 *
	 * @ORM\Column(name="path", type="string", length=255, nullable=true)
	 */
	private $path; //hierarchy of id father exemp 1, 1/2, 1/2/4

	/**
	 * @var float
	 *
	 * @ORM\Column(name="orderidpath", type="float", precision=10, scale=0, nullable=false)
	 */
	private $orderidpath; //order of id tree

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="sortorder", type="bigint", nullable=true)
	 */
	private $sortorder;

	/**
     * @var string
     *
     * @ORM\Column(name="marker", type="string", length=255, nullable=true)
     */
    private $marker;
	

	       /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted;


	public function getId() {
		return $this->id;
	}

	public function getEntity() {
		return $this->entity;
	}

	public function getName() {
		return $this->name;
	}

	public function getIdpath() {
		return $this->idpath;
	}

	public function getPath() {
		return $this->path;
	}

	public function getOrderidpath() {
		return $this->orderidpath;
	}

	public function getSortorder() {
		return $this->sortorder;
	}

	public function getIdnumber() {
		return $this->idnumber;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getParam() {
		return $this->param;
	}

	public function getTimecreated() {
		return $this->timecreated;
	}

	public function getTimemodified() {
		return $this->timemodified;
	}

	public function getUseridadd() {
		return $this->useridadd;
	}

	public function getUseridedit() {
		return $this->useridedit;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setEntity($entity) {
		$this->entity = $entity;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setIdpath($idpath) {
		$this->idpath = $idpath;
	}

	public function setPath($path) {
		$this->path = $path;
	}

	public function setOrderidpath($orderidpath) {
		$this->orderidpath = $orderidpath;
	}

	public function setSortorder($sortorder) {
		$this->sortorder = $sortorder;
	}

	public function setIdnumber($idnumber) {
		$this->idnumber = $idnumber;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setParam($param) {
		$this->param = $param;
	}

	public function setTimecreated(\DateTime $timecreated) {
		$this->timecreated = $timecreated;
	}

	public function setTimemodified(\DateTime $timemodified) {
		$this->timemodified = $timemodified;
	}

	public function setUseridadd($useridadd) {
		$this->useridadd = $useridadd;
	}

	public function setUseridedit($useridedit) {
		$this->useridedit = $useridedit;
	}

	public function getDeleted() {
		return $this->deleted;
	}

	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}

	/**
	 * @return string
	 */
	public function getShortname() {
		return $this->shortname;
	}

	/**
	 * @param string $shortname
	 */
	public function setShortname($shortname) {
		$this->shortname = $shortname;
	}

	/**
	 * @return string
	 */
	public function getDtype() {
		return $this->dtype;
	}

	/**
	 * @param string $dtype
	 */
	public function setDtype($dtype) {
		$this->dtype = $dtype;
	}

	function getParent() {
		return $this->parent;
	}

	function getLevel() {
		return $this->level;
	}

	function setParent($parent) {
		$this->parent = $parent;
	}

	function setLevel($level) {
		$this->level = $level;
	}

	function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function getMarker() {
        return $this->marker;
    }
    function setMarker($marker) {
        $this->marker = $marker;
    }

    function getStatusid() {
        return $this->statusid;
    }

    function setStatusid(AdminCmsResourceStructureStatus $statusid) {
        $this->statusid = $statusid;
    }

	function getRepositoryid(){
		return $this->repositoryid;
	}
	function setRepositoryid(AdminCmsRepository $repositoryid) {
		$this->repositoryid = $repositoryid;
	}
	
	function getDefaulturl() {
        return $this->defaulturl;
    }

    function setDefaulturl($defaulturl) {
        $this->defaulturl = $defaulturl;
    }
}
