<?php

namespace Badiu\Admin\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminCmsResourceContent
 *
 * @ORM\Table(name="admin_cms_resource_content", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_cms_resource_content_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="admin_cms_resource_content_name_uix", columns={"entity", "name"})},
 *       indexes={@ORM\Index(name="admin_cms_resource_content_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_cms_resource_content_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_cms_resource_content_idnumber_ix", columns={"idnumber"}),
 *              @ORM\Index(name="admin_cms_resource_content_description_ix", columns={"description"}),
 *              @ORM\Index(name="admin_cms_resource_content_name_ix", columns={"name"}),
 *              @ORM\Index(name="admin_cms_resource_content_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="admin_cms_resource_content_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_cms_resource_content_useridadd_ix", columns={"useridadd"})})
 *          @ORM\Entity
 */
class AdminCmsResourceContent {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

/**
	 * @var AdminCmsResource
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsResource")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="resourceid", referencedColumnName="id", nullable=false)
	 * })
	 */
	private $resourceid;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string",length=255, nullable=true)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255,nullable=true)
	 */
	private $shortname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="text", nullable=true)
	 */
	private $content;
	
	      /**
	 * @var string 
	 *
	 * @ORM\Column(name="btype", type="string", length=50, nullable=true)
	 */
	private $btype;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="edition", type="integer",length=255, nullable=true)
	 */
	private $edition = 1;

	/**
         * this is the version key
         * 
	 * @var string 
	 *
	 * @ORM\Column(name="vkey", type="string",length=255, nullable=true)
	 */
	private $vkey; 

	/**
         * this is the key unique for each row it is used for access content 
	 * @var string
	 *
	 * @ORM\Column(name="skey", type="string",length=255, nullable=true)
	 */
	private $skey;

    	/**
         * this is the key unique for each row entity it is used for key in javascritpy and other
	 * @var string
	 *
	 * @ORM\Column(name="dkey", type="string",length=255, nullable=true)
	 */
	private $dkey;
        /**
	 * @var boolean
	 *
	 * @ORM\Column(name="vrelease", type="integer", nullable=true)
	 */
	private $vrelease;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="sortorder", type="float", precision=10, scale=0, nullable=false)
	 */
	private $sortorder;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=255,nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted;

	public function getId() {
		return $this->id;
	}

	public function getEntity() {
		return $this->entity;
	}

	public function getIdnumber() {
		return $this->idnumber;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getParam() {
		return $this->param;
	}

	public function getTimecreated() {
		return $this->timecreated;
	}

	public function getTimemodified() {
		return $this->timemodified;
	}

	public function getUseridadd() {
		return $this->useridadd;
	}

	public function getUseridedit() {
		return $this->useridedit;
	}

	public function getDeleted() {
		return $this->deleted;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setEntity($entity) {
		$this->entity = $entity;
	}

	public function setIdnumber($idnumber) {
		$this->idnumber = $idnumber;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setParam($param) {
		$this->param = $param;
	}

	public function setTimecreated(\DateTime $timecreated) {
		$this->timecreated = $timecreated;
	}

	public function setTimemodified(\DateTime $timemodified) {
		$this->timemodified = $timemodified;
	}

	public function setUseridadd($useridadd) {
		$this->useridadd = $useridadd;
	}

	public function setUseridedit($useridedit) {
		$this->useridedit = $useridedit;
	}

	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}

	public function getDtype() {
		return $this->dtype;
	}

	public function setDtype($dtype) {
		$this->dtype = $dtype;
	}

	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}

	public function getShortname() {
		return $this->name;
	}
	public function setShortname($shortname) {
		$this->shortname = $shortname;
	}

 function getContent() {
            return $this->content;
        }
 function setContent($content) {
            $this->content = $content;
        }
		
		function getResourceid() {
            return $this->resourceid;
        }
 function setResourceid($resourceid) {
            $this->resourceid = $resourceid;
        }
		
		 function getSortorder() {
            return $this->sortorder;
        }
		
		 function setSortorder($sortorder) {
            $this->sortorder = $sortorder;
        }
		
		 function setEdition($edition) {
            $this->edition = $edition;
        }

        function setVkey($vkey) {
            $this->vkey = $vkey;
        }

        function setSkey($skey) {
            $this->skey = $skey;
        }

        function setDkey($dkey) {
            $this->dkey = $dkey;
        }

        function getDkey() {
          return  $this->dkey;
        }

        function setVrelease($vrelease) {
            $this->vrelease = $vrelease;
        }
		
		 function getEdition() {
            return $this->edition;
        }

        function getVkey() {
            return $this->vkey;
        }

        function getSkey() {
            return $this->skey;
        }

        function getVrelease() {
            return $this->vrelease;
        }
		
		 function getBtype() {
            return $this->btype;
        }
		function setBtype($btype) {
            $this->btype = $btype;
        }

}
