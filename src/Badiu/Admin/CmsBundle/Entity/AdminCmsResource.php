<?php

namespace Badiu\Admin\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminCmsResource
 *
 * @ORM\Table(name="admin_cms_resource", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_cms_resource_skey_uix", columns={"skey"}),
 *       @ORM\UniqueConstraint(name="admin_cms_resource_shortname_uix", columns={"entity", "shortname","edition"}),
 *       @ORM\UniqueConstraint(name="admin_cms_resource_dkey_uix", columns={"entity", "dkey"}),
 *      @ORM\UniqueConstraint(name="admin_cms_resource_name_uix", columns={"entity", "name","repositoryid","structureid","edition"})},
 *       indexes={@ORM\Index(name="admin_cms_resource_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_cms_resource_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_cms_resource_idnumber_ix", columns={"idnumber"}),
 *              @ORM\Index(name="admin_cms_resource_statusid", columns={"statusid"}),
 *              @ORM\Index(name="admin_cms_resource_statusid", columns={"repositoryid"}),
 *              @ORM\Index(name="admin_cms_resource_name_ix", columns={"name"}),
 *              @ORM\Index(name="admin_cms_resource_edition_ix", columns={"edition"}),
 *              @ORM\Index(name="admin_cms_resource_vkey_ix", columns={"vkey"}),
 *              @ORM\Index(name="admin_cms_resource_skey_ix", columns={"skey"}),
 *              @ORM\Index(name="admin_cms_resource_dkey_ix", columns={"dkey"}),
 *              @ORM\Index(name="admin_cms_resource_vrelease_ix", columns={"vrelease"}), 
 *              @ORM\Index(name="admin_cms_resource_structureid_ix", columns={"structureid"}),
 *              @ORM\Index(name="admin_cms_resource_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="admin_cms_resource_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_cms_resource_marker_ix", columns={"marker"}), 
 *              @ORM\Index(name="admin_cms_resource_categoryid_ix", columns={"categoryid"}), 
 *              @ORM\Index(name="admin_cms_resource_typeid_ix", columns={"typeid"}), 
 *               @ORM\Index(name="admin_cms_resource_sortorder_ix", columns={"sortorder"}),
 *              @ORM\Index(name="admin_cms_resource_useridadd_ix", columns={"useridadd"})})
 *          @ORM\Entity
 */
class AdminCmsResource {

	/**
	 * @var integer 
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var AdminCmsResourceStatus
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsResourceStatus")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
	 * })
	 */
	private $statusid;


	/**
	 * @var AdminCmsRepository
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsRepository")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="repositoryid", referencedColumnName="id")
	 * })
	 */
	private $repositoryid;

	/**
	 * @var AdminCmsResourceStructure
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsResourceStructure")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="structureid", referencedColumnName="id")
	 * })
	 */
	private $structureid;
	
	/**
	 * @var AdminCmsResourceCategory
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsResourceCategory")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
	 * })
	 */
	private $categoryid;
	
	/**
	 * @var AdminCmsResourceType
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsResourceType")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="typeid", referencedColumnName="id")
	 * })
	 */
	private $typeid;
	/**
	 * @var AdminCmsTemplate
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsTemplate")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="templateid", referencedColumnName="id")
	 * })
	 */
	private $templateid;
	
		/**
	 * @var AdminCmsTemplate
	 *
	 * @ORM\ManyToOne(targetEntity="AdminCmsTemplate")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="listabstracttemplateid", referencedColumnName="id")
	 * })
	 */
	private $listabstracttemplateid;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

	

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype = 'content'; // content | quiz

        /**
	 * @var string 
	 *
	 * @ORM\Column(name="btype", type="string", length=50, nullable=true)
	 */
	private $btype = 'page'; //  for content: page |videolist| | image | url | file | video  =>     for quiz: boolean | choice | 
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string",length=255, nullable=false)
	 */
	private $name; 

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255,nullable=true)
	 */
	private $shortname;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="edition", type="integer",length=255, nullable=false)
	 */
	private $edition = 1;

	/**
         * this is the version key
         * 
	 * @var string 
	 *
	 * @ORM\Column(name="vkey", type="string",length=255, nullable=false)
	 */
	private $vkey; 

	/**
         * this is the key unique for each row it is used for access content 
	 * @var string
	 *
	 * @ORM\Column(name="skey", type="string",length=255, nullable=false)
	 */
	private $skey;

    	/**
         * this is the key unique for each row entity it is used for key in javascritpy and other
	 * @var string
	 *
	 * @ORM\Column(name="dkey", type="string",length=255, nullable=false)
	 */
	private $dkey;
        /**
	 * @var boolean
	 *
	 * @ORM\Column(name="vrelease", type="integer", nullable=false)
	 */
	private $vrelease;
	
	
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="abstract", type="string",length=255, nullable=true)
	 */
	private $abstract;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="text", nullable=true)
	 */
	private $content;

		 /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey; 

  /**
     * @var integer
     *
     * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;

/**
     * @var string
     *
     * @ORM\Column(name="defaultimage", type="string", length=255, nullable=true)
     */
    private $defaultimage;
	
	/**
     * @var string
     *
     * @ORM\Column(name="defaulturl", type="string", length=255, nullable=true)
     */
    private $defaulturl;
  
  
  	/**
	 * @var integer
	 *
	 * @ORM\Column(name="adminformattemptid", type="bigint", nullable=true)
	 */
	private $adminformattemptid;
	
/**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
	
	/**
     * @var string
     *
     * @ORM\Column(name="summary", type="text", nullable=true)
     */
    private $summary;
	
		/**
     * @var string
     *
     * @ORM\Column(name="competence", type="text", nullable=true)
     */
    private $competence;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProject
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProject")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="projectid", referencedColumnName="id")
	 * })
	 */
	private $projectid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProjectTypePartnership
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProjectTypePartnership")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="typepartnershipid", referencedColumnName="id")
	 * })
	 */
	private $typepartnershipid;
	
	/**
	 * @var \Badiu\Admin\ProjectBundle\Entity\AdminProjectTypeDev
	 *
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\ProjectBundle\Entity\AdminProjectTypeDev")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="typedevid", referencedColumnName="id")
	 * })
	 */
	private $typedevid;
	
	/**
	 *
	 * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="authordevid", referencedColumnName="id")
	 * })
	 */
	private $authordevid;
	
	
	/**
     * @var integer
     *
     * @ORM\Column(name="costdev", type="float", precision=10, scale=0, nullable=true)
     */
    private $costdev;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="devinfo", type="text", nullable=true)
     */
    private $devinfo;
	/**
	 *
	 * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise
	 * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterprise")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="enterpriseid", referencedColumnName="id")
	 * })
	 */
	private $enterpriseid;
	
	 /**
     * @var \Badiu\Admin\EnterpriseBundle\Entity\AdminEnterpriseDepartment
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Admin\EnterpriseBundle\Entity\AdminEnterpriseDepartment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="departmentid", referencedColumnName="id")
     * })
     */

    private $departmentid; 
	
	/**
     * @var string
     *
     * @ORM\Column(name="usertarget", type="text", nullable=true)
     */
    private $usertarget;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="usertargetestimatedamout", type="bigint", nullable=true)
     */
    private $usertargetestimatedamout;
	
	/**
     * @var string
     *
     * @ORM\Column(name="objective", type="text", nullable=true)
     */
    private $objective;
	
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timeupdate", type="datetime", nullable=true)
	 */
	private $timeupdate;
	 
/**
	 * @var string
	 *
	 * @ORM\Column(name="customchar1", type="string", length=255, nullable=true)
	 */
	private $customchar1;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customchar2", type="string", length=255, nullable=true)
	 */
	private $customchar2;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customchar3", type="string", length=255, nullable=true)
	 */
	private $customchar3;
	
	
	   /**
     * @var integer
     *
     * @ORM\Column(name="customint1", type="bigint",  nullable=true)
     */
    private $customint1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint2", type="bigint",  nullable=true)
     */
    private $customint2;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="customint3", type="bigint",  nullable=true)
     */
    private $customint3;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

        /**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;

		 	      /**
     * @var string
     *
     * @ORM\Column(name="marker", type="string", length=255, nullable=true)
     */
		private $marker;
	
	      /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="float", precision=10, scale=0, nullable=true)
     */
    private $sortorder;
	
	/**
     * @var string
     *
     * @ORM\Column(name="customcss", type="text", nullable=true)
     */
    private $customcss;
	
	/**
     * @var string
     *
     * @ORM\Column(name="customjs", type="text", nullable=true)
     */
    private $customjs;
	
        /**
	 * @var string
	 *
	 * @ORM\Column(name="dconfig", type="text", nullable=true)
	 */
	private $dconfig;
        
	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted;

        function getId() {
            return $this->id;
        }

        function getStatusid() {
            return $this->statusid;
        }

        function getStructureid(){
            return $this->structureid;
        }

        function getRepositoryid(){
            return $this->repositoryid;
        }

        function getEntity() {
            return $this->entity;
        }

        function getDtype() {
            return $this->dtype;
        }

        function getBtype() {
            return $this->btype;
        }

        function getName() {
            return $this->name;
        }

        function getShortname() {
            return $this->shortname;
        }

        function getEdition() {
            return $this->edition;
        }

        function getVkey() {
            return $this->vkey;
        }

        function getSkey() {
            return $this->skey;
        }

        function getVrelease() {
            return $this->vrelease;
        }

        function getContent() {
            return $this->content;
        }

        function getModulekey() {
            return $this->modulekey;
        }

        function getModuleinstance() {
            return $this->moduleinstance;
        }

        function getDescription() {
            return $this->description;
        }

        function getIdnumber() {
            return $this->idnumber;
        }

        function getMarker() {
            return $this->marker;
        }

        function getDconfig() {
            return $this->dconfig;
        }

        function getParam() {
            return $this->param;
        }

        function getTimecreated() {
            return $this->timecreated;
        }

        function getTimemodified() {
            return $this->timemodified;
        }

        function getUseridadd() {
            return $this->useridadd;
        }

        function getUseridedit() {
            return $this->useridedit;
        }

        function getDeleted() {
            return $this->deleted;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setStatusid(AdminCmsResourceStatus $statusid) {
            $this->statusid = $statusid;
        }

        function setStructureid(AdminCmsResourceStructure $structureid) {
            $this->structureid = $structureid;
        }
        function setRepositoryid(AdminCmsRepository $repositoryid) {
            $this->repositoryid = $repositoryid;
        }
        function setEntity($entity) {
            $this->entity = $entity;
        }

        function setDtype($dtype) {
            $this->dtype = $dtype;
        }

        function setBtype($btype) {
            $this->btype = $btype;
        }

        function setName($name) {
            $this->name = $name;
        }

        function setShortname($shortname) {
            $this->shortname = $shortname;
        }

        function setEdition($edition) {
            $this->edition = $edition;
        }

        function setVkey($vkey) {
            $this->vkey = $vkey;
        }

        function setSkey($skey) {
            $this->skey = $skey;
        }

        function setDkey($dkey) {
            $this->dkey = $dkey;
        }

        function getDkey() {
          return  $this->dkey;
        }

        function setVrelease($vrelease) {
            $this->vrelease = $vrelease;
        }

        function setContent($content) {
            $this->content = $content;
        }

        function setModulekey($modulekey) {
            $this->modulekey = $modulekey;
        }

        function setModuleinstance($moduleinstance) {
            $this->moduleinstance = $moduleinstance;
        }

        function setDescription($description) {
            $this->description = $description;
        }

        function setIdnumber($idnumber) {
            $this->idnumber = $idnumber;
        }

        function setMarker($marker) {
            $this->marker = $marker;
        }

        function setDconfig($dconfig) {
            $this->dconfig = $dconfig;
        }

        function setParam($param) {
            $this->param = $param;
        }

        function setTimecreated(\DateTime $timecreated) {
            $this->timecreated = $timecreated;
        }

        function setTimemodified(\DateTime $timemodified) {
            $this->timemodified = $timemodified;
        }

        function setUseridadd($useridadd) {
            $this->useridadd = $useridadd;
        }

        function setUseridedit($useridedit) {
            $this->useridedit = $useridedit;
        }

        function setDeleted($deleted) {
            $this->deleted = $deleted;
        }

		function getCustomchar1() {
            return $this->customchar1;
        }

        function getCustomchar2() {
            return $this->customchar2;
        }

        function getCustomchar3() {
            return $this->customchar3;
        }

 function setCustomchar1($customchar1) {
            $this->customchar1 = $customchar1;
        }

        function setCustomchar2($customchar2) {
            $this->customchar2 = $customchar2;
        }

        function setCustomchar3($customchar3) {
            $this->customchar3 = $customchar3;
        }
		
		  function getDefaultimage() {
        return $this->defaultimage;
    }

    function setDefaultimage($defaultimage) {
        $this->defaultimage = $defaultimage;
    }
	
	function getAbstract() {
        return $this->abstract;
    }

    function setAbstract($abstract) {
        $this->abstract = $abstract;
    }
	
	 public function getSortorder() {
        return $this->sortorder;
    }

    public function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }
	
	/**
     * @return string
     */
    public function getCustomcss() {
        return $this->customcss;
    }

    /**
     * @param string $customcss
     */
    public function setCustomcss($customcss) {
        $this->customcss = $customcss;
    }
	
	/**
     * @return string
     */
    public function getCustomjs() {
        return $this->customjs;
    }

    /**
     * @param string $customjs
     */
    public function setCustomjs($customjs) {
        $this->customjs = $customjs;
    }
	
	/**
     * @return string
     */
    public function getTemplateid() {
        return $this->templateid;
    }

    /**
     * @param string $templateid
     */
    public function setTemplateid($templateid) {
        $this->templateid = $templateid;
    }
	
	
/**
     * @return string
     */
    public function getListabstracttemplateid() {
        return $this->listabstracttemplateid;
    }

    /**
     * @param string $listabstracttemplateid;
     */
    public function setListabstracttemplateid($listabstracttemplateid) {
        $this->listabstracttemplateid = $listabstracttemplateid;
    }
	
		  function getDefaulturl() {
        return $this->defaulturl;
    }

    function setDefaulturl($defaulturl) {
        $this->defaulturl = $defaulturl;
    }
	
	
    function getCustomint1() {
        return $this->customint1;
    }

    function getCustomint2() {
        return $this->customint2;
    }

    function getCustomint3() {
        return $this->customint3;
    }
	
	
    function setCustomint1($customint1) {
        $this->customint1 = $customint1;
    }

    function setCustomint2($customint2) {
        $this->customint2 = $customint2;
    }

    function setCustomint3($customint3) {
        $this->customint3 = $customint3;
    }


 function getCategoryid() {
        return $this->categoryid;
    }

    function setCategoryid($categoryid) {
        $this->categoryid = $categoryid;
    }
	
	
 function getTypeid() {
        return $this->typeid;
    }

    function setTypeid($typeid) {
        $this->typeid = $typeid;
    }


		public function getProjectid() {
		return $this->projectid;
	}
	
	public function setProjectid($projectid) {
		$this->projectid = $projectid;
	}
	
	function getEnterpriseid() {
            return $this->enterpriseid;
        }
	function setEnterpriseid($enterpriseid) {
            $this->enterpriseid = $enterpriseid;
        }

	public function setDepartmentid($departmentid)
    {
        $this->departmentid = $departmentid;
	}
	
	public function getDepartmentid()
    {
        return $this->departmentid;
    }

public function setUsertarget($usertarget)
    {
        $this->usertarget = $usertarget;
	}
	
	public function getUsertarget()
    {
        return $this->usertarget;
    }

public function setAdminformattemptid($adminformattemptid)
    {
        $this->adminformattemptid = $adminformattemptid;
	}
	
	public function getAdminformattemptid()
    {
        return $this->adminformattemptid;
    }	
public function setTypepartnershipid($typepartnershipid)
    {
        $this->typepartnershipid = $typepartnershipid;
	}
	
	public function getTypepartnershipid()
    {
        return $this->typepartnershipid;
    }	

public function setTypedevid($typedevid)
    {
        $this->typedevid = $typedevid;
	}
	
	public function getTypedevid()
    {
        return $this->typedevid;
    }	

public function setObjective($objective)
    {
        $this->objective = $objective;
	}
	
	public function getObjective()
    {
        return $this->objective;
    }	



	public function setTimeupdate($timeupdate)
    {
        $this->timeupdate = $timeupdate;
	}
	
	public function getTimeupdate()
    {
        return $this->timeupdate;
    }


public function setAuthordevid($authordevid)
    {
        $this->authordevid = $authordevid;
	}
	
	public function getAuthordevid()
    {
        return $this->authordevid;
    }		
	
	
public function setCostdev($costdev)
    {
        $this->costdev = $costdev;
	}
	
	public function getCostdev()
    {
        return $this->costdev;
    }	


public function setDevinfo($devinfo)
    {
        $this->devinfo = $devinfo;
	}
	
	public function getDevinfo()
    {
        return $this->devinfo;
    }


function getCompetence() {
            return $this->competence;
        }
	function setCompetence($competence) {
            $this->competence = $competence;
        }	
		
public function setUsertargetestimatedamout($usertargetestimatedamout)
    {
        $this->usertargetestimatedamout = $usertargetestimatedamout;
	}
	
	public function getUsertargetestimatedamout()
    {
        return $this->usertargetestimatedamout;
    }
	public function getTimestart() {
	  return $this->timestart;
    }

    public function getTimeend() {
        return $this->timeend;
    }
	
	  public function setTimestart($timestart) {
        $this->timestart = $timestart;
    }

    public function setTimeend($timeend) {
        $this->timeend = $timeend;
    }
	
	
	public function getSummary() {
        return $this->summary;
    }

    public function setSummary($summary) {
        $this->summary = $summary;
    }
}
