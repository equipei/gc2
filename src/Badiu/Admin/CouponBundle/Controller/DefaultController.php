<?php

namespace Badiu\Admin\CouponBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuAdminCouponBundle:Default:index.html.twig', array('name' => $name));
    }
}
