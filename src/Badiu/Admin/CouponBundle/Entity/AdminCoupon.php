<?php

namespace Badiu\Admin\CouponBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *AdminCoupon
 *  
 * @ORM\Table(name="admin_coupon", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_coupon_skey_uix", columns={"entity", "code"}),
 *      @ORM\UniqueConstraint(name="admin_coupon_idnumber_uix", columns={"entity", "idnumber"})},
  *      indexes={@ORM\Index(name="admin_coupon_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_coupon_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_coupon_code_ix", columns={"code"}),
 *              @ORM\Index(name="admin_coupon_statusinfo_ix", columns={"statusinfo"}),
 *              @ORM\Index(name="admin_coupon_statusid_ix", columns={"statusid"}), 
 *              @ORM\Index(name="admin_coupon_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_coupon_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_coupon_timestart_ix", columns={"timestart"}), 
 *              @ORM\Index(name="admin_coupon_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="admin_coupon_timeend_ix", columns={"timeend"}),
 *              @ORM\Index(name="admin_coupon_timeuse_ix", columns={"timeuse"}), 
 *              @ORM\Index(name="admin_coupon_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminCoupon
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    
	 /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false)
     */
    private $code;

  /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype;
	 /**
     * @var string
     *
     * @ORM\Column(name="statusinfo", type="string", length=50, nullable=true)
     */
    private $statusinfo;  
	
 /**
     * @var AdminCouponStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminCouponStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;
    
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="timeuse", type="bigint", nullable=false)
	 */
	private $timeuse;
		/**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;

		/**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;	
	    
	 /**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;

    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
	/**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;
	


     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
   
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }



    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

  
    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getDconfig() {
        return $this->dconfig;
    }

   

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

  /**
     * @return string
     */
    public function getDtype() {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype) {
        $this->dtype = $dtype;
    }

	
	 function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
	 /**
     * @return string
     */
    public function getStatusinfo() {
        return $this->statusinfo;
    }

    /**
     * @param string $dstatus
     */
    public function setStatusinfo($dstatusinfo) {
        $this->statusinfo = $statusinfo;
    }
	
function getStatusid() {
        return $this->statusid;
    }
	
	   function setStatusid(AdminCouponStatus $statusid) {
        $this->statusid = $statusid;
    }

public function getTimeuse() {
        return $this->timeuse;
    }

    // Setter para $timeuse
    public function setTimeuse($timeuse) {
        $this->timeuse = $timeuse;
    }

}
