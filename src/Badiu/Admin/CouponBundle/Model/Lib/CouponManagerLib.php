<?php

namespace Badiu\Admin\CouponBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class CouponManagerLib extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function generate($param) {
		$amout=$this->getUtildata()->getVaueOfArray($param,"amout");
		$modulekey=$this->getUtildata()->getVaueOfArray($param,"modulekey");
		$moduleinstance=$this->getUtildata()->getVaueOfArray($param,"moduleinstance");
		$amoutchar=$this->getUtildata()->getVaueOfArray($param,"amoutchar");
		$chartype=$this->getUtildata()->getVaueOfArray($param,"chartype");
		$entity=$this->getEntity();
		
		$dtype='general';
		$statusid=$this->getContainer()->get('badiu.admin.coupon.status.data')->getGlobalColumnValue('id',array('entity'=>$entity,'shortname'=>'active'));
		$timestart=$this->getUtildata()->getVaueOfArray($param,"timestart");
		$timeend=$this->getUtildata()->getVaueOfArray($param,"timeend");
		$description=$this->getUtildata()->getVaueOfArray($param,"description");
		$pparam=$this->getUtildata()->getVaueOfArray($param,"param");
		$dconfig=$this->getUtildata()->getVaueOfArray($param,"dconfig");
		$codestartwith=$this->getUtildata()->getVaueOfArray($param,"codestartwith");
		$codeendwith=$this->getUtildata()->getVaueOfArray($param,"codeendwith");
		$timeuse=$this->getUtildata()->getVaueOfArray($param,"timeuse");
		if(empty($timeuse)){$timeuse=0;}
		$type=1;
		if($chartype=='onlytext'){$type=2;}
		else if($chartype=='onlynumber'){$type=1;}
		else if($chartype=='textandnumber'){$type=3;}
		
		$coupondata=$this->getContainer()->get('badiu.admin.coupon.coupon.data');
		
		$cont=0;
		$exec=true;
		
		if(!empty($codestartwith)){$amoutchar=$amoutchar- strlen($codestartwith);}
		if(!empty($codeendwith)){$amoutchar=$amoutchar- strlen($codeendwith);}
		if($amoutchar <=0){return 0;}
		while ($exec) { 
			$code=$this->generateString($amoutchar, $type);
			if(!empty($codestartwith)){$code=$codestartwith.$code;}
			if(!empty($codeendwith)){$code=$code.$codeendwith;}
			$eparam=array('entity'=>$entity,'code'=>$code);
			$exist=$coupondata->countGlobalRow($eparam);
			if(!$exist){
				$fparam=array('entity'=>$entity,'code'=>$code,'timeuse'=>$timeuse,'dtype'=>$dtype,'statusid'=>$statusid,'timestart'=>$timestart,'timeend'=>$timeend,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'description'=>$description,'param'=>$pparam,'dconfig'=>$dconfig,'deleted'=>0,'timecreated'=>new \DateTime());
				$result=$coupondata->insertNativeSql($fparam,false); 
				if($result){
					$cont++;
					if($cont==$amout){$exec=false;}
				}
			}
			
			
			
		}
		return $cont; 
	}
  
/**
 * Generates a random string.
 *
 * This function generates a random string of a specified length and character type. The character type can be numeric (1), alphabetic (2), or alphanumeric (3).
 *
 * @param int $length The length of the string to be generated.
 * @param int $type The type of characters to include in the string (1 = numeric, 2 = alphabetic, 3 = alphanumeric).
 *
 * @return string The generated string.
 */
function generateString($length, $type) {
    $generatedString = '';
    $alphabetic = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $numeric = '0123456789';

    switch($type) {
        case 1:
            $characters = $numeric;
            break;
        case 2:
            $characters = $alphabetic;
            break;
        case 3:
            $characters = $alphabetic . $numeric;
            break;
        default:
            return null;
    }

    for ($i = 0; $i < $length; $i++) {
        $generatedString .= $characters[rand(0, strlen($characters) - 1)];
    }

    return $generatedString;
}
  
}
