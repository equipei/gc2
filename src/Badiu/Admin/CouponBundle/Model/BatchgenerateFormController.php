<?php

namespace Badiu\Admin\CouponBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class BatchgenerateFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
    private $totalexec=0;
    public function saveExec() {
		
		$couponmanagerlib=$this->getContainer()->get('badiu.admin.coupon.core.managerlib');
		$result=$couponmanagerlib->generate($this->getParam());
		$this->totalexec=$result;
		return $result;
	}
	
	

public function execResponse() {
		if($this->totalexec < 2){$this->setSuccessmessage($this->getTranslator()->trans('badiu.admin.coupon.batchgenerate.message.coupongenarated',array('%amount%'=>$this->totalexec)));}
		else {$this->setSuccessmessage($this->getTranslator()->trans('badiu.admin.coupon.batchgenerate.message.couponsgenarated',array('%amount%'=>$this->totalexec)));}
		
		$outrsult=array('result'=>$this->getResultexec(),'message'=>$this->getSuccessmessage(),'urlgoback'=>$this->getUrlgoback());
		$this->getResponse()->setStatus("accept");
		$this->getResponse()->setMessage($outrsult);
		return $this->getResponse()->get();
	}
}
