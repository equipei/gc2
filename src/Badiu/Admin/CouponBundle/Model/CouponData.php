<?php

namespace Badiu\Admin\CouponBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class CouponData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
 
	public function getInfo($entity,$code) {
	   $sql="SELECT  o.id,o.timeuse,o.timestart,o.timeend,s.shortname AS statusshortname,s.name AS statusname, o.modulekey, o.moduleinstance  FROM ".$this->getBundleEntity()." o JOIN o.statusid s  WHERE o.entity = :entity AND o.code = :code";
       $query = $this->getEm()->createQuery($sql);
       $query->setParameter('entity',$entity);
	   $query->setParameter('code',$code);
	   $result= $query->getOneOrNullResult();
       return $result;
   }
}
