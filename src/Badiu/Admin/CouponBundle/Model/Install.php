<?php
 
namespace Badiu\Admin\CouponBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }
 

    public function exec() {
       $result=$this->initDbStatus() ;
     
  		return $result;
	} 
	
	 public function initDbStatus() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.coupon.status.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist && !$this->getForceupdate()){return 0;}
        
		$entity=$this->getEntity();
         $list=array(
			array('entity'=>$entity,'shortname'=>'active','name'=>$this->getTranslator()->trans('badiu.admin.coupon.status.active')),
      array('entity'=>$entity,'shortname'=>'inuse','name'=>$this->getTranslator()->trans('badiu.admin.coupon.status.inuse')),
	  array('entity'=>$entity,'shortname'=>'limitreached','name'=>$this->getTranslator()->trans('badiu.admin.coupon.status.limitreached')),
	  array('entity'=>$entity,'shortname'=>'used','name'=>$this->getTranslator()->trans('badiu.admin.coupon.status.used')),
      array('entity'=>$entity,'shortname'=>'canceled','name'=>$this->getTranslator()->trans('badiu.admin.coupon.status.canceled')),
	  array('entity'=>$entity,'shortname'=>'expired','name'=>$this->getTranslator()->trans('badiu.admin.coupon.status.expired')),
      				
		 ); 
		 
         
         
         foreach ($list as $param) {
			 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 
			 $paramedit=null;
			 if($this->getForceupdate()){
				 $paramedit= $param;
			 }
			 $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
			 $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
			 $r=$this->getUtildata()->getVaueOfArray($result,'id');
              if($r){$cont++;}
            }
	return $cont;
    }

 
}
