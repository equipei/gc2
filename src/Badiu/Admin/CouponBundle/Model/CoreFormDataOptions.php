<?php
namespace Badiu\Admin\CouponBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class CoreFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
	   
	
	public  function getChartype(){
        
        $list=array();
        $list['onlytext']=$this->getChartypeLabel('onlytext');
		$list['onlynumber']=$this->getChartypeLabel('onlynumber');
		$list['textandnumber']=$this->getChartypeLabel('textandnumber');
		return  $list;
    }
	
	 public  function getChartypeLabel($value){
        $label=$this->getTranslator()->trans('badiu.admin.coupon.chartype.'.$value);
		return  $label;
    }
	
}
