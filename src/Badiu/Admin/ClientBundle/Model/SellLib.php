<?php

namespace  Badiu\Admin\ClientBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class SellLib extends BadiuSqlFilter{

    function __construct(Container $container, $baseKey = null) {
        parent::__construct($container, $baseKey);
    }

    public function getStatusFilter() {
       
        $status= $this->getUtildata()->getVaueOfArray($this->getParam(),'status');
        $wsql="";
        if($status=='completed'){$wsql=" AND s.shortname='completed' ";}
        else if($status=='canceled'){$wsql=" AND s.shortname='canceled' ";}
        else if($status=='pending'){$wsql=" AND s.shortname='pending'  AND  o.timeexec <= :temeexec ";}
        else if($status=='provisioned'){$wsql=" AND s.shortname='pending' AND  o.timeexec > :temeexec ";}
     
       return $wsql;
    }


}
