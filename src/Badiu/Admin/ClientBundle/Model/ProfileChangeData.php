<?php

namespace  Badiu\Admin\ClientBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportChangeData;
class ProfileChangeData extends BadiuReportChangeData
{

    function __construct(Container $container) {
            parent::__construct($container);
              }
              
  

public function all($data){
		if(!$this->getContainer()->has('badiu.admin.form.lib.factoryselectdata')){ return $data;}
		$adminformattemptid=$this->getUtildata()->getVaueOfArray($data, 'badiu_list_data_row.data.1.adminformattemptid',true);
		 $fparam=array('attemptid'=>$adminformattemptid);
		 $factoryselectdata=$this->getContainer()->get('badiu.admin.form.lib.factoryselectdata');
		 $fresult=$factoryselectdata->getByAttempt($fparam); 
		 $data['badiu_list_data_row']['data'][1]['adminformattemptdata']=$fresult;
  
 
      return $data;
      
  }
 
  
    
}
