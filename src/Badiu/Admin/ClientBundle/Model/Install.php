<?php

namespace  Badiu\Admin\ClientBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Install extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
        $result=$this->initPermissions();
           $result+= $this->initStudentPermission();
		    $result+= $this->initTeachearPermission();
			$result+= $this->initCoordenatorPermission();
           return $result;  
    } 

    public function initPermissions() {
        $cont=0;
        $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');

         //guest  
        $param=array(
		'entity'=>$this->getEntity(),
          'roleshortname'=>'guest',
          'permissions'=>array('badiu.admin.client.newdefaultprofile.add')
        );
        $cont+=$libpermission->add($param);
		
		     //authenticateduser
        $param=array(
		'entity'=>$this->getEntity(),
          'roleshortname'=>'authenticateduser',
          'permissions'=>array('badiu.admin.client.client.frontpage','badiu.admin.client.defaultprofile.dashboard','badiu.admin.client.changepassword.add','badiu.admin.client.changedefaultprofile.edit')
        ); 
        $cont+=$libpermission->add($param);
		return $cont;
    }
    
     public function initStudentPermission() {
         $cont=0;
         $entity=$this->getEntity();
         $list=array('badiu.ams.my.student.frontpage');
                 
         $datarole = $this->getContainer()->get('badiu.system.access.role.data');
         $dataperm = $this->getContainer()->get('badiu.system.access.permission.data');
         $roleid=  $datarole->getIdByShortname($entity,'student');
         if(empty($roleid)){return 0;}
         foreach ($list as  $value) {
              $param=array();
              $param['entity']=$entity;
              $param['roleid']=$roleid;
              $param['pemissionkey']=$value;
              $param['timecreated']=new \DateTime();
              $param['deleted']=0;

              if(!$dataperm->countNativeSql(array('entity'=>$entity,'roleid'=>$roleid,'pemissionkey'=>$value),false)){
                  $result = $dataperm->insertNativeSql($param,false); 
                  if($result){$cont++;}
              }
         }

         return $cont;
     }
  public function initTeachearPermission() {
         $cont=0;
         $entity=$this->getEntity();
         $list=array('badiu.ams.my.teachear.frontpage',
			'badiu.ams.my.teachearclasseenrol.index'
		 );
                 
         $datarole = $this->getContainer()->get('badiu.system.access.role.data');
         $dataperm = $this->getContainer()->get('badiu.system.access.permission.data');
         $roleid=  $datarole->getIdByShortname($entity,'teachear');
         if(empty($roleid)){return 0;}
         foreach ($list as  $value) {
              $param=array();
              $param['entity']=$entity;
              $param['roleid']=$roleid;
              $param['pemissionkey']=$value;
              $param['timecreated']=new \DateTime();
              $param['deleted']=0;

              if(!$dataperm->countNativeSql(array('entity'=>$entity,'roleid'=>$roleid,'pemissionkey'=>$value),false)){
                  $result = $dataperm->insertNativeSql($param,false); 
                  if($result){$cont++;}
              }
         }

         return $cont;
     }

     public function initCoordenatorPermission() {
         $cont=0;
         $entity=$this->getEntity();
         $list=array('badiu.ams.my.coordenator.frontpage',
			'badiu.ams.my.coordenatoroffer.index');
                 
         $datarole = $this->getContainer()->get('badiu.system.access.role.data');
         $dataperm = $this->getContainer()->get('badiu.system.access.permission.data');
         $roleid=  $datarole->getIdByShortname($entity,'coordenator');
         if(empty($roleid)){return 0;}
         foreach ($list as  $value) {
              $param=array();
              $param['entity']=$entity;
              $param['roleid']=$roleid;
              $param['pemissionkey']=$value;
              $param['timecreated']=new \DateTime();
              $param['deleted']=0;

              if(!$dataperm->countNativeSql(array('entity'=>$entity,'roleid'=>$roleid,'pemissionkey'=>$value),false)){
                  $result = $dataperm->insertNativeSql($param,false); 
                  if($result){$cont++;}
              }
         }

         return $cont;
     } 
}
