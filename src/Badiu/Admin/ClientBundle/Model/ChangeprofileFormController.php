<?php

namespace  Badiu\Admin\ClientBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\UserBundle\Model\UserFormController;

class ChangeprofileFormController extends UserFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
           $this->setSuccessmessage($this->getTranslator()->trans('badiu.admin.client.changepersonaladdress.success'));
           $this->setOperaction('edit');
         }
     
     public function changeParamOnOpen() {
        $data=$this->getContainer()->get($this->getKminherit()->data());
        $userid=$this->getContainer()->get('badiu.system.access.session')->get()->getUser()->getId();
        $param =$data->findById($userid);
      
        $objectutil=$this->getContainer()->get('badiu.system.core.lib.util.objectutil');
        $param= $objectutil->castEntityToArrayForForm($param);
        $this->setParam($param); 
         
        $this->setFormAddress();
        $this->setFormDconfig();
        $this->setFormDocumentdata();
        $this->setForm();
     }        
    public function changeParam() {
        $this->getFormAddress();
        $this->getFormDconfig();
        $this->getFormDocumentdata();
        $this->getForm();
        $userid=$this->getContainer()->get('badiu.system.access.session')->get()->getUser()->getId();
        $this->addParamItem('id',$userid);
       
    }
    
    public function validation() {
        
        $isvalid = $this->isCpfValid();
        if ($isvalid != null) {
            return $isvalid;
        }

        $isvalid = $this->isPassaportValid();
        if ($isvalid != null) {
            return $isvalid;
        }

        $isvalid = $this->isEmailValid();
        if ($isvalid != null) {
            return $isvalid;
        }
       
        $isemailduplicate = $this->isEmailDuplicate();
        if ($isemailduplicate != null) {
            return $isemailduplicate;
        }
       
        
        return null;
    }

    public function isCpfValid() {
        $nationalitystatus=$this->getParamItem('nationalitystatus');
        if($nationalitystatus!='native'){return null;}

        $docnumber=$this->getParamItem('cpf');
        $docnumber=trim($docnumber);
        if(empty($docnumber)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.form.error.fillallrequiredfield');
            $message['cpf']= $this->getTranslator()->trans('badiu.system.form.error.fiedrequired');
            $info = 'badiu.system.user.user.message.cpfexis';
            return $this->getResponse()->denied($info, $message);
       }

        //validade
        $cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
         $valid = $cpfdata->isValid($docnumber);
         if (!$valid) {
               
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['cpf']= $this->getTranslator()->trans('badiu.system.user.user.message.cpfnotvalid');
                $info = 'badiu.system.user.user.message.cpfnotvalid';
                return $this->getResponse()->denied($info, $message);
            }

         $duplicatecpf=null;
         $data = $this->getContainer()->get($this->getKminherit()->data());
         $entity =$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
         $id=$this->getContainer()->get('badiu.system.access.session')->get()->getUser()->getId();
         $number = $cpfdata->clean($docnumber);

        $duplicatecpf = $data->existColumnEdit($id,$entity, 'docnumber', $number);        
          
           
          if ($duplicatecpf) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['cpf']= $this->getTranslator()->trans('badiu.system.user.user.message.cpfexist');
                $info = 'badiu.system.user.user.message.cpfexis';
                return $this->getResponse()->denied($info, $message);
      
            }

        return null;
    }

    public function isPassaportValid() {
        $nationalitystatus=$this->getParamItem('nationalitystatus');
        if($nationalitystatus!='foreign'){return null;}
        $passaport=$this->getParamItem('passaport');
        $passaport=trim($passaport);
        if(empty($passaport)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.form.error.fillallrequiredfield');
            $message['passaport']= $this->getTranslator()->trans('badiu.system.form.error.fiedrequired');
            $info = 'badiu.system.user.user.message.cpfexis';
            return $this->getResponse()->denied($info, $message);
       }
    }
     public function getAddress() {
          $badiuSession = $this->getContainer()->get('badiu.system.access.session');
          $userid=$badiuSession->get()->getUser()->getId();
          $entity = $badiuSession->get()->getEntity();
          $udata=$this->getContainer()->get('badiu.system.user.user.data');
          $contactdata=$udata->getColumnValue($entity,'contactdata',$paramf=array('id'=>$userid));
          $contactdata=$this->getJson()->decode($contactdata,true); 
        
          return $contactdata;
     }
   
     
     public function isEmailDuplicate() {
        $data = $this->getContainer()->get($this->getKminherit()->data());
        $email = $this->getUtildata()->getVaueOfArray($this->getParam(), 'email');
        $id=$this->getContainer()->get('badiu.system.access.session')->get()->getUser()->getId();
         $entity =$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
        if ($email) {
            $duplicateemail = $data->existColumnEdit($id,$entity, 'email', $email);
           
            if ($duplicateemail) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.duplication.message');
                $message['email']= $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                $info = 'badiu.system.user.user.message.duplicationemail';
                return $this->getResponse()->denied($info, $message);
            }
        }
        
    }

    function  getForm(){
      
        //document
        $nationalitystatus=$this->getParamItem('nationalitystatus');
        $cpf=$this->getParamItem('cpf');
        $passaport=$this->getParamItem('passaport');

        if($nationalitystatus=='native'){
            $this->addParamItem('doctype','CPF');
            $this->addParamItem('docnumber',$cpf);
        }else  if($nationalitystatus=='foreign'){
            $this->addParamItem('doctype','PASSAPORTE');
            $this->addParamItem('docnumber',$passaport);
        }

        $param=$this->getParam();
        unset($param['cpf']);
        unset($param['passaport']);
           
         $this->setParam($param);
       
    }

    function  setForm(){
       
        $doctype = $this->getParamItem('doctype');
        $docnumber=  $this->getParamItem('docnumber');
        
        if($doctype=='CPF'){
            $this->addParamItem('cpf',$docnumber);
        }else  if($doctype=='PASSAPORTE'){
             $this->addParamItem('passaport',$docnumber);
        }
        
}
}
