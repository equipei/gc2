<?php

namespace  Badiu\Admin\ClientBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\UserBundle\Model\UserDefaultFormController ;

class ChangedefaultprofileFormController  extends UserDefaultFormController 
{
	  private $userdb;
	  private $password;
    function __construct(Container $container) {
            parent::__construct($container);
           $this->setSuccessmessage($this->getTranslator()->trans('badiu.admin.client.changepersonaladdress.success'));
           //$this->setOperaction('edit');
		    $this->userdb=$this->getContainer()->get('badiu.system.user.user.data');
         }
      
     public function changeParamOnOpen() {
		  $badiuSession = $this->getContainer()->get('badiu.system.access.session');
		  if(!$badiuSession->exist()){return null;}
		  
       $data=$this->getContainer()->get($this->getKminherit()->data());
        $userid=$badiuSession->get()->getUser()->getId();
        $param =$data->findById($userid);
      
        $objectutil=$this->getContainer()->get('badiu.system.core.lib.util.objectutil');
        $param= $objectutil->castEntityToArrayForForm($param);
		
        $this->setParam($param);
		$this->setForm();
		$this->setFormDconfig();
     }        
    public function changeParam() {
		 $badiuSession = $this->getContainer()->get('badiu.system.access.session');
		 $this->getFormDconfig();
		
        if($badiuSession->exist()){
			$userid=$badiuSession->get()->getUser()->getId();
			$this->addParamItem('id',$userid);
		}
	else{
			/*$userutil=$this->getContainer()->get('badiu.system.user.user.util');
			$ownername=$this->getParamItem('name');
			$people=$userutil->splitFirtnameLastname($ownername);
		
			$this->addParamItem('firstname',$people->firtname);
			$this->addParamItem('lastname',$people->lastname);*/
			$this->removeParamItem('name');
		}
       $this->addParamItem('statusregister','complete');
	   
	   $intervaltimedayupdate=$this->getContainer()->get('badiu.system.access.session')->getValue('badiu.system.core.param.config.userupdateprofileintervalday');
		 if(empty($intervaltimedayupdate)){$intervaltimedayupdate=180;}
		 $now=new \DateTime();
		 $now->modify("+$intervaltimedayupdate day");
		 $this->addParamItem('registernextupadtetime',$now);
	   
	  
    }
    
	
    public function validation() {
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $userid=null;
		if($badiuSession->exist()){
			$userid=$badiuSession->get()->getUser()->getId();
			$this->addParamItem('id',$userid);
		}
		
		/*$mvalidate=$this->isSecurityPolicyAccept();
		if ($mvalidate != null) { return $mvalidate;}
		*/
		
       $mvalidate=$this->isValidCpf();
		if ($mvalidate != null) { return $mvalidate;}
	 
		$mvalidate=$this->isValidEmail();
		if ($mvalidate != null) { return $mvalidate;}
	
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        if($badiuSession->exist()){
			$mvalidate=$this->isValidNameSpleted();
			if ($mvalidate != null) { return $mvalidate;}
		}else{
			$mvalidate=$this->isValidName();
			if ($mvalidate != null) { return $mvalidate;}
		} 
		
		$mvalidate=$this->isValidPassword();
		if ($mvalidate != null) { return $mvalidate;}
	
		$mvalidate=$this->isDuplicateDocument();
		if ($mvalidate != null) { return $mvalidate;}
		
	
       /* $datebirthvalid = $this->isValidDateofbirth();
        if ($datebirthvalid != null) {
            return $datebirthvalid;
        }*/
       
       /* $tempvalid = $this->tempGeneralGlobalValidate() ;
	   if ($tempvalid != null) {
            return $tempvalid;
        } */
	$customvalid = $this->customValidate();
	   if ($customvalid != null) {
            return $customvalid;
        }
        return null;
    }

public function execResponse() {
	$badiuSession = $this->getContainer()->get('badiu.system.access.session');
    if($badiuSession->exist()){
		$result=parent::execResponse();
		return $result;
	}
	
	$utilapp=$this->getContainer()->get('badiu.system.core.lib.util.app');
	$url=$utilapp->getUrlByRoute('badiu.tms.my.studentfviewdefault.dashboard');
	$this->setUrlgoback($url);
			
	$userid=$this->getResultexec();
	if($userid){
		$username=$this->getParamItem('username');
        $password=$this->password;
       $usernamepasswordmatch=$this->getContainer()->get('badiu.auth.core.login.manual')->exec($username,$password);
        
			 
     }else{
         $message=array();
         $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.failed');
         $info='badiu.admin.selection.loginsingindoc.failed';
         return $this->getResponse()->denied($info, $message);
     } 
	 $result=parent::execResponse();
	 return $result;
	}
	
  function  setForm(){
       
        $doctype = $this->getParamItem('doctype');
        $docnumber=  $this->getParamItem('docnumber');
        
        if($doctype=='CPF'){
            $this->addParamItem('cpf',$docnumber);
        }
		//review
		/*else  if($doctype=='PASSAPORTE'){
             $this->addParamItem('passaport',$docnumber);
        }*/
   }
   
    public function isValidCpf() {
		 $id=$this->getParamItem('id');
		 $cpf=$this->getParamItem('cpf');
		 $cpf=trim($cpf);
		
		 $nationalitystatus=$this->getParamItem('nationalitystatus');
		 $cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
         $valid = $cpfdata->isValid($cpf);
		 
		 if($nationalitystatus!='native' && !$valid){
			 $this->removeParamItem('cpf'); 
			 $this->addParamItem('doctype','EMAIL');
			 $this->addParamItem('docnumber',$this->getParamItem('email'));
			 if(empty($id)){$this->addParamItem('username',$this->getParamItem('email'));}
			 if(empty($cpf))return null;
		 }
		
        if(empty($cpf)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.cpfrequired');
            $info='badiu.admin.selection.loginsingin.cpfrequired';
            return $this->getResponse()->denied($info, $message);
        }
		
		 if (!$valid) {
                $message=array();
                $message['generalerror']= $this->getTranslator()->trans('badiu.system.user.user.message.cpfnotvalid');
                $info = 'badiu.admin.selection.loginsingin.cpfnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
			
		$cpf=$cpfdata->clean($cpf);	
        $this->addParamItem('doctype','CPF');
		$this->addParamItem('docnumber',$cpf);
		if(empty($id)){$this->addParamItem('username',$cpf);}
		$this->removeParamItem('cpf');
		
		
		
        return null;
	 }
	 
	  public function isDuplicateDocument() {
		 //duplicate 
		  $entity=$this->getEntity();
		  $id=$this->getParamItem('id');
		  $doctype=$this->getParamItem('doctype');
		  $docnumber=$this->getParamItem('docnumber');
		  $data=$this->userdb;
		 
		  
		  
		 if ($doctype == 'CPF') {
           $duplicatecpf=null;
           if(empty($id)){$duplicatecpf = $data->existColumnAdd($entity, 'docnumber', $docnumber);}
			else{ $duplicatecpf = $data->existColumnEdit($id,$entity, 'docnumber', $docnumber);}
          
           
          if ($duplicatecpf) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.cpfexist');
                $info = 'badiu.admin.selection.loginsingin.document.cpf.justexist';
                return $this->getResponse()->denied($info, $message);
      
            }
		}else  if ($doctype == 'EMAIL') {
          
            $duplicateemail=null;
          if(empty($id)){$duplicateemail=$data->existColumnAdd($entity, 'docnumber', $docnumber);}
		  else{ $duplicateemail = $data->existColumnEdit($id,$entity, 'docnumber', $docnumber);}
           
          
            if ($duplicateemail) {
                $message=array();
                 $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $docnumber));
                 $info = 'badiu.admin.selection.loginsingin.document.email.justexist';
                return $this->getResponse()->denied($info, $message);
            }  
        }
     
        return null;
	 }
	 
	 
    public function isValidEmail() {
       
        $email=$this->getParamItem('email');
        $email=trim($email);
        
        if(empty($email)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingin.message.emailrequired');
            $info='badiu.admin.selection.loginsingin.emailrequired';
            return $this->getResponse()->denied($info, $message);
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.emailnotvalid');
            $info='badiu.admin.selection.loginsingindoc.emailnotvalid';
            return $this->getResponse()->denied($info, $message);
        }
		$entity=$this->getEntity();
		$id=$this->getParamItem('id');
		$duplicateemail = null;
		
		if(empty($id)){$duplicateemail=$this->userdb->existColumnAdd($entity, 'email', $email);}
		else {$duplicateemail = $this->userdb->existColumnEdit($id,$entity, 'email',$email);}
       
       
		if ($duplicateemail) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                //$message['email']= $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                $info = 'badiu.system.user.user.message.duplicationemail';
                return $this->getResponse()->denied($info, $message);
        }
			
     
        return null;
    }
	
	public function isValidPassword() {
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		if($badiuSession->exist()){return null;}
		
        $password=$this->getParamItem('password');
        $password=trim($password);
        if(empty($password)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingin.message.passwordrequired');
            $info='badiu.admin.selection.loginsingin.passwordrequired';
            return $this->getResponse()->denied($info, $message);
        }
         $this->password=$password;
		 $this->addParamItem('password',md5($password));
        return null;
    }
}
