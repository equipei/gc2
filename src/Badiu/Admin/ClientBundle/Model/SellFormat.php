<?php

namespace  Badiu\Admin\ClientBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class SellFormat extends BadiuFormat{
      private $router;
     function __construct(Container $container) {
            parent::__construct($container);
             $this->router=$this->getContainer()->get("router");
       } 

   
    public  function invoice($data){
       
           $value=null;
           $invoicecode=null;
           $invoiceurl=null;
           $statusinvoice=null;
           $portion=null;
           $status=null;
           $id=null;
           $invoiceduedate=null;
           if (array_key_exists("invoicecode",$data)){$invoicecode=$data['invoicecode'];}
           if (array_key_exists("invoiceurl",$data)){$invoiceurl=$data['invoiceurl'];}
           if (array_key_exists("invoicestatus",$data)){$statusinvoice=$data['invoicestatus'];}
           if (array_key_exists("portion",$data)){$portion=$data['portion'];}
           if (array_key_exists("invoiceduedate",$data)){$invoiceduedate=$data['invoiceduedate'];}
           
           if (array_key_exists("statusshortname",$data)){$status=$data['statusshortname'];}
            if (array_key_exists("id",$data)){$id=$data['id'];}
           
            
            if(!empty($invoicecode)){
                $value="Codigo: ".$invoicecode."<br />";
               
             }
             if(!empty($invoiceurl)){
                $link="Link: <a href=\"$invoiceurl\" target=\"_blank\">$invoiceurl</a> <br />";
                $value.=$link;
             }
        if(!empty($statusinvoice)){
                $invoicestatus="Status: $statusinvoice   <br />";
                $value.=$invoicestatus;
             }
             
              if(!empty($portion)){
                $portioninvoice="Num. de parcela: $portion  <br />";
                $value.=$portioninvoice;
             }
          
              if(!empty($invoiceduedate)){
                  $fduedate=$invoiceduedate->format('d/m/Y');
                $duedate="Data de venc.: $fduedate  <br />";
                $value.=$duedate;
             } 
             
     
           
           
           if(!empty($value)){
               $value = "<div style=\"font-size:11px\"> $value</div> ";
             }   
           
            
        return $value; 
    } 
 
}
