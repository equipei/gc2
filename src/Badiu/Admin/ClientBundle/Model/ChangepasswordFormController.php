<?php

namespace  Badiu\Admin\ClientBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ChangepasswordFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
            $this->setOperaction('edit');
            $this->setSuccessmessage($this->getTranslator()->trans('badiu.admin.client.changepassword.success'));
         }
     
            
    public function changeParam() {
       $newpassword=$this->getUtildata()->getVaueOfArray($this->getParam(), 'newpassword');
       $badiuSession = $this->getContainer()->get('badiu.system.access.session');
       $userid=$badiuSession->get()->getUser()->getId();
     
        $param=array();
        $param['id']=$userid;
        $param['password']=md5($newpassword);
        $this->setParam($param);
    }
    
}
