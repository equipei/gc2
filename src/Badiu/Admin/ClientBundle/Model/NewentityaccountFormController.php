<?php

namespace  Badiu\Admin\ClientBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class NewentityaccountFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
                    }
     
    
    public function save() {
         $check=$this->validation();
         if($check != null){return $check;}
         
         $entity=$this->addEntity();
		 $this->updateEntitySession($entity);
         $userid=$this->addUser($entity);
         $config=$this->installEntity($entity,$userid);
         $this->startSession($entity,$userid);
         $result=array('entity'=>$entity,'userid'=>$userid,'config'=>$config);
         $this->setSuccessmessage($this->getTranslator()->trans('badiu.admin.client.newentityaccount.createdsuccess')); 
         $urlgoback=$this->getContainer()->get('badiu.system.core.lib.util.app')->getBaseUrl();
         $outrsult=array('result'=>$result,'message'=>$this->getSuccessmessage(),'urlgoback'=>$urlgoback);
         $this->getResponse()->setStatus("accept"); 
         $this->getResponse()->setMessage($outrsult);
         return $this->getResponse()->get();
         
     } 
     
      public function validation() { 
           $name= $this->getParamItem('name');
            $haslastname=$this->getContainer()->get('badiu.system.user.user.util')->nameHasLastname($name);
            if(!$haslastname){
                 $message = array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.client.newentityaccount.requiredfirstnameandlastname');
                $info = 'badiu.admin.client.name.requiredfirstnameandlastname';
                return $this->getResponse()->denied($info, $message);
            }
            $email= $this->getParamItem('email');
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $message = array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.client.newentityaccount.emailnotvalid');
                $info = 'badiu.admin.client.email.notvalid';
                return $this->getResponse()->denied($info, $message);
            } 
            $data = $this->getContainer()->get('badiu.system.user.user.data');
            $duplicate=$data->countGlobalRow(array('username'=>$email));
           if ($duplicate) {

                $message = array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.client.newentityaccount.justexistkeyemail');
                $info = 'badiu.admin.client.duplication.username';
                return $this->getResponse()->denied($info, $message);
            }
            
            $password=$this->getParamItem('password');
            $confirmpassword=$this->getParamItem('confirmpassword');
           
           if(strlen($password)<4) {
               $message = array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.client.newentityaccount.passwordrequredlength');
                $info = 'badiu.admin.client.password.passwordrequredlength';
                return $this->getResponse()->denied($info, $message);
           } 
           if($password!=$confirmpassword) {
               $message = array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.client.newentityaccount.confirmpasswordnotmatch');
                $info = 'badiu.admin.client.password.confirmnotmatch';
                return $this->getResponse()->denied($info, $message);
           }
            return null;
      } 
      
       public function addEntity() { 
            $data= $this->getContainer()->get('badiu.system.entity.entity.data');
            $name= $this->getParamItem('name');
            $email= $this->getParamItem('email');
			$shortname=$email.'_'.time();
            $param=array('name'=>$name,'dtype'=>'generalppp','shortname'=>$shortname);
            $result=$data->insertNativeSql($param, false);
            return $result;
       }
	   
	    public function updateEntitySession($entity) { 
			$badiuSession =  $this->getContainer()->get('badiu.system.access.session');
			$dsession=$badiuSession->get();
			$dsession->setEntity($entity);
			$badiuSession->save($dsession);
			
		}
       public function addUser($entity) { 
            $data= $this->getContainer()->get('badiu.system.user.user.data');
            $name= $this->getParamItem('name');
            $email= $this->getParamItem('email');
            $people=$this->getContainer()->get('badiu.system.user.user.util')->splitFirtnameLastname($name);
            $firstname=$people->firtname;
            $lastname= $people->lastname;
            $password= md5($this->getParamItem('password'));
            $doctype="EMAIL";
            $param=array('entity'=>$entity,'firstname'=>$firstname,'lastname'=>$lastname,'username'=>$email,'email'=>$email,'auth'=>'manual','password'=>$password,'doctype'=>$doctype,'docnumber'=>$email,'deleted'=>0);
            $result=$data->insertNativeSql($param, false);
            return $result;
        }
        public function installEntity($entity,$userid) { 
            $result=0;
            $dinstall= $this->getContainer()->get('badiu.system.module.install');
            $result= $dinstall->setupentity($entity);
            
            $datarole = $this->getContainer()->get('badiu.system.access.role.data');
            $roleadminid=  $datarole->getIdByShortname($entity,'admin');
            $datauseraccess = $this->getContainer()->get('badiu.system.access.user.data');
            $param=array('entity'=>$entity,'roleid'=>$roleadminid,'userid'=>$userid,'deleted'=>0);
            $result+=$datauseraccess->insertNativeSql($param, false);
            return $result;
        }
        
        public function startSession($entity,$userid) { 
            $name= $this->getParamItem('name');
            $email= $this->getParamItem('email');
             $param=array('userid'=>$userid,'entity'=>$entity,'firstname'=>$name,'lastname'=>'','email'=>$email,'username'=>$email);
             $this->getContainer()->get('badiu.system.access.session')->start($param);
        }
}
