<?php

namespace  Badiu\Admin\ClientBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;

class SellDataOptions extends BadiuFormDataOptions {

    function __construct(Container $container, $baseKey = null) {
        parent::__construct($container, $baseKey);
    }

    public function getStatus() {
        $list = array();
        $list['pending'] = $this->getTranslator()->trans('badiu.admin.client.sell.statuspending');
        $list['provisioned'] = $this->getTranslator()->trans('badiu.admin.client.sell.statusprovisioned');
        $list['completed'] = $this->getTranslator()->trans('badiu.admin.client.sell.statuscompleted');
        $list['canceled'] = $this->getTranslator()->trans('badiu.admin.client.sell.statuscanceled');
         return $list;
    }


}
