<?php

namespace  Badiu\Admin\ClientBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuCheckForm;

class ChangepasswordCheckForm extends BadiuCheckForm {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function validation() {
    
       $currentpassword=$this->getUtildata()->getVaueOfArray($this->getParam(), 'currentpassword');
       $newpassword=$this->getUtildata()->getVaueOfArray($this->getParam(), 'newpassword');
       $newpasswodrconfirm=$this->getUtildata()->getVaueOfArray($this->getParam(), 'newpasswodrconfirm');
       
       $badiuSession = $this->getContainer()->get('badiu.system.access.session');
       $userid=$badiuSession->get()->getUser()->getId();
       $udata=$this->getContainer()->get('badiu.system.user.user.data');
    
       $isvalidate=$udata->validatePasswordById($userid,$currentpassword);
      
       if(!$isvalidate){
       
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.client.changepassword.currentpasswordfailed');
                $info = 'badiu.system.user.changepassword.currentpasswordnotvalid';
                return $this->getResponse()->denied($info, $message);
       }
     
       if($newpassword!==$newpasswodrconfirm){
           
            $message=array();
            $message['generalerror']=$this->getTranslator()->trans('badiu.admin.client.changepassword.newpasswodrconfirmfailed');
            $info = 'badiu.system.user.changepassword.passwodrconfirmfailed';
            return $this->getResponse()->denied($info, $message);
       }
        return null;
    }

  
}
