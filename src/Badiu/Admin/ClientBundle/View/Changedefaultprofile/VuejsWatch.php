'badiuform.nationalitystatus': function () {
	if(this.badiuform.nationalitystatus=='native'){this.badiurequiredcontrol.cpf=true;}
	else {this.badiurequiredcontrol.cpf=false;}
},
'badiuform.enablealternatename': function () {
   this.systemuserdefaultform_alternatename();
},
'badiuform.sex': function () {
   this.systemuserdefaultform_sex();
},
'badiuform.schoollevel': function () {
   this.systemuserdefaultform_schoollevel();
},

'badiuform.dateofbirth_badiudatedefault': function () {
	var vdate=this.badiuform.dateofbirth_badiudatedefault;
	var datevalid=this.badiudatevalidade(vdate,'dd/mm/yyyy');
	
	if(!datevalid){
		this.badiuaerrorcontrol.dateofbirth_badiudatedefault=true;
		//this.badiuaerrormessage.dateofbirth_badiudatedefault="Data inválida";
		this.formcontrol.message="Data de nascimento inválido";
		
		
	}else{
		this.formcontrol.haserror=false;
		//this.formcontrol.processing=true;
	}
   
},
  