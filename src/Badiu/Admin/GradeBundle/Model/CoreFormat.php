<?php

namespace Badiu\Admin\GradeBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
use Badiu\Admin\GradeBundle\Model\DOMAINTABLE;
class CoreFormat extends BadiuFormat{
     private $formdataoptions=null;
     function __construct(Container $container) {
            parent::__construct($container);
			$this->formdataoptions=$this->getContainer()->get('badiu.admin.grade.core.form.dataoptions');
       } 

public  function itemtype($data){
            $value="";
            $type=null;
             $type=$this->getUtildata()->getVaueOfArray($data, 'itemtype');
			 $value=$this->formdataoptions->getItemtypeLabel($type);
            return $value; 
     } 
	 
public  function gradetype($data){
            $value="";
            $type=null;
             $type=$this->getUtildata()->getVaueOfArray($data, 'gradetype');
			 $value=$this->formdataoptions->getGradetypeLabel($type);
            return $value; 
     } 	 
public  function itemsource($data){
            $value="";
            $type=null;
             $type=$this->getUtildata()->getVaueOfArray($data, 'itemsource');
			 $value=$this->formdataoptions->getItemsourceLabel($type);
            return $value; 
     }  

public  function scale($data){
            $value="";
            $gradetype=$this->getUtildata()->getVaueOfArray($data, 'gradetype');
			$maxgrade=$this->getUtildata()->getVaueOfArray($data, 'maxgrade');
			if($gradetype==1 && $maxgrade > 0){$value=$this->getTranslator()->trans('badiu.admin.grade.item.scalenumeric',array('%start%'=>0,'%end%'=>$maxgrade));}
			else if ($gradetype==3){$value=$this->formdataoptions->getGradetypeLabel(3);}
            return $value; 
     }	 
}
