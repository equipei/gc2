<?php

namespace Badiu\Admin\GradeBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ItemData  extends BadiuDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }


	
   public function existItem($entity,$odisciplineid,$itemtype) {
		$r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.modulekey=:modulekey AND o.moduleinstance=:moduleinstance AND o.itemtype=:itemtype";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('modulekey','badiu.ams.offer.discipline');
			$query->setParameter('moduleinstance',$odisciplineid);
			$query->setParameter('itemtype',$itemtype);
			$result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
   }
   
    public function findByItemtype($entity,$odisciplineid,$itemtype) {
		$r=FALSE;
            $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.modulekey=:modulekey AND o.moduleinstance=:moduleinstance AND o.itemtype=:itemtype";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('modulekey','badiu.ams.offer.discipline');
			$query->setParameter('moduleinstance',$odisciplineid);
			$query->setParameter('itemtype',$itemtype);
			$result= $query->getSingleResult();
            
             return $result;
   }
   
    public function existFinalItem($entity,$odisciplineid) {
			$itemtype='finalgrade';
			$r=$this->existItem($entity,$odisciplineid,$itemtype);
            return $r;
   }
   
     public function findFinalItem($entity,$odisciplineid) {
			$itemtype='finalgrade';
			$r=$this->findByItemtype($entity,$odisciplineid,$itemtype);
            return $r;
   }
   
 
}
