<?php

namespace Badiu\Admin\GradeBundle\GradeBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class GradeAddBatchMenu extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function exec() {
        //get param
      
         $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
         $key=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_key');
         $urlback=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urlback');
         $graderate=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_graderate');
         $gradebatchaddtype=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_gradebatchaddtype');
         $gradeitemid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gradeitemid');
         $user=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('user');
         $statusid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('statusid');
       
         $component=null;//discipline | classe
         if($key=='badiu.ams.offer.disciplinegrade.index'){$component='discipline';}
         else if($key=='badiu.ams.offer.classegrade.index'){$component='classe';}
         $forceupadate=true;
         if($gradebatchaddtype=='addgradetowithoutgrade'){$forceupadate=false;}

         $urlback=urldecode($urlback); 
       
         if(empty($parentid) || empty($key) || empty($gradebatchaddtype)){ header("Location: $urlback");}
         if(! is_numeric($graderate)){ header("Location: $urlback");}
         if(empty($component)){ header("Location: $urlback");}

         $badiuSession=$this->getContainer()->get('badiu.system.access.session');
         $entity=$badiuSession->get()->getEntity();
         $param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
         $param['entity']=$entity;
       
       
         //list of user
         $listuser=$this->getEnrrolUserid($component,$param);

         $listitem=array();
         if(!empty($gradeitemid)){$listitem['id']=$gradeitemid;}
        else{
            $gradeitemdata=$this->getContainer()->get('badiu.ams.grade.item.data');
            $paramfilter=null;
            if($component=='discipline'){ $paramfilter=array('moduleinstance'=>$parentid,'modulekey'=>'badiu.ams.offer.discipline');}
            if($component=='classe'){ $paramfilter=array('moduleinstance'=>$parentid,'modulekey'=>'badiu.ams.offer.classe');}
            $listitem= $this->getContainer()->get('badiu.ams.grade.item.data')->getGlobalColumnValues('id',$paramfilter);
        }
       
        $gradedata=$this->getContainer()->get('badiu.ams.grade.grade.data');
       

        foreach ($listitem as $l) {
		
            $itemid=$this->getUtildata()->getVaueOfArray($l, 'id');
			if(empty($itemid) && !is_array($l)){$itemid=$l;}
            foreach ($listuser as $lu) {
                $userid=$this->getUtildata()->getVaueOfArray($lu, 'id');
				$gradedata->add($entity,$itemid,$userid,$graderate,$forceupadate);
            }
        }
        $gradedata=null;
        $gradeitemdata=null;
        $listitem=null;
        $listuser=null;
         header("Location: $urlback");
        exit;
        return null;
    }
 private function getEnrrolUserid($component,$param){
    $enroldata=null;
    $keysqlbase=null;
    if($component=='discipline'){
        $enroldata=$this->getContainer()->get('badiu.ams.enrol.discipline.data');
        $keysqlbase='badiu.ams.offer.disciplinegrade.addbatchmenu';
    }
    if($component=='classe'){
        $enroldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
        $keysqlbase='badiu.ams.offer.classegrade.addbatchmenu';
    }

    $this->getSearch()->getKeymanger()->setBaseKey($keysqlbase);
    $result = $this->getSearch()->searchList($param);
 
    if(isset($result['data'][1])){$result=$result['data'][1];}
    
   return $result; 
 }
    

}
