<?php

namespace Badiu\Admin\GradeBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class GradeManagerLib extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function addItem($param) {
		 $formdataoptions=$this->getContainer()->get('badiu.admin.grade.core.form.dataoptions');
		
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		 
		 $itemtype=$this->getUtildata()->getVaueOfArray($param,'itemtype');
		 $name=$this->getUtildata()->getVaueOfArray($param,'name');
		 if(empty($name)){
			 $name=$formdataoptions->getItemtypeLabel($itemtype);
		 }
		 
		 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
		 $statusid=$this->getUtildata()->getVaueOfArray($param,'statusid');
		 if(empty($statusid)){
			 $statusshortname=$this->getUtildata()->getVaueOfArray($param,'statusshortname');
			 if(empty($statusshortname)){$statusshortname='active';}
			$statusid=$this->getContainer()->get('badiu.admin.grade.itemstatus.data')->getIdByShortname($entity,$statusshortname);
		 }
		
		 $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
		 if(empty($name)){$dtype='grade';}
		 if($itemtype=='finalgrade'){$dtype='grade';}
		 else if($itemtype=='activity'){$dtype='grade';}
		 else if($itemtype=='finalcompletation'){$dtype='progress';}
		 else if($itemtype=='progress'){$dtype='progress';}
		 else if($itemtype=='lmscourseaccess'){$dtype='access';}
		 
		 $gradetype=$this->getUtildata()->getVaueOfArray($param,'gradetype');
		 if(empty($gradetype)){
			if($itemtype=='finalgrade'){$gradetype=1;}
			else if($itemtype=='activity'){$gradetype=1;}
			else if($itemtype=='finalcompletation'){$gradetype=3;}
			else if($itemtype=='progress'){$gradetype=1;}
			else if($itemtype=='lmscourseaccess'){$gradetype=3;}
		  }
		  
		  
		 $maxgrade=$this->getUtildata()->getVaueOfArray($param,'maxgrade');
		 if(empty($maxgrade)){$maxgrade=10;}
		  if($itemtype=='finalcompletation'){$maxgrade=1;}
		  if($itemtype=='progress'){$maxgrade=100;}
		  if($itemtype=='lmscourseaccess'){$maxgrade=1;}
		 
		 $itemsource=$this->getUtildata()->getVaueOfArray($param,'itemsource');
		 if(empty($itemsource)){
			 $itemsource='lms';
		 }
		
		 $fparam=array();
		 $fparam['modulekey']=$modulekey;
		 $fparam['moduleinstance']=$moduleinstance;
		 $fparam['entity']=$entity;
		 $fparam['name']=$name;
		 $fparam['shortname']=$shortname;
		 $fparam['statusid']=$statusid;
		 $fparam['itemtype']=$itemtype;
		 $fparam['dtype']=$dtype;
		 $fparam['gradetype']=$gradetype;
		 $fparam['maxgrade']=$maxgrade;
		 $fparam['itemsource']=$itemsource;
	
		$itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'itemtype'=>$itemtype);
        $result=$itemdata->addNativeSql($paramcheckexist,$fparam,$fparam,true);
        $r=$this->getUtildata()->getVaueOfArray($result,'id');
		  
		return $r; 
	}
    
	public function addGrade($param) {
		$classeid=$this->getUtildata()->getVaueOfArray($param,'classeid');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		$itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		$gradedata=$this->getContainer()->get('badiu.admin.grade.grade.data');
		$paramfilter=array('entity'=>$entity,'modulekey'=>'badiu.ams.offer.classe','moduleinstance'=>$classeid,'itemtype'=>'finalgrade');
		
		$itemid=$itemdata->getGlobalColumnValue('id',$paramfilter); 
		if(empty($itemid)){
			//try to create
			$fiparam=array();
			$itemid=$this->addItem($paramfilter);
		}
		if(empty($itemid)){return null;}

		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		$grade=$this->getUtildata()->getVaueOfArray($param,'finalgrade');
		$timecreated=$this->getUtildata()->getVaueOfArray($param,'timecreated');
		$timemodified=$this->getUtildata()->getVaueOfArray($param,'timemodified');
		
		$timemodifiedgrade=null;
		if(empty($timemodified)){$timemodified=$timecreated;}
		if(!empty($timemodified)){
		
			$timemodifiedgrade=new \DateTime();
			$timemodifiedgrade->setTimestamp($timemodified);
		}
		
		$gparamcheckexist=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid);
		$gparamadd=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid,'grade'=>$grade,'deleted'=>0,'timecreated'=>new \DateTime(),'timemodified'=>new \DateTime(),'timemodifiedgrade'=>$timemodifiedgrade);
		$gparamedit=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid,'grade'=>$grade,'timemodified'=>new \DateTime(),'timemodifiedgrade'=>$timemodifiedgrade);
	
		$dresult=$gradedata->addNativeSql($gparamcheckexist,$gparamadd,$gparamedit);
		$rid=$this->getUtildata()->getVaueOfArray($dresult,'id');
		
		return $rid; 
	}
	
	public function addFinalCompletation($param) {
		$classeid=$this->getUtildata()->getVaueOfArray($param,'classeid');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		if(empty($entity)){$entity=$this->getEntity();}
		$itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		$gradedata=$this->getContainer()->get('badiu.admin.grade.grade.data');
		$paramfilter=array('entity'=>$entity,'modulekey'=>'badiu.ams.offer.classe','moduleinstance'=>$classeid,'itemtype'=>'finalcompletation');
		
		$itemid=$itemdata->getGlobalColumnValue('id',$paramfilter); 
		if(empty($itemid)){
			//try to create
			$fiparam=array();
			$itemid=$this->addItem($paramfilter);
		}
	
		if(empty($itemid)){return null;}

		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		$grade=$this->getUtildata()->getVaueOfArray($param,'finalgrade');
		$timecreated=$this->getUtildata()->getVaueOfArray($param,'timecreated');
		$timemodified=$this->getUtildata()->getVaueOfArray($param,'timemodified');
		
		$timemodifiedgrade=null;
		$timecreatedgrade=null;
		if(empty($timemodified)){$timemodified=$timecreated;}
		if(!empty($timemodified)){
			$timemodifiedgrade=new \DateTime();
			$timemodifiedgrade->setTimestamp($timemodified);
		}
		
		if(!empty($timecreated)){
			$timecreatedgrade=new \DateTime();
			$timecreatedgrade->setTimestamp($timecreated);
		}
		$gparamcheckexist=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid);
		$gparamadd=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid,'grade'=>$grade,'deleted'=>0,'timecreated'=>new \DateTime(),'timemodified'=>new \DateTime(),'timecreatedgrade'=>$timecreatedgrade,'timemodifiedgrade'=>$timemodifiedgrade);
		$gparamedit=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid,'grade'=>$grade,'timemodified'=>new \DateTime(),'timemodifiedgrade'=>$timemodifiedgrade);
		$dresult=$gradedata->addNativeSql($gparamcheckexist,$gparamadd,$gparamedit);
		$rid=$this->getUtildata()->getVaueOfArray($dresult,'id');
		
		return $rid; 
	}
	
	public function addProgress($param) {
		
		$classeid=$this->getUtildata()->getVaueOfArray($param,'classeid');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		$itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		$gradedata=$this->getContainer()->get('badiu.admin.grade.grade.data');
		$paramfilter=array('entity'=>$entity,'modulekey'=>'badiu.ams.offer.classe','moduleinstance'=>$classeid,'itemtype'=>'progress');
	
		$itemid=$itemdata->getGlobalColumnValue('id',$paramfilter); 
		if(empty($itemid)){
			//try to create
			$fiparam=array();
			$itemid=$this->addItem($paramfilter);
		}
		if(empty($itemid)){return null;}

		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		
		
		$countactivitycompleted=$this->getUtildata()->getVaueOfArray($param,'countactivitycompleted');
		$countactivityanebleprogress=$this->getUtildata()->getVaueOfArray($param,'countactivityanebleprogress');
		$countactivityrequiretocoursecomplete=$this->getUtildata()->getVaueOfArray($param,'countactivityrequiretocoursecomplete');
		$countactivitycomletedrequiretocoursecomplete=$this->getUtildata()->getVaueOfArray($param,'countactivitycomletedrequiretocoursecomplete');
		
		$x=$countactivitycompleted;
		$y=$countactivityanebleprogress;
		
		
		
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		$badiuSession->setHashkey($this->getSessionhashkey());
		$courseprogresscountonlyrequiredactivities=$badiuSession->getValue('badiu.ams.offer.synclms.param.config.courseprogresscountonlyrequiredactivities');
		if($courseprogresscountonlyrequiredactivities){
			$x=$countactivitycomletedrequiretocoursecomplete;
			$y=$countactivityrequiretocoursecomplete;
		}
		
		$perc=0;
		if($y >=0 && $x >=0 && $y>= $x){
            if($y==0){$perc=0;}
                else{
                     $perc= $x*100/$y;
                 }
		}	
		$grade=$perc;
		$info=array('countactivitycompleted'=>$countactivitycompleted,'countactivityanebleprogress'=>$countactivityanebleprogress,'countactivityanebleprogressincoursecompletetionconfig'=>$countactivityrequiretocoursecomplete,'countactivitycompleincoursecompletetionconfig'=>$countactivitycomletedrequiretocoursecomplete);
		$dconfig=array();
		$dconfig['progress']=$info; 
		
		//review get original data
		$dconfig = $this->getJson()->encode($dconfig);	
		
		$gparamcheckexist=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid);
		$gparamadd=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid,'grade'=>$grade,'customint1'=>$countactivityanebleprogress,'customint2'=>$countactivitycompleted,'customint3'=>$countactivityrequiretocoursecomplete,'customint4'=>$countactivitycomletedrequiretocoursecomplete,'dconfig'=>$dconfig,'deleted'=>0,'timecreated'=>new \DateTime(),'timemodified'=>new \DateTime());
		$gparamedit=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid,'grade'=>$grade,'customint1'=>$countactivityanebleprogress,'customint2'=>$countactivitycompleted,'customint3'=>$countactivityrequiretocoursecomplete,'customint4'=>$countactivitycomletedrequiretocoursecomplete,'dconfig'=>$dconfig,'timemodified'=>new \DateTime());
		$dresult=$gradedata->addNativeSql($gparamcheckexist,$gparamadd,$gparamedit);
		$rid=$this->getUtildata()->getVaueOfArray($dresult,'id');
		
		return $rid; 
	}
	
	public function addLastaccess($param) {
		$classeid=$this->getUtildata()->getVaueOfArray($param,'classeid');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		$itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		$gradedata=$this->getContainer()->get('badiu.admin.grade.grade.data');
		$paramfilter=array('entity'=>$entity,'modulekey'=>'badiu.ams.offer.classe','moduleinstance'=>$classeid,'itemtype'=>'lmscourseaccess');
		
		$itemid=$itemdata->getGlobalColumnValue('id',$paramfilter); 
		if(empty($itemid)){
			//try to create
			$fiparam=array();
			$itemid=$this->addItem($paramfilter);
		}
		if(empty($itemid)){return null;}

		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		$grade=$this->getUtildata()->getVaueOfArray($param,'grade');
		$timeaccess=$this->getUtildata()->getVaueOfArray($param,'timeaccess');
		
		
		$gparamcheckexist=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid);
		$gparamadd=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid,'grade'=>$grade,'timecreatedgrade'=>$timeaccess,'timemodifiedgrade'=>$timeaccess,'deleted'=>0,'timecreated'=>new \DateTime(),'timemodified'=>new \DateTime());
		$gparamedit=array('entity'=>$entity,'itemid'=>$itemid,'userid'=>$userid,'grade'=>$grade,'timemodifiedgrade'=>$timeaccess,'timemodified'=>new \DateTime());
		$dresult=$gradedata->addNativeSql($gparamcheckexist,$gparamadd,$gparamedit);
		$rid=$this->getUtildata()->getVaueOfArray($dresult,'id');
		
		return $rid; 
	}
	public function addGradeSingle($param=null) {
		if(empty($param)){
			$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		}else if(is_array($param) && sizeof($param)==0){
			$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		}
		if(empty($param)){return null;}
		if(!is_array($param)){return null;}
		
		$gradedata=$this->getContainer()->get('badiu.admin.grade.grade.data');
		$grade=$this->getUtildata()->getVaueOfArray($param,'grade');
		$gradeid=$this->getUtildata()->getVaueOfArray($param,'gradeid');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		//update
		if(!empty($gradeid)){
			$uparam=array('id'=>$gradeid,'grade'=>$grade,'timemodified'=>new \DateTime());
			$dresult=$gradedata->updateNativeSql($uparam,false);
			return $dresult;
		}
		//add new or update
		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		$modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		$moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		$dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
		$referencekey=$this->getUtildata()->getVaueOfArray($param,'referencekey');;
		if(empty($userid)){$userid=$this->getUserid();}
		
		$gparamcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'dtype'=>$dtype,'userid'=>$userid);
		$gparamadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'dtype'=>$dtype,'userid'=>$userid,'grade'=>$grade,'referencekey'=>$referencekey,'deleted'=>0,'timecreated'=>new \DateTime(),'timemodified'=>new \DateTime());
		$gparamedit=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'dtype'=>$dtype,'userid'=>$userid,'grade'=>$grade,'referencekey'=>$referencekey,'timemodified'=>new \DateTime());
		$dresult=$gradedata->addNativeSql($gparamcheckexist,$gparamadd,$gparamedit);
		$rid=$this->getUtildata()->getVaueOfArray($dresult,'id');
		
		return $rid; 
	}
	
	public function updateGrade($param=null) {
		if(empty($param)){$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();}
		$gradeid=$this->getUtildata()->getVaueOfArray($param,'gradeid');
		$grade=$this->getUtildata()->getVaueOfArray($param,'grade');
		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		$itemid=$this->getUtildata()->getVaueOfArray($param,'itemid');
		
		$rvalid=$this->validateGrade($param);
		if(!empty($rvalid)){return $rvalid;}
		$grade=str_replace(",",".",$grade);
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		if(empty($entity)){$entity=$this->getEntity();}
		
		$dresult=null;
		$gradedata=$this->getContainer()->get('badiu.admin.grade.grade.data');
		if(!empty($gradeid)){
			$fparam=array('entity'=>$entity,'id'=>$gradeid,'grade'=>$grade,'timemodified'=>new \DateTime(),'timemodifiedgrade'=>new \DateTime());
			$dresult=$gradedata->updateNativeSql($fparam,false);
		}else{
			//insert grade
			$paramcheckexist=array('entity'=>$entity,'userid'=>$userid,'itemid'=>$itemid);
			$paramadd=array('entity'=>$entity,'userid'=>$userid,'grade'=>$grade,'itemid'=>$itemid,'timecreated'=>new \DateTime(),'timemodified'=>new \DateTime(),'timemodifiedgrade'=>new \DateTime());
			$paramedit=array('entity'=>$entity,'userid'=>$userid,'grade'=>$grade,'itemid'=>$itemid,'timemodified'=>new \DateTime(),'timemodifiedgrade'=>new \DateTime());
			$presult=$gradedata->addNativeSql($paramcheckexist,$paramadd,$paramedit);
			$dresult=$this->getUtildata()->getVaueOfArray($presult,'id');	
		}
		
		return $dresult;
	}
	
	public function validateGrade($param) {
		$gradeid=$this->getUtildata()->getVaueOfArray($param,'gradeid');
		$grade=$this->getUtildata()->getVaueOfArray($param,'grade');
		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		$itemid=$this->getUtildata()->getVaueOfArray($param,'itemid');
		
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		if(empty($entity)){$entity=$this->getEntity();}
		
		if(empty($gradeid) && empty($itemid) ){
			return $this->getResponse()->denied('badiu.admin.grade.error.paramgradeidoritemidrequired','param gradeid or itemid is required');		
		}
		if(empty($gradeid) && (empty($itemid) || empty($userid) )){
			return $this->getResponse()->denied('badiu.admin.grade.error.paramuseridanditemidrequired','param usereid and itemid is required');		
		}
		
		
		$grade=str_replace(",",".",$grade);
		if(!is_numeric($grade)){
			return $this->getResponse()->denied('badiu.admin.grade.error.gradeisnotnumber','grade is not a number');		
		}
		
		//check max grade
		$itemdata=$this->getContainer()->get('badiu.admin.grade.item.data');
		$paramfilter=array('entity'=>$entity,'id'=>$itemid);
		
		$maxgrade=$itemdata->getGlobalColumnValue('maxgrade',$paramfilter); 
		
		if($grade > $maxgrade){
			$message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.grade.gradeisupperthanmaxgrade',array('%maxgrade%'=>$maxgrade));
            $info='badiu.admin.grade.gradeisupperthanmaxgrade';
            return $this->getResponse()->denied($info, $message);
		}
		
		return null;
	}
	
	public function deleteGradeByItemid($param) {
		 
		$itemid=$this->getUtildata()->getVaueOfArray($param,'itemid');
		$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		if(empty($entity)){$entity=$this->getEntity();}
		if(empty($itemid)){return 0;}
		$filter=array('entity'=>$entity,'itemid'=>$itemid);
		
		 $gradedata=$this->getContainer()->get('badiu.admin.grade.grade.data');
		 $count= $gradedata->countNativeSql($filter,false);
		
		 if(empty($count)){return 0;} 
		 $result= $gradedata->removeBatchNativeSql($filter,false);
		
		 if($result){return $count;}
		  return 0;
		
	}
}
