<?php

namespace Badiu\Admin\GradeBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Badiu\Admin\GradeBundle\Model\DOMAINTABLE;
class CoreFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }

    public  function getItemtype(){
        $list=array();
        $list['finalgrade']=$this->getItemtypeLabel('finalgrade');
        $list['finalcompletation']=$this->getItemtypeLabel('finalcompletation');
		$list['activity']=$this->getItemtypeLabel('activity');
		$list['progress']=$this->getItemtypeLabel('progress');
        return $list;
    }
	public  function getItemtypeLabel($key){
        $label="";
		if($key=='finalgrade'){$label=$this->getTranslator()->trans('badiu.admin.grade.item.itemtype.finalgrade');}
		if($key=='activity'){$label=$this->getTranslator()->trans('badiu.admin.grade.item.itemtype.activity');}
		if($key=='finalcompletation'){$label=$this->getTranslator()->trans('badiu.admin.grade.item.itemtype.finalcompletation');}
		if($key=='progress'){$label=$this->getTranslator()->trans('badiu.admin.grade.item.itemtype.progress');}
		if($key=='lmscourseaccess'){$label=$this->getTranslator()->trans('badiu.admin.grade.item.itemtype.lmscourseaccess');}
		return  $label; 
    }
	
	public  function getGradetype(){
        $list=array();
        $list[1]=$this->getGradetypeLabel(1);
		//$list[2]=$this->getGradetypeLabel(2);
		$list[3]=$this->getGradetypeLabel(3);
        
        return $list;
    }
	public  function getGradetypeLabel($key){
        $label="";
		if($key==1){$label=$this->getTranslator()->trans('badiu.admin.grade.item.gradetype.numeric');}
		if($key==3){$label=$this->getTranslator()->trans('badiu.admin.grade.item.gradetype.boolean');}
		if($key==2){$label=$this->getTranslator()->trans('badiu.admin.grade.item.gradetype.scale');}
		return  $label;
    }
	
 public  function getItemsource(){
        $list=array();
        $list['manual']=$this->getItemsourceLabel('manual');
        $list['lms']=$this->getItemsourceLabel('lms');
	   return $list;
    }
	public  function getItemsourceLabel($key){
        $label="";
		if($key=='lms'){$label=$this->getTranslator()->trans('badiu.admin.grade.item.itemsource.lms');}
		if($key=='manual'){$label=$this->getTranslator()->trans('badiu.admin.grade.item.itemsource.manual');}
		return  $label;
    }
	
}
