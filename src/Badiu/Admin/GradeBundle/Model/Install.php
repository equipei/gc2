<?php
 
 namespace Badiu\Admin\GradeBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }

    public function exec() {
      $result=$this->initDbItemStatus() ;
     
     return $result;
 } 
 

  public function initDbItemStatus() {
        $cont=0;
        $entity=$this->getEntity();
        $data = $this->getContainer()->get('badiu.admin.grade.itemstatus.data');
        $exist=$data->countGlobalRow(array('entity'=>$entity));
        if($exist && !$this->getForceupdate()){return 0;}

  $entity=$this->getEntity();
        $list=array(
     array('entity'=>$entity,'deleted'=>0,'shortname'=>'active','timecreated'=>new \DateTime(),'name'=>$this->getTranslator()->trans('badiu.admin.grade.itemstatus.active')),
     array('entity'=>$entity,'deleted'=>0,'shortname'=>'locked','timecreated'=>new \DateTime(),'name'=>$this->getTranslator()->trans('badiu.admin.grade.itemstatus.locked')),
     array('entity'=>$entity,'deleted'=>0,'shortname'=>'inapproval','timecreated'=>new \DateTime(),'name'=>$this->getTranslator()->trans('badiu.admin.grade.itemstatus.inapproval')),
              
    );  
     
    
        
        
        foreach ($list as $param) {
      $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
      $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
      
      $paramedit=null;
      if($this->getForceupdate()){
        $paramedit= $param;
      }
      $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
      $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,false);
      $r=$this->getUtildata()->getVaueOfArray($result,'id');
             if($r){$cont++;}
           }
 return $cont;
   } 


  

}
