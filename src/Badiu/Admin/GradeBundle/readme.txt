--rever itemtype is seted discipline not type of item
#UPDATE `ams_grade_item` SET `itemtype` = 'finalgrade' WHERE `ams_grade_item`.`id` = 1990;

UPDATE  ams_grade_item SET modulekey='badiu.ams.offer.discipline', moduleinstance=odisciplineid WHERE odisciplineid IS NOT NULL AND dtype='discipline'
UPDATE  ams_grade_item SET modulekey='badiu.ams.offer.classe', moduleinstance=classeid WHERE classeid IS NOT NULL AND dtype='classe' 
#check duplication
SELECT moduleinstance,count(moduleinstance) FROM admin_grade_item WHERE moduleinstance > 0 AND itemtype='finalgrade' AND entity=1 GROUP BY moduleinstance HAVING count(moduleinstance) >1
SELECT g.itemid,g.userid,count(g.id) FROM admin_grade g INNER JOIN admin_grade_item i ON g.itemid=i.id WHERE i.itemtype ='finalgrade' GROUP BY g.itemid,g.userid HAVING count(g.userid) >1
