<?php

namespace Badiu\Admin\GradeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminGrade
 *  
 * @ORM\Table(name="admin_grade", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_grade_uix", columns={"entity", "itemid", "userid"}),
 *      @ORM\UniqueConstraint(name="admin_grade_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="admin_grade_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="admin_grade_itemid_ix", columns={"itemid"}), 
 *              @ORM\Index(name="admin_grade_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="admin_grade_grade_ix", columns={"grade"}),
 *              @ORM\Index(name="admin_grade_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_grade_timecreatedgrade_ix", columns={"timecreatedgrade"}),
 *              @ORM\Index(name="admin_grade_timemodifiedgrade_ix", columns={"timemodifiedgrade"}), 
 *              @ORM\Index(name="admin_grade_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_grade_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_grade_moduleinstance_ix", columns={"moduleinstance"}), 
 *              @ORM\Index(name="admin_grade_referencekey_ix", columns={"referencekey"}), 
 *              @ORM\Index(name="admin_grade_customint1_ix", columns={"customint1"}), 
  *              @ORM\Index(name="admin_grade_customint2_ix", columns={"customint2"}), 
*              @ORM\Index(name="admin_grade_customint3_ix", columns={"customint3"}),  
*              @ORM\Index(name="admin_grade_customint4_ix", columns={"customint4"}), 
 *              @ORM\Index(name="admin_grade_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminGrade
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

	/**
     * @var AdminGradeItem
     *
     * @ORM\ManyToOne(targetEntity="AdminGradeItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="itemid", referencedColumnName="id")
     * })
     */
    private $itemid;

	
	/**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id", nullable=false)
     * })
     */
    private $userid;

    /**
     * @var float
     *
     * @ORM\Column(name="grade", type="float", precision=10, scale=0, nullable=true)
     */
    private $grade=null;

    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;
	    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;

/**
	 * @var integer
	 *
	 * @ORM\Column(name="referencekey", type="bigint", nullable=true)
	 */
	private $referencekey;

    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreatedgrade", type="datetime", nullable=true)
     */
    private $timecreatedgrade;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodifiedgrade", type="datetime", nullable=true)
     */
    private $timemodifiedgrade;
	  /**
     * @var boolean
     *
     * @ORM\Column(name="overridden", type="integer", nullable=true)
     */
    private $overridden;
	  /**
     * @var boolean
     *
     * @ORM\Column(name="loked", type="integer", nullable=true)
     */
    private $loked;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;

    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
      
    private $idnumber;
 
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customint1", type="bigint",  nullable=true)
	 */
	private $customint1;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customint2", type="bigint",  nullable=true)
	 */
	private $customint2;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customint3", type="bigint",  nullable=true)
	 */
	private $customint3;
	
		/**
	 * @var integer
	 *
	 * @ORM\Column(name="customint4", type="bigint",  nullable=true)
	 */
	private $customint4;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

  
    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getGrade() {
        return $this->grade;
    }
  

    public function setGrade($grade) {
        $this->grade = $grade;
    }


	  
    public function getItemid() {
        return $this->itemid;
    }

    public function setItemid(AdminGradeItem $itemid) {
        $this->itemid = $itemid;
    }
 
 
    /**
     * @return \Badiu\System\UserBundle\Entity\SystemUser
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param \Badiu\System\UserBundle\Entity\SystemUser $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    public function setOverridden($overridden) {
        $this->overridden = $overridden;
    }

	
    public function getOverridden() {
        return $this->overridden;
    }

	
	    public function setLoked($loked) {
        $this->loked = $loked;
    }

	
    public function getLoked() {
        return $this->loked;
    }

	
    function getDconfig() {
        return $this->dconfig;
    }
	
	
    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }
	
		  
  /**
     * @return string
     */
    public function getModulekey()
    {
        return $this->modulekey;
    }

    /**
     * @param string $dtype
     */
    public function setModulekey($modulekey)
    {
        $this->modulekey = $modulekey;
    }


     /**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string $dtype
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }
	
	
  /**
     * @return string
     */
    public function getReferencekey()
    {
        return $this->referencekey;
    }

    /**
     * @param string $referencekey
     */
    public function setReferencekey($referencekey)
    {
        $this->referencekey = $referencekey;
    }


    function getDtype() {
        return $this->dtype;
    }

  

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }
   function getCustomint1() {
	   return $this->customint1;
        }

        function getCustomint2() {
            return $this->customint2;
        }

        function getCustomint3() {
            return $this->customint3;
        }

	function setCustomint1($customint1) {
            $this->customint1 = $customint1;
        }

        function setCustomint2($customint2) {
            $this->customint2 = $customint2;
        }

        function setCustomint3($customint3) {
            $this->customint3 = $customint3;
        }
		
function getCustomint4() {
            return $this->customint4;
        }
 function setCustomint4($customint4) {
            $this->customint4 = $customint4;
        }
}
