<style>
@page :first{

	background: url("<?php echo $param->imagebackground;?>") no-repeat 0 0;
    background-image-resize: 6;  	
}

.content_certificate{
 position: fixed;
 top: 25%;
 left: 0;
 right: 0;
 padding: 50px;
}

.date_certificate{
 position: fixed;
 top: 42%;   
 right: 0;
 padding: 50px;
}

</style>

	<body>

	<div class='content_certificate'>
		
		<p>O Ministério da Cidadania (CNPJ:05.526.783/0001-65) certifica que <b><?php echo $param->userfullname;?></b>, CPF: <b><?php echo $param->usercpf;?></b>, concluiu, com
aproveitamento, o curso <b><?php echo $param->coursename;?></b> na modalidade a distância, disponibilizado no período de
<b><?php echo $param->timestart;?></b> a <b><?php echo $param->timeend;?></b>, com carga horária de <?php echo $param->hour;?> horas.</p>

	</div>

	<div class='date_certificate'>
		
		<p>Brasília, <?php echo $param->dategenerate;?>.</p>

	</div>

</body>


