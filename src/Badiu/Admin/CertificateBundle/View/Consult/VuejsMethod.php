  searchCertificate: function () {

	var self = this;
		self.fparam.withoutdata=false;
		if(this.isparamempty(this.badiuform.skey)){return null;}
	
			this.formcontrol.processing=true;
			
			this.badiuform._service="badiu.admin.certificate.consult.formcontroller";
			this.badiuform._function="search";
			
			
			   axios.post(this.serviceurl,this.badiuform).then(function (response) {
				      console.log(response.data);
					 if(response.data.status=='accept'){
						
						 if(self.isparamempty(response.data.message.result)){self.fparam.withoutdata=true;}
						 else {
							 self.fparam.withoutdata=false;
							 self.fparam.certificate=response.data.message.result;
						  }
						 self.formcontrol.processing=false;
					 }else if(response.data.status=='danied'){
						self.loginsinginManageFormView('withoutregister');
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
						if(response.data.info=='badiu.financ.ecommerce.loginsingin.userjustexistonadd'){self.loginsinginManageFormView('hasregisteronadd');}
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 
  },

  getCertificateUrl: function (skey) {
	  var url= "<?php echo $container->get('badiu.system.core.lib.util.app')->getBaseUrl(); ?>/admin/certificate/get/"+skey;
	  return url;
  }