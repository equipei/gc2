<?php
$utilapp=$container->get('badiu.system.core.lib.util.app');
 $urlhtmlcontentcore=$utilapp->getFilePath($page->getConfig()->getFileprocessformadd());
 require_once($urlhtmlcontentcore);
$formconfig=new stdClass();
$formconfig->showsubmitrow=false;
$factoryformfilter->setConfig($formconfig);
$iconProcess=$container->get('badiu.theme.core.lib.template.vuejs.factoryutil')->iconProcess();
$hideffilter=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_hideffilter');

?>
<h3><?php echo $page->getOperation();?></h3>
<div id="_badiu_theme_base_form_vuejs">
 <?php if(!$hideffilter){?>
		<div class="card">
			<div class="card-body">
			 
				<div><?php echo $factoryformfilter->exec('add');?></div>
				<div ><button  @click="searchCertificate()" type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.admin.certificate.consult.search'); ?></button><?php echo $iconProcess; ?></div>
			</div>
			
		</div>
		<br />
  <?php } ?>
	
	<div class="alert alert-danger" role="alert" v-if="fparam.withoutdata">
		<?php echo $translator->trans('badiu.admin.certificate.consult.menssege.notfound'); ?>
	</div>


		<div class="card"  id="certificate_data"  v-if="fparam.certificate.id">
			<div class="card-header">
				Dados do Certificado
			</div>
			<div class="card-body">
				<table class="table table-striped">
   
    <tbody>
     
      <tr>
        <td style="width: 15%">Usuário</td>
        <td>{{fparam.certificate.firstname}} {{fparam.certificate.lastname}}</td>
        
      </tr>
     <tr>
        <td style="width: 15%">{{fparam.certificate.doctype}}</td>
        <td>{{fparam.certificate.docnumber}}</td>
       
      </tr>
	  <tr>
        <td style="width: 15%">Status</td>
        <td>{{fparam.certificate.statusname}}</td>
       
      </tr>
	  <tr>
        <td style="width: 15%">Código</td>
        <td>{{fparam.certificate.skey}}</td>
       
      </tr>
	   <tr v-if="fparam.certificate.modulename">
        <td style="width: 15%">Curso</td>
        <td>{{fparam.certificate.modulename}}</td>
       
      </tr>
	  <tr>
        <td style="width: 15%">Carga horária</td>
        <td>{{fparam.certificate.dhour}}</td>
       
      </tr>
	
    </tbody>
  </table>
			</div>
			<div class="card-footer">

			   	<a class="btn btn-primary"  v-bind:href="getCertificateUrl(fparam.certificate.skey)"  target="_blank" role="button">Visualizar certificado</a>
			  
			</div>
		</div>
		

</div>
<?php echo $formappvuejs;?>