<?php
 
 namespace Badiu\Admin\CertificateBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }
 

    public function exec() {
       $result=$this->initDbStatus() ;
      $result+=$this->initDbTemplateStatus();
	   $result+=$this->initPermissions();
  		return $result;
	} 
	
	 public function initDbStatus() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.certificate.status.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist && !$this->getForceupdate()){return 0;}
        
		$entity=$this->getEntity();
         $list=array(
			array('entity'=>$entity,'shortname'=>'published','name'=>$this->getTranslator()->trans('badiu.admin.certificate.status.published')),
      array('entity'=>$entity,'shortname'=>'inapproval','name'=>$this->getTranslator()->trans('badiu.admin.certificate.status.inapproval')),
	  array('entity'=>$entity,'shortname'=>'draft','name'=>$this->getTranslator()->trans('badiu.admin.certificate.status.draft')),
      array('entity'=>$entity,'shortname'=>'canceled','name'=>$this->getTranslator()->trans('badiu.admin.certificate.status.canceled')),
      				
		 ); 
		 
         
         
         foreach ($list as $param) {
			 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 
			 $paramedit=null;
			 if($this->getForceupdate()){
				 $paramedit= $param;
			 }
			 $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
			 $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
			 $r=$this->getUtildata()->getVaueOfArray($result,'id');
              if($r){$cont++;}
            }
	return $cont;
    }

  
	 
public function initDbTemplateStatus() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.certificate.templatestatus.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist && !$this->getForceupdate()){return 0;}
        
		$entity=$this->getEntity();
         $list=array(
			array('entity'=>$entity,'shortname'=>'active','name'=>$this->getTranslator()->trans('badiu.admin.certificate.templatestatus.active')),
			array('entity'=>$entity,'shortname'=>'inactive','name'=>$this->getTranslator()->trans('badiu.admin.certificate.templatestatus.inactive')),
      				
		 ); 
		 
         
         
         foreach ($list as $param) {
			 $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
			 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			 
			 $paramedit=null;
			 if($this->getForceupdate()){
				 $paramedit= $param;
			 }
			 $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
			 $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
			 $r=$this->getUtildata()->getVaueOfArray($result,'id');
              if($r){$cont++;}
            }
	return $cont;
    }
	
	  public function initPermissions() {
        $cont=0;
		
        $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');

        //guest  
        $param=array(
          'roleshortname'=>'guest',
          'permissions'=>array('badiu.admin.certificate.get.link',
          'badiu.admin.certificate.consult.add')
        );
        $cont+=$libpermission->add($param);

        //authenticateduser
        $param=array(
            'roleshortname'=>'authenticateduser',
            'permissions'=>array('badiu.admin.certificate.request.link',
			'badiu.admin.certificate.my.index')
		);
		
          $cont+=$libpermission->add($param);

		return  $cont;
    }
 
}
