<?php

namespace Badiu\Admin\CertificateBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ConsultFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
    
    public function search() {
		$this->initParam();
		$skey=$this->getParamItem('skey'); 
		$certificatedata=$this->getContainer()->get('badiu.admin.certificate.certificate.data');
		$cdresult=$certificatedata->getBySkey($skey);
		$cdresult=$this->format($cdresult);
		$this->setResultexec($cdresult);
		 $outrsult=$this->execResponse();
		return $outrsult;
	}
   
     private function format($param) {
		 $doctype=$this->getUtildata()->getVaueOfArray($param,'doctype');
		 $docnumber=$this->getUtildata()->getVaueOfArray($param,'docnumber');
		 
		 if($doctype=='CPF' && !empty($docnumber)){
				$docnumber=$this->getContainer()->get('badiu.util.document.role.cpf')->format($docnumber);
				 $param['docnumber']=$docnumber;
		 }
		 $dhour=$this->getUtildata()->getVaueOfArray($param,'dhour');
		 $configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
		 $dhour=$configformat->textHour($dhour);
		 $param['dhour']=$dhour;
		 
		 $certficateformat=$this->getContainer()->get('badiu.admin.certificate.format');
		 $modulename=$certficateformat->getModuleName($param);
		 $param['modulename']=$modulename;
		 return $param;
	 }
}
