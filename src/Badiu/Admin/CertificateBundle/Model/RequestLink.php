<?php

 namespace Badiu\Admin\CertificateBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class RequestLink extends BadiuModelLib{
    private $param;
    function __construct(Container $container) {
            parent::__construct($container);
			$this->param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
            }
    
        
        public function exec() {
				$this->viewTemplate();
				$this->generate();
				//$this->view();
				return null;
         }
        
          private function viewTemplate() {
				$operation=$this->getUtildata()->getVaueOfArray($this->param, '_operation');
				if($operation!='viewtemplate'){return null;}
				
				$templateid=$this->getUtildata()->getVaueOfArray($this->param, 'templateid');
				if(empty($templateid)){echo "param templateid is required";exit;}
				
				$factoryteste=$this->getContainer()->get('badiu.admin.certificate.factoryteste');
				$enroldata=$factoryteste->getDafaultEnrolData();
				
				$factorycreate=$this->getContainer()->get('badiu.admin.certificate.factorygenerate');
				$templatedata=$factorycreate->getTemplatedata($templateid);
				$param=$factorycreate->addTemplatedataToparam($enroldata,$templatedata);
				$cdata=$factorycreate->makeData($param);
				$cdata['_operation']='viewtemplate';
				$factorycreate->make($cdata);
				
				//$factorycreate
				
				exit;
         }

		 
		 function messageout($source,$message){
			 if($source=='api'){return 'failure';}
			 else {echo $message;exit;}
			 return null;
		 }
		 private function createManageParam($param) {
				$source=$this->getUtildata()->getVaueOfArray($param,'_source');
				$configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
				$modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
				$moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
				$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
				$enrolid=$this->getUtildata()->getVaueOfArray($param,'enrolid');
				$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
				$version=$this->getUtildata()->getVaueOfArray($param,'version');
				$operation=$this->getUtildata()->getVaueOfArray($this->param, '_operation');
				$skey=$this->getUtildata()->getVaueOfArray($this->param, 'skey');
				if(empty($entity)){$entity=$this->getEntity();}
		 
				if(empty($modulekey)){return $this->messageout($source,'parametro invalido');}
				if(empty($moduleinstance)){return $this->messageout($source,'parametro invalido');}
				if(empty($userid)){return $this->messageout($source,'parametro invalido');}
				$statusshortname='approvedenrol';
				$roleshortname='studentcorporate';
				
				$dhour=null;
				$enrolinfo=null;
				
				$certificateupdateuser=1;
				$certificateupdateenrol=1;
				$certificateupdateobjectinfo=1;
				$certificateupdateobjectdata=1;
			
				
				if($modulekey=='badiu.ams.enrol.classe'){
					//check user completed
					$classeenroldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
		
					$checked=$classeenroldata->checkSatus($entity,$moduleinstance,$userid,$statusshortname);
					if(!$checked){return $this->messageout($source,'Nao atende requisito para certificao');}
		 
					$roleid=$this->getContainer()->get('badiu.ams.role.role.data')->getIdByShortname($entity,$roleshortname);
					
					if(empty($enrolid)){$enrolid=$classeenroldata->getEnrolid($entity,$moduleinstance,$userid,$roleid);}
					if(empty($enrolid)){return $this->messageout($source,'matricula nao localizada');}
		 
					$enrolinfo=$classeenroldata->geInfo($entity,$enrolid);
					if(!is_array($enrolinfo)){return $this->messageout($source,'dados da mantricula nao localizado');}
					
					$certificateupdateuser=$this->getUtildata()->getVaueOfArray($enrolinfo,'classecertificateupdateuser');
					$certificateupdateenrol=$this->getUtildata()->getVaueOfArray($enrolinfo,'classecertificateupdateenrol');
					$certificateupdateobjectinfo=$this->getUtildata()->getVaueOfArray($enrolinfo,'classecertificateupdateobjectinfo');
					$certificateupdateobjectdata=$this->getUtildata()->getVaueOfArray($enrolinfo,'classecertificateupdateobjectdata');
			
				}else if($modulekey=='badiu.ams.enrol.offer'){
					 
					//check user completed
					$offerenroldata=$this->getContainer()->get('badiu.ams.enrol.offer.data');
		
					$checked=$offerenroldata->checkSatus($entity,$moduleinstance,$userid,$statusshortname);
					if(!$checked){return $this->messageout($source,'Nao atende requisito para certificao');}
		 
					$roleid=$this->getContainer()->get('badiu.ams.role.role.data')->getIdByShortname($entity,$roleshortname);
					
					if(empty($enrolid)){$enrolid=$offerenroldata->getEnrolid($entity,$moduleinstance,$userid,$roleid);}
					if(empty($enrolid)){return $this->messageout($source,'matricula nao localizada');}
		 
					$enrolinfo=$offerenroldata->geInfo($entity,$enrolid,'training');
					if(!is_array($enrolinfo)){return $this->messageout($source,'dados da mantricula nao localizado');}
				
					$certificateupdateuser=$this->getUtildata()->getVaueOfArray($enrolinfo,'offercertificateupdateuser');
					$certificateupdateenrol=$this->getUtildata()->getVaueOfArray($enrolinfo,'offercertificateupdateenrol');
					$certificateupdateobjectinfo=$this->getUtildata()->getVaueOfArray($enrolinfo,'offercertificateupdateobjectinfo');
					$certificateupdateobjectdata=$this->getUtildata()->getVaueOfArray($enrolinfo,'offercertificateupdateobjectdata');
				
				}else if($modulekey=='badiu.ams.enrol.migration.legacy'){
					//get service lagacy
					$badiuSession = $this->getContainer()->get('badiu.system.access.session');
					$legacycertificatekey=$badiuSession->getValue('badiu.ams.enrol.migration.legacy.param.config.certificateservice');
					
					if(!$this->getContainer()->has($legacycertificatekey)){return $this->messageout($source,"Servico de legado para emitir certificado com  chave $legacycertificatekey não existe ");}
					$legacycertificadata=$this->getContainer()->get($legacycertificatekey);
					 
					$fparam=array();
					$checked=$legacycertificadata->checkSatus($param);
					if(!$checked){return $this->messageout($source,'Nao atende requisito para certificao');}
				
					$enrolinfo=$legacycertificadata->getEnrolInfo($param);
					$operation=$this->getUtildata()->getVaueOfArray($this->param, '_operation');
					if($operation!='view'){$userid=$this->getUtildata()->getVaueOfArray($enrolinfo,'userid');}
				}
				$firstname=$this->getUtildata()->getVaueOfArray($enrolinfo,'firstname');
				$lastname=$this->getUtildata()->getVaueOfArray($enrolinfo,'lastname');
				$enablealternatename=$this->getUtildata()->getVaueOfArray($enrolinfo,'enablealternatename');
				$alternatename=$this->getUtildata()->getVaueOfArray($enrolinfo,'alternatename');
				
				$userfullname=$firstname;
				if(!empty($lastname)){$userfullname.=" ".$lastname;}
				
				if(!$enablealternatename){$alternatename="";}
				$userfullnamewithalternatename=$userfullname;
				if(!empty($alternatename)){$userfullnamewithalternatename.= " ($alternatename)";}
				
				$useralternatenamewithfullname=$userfullname;
				if(!empty($alternatename)){$useralternatenamewithfullname= "$alternatename ($userfullname)";}
				
			
				$doctype=$this->getUtildata()->getVaueOfArray($enrolinfo,'doctype');
				$docnumber=$this->getUtildata()->getVaueOfArray($enrolinfo,'docnumber');
				$timestart=$this->getUtildata()->getVaueOfArray($enrolinfo,'timestart');
				if(empty($timestart)){$timestart=$this->getUtildata()->getVaueOfArray($enrolinfo,'timecreated');}
				$timeend=$this->getUtildata()->getVaueOfArray($enrolinfo,'timeend');
				$timefinish=$this->getUtildata()->getVaueOfArray($enrolinfo,'timefinish');
				$disciplinename=$this->getUtildata()->getVaueOfArray($enrolinfo,'disciplinename');
				$disciplinehour=$this->getUtildata()->getVaueOfArray($enrolinfo,'disciplinehour');
				$dhour=$disciplinehour;
				$moduleinstancename=$disciplinename;
				$disciplinehourf=$configformat->defaultHourRound($disciplinehour);
				$enablecertificate=$this->getUtildata()->getVaueOfArray($enrolinfo,'classeenablecertificate');
				$certificatetempleid=$this->getUtildata()->getVaueOfArray($enrolinfo,'classecertificatetempleid');
				$certificatecontent=$this->getUtildata()->getVaueOfArray($enrolinfo,'classecertificatecontent');
		 
				$classetimestart=$this->getUtildata()->getVaueOfArray($enrolinfo,'classetimestart');
				$classetimeend=$this->getUtildata()->getVaueOfArray($enrolinfo,'classetimeend');
				
				$classecertificatetimestart=$this->getUtildata()->getVaueOfArray($enrolinfo,'classecertificatetimestart');
				
				$classecertificatetimeend=$this->getUtildata()->getVaueOfArray($enrolinfo,'classecertificatetimeend');
				
				
					
				if(!empty($classecertificatetimestart)){$classetimestart=$classecertificatetimestart;}
				if(!empty($classecertificatetimeend)){$classetimeend=$classecertificatetimeend;}
				
				
				//for offer
				
				$offername=$this->getUtildata()->getVaueOfArray($enrolinfo,'offername');
				$offerhour=$this->getUtildata()->getVaueOfArray($enrolinfo,'offerhour');
				$offerhourf=$configformat->defaultHourRound($offerhour);
				
				$offerhourcompleted=$this->getUtildata()->getVaueOfArray($enrolinfo,'offerhourcompleted');
				$offerhourcompletedf=$configformat->defaultHourRound($offerhourcompleted);
			
				$offercertificatetimestart=$this->getUtildata()->getVaueOfArray($enrolinfo,'offercertificatetimestart');
				$offercertificatetimeend=$this->getUtildata()->getVaueOfArray($enrolinfo,'offercertificatetimeend');
				$offerid=$this->getUtildata()->getVaueOfArray($enrolinfo,'offerid');
				$offercertificatecontent=$this->getUtildata()->getVaueOfArray($enrolinfo,'offercertificatecontent');
				
				
				
				$offercoursetrial=null;
				if($modulekey=='badiu.ams.enrol.offer'){
					$certificatetempleid=$this->getUtildata()->getVaueOfArray($enrolinfo,'offercertificatetempleid');
					$ctfparam=array('userid'=>$userid,'offerid'=>$offerid);
					$oferdisciplineuser=$this->getContainer()->get('badiu.tms.enrol.offer.lib.disciplineuser');
					$ocllist=$oferdisciplineuser->getDisciplines($ctfparam);
					$offercoursetrial=$oferdisciplineuser->getDisciplinesTableFormat($ocllist);
					
					$dhour=$offerhour;
					$moduleinstancename=$offername;
				}
				
				$offertimestart=null;
				$offertimeend=null;
				if(!empty($offercertificatetimestart)){$offertimestart=$offercertificatetimestart;}
				if(!empty($offercertificatetimeend)){$offertimeend=$offercertificatetimeend;}
				
				if(empty($certificatetempleid)){return $this->messageout($source,"param templateid is required");}
				
				$userdoc=$this->getContainer()->get('badiu.system.user.user.format')->docnumbershowtype($enrolinfo);
				$doctype=$this->getUtildata()->getVaueOfArray($enrolinfo,'doctype');
				if($doctype=='EMAIL'){$userdoc=null;}
		
		
				$param=array();
				$param['modulekey']=$modulekey;
				$param['moduleinstance']=$moduleinstance;
				$param['entity']=$entity;
				$param['templateid']=$certificatetempleid;
				
				$param['coursename']=$disciplinename;
				
				$param['coursehour']=$disciplinehour;
				$param['coursehourf']=$disciplinehourf;
				
				$param['coursetimestart']=$classetimestart;
				$param['coursetimestartf']=$configformat->defaultDate($classetimestart);
				
				$param['coursetimeend']=$classetimeend;
				$param['coursetimeendf']=$configformat->defaultDate($classetimeend);
				
				
				$param['coursecontent']=$certificatecontent;
		
				$param['offername']=$offername;
				$param['offerhour']=$offerhour;
				$param['offerhourf']=$offerhourf;
				$param['offerhourcompleted']=$offerhourcompleted;
				$param['offerhourcompletedf']=$offerhourcompletedf;
				
				$param['offertimestart']=$offertimestart;
				$param['offertimestartf']=$configformat->defaultDate($offertimestart);
				
				$param['offertimeend']=$offertimeend;
				$param['offertimeendf']=$configformat->defaultDate($offertimeend);
				
				$param['offercoursetrial']=$offercoursetrial;
				$param['offercontent']=$offercertificatecontent;
				
				//user
				$param['userfullname']=$userfullname;
				$param['useralternatename']=$alternatename;
				$param['userfullnamewithalternatename']=$userfullnamewithalternatename;
				$param['useralternatenamewithfullname']=$useralternatenamewithfullname;
				$param['userdoc']=$userdoc;
				$param['userid']=$userid;
	
				//enrol
				if(empty($timefinish)){$timefinish=new \DateTime();}
				$param['enroltimestart']=$timestart;
				$param['enroltimestartf']=$configformat->defaultDate($timestart);
				
				$param['enroltimeend']=$timeend;
				$param['enroltimeendf']=$configformat->defaultDate($timeend);
				
				$param['enroltimecompleted']=$timefinish;
				$param['enroltimecompletedf']=$configformat->defaultDate($timefinish);
				
				$param['enrolgrade']=null;
				
				$param['enroltimecompleted']=$this->getContainer()->get('badiu.admin.certificate.certificate.manage.lib')->getNewTimeendRealization($param);
				
				$param['dhour']=$dhour; //to save certificate database
				$param['moduleinstancename']=$moduleinstancename;
			
				$param['certificateupdateuser']=$certificateupdateuser;
				$param['certificateupdateenrol']=$certificateupdateenrol;
				$param['certificateupdateobjectinfo']=$certificateupdateobjectinfo;
				$param['certificateupdateobjectdata']=$certificateupdateobjectdata;
				
				$certificateupdate=0;
				if($certificateupdateuser){$certificateupdate++;}
				if($certificateupdateenrol){$certificateupdate++;}
				if($certificateupdateobjectinfo){$certificateupdate++;}
				if($certificateupdateobjectdata){$certificateupdate++;}
				$param['certificateupdate']=$certificateupdate;
				
				
				$param['skey']=$skey;
				$param['_operation']=$operation;
				if(empty($version)){$version=1;}
				$param['referencekey']=$version;
				
				$param['dtype']='default';
				$param['statushortname']='published';
				$param['timelastprint']=new \DateTime();
				 $operation=$this->getUtildata()->getVaueOfArray($param, '_operation');
				if($operation=='savedraft'){$param['statushortname']='draft';$param['timelastprint']=null;}
				return $param;
         }
		 
		 // /admin/certificate/request/index?_operation=create&modulekey=badiu.ams.enrol.classe&moduleinstance=47&userid=1025
		public function generate() {
		
				$operation=$this->getUtildata()->getVaueOfArray($this->param, '_operation');
				$skey=$this->getUtildata()->getVaueOfArray($this->param, 'skey');
				$source='web';
				if($operation!='create' && $operation!='createnewversion' && $operation!='view' && $operation!='savedraft'){echo "Param _operation not valid";exit;}
				$data=$this->getContainer()->get('badiu.admin.certificate.certificate.data');
				$factorycreate=$this->getContainer()->get('badiu.admin.certificate.factorygenerate');
				
				if($operation=='view'){
					
					if(empty($skey)){return $this->messageout($source,'esta faltando o parametro skey');}
					$data=$this->getContainer()->get('badiu.admin.certificate.certificate.data');
					$certdto=$data->getBySkey($skey);
					
					if(empty($certdto)){return $this->messageout($source,'parametro skey invalido');}
					$this->param['_dbdata']=$certdto;
				
					$this->param['userid']=$this->getUtildata()->getVaueOfArray($certdto, 'userid');
					$this->param['modulekey']=$this->getUtildata()->getVaueOfArray($certdto, 'modulekey');
					$this->param['moduleinstance']=$this->getUtildata()->getVaueOfArray($certdto, 'moduleinstance');
					$this->param['moduleinstancename']=$this->getUtildata()->getVaueOfArray($certdto, 'moduleinstancename');
				}else if($operation=='createnewversion'){
					if(empty($skey)){return $this->messageout($source,'esta faltando o parametro skey');}
					$data=$this->getContainer()->get('badiu.admin.certificate.certificate.data');
					$certdto=$data->getBySkey($skey);
					
					if(empty($certdto)){return $this->messageout($source,'parametro skey invalido');}
					$this->param['_dbdata']=$certdto;
					
				}
				
				$param=$this->createManageParam($this->param);
				if(empty($param) || $param=='failure'){echo "ocorreu uma falha";exit;}
				
				//manage operation
				$param=$this->manageOperationExec($param);
				
				$operationexec=$this->getUtildata()->getVaueOfArray($param,'_operationexec');
				$skey=$this->getUtildata()->getVaueOfArray($param, 'skey');
				if($operationexec=='view' && !empty($skey)){
						$gparam=array('skey'=>$skey);
						$factorycreate->get($gparam);
				}
			
				$certificatetempleid=$this->getUtildata()->getVaueOfArray($param,'templateid');
				
				
				$templatedata=$factorycreate->getTemplatedata($certificatetempleid);
				
				$param=$factorycreate->addTemplatedataToparam($param,$templatedata);
				$this->checkValidate($param);
				$param['_operation']=$operation;
				$param['skey']=$skey;
				if(isset($param['_dbdata'])){unset($param['_dbdata']);}
				$param=$factorycreate->merge($param);
				$cdata=$factorycreate->makeData($param);
				$factorycreate->make($cdata);
				
				exit;
         }
		 public function manageOperationExec($param) {
			 $operationexec='view';
			
			 $skey=$this->getUtildata()->getVaueOfArray($param, 'skey');
			
			  $data=$this->getContainer()->get('badiu.admin.certificate.certificate.data');
			 if(!empty($skey)){
				 $fparam=array('skey'=>$skey,'entity'=>$this->getEntity());
				 $certdto=$data->getBySkey($skey);
				 $param['_dbdata']=$certdto;
			 }
			 
			 $managelib=$this->getContainer()->get('badiu.admin.certificate.certificate.manage.lib');
			 $ckrstatus=$managelib->checkRowStatus($param);
			//print_r($ckrstatus);exit;
			 $operation=$this->getUtildata()->getVaueOfArray($param, '_operation');
			 
			 $statusshortname=$this->getUtildata()->getVaueOfArray($param, '_dbdata.statusshortname',true);
			 $dtype=$this->getUtildata()->getVaueOfArray($param, '_dbdata.dtype',true);
			
			
			 $certificateupdate=$this->getUtildata()->getVaueOfArray($param, 'certificateupdate');

			 if($operation=='view'){
				 if($certificateupdate ==0  &&  $operation=='view'){$operationexec='view';}
				 else if($certificateupdate > 0  &&  $operation=='view'){$operationexec='update';}
			
				 if($statusshortname=='canceled'){$operationexec='view';}
				 else if($statusshortname=='draft'){$operationexec='updatedraft';}
			 }else if($operation=='create'){
				 	if($ckrstatus->status=='new'){$operationexec='new';}
					else if($ckrstatus->status=='requreupdate' || $ckrstatus->status=='requreupdate'){
						$operationexec='update';
						 if($statusshortname=='draft'){$operationexec='updatedraft';}
					}else if($ckrstatus->status=='justexist'){
						$operationexec='update';
						 $param['skey']=$ckrstatus->lastversionskey;
					}
				
			 }
			 else if($operation=='createnewversion'){
				
				 	 $lastversion=$this->getUtildata()->getVaueOfArray($ckrstatus, 'lastversion');
					 $countdtype=$this->getUtildata()->getVaueOfArray($ckrstatus, 'countdtype');
					 $lastversionskey=$this->getUtildata()->getVaueOfArray($ckrstatus, 'lastversionskey');
					 if($skey==$lastversionskey){
						 $operationexec='newversion';
						 $param['referencekey']=$lastversion++;
					}
					 else {
						 $operationexec='view';
						 $param['skey']=$lastversionskey;
					}
				
			 }else if($operation=='savedraft'){
				 if($ckrstatus->status=='new'){$operationexec='new';}
			 }
			
			 $param['_operationexec']=$operationexec;
			
			return $param;
		 }
		/* public function testsavedraft() {
			 $param=array('modulekey'=>'badiu.ams.enrol.offer','moduleinstance'=>96,'userid'=>1015,'_operation'=>'savedraft','_source'=>'api');
			 $this->setParam($param);
			 $result= $this->savedraft();
		 }*/
		public function savedraft() {
				$operation=$this->getUtildata()->getVaueOfArray($this->param, '_operation');
				if($operation!='savedraft'){return null;}
				
				$param=$this->createManageParam($this->param);
				if(empty($param) || $param=='failure'){return null;}
				
				$param=$this->manageOperationExec($param);
				
				$certificatetempleid=$this->getUtildata()->getVaueOfArray($param,'templateid');
				
				$factorycreate=$this->getContainer()->get('badiu.admin.certificate.factorygenerate');
				$templatedata=$factorycreate->getTemplatedata($certificatetempleid);
				$param=$factorycreate->addTemplatedataToparam($param,$templatedata);
				
				$param['_operation']='savedraft';
				$cdata=$factorycreate->makeData($param);
				
				$cresult=$factorycreate->make($cdata);
			
				return $cresult;
				
         }
		/* private function view() {
				$operation=$this->getUtildata()->getVaueOfArray($this->param, '_operation');
				if($operation!='view'){return null;}
				
				$skey=$this->getUtildata()->getVaueOfArray($this->param,'skey');
				if(empty($skey)){echo 'parametro skey invalido';exit;}
				$factorycreate=$this->getContainer()->get('badiu.admin.certificate.factorygenerate');
				$fparam=array('skey'=>$skey);
				$factorycreate->get($fparam);
				exit;
		 }*/
		 
		  private function checkValidate($param) {
			  $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
			  $allpagetextcontent=$this->getUtildata()->getVaueOfArray($param,'allpagetextcontent');
			  $hour=null;
			  $timestart=null;
			  $timeend=null;
			
			  $factorycreatevalidation=$this->getContainer()->get('badiu.admin.certificate.factorygeneratevalidation');
			  if($modulekey=='badiu.ams.enrol.classe'){ 
				  $hour=$this->getUtildata()->getVaueOfArray($param,'coursehour');
				  $timestart=$this->getUtildata()->getVaueOfArray($param,'coursetimestart');
				  $timeend=$this->getUtildata()->getVaueOfArray($param,'coursetimeend');
				  
				  $pos1=stripos($allpagetextcontent, "{COURSE_TIMESTART}");
				  if($pos1!== false){
					  $cparam=array('date'=>$timestart,'type'=>'classeperiodconfigtimestart');
				      $result=$factorycreatevalidation->checkCurrentDate($cparam);
				      if(!empty($result)){$factorycreatevalidation->showError($result);}
				  }
				  
				  $pos2=stripos($allpagetextcontent, "{COURSE_TIMEEND}");
				  if($pos2!== false){
					  $cparam=array('date'=>$timeend,'type'=>'classeperiodconfigtimeend');
				      $result=$factorycreatevalidation->checkCurrentDate($cparam);
				      if(!empty($result)){$factorycreatevalidation->showError($result);}
				  }
				 
				 $pos3=stripos($allpagetextcontent, "{COURSE_HOUR}");
				 
				if($pos1!== false && $pos2!== false && $pos3!== false){				 
				  $cparam=array('hour'=>$hour,'timestart'=>$timestart,'timeend'=>$timeend,'type'=>'classeperiodconfig','infonextdate'=>false);
				  $result=$factorycreatevalidation->hourTimeRealization($cparam);
				  if(!empty($result)){$factorycreatevalidation->showError($result);}
				} 
				  //enrol period 
			  }else if($modulekey=='badiu.ams.enrol.offer'){
				   $hour=$this->getUtildata()->getVaueOfArray($param,'offerhour');
				   $timestart=$this->getUtildata()->getVaueOfArray($param,'offertimestart');
				   $timeend=$this->getUtildata()->getVaueOfArray($param,'offertimeend');
				   
				    $pos1=stripos($allpagetextcontent, "{TRAIL_TIMESTART}");
				  if($pos1!== false){
					  $cparam=array('date'=>$timestart,'type'=>'offerperiodconfigtimestart');
				      $result=$factorycreatevalidation->checkCurrentDate($cparam);
				      if(!empty($result)){$factorycreatevalidation->showError($result);}
				  }
				  
				  $pos2=stripos($allpagetextcontent, "{TRAIL_TIMEEND}");
				  if($pos2!== false){
					  $cparam=array('date'=>$timeend,'type'=>'offerperiodconfigtimeend');
				      $result=$factorycreatevalidation->checkCurrentDate($cparam);
				      if(!empty($result)){$factorycreatevalidation->showError($result);}
				  }
				 
				 $pos3=stripos($allpagetextcontent, "{TRAIL_HOUR}");
				 
				if($pos1!== false && $pos2!== false && $pos3!== false){				 
				  $cparam=array('hour'=>$hour,'timestart'=>$timestart,'timeend'=>$timeend,'type'=>'offerperiodconfig','infonextdate'=>false);
				  $result=$factorycreatevalidation->hourTimeRealization($cparam);
				  if(!empty($result)){$factorycreatevalidation->showError($result);}
				} 
			
			 }
			  //enrol
			  $timestart=$this->getUtildata()->getVaueOfArray($param,'enroltimestart');
			  $timeend=$this->getUtildata()->getVaueOfArray($param,'enroltimeend');
			  $timecompleted=$this->getUtildata()->getVaueOfArray($param,'timecompleted');
			  
			   $pos1=stripos($allpagetextcontent, "{ENROL_TIMESTART}");
				  if($pos1!== false){
					  $cparam=array('date'=>$timestart,'type'=>'enrolperiodconfigtimestart');
				      $result=$factorycreatevalidation->checkCurrentDate($cparam);
				      if(!empty($result)){$factorycreatevalidation->showError($result);}
				  }
				  
				  $pos2=stripos($allpagetextcontent, "{ENROL_TIMEEND}");
				  if($pos2!== false){
					  $cparam=array('date'=>$timeend,'type'=>'enrolperiodconfigtimeend');
				      $result=$factorycreatevalidation->checkCurrentDate($cparam);
				      if(!empty($result)){$factorycreatevalidation->showError($result);}
				  }
				 
				$hourxepression="{COURSE_HOUR}";
				if($modulekey=='badiu.ams.enrol.offer'){/*$hourxepression="{TRAIL_HOUR}";*/ $hourxepression="{TRAIL_HOUR_COMPLETED}";}
				 $pos3=stripos($allpagetextcontent, $hourxepression);
				 
				if($pos1!== false && $pos2!== false && $pos3!== false){				 
				  $cparam=array('hour'=>$hour,'timestart'=>$timestart,'timeend'=>$timeend,'type'=>'enrolperiodconfig','infonextdate'=>false);
				  $result=$factorycreatevalidation->hourTimeRealization($cparam);
				  if(!empty($result)){$factorycreatevalidation->showError($result);}
				} 
				
				$pos4=stripos($allpagetextcontent, "{ENROL_TIMECOMPLETED}");
				 if($pos4!== false){
					  $cparam=array('date'=>$timecompleted,'type'=>'enroltimecompleted');
				      $result=$factorycreatevalidation->checkCurrentDate($cparam);
				      if(!empty($result)){$factorycreatevalidation->showError($result);}
				  }
				  
			  
			  return null ;
				
		  }


   function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }

}
