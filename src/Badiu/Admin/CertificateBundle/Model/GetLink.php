<?php

 namespace Badiu\Admin\CertificateBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class GetLink extends BadiuModelLib{

    function __construct(Container $container) {
            parent::__construct($container);
		  }
    
        
        public function exec() {
				$this->view();
				return null;
         }
       
        
		 
		 private function view() {
				$skey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
				if(empty($skey)){echo 'parametro skey invalido';exit;}
				$requestlink=$this->getContainer()->get('badiu.admin.certificate.request.link');
				$fparam=array('skey'=>$skey,'_operation'=>'view');
				$requestlink->setParam($fparam);
				$requestlink->generate();
				exit;
		 }
		 
}
