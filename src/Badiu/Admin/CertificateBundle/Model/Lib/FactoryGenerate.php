<?php

namespace Badiu\Admin\CertificateBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryGenerate extends BadiuModelLib {

	
     function __construct(Container $container) {
        parent::__construct($container);
	} 
 
	public function make($param) {
	
		$operation=$this->getUtildata()->getVaueOfArray($param, '_operation');
		$tempathdir=$this->makeTempFolder();
		$filename=$this->makeFilename($param);
		
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L','tempDir' =>$tempathdir]);
		$firstpagehead=$this->getUtildata()->getVaueOfArray($param,'firstpagehead');
		$p1footer=$this->getUtildata()->getVaueOfArray($param,'firstpagefooter');
		$page1=$this->makeContetFirstPage($param);
		
		$secondpagecontent=$this->getUtildata()->getVaueOfArray($param,'secondpagecontent');
		$secondpagehead=$this->getUtildata()->getVaueOfArray($param,'secondpagehead');
		$p2footer=$this->getUtildata()->getVaueOfArray($param,'secondpagefooter');
		$page2=$this->makeContetSecondPage($param);
		
		$param['content']['page1']['head']=$firstpagehead;
		$param['content']['page1']['body']=$page1;
		$param['content']['page1']['footer']=$p1footer;
		
		$param['content']['page2']['head']=$secondpagehead;
		$param['content']['page2']['body']=$page2;
		$param['content']['page2']['footer']=$p2footer;
		
		
		
		$cparam=$this->castDataToDbCertificate($param);
		$managelib=$this->getContainer()->get('badiu.admin.certificate.certificate.manage.lib');
		
		$skey=$this->getUtildata()->getVaueOfArray($param,'skey');
		$managelib->updateTimelastprint($skey);
		
		$certresult=null;
		if($operation!='viewtemplate'){$certresult=$managelib->add($cparam);}
		if($operation=='savedraft'){return $certresult;}
		
		$firstpagehead = mb_convert_encoding($firstpagehead, 'UTF-8', 'UTF-8');
		$page1 = mb_convert_encoding($page1, 'UTF-8', 'UTF-8');
		$p1footer = mb_convert_encoding($p1footer, 'UTF-8', 'UTF-8');
		$page2 = mb_convert_encoding($page2, 'UTF-8', 'UTF-8');
		$p2footer = mb_convert_encoding($p2footer, 'UTF-8', 'UTF-8');   
		
		$mpdf->SetTitle($this->getUtildata()->getVaueOfArray($param,'moduleinstancename'));
		$mpdf->SetHTMLHeader($firstpagehead);
		$mpdf->WriteHTML($page1);
		$mpdf->SetHTMLFooter($p1footer);
		if(!empty($secondpagecontent)){
			$mpdf->AddPage();
			$mpdf->WriteHTML($page2); 
			$mpdf->SetHTMLFooter($p2footer);
		}
		
		$mpdf->Output($filename, 'I');
		exit;
	}
	public function makeTempFolder() { 
		$defaultpath =  $this->getContainer()->getParameter('badiu.system.file.defaultpath');
		$basepath=$defaultpath.'/temp/'.$this->getEntity().'/certificate';
		if (!file_exists($basepath)) {
			mkdir($basepath, 0777, true);
		}
		return $basepath;
	}
	public function get($param) { 
		$data=$this->getContainer()->get('badiu.admin.certificate.certificate.data');
		$managelib=$this->getContainer()->get('badiu.admin.certificate.certificate.manage.lib');
		
		$skey=$this->getUtildata()->getVaueOfArray($param,'skey');
		$managelib->updateTimelastprint($skey);
		
		$fparam=array('skey'=>$skey);
		
		$content=$data->getGlobalColumnValue('content',$fparam); 
		$content= $this->getJson()->decode($content, true);
      
		$firstpagehead=$this->getUtildata()->getVaueOfArray($content,'content.page1.head',true);
		$page1=$this->getUtildata()->getVaueOfArray($content,'content.page1.body',true);
		$p1footer=$this->getUtildata()->getVaueOfArray($content,'content.page1.footer',true);
		
		$secondpagehead=$this->getUtildata()->getVaueOfArray($content,'content.page2.head',true);
		$page2=$this->getUtildata()->getVaueOfArray($content,'content.page2.body',true);
		$p2footer=$this->getUtildata()->getVaueOfArray($content,'content.page2.footer',true);
		
		$tempathdir=$this->makeTempFolder();
		$filename=$this->makeFilename($param);
		
		$mpdf = new \Mpdf\Mpdf(['orientation' => 'L','tempDir' =>$tempathdir]);
		$mpdf->SetTitle($this->getUtildata()->getVaueOfArray($param,'moduleinstancename'));
		$mpdf->SetHTMLHeader($firstpagehead);
		$mpdf->WriteHTML($page1);
		$mpdf->SetHTMLFooter($p1footer);
		if(!empty($page2)){
			$mpdf->AddPage();
			$mpdf->WriteHTML($page2); 
			$mpdf->SetHTMLFooter($p2footer);
		}
		$mpdf->Output($filename, 'I');
		
		exit;
	}
	
	public function getTemplatedata($templateid) {
		if(empty($templateid)){return null;}
		$data=$this->getContainer()->get('badiu.admin.certificate.template.data');
		$param=array('id'=>$templateid,'entity'=>$this->getEntity());
		$result=$data->getGlobalColumnsValue('o.firstpagehead,o.firstpagecontent,o.firstpageimgbackgroud,o.firstpagefooter,o.secondpagehead,o.secondpagecontent,o.secondpageimgbackgroud,o.secondpagefooter,o.customformat',$param);
		return $result;
	}
	public function addTemplatedataToparam($param,$templatedata) {
		$baseurl=$this->getContainer()->get('badiu.system.core.lib.util.app')->getBaseUrl();
		$fullurlget= $baseurl."/system/file/get/";
		
		$customformat=$this->getUtildata()->getVaueOfArray($templatedata,'customformat');
		$firstpagehead=$this->getUtildata()->getVaueOfArray($templatedata,'firstpagehead');
		$firstpagecontent=$customformat.$this->getUtildata()->getVaueOfArray($templatedata,'firstpagecontent');
		$firstpageimgbackgroud=$this->getUtildata()->getVaueOfArray($templatedata,'firstpageimgbackgroud');
		if(!empty($firstpageimgbackgroud)){$firstpageimgbackgroud=$fullurlget.$firstpageimgbackgroud;}
		
		$firstpagefooter=$this->getUtildata()->getVaueOfArray($templatedata,'firstpagefooter');
		
		$secondpagehead=$this->getUtildata()->getVaueOfArray($templatedata,'secondpagehead');
		$secondpagecontent=$customformat.$this->getUtildata()->getVaueOfArray($templatedata,'secondpagecontent');
		$secondpageimgbackgroud=$this->getUtildata()->getVaueOfArray($templatedata,'secondpageimgbackgroud');
		if(!empty($secondpageimgbackgroud)){$secondpageimgbackgroud=$fullurlget.$secondpageimgbackgroud;}
		
		$secondpagefooter=$this->getUtildata()->getVaueOfArray($templatedata,'secondpagefooter');
		
		
		$allpagetextcontent=$firstpagehead.$firstpagecontent.$firstpagefooter.$secondpagehead.$secondpagecontent.$secondpagefooter;
		
		$param['firstpagehead']=$customformat;
		
		$param['firstpagehead']=$firstpagehead;
		$param['firstpagecontent']=$firstpagecontent;
		$param['firstpageimgbackgroud']=$firstpageimgbackgroud;
		$param['firstpagefooter']=$firstpagefooter;
		
		//secondpage
		$param['secondpagehead']=$secondpagehead;
		$param['secondpagecontent']=$secondpagecontent;
		$param['secondpageimgbackgroud']=$secondpageimgbackgroud;
		$param['secondpagefooter']=$secondpagefooter;
		
		$param['allpagetextcontent']=$allpagetextcontent;
		
		
		return $param;
	}
	
	public function makeData($fcparam) {
		
		$skey=$this->getUtildata()->getVaueOfArray($fcparam,'skey');
		$operationexec=$this->getUtildata()->getVaueOfArray($fcparam,'_operationexec');
		$baseurl=$this->getContainer()->get('badiu.system.core.lib.util.app')->getBaseUrl();
		$configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
		
		$factoryexpression=$this->getContainer()->get('badiu.admin.certificate.factoryexpression');
		
		$cdata=array();
		
		
		//identify
		$managelib=$this->getContainer()->get('badiu.admin.certificate.certificate.manage.lib');
		$identifycode=$managelib->createIdentify($fcparam);
		
		$tmpdategenerate=time();

		if(!empty($skey) && $operationexec!='newversion'){
				$modulekey=$this->getUtildata()->getVaueOfArray($fcparam,'modulekey');
				$moduleinstance=$this->getUtildata()->getVaueOfArray($fcparam,'moduleinstance');
				$userid=$this->getUtildata()->getVaueOfArray($fcparam,'userid');
				$certificatedata=$this->getContainer()->get('badiu.admin.certificate.certificate.data');
				$paramfilter=array('skey'=>$skey,'entity'=>$this->getEntity(),'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'userid'=>$userid);
				$tmpdategenerate=$certificatedata->getGlobalColumnValue('timecreated',$paramfilter); 
				if(!empty($tmpdategenerate)){$tmpdategenerate=$tmpdategenerate->getTimestamp();}
				$identifycode=$skey;
				
				//for trial
				
				//update content
				
				//update dhour
		}
		
		
		$dategenerate=strftime('%d de %B de %Y', $tmpdategenerate);  
		
		$fcparam['identifycode']=$identifycode;
		$fcparam['identifysiteurl']="https://www.badiu.com.br/validate/carteificate";
		$fcparam['identifydategenerate']=$dategenerate;
		
		$timelastprintf=$this->getUtildata()->getVaueOfArray($fcparam,'timelastprint');
		if(!empty($timelastprintf)){$timelastprintf=strftime('%d de %B de %Y', $timelastprintf->getTimestamp());}
		$fcparam['timelastprintf']=$timelastprintf;
		
		$lparam=array('skey'=>$identifycode,'_hideffilter'=>1);
		$codbartext= $this->getUtilapp()->getUrlByRoute('badiu.admin.certificate.consult.add',$lparam);
		$codbartext=urlencode($codbartext);
		$codbartext=urlencode($codbartext);
	
		
		$fcparam['identifyqrcode']="<img src=\"$baseurl/system/core/qrcode/create/index?d=$codbartext&e=L&s=4&t=P\" style=\"width:110px;height:110px;\">";
		
		
		//expression
		
		//first page
		$factoryexpression->init($fcparam['firstpagehead'],$fcparam);
		$factoryexpression->replace();
       $fcparam['firstpagehead']=$factoryexpression->getMessage();
		
		$factoryexpression->init($fcparam['firstpagecontent'],$fcparam);
		$factoryexpression->replace();
       $fcparam['firstpagecontent']=$factoryexpression->getMessage();
		
		$factoryexpression->init($fcparam['firstpagefooter'],$fcparam);
		$factoryexpression->replace();
       $fcparam['firstpagefooter']=$factoryexpression->getMessage();
		
		//second page
		$factoryexpression->init($fcparam['secondpagehead'],$fcparam);
		$factoryexpression->replace();
       $fcparam['secondpagehead']=$factoryexpression->getMessage();
		
		$factoryexpression->init($fcparam['secondpagecontent'],$fcparam);
		$factoryexpression->replace();
       $fcparam['secondpagecontent']=$factoryexpression->getMessage();
		
		$factoryexpression->init($fcparam['secondpagefooter'],$fcparam);
		$factoryexpression->replace();
       $fcparam['secondpagefooter']=$factoryexpression->getMessage();
		
		return $fcparam;
	}

	

	public function makeContetFirstPage($param) {
	
		//firstpage
		$firstpagecontent=$this->getUtildata()->getVaueOfArray($param,'firstpagecontent');
		$firstpageimgbackgroud=$this->getUtildata()->getVaueOfArray($param,'firstpageimgbackgroud');
		
		$html="
		<style>
			@page :first{
				background: url(\"$firstpageimgbackgroud\") no-repeat 0 0;
				background-image-resize: 6;  	
			}
		</style>
		<body>
		$firstpagecontent
		</body>
		";
		
		return $html;
		}

	public function makeContetSecondPage($param) {
	
		//second page
		$secondpagecontent=$this->getUtildata()->getVaueOfArray($param,'secondpagecontent');
		$secondpageimgbackgroud=$this->getUtildata()->getVaueOfArray($param,'secondpageimgbackgroud');
		 
		
		$html="
		<style>
		body {
				background-image: url(\"$secondpageimgbackgroud\") no-repeat 0 0; 
				background-image-resize: 6;  
			}
			
		</style>
		<body>
		$secondpagecontent
		</body>
		";
		
		return $html;
		}
		
		public function merge($param) {
			$certificatedata=$this->getContainer()->get('badiu.admin.certificate.certificate.data');
			//count by skey
			$operationexec=$this->getUtildata()->getVaueOfArray($param,'_operationexec');
			if($operationexec=='view'){return $param; }
			
			$dbparam=null;
			$skey=$this->getUtildata()->getVaueOfArray($param,'skey');
			$entity=$this->getEntity();
			$fparam=array('skey'=>$skey,'entity'=>$entity);
			$exist=$certificatedata->countGlobalRow($fparam);
			
			if($exist){
				$dconfig=$certificatedata->getGlobalColumnValue('dconfig',$fparam);
				$dbparam= $this->getJson()->decode($dconfig, true);
			}else{
				$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
				$modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
				$moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
				//"entity","userid","modulekey","moduleinstance","dtype","referencekey"
				
				$fparam=array('entity'=>$entity,'userid'=>$userid,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'dtype'=>'default');
				$exist=$certificatedata->countGlobalRow($fparam);
				if($exist){
					$dconfig=$certificatedata->getGlobalColumnValue('dconfig',$fparam);
					$dbparam= $this->getJson()->decode($dconfig, true);
				}else{ return $param;}
			}
			//count by param
			
			if(empty($dbparam)){return $param;}
			
			$certificateupdateuser=$this->getUtildata()->getVaueOfArray($param,'certificateupdateuser');
			$certificateupdateenrol=$this->getUtildata()->getVaueOfArray($param,'certificateupdateenrol');
			$certificateupdateobjectinfo=$this->getUtildata()->getVaueOfArray($param,'certificateupdateobjectinfo');
			$certificateupdateobjectdata=$this->getUtildata()->getVaueOfArray($param,'certificateupdateobjectdata');
			
			//if($certificateupdateuser && $certificateupdateenrol && $certificateupdateobjectinfo && $certificateupdateobjectdata){$param['_forceupdate']=0;return $param;}
			//else {$param['_forceupdate']=1;}
			//user
			if(!$certificateupdateuser){ 
				$param['userfullname']=$this->getUtildata()->getVaueOfArray($dbparam,'userfullname');
				$param['useralternatename']=$this->getUtildata()->getVaueOfArray($dbparam,'useralternatename');
				$param['userfullnamewithalternatename']=$this->getUtildata()->getVaueOfArray($dbparam,'userfullnamewithalternatename');
				$param['useralternatenamewithfullname']=$this->getUtildata()->getVaueOfArray($dbparam,'useralternatenamewithfullname');
				$param['userdoc']=$this->getUtildata()->getVaueOfArray($dbparam,'userdoc');
				$param['userid']=$this->getUtildata()->getVaueOfArray($dbparam,'userid');
			}
			
			if(!$certificateupdateenrol){
				$param['enroltimestart']=$this->getUtildata()->getVaueOfArray($dbparam,'enroltimestart');
				$param['enroltimestartf']=$this->getUtildata()->getVaueOfArray($dbparam,'enroltimestartf');
				$param['enroltimeend']=$this->getUtildata()->getVaueOfArray($dbparam,'enroltimeend');
				$param['enroltimeendf']=$this->getUtildata()->getVaueOfArray($dbparam,'enroltimeendf');
				$param['enroltimecompleted']=$this->getUtildata()->getVaueOfArray($dbparam,'enroltimecompleted');
				$param['enroltimecompletedf']=$this->getUtildata()->getVaueOfArray($dbparam,'enroltimecompletedf');
				$param['enrolgrade']=$this->getUtildata()->getVaueOfArray($dbparam,'enrolgrade');
				$param['enroltimecompleted']=$this->getUtildata()->getVaueOfArray($dbparam,'enroltimecompleted');
				$param['dhour']=$this->getUtildata()->getVaueOfArray($dbparam,'dhour');
				
			}
			if(!$certificateupdateobjectinfo){
				
				$param['templateid']=$this->getUtildata()->getVaueOfArray($dbparam,'templateid');
				$param['coursename']=$this->getUtildata()->getVaueOfArray($dbparam,'coursename');
				$param['coursehour']=$this->getUtildata()->getVaueOfArray($dbparam,'coursehour');
				$param['coursehourf']=$this->getUtildata()->getVaueOfArray($dbparam,'coursehourf');
				$param['coursetimestart']=$this->getUtildata()->getVaueOfArray($dbparam,'coursetimestart');
				$param['coursetimestartf']=$this->getUtildata()->getVaueOfArray($dbparam,'coursetimestartf');
				$param['coursetimeend']=$this->getUtildata()->getVaueOfArray($dbparam,'coursetimeend');
				$param['coursetimeendf']=$this->getUtildata()->getVaueOfArray($dbparam,'coursetimeendf');
				
		
				$param['offername']=$this->getUtildata()->getVaueOfArray($dbparam,'offername');
				//$param['offerhour']=$this->getUtildata()->getVaueOfArray($dbparam,'offerhour');
				//$param['offerhourf']=$this->getUtildata()->getVaueOfArray($dbparam,'offerhourf');
				$param['offerhourcompleted']=$this->getUtildata()->getVaueOfArray($dbparam,'offerhourcompleted');
				$param['offerhourcompletedf']=$this->getUtildata()->getVaueOfArray($dbparam,'offerhourcompletedf');
				$param['offertimestart']=$this->getUtildata()->getVaueOfArray($dbparam,'offertimestart');
				$param['offertimestartf']=$this->getUtildata()->getVaueOfArray($dbparam,'offertimestartf');
				
				$param['offertimeend']=$this->getUtildata()->getVaueOfArray($dbparam,'offertimeend');
				$param['offertimeendf']=$this->getUtildata()->getVaueOfArray($dbparam,'offertimeendf');
				
				/*echo $param['coursename'];
				echo "<hr />";
				echo $param['coursehour'];
				exit;*/
				
			}
			if(!$certificateupdateobjectdata){
				
				$param['offerhour']=$this->getUtildata()->getVaueOfArray($dbparam,'offerhour');
				$param['offerhourf']=$this->getUtildata()->getVaueOfArray($dbparam,'offerhourf');
				
				$param['coursecontent']=$this->getUtildata()->getVaueOfArray($dbparam,'coursecontent');
				
				$param['offercoursetrial']=$this->getUtildata()->getVaueOfArray($dbparam,'offercoursetrial');
				$param['offercontent']=$this->getUtildata()->getVaueOfArray($dbparam,'offercontent');
			}
			
			return $param;
		}
		public function castDataToDbCertificate($param) {
		$operationexec=$this->getUtildata()->getVaueOfArray($param,'_operationexec');
		 $certparam=array();
		 $certparam['modulekey']=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $certparam['moduleinstance']=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $certparam['entity']=$this->getUtildata()->getVaueOfArray($param,'entity');
		 $certparam['templateid']=$this->getUtildata()->getVaueOfArray($param,'templateid');
		 $certparam['dhour']=$this->getUtildata()->getVaueOfArray($param,'dhour');
		 $certparam['moduleinstancename']=$this->getUtildata()->getVaueOfArray($param,'moduleinstancename');
		 $skey=$this->getUtildata()->getVaueOfArray($param,'skey');
		
		 if(empty($skey)){$skey=$this->getUtildata()->getVaueOfArray($param,'identifycode');}
		 else if(!empty($skey) && $operationexec=='newversion'){$skey=$this->getUtildata()->getVaueOfArray($param,'identifycode');}
		 $certparam['skey']=$skey;
		 
		 $content=array('content'=>$this->getUtildata()->getVaueOfArray($param,'content'));
		 $content=$this->getJson()->encode($content);
		 $dconfig=$this->getJson()->encode($param);
		 $certparam['dconfig']=$dconfig;
		
		 $certparam['referencekey']=$this->getUtildata()->getVaueOfArray($param,'referencekey');
		 $certparam['content']=$content;
		 $certparam['dtype']=$this->getUtildata()->getVaueOfArray($param,'dtype');
		 $certparam['userid']=$this->getUtildata()->getVaueOfArray($param,'userid');
		 $certparam['statushortname']=$this->getUtildata()->getVaueOfArray($param,'statushortname');
		 
		 $certparam['_operationexec']=$this->getUtildata()->getVaueOfArray($param, '_operationexec');
		 $certparam['_operation']=$this->getUtildata()->getVaueOfArray($param, '_operation');
		 return $certparam;
		}
		
		
	public function makeFilename($param) {
		$name=$this->getUtildata()->getVaueOfArray($param,'moduleinstancename');
		if(empty($name)){$name="badiu_admin_certificate_".time()."_.pdf";}
		else{ $name=$this->getUtildata()->removeSpecialChar($name);$name.=".pdf";}
		
        return $name;
    }
	
	
}

