<?php

namespace Badiu\Admin\CertificateBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSchedulerExpression;

class FactoryExpression extends BadiuSchedulerExpression {
 
	  function __construct(Container $container) {
        parent::__construct($container);
     
    } 
	
	public function initTblexpresion(){
     $this->add('coursename','COURSE_NAME');
     $this->add('coursehourf','COURSE_HOUR');
     $this->add('coursetimestartf','COURSE_TIMESTART');
     $this->add('coursetimeendf','COURSE_TIMEEND');
     $this->add('coursecontent','COURSE_CONTENT');
    
	 $this->add('offername','TRAIL_NAME');
     $this->add('offerhourf','TRAIL_HOUR');
	  $this->add('offerhourcompletedf','TRAIL_HOUR_COMPLETED');
     $this->add('offertimestartf','TRAIL_TIMESTART');
     $this->add('offertimeendf','TRAIL_TIMEEND');
     $this->add('offercontent','TRAIL_CONTENT');
	 $this->add('offercoursetrial','TRAIL_COURSE');
	 
	 $this->add('userfullname','USER_FULLNAME'); 
	 $this->add('useralternatename','USER_ALTERNATENAME'); 
	 $this->add('userfullnamewithalternatename','USER_FULLNAME_WITH_ALTERNATENAME'); 
	 $this->add('useralternatenamewithfullname','USER_ALTERNATENAME_WITH_FULLNAME'); 
     $this->add('userdoc','USER_DOC');
	 
     $this->add('enroltimestartf','ENROL_TIMESTART');
     $this->add('enroltimeendf','ENROL_TIMEEND');
     $this->add('enroltimecompletedf','ENROL_TIMECOMPLETED');
     $this->add('enrolgrade','ENROL_GRADE');
	 
	 $this->add('identifycode','IDENTIFY_CODE');
	 $this->add('identifyqrcode','IDENTIFY_QRCODE');
	 $this->add('identifysiteurl','IDENTIFY_SITEURL');
	 $this->add('identifydategenerate','IDENTIFY_DATEGENERATE');
	  $this->add('timelastprintf','IDENTIFY_DATELASTPRINT');
  }

}

