<?php

namespace Badiu\Admin\CertificateBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class CertificateManagerLib extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function add($param) {
		
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 $templateid=$this->getUtildata()->getVaueOfArray($param,'templateid');
		 $dhour=$this->getUtildata()->getVaueOfArray($param,'dhour');
		 $moduleinstancename=$this->getUtildata()->getVaueOfArray($param,'moduleinstancename');
		 $skey=$this->getUtildata()->getVaueOfArray($param,'skey');
		 $content=$this->getUtildata()->getVaueOfArray($param,'content');
		 $dconfig=$this->getUtildata()->getVaueOfArray($param,'dconfig');
		 $dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 $referencekey=$this->getUtildata()->getVaueOfArray($param,'referencekey');
		 $statushortname=$this->getUtildata()->getVaueOfArray($param,'statushortname');
		 $forceupdate=$this->getUtildata()->getVaueOfArray($param,'_forceupdate');
		 $operation=$this->getUtildata()->getVaueOfArray($param,'_operation');
		 $operationexec=$this->getUtildata()->getVaueOfArray($param,'_operationexec');
		  
		 if(empty($entity)){$entity=$this->getEntity();}
		 $statusid=$this->getContainer()->get('badiu.admin.certificate.status.data')->getIdByShortname($entity,$statushortname); 
		 
		 $data=$this->getContainer()->get('badiu.admin.certificate.certificate.data');
		 
		 $faparam=array();
		 $faparam['modulekey']=$modulekey;
		 $faparam['moduleinstance']=$moduleinstance;
		 $faparam['moduleinstancename']=null;
		  $faparam['entity']=$entity;
		  $faparam['templateid']=$templateid;
		  $faparam['dhour']=$dhour;
		  $faparam['moduleinstancename']=$moduleinstancename;
		  $faparam['skey']=$skey;
		  $faparam['content']=$content;
		  $faparam['dconfig']=$dconfig;
		  $faparam['dtype']=$dtype;
		  $faparam['userid']=$userid;
		  $faparam['statusid']=$statusid;
		  $faparam['referencekey']=$referencekey;
		  $faparam['timecreated']=new \DateTime();
		  $faparam['timegenerated']=new \DateTime();
		  $faparam['timelastprint']=new \DateTime();
		   $result=null;
		   
		  //check 
		 
		  //$ckrstatus=$this->checkRowStatus($param);
		 
		  if($operationexec=='new'){
			  if($operation=='savedraft'){unset($faparam['timegenerated']);unset($faparam['timelastprint']);}
			   $faparam['deleted']=0; 
			   $result=$data->insertNativeSql($faparam,false);
			   return $result;
		  }else if($operationexec=='newversion'){
			  
			  //update dtype
			  $fparam=array('modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'userid'=>$userid,'entity'=>$entity);
			  $list=$data->getGlobalColumnValues('id',$fparam);
			  if(is_array($list)){
				  foreach ($list as $row) {
					  $id=$this->getUtildata()->getVaueOfArray($row,'id');
					  $apparam=array('id'=>$id,'dtype'=>'old');
					  $data->updateNativeSql($apparam,false);
				  }
			  }
			  //insert new
			   $faparam['deleted']=0; 
			   $result=$data->insertNativeSql($faparam,false);
			   return $result;
			   
		  }else if($operationexec=='update' || $operationexec=='updatedraft'){
			   $fparam=array('skey'=>$skey,'userid'=>$userid);
			   $id=$data->getGlobalColumnValue('id',$fparam);
			   if(empty($id)){echo "id do certificado nao localizado";exit;}
			   unset($faparam['skey']);
			   unset($faparam['timecreated']);
			   if($operationexec=='update'){ unset($faparam['statusid']);}
			   unset($faparam['userid']);
			   unset($faparam['modulekey']);
			   unset($faparam['moduleinstance']);
			   unset($faparam['dtype']);
			   unset($faparam['referencekey']);
			   if($operationexec=='update'){unset($faparam['timegenerated']);}
			   if($operationexec=='updatedraft'){$faparam['timegenerated']=new \DateTime();unset($faparam['timelastprint']);}
			    $faparam['timemodified']=new \DateTime();
				$faparam['id']=$id;
			   //$iparam=array('id'=>$id,'content'=>$content,'dhour'=>$dhour,'templateid'=>$templateid,'dhour'=>$dhour,'timemodified'=>new \DateTime());
			   $result=$data->updateNativeSql($faparam,false);
		   }
		
        return $result; 
	}
	
	 public function checkRowStatus($param) {
		 $modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 $operation=$this->getUtildata()->getVaueOfArray($param,'_operation');
		 $version=$this->getUtildata()->getVaueOfArray($param,'version');
		 $skey=$this->getUtildata()->getVaueOfArray($param,'skey');
		 $version=$this->getUtildata()->getVaueOfArray($param,'referencekey');  
		 $data=$this->getContainer()->get('badiu.admin.certificate.certificate.data');
		 
		 $dto=new \stdClass();
		 //check by modulekey and moduleinstance
		 $fparam=array('modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'userid'=>$userid,'entity'=>$entity);
		 $dto->countmoduleintance=$data->countGlobalRow($fparam);
		 //check by skey
		 $dto->countskey=0;
		 if(!empty($skey)){
			  $fparam=array('skey'=>$skey);
			 $dto->countskey=$data->countGlobalRow($fparam);
		 }
		 
		//check last version
		$fparam=array('modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'userid'=>$userid,'entity'=>$entity);
		$dto->lastversion=$data->getMaxGlobalColumnValue('referencekey',$fparam);
		
		//get skey of lastversion
		$dto->lastversionskey=null;
		if(!empty($dto->lastversion)){
			$fparam=array('modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'userid'=>$userid,'entity'=>$entity,'dtype'=>'default','referencekey'=>$dto->lastversion);
			$dto->lastversionskey=$data->getGlobalColumnValue('skey',$fparam);
		}
		 
		//ckeck by dtype
		$fparam=array('modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'userid'=>$userid,'entity'=>$entity,'dtype'=>'default');
		$dto->countdtype=$data->countGlobalRow($fparam);
		
		//new 
		$dto->status='none';
		if( $dto->countmoduleintance==0 && $dto->countskey==0 && empty($dto->lastversion) && $dto->countdtype==0 ){$dto->status='new';}
		else  if($dto->countmoduleintance==1 && $dto->countskey==0 && !empty($dto->lastversion) &&  $dto->countdtype==1){$dto->status='justexist';}
		else  if($dto->countmoduleintance==1 && $dto->countskey==1 && !empty($dto->lastversion) &&  $dto->countdtype==1){$dto->status='requreupdate';}
		else  if($dto->countmoduleintance >= 1 && $dto->countskey >=0 && $dto->lastversion  <  $version  &&  $dto->countdtype==1){$dto->status='newversion';}
		else  if($dto->countmoduleintance >= 1 && $dto->countskey >=0 && $dto->lastversion  ==  $version  &&  $dto->countdtype==1){$dto->status='justexistnewversion';}
		else  if($dto->countdtype > 1){$dto->status='duplicateversiondtypedefult';}
		 
		return $dto;
	 }
	 public function createIdentify($param) {
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		 $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
		 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		 
		 $hash=$this->getContainer()->get('badiu.system.core.lib.util.hash');
		 $token=$hash->make(30);
		 
		 $idenf="$moduleinstance.$userid-";
		 $sizet=30-strlen($idenf);
		 if( $sizet > 0){
			 $idenf.=$hash->make($sizet);
		 }
		$idenf=strtoupper($idenf);
		return $idenf;
	 }
	 
	 public function getNewTimeendRealization($param) {
		$hour=null;
		$modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		
		if($modulekey=='badiu.ams.enrol.classe'){$hour=$this->getUtildata()->getVaueOfArray($param,'coursehour'); }
		else if($modulekey=='badiu.ams.enrol.offer'){$hour=$this->getUtildata()->getVaueOfArray($param,'offerhour');}
				 
		$timestart=$this->getUtildata()->getVaueOfArray($param,'enroltimestart');
		$timeend=$this->getUtildata()->getVaueOfArray($param,'enroltimecompleted');
		
		if (empty($hour)) {return $timeend;}
		
		if (!is_a($timestart, 'DateTime')) {return $timeend;}
		if (!is_a($timeend, 'DateTime')) {return $timeend;}
		
		$diff=$timeend->getTimestamp()-$timestart->getTimestamp(); 
		
		
		$diffminutes=$diff / 60;
		
		if($hour  > $diffminutes){
			$timeshouldgenerateceratificate=$timestart->getTimestamp()+($hour*60);
			$ftime=new \DateTime();
			$timeend->setTimestamp($timeshouldgenerateceratificate);
		}
		
		return $timeend;
	}
	
	 public function updateTimelastprint($skey) {
		 if(empty($skey)){return null;}
		  $data=$this->getContainer()->get('badiu.admin.certificate.certificate.data');
		  $fparam=array('skey'=>$skey);
		  $countcert=$data->countGlobalRow($fparam);
		  if($countcert!= 1){return null;}
		  $id=$data->getGlobalColumnValue('id',$fparam);
		  $faparam=array();
		  $faparam['timelastprint']=new \DateTime();
		  $faparam['id']= $id;
		  $faparam['timemodified']=new \DateTime();
		  $result=$data->updateNativeSql($faparam,false);
		 return $result;
	 }
}
