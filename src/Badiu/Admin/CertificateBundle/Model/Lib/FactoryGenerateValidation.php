<?php

namespace Badiu\Admin\CertificateBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryGenerateValidation extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

  /*  public function check($param) {
		
		//hour  e time exec
		$result=$this->hourTimeRealization($param);
		if(!empty($result)){return $result;}
		
		//date completed and date generate
		$result=$this->timeGenerateTimeCompleted($param);
		if(!empty($result)){return $result;}
		
        return null; 
	}
	*/
	public function checkCurrentDate($param) {
		$date=$this->getUtildata()->getVaueOfArray($param,'date');
		$type=$this->getUtildata()->getVaueOfArray($param,'type');
		$now=new \DateTime();
		if (!is_a($date, 'DateTime')) {return null;}
		
		if($date->getTimestamp() > $now->getTimestamp()){return array('error'=>'dateisbiggerthantimenow','type'=>$type);}
		
		return null;
	}
	public function hourTimeRealization($param) {
		$hour=$this->getUtildata()->getVaueOfArray($param,'hour');
		$timestart=$this->getUtildata()->getVaueOfArray($param,'timestart');
		$timeend=$this->getUtildata()->getVaueOfArray($param,'timeend');
		$type=$this->getUtildata()->getVaueOfArray($param,'type');
		$infonextdate=$this->getUtildata()->getVaueOfArray($param,'infonextdate');
		if (empty($hour)) {return null;}
		
		if (!is_a($timestart, 'DateTime')) {return null;}
		if (!is_a($timeend, 'DateTime')) {return null;}
		
		$diff=$timeend->getTimestamp()-$timestart->getTimestamp(); 
		
		if($diff < 0 ){
			return array('error'=>'timeendislessthantimestart','type'=>$type);
		}
		$diffminutes=$diff / 60;
		
		
		if($hour > $diffminutes ){
			$timeshouldgenerateceratificate=$timeend->getTimestamp()+($hour*60);
			$ftime=new \DateTime();
			$ftime->setTimestamp($timeshouldgenerateceratificate);
			
			return array('error'=>'timerealizationislessthanhour','timeshouldgenerateceratificate'=>$ftime,'type'=>$type,'infonextdate'=>$infonextdate);
		}
		
		return null;
	}
	
	public function timeGenerateTimeCompleted($param) {
		$timecompleted=$this->getUtildata()->getVaueOfArray($param,'timecompleted');
		$now=new \DateTime();
		if (!is_a($timecompleted, 'DateTime')) {return null;}
		
		if($timecompleted->getTimestamp() > $now->getTimestamp()){return array('error'=>'timecompletedisbiggerthantimenow');}
		
		return null;
	}
	
	public function showError($param) {
		
		if(empty($param)){return null;}
		$error=$this->getUtildata()->getVaueOfArray($param,'error');
		$type=$this->getUtildata()->getVaueOfArray($param,'type');
		$infonextdate=$this->getUtildata()->getVaueOfArray($param,'infonextdate');
		$msg="";
		$key="";
		if($error=='dateisbiggerthantimenow'){
			if(!empty($type)){$key=".$type";}
			$msg=$this->getTranslator()->trans("badiu.admin.certificate.message$key.currentdate");
		}
		else if($error=='timeendislessthantimestart'){
			if(!empty($type)){$key=".$type";}
			$msg=$this->getTranslator()->trans("badiu.admin.certificate.message$key.timeendislessthantimestart");
		}else if($error=='timerealizationislessthanhour'){
			$msg=$this->getTranslator()->trans('badiu.admin.certificate.message.timerealizationislessthanhour');
		}
		else if($error=='timecompletedisbiggerthantimenow'){
			$msg=$this->getTranslator()->trans('badiu.admin.certificate.message.timeendislessthantimestart');
		}
		
			echo '<br><br><link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">';
			echo "<div class=\"alert alert-danger\" role=\"alert\">$msg</div></hr>";
			exit;
		
	}
}
