<?php

namespace Badiu\Admin\CertificateBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class FactoryTeste extends BadiuModelLib {
 
	private $templateid;
     function __construct(Container $container) {
        parent::__construct($container);
     
    } 
	
 	public function getDafaultEnrolData() {
		$param=array();
		
		//user
		$param['userfullname']='Usuário Teste do Certificado';
		$param['userdoc']='CPF Nº 000.000.000-00';
		
		//enrol
		$timestart=new \DateTime();
		$timestart->modify('-1 month');

		
		$timeend=new \DateTime();
		
		
		$param['enroltimestart']=$timestart;
		$param['enroltimeend']=$timeend;
		$param['enroltimecompleted']=$timeend;
		$param['enrolgrade']="8,9";
		
		return $param;
	}
	
	public function getTemplatedata() {
		if(empty($this->templateid)){return null;}
		$data=$this->getContainer()->get('badiu.tms.discipline.discipline.data');
	}	 
	 public function setTemplateid($templateid) {
        $this->templateid = $templateid;
    }
	
	function getTemplateid() {
        return $this->templateid;
    }

}

