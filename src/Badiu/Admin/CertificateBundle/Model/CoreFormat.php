<?php
namespace Badiu\Admin\CertificateBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class CoreFormat extends BadiuFormat{
    private  $userdataoptions=null;
     function __construct(Container $container) {
            parent::__construct($container);
            $this->userdataoptions=$this->getContainer()->get('badiu.system.user.user.form.dataopotions');
       } 

              
     
    public  function templateview($data){
            $id=$this->getUtildata()->getVaueOfArray($data,'id'); 
			
			$param=array('templateid'=>$id,'_operation'=>'viewtemplate');
			$url=$this->getUtilapp()->getUrlByRoute('badiu.admin.certificate.request.link',$param);
			if(empty($url)){return  null;}
			$link="<a href=\"$url\"  target=\"_blank\">Ver certificado</a>";
			return  $link;
	  
        return $value; 
    } 
     public  function getcertificate($data){
            $skey=$this->getUtildata()->getVaueOfArray($data,'skey'); 
			$param=array('parentid'=>$skey);
			$url=$this->getUtilapp()->getUrlByRoute('badiu.admin.certificate.get.link',$param);
			if(empty($url)){return  null;}
			$link="<a href=\"$url\"  target=\"_blank\">Ver certificado</a>";
			return  $link;
	  
        return $value; 
    } 
	
	 public  function getModuleName($data){
		    $name="";
            $dconfig=$this->getUtildata()->getVaueOfArray($data,'dconfig'); 
			$dconfig= $this->getJson()->decode($dconfig, true);
			$modulekey=$this->getUtildata()->getVaueOfArray($dconfig,'modulekey'); 
			if($modulekey=='badiu.ams.enrol.classe'){$name=$this->getUtildata()->getVaueOfArray($dconfig,'coursename'); }
			else if($modulekey=='badiu.tms.enrol.classe'){$name=$this->getUtildata()->getVaueOfArray($dconfig,'coursename'); }
			else if($modulekey=='badiu.ams.enrol.migration.legacy'){$name=$this->getUtildata()->getVaueOfArray($dconfig,'coursename'); }
			else if($modulekey=='badiu.tms.enrol.migration.legacy'){$name=$this->getUtildata()->getVaueOfArray($dconfig,'coursename'); }
			else if($modulekey=='badiu.ams.enrol.offer'){$name=$this->getUtildata()->getVaueOfArray($dconfig,'offername'); }
			else if($modulekey=='badiu.tms.enrol.offer'){$name=$this->getUtildata()->getVaueOfArray($dconfig,'offername'); }
			
			return  $name;
	   
    } 
   
}
