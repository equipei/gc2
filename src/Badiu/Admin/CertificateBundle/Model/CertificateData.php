<?php

namespace Badiu\Admin\CertificateBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class CertificateData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
 

	
   public function existByReferencekey($entity,$userid,$modulekey,$moduleinstance,$referencekey) {
		$r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.userid=:userid AND o.modulekey=:modulekey AND o.moduleinstance=:moduleinstance  AND o.referencekey=:referencekey";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('userid',$userid);
			$query->setParameter('modulekey',$modulekey);
			$query->setParameter('moduleinstance',$moduleinstance);
			$query->setParameter('referencekey',$referencekey);
			
			
			$result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
   }
   
   public function getBySkey($skey) {
	   $sql="SELECT  o.id,u.id AS userid,u.firstname,u.lastname,u.alternatename,u.enablealternatename,u.email,u.doctype,u.docnumber, o.statusinfo,s.id AS statusid,s.name AS statusname,s.shortname AS  statusshortname,o.modulekey,o.moduleinstance,o.moduleinstancename,o.referencekey,o.dhour,o.content,o.skey,o.dconfig,o.description,o.timecreated,o.timemodified,o.timegenerated  FROM ".$this->getBundleEntity()." o JOIN o.statusid s JOIN o.userid u WHERE o.skey = :skey";
       $query = $this->getEm()->createQuery($sql);
       $query->setParameter('skey',$skey);
	   $result= $query->getOneOrNullResult();
       return $result;
   }
}
