<?php

namespace Badiu\Admin\CertificateBundle\Entity;
use Badiu\System\ComponentBundle\Model\Form\Cast\BadiuHourFormCast;
use Doctrine\ORM\Mapping as ORM;

/**
 * AdminCertificateTemplate
 *  
 * @ORM\Table(name="admin_certificate_template", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_certificate_template_idnumber_uix", columns={"entity", "idnumber"})},
 *      indexes={@ORM\Index(name="admin_certificate_template_entity_ix", columns={"entity"}),
  *              @ORM\Index(name="admin_certificate_template_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_certificate_template_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="admin_certificate_template_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_certificate_template_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminCertificateTemplate  {
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    
     /**
     * @var AdminCertificateTemplateStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminCertificateTemplateStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;

  	
/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string",length=255, nullable=true)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255,nullable=true)
	 */
	private $shortname;
    
	
    	 /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;  //plugin | dynamic
	 /**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
	
	/**
     * @var string
     *
     * @ORM\Column(name="firstpagehead", type="text", nullable=true)
     */
    private $firstpagehead;
	/**
     * @var string
     *
     * @ORM\Column(name="firstpagecontent", type="text", nullable=true)
     */
    private $firstpagecontent;
	
	/**
     * @var string
     *
     * @ORM\Column(name="firstpageimgbackgroud",  type="string", length=255, nullable=true)
     */
    private $firstpageimgbackgroud;
	
	/**
     * @var string
     *
     * @ORM\Column(name="firstpagefooter", type="text", nullable=true)
     */
    private $firstpagefooter;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="secondpagehead", type="text", nullable=true)
     */
    private $secondpagehead;
/**
     * @var string
     *
     * @ORM\Column(name="secondpagecontent", type="text", nullable=true)
     */
    private $secondpagecontent;
	
	/**
     * @var string
     *
     * @ORM\Column(name="secondpageimgbackgroud",  type="string", length=255, nullable=true)
     */
    private $secondpageimgbackgroud;
	
	/**
     * @var string
     *
     * @ORM\Column(name="secondpagefooter", type="text", nullable=true)
     */
    private $secondpagefooter;
	
	/**
     * @var string
     *
     * @ORM\Column(name="customformat", type="text", nullable=true)
     */
    private $customformat;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;

    
         /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

 

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }



    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getStatusid() {
        return $this->statusid;
    }

   
    function setStatusid(AdminCertificateTemplateStatus $statusid) {
        $this->statusid = $statusid;
    }

   

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }
    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

		
	 function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
	
	

    /**
     * @return string
     */
    public function getDtype() {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype) {
        $this->dtype = $dtype;
    }
	
	/**
     * @return string
     */
    public function getFirstpagecontent() {
        return $this->firstpagecontent;
    }

    /**
     * @param string $firstpagecontent
     */
    public function setFirstpagecontent($firstpagecontent) {
        $this->firstpagecontent = $firstpagecontent;
    }
	
	 /**
     * @return string
     */
    public function getFirstpageimgbackgroud() {
        return $this->firstpageimgbackgroud;
    }

    /**
     * @param string $firstpageimgbackgroud
     */
    public function setFirstpageimgbackgroud($firstpageimgbackgroud) {
        $this->firstpageimgbackgroud = $firstpageimgbackgroud;
    }
	
	
 /**
     * @return string
     */
    public function getFirstpagefooter() {
        return $this->firstpagefooter;
    }

    /**
     * @param string $firstpagefooter
     */
    public function setFirstpagefooter($firstpagefooter) {
        $this->firstpagefooter = $firstpagefooter;
    }

/**
     * @return string
     */
    public function getSecondpagecontent() {
        return $this->secondpagecontent;
    }

    /**
     * @param string $secondpagecontent
     */
    public function setSecondpagecontent($secondpagecontent) {
        $this->secondpagecontent = $secondpagecontent;
    }
	
	 /**
     * @return string
     */
    public function getSecondpageimgbackgroud() {
        return $this->secondpageimgbackgroud;
    }

    /**
     * @param string $secondpageimgbackgroud
     */
    public function setSecondpageimgbackgroud($secondpageimgbackgroud) {
        $this->secondpageimgbackgroud = $secondpageimgbackgroud;
    }
	
	
 /**
     * @return string
     */
    public function getSecondpagefooter() {
        return $this->secondpagefooter;
    }

    /**
     * @param string $secondpagefooter
     */
    public function setSecondpagefooter($secondpagefooter) {
        $this->secondpagefooter = $secondpagefooter;
    }
	
	/**
     * @return string
     */
    public function getSecondpagehead() {
        return $this->secondpagehead;
    }

    /**
     * @param string $secondpagehead
     */
    public function setSecondpagehead($secondpagehead) {
        $this->secondpagehead = $secondpagehead;
    }
	
	
	
/**
     * @return string
     */
    public function getFirstpagehead() {
        return $this->firstpagehead;
    }

    /**
     * @param string $firstpagehead
     */
    public function setFrstpagehead($firstpagehead) {
        $this->firstpagehead = $firstpagehead;
    }
	
	/**
     * @return string
     */
    public function getCustomformat() {
        return $this->customformat;
    }

    /**
     * @param string $customformat
     */
    public function setCustomformat($customformat) {
        $this->customformat = $customformat;
    }
}
