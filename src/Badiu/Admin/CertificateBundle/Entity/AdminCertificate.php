<?php

namespace Badiu\Admin\CertificateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *AdminCertificate
 *  
 * @ORM\Table(name="admin_certificate", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_certificate_skey_uix", columns={"entity", "skey"}),
 *      @ORM\UniqueConstraint(name="admin_certificate_idnumber_uix", columns={"entity", "idnumber"}),
 *      @ORM\UniqueConstraint(name="admin_certificate_referencekey", columns={"entity","userid","modulekey","moduleinstance","dtype","referencekey"})},
 *      indexes={@ORM\Index(name="admin_certificate_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_certificate_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_certificate_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="admin_certificate_statusinfo_ix", columns={"statusinfo"}),
 *              @ORM\Index(name="admin_certificate_statusid_ix", columns={"statusid"}), 
 *              @ORM\Index(name="admin_certificate_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_certificate_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_certificate_referencekey_ix", columns={"referencekey"}), 
 *              @ORM\Index(name="admin_certificate_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="admin_certificate_moduleinstancename_ix", columns={"moduleinstancename"}),
 *              @ORM\Index(name="admin_certificate_dhour_ix", columns={"dhour"}),
 *              @ORM\Index(name="admin_certificate_timegenerated_ix", columns={"timegenerated"}), 
 *              @ORM\Index(name="admin_certificate_skey_ix", columns={"skey"}), 
 *              @ORM\Index(name="admin_certificate_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminCertificate
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

      
         /**
     * @var AdminCertificateTemplate
     *
     * @ORM\ManyToOne(targetEntity="AdminCertificateTemplate")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="templateid", referencedColumnName="id")
     * })
     */
    private $templateid;
	/**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id", nullable=false)
     * })
     */
    private $userid; 
    
	 /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;  //default | oldversion

 
	 /**
     * @var string
     *
     * @ORM\Column(name="statusinfo", type="string", length=50, nullable=true)
     */
    private $statusinfo;  //planned | executed
	
 /**
     * @var AdminCertificateStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminCertificateStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;
    
	    
	 /**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
	
	 /**
	 * @var string
	 *
	 * @ORM\Column(name="moduleinstancename", type="string", length=255, nullable=true)
	 */
	private $moduleinstancename;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="referencekey", type="bigint", nullable=true)
	 */
	private $referencekey;

	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="dhour", type="bigint", nullable=true)
	 */
	private $dhour;
	/**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;
	
	/**
     * @var string
     *
     * @ORM\Column(name="skey", type="string", length=255, nullable=false)
     */
    private $skey;
    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
	   /**
     * @var \DateTime
     *
     * @ORM\Column(name="timelastprint", type="datetime", nullable=true)
     */
    private $timelastprint;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="timegenerated", type="datetime", nullable=true)
     */
    private $timegenerated;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
   
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }


    function getUserid() {
        return $this->userid;
    }

 

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

  
    function setUserid(\Badiu\System\UserBundle\Entity\SystemUser $userid) {
        $this->userid = $userid;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

  
    function getTemplateid(){
        return $this->templateid;
    }

    

    function getDconfig() {
        return $this->dconfig;
    }

   

    function setTemplateid(AdminCertificateTemplate $templateid) {
        $this->templateid = $templateid;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function getThour() {
        return $this->thour;
    }

    function setThour($thour) {
        $this->thour = $thour;
    }

  /**
     * @return string
     */
    public function getDtype() {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype) {
        $this->dtype = $dtype;
    }

	
	 function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
	 /**
     * @return string
     */
    public function getStatusinfo() {
        return $this->statusinfo;
    }

    /**
     * @param string $dstatus
     */
    public function setStatusinfo($dstatusinfo) {
        $this->statusinfo = $statusinfo;
    }
	
function getStatusid() {
        return $this->statusid;
    }
	
	   function setStatusid(AdminCertificateStatus $statusid) {
        $this->statusid = $statusid;
    }
		function setContent($content) {
        $this->content = $content;
    }

    public function getContent() {
        return $this->content;
    }
	 /**
     * @return string
     */
    public function getSkey() {
        return $this->skey;
    }

    /**
     * @param string $skey
     */
    public function setSkey($skey) {
        $this->skey = $skey;
    }
	
	
	 /**
     * @return string
     */
    public function getDhour() {
        return $this->dhour;
    }

    /**
     * @param string $dhour
     */
    public function setDhour($dhour) {
        $this->dhour = $dhour;
    }

function setTimegenerated(\DateTime $timegenerated) {
        $this->timegenerated = $timegenerated;
    }

    function getTimegenerated() {
        return $this->timegenerated;
    }

  function getModuleinstancename() {
        return $this->moduleinstancename;
    }

    function setModuleinstancename($moduleinstancename) {
        $this->moduleinstancename = $moduleinstancename;
    }	




function setTimelastprint(\DateTime $timelastprint) {
        $this->timelastprint = $timelastprint;
    }

    function getTimelastprint() {
        return $this->timelastprint;
    }
	
}
