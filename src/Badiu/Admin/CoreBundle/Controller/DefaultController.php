<?php

namespace Badiu\Admin\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuAdminCoreBundle:Default:index.html.twig', array('name' => $name));
    }
}
