<?php

namespace Badiu\Admin\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  AdminProject
 * @ORM\Table(name="admin_project", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_project_name_uix", columns={"entity", "name"}),
 *      @ORM\UniqueConstraint(name="admin_project_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="admin_project_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="admin_project_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_project_name_ix", columns={"name"}),
 *              @ORM\Index(name="admin_project_statusid_ix", columns={"statusid"}),
 *              @ORM\Index(name="admin_project_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="admin_project_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_project_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="admin_project_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_project_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminProject {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var \AdminProjectStatus
	 *
	 * @ORM\ManyToOne(targetEntity="AdminProjectStatus")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
	 * })
	 */

	private $statusid;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
	 */
	private $shortname;

        /**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customint1", type="bigint",  nullable=true)
	 */
	private $customint1;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customint2", type="bigint",  nullable=true)
	 */
	private $customint2;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customint3", type="bigint",  nullable=true)
	 */
	private $customint3;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customint4", type="bigint",  nullable=true)
	 */
	private $customint4;
	/**
	 * @var float
	 *
	 * @ORM\Column(name="customdec1", type="float",  precision=10, scale=0, nullable=true)
	 */
	private $customdec1;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="customdec2", type="float",  precision=10, scale=0, nullable=true)
	 */
	private $customdec2;
	/**
	 * @var float
	 *
	 * @ORM\Column(name="customdec3", type="float",  precision=10, scale=0, nullable=true)
	 */
	private $customdec3;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="customchar1", type="string", length=255, nullable=true)
	 */
	private $customchar1;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customchar2", type="string", length=255, nullable=true)
	 */
	private $customchar2;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customchar3", type="string", length=255, nullable=true)
	 */
	private $customchar3;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customtext1", type="text", nullable=true)
	 */
	private $customtext1;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customtext2", type="text", nullable=true)
	 */
	private $customtext2;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="customtext3", type="text", nullable=true)
	 */
	private $customtext3;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="param", type="text", nullable=true)
	 */
	private $param;
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted;

	public function getId() {
		return $this->id;
	}

	public function getEntity() {
		return $this->entity;
	}

	public function getName() {
		return $this->name;
	}

	public function getIdnumber() {
		return $this->idnumber;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getParam() {
		return $this->param;
	}

	public function getTimecreated() {
		return $this->timecreated;
	}

	public function getTimemodified() {
		return $this->timemodified;
	}

	public function getUseridadd() {
		return $this->useridadd;
	}

	public function getUseridedit() {
		return $this->useridedit;
	}

	public function getDeleted() {
		return $this->deleted;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setEntity($entity) {
		$this->entity = $entity;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setIdnumber($idnumber) {
		$this->idnumber = $idnumber;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setParam($param) {
		$this->param = $param;
	}

	public function setTimecreated(\DateTime $timecreated) {
		$this->timecreated = $timecreated;
	}

	public function setTimemodified(\DateTime $timemodified) {
		$this->timemodified = $timemodified;
	}

	public function setUseridadd($useridadd) {
		$this->useridadd = $useridadd;
	}

	public function setUseridedit($useridedit) {
		$this->useridedit = $useridedit;
	}

	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}

	public function getShortname() {
		return $this->shortname;
	}

	public function setShortname($shortname) {
		$this->shortname = $shortname;
	}

	/**
	 * Set statusid
	 *
	 * @param ProjectManagementBundle\Entity\AdminProjectStatus $statusid
	 * @return statusid
	 */
	public function setStatusid(AdminProjectStatus $statusid = null) {
		$this->statusid = $statusid;

		return $this;
	}

	/**
	 * Get statusid
	 *
	 * @return ProjectManagementBundle\Entity\AdminProjectStatus $statusid
	 */
	public function getStatusid() {
		return $this->statusid;
	}

	public function getDtype() {
		return $this->dtype;
	}

	public function setDtype($dtype) {
		$this->dtype = $dtype;
	}

 function setModulekey($modulekey) {
            $this->modulekey = $modulekey;
        }

        function setModuleinstance($moduleinstance) {
            $this->moduleinstance = $moduleinstance;
        }

		
 function getModulekey() {
            return $this->modulekey;
        }

        function getModuleinstance() {
            return $this->moduleinstance;
        }
}
