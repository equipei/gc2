<?php

namespace Badiu\Admin\SelectionBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AdminSelectionProject
 *  
 * @ORM\Table(name="admin_selection_project", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_selection_project_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="admin_selection_project_skey_uix", columns={"skey"}), 
 *      @ORM\UniqueConstraint(name="admin_selection_project_idnumber_uix", columns={"entity", "idnumber"})},
 *      indexes={@ORM\Index(name="admin_selection_project_entity_ix", columns={"entity"}),
 *               @ORM\Index(name="admin_selection_project_dtype_ix", columns={"dtype"}),
 *               @ORM\Index(name="admin_selection_project_ttype_ix", columns={"ttype"}),
 *              @ORM\Index(name="admin_selection_project_timestart_ix", columns={"requesttimestart"}),
 *              @ORM\Index(name="admin_selection_project_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_selection_project_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="admin_selection_project_requesttimeend_ix", columns={"requesttimeend"}),
 *              @ORM\Index(name="admin_selection_project_skey_ix", columns={"skey"}),
 *              @ORM\Index(name="admin_selection_project_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_selection_project_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminSelectionProject  {
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    
     /**
     * @var AdminSelectionProjectStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminSelectionProjectStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;

  	
/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string",length=255, nullable=true)
	 */
	private $name;
	

	/**
	 * @var string
	 *
	 * @ORM\Column(name="namealternative", type="string",length=255, nullable=true)
	 */
	private $namealternative;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255,nullable=true)
	 */
	private $shortname;
	
        /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype; // course |training| event

    
        /**
     * @var string
     *
     * @ORM\Column(name="ttype", type="string", length=50, nullable=true)
     */
    private $ttype; //default | student | teachear | manager
	/**
	 * @var string
	 *
	 * @ORM\Column(name="skey", type="string", length=255,nullable=false)
	 */
	private $skey;
    
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="requesttimestart", type="datetime", nullable=true)
     */
    private $requesttimestart;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="requesttimeend", type="datetime", nullable=true)
     */
    private $requesttimeend;
    
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="limitaccept", type="bigint", nullable=true)
	 */
	private $limitaccept;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="analysiscriteria", type="string", length=255, nullable=true)
	 */
	private $analysiscriteria; //manualcheck | automaticregistration | automaticservicecheck
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="servicecheckcriteria", type="string", length=255, nullable=true)
	 */
	private $servicecheckcriteria;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="listdocumentnumber", type="text", nullable=true)
     */
    private $listdocumentnumber;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=false)
	 */
	private $modulekey;



	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=false)
	 */
	private $moduleinstance;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="routeuserregistration", type="string", length=255, nullable=true)
	 */
	private $routeuserregistration;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="msgpresentation", type="text", nullable=true)
     */
    private $msgpresentation;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="msgconfirmationrequest", type="text", nullable=true)
     */
    private $msgconfirmationrequest;
	
	/**
     * @var string
     *
     * @ORM\Column(name="msgacceptrequest", type="text", nullable=true)
     */
    private $msgacceptrequest;
	
	/**
     * @var string
     *
     * @ORM\Column(name="msgrejectionrequest", type="text", nullable=true)
     */
    private $msgrejectionrequest;
	
	/**
     * @var string
     *
     * @ORM\Column(name="msgqueue", type="text", nullable=true)
     */
    private $msgqueue;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="defaultrole", type="string",length=255, nullable=true)
	 */
	private $defaultrole;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="defaultstatus", type="string",length=255, nullable=true)
	 */
	private $defaultstatus;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;

    
         /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

   

    function getRequesttimestart() {
        return $this->requesttimestart;
    }

    function getRequesttimeend() {
        return $this->requesttimeend;
    }

   

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }


    function setRequesttimestart(\DateTime $requesttimestart) {
        $this->requesttimestart = $requesttimestart;
    }

    function setRequesttimeend(\DateTime $requesttimeend) {
        $this->requesttimeend = $requesttimeend;
    }

    

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getStatusid() {
        return $this->statusid;
    }

    function setStatusid(AdminSelectionProjectStatus $statusid) {
        $this->statusid = $statusid;
    }

   
    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }
    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

		
	 function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
	 function getLimitaccept() {
        return $this->limitaccept;
    }
	function setLimitaccept($limitaccept) {
        $this->limitaccept = $limitaccept;
    }
	
	function getAnalysiscriteria() {
        return $this->analysiscriteria;
    }
	function setAnalysiscriteria($analysiscriteria) {
        $this->analysiscriteria = $analysiscriteria;
    }
	
	function getServicecheckcriteria() {
        return $this->servicecheckcriteria;
    }
	function setServicecheckcriteria($servicecheckcriteria) {
        $this->servicecheckcriteria = $servicecheckcriteria;
    }
	
	function getMsgpresentation() {
        return $this->msgpresentation;
    }
	function setMsgpresentation($msgpresentation) {
        $this->msgpresentation = $msgpresentation;
    }
	
function getMsgconfirmationrequest() {
        return $this->msgconfirmationrequest;
    }
	function setMsgconfirmationrequest($msgconfirmationrequest) {
        $this->msgconfirmationrequest = $msgconfirmationrequest;
    }
	function getMsgacceptrequest() {
        return $this->msgacceptrequest;
    }
	function setMsgacceptrequest($msgacceptrequest) {
        $this->msgacceptrequest = $msgacceptrequest;
    }
	function getMsgrejectionrequest() {
        return $this->msgrejectionrequest;
    }
	function setMsgrejectionrequest($msgrejectionrequest) {
        $this->msgrejectionrequest = $msgrejectionrequest;
    }
function getMsgqueue() {
        return $this->msgqueue;
    }
	function setMsgqueue($msgqueue) {
        $this->msgqueue = $msgqueue;
    }	
	
	 function setSkey($skey) {
            $this->skey = $skey;
        }
		
	function getSkey() {
            return $this->skey;
        }

	 function setRouteuserregistration($routeuserregistration) {
            $this->routeuserregistration = $routeuserregistration;
        }
		
	function getRouteuserregistration() {
            return $this->routeuserregistration;
        }
		
  function setNamealternative($namealternative) {
        $this->namealternative = $namealternative;
    }
	
	 function getNamealternative() {
        return $this->namealternative;
    }	

 function getDefaultrole() {
        return $this->defaultrole;
    }

    function setDefaultrole($defaultrole) {
        $this->defaultrole = $defaultrole;
    }

 function getDefaultstatus() {
        return $this->defaultstatus;
    }

    function setDefaultstatus($defaultstatus) {
        $this->defaultstatus = $defaultstatus;
    }	

    /**
     * @return string
    */
   public function getDtype()
   {
       return $this->dtype;
   }

   /**
    * @param string $dtype
    */
   public function setDtype($dtype)
   {
       $this->dtype = $dtype;
   }

       /**
     * @return string
    */
    public function getTtype()
    {
        return $this->ttype;
    }
 
    /**
     * @param string $ttype
     */
    public function setTtype($ttype)
    {
        $this->ttype = $ttype;
    }
	
	   /**
     * @return string
    */
    public function getListdocumentnumber()
    {
        return $this->listdocumentnumber;
    }
 
    /**
     * @param string $listdocumentnumber
     */
    public function setListdocumentnumber($listdocumentnumber)
    {
        $this->listdocumentnumber = $listdocumentnumber;
    }
}
