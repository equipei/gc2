<?php

namespace Badiu\Admin\SelectionBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AdminSelectionRequest
 *  
 * @ORM\Table(name="admin_selection_request", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_selection_request_idnumber_uix", columns={"entity", "idnumber"})},
 *      indexes={@ORM\Index(name="admin_selection_request_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_selection_request_timerequest_ix", columns={"timerequest"}),
 *              @ORM\Index(name="admin_selection_request_project_ix", columns={"projectid"}),
 *              @ORM\Index(name="admin_selection_request_userid_ix", columns={"userid"}),
*              @ORM\Index(name="admin_selection_request_statusid_ix", columns={"statusid"}),
*              @ORM\Index(name="admin_selection_request_statusinfo_ix", columns={"statusinfo"}), 
 *              @ORM\Index(name="admin_selection_request_timeanalyzed_ix", columns={"timeanalyzed"}),
 *              @ORM\Index(name="admin_selection_request_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_selection_request_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminSelectionRequest  {
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    
     /**
     * @var AdminSelectionProject
     *
     * @ORM\ManyToOne(targetEntity="AdminSelectionProject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="projectid", referencedColumnName="id", nullable=false)
     * })
     */
    private $projectid;

  	/**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id")
     * })
     */
    private $userid; 
	
	/**
     * @var AdminSelectionStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminSelectionStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id", nullable=false)
     * })
     */
    private $statusid;
	
		 /**
     * @var string
     *
     * @ORM\Column(name="statusinfo", type="string", length=50, nullable=true)
     */
    private $statusinfo; 
	 
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="timerequest", type="datetime", nullable=false)
     */
    private $timerequest;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeanalyzed", type="datetime", nullable=true)
     */
    private $timeanalyzed;
    
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
	private $dtype; //manual | automatic

	/**
     * @var string
     *
     * @ORM\Column(name="coupon", type="string", length=255, nullable=true)
     */
    private $coupon;
	
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;

     
         /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

   

    function getTimerequest() {
        return $this->timerequest;
    }

    function getTimeanalyzed() {
        return $this->timeanalyzed;
    }

   

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }


    function setTimerequest(\DateTime $timerequest) {
        $this->timerequest = $timerequest;
    }

    function setTimeanalyzed(\DateTime $timeanalyzed) {
        $this->timeanalyzed = $timeanalyzed;
    }

    

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getStatusid() {
        return $this->statusid;
    }

    function setStatusid(AdminSelectionStatus $statusid) {
        $this->statusid = $statusid;
    }

   
    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }
    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

		
	 function getProjectid() {
        return $this->projectid;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	function setProjectid($projectid) {
        $this->projectid = $projectid;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
	
    function getUserid() {
        return $this->userid;
    }


    function setUserid(\Badiu\System\UserBundle\Entity\SystemUser $userid) {
        $this->userid = $userid;
    }


	 /**
     * @return string
     */
    public function getStatusinfo() {
        return $this->statusinfo;
    }

 /**
     * @param string $dstatus
     */
    public function setStatusinfo($dstatusinfo) {
        $this->statusinfo = $statusinfo;
    }	
	
	 /**
     * @return string
     */
    public function getDtype() {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype) {
        $this->dtype = $dtype;
    }
	
	// Getter
    public function getCoupon() {
        return $this->coupon;
    }

    // Setter
    public function setCoupon($coupon) {
        $this->coupon = $coupon;
    }
}
