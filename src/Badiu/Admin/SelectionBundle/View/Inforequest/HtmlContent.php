<?php 

	$buttonlabel=$translator->trans('badiu.admin.selection.request.button.requestenroll');
	$modulekey=$utildata->getVaueOfArray($page->getData(),'badiu_list_data_row.data.1.modulekey',true);
	if(!empty($modulekey)){$buttonlabel=$translator->trans($modulekey.'.selectionprocessrequest');}
	
	$courselabel=$translator->trans('badiu.admin.selection.request.course.name');
	if($modulekey=='badiu.ams.offer.offer'){$courselabel=$translator->trans('badiu.tms.offer.offer.name');}
	$isuseranonymous = $container->get('badiu.system.access.session')->get()->getUser()->getAnonymous();
	$parentid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
	$prefixid="";
	if(!empty($parentid)){$prefixid="-".$parentid;}
	
?>
<br />

<div id="_badiu_theme_core_dashboard_vuejs<?php echo $prefixid;?>" class="badiu-admin-selection-process  badiu-admin-selection-statusinfo">
<?php 
if($isuseranonymous){require_once("StepNotloguedin.php");}
else {require_once("StepLoguedin.php");}
?> 
<div class="card">
  <div class="card-header" >
 <?php echo $translator->trans('badiu.admin.selection.inforequest.title'); ?>
  </div>
  <div class="card-body">
  
   <div class="alert alert-danger text-center" role="alert"   v-show="rowdata1.statusshortame=='inaccessible'"><?php echo $translator->trans('badiu.admin.selection.request.naccessible.message'); ?></div>
   <div class="alert alert-danger text-center" role="alert"   v-show="rowdata1.statusshortame!='inaccessible' && fparam.messagerestriction" v-html="fparam.messagerestriction"> </div>
   <table class="table table-striped">
    
    <tbody>
      <tr>
        <td  class="tblinfotdtitle" width="25%"><div class="tblinfotitle"><?php echo $courselabel; ?></div></td>
        <td class="tblinfotdcontent"><div class="tblinfocontent" v-html="rowdata1.name"></div></td>
      
      </tr> 
      
	  <tr v-show="rowdata1.limitaccept">
        <td  class="tblinfotdtitle" width="25%"><div class="tblinfotitle"><?php echo $translator->trans('badiu.admin.selection.project.limitaccept'); ?></div></td>
        <td class="tblinfotdcontent"><div class="tblinfocontent" v-html="rowdata1.limitaccept"></div></td>
      
      </tr>
	
	 
	   <tr v-show="rowdata1.requesttimestart">
        <td  class="tblinfotdtitle" width="25%"><div class="tblinfotitle"><?php echo $translator->trans('badiu.admin.selection.project.timestart'); ?></div></td>
        <td class="tblinfotdcontent"><div class="tblinfocontent" v-html="rowdata1.requesttimestart"></div></td>
      
      </tr>
	  
	   <tr v-show="rowdata1.requesttimeend">
        <td  class="tblinfotdtitle" width="25%"><div class="tblinfotitle"><?php echo $translator->trans('badiu.admin.selection.project.timeend'); ?></div></td>
        <td class="tblinfotdcontent"><div class="tblinfocontent" v-html="rowdata1.requesttimeend"></div></td>
      
      </tr>
	  
	    <tr v-show="fparam.showcoupon">
        <td  class="tblinfotdtitle" width="25%"><div class="tblinfotitle"><?php echo $translator->trans('badiu.admin.selection.inforequest.coupon'); ?></div></td>
        <td class="tblinfotdcontent">
		  <div class="alert alert-danger" role="alert" v-show="couponerror" v-html="couponerror"></div>
		  <div class="tblinfocontent">
			<input type="text" v-model="fparam.coupon" class="form-control badiu-admin-selection-inforequest-coupon" id="badiuadminselectioninforequestcoupon" aria-describedby="emailHelp" placeholder="<?php echo $translator->trans('badiu.admin.selection.inforequest.coupon.placeholder'); ?>">
		  </div>
		</td>
      
      </tr>
	  
	  
	  
    </tbody>
  </table>
  <hr />
    <p class="card-text" v-html="rowdata1.msgpresentation"></p>
    
	<div class="row">
		<div class="col"></div>
		<div class="col" v-show="rowdata1.statusshortame=='preregistration' && !fparam.messagerestriction"><a @click="requestProcess()" class="form-control btn btn-primary"><?php echo $buttonlabel; ?></a></div>
	</div>
  </div>
</div>

</div>
