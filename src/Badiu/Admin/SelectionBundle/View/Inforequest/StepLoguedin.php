<?php
$urlfinishshopping=2;
?>

 <div class="stepwizard">   
 <div class="stepwizard-row setup-panel">
     <div class="stepwizard-step  col-xs-4">  
         <a href="<?php echo $utilapp->getCurrentUrl();?>" type="button" class="btn btn-warning btn-circle"><i class="fas fa-info"></i></a>
         <p><small><?php echo $translator->trans('badiu.admin.selection.request.courseinfo'); ?></small></p>
     </div>
  
      <div class="stepwizard-step  col-xs-6"> 
         <a href="#" type="button" class="btn btn-secondary btn-circle" disabled="disabled"><i class="fas fa-check"></i></a>
         <p><small><?php echo $translator->trans('badiu.admin.selection.request.stausinfo'); ?></small></p>
     </div>
                 
 </div>
</div>

