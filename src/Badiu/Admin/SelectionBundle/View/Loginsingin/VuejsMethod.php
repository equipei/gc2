loginsinginStartFormEdit: function () {
	if(this.isedit){this.loginsinginManageFormView('withoutregister');}
	
	//cpf
	if(this.badiuform.doctype=='CPF'){this.badiuform.cpf=this.badiuform.docnumber;}
	if(this.badiuform.nationalitystatus==''){this.badiuform.nationalitystatus='native';}
	
	this.badiuform.name=this.badiuform.firstname+' '+this.badiuform.lastname;
	if(this.isedit){$("#fgitem_password").hide();}
	
},
loginsinginManageFormView: function (status) {
		
			this.loginsinginstatus=status;
		 $("#card_formlogin").show();
		 $("#card_inforecoverpwd").hide();
		 $("#card_formadd").hide();
		  
	   if(status== 'started'){
		   $("#fidentifyparam_item_password").hide();
		
		}
	   else if(status== 'hasregister'){
		   $("#fidentifyparam_item_password").show();
		
		}else if(status== 'hasregisteronadd'){
		   $("#fidentifyparam_item_password").show();
		}else if(status== 'withoutregister'){
			this.withoutregisterTransferUsername();
			 this.formcontrol.haserror=false;
             this.formcontrol.message="";
			$("#card_formlogin").hide(); 
		    $("#card_formadd").show();
		}else if(status== 'addrregisterfailed'){
		    $("#card_formlogin").hide(); 
		   $("#card_formadd").show();
		}
	
	 
  },

withoutregisterTransferUsername: function () {
	var username=this.fidentifyparam.username;
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(username.match(mailformat)){this.badiuform.email=username;}
	else if (this.badiubrcpfvalidade(username)){this.badiuform.cpf=username;}
},
  loginsinginCheckUsername: function () {
			var self = this;
			this.formcontrol.processing=true;
			
			this.fidentifyparam._service=this.loginsinginservice;
			this.fidentifyparam._function="checkUsername";
			this.fidentifyparam.requestid=this.badiuform.requestid;
			
			//console.log(this.fidentifyparam);
			 
			   axios.post(this.serviceurl,this.fidentifyparam).then(function (response) {
				   console.log(response.data);
				    if(response.data.status=='accept'){
						var status=response.data.message.status;
						self.loginsinginstatus=status;
						self.loginsinginManageFormView(status);
						self.formcontrol.processing=false;
						self.formcontrol.typemessage='info';
						if(response.data.message.typemessage!== undefined && response.data.message.typemessage=='info'){
								self.formcontrol.typemessage='info';
								self.formcontrol.message=response.data.message.message;
								self.formcontrol.haserror=true;
							}else{
								self.formcontrol.haserror=false;
							}
					 }else if(response.data.status=='danied'){
						self.formcontrol.typemessage='error';
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
						
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
                     self.formcontrol.message="Ocorreu um erro no sistema";
					 self.formcontrol.typemessage='error';
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 
  },
  loginsinginExecLogin: function () {
	  
			var self = this;
			this.formcontrol.processing=true;
			this.formcontrol.typemessage='error';
			this.formcontrol.haserror=false;
			this.formcontrol.message='';
			
			this.fidentifyparam._service=this.loginsinginservice;
			this.fidentifyparam._function="execLogin";
			this.fidentifyparam.requestid=this.badiuform.requestid;
			//console.log(this.badiuform);
			   axios.post(this.serviceurl,this.fidentifyparam).then(function (response) {
				      console.log(response.data);
					 if(response.data.status=='accept'){
						 self.formcontrol.typemessage='info';
							self.urlgoback=response.data.message.urlgoback;
							if(!self.isparamempty(self.urlgoback)){window.location.replace(self.urlgoback);}
							self.formcontrol.processing=false;
					 }else if(response.data.status=='danied'){
						  self.formcontrol.typemessage='error';
						self.loginsinginManageFormView('passordnotmatch');
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
				 if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
					 self.formcontrol.typemessage='error';
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 
  },



  loginsinginExecSingin: function () {

	var self = this;
			this.formcontrol.processing=true;
			
			 var execserviceremote=true;
             <?php echo makeErrorCheckRequired($container,$page,$formfields);?>
			//console.log("isparamemptynationalitystatus: "+ isparamemptynationalitystatus +"   execserviceremote: "+ execserviceremote +"    badiurequiredcontrol.nationalitystatus: "+this.badiurequiredcontrol.nationalitystatus);
			// console.log("for remote exe: "+execserviceremote);return null;
			if(!execserviceremote){ return null; }  	
			//this.badiuform._service=this.loginsinginservice;
			this.badiuform._service=this.servicekey;
			this.badiuform._key=this.currentroute;
			//this.badiuform._function="execSingin"; 
			//this.badiuform.badiuform.username=this.badiuform.username;
			//console.log(this.badiuform);
			   axios.post(this.serviceurl,this.badiuform).then(function (response) {
				      console.log(response.data);
					 if(response.data.status=='accept'){
							self.formcontrol.typemessage='info';
							self.urlgoback=response.data.message.urlgoback;
							if(!self.isparamempty(self.urlgoback)){window.location.replace(self.urlgoback);}
							self.formcontrol.processing=false;
					 }else if(response.data.status=='danied'){
						self.loginsinginManageFormView('withoutregister');
						self.formcontrol.typemessage='error';
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
						if(response.data.info=='badiu.financ.ecommerce.loginsingin.userjustexistonadd'){self.loginsinginManageFormView('hasregisteronadd');}
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
					 self.formcontrol.typemessage='error';
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 
  },

  loginsinginRestourePassword: function () {
		
			var self = this;
			this.formcontrol.processing=true;
			
			this.fidentifyparam._service=this.loginsinginservice;
			this.fidentifyparam._function="recoverPassword";
			this.fidentifyparam.requestid=this.badiuform.requestid;
		
			   axios.post(this.serviceurl,this.fidentifyparam).then(function (response) {
				      console.log(response.data);
					 if(response.data.status=='accept'){
						 self.formcontrol.typemessage='info';
						 self.loginsinginstatus=response.data.message.status;
						 self.messegepasswordsendtoemail=response.data.message.message;
						 self.formcontrol.processing=false;
						  
						  $("#card_formlogin").hide();
						  $("#card_inforecoverpwd").show();
						 
					 }else if(response.data.status=='danied'){
						 self.formcontrol.typemessage='error';
						self.loginsinginManageFormView('withoutregister');
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
					  self.formcontrol.typemessage='error';
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 
},

 tryNewExecLogin: function () {
			this.loginsinginManageFormView('hasregister');
			this.fidentifyparam.password="";
			this.formcontrol.haserror=false;
			this.formcontrol.message="";

},


systemuserdefaultform_alternatename: function () {
	if(this.badiuform.enablealternatename==1){
		$("#fgitem_alternatename").show(); 		
	}else  { 
	    $("#fgitem_alternatename").hide(); 	
	}
},

systemuserdefaultform_sex: function () {
	if(this.badiuform.sex=='other'){
		$("#fgitem_sexother").show(); 		
	}else  { 
	    $("#fgitem_sexother").hide(); 	
	}
}, 
