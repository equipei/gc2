<?php

	$modulekey=$utildata->getVaueOfArray($page->getData(),'badiu_list_data_row.data.1.modulekey',true);
	$courselabel=$translator->trans('badiu.admin.selection.request.course.name');
	if($modulekey=='badiu.ams.offer.offer'){$courselabel=$translator->trans('badiu.tms.offer.offer.name');}
	 
	
	$parentid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
	$prefixid="";
	if(!empty($parentid)){$prefixid="-".$parentid;}
	
	//link to access 
	$requestlib=$container->get('badiu.admin.selection.request.lib');
	$rparam=array('requestid'=>$parentid);
	$urltoaccess=$requestlib->getUrlAccess($rparam);
	
	$urltostudendashboard=$utilapp->getUrlByRoute('badiu.tms.my.studentfviewdefault.dashboard');
	if($modulekey=='badiu.ams.offer.offer'){$urltoaccess=null;}
	
?>

<br />

<div id="_badiu_theme_core_dashboard_vuejs<?php echo $prefixid;?>"  class="badiu-admin-selection-process badiu-admin-selection-statusrequest">
<?php require_once("StepLoguedin.php");?> 

<div class="card">
  <div class="card-header"><?php echo $translator->trans('badiu.admin.selection.statusrequest.title'); ?></div>
<div class="card-body">
  
  <div class="alert alert-success text-center" role="alert"   v-show="rowdata1.statusshortame=='acept'"><?php echo $translator->trans('badiu.admin.selection.request.accept.message'); ?></div>
  <div class="alert alert-info text-center" role="alert"   v-show="rowdata1.statusshortame=='waitanalysis'"><?php echo $translator->trans('badiu.admin.selection.request.waitanalysis.message'); ?></div>
  <div class="alert alert-info text-center" role="alert"   v-show="rowdata1.statusshortame=='inqueue'"><?php echo $translator->trans('badiu.admin.selection.request.inqueue.message'); ?></div>
  <div class="alert alert-danger text-center" role="alert"   v-show="rowdata1.statusshortame=='dained'"><?php echo $translator->trans('badiu.admin.selection.request.dained.message'); ?></div>
  <div class="alert alert-warning text-center" role="alert"   v-show="rowdata1.statusshortame=='cancelled'"><?php echo $translator->trans('badiu.admin.selection.request.cancelled.message'); ?></div>
  <div class="alert alert-danger text-center" role="alert"   v-show="rowdata1.statusshortame=='inaccessible'"><?php echo $translator->trans('badiu.admin.selection.request.naccessible.message'); ?></div>

   <table class="table table-striped">
    
    <tbody>
      <tr>
        <td  class="tblinfotdtitle" width="10%"><div class="tblinfotitle"><?php echo $courselabel; ?></div></td>
        <td class="tblinfotdcontent"><div class="tblinfocontent" v-html="rowdata1.name"></div></td>
      
      </tr>
      
	
	 <tr>
        <td  class="tblinfotdtitle" width="10%"><div class="tblinfotitle"><?php echo $translator->trans('badiu.admin.selection.statusrequest.date'); ?></div></td>
        <td class="tblinfotdcontent"><div class="tblinfocontent"  v-html="rowdata1.timerequest"></div></td>
      
      </tr>
	 
	  <tr>
        <td  class="tblinfotdtitle" width="10%"><div class="tblinfotitle"><?php echo $translator->trans('badiu.admin.selection.statusrequest.status'); ?></div></td>
        <td class="tblinfotdcontent"><div class="tblinfocontent" v-html="rowdata1.statusname"></div></td>
      
      </tr>
	  
	  <tr>
        <td  class="tblinfotdtitle" width="10%"><div class="tblinfotitle"><?php echo $translator->trans('badiu.admin.selection.request.user'); ?></div></td>
        <td class="tblinfotdcontent"><div class="tblinfocontent" v-html="rowdata1.firstname +' '+ rowdata1.lastname +' <' + rowdata1.email + '>'"></div></td>
      
      </tr>
	  
	  
    </tbody>
  </table>
  <hr />
    <p class="card-text" v-html="rowdata1.msgpresentation" v-show="rowdata1.statusshortame=='preregistration'"></p>
	<p class="card-text" v-html="rowdata1.msgconfirmationrequest" v-show="rowdata1.statusshortame=='waitanalysis'"></p>
	<p class="card-text" v-html="rowdata1.msgacceptrequest" v-show="rowdata1.statusshortame=='acept'"></p>
	<p class="card-text" v-html="rowdata1.msgrejectionrequest" v-show="rowdata1.statusshortame=='dained'"></p>
	<p class="card-text" v-html="rowdata1.msgqueue" v-show="rowdata1.statusshortame=='inqueue'"></p>
	<p class="card-text" v-html="rowdata1.msgrejectionrequest" v-show="rowdata1.statusshortame=='cancelled'"></p>

	<div class="row">
		<div class="col"></div>
		
		<?php if($urltoaccess){?><div  v-show="rowdata1.statusshortame=='acept'" class="col" ><a  href="<?php echo $urltoaccess;?>" class="form-control btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.request.course.access'); ?></a></div><?php }?>
		<?php if(!$urltoaccess){?><div v-show="rowdata1.statusshortame=='acept'" class="col" ><a  href="<?php echo $urltostudendashboard;?>" class="form-control btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.loginsingin.continue'); ?></a></div><?php }?>
		
	</div>
  </div>
</div>

</div>
