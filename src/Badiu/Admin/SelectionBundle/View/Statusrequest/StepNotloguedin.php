<?php
$urlfinishshopping=2;
?>

 <div class="stepwizard">   
 <div class="stepwizard-row setup-panel">
     <div class="stepwizard-step  col-xs-3">  
         <a href="<?php echo $utilapp->getCurrentUrl();?>" type="button" class="btn btn-warning btn-circle"><i class="fa fa-shopping-cart"></i></a>
         <p><small><?php echo $translator->trans('badiu.financ.ecommerce.cart'); ?></small></p>
     </div>
     <div class="stepwizard-step col-xs-3"> 
         <a @click="finishShopping()" type="button" class="btn btn-primary btn-circle" ><i class="fa fa-user"></i></a>
         <p><small><?php echo $translator->trans('badiu.financ.ecommerce.login.indetify'); ?></small></p>
     </div>
     <div class="stepwizard-step  col-xs-3"> 
         <a href="#" type="button" class="btn btn-secondary btn-circle" disabled="disabled"><i class="fas fa-dollar-sign"></i></a>
         <p><small><?php echo $translator->trans('badiu.financ.ecommerce.checkout.payment'); ?></small></p>
     </div>
      <div class="stepwizard-step  col-xs-3"> 
         <a href="#" type="button" class="btn btn-secondary btn-circle" disabled="disabled"><i class="fas fa-check"></i></a>
         <p><small><?php echo $translator->trans('badiu.financ.ecommerce.finish.step'); ?></small></p>
     </div>
                 
 </div>
</div>

