<?php
$utilapp=$container->get('badiu.system.core.lib.util.app');

$formconfig=new stdClass();
$formconfig->showsubmitrow=false;
$factoryformfilter->setConfig($formconfig);
$iconProcess=$container->get('badiu.theme.core.lib.template.vuejs.factoryutil')->iconProcess();
$badiuSession = $this->container->get('badiu.system.access.session');
$urlrecoverpwd=$utilapp->getUrlByRoute('badiu.system.user.recoverpwd.add',array('parentid'=>$badiuSession->get()->getEntity()));



//$badiuSession->setHashkey($this->getSessionhashkey());
$ssogovbrenable=$badiuSession->getValue('badiu.auth.ssogovbr.formlogin.param.config.enable');

$parentid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
$prefixid="";
if(!empty($parentid)){$prefixid="-".$parentid;}

?>
<br />
<div id="_badiu_theme_base_form_vuejs<?php echo $prefixid;?>"   class="badiu-admin-selection-process badiu-admin-selection-loginsingindoc">
<?php require_once("Step.php");?> 
<div class="row" > 
 
	<div class="col  table-responsive ecommerce-shopping"> 
		<div class="card"  id="card_formlogin">
			<div class="card-header"><?php echo $translator->trans('badiu.admin.selection.loginsingindoc.identify'); ?></div>
			<div class="card-body"><?php echo $factoryformfilter->exec('add');?></div> 
			<div class="card-footer">

			  <div v-if="loginsinginstatus == 'started'">
			 	<button  @click="loginsinginCheckUsername()" type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.loginsingin.continue'); ?></button><?php echo $iconProcess; ?>
			  </div>

			  <div v-else-if="loginsinginstatus == 'hasregister'">
			  	<button  @click="loginsinginExecLogin()" type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.loginsingin.continue'); ?></button><?php echo $iconProcess; ?>
			  </div> 

			  <div v-else-if="loginsinginstatus == 'hasregisteronadd'">
					<button  @click="loginsinginExecLogin()" type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.loginsingin.login'); ?></button><?php echo $iconProcess; ?>
					<button  @click="loginsinginRestourePassword()" type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.loginsingin.recoverpassword'); ?></button>
					<button  @click="loginsinginManageFormView('withoutregister')" type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.loginsingin.registertryaagain'); ?></button>
				</div>
			   <div v-else-if="loginsinginstatus == 'passordnotmatch'">
			   		<button  @click="loginsinginExecLogin()" type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.loginsingin.tryagain'); ?></button><?php echo $iconProcess; ?>
					<button  @click="loginsinginRestourePassword()" type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.loginsingin.recoverpassword'); ?></button>
					<button  @click="loginsinginManageFormView('withoutregister')" type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.loginsingin.register'); ?></button>
			   </div> 
			
			   <div v-else-if="loginsinginstatus == 'withoutregister'">
			   	<button  @click="loginsinginExecSingin()" type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.loginsingin.continue'); ?></button><?php echo $iconProcess; ?>
			   </div>   

				
			</div>
		</div>

		<div class="card" id="card_inforecoverpwd">
			<div class="card-header"><?php echo $translator->trans('badiu.admin.selection.loginsingin.recoveredpassword'); ?></div>
			<div class="card-body">
				<div class="alert alert-success" role="alert"  v-html="messegepasswordsendtoemail"></div>
			</div> 
			<div class="card-footer">

			 
			  	<button  @click="tryNewExecLogin()" type="button" class="btn btn-primary"><?php echo $translator->trans('badiu.admin.selection.loginsingin.loginagain'); ?></button><?php echo $iconProcess; ?>
			  

			</div>
		</div>
		
		<?php if($ssogovbrenable){ 
			$requestid=$container->get('badiu.system.core.lib.http.querystringsystem')->getParameter('requestid');
			$routegoback= "badiu.admin.selection.requestadd.link_$requestid";
			$ssogovbrurlrequest=$utilapp->getUrlByRoute('badiu.auth.ssogovbr.request.link',array('_routeparentidgoback'=>$routegoback));
		?>
						<br /><a class="btn btn-primary btn-block" href="<?php echo $ssogovbrurlrequest;?>" ><?php echo $translator->trans('badiu.auth.ssogovbr.formlogin.button'); ?></a>
		<?php }?>
	</div>
	
 
	
</div>
</div>
