loginsinginManageFormView: function (status) {
	
		this.loginsinginstatus=status;
		 $("#card_formlogin").show();
		 $("#card_inforecoverpwd").hide();
		  
	   if(status== 'started'){
		   $("#item_password").hide();
		   $("#item_email").hide();
		   $("#item_name").hide();
		   $("#item_addpassword").hide();
		   $("#item_personalphonemobile").hide();
		   $("#item_nationalitystatus").hide();
		}
	   else if(status== 'hasregister'){
		   $("#item_password").show();
		   $("#item_email").hide();
		   $("#item_name").hide();
		   $("#item_addpassword").hide();
		   $("#item_personalphonemobile").hide();
		   $("#item_nationalitystatus").hide();
	
		}else if(status== 'hasregisteronadd'){
		   $("#item_password").show();
		   $("#item_email").hide();
		   $("#item_name").hide();
		   $("#item_addpassword").hide();
		   $("#item_personalphonemobile").hide();
		   $("#item_nationalitystatus").hide();
	
		}else if(status== 'withoutregister'){
		   $("#item_password").hide(); 
		   $("#item_email").show();
		   $("#item_name").show();
		   $("#item_addpassword").show();
		   $("#item_personalphonemobile").show();
		   $("#item_nationalitystatus").show();
		   this.formcontrol.message="";
		   this.formcontrol.haserror=false;
		}else if(status== 'addrregisterfailed'){
		   $("#item_password").hide();
		   $("#item_email").show();
		   $("#item_name").show();
		   $("#item_addpassword").show();
		   $("#item_personalphonemobile").show();
		   $("#item_nationalitystatus").show();
		}
	
	 
  },

  loginsinginCheckUsername: function () {
			var self = this;
			this.formcontrol.processing=true;
			
			this.badiuform._service=this.loginsinginservice;
			this.badiuform._function="checkUsername";

			
			   axios.post(this.serviceurl,this.badiuform).then(function (response) {
				   console.log(response.data);
				    if(response.data.status=='accept'){
						self.loginsinginstatus=response.data.message;
						self.loginsinginManageFormView(response.data.message);
						self.formcontrol.processing=false;

					 }else if(response.data.status=='danied'){
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
						
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 
  },
  loginsinginExecLogin: function () {
	  
			var self = this;
			this.formcontrol.processing=true;
			
			this.badiuform._service=this.loginsinginservice;
			this.badiuform._function="execLogin";

			//console.log(this.badiuform);
			   axios.post(this.serviceurl,this.badiuform).then(function (response) {
				      console.log(response.data);
					 if(response.data.status=='accept'){
							self.urlgoback=response.data.message.urlgoback;
							if(!self.isparamempty(self.urlgoback)){window.location.replace(self.urlgoback);}
							self.formcontrol.processing=false;
					 }else if(response.data.status=='danied'){
						self.loginsinginManageFormView('passordnotmatch');
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
				 if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 
  },

  loginsinginExecSingin: function () {

	var self = this;
			this.formcontrol.processing=true;
			
			this.badiuform._service=this.loginsinginservice;
			this.badiuform._function="execSingin";

			//console.log(this.badiuform);
			   axios.post(this.serviceurl,this.badiuform).then(function (response) {
				      console.log(response.data);
					 if(response.data.status=='accept'){
							self.urlgoback=response.data.message.urlgoback;
							if(!self.isparamempty(self.urlgoback)){window.location.replace(self.urlgoback);}
							self.formcontrol.processing=false;
					 }else if(response.data.status=='danied'){
						self.loginsinginManageFormView('withoutregister');
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
						if(response.data.info=='badiu.financ.ecommerce.loginsingin.userjustexistonadd'){self.loginsinginManageFormView('hasregisteronadd');}
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 
  },

  loginsinginRestourePassword: function () {
		
			var self = this;
			this.formcontrol.processing=true;
			
			this.badiuform._service=this.loginsinginservice;
			this.badiuform._function="recoverPassword";

		
			   axios.post(this.serviceurl,this.badiuform).then(function (response) {
				      console.log(response.data);
					 if(response.data.status=='accept'){
						 self.loginsinginstatus=response.data.message.status;
						 self.messegepasswordsendtoemail=response.data.message.message;
						 self.formcontrol.processing=false;
						  
						  $("#card_formlogin").hide();
						  $("#card_inforecoverpwd").show();
						 
					 }else if(response.data.status=='danied'){
						self.loginsinginManageFormView('withoutregister');
						self.formcontrol.status='open';
						self.formcontrol.haserror=true;
						self.formcontrol.processing=false;
						var generalerror=response.data.message.generalerror; 
                  if(generalerror!== undefined){self.formcontrol.message=generalerror;}
                  else{self.formcontrol.message="Ocorreu um erro no sistema";}


					 }
                }).catch(function (error) {
							self.formcontrol.status='open';
                     self.formcontrol.haserror=true;
                     self.formcontrol.message="Ocorreu um erro no sistema";
							console.log(error.response);
				   		if(error.response!== undefined &&  error.response.data!== undefined){
					   		var resperror = error.response.data.match(/<title[^>]*>([^<]+)<\/title>/)[1];
					   		console.log(resperror);
					   	}
					 
                }); 
},

 tryNewExecLogin: function () {
			this.loginsinginManageFormView('hasregister');
			this.badiuform.password="";
			this.formcontrol.haserror=false;
			this.formcontrol.message="";

},