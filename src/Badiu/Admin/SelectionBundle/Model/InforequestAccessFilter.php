<?php

namespace Badiu\Admin\SelectionBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuAccessFilter;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class InforequestAccessFilter extends  BadiuAccessFilter {

	function __construct(Container $container) {
		parent::__construct($container);
	}

       public function exec(){

		//check session
		   $badiuSession = $this->getContainer()->get('badiu.system.access.session');
           if (!$badiuSession->exist()) {return true;}
		   
         $filter=true;
        
          $requestid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
          if(empty($requestid)){echo $this->getTranslator()->trans('badiu.admin.selection.inforeques.message.requestidrequired');exit;}
          
		  $requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
		   $requestinfo=$requestdata->getInfo($requestid);
		 
		  $statusshortname=$this->getUtildata()->getVaueOfArray($requestinfo, 'statusshortname');
		   $ruserid=$this->getUtildata()->getVaueOfArray($requestinfo, 'userid');
		   
		  $badiuSession = $this->getContainer()->get('badiu.system.access.session');
		  //review it
			//	if(!empty($clientsession)){$badiuSession->setHashkey($clientsession);}
		 $isuseranonymous = $badiuSession->get()->getUser()->getAnonymous();
		
		 if(!$isuseranonymous && $ruserid!==$badiuSession->get()->getUser()->getId()){
			 $permission=$this->container->get('badiu.system.access.permission');
			 if(!$permission->has_access("badiu.admin.selection.")){
				 echo $this->getTranslator()->trans('badiu.admin.selection.request.withoupermission');exit;
			 }
		 }
		 
		 //coupon
		 $analysiscriteria=$this->getUtildata()->getVaueOfArray($requestinfo, 'analysiscriteria');
		 $coupon=$this->getUtildata()->getVaueOfArray($requestinfo, 'coupon');
		 $requirecoupon=false;
		 if($statusshortname !='preregistration' && !$isuseranonymous && $analysiscriteria=='badiuadmincoupon' && empty($coupon)){$requirecoupon=true;}
		 
		  
		  if($statusshortname !='preregistration' && !$isuseranonymous && !$requirecoupon){
			  $apputil=$this->getContainer()->get('badiu.system.core.lib.util.app');
				$url=$apputil->getUrlByRoute('badiu.admin.selection.statusrequest.dashboard',array('parentid'=>$requestid));
				header('Location: '.$url); 
				exit;
          }
         
		 
        return $filter;
         } 
   
         
       
}
