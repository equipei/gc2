<?php
namespace Badiu\Admin\SelectionBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;

class LoginsingindocFormController extends BadiuFormController {
    private $userdb;
    function __construct(Container $container) {
        parent::__construct($container);
        $this->userdb=$this->getContainer()->get('badiu.system.user.user.data');
        $this->initParam();
    }

    //delete this function $this->iniParam(); in constructo do same 
    public function init() {
        $this->setParam($this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters());
    }
  
     public function checkUsername() {
		    $this->init();
			
            $validusername=$this->isValidUsername();
            if(!empty($validusername)){return $validusername; }

           //check if exist login
			$requestid=$this->getParamItem('requestid');
           $username=$this->getParamItem('username');
           $param=array('username'=>$username,'entity'=>$this->getEntity());
           $countlogin=$this->userdb->countGlobalRow($param);
           $lmessage="";

           if($countlogin==1){$lmessage='hasregister';}
           else  if($countlogin==0){$lmessage='withoutregister';}
          /* else{
            $this->registerTrasactionEmail($requestid,$username,'generalerror');
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorcheckemail');
            $info='badiu.admin.selection.loginsingindoc.errorcheckemial';
            return $this->getResponse()->denied($info, $message);
           }
           $this->registerTrasactionEmail($requestid,$username,$lmessage);*/
           return $this->getResponse()->accept($lmessage);
            
     }
     public function recoverPassword() {
        $this->init();
        $validusername=$this->isValidUsername();
        if(!empty($validusername)){return $validusername; }

        //change passoword
        $newpwd="";
        for ($i = 0; $i < 8; $i++) { $newpwd .= rand(0, 9); }
        $newpwdmd5=md5($newpwd);
        $paramfilter=array('username'=>$username=$this->getParamItem('username'));
        $userid=$this->userdb->getGlobalColumnValue('id',$paramfilter);
        $userinfo=$this->userdb->getGlobalColumnsValue(" CONCAT(o.firstname,' ',o.lastname) AS fullname ",array('id'=>$userid)); 
        $userfullname =$this->getUtildata()->getVaueOfArray($userinfo, 'fullname');

        //if userid not found
        if(!$userid){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorfinduserid');
            $info='badiu.admin.selection.loginsingindoc.errorfinduserid';
            return $this->getResponse()->denied($info, $message);
        }

         //if email not found
        $email=$this->userdb->getGlobalColumnValue('email',$paramfilter);
        if(!$email){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorfindemail');
            $info='badiu.admin.selection.loginsingindoc.errorfindemail';
            return $this->getResponse()->denied($info, $message);
        }

        //if email not valid
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorfindnotvalid');
            $info='badiu.admin.selection.loginsingindoc.errorfindnotvalid';
            return $this->getResponse()->denied($info, $message);
        }

        $paramuppwd=array('password'=>$newpwdmd5,'id'=>$userid);
        $pwdupresult=$this->userdb->updateNativeSql($paramuppwd,false);
        
        if(!$pwdupresult){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorcreatenewpassword');
            $info='badiu.admin.selection.loginsingindoc.errorcreatenewpassword';
            return $this->getResponse()->denied($info, $message);
        }
        
        //get message template
		$dataemailtemplate=null;
		$subject=null;
		$message=null;
		if($this->getContainer()->has('badiu.admin.selection.emailtemplate.data')){
			$dataemailtemplate=$this->getContainer()->get('badiu.admin.selection.emailtemplate.data');
			$dconfig=$dataemailtemplate->getGlobalColumnValue('value',array('entity'=>$this->getEntity(),'dtype'=>'email','modulekey'=>'badiu.admin.selection.emailtemplate','shortname'=>'badiu.admin.selection.emailtemplate.type.recoverpassword','deleted'=>false));
			$dconfig  = $this->getJson()->decode($dconfig,true);
        	$subject=$this->getUtildata()->getVaueOfArray($dconfig,'subject');
			$message=$this->getUtildata()->getVaueOfArray($dconfig,'message');
    	}
         
        
        
        if(empty($subject)){$subject=$this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.recoverpwdsendmail.subject');}
        if(empty($message)){$message=$this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.recoverpwdsendmail.message');}
        
        $message=str_replace("{NEW_PASSWORD}",$newpwd,$message);
       
        

        $mconfig=array();
        $mconfig['useridto']=$userid;
        $mconfig['userto']=$userfullname;
        $mconfig['recipient']['to']=$email;
        $mconfig['message']['subject']=$subject;
        $mconfig['message']['body']=$message;
        $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
        //$smtp->setUsersender($this->getUsersender());
        $entity=$this->getEntity();
        $smtp->setEntity($entity);
     
		$systemdata=$this->getContainer()->get('badiu.system.core.functionality.systemdata');
		$systemdata->setEntity($this->getEntity());
		$systemdata->init();
		$smtp->setSystemdata($systemdata);
		
        $sendmsg=$smtp->sendMail($mconfig);
      
        if(!$sendmsg){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorsendmailwithnewpassword');
            $info='badiu.admin.selection.loginsingindoc.errorsendmailwithnewpassword';
            return $this->getResponse()->denied($info, $message);
        }else{
            $this->addlogSendmailRecoverPassword($mconfig);
        }
        $message=array('status'=>'passwordsendtoemail','message'=>"Foi gerada uma nova senha e foi enviado para o e-mail $email <br /> Recupere a nova senha enviada no e-mail e logue novamente");
        return $this->getResponse()->accept($message);
       
       
        //send message

        //
         //check if exist login
       /*  $username=$this->getParamItem('username');
         $param=array('username'=>$username);
         $countlogin=$this->userdb->countGlobalRow($param);
         $lmessage="";
         if($countlogin==1){$lmessage='hasregister';}
         else  if($countlogin==0){$lmessage='withoutregister';}
         else{
          $message=array();
          $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorcheckemail');
          $info='badiu.admin.selection.loginsingindoc.errorcheckemial';
          return $this->getResponse()->denied($info, $message);
         }*/

        
    }
     public function execLogin() {
        $this->init();
         $mvalidate=$this->isValidrequestid();
         if ($mvalidate != null) { 
              return $mvalidate;
          }
          $mvalidate=$this->isValidUsername();
         if ($mvalidate != null) { 
              return $mvalidate;
          }
          $mvalidate=$this->isValidPassword();
         if ($mvalidate != null) { 
              return $mvalidate;
          }

          $mvalidate=$this->isUsernamepasswordmatch();
         if ($mvalidate != null) { 
              return $mvalidate;
          }
          
          $autenticated=$this->getParamItem('autenticated');
          $requestid=$this->getParamItem('requestid');
         if($autenticated){
            
			//check duplicate request
			
			//exec request
			$processreq=$this->processRequest();
			if(!empty($processreq)){return $processreq;}
			
			//rediret to confirm request 
			$apputil=$this->getContainer()->get('badiu.system.core.lib.util.app');
			$url=$apputil->getUrlByRoute('badiu.admin.selection.inforequest.dashboard',array('parentid'=>$requestid));
			$outrsult=array('message'=>$autenticated,'urlgoback'=>$url);
			$this->getResponse()->setStatus("accept");
            $this->getResponse()->setMessage($outrsult);
			return $this->getResponse()->get(); 
		   
         }else{
             $message=array();
             $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.failed');
             $info='badiu.admin.selection.loginsingindoc.failed';
             return $this->getResponse()->denied($info, $message);
         }
        
 }
     
 public function execSingin() {
    
    $this->init();
     $mvalidate=$this->isValidrequestid();
     if ($mvalidate != null) { 
          return $mvalidate;
      }
	  
	    $mvalidate=$this->isValidUsername();
         if ($mvalidate != null) { 
              return $mvalidate;
          }
		  
      $mvalidate=$this->isValidEmail();
     if ($mvalidate != null) { 
          return $mvalidate;
      }
      $password=$this->getParamItem('addpassword');
      $password=trim($password);
      $mvalidate=$this->isValidPassword($password);
     if ($mvalidate != null) { 
          return $mvalidate;
      }

     $mvalidate=$this->isValidProfile();
     if ($mvalidate != null) { 
          return $mvalidate;
      }
   
      //check if exist login
      $username=$this->getParamItem('username');
      $param=array('username'=>$username,'entity'=>$this->getEntity());
      $countlogin=$this->userdb->countGlobalRow($param);
     
      if($countlogin > 0){
        $message=array();
        $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.userjustexistonadd');
        $info='badiu.admin.selection.loginsingindoc.userjustexistonadd';
        return $this->getResponse()->denied($info, $message);
       
      }
	  //check doc number
	  $param=array('doctype'=>'CPF','docnumber'=>$username,'entity'=>$this->getEntity());
      $countlogin=$this->userdb->countGlobalRow($param);
      
	   if($countlogin > 0){
        $message=array();
        $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.userjustexisbydocnumber');
        $info='badiu.admin.selection.loginsingindoc.userjustexisbydocnumber';
        return $this->getResponse()->denied($info, $message);
       
      }
	  
      $requestid=$this->getParamItem('requestid');
      $paramtoinsert=$this->getParamItem('paramtoinsert');
      $userid=$this->userdb->insertNativeSql($paramtoinsert,false);
      
     if($userid){
		 $this->addParamItem('id',$userid) ;
		$this->sysFormAfter();
        $reusltauth=$this->loginAfterSingin(); 
        if(!empty($reusltauth)){return $reusltauth;} 
		$processreq=$this->processRequest();
		if(!empty($processreq)){return $processreq;}
			
     }else{
         $message=array();
         $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.failed');
         $info='badiu.admin.selection.loginsingindoc.failed';
         return $this->getResponse()->denied($info, $message);
     }
    
}
    public function changeParam() {
        $param=$this->getParam();
       
        $this->setParam($param);
     }

  
    public function isValidEmail() {
       
        $email=$this->getParamItem('email');
        $email=trim($email);
        
        if(empty($email)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.emailrequired');
            $info='badiu.admin.selection.loginsingindoc.emailrequired';
            return $this->getResponse()->denied($info, $message);
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.emailnotvalid');
            $info='badiu.admin.selection.loginsingindoc.emailnotvalid';
            return $this->getResponse()->denied($info, $message);
        }
		
		//check duplicate email
		 //check if exist login
      $param=array('email'=>$email,'entity'=>$this->getEntity());
      $countemail=$this->userdb->countGlobalRow($param);
     
      if($countemail > 0){
        $message=array();
        $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.emailjustexistonadd');
        $info='badiu.admin.selection.loginsingindoc.emailjustexistonadd';
        return $this->getResponse()->denied($info, $message);
       
      }
   
		
        return null;
    }
     public function isValidUsername() {
       
        $username=$this->getParamItem('username');
        $username=trim($username);
        if(empty($username)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.usernamerequired');
            $info='badiu.admin.selection.loginsingindoc.usernamerequired';
            return $this->getResponse()->denied($info, $message);
        }
		
		 $cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
         $valid = $cpfdata->isValid($username);
         if (!$valid) {
                $message=array();
                $message['generalerror']= $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.cpfnotvalid');
                $info = 'badiu.admin.selection.loginsingindoc.cpfnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
		$username=$cpfdata->clean($username);	
        $this->addParamItem('username',$username);
        return null;
    }

    public function isValidPassword($password=null) {
       if(empty($password)){ $password=$this->getParamItem('password');}
        $password=trim($password);
        if(empty($password)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.passwordrequired');
            $info='badiu.admin.selection.loginsingindoc.passwordrequired';
            return $this->getResponse()->denied($info, $message);
        }
        $this->addParamItem('password',$password);
        return null;
    }

    public function isUsernamepasswordmatch() {
        $username=$this->getParamItem('username');
        $password=$this->getParamItem('password');
        $username=trim($username);
        $password=trim($password);
        $usernamepasswordmatch=$this->getContainer()->get('badiu.auth.core.login.manual')->exec($username,$password);
        if(!$usernamepasswordmatch){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.usernamepasswordnotmatch');
            $info='badiu.admin.selection.loginsingindoc.usernamepasswordnotmatch';
            return $this->getResponse()->denied($info, $message);
        }
        $param=$this->getParam();
        $param['autenticated']=TRUE;
        $this->setParam($param);
       
        return null;
    }
   
    public function loginAfterSingin() {
        $username=$this->getParamItem('username');
        $password=$this->getParamItem('addpassword');
        $username=trim($username);
        $password=trim($password);
        $usernamepasswordmatch=$this->getContainer()->get('badiu.auth.core.login.manual')->exec($username,$password);
        if(!$usernamepasswordmatch){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.failedloginaftersingin');
            $info='badiu.admin.selection.loginsingindoc.failedloginaftersingin';
            return $this->getResponse()->denied($info, $message);
        }
        $param=$this->getParam();
        $this->setParam($param);

        return null;
    }
    public function isValidProfile() {
        $name=$this->getParamItem('name');
        $addpassword=$this->getParamItem('addpassword');
        $nationalitystatus=$this->getParamItem('nationalitystatus');
        $username=$this->getParamItem('username');
		$email=$this->getParamItem('email');
        $name=trim($name);
        if(empty($name)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.namerequired');
            $info='badiu.admin.selection.loginsin.namerequired';
            return $this->getResponse()->denied($info, $message);
        }
      
        if(empty($addpassword)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.passwordrequired');
            $info='badiu.admin.selection.loginsin.addpasswordrequired';
            return $this->getResponse()->denied($info, $message);
        }
       

        $peple=$this->getContainer()->get('badiu.system.user.user.util')->splitFirtnameLastname($name);
        if(empty($peple->lastname)){$peple->lastname=" ";}
        $param=$this->getParam();

        $haspassword=md5(trim($addpassword)); 
		$contactdata=$this->getContactdata();
		$this->setKey('badiu.systeam.user.user.add');
		$this->sysFormBefore();
		$adminformattemptid=$this->getParamItem('adminformattemptid');
        $paramtoinsert=array('firstname'=>$peple->firtname,'lastname'=>$peple->lastname,'email'=> trim($email),'username'=>trim($username),'password'=>$haspassword,'nationalitystatus'=>$nationalitystatus,'timecreated'=>new \DateTime(),'deleted'=>FALSE,'entity'=>$this->getEntity(),'confirmed'=>1,'doctype'=>'CPF','docnumber'=>$username,'contactdata'=>$contactdata,'adminformattemptid'=>$adminformattemptid);
        
		$paramtoinsert['city']=$this->getParamItem('city');
		$paramtoinsert['state']=$this->getParamItem('state');
		$paramtoinsert['dateofbirth']=$this->getContainer()->get('badiu.system.core.lib.form.castdatedefault')->convertToDbDate($this->getParamItem('dateofbirth_badiudatedefault')); 
		//$paramtoinsert['statusid']=$this->getParamItem('statusid');
		
		
		$param['paramtoinsert']=$paramtoinsert;
        $this->setParam($param);
        return null;
    }
    public function isValidrequestid() {
       $requestid=$this->getParamItem('requestid');
       
        if(empty($requestid)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.requestidrequired');
            $info='badiu.admin.selection.loginsingindoc.requestidrequired';
            return $this->getResponse()->denied($info, $message);
        }
      
        return null;
    }
   
    

    private function addlogSendmailRecoverPassword($mconfig) {
        $targetid =$this->getUtildata()->getVaueOfArray($mconfig, 'useridto');
        $message= $this->getJson()->encode($mconfig);
  
       $dloglib=$this->getContainer()->get('badiu.system.log.core.lib');
        $param=array();
       $param['action']='exec';
       $param['userid']=$targetid;
       $param['entity']=$this->getEntity();
       $param['dtype']='out';
       $param['clientdevice']=null;
       $param['modulekey']='badiu.admin.selection.core';
       $param['moduleinstance']=$this->getParamItem('requestid');
       $param['rkey']='badiu.admin.selection.loginsingin';
       $param['param']=$message;
       $param['dinfo']=null;
       $param['dnavegation']=null;
       $param['targetid']= $targetid;
       $dloglib->add($param);
       
   }
   

   private function processRequest() {
         $reqlinkcontroller= $this->getContainer()->get('badiu.admin.selection.inforequest.linkcontroller');
         $requestid=$this->getParamItem('requestid');
		 $fparam=array('requestid'=>$requestid);
		 $reqlinkcontroller->setParam($fparam);
		 
		 $radduser=$reqlinkcontroller->addUserToRequest();
		 if(!empty($radduser)){return $radduser;}
		 
		 $reqlinkcontroller->keepOneActiveRequest();
		 $this->addParamItem('requestid',$reqlinkcontroller->getParamItem('requestid'));
		  
		 $nextstatus=$reqlinkcontroller->getNextStatus();
		 if(!empty($nextstatus)){return $nextstatus;}
		 $newstatusid=$reqlinkcontroller->getParamItem('newstatusid');
		 $this->addParamItem('newstatusid',$newstatusid);
		  
		  //check vaidate newstatusid
		 //request change status
		 $requestlib=$this->getContainer()->get('badiu.admin.selection.request.lib');
		 $param=array('requestid'=>$requestid,'newstatusid'=>$newstatusid);
		 $rchangstatus=$requestlib->changeStatus($param);
		 
		$rnstatus=$reqlinkcontroller->nextStep();
		if(!empty($rnstatus)){return $rnstatus;}
        return $result;
   }
   
   private function getContactdata(){
	    
        
		$personalcontact                      = array();
        $personalcontact['postcode']          = "";
        $personalcontact['address']           = "";
        $personalcontact['addressnumber']     = "";
        $personalcontact['addresscomplement'] = "";
        $personalcontact['neighborhood']      = "";
        $personalcontact['city']              = "";
        $personalcontact['stateid']           = "";
        $personalcontact['state']             = "";
        $personalcontact['stateshortname']    = "";
  
        $personalcontact['country']  = "";

        $personalcontact['phone']       = "";
        $personalcontact['phonemobile'] = $this->getParamItem('personalphonemobile');
        $personalcontact['email']       = $this->getParamItem('email');
		
		$contact                 = array();
        $contact['personal']     = $personalcontact;
        $contact['professional'] = "";
		$contactdatajsont        = $this->getJson()->encode($contact);
		return $contactdatajsont;
		
	}
}
