<?php

namespace Badiu\Admin\SelectionBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuLinkController;
class ResquestLinkController extends BadiuLinkController{

    function __construct(Container $container) {
            parent::__construct($container);
          
        }
    
        public function addNew($param=null){
			if(empty($param)){$this->init();}
            else{$this->setParam($param);}
			 $projectid=$this->getParamItem('projectid');
		
			$checkproject=$this->isProjectValid();
            if($checkproject != null){return $checkproject;}
			
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			$sessionData=$badiuSession->get();
			
			$isuseranonymous = $sessionData->getUser()->getAnonymous();
			$userid=null;
			if(!$isuseranonymous){
				$userid=$sessionData->getUser()->getId();
				 $this->addParamItem('userid',$userid);
			}
			 $projectid=$this->getParamItem('projectid');
			 $userid=$this->getParamItem('userid');
			 
			$requestlib=$this->getContainer()->get('badiu.admin.selection.request.lib');
			$param=array('projectid'=>$projectid,'userid'=>$userid);
			$requestid=$requestlib->add($param);
		
			$apputil=$this->getContainer()->get('badiu.system.core.lib.util.app');
			$url=$apputil->getUrlByRoute('badiu.admin.selection.inforequest.dashboard',array('parentid'=>$requestid));
			$this->setUrlgoback($url);
			$outrsult=array('result'=>$requestid,'message'=>$this->getSuccessmessage(),'urlgoback'=>$this->getUrlgoback());
			$this->getResponse()->setStatus("accept");
            $this->getResponse()->setMessage($outrsult);
			return $this->getResponse()->get(); 
				
		
        }
       
	    public function requestEnrol($param=null){
			if(!empty($param) && is_array($param)){$this->setParam($param);}
            else {$this->init();}
			
			
			$requestid=$this->getParamItem('requestid');
			//validate request
			
			
			//check coupon
			$ckcoupo=$this->checkCoupon();
			 if(!empty($ckcoupo)){return $ckcoupo;}
			 
			
			//send to login if is anonimous
			 $lsigin=$this->loginSingin();
			 if(!empty($lsigin)){return $lsigin;}
			 
			
			
			//add user if is logguein
			 $radduser=$this->addUserToRequest();
			 if(!empty($radduser)){return $radduser;}
			
			//check restriction
			$checkrestriction=$this->checkRestriction();
			if(!empty($checkrestriction)){return $checkrestriction;}
			
			$this->keepOneActiveRequest();
			
			 //get new status
			 $nextstatus=$this->getNextStatus();
			 if(!empty($nextstatus)){return $nextstatus;}
			
			//request change status
			$newstatusid=$this->getParamItem('newstatusid');
			$requestlib=$this->getContainer()->get('badiu.admin.selection.request.lib');
			$param=array('requestid'=>$requestid,'newstatusid'=>$newstatusid);
			$rchangstatus=$requestlib->changeStatus($param);
			
			
			$rnstatus=$this->nextStep();
			if(!empty($rnstatus)){return $rnstatus;}
		
			
            /*$outrsult=array('result'=>$this->getResultout(),'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
            $this->getResponse()->setStatus("accept");
            $this->getResponse()->setMessage($result);
            return $this->getResponse()->get();*/
		}
		
	   public function changeStatus(){
            $this->init();
			
			 $requestid=$this->getParamItem('requestid');
			 $newstatusid=$this->getParamItem('newstatusid');
			$requestlib=$this->getContainer()->get('badiu.admin.selection.request.lib');
			$param=array('requestid'=>$requestid,'newstatusid'=>$newstatusid);
			$result=$requestlib->changeStatus($param);
		
			
            $outrsult=array('result'=>$this->getResultout(),'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
            $this->getResponse()->setStatus("accept");
            $this->getResponse()->setMessage($result);
            return $this->getResponse()->get();
		}
   
		public function checkRestriction(){
			 $requestid=$this->getParamItem('requestid');
			 $requestlib=$this->getContainer()->get('badiu.admin.selection.request.lib');
			 $param=array('requestid'=>$requestid);
			 $msg=$requestlib->checkRestriction($param);
			 if(!empty($msg)){
				   $apputil=$this->getContainer()->get('badiu.system.core.lib.util.app');
				   $url=$apputil->getUrlByRoute('badiu.admin.selection.inforequest.dashboard',array('parentid'=>$requestid));
					$outrsult=array('message'=>null,'urlgoback'=>$url);
					$this->getResponse()->setStatus("accept");
					$this->getResponse()->setMessage($outrsult);
					return $this->getResponse()->get(); 
			 }
			return null;
		}
       
	   public function urlAccess(){
            $this->init();
			$requestid=$this->getParamItem('requestid');
			$requestlib=$this->getContainer()->get('badiu.admin.selection.request.lib');
			$param=array('requestid'=>$requestid);
			
			$result=$requestlib->getUrlAccess($param);
	
			if(!empty($result)){
				$outrsult=array('result'=>$this->getResultout(),'message'=>$this->getSuccessmessage(),'urlgoback'=>$result);
				$this->getResponse()->setStatus("accept");
				$this->getResponse()->setMessage($outrsult);
				return $this->getResponse()->get();
			}else{
				//send message error
			}
            
		}
		
         public function isProjectValid() {
            $projectid=$this->getParamItem('projectid');
          
            return null;
        }

	public function isRequestValid() {
            $requestid=$this->getParamItem('requestid');
			
			//is guest
			
			//status
			
			//period enrol
			
			//
          
            return null;
        }
	 public function addUserToRequest() {
            $requestid=$this->getParamItem('requestid');
          
		   //check userid 
		   $badiuSession = $this->getContainer()->get('badiu.system.access.session');
		   if(!empty($clientsession)){$badiuSession->setHashkey($clientsession);}
		   $sessionData=$badiuSession->get();
			
			$isuseranonymous = $sessionData->getUser()->getAnonymous();
			$userid=null;
			
			if(!$isuseranonymous){
				
				$userid=$sessionData->getUser()->getId();
				$fparam=array('userid'=>$userid,'requestid'=>$requestid);
				$requestlib=$this->getContainer()->get('badiu.admin.selection.request.lib');
				$requestlib->setUserToanonymousRequest($fparam);
				$this->addParamItem('userid',$userid);
			}else {
				//redirect do logguin
			}
			
			return null;
        
}

	public function getNextStatus(){
		 $requestid=$this->getParamItem('requestid');
		 $fparam=array('requestid'=>$requestid);
		 $requestlib=$this->getContainer()->get('badiu.admin.selection.request.lib');
		 $nextsatus=$requestlib->getNextStatus($fparam);
		 $this->addParamItem('newstatusid',$nextsatus);
		
		return  null;
	}
	
	public function nextStep(){
		 $requestid=$this->getParamItem('requestid');
		 $requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
		 $requestinfo=$requestdata->getInfo($requestid);
		
		 $statusshortname=$this->getUtildata()->getVaueOfArray($requestinfo, 'statusshortname');
		 if($statusshortname=='preregistration'){  
				$info = 'badiu.admin.selection.inforequest.useridrequired';
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.inforequest.message.useridrequired');
                return $this->getResponse()->denied($info, $message);
		 }else{
			 $apputil=$this->getContainer()->get('badiu.system.core.lib.util.app');
			$url=$apputil->getUrlByRoute('badiu.admin.selection.statusrequest.dashboard',array('parentid'=>$requestid));
			$outrsult=array('result'=>$this->getResultout(),'message'=>$this->getSuccessmessage(),'urlgoback'=>$url);
            $this->getResponse()->setStatus("accept");
            $this->getResponse()->setMessage($outrsult);
            return $this->getResponse()->get();
			 
		 }
		
		return  null;
	}
	
	public function loginSingin(){
			$requestid=$this->getParamItem('requestid');
			$clientsession=$this->getParamItem('_clientsession');
            $apputil=$this->getContainer()->get('badiu.system.core.lib.util.app');
			
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			if(!empty($clientsession)){$badiuSession->setHashkey($clientsession);}
			$isuseranonymous = $badiuSession->get()->getUser()->getAnonymous();
			
			$singinroute= $badiuSession->getValue('badiu.admin.selection.singin.param.config.defaultroute');
			if(empty($singinroute)){$singinroute='badiu.admin.selection.loginsingin.add';}
			
	 
			if($this->getClientsession()->anable && !$this->getClientsession()->userislogin){ 
				$url=$this->getClientsessionUrl(array('_key'=>$singinroute,'requestid'=>$requestid));
				$outrsult=array('result'=>$this->getResultout(),'message'=>$this->getSuccessmessage(),'urlgoback'=>$url);
				$this->getResponse()->setStatus("accept");
                $this->getResponse()->setMessage($outrsult);
			    return $this->getResponse()->get(); 
			}
		if($isuseranonymous){ 
			
				$url=$apputil->getUrlByRoute($singinroute,array('requestid'=>$requestid));
				$outrsult=array('result'=>$this->getResultout(),'message'=>$this->getSuccessmessage(),'urlgoback'=>$url);
				$this->getResponse()->setStatus("accept");
                $this->getResponse()->setMessage($outrsult);
			    return $this->getResponse()->get(); 
			}
		return  null;
	}
	
		public function keepOneActiveRequest(){
			$requestid=$this->getParamItem('requestid');
			$param=array('requestid'=>$requestid);
			$requestlib=$this->getContainer()->get('badiu.admin.selection.request.lib');
			$newrequestid=$requestlib->keepOneActiveRequest($param);
			if(!empty($newrequestid)){$this->addParamItem('requestid',$newrequestid);}
		}
		
		public function checkCoupon(){
			$requestid=$this->getParamItem('requestid');
			$requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
			$requestinfo=$requestdata->getInfo($requestid);
			$analysiscriteria=$this->getUtildata()->getVaueOfArray($requestinfo,'analysiscriteria');
			if($analysiscriteria!='badiuadmincoupon'){return null;} 
			
			$coupon=$this->getParamItem('coupon');
			$coupon=trim($coupon);
			if(empty($coupon)){
				$message=array();
				$message['coupon'] = $this->getTranslator()->trans('badiu.admin.coupon.message.required');
				$info='badiu.admin.selection.inforequest.couponerror';
				return $this->getResponse()->denied($info, $message);
				return $this->getResponse()->get();
			}
			//check if coupon is valid
			$coupondata=$this->getContainer()->get('badiu.admin.coupon.coupon.data');
			$entity=$this->getEntity();
			$couponinfo=$coupondata->getInfo($entity,$coupon);
			
			$couponid=$this->getUtildata()->getVaueOfArray($couponinfo,'id');
			$timeuse=$this->getUtildata()->getVaueOfArray($couponinfo,'timeuse');
			$timestart=$this->getUtildata()->getVaueOfArray($couponinfo,'timestart');
			$timeend=$this->getUtildata()->getVaueOfArray($couponinfo,'timeend');
			$statusshortname=$this->getUtildata()->getVaueOfArray($couponinfo,'statusshortname');
			$modulekey=$this->getUtildata()->getVaueOfArray($couponinfo,'modulekey');
			$moduleinstance=$this->getUtildata()->getVaueOfArray($couponinfo,'moduleinstance');
			
			$pmodulekey=$this->getUtildata()->getVaueOfArray($requestinfo,'modulekey');
			$pmoduleinstance=$this->getUtildata()->getVaueOfArray($requestinfo,'moduleinstance');
			
			
			$messegefailed=null;
			if(empty($couponid)){$messegefailed=$this->getTranslator()->trans('badiu.admin.coupon.message.notfound');}
			else if(($modulekey=='badiu.tms.offer.classe' || $modulekey=='badiu.tms.offer.classe') && ($modulekey==$pmodulekey) && ($pmoduleinstance!=$moduleinstance) ){$messegefailed=$this->getTranslator()->trans('badiu.admin.coupon.message.notjoinedclasse');}
			else if($statusshortname=='expired'){$messegefailed=$this->getTranslator()->trans('badiu.admin.coupon.message.expired');}
			else if($statusshortname=='canceled'){$messegefailed=$this->getTranslator()->trans('badiu.admin.coupon.message.canceled');}
			else if($statusshortname=='limitreached'){$messegefailed=$this->getTranslator()->trans('badiu.admin.coupon.message.badiu.admin.coupon.message.limiteuseover');}
			else if($statusshortname=='used'){$messegefailed=$this->getTranslator()->trans('badiu.admin.coupon.message.used');}
			else if(!empty($timestart) && $timestart->getTimestamp() > time()){$messegefailed=$this->getTranslator()->trans('badiu.admin.coupon.message.avaliblefuturedate');	}
			else if(!empty($timeend) && $timeend->getTimestamp() < time()){$messegefailed=$this->getTranslator()->trans('badiu.admin.coupon.message.expired');	}
			
			if(!empty($messegefailed)){
				$message=array();
				$message['coupon'] = $messegefailed;
				$info='badiu.admin.selection.inforequest.couponerror';
				return $this->getResponse()->denied($info, $message);
				return $this->getResponse()->get();
			}
			
			$upparam=array('id'=>$requestid,'coupon'=>$coupon);
			$requestdata->updateNativeSql($upparam,false);
			return null;
			
		}
}