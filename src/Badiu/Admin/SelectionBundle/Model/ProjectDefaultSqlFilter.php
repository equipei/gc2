<?php
namespace Badiu\Admin\SelectionBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class ProjectDefaultSqlFilter extends BadiuSqlFilter{
    /**
     * @var object
     */
   private  $sqlservice;
   

    function __construct(Container $container) {
            parent::__construct($container);
     }

        function filterlimitacceptexhausted() {
			$sql="";
			$param= $this->getUtildata()->getVaueOfArray($this->getParam(),'limitacceptexhausted');
            $sql=" ";
			$operator=null;	
			if($param==1){$operator=" >= ";}
			if($param=="0"){$operator=" < ";}

			if(!empty($operator)){
				$sql=" AND o.limitaccept > 0 AND (SELECT COUNT(o2.id) FROM BadiuAdminSelectionBundle:AdminSelectionRequest o2 JOIN o2.statusid s2 WHERE o2.projectid=o.id AND s2.shortname='acept') $operator o.limitaccept ";
			}
            return $sql;
        }
     
          
}
