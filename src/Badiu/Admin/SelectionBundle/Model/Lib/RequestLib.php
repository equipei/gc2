<?php

namespace Badiu\Admin\SelectionBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class RequestLib extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }
#system/service/process?_service=badiu.admin.selection.request.lib&_function=batchChangeStatus&currentrequeststatusid=5&newrequeststatusid=3
	public function batchChangeStatus() {
		exit;
		$currentrequeststatusid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('currentrequeststatusid');
		$newrequeststatusid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('newrequeststatusid');
		
		$requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
		$fparam=array('statusid'=>$currentrequeststatusid,'entity'=>$this->getEntity());
		$list=$requestdata->getGlobalColumnValues('id',$fparam);
		$cont=0;		
		foreach ($list as $row) {
			  $requestid=$this->getUtildata()->getVaueOfArray($row,'id');
			  $param=array('requestid'=>$requestid,'newstatusid'=>$newrequeststatusid);
			  $result=$this->changeStatus($param);
			  if($result){$cont++;}
		 }
		return $cont;
	}

    public function add($param) {
      
	   $userid=$this->getUtildata()->getVaueOfArray($param, 'userid');
	   $projectid=$this->getUtildata()->getVaueOfArray($param, 'projectid');
	
	   if(!empty($userid)){
		   $requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
		   $entity=$this->getEntity();
		   $countreq=$requestdata->countUserExist($entity,$userid,$projectid);
		   if($countreq > 0){
			   $lid=$requestdata->getLastIdCreated($entity,$userid,$projectid,'acept');
				if(!empty($lid)){return $lid;}
				$lid=$requestdata->getLastIdCreated($entity,$userid,$projectid,'waitanalysis');
				if(!empty($lid)){return $lid;}
				$lid=$requestdata->getLastIdCreated($entity,$userid,$projectid,'preregistration');
				if(!empty($lid)){return $lid;}
				$lid=$requestdata->getLastIdCreated($entity,$userid,$projectid,'inqueue');
				if(!empty($lid)){return $lid;}
		   } 
		   
	   }
	   $statusid=null;
	   $statusinfo=null;
	   $description=null;
	  
		//check time validate
		$pfparam=array('id'=>$projectid);
		$projectdata=$this->getContainer()->get('badiu.admin.selection.project.data');
		$projectdto=$projectdata->getGlobalColumnsValue('o.requesttimestart,o.requesttimeend',$pfparam);
		$requesttimestart=$this->getUtildata()->getVaueOfArray($projectdto, 'requesttimestart');
		$requesttimeend=$this->getUtildata()->getVaueOfArray($projectdto, 'requesttimeend');
		
		$now=new \DateTime();
		$now=$now->getTimestamp();
		$istimestarok=true;
		$istimeendok=true;
		//check time start
		if (!empty($requesttimestart) && is_a($requesttimestart, 'DateTime')) {
			$tstartnumber=$requesttimestart->getTimestamp();
			
			if($tstartnumber > $now ){
				 $statusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'inaccessible'); 
				 $statusinfo='requestbeforedate';
				 $description= $this->getTranslator()->trans('badiu.admin.selection.request.requestbeforedate');
				 $istimeendok=false;
			}
		}
		
		//check time end
		if ($istimestarok && !empty($requesttimeend) && is_a($requesttimeend, 'DateTime')) {
			$tendnumber=$requesttimeend->getTimestamp();
			
			if($tendnumber < $now ){
				 $statusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'inaccessible'); 
				 $statusinfo='requestafterdate';
				$description= $this->getTranslator()->trans('badiu.admin.selection.request.requestafterdate');
				 $istimeendok=false;
			}
		}
		
		if(empty($statusid)){
			$statusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'preregistration'); 
		}
		
		$requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
	    $paramadd=array('entity'=>$this->getEntity(),'projectid'=>$projectid,'statusid'=>$statusid,'description'=>$description,'deleted'=>0,'timerequest'=>new \DateTime(),'timecreated'=>new \DateTime());
		if(!empty($statusinfo)){$paramadd['statusinfo']=$statusinfo;}
	   	if(!empty( $userid)){$paramadd['userid']=$userid;}
     
	   $dresult=$requestdata->insertNativeSql($paramadd,false);
	  
       return   $dresult;
    }  
	public function setUserToanonymousRequest($param) {
		$userid=$this->getUtildata()->getVaueOfArray($param, 'userid');
		$requestid=$this->getUtildata()->getVaueOfArray($param, 'requestid');
		
		if(empty($userid)){return null;}
		if(empty($requestid)){return null;}
		
		$requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
		$requestinfo=$requestdata->getInfo($requestid);
		
		$preruserid=$this->getUtildata()->getVaueOfArray($requestinfo, 'userid');
		$result=null;
		if(empty($preruserid)){
			$paramedit=array('userid'=>$userid,'id'=>$requestid);
			$result=$requestdata->updateNativeSql($paramedit,false);
		}
		return $result;
	}
	
	public function getNextStatus($param) {
		$requestid=$this->getUtildata()->getVaueOfArray($param, 'requestid');
		$requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
		$entity=$this->getEntity();
		$requestinfo=$requestdata->getInfo($requestid);
		
		$statusshortname=$this->getUtildata()->getVaueOfArray($requestinfo, 'statusshortname');
		$analysiscriteria=$this->getUtildata()->getVaueOfArray($requestinfo, 'analysiscriteria');
		$servicecheckcriteria=$this->getUtildata()->getVaueOfArray($requestinfo, 'servicecheckcriteria');
		
		$projectid=$this->getUtildata()->getVaueOfArray($requestinfo, 'projectid');
		$limitaccept=$this->getUtildata()->getVaueOfArray($requestinfo, 'limitaccept');
		$listdocumentnumber=$this->getUtildata()->getVaueOfArray($requestinfo, 'listdocumentnumber');
		$newstatusid=null;
		
		//status preregistration
		if($statusshortname=='preregistration'){
			
			//check limitaccept
			if(!empty($limitaccept) && $limitaccept > 0){
				$requestfill=$requestdata->countAccept($entity,$projectid,'acept');
				if($requestfill >=$limitaccept ){
					$newstatusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'inqueue');
					return $newstatusid;
				}
			}
			
			if($analysiscriteria=='manual'){$newstatusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'waitanalysis');}
			else if($analysiscriteria=='automaticregistration'){$newstatusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'acept');}
			else if($analysiscriteria=='listdocumentnumber'){
				if(empty($listdocumentnumber)){
					$newstatusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'inqueue');
					return $newstatusid;
				}
				$listdocumentnumber=str_replace(".","",$listdocumentnumber);
				$listdocumentnumber=str_replace("-","",$listdocumentnumber);
				$listdocumentnumber=str_replace(" ","",$listdocumentnumber);
				
				$userdoctype=$this->getUtildata()->getVaueOfArray($requestinfo, 'userdoctype');
				$userdocnumber=$this->getUtildata()->getVaueOfArray($requestinfo, 'userdocnumber');
				if($userdoctype!='CPF'){
					$newstatusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'dained');
					return $newstatusid;
				}
				if(empty($userdocnumber)){
					$newstatusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'dained');
					return $newstatusid;
				}
				$cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
				$validcpf = $cpfdata->isValid($userdocnumber);
				if(!$validcpf){
					$newstatusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'dained');
					return $newstatusid;
				}
				
				$userdocnumber = $cpfdata->clean($userdocnumber);
				$pos=stripos($listdocumentnumber, ",");
				$listdoc=array();
                if($pos=== false){
                  $listdoc=array($listdocumentnumber);
				}else{
                  $listdoc= explode(",", $listdocumentnumber);
				}
				if (in_array($userdocnumber, $listdoc)) {
					$newstatusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'acept');
					return $newstatusid;
				}else{
					$newstatusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'dained');
					return $newstatusid;
				}
				
			}
			else if($analysiscriteria=='automaticservicecheck'){
				if($this->getContainer()->has($servicecheckcriteria)){
					$fparam=array('requestid'=>$requestid);
					$service=$this->getContainer()->get($servicecheckcriteria);
					$service->setParam($fparam);
					$newstatusid=$service->exec();
				}
			}else if($analysiscriteria=='badiuadmincoupon'){
				$coupon=$this->getUtildata()->getVaueOfArray($requestinfo,'coupon');
				$coupondata=$this->getContainer()->get('badiu.admin.coupon.coupon.data');
				$entity=$this->getEntity();
				$couponinfo=$coupondata->getInfo($entity,$coupon);
				$couponid=$this->getUtildata()->getVaueOfArray($couponinfo,'id');
				 
				$statusid=$this->getContainer()->get('badiu.admin.coupon.status.data')->getGlobalColumnValue('id',array('entity'=>$entity,'shortname'=>'used'));
				$upparam=array('id'=>$couponid,'statusid'=>$statusid);
				$coupondata->updateNativeSql($upparam,false);
				$newstatusid=$this->getContainer()->get('badiu.admin.selection.status.data')->getIdByShortname($this->getEntity(),'acept');
			}
		}
		 return $newstatusid;
	 }

 public function checkRestriction($param) {
	  $requestid=$this->getUtildata()->getVaueOfArray($param, 'requestid');
	 
	  if(empty($requestid)){return null;}
	  $requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
	    $requestinfo=$requestdata->getInfo($requestid);
		
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		$badiuSession->setHashkey($this->getSessionhashkey());
		$serviceinforequestcheckrestriction = $badiuSession->getValue('badiu.admin.selection.request.param.config.serviceinforequestcheckrestriction');
		$messagerestriction=null;

		if(!empty($serviceinforequestcheckrestriction) && $this->getContainer()->has($serviceinforequestcheckrestriction)){
			$serviceretriction=$this->getContainer()->get($serviceinforequestcheckrestriction); 
			$messagerestriction=$serviceretriction->exec($requestinfo);
		}
		return $messagerestriction;
 }
 
 public function changeStatus($param) {
	  $requestid=$this->getUtildata()->getVaueOfArray($param, 'requestid');
	  $newstatusid=$this->getUtildata()->getVaueOfArray($param, 'newstatusid');
	  $requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
	  
	  if(empty($newstatusid)){return null;}
	 
	  //update status
	  $paramedit=array('statusid'=>$newstatusid,'id'=>$requestid,'timeanalyzed'=>new \DateTime(),'timemodified'=>new \DateTime());
	  
	 
	  $result=$requestdata->updateNativeSql($paramedit,false);
	  
	   $requestinfo=$requestdata->getInfo($requestid);
	   $projectid=$this->getUtildata()->getVaueOfArray($requestinfo, 'projectid');
	   $userid=$this->getUtildata()->getVaueOfArray($requestinfo, 'userid');
	   $statusshortname=$this->getUtildata()->getVaueOfArray($requestinfo, 'statusshortname');
	   $defaultstatus=$this->getUtildata()->getVaueOfArray($requestinfo, 'defaultstatus');
	   $defaultrole=$this->getUtildata()->getVaueOfArray($requestinfo, 'defaultrole');
	   
	   $modulekey=$this->getUtildata()->getVaueOfArray($requestinfo, 'modulekey');
	   $moduleinstance=$this->getUtildata()->getVaueOfArray($requestinfo, 'moduleinstance');
	   
	   if($statusshortname=='acept'){
		   //process service
			$servicekey=$modulekey.'.lib.selectionrequest';
	
			if($this->getContainer()->has($servicekey)){
				$sparam=array('userid'=>$userid,'moduleinstance'=>$moduleinstance,'modulekey'=>$modulekey,'defaultrole'=>$defaultrole,'defaultstatus'=>$defaultstatus);
				$this->getContainer()->get($servicekey)->exec($sparam);
				$this->updateSession($userid);
			}
		
		}  
	
		//send email
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
		$badiuSession->setHashkey($this->getSessionhashkey());
		$servicesendmailafter=$badiuSession->getValue('badiu.admin.selection.request.param.config.servicesendmailafter');
		if(!empty($servicesendmailafter) && $this->getContainer()->has($servicesendmailafter)){
			$servicesendmail=$this->getContainer()->get($servicesendmailafter);
			$servicesendmail->setParam($param);
			$servicesendmail->exec();
		}
		
	  return $result;
	}

	public function updateSession($userid) {
		if(empty($userid)){return null;}
		//if same user restart session
		$badiuSession=$this->getContainer()->get('badiu.system.access.session');
		$badiuSession->setHashkey($this->getSessionhashkey());
		$dsession = $badiuSession->get();
		if($dsession->getUser()->getId()!=$userid){return null;}

		//restart session user
		$userdata=$this->getContainer()->get('badiu.system.user.user.data');
		$param=$userdata->getGlobalColumnsValue('o.id AS userid,o.entity,o.firstname,o.lastname,o.email,o.username ',array('id'=>$userid));
		
	   // $param=array('userid'=>$user->getId(),'entity'=>$user->getEntity(),'firstname'=>$user->getFirstname(),'lastname'=>$user->getLastname(),'email'=>$user->getEmail(),'username'=>$user->getUsername());
		$this->getContainer()->get('badiu.system.access.session')->start($param);
	}
 public function getUrlAccess($param) {
	
	 $requestid=$this->getUtildata()->getVaueOfArray($param, 'requestid');
	 if(empty($requestid)){return null;}
	 
	   $requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
	   $requestinfo=$requestdata->getInfo($requestid);
	   $projectid=$this->getUtildata()->getVaueOfArray($requestinfo, 'projectid');
	   $userid=$this->getUtildata()->getVaueOfArray($requestinfo, 'userid');
	   $statusshortname=$this->getUtildata()->getVaueOfArray($requestinfo, 'statusshortname');
	   
	   $modulekey=$this->getUtildata()->getVaueOfArray($requestinfo, 'modulekey');
	   $moduleinstance=$this->getUtildata()->getVaueOfArray($requestinfo, 'moduleinstance');
	   $defaultstatus=$this->getUtildata()->getVaueOfArray($requestinfo, 'defaultstatus');
	   $defaultrole=$this->getUtildata()->getVaueOfArray($requestinfo, 'defaultrole');
	   $result=null;
	   if($statusshortname=='acept'){
		   //process service
		
			$servicekey=$modulekey.'.lib.selectionrequest';
			
	  		if($this->getContainer()->has($servicekey)){
				$sparam=array('userid'=>$userid,'moduleinstance'=>$moduleinstance,'modulekey'=>$modulekey,'defaultrole'=>$defaultrole,'defaultstatus'=>$defaultstatus);
				$result=$this->getContainer()->get($servicekey)->getUrl($sparam);
			}
		}
		
	  return $result;
	}

public function automaticCancelRequest($param) {
		 $requestid=$this->getUtildata()->getVaueOfArray($param, 'requestid');
		 $activerequestid=$this->getUtildata()->getVaueOfArray($param, 'activerequestid');
		 $statusdata=$this->getContainer()->get('badiu.admin.selection.status.data');
		 $cancelledid=$statusdata->getIdByShortname($this->getEntity(),'cancelled');
		 $description=$this->getTranslator()->trans('badiu.admin.selection.request.cancelledtokeeponeactiverequest',array('%requestid%'=>$activerequestid));
		
		 $upparam=array('id'=>$requestid,'statusid'=>$cancelledid,'description'=>$description,'param'=>$activerequestid,'timemodified'=>new \DateTime());
		 $requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
		 $requestdata->updateNativeSql($upparam,false);
	 }
	 public function keepOneActiveRequest($param) {
		$requestid=$this->getUtildata()->getVaueOfArray($param, 'requestid');
		$requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
	    $requestinfo=$requestdata->getInfo($requestid);
	    $projectid=$this->getUtildata()->getVaueOfArray($requestinfo, 'projectid');
	    $userid=$this->getUtildata()->getVaueOfArray($requestinfo, 'userid');
	    $statusshortname=$this->getUtildata()->getVaueOfArray($requestinfo, 'statusshortname');
		
		if(empty($projectid)){return null;}
		if(empty($statusshortname)){return null;}
		if($statusshortname !='preregistration'){return null;}
		if(empty($userid)){return null;}
		
		$statusdata=$this->getContainer()->get('badiu.admin.selection.status.data');
		$entity=$this->getEntity();
		$preregistrationid=$statusdata->getIdByShortname($this->getEntity(),'preregistration');
	
		$lastid=$requestdata->getLastIdDuplicate($entity,$requestid,$userid,$projectid,'preregistration');
		if(!empty($lastid)){
			if($lastid == $requestid){return null;}
			$cparam=array('requestid'=>$requestid,'activerequestid'=>$lastid);
			$this->automaticCancelRequest($cparam);
			return $lastid;
		}
	
		
		$lastid=$requestdata->getLastIdDuplicate($entity,$requestid,$userid,$projectid,'waitanalysis');
		if(!empty($lastid)){
			$cparam=array('requestid'=>$requestid,'activerequestid'=>$lastid);
			$this->automaticCancelRequest($cparam);
			return $lastid;
		}
		
		$lastid=$requestdata->getLastIdDuplicate($entity,$requestid,$userid,$projectid,'acept');
		if(!empty($lastid)){
			$cparam=array('requestid'=>$requestid,'activerequestid'=>$lastid);
			$this->automaticCancelRequest($cparam);
			return $lastid;
		}
		return null;
		
	 }
	
}
