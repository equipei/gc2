<?php

namespace Badiu\Admin\SelectionBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class ProjectLib extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function add($param) {
      
	   $modulekey=$this->getUtildata()->getVaueOfArray($param, 'modulekey');
	   $moduleinstance=$this->getUtildata()->getVaueOfArray($param, 'moduleinstance');
	   
	   $requesttimestart=$this->getUtildata()->getVaueOfArray($param, 'requesttimestart');
	   $requesttimeend=$this->getUtildata()->getVaueOfArray($param, 'requesttimeend');
	   $analysiscriteria=$this->getUtildata()->getVaueOfArray($param, 'analysiscriteria');
	   $servicecheckcriteria=$this->getUtildata()->getVaueOfArray($param, 'servicecheckcriteria');
	   $defaultstatus=$this->getUtildata()->getVaueOfArray($param, 'defaultstatus');
	   $defaultrole=$this->getUtildata()->getVaueOfArray($param, 'defaultrole');
	   $dtype=$this->getUtildata()->getVaueOfArray($param, 'dtype');
	   $ttype=$this->getUtildata()->getVaueOfArray($param, 'ttype');
	   $listdocumentnumber=$this->getUtildata()->getVaueOfArray($param, 'listdocumentnumber');
	   
	   //message
		$msgpresentation=$this->getTranslator()->trans('badiu.admin.selection.project.default.msgpresentation');
		$msgconfirmationrequest=$this->getTranslator()->trans('badiu.admin.selection.project.default.msgconfirmationrequest');
		$msgacceptrequest=$this->getTranslator()->trans('badiu.admin.selection.project.default.msgacceptrequest');
		$msgrejectionrequest=$this->getTranslator()->trans('badiu.admin.selection.project.default.msgrejectionrequest');
		$msgqueue=$this->getTranslator()->trans('badiu.admin.selection.project.default.msgqueue');
		if($analysiscriteria=='listdocumentnumber'){
		  $msgpresentation=$this->getTranslator()->trans('badiu.admin.selection.project.default.doclist.msgpresentation');
		  $msgrejectionrequest=$this->getTranslator()->trans('badiu.admin.selection.project.default.doclist.msgrejectionrequest');
		
		}
		   $name=$this->getUtildata()->getVaueOfArray($param, 'name');
	   $limitaccept=$this->getUtildata()->getVaueOfArray($param, 'limitaccept');
       $statusid=$this->getContainer()->get('badiu.admin.selection.projectstatus.data')->getIdByShortname($this->getEntity(),'active'); 
	   
	   $projectdata=$this->getContainer()->get('badiu.admin.selection.project.data');
	   $skey=$this->createSkey();
	   $paramcheckexist=array('entity'=>$this->getEntity(),'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance);
       $paramadd=array('entity'=>$this->getEntity(),'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'skey'=>$skey,'deleted'=>0,'statusid'=>$statusid,'requesttimestart'=>$requesttimestart,'requesttimeend'=>$requesttimeend,'analysiscriteria'=>$analysiscriteria,'servicecheckcriteria'=>$servicecheckcriteria,'name'=>$name,'limitaccept'=>$limitaccept,'defaultstatus'=>$defaultstatus,'defaultrole'=>$defaultrole,'dtype'=>$dtype,'ttype'=>$ttype,'listdocumentnumber'=>$listdocumentnumber,'msgpresentation'=>$msgpresentation,'msgconfirmationrequest'=>$msgconfirmationrequest,'msgacceptrequest'=>$msgacceptrequest,'msgrejectionrequest'=>$msgrejectionrequest,'msgqueue'=>$msgqueue,'timecreated'=>new \DateTime());
       $paramedit=array('entity'=>$this->getEntity(),'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'deleted'=>0,'statusid'=>$statusid,'requesttimestart'=>$requesttimestart,'requesttimeend'=>$requesttimeend,'analysiscriteria'=>$analysiscriteria,'servicecheckcriteria'=>$servicecheckcriteria,'name'=>$name,'limitaccept'=>$limitaccept,'defaultstatus'=>$defaultstatus,'defaultrole'=>$defaultrole,'dtype'=>$dtype,'ttype'=>$ttype,'listdocumentnumber'=>$listdocumentnumber,'timemodified'=>new \DateTime());
     
	  $dresult=$projectdata->addNativeSql($paramcheckexist,$paramadd,$paramedit);
	   $dresultid=$this->getUtildata()->getVaueOfArray($dresult,'id'); 
       return  $dresultid;
    }  
	public function createSkey() {
		$hash = $this->getContainer()->get('badiu.system.core.lib.util.hash');
		$hkey = $hash->make(50);
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $badiuSession->setHashkey($this->getSessionhashkey());
        $entity=$badiuSession->get()->getEntity();
		$userid=$badiuSession->get()->getUser()->getId();
		$pkey='.'.time().'.'.$entity.'.'.$userid;
		$key=$hkey.$pkey;
		return $key;
	}
}
