<?php

namespace Badiu\Admin\SelectionBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class RequestSendMailLib extends BadiuModelLib {

private $param;
     function __construct(Container $container) {
        parent::__construct($container);
      
    }

	 public function exec(){
		
			$requestid=$projectid=$this->getUtildata()->getVaueOfArray($this->param, 'requestid');
			
			$requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
			$requestinfo=$requestdata->getInfo($requestid);
			
			$projectid=$this->getUtildata()->getVaueOfArray($requestinfo, 'projectid');
			$userid=$this->getUtildata()->getVaueOfArray($requestinfo, 'userid');
			$modulekey=$this->getUtildata()->getVaueOfArray($requestinfo, 'modulekey');
			$moduleinstance=$this->getUtildata()->getVaueOfArray($requestinfo, 'moduleinstance');
			$statusshortname=$this->getUtildata()->getVaueOfArray($requestinfo, 'statusshortname');
			
			if($statusshortname !='acept'){return null;}
			//get user info
			$userdata=$this->getContainer()->get('badiu.system.user.user.data');
			$fparam=array('id'=>$userid);
			$usrinfo=$userdata->getGlobalColumnsValue('o.id,o.firstname,o.lastname,o.email,o.doctype,o.docnumber,o.city,o.state,o.dateofbirth',$fparam);
			
			
			//get project  name, description
			$objectinfo=$this->getObjectInfo($moduleinstance);
			
			
			$vparam=array('user'=>$usrinfo,'object'=>$objectinfo);
			
			//make message
			$message=$this->makeMessage($vparam);
			//send mail
			$email=$this->getUtildata()->getVaueOfArray($usrinfo,'email');
			//$email="badiu.net@gmail.com";
			$mparam=array('message'=>$message,'email'=>$email,'subject'=>'Confirmação de inscrição');
			$mresult=$this->sendMailMessage($mparam);
		
	   return $mresult;
		}
   
	//review
    public function getObjectInfo($moduleinstance) {
		$classedata=$this->getContainer()->get('badiu.ams.offer.classe.data');
		$sql = "SELECT d.name AS disciplinename,d.dhour AS disciplinehour,d.description AS disciplinedescription FROM BadiuAmsOfferBundle:AmsOfferClasse o  JOIN o.odisciplineid od JOIN od.disciplineid d  WHERE o.id=:classeid ";
		$query = $classedata->getEm()->createQuery($sql);
        $query->setParameter('classeid',$moduleinstance );
		 $result= $query->getOneOrNullResult();
		return $result;
    } 	
 function makeMessage($param) {
		$id=$this->getUtildata()->getVaueOfArray($param, 'user.id',true);
		$firstname=$this->getUtildata()->getVaueOfArray($param, 'user.firstname',true);
		$lastname=$this->getUtildata()->getVaueOfArray($param, 'user.lastname',true);
		$email=$this->getUtildata()->getVaueOfArray($param, 'user.email',true);
		$doctype=$this->getUtildata()->getVaueOfArray($param, 'user.doctype',true);
		$docnumber=$this->getUtildata()->getVaueOfArray($param, 'user.docnumber',true);
		$city=$this->getUtildata()->getVaueOfArray($param, 'user.city',true);
		$state=$this->getUtildata()->getVaueOfArray($param, 'user.state',true);
		$dateofbirth=$this->getUtildata()->getVaueOfArray($param, 'user.dateofbirth',true);
		
		$entity= $this->getEntity();
		if(!empty($city)){$city=$this->getContainer()->get('badiu.util.address.city.data')->getNameByShortname($entity,$city);}
		if(!empty($state)){$state=$this->getContainer()->get('badiu.util.address.state.data')->getNameByShortname($entity,$state);}
		
		
		$configformat=$this->getContainer()->get('badiu.system.core.lib.format.configformat');
		$dateofbirth=$configformat->defaultDate($dateofbirth);
	 
		$objectname=$this->getUtildata()->getVaueOfArray($param, 'object.disciplinename',true);
		$objectdescription=$this->getUtildata()->getVaueOfArray($param, 'object.disciplinedescription',true);
		$objecthour=$this->getUtildata()->getVaueOfArray($param, 'object.disciplinehour',true);

		$objecthour=$configformat->textHourRound($objecthour);
		
	 $tview="";
	 $tview.="<h3>Dados Cadastrais</h3>";
	 $tview.='<table   border="0" width="100%">'; 
	 
	 
	 $tview.='<tr  bgcolor="#ded9d3" style="font-family: Arial;">';
	 $tview.='<td style="font-family: Arial; font-weight: bold;" width="15%">Nome</td>';
	 $tview.='<td style="font-family: Arial; ">'.$firstname.'</td>';
	 $tview.='</tr>';
	
	 $tview.='<tr  bgcolor="#f9f9f9" style="font-family: Arial;">';
	 $tview.='<td style="font-family: Arial; font-weight: bold;" width="15%">Sobrenme</td>';
	 $tview.='<td style="font-family: Arial; ">'.$lastname.'</td>';
	 $tview.='</tr>';
	 
	 $tview.='<tr  bgcolor="#ded9d3" style="font-family: Arial;">';
	 $tview.='<td style="font-family: Arial; font-weight: bold;" width="15%">CPF</td>';
	 $tview.='<td style="font-family: Arial;">'.$docnumber.'</td>';
	 $tview.='</tr>';
	 
	 $tview.='<tr  bgcolor="#f9f9f9" style="font-family: Arial;">';
	 $tview.='<td style="font-family: Arial; font-weight: bold;" width="15%">E-mail</td>';
	 $tview.='<td style="font-family: Arial;">'.$email.'</td>';
	 $tview.='</tr>';
	 
	 $tview.='<tr  bgcolor="#ded9d3" style="font-family: Arial;">';
	 $tview.='<td style="font-family: Arial; font-weight: bold;" width="15%">Data de nascimento</td>';
	 $tview.='<td style="font-family: Arial;">'.$dateofbirth.'</td>';
	 $tview.='</tr>';
	 
	 $tview.='<tr  bgcolor="#f9f9f9" style="font-family: Arial;">';
	 $tview.='<td style="font-family: Arial; font-weight: bold;" width="15%">Cidade</td>';
	 $tview.='<td style="font-family: Arial;">'.$city.'</td>';
	 $tview.='</tr>';
	 
	 $tview.='<tr  bgcolor="#ded9d3" style="font-family: Arial; ">';
	 $tview.='<td style="font-family: Arial; font-weight: bold;" width="15%">Estado</td>';
	 $tview.='<td style="font-family: Arial; ">'.$state.'</td>';
	 $tview.='</tr>';
	 
	
	$tview.='</tbody>';
	$tview.='</table>';
	
	$tview.='</hr>';
	 $tview.="<h3>Dados da Inscrição</h3>";
	 
	 $tview.='<table   border="0" width="100%">'; 
	 
	 
	 $tview.='<tr  bgcolor="#ded9d3" style="font-family: Arial;">';
	 $tview.='<td style="font-family: Arial; font-weight: bold;" width="15%">Oficina</td>';
	 $tview.='<td style="font-family: Arial; ">'.$objectname.'</td>';
	 $tview.='</tr>';
	
	 $tview.='<tr  bgcolor="#f9f9f9" style="font-family: Arial;">';
	 $tview.='<td style="font-family: Arial; font-weight: bold;" width="15%">Carga horária</td>';
	 $tview.='<td style="font-family: Arial; ">'.$objecthour.'</td>';
	 $tview.='</tr>';
	 
	 $tview.='<tr  bgcolor="#ded9d3" style="font-family: Arial;">';
	 $tview.='<td style="font-family: Arial; font-weight: bold;" width="15%">Data de realização</td>';
	 $tview.='<td style="font-family: Arial;">'.$objectdescription.'</td>';
	 $tview.='</tr>';
	 
	
	$tview.='</tbody>';
	$tview.='</table>';
	return $tview;
 }
 
 public function sendMailMessage($param) {
		 
		 $message=$this->getUtildata()->getVaueOfArray($param,'message');
		 $email=$this->getUtildata()->getVaueOfArray($param,'email');
		 $subject=$this->getUtildata()->getVaueOfArray($param,'subject');
		
		$mconfig=array(); 
      
        $mconfig['useridto']=null;
        $mconfig['userto']=null;
        $mconfig['recipient']['to']=$email;
        $mconfig['recipient']['bcc']= null;
        $mconfig['message']['subject']=$subject;
        $mconfig['message']['body']=$message;
        $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
        //$smtp->setUsersender($this->getUsersender());
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        //$badiuSession->setHashkey($this->getSessionhashkey());
        $entity= $this->getEntity();

        //$entity=$this->getEntity();
        $smtp->setEntity($entity);
		
		$systemdata=$this->getContainer()->get('badiu.system.core.functionality.systemdata');
		$systemdata->setEntity($this->getEntity());
		$systemdata->init();
		$smtp->setSystemdata($systemdata);
		
        $result=$smtp->sendMail($mconfig);

       return $result;
	}
 function getParam() {
     return $this->param;
 }


 function setParam($param) {
     $this->param = $param;
 }
 
}
