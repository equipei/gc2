<?php

namespace Badiu\Admin\SelectionBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuAccessFilter;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class LoginsinginAccessFilter extends  BadiuAccessFilter {

	function __construct(Container $container) {
		parent::__construct($container);
	}

       public function exec(){
		   
		   //check session
		   $badiuSession = $this->getContainer()->get('badiu.system.access.session');
           if (!$badiuSession->exist()) {return true;}
		   
         $filter=true;
        
          $requestid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('requestid');
          if(empty($requestid)){echo $this->getTranslator()->trans('badiu.admin.selection.inforeques.message.requestidrequired');exit;}
          
		  $requestdata=$this->getContainer()->get('badiu.admin.selection.request.data');
		  $exist=$requestdata->exist($requestid);
		  if(!$exist){echo $this->getTranslator()->trans('badiu.admin.selection.request.message.idnotvalid');exit;}
		  $badiuSession = $this->getContainer()->get('badiu.system.access.session');
		  //review it
			//	if(!empty($clientsession)){$badiuSession->setHashkey($clientsession);}
		 $isuseranonymous = $badiuSession->get()->getUser()->getAnonymous();
		 
		  $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
		  if($sysoperation->isEdit()){ return true;}
		  if(!$isuseranonymous){
			  $apputil=$this->getContainer()->get('badiu.system.core.lib.util.app');
				$url=$apputil->getUrlByRoute('badiu.admin.selection.statusrequest.dashboard',array('parentid'=>$requestid));
				header('Location: '.$url); 
				exit;
          }
         
        return $filter;
         } 
   
         
       
}
