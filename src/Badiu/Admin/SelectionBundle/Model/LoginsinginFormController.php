<?php
namespace Badiu\Admin\SelectionBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;

//edit exemple
//http://54.156.96.187/badiunet/web/app_dev.php/admin/selection/user/edit/1249?requestid=165
class LoginsinginFormController extends BadiuFormController {
    private $userdb;
	private $requestid;
	private $password;
    function __construct(Container $container) {
        parent::__construct($container);
        $this->userdb=$this->getContainer()->get('badiu.system.user.user.data');
        $this->initParam();
    }

    //delete this function $this->iniParam(); in constructo do same 
    public function init() {
        $this->setParam($this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters());
    }
  
	  public function changeParamOnOpen() {
		  if($this->isedit()){$this->removeParamItem('password');}
		   
		   $param = $this->getParam();
		  
	  }
     public function checkUsername() {
		    $this->init();
			
            $validusername=$this->isValidUsername();
            if(!empty($validusername)){return $validusername; }

           //check if exist login
			$userid=$this->getUseridByUsername();
		   
           $lmessage="";

           if($userid){$lmessage='hasregister';}
           else {$lmessage='withoutregister';}
		   $message=array('status'=>$lmessage);
			if($userid){		  
				$badiuSession = $this->getContainer()->get('badiu.system.access.session');
				$execfuncionservice=$this->getContainer()->get('badiu.system.core.lib.util.execfuncionservice');
				$serviceexecbefore=$badiuSession->getValue('badiu.auth.core.login.param.config.serviceexecbefore');
				if(!empty($serviceexecbefore)){
					$sebresult=$execfuncionservice->exec($serviceexecbefore,'exec',$this->getParam());
					if(!empty($sebresult)){
						$message['message']=$this->getUtildata()->getVaueOfArray($sebresult, 'message.message',true);
						$message['typemessage']='info';
						
					}	 		
				}
			}
          
           return $this->getResponse()->accept($message);
            
     }
	
	 public function getUseridByUsername() {
		   $username=$this->getParamItem('username');
		   $username=trim($username);
		   $type=null;
		   $userid=null;
		   $param=array('username'=>$username,'entity'=>$this->getEntity());
           $userid=$this->userdb->getGlobalColumnValue('id',$param);
		   if($userid){return $userid;}
		   
		   if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
				$param=array('email'=>$username,'entity'=>$this->getEntity());
				$userid=$this->userdb->getGlobalColumnValue('id',$param);
				if($userid){return $userid;}
			}
			   
			$cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
			$valid = $cpfdata->isValid($username);
			if($valid){
				$username=$cpfdata->clean($username);	
				$param=array('doctype'=>'CPF','docnumber'=>$username,'entity'=>$this->getEntity());
				$userid=$this->userdb->getGlobalColumnValue('id',$param);
				if($userid){return $userid;}
			}
		return null;
	 }
     public function recoverPassword() {
        $this->init();
        $validusername=$this->isValidUsername();
        if(!empty($validusername)){return $validusername; }

        //change passoword
        $newpwd="";
        for ($i = 0; $i < 8; $i++) { $newpwd .= rand(0, 9); }
        $newpwdmd5=md5($newpwd);
        $paramfilter=array('username'=>$username=$this->getParamItem('username'));
        $userid=$this->userdb->getGlobalColumnValue('id',$paramfilter);
        $userinfo=$this->userdb->getGlobalColumnsValue(" CONCAT(o.firstname,' ',o.lastname) AS fullname ",array('id'=>$userid)); 
        $userfullname =$this->getUtildata()->getVaueOfArray($userinfo, 'fullname');

        //if userid not found
        if(!$userid){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorfinduserid');
            $info='badiu.admin.selection.loginsingindoc.errorfinduserid';
            return $this->getResponse()->denied($info, $message);
        }

         //if email not found
        $email=$this->userdb->getGlobalColumnValue('email',$paramfilter);
        if(!$email){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorfindemail');
            $info='badiu.admin.selection.loginsingindoc.errorfindemail';
            return $this->getResponse()->denied($info, $message);
        }

        //if email not valid
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorfindnotvalid');
            $info='badiu.admin.selection.loginsingindoc.errorfindnotvalid';
            return $this->getResponse()->denied($info, $message);
        }

        $paramuppwd=array('password'=>$newpwdmd5,'id'=>$userid);
        $pwdupresult=$this->userdb->updateNativeSql($paramuppwd,false);
        
        if(!$pwdupresult){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorcreatenewpassword');
            $info='badiu.admin.selection.loginsingindoc.errorcreatenewpassword';
            return $this->getResponse()->denied($info, $message);
        }
        
        //get message template
		$dataemailtemplate=null;
		$subject=null;
		$message=null;
		if($this->getContainer()->has('badiu.admin.selection.emailtemplate.data')){
			$dataemailtemplate=$this->getContainer()->get('badiu.admin.selection.emailtemplate.data');
			$dconfig=$dataemailtemplate->getGlobalColumnValue('value',array('entity'=>$this->getEntity(),'dtype'=>'email','modulekey'=>'badiu.admin.selection.emailtemplate','shortname'=>'badiu.admin.selection.emailtemplate.type.recoverpassword','deleted'=>false));
			$dconfig  = $this->getJson()->decode($dconfig,true);
        	$subject=$this->getUtildata()->getVaueOfArray($dconfig,'subject');
			$message=$this->getUtildata()->getVaueOfArray($dconfig,'message');
    	}
         
        
        
        if(empty($subject)){$subject=$this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.recoverpwdsendmail.subject');}
        if(empty($message)){$message=$this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.recoverpwdsendmail.message');}
        
        $message=str_replace("{NEW_PASSWORD}",$newpwd,$message);
       
        

        $mconfig=array();
        $mconfig['useridto']=$userid;
        $mconfig['userto']=$userfullname;
        $mconfig['recipient']['to']=$email;
        $mconfig['message']['subject']=$subject;
        $mconfig['message']['body']=$message;
        $smtp=$this->getContainer()->get('badiu.system.core.lib.util.smtp');
        //$smtp->setUsersender($this->getUsersender());
        $entity=$this->getEntity();
        $smtp->setEntity($entity);
     
		$systemdata=$this->getContainer()->get('badiu.system.core.functionality.systemdata');
		$systemdata->setEntity($this->getEntity());
		$systemdata->init();
		$smtp->setSystemdata($systemdata);
        $sendmsg=$smtp->sendMail($mconfig);
      
        if(!$sendmsg){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorsendmailwithnewpassword');
            $info='badiu.admin.selection.loginsingindoc.errorsendmailwithnewpassword';
            return $this->getResponse()->denied($info, $message);
        }else{
            $this->addlogSendmailRecoverPassword($mconfig);
        }
        $message=array('status'=>'passwordsendtoemail','message'=>"Foi gerada uma nova senha e foi enviado para o e-mail $email <br /> Recupere a nova senha enviada no e-mail e logue novamente");
        return $this->getResponse()->accept($message);
       
       
        //send message

        //
         //check if exist login
       /*  $username=$this->getParamItem('username');
         $param=array('username'=>$username);
         $countlogin=$this->userdb->countGlobalRow($param);
         $lmessage="";
         if($countlogin==1){$lmessage='hasregister';}
         else  if($countlogin==0){$lmessage='withoutregister';}
         else{
          $message=array();
          $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.errorcheckemail');
          $info='badiu.admin.selection.loginsingindoc.errorcheckemial';
          return $this->getResponse()->denied($info, $message);
         }*/

        
    }
     public function execLogin() {
         $this->init();
         $mvalidate=$this->isValidrequestid();
         if ($mvalidate != null) {return $mvalidate;}
		 
		
       $mvalidate=$this->isValidUsername();
       if ($mvalidate != null) {return $mvalidate;}
	   
       $mvalidate=$this->isValidPassword();
        if ($mvalidate != null) {return $mvalidate;}

          $mvalidate=$this->isUsernamepasswordmatch();
         if ($mvalidate != null) { 
              return $mvalidate;
          }
          
          $autenticated=$this->getParamItem('autenticated');
          $requestid=$this->getParamItem('requestid');
		 
         if($autenticated){
            
			//check duplicate request
			
			//exec request
			$processreq=$this->processRequest();
			if(!empty($processreq)){return $processreq;}
			
			//rediret to confirm request 
			$apputil=$this->getContainer()->get('badiu.system.core.lib.util.app');
			$url=$apputil->getUrlByRoute('badiu.admin.selection.inforequest.dashboard',array('parentid'=>$requestid));
			$outrsult=array('message'=>$autenticated,'urlgoback'=>$url);
			$this->getResponse()->setStatus("accept");
            $this->getResponse()->setMessage($outrsult);
			return $this->getResponse()->get(); 
		   
         }else{
             $message=array();
             $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.failed');
             $info='badiu.admin.selection.loginsingindoc.failed';
             return $this->getResponse()->denied($info, $message);
         }
        
 }
  /*  
 public function execSingin() {
	 
		$this->init();
		
		$this->addDefaultData();
		
		$this->castData();
		$check=$this->validation();
		
		if($check != null){return $check;}
	
   
      //check if exist login
      $username=$this->getParamItem('username');
      $param=array('username'=>$username,'entity'=>$this->getEntity());
      $countlogin=$this->userdb->countGlobalRow($param);
     
      if($countlogin > 0){
        $message=array();
        $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.userjustexistonadd');
        $info='badiu.admin.selection.loginsingindoc.userjustexistonadd';
        return $this->getResponse()->denied($info, $message);
       
      }
	  //check doc number
	  $param=array('doctype'=>'CPF','docnumber'=>$username,'entity'=>$this->getEntity());
      $countlogin=$this->userdb->countGlobalRow($param);
      
	   if($countlogin > 0){
        $message=array();
        $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.userjustexisbydocnumber');
        $info='badiu.admin.selection.loginsingindoc.userjustexisbydocnumber';
        return $this->getResponse()->denied($info, $message);
       
      }
	  
      $requestid=$this->getParamItem('requestid');
      $paramtoinsert=$this->getParamItem('paramtoinsert');
      $userid=$this->userdb->insertNativeSql($paramtoinsert,false);
      
     if($userid){
		$this->addParamItem('id',$userid) ;
		$this->sysFormAfter();
        $reusltauth=$this->loginAfterSingin(); 
        if(!empty($reusltauth)){return $reusltauth;} 
		$processreq=$this->processRequest();
		if(!empty($processreq)){return $processreq;}
			
     }else{
         $message=array();
         $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.failed');
         $info='badiu.admin.selection.loginsingindoc.failed';
         return $this->getResponse()->denied($info, $message);
     }
   
} */
	public function validation() {
		
		$mvalidate=$this->isValidrequestid();
		if ($mvalidate != null) { return $mvalidate;}
		
	//	$mvalidate=$this->isSecurityPolicyAccept();
		//if ($mvalidate != null) { return $mvalidate;}
		
		$mvalidate=$this->isValidCpf();
		if ($mvalidate != null) { return $mvalidate;}
	 
		$mvalidate=$this->isValidEmail();
		if ($mvalidate != null) { return $mvalidate;}
	
		$mvalidate=$this->isValidName();
		if ($mvalidate != null) { return $mvalidate;}
		
		$mvalidate=$this->isDuplicateDocument();
		if ($mvalidate != null) { return $mvalidate;}
		
		
		$mvalidate=$this->isValidPassword();
		if ($mvalidate != null) { return $mvalidate;}
	 
		
		/*$mvalidate=$this->isValidDateofbirth();
		if ($mvalidate != null) {return $mvalidate;}*/
		
		$customvalid = $this->customValidate();
	   if ($customvalid != null) {
            return $customvalid;
        }
		return null;
	}


    public function changeParam() {
		
		/*$userutil=$this->getContainer()->get('badiu.system.user.user.util');
        $ownername=$this->getParamItem('name');
        $people=$userutil->splitFirtnameLastname($ownername);
		
		$this->addParamItem('firstname',$people->firtname);
		$this->addParamItem('lastname',$people->lastname);*/
		$this->removeParamItem('name');
        
	   $this->requestid=$this->getParamItem('requestid');
	   $this->removeParamItem('requestid');
	   
	    $this->addParamItem('statusregister','complete');
		
		$this->addParamItem('username',$this->getParamItem('docnumber'));
	   //print_r($this->getParam());exit;
	   
	   $intervaltimedayupdate=$this->getContainer()->get('badiu.system.access.session')->getValue('badiu.system.core.param.config.userupdateprofileintervalday');
		 if(empty($intervaltimedayupdate)){$intervaltimedayupdate=180;}
		 $now=new \DateTime();
		 $now->modify("+$intervaltimedayupdate day");
		 $this->addParamItem('registernextupadtetime',$now);
       
     } 

  public function execResponse() {
	   $userid=$this->getResultexec();
	   if($userid){
		    $this->addParamItem('requestid', $this->requestid);
			if(!$this->isedit()){
				$this->addParamItem('id',$userid);
				$reusltauth=$this->loginAfterSingin(); 
				if(!empty($reusltauth)){return $reusltauth;} 
			}
				
			$processreq=$this->processRequest();
			if(!empty($processreq)){return $processreq;}
			 
     }else{
		 $message=array();
         $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.failed');
         $info='badiu.admin.selection.loginsingindoc.failed';
         return $this->getResponse()->denied($info, $message);
     }
	 
	}
	
	public function isSecurityPolicyAccept() {
	   $securitypolicyaccept=$this->getParamItem('securitypolicyaccept');
	   if(is_array($securitypolicyaccept)){
			$securitypolicyaccept=$this->getUtildata()->getVaueOfArray($securitypolicyaccept,0);
			 $this->addParamItem('securitypolicyaccept',$securitypolicyaccept);
		}
	   if(empty($securitypolicyaccept)){
				$message=array();
                $message['generalerror'] =$this->getTranslator()->trans('badiu.system.user.user.message.securitypolicyacceptrequired');
                $info = 'badiu.system.user.user.securitypolicyacceptrequired';
                return $this->getResponse()->denied($info, $message);
		}
		return null;
	}
    public function isValidEmail() {
       
        $email=$this->getParamItem('email');
        $email=trim($email);
        
        if(empty($email)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingin.message.emailrequired');
            $info='badiu.admin.selection.loginsingin.emailrequired';
            return $this->getResponse()->denied($info, $message);
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.emailnotvalid');
            $info='badiu.admin.selection.loginsingindoc.emailnotvalid';
            return $this->getResponse()->denied($info, $message);
        }
		$entity=$this->getEntity();
		$id=$this->getParamItem('id');
		$duplicateemail = null;
		if($this->isedit()){$duplicateemail = $this->userdb->existColumnEdit($id,$entity, 'email', $email);}
        else{$duplicateemail = $this->userdb->existColumnAdd($entity, 'email', $email);}
        
		if ($duplicateemail) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                //$message['email']= $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                $info = 'badiu.system.user.user.message.duplicationemail';
                return $this->getResponse()->denied($info, $message);
        }
			
     /* $param=array('email'=>$email,'entity'=>$this->getEntity());
      $countemail=$this->userdb->countGlobalRow($param);
     
      if($countemail > 0  !$this->isedit()){
        $message=array();
        $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.emailjustexistonadd');
        $info='badiu.admin.selection.loginsingindoc.emailjustexistonadd';
        return $this->getResponse()->denied($info, $message);
       
      }*/
   
		
        return null;
    }
	
	 public function isValidName() {
        $userutil=$this->getContainer()->get('badiu.system.user.user.util');
		 $ownername=$this->getParamItem('name');
		if(!empty($ownername) && $userutil->hasSpecialCharacter($ownername)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.name.invalidcharacter');
            $info='badiu.admin.selection.loginsingin.namewithinvalidcharacter';
            return $this->getResponse()->denied($info, $message);
        }
		$ownername=$userutil->cleanNameSpace($ownername);
        $people=$userutil->splitFirtnameLastname($ownername);
		
		$this->addParamItem('firstname',$people->firtname);
		$this->addParamItem('lastname',$people->lastname);
        
        if(empty($people->firtname) || empty($people->lastname)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingin.message.namerequired');
            $info='badiu.admin.selection.loginsingin.namerequired';
            return $this->getResponse()->denied($info, $message);
        }
        
		$usernameuppercase=$this->getContainer()->get('badiu.system.access.session')->getValue('badiu.system.core.param.config.usernameuppercase');
		if($usernameuppercase){
			$this->addParamItem('firstname',mb_strtoupper($people->firtname,'UTF-8'));
			$this->addParamItem('lastname',mb_strtoupper($people->lastname,'UTF-8'));
		}
		
		//alternatename
		$enablealternatename=$this->getParamItem('enablealternatename');
		if(!$enablealternatename){$this->addParamItem('alternatename',null);}
		
		$alternatename=$this->getParamItem('alternatename');
		if(empty($alternatename)){return null;}
		
		$alternatename=$userutil->cleanNameSpace($alternatename);
		if($userutil->hasSpecialCharacter($alternatename)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.alternatename.invalidcharacter');
            $info='badiu.admin.selection.loginsingin.alternatenamewithinvalidcharacter';
            return $this->getResponse()->denied($info, $message);
        }
		
		if($usernameuppercase){
			$alternatename=mb_strtoupper($alternatename,'UTF-8');
		}
		$this->addParamItem('alternatename',$alternatename);
		$this->removeParamItem('name');
        return null;
    } 
	
	
	 public function isValidCpf() {
		 $cpf=$this->getParamItem('cpf');
		 $cpf=trim($cpf);
		
		 $nationalitystatus=$this->getParamItem('nationalitystatus');
		 $cpfdata = $this->getContainer()->get('badiu.util.document.role.cpf');
         $valid = $cpfdata->isValid($cpf);
		 
		 if($nationalitystatus!='native' && !$valid){
			 $this->removeParamItem('cpf'); 
			 $this->addParamItem('doctype','EMAIL');
			 $this->addParamItem('docnumber',$this->getParamItem('email'));
			 if(empty($cpf))return null;
		 }
		
        if(empty($cpf)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.cpfrequired');
            $info='badiu.admin.selection.loginsingin.cpfrequired';
            return $this->getResponse()->denied($info, $message);
        }
		
		 if (!$valid) {
                $message=array();
                $message['generalerror']= $this->getTranslator()->trans('badiu.system.user.user.message.cpfnotvalid');
                $info = 'badiu.admin.selection.loginsingin.cpfnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
			
		$cpf=$cpfdata->clean($cpf);	
        $this->addParamItem('doctype','CPF');
		$this->addParamItem('docnumber',$cpf);
		$this->removeParamItem('cpf');
        return null;
	 }
	 
	  public function isDuplicateDocument() {
		 //duplicate 
		  $entity=$this->getEntity();
		  $id=$this->getParamItem('id');
		  $doctype=$this->getParamItem('doctype');
		  $docnumber=$this->getParamItem('docnumber');
		  $data=$this->userdb;
		 if ($doctype == 'CPF') {
           $duplicatecpf=null;
           if($this->isedit()){$duplicatecpf = $data->existColumnEdit($id,$entity, 'docnumber', $docnumber);}
           else{$duplicatecpf = $data->existColumnAdd($entity, 'docnumber', $docnumber);}
          
           
          if ($duplicatecpf) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.cpfexist');
                $info = 'badiu.admin.selection.loginsingin.document.cpf.justexist';
                return $this->getResponse()->denied($info, $message);
      
            }
		}else  if ($doctype == 'EMAIL') {
          
            $duplicateemail=null;
            if($this->isedit()){$duplicateemail = $data->existColumnEdit($id,$entity, 'docnumber', $docnumber);}
            else{$duplicateemail = $data->existColumnAdd($entity, 'docnumber', $docnumber);}
          
            if ($duplicateemail) {
                $message=array();
                 $message['generalerror'] = $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $docnumber));
                 $info = 'badiu.admin.selection.loginsingin.document.email.justexist';
                return $this->getResponse()->denied($info, $message);
            }  
        }
     
        return null;
	 }
	 
	 
    public function isValidUsername() {
       return null;  
        $username=$this->getParamItem('username');
        $username=trim($username);
        if(empty($username)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingin.message.usernamerequired');
            $info='badiu.admin.selection.loginsingin.usernamerequired';
            return $this->getResponse()->denied($info, $message);
        }
		
		 $this->addParamItem('username',$username);
        return null;
    }

    public function isValidPassword() {
		if($this->isedit()){$this->removeParamItem('password'); return null;}
        $password=$this->getParamItem('password');
        $password=trim($password);
        if(empty($password)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingin.message.passwordrequired');
            $info='badiu.admin.selection.loginsingin.passwordrequired';
            return $this->getResponse()->denied($info, $message);
        }
         $this->password=$password;
		 $this->addParamItem('password',md5($password));
        return null;
    }

	public function isValidDateofbirth() {
		$dateofbirth=$this->getParamItem('dateofbirth');
		
        if(empty($dateofbirth)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingin.message.dateofbirthrequired');
            $info='badiu.admin.selection.loginsingin.dateofbirthrequired';
            return $this->getResponse()->denied($info, $message);
        }
		if(!is_a($dateofbirth, 'DateTime')){
			
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingin.message.dateofbirthnotvalid');
            $info='badiu.admin.selection.loginsingin.dateofbirthnotvalid';
            return $this->getResponse()->denied($info, $message);
        }
		
		if(is_a($dateofbirth, 'DateTime')){
			$y = $dateofbirth->format('Y');
			if($y < 1900){
				$message=array();
				$message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingin.message.dateofbirthnotvalid');
				$info='badiu.admin.selection.loginsingin.dateofbirthnotvalid';
				return $this->getResponse()->denied($info, $message);
			}
            
        }
	/*	
		$now=new \DateTime();
		$now->modify('-14 year');
		
		if($dateofbirth->getTimestamp() > $now->getTimestamp()){
			$message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingin.message.dateofbirthunder14yearsold');
            $info='badiu.admin.selection.loginsingin.dateofbirtunder14yearsold';
            return $this->getResponse()->denied($info, $message);
		}
		*/
        return null;
    }
    public function isUsernamepasswordmatch() {
        $username=$this->getParamItem('username');
        $password=$this->getParamItem('password');
        $username=trim($username);
        $password=$this->password;//trim($password);
		$userid=$this->getUseridByUsername();
		$fparam=array('userid'=>$userid,'trybyid'=>1);
        $usernamepasswordmatch=$this->getContainer()->get('badiu.auth.core.login.manual')->exec($username,$password,$fparam);
        if(!$usernamepasswordmatch){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.usernamepasswordnotmatch');
            $info='badiu.admin.selection.loginsingindoc.usernamepasswordnotmatch';
            return $this->getResponse()->denied($info, $message);
        }
        $param=$this->getParam();
        $param['autenticated']=TRUE;
        $this->setParam($param);
       
        return null;
    }
   
    public function loginAfterSingin() {
        $username=$this->getParamItem('username');
        $password=$this->password;
        //$username=trim($username);
        //$password=trim($password);
		$usernamepasswordmatch=$this->getContainer()->get('badiu.auth.core.login.manual')->exec($username,$password);
        
		if(!$usernamepasswordmatch){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.failedloginaftersingin');
            $info='badiu.admin.selection.loginsingindoc.failedloginaftersingin';
            return $this->getResponse()->denied($info, $message);
        }
        $param=$this->getParam();
        $this->setParam($param);

        return null;
    }
    public function isValidProfile() {
        $name=$this->getParamItem('name');
        $addpassword=$this->getParamItem('addpassword');
        $nationalitystatus=$this->getParamItem('nationalitystatus');
        $username=$this->getParamItem('username');
		$email=$this->getParamItem('email');
        $name=trim($name);
        if(empty($name)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.namerequired');
            $info='badiu.admin.selection.loginsin.namerequired';
            return $this->getResponse()->denied($info, $message);
        }
      
        if(empty($addpassword)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.passwordrequired');
            $info='badiu.admin.selection.loginsin.addpasswordrequired';
            return $this->getResponse()->denied($info, $message);
        }
       

        $peple=$this->getContainer()->get('badiu.system.user.user.util')->splitFirtnameLastname($name);
        if(empty($peple->lastname)){$peple->lastname=" ";}
        $param=$this->getParam();

        $haspassword=md5(trim($addpassword));
		$contactdata=$this->getContactdata();
		$this->setKey('badiu.systeam.user.user.add');
		$this->sysFormBefore();
		$adminformattemptid=$this->getParamItem('adminformattemptid');
        $paramtoinsert=array('firstname'=>$peple->firtname,'lastname'=>$peple->lastname,'email'=> trim($email),'username'=>trim($username),'password'=>$haspassword,'nationalitystatus'=>$nationalitystatus,'timecreated'=>new \DateTime(),'deleted'=>FALSE,'entity'=>$this->getEntity(),'confirmed'=>1,'doctype'=>'CPF','docnumber'=>$username,'contactdata'=>$contactdata,'adminformattemptid'=>$adminformattemptid);
        $param['paramtoinsert']=$paramtoinsert;
        $this->setParam($param);
        return null;
    }
    public function isValidrequestid() {
       $requestid=$this->getParamItem('requestid');
       
        if(empty($requestid)){
            $message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.selection.loginsingindoc.message.requestidrequired');
            $info='badiu.admin.selection.loginsingindoc.requestidrequired';
            return $this->getResponse()->denied($info, $message);
        }
      
        return null;
    }
   
    public function customValidate() {
		$badiuSession = $this->getContainer()->get('badiu.system.access.session');
		$formvalidatekey=$badiuSession->getValue('badiu.system.user.default.param.config.formvalidate');
		if(!$this->getContainer()->has($formvalidatekey)){return null;}
		$formvalidate=$this->getContainer()->get($formvalidatekey);
		$formvalidate->setParam($this->getParam());
		$validate=$formvalidate->get();
		return $validate;
	}

    private function addlogSendmailRecoverPassword($mconfig) {
        $targetid =$this->getUtildata()->getVaueOfArray($mconfig, 'useridto');
        $message= $this->getJson()->encode($mconfig);
  
       $dloglib=$this->getContainer()->get('badiu.system.log.core.lib');
        $param=array();
       $param['action']='exec';
       $param['userid']=$targetid;
       $param['entity']=$this->getEntity();
       $param['dtype']='out';
       $param['clientdevice']=null;
       $param['modulekey']='badiu.admin.selection.core';
       $param['moduleinstance']=$this->getParamItem('requestid');
       $param['rkey']='badiu.admin.selection.loginsingin';
       $param['param']=$message;
       $param['dinfo']=null;
       $param['dnavegation']=null;
       $param['targetid']= $targetid;
       $dloglib->add($param);
       
   }
   

   private function processRequest() {
         $reqlinkcontroller= $this->getContainer()->get('badiu.admin.selection.inforequest.linkcontroller');
         $requestid=$this->getParamItem('requestid');
		 $fparam=array('requestid'=>$requestid);
		 $reqlinkcontroller->setParam($fparam);
		 
		 $radduser=$reqlinkcontroller->addUserToRequest();
		 if(!empty($radduser)){return $radduser;}
		
		 $checkrestriction=$reqlinkcontroller->checkRestriction();
		if(!empty($checkrestriction)){return $checkrestriction;}
		
			
		 $reqlinkcontroller->keepOneActiveRequest();
		 $this->addParamItem('requestid',$reqlinkcontroller->getParamItem('requestid'));
		  
		 $nextstatus=$reqlinkcontroller->getNextStatus();
		 if(!empty($nextstatus)){return $nextstatus;}
		 $newstatusid=$reqlinkcontroller->getParamItem('newstatusid');
		 $this->addParamItem('newstatusid',$newstatusid);
		  
		  //check vaidate newstatusid
		 //request change status
		 $requestlib=$this->getContainer()->get('badiu.admin.selection.request.lib');
		 $param=array('requestid'=>$requestid,'newstatusid'=>$newstatusid);
		 $rchangstatus=$requestlib->changeStatus($param);
		
		$rnstatus=$reqlinkcontroller->nextStep();
		if(!empty($rnstatus)){return $rnstatus;}
        return $result;
   }
   
   private function getContactdata(){
	    
        
		$personalcontact                      = array();
        $personalcontact['postcode']          = "";
        $personalcontact['address']           = "";
        $personalcontact['addressnumber']     = "";
        $personalcontact['addresscomplement'] = "";
        $personalcontact['neighborhood']      = "";
        $personalcontact['city']              = "";
        $personalcontact['stateid']           = "";
        $personalcontact['state']             = "";
        $personalcontact['stateshortname']    = "";
  
        $personalcontact['country']  = "";

        $personalcontact['phone']       = "";
        $personalcontact['phonemobile'] = $this->getParamItem('personalphonemobile');
        $personalcontact['email']       = $this->getParamItem('email');
		
		$contact                 = array();
        $contact['personal']     = $personalcontact;
        $contact['professional'] = "";
		$contactdatajsont        = $this->getJson()->encode($contact);
		return $contactdatajsont;
		
	}
}
