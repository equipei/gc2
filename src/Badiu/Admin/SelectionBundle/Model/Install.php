<?php

namespace Badiu\Admin\SelectionBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
        $result= $this->initDbStatus();
		$result+= $this->initDbProjectStatus();
		$result+= $this->initPermission();
           return $result;
    } 

     public function initDbStatus() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.selection.status.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist){return 0;}
         
         $list=array();
         $list['preregistration']=$this->getTranslator()->trans('badiu.admin.selection.status.preregistration');
         $list['waitanalysis']=$this->getTranslator()->trans('badiu.admin.selection.status.waitanalysis');
		 $list['acept']=$this->getTranslator()->trans('badiu.admin.selection.status.acept');
		 $list['dained']=$this->getTranslator()->trans('badiu.admin.selection.status.dained');
		 $list['inqueue']=$this->getTranslator()->trans('badiu.admin.selection.status.inqueue');
		 $list['cancelled']=$this->getTranslator()->trans('badiu.admin.selection.status.cancelled');
		 $list['inaccessible']=$this->getTranslator()->trans('badiu.admin.selection.status.inaccessible');
		 
                 
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity; 
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
             $param['deleted']=0;
            
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
         
     }
   
    public function initDbProjectStatus() { 
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.selection.projectstatus.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist){return 0;}
         
         $list=array();
         $list['active']=$this->getTranslator()->trans('badiu.admin.selection.projectstatus.active');
         $list['inactive']=$this->getTranslator()->trans('badiu.admin.selection.projectstatus.inactive');
                 
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
             $param['deleted']=0;
            
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
        return $cont; 
     }
	 
	  public function initPermission() {
         
         $libpermission = $this->getContainer()->get('badiu.system.access.lib.permission');
         $cont=0;

		$param=array('roleshortname'=>'guest',
		'permissions'=>array('badiu.admin.selection.requestenroll.link',
			'badiu.admin.selection.inforequest.dashboard',
			'badiu.admin.selection.loginsingin.add',
			'badiu.admin.selection.loginsingindoc.add',
			'badiu.system.core.service.process/badiu.admin.selection.inforequest.linkcontroller',
			'badiu.system.core.service.process/badiu.admin.selection.loginsingin.formcontroller',
			'badiu.system.core.service.process/badiu.admin.selection.loginsingindoc.formcontroller'
			));
         $cont+=$libpermission->add($param);
		 
         $param=array('roleshortname'=>'authenticateduser','permissions'=>array('badiu.admin.selection.statusrequest.dashboard','badiu.admin.selection.loginsingin.add','badiu.admin.selection.requestadd.link','badiu.admin.selection.loginsingin.edit'));
         $cont+=$libpermission->add($param);
          return $cont;
     }
}
