<?php

namespace Badiu\Admin\SelectionBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class RequestLink extends BadiuModelLib{
    private $selectioncode;
    function __construct(Container $container) {
            parent::__construct($container);
                $this->selectioncode=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
            }
    
        
        public function exec() {
			 
              //validate ukey  project code
              $this->checkSelectioncodeValidate();
			  
			  //change entity
			  $this->updateEntity();
			  
              //create transaction and redirect
              $this->creatRequest();
             

               return null;
         }
       
        private function checkSelectioncodeValidate() {
             if(empty($this->selectioncode)){
                 echo $this->getTranslator()->trans('badiu.admin.selection.inforequest.message.selectionkeyrequired');
                 exit;
             }
            $exist=$this->getContainer()->get('badiu.admin.selection.project.data')->countGlobalRow(array('skey'=>$this->selectioncode));
            if(!$exist){
                echo $this->getTranslator()->trans('badiu.admin.selection.inforequest.message.selectionkeynotfinddb',array('%ukey%'=>$this->selectioncode));exit;
            } else if ($exist > 1){
				  echo $this->getTranslator()->trans('badiu.admin.selection.inforequest.message.selectionkeyreplication',array('%ukey%'=>$this->selectioncode));exit;
			}
         }

		 private function updateEntity() {
			  $entity=$this->getContainer()->get('badiu.admin.selection.project.data')->getGlobalColumnValue('entity',array('skey'=>$this->selectioncode));
			  if(empty($entity)){  echo $this->getTranslator()->trans('badiu.admin.selection.inforequest.message.selectionentitynotfinddb',array('%ukey%'=>$this->selectioncode));exit;}
			 
			  //check is user loggued
			  $badiuSession = $this->getContainer()->get('badiu.system.access.session');
			  $badiuSession->setHashkey($this->getSessionhashkey());
			  $sessionData=$badiuSession->get();
			
			  $exist=$badiuSession->exist();
			
			  //check if entity of project if same of user loggued
			   if($exist){
				   if($entity!=$sessionData->getEntity()){$badiuSession->start($param,true); }
			   }else{
				   $param=array('entity'=>$entity);
				   $badiuSession->start($param,true); 
			   }
              
	 
		  }  
          private function creatRequest() {
			
                $projectid=$this->getContainer()->get('badiu.admin.selection.project.data')->getGlobalColumnValue('id',array('skey'=>$this->selectioncode));
				
                $param=array('projectid'=>$projectid);
                $inforeq=$this->getContainer()->get('badiu.admin.selection.inforequest.linkcontroller');
                $inforeq->addNew($param);
                $url=$inforeq->getUrlgoback();
				
				if(isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING'])){
					$queryString = $_SERVER['QUERY_STRING'];
					if(strpos($url,'?')=== FALSE){$url.="?$queryString";}
					else {$url.="&$queryString";}
				}
				header('Location: '.$url);
                exit;
         }

}
