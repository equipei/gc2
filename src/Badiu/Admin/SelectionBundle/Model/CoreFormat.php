<?php
namespace Badiu\Admin\SelectionBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;

class CoreFormat extends BadiuFormat {
  private $dataoptions;
  private $apputil;
 
    function __construct(Container $container) {
        parent::__construct($container);
        $this->dataoptions=$this->getContainer()->get('badiu.admin.selection.form.dataoptions');
		$this->apputil=$this->getContainer()->get('badiu.system.core.lib.util.app');
  }
  
    public function criteria($data) {
      $result = null;
      $criteria =$this->getUtildata()->getVaueOfArray($data,'analysiscriteria');
      $result =$this->dataoptions->getAnalysiscriteriaLabel($criteria);
	  return $result;
  }
public function linkaccess($data) {
      $result = null;
      $skey =$this->getUtildata()->getVaueOfArray($data,'skey');
	  if(!empty($skey)){$result=$this->apputil->getUrlByRoute('badiu.admin.selection.requestenroll.link',array('parentid'=>$skey));}
      return $result;
  }
}

