<?php

namespace Badiu\Admin\SelectionBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class RequestAddLink extends BadiuModelLib{
     function __construct(Container $container) {
            parent::__construct($container);
         }
    
        
        public function exec() {
            $requestid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
           
            $inforeq=$this->getContainer()->get('badiu.admin.selection.inforequest.linkcontroller');
            
            $param=array('requestid'=>$requestid);
            $result=$inforeq->requestEnrol($param);
            $info=$this->getUtildata()->getVaueOfArray($result,'info');
            $status=$this->getUtildata()->getVaueOfArray($result,'status');
            $urlgoback=$this->getUtildata()->getVaueOfArray($result,'message.urlgoback',true);
            $message=$this->getUtildata()->getVaueOfArray($result,'message.message',true);
           
            if($status=='accept'){
               if(empty($urlgoback)){echo 'url is null';exit;}
               header('Location: '.$urlgoback);
               exit;
            }else{echo " $info / $message";exit;}
       }
}
