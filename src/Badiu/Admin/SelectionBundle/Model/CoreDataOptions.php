<?php
namespace Badiu\Admin\SelectionBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class CoreDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
	   
	
	public  function getAnalysiscriteria(){
        
        $list=array();
        $list['manual']=$this->getAnalysiscriteriaLabel('manual');
		$list['automaticregistration']=$this->getAnalysiscriteriaLabel('automaticregistration');
		$list['listdocumentnumber']=$this->getAnalysiscriteriaLabel('listdocumentnumber');
		//$list['automaticservicecheck']=$this->getAnalysiscriteriaLabel('automaticservicecheck');
		$list['badiuadmincoupon']=$this->getAnalysiscriteriaLabel('badiuadmincoupon');
	    return  $list;
    }
	
	 public  function getAnalysiscriteriaLabel($value){
        $label=null;
        if($value=='manual'){$label=$this->getTranslator()->trans('badiu.admin.selection.project.analysiscriteria.manual');}
		else if($value=='automaticregistration'){$label=$this->getTranslator()->trans('badiu.admin.selection.project.analysiscriteria.none');}
		else if($value=='automaticservicecheck'){$label=$this->getTranslator()->trans('badiu.admin.selection.project.analysiscriteria.srvicecheck');}
		else if($value=='listdocumentnumber'){$label=$this->getTranslator()->trans('badiu.admin.selection.project.analysiscriteria.listdocumentnumber');}
		else if($value=='badiuadmincoupon'){$label=$this->getTranslator()->trans('badiu.admin.selection.project.analysiscriteria.badiuadmincoupon');}
	    return  $label;
    }
	
}
