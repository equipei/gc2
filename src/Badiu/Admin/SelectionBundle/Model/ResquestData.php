<?php

namespace Badiu\Admin\SelectionBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ResquestData extends BadiuDataBase
{

    public function __construct(Container $container, $bundleEntity)
    {
        parent::__construct($container, $bundleEntity);
    }

    public function getInfo($id)
    {
		$sql = "SELECT p.id AS projectid,p.analysiscriteria,p.servicecheckcriteria,p.listdocumentnumber,p.modulekey,p.moduleinstance,p.limitaccept,p.defaultstatus,p.defaultrole,u.id AS userid,u.doctype AS userdoctype,u.docnumber AS userdocnumber, s.id AS statusid,s.shortname AS statusshortname,o.coupon FROM " . $this->getBundleEntity() . " o JOIN o.projectid p JOIN o.statusid s LEFT JOIN o.userid u WHERE o.id=:id";
        $query      = $this->getEm()->createQuery($sql);
        $query->setParameter('id', $id);
        $result= $query->getOneOrNullResult();
		if ($result!=null && array_key_exists('id',$result)){$result=$result['id'];}
        return $result;
    }
	 public function getLastIdDuplicate($entity,$id,$userid,$projectid,$statusshortname){
		 $sql = "SELECT MAX(o.id) AS lastrequest FROM " . $this->getBundleEntity() . " o JOIN o.statusid s WHERE o.entity=:entity AND o.projectid =:projectid AND o.userid=:userid AND s.shortname = :statusshortname AND o.id != :id ";
		 $query      = $this->getEm()->createQuery($sql);
         $query->setParameter('entity', $entity);
		 $query->setParameter('id', $id);
		 $query->setParameter('userid', $userid);
		 $query->setParameter('projectid', $projectid);
		 $query->setParameter('statusshortname', $statusshortname);
		
        $result= $query->getOneOrNullResult();
		if ($result!=null && array_key_exists('lastrequest',$result)){$result=$result['lastrequest'];}
        return $result;
	 }
	 public function getLastIdCreated($entity,$userid,$projectid,$statusshortname){
		 $sql = "SELECT MAX(o.id) AS lastrequest FROM " . $this->getBundleEntity() . " o JOIN o.statusid s WHERE o.entity=:entity AND o.projectid =:projectid AND o.userid=:userid AND s.shortname = :statusshortname ";
		 $query      = $this->getEm()->createQuery($sql);
         $query->setParameter('entity', $entity);
		 $query->setParameter('userid', $userid);
		 $query->setParameter('projectid', $projectid);
		 $query->setParameter('statusshortname', $statusshortname);
		
        $result= $query->getOneOrNullResult();
		if ($result!=null && array_key_exists('lastrequest',$result)){$result=$result['lastrequest'];}
        return $result;
	 }
    public function countAccept($entity,$projectid,$statusshortname){
		 $sql = "SELECT COUNT(o.id) AS countrecord FROM " . $this->getBundleEntity() . " o JOIN o.statusid s WHERE o.entity=:entity AND o.projectid =:projectid AND s.shortname = :statusshortname ";
		 $query      = $this->getEm()->createQuery($sql);
         $query->setParameter('entity', $entity); 
		 $query->setParameter('projectid', $projectid);
		 $query->setParameter('statusshortname', $statusshortname);
		
        $result= $query->getOneOrNullResult();
		if ($result!=null && array_key_exists('countrecord',$result)){$result=$result['countrecord'];}
        return $result;
	 }	

	public function countUserExist($entity,$userid,$projectid){
		 $sql = "SELECT COUNT(o.id) AS countrecord FROM " . $this->getBundleEntity() . " o JOIN o.statusid s WHERE o.entity=:entity AND o.projectid =:projectid AND o.userid=:userid ";
		 $query      = $this->getEm()->createQuery($sql);
         $query->setParameter('entity', $entity); 
		 $query->setParameter('userid', $userid);
		 $query->setParameter('projectid', $projectid);
		
        $result= $query->getOneOrNullResult();
		if ($result!=null && array_key_exists('countrecord',$result)){$result=$result['countrecord'];}
        return $result;
	 }		 
}
