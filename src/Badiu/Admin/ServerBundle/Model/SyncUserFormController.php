<?php
namespace Badiu\Admin\ServerBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;

class SyncUserFormController extends BadiuFormController {

    
    function __construct(Container $container) {
        parent::__construct($container);
        $this->router=null;
    }
    
   public function validation() {
       
        $dpluser = $this->checkUserDuplicate();
        if ($dpluser != null) {
            return $dpluser;
        }
		
		 return null;
   }
public function changeParam() {
    if(!$this->isEdit()){
		 $userdata = $this->getContainer()->get('badiu.system.user.user.data');
		 $sysuserid=$this->getParamItem('sysuserid');
		 $userid=$this->getParamItem('userid');
		
		$fparam=array('id'=>$sysuserid);
		$userdto=$userdata->getGlobalColumnsValue('o.username,o.firstname,o.lastname,o.email',$fparam);
		$username=$this->getUtildata()->getVaueOfArray($userdto, 'username');
		$firstname=$this->getUtildata()->getVaueOfArray($userdto, 'firstname');
		$lastname=$this->getUtildata()->getVaueOfArray($userdto, 'lastname');
		$email=$this->getUtildata()->getVaueOfArray($userdto, 'email');
		
		$this->addParamItem('dtype','fromremoteservicetobadiunet');
		$this->addParamItem('keysync','username');
		$this->addParamItem('valuesync',$username);
		$this->addParamItem('sstatus','sucess');
		$this->addParamItem('istatus','completed');
		$this->addParamItem('tprocess','manual');
		$this->addParamItem('username',$username);
		$this->addParamItem('firstname',$firstname);
		$this->addParamItem('lastname',$lastname);
		$this->addParamItem('email',$email);


    }
    
    
 }
 


 public function checkUserDuplicate() {
        $syncuserdata = $this->getContainer()->get('badiu.admin.server.syncuser.data');
        
		$sysuserid = $this->getUtildata()->getVaueOfArray($this->getParam(), 'sysuserid');
		$serviceid = $this->getUtildata()->getVaueOfArray($this->getParam(), 'serviceid');
		$userid = $this->getUtildata()->getVaueOfArray($this->getParam(), 'userid');
		$entity=$this->getEntity();
		$fparam=array('sysuserid'=>$sysuserid,'serviceid'=>$serviceid,'entity'=>$entity);
		$count=$syncuserdata->countGlobalRow($fparam);
		$isedit=$this->isEdit();
		
		if($count > 0 && !$isedit){
			$message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.server.syncuser.duplicate');
            $info = 'badiu.admin.server.syncuser.duplicate';
            return $this->getResponse()->denied($info, $message);
		}
        if($count > 1 && $isedit){
			$message=array();
            $message['generalerror'] = $this->getTranslator()->trans('badiu.admin.server.syncuser.duplicate');
            $info = 'badiu.admin.server.syncuser.duplicate';
            return $this->getResponse()->denied($info, $message);
		}
        
        return null;
    }

}
