<?php

namespace Badiu\Admin\ServerBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ServiceFormDataOptions extends BadiuFormDataOptions {

	function __construct(Container $container, $baseKey = null) {
		parent::__construct($container, $baseKey);
	}

	 public function getDbType() {
         $list=array();
         $list['mysql']=$this->getTranslator()->trans('badiu.admin.server.dbtype.mysql');
         $list['pgsql']=$this->getTranslator()->trans('badiu.admin.server.dbtype.pgsql');
         $list['mssql']=$this->getTranslator()->trans('badiu.admin.server.dbtype.mssql');
         $list['oracle']=$this->getTranslator()->trans('badiu.admin.server.dbtype.oracle');
         $list['mongodb']=$this->getTranslator()->trans('badiu.admin.server.dbtype.mongodb');
         return $list;
     }

     public  function getFormChoice(){
        $badiuSession = $this->getContainer()->get('badiu.system.access.session');
        $entity = $badiuSession->get()->getEntity();
      
        $param=array('dtype'=>'site_moodle' );
        $data= $this->getContainer()->get('badiu.admin.server.service.data');
        $list=$data->getFormChoice($entity,$param);

        return $list;
    } 
	
	 public function getConnremotesatus() {
         $list=array();
         $list[200]=$this->getTranslator()->trans('badiu.admin.server.connremotesatus.online');
         $list[400]=$this->getTranslator()->trans('badiu.admin.server.connremotesatus.offline');
		 $list[410]=$this->getTranslator()->trans('badiu.admin.server.connremotesatus.offlinelocal');
		 $list[420]=$this->getTranslator()->trans('badiu.admin.server.connremotesatus.offlineintranet');
        return $list;
     }
	  public function getConnremotesatusLabel($status) {
         $label=null;
         if($status==200){$label=$this->getTranslator()->trans('badiu.admin.server.connremotesatus.online');}
         else if($status==400){$label=$this->getTranslator()->trans('badiu.admin.server.connremotesatus.offline');}
		 else if($status==410){$label=$this->getTranslator()->trans('badiu.admin.server.connremotesatus.offlinelocal');}
		 else if($status==420){$label=$this->getTranslator()->trans('badiu.admin.server.connremotesatus.offlineintranet');}
        return $label;
     }
}
