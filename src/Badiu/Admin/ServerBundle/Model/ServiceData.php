<?php

namespace Badiu\Admin\ServerBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class ServiceData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

			  
public function getInfoOfServiceForDs($id) {
        	$sql="SELECT o.id,o.url,o.name,o.dbtype,o.dbtblprefix,o.servicetoken,o.servicerurl,o.dtype,o.servicerurl1,o.servicerurl2,o.servicerurl3,o.deleted FROM ".$this->getBundleEntity()." o WHERE o.id=:id";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('id',$id);
                $result= $query->getOneOrNullResult();
                return  $result;
        
    }

public function getSinglelistByDtype($dtype) {
		$sql="SELECT o.id,o.name,o.entity FROM ".$this->getBundleEntity()." o WHERE o.dtype=:dtype AND o.deleted=:deleted";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('dtype',$dtype);
                $query->setParameter('deleted',FALSE);
                $result= $query->getResult();
        return  $result;
        
    }	
public function getMenuService($id) {

        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit=$sysoperation->isEdit();
      
        if($isEdit){
            $servicedata=$this->getContainer()->get('badiu.admin.server.service.data');
            $sql="SELECT c.id, c.name FROM ".$servicedata->getBundleEntity()." o JOIN o.machineid c WHERE o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
            return  $result;
           
            
        }else{
            
            return  $this->getNameById($id);
        }
       
       
    }

	public function exitToken($token) {
		$sql="SELECT COUNT(o.id) AS countrecord  FROM ".$this->getBundleEntity()." o WHERE o.servicetoken=:token";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('token',$token);
        $result= $query->getOneOrNullResult();
		$r=FALSE;
		if($result['countrecord']>0){$r=TRUE;}
        return $r;
       
        
    }
	
	public function getIdByToken($token) {
		$sql="SELECT o.id  FROM ".$this->getBundleEntity()." o WHERE o.servicetoken=:token";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('token',$token);
        $result= $query->getOneOrNullResult();
		$result=$result['id'];
        return $result;
       
        
    }
	
	public function findByToken($token) {
		$sql="SELECT o  FROM ".$this->getBundleEntity()." o WHERE o.servicetoken=:token";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('token',$token);
        $result= $query->getOneOrNullResult();
		return $result;
     }
     
    public function getInfoByToken($token) {
		$sql="SELECT o.id,o.url,o.name,o.entity,o.versionnumber,o.theme FROM ".$this->getBundleEntity()." o WHERE o.servicetoken=:token";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('token',$token);
                $result= $query->getOneOrNullResult();
        return  $result;
        
    }
    
    public function getInfoById($id) {
		$sql="SELECT o.id,o.url,o.name,o.entity FROM ".$this->getBundleEntity()." o WHERE o.id=:id";
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('id',$id);
                $result= $query->getOneOrNullResult();
        return  $result;
        
    }
	
	 public function getListByStringShortname($param) {
				$withoutstartshortnameinscheduler=null;
				if (array_key_exists('_withoutstartshortnameinscheduler',$param)){
					$withoutstartshortnameinscheduler=$param['_withoutstartshortnameinscheduler'];
					unset($param['_withoutstartshortnameinscheduler']);
				}
				
				$modulekeyinscheduler=null;
				if (array_key_exists('_modulekeyinscheduler',$param)){
					$modulekeyinscheduler=$param['_modulekeyinscheduler'];
					unset($param['_modulekeyinscheduler']);
				}
				$maxrecord=2500;
				if (array_key_exists('_maxrecord',$param)){
					$maxrecord=$param['_maxrecord'];
					unset($param['_maxrecord']);
				}
				if(empty($maxrecord)){$maxrecord=2500;}
				$wsqlst="";
				if(!empty($withoutstartshortnameinscheduler) && !empty($modulekeyinscheduler) ){
					$ltext="'".$withoutstartshortnameinscheduler."'";
					$wsqlst=" AND (SELECT COUNT(s.id) FROM BadiuSystemSchedulerBundle:SystemSchedulerTask s WHERE s.moduleinstance=o.id AND s.modulekey='".$modulekeyinscheduler."' AND  s.shortname = $ltext ) = 0 ";
				}
				
				
				$wsql=$this->makeSqlWhere($param);
				$sql="SELECT o.id,o.entity FROM ".$this->getBundleEntity()." o  WHERE  o.id > :bnetctrid $wsql $wsqlst ";
				
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('bnetctrid',0);
                $query=$this->makeSqlFilter($query, $param);
				$query->setMaxResults($maxrecord);
               $result= $query->getArrayResult();
             
			return $result;
    }
}
