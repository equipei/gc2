<?php

namespace Badiu\Admin\ServerBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ServiceController extends BadiuController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
              

     public function addMachineidToService($dto,$dtoParent) {
        $dto->setMachineid($dtoParent);
        return $dto;
        }


     public function findMachineidService($dto) {
      
        return $dto->getMachineid()->getId();
    }

}
