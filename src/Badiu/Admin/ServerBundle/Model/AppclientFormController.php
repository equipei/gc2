<?php
namespace Badiu\Admin\ServerBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;

class AppclientFormController extends BadiuFormController {

    
    function __construct(Container $container) {
        parent::__construct($container);
        $this->router=null;
    }
    
 
public function changeParam() {
    if(!$this->isEdit()){
      $this->addParamItem('servicerurl','/local/badiunet/synchttp/expdata.php');
      $this->addParamItem('servicerurl1','/local/badiuws/sync.php');
    
      $hash=$this->getContainer()->get('badiu.system.core.lib.util.hash');
      $skey=$hash->make(45);
      $this->addParamItem('servicekey',$skey);

    }
    
    
 }
 
 public function execAfter(){
  parent::execAfter();
    if(!$this->isEdit()){
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $dsession = $badiuSession->get();
        $userid=$dsession->getUser()->getId();
        $entity=$dsession->getEntity();
        $currentid=$this->getParamItem('id');

        $utiltoken=$this->getContainer()->get('badiu.system.core.lib.util.token');
        $paramtoken=array('user'=>$userid,'module'=>'appclient','entity'=>$entity,'instanceid'=>$currentid);
        $ntoken=$utiltoken->generate($paramtoken); 
        $servicedb=$this->getContainer()->get($this->getKminherit()->data());
        $paramupdate=array('id'=>$currentid,'servicetoken'=>$ntoken);
        $servicedb->updateNativeSql($paramupdate,false);
        $this->addParamItem('servicetoken',$ntoken);
        
      }
    } 


}
