<?php

namespace Badiu\Admin\ServerBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ServiceFormat extends BadiuFormat {
	private $servicedataoptions;
	function __construct(Container $container) {
		 parent::__construct($container);
		 $this->servicedataoptions=$this->getContainer()->get('badiu.admin.server.service.form.dataoptions');
	}

       public function connremotesatus($data) {
		   $status=$this->getUtildata()->getVaueOfArray($data,'connremotesatus');
           $status= $this->servicedataoptions->getConnremotesatusLabel($status);
		   return $status;
        }
    
}
