<?php

namespace Badiu\Admin\ServerBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class SmtpFormDataOptions extends BadiuFormDataOptions {

	function __construct(Container $container, $baseKey = null) {
		parent::__construct($container, $baseKey);
	}

	public function getSecureConnetion() {
		$list = array();
		$list['STARTTLS'] = 'STARTTLS';
		$list['SSL/TLS'] = 'SSL/TLS';

		return $list;
	}

	public function getAuthenticationMode() {
		$list = array();
		$list['passwords'] = 'passwords';
		$list['CRAM-MD5'] = 'CRAM-MD5';

		return $list;
	}

}
