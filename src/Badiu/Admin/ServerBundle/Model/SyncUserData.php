<?php

namespace Badiu\Admin\ServerBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class SyncUserData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

			  
public function getSysuserid($param) {
			$wsql=$this->makeSqlWhere($param);
        	$sql="SELECT u.id FROM ".$this->getBundleEntity()." o JOIN o.sysuserid u WHERE o.id > :bnetctrid $wsql";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('bnetctrid',0);
			$query=$this->makeSqlFilter($query, $param);
            $result= $query->getSingleResult();
            if(isset($result['id'])){ $result=$result['id'];}
            else {return null;}
             return $result;
        
    } 

}
