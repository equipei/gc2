<?php

namespace Badiu\Admin\ServerBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ManageServerSyncUser extends BadiuModelLib {
	
	private $param;
	
    function __construct(Container $container) {
               parent::__construct($container);
    }

    public function process($param){
       $this->param=$param;
	   $result=$this->checkConfig();
	   if(!empty($result)){return $result;}
	  
	   $result=$this->checkExist();
	   if(!empty($result)){return $result;}
	   
	   //add sync
	   $result=$this->addSync();
	   if(!empty($result)){return $result;}
	  
	  return null;	   
    }
	 public function checkConfig(){
		
		 $paramreq=$this->param;
		 $anonymousaccess=$this->getUtildata()->getVaueOfArray($paramreq,'user.anonymousaccess',true);
		 $autosync=$this->getUtildata()->getVaueOfArray($paramreq,'user.autosync',true);
		 
		 if(!$autosync){
			$result=array();
			$result['status']='danied';
			$result['info']='badiu.admin.server.syncuser.remoteconfigautosyncdesabled';
			$result['message']='Remote config autosync not enabled';
			return  $result; 
		    
		 }
		 if($anonymousaccess){
			 $result=array();
			 $result['status']='danied';
			 $result['info']='badiu.admin.server.syncuser.remoteanonymousaccess';
			 $result['message']='Remote user access is anonymous';
			 return  $result; 
		 }
		
		 $serviceid=$this->getUtildata()->getVaueOfArray($paramreq,'sserver.id',true);
		 $entity=$this->getUtildata()->getVaueOfArray($paramreq,'sserver.entity',true);
		   
		 if(empty($serviceid)){
			$result=array();
			$result['status']='danied';
			$result['info']='badiu.admin.server.syncuser.paramserverserviceisempty';
			$result['message']='Require id of serser service';
			return  $result; 
		 }
		 
		 if(empty($entity)){
			$result=array();
			$result['status']='danied';
			$result['info']='badiu.admin.server.syncuser.paramentityisempty';
			$result['message']='Require entity param';
			return  $result; 
		 }
		 
		 //validate server serive param
	
		 $paramcheck=array('id'=>$serviceid,'entity'=>$entity);
		 $sservicedata=$this->getContainer()->get('badiu.admin.server.service.data');
		 $eexist=$sservicedata->countGlobalRow($paramcheck);
		
		if(!$eexist){
			$result=array();
			$result['status']='danied';
			$result['info']='badiu.admin.server.syncuser.paramentitynotvalidforserverserviceid';
			$result['message']='Param entity not setted in server service with id '.$serviceid;
			return  $result; 
		 }
		 return null;
 
	 }
	 
	 public function checkExist(){
		 $paramreq=$this->param;
		 $userid=$this->getUtildata()->getVaueOfArray($paramreq,'user.id',true);
		 $serviceid=$this->getUtildata()->getVaueOfArray($paramreq,'sserver.id',true);
		 $entity=$this->getUtildata()->getVaueOfArray($paramreq,'sserver.entity',true);
		 
		 $email=$this->getUtildata()->getVaueOfArray($paramreq,'user.email',true);
		 $firstname=$this->getUtildata()->getVaueOfArray($paramreq,'user.firstname',true);
		 $lastname=$this->getUtildata()->getVaueOfArray($paramreq,'user.lastname',true);
		 if(empty($lastname)){$lastname=' ';}
		 $forceupdate=$this->getUtildata()->getVaueOfArray($paramreq,'user._forceupdate',true);
		 $paramcheck=array('userid'=>$userid,'serviceid'=>$serviceid,'entity'=>$entity);
	
		//check exit in serverservice
		 $syncuserdata=$this->getContainer()->get('badiu.admin.server.syncuser.data');
		 $uexist=$syncuserdata->countGlobalRow($paramcheck);
	
		 $result=array();
		if($uexist){
			 $sisuser=$syncuserdata->getSysuserid($paramcheck);
			 $result=array();
			 if(!empty($sisuser)){
				  if($forceupdate){$this->updateSync($sisuser);}
				  $result['status']='acept';
				  $result['info']='badiu.admin.server.syncuser.remotesyncjustexist';
				  $result['message']=$sisuser;
				 
				return  $result; 
			}else{
				 $result['status']='danied';;
				 $result['info']='badiu.admin.server.syncuser.remotesyncjustexistfailuretofind';
				 $result['message']='Problem to find user just syncronized';
				 return  $result; 
			}
			
		 }
		return null;
		
	 }
	 
	 public function addSync(){
		 
		 $result=array();
		 $paramreq=$this->param;
	     $keysync=$this->getUtildata()->getVaueOfArray($paramreq,'user.keysync',true);
		 $userid=$this->getUtildata()->getVaueOfArray($paramreq,'user.id',true);
		 $username=$this->getUtildata()->getVaueOfArray($paramreq,'user.username',true);
		 $idnumber=$this->getUtildata()->getVaueOfArray($paramreq,'user.idnumber',true);
		 $email=$this->getUtildata()->getVaueOfArray($paramreq,'user.email',true);
		 $firstname=$this->getUtildata()->getVaueOfArray($paramreq,'user.firstname',true);
		 $lastname=$this->getUtildata()->getVaueOfArray($paramreq,'user.lastname',true);
		 if(empty($lastname)){$lastname=' ';}
		 $serviceid=$this->getUtildata()->getVaueOfArray($paramreq,'sserver.id',true);
		 $entity=$this->getUtildata()->getVaueOfArray($paramreq,'sserver.entity',true);
		 
		 
		 //check exit  in serverservice
		 $paramcheck=array('userid'=>$userid,'serviceid'=>$serviceid,'entity'=>$entity);
	 	 $syncuserdata=$this->getContainer()->get('badiu.admin.server.syncuser.data');
		 $uexist=$syncuserdata->countGlobalRow($paramcheck);
		
		if($uexist){
			 $sisuser=$syncuserdata->getSysuserid($paramcheck);
			 $result=array();
			 if(!empty($sisuser)){
				  $result['status']='acept';
				  $result['info']='badiu.admin.server.syncuser.remotesyncjustexist';
				  $result['message']=$sisuser;
				return  $result; 
			}else{
				 $result['status']='danied';;
				 $result['info']='badiu.admin.server.syncuser.remotesyncjustexistfailuretofind';
				 $result['message']='Problem to find user just syncronized';
				 return  $result; 
			}
			
		 }
		
		 
		 if(empty($keysync)){
			   $result['status']='danied';
			   $result['info']='badiu.admin.server.syncuser.remoteconfigkeysyncisempty';
				$result['message']='Remote config keysync is empty';
				return  $result; 
		 }
		 
		 $key=$keysync;
		 $value=null;
		 if($keysync=='username'){$value=$username;}
		 else if($keysync=='idnumber'){$value=$idnumber;}
		 else if($keysync=='email'){$value=$email;}
		
		
		if(empty($value)){
			   $result['status']='danied';
			   $result['info']='badiu.admin.server.syncuser.remoteconfigkeysyncvalueisempty';
				$result['message']='Remote config keysync value is empty';
				return  $result; 
		 }
		 
		 
		 $paramexist=array('entity'=>$entity,$key=>$value);
		 $userdata=$this->getContainer()->get('badiu.system.user.user.data');
		
		 $existuser=$userdata->countGlobalRow($paramexist);
		 $sysuserid=$userdata->getGlobalColumnValue('id',$paramexist);
		if($existuser && empty($sysuserid)){
				 $result['status']='danied';
				 $result['info']='badiu.admin.server.syncuser.remotesyncjustexistinsystemuserfailuretofind';
				 $result['message']='Problem to find user existent user not syncronized yet in table system_user';
				 return  $result; 
		}else if($existuser && !empty($sysuserid)){
			$syncuserparami=array('entity'=>$entity,'sysuserid'=>$sysuserid,'serviceid'=>$serviceid,'userid'=>$userid,'dtype'=>'fromremoteservicetobadiunet','keysync'=>$keysync,'valuesync'=>$value,'sstatus'=>'sucess','istatus'=>'completed','tprocess'=>'automatic','username'=>$username,'firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'idnumber'=>$idnumber,'deleted'=>0,'timecreated'=>new \DateTime());
			$syncnewuserid=$syncuserdata->insertNativeSql($syncuserparami,false);
			$result['status']='acept';
			$result['info']='badiu.admin.server.syncuser.remotesyncjustexistsyncsuccess';
			$result['message']=$sysuserid;
			return  $result; 
		}
		
		$clientuser=$this->getUtildata()->getVaueOfArray($paramreq,'user');
		$server=$this->getUtildata()->getVaueOfArray($paramreq,'sserver');
		$syncserver=array('sync'=>array('server'=>$server,'user'=>$clientuser));
		$dconfig=$this->getJson()->encode($syncserver);
	  
		$parami=array('entity'=>$entity,'username'=>$username,'doctype'=>'EMAIL','docnumber'=>$email,'firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'idnumber'=>$idnumber,'dconfig'=>$dconfig,'auth'=>'manual','deleted'=>0,'timecreated'=>new \DateTime());
		$newuserid=$userdata->insertNativeSql($parami,false);
		 
		//add to sync
		if(!empty($newuserid)){
			$syncuserparami=array('entity'=>$entity,'sysuserid'=>$newuserid,'serviceid'=>$serviceid,'userid'=>$userid,'dtype'=>'fromremoteservicetobadiunet','keysync'=>$keysync,'valuesync'=>$value,'sstatus'=>'sucess','istatus'=>'completed','tprocess'=>'automatic','username'=>$username,'firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'idnumber'=>$idnumber,'deleted'=>0,'timecreated'=>new \DateTime());
			$syncnewuserid=$syncuserdata->insertNativeSql($syncuserparami,false);
			
			$result['status']='acept';
			$result['info']='badiu.admin.server.syncuser.newsyncedusersuccess';
			$result['message']=$newuserid;
			return  $result; 
		}else{
			 $result['status']='danied';;
			 $result['info']='badiu.admin.server.syncuser.newsynceduserfailure';
			 $result['message']='Problem to add new user in table system_user';
			 return  $result; 
		}
		
	
		return $result;
	 }
	 
	  public function updateSync($sysuserid){
		 $paramreq=$this->param;
		 $firstname=$this->getUtildata()->getVaueOfArray($paramreq,'user.firstname',true);
		 $lastname=$this->getUtildata()->getVaueOfArray($paramreq,'user.lastname',true);
		 $email=$this->getUtildata()->getVaueOfArray($paramreq,'user.email',true);
		 $userdata=$this->getContainer()->get('badiu.system.user.user.data');
		 $parami=array('id'=>$sysuserid,'firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'timemodified'=>new \DateTime());
		$newuserid=$userdata->updateNativeSql($parami,false);
		
	  }
    function getParam() { 
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }


}
