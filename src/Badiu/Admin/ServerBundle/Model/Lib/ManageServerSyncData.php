<?php

namespace Badiu\Admin\ServerBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class ManageServerSyncData extends BadiuModelLib {
	
	private $param;
	
    function __construct(Container $container) {
               parent::__construct($container);
    }

    public function process($param){
		$entity=$this->getEntity();
		$serviceid=$this->getUtildata()->getVaueOfArray($param,'lmsintegration.sserviceid',true);
		$modulekey=$this->getUtildata()->getVaueOfArray($param,'_sysconfig.modulekey',true);
		$moduleinstance=$this->getUtildata()->getVaueOfArray($param,'_sysconfig.moduleinstance',true);
		
		$dtype='frombadiunetremoteservice';
		$remotedatatype=$this->getUtildata()->getVaueOfArray($param,'lmsintegration.lmssynclevel',true);
		$remotedataname=null;
		$remotedataid=null;
		
		if($remotedatatype=='coursecat'){
			$remotedataname=$this->getUtildata()->getVaueOfArray($param,'lmsintegration.lmscoursecatname',true);
			$remotedataid=$this->getUtildata()->getVaueOfArray($param,'lmsintegration.lmscoursecatid',true);
		}else if($remotedatatype=='course'){
			$remotedataname=$this->getUtildata()->getVaueOfArray($param,'lmsintegration.lmscoursename',true);
			$remotedataid=$this->getUtildata()->getVaueOfArray($param,'lmsintegration.lmscourseid',true);
		}else if($remotedatatype=='group'){
			$remotedataname=$this->getUtildata()->getVaueOfArray($param,'lmsintegration.lmscoursegroupname',true);
			$remotedataid=$this->getUtildata()->getVaueOfArray($param,'lmsintegration.lmscoursegroupid',true);
		}
		
		$sstatus=$this->getUtildata()->getVaueOfArray($param,'lmsintegration.status',true);
		$tprocess='automatic';
		$dconfig=$this->getUtildata()->getVaueOfArray($param,'lmsintegration');
		$dconfig= $this->getJson()->encode($dconfig);
		$syncdata=$this->getContainer()->get('badiu.admin.server.syncdata.data');
		$paramcheckexist=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'serviceid'=>$serviceid);
		$paramadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'serviceid'=>$serviceid,'dtype'=>$dtype,'remotedatatype'=>$remotedatatype,'remotedataname'=>$remotedataname,'remotedataid'=>$remotedataid,'sstatus'=>$sstatus,'tprocess'=>$tprocess,'dconfig'=>$dconfig,'timecreated'=>new \DateTime());
		$paramedit=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'serviceid'=>$serviceid,'dtype'=>$dtype,'remotedatatype'=>$remotedatatype,'remotedataname'=>$remotedataname,'remotedataid'=>$remotedataid,'sstatus'=>$sstatus,'tprocess'=>$tprocess,'dconfig'=>$dconfig,'timemodified'=>new \DateTime());
        $result=$syncdata->addNativeSql($paramcheckexist,$paramadd,$paramedit,true);
        $r=$this->getUtildata()->getVaueOfArray($result,'id');
	
	  return  $r;	   
    }
	
    function getParam() { 
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }


}
