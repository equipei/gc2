<?php

namespace Badiu\Admin\ServerBundle\Model\Lib\Moodle;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Admin\ServerBundle\Model\ServiceData;

class MoodleData  extends ServiceData {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

    public function findByEntity($entity,$orderby="",$deleted=false) {
             $sql="SELECT  o FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.dtype=:dtype  AND o.deleted=:deleted $orderby";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
            $query->setParameter('dtype','site_moodle');
	    $query->setParameter('deleted',$deleted);
            $result= $query->getResult();
            return  $result; 
        }
		
	public function getFormChoice($entity,$param=array('dtype'=>'site_moodle'),$orderby="") {
	   $result=parent::getFormChoice($entity,$param,$orderby);
       return  $result;
    }
}


