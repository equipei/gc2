<?php

namespace  Badiu\Admin\ServerBundle\Model\Lib\WordPress;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class WordPressWebserviceClient extends BadiuModelLib{ 
      
    private $sserviceid=null;
    function __construct(Container $container) {
            parent::__construct($container);
            $this->sserviceid=$this->getContainer()->get('request')->get('_sservice');
              } 
          
              
              //http://fabrica.badiu.com.br/~colaborador7/badiunet/web/app_dev.php/system/service/process?_service=badiu.admin.server.servicewordpress.lib.webserviceclient
             public function exec() {
                 $this->sserviceid=17;
            // return $this->countNewOrder(5009);
             // return $this->getNewOrderid(5000);
         return $this->getProductsCodeByOrderid(5009);
             // return   $this->getPostWithConfig(5055);
            // return  $this->getUserOrder(5009);
           //   return  $this->getUserById(2);
          //   return  $this->getItemsByOrderId(5055);
              //   return  $this->getItemById(1);
             //    return  $this->getPostValueByKey(4985,'_sku');
         }
         
           public function countNewOrder($idorder=0) {
               $param=array('_serviceid'=>$this->sserviceid,'key'=>'sql.command.exec','type'=>'S','query'=>"SELECT COUNT(ID) AS coutrecord FROM {_pfx}posts WHERE ID > $idorder AND  post_type='shop_order'");
               $result=  $this->getSearch()->searchWebService($param);
               return $result['message']['coutrecord']; 
          }
          
            public function getNewOrderid($idorder=0) {
             $param=array('_serviceid'=>$this->sserviceid,'key'=>'sql.command.exec','type'=>'M','query'=>"SELECT ID AS id,post_name AS name FROM {_pfx}posts WHERE ID > $idorder AND  post_type='shop_order'",'limit'=>50000);
              $result=  $this->getSearch()->searchWebService($param);
              return $result['message']; 
          }
          
           public function getUserOrder($idorder) {
              $param=array('_serviceid'=>$this->sserviceid,'key'=>'sql.command.exec','type'=>'S','query'=>"SELECT meta_value FROM {_pfx}postmeta WHERE post_id=$idorder  AND meta_key='_customer_user'");
              $result=  $this->getSearch()->searchWebService($param);
              return $result['message']['meta_value']; 
          }
          //review on update postlib.php
           public function getPostWithConfig($id) {
              $param=array('_serviceid'=>$this->sserviceid,'key'=>'post.post.getwithconfig','id'=>$id);
              $result=  $this->getSearch()->searchWebService($param);
              $result=$result['message'];
              //temp
              $param=array('_serviceid'=>$this->sserviceid,'key'=>'sql.command.exec','type'=>'M','limit'=>2500,'query'=>"SELECT meta_id,meta_key,meta_value FROM {_pfx}postmeta WHERE post_id=$id");
              $resultconf=  $this->getSearch()->searchWebService($param);
             
              $resultconf=$resultconf['message']; 
             $result['metadataconfig']=$resultconf;
              return $result;
          }
          
           public function getPostValueByKey($postid,$metakey) {
              $param=array('_serviceid'=>$this->sserviceid,'key'=>'sql.command.exec','type'=>'S','query'=>"SELECT meta_value FROM {_pfx}postmeta WHERE post_id=$postid AND meta_key='".$metakey."'");
              $result=  $this->getSearch()->getDataFromWebService($param);
              $result= $result['message']['meta_value'];
              return $result;
          }
          //user
           public function getUserById($id) {
               $param=array('_serviceid'=>$this->sserviceid,'key'=>'user.user.getwithconfig','id'=>$id);
               $result=  $this->getSearch()->searchWebService($param);
               return $result['message']; 
          }
          
          public function getValueFromMetadaByValue($metadata,$key) {
               foreach ($metadata as $meta) {
                   $mkey=$meta['meta_key'];
                   $mvalue=$meta['meta_value'];
                   if ($mkey==$key){return $mvalue;}
               }
               return null;
            } 
            
           
            
            //item
          public function getItemsByOrderId($id) {
              $param=array('_serviceid'=>$this->sserviceid,'key'=>'woocommerce.item.getlistbyorderid','id'=>$id);
              $result=  $this->getSearch()->searchWebService($param);
               return $result['message']; 
          }
           
            public function getItemById($id) {
              $param=array('_serviceid'=>$this->sserviceid,'key'=>'woocommerce.item.getwithconfig','id'=>$id);
               $result=  $this->getSearch()->searchWebService($param);
              return $result['message']; 
          }
        
           public function getProductsCodeByOrderid($orderid) {
              $param=array('_serviceid'=>$this->sserviceid,'key'=>'sql.command.exec','type'=>'M','limit'=>2500,'query'=>"SELECT DISTINCT pm.meta_value,i.order_item_id FROM {_pfx}woocommerce_order_items i INNER JOIN {_pfx}woocommerce_order_itemmeta im ON i.order_item_id=im.order_item_id INNER JOIN {_pfx}postmeta pm ON pm.post_id=im.meta_value WHERE i.order_id =$orderid AND im.meta_key='_product_id' AND pm.meta_key ='_sku'");
              $result=  $this->getSearch()->getDataFromWebService($param);
              $result= $result['message'];
              return $result;
          }
          public function getUsridBySgauserid($userid) {
               $param=array('_serviceid'=>$this->sserviceid,'key'=>'sql.command.exec','type'=>'S','query'=>"SELECT user_id AS userid FROM {_pfx}usermeta WHERE meta_key='sga_uid' AND meta_value=$userid");
               $result=  $this->getSearch()->searchWebService($param);
               $r=null;
               if(isset($result['message']['userid'])){$r=$result['message']['userid'];}
               return $r; 
          }
         
           public function getUsridBySgaukey($ukey) {
               $param=array('_serviceid'=>$this->sserviceid,'key'=>'sql.command.exec','type'=>'S','query'=>"SELECT user_id AS userid FROM {_pfx}usermeta  WHERE meta_key='sga_ukey' AND meta_value='".$ukey."'");
               $result=  $this->getSearch()->searchWebService($param);
               $r=null;
               if(isset($result['message']['userid'])){$r=$result['message']['userid'];}
               return $r; 
          }
          
           public function getProductidByCode($code) {
               $param=array('_serviceid'=>$this->sserviceid,'key'=>'sql.command.exec','type'=>'S','query'=>"SELECT post_id AS productid FROM {_pfx}postmeta WHERE meta_key='_sku' AND meta_value='".$code."'");
               $result=  $this->getSearch()->searchWebService($param);
               $r=null;
               if(isset($result['message']['productid'])){$r=$result['message']['productid'];}
               return $r; 
          }
       
           public function getOrdersidByProductid($productid,$lastid=0,$limit=2500) {
              $param=array('_serviceid'=>$this->sserviceid,'key'=>'sql.command.exec','type'=>'M','query'=>"SELECT DISTINCT wo.order_id AS orderid FROM {_pfx}woocommerce_order_items wo INNER JOIN {_pfx}woocommerce_order_itemmeta woi ON wo.order_item_id=woi.order_item_id WHERE woi.meta_key='_product_id' AND woi.meta_value ='".$productid."' AND wo.order_id > $lastid ORDER BY wo.order_id ",'limit'=>$limit);
              $result=  $this->getSearch()->searchWebService($param);
             if(isset($result['message'])){
                  $result=$result['message'];
              }
              return $result; 
            
          }
            
            function getSserviceid() {
                return $this->sserviceid;
            }

            function setSserviceid($sserviceid) {
                $this->sserviceid = $sserviceid;
            }


}              
