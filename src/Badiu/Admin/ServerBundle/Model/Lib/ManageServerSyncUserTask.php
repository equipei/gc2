<?php

namespace Badiu\Admin\ServerBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

class ManageServerSyncUserTask  extends  TaskGeneralExec{
   private $cresult;
   function __construct(Container $container) {
                parent::__construct($container);
				$this->cresult=array('datashouldexec' => 0, 'dataexec' => 0);
        } 

  public function import() {
   
    
   return $this->cresult;
}

public function update() {
   $this->syncUpdate();
   
   return $this->cresult;
}


public function syncUpdate() {
		$operation=$this->getUtildata()->getVaueOfArray($this->getParam(),'operation');
		if($operation!='syncupdatemoodle'){return null;}
		
		$target=$this->getUtildata()->getVaueOfArray($this->getParam(),'target');
		$maxrecord=$this->getUtildata()->getVaueOfArray($this->getParam(),'maxrecord');
		$timeupdate=$this->getUtildata()->getVaueOfArray($this->getParam(),'timeupdate');
		if(empty($maxrecord)){$maxrecord=50;}
		
		$listusers=null;
		if($target=='moodle'){
			$fparam=array('maxrecord'=>$maxrecord,'timeupdate'=>$timeupdate);
			$listusers=$this->getNewUserUpdated($fparam);
		}
		if(!is_array($listusers)){return null;}
		 $presult=array('shouldprocess'=>sizeof($listusers),'executed'=>0);
		 $count=0;
		
		foreach ($listusers as $row) {
			//o.id,o.username,o.ukey, o.firstname,o.lastname,o.email,o.doctype,o.docnumber,sy.userid, ss.id AS serviceid
			  $serviceid =$this->getUtildata()->getVaueOfArray($row,'serviceid');
			  $userid =$this->getUtildata()->getVaueOfArray($row,'userid');
			  $username = $this->getUtildata()->getVaueOfArray($row,'username');
			  $email= $this->getUtildata()->getVaueOfArray($row,'email');
		      $firstname = $this->getUtildata()->getVaueOfArray($row,'firstname');
		      $lastname = $this->getUtildata()->getVaueOfArray($row,'lastname');
		      $idnumber = $this->getUtildata()->getVaueOfArray($row,'idnumber');
			  $enablealternatename = $this->getUtildata()->getVaueOfArray($row,'enablealternatename');
			  $alternatename = $this->getUtildata()->getVaueOfArray($row,'alternatename');
		 
			  $param = array('_serviceid' => $serviceid, '_key' => 'user.user.update', 'id' => $userid,'username' => $username, 'email' => $email,'auth' => "badiuauth", 'firstname' => $firstname, 'lastname' => $lastname,'alternatename'=>$alternatename);
			  $result = $this->getSearch()->execWebService($param);
			  $rstatus=$this->getUtildata()->getVaueOfArray($result,'status');
			
			  $rstatus=$this->getUtildata()->getVaueOfArray($result,'status');
			  if($rstatus == 'accept'){$presult['executed']++;}
			  $count++;
		  }
		  
		  $datashouldexec=$this->getUtildata()->getVaueOfArray($presult,'shouldprocess');
		  $dataexec=$this->getUtildata()->getVaueOfArray($presult,'executed');
			    $message=null;
			    $resultmdl=array('datashouldexec' => $datashouldexec, 'dataexec' => $dataexec, 'message' => $message);
				$this->cresult = $this->addResult($this->cresult, $resultmdl); 
			  
		  return $count;
	}
	
	

	// revew set incremental data
  public function getNewUserUpdated($param) {
		$maxrecord=$this->getUtildata()->getVaueOfArray($param,'maxrecord');
		$timeupdate=$this->getUtildata()->getVaueOfArray($param,'timeupdate');
		$data=$this->getContainer()->get('badiu.admin.server.syncuser.data');
		if(empty($timeupdate)){$timeupdate=20;}
		$datemodified=new \DateTime();
		$datemodified->modify('-'.$timeupdate.' minutes');
				 
	    $sql = "SELECT o.id,o.username,o.ukey, o.firstname,o.lastname,o.email,o.doctype,o.docnumber,o.alternatename,o.enablealternatename,sy.userid, ss.id AS serviceid FROM  ".$data->getBundleEntity()." sy JOIN sy.sysuserid o JOIN sy.serviceid ss WHERE  sy.id > 0 AND o.timemodified >= :timemodified ORDER BY o.timemodified DESC ";
        $query = $data->getEm()->createQuery($sql);
		$query->setParameter('timemodified',$datemodified);
		$query->setMaxResults($maxrecord);
		$result = $query->getResult();
		return $result;
		
    } 	
}
?>