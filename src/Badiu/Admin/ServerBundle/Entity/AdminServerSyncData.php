<?php

namespace Badiu\Admin\ServerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminServerSyncData
 *
 * @ORM\Table(name="admin_server_sync_data", uniqueConstraints={
 *
 *      @ORM\UniqueConstraint(name="admin_server_sync_data_uix", columns={"entity","serviceid","modulekey","moduleinstance"})},
 *       indexes={@ORM\Index(name="admin_server_sync_data_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_server_sync_data_idnumber_ix", columns={"idnumber"}),
 *              @ORM\Index(name="admin_server_sync_data_remotedatatype_ix", columns={"remotedatatype"}),
 *              @ORM\Index(name="admin_server_sync_data_deleted_ix", columns={"deleted"}),
 *               @ORM\Index(name="admin_server_sync_data_remotedataid_ix", columns={"remotedataid"}),
 *              @ORM\Index(name="admin_server_sync_data_remotedataid_ix", columns={"remotedataid"}),
 *              @ORM\Index(name="admin_server_sync_data_remotedatakey_ix", columns={"remotedatakey"}),
 *              @ORM\Index(name="admin_server_sync_data_keysync_ix", columns={"keysync"}),
 *              @ORM\Index(name="admin_server_sync_data_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_server_sync_data_sstatus_ix", columns={"sstatus"}),
 *              @ORM\Index(name="admin_server_sync_data_istatus_ix", columns={"istatus"}),
 *              @ORM\Index(name="admin_server_sync_data_tprocess_ix", columns={"tprocess"}),
 *              @ORM\Index(name="admin_server_sync_data_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_server_sync_data_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="admin_server_sync_data_keysync_ix", columns={"keysync"})})
 *
 * @ORM\Entity
 */

class AdminServerSyncData {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

		
	/**
     * @var AdminServerService
     *
     * @ORM\ManyToOne(targetEntity="AdminServerService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="serviceid", referencedColumnName="id")
     * })
     */
    private $serviceid;
		
	/**
	@var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
	 */
	private $dtype; //fromremoteservicetobadiunet | frombadiunetremoteservice
	
	/**
	@var string
	 *
	 * @ORM\Column(name="remotedatatype", type="string", length=50, nullable=false)
	 */
	private $remotedatatype;
	
	/**
     * @var string
     *
     * @ORM\Column(name="remotedataname", type="string", length=255, nullable=true)
     */
    private $remotedataname;
	
	
	  /**
   * @var integer
   *
   * @ORM\Column(name="remotedataid", type="bigint", nullable=true)
     */
    private $remotedataid;
	
	/**
     * @var string
     *
     * @ORM\Column(name="remotedatakey", type="string", length=255, nullable=true)
     */
    private $remotedatakey;
	
		 /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=false)
     */
    private $modulekey;


    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=false)
     */
    private $moduleinstance;
	
	
	/**
	@var string
	 *
	 * @ORM\Column(name="keysync", type="string", length=50, nullable=true)
	 */
	private $keysync; //id | shortname | idnumber
	
	
	
	
	/**
	@var string
	 *
	 * @ORM\Column(name="sstatus", type="string", length=50, nullable=true)
	 */
	private $sstatus; //sucess | failure
	
	/**
	@var string
	 *
	 * @ORM\Column(name="istatus", type="string", length=50, nullable=true)
	 */
	private $istatus; //completed | duplication | parentsync
	
	/**
	@var string
	 *
	 * @ORM\Column(name="tprocess", type="string", length=50, nullable=true)
	 */
	private $tprocess; //automatic | manual
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
     

/**
 * @var string
 *
 * @ORM\Column(name="param", type="text", nullable=true)
 */
	private $param;
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="integer", nullable=false)
	 */
	private $deleted;

	
        function getId() {
            return $this->id;
        }

        function getEntity() {
            return $this->entity;
        }

       
       
        function getIdnumber() {
            return $this->idnumber;
        }

       
        function getParam() {
            return $this->param;
        }

        function getTimecreated(){
            return $this->timecreated;
        }

        function getTimemodified(){
            return $this->timemodified;
        }

        function getUseridadd() {
            return $this->useridadd;
        }

        function getUseridedit() {
            return $this->useridedit;
        }

        function getDeleted() {
            return $this->deleted;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setEntity($entity) {
            $this->entity = $entity;
        }


        function setIdnumber($idnumber) {
            $this->idnumber = $idnumber;
        }



        function setParam($param) {
            $this->param = $param;
        }

        function setTimecreated(\DateTime $timecreated) {
            $this->timecreated = $timecreated;
        }

        function setTimemodified(\DateTime $timemodified) {
            $this->timemodified = $timemodified;
        }

        function setUseridadd($useridadd) {
            $this->useridadd = $useridadd;
        }

        function setUseridedit($useridedit) {
            $this->useridedit = $useridedit;
        }

        function setDeleted($deleted) {
            $this->deleted = $deleted;
        }

       function getDconfig() {
            return $this->dconfig;
        }
    
        function setDconfig($dconfig) {
            $this->dconfig = $dconfig;
        }
    
	  public function setServiceid($serviceid) {
        $this->serviceid = $serviceid;
    }

	public function getServiceid() {
        return $this->serviceid;
    }
	

	function getValuesync() {
            return $this->valuesync;
        }
    
        function setValuesync($valuesync) {
            $this->valuesync = $valuesync;
        }

	function getSstatus() {
            return $this->sstatus;
        }
    
        function setSstatus($sstatus) {
            $this->sstatus = $sstatus;
        }

	function getIstatus() {
            return $this->istatus;
        }
    
        function setIstatus($istatus) {
            $this->istatus = $istatus;
        }	

	function getTprocess() {
            return $this->tprocess;
        }
    
        function setTprocess($tprocess) {
            $this->tprocess = $tprocess;
        }

 function getDtype() {
            return $this->dtype;
        }

 function setDtype($dtype) {
            $this->dtype = $dtype;
        }
		
 function getRemotedatatype() {
            return $this->remotedatatype;
        }

 function setRemotedatatype($remotedatatype) {
            $this->remotedatatype = $remotedatatype;
        }	
		
 function getRemotedataid() {
            return $this->remotedataid;
        }

 function setRemotedataid($remotedataid) {
            $this->remotedataid = $remotedataid;
        }

 function getRemotedatakey() {
            return $this->remotedatakey;
        }

 function setRemotedatakey($remotedatakey) {
            $this->remotedatakey = $remotedatakey;
        }
		
 function getKeysync() {
            return $this->keysync;
        }

 function setKeysync($keysync) {
            $this->keysync = $keysync;
        }
	

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }
   
    function getModulekey() {
        return $this->modulekey;
    }
	
   /**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string $moduleinstance
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }
 		
}
