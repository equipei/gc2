<?php
namespace Badiu\Admin\ServerBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 *  AdminServerService
 *
 * @ORM\Table(name="admin_server_service", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_server_service_shortname_uix", columns={"entity", "shortname"}),
 *      @ORM\UniqueConstraint(name="admin_server_service_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="admin_server_service_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_server_service_name_ix", columns={"name"}),
 *              @ORM\Index(name="admin_server_service_statusid_ix", columns={"statusid"}),
 *              @ORM\Index(name="admin_server_service_statusinfo_ix", columns={"statusinfo"}), 
 *              @ORM\Index(name="admin_server_service_machineid_ix", columns={"machineid"}),
 *              @ORM\Index(name="admin_server_service_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_server_service_ip_ix", columns={"ip"}),
 *              @ORM\Index(name="admin_server_service_url_ix", columns={"url"}),
 *              @ORM\Index(name="admin_server_service_servicetoken_ix", columns={"servicetoken"}),
 *              @ORM\Index(name="admin_server_service_servicetokendna_ix", columns={"servicetokendna"}),
 *              @ORM\Index(name="admin_server_service_servicekey_ix", columns={"servicekey"}),
 *              @ORM\Index(name="admin_server_service_dirinstall_ix", columns={"dirinstall"}),
 *              @ORM\Index(name="admin_server_service_secureauth_ix", columns={"secureauth"}),
 *              @ORM\Index(name="admin_server_service_authmod_ix", columns={"authmod"}),
 *              @ORM\Index(name="admin_server_service_aversion_ix", columns={"aversion"}),
 *              @ORM\Index(name="admin_server_service_dirvhost_ix", columns={"dirvhost"}),
 *              @ORM\Index(name="admin_server_service_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="admin_server_service_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_server_service_lastaccess_ix", columns={"lastaccess"}), 
 *              @ORM\Index(name="admin_server_service_scheduleinherit_ix", columns={"scheduleinherit"}), 
 *              @ORM\Index(name="admin_server_service_scheduleexec_ix", columns={"scheduleexec"}), 
  *              @ORM\Index(name="admin_server_service_connremotesatus_ix", columns={"connremotesatus"}), 
 *              @ORM\Index(name="admin_server_service_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminServerService {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var AdminServerMachine
     *
     * @ORM\ManyToOne(targetEntity="AdminServerMachine")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="machineid", referencedColumnName="id")
     * })
     */
    private $machineid;

    /**
     * @var AdminServerServiceCategory
     *
     * @ORM\ManyToOne(targetEntity="AdminServerServiceCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;

    /**
     * @var AdminServerServiceStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminServerServiceStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;

     /**
     * @var string
     *
     * @ORM\Column(name="statusinfo", type="string", length=255, nullable=true)
     */
    private $statusinfo; //servicefree | servicedisabled
    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="secureauth", type="string", length=255, nullable=true)
     */
    private $secureauth;

    /**
     * @var string
     *
     * @ORM\Column(name="aversion", type="string", length=255, nullable=true)
     */
    private $aversion;

    /**
      @var string
     *
     * @ORM\Column(name="dirvhost", type="string", length=255, nullable=true)
     */
    private $dirvhost;

    /**
     * @var string
     *
     * @ORM\Column(name="authmod", type="string", length=255, nullable=true)
     */
    private $authmod;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;

    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=255, nullable=true)
     */
    private $host;

    /**
     * @var string
     *
     * @ORM\Column(name="dirinstall", type="string", length=255, nullable=true)
     */
    private $dirinstall;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    
     /**
     * @var string
     *
     * @ORM\Column(name="tcharset", type="string", length=255, nullable=true)
     */
    private $tcharset="UTF8";
    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=true)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="versiontxt", type="string", length=255, nullable=true)
     */
    private $versiontxt;

    /**
     * @var string
     *
     * @ORM\Column(name="versionnumber", type="string", length=255, nullable=true)
     */
    private $versionnumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="port", type="integer",  nullable=true)
     */
    private $port;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="userpwd", type="string", length=255, nullable=true)
     */
    private $userpwd;

    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
     */
    private $dtype; //site_moodle | site_wordpress  |  service_ssh | service_apache |  service_smtp |  service_db

    /**
     * @var string
     *
     * @ORM\Column(name="dbhost", type="string", length=255, nullable=true)
     */
    private $dbhost;

    /**
     * @var string
     *
     * @ORM\Column(name="dbtype", type="string", length=255, nullable=true)
     */
    private $dbtype;

    /**
     * @var string
     *
     * @ORM\Column(name="dbname", type="string", length=255, nullable=true)
     */
    private $dbname;

    /**
     * @var string
     *
     * @ORM\Column(name="dbuser", type="string", length=255, nullable=true)
     */
    private $dbuser;

    /**
     * @var string
     *
     * @ORM\Column(name="dbpwd", type="string", length=255, nullable=true)
     */
    private $dbpwd;

    /**
     * @var string
     *
     * @ORM\Column(name="dbport", type="integer",  nullable=true)
     */
    private $dbport;

    /**
     * @var string
     *
     * @ORM\Column(name="dbtblprefix", type="string", length=255, nullable=true)
     */
    private $dbtblprefix;

    /**
     * @var string
     *
     * @ORM\Column(name="servicerurl", type="string", length=255, nullable=true)
     */
    private $servicerurl;

	/**
     * @var string
     *
     * @ORM\Column(name="serviceversiontext", type="string", length=255, nullable=true)
     */
    private $serviceversiontext;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="serviceversionumber", type="bigint", nullable=true)
     */
    private $serviceversionumber;
    /**
     * @var string
     *
     * @ORM\Column(name="servicerurl1", type="string", length=255, nullable=true)
     */
    private $servicerurl1;
    
	/**
     * @var string
     *
     * @ORM\Column(name="serviceversiontext1", type="string", length=255, nullable=true)
     */
    private $serviceversiontext1;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="serviceversionumber1", type="bigint", nullable=true)
     */
    private $serviceversionumber1;
	
     /**
     * @var string
     *
     * @ORM\Column(name="servicerurl2", type="string", length=255, nullable=true)
     */
    private $servicerurl2;
    
	/**
     * @var string
     *
     * @ORM\Column(name="serviceversiontext2", type="string", length=255, nullable=true)
     */
    private $serviceversiontext2;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="serviceversionumber2", type="bigint", nullable=true)
     */
    private $serviceversionumber2;
     /**
     * @var string
     *
     * @ORM\Column(name="servicerurl3", type="string", length=255, nullable=true)
     */
    private $servicerurl3;
	
	/**
     * @var string
     *
     * @ORM\Column(name="serviceversiontext3", type="string", length=255, nullable=true)
     */
    private $serviceversiontext3;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="serviceversionumber3", type="bigint", nullable=true)
     */
    private $serviceversionumber3;
    /**
     * @var string
     *
     * @ORM\Column(name="servicetoken", type="string", length=255, nullable=true)
     */
    private $servicetoken;

     /**
     * @var string
     *
     * @ORM\Column(name="servicetokendna", type="string", length=255, nullable=true)
     */
    private $servicetokendna;
    
        /**
     * @var string
     *
     * @ORM\Column(name="servicekey", type="string", length=255, nullable=true)
     */
    private $servicekey;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="dirdata", type="string", length=255, nullable=true)
     */
    private $dirdata;

    /**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
    
    /**
	 * @var string
	 *
	 * @ORM\Column(name="theme", type="string", length=255, nullable=true)
	 */
	private $theme;
      /**
     * @var string
     *
     * @ORM\Column(name="criptk1",  type="string", length=255, nullable=true)
     */
    private $criptk1; 
    /**
     * @var string
     *
     * @ORM\Column(name="criptk2",  type="string", length=255, nullable=true)
     */
    private $criptk2; 
        /**
	 * @var boolean
	 *
	 * @ORM\Column(name="syncdb", type="integer", nullable=true)
	 */
	private $syncdb;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

       /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig; 
    
    /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    
     /**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="lastaccess", type="datetime", nullable=true)
	 */
    private $lastaccess;
     
 /**
     *200 - sucess | 400 - error
 
     * @var integer
     *
     * @ORM\Column(name="connremotesatus", type="integer", nullable=true)
     */
    private $connremotesatus;	 

 /**
     * @var integer
     *
     * @ORM\Column(name="scheduleinherit", type="integer", nullable=true)
     */
    private $scheduleinherit;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="scheduleexec", type="integer", nullable=true)
     */
    private $scheduleexec;

	/**
     * @var integer
     *
     * @ORM\Column(name="leveldataexec", type="integer", nullable=true)
     */
    private $leveldataexec;	
	
    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    public function getShortname() {
        return $this->shortname;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    public function setStatusid(AdminServerServiceStatus $statusid) {
        $this->statusid = $statusid;

        return $this;
    }

    public function getStatusid() {
        return $this->statusid;
    }

    public function getCategoryid() {
        return $this->categoryid;
    }

    public function setMachineid(AdminServerMachine $machineid) {

        $this->machineid = $machineid;
    }

    public function getMachineid() {
        return $this->machineid;
    }

    public function setCategoryid(AdminServerServiceCategory $categoryid) {
        $this->categoryid = $categoryid;
    }

    /**
     * @return string
     */
    public function getDtype() {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    /**
     * @return string
     */
    public function getIp() {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip) {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getHost() {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host) {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getVersiontxt() {
        return $this->versiontxt;
    }

    /**
     * @param string $versiontxt
     */
    public function setVersiontxt($versiontxt) {
        $this->versiontxt = $versiontxt;
    }

    /**
     * @return string
     */
    public function getVersionnumber() {
        return $this->versionnumber;
    }

    /**
     * @param string $versionnumber
     */
    public function setVersionnumber($versionnumber) {
        $this->versionnumber = $versionnumber;
    }

    /**
     * @return int
     */
    public function getPort() {
        return $this->port;
    }

    /**
     * @param int $port
     */
    public function setPort($port) {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username) {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getUserpwd() {
        return $this->userpwd;
    }

    /**
     * @param string $userpwd
     */
    public function setUserpwd($userpwd) {
        $this->userpwd = $userpwd;
    }

    /**
     * @return string
     */
    public function getDbhost() {
        return $this->dbhost;
    }

    /**
     * @param string $dbhost
     */
    public function setDbhost($dbhost) {
        $this->dbhost = $dbhost;
    }

    /**
     * @return string
     */
    public function getDbtype() {
        return $this->dbtype;
    }

    /**
     * @param string $dbtype
     */
    public function setDbtype($dbtype) {
        $this->dbtype = $dbtype;
    }

    /**
     * @return string
     */
    public function getDbname() {
        return $this->dbname;
    }

    /**
     * @param string $dbname
     */
    public function setDbname($dbname) {
        $this->dbname = $dbname;
    }

    /**
     * @return string
     */
    public function getDbuser() {
        return $this->dbuser;
    }

    /**
     * @param string $dbuser
     */
    public function setDbuser($dbuser) {
        $this->dbuser = $dbuser;
    }

    /**
     * @return string
     */
    public function getDbpwd() {
        return $this->dbpwd;
    }

    /**
     * @param string $dbpwd
     */
    public function setDbpwd($dbpwd) {
        $this->dbpwd = $dbpwd;
    }

    /**
     * @return string
     */
    public function getDbport() {
        return $this->dbport;
    }

    /**
     * @param string $dbport
     */
    public function setDbport($dbport) {
        $this->dbport = $dbport;
    }

    /**
     * @return string
     */
    public function getDbtblprefix() {
        return $this->dbtblprefix;
    }

    /**
     * @param string $dbtblprefix
     */
    public function setDbtblprefix($dbtblprefix) {
        $this->dbtblprefix = $dbtblprefix;
    }

    /**
     * @return string
     */
    public function getServicerurl() {
        return $this->servicerurl;
    }

    /**
     * @param string $servicerurl
     */
    public function setServicerurl($servicerurl) {
        $this->servicerurl = $servicerurl;
    }

    /**
     * @return string
     */
    public function getServicetoken() {
        return $this->servicetoken;
    }

    /**
     * @param string $servicetoken
     */
    public function setServicetoken($servicetoken) {
        $this->servicetoken = $servicetoken;
    }

    function getDirdata() {
        return $this->dirdata;
    }

    function getDirinstall() {
        return $this->dirinstall;
    }

    function setDirdata($dirdata) {
        $this->dirdata = $dirdata;
    }

    function setDirinstall($dirinstall) {
        $this->dirinstall = $dirinstall;
    }

    /**
     * @return string
     */
    public function getSecureauth() {
        return $this->secureauth;
    }

    /**
     * @param string $servicetoken
     */
    public function setSecureauth($secureauth) {
        $this->secureauth = $secureauth;
    }

    /**
     * @return string
     */
    public function getAuthmod() {
        return $this->authmod;
    }

    /**
     * @param string $servicetoken
     */
    public function setAuthmod($authmod) {
        $this->authmod = $authmod;
    }

    function getAversion() {
        return $this->aversion;
    }

    function getDirvhost() {
        return $this->dirvhost;
    }

    function setAversion($aversion) {
        $this->aversion = $aversion;
    }

    function setDirvhost($dirvhost) {
        $this->dirvhost = $dirvhost;
    }

    function getServicerurl1() {
        return $this->servicerurl1;
    }

    function getServicerurl2() {
        return $this->servicerurl2;
    }

    function getServicerurl3() {
        return $this->servicerurl3;
    }

    function setServicerurl1($servicerurl1) {
        $this->servicerurl1 = $servicerurl1;
    }

    function setServicerurl2($servicerurl2) {
        $this->servicerurl2 = $servicerurl2;
    }

    function setServicerurl3($servicerurl3) {
        $this->servicerurl3 = $servicerurl3;
    }
    function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }

    function getSyncdb() {
        return $this->syncdb;
    }

    function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }

    function setSyncdb($syncdb) {
        $this->syncdb = $syncdb;
    }

    function getTcharset() {
        return $this->tcharset;
    }

    function setTcharset($tcharset) {
        $this->tcharset = $tcharset;
    }

    function getServicetokendna() {
        return $this->servicetokendna;
    }

    function setServicetokendna($servicetokendna) {
        $this->servicetokendna = $servicetokendna;
    }

    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function getStatusinfo() {
        return $this->statusinfo;
    }

    function setStatusinfo($statusinfo) {
        $this->statusinfo = $statusinfo;
    }

    function getCriptk1() {
        return $this->criptk1;
    }

    function getCriptk2() {
        return $this->criptk2;
    }

    function setCriptk1($criptk1) {
        $this->criptk1 = $criptk1;
    }

    function setCriptk2($criptk2) {
        $this->criptk2 = $criptk2;
    }

    function getLastaccess(){
        return $this->lastaccess;
    }

    function setLastaccess(\DateTime $lastaccess) {
        $this->lastaccess = $lastaccess;
    }


    function getTheme(){
        return $this->theme;
    }

    function setTheme($theme) {
        $this->theme = $theme;
    }

    /**
     * @return string
     */ 
    public function getServicekey() {
        return $this->servicekey;
    }

    /**
     * @param string $servicekey
     */
    public function setServicekey($servicekey) {
        $this->servicekey = $servicekey;
    }

/**
     * @return integer
     */ 
    public function getScheduleinherit() {
        return $this->scheduleinherit;
    }

    /**
     * @param integer $scheduleinherit
     */
    public function setScheduleinherit($scheduleinherit) {
        $this->scheduleinherit = $scheduleinherit;
    }


    /**
     * @return integer
     */ 
    public function getScheduleexec() {
        return $this->scheduleexec;
    }

    /**
     * @param integer $scheduleexec
     */
    public function setScheduleexec($scheduleexec) {
        $this->scheduleexec = $scheduleexec;
    }

    /**
     * @return integer
     */ 
    public function getConnremotesatus() {
        return $this->connremotesatus;
    }

    /**
     * @param integer $connremotesatus
     */
    public function setConnremotesatus($connremotesatus) {
        $this->connremotesatus = $connremotesatus;
    }
	
	
    /**
     * @return integer
     */ 
    public function getLeveldataexec() {
        return $this->leveldataexec;
    }

    /**
     * @param integer $leveldataexec
     */
    public function setLeveldataexec($leveldataexec) {
        $this->leveldataexec = $leveldataexec;
    }

    public function getServiceversiontext() {
        return $this->serviceversiontext;
    }

 
    public function setServiceversiontext($serviceversiontext) {
        $this->serviceversiontext = $serviceversiontext;
    }
	
	
	 
    public function getServiceversionumber() {
        return $this->serviceversionumber;
    }

 
    public function setServiceversionumber($serviceversionumber) {
        $this->serviceversionumber = $serviceversionumber;
    }
	
	    public function getServiceversiontext1() {
        return $this->serviceversiontext1;
    }

 
    public function setServiceversiontext1($serviceversiontext1) {
        $this->serviceversiontext1 = $serviceversiontext1;
    }
	
	
	 
    public function getServiceversionumber1() {
        return $this->serviceversionumber1;
    }

 
    public function setServiceversionumber1($serviceversionumber1) {
        $this->serviceversionumber1 = $serviceversionumber1;
    }
	
	   
    public function getServiceversiontext2() {
        return $this->serviceversiontext2;
    }

 
    public function setServiceversiontext2($serviceversiontext2) {
        $this->serviceversiontext2 = $serviceversiontext2;
    }
	
	
	 
    public function getServiceversionumber2() {
        return $this->serviceversionumber2;
    }

 
    public function setServiceversionumber2($serviceversionumber2) {
        $this->serviceversionumber2 = $serviceversionumber2;
    }
	
	    public function getServiceversiontext3() {
        return $this->serviceversiontext3;
    }

 
    public function setServiceversiontext3($serviceversiontext3) {
        $this->serviceversiontext3 = $serviceversiontext3;
    }
	
	
	 
    public function getServiceversionumber3() {
        return $this->serviceversionumber3;
    }

 
    public function setServiceversionumber3($serviceversionumber3) {
        $this->serviceversionumber3 = $serviceversionumber3;
    }
}
