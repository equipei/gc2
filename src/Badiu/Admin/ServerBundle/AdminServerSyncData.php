<?php

namespace Badiu\Admin\ServerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminServerSyncUser
 *
 * @ORM\Table(name="admin_server_sync_user", uniqueConstraints={
 *
 *      @ORM\UniqueConstraint(name="admin_server_sync_user_uix", columns={"entity","sysuserid","serviceid","userid"})},
 *       indexes={@ORM\Index(name="admin_server_sync_user_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_server_sync_user_idnumber_ix", columns={"idnumber"}),
 *              @ORM\Index(name="admin_server_sync_user_sysuserid_ix", columns={"sysuserid"}),
 *              @ORM\Index(name="admin_server_sync_user_deleted_ix", columns={"deleted"}),
 *               @ORM\Index(name="admin_server_sync_user_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="admin_server_sync_user_firstname_ix", columns={"firstname"}),
 *              @ORM\Index(name="admin_server_sync_user_lastname_ix", columns={"lastname"}),
 *              @ORM\Index(name="admin_server_sync_user_keysync_ix", columns={"keysync"}),
 *              @ORM\Index(name="admin_server_sync_user_valuesync_ix", columns={"valuesync"}),
 *              @ORM\Index(name="admin_server_sync_user_sstatus_ix", columns={"sstatus"}),
 *              @ORM\Index(name="admin_server_sync_user_istatus_ix", columns={"istatus"}),
 *              @ORM\Index(name="admin_server_sync_user_tprocess_ix", columns={"tprocess"}),
 *              @ORM\Index(name="admin_server_sync_user_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_server_sync_user_parentid_ix", columns={"parentid"}),
 *              @ORM\Index(name="admin_server_sync_user_email_ix", columns={"email"})})
 *
 * @ORM\Entity
 */

class AdminServerSyncUser {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="entity", type="bigint", nullable=false)
	 */
	private $entity;

	 /**
	 *
	 * @var \Badiu\System\UserBundle\Entity\SystemUser
	 * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="sysuserid", referencedColumnName="id")
	 * })
	 */ 
	private $sysuserid;
	
	
	/**
     * @var AdminServerService
     *
     * @ORM\ManyToOne(targetEntity="AdminServerService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="serviceid", referencedColumnName="id")
     * })
     */
    private $serviceid;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="userid", type="bigint", nullable=false)
	 */
	private $userid;
	
	/**
	@var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=false)
	 */
	private $dtype; //fromremoteservicetobadiunet | frombadiunetremoteservice
	
	
	/**
	@var string
	 *
	 * @ORM\Column(name="keysync", type="string", length=50, nullable=false)
	 */
	private $keysync; //username | idnumber | email
	
	
	/**
	@var string
	 *
	 * @ORM\Column(name="valuesync", type="string", length=255, nullable=true)
	 */
	private $valuesync; // vaulue of keysync
	
	/**
	@var string
	 *
	 * @ORM\Column(name="sstatus", type="string", length=50, nullable=false)
	 */
	private $sstatus; //sucess | failure
	
	/**
	@var string
	 *
	 * @ORM\Column(name="istatus", type="string", length=50, nullable=false)
	 */
	private $istatus; //completed | duplication | parentsync
	
	/**
	@var string
	 *
	 * @ORM\Column(name="tprocess", type="string", length=50, nullable=false)
	 */
	private $tprocess; //automatic | manual
	
	/**
	@var string
	 *
	 * @ORM\Column(name="parentid", type="bigint", nullable=true)
	 */
	private $parentid;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="firstname", type="string", length=255, nullable=false)
	 */
	private $firstname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lastname", type="string", length=255, nullable=false)
	 */
	private $lastname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255, nullable=true)
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="username", type="string", length=100, nullable=false)
	 */
	private $username;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
	 */
	private $idnumber;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
     

/**
 * @var string
 *
 * @ORM\Column(name="param", type="text", nullable=true)
 */
	private $param;
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timecreated", type="datetime", nullable=false)
	 */
	private $timecreated;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timemodified", type="datetime", nullable=true)
	 */
	private $timemodified;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridadd", type="bigint", nullable=true)
	 */
	private $useridadd;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="useridedit", type="bigint", nullable=true)
	 */
	private $useridedit;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="boolean", nullable=false)
	 */
	private $deleted;

	
        function getId() {
            return $this->id;
        }

        function getEntity() {
            return $this->entity;
        }

        function getFirstname() {
            return $this->firstname;
        }

        function getLastname() {
            return $this->lastname;
        }

        function getEmail() {
            return $this->email;
        }

      
        function getUsername() {
            return $this->username;
        }

       
        function getIdnumber() {
            return $this->idnumber;
        }

       
        function getParam() {
            return $this->param;
        }

        function getTimecreated(){
            return $this->timecreated;
        }

        function getTimemodified(){
            return $this->timemodified;
        }

        function getUseridadd() {
            return $this->useridadd;
        }

        function getUseridedit() {
            return $this->useridedit;
        }

        function getDeleted() {
            return $this->deleted;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setEntity($entity) {
            $this->entity = $entity;
        }

        function setFirstname($firstname) {
            $this->firstname = $firstname;
        }

        function setLastname($lastname) {
            $this->lastname = $lastname;
        }

        function setEmail($email) {
            $this->email = $email;
        }

       
        function setUsername($username) {
            $this->username = $username;
        }

       

        function setIdnumber($idnumber) {
            $this->idnumber = $idnumber;
        }



        function setParam($param) {
            $this->param = $param;
        }

        function setTimecreated(\DateTime $timecreated) {
            $this->timecreated = $timecreated;
        }

        function setTimemodified(\DateTime $timemodified) {
            $this->timemodified = $timemodified;
        }

        function setUseridadd($useridadd) {
            $this->useridadd = $useridadd;
        }

        function setUseridedit($useridedit) {
            $this->useridedit = $useridedit;
        }

        function setDeleted($deleted) {
            $this->deleted = $deleted;
        }

       function getDconfig() {
            return $this->dconfig;
        }
    
        function setDconfig($dconfig) {
            $this->dconfig = $dconfig;
        }
    
	  public function setServiceid(AdminServerService $serviceid) {
        $this->serviceid = $serviceid;
    }

	public function getServiceid() {
        return $this->serviceid;
    }
	
	 function getKeysync() {
            return $this->keysync;
        }
    
        function setKeysync($keysync) {
            $this->keysync = $keysync;
        }
	function getValuesync() {
            return $this->valuesync;
        }
    
        function setValuesync($valuesync) {
            $this->valuesync = $valuesync;
        }

	function getSstatus() {
            return $this->sstatus;
        }
    
        function setSstatus($sstatus) {
            $this->sstatus = $sstatus;
        }

	function getIstatus() {
            return $this->istatus;
        }
    
        function setIstatus($istatus) {
            $this->istatus = $istatus;
        }	

	function getTprocess() {
            return $this->tprocess;
        }
    
        function setTprocess($tprocess) {
            $this->tprocess = $tprocess;
        }

	function getParentid() {
            return $this->parentid;
        }
    
        function setParentid($parentid) {
            $this->parentid = $parentid;
        }
 function getSysuserid() {
            return $this->sysuserid;
        }
    
        function setSysuserid($sysuserid) {
            $this->sysuserid = $sysuserid;
        }
		function getUserid() {
            return $this->userid;
        }
    
        function setUserid($userid) {
            $this->userid = $userid;
        }		
}
