<?php

namespace Badiu\Admin\ServerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuAdminServerBundle:Default:index.html.twig', array('name' => $name));
    }
}
