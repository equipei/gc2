<?php

namespace Badiu\Admin\BadiuAdminEnterpriseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BadiuAdminEnterpriseBundle:Default:index.html.twig', array('name' => $name));
    }
}
