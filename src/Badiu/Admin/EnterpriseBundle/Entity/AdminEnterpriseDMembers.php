<?php

namespace Badiu\Admin\EnterpriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  adminenterprisedmembers
 *
 * @ORM\Table(name="admin_enterprise_dmembers", uniqueConstraints={    
 *      @ORM\UniqueConstraint(name="admin_enterprise_dmembers_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="admin_enterprise_dmembers_entity_ix", columns={"entity"}),   
 *              @ORM\Index(name="admin_enterprise_dmembers_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_enterprise_dmembers_officeid_ix", columns={"officeid"}),
 *              @ORM\Index(name="admin_enterprise_dmembers_departmentid_ix", columns={"departmentid"}),
 *              @ORM\Index(name="admin_enterprise_dmembers_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="admin_enterprise_dmembers_userid_ix", columns={"statusid"}),
 *              @ORM\Index(name="admin_enterprise_dmembers_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class  AdminEnterpriseDMembers
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


     /**
     * @var AdminEnterpriseOffice
     *
     * @ORM\ManyToOne(targetEntity="AdminEnterpriseOffice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="officeid", referencedColumnName="id")
     * })
     */

    private $officeid; 


     /**
     * @var AdminEnterpriseDepartment
     *
     * @ORM\ManyToOne(targetEntity="AdminEnterpriseDepartment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="departmentid", referencedColumnName="id")
     * })
     */

    private $departmentid; 

/**
     * @var AdminEnterpriseDMembersStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminEnterpriseDMembersStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */

    private $statusid; 

    /**
    *
    *@var \Badiu\System\UserBundle\Entity\SystemUser 
    *@ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
    *@ORM\JoinColumns({
    *@ORM\JoinColumn(name="userid", referencedColumnName="id")
    * })
    */
    private $userid;
    
   
   /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

   
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;


    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }
   

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }
   
    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }
   
       

    public function setOfficeid(AdminEnterpriseOffice $officeid = null)
    {
        $this->officeid = $officeid;

        return $this;
    }

  
    public function getOfficeid()
    {
        return $this->officeid;
    }



    public function setUserid(\Badiu\System\UserBundle\Entity\SystemUser $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    public function getUserid()
    {
        return $this->userid;
    }



    public function setDepartmentid(AdminEnterpriseDepartment $departmentid)
    {
        $this->departmentid = $departmentid;

        return $this;
    }

  
    public function getDepartmentid()
    {
        return $this->departmentid;
    }


 public function setStatusid(AdminEnterpriseDMembersStatus $statusid)
    {
        $this->statusid = $statusid;

        return $this;
    }

  
    public function getStatusid()
    {
        return $this->statusid;
    }


}
 