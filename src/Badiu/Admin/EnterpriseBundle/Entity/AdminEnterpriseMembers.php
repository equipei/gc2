<?php

namespace Badiu\Admin\EnterpriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  AdminEnterpriseMembers
 *
 * @ORM\Table(name="admin_enterprise_members", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_enterprise_members_useridentrp_uix", columns={"entity", "userid", "enterpriseid"}),
 *      @ORM\UniqueConstraint(name="admin_enterprise_members_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="admin_enterprise_members_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="admin_enterprise_members_enterpriseid_ix", columns={"enterpriseid"}), 
 *              @ORM\Index(name="admin_enterprise_members_statusid_ix", columns={"statusid"}), 
 *              @ORM\Index(name="admin_enterprise_members_userid_ix", columns={"userid"}), 
 *              @ORM\Index(name="admin_enterprise_members_timeend_ix", columns={"timeend"}), 
 *              @ORM\Index(name="admin_enterprise_members_timestart_ix", columns={"timestart"}),
 *              @ORM\Index(name="admin_enterprise_members_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_enterprise_members_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */  
class  AdminEnterpriseMembers
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var AdminEnterprise
     *
     * @ORM\ManyToOne(targetEntity="AdminEnterprise")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="enterpriseid", referencedColumnName="id")
     * })
     */

    private $enterpriseid;   


     /**
     * @var AdminEnterpriseMembersStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminEnterpriseMembersStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */

    private $statusid; 
    
	
     /**
     * @var AdminEnterpriseBond
     *
     * @ORM\ManyToOne(targetEntity="AdminEnterpriseBond")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bondid", referencedColumnName="id")
     * })
     */

    private $bondid; 
       /**
    *
    *@var \Badiu\System\UserBundle\Entity\SystemUser 
    *@ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
    *@ORM\JoinColumns({
    *@ORM\JoinColumn(name="userid", referencedColumnName="id")
    * })
    */
    private $userid;
    

   /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

   
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

 /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

 
    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }
   

   
   
    public function setEnterpriseid(AdminEnterprise $enterpriseid)
    {
        $this->enterpriseid = $enterpriseid;

        return $this;
    }

  
    public function getEnterpriseid()
    {
        return $this->enterpriseid;
    }

  



    public function setStatusid(AdminEnterpriseMembersStatus $statusid)
    {
        $this->statusid = $statusid;

        return $this;
    }

  
    public function getStatusid()
    {
        return $this->statusid;
    }


    public function setUserid(\Badiu\System\UserBundle\Entity\SystemUser $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    public function getUserid()
    {
        return $this->userid;
    }

    function getTimestart() {
        return  $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }
	
	
    function getDconfig() {
        return $this->dconfig;
    }
  
   function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

 public function setBondid(AdminEnterpriseBond $bondid)
    {
        $this->bondid = $bondid;

        return $this;
    }

  
    public function getBondid()
    {
        return $this->bondid;
    }

}
