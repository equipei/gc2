<?php

namespace Badiu\Admin\EnterpriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  AdminEnterprise
 *
 * @ORM\Table(name="admin_enterprise", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_enterprise_shortname_uix", columns={"entity", "shortname"}), 
 *      @ORM\UniqueConstraint(name="admin_enterprise_doctypenumber_uix", columns={"entity","doctype", "docnumber"}), 
 *      @ORM\UniqueConstraint(name="admin_enterprise_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="admin_enterprise_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="admin_enterprise_name_ix", columns={"name"}), 
 *              @ORM\Index(name="admin_enterprise_statusid_ix", columns={"statusid"}),                         
 *              @ORM\Index(name="admin_enterprise_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="admin_enterprise_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_enterprise_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_enterprise_marker_ix", columns={"marker"}),
 *              @ORM\Index(name="admin_enterprise_musterid_ix", columns={"musterid"}),
 *              @ORM\Index(name="admin_enterprise_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminEnterprise {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var AdminEnterpriseType
     *
     * @ORM\ManyToOne(targetEntity="AdminEnterpriseType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="typeid", referencedColumnName="id")
     * })
     */
    private $typeid;

    /**
     * @var AdminEnterpriseStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminEnterpriseStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;

    /**
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=255, nullable=true)
     */
    private $nickname;

    /**
     * @var string
     *
     * @ORM\Column(name="doctype", type="string", length=255, nullable=true)
     */
    private $doctype;

    /**
     * @var string
     *
     * @ORM\Column(name="docnumber", type="string", length=255, nullable=true)
     */
    private $docnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="contactdata", type="text", nullable=true)
     */
    private $contactdata;

	 /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", length=255, nullable=true)
     */
    private $site;
    
	 /**
     * @var string
     *
     * @ORM\Column(name="contactperson", type="string", length=255, nullable=true)
     */
    private $contactperson;
    
   /* @var string
   *
   * @ORM\Column(name="addressdata", type="text", nullable=true)
     */
    private $addressdata;

    /**
     * @var string
     *
     * @ORM\Column(name="documentdata", type="text", nullable=true)
     */
    private $documentdata;

    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
	 * @var string
	 *
	 * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
	 */
    private $dtype; //muster | branch | other 
    
	   /**
     * @var integer
     *
     * @ORM\Column(name="musterid", type="bigint", nullable=true)
     */
    private $musterid;
     /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig; 
    
          /**
     * @var string
     *
     * @ORM\Column(name="marker", type="string", length=255, nullable=true)
     */
    private $marker;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;

    /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    function getId() {
        return $this->id;
    }

    function getTypeid() {
        return $this->typeid;
    }

    function getStatusid() {
        return $this->statusid;
    }

    function getEntity() {
        return $this->entity;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function getNickname() {
        return $this->nickname;
    }

    function getDoctype() {
        return $this->doctype;
    }

    function getDocnumber() {
        return $this->docnumber;
    }

    function getEmail() {
        return $this->email;
    }

    function getSite() {
        return $this->site;
    }

    function getContactperson() {
        return $this->contactperson;
    }

    function getAddressdata() {
        return $this->addressdata;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTypeid(AdminEnterpriseType $typeid) {
        $this->typeid = $typeid;
    }

    function setStatusid(AdminEnterpriseStatus $statusid) {
        $this->statusid = $statusid;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    function setNickname($nickname) {
        $this->nickname = $nickname;
    }

    function setDoctype($doctype) {
        $this->doctype = $doctype;
    }

    function setDocnumber($docnumber) {
        $this->docnumber = $docnumber;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setSite($site) {
        $this->site = $site;
    }

    function setContactperson($contactperson) {
        $this->contactperson = $contactperson;
    }

    function setAddressdata($addressdata) {
        $this->addressdata = $addressdata;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

     function getContactdata() {
                return $this->contactdata;
            }
    function setContactdata($contactdata) {
                $this->contactdata = $contactdata;
            }


    function getDocumentdata() {
            return $this->documentdata;
        }
 function setDocumentdata($documentdata) {
            $this->documentdata = $documentdata;
        }
        function getDtype() {
            return $this->dtype;
        }

        function setDtype($dtype) {
            $this->dtype = $dtype;
        }

        function setDconfig($dconfig) {
            $this->dconfig = $dconfig;
        }

        function getDconfig() {
            return $this->dconfig;
        }

        function getMarker() {
            return $this->marker;
        }
        function setMarker($marker) {
            $this->marker = $marker;
        }
    
        function getMusterid() {
            return $this->musterid;
        }
        function setMusterid($musterid) {
            $this->musterid = $musterid;
        }
}
