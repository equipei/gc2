<?php

namespace Badiu\Admin\EnterpriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  AdminEnterpriseCategoryData
 *
 * @ORM\Table(name="admin_enterprise_category_data", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_enterprise_category_data_idnumber_uix", columns={"entity", "enterpriseid","categoryid"})},
 *       indexes={@ORM\Index(name="admin_enterprise_category_data_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="admin_enterprise_category_data_enterpriseid_ix", columns={"enterpriseid"}), 
 *              @ORM\Index(name="admin_enterprise_category_data_statusid_ix", columns={"categoryid"}), 
 *              @ORM\Index(name="admin_enterprise_category_data_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_enterprise_category_data_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */  
class  AdminEnterpriseCategoryData
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var AdminEnterprise
     *
     * @ORM\ManyToOne(targetEntity="AdminEnterprise")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="enterpriseid", referencedColumnName="id")
     * })
     */

    private $enterpriseid;   


     /**
     * @var AdminEnterpriseCategory
     *
     * @ORM\ManyToOne(targetEntity="AdminEnterpriseCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */

    private $categoryid; 
    
     
   /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

   
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    function getId() {
        return $this->id;
    }

    function getEnterpriseid() {
        return $this->enterpriseid;
    }

    function getCategoryid() {
        return $this->categoryid;
    }

    function getEntity() {
        return $this->entity;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEnterpriseid(AdminEnterprise $enterpriseid) {
        $this->enterpriseid = $enterpriseid;
    }

    function setCategoryid(AdminEnterpriseCategory $categoryid) {
        $this->categoryid = $categoryid;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


}
