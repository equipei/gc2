<?php
namespace Badiu\Admin\EnterpriseBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnterpriseFormController extends BadiuFormController
{
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
              
              
      public function changeParamOnOpen() {
       
          if ($this->isEdit() || $this->isClone()) {
             $this->setFormAddress();
            $this->setForm();
        }
     
     }      
    public function changeParam() {
         $this->getFormAddress();
         $this->getForm();
     }
    
    public function validation() {
         
        $isdocumentvalid = $this->isDocumentValid();
        if ($isdocumentvalid != null) {
            return $isdocumentvalid;
        }
        
        $isemailvalid = $this->isEmailValid();
        if ($isemailvalid != null) {
            return $isemailvalid;
        }

        $isemailduplicate = $this->isEmailDuplicate();
        if ($isemailduplicate != null) {
            return $isemailduplicate;
        }
       
        
        return null;
    }
    
     public function isDocumentValid() {
    
        $doctype = $this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield1');
        $docnumber = $this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield2');
        $entity =$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
         $id=$this->getParamItem('id');
         $data = $this->getContainer()->get($this->getKminherit()->data());
        if ($doctype == 'CNPJ') {
            
            $cpfdata = $this->getContainer()->get('badiu.util.document.role.cnpj');
            $valid = $cpfdata->isValid($docnumber);
            if (!$valid) {
               
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['document']= $this->getTranslator()->trans('badiu.admin.enterprise.enterprise.message.cnpjnotvalid');
                $info = 'badiu.admin.enterprise.enterprise.message.cnpjnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
            
          $number = $cpfdata->clean($docnumber);
          
           
           $duplicatecnpj=null;
           if($this->isedit()){$duplicatecnpj = $data->existColumnEdit($id,$entity, 'docnumber', $number);}
           else{$duplicatecnpj = $data->existColumnAdd($entity, 'docnumber', $number);}
          
           
          if ($duplicatecnpj) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['document']= $this->getTranslator()->trans('badiu.admin.enterprise.enterprise.message.cnpjexist');
                $info = 'badiu.admin.enterprise.enterprise.message.cnpjexist';
                return $this->getResponse()->denied($info, $message);
      
            }
      }else  if ($doctype == 'EMAIL') {
          //set key message for document
         $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');
         $email = $docnumber;
        
        if ($email) {
            $validate = $validatemail->email($email);
            if (!$validate) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['document']= $this->getTranslator()->trans('badiu.admin.enterprise.enterprise.message.emailnotvalid');
                $info = 'badiu.admin.enterprise.enterprise.message.emailnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
            $duplicateemail=null;
            if($this->isedit()){$duplicateemail = $data->existColumnEdit($id,$entity, 'docnumber', $email);}
            else{$duplicateemail = $data->existColumnAdd($entity, 'docnumber', $email);}
          
            if ($duplicateemail) {
                $message=array();
                 $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['document']= $this->getTranslator()->trans('badiu.admin.enterprise.enterprise.message.duplicationemail', array('%record%' => $email));
                $info = 'badiu.admin.enterprise.enterprise.message.duplicationemail';
                return $this->getResponse()->denied($info, $message);
            }  
        }
      }
       
      return null;
    }
    public function isEmailValid() {
        $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');
        $email = $this->getUtildata()->getVaueOfArray($this->getParam(), 'email');
        
        if ($email) {
            $validate = $validatemail->email($email);
            if (!$validate) {
                $message=array();
                 $message['generalerror'] = $this->getTranslator()->trans('badiu.system.operation.process.failed');
                $message['email']=$this->getTranslator()->trans('badiu.admin.enterprise.enterprise.message.emailnotvalid');
                $info = 'badiu.admin.enterprise.enterprise.message.emailnotvalid';
                return $this->getResponse()->denied($info, $message);
            }
        }
        return null;
    }

    public function isEmailDuplicate() {
        $data = $this->getContainer()->get($this->getKminherit()->data());
        $email = $this->getUtildata()->getVaueOfArray($this->getParam(), 'email');
         $id=$this->getParamItem('id');
         $entity =$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
        if ($email) {
            $duplicateemail = null;
             if($this->isedit()){$duplicateemail = $data->existColumnEdit($id,$entity, 'email', $email);}
            else{$duplicateemail = $data->existColumnAdd($entity, 'email', $email);}
          
            if ($duplicateemail) {
                $message=array();
                $message['generalerror'] = $this->getTranslator()->trans('badiu.system.duplication.message');
                $message['email']= $this->getTranslator()->trans('badiu.system.user.user.message.duplicationemail', array('%record%' => $email));
                $info = 'badiu.system.user.user.message.duplicationemail';
                return $this->getResponse()->denied($info, $message);
            }
        }
        
    }
    
   function  setFormAddress(){
        $param = $this->getParam();
        $contactdata     = $this->getUtildata()->getVaueOfArray($this->getParam(), 'contactdata');
         $contactdata=$this->getJson()->decode($contactdata,true); 
         $defaultcontact     = $this->getUtildata()->getVaueOfArray($contactdata, 'default');
        $param['postcode']          = $this->getUtildata()->getVaueOfArray($defaultcontact, 'postcode');
        $param['address']           = $this->getUtildata()->getVaueOfArray($defaultcontact, 'address');
        $param['addressnumber']     = $this->getUtildata()->getVaueOfArray($defaultcontact, 'addressnumber');
        $param['addresscomplement'] = $this->getUtildata()->getVaueOfArray($defaultcontact, 'addresscomplement');
        $param['neighborhood']      = $this->getUtildata()->getVaueOfArray($defaultcontact, 'neighborhood');
        $param['city']              = $this->getUtildata()->getVaueOfArray($defaultcontact, 'city');
        $param['stateid']             = $this->getUtildata()->getVaueOfArray($defaultcontact, 'stateid');
        $param['country']           = $this->getUtildata()->getVaueOfArray($defaultcontact, 'country');
        $param['phone']             = $this->getUtildata()->getVaueOfArray($defaultcontact, 'phone');
        $param['phonemobile']       = $this->getUtildata()->getVaueOfArray($defaultcontact, 'phonemobile');
        $param['email']             =$this->getUtildata()->getVaueOfArray($defaultcontact, 'email');
        $this->setParam($param);
   }
   
  
   function  getFormAddress(){
       //contact
        $defaultcontact                      = array();
        $defaultcontact['postcode']          = $this->getUtildata()->getVaueOfArray($this->getParam(), 'postcode');
        $defaultcontact['address']           = $this->getUtildata()->getVaueOfArray($this->getParam(), 'address');
        $defaultcontact['addressnumber']     = $this->getUtildata()->getVaueOfArray($this->getParam(), 'addressnumber');
        $defaultcontact['addresscomplement'] = $this->getUtildata()->getVaueOfArray($this->getParam(), 'addresscomplement');
        $defaultcontact['neighborhood']      = $this->getUtildata()->getVaueOfArray($this->getParam(), 'neighborhood');
        $defaultcontact['city']              = $this->getUtildata()->getVaueOfArray($this->getParam(), 'city');
        $defaultcontact['stateid']           = $this->getUtildata()->getVaueOfArray($this->getParam(), 'stateid');
        $defaultcontact['state']             = "";
        $defaultcontact['stateshortname']    = "";

        if (!empty($defaultcontact['stateid'])) {
            $stateinfo                         = $this->getContainer()->get('badiu.util.address.state.data')->getInfoById($defaultcontact['stateid']);
            $defaultcontact['state']          = $this->getUtildata()->getVaueOfArray($stateinfo, 'name');
            $defaultcontact['stateshortname'] = $this->getUtildata()->getVaueOfArray($stateinfo, 'shortname');
        }
        
        $defaultcontact['country']  = $this->getUtildata()->getVaueOfArray($this->getParam(), 'country');

        $defaultcontact['phone']       = $this->getUtildata()->getVaueOfArray($this->getParam(), 'phone');
        $defaultcontact['phonemobile'] = $this->getUtildata()->getVaueOfArray($this->getParam(), 'phonemobile');
        $defaultcontact['email']       = $this->getUtildata()->getVaueOfArray($this->getParam(), 'email');
        
        
        
        
        $contact                 = array();
        $contact['default']     = $defaultcontact;
       $contactdatajsont        = $this->getJson()->encode($contact);
        
        $newparam=$this->getParam();
        $newparam['contactdata']=$contactdatajsont;
        
        //clean conctat field
        unset($newparam['postcode']);
         unset($newparam['address']);
        unset($newparam['addressnumber']);
        unset($newparam['addresscomplement']);
        unset($newparam['neighborhood']);
        unset($newparam['city']);
        unset($newparam['stateid']);
        unset($newparam['phone']);
        unset($newparam['phonemobile']);
        unset($newparam['phonemobile']);
        unset($newparam['country']);
        
       
       
        $this->setParam($newparam);
   }
  
   function  getForm(){
       $newparam=$this->getParam();
       //document
        $newparam['doctype']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield1');
        $newparam['docnumber']=$this->getUtildata()->getVaueOfArray($this->getParam(), 'document_badiuchoicetextfield2');
       
        $newparam['entity']=$this->getContainer()->get('badiu.system.access.session')->get()->getEntity();
        $newparam['deleted']=FALSE; 
   
         unset($newparam['document']);
         unset($newparam['document_badiuchoicetextfield1']);
         unset($newparam['document_badiuchoicetextfield2']);
         
          
        $this->setParam($newparam);
      
   }
   
    function  setForm(){
       
            $param = $this->getParam();
             //doc
            $docconf=array();
            $docconf['field1'] = $this->getUtildata()->getVaueOfArray($this->getParam(), 'doctype');
            $docconf['field2'] =  $this->getUtildata()->getVaueOfArray($this->getParam(), 'docnumber');
            $param['document'] =  $docconf;
            
            $this->setParam($param);
   }

  
    
}
