<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class AddressController extends BadiuController
{
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

      public function addEnterpriseidToAddress($dto,$dtoParent) {
			$dto->setModuleinstance($dtoParent->getId());
			return $dto;
    }

	public function findEnterpriseidInAddress($dto) {
        return $dto->getModuleinstance();
    }

}
