<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormError;

class EnterpriseController extends BadiuController
{
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

   

    public function setDefaultDataOnAddForm($dto)
    {
        $defaultData = $this->getDefaultdata();
        if (!empty($defaultData)) {
            foreach ($defaultData as $key => $value) {
                $dto[$key] = $value;
            }
        }

        return $dto;
    }

    public function setDefaultDataOnOpenEditForm($dto)
    {
        $userlib = $this->getContainer()->get('badiu.admin.enterprise.enterprise.form');
        $id      = $this->getContainer()->get('request')->get('id');
        $dto     = $userlib->getDataFormEdit($id);
        return $dto;
    }

    public function save($data)
    {
        $dto     = $data->getDto();
        $userlib = $this->getContainer()->get('badiu.admin.enterprise.enterprise.form');
        $result  = $data->setDto($userlib->save($data));
        return $result;
    }

	
	
     public function addEnterpriseidToDepartment($dto,$dtoParent) {
        $dto->setEnterpriseid($dtoParent);
        return $dto;
    }

	public function findEnterpriseidInDepartment($dto) {
        return $dto->getEnterpriseid()->getId();
    }
	
	   public function addEnterpriseidToOffice($dto,$dtoParent) {
        $dto->setEnterpriseid($dtoParent);
        return $dto;
    }

	public function findEnterpriseidInOffice($dto) {
        return $dto->getEnterpriseid()->getId();
    }
}
