<?php
namespace Badiu\Admin\EnterpriseBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class CoreFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
    
  

  
  public  function getDtype(){
    $list=array();
    $list['muster']=$this->getDtypeLabel('muster');
	$list['branch']=$this->getDtypeLabel('branch');
	return $list;
}

public  function getDtypeLabel($key){
    $list=array();
	if(empty($key)){return null;}
    $label=$this->getTranslator()->trans('badiu.admin.enterprise.dtype.'.$key);
    return $label;
}

}
