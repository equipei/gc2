<?php
namespace Badiu\Admin\EnterpriseBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnterpriseMembersAttendancePlannedFormController extends BadiuFormController
{ 
 private $deleteattencance=false;
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     public function changeParamOnOpen() {
        
			 $param = $this->getParam();
			 $param['updateattencance'] =1;
			 $param['deleteattencance'] =1;
			 $this->setParam($param); 
     }   
     
    public function changeParam() {

  
        $param = $this->getParam();

        $hourstart=$this->getParamItem('hourstart');
        $hourend=$this->getParamItem('hourend');
        $hourduration=null;
        if(!empty($hourstart) && !empty($hourend)){$hourduration=$hourend-$hourstart;}
        
		$param['hourduration']=$hourduration; 
		
		$this->deleteattencance=$this->getParamItem('deleteattencance');
		
        $this->setParam($param);
		$this->removeParamItem('deleteattencance');
        
	}
     
       public function execAfter(){
		   $param = $this->getParam();
		   $entity=$this->getEntity();
		   $id=$this->getParamItem('id');
		   $modulekey=$this->getParamItem('modulekey');
		   $moduleinstance = $this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		   $membersdata=$this->getContainer()->get('badiu.admin.enterprise.members.data');
		   $userid=$membersdata->getUseridById($entity,$moduleinstance);
		  
		   if($this->deleteattencance){
			   $attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
			   $rparam=array('referencekey'=>$id,'userid'=>$userid,'modulekey'=>$modulekey,'statusinfo'=>'planned');
			   $attendancedata->removeByParam($entity,$rparam);
		   }
		   
		   $updateattencance=$this->getParamItem('updateattencance');

		   if($updateattencance){
				
				
				
				$param['referencekey']=$id;
				
				$param['userid']=$userid;
			
				$plannedautomaticfill=$this->getContainer()->get('badiu.admin.attendance.planned.lib.automaticfill');
				$param['targettype']='oneuser';
				
				//dayofweek
				 $dayofweek=$this->getParamItem('dayofweek');
				$ldw=array();
				$pos=stripos($dayofweek, ",");
				if($pos=== false){
					$ldw=array($dayofweek);
				}else{
					$ldw= explode(",", $dayofweek);
				}
			    $param['weekday']=$ldw;
			    $param['dhour']=26280000; //five year
				$plannedautomaticfill->setParam($param);
				$result= $plannedautomaticfill->exec();
				
				//$countprocess=$this->getUtildata()->getVaueOfArray($result,'message.countexec',true);
				
		   }
			
		
		   parent::execAfter();
	   }
    
}
