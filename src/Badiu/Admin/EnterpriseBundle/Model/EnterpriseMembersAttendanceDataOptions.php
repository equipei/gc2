<?php

namespace Badiu\Admin\EnterpriseBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\Admin\AttendanceBundle\Model\AttendanceDataOptions;
class EnterpriseMembersAttendanceDataOptions extends AttendanceDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
 
	 public  function getCriteria(){
        
        $list=array();
        $list['manual']=$this->getCriteriaLabel('manual');
	    $list['automatic_lmssystemaccessintimeplanned']=$this->getCriteriaLabel('automatic_lmssystemaccessintimeplanned');
	    return  $list;
    }
	
	 public  function getCriteriaLabel($value){
        $label=null;
        if($value=='manual'){$label=$this->getTranslator()->trans('badiu.admin.enterprise.membersattendanceplanned.criteria.manual');}
	    else if($value=='automatic_lmssystemaccessintimeplanned'){$label=$this->getTranslator()->trans('badiu.admin.enterprise.membersattendanceplanned.criteria.automaticlmssystemaccessintimeplanned');}
	    return  $label;
    }
	
}
