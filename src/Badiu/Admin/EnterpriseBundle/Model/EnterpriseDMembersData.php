<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class EnterpriseDMembersData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

			  
     public function geMembers($departmentid,$statusid) {
				$sql="SELECT o FROM ".$this->getBundleEntity()." o  WHERE o.departmentid=:departmentid AND o.statusid=:statusid"; 
                $query = $this->getEm()->createQuery($sql);
                $query->setParameter('departmentid',$departmentid);
				$query->setParameter('statusid',$statusid);
                $result= $query->getResult();
                return  $result;
       }

}
 