<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class EnterpriseMembersData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

	public function existMember($entity,$userid,$enterpriseid) {
		$r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.userid=:userid AND o.enterpriseid=:enterpriseid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('userid',$userid);
			$query->setParameter('enterpriseid',$enterpriseid);
            $result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
   }

	public function getUseridById($entity,$id) {
		$r=FALSE;
            $sql="SELECT u.id AS userid FROM ".$this->getBundleEntity()." o JOIN o.userid u WHERE o.entity = :entity AND o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
             if(isset($result['userid'])){ $result=$result['userid'];}
             return $result;
	}
 
 public function getInfoById($entity,$id) {
		$r=FALSE;
            $sql="SELECT o.id,u.id AS userid,o.timestart,o.timeend FROM ".$this->getBundleEntity()." o JOIN o.userid u WHERE o.entity = :entity AND o.id=:id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('id',$id);
            $result= $query->getOneOrNullResult();
           return $result;
	}
    public function getForAutocomplete() {
         $name=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
         $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        
        $wsql="";
        if(!empty($name)){
            $wsql=" AND LOWER(CONCAT(u.id,u.firstname,u.email,u.username,e.name))  LIKE :name ";
            $name=strtolower($name);
        }
         $sql="SELECT DISTINCT o.id,CONCAT(u.firstname,' ',u.lastname,' ',u.email,' / ',e.name) AS name FROM ".$this->getBundleEntity()." o JOIN o.userid u JOIN o.enterpriseid e WHERE  o.entity = :entity  $wsql ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(50);
        $result= $query->getResult();
        return  $result;
    }
  public function getNameByIdOnEditAutocomplete($id) {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT CONCAT(u.firstname,' ',u.lastname,' ',u.email,' / ',e.name) AS name FROM ".$this->getBundleEntity()." o JOIN o.userid u JOIN o.enterpriseid e WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }
}
 