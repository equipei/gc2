<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnterpriseData extends BadiuDataBase
{

    public function __construct(Container $container, $bundleEntity)
    {
        parent::__construct($container, $bundleEntity);
    }

    public function getMenuOffice($id)
    {

        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit       = $sysoperation->isEdit();

        if ($isEdit) {
            $officedata = $this->getContainer()->get('badiu.admin.enterprise.office.data');
            $sql        = "SELECT c.id, c.name FROM " . $officedata->getBundleEntity() . " o JOIN o.enterpriseid c WHERE o.id=:id";
            $query      = $this->getEm()->createQuery($sql);
            $query->setParameter('id', $id);
            $result = $query->getSingleResult();
            return $result;

        } else {

            return $this->getNameById($id);
        }

    }

    public function getMenuDepartment($id)
    {

        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit       = $sysoperation->isEdit();

        if ($isEdit) {
            $departmentdata = $this->getContainer()->get('badiu.admin.enterprise.department.data');
            $sql            = "SELECT c.id, c.name FROM " . $departmentdata->getBundleEntity() . " o JOIN o.enterpriseid c WHERE o.id=:id";
            $query          = $this->getEm()->createQuery($sql);
            $query->setParameter('id', $id);
            $result = $query->getSingleResult();
            return $result;

        } else {

            return $this->getNameById($id);
        }

    }
 
    public function getMenuMembers($id)
    {

        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit       = $sysoperation->isEdit();

        if ($isEdit) {
            $membersdata = $this->getContainer()->get('badiu.admin.enterprise.members.data');
            $sql         = "SELECT c.id, c.name FROM " . $membersdata->getBundleEntity() . " o JOIN o.enterpriseid c WHERE o.id=:id";
            $query       = $this->getEm()->createQuery($sql);
            $query->setParameter('id', $id);
            $result = $query->getSingleResult();
            return $result;

        } else {

            return $this->getNameById($id);
        }

    }
public function getMenuAddress($id) {
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit=$sysoperation->isEdit();
        
        if($isEdit ){
			$adata=$this->getContainer()->get('badiu.util.address.data.data');
			$enterpriseid=$adata->getModuleinstance($id);
            if(!empty($enterpriseid))  return  $this->getNameById($enterpriseid);
            else return  'user not found';
         }else{
           return  $this->getNameById($id);
        }
       
    }

    public function getMenuTelephone($id) {
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit=$sysoperation->isEdit();
        
        if($isEdit ){
			$adata=$this->getContainer()->get('badiu.util.address.telephone.data');
			$enterpriseid=$adata->getModuleinstance($id);
            if(!empty($enterpriseid))  return  $this->getNameById($enterpriseid);
            else return  'user not found';
         }else{
           return  $this->getNameById($id);
        }
       
    }
    
    public function getMenuDocument($id) {
        $sysoperation=$this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit=$sysoperation->isEdit();
        
        if($isEdit ){
			$adata=$this->getContainer()->get('badiu.util.address.document.data');
			$enterpriseid=$adata->getModuleinstance($id);
            if(!empty($enterpriseid))  return  $this->getNameById($enterpriseid);
            else return  'user not found';
         }else{
           return  $this->getNameById($id);
        }
       
    }

    public function getForAutocomplete() {
        $name=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
       
       $wsql="";
       if(!empty($name)){
           $wsql=" AND LOWER(CONCAT(o.id,o.name,o.docnumber))  LIKE :name ";
           $name=strtolower($name);
       }
        $sql="SELECT DISTINCT o.id,CONCAT(o.name,' ',o.docnumber) AS name FROM ".$this->getBundleEntity()." o WHERE  o.entity = :entity  $wsql ";

       $query = $this->getEm()->createQuery($sql);
       $query->setParameter('entity',$badiuSession->get()->getEntity());
       if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
       $query->setMaxResults(50);
       $result= $query->getResult();
       return  $result;
   }
 public function getNameByIdOnEditAutocomplete($id) {
       $badiuSession=$this->getContainer()->get('badiu.system.access.session');
       $sql="SELECT CONCAT(o.name,' ',o.docnumber) AS name FROM ".$this->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
       $query = $this->getEm()->createQuery($sql);
       $query->setParameter('entity',$badiuSession->get()->getEntity());
       $query->setParameter('id',$id);
       $result= $query->getOneOrNullResult();
       if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
       return $result;
 }

 public function getInfoForSync($id) {
    $badiuSession=$this->getContainer()->get('badiu.system.access.session');
    $sql="SELECT o.id,o.name,o.shortname,o.nickname,o.email,o.idnumber,o.doctype,o.docnumber,o.documentdata,o.contactdata FROM ".$this->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
    $query = $this->getEm()->createQuery($sql);
    $query->setParameter('entity',$badiuSession->get()->getEntity());
    $query->setParameter('id',$id);
    $result= $query->getOneOrNullResult();
    if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
    return $result;
}

}
