<?php

namespace Badiu\Admin\EnterpriseBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;

class CoreFormat extends BadiuFormat{
     private $formdataoptions=null;
	 
     function __construct(Container $container) {
            parent::__construct($container);
			$this->formdataoptions=$this->getContainer()->get('badiu.admin.enterprise.core.formdataoptions');
	} 
public  function dtype($data){
	
            $value="";
            $type=null;
             $type=$this->getUtildata()->getVaueOfArray($data, 'dtype');
			 $value=$this->formdataoptions->getDtypeLabel($type);
            return $value; 
     } 
	

}
