<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnterpriseBankController extends BadiuController
{
function __construct(Container $container) {
            parent::__construct($container);
              }
              
    public function addCheckForm($form,$data) {
           $check=true;
		 
         // if ($data->existEdit()) {
            //    $form->get('cnpj')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.cpfnotvalid')));
        //        $check= false; 
          //  }
           return  $check;
       }
	   
	 public function setDefaultDataOnAddForm($dto) {
            $defaultData=$this->getDefaultdata();
          if(!empty($defaultData)){
              foreach ($defaultData as $key => $value){
                  $dto[$key]=$value;
              }  
          }
         
        return $dto;
     }
	 
	 public function setDefaultDataOnOpenEditForm($dto) {
          $userlib=$this->getContainer()->get('badiu.system.user.userbr.lib');
		  $id=$this->getContainer()->get('request')->get('id');
		  $dto=$userlib->getUserDb($id);
		 
           return $dto;
     }
    
	 public function save($data) {
	   
		$dto=$data->getDto();
		$banckdatalib=$this->getContainer()->get('badiu.admin.enterprise.bank.lib');
		
		//save bankdata
		$data->setDto($banckdatalib->getBanckDataForm($dto));
		$result=$data->save();
		
		return $data->getDto();
    }   
	


}
