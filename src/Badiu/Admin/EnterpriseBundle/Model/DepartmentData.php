<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class DepartmentData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

	 public function getFormChoice($entity,$param=array(),$orderby="ORDER BY o.sortorder ") {
       $result= parent::getFormChoice($entity,$param,$orderby);
        return  $result;
    }

}
 