<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnterpriseMembersAttendanceFormController extends BadiuFormController
{ 
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
     public function changeParamOnOpen() {
         $param = $this->getParam();
		 if($this->isEdit()){
			 $timestart = $this->getUtildata()->getVaueOfArray($param,'timestart');
			
			$hourstart=0;			 
			 if(!empty($timestart)){
				  $ts=new \DateTime();
				  $ts->setTimestamp($timestart);
				  $h=$ts->format('H');
				  $m=$ts->format('i');
				 
				  $hourstart=($h*60)+$m;
				
			 }
			 
			  $param['hourstart']=$hourstart; 
			  $this->setParam($param);
		}
       
        
     }     
      
    public function changeParam() {
           $param = $this->getParam();
		 
			//get userid by moduleince
			$moduleinstance = $this->getUtildata()->getVaueOfArray($param,'moduleinstance');
			$membersdata=$this->getContainer()->get('badiu.admin.enterprise.members.data');
			$entity=$this->getEntity();
			$userid=$membersdata->getUseridById($entity,$moduleinstance);
			$param['userid']=$userid;
		   $timestart = $this->getUtildata()->getVaueOfArray($param,'timestart');
		   $hourstart = $this->getUtildata()->getVaueOfArray($param,'hourstart');
		   $dhour = $this->getUtildata()->getVaueOfArray($param,'dhour');
		   
		   if(!empty($timestart) && $hourstart > 0){
			   $timestart->setTime(0,0);
			   $timestartlong=$timestart->getTimestamp()+($hourstart*60);
			   $timestart->setTimestamp($timestartlong);
			   
			   $timeend=clone $timestart;
			   if(empty($dhour)){$dhour=0;}
			   $timeendlong=$timeend->getTimestamp()+($dhour*60);
			   $timeend->setTimestamp($timeendlong);
			   $param['timestart']=$timestart;
			   $param['timeend']=$timeend;
		   }
		   unset($param['hourstart']);
		   
		    $statusinfo = $this->getUtildata()->getVaueOfArray($param,'statusinfo');
			$thour = $this->getUtildata()->getVaueOfArray($param,'thour');
			if($statusinfo=='executed' && empty($thour)){$param['thour']=$dhour;}
			else if($statusinfo=='planned'){$param['thour']=0;}
			
			$statusid = $this->getUtildata()->getVaueOfArray($param,'statusid');
			if(!empty($statusid)){
				$statusshortname=$this->getContainer()->get('badiu.admin.attendance.status.data')->getShortnameById($statusid);
				if($statusshortname=='absent'){$param['thour']=0;}
				$param['statusinfo']='executed';
			}
			
			$param['daykey']=$timestart->format('Y_m_d');
		    $this->setParam($param);
       }
     
    
    
}
