<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Install extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
        $result= $this->initDbStatus();
            
           return $result;
    } 

     public function initDbStatus() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.enterprise.status.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist){return 0;}
         
         $list=array();
         $list['active']=$this->getTranslator()->trans('badiu.admin.enterprise.status.active');
         $list['inactive']=$this->getTranslator()->trans('badiu.admin.enterprise.status.inactive');
                 
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
            
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
         
     }
   
}
