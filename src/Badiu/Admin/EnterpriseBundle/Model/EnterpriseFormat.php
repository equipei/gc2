<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnterpriseFormat extends BadiuFormat
{

    public function __construct(Container $container)
    {

        parent::__construct($container);
    }

    public function docnumber($data)
    {
        $value     = "";
        $docnumber = null;
        $doctype   = null;

        if (isset($data["docnumber"])) 
        {


        	$docnumber = $data["docnumber"];

        }

        if (isset($data["doctype"])) 
        {


        	$doctype = $data["doctype"];

        }

        //if($doctype=='CNPJ'){
        $docservice = $this->getContainer()->get('badiu.util.document.role.cpnj');

        $value = $docservice->format($docnumber);
        //}else{
        //    $value=$docnumber;
        //}

        return $value;
    }
    public  function defaultaddress($data){
        $contactdata=$this->getUtildata()->getVaueOfArray($data,'contactdata');
        $contactdata=$this->getJson()->decode($contactdata,true); 
        $defaultcontact=$this->getUtildata()->getVaueOfArray($contactdata,'default');
        $defaultcontact=$this->getContainer()->get('badiu.util.address.data.format')->defaultdd($defaultcontact);
        return $defaultcontact;
    }
}
