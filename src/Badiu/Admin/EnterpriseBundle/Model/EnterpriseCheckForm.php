<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuCheckForm;
use Symfony\Component\Form\FormError;

class EnterpriseCheckForm extends BadiuCheckForm {

    function __construct(Container $container) {
        parent::__construct($container);
    }

    public function addCheckForm($form, $data) {

        $check = true;

        // validate cpnj
        $doc = $form->get('document')->getData();
        $doctype = $this->getUtildata()->getVaueOfArray($doc, 'field1');

        $docnumber = $this->getUtildata()->getVaueOfArray($doc, 'field2');

        if ($doctype == 'CNPJ') {
            $cnpjdata = $this->getContainer()->get('badiu.util.document.role.cnpj');
            $valid = $cnpjdata->isValid($docnumber);

            if (!$valid) {
                $form->get('document')->addError(new FormError($this->getTranslator()->trans('badiu.admin.enterprise.enterprise.message.cpnjnotvalid')));
                $check = false;
            }

            $number = $cnpjdata->clean($docnumber);

            $duplicatecpnj = $data->existColumnAdd($this->getEntity(), 'docnumber', $number);

            if ($duplicatecpnj) {

                $form->get('document')->addError(new FormError($this->getTranslator()->trans('badiu.admin.enterprise.enterprise.message.cpnjexist')));
                $check = false;
            }
        }

        //validate cep
        $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');

        $cep = $form->get('professionalpostcode')->getData();

        if ($cep) {

            $validate = $validatecontact->cep($cep);
            if (!$validate) {
                $form->get('professionalpostcode')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.cepnotvalid')));
                $check = false;
            }
        }

        //validate telefone fixo        
        $number_fixed = $form->get('professionalphone')->getData();
        if ($number_fixed) {

            $validate = $validatecontact->phoneFixe($number_fixed);
            if (!$validate) {
                $form->get('professionalphone')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonenotvalid')));
                $check = false;
            }
        }


        //validate telefone mobile        

        $number_mobile = $form->get('professionalphonemobile')->getData();

        if ($number_mobile) {

            $validate = $validatecontact->phoneMobile($number_mobile);
            if (!$validate) {
                $form->get('professionalphonemobile')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonemobilenotvalid')));
                $check = false;
            }
        }


        //validate email
        $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');

        $email = $form->get('professionalemail')->getData();

        if ($email) {

            $validate = $validatemail->email($email);
            if (!$validate) {
                $form->get('professionalemail')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid')));
                $check = false;
            }
        }


        //check rason social
        $reason = $form->get('name')->getData();
        $duplicatename = $data->existColumnAdd($this->getEntity(), 'name', $reason);
        if ($duplicatename) {
            $form->get('name')->addError(new FormError($this->getTranslator()->trans('badiu.admin.enterprise.enterprise.message.duplicatename')));
            $check = false;
        }



        return $check;
    }

    public function editCheckForm($form, $data) {
        $check = true;
        $validatecontact = $this->getContainer()->get('badiu.util.address.lib.validate.brcontact');
        $doc = $form->get('document')->getData();
        $doctype = $this->getUtildata()->getVaueOfArray($doc, 'field1');
        $docnumber = $this->getUtildata()->getVaueOfArray($doc, 'field2');

        if ($doctype == 'CNPJ') {
            $cnpjdata = $this->getContainer()->get('badiu.util.document.role.cnpj');
            $valid = $cnpjdata->isValid($docnumber);

            if (!$valid) {
                $form->get('document')->addError(new FormError($this->getTranslator()->trans('badiu.admin.enterprise.enterprise.message.cpnjnotvalid')));
                $check = false;
            }

            $number = $cnpjdata->clean($docnumber);
            $duplicatecpnj = $data->existColumnEdit($this->getId(), $this->getEntity(), 'docnumber', $number);

            if ($duplicatecpnj) {

                $form->get('document')->addError(new FormError($this->getTranslator()->trans('badiu.admin.enterprise.enterprise.message.cpnjexist')));
                $check = false;
            }
        }

        //validate cep       

        $cep = $form->get('professionalpostcode')->getData();

        if ($cep) {

            $validate = $validatecontact->cep($cep);
            if (!$validate) {
                $form->get('professionalpostcode')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.cepnotvalid')));
                $check = false;
            }
        }


        //validate telefone fixo        
        $number_fixed = $form->get('professionalphone')->getData();
        if ($number_fixed) {

            $validate = $validatecontact->phoneFixe($number_fixed);
            if (!$validate) {
                $form->get('professionalphone')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonenotvalid')));
                $check = false;
            }
        }


        //validate telefone mobile

        $number_mobile = $form->get('professionalphonemobile')->getData();

        if ($number_mobile) {

            $validate = $validatecontact->phoneMobile($number_mobile);
            if (!$validate) {
                $form->get('professionalphonemobile')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.personalphonemobilenotvalid')));
                $check = false;
            }
        }

        //validate email
        $validatemail = $this->getContainer()->get('badiu.util.address.lib.validate.contact');

        $email = $form->get('professionalemail')->getData();

        if ($email) {

            $validate = $validatemail->email($email);
            if (!$validate) {
                $form->get('professionalemail')->addError(new FormError($this->getTranslator()->trans('badiu.system.user.user.message.emailnotvalid')));
                $check = false;
            }
        }

        //check rason social
        $reason = $form->get('name')->getData();
        $duplicatename = $data->existColumnEdit($this->getId(), $this->getEntity(), 'name', $reason);
        if ($duplicatename) {
            $form->get('name')->addError(new FormError($this->getTranslator()->trans('badiu.admin.enterprise.enterprise.message.duplicatename')));
            $check = false;
        }


        return $check;
    }

}
