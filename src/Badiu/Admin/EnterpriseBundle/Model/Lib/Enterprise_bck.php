<?php
namespace Badiu\Admin\EnterpriseBundle\Model\Lib;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class Enterprise extends BadiuModelLib
{

    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    public function getDataFormEdit($enterpriseid)
    {
        $fdata = array();
        $fdata = $this->getEnterpriseDb($enterpriseid, $fdata);
        $fdata = $this->getAddressDb($enterpriseid, $fdata);
        $fdata = $this->getTelephoneFixeDb($enterpriseid, $fdata);
        $fdata = $this->getTelephoneMobileDb($enterpriseid, $fdata);
		$fdata = $this->getStateRegistrationDb($enterpriseid, $fdata);
		$fdata = $this->getMunicipalRegistrationDb($enterpriseid, $fdata);
        return $fdata;
    }
    public function save($data)
    {
        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit       = $sysoperation->isEdit();
        $enterpriseid = null;
        if ($isEdit) {
            $enterpriseid = $this->getContainer()->get('request')->get('id');
        }
        $dto = $data->getDto();

        //save user
        $data->setDto($this->getEnterpriseForm($enterpriseid, $dto));
        $result       = $data->save();
        $enterpriseid = $data->getDto()->getId();

        //save address
        $addressDto  = $this->getAddressForm($enterpriseid, $dto, $isEdit);
        $addressData = $this->getContainer()->get('badiu.util.address.data.data');
        $addressData->setDto($addressDto);
        $addressData->save();

        //tel
        $telData = $this->getContainer()->get('badiu.util.address.telephone.data');

        //save telfixe
        $telfixeDto = $this->getTelephoneFixeForm($enterpriseid, $dto, $isEdit);
        $telData->setDto($telfixeDto);
        $telData->save();

        //save telmobile
        $telmobileDto = $this->getTelephoneMobileForm($enterpriseid, $dto, $isEdit);
        $telData->setDto($telmobileDto);
        $telData->save();

		

        //document          
        $documentData = $this->getContainer()->get('badiu.util.document.data.data');

        //save BRPJIE
        $documentDto = $this->getStateRegistrationForm($enterpriseid, $dto, $isEdit);
        $documentData->setDto($documentDto);
        $documentData->save();


        //save BRPJIM
        $documentDto = $this->getMunicipalRegistrationForm($enterpriseid, $dto, $isEdit);
        $documentData->setDto($documentDto);
        $documentData->save();


        return $data->getDto();
    }

    //todos os campos do formulario do usuario
    public function getEnterpriseDb($enterpriseid, $fdata)
    {

        $data               = $this->getContainer()->get('badiu.admin.enterprise.enterprise.data');
        $dto                = $data->findById($enterpriseid);
        $fdata['name']      = $dto->getName();
        $fdata['shortname'] = $dto->getShortname();
      
        $fdata['statusid']  = $dto->getStatusid();
        $fdata['categoryid']  = $dto->getCategoryid();

        
        $fdata['idnumber']    = $dto->getIdnumber();
        $fdata['param']       = $dto->getParam();
        $fdata['description'] = $dto->getDescription();
		$fdata['contactperson'] = $dto->getContactperson();
		$fdata['email'] = $dto->getEmail();
		$fdata['site'] = $dto->getSite();
        $fdata['document']=array();        
        $fdata['document']['field1']=$dto->getDoctype();
        $fdata['document']['field2']=$dto->getDocnumber();
        return $fdata;

    }

    public function getEnterpriseForm($enterpriseid, $fdata)
    {
        $dto = null;
        if (!empty($enterpriseid)) {
            $data = $this->getContainer()->get('badiu.admin.enterprise.enterprise.data');
            $dto  = $data->findById($enterpriseid);
        } 

		if(empty($dto))	{
            $dto = $this->getContainer()->get('badiu.admin.enterprise.enterprise.entity');
            $dto = $this->initDefaultEntityData($dto);
        }

        if (isset($fdata['name'])) {$dto->setName($fdata['name']);}
        if (isset($fdata['shortname'])) {$dto->setShortname($fdata['shortname']);}
       
        if (isset($fdata['statusid'])) {$dto->setStatusid($fdata['statusid']);}
        if (isset($fdata['categoryid'])) {$dto->setCategoryid($fdata['categoryid']);}
        if (isset($fdata['idnumber'])) {$dto->setIdnumber($fdata['idnumber']);}
        if (isset($fdata['param'])) {$dto->setParam($fdata['param']);}
        if (isset($fdata['description'])) {$dto->setDescription($fdata['description']);}
		if (isset($fdata['email'])) {$dto->setEmail($fdata['email']);}
		if (isset($fdata['contactperson'])) {$dto->setContactperson($fdata['contactperson']);}
		if (isset($fdata['site'])) {$dto->setSite($fdata['site']);}
       
       $doctype=null;
       $docnumber=null;
        if(isset($fdata['document']['field1'])) {$doctype=$fdata['document']['field1'];}
        if(isset($fdata['document']['field2'])) {$docnumber=$fdata['document']['field2'];}
        	
        if ($doctype=='CNPJ') {
		$docnumber=preg_replace('/[^0-9]/', '', $docnumber);
		
	}
        $dto->setDocnumber($docnumber);
        $dto->setDoctype($doctype);
        return $dto;

    }

    public function getAddressDb($enterpriseid, $fdata)
    {

        $data = $this->getContainer()->get('badiu.util.address.data.data');
        $dto  = $data->findLastByShortnameCategory('badiu.admin.enterprise.enterprise', $enterpriseid, 'residence');

        if ($dto != null) {
			$stateid='';
			if(!empty($dto->getStateid())){$stateid=$dto->getStateid()->getId();}
			$fdata['state']=$stateid;
			$fdata['city']    = $dto->getCity();
            $fdata['address'] = $dto->getAddress();
            $fdata['cep']     = $dto->getCep();
			$fdata['neighborhood']=$dto->getNeighborhood();
			$fdata['addressnumber']=$dto->getAddressnumber();
			$fdata['addressinfo']=$dto->getAddressinfo();	
        }
        return $fdata;
    }
    public function getAddressForm($enterpriseid, $fdata, $edit = false)
    {
        $dto = null;
        if ($edit) {
            $data = $this->getContainer()->get('badiu.util.address.data.data');
            $dto  = $data->findLastByShortnameCategory('badiu.admin.enterprise.enterprise', $enterpriseid, 'residence');
        }
		if(empty($dto))	{
            $dto = $this->getContainer()->get('badiu.util.address.data.entity');
            $dto = $this->initDefaultEntityData($dto);
            $dto->setModulekey('badiu.admin.enterprise.enterprise');
            $dto->setModuleinstance($enterpriseid);
            $dto->setCategoryid($this->getAddressCategory());
        }

       
			
		if(isset($fdata['state'])) {
			 if(empty($dto->getStateid())){$dto->setStateid($this->getContainer()->get('badiu.util.address.state.entity'));}
			 $dto->setStateid($this->getContainer()->get('badiu.util.address.state.data')->findById($fdata['state']));
		}
		
		if (isset($fdata['city'])) {$dto->setCity($fdata['city']);}
        if (isset($fdata['address'])) {$dto->setAddress($fdata['address']);}
        if (isset($fdata['cep'])) {$dto->setCep($fdata['cep']);}
		if(isset($fdata['neighborhood'])) {$dto->setNeighborhood($fdata['neighborhood']);}
		if(isset($fdata['addressnumber'])) {$dto->setAddressnumber($fdata['addressnumber']);}
		if(isset($fdata['addressinfo'])) {$dto->setAddressinfo($fdata['addressinfo']);}


        return $dto;

    }
    public function getTelephoneFixeDb($enterpriseid, $fdata)
    {

        $data = $this->getContainer()->get('badiu.util.address.telephone.data');
        $dto  = $data->findLastByShortnameCategory('badiu.admin.enterprise.enterprise', $enterpriseid, 'residence', 'fixe');

		$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
		$fdata=$telephonefromtypedata->updateFormDataByDto($dto,$fdata,'telfixe');
		return $fdata;
    }
    public function getTelephoneFixeForm($enterpriseid, $fdata, $edit = false)
    {
        $dto = null;
        if ($edit) {
            $data = $this->getContainer()->get('badiu.util.address.telephone.data');
            $dto  = $data->findLastByShortnameCategory('badiu.admin.enterprise.enterprise', $enterpriseid, 'residence', 'fixe');
        } 
		
		if(empty($dto))	{
            $dto = $this->getContainer()->get('badiu.util.address.telephone.entity');
            $dto = $this->initDefaultEntityData($dto);
            $dto->setModulekey('badiu.admin.enterprise.enterprise');
            $dto->setModuleinstance($enterpriseid);
            $dto->setDtype('fixe');
            $dto->setCategoryid($this->getAddressCategory());
        }

		$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
		$dto=$telephonefromtypedata->updateDtoByFormData($dto,$fdata,'telfixe');
	
        return $dto;

    }
    public function getTelephoneMobileDb($enterpriseid, $fdata)
    {

        $data = $this->getContainer()->get('badiu.util.address.telephone.data');
        $dto  = $data->findLastByShortnameCategory('badiu.admin.enterprise.enterprise', $enterpriseid, 'residence', 'mobile');

		$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
		$fdata=$telephonefromtypedata->updateFormDataByDto($dto,$fdata,'telmobileoperator');
		
        return $fdata;
    }
    public function getTelephoneMobileForm($enterpriseid, $fdata, $edit = false)
    {
        $dto = null;
        if ($edit) {
            $data = $this->getContainer()->get('badiu.util.address.telephone.data');
            $dto  = $data->findLastByShortnameCategory('badiu.admin.enterprise.enterprise', $enterpriseid, 'residence', 'mobile');
        } 
		if(empty($dto))	{
            $dto = $this->getContainer()->get('badiu.util.address.telephone.entity');
            $dto = $this->initDefaultEntityData($dto);
            $dto->setModulekey('badiu.admin.enterprise.enterprise');
            $dto->setModuleinstance($enterpriseid);
            $dto->setDtype('mobile');
            $dto->setCategoryid($this->getAddressCategory());
        }

		$telephonefromtypedata=$this->getContainer()->get('badiu.util.address.telephone.form.type.data');
		$dto=$telephonefromtypedata->updateDtoByFormData($dto,$fdata,'telmobile');
		
        return $dto;

    }

   public function getAddressCategory() {
		$data=$this->getContainer()->get('badiu.util.address.category.data');
		$dto=$data->findByShortname($this->getEntity(),'professional');
		return $dto;
	}

    

    //informacoes vindo do bundle document

    public function getStateRegistrationDb($enterpriseid, $fdata)
    {

        $data = $this->getContainer()->get('badiu.util.document.data.data');
        $dto  = $data->findLastByShortnameType('badiu.admin.enterprise.enterprise', $enterpriseid, 'BRPJIE');

        if ($dto != null) {
            $fdata['stateregistration'] = $dto->getNumbertext();
        }
        return $fdata;
    }
    public function getStateRegistrationForm($enterpriseid, $fdata, $edit = false)
    {
        $dto = null;
        if ($edit) {
		
            $data = $this->getContainer()->get('badiu.util.document.data.data');
            $dto  = $data->findLastByShortnameType('badiu.admin.enterprise.enterprise', $enterpriseid,'BRPJIE');
			
        }
		if(empty($dto)) {
			
            $dto = $this->getContainer()->get('badiu.util.document.data.entity');
            $dto = $this->initDefaultEntityData($dto);
            $dto->setModulekey('badiu.admin.enterprise.enterprise');
            $dto->setModuleinstance($enterpriseid);
			$categorydto=$this->getDocumentCategory('BRPJ');
			$dto->setCategoryid($categorydto);			 
			$typedto=$this->getDocumentType('BRPJIE');
			$dto->setTypeid($typedto);
        }
        if (isset($fdata['stateregistration'])) {$dto->setNumbertext($fdata['stateregistration']);}     


        return $dto;

    }


    
    public function getMunicipalRegistrationDb($enterpriseid, $fdata)
    {

        $data = $this->getContainer()->get('badiu.util.document.data.data');
        $dto  = $data->findLastByShortnameType('badiu.admin.enterprise.enterprise', $enterpriseid,'BRPJIM');

        if ($dto != null) {
            $fdata['municipalregistration'] = $dto->getNumbertext();
        }
        return $fdata;
    }

    public function getMunicipalRegistrationForm($enterpriseid, $fdata, $edit = false)
    {
        $dto = null;
        if ($edit) {
        
            $data = $this->getContainer()->get('badiu.util.document.data.data');
            $dto  = $data->findLastByShortnameType('badiu.admin.enterprise.enterprise', $enterpriseid, 'BRPJIM');
            
        } 
		if(empty($dto)) {
            
            $dto = $this->getContainer()->get('badiu.util.document.data.entity');
            $dto = $this->initDefaultEntityData($dto);
            $dto->setModulekey('badiu.admin.enterprise.enterprise');
            $dto->setModuleinstance($enterpriseid);
            $categorydto=$this->getDocumentCategory('BRPJ');
            $dto->setCategoryid($categorydto);           
            $typedto=$this->getDocumentType('BRPJIM');
            $dto->setTypeid($typedto);
        }
        if (isset($fdata['municipalregistration'])) {$dto->setNumbertext($fdata['municipalregistration']);}

        return $dto;

    }


    



    public function getDocumentCategory($shorname) {
		$data=$this->getContainer()->get('badiu.util.document.category.data');
		$dto=$data->findByShortname($this->getEntity(),$shorname);
		return $dto;
	}

   public function getDocumentType($shorname) {
		$data=$this->getContainer()->get('badiu.util.document.type.data');
		$dto=$data->findByShortname($this->getEntity(),$shorname);
		return $dto;
	}
	public function getAddressTelephoneOperator($operatorid) {
		$dto=null;
		$data=$this->getContainer()->get('badiu.util.address.telephoneoperator.data');
		$exist=$data->exist($operatorid);
		if($exist){$dto=$data->findById($operatorid);}
		return $dto;
	}
}
