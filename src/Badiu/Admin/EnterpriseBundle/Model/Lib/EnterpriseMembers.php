<?php

namespace Badiu\Admin\EnterpriseBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class EnterpriseMembers extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	//http://54.156.96.187/badiunet/web/app_dev.php/system/service/process?_service=badiu.admin.enterprise.members.lib&_function=getTimestartTimeendService&enterprisememberid=5
	public function getTimestartTimeendService(){
		$enterprisememberid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('enterprisememberid');
		if(empty($enterprisememberid)){return null;}
		
		$data=$this->getContainer()->get('badiu.admin.enterprise.members.data');
		$dto=$data->getGlobalColumnsValue('o.timestart,o.timeend',array('id'=>$enterprisememberid));
		$castdate=$this->getContainer()->get('badiu.system.core.lib.form.castdatedefault');
		
		$timestart=$this->getUtildata()->getVaueOfArray($dto,'timestart');
		$timestart=$castdate->convertToForm($timestart);
		
		$timeend=$this->getUtildata()->getVaueOfArray($dto,'timeend');
		$timeend=$castdate->convertToForm($timeend);

		$result=array('timestart'=>$timestart,'timeend'=>$timeend);

		return $result;
		
	}
	
}
