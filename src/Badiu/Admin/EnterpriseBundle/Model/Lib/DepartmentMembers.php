<?php

namespace Badiu\Admin\EnterpriseBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class DepartmentMembers extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	/*public function exec()
    {
	
     $add=$this->getContainer()->get('request')->get('add');  
	 $remove=$this->getContainer()->get('request')->get('rmv');  
	 
	if (isset($add)) {
		echo 'ADD';
		
		print_r($_POST['add']);
		
		$this->enrol();
	}else if (isset($remove)) {
		echo 'REMOVE';
		print_r($remove);
    }
	 		
    }*/
	
	public function add($userid,$departmentid,$statusid,$officeid){
		$data=$this->getContainer()->get('badiu.admin.enterprise.dmembers.data');
		$exist=$data->existMember($this->getEntity(),$userid,$departmentid);
		if(!$exist){
			$dto=$this->getContainer()->get('badiu.admin.enterprise.dmembers.entity');
			$dto=$this->initDefaultEntityData($dto);
			$dto->setDepartmentid($this->getDepartement($departmentid));
			if(!empty($officeid)){$dto->setOfficeid($this->getOffice($id));}
			if(!empty($statusid)){$dto->setStatusid($this->getStatus($id));}
			$dto->setUserid($this->getUser($userid));
			$data->setDto($dto);
			$data->save();
			return $data->getDto()->getId();
		}
		
	}
	
	
	public function addPicklist(){
			$fdata=$_GET['add'];
			$departmentid=$this->getContainer()->get('request')->get('parentid'); 
			
			foreach ($fdata as $data){
				$userid=null;
				if(isset($data['id'])){$userid=$data['id'];}
				//if(isset($data['field1'])){$roleid=$data['field1'];}
				//if(isset($data['field2'])){$statusid=$data['field2'];}
				
				if(!empty($userid)){
					$this->add($userid,$departmentid);
				}
			}
			
			
	}
	
	
	
	public function getUser($id) {
		$data=$this->getContainer()->get('badiu.system.user.user.data');
		$dto=$data->findById($id);
		return $dto;
	}
	public function getDepartement($id) {
		$data=$this->getContainer()->get('badiu.admin.enterprise.department.data');
		$dto=$data->findById($id);
		return $dto;
	}
	
	public function getOffice($id) {
		$data=$this->getContainer()->get('badiu.admin.enterprise.office.data');
		$dto=$data->findById($id);
		return $dto;
	}
	
	public function getStatus($id) {
		$data=$this->getContainer()->get('badiu.admin.enterprise.dmembersstatus.data');
		$dto=$data->findById($id);
		return $dto;
	}
}
