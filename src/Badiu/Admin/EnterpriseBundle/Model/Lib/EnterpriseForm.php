<?php
namespace Badiu\Admin\EnterpriseBundle\Model\Lib;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnterpriseForm extends BadiuModelLib
{
    
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }
    
    public function getDataFormEdit($enterpriseid)
    {
        $fdata = array();
        $fdata = $this->getDefaultDb($enterpriseid, $fdata);
        
        return $fdata;
    }
    public function save($data)
    {
        $sysoperation = $this->getContainer()->get('badiu.system.core.lib.util.systemoperation');
        $isEdit       = $sysoperation->isEdit();
        $enterpriseid = null;
        if ($isEdit) {
            $enterpriseid = $this->getContainer()->get('request')->get('id');
        }
        $dto = $data->getDto();
        
        //save user
        $data->setDto($this->getDefaultForm($enterpriseid, $dto));
        $result       = $data->save();
        $enterpriseid = $data->getDto()->getId();
        
        return $data->getDto();
    }
    
    
    public function getDefaultDb($enterpriseid, $fdata)
    {
        
        $data               = $this->getContainer()->get('badiu.admin.enterprise.enterprise.data');
        $dto                = $data->findById($enterpriseid);
        $fdata['name']      = $dto->getName();
        $fdata['shortname'] = $dto->getShortname();
        
        $fdata['statusid'] = $dto->getStatusid();
        
        
        
        $fdata['idnumber']           = $dto->getIdnumber();
        $fdata['param']              = $dto->getParam();
        $fdata['description']        = $dto->getDescription();
        $fdata['contactperson']      = $dto->getContactperson();
        $fdata['email']              = $dto->getEmail();
        $fdata['site']               = $dto->getSite();
        $fdata['document']           = array();
        $fdata['document']['field1'] = $dto->getDoctype();
        $fdata['document']['field2'] = $dto->getDocnumber();

        $contact             = $this->getJson()->decode($dto->getContactdata(), true);
        $professionalcontact = $this->getUtildata()->getVaueOfArray($contact, 'professional');
        
        //professional
        $fdata['professionalpostcode']          = $this->getUtildata()->getVaueOfArray($professionalcontact, 'postcode');
        $fdata['professionaladdress']           = $this->getUtildata()->getVaueOfArray($professionalcontact, 'address');
        $fdata['professionaladdressnumber']     = $this->getUtildata()->getVaueOfArray($professionalcontact, 'addressnumber');
        $fdata['professionaladdresscomplement'] = $this->getUtildata()->getVaueOfArray($professionalcontact, 'addresscomplement');
        $fdata['professionalneighborhood']      = $this->getUtildata()->getVaueOfArray($professionalcontact, 'neighborhood');
        $fdata['professionalcity']              = $this->getUtildata()->getVaueOfArray($professionalcontact, 'city');
        $fdata['professionalstateid']           = $this->getUtildata()->getVaueOfArray($professionalcontact, 'stateid');
        $fdata['professionalcountry']           = $this->getUtildata()->getVaueOfArray($professionalcontact, 'country');
        $fdata['professionalphone']             = $this->getUtildata()->getVaueOfArray($professionalcontact, 'phone');
        $fdata['professionalphonemobile']       = $this->getUtildata()->getVaueOfArray($professionalcontact, 'phonemobile');
        $fdata['professionalemail']             = $this->getUtildata()->getVaueOfArray($professionalcontact, 'email');


       

        return $fdata;
        
    }
    
    public function getDefaultForm($enterpriseid, $fdata)
    {
        $dto = null;
        if (!empty($enterpriseid)) {
            $data = $this->getContainer()->get('badiu.admin.enterprise.enterprise.data');
            $dto  = $data->findById($enterpriseid);
        }
        
        if (empty($dto)) {
            $dto = $this->getContainer()->get('badiu.admin.enterprise.enterprise.entity');
            $dto = $this->initDefaultEntityData($dto);
        }
        
        if (isset($fdata['name'])) {
            $dto->setName($fdata['name']);
        }
        if (isset($fdata['shortname'])) {
            $dto->setShortname($fdata['shortname']);
        }
        
        if (isset($fdata['statusid'])) {
            $dto->setStatusid($fdata['statusid']);
        }
        
        if (isset($fdata['idnumber'])) {
            $dto->setIdnumber($fdata['idnumber']);
        }
        if (isset($fdata['param'])) {
            $dto->setParam($fdata['param']);
        }
        if (isset($fdata['description'])) {
            $dto->setDescription($fdata['description']);
        }
        if (isset($fdata['email'])) {
            $dto->setEmail($fdata['email']);
        }
        if (isset($fdata['contactperson'])) {
            $dto->setContactperson($fdata['contactperson']);
        }
        if (isset($fdata['site'])) {
            $dto->setSite($fdata['site']);
        }
        
        $doctype   = null;
        $docnumber = null;
        if (isset($fdata['document']['field1'])) {
            $doctype = $fdata['document']['field1'];
        }
        if (isset($fdata['document']['field2'])) {
            $docnumber = $fdata['document']['field2'];
        }
        
        if ($doctype == 'CNPJ') {
            $docnumber = preg_replace('/[^0-9]/', '', $docnumber);
            
        }
        $dto->setDocnumber($docnumber);
        $dto->setDoctype($doctype);


        $professionalcontact                      = array();
        $professionalcontact['postcode']          = $this->getUtildata()->getVaueOfArray($fdata, 'professionalpostcode');
        $professionalcontact['address']           = $this->getUtildata()->getVaueOfArray($fdata, 'professionaladdress');
        $professionalcontact['addressnumber']     = $this->getUtildata()->getVaueOfArray($fdata, 'professionaladdressnumber');
        $professionalcontact['addresscomplement'] = $this->getUtildata()->getVaueOfArray($fdata, 'professionaladdresscomplement');
        $professionalcontact['neighborhood']      = $this->getUtildata()->getVaueOfArray($fdata, 'professionalneighborhood');
        $professionalcontact['city']             = $this->getUtildata()->getVaueOfArray($fdata, 'professionalcity');       
         $professionalcontact['stateid']             = $this->getUtildata()->getVaueOfArray($fdata, 'professionalstateid');
        $professionalcontact['state']             = "";
        $professionalcontact['stateshortname']    = ""; 
        if (!empty($professionalcontact['stateid'])) {
                $stateinfo                         = $this->getContainer()->get('badiu.util.address.state.data')->getInfoById($professionalcontact['stateid']);
                $professionalcontact['state']          = $this->getUtildata()->getVaueOfArray($stateinfo, 'name');
                $professionalcontact['stateshortname'] = $this->getUtildata()->getVaueOfArray($stateinfo, 'shortname');
            }

        $professionalcontact['country']           = "BR";
        $professionalcontact['phone']             = $this->getUtildata()->getVaueOfArray($fdata, 'professionalphone');
        $professionalcontact['phonemobile']       = $this->getUtildata()->getVaueOfArray($fdata, 'professionalphonemobile');
        $professionalcontact['email']             = $this->getUtildata()->getVaueOfArray($fdata, 'professionalemail');       
       

        $contact                 = array();
       
        $contact['professional'] = $professionalcontact;
        $contactdatajsont        = $this->getJson()->encode($contact);

        $dto->setContactdata($contactdatajsont);

        //  echo "<pre>";        
        // print_r($contactdatajsont);
        // echo "</pre>";exit();

        return $dto;
        
    }
    
}