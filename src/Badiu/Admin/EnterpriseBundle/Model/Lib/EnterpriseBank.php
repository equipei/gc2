<?php

namespace Badiu\Admin\EnterpriseBundle\Model\Lib;

use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EnterpriseBank extends BadiuModelLib
{

    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    public function getBanckDataDb($moduleinstace)
    {
        $data             = array();
        $bdata            = $this->getContainer()->get('badiu.financ.banck.data.data');
        $dto              = $bdata->findById($userid);
        $data['bankid']     = $dto->getBankid();
        $data['dtype']    = $dto->getDtype();
        $data['agence']   = $dto->getAgence();
        $data['account'] = $dto->getAccount();
        $data['agencevdigit'] = $dto->getAgenceVdigit();
        $data['accountvdigit'] = $dto->getAccountVdigit();       
        $data['description'] = $dto->getDescription();
        $data['shortname'] = $dto->getShortname();
        $data['idnumber'] = $dto->getIdnumber();
        $data['param'] = $dto->getParam();
        
        return $data;

    }

    public function getBanckDataForm($data, $operation = 'add')
    {
        $dto = $this->getContainer()->get('badiu.financ.bank.data.entity');
        $dto = $this->initDefaultEntityData($dto);

        $bankdto = null;
        $bankid  = null;

        if (isset($data['bankid'])) {$bankid = $data['bankid'];}
        if (isset($data['dtype'])) {$dto->setDtype($data['dtype']);}
        if (isset($data['agence'])) {$dto->setAgence($data['agence']);}
        if (isset($data['account'])) {$dto->setAccount($data['account']);}         
        if (isset($data['agencevdigit'])) {$dto->setAgenceVdigit($data['agencevdigit']);}
        if (isset($data['accountvdigit'])) {$dto->setAccountvdigit($data['accountvdigit']);} 
        if (isset($data['description'])) {$dto->setDescription($data['description']);}
        if (isset($data['shortname'])) {$dto->setShortname($data['shortname']);}
        if (isset($data['idnumber'])) {$dto->setIdnumber($data['idnumber']);}
        if (isset($data['param'])) {$dto->setParam($data['param']);}




        $dto->setModulekey('badiu.admin.enterprise.enterprise');
        $enterpriseid = $this->getContainer()->get('request')->get('parentid');
        $dto->setModuleinstance($enterpriseid);

        //backid
        $bankdata = $this->getContainer()->get('badiu.financ.bank.bank.data');
		if(!empty($bankid)){
			$bankdto  = $bankdata->findById($bankid);
		    $dto->setBankid($bankdto);
		}
        
        return $dto;

    }
}
