<?php

namespace Badiu\Admin\EnterpriseBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class EnterpriseMembersAttendanceLockAccess    extends BadiuModelLib{
   
//http://d1.badiu21.com.br/badiunet/web/app_dev.php/system/service/process?key=&_token=13|9gKCMkYtcdbRfJPMQ5JxVbMmQZIgk1|2|mreport|20&_externalclinet=1&_service=badiu.admin.proxy.check.service&_function=sservice&url=&uri=/user/profile.php?id=4&userid=4&context=system&shortnamerole=editingteacher,student&isloggedin=1&time=1506639400&badiuproxyprofiledayofweek=2-5&badiuproxyprofilehour=19-20,20:10-21:12
  
    function __construct(Container $container) {
                parent::__construct($container);
         }
         
        
     public function run($proxyparam,$clientparam) {
          
       // return "123";// $proxyparam;   
         /*print_r($proxyparam);
         echo "<hr>";
         print_r($clientparam);exit;  */
         $unlock=0;
         $lock=1;
       
         $unlock=0;
         $lock=1;
         $proxyrole=$this->getUtildata()->getVaueOfArray($proxyparam,'drole');
         $proxyrole=$this->getJson()->decode($proxyrole, true);

         $proxyroleexception=$this->getUtildata()->getVaueOfArray($proxyparam,'droleexception');
         $proxyroleexception=$this->getJson()->decode($proxyroleexception, true);
               
          $globaroles=$this->getContainer()->get('badiu.admin.proxy.lib.role.globalroles');
        
        
         $isloggedin= $globaroles->isLoggedin($proxyrole,$clientparam);
        if(!$isloggedin){return $unlock;}
       
         $hasuriexception=$globaroles->hasUriexception($proxyroleexception,$clientparam);
         if($hasuriexception){return $unlock;}
        
         $hasroleexception=$globaroles->hasRoleexception($proxyroleexception,$clientparam);
         if($hasroleexception){return $unlock;}
       

         
         $hasuri=$globaroles->hasUri($proxyrole,$clientparam);
         // if(!$hasuri){return $unlock;}
       
         
         $hasrole=$globaroles->hasRole($proxyrole,$clientparam);
         if(!$hasrole){return $unlock;}

		 $hasexpection=$this->listException($clientparam);
		 if($hasexpection){return $unlock;}
		  
         $lockattendancetime=$this->lockAttendanceTime($clientparam);
         if($lockattendancetime){return $lock;}
         return  $unlock;
          
     }
    
     public function  lockAttendanceTime($clientparam){
          $unlock=0;

          $this->setSessionhashkey($this->getSessionhashkey());
          $badiuSession = $this->getContainer()->get('badiu.system.access.session');
          $badiuSession->setHashkey($this->getSessionhashkey());
          $userid=$badiuSession->get()->getUser()->getId();
          $attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
          $param=array('modulekey'=>'badiu.admin.enterprise.members','time'=>new \DateTime(),'userid'=>$userid,'entity'=>$this->getEntity(true));
          $reslt=$attendancedata->isTimeAvailable($param);
          if(!$reslt){$unlock=1;}
          return  $unlock;
     }
	 
	   public function  listException($clientparam){
		   $unlock=0;
		   $userid=$this->getUtildata()->getVaueOfArray($clientparam, 'userid');
		  
		   $this->setSessionhashkey($this->getSessionhashkey());
           $badiuSession = $this->getContainer()->get('badiu.system.access.session');
           $badiuSession->setHashkey($this->getSessionhashkey());
           $listusers=$badiuSession->getValue('badiu.admin.attendance.attendance.unlockaccess.moodle.users');
		  
		   $listids=array();
		   $pos=stripos($listusers, ",");
            if($pos=== false){
                  $listids=array($listusers);
            }else{
                $listids= explode(",", $listusers);
            }
			$exist=in_array($userid, $listids);
			
			
			if($exist){$unlock=1;}
		   return  $unlock;
	   }
}
