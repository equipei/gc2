<?php

namespace Badiu\Admin\EnterpriseBundle\Model\Lib;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class EnterpriseMembersAttendance extends BadiuModelLib{
    
    function __construct(Container $container) {
            parent::__construct($container);
              }
    
	//http://54.156.96.187/badiunet/web/app_dev.php/system/service/process?_service=badiu.admin.enterprise.members.lib.attendance&_function=moodleLogFillAttendance&atlastday=7&serviceid=4&review=1
	public function moodleLogFillAttendance($param=array()){
		$atlastday=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('atlastday');
		$lesshour=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('lesshour');
		$serviceid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('serviceid');
		$username=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('username');
		$review=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('review');
		$entity=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('entity');
		
		if(empty($atlastday)){$atlastday=$this->getUtildata()->getVaueOfArray($param,'atlastday');}
		if(empty($serviceid)){$serviceid=$this->getUtildata()->getVaueOfArray($param,'serviceid');}
		if(empty($atlastday)){$atlastday=1;}
		if(empty($lesshour)){$lesshour=0;}
		if(empty($serviceid)){return null;}
		if(empty($entity)){$entity=$this->getEntity();}
		//get list attendance by
		
		$attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
		
		$date1=new \DateTime();
		$date1->modify("-$atlastday day");
		$date2=new \DateTime();
		if(!empty($lesshour)){$date2->modify("-$lesshour hour");} 
		$webservice=$this->getContainer()->get('badiu.system.core.lib.webservice');
		$fparam=array('serviceid'=>$serviceid,'modulekey'=>'badiu.admin.enterprise.members','timestart'=>$date1,'timeend'=>$date2,'criteria'=>'automatic_lmssystemaccessintimeplanned','entity'=>$entity,'review'=>$review,'username'=>$username);
		$atlist=$this->getListAttendance($fparam);
		 
		$present=$this->getContainer()->get('badiu.admin.attendance.status.data')->getIdByShortname($entity,'present'); 
		$absent=$this->getContainer()->get('badiu.admin.attendance.status.data')->getIdByShortname($entity,'absent'); 
		$attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
		$presult = array('datashouldexec' => sizeof($atlist), 'dataexec' => 0, 'message' => '');
		
		foreach ($atlist as $row) {
			$attendanceid=$this->getUtildata()->getVaueOfArray($row,'id');
			$username=$this->getUtildata()->getVaueOfArray($row,'username');
			$userid=$this->getUtildata()->getVaueOfArray($row,'userid');
			$timestart=$this->getUtildata()->getVaueOfArray($row,'timestart');
			$timeend=$this->getUtildata()->getVaueOfArray($row,'timeend');
			$dhour=$this->getUtildata()->getVaueOfArray($row,'dhour');
			$mdluserid=$this->getUtildata()->getVaueOfArray($row,'mdluserid');
			$mdltimestart=0;
			$mdltimeend=0;
			if (is_a($timestart, 'DateTime')) {$mdltimestart=$timestart->getTimestamp();/*$mdltimestart+=3600;*/}
			if (is_a($timeend, 'DateTime')) {$mdltimeend=$timeend->getTimestamp();/*$mdltimeend+=3600;*/}
			$username="'".$username."'";
			//$sql="SELECT COUNT(l.id) AS countrecord FROM {_pfx}logstore_standard_log l INNER JOIN {_pfx}user u ON u.id = l.userid  WHERE u.username=$username AND l.timecreated >= $mdltimestart AND l.timecreated <= $mdltimeend ";
			$sql="SELECT COUNT(l.id) AS countrecord FROM {_pfx}logstore_standard_log l   WHERE l.userid=$mdluserid AND l.timecreated >= $mdltimestart AND l.timecreated <= $mdltimeend ";
			
			/*$webservice->setServiceid($serviceid);
			$result=$webservice->searchSingle($sql);
			$mdlcoutaccess=$this->getUtildata()->getVaueOfArray($result,'countrecord');*/
			
			$psql=array('_serviceid'=>$serviceid,'_datasource'=>'servicesql');
			$search =$this->getContainer()->get('badiu.system.core.functionality.search');
			$search->initDatasourceService($psql);
		 
		
			$x=0;
			$y=0;
			$result=$search->connSqlService($sql,'S',$x,$y);
			//$result=$webservice->search($sql,0,$maxrows);
			//print_r($result);exit;
			$result=$result['message']; 
			$mdlcoutaccess=$this->getUtildata()->getVaueOfArray($result,'countrecord');
			$thour=0;
			$statusid=$absent;
			if($mdlcoutaccess >=8 ){$thour=$dhour;$statusid=$present;}
			$tfparam=array('id'=>$attendanceid,'statusinfo'=>'executed','thour'=>$thour,'statusid'=>$statusid,'timemodified'=>new \DateTime());
			$ru=$attendancedata->updateNativeSql($tfparam,false); 
			if(!empty($ru)){$presult['dataexec']++;}
			//echo $sql; 
			/*echo "<hr/>";
			print_r($row);
			echo "=================================";
			print_r($result);
			
			echo "<hr/>-----------------------------------";*/
			
		}
		return $presult;
		//loop attendance
		
		// attendance status
		
	}
	
	private function getListAttendance($param) {
		
			$attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
			$modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
			
			$timestart=$this->getUtildata()->getVaueOfArray($param,'timestart');
			$timeend=$this->getUtildata()->getVaueOfArray($param,'timeend');
			$criteria=$this->getUtildata()->getVaueOfArray($param,'criteria');
			$entity=$this->getUtildata()->getVaueOfArray($param,'entity');
			$review=$this->getUtildata()->getVaueOfArray($param,'review');
			$username=$this->getUtildata()->getVaueOfArray($param,'username');
			$serviceid=$this->getUtildata()->getVaueOfArray($param,'serviceid');
			$wsql="";
			$statusinfo=" AND o.statusinfo=:statusinfo ";
			if($review==1){$statusinfo="";}
			
			$wsql.=$statusinfo;
			if(!empty($username)){$wsql.=" AND u.username=:username ";}
			
            $sql="SELECT  o.id,o.timestart,o.timeend,o.dhour,u.id AS userid,u.email,u.username,ssu.userid AS mdluserid FROM ".$attendancedata->getBundleEntity()." o JOIN o.userid u INNER JOIN BadiuAdminServerBundle:AdminServerSyncUser ssu WITH ssu.sysuserid=o.userid WHERE o.entity = :entity AND ssu.entity=o.entity AND ssu.serviceid=:serviceid AND o.modulekey = :modulekey  AND o.timestart >= :timestart AND o.timeend <= :timeend AND o.criteria=:criteria $wsql AND o.deleted=:deleted ";
			$query = $attendancedata->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('serviceid',$serviceid);
			$query->setParameter('modulekey',$modulekey);
			if(!$review){$query->setParameter('statusinfo','planned');}
			if(!empty($username)){$query->setParameter('username',$username);}
			$query->setParameter('timestart',$timestart);
			$query->setParameter('timeend',$timeend);
			$query->setParameter('criteria',$criteria);
			$query->setParameter('deleted',false);
		    $result= $query->getResult();
			return $result; 
   }
	//http://54.156.96.187/badiunet/web/app_dev.php/system/service/process?_service=badiu.admin.enterprise.members.lib.attendance&_function=addAttendanceByPlanned&timestart=now
	public function addAttendanceByPlanned(){
		echo "travado";exit;
		//get members planned
		$membersattdata=$this->getContainer()->get('badiu.admin.enterprise.membersattendanceplanned.data');
		$list=$membersattdata->getGlobalColumnsValues('o.id,o.moduleinstance,o.dayofweek,o.hourstart,o.hourend,o.timestart,o.timeend,o.criteria,o.calendar',array('entity'=>$entity,'modulekey'=>'badiu.admin.enterprise.members','param'=>100));
		//echo sizeof($list);exit;
		$membersdata=$this->getContainer()->get('badiu.admin.enterprise.members.data');
		$plannedautomaticfill=$this->getContainer()->get('badiu.admin.attendance.planned.lib.automaticfill');
		$ptimestart=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('timestart');
		foreach ($list as $row) {
			 $moduleinstance=$this->getUtildata()->getVaueOfArray($row, 'moduleinstance');
			 $mdinfo=$membersdata->getInfoById($entity,$moduleinstance);
			 $row['userid']=$this->getUtildata()->getVaueOfArray($mdinfo, 'userid');
			 $row['targettype']='oneuser';
			 $row['modulekey']='badiu.admin.enterprise.members';
			 $otimestart=$this->getUtildata()->getVaueOfArray($row, 'timestart');
			 if(empty($otimestart)){
				 $row['timestart']=$this->getUtildata()->getVaueOfArray($mdinfo, 'timestart');
				 $row['timeend']=$this->getUtildata()->getVaueOfArray($mdinfo, 'timeend');
			 }
			if($ptimestart=='now') {$row['timestart']=new \DateTime();}
			
			//dayofweek
				$dayofweek=$this->getUtildata()->getVaueOfArray($row, 'dayofweek');
				$ldw=array();
				$pos=stripos($dayofweek, ",");
				if($pos=== false){
					$ldw=array($dayofweek);
				}else{
					$ldw= explode(",", $dayofweek);
				}
			    $row['weekday']=$ldw;
			    $row['dhour']=26280000; //five year
				$plannedautomaticfill->setParam($row);
				$result= $plannedautomaticfill->exec();
				print_r($result);
				echo "<hr />";
		 }
		
		
	}
	//http://54.156.96.187/badiunet/web/app_dev.php/system/service/process?_service=badiu.admin.enterprise.members.lib.attendance&_function=addPlanned
	public function addPlanned(){
		echo "travado";exit;
		//get members list
		$membersdata=$this->getContainer()->get('badiu.admin.enterprise.members.data');
		$list=$membersdata->getGlobalColumnsValues('o.id,o.description',array('entity'=>$entity));//,'param'=>100
		 $calendartimeutil=$this->getContainer()->get('badiu.system.core.lib.date.calendartimeutil');
		$attendanceplanneddata=$this->getContainer()->get('badiu.admin.enterprise.membersattendanceplanned.data');
		
		 foreach ($list as $row) {
			    $id=$this->getUtildata()->getVaueOfArray($row, 'id');
				$description=$this->getUtildata()->getVaueOfArray($row, 'description');
				if(!empty($description)){
					$description=strip_tags($description);
					$p=explode("|",$description);
					$days=$this->getUtildata()->getVaueOfArray($p,0);
					$listdays=$calendartimeutil->castTimeSequence($days);
				
					$hours=$this->getUtildata()->getVaueOfArray($p,1);
					$h=$this->getHour($hours);
					$hourstart=$h->start->h*60+$h->start->m;
					$hourend=$h->endd->h*60+$h->endd->m;
				
					$dayofweek=implode(",",$listdays);
					//,dayofweek,hourstart,hourend,criteria,calendar,hroomid,modulekey
					$param=array();
					$param['entity']=$entity;
					$param['moduleinstance']=$id;
					$param['modulekey']='badiu.admin.enterprise.members';
					$param['dayofweek']=$dayofweek;
					$param['hourstart']=$hourstart;
					$param['hourend']=$hourend;
					$param['criteria']='automatic_lmssystemaccessintimeplanned';
					$param['hourduration']=$hourend-$hourstart;
					$param['timecreated']=new \DateTime();
				
					$result=$attendanceplanneddata->insertNativeSql($param,false);
				
					echo "$id / $description / $result <hr />";
				}else {echo "$id /sem config<hr />";}
			
			}
	} 

	public function getHour($txthour){
		
		if(empty($txthour)){return $result;}
		$h=new \stdClass();
		$h->start=new \stdClass();
		$h->start->h=0;
		$h->start->m=0;
		
		$h->endd=new \stdClass();
		$h->endd->h=0;
		$h->endd->m=0;
		
		$pos=stripos($txthour, "-");
		if($pos=== false){
			$pos1=stripos($txthour, ":");
			if($pos1=== false){

				$h->start=$txthour;
				$h->endd->h=$txthour+1;
			}else{
				$p1=explode(":",$txthour);
				$h->start->h=$this->getUtildata()->getVaueOfArray($p1,0);
				$h->start->m=$this->getUtildata()->getVaueOfArray($p1,1);
				
				$h->endd->h=$h->start->h+1;
			}
			
		}else{
			$p=explode("-",$txthour);
			$h1=$this->getUtildata()->getVaueOfArray($p,0);
			$h2=$this->getUtildata()->getVaueOfArray($p,1);
			
			//start
			$pos1=stripos($h1, ":");
			if($pos1=== false){
				$h->start->h=$h1;
				$h->start->m=0;
			}else{
				$p1=explode(":",$h1);
				$h->start->h=$this->getUtildata()->getVaueOfArray($p1,0);
				$h->start->m=$this->getUtildata()->getVaueOfArray($p1,1);
			}
			
			//end
			$pos1=stripos($h2, ":");
			if($pos1=== false){
				$h->endd->h=$h2;
				$h->endd->m=0;
			}else{
				$p1=explode(":",$h2);
				$h->endd->h=$this->getUtildata()->getVaueOfArray($p1,0);
				$h->endd->m=$this->getUtildata()->getVaueOfArray($p1,1);
			}
			
		}
		return $h;
	}
	
          
		
}
