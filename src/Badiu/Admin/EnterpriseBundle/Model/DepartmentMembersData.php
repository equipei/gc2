<?php

namespace Badiu\Admin\EnterpriseBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;

class DepartmentMembersData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }

	public function existMember($entity,$userid,$departmentid) {
		$r=FALSE;
            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.userid=:userid AND o.departmentid=:departmentid";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('userid',$userid);
			$query->setParameter('departmentid',$departmentid);
            $result= $query->getSingleResult();
             if($result['countrecord']>0){$r=TRUE;}
             return $r;
   }

}
 