<?php

namespace Badiu\Admin\FormBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminFormAttempt
 *  
 * @ORM\Table(name="admin_form_attempt",
 *      indexes={@ORM\Index(name="admin_form_attempt_entity_ix", columns={"entity"}),
*              @ORM\Index(name="admin_form_attempt_formid_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_form_attempt_formid_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="admin_form_attempt_formid_ix", columns={"formid"}),
 *              @ORM\Index(name="admin_form_attempt_timestart_ix", columns={"timestart"}),
 *              @ORM\Index(name="admin_form_attempt_timeend_ix", columns={"timeend"}),
 *              @ORM\Index(name="admin_form_attempt_status_ix", columns={"status"}),
  *              @ORM\Index(name="admin_form_attempt_deleted_ix", columns={"deleted"}),
  *              @ORM\Index(name="admin_form_attempt_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminFormAttempt
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

 /**
     * @var AdminForm
     *
     * @ORM\ManyToOne(targetEntity="AdminForm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="formid", referencedColumnName="id", nullable=false)
     * })
     */
    private $formid;
	
		 /**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=false)
     */
    private $status;  //started | inprogress | completed
	
    
	/**
     * @var float
     *
     * @ORM\Column(name="grade", type="float", precision=10, scale=0, nullable=true)
     */
    private $grade; 
	
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=false)
     */
    private $timeend;
    	
	
    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
   
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }


   
    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

    
    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    
    function getTimeend() {
        return $this->timeend;
    }

    function getDconfig() {
        return $this->dconfig;
    }

   

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

  
	 /**
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param string $dstatus
     */
    public function setStatus($dstatus) {
        $this->status = $status;
    }
	
		
	/**
     * @return string
     */
    public function getFormid() {
        return $this->formid;
    }

    /**
     * @param string $formid
     */
    public function setFormid($formid) {
        $this->formid = $formid;
    }
	
	function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
		function setGrade($grade) {
        $this->grade = $grade;
    }

 function getgGrade() {
        return $this->grade;
    }
}
