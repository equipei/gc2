<?php

namespace Badiu\Admin\FormBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminFormItems
 *  
 * @ORM\Table(name="admin_form_items", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_form_items_uix", columns={"entity", "formid", "fieldid"})},
 *      indexes={@ORM\Index(name="admin_form_items_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_form_items_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_form_items_fieldid_ix", columns={"fieldid"}),
 *              @ORM\Index(name="admin_form_items_formid_ix", columns={"formid"}),
 *              @ORM\Index(name="admin_form_items_categoryid_ix", columns={"categoryid"}),
*               @ORM\Index(name="admin_form_items_sortorder_ix", columns={"sortorder"}),
 *              @ORM\Index(name="admin_form_items_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_form_items_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminFormItems
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

 /**
     * @var AdminForm
     *
     * @ORM\ManyToOne(targetEntity="AdminForm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="formid", referencedColumnName="id", nullable=false)
     * })
     */
    private $formid;
	
		/**
     * @var AdminFormField
     *
     * @ORM\ManyToOne(targetEntity="AdminFormField")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fieldid", referencedColumnName="id", nullable=false)
     * })
     */
    private $fieldid;
	/**
     * @var AdminFormItemsCategory
     *
     * @ORM\ManyToOne(targetEntity="AdminFormItemsCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id", nullable=true)
     * })
     */
    private $categoryid; 
    
        /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="float", precision=10, scale=0, nullable=true)
     */
    private $sortorder;
	 /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype; 

     /**
     * @var integer
     *
     * @ORM\Column(name="drequired", type="integer", nullable=true)
     */
    private $drequired;

    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
   
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }


    function getCategoryid() {
        return $this->categoryid;
    }

    
    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

  
    function setCategoryid($categoryid) {
        $this->categoryid = $categoryid;
    }

    function setDhour($dhour) {
        $this->dhour = $dhour;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

   
    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

  

   

    function getDconfig() {
        return $this->dconfig;
    }

   

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    

  /**
     * @return string
     */
    public function getDtype() {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype) {
        $this->dtype = $dtype;
    }

  
 /**
     * @return string
     */
    public function getFormid() {
        return $this->formid;
    }

    /**
     * @param string $formid
     */
    public function setFormid($formid) {
        $this->formid = $formid;
    }
	
	 /**
     * @return string
     */
    public function getFieldid() {
        return $this->fieldid;
    }

    /**
     * @param string $fieldid
     */
    public function setFieldid($fieldid) {
        $this->fieldid = $fieldid;
    }
    public function getSortorder() {
        return $this->sortorder;
    }

    public function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }

    public function getDrequired() {
        return $this->drequired;
    }

    public function setDrequired($drequired) {
        $this->drequired = $drequired;
    }

}
