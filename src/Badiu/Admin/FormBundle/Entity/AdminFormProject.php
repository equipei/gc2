<?php

namespace Badiu\Admin\FormBundle\Entity;
use Badiu\System\ComponentBundle\Model\Form\Cast\BadiuHourFormCast;
use Doctrine\ORM\Mapping as ORM;

/**
 * AdminFormProject
 *  
 * @ORM\Table(name="admin_form_project", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_form_project_moduele_uix", columns={"entity", "modulekey", "moduleinstance"}),
 *      @ORM\UniqueConstraint(name="admin_form_project_idnumber_uix", columns={"entity", "idnumber"})},
 *      indexes={@ORM\Index(name="admin_form_project_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_form_project_timestart_ix", columns={"timestart"}),
 *              @ORM\Index(name="admin_form_project_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_form_project_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="admin_form_project_timeend_ix", columns={"timeend"}),
 *              @ORM\Index(name="admin_form_project_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_form_project_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminFormProject  {
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    
     /**
     * @var AdminFormProjectStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminFormProjectStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;

  	
/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string",length=255, nullable=true)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255,nullable=true)
	 */
	private $shortname;
    
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
	/**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=false)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=false)
	 */
	private $moduleinstance;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;

    
         /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

   

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

   

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }


    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getStatusid() {
        return $this->statusid;
    }

    function setStatusid(AdminFormProjectStatus $statusid) {
        $this->statusid = $statusid;
    }

   
    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }
    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

		
	 function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
}
