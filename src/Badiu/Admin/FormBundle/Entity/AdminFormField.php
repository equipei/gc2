<?php

namespace Badiu\Admin\FormBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminFormField
 *  
 * @ORM\Table(name="admin_form_field", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_form_field_shortname_uix", columns={"entity","shortname"}),
 *      @ORM\UniqueConstraint(name="admin_form_field_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="admin_form_field_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="admin_form_field_statusid_ix", columns={"statusid"}), 
  *              @ORM\Index(name="admin_form_field_name_ix", columns={"name"}), 
 *              @ORM\Index(name="admin_form_field_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_form_field_moduleinstance_ix", columns={"moduleinstance"}), 
 *              @ORM\Index(name="admin_form_field_marker_ix", columns={"marker"}),
*              @ORM\Index(name="admin_form_field_categoryid_ix", columns={"categoryid"}), 
 *              @ORM\Index(name="admin_form_field_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_form_field_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="admin_form_field_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminFormField
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;
	
	  /**
     * @var AdminFormFieldCategory
     *
     * @ORM\ManyToOne(targetEntity="AdminFormFieldCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryid", referencedColumnName="id")
     * })
     */
    private $categoryid;
	
	     /**
     * @var AdminFormFieldStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminFormFieldStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id", nullable=false)
     * })
     */
    private $statusid;
	
			 /**
     * @var string
     *
     * @ORM\Column(name="statusinfo", type="string", length=50, nullable=true)
     */
    private $statusinfo;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

	
	 /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype; //text | textarea | select | radio | checkbox | multiselect | autocomplete | date 
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="textlabel", type="string", length=255, nullable=true)
     */
    private $textlabel;
	
	/**
     * @var string
     *
     * @ORM\Column(name="defaultvalue", type="string", length=255, nullable=true)
     */
    private $defaultvalue;
	
	/**
     * @var string
     *
     * @ORM\Column(name="dservice", type="string", length=255, nullable=true)
     */
    private $dservice;
	
    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;
  

/**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="feedback", type="text", nullable=true)
     */
    private $feedback;

       /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="bigint", nullable=true)
     */
    private $sortorder;

    /**
     * @var float
     *
     * @ORM\Column(name="penalty", type="float", precision=10, scale=0, nullable=true)
     */
    private $penalty; 
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
	
		      /**
     * @var string
     *
     * @ORM\Column(name="marker", type="string", length=255, nullable=true)
     */
    private $marker;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
	
	         /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getName() {
        return $this->name;
    }

    public function getShortname() {
        return $this->shortname;
    }

    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

		
	 function getCategoryid() {
        return $this->categoryid;
    }
	
		function setCategoryid($categoryid) {
        $this->categoryid = $categoryid;
    }
	
	 /**
     * @return string
     */
    public function getDtype() {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype) {
        $this->dtype = $dtype;
    }

    function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
	 function getStatusid() {
        return $this->statusid;
    }

    function setStatusid($statusid) {
        $this->statusid = $statusid;
    }



    function getMarker() {
        return $this->marker;
    }
    function setMarker($marker) {
        $this->marker = $marker;
    }
	
	
	 /**
     * @return string
     */
    public function getStatusinfo() {
        return $this->statusinfo;
    }

 /**
     * @param string $dstatus
     */
    public function setStatusinfo($dstatusinfo) {
        $this->statusinfo = $statusinfo;
    }	
	
	function getContent() {
        return $this->content;
    }
	
	function setContent($content) {
        $this->content = $content;
    }

    
public function getSortorder() {
    return $this->sortorder;
}

public function setSortorder($sortorder) {
    $this->sortorder = $sortorder;
}

public function getFeedback() {
    return $this->feedback;
}

public function setFeedback($feedback) {
    $this->feedback = $feedback;
}

 /**
     * @return string
     */
    public function getDservice() {
        return $this->dservice;
    }

    /**
     * @param string $dservice
     */
    public function setDservice($dservice) {
        $this->dservice = $dservice;
    }
	
	
	 /**
     * @return string
     */
    public function getDefaultvalue() {
        return $this->defaultvalue;
    }

    /**
     * @param string $defaultvalue
     */
    public function setDefaultvalue($defaultvalue) {
        $this->defaultvalue = $defaultvalue;
    }
	
	function getTextlabel() {
        return $this->textlabel;
    }

    function setTextlabel($textlabel) {
        $this->textlabel = $textlabel;
    }
}

