<?php

namespace Badiu\Admin\FormBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminFormResponse
 *  
 * @ORM\Table(name="admin_form_response",
 *      indexes={@ORM\Index(name="admin_form_response_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_form_response_attemptid_ix", columns={"attemptid"}),
 *              @ORM\Index(name="admin_form_response_formitemid_ix", columns={"formitemid"}),
*              @ORM\Index(name="admin_form_response_fieldoptionsid_ix", columns={"fieldoptionsid"}), 
 *              @ORM\Index(name="admin_form_response_deleted_ix", columns={"deleted"}),
  *              @ORM\Index(name="admin_form_response_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminFormResponse
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

 /**
     * @var AdminFormAttempt
     *
     * @ORM\ManyToOne(targetEntity="AdminFormAttempt")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attemptid", referencedColumnName="id", nullable=false)
     * })
     */
    private $attemptid;
	
	
	 /**
     * @var AdminFormItems
     *
     * @ORM\ManyToOne(targetEntity="AdminFormItems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="formitemid", referencedColumnName="id", nullable=false)
     * })
     */
    private $formitemid;


	 /**
     * @var AdminFormFieldOptions
     *
     * @ORM\ManyToOne(targetEntity="AdminFormFieldOptions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fieldoptionsid", referencedColumnName="id", nullable=true)
     * })
     */ 
    private $fieldoptionsid;

   /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", length=255, nullable=true)
     */
    private $answer; 
	
	/**
     * @var float
     *
     * @ORM\Column(name="grade", type="float", precision=10, scale=0, nullable=true)
     */
    private $grade; 
	/**
     * @var string
     *
     * @ORM\Column(name="answertext", type="text", nullable=true)
     */
    private $answertext;
    	
	
    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
   
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }


    function getAttemptid() {
        return $this->attemptid;
    }

   
    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

  
    function setAttemptid($attemptid) {
        $this->attemptid = $attemptid;
    }

   
    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


    function getDconfig() {
        return $this->dconfig;
    }

   

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

  
	
   function getAnswer() {
        return $this->answer;
    }
	
	function setAnswer($answer) {
        $this->answer = $answer;
    }
	
	function getAnswertext() {
        return $this->answertext;
    }
	
	function setAnswertext($answertext) {
        $this->answertext = $answertext;
    }
	
	
	function getFormitemid() {
        return $this->formitemid;
    }
	
	function setFormitemid($formitemid) {
        $this->formitemid = $formitemid;
    }
	
		function getFieldoptionsid() {
        return $this->fieldoptionsid;
    }
	
	function setFieldoptionsid($fieldoptionsid) {
        $this->fieldoptionsid = $fieldoptionsid;
    }
	
		function setGrade($grade) {
        $this->grade = $grade;
    }

 function getgGrade() {
        return $this->grade;
    }
}
