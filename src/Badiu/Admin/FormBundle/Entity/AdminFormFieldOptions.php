<?php

namespace Badiu\Admin\FormBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminFormFieldOptions
 *  
 * @ORM\Table(name="admin_form_field_options", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_form_field_options_shortname_uix", columns={"entity","fieldid", "shortname"}),
 *      @ORM\UniqueConstraint(name="admin_form_field_options_idnumber_uix", columns={"entity", "idnumber"})},
 *       indexes={@ORM\Index(name="admin_form_field_options_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="admin_form_field_options_fieldid_ix", columns={"fieldid"}), 
 *              @ORM\Index(name="admin_form_field_options_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_form_field_options_shortname_ix", columns={"shortname"}),
 *              @ORM\Index(name="admin_form_field_options_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminFormFieldOptions
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;
	
	  /**
     * @var AdminFormField
     *
     * @ORM\ManyToOne(targetEntity="AdminFormField")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fieldid", referencedColumnName="id")
     * })
     */
    private $fieldid;

        /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
   /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

        /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255, nullable=true)
     */
    private $shortname;

    /**
     * @var string
     *
     * @ORM\Column(name="feedback", type="text", nullable=true)
     */
    private $feedback;


        /**
     * @var integer
     *
     * @ORM\Column(name="sortorder", type="bigint", nullable=true)
     */
    private $sortorder;

    /**
     * @var float
     *
     * @ORM\Column(name="fraction", type="float", precision=10, scale=0, nullable=true)
     */
    private $fraction; 

    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
	
	         /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

    
    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

   
    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

		
	 function getFieldid() {
        return $this->fieldid;
    }
	
		function setFieldid($fieldid) {
        $this->fieldid = $fieldid;
    }
	
	 

	function getContent() {
        return $this->content;
    }
	
	function setContent($content) {
        $this->content = $content;
    }

    public function getShortname() {
        return $this->shortname;
    }

    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    public function getSortorder() {
        return $this->sortorder;
    }

public function setSortorder($sortorder) {
        $this->sortorder = $sortorder;
    }
    public function getFeedback() {
        return $this->feedback;
    }

public function setFeedback($feedback) {
        $this->feedback = $feedback;
    }

    
public function getFraction() {
    return $this->fraction;
}

public function setFraction($fraction) {
    $this->fraction = $fraction;
}


public function getName() {
    return $this->name;
}

public function setName($name) {
    $this->name = $name;
}

}
