<?php

namespace Badiu\Admin\FormBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuInstall;
class Install extends BadiuInstall {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
        $result= $this->initDbStatus();
        $result+= $this->initDbFieldStatus();
        return $result;
    } 

     public function initDbStatus() {
         $cont=0;
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.form.status.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist && !$this->getForceupdate()){return 0;}
         
		 $entity=$this->getEntity();
		  $list=array(
           array('entity'=>$entity,'shortname'=>'active','name'=>$this->getTranslator()->trans('badiu.admin.form.status.active')),
           array('entity'=>$entity,'shortname'=>'inactive','name'=>$this->getTranslator()->trans('badiu.admin.form.status.inactive')),
              
        );
		
           foreach ($list as $param) {
            $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
            $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
            
            $paramedit=null;
            if($this->getForceupdate()){
                $paramedit= $param;
            }
            $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
            $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
            $r=$this->getUtildata()->getVaueOfArray($result,'id');
             if($r){$cont++;}
           }
           return $cont; 
         
     }

     public function initDbFieldStatus() {
        $cont=0;
        $entity=$this->getEntity();
        $data = $this->getContainer()->get('badiu.admin.form.fieldstatus.data');
        $exist=$data->countGlobalRow(array('entity'=>$entity));
        if($exist && !$this->getForceupdate()){return 0;}
        
        $entity=$this->getEntity();
         $list=array(
          array('entity'=>$entity,'shortname'=>'inpreparation','name'=>$this->getTranslator()->trans('badiu.admin.form.fieldstatus.inpreparation')),
          array('entity'=>$entity,'shortname'=>'inrevision','name'=>$this->getTranslator()->trans('badiu.admin.form.fieldstatus.inrevision')),
          array('entity'=>$entity,'shortname'=>'approved','name'=>$this->getTranslator()->trans('badiu.admin.form.fieldstatus.approved')),
          array('entity'=>$entity,'shortname'=>'applied','name'=>$this->getTranslator()->trans('badiu.admin.form.fieldstatus.applied')),
          array('entity'=>$entity,'shortname'=>'disapproved','name'=>$this->getTranslator()->trans('badiu.admin.form.fieldstatus.disapproved')),
          array('entity'=>$entity,'shortname'=>'canceled','name'=>$this->getTranslator()->trans('badiu.admin.form.fieldstatus.canceled')),
             
       );
       
          foreach ($list as $param) {
           $shortname=$this->getUtildata()->getVaueOfArray($param,'shortname');
           $entity=$this->getUtildata()->getVaueOfArray($param,'entity');
           
           $paramedit=null;
           if($this->getForceupdate()){
               $paramedit= $param;
           }
           $paramcheckexist=array('entity'=>$entity,'shortname'=>$shortname);
           $result=$data->addNativeSql($paramcheckexist,$param,$paramedit,true);
           $r=$this->getUtildata()->getVaueOfArray($result,'id');
            if($r){$cont++;}
          }
          return $cont; 
        
    }
  
}
