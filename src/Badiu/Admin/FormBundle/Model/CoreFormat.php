<?php
namespace Badiu\Admin\FormBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;
class CoreFormat extends BadiuFormat{
    private  $formdataoptions=null;
     function __construct(Container $container) {
            parent::__construct($container);
            $this->formataoptions=$this->getContainer()->get('badiu.admin.form.core.form.dataoptions');
       } 

              
     
    public  function dtype($data){
            $dtype=$this->getUtildata()->getVaueOfArray($data,'dtype'); 
			$dtype=$this->formataoptions->getDtypeLabel($dtype);
	     return $dtype; 
    } 
     
   
}
