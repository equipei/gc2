<?php

namespace Badiu\Admin\FormBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ItemsData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
  public function getFieldsByFormid($entity,$formid) {
        $sql="SELECT o.id AS itemid, fd.id,fd.name,fd.shortname,fd.dtype,o.drequired AS required,fd.defaultvalue,fd.dservice,fd.description,ct.name AS categoryname,ct.shortname AS categoryshortname,ct.id AS categoryid FROM ".$this->getBundleEntity()." o INNER JOIN o.fieldid fd  JOIN o.formid f   JOIN o.categoryid ct   WHERE  o.entity = :entity AND  o.formid=:formid ORDER BY o.sortorder ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('formid',$formid); 
        $result= $query->getResult();
       return $result;
  }

  public function getFieldsOptionsByFormid($entity,$formid) {
    $sql="SELECT op.id,op.name,op.shortname, fd.id AS  fieldid FROM ".$this->getBundleEntity()." o INNER JOIN o.fieldid fd  JOIN o.formid f   JOIN  BadiuAdminFormBundle:AdminFormFieldOptions op WITH op.fieldid=fd.id   WHERE  o.entity = :entity AND  o.formid=:formid ";
    $query = $this->getEm()->createQuery($sql);
    $query->setParameter('entity',$entity);
    $query->setParameter('formid',$formid);
    $result= $query->getResult();
   return $result;
}

  public function getFieldsOptionsIdByShortname($entity,$formid,$fieldid,$optionshortname) {
    $sql="SELECT op.id FROM ".$this->getBundleEntity()." o INNER JOIN o.fieldid fd  JOIN o.formid f   JOIN  BadiuAdminFormBundle:AdminFormFieldOptions op WITH op.fieldid=fd.id   WHERE  o.entity = :entity AND  o.formid=:formid AND fd.id=:fieldid AND op.shortname=:optionshortname ";
    $query = $this->getEm()->createQuery($sql);
    $query->setParameter('entity',$entity);
    $query->setParameter('formid',$formid);
	$query->setParameter('fieldid',$fieldid);
	$query->setParameter('optionshortname',$optionshortname);
    $result= $query->getOneOrNullResult();
    if(isset($result['id'])){ $result=$result['id'];}
    return $result;
}
}
