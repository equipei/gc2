<?php

namespace Badiu\Admin\FormBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class FieldData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }


    
    
    public function getForAutocomplete() {
         $name=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
         $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        
        $wsql="";
        if(!empty($name)){
            $wsql=" AND LOWER(CONCAT(o.id,o.name))  LIKE :name ";
            $name=strtolower($name);
        }
        // $sql="SELECT DISTINCT o.id,CONCAT(o.name,' / ',c.name ) AS name FROM ".$this->getBundleEntity()." o INNER JOIN o.categoryid c WHERE  o.entity = :entity  $wsql ";
		 $sql="SELECT DISTINCT o.id,o.name FROM ".$this->getBundleEntity()." o  WHERE  o.entity = :entity  $wsql ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(50);
        $result= $query->getResult();
        return  $result;
    }
  public function getNameByIdOnEditAutocomplete($id) {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT o.name,c.name AS categoryname FROM ".$this->getBundleEntity()." o LEFT JOIN o.categoryid c WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
		$name="";
		$catname="";
        if ($result!=null && array_key_exists('name',$result)){$name=$result['name'];}
		if ($result!=null && array_key_exists('categoryname',$result)){$catname=$result['categoryname'];}
		$dresult=$name;
		if(!empty($catname)){$dresult.=" / $catname";}
        return $dresult;
  }

}
