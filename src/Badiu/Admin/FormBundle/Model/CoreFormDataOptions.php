<?php
namespace Badiu\Admin\FormBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class CoreFormDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
 

 
 
public  function getDdtype(){
    $list=array();
    $list['select.one.option']=$this->getDtypeLabel('select.one.option');
    $list['select.multiple.option']=$this->getDtypeLabel('select.multiple.option');
    $list['text.single']=$this->getDtypeLabel('text.single');
    $list['text.long']=$this->getDtypeLabel('text.long');
    $list['number']=$this->getDtypeLabel('number');
    $list['date']=$this->getDtypeLabel('date');
	$list['autocomplete']=$this->getDtypeLabel('autocomplete');
    return $list;
}

public  function getDtypeLabel($type){
    $label=$this->getTranslator()->trans('badiu.admin.form.field.dtype.'.$type);
   
    return $label;
}

public  function getFormCategoriesParent(){
    $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
    $list=array();
    if(empty($parentid)){return  $list;}
   $param=array('formid'=>$parentid,'deleted'=>0);
   $list= $this->getContainer()->get('badiu.admin.form.itemscategory.data')->getGlobalColumnsValues('o.id,o.name',$param);
   
   return $list;
}
}
