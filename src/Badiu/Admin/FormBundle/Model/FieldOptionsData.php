<?php

namespace Badiu\Admin\FormBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class FieldOptionsData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }


	public function getFormChoiceByFieldshortname($entity,$fieldshortname) {
        $sql="SELECT  o.id,o.name FROM ".$this->getBundleEntity()." o  JOIN o.fieldid f WHERE o.entity = :entity AND f.shortname=:fieldshortname	ORDER by o.name ";
      
       $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('fieldshortname',$fieldshortname);
         $result= $query->getResult();
        return  $result;
    }
	public function getListByFormid($entity,$formid) {
        $sql="SELECT  o.id,o.name,o.shortname FROM ".$this->getBundleEntity()." o  JOIN o.fieldid f JOIN BadiuAdminFormBundle:AdminFormItems  fi WITH fi.fieldid =o.fieldid  WHERE o.entity = :entity AND fi.formid=:formid	ORDER by o.name ";
      
       $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
		$query->setParameter('formid',$formid);
         $result= $query->getResult();
        return  $result;
    }
	public function getForAutocomplete($fieldshortname) {
		$name=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('gsearch'); 
         $badiuSession=$this->getContainer()->get('badiu.system.access.session');
       $wsql="";
	  
        if(!empty($name)){
            $wsql=" AND LOWER(CONCAT(o.id,o.name))  LIKE :name ";
            $name=strtolower($name);
        }
         $sql="SELECT DISTINCT o.id,o.name FROM ".$this->getBundleEntity()." o JOIN o.fieldid f  WHERE  o.entity = :entity AND f.shortname=:fieldshortname  $wsql ";
       
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
		$query->setParameter('fieldshortname',$fieldshortname);
        if(!empty($name)){$query->setParameter('name','%'.$name.'%');}
        $query->setMaxResults(15);
        $result= $query->getResult();
        return  $result;
    }
   public function getNameByIdOnEditAutocomplete($id) {
        $badiuSession=$this->getContainer()->get('badiu.system.access.session');
        $sql="SELECT o.name FROM ".$this->getBundleEntity()." o  WHERE  o.entity = :entity AND  o.id=:id ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$badiuSession->get()->getEntity());
        $query->setParameter('id',$id);
        $result= $query->getOneOrNullResult();
        if ($result!=null && array_key_exists('name',$result)){$result=$result['name'];}
        return $result;
  }
}
