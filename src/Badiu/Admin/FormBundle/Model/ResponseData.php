<?php

namespace Badiu\Admin\FormBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ResponseData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
  public function getAllByAttempt($entity,$attemptid) {
        $sql="SELECT i.id AS itemid, o.answer AS fieldoptionsid FROM ".$this->getBundleEntity()." o JOIN o.formitemid i    WHERE  o.entity = :entity AND  o.attemptid=:attemptid ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('attemptid',$attemptid); 
        $result= $query->getResult();
       return $result;
  }

}
