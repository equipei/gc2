<?php

namespace Badiu\Admin\FormBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class ItemsCategoryData   extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
  public function getByFormid($entity,$formid) {
        $sql="SELECT o.id,o.name,o.shortname FROM ".$this->getBundleEntity()." o WHERE  o.entity = :entity AND  o.formid=:formid ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('formid',$formid); 
        $result= $query->getResult();
       return $result;
  }

 
}
