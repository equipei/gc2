<?php

namespace Badiu\Admin\FormBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FactoryFormSelectData extends BadiuModelLib {

	  function __construct(Container $container) {
        parent::__construct($container);
        $this->fdata=array();
    }
 
	public function getByAttempt($param) {
		$attemptid=$this->getUtildata()->getVaueOfArray($param, 'attemptid');
		$entity=$this->getEntity();
		$factorydata=$this->getContainer()->get('badiu.admin.form.lib.factorydata');
		$attemptdata=$this->getContainer()->get('badiu.admin.form.attempt.data');
		$formid=$attemptdata->getFormid($entity,$attemptid);
		
		//get itmscategory
		$itemscategorydata=$this->getContainer()->get('badiu.admin.form.itemscategory.data');
		$listcategory=$itemscategorydata->getByFormid($entity,$formid);
		
		$fieldoptionsdata=$this->getContainer()->get('badiu.admin.form.fieldoptions.data');
		$listoptions=$fieldoptionsdata->getListByFormid($entity,$formid);
		$listoptionskv=array();
		if(is_array($listoptions)){
			foreach ($listoptions as $lorow) {
				$optionsid=$this->getUtildata()->getVaueOfArray($lorow, 'id');
				$optionsname=$this->getUtildata()->getVaueOfArray($lorow, 'name');
				$listoptionskv[$optionsid]=$optionsname;
			}
		}
		
		//get data by attemptid
		$factorydata=$this->getContainer()->get('badiu.admin.form.lib.factorydata');
		$attemptresponsekv=$factorydata->getAttemptResponse($entity,$attemptid);
		
		
		//get form items by attemptid
		 $itemsdata=$this->getContainer()->get('badiu.admin.form.items.data');
		$listitems=$itemsdata->getFieldsByFormid($entity,$formid);
		
		
		$itemsbycategory=array();
		if(!is_array($listcategory)){return null;}
		foreach ($listcategory as $row) {
			$categoryid=$this->getUtildata()->getVaueOfArray($row, 'id');
			$categoryname=$this->getUtildata()->getVaueOfArray($row, 'name');
			$itemsbycategory[$categoryid]['id']=$categoryid;
			$itemsbycategory[$categoryid]['name']=$categoryname;
			$itemsbycategory[$categoryid]['items']=array();
			if(is_array($listitems)){
				foreach ($listitems as $ritem) {
					$itemid=$this->getUtildata()->getVaueOfArray($ritem, 'itemid');
					$fieldid=$this->getUtildata()->getVaueOfArray($ritem, 'id');
					$fieldname=$this->getUtildata()->getVaueOfArray($ritem, 'name');
					$fieldshortname=$this->getUtildata()->getVaueOfArray($ritem, 'shortname');
					$dservice=$this->getUtildata()->getVaueOfArray($ritem,'dservice');
					$itemdtype=$this->getUtildata()->getVaueOfArray($ritem,'dtype');
					$itemvalue=$this->getUtildata()->getVaueOfArray($attemptresponsekv,$itemid);
					//if($fieldid==)
					if($itemdtype=='select.one.option'&& !empty($itemvalue)){
						if(!empty($dservice)){
							$acparam=array('config'=>$dservice,'value'=>$itemvalue,'label'=>1);
							$itemvalue=$factorydata->getChoiceServiceValue($acparam); //review this functon and add badiu.system.core.lib.form.configchoice
							//$itemvalue=$factorydata->getAutocompleteValue($acparam); //review this functon and add badiu.system.core.lib.form.configchoice
						}
						else {$itemvalue=$this->getUtildata()->getVaueOfArray($listoptionskv,$itemvalue);}
						
					}else if($itemdtype=='autocomplete' && !empty($itemvalue)){
						$acparam=array('config'=>$dservice,'value'=>$itemvalue);
						$itemvalue=$factorydata->getAutocompleteValue($acparam); //review this functon and add badiu.system.core.lib.form.configchoice
						
					}
					$itemsbycategory[$categoryid]['items'][$fieldshortname]['id']=$fieldid;
					$itemsbycategory[$categoryid]['items'][$fieldshortname]['itemid']=$itemid;
					$itemsbycategory[$categoryid]['items'][$fieldshortname]['name']=$fieldname;
					$itemsbycategory[$categoryid]['items'][$fieldshortname]['shortname']=$fieldshortname;
					$itemsbycategory[$categoryid]['items'][$fieldshortname]['value']=$itemvalue;
				}
			}
		}
		
		
		return $itemsbycategory;
			
	}

}

