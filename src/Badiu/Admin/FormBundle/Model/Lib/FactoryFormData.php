<?php

namespace Badiu\Admin\FormBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class FactoryFormData extends BadiuModelLib {

	//http://54.156.96.187/badiunet/web/app_dev.php/system/service/process?_service=badiu.admin.form.lib.factorydata&_function=teste

	private $fadta;
     function __construct(Container $container) {
        parent::__construct($container);
        $this->fdata=array();
    }
 
	public function teste() {
		$this->make(array());
	}

 public function make($param) {
	  $formid=$this->getUtildata()->getVaueOfArray($param,'formid');
	  $attemptid=$this->getUtildata()->getVaueOfArray($param,'attemptid');
	  
	  $formshortname=$this->getUtildata()->getVaueOfArray($param,'formshortname');
	  $entity=$this->getEntity();
	  $formdata=$this->getContainer()->get('badiu.admin.form.form.data');
	  $fieldoptionsdata=$this->getContainer()->get('badiu.admin.form.fieldoptions.data');
	  if(empty($formid)){
		 $formid=$formdata->getIdByShortname($entity,$formshortname);
	  }
	  if(empty($formshortname) && !empty($formid)){$formshortname=$formdata->getShortnameById($formid);}
	 if(empty($formshortname)) {return null;}

	  $result=array();
	  $result['fields']=array();
	  $result['groups']=array();
	  if(empty($formid)){return $result;}
	   
	  $itemsdata=$this->getContainer()->get('badiu.admin.form.items.data');
	  $list=$itemsdata->getFieldsByFormid($entity,$formid);

	  if(!is_array($list)){return $fdata;}
	  $options=$itemsdata->getFieldsOptionsByFormid($entity,$formid);
	  $options=$this->getListOptions($options);
	  $attemptresponse= $this->getAttemptResponse($entity,$attemptid);
	 
	 $fdata=array();
	 $fgroup=array();
	 foreach ($list as $row) {
		$id=$this->getUtildata()->getVaueOfArray($row,'id');
		
		$name=$this->getUtildata()->getVaueOfArray($row,'name');
		$shortname=$this->getUtildata()->getVaueOfArray($row,'shortname');
		$dtype=$this->getUtildata()->getVaueOfArray($row,'dtype');
		$dservice=$this->getUtildata()->getVaueOfArray($row,'dservice');
		$defaultvalue=$this->getUtildata()->getVaueOfArray($row,'defaultvalue');
		$required=$this->getUtildata()->getVaueOfArray($row,'required');
		$categoryname=$this->getUtildata()->getVaueOfArray($row,'categoryname');
		$categoryshortname=$this->getUtildata()->getVaueOfArray($row,'categoryshortname');
		$itemid=$this->getUtildata()->getVaueOfArray($row,'itemid');
		
		$choicelistvalues=$this->getUtildata()->getVaueOfArray($options,$id);
		$value=$defaultvalue;
		if(!empty($attemptresponse)){
			$value=$this->getUtildata()->getVaueOfArray($attemptresponse,$itemid);
			if($dtype=='select.one.option' && !empty($value)){
				$optinshortname=$fieldoptionsdata->getShortnameById($value);
				if(!empty($optinshortname)){$value=$optinshortname;}
			}
			
			if($dtype=='select.multiple.option' && !empty($value)){
				$value=$this->getUtildata()->castStringToArray($value);
				
			}
		}
		
		if($dtype=='autocomplete' && !empty($value)){
			$akey=$value;
			$acparam=array('config'=>$dservice,'value'=>$akey);
			$aname=$this->getAutocompleteValue($acparam); //review this functon and add badiu.system.core.lib.form.configchoice
			$value=array('field1'=>$akey,'field2'=>$aname);
		}
		
		//add list by service
		if($dtype=='select.one.option' && !empty($dservice)){
			 $fparamfc=array('config'=>$dservice,'type'=>'choice');
			 $choicelistvalues=$this->getContainer()->get('badiu.system.core.lib.form.configchoice')->getDefaultChoiceList($fparamfc);
		}
			
		//$kname=$shortname;
		//if(empty($kname)){$kname=$name;}
		$fkname="banetsysform_".$formshortname."_".$shortname;
		$type=$this->castType($dtype);

		
		$item=array();
		$item['name']=$fkname;
		$item['label']=$name;
		$item['required']=$required;
		$item['cssClasss']=' form-control ';
		$item['type']=$type;
		$item['choicelist']=$dservice;
		$item['choicelistvalues']=$choicelistvalues;
		$item['format']=null;
		$item['value']=$value;
		$item['placeholder']=null;
		$item['config']=null;
		$item['group']=$categoryshortname;
		$item['grouplabel']=$categoryname;

		$fgroup[$categoryshortname]=$categoryname;
		array_push($fdata,$item);
	 }
	 $result['fields']=$fdata;
	 $result['groups']=$fgroup;
	/*
	
	 echo "<pre>";
	 print_r($result);
	 echo "</pre>";exit;

	  echo "<pre>";
	  print_r($list);
	  echo "</pre>";
	
	  echo "<hr />";
	  echo "<pre>";
	  print_r(  $options);
	  echo "</pre>";*/

	 /* $listsepotions=$this->getListOptions($options);

	  echo "<hr />";
	  echo "<pre>";
	  print_r( $listsepotions);
	  echo "</pre>";
	  exit;
	  //get param

	  //get fields
*/
	  

	  return $result;

	}

	public function getListOptions($list) {
		$newlist=array();
		if(!is_array($list)){return $newlist;}

		foreach ($list as $row) {
			$fieldid=$this->getUtildata()->getVaueOfArray($row,'fieldid');
			$id=$this->getUtildata()->getVaueOfArray($row,'id');
			$name=$this->getUtildata()->getVaueOfArray($row,'name');
			$shortname=$this->getUtildata()->getVaueOfArray($row,'shortname');
			if(!empty($shortname)){$id=$shortname;}
		//	$name=strip_tags($name);//review 
			$options=$this->getUtildata()->getVaueOfArray($newlist,$fieldid);
			if(!is_array($options)){$options=array();}
			$options[$id]=$name;
			$newlist[$fieldid]=$options;

		 }
		return $newlist;	 
	}

	public function getAttemptResponse($entity,$attemptid) {
			$result=array();
			if(empty($attemptid)){return $result; }
			$responsedata=$this->getContainer()->get('badiu.admin.form.response.data');	
			$list=$responsedata->getAllByAttempt($entity,$attemptid);
			foreach ($list as $row) {
				$itemid=$this->getUtildata()->getVaueOfArray($row,'itemid');
				$fieldoptionsid=$this->getUtildata()->getVaueOfArray($row,'fieldoptionsid');
				$result[$itemid]=$fieldoptionsid;
			}
			return $result;
			
	}
	public function castType($type) {
		$fatype="text";
		if($type=='text.single'){$fatype="text";}
		else if($type=='text.long'){$fatype="textarea";}
		else if($type=='date'){$fatype="date";}
		else if($type=='number'){$fatype="text";}
		else if($type=='select.one.option'){$fatype="choice";}
		else if($type=='select.multiple.option'){$fatype="checkbox";}
		else if($type=='autocomplete'){$fatype="badiuautocomplete";}
		return $fatype;
	}
	
	public function addAttempt($param,$confparam) {
		
		if(!is_array($param)){return null;}
		
		$formid=$this->getUtildata()->getVaueOfArray($confparam,'formid');
		$modulekey=$this->getUtildata()->getVaueOfArray($confparam,'key');
		
		$moduleinstance=$this->getUtildata()->getVaueOfArray($confparam,'moduleinstance');
		$entity=$this->getEntity();
		
		$fparam['entity']=$entity;
		$fparam['formid']=$formid;
		$fparam['modulekey']=$modulekey;
		$fparam['moduleinstance']=$moduleinstance;
		$fparam['status']='started';
	
		
		$attemptdata=$this->getContainer()->get('badiu.admin.form.attempt.data');	
		
		$paramcheckexist=array('entity'=>$entity,'formid'=>$formid,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance);
        $result=$attemptdata->addNativeSql($paramcheckexist,$fparam,$fparam,true);
        $attemptid=$this->getUtildata()->getVaueOfArray($result,'id');
		
		return $attemptid;
	}
	
	
	
	public function changeListItemAddId($items,$paramconfig) {
		if(!is_array($items)){return $items;}
		$formid=$this->getUtildata()->getVaueOfArray($paramconfig,'formid');
		$entity=$this->getUtildata()->getVaueOfArray($paramconfig,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		 
		
		$newitems=array();
		$itemsdata=$this->getContainer()->get('badiu.admin.form.items.data');
		$list=$itemsdata->getFieldsByFormid($entity,$formid);
		if(!is_array($list)){return $items;}
		 $keylist=array();
		 foreach ($list as $row) {
			//$itemid=$this->getUtildata()->getVaueOfArray($row,'itemid');
			$shortname=$this->getUtildata()->getVaueOfArray($row,'shortname');
			$keylist[$shortname]=$row;
		 }
		 
		  foreach ($items as $key => $value) {
			  $rowkey=$this->getUtildata()->getVaueOfArray($keylist,$key);
			  $itkey=$this->getUtildata()->getVaueOfArray($rowkey,'itemid');
			  if(!empty($itkey)){
				 $newitems[$itkey]['response']= $value;
				 $newitems[$itkey]['field']= $rowkey;
			  }
			  
		  }
		return $newitems;
	}
	public function addResponse($param,$paraminfo) {

		if(!is_array($param)){return $param;}
		$cont=0;
		$formid=$this->getUtildata()->getVaueOfArray($paraminfo,'formid');
		$attemptid=$this->getUtildata()->getVaueOfArray($paraminfo,'attemptid');
		$entity=$this->getUtildata()->getVaueOfArray($paraminfo,'entity');
		if(empty($entity)){$entity=$this->getEntity();}
		$responsedata=$this->getContainer()->get('badiu.admin.form.response.data');
		$itemsdata=$this->getContainer()->get('badiu.admin.form.items.data');		
		foreach ($param as $key => $value) {
			 $response=$this->getUtildata()->getVaueOfArray($value,'response');
			 $fieldid=$this->getUtildata()->getVaueOfArray($value,'field.id',true);
			 $dtype=$this->getUtildata()->getVaueOfArray($value,'field.dtype',true);
			 if($dtype=='select.one.option'){
				 $optionsid=$itemsdata->getFieldsOptionsIdByShortname($entity,$formid,$fieldid,$response);
				 if(!empty($optionsid)){$response=$optionsid;}
			 }
			 $fparam=array();
			 $fparam['entity']=$entity;
			  $fparam['attemptid']=$attemptid;
			  $fparam['formitemid']=$key;
			  $fparam['answer']=$response;
			 
			$paramcheckexist=array('entity'=>$entity,'attemptid'=>$attemptid,'formitemid'=>$key);
            $result=$responsedata->addNativeSql($paramcheckexist,$fparam,$fparam,true);
            $r=$this->getUtildata()->getVaueOfArray($result,'id');
			if($r){$cont++;}
		}
	 return $cont;
	}
	public function addFormControllerData($param,$confparam) {
		$newlist=array();
		if(!is_array($param)){return $param;}
		$formshortname=null;
		
		$paramvalues=array();
		$newparam=array();
		
		
		$contpf=0;
		foreach ($param as $key => $value) {
			$pos = stripos($key, "banetsysform_");
			if ($pos !== false){
				$p=explode("_", $key);
				
				if(empty($formshortname)){$formshortname=$this->getUtildata()->getVaueOfArray($p,1);}
				$itemshortname=$this->getUtildata()->getVaueOfArray($p,2);
				$paramvalues[$itemshortname]=$value;
				$contpf++;
			}else{$newparam[$key]=$value;}
		 }
		 if($contpf==0){return $param;}
		 if(empty($formshortname)){return $param;}
		
  		 $adminformattemptid=$this->getUtildata()->getVaueOfArray($param,'adminformattemptid');
		 $confparam['formshortname']=$formshortname;
		 
		 $entity=$this->getUtildata()->getVaueOfArray($confparam,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		
		 $formid=null;
		 $formdata=$this->getContainer()->get('badiu.admin.form.form.data');
	     $formid=$formdata->getIdByShortname($entity,$formshortname);
	     if(empty($formid)){return $param;}
	  
		 $confparam['formid']=$formid;
		 $confparam['id']=$this->getUtildata()->getVaueOfArray($param,'id');
		 $confparam['adminformattemptid']=$adminformattemptid;
		
	
		 $paramvalues=$this->changeListItemAddId($paramvalues,$confparam);
		
		 if(empty($adminformattemptid)){
			$adminformattemptid=$this->addAttempt($paramvalues,$confparam); 
		 }
		 $newparam['adminformattemptid']=$adminformattemptid;
		 $confparam['attemptid']=$adminformattemptid;
		 $this->addResponse($paramvalues,$confparam);
		
		return $newparam;
	}
	
	
	public function updateModuleinstanceFormControllerData($param) {
		$id=$this->getUtildata()->getVaueOfArray($param,'id');
		$adminformattemptid=$this->getUtildata()->getVaueOfArray($param,'adminformattemptid');
		
		$attemptdata=$this->getContainer()->get('badiu.admin.form.attempt.data');
		$upparam=array('id'=>$adminformattemptid,'moduleinstance'=>$id);
		$result=$attemptdata->updateNativeSql($upparam,false);
		return $param;
	}
	
	//review send to classe
	public function getAutocompleteValue($param) {
		$config=$this->getUtildata()->getVaueOfArray($param,'config');
		$value=$this->getUtildata()->getVaueOfArray($param,'value');
		if(empty($config)){return null;}
		if(empty($value)){return null;}
		$p=explode ("|",$config);
		
        $servicekey=$this->getUtildata()->getVaueOfArray($p,1);
        $function=$this->getUtildata()->getVaueOfArray($p,3);
        
		$result=null;
		if ($this->getContainer()->has($servicekey)) {
            $service=$this->getContainer()->get($servicekey);
            if (method_exists($service, $function)) {$result=$service->$function($value);}
        }
		
		return $result;
	}

//review send to classe
	public function getChoiceServiceValue($param) {
		$config=$this->getUtildata()->getVaueOfArray($param,'config');
		$value=$this->getUtildata()->getVaueOfArray($param,'value');
		$label=$this->getUtildata()->getVaueOfArray($param,'label');
		if(empty($config)){return null;}
		if(empty($value)){return null;}
		$p=explode ("|",$config);
		
        $servicekey=$this->getUtildata()->getVaueOfArray($p,1);
        $function=$this->getUtildata()->getVaueOfArray($p,2);
		$function1=$this->getUtildata()->getVaueOfArray($p,4);
        $entity=$this->getEntity();
		
		$result=null;
		if ($this->getContainer()->has($servicekey)) {
            $service=$this->getContainer()->get($servicekey);
			
            if (method_exists($service, $function)) {
				if(!$label){$result=$service->$function($value);}
				else if($label && $function1=='getNameByShortname') {$result=$service->$function1($entity,$value);}
				else if($label && $function1!='getNameByShortname') {$result=$service->$function1($value);}
				
			}
        }
		
		return $result;
	}	
public function getFdata() {
    return $this->fdata;
}

public function setFdata($fdata) {
    $this->fdata = $fdata;
}	

}

