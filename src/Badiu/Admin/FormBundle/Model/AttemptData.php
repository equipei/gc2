<?php

namespace Badiu\Admin\FormBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class AttemptData  extends BadiuDataBase {
   
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
  public function getFormid($entity,$attemptid) {
        $sql="SELECT f.id  FROM ".$this->getBundleEntity()." o JOIN o.formid f    WHERE  o.entity = :entity AND  o.id=:attemptid ";
        $query = $this->getEm()->createQuery($sql);
        $query->setParameter('entity',$entity);
        $query->setParameter('attemptid',$attemptid); 
        $result= $query->getResult();
       return $result; 
  }

}
