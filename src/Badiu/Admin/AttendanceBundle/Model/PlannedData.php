<?php

namespace Badiu\Admin\AttendanceBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuDataBase;
class PlannedData  extends BadiuDataBase {
    
    function __construct(Container $container,$bundleEntity) {
            parent::__construct($container,$bundleEntity);
              }
    
  

  public function getTimes($modulekey,$moduleinstance) {
		$r=FALSE;
            $sql="SELECT  o.id,o.timestart,o.timeend FROM ".$this->getBundleEntity()." o  WHERE o.modulekey = :modulekey AND o.moduleinstance = :moduleinstance AND o.deleted=:deleted";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('modulekey',$modulekey);
			$query->setParameter('moduleinstance',$moduleinstance);
			$query->setParameter('deleted',false);
			$result= $query->getResult();
            return $result;
   }

  
   public function getRoomById($id) {
		        $sql="SELECT  r.id,r.name FROM ".$this->getBundleEntity()." o JOIN o.hroomid r WHERE o.id = :id";
            $query = $this->getEm()->createQuery($sql);
            $query->setParameter('id',$id);
	          $result= $query->getSingleResult();
            return $result;
   }
   
     public function isTimeBusy($param) { 
			$utildata = $this->getContainer()->get('badiu.system.core.lib.util.data');
			$modulekey=$utildata->getVaueOfArray($param,'modulekey');
			$moduleinstance=$utildata->getVaueOfArray($param,'moduleinstance');
			$timestart=$utildata->getVaueOfArray($param,'timestart');
			$timeend=$utildata->getVaueOfArray($param,'timeend');
			$entity=$utildata->getVaueOfArray($param,'entity');

            $sql="SELECT  COUNT(o.id) AS countrecord FROM ".$this->getBundleEntity()." o  WHERE o.entity = :entity AND o.modulekey = :modulekey AND o.moduleinstance = :moduleinstance AND o.timestart >= :timestart AND o.timeend <= :timeend AND o.deleted=:deleted ";
			$query = $this->getEm()->createQuery($sql);
            $query->setParameter('entity',$entity);
			$query->setParameter('modulekey',$modulekey);
			$query->setParameter('moduleinstance',$moduleinstance);
			$query->setParameter('timestart',$timestart);
			$query->setParameter('timeend',$timeend);
			$query->setParameter('deleted',false);
		    $result= $query->getSingleResult();
			$result=$result['countrecord'];
		
			return $result; 
   }
}
