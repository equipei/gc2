<?php
namespace Badiu\Admin\AttendanceBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class AttendanceDefaultSqlFilter extends BadiuSqlFilter{
    /**
     * @var object
     */
   private  $sqlservice;
   

    function __construct(Container $container) {
            parent::__construct($container);
     }

        function addalluserconsolidatefields() {
			$sql="";
			$param1=array('_prefix'=>1,'_aliasuser'=>'o.userid','_column'=>'id','_operation'=>'COUNT','_columnalias'=>'countplannedsession');
			$sql.=$this->getCommandsqlUserAgrgegate($param1);
			
			$param1=array('_prefix'=>2,'_aliasuser'=>'o.userid','_column'=>'id','_operation'=>'COUNT','_columnalias'=>'countexecutedsession','_statusinfo'=>'executed');
			$sql.=$this->getCommandsqlUserAgrgegate($param1);
			
			$param1=array('_prefix'=>3,'_aliasuser'=>'o.userid','_column'=>'dhour','_operation'=>'SUM','_columnalias'=>'countplannedhour');
			$sql.=$this->getCommandsqlUserAgrgegate($param1);
			
			$param1=array('_prefix'=>4,'_aliasuser'=>'o.userid','_column'=>'thour','_operation'=>'SUM','_columnalias'=>'countexecutedhour','_statusinfo'=>'executed');
			$sql.=$this->getCommandsqlUserAgrgegate($param1);
			
            return $sql;
        }
     

	  function getCommandsqlUserAgrgegate($param) {
          $fqueryn=$this->getContainer()->get('badiu.system.core.lib.sql.factoryqueryselectnative');
          $prefix= $this->getUtildata()->getVaueOfArray($param,'_prefix');
          $aliasuser= $this->getUtildata()->getVaueOfArray($param,'_aliasuser');
		  $aliasmodulekey= $this->getUtildata()->getVaueOfArray($param,'_aliasmodulekey');
		  $aliamoduleinstance= $this->getUtildata()->getVaueOfArray($param,'_aliasmoduleinstance');
          $column= $this->getUtildata()->getVaueOfArray($param,'_column');
		  $operation= $this->getUtildata()->getVaueOfArray($param,'_operation');
		  $columnalias= $this->getUtildata()->getVaueOfArray($param,'_columnalias');
		  $statusinfo= $this->getUtildata()->getVaueOfArray($param,'_statusinfo'); 
		  
		  if(empty($aliasmodulekey)){$aliasmodulekey="o.modulekey"; }
		  if(empty($aliamoduleinstance)){$aliamoduleinstance="o.moduleinstance"; }
		  
		  $wsql="";
		  if(empty($operation)){$operation=" COUNT ";}
		  if($columnalias){$columnalias=" AS $columnalias ";}
		  
		 
		  if(!empty($statusinfo)){$wsql.=" AND  o$prefix.statusinfo ='".$statusinfo."'";}
		  
		  $wsql.=$fqueryn->period($this->getParam(),'timestart',"o$prefix.timestart",false);
          $wsql.=$fqueryn->period($this->getParam(),'timestartp',"o$prefix.timestart",false);
			
		  
          $csql=" SELECT $operation(o$prefix.$column) FROM BadiuAdminAttendanceBundle:AdminAttendance o$prefix  WHERE o$prefix.entity=o.entity  AND o$prefix.modulekey = $aliasmodulekey AND o$prefix.moduleinstance = $aliamoduleinstance AND o$prefix.userid=o.userid $wsql ";
          
		  $sql="";
		  if($columnalias){$sql=",($csql) $columnalias  "; }
		  else {$sql=" AND ($csql) > 0  ";}
		  return $sql;
	  }  
		  
          
}
