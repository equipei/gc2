<?php

namespace Badiu\Admin\AttendanceBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuLinkController;
class AttendanceLinkController extends BadiuLinkController{

    function __construct(Container $container) {
            parent::__construct($container);
          
        }
     public function changeStatus(){
            $this->init();
			
			 $id=$this->getParamItem('id');
			 $newstatusid=$this->getParamItem('newstatusid');
			$requestlib=$this->getContainer()->get('badiu.admin.attendance.attendance.lib');
			$param=array('id'=>$id,'newstatusid'=>$newstatusid);
			$result=$requestlib->changeStatus($param);
		
			
            $outrsult=array('result'=>$this->getResultout(),'message'=>$this->getSuccessmessage(),'urlgoback'=>null);
            $this->getResponse()->setStatus("accept");
            $this->getResponse()->setMessage($result);
            return $this->getResponse()->get();
		}
}