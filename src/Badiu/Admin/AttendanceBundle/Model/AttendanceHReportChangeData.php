<?php
namespace Badiu\Admin\AttendanceBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportChangeData;
class AttendanceHReportChangeData extends BadiuReportChangeData
{

    function __construct(Container $container) {
            parent::__construct($container);
              }
              

public function all($data){
	
	//list attendance
	$listattendance=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows.data.1',true); 
	$listattendance=$this->splitListAttendanceByUser($listattendance);
	
	//list users
	$listuser=$this->getUtildata()->getVaueOfArray($data,'badiu_table1_rows'); 
	$listuser=$this->addColumnPlanneds($listuser,$listattendance);
	
	$data['badiu_table1_rows']=$listuser;
	
	/*echo "<pre>";
	print_r($data);
	echo "</pre>";exit;*/
     return $data;
      
  }
 function splitListAttendanceByUser($listattendance){
	 $newlist=array();
	 if(empty($listattendance)){return $newlist;}
	 if(!is_array($listattendance)){return $newlist;}
	 
	 foreach ($listattendance as $row){
		$userid=$this->getUtildata()->getVaueOfArray($row,'userid'); 
		$daykey=$this->getUtildata()->getVaueOfArray($row,'daykey'); 
		$plannedid=$this->getUtildata()->getVaueOfArray($row,'plannedid'); 
		
		$key="_badiuadminattendanteplanned_".$daykey."_".$userid;
		$ulist=$this->getUtildata()->getVaueOfArray($newlist,$key);
		if(empty($ulist)){$ulist=array();}
		array_push($ulist,$row);
		$newlist[$key]=$ulist;
	 }
	 return $newlist;
 }
 
  function addColumnPlanneds($listusers,$listattendance){
	   $newlist=array();
	   
	 if(empty($listusers)){return $newlist;}
	 if(!is_array($listusers)){return $newlist;}
	 
	  if(empty($listattendance)){return $listusers;}
	  if(!is_array($listattendance)){return $listusers;}
	 
	 $badiuSession = $this->getContainer()->get('badiu.system.access.session');
     $badiuSession->setHashkey($this->getSessionhashkey());
     $listcolumn=$badiuSession->getValue('_addcolumndynamicallyadminattendanceplanned');	
	 
	  if(empty($listcolumn)){return $listusers;}
	  if(!is_array($listcolumn)){return $listusers;}
	 
	 
	 foreach ($listusers as $row){
		 $userid=$this->getUtildata()->getVaueOfArray($row,'userid'); 
		 
		  foreach ($listcolumn as $ckey => $cvalue){
			  $akey=$ckey."_".$userid;
			  $uattandance=$this->getUtildata()->getVaueOfArray($listattendance, $akey); 
			  $fuattandance=$this->castAttendanceLabel($uattandance);
			  $row[$ckey]=$fuattandance;
		  }
		array_push($newlist,$row);
	  }
	 return $newlist;
  }
  
    function castAttendanceLabel($listattendance){
		$label="";
		
		 if(empty($listattendance)){return $label;}
		 if(!is_array($listattendance)){return $label;}
	 
		foreach ($listattendance as $row){
			$timestart=$this->getUtildata()->getVaueOfArray($row,'timestart'); 
			$timeend=$this->getUtildata()->getVaueOfArray($row,'timeend'); 
			$statusname=$this->getUtildata()->getVaueOfArray($row,'statusname'); 
			$statusshortname=$this->getUtildata()->getVaueOfArray($row,'statusshortname'); 
			if(empty($statusshortname)){$statusshortname="undefined";}
			if(!empty($timestart) && !empty($timeend)){
				$label.="<i class=\"fas fa-circle attendance-status-".$statusshortname."\"></i> ";
				$hourstart= $timestart->format('H:i');
				$hourend= $timeend->format('H:i');
				$label.=$this->getTranslator()->trans('badiu.system.time.hour.interval',array('%hourstart%'=>$hourstart,'%hourend%'=>$hourend));
				$label.= " ".$statusname."<br />";
			}
		}
		return $label;
	}
}
