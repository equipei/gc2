<?php

namespace Badiu\Admin\AttendanceBundle\Model;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportController;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class AttendanceReportController extends BadiuReportController {

	function __construct(Container $container) {
		parent::__construct($container);
	}

  public function exec() {
	
    $checklimit=$this->getContainer()->get('badiu.system.core.lib.date.checklimit');
    $checklimit->init($this->getFparam(),$this->getFconfig());
	
    //review to only one parameter remove it: coursetimelastaccess. Change param to period this give error if date is null
    $result=$checklimit->periodordate('timestartp','timestart','badiu.admin.attendance.timestart.excedlimitofperiodordate');
	
    if(!empty($result)){return $result;}
	
    return null;
  }

}
