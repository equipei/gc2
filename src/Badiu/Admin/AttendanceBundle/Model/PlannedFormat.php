<?php

namespace Badiu\Admin\AttendanceBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;

class PlannedFormat extends BadiuFormat {

    private $dateformat;
    private $dateuser;
    function __construct(Container $container) {
        parent::__construct($container);
         $this->dateformat=$this->getContainer()->get('badiu.system.core.lib.format.dateformat');
         $this->dateuser=$this->getContainer()->get('badiu.system.user.user.data');
    }
    public function classehour($data) {
        $result = null;
        $timestart =$this->getUtildata()->getVaueOfArray($data,'timestart');
        $timeend = $this->getUtildata()->getVaueOfArray($data,'timeend');
        
        if ($timestart!=null && $timeend!=null ) {
             $result= $this->dateformat->period($timestart,$timeend);
        }
            



        return $result;
    }
    public function responsibleteacher($data) {
        $result = "";
        $dconfig =$this->getUtildata()->getVaueOfArray($data,'dconfig');
     
        $dconfig = $this->getJson()->decode($dconfig, true);
        
        $dconfig=$this->getUtildata()->getVaueOfArray( $dconfig, 'responsibleteachers');
        if(empty($dconfig)){return $result;}
        foreach ($dconfig as $userid){
             $result.= $this->getUser($userid);
            $result.="<br />";
        }
        return $result;
    }

    private function getUser($userid) {
        $badiusession=$this->getContainer()->get('badiu.system.access.session');
        $sessionkey='badiu.ams.offer.classeplanned.responsibleteachers.'.$userid;
        //get is session if exist
        if($badiusession->existValue($sessionkey)){
            $name= $badiusession->getValue($sessionkey);
            return $name;
        } else{
            $name=$this->dateuser->getNameById($userid);
            $badiusession->addValue($sessionkey,$name);
            return $name;
        }
        return null;
    }
}
