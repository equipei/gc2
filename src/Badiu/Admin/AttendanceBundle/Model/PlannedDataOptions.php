<?php

namespace Badiu\Admin\AttendanceBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class PlannedDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
  
	/*
    public  function getFormChoice(){
        $data=$this->getContainer()->get('badiu.admin.attendance.planned.data');
        $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
        $list=$data->getTimes($modulekey,$moduleinstance);
        $newlist=array();
        $dateformat=$this->getContainer()->get('badiu.system.core.lib.format.dateformat');
        $cont=0;
        foreach ($list as $row) {
            $cont++;
            $timestart =$this->getUtildata()->getVaueOfArray($row,'timestart');
            $timeend = $this->getUtildata()->getVaueOfArray($row,'timeend');
            $id = $this->getUtildata()->getVaueOfArray($row,'id');
            $seq=$this->getTranslator()->trans('badiu.admin.attendance.planned.seq',array('%number%'=>$cont));
            if ($timestart!=null && $timeend!=null ) {
                $result= $dateformat->period($timestart,$timeend);
                $newlist[$id]=$seq.' - '.$result;
            }
        
        }
        return  $newlist;
    }
    
    public  function getClasseTeacher(){
		 $list=array();
        $enroldata=$this->getContainer()->get('badiu.ams.enrol.classe.data');
        $parentid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
		if(empty($parentid)){ return $list;}
        $list=$enroldata->getUserFormChoice($parentid);
       
        return $list;
    }*/
}
