<?php

namespace Badiu\Admin\AttendanceBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class AttendanceLib extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


 public function changeStatus($param) {
	  $id=$this->getUtildata()->getVaueOfArray($param, 'id');
	  $newstatusid=$this->getUtildata()->getVaueOfArray($param, 'newstatusid');
	 
	  $entity=$this->getEntity();
               
	   if(empty($id)){return null;}
	   if(empty($entity)){return null;}
	    
				
	    $statusinfo="executed";
	   if(empty($newstatusid)){$statusinfo="planned";}
	   
	   $attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
	   $dhour=$attendancedata->getGlobalColumnValue('dhour',array('id'=>$id)); 
	   $thour=$dhour;
	   $statusshortname=$this->getContainer()->get('badiu.admin.attendance.status.data')->getShortnameById($newstatusid); 
	   if($statusshortname=='absent'){$thour=0;}
	   $paramedit=array('entity'=>$entity,'id'=>$id,'statusid'=>$newstatusid,'thour'=>$thour,'statusinfo'=>$statusinfo,'timemodified'=>new \DateTime());
	   $dresult=$attendancedata->updateNativeSql($paramedit,false);
				
	   return $dresult;

	}
}
