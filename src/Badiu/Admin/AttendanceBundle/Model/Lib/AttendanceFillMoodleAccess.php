<?php

namespace Badiu\Admin\AttendanceBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\SchedulerBundle\Model\Lib\TaskGeneralExec;

class AttendanceFillMoodleAccess extends TaskGeneralExec {
   
    function __construct(Container $container) {
        parent::__construct($container);
      }

    public function update() {
         
          $atlastday=$this->getUtildata()->getVaueOfArray($this->getParam(),'atlastday');
          $serviceid=$this->getUtildata()->getVaueOfArray($this->getParam(),'serviceid');
		  $servicekey=$this->getUtildata()->getVaueOfArray($this->getParam(),'servicekey');
		  $function=$this->getUtildata()->getVaueOfArray($this->getParam(),'function');
         
		 if(!$this->getContainer()->has($servicekey)){return  array('datashouldexec' => 0, 'dataexec' => 0, 'message' => 'badiu.attendance.service.error.servicekeynotvalid');}
		 $servicedata=$this->getContainer()->get($servicekey); 
		 if(!method_exists ($servicedata,$function)){return  array('datashouldexec' => 0, 'dataexec' => 0, 'message' => 'badiu.attendance.service.error.functionnotexistinservice');}
		 $fparam=array('atlastday'=>$atlastday,'serviceid'=>$serviceid);
		 $result=$servicedata->moodleLogFillAttendance($fparam);
		
         return  $result;
   }
  
}
