<?php
namespace Badiu\Admin\AttendanceBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class AttendanceAutomaticFill extends BadiuModelLib {
	private $param;
		/**
     * @var Object
    */
    private $calendar; 
    function __construct(Container $container) {
        parent::__construct($container);
		$this->param=null;
		
    }
	
	function exec() {
		if(empty($this->param)){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramsnotdefined','The param is not defined in object');} 
		if(!is_array($this->param)){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramisnotarray','The param  defined in object is not array');}
		
		
		
		
		$targettype=$this->getUtildata()->getVaueOfArray($this->param,'targettype');
		$userid=$this->getUtildata()->getVaueOfArray($this->param,'userid');
		
		$referencekey=$this->getUtildata()->getVaueOfArray($this->param,'referencekey');
		
		$modulekey=$this->getUtildata()->getVaueOfArray($this->param,'modulekey');
		$moduleinstance=$this->getUtildata()->getVaueOfArray($this->param,'moduleinstance');
		
		$weekday=$this->getUtildata()->getVaueOfArray($this->param,'weekday');
		$hourstart=$this->getUtildata()->getVaueOfArray($this->param,'hourstart');
		$hourend=$this->getUtildata()->getVaueOfArray($this->param,'hourend');
		
		$timestart=$this->getUtildata()->getVaueOfArray($this->param,'timestart');
		$timeend=$this->getUtildata()->getVaueOfArray($this->param,'timeend');
		
		$dhour=$this->getUtildata()->getVaueOfArray($this->param,'dhour');
		$calendar=$this->getUtildata()->getVaueOfArray($this->param,'calendar');
		$hroomid=$this->getUtildata()->getVaueOfArray($this->param,'hroomid');
		
		$entity=$this->getUtildata()->getVaueOfArray($this->param,'entity');
		
		$criteria=$this->getUtildata()->getVaueOfArray($this->param,'criteria');
		
		if(empty($entity)){$entity=$this->getEntity();}
		//check param
		if(empty($targettype)){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramtargettyperequired','The param  targettype is null. This param is required');}
		if($targettype=='oneuser' && empty($userid)){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramuseridrequired','The param  userid is null. This param is required');}
		if(empty($modulekey)){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.parammodulekeyrequired','The param  modulekey is null. This param is required');}
		if(empty($moduleinstance)){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.parammoduleinstancerequired','The param  instance is null. This param is required');}
		if(empty($weekday)){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramweekdayerequired','The param  weekday is null. This param is required');}
		if(!is_array($weekday)){return  $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramweekisnotarray','The param  weekday is not array. Set is as array');}
		if(sizeof($weekday)==0){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramweekdaywithoutvalue','The param  weekday is array without value. Set at least one value');}
		if(empty($dhour)){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramdhourrequired','The param  dhour is null. This param is required');}
		
		 $difftime=$timeend->getTimestamp()-$timestart->getTimestamp();
		 if($difftime <0 ){ return  $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramtimeendbiggerthantimestart','The param  timeend is bigger than timestart');}
		 if($hourend <  $hourstart){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramhourendbiggerthanhourstart','The param  hourend is bigger than hourstart');}
		 
		 $hourduration=$hourend-$hourstart;
		
		 $timestart->setTime(0,0);
		 $timeend->setTime(23,59);
		 
		 $hourstart=$this->getContainer()->get('badiu.system.core.lib.form.casthour')->convertToForm($hourstart);
		 $hourend=$this->getContainer()->get('badiu.system.core.lib.form.casthour')->convertToForm($hourend);
		 
         if(!is_array($hourstart)){ return  $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramhourstartisnotarry','The param  hourstrat is not array after cast');}
		 if(!is_array($hourend)){return  $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramhourendisnotarry','The param  hourend is not array after cast');}
			 
		 $planneddata=$this->getContainer()->get('badiu.admin.attendance.planned.data');
		 $attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
		 
		 
	
		//count registred hour
		$countregistredhour=0;
		$paramfcounthour=null;
		if($targettype=='defaultplanned'){
				$paramfcounthour=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'deleted'=>false);
				$countregistredhour=$planneddata->sumGlobalColumn('hourduration',$paramfcounthour);
				if($countregistredhour >= $dhour){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramdhourifjustfilled','The param  dhour defined is just filled in planned');}
		}else if($targettype=='oneuser'){
				$paramfcounthour=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'userid'=>$userid,'deleted'=>false);
				$countregistredhour=$attendancedata->sumGlobalColumn('dhour',$paramfcounthour);
				if($countregistredhour >= $dhour){return $this->getResponse()->denied('badiu.admin.attendance.planned.automaticfill.paramdhourifjustfilled','The param  dhour defined is just filled in planned');}
		}
		
		//calendar
		if($calendar=='default'){
			$this->calendar=$this->getContainer()->get('badiu.system.core.lib.date.calendartimeutil');
			$badiuSession = $this->getContainer()->get('badiu.system.access.session');
			$badiuSession->setHashkey($this->getSessionhashkey());
			$calendarconfig=$badiuSession->getValue('badiu.system.core.param.config.date.calendar');
			$this->calendar->init($calendarconfig);
			
		 }
		
		//statusid
		$plannedstatusid=null;
		$attendancestatusid=null;
		if($targettype=='defaultplanned'){
			$plannedstatusid=$this->getContainer()->get('badiu.admin.attendance.plannedstatus.data')->getIdByShortname($entity,'planned');
		}
		//fill planned
		$exec=true;
		$cont=0;
		$plannedadd=0;
		$resultid=array();
		$paramf=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'deleted'=>false);
		while($exec){
			  if( $cont > 0){$timestart->modify('+1 day');}
			
			  
			  $countregistredhour=0;
			  if($targettype=='defaultplanned'){
				   $countregistredhour=$planneddata->sumGlobalColumn('hourduration',$paramfcounthour);
			  }else if($targettype=='oneuser'){
				  $countregistredhour=$attendancedata->sumGlobalColumn('dhour',$paramfcounthour);
			  }
			 
			  if($countregistredhour >= $dhour){break;$exec=false;}
			  if(($countregistredhour+ $hourduration) > $dhour){break;$exec=false;}	 
			 $dayofweek=$timestart->format('w');
		
			 if (in_array($dayofweek, $weekday)) { 
			 
				$timestart->setTime($hourstart['hour'],$hourstart['minute']);
				$timeendc=clone $timestart;
				$timeendc->setTime($hourend['hour'],$hourend['minute']);
				
				$paramf=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'timestart'=>$timestart,'timeend'=>$timeendc);
			  
				$isbusy=true;
				 if($targettype=='defaultplanned'){$isbusy=$planneddata->isTimeBusy($paramf);}
				 else if($targettype=='oneuser'){$isbusy=$attendancedata->isTimeBusy($paramf);}
				 
				  //calendar
				$isworkingday=true;
				if(!empty($calendar)){if(!$this->calendar->isWorkingDay($timestart)){$isworkingday=false;};}
			
				if($isbusy==0 && $isworkingday){ 
					 if(!empty($timeendc) && $timeendc->getTimestamp() > $timeend->getTimestamp() ){break;$exec=false;}
					  if($targettype=='defaultplanned'){
						  $parami=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'timestart'=>$timestart,'timeend'=>$timeendc,'hourduration'=>$hourduration,'hroomid'=>$hroomid,'statusid'=>$plannedstatusid,'criteria'=>$criteria,'deleted'=>0,'timecreated'=>new \DateTime());
						  $r=$planneddata->insertNativeSql($parami,false);
						  if($r){$plannedadd++;array_push($resultid,$r);}
					  }else if($targettype=='oneuser'){
						  $daykey=$timestart->format('Y_m_d');
						  $parami=array('entity'=>$entity,'statusinfo'=>'planned','hroomid'=>$hroomid,'timestart'=>$timestart,'timeend'=>$timeendc,'daykey'=>$daykey,'dhour'=>$hourduration,'criteria'=>$criteria,'userid'=>$userid,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'referencekey'=>$referencekey,'deleted'=>0,'timecreated'=>new \DateTime());
						  $r=$attendancedata->insertNativeSql($parami,false);
						  if($r){$plannedadd++;array_push($resultid,$r);}
					  }
				}
			
		   }
		 
		  if( $cont > 1500){break;$exec=false;}
		 // if($result){break;$exec=false;}
		  $cont++;
		}
		$fresult=array('countexec'=>$plannedadd,'listidsexec'=>$resultid);
		 return $this->getResponse()->accept($fresult);
		}
	/*function isTimeBusy($timestart,$hourduration) {
		 $planneddata=$this->getContainer()->get('badiu.admin.attendance.planned.data');
		 print_r($timestart);
		 $modulekey=$this->getUtildata()->getVaueOfArray($this->param,'modulekey');
		 $moduleinstance=$this->getUtildata()->getVaueOfArray($this->param,'moduleinstance');
	
		 $entity=$this->getUtildata()->getVaueOfArray($this->param,'entity');
		 if(empty($entity)){$entity=$this->getEntity();}
		
		 $difftime=$hourduration*60;
		 if($difftime > 0 && $difftime > 20){$difftime=$difftime/5;}
		 $paramf=array('modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'entity'=>$entity);
		 
		 $exec=true;
		 $cont=0;
		 $tstart= clone $timestart;
		 $tend= clone $timestart;
		 $tendlimit= clone $timestart;
		 $tendlimit->setTimestamp($tendlimit->getTimestamp()+($hourduration*60));
		 $result=0;
		 while($exec){
			
			 if( $cont==0){
				  $tend->setTimestamp($tend->getTimestamp()+$difftime);
			 }else{
				$tstart->setTimestamp($tstart->getTimestamp()+$difftime);
				$tend->setTimestamp($tend->getTimestamp()+$difftime);
			 }
			 echo "start: ";
			 print_r($tstart);
			 echo "end: ";
			  print_r($tend);
			 if($tend->getTimestamp() >= $tendlimit->getTimestamp()){echo "ultrapassou";break;$exec=false;}
			 $paramf['timestart']=$tstart;
			 $paramf['timeend']=$tend;
			 $result=$planneddata->isTimeBusy($paramf); 
			 echo "result: $result";
			 if( $cont > 100){break;$exec=false;}
			 if($result){break;$exec=false;}
			  $cont++;
		 }
		
		return $result;
    }*/
	
	function getParam() {
		return $this->param;
    }

        function setparam($param) {
            $this->param = $param;
        }
 function setCalendar($calendar) {
         $this->calendar = $calendar;
     }

     function getCalendar() {
         return $this->calendar;
     }
}
