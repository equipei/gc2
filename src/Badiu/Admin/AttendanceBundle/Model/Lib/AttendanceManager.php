<?php
namespace Badiu\Admin\AttendanceBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class AttendanceManager extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

	   public function updateStatus() {
               $id=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('id');
			   $statusid=$datasource=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('statusid');
                $entity=$this->getEntity();
               
			   if(empty($id)){return null;}
			   if(empty($entity)){return null;}
			    
				
			   $statusinfo="executed";
			   if(empty($statusid)){$statusinfo="planned";}
			   
			   $attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
			   $dhour=$attendancedata->getGlobalColumnValue('dhour',array('id'=>$id)); 
			   $thour=$dhour;
			   $statusshortname=$this->getContainer()->get('badiu.admin.attendance.status.data')->getShortnameById($statusid); 
				if($statusshortname=='absent'){$thour=0;}
			 	$paramedit=array('entity'=>$entity,'id'=>$id,'statusid'=>$statusid,'thour'=>$thour,'statusinfo'=>$statusinfo,'timemodified'=>new \DateTime());
				$dresult=$attendancedata->updateNativeSql($paramedit,false);
				
			 return $dresult;
   }

   public function updateAllNullStatus() {
                $querystringsystem=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem');
				$entity=$this->getEntity();
			    $key=$querystringsystem->getParameter('_key');
			   $attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
			   $fparam=$querystringsystem->getParameters();  
			   $fparam['entity']=$entity;
			 //  $fparam['moudlekey']='badiu.ams.offer.classeattendance';
			    $fparam['_page']=0;
			  /* echo "<pre>";
			  print_r($fparam);
			  echo "</pre>";exit;*/
			 
			  // $fparam['daykey']=$daykey
			   $key=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
			   $this->getSearch()->setSessionhashkey($this->getSessionhashkey());
			   $this->getSearch()->getKeymanger()->setBaseKey($key);
			   $result = $this->getSearch()->search($fparam);
			   //echo sizeof($result);exit;
			/*     echo "<pre>";
			  print_r($result);
			  echo "</pre>";exit;*/
			   if(empty($result)){return null;}
			   if(!is_array($result)){return null;}
			   $cont=0;
			   $statusid=$this->getContainer()->get('badiu.admin.attendance.status.data')->getIdByShortname($entity,'present');
			   $statusinfo="executed";
			   if(empty($statusid)){return null;}
			   foreach ($result as $row) {
				    $id=$this->getUtildata()->getVaueOfArray($row,'id');
					$dhour=$this->getUtildata()->getVaueOfArray($row,'dhour');
				   	$paramedit=array('id'=>$id,'statusid'=>$statusid,'thour'=>$dhour,'statusinfo'=>$statusinfo,'timemodified'=>new \DateTime());
					$rid=$attendancedata->updateNativeSql($paramedit,false);
					if($rid){$cont++;}
			   }
			   
			 return $cont;
   }
     public function removeAll() {
                $querystringsystem=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem');
				$entity=$this->getEntity();
			   
			   $attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
			   $fparam=$querystringsystem->getParameters();  
			   $key=$querystringsystem->getParameter('_key');
			   $statusinfo=$querystringsystem->getParameter('statusinfo');
			   if($statusinfo!='planned'){return null;}
			   $fparam['entity']=$entity;
			  
			 //  $fparam['moudlekey']='badiu.ams.offer.classeattendance';
			    $fparam['_page']=0;
			   
			 // print_r($fparam);exit;
			 
			  // $fparam['daykey']=$daykey
			    $key=$this->getContainer()->get('badiu.system.core.lib.config.keyutil')->removeLastItem($key);
			   $this->getSearch()->setSessionhashkey($this->getSessionhashkey());
			   $this->getSearch()->setPaginglimit(5000);
			   $this->getSearch()->getKeymanger()->setBaseKey($key);
			   $result = $this->getSearch()->search($fparam);
			   
			   
			   if(empty($result)){return null;}
			   if(!is_array($result)){return null;}
			   $cont=0;
			   
			   foreach ($result as $row) {
				    $id=$this->getUtildata()->getVaueOfArray($row,'id');
					$rid=$attendancedata->removeNativeSql(array('id'=>$id),false);
					if($rid){$cont++;}
			   }
			   
			 return $cont;
   }
   public function updateAllNullStatusMenu() {
	     $classeid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
         $plannedid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('plannedid');
		 $daykey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('daykey');
         $urlback=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urlback');
         $urlback=urldecode($urlback);
		
         //if(empty($plannedid) &&  empty($daykey) ){ header("Location: $urlback");}
        // if(empty($classeid)){ header("Location: $urlback");}
		
       
		 $resqul=$this->updateAllNullStatus();
         header("Location: $urlback");
         exit;
        return null;
	   
   }
   
   public function  removeAllMenu() {
	     $classeid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('parentid');
         $plannedid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('plannedid');
		 $daykey=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('daykey');
         $urlback=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('_urlback');
         $urlback=urldecode($urlback);
		
		 $resqul=$this->removeAll();
         header("Location: $urlback");
         exit;
        return null;
	   
   }
   //http://d1.badiu21.com.br/badiunet/web/app_dev.php/ams/offer/classe/attendance/index/139?user=&plannedid=&statusid=&moduleinstance=139
      public function changeUserTimestartHour() { 
		  $userid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('userid');  
		  $hour=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('hour'); 
		  
		   $attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
		   $list= $attendancedata->getGlobalColumnsValues('o.id,o.timestart', array('userid'=>$userid));
		   
		   foreach ($list as $row) {
			  $id= $this->getUtildata()->getVaueOfArray($row,'id');
			  $timestart= $this->getUtildata()->getVaueOfArray($row,'timestart');
			 
			   $minutes=$timestart->format('i');
			   $timestart->setTime($hour,$minutes);
			   $uparam=array('id'=>$id,'timestart'=>$timestart);
			   $rid=$attendancedata->updateNativeSql($uparam,false);
			   
			   print_r($uparam);echo "<hr />";
		   }
		   
	  }
	  
	   public function changeDThour() { 
		  $plannedid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('plannedid');  
		  $userid=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('userid');  
		  $hour=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameter('hour'); 
		  
		  $flparam=array();
		  $flparam['entity']=$this->getEntity();
		  if(!empty($plannedid)){$flparam['plannedid']=$plannedid;}
		  if(!empty($userid)){$flparam['userid']=$userid;}
		  if(!empty($modulekey)){$flparam['modulekey']=$modulekey;}
		  if(!empty($moduleinstance)){$flparam['moduleinstance']=$moduleinstance;}
		  
		   $attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
		   $list= $attendancedata->getGlobalColumnsValues('o.id,o.timestart,o.timeend,o.statusinfo',$flparam);
		   
		   $result=array('shoudexec'=>0,'exec'=>0);
		   foreach ($list as $row) {
			   $result['shoudexec']++;
			  $id= $this->getUtildata()->getVaueOfArray($row,'id');
			  $timestart= $this->getUtildata()->getVaueOfArray($row,'timestart');
			  $timeend= $this->getUtildata()->getVaueOfArray($row,'timeend');
			  $statusinfo= $this->getUtildata()->getVaueOfArray($row,'statusinfo');
			 
			  if(!empty($timestart) && !empty($timeend) ){
				  $hourstart=($timestart->format('H')*60)+$timestart->format('i');
				  $hourend=($timeend->format('H')*60)+$timeend->format('i');
				  $dhour=$hourend-$hourstart;
				  if($dhour > 0){
					  $uparam=array('id'=>$id,'dhour'=>$dhour);
					  if($statusinfo=='executed'){$uparam['thour']=$dhour;}
					  $rid=$attendancedata->updateNativeSql($uparam,false);
					  if($rid){$result['exec']++;}
				  }
				  
			  }
			  
		   }
		 return   $result;
	  }
	  //http://54.156.96.187/badiunet/web/app_dev.php/system/service/process?_service=badiu.admin.attendance.attendance.manager&_function=fillDaykey
	   public function fillDaykey() { 
		  $flparam=array();
		 // $flparam['entity']=$this->getEntity();
		  
		   $attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
		   $list= $attendancedata->getGlobalColumnsValues('o.id,o.timestart',$flparam);
		   
		   $result=array('shoudexec'=>0,'exec'=>0);
		   foreach ($list as $row) {
			  $result['shoudexec']++;
			  $id= $this->getUtildata()->getVaueOfArray($row,'id');
			  $timestart= $this->getUtildata()->getVaueOfArray($row,'timestart');
			 
			  $daykey=$timestart->format('Y_m_d');
			 
			  $uparam=array('id'=>$id,'daykey'=>$daykey);
			  $rid=$attendancedata->updateNativeSql($uparam,false);
			  if($rid){$result['exec']++;}
			  
		   }
		 return   $result;
	  }
}
