<?php
namespace Badiu\Admin\AttendanceBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormat;

class AttendanceFormat extends BadiuFormat {
  private $dateformat;
 
    function __construct(Container $container) {
        parent::__construct($container);
        $this->dateformat=$this->getContainer()->get('badiu.system.core.lib.format.dateformat');
  }
    public function percentage($data) {
        $result = null;
        $dhour =$this->getUtildata()->getVaueOfArray($data,'dhour');
        $thour = $this->getUtildata()->getVaueOfArray($data,'thour');
        if($dhour > 0 && $thour >=0){
          $result =  $thour* 100/$dhour;
          $result=round($result);
        }
         return $result;
    }

    public function hour($data) {
      
      $result = null;
      $timestart =$this->getUtildata()->getVaueOfArray($data,'timestart');
      $timeend = $this->getUtildata()->getVaueOfArray($data,'timeend');
       
	
	  $statusshortname = $this->getUtildata()->getVaueOfArray($data,'statusshortname');
      if(empty($statusshortname)){$statusshortname="undefined";}
	  $result.="<i class=\"fas fa-circle attendance-status-".$statusshortname."\"></i> ";
	  
    /*  $ttimestart =$this->getUtildata()->getVaueOfArray($data,'ttimestart');
      $ttimeend = $this->getUtildata()->getVaueOfArray($data,'ttimeend');

      if(!empty($ttimestart) && $ttimestart->getTimestamp() > 0 ){$timestart=$ttimestart;}
      if(!empty($ttimeend)  && $ttimeend->getTimestamp() > 0 ){$timeend=$ttimeend;}
*/
      if ($timestart!=null && $timeend!=null ) {
           $result.= $this->dateformat->period($timestart,$timeend);
      }
           



      return $result;
  }
}
