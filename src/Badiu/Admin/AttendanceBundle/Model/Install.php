<?php
namespace Badiu\Admin\AttendanceBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;
class Install extends BadiuModelLib {

     function __construct(Container $container) {
        parent::__construct($container);
      
    }


    public function exec() {
       
           $result= $this->initAttendanceStatus();
		   $result+= $this->initPlannedStatus();
		   return  $result;
    } 

     public function initAttendanceStatus() {
         $cont=0;
         
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.attendance.status.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
          
         $list=array();
         $list['present']=array('label'=>$this->getTranslator()->trans('badiu.admin.attendance.status.present'),'abbreviation'=>'P');
         $list['absent']=array('label'=>$this->getTranslator()->trans('badiu.admin.attendance.status.absent'),'abbreviation'=>'F');
         $list['late']=array('label'=>$this->getTranslator()->trans('badiu.admin.attendance.status.late'),'abbreviation'=>'A');
         $list['justified']=array('label'=>$this->getTranslator()->trans('badiu.admin.attendance.status.justified'),'abbreviation'=>'J');
       
         
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$this->getUtildata()->getVaueOfArray($value, 'label');
			$param['abbreviation']=$this->getUtildata()->getVaueOfArray($value, 'abbreviation');
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
            
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
         
     }


     public function initPlannedStatus() {
         $cont=0;
         
         $entity=$this->getEntity();
         $data = $this->getContainer()->get('badiu.admin.attendance.plannedstatus.data');
         $exist=$data->countGlobalRow(array('entity'=>$entity));
         if($exist){return 0;}
         
         $list=array();
         $list['planned']=$this->getTranslator()->trans('badiu.admin.attendance.plannedstatus.planned');
         $list['executed']=$this->getTranslator()->trans('badiu.admin.attendance.plannedstatus.executed');
        
         
         foreach ($list as $key => $value) {
             $param=array();
            $param['entity']=$entity;
            $param['name']=$value;
            $param['shortname']=$key;
            $param['timecreated']=new \DateTime();
            $param['deleted']=0;
            
            if(!$data->existByShortname($entity,$key)){
              $result = $data->insertNativeSql($param,false); 
              if($result){$cont++;}
            }
         }
         
     }	 
}
