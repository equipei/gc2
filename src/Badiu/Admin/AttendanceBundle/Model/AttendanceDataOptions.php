<?php

namespace Badiu\Admin\AttendanceBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuFormDataOptions;
class AttendanceDataOptions extends BadiuFormDataOptions{
    
     function __construct(Container $container,$baseKey=null) {
            parent::__construct($container,$baseKey);
       }
	   
	   
	public  function getTargetBatchType(){
			$list=array();
			$list['defaultplanned']=$this->getTranslator()->trans('badiu.admin.attendance.batchtype.defaultplanned');
			$list['customuser']=$this->getTranslator()->trans('badiu.admin.attendance.batchtype.customuser');
			$list['oneuser']=$this->getTranslator()->trans('badiu.admin.attendance.batchtype.oneuser');
			return $list;
	 }
   	 public  function getCriteria(){
        
        $list=array();
        $list['manual']=$this->getCriteriaLabel('manual');
	    return  $list;
    }
	
	 public  function getCriteriaLabel($value){
        $label=null;
        if($value=='manual'){$label=$this->getTranslator()->trans('badiu.admin.attendance.attendance.criteria.manual');}
	    return  $label;
    }
	
	public  function getStatusinfo(){
        
        $list=array();
        $list['planned']=$this->getStatusinfoLabel('planned');
		 $list['executed']=$this->getStatusinfoLabel('executed');
	    return  $list;
    }
	
	 public  function getStatusinfoLabel($value){
        $label=null;
        if($value=='planned'){$label=$this->getTranslator()->trans('badiu.admin.attendance.attendance.statusinfo.planned');}
		else if($value=='executed'){$label=$this->getTranslator()->trans('badiu.admin.attendance.attendance.statusinfo.executed');}
	    return  $label;
    }
	
	 public  function getDaysPlanned(){
        //get modulekey | moduleinstance | entity
		//get list
		//format list
    }
}
