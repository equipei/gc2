<?php
namespace Badiu\Admin\AttendanceBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuReportChangeData;
class AttendanceAbsenceChangeData extends BadiuReportChangeData
{

    function __construct(Container $container) {
            parent::__construct($container);
              }
              

public function all($data){
	
	//list attendance
	$listattendance=$this->getUtildata()->getVaueOfArray($data,'badiu_list_data_rows.data.1',true); 
	$listattendance=$this->splitListAttendanceByUser($listattendance);
	
	//list users
	$listuser=$this->getUtildata()->getVaueOfArray($data,'badiu_table1_rows'); 
	$listuser=$this->addColumnsAbsence($listuser,$listattendance);
	
	$data['badiu_table1_rows']=$listuser;
	
	/*echo "<pre>";
	print_r($data);
	echo "</pre>";exit;*/
     return $data;
      
  }
 function splitListAttendanceByUser($listattendance){
	 $newlist=array();
	 if(empty($listattendance)){return $newlist;}
	 if(!is_array($listattendance)){return $newlist;}
	 
	 foreach ($listattendance as $row){
		$userid=$this->getUtildata()->getVaueOfArray($row,'userid'); 
		$enterpriseid=$this->getUtildata()->getVaueOfArray($row,'enterpriseid'); 
		$timestart=$this->getUtildata()->getVaueOfArray($row,'timestart');
		
		if (is_a($timestart, 'DateTime')) {
			$timestart=$timestart->format('d/M/Y');
		}
		
		$key=$userid.'/'.$enterpriseid; 
		$listime=$this->getUtildata()->getVaueOfArray($newlist,$key);
		if(empty($listime)){$listime=array('count'=>1,'days'=>$timestart);}
		else {
			$listime['count']++;
			$listime['days'].=" ".$timestart;
		}
		$newlist[$key]=$listime;
	  }
	 return $newlist;
 }
 
  function addColumnsAbsence($listusers,$listattendance){
	   $newlist=array();
	   
	 if(empty($listusers)){return $newlist;}
	 if(!is_array($listusers)){return $newlist;}
	 
	  if(empty($listattendance)){return $listusers;}
	  if(!is_array($listattendance)){return $listusers;}
	 
	 
	 foreach ($listusers as $row){
		 $userid=$this->getUtildata()->getVaueOfArray($row,'userid'); 
		 $enterpriseid=$this->getUtildata()->getVaueOfArray($row,'enterpriseid'); 
		 
		 $key=$userid.'/'.$enterpriseid;
		 $countabsence=$this->getUtildata()->getVaueOfArray($listattendance,$key.'.count',true); 
		 $listabsence=$this->getUtildata()->getVaueOfArray($listattendance,$key.'.days',true); 
		 $row['countabsence']=$countabsence;
		 $row['listabsence']=$listabsence;
		array_push($newlist,$row);
	  }
	 return $newlist;
  }
  
}
