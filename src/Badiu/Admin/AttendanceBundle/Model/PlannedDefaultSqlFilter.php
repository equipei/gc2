<?php
namespace Badiu\Admin\AttendanceBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuSqlFilter;

class PlannedDefaultSqlFilter extends BadiuSqlFilter{
    /**
     * @var object
     */
   private  $sqlservice;
   

    function __construct(Container $container) {
            parent::__construct($container);
     }

        function adddatefields() {
         
           $param= $this->getUtildata()->getVaueOfArray($this->getParam(),'_addcolumndynamicallyadminattendanceplanned');
		   $modulekey=$this->getUtildata()->getVaueOfArray($this->getParam(),'modulekey');
		   $moduleinstance=$this->getUtildata()->getVaueOfArray($this->getParam(),'moduleinstance');
		   $defaultdateplanned=$this->getUtildata()->getVaueOfArray($this->getParam(),'defaultdateplanned');
		   if($defaultdateplanned==1){$defaultdateplanned=true;}
		   else {$defaultdateplanned=false;}
		   $sql="";
         
		
            if($param==1){
                
				//$badiuSession=$this->getContainer()->get('badiu.system.access.session');
			//	$entity=$badiuSession->get()->getEntity();
			//	$paramfilter=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'deleted'=>false);
				$fields=array();
				/* if($defaultdateplanned){
					$planneddata=$this->getContainer()->get('badiu.admin.attendance.planned.data');
					$fields=$planneddata->getGlobalColumnsValues('o.id,o.timestart,o.timeend',$paramfilter);
				}else{*/
					// $attendancedata=$this->getContainer()->get('badiu.admin.attendance.attendance.data');
					// $fields= $attendancedata->getGlobalColumnsValues('DISTINCT o.daykey',$paramfilter);
					
					//review 
					$search =$this->getContainer()->get('badiu.system.core.functionality.search');
					$search->setPaginglimit(60);
					//$search->setSessionhashkey($this->getSessionhashkey());
					$keysql=$this->getRouterKey(true);
					$keysql.="daykey";
					$search->getKeymanger()->setBaseKey($keysql);
					$fields = $search->search($this->getParam());
			 //  print_r($fields);exit;
				//}
				 
           
                 if(is_array($fields)){
                   $this->addSessionColumnProfile($fields,$defaultdateplanned);
                     foreach ($fields as $f) {
						 
                       //  if($defaultdateplanned){
						//	$id= $this->getUtildata()->getVaueOfArray($f,'id');
							//$psql=" (SELECT _bamdminattst$id.name FROM BadiuAdminAttendanceBundle:AdminAttendance _bamdminatt$id JOIN _bamdminatt$id.plannedid _bamdminattpl$id JOIN _bamdminatt$id.statusid _bamdminattst$id WHERE _bamdminatt$id.id > 0 AND _bamdminatt$id.plannedid =$id  AND _bamdminatt$id.userid=o.userid ) AS _badiuadminattendanteplanned_$id  ";
							//$sql.=", $psql" ;
						// }else{
							$id= $this->getUtildata()->getVaueOfArray($f,'daykey');
							//$psql=" (SELECT _bamdminattst$id.name FROM BadiuAdminAttendanceBundle:AdminAttendance _bamdminatt$id JOIN _bamdminatt$id.statusid _bamdminattst$id WHERE _bamdminatt$id.id > 0 AND _bamdminatt$id.daykey ='".$id."' AND _bamdminatt$id.userid=o.userid ) AS _badiuadminattendanteplanned_$id  ";
							//$sql.=", $psql" ;
						//}
                         
                     }
                 }
             }
            if($param=="0"){$sql=" ";}
           
            return $sql;
        }
        
       
        function addSessionColumnProfile($fields,$defaultdateplanned) {
            $list=array();
			  $dateformat=$this->getContainer()->get('badiu.system.core.lib.format.dateformat');
			  $dataoptions=$this->getContainer()->get('badiu.system.core.lib.date.period.form.dataoptions');
            foreach ($fields as $f) {
				$id=null;
				$name=null;
				//if($defaultdateplanned){
					/*$timestart= $this->getUtildata()->getVaueOfArray($f,'timestart');
					$timeend= $this->getUtildata()->getVaueOfArray($f,'timeend');
					$id= $this->getUtildata()->getVaueOfArray($f,'id');
					$name= $dateformat->period($timestart,$timeend);*/
				//}else{
					$id= $this->getUtildata()->getVaueOfArray($f,'daykey');
					$p=explode("_",$id);
					$d=$this->getUtildata()->getVaueOfArray($p,2);
					$m=$this->getUtildata()->getVaueOfArray($p,1);
					$y=$this->getUtildata()->getVaueOfArray($p,0);
					
					$ml=$dataoptions->getMonthLabel($m);
					$now=new \DateTime();
					$currentyear=true;
					if($now->format('Y')!=$y){$currentyear=false;};
					if($currentyear){$name=$this->getTranslator()->trans('badiu.system.time.format.daymonth',array('%day%'=>$d,'%month%'=>$ml));}
					else{$name=$this->getTranslator()->trans('badiu.system.time.format.date',array('%day%'=>$d,'%month%'=>$ml,'%year%'=>$y));}
					
				//}
                 
                 $key="_badiuadminattendanteplanned_$id";
                 $list[$key]=$name;
             
             
 
             $badiuSession = $this->getContainer()->get('badiu.system.access.session');
             $badiuSession->setHashkey($this->getSessionhashkey());
             $badiuSession->addValue('_addcolumndynamicallyadminattendanceplanned',$list);
			}
        }
        function getSqlservice() {
           if(empty($this->sqlservice)){
                $this->sqlservice=$this->getContainer()->get('badiu.system.core.lib.sqlservice.sqlservice');
            }
            $this->sqlservice->setSessionhashkey($this->getSessionhashkey());
            return $this->sqlservice;
        }

        function setSqlservice($sqlservice) {
            $this->sqlservice = $sqlservice;
        }
      


}
