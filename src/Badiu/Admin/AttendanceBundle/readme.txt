---migragration---
INSERT INTO admin_attendance_planned(id,moduleinstance,statusid,entity,timestart,timeend,hourduration,description,timecreated,timemodified,deleted,dconfig) SELECT id,classeid AS moduleinstance,statusid,entity,timestart,timeend,hourduration,description,timecreated,timemodified,deleted,dconfig FROM ams_offer_classe_planned
UPDATE admin_attendance_planned SET modulekey='badiu.ams.offer.classeattendance',criteria='manual'

INSERT INTO admin_attendance(id,moduleinstance,userid,entity,timestart,timeend,dhour,thour,timecreated,timemodified,deleted,plannedid) SELECT id,classeid AS moduleinstance,userid,entity,timestart,timeend,dhour,thour,timecreated,timemodified,deleted,plannedid FROM ams_offer_classe_attendance
UPDATE admin_attendance SET statusinfo='executed' WHERE thour > 0
UPDATE admin_attendance SET statusinfo='executed' WHERE statusid IS NOT NULL
UPDATE admin_attendance SET statusinfo='planned' WHERE thour = 0 OR thour is null
UPDATE admin_attendance SET statusid=1 WHERE thour > 0 AND thour=dhour
UPDATE admin_attendance SET statusid=3 WHERE thour > 0 AND thour < dhour
UPDATE admin_attendance SET statusid=2,statusinfo='executed' WHERE dhour > 0 AND thour=0

UPDATE admin_attendance SET admin_attendance.timestart=admin_attendance_planned.timestart,admin_attendance.timeend=admin_attendance_planned.timeend 

UPDATE  admin_attendance AS a INNER JOIN admin_attendance_planned AS p ON (a.plannedid=p.id) SET a.timestart=p.timestart,a.timeend=p.timeend

-- justificar faltal em lote
UPDATE admin_attendance SET statusid=24,statusinfo='executed',description='Feriado nacional' WHERE entity=12 AND daykey='2022_04_15'
