<?php

namespace Badiu\Admin\AttendanceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *AdminAttendance
 *  
 * @ORM\Table(name="admin_attendance", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_attendance_idnumber_uix", columns={"entity", "idnumber"})},
 *      indexes={@ORM\Index(name="admin_attendance_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_attendance_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_attendance_daykey_ix", columns={"daykey"}),
 *              @ORM\Index(name="admin_attendance_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="admin_attendance_dhour_ix", columns={"dhour"}),
 *              @ORM\Index(name="admin_attendance_timestart_ix", columns={"timestart"}),
 *              @ORM\Index(name="admin_attendance_timeend_ix", columns={"timeend"}),
 *              @ORM\Index(name="admin_attendance_statusinfo_ix", columns={"statusinfo"}),
 *              @ORM\Index(name="admin_attendance_statusid_ix", columns={"statusid"}), 
 *              @ORM\Index(name="admin_attendance_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_attendance_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_attendance_referencekey_ix", columns={"referencekey"}), 
 *              @ORM\Index(name="admin_attendance_hroomid_ix", columns={"hroomid"}),
 *              @ORM\Index(name="admin_attendance_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="admin_attendance_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminAttendance
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

      
         /**
     * @var AdminAttendancePlanned
     *
     * @ORM\ManyToOne(targetEntity="AdminAttendancePlanned")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plannedid", referencedColumnName="id")
     * })
     */
    private $plannedid;
	/**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id", nullable=false)
     * })
     */
    private $userid; 
    
	 /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype; 

 /**
     * @var string
     *
     * @ORM\Column(name="daykey", type="string", length=50, nullable=false)
     */
    private $daykey; 
	 /**
     * @var string
     *
     * @ORM\Column(name="statusinfo", type="string", length=50, nullable=false)
     */
    private $statusinfo;  //planned | executed
	
 /**
     * @var AdminAttendanceStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminAttendanceStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;
    
	/**
	 * @var string
	 *
	 * @ORM\Column(name="criteria", type="string", length=50,nullable=true)
	 */
	private $criteria; //manual | automatic_lmsclasroomaccessintimeplanned | automatic_lmsactivitycompleted | automatic_lmssystemaccessintimeplanned

	/**
     * @var integer
     *
     * @ORM\Column(name="dhour", type="integer", nullable=true)
     */
    private $dhour; 

         /**
     * @var integer
     *
     * @ORM\Column(name="thour", type="integer", nullable=true)
     */
    private $thour;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=false)
     */
    private $timestart;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=false)
     */
    private $timeend;
    
	 /**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="referencekey", type="bigint", nullable=true)
	 */
	private $referencekey;
	  	/**
     * @var \Badiu\Util\HousingBundle\Entity\UtilHousingRoom
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Util\HousingBundle\Entity\UtilHousingRoom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hroomid", referencedColumnName="id")
     * })
     */
    
    
    private $hroomid;
    /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
   
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }


    function getUserid() {
        return $this->userid;
    }

    function getDhour() {
        return $this->dhour;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }

  
    function setUserid(\Badiu\System\UserBundle\Entity\SystemUser $userid) {
        $this->userid = $userid;
    }

    function setDhour($dhour) {
        $this->dhour = $dhour;
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

  
    function getPlannedid(){
        return $this->plannedid;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getDconfig() {
        return $this->dconfig;
    }

   

    function setPlannedid(AdminAttendancePlanned $plannedid) {
        $this->plannedid = $plannedid;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

    function getThour() {
        return $this->thour;
    }

    function setThour($thour) {
        $this->thour = $thour;
    }

  /**
     * @return string
     */
    public function getDtype() {
        return $this->dtype;
    }

    /**
     * @param string $dtype
     */
    public function setDtype($dtype) {
        $this->dtype = $dtype;
    }

	
	 function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
	 /**
     * @return string
     */
    public function getStatusinfo() {
        return $this->statusinfo;
    }

    /**
     * @param string $dstatus
     */
    public function setStatusinfo($dstatusinfo) {
        $this->statusinfo = $statusinfo;
    }
	
function getStatusid() {
        return $this->statusid;
    }
	
	   function setStatusid(AdminAttendanceStatus $statusid) {
        $this->statusid = $statusid;
    }
	
		function setCriteria($criteria) {
        $this->criteria = $criteria;
    }

    public function getCriteria() {
        return $this->criteria;
    }
	
	  function getHroomid() {
        return $this->hroomid;
    }
    function setHroomid($hroomid) {
        $this->hroomid = $hroomid;
    }
	
	  /**
     * @return string
     */
    public function getDaykey() {
        return $this->daykey;
    }

    /**
     * @param string $daykey
     */
    public function setDaykey($daykey) {
        $this->daykey = $daykey;
    }

}
