<?php

namespace Badiu\Admin\AttendanceBundle\Entity;
use Badiu\System\ComponentBundle\Model\Form\Cast\BadiuHourFormCast;
use Doctrine\ORM\Mapping as ORM;

/**
 * AdminAttendancePlanned
 *  
 * @ORM\Table(name="admin_attendance_planned", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="admin_attendance_planned_idnumber_uix", columns={"entity", "idnumber"})},
 *      indexes={@ORM\Index(name="admin_attendance_planned_entity_ix", columns={"entity"}),
 *              @ORM\Index(name="admin_attendance_planned_hroomid_ix", columns={"hroomid"}),
 *              @ORM\Index(name="admin_attendance_planned_timestart_ix", columns={"timestart"}),
 *              @ORM\Index(name="admin_attendance_planned_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_attendance_planned_moduleinstance_ix", columns={"moduleinstance"}),
 *              @ORM\Index(name="admin_attendance_planned_timeend_ix", columns={"timeend"}),
 *              @ORM\Index(name="admin_attendance_planned_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_attendance_planned_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminAttendancePlanned  {
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

    
     /**
     * @var AdminAttendancePlannedStatus
     *
     * @ORM\ManyToOne(targetEntity="AdminAttendancePlannedStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusid", referencedColumnName="id")
     * })
     */
    private $statusid;

  	/**
     * @var \Badiu\Util\HousingBundle\Entity\UtilHousingRoom
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\Util\HousingBundle\Entity\UtilHousingRoom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hroomid", referencedColumnName="id")
     * })
     */
    
    
    private $hroomid;
/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string",length=255, nullable=true)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="shortname", type="string", length=255,nullable=true)
	 */
	private $shortname;
    
		/**
	 * @var string
	 *
	 * @ORM\Column(name="criteria", type="string", length=50,nullable=true)
	 */
	private $criteria; //manual | automatic_lmsclasroomaccessintimeplanned | automatic_lmsclasroomaccessintimeplanned | automatic_lmsactivitycompleted | automatic_lmssystemaccessintimeplanned
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestart", type="datetime", nullable=true)
     */
    private $timestart;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeend", type="datetime", nullable=true)
     */
    private $timeend;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hourstart",  type="bigint", nullable=true)
     */
    private $hourstart;
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="hourend",  type="bigint", nullable=true)
     */
    private $hourend;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="hourduration", type="integer", nullable=true)
     */
    private $hourduration;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dayofweek", type="string", length=255, nullable=true)
	 */
	private $dayofweek;
	
     /**
     * @var integer
     *
     * @ORM\Column(name="hourdurationexec", type="integer", nullable=true)
     */
    private $hourdurationexec;
    
	/**
	 * @var string
	 *
	 * @ORM\Column(name="calendar", type="string", length=255, nullable=true)
	 */
	private $calendar;
	
     /**
     * @var integer
     *
     * @ORM\Column(name="dsequence", type="integer", nullable=true)
     */
    private $dsequence;
    
     /**
     * @var string
     *
     * @ORM\Column(name="contentexec", type="text", nullable=true)
     */
    private $contentexec;
	
	 /**
     * @var boolean
     *
     * @ORM\Column(name="updateattencance", type="integer", nullable=true)
     */
    private $updateattencance;

	 /**
     * @var boolean
     *
     * @ORM\Column(name="deleteattencance", type="integer", nullable=true)
     */
    private $deleteattencance;	
	 /**
	 * @var string
	 *
	 * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
	 */
	private $modulekey;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
	 */
	private $moduleinstance;
    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;

    
         /**
     * @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;
    function getId() {
        return $this->id;
    }

    function getEntity() {
        return $this->entity;
    }

    function getHroomid() {
        return $this->hroomid;
    }

    function getTimestart() {
        return $this->timestart;
    }

    function getTimeend() {
        return $this->timeend;
    }

    function getHourduration() {
         $fhour=new BadiuHourFormCast();
         $dhour= $fhour->convertToForm($this->hourduration);
         return $dhour;
    }

    function getIdnumber() {
        return $this->idnumber;
    }

    function getDescription() {
        return $this->description;
    }

    function getParam() {
        return $this->param;
    }

    function getTimecreated() {
        return $this->timecreated;
    }

    function getTimemodified() {
        return $this->timemodified;
    }

    function getUseridadd() {
        return $this->useridadd;
    }

    function getUseridedit() {
        return $this->useridedit;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEntity($entity) {
        $this->entity = $entity;
    }


    function setHroomid(\Badiu\Util\HousingBundle\Entity\UtilHousingRoom $hroomid) {
        $this->hroomid = $hroomid;
    }

    function setTimestart(\DateTime $timestart) {
        $this->timestart = $timestart;
    }

    function setTimeend(\DateTime $timeend) {
        $this->timeend = $timeend;
    }

    function setHourduration($hourduration) {
          $fhour=new BadiuHourFormCast();
        $dhour= $fhour->convertFromForm($hourduration);
        $this->hourduration = $dhour;
        
    }

    function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getStatusid() {
        return $this->statusid;
    }

    function getDsequence() {
        return $this->dsequence;
    }

    function setStatusid(AdminAttendancePlannedStatus $statusid) {
        $this->statusid = $statusid;
    }

    function setDsequence($dsequence) {
        $this->dsequence = $dsequence;
    }


    function getHourdurationexec() {
        return $this->hourdurationexec;
    }

    function getContentexec() {
        return $this->contentexec;
    }

    function setHourdurationexec($hourdurationexec) {
        $this->hourdurationexec = $hourdurationexec;
    }

    function setContentexec($contentexec) {
        $this->contentexec = $contentexec;
    }

    function getName() {
        return $this->name;
    }

    function getShortname() {
        return $this->shortname;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setShortname($shortname) {
        $this->shortname = $shortname;
    }
    function getDconfig() {
        return $this->dconfig;
    }

    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }

		
	 function getModulekey() {
        return $this->modulekey;
    }

    function getModuleinstance() {
        return $this->moduleinstance;
    }
	
	function setModulekey($modulekey) {
        $this->modulekey = $modulekey;
    }

    function setModuleinstance($moduleinstance) {
        $this->moduleinstance = $moduleinstance;
    }
	
	function setCriteria($criteria) {
        $this->criteria = $criteria;
    }

     public function getCriteria() {
        return $this->criteria;
    }
	
	 function setHourstart($hourstart) {
        $this->hourstart = $hourstart;
    }

    function setHourend($hourend) {
        $this->hourend = $hourend;
    }
	
	
    function getHourstart() {
        return $this->hourstart;
    }

    function getHourend() {
        return $this->hourend;
    }
	
	function setDayofweek($dayofweek) {
        $this->dayofweek = $dayofweek;
    }
    function getDayofweek() {
        return $this->dayofweek;
    }
	
	function setCalendar($calendar) {
        $this->calendar = $calendar;
    }
    function getCalendar() {
        return $this->calendar;
    }
	
	function setUpdateattencance($updateattencance) {
        $this->updateattencance = $updateattencance;
    }
    function getUpdateattencance() {
        return $this->updateattencance;
    }
	
		function setDeleteattencance($deleteattencance) {
        $this->deleteattencance = $deleteattencance;
    }
    function getDeleteattencance() {
        return $this->deleteattencance;
    }
}
