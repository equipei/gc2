<?php

namespace Badiu\Admin\MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminMessage
 *  
 * @ORM\Table(name="admin_message",
 *       indexes={@ORM\Index(name="admin_message_entity_ix", columns={"entity"}), 
 *              @ORM\Index(name="admin_message_discussionid_ix", columns={"discussionid"}), 
 *              @ORM\Index(name="admin_message_userid_ix", columns={"userid"}),
 *              @ORM\Index(name="admin_message_dtype_ix", columns={"dtype"}),
 *              @ORM\Index(name="admin_message_timecreatedcontent_ix", columns={"timecreatedcontent"}),
 *              @ORM\Index(name="admin_message_timemodifiedcontent_ix", columns={"timemodifiedcontent"}), 
 *              @ORM\Index(name="admin_message_deleted_ix", columns={"deleted"}),
 *              @ORM\Index(name="admin_message_modulekey_ix", columns={"modulekey"}),
 *              @ORM\Index(name="admin_message_moduleinstance_ix", columns={"moduleinstance"}), 
 *              @ORM\Index(name="admin_messagereferencekey_ix", columns={"referencekey"}), 
 *              @ORM\Index(name="admin_message_idnumber_ix", columns={"idnumber"})})
 * @ORM\Entity
 */
class AdminMessage
{
    /** 
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="entity", type="bigint", nullable=false)
     */
    private $entity;

	/**
     * @var AdminMessageDiscussion
     *
     * @ORM\ManyToOne(targetEntity="AdminMessageDiscussion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="discussionid", referencedColumnName="id")
     * })
     */
    private $discussionid;

	
	/**
     * @var \Badiu\System\UserBundle\Entity\SystemUser
     *
     * @ORM\ManyToOne(targetEntity="\Badiu\System\UserBundle\Entity\SystemUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id", nullable=false)
     * })
     */
    private $userid;

    /**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="text", nullable=true)
	 */
	private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="dtype", type="string", length=50, nullable=true)
     */
    private $dtype;
	    /**
     * @var string
     *
     * @ORM\Column(name="modulekey", type="string", length=255, nullable=true)
     */
    private $modulekey;
 
/**
	 * @var integer
	 *
	 * @ORM\Column(name="referencekey", type="bigint", nullable=true)
	 */
	private $referencekey;

    /**
   * @var integer
   *
   * @ORM\Column(name="moduleinstance", type="bigint", nullable=true)
     */
    private $moduleinstance;
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreatedcontent", type="datetime", nullable=true)
     */
    private $timecreatedcontent;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodifiedcontent", type="datetime", nullable=true)
     */
    private $timemodifiedcontent;
	
	
	/* @var string
     *
     * @ORM\Column(name="dconfig", type="text", nullable=true)
     */
    private $dconfig;  

    /**
     * @var string
     *
     * @ORM\Column(name="idnumber", type="string", length=255, nullable=true)
     */
      
    private $idnumber;
 
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="param", type="text", nullable=true)
     */
    private $param;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timecreated", type="datetime", nullable=false)
     */
    private $timecreated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timemodified", type="datetime", nullable=true)
     */
    private $timemodified;

     /**
     * @var integer
     *
     * @ORM\Column(name="useridadd", type="bigint", nullable=true)
     */
    private $useridadd;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="useridedit", type="bigint", nullable=true)
     */
    private $useridedit;
    
   
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted;

    public function getId() {
        return $this->id;
    }

    public function getEntity() {
        return $this->entity;
    }

  
    public function getIdnumber() {
        return $this->idnumber;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getParam() {
        return $this->param;
    }

    public function getTimecreated() {
        return $this->timecreated;
    }

    public function getTimemodified() {
        return $this->timemodified;
    }

    public function getUseridadd() {
        return $this->useridadd;
    }

    public function getUseridedit() {
        return $this->useridedit;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setIdnumber($idnumber) {
        $this->idnumber = $idnumber;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setParam($param) {
        $this->param = $param;
    }

    public function setTimecreated(\DateTime $timecreated) {
        $this->timecreated = $timecreated;
    }

    public function setTimemodified(\DateTime $timemodified) {
        $this->timemodified = $timemodified;
    }

    public function setUseridadd($useridadd) {
        $this->useridadd = $useridadd;
    }

    public function setUseridedit($useridedit) {
        $this->useridedit = $useridedit;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    function getContent() {
            return $this->content;
        }
 function setContent($content) {
            $this->content = $content;
        }


	  
    public function getDiscussionid() {
        return $this->discussionid;
    }

    public function setDiscussionid($discussionid) {
        $this->discussionid = $discussionid;
    }
 
 
    /**
     * @return \Badiu\System\UserBundle\Entity\SystemUser
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param \Badiu\System\UserBundle\Entity\SystemUser $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

	
    function getDconfig() {
        return $this->dconfig;
    }
	
	
    function setDconfig($dconfig) {
        $this->dconfig = $dconfig;
    }
	
		  
  /**
     * @return string
     */
    public function getModulekey()
    {
        return $this->modulekey;
    }

    /**
     * @param string $dtype
     */
    public function setModulekey($modulekey)
    {
        $this->modulekey = $modulekey;
    }


     /**
     * @return string
     */
    public function getModuleinstance()
    {
        return $this->moduleinstance;
    }

    /**
     * @param string $dtype
     */
    public function setModuleinstance($moduleinstance)
    {
        $this->moduleinstance = $moduleinstance;
    }
	
	
  /**
     * @return string
     */
    public function getReferencekey()
    {
        return $this->referencekey;
    }

    /**
     * @param string $referencekey
     */
    public function setReferencekey($referencekey)
    {
        $this->referencekey = $referencekey;
    }


    function getDtype() {
        return $this->dtype;
    }

  

    function setDtype($dtype) {
        $this->dtype = $dtype;
    }
	
	
  

}
