<?php

namespace Badiu\Admin\MessageBundle\Model\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Badiu\System\CoreBundle\Model\Functionality\BadiuModelLib;

class MessageManagerLib extends BadiuModelLib {

    function __construct(Container $container) {
        parent::__construct($container);
    }

	
	public function addMessage($param=null) {
		if(empty($param)){
			$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		}else if(is_array($param) && sizeof($param)==0){
			$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		}
		if(empty($param)){return null;}
		if(!is_array($param)){return null;}
		
		$messagedata=$this->getContainer()->get('badiu.admin.message.message.data');
		$content=$this->getUtildata()->getVaueOfArray($param,'content');
		$messageid=$this->getUtildata()->getVaueOfArray($param,'messageid');
		$discussionid=$this->getUtildata()->getVaueOfArray($param,'discussionid');
		$entity=$this->getEntity();
		//update
		if(!empty($messageid)){
			$uparam=array('id'=>$messageid,'content'=>$content,'timemodified'=>new \DateTime());
			if(!empty($discussionid)){$uparam['discussionid']=$discussionid;}
			$dresult=$messagedata->updateNativeSql($uparam,false);
			return $dresult;
		}
		//add new or update
		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		$modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		$moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		$dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
		$referencekey=$this->getUtildata()->getVaueOfArray($param,'referencekey');;
		if(empty($userid)){$userid=$this->getUserid();}
		
		$gparamadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'dtype'=>$dtype,'userid'=>$userid,'content'=>$content,'referencekey'=>$referencekey,'deleted'=>0,'timecreated'=>new \DateTime());
		if(!empty($discussionid)){$gparamadd['discussionid']=$discussionid;}
		$dresult=$messagedata->insertNativeSql($gparamadd,false);
		return $dresult; 
	}
	
	public function addDiscussion($param=null) {
		if(empty($param)){
			$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		}else if(is_array($param) && sizeof($param)==0){
			$param=$this->getContainer()->get('badiu.system.core.lib.http.querystringsystem')->getParameters();
		}
		if(empty($param)){return null;}
		if(!is_array($param)){return null;}
		
		$discussiondata=$this->getContainer()->get('badiu.admin.message.discussion.data');
		$content=$this->getUtildata()->getVaueOfArray($param,'content');
		$discussionid=$this->getUtildata()->getVaueOfArray($param,'discussionid');
		$entity=$this->getEntity();
		//update
		if(!empty($discussionid)){
			$uparam=array('id'=>$discussion,'content'=>$content,'timemodified'=>new \DateTime());
			$dresult=$discussiondata->updateNativeSql($uparam,false);
			return $dresult;
		}
		//add new or update
		$userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		$modulekey=$this->getUtildata()->getVaueOfArray($param,'modulekey');
		$moduleinstance=$this->getUtildata()->getVaueOfArray($param,'moduleinstance');
		$dtype=$this->getUtildata()->getVaueOfArray($param,'dtype');
		$referencekey=$this->getUtildata()->getVaueOfArray($param,'referencekey');;
		if(empty($userid)){$userid=$this->getUserid();}
		
		$gparamadd=array('entity'=>$entity,'modulekey'=>$modulekey,'moduleinstance'=>$moduleinstance,'dtype'=>$dtype,'userid'=>$userid,'content'=>$content,'referencekey'=>$referencekey,'deleted'=>0,'timecreated'=>new \DateTime());
		$dresult=$discussiondata->insertNativeSql($gparamadd,false);
		return $dresult; 
	}
}
