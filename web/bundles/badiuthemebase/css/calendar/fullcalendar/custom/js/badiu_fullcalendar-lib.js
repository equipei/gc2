$(document).ready(function() {

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'calendartitle today prev,next',
            center: '',
            right: 'agendaDay,agendaWeek,month,commitment'
        },
        views: {
            calendartitle: {
                type: 'agendaWeek',
                buttonText: 'Calendar'
            },
            commitment: {
                type: 'custom',
                buttonText: 'Atividades'
            }
        },
        viewRender: function(view, element) {
            $('.fc-toolbar').addClass('row');
            $('.fc-toolbar').find('button').addClass('btn btn-default');
            $('.fc-left').children().addClass('pull-left');
            /*$('.fc-left').find('.fc-prev-button').addClass('glyphicon glyphicon-menu-left');
             $('.fc-left').find('.fc-next-button').addClass('glyphicon glyphicon-menu-right');*/
            $('.fc-left').addClass('col-lg-6 pull-left');
            $('.fc-right').addClass('col-lg-4 pull-right');
            $('.fc-calendartitle-button').css({
                'color': 'red',
                'font-size': '20px',
                'box-shadow': 'none',
                'border': 'none',
                'margin-top': '-8px',
            });
            if (!$('#header-title').length) {
                $('.fc-left').append('<span class"pull-left" id="header-title" >' + view.title + '</span>');
            } else {

                $('#header-title').text(view.title);
            }
            //col-lg-2 table-bordered

            $('.fc-day-header').addClass('table-bordered');
            //element.children().addClass('table');
            //element.find('.fc-body').

            console.log(element);
        },
        events: {
            url: badiuCalendarList,
            error: function() {
                $('#script-warning').show();
            }
        },
        loading: function(bool) {
            $('#loading').toggle(bool);
        },
        // Convert the allDay from string to boolean
        eventRender: function(event, element, view) {
            if (event.allDay === 'true') {
                event.allDay = true;
            } else {
                event.allDay = false;
            }
        },
        selectable: true,
        selectHelper: true,
        select: function(start, end, allDay) {
            var title = prompt('Titulo do Evento:');
            var url = prompt('Url do evento Caso Exista:');
            if (title) {
                start = moment(start).format('YYYY/MM/DD');
                end = moment(end).format('YYYY/MM/DD');
                $.ajax({
                    url: badiuCalendarAdd,
                    data: 'title=' + title + '&start=' + start + '&end=' + end + '&url=' + url,
                    type: "POST",
                    success: function(json) {
                        alert('Adicionado com Sucesso');
                    }
                });
                calendar.fullCalendar('renderEvent', {
                        title: title,
                        start: start,
                        end: end,
                        allDay: allDay
                    },
                    true // make the event "stick"
                );
            }
            calendar.fullCalendar('unselect');
        },
          editable: true,
      eventDrop: function(event, delta) {
            var start = moment(start).format('YYYY/MM/DD');
            var end = moment(end).format('YYYY/MM/DD');
            $.ajax({
                url: badiuCalendarUpdate,
                data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,
                type: "POST",
                success: function(json) {
                    alert("Atualizado com Sucesso");
                }
            });
        },
        eventResize: function(event) {
            var start = moment(start).format('YYYY/MM/DD');
            var end = moment(end).format('YYYY/MM/DD');
            $.ajax({
                url: badiuCalendarUpdate,
                data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,
                type: "POST",
                success: function(json) {
                    alert("Atualizado com Sucesso");
                }
            });
        },
        eventClick: function(event) {
            var decision = confirm("Desejas Continuar");
            if (decision) {
                $.ajax({
                    url: badiuCalendarDelete,
                    data: "&id=" + event.id,
                    type: "POST",
                });
                $('#calendar').fullCalendar('removeEvents', event.id);
            } else {}
        },

        lang: 'pt-br',
        //timeFormat: 'H:mm'
    });
});