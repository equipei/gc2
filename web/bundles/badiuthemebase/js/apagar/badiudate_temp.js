
function badiudatetime_init(selector, format){
    obj = undefined;
    if ($(selector).is('input')) {
       // $(selector).closest('div').css('position','relative');
        obj = $(selector);
    } else {
        obj = $(selector);
    }
    obj.datetimepicker({
        locale: 'pt-br',
        format: format,
		useCurrent: false
    });
}

$(function () {
    badiudatetime_init(".badiudate_datetime", "DD/MM/YYYY LT");
    badiudatetime_init(".badiudate_time", "LT");
    badiudatetime_init(".badiudate", "DD/MM/YYYY");
});
