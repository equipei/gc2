$('#option-data').hide();
$(document).ready(function () {
    $('.badiudatetime').datetimepicker({
        locale: 'pt-br',
        format: 'DD/MM/YYYY'
    });
});

function showfield() {
    var valorEscolhido;
    $("select[name$='badiu_system_core_form_type[coursetimestart1][field1]']").change(function () {
        // obtendo o valor do atributo value da tag option
        valorEscolhido = $("#badiu_system_core_form_type_coursetimestart1_field1 option:selected").text();
        // exibindo uma janela com o valor selecionado
       // alert(valorEscolhido);
        if (valorEscolhido === 'periodo') {
            $('#option-data').show();
        } else {
            $('#option-data').hide();
        }
    });
}
showfield();