$(document).ready(function () {

//alert(options);

 if (typeof options != "undefined"){
     badiu_options(options);
 }


function badiu_options(type){

    $("#accordion").accordion({fillSpace: true, icons: {'header': 'ui-icons-plus', 'headerselected': 'ui-icon-minus'}, collapsible: true, active: 2});

    var headers = $('#accordion .accordion-header');

 if (type == 'allclosed' ) {
 var contentAreas = $('#accordion .ui-accordion-content ').hide().end();

 }else if (type == 'allopen') {

        var contentAreas = $('#accordion .ui-accordion-content ').hide().show().end();
 }else if (type == 'firstopen') {

           var contentAreas = $('#accordion .ui-accordion-content ').hide().first().show().end();
 }else if (type == 'lastopen') {

              var contentAreas = $('#accordion .ui-accordion-content ').hide().last().show().end();
 }

    var expandLink = $('.accordion-expand-all');

    // add the accordion functionality
    headers.click(function () {
        // close all panels
        contentAreas.slideUp();
        // open the appropriate panel
        $(this).next().slideDown();
        // reset Expand all button
        expandLink.text('Expandir tudo').data('isAllOpen', false);
        // stop page scroll
        return false;
    });

    // hook up the expand/collapse all
    expandLink.click(function () {
        var isAllOpen = !$(this).data('isAllOpen');
        console.log({isAllOpen: isAllOpen, contentAreas: contentAreas})
        contentAreas[isAllOpen ? 'slideDown' : 'slideUp']();

        expandLink.text(isAllOpen ? 'Contrair tudo' : 'Expandir tudo').data('isAllOpen', isAllOpen);
    });

}


    //this function is responsable to add and remove class
    remove_addclass();
    function remove_addclass() {
        //when btn_send are clicked
        $('.btn_send').click(function () {
            //remove class div footer
            $(".btn_footer").remove();
            // add div class form in footer in html format
            $(".footer_form").append('<div class="span12 footer_form_line"><form><div class="form-group"><div class="footer_title"><label for=" Email1msg">Resposta:</label></div><textarea class="form-control" rows="5"></textarea></div><button type="submit" class="btn btn-primary btn_form">Enviar</button><button type="submit" class="btn btn-primary">Cancelar</button></form></div>');
        });
    }

});
