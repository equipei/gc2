$(document).ready(function(){

var FC = $.fullCalendar; // a reference to FullCalendar's root namespace
		var View = FC.View;      // the class that all views must inherit from
		var CustomView;          // our subclass

		CustomView = View.extend({ // make a subclass of View
			type: null, // subclass' view name (string)
			name: null, // deprecated. use `type` instead
			title: null, // the text that will be displayed in the header's title

			calendar: null, // owner Calendar object
			options: null, // hash containing all options. already merged with view-specific-options
			coordMap: null, // a CoordMap object for converting pixel regions to dates
			el: null, // the view's containing element. set by Calendar

			isDisplayed: false,
			isSkeletonRendered: false,
			isEventsRendered: false,

			// range the view is actually displaying (moments)
			start: null,
			end: null, // exclusive

			// range the view is formally responsible for (moments)
			// may be different from start/end. for example, a month view might have 1st-31st, excluding padded dates
			intervalStart: null,
			intervalEnd: null, // exclusive
			intervalDuration: null,
			intervalUnit: null, // name of largest unit being displayed, like "month" or "week"

			isSelected: false, // boolean whether a range of time is user-selected or not

		    initialize: function() {
		        // called once when the view is instantiated, when the user switches to the view.
		        // initialize member variables or do other setup tasks.

		    },

		    render: function() {
		        // responsible for displaying the skeleton of the view within the already-defined
		        // this.el, a jQuery element.
		        this.el.html(this.renderHtml());


				//$('#calendar').fullCalendar('updateEvent', event);

		       //console.log(this);
		    },


			displayEvents: function(events) {
				//var scrollState = this.queryScroll();

				this.clearEvents();
				this.isEventsRendered = true;
				this.renderEvents(events);
				//this.setScroll(scrollState);
				this.triggerEventRender();
			},

			// Does everything necessary to clear the view's currently-rendered events
			clearEvents: function() {
				if (this.isEventsRendered) {
					this.triggerEventDestroy();
					this.destroyEvents();
					this.isEventsRendered = false;
				}
			},

					// Signals that all events have been rendered
			triggerEventRender: function() {
				this.renderedEventSegEach(function(seg) {
					this.trigger('eventAfterRender', seg.event, seg.event, seg.el);
				});
				this.trigger('eventAfterAllRender');
			},

			// Signals that all event elements are about to be removed
			triggerEventDestroy: function() {
				this.renderedEventSegEach(function(seg) {
					this.trigger('eventDestroy', seg.event, seg.event, seg.el);
				});
			},

		    setHeight: function(height, isAuto) {
		        // responsible for adjusting the pixel-height of the view. if isAuto is true, the
		        // view may be its natural height, and `height` becomes merely a suggestion.
		    },

		    renderEvents: function(events) {
		        // reponsible for rendering the given Event Objects
		        if (this.isEventsRendered) {
			        var content = '';
			        var diateste = 0;
			     	events.sort(function(a,b){
			     		var formattedDateA = new Date(a.start._i);
			     		var formattedDateB = new Date(b.start._i);
			     		var A = new Date(formattedDateA.getFullYear()+'/'+(formattedDateA.getMonth()+1)+'/'+formattedDateA.getDate());
			     		var B = new Date(formattedDateB.getFullYear()+'/'+(formattedDateB.getMonth()+1)+'/'+formattedDateB.getDate());
			     		console.log(A);

			     		return A - B;
			     	});

			     		var dateEqual = new Array();
			     		var newEventList = new Array();
			     		var formattedDateB = null;// new Date(events[i].start._i);
			     		var aux1 = 0;
			     		var aux2 = 0;
			     		var equalLastEvent = false;
			     		for (var i = 0; i < events.length; i++) {
			     			//console.log(events);
			     			var formattedDateA = new Date(events[i].start._i);
			     			if(formattedDateB != null){
			     				if(formattedDateA.getDate() === formattedDateB.getDate() ){
			     					dateEqual[aux1]=events[i];
			     					aux1++;
				     				//console.log(dateEqual);
				     			}
				     			if(i < events.length-1){
					     			if(new Date(events[i].start._i).getDate() != formattedDateB.getDate() ){

					     				if (aux1>0){
					     					aux2=aux2-1;
					     					dateEqual.push(newEventList [aux2]);
					     					newEventList [aux2] = dateEqual;
					     					dateEqual = null;
					     					dateEqual = new Array();
					     					//console.log(newEventList);
					     					aux1=0;
					     					aux2++;
					     				}else{
					     					aux2++;
					     					newEventList [aux2] = events[i];

					     				}


						     		}
						     	}else{

				     				if (aux1>0){
				     					aux2=aux2-1;
				     					dateEqual.push(newEventList [aux2]);
				     					newEventList [aux2] = dateEqual;
				     					dateEqual = null;
				     					dateEqual = new Array();
				     					//console.log(newEventList);
				     					aux1=0;

				     				}else{
				     					aux2++;
				     					newEventList [aux2] = events[i];

				     				}
						     	}
			     			}else{
			     				newEventList[aux2]=events[i];
			     			}



			     			formattedDateB = new Date(events[i].start._i);
			     		}

			     		console.log(newEventList);



			       	for (var i = 0; i < newEventList.length; i++) {


			       		if (newEventList[i] instanceof Array) {
			       			console.log(newEventList[i][0]);
			       			formattedDate = new Date(newEventList[i][0].start._d);
			       		}else{
			       			formattedDate = new Date(newEventList[i].start._d);
			       		}
				        	content+= '<div class="row">' +
											'<table class="table border">' +
												'<tbody>' +
													'<tr>'+
														'<td class="col-lg-2" >'+
															'<span class="text">'+
																'<a href="#">'+formattedDate.toDateString()+'</a>'+
															'</span>'+
														'</td>'+
														'<td class="col-lg-11" >';

			       		if (newEventList[i] instanceof Array) {
			       			for (var j = 0; j < newEventList[i].length; j++) {
			       				formattedDate = new Date(newEventList[i][j].start._d);

			       				content+=	'<div class="row">'+
											'<div class="col-md-2">'+
												'<span class="text">'+
													'<a href="#">'+((newEventList[i][j].allDay)?'Dia inteiro':formattedDate.toTimeString())+
													'</a>'+
												'</span>'+
											'</div>'+
											'<div class="col-md-6">'+
												'<table class="">'+
													'<thead>'+
													'<tr class="">'+

		                                                '<td>'+
		                                                    '<span class="icon1">'+
		                                                    	'<i class="fa fa-birthday-cake fa-lg"></i>'+
		                                                    '</span>'+
		                                                    '<span class="text">'+
		                                                    	'<i class="fa fa-plus-circle icone event-expand"></i>'+
		                                                    	'<a class="event-expand" href="#">'+
		                                                    	newEventList[i][j].title+
		                                                    	'</a>'+
		                                                        '<table class="hide expancible">'+
		                                                            '<thead>'+
		                                                            	'<tr class="">'+
		                                                            		'<td class="col-lg ">'+newEventList[i][j].title+
		                                                            		'</td>'+
		                                                            	'</tr>'+
		                                                            '</thead>'+
		                                                            '<tbody>'+
		                                                            	'<tr class="">'+
		                                                            	    '<td class="col-lg ">'+
		                                                            	    	'<a href="#" class="pull-left">copiar para minha agenda</a>'+
		                                                            	    	'<a href="#" class="pull-right">Mais detalhes »</a>'+
		                                                            	    '</td>'+
		                                                            	'</tr>'+
		                                                            '</tbody>'+
		                                                        '</table>'+
		                                                    '</span>'+
		                                                '</td>'+
		                                            '</tr>'+
													'</thead>'+
												'</table>'+
											'</div>'+
										'</div>';

			       			}

			        		//console.log('array');
			       		}else{

							content+=	'<div class="row">'+
											'<div class="col-md-2">'+
												'<span class="text">'+
													'<a href="#">'+((newEventList[i].allDay)?'Dia inteiro':formattedDate.toTimeString())+
													'</a>'+
												'</span>'+
											'</div>'+
											'<div class="col-md-6">'+
												'<table class="">'+
													'<thead>'+
													'<tr class="">'+

		                                                '<td>'+
		                                                    '<span class="icon1">'+
		                                                    	'<i class="fa fa-birthday-cake fa-lg"></i>'+
		                                                    '</span>'+
		                                                    '<span class="text">'+
		                                                    	'<i class="fa fa-plus-circle icone event-expand"></i>'+
		                                                    	'<a class="event-expand" href="#">'+
		                                                    	newEventList[i].title+
		                                                    	'</a>'+
		                                                        '<table class="border hide expancible">'+
		                                                            '<thead>'+
		                                                            	'<tr class="">'+
		                                                            		'<td class="col-lg ">'+newEventList[i].title+
		                                                            		'</td>'+
		                                                            	'</tr>'+
		                                                            '</thead>'+
		                                                            '<tbody>'+
		                                                            	'<tr class="">'+
		                                                            	    '<td class="col-lg ">'+
		                                                            	    	'<a href="#" class="pull-left">copiar para minha agenda</a>'+
		                                                            	    	'<a href="#" class="pull-right">Mais detalhes »</a>'+
		                                                            	    '</td>'+
		                                                            	'</tr>'+
		                                                            '</tbody>'+
		                                                        '</table>'+
		                                                    '</span>'+
		                                                '</td>'+
		                                            '</tr>'+
													'</thead>'+
												'</table>'+
											'</div>'+
										'</div>';


			       		}

			       		content+=					'</td>'+
													'</tr>'
												'</tbody>' +
											'</table>' +
										'</div>';

			        };

			        $('#comprimise').append(content);

			        //funcao escuta o click para expandir detalhes da tabela
					$('.event-expand').click(function(){

						if($(this).hasClass('all')){//testa se o click foi de elemento expandir tudo

							if(!$(this).hasClass('expanded')){//testa se os os detalhes estao fechados

								$('.expancible').removeClass('hide');//fecha os detalhes


								$('.icone').removeClass('fa-plus-circle').addClass('fa-minus-circle');//muda sinal do icone de + para -


							}else{//caso os detalhes estejam a mostra

								$('.expancible').addClass('hide');//esconde os detalhes

								$('.icone').removeClass('fa-minus-circle').addClass('fa-plus-circle');//muda sinal do icone de - para +
							}
						}else{//caso o click foi de elemento expandir especifico

							if($(this).parent().find('table.hide').length){//testa se os os detalhes estao fechados
								$(this).parent().find('table').removeClass('hide');//mostra os detalhes
								$(this).parent().find('.icone').removeClass('fa-plus-circle').addClass('fa-minus-circle');//muda sinal do icone de + para -

							}else{//caso os detalhes estejam a mostra
								$(this).parent().find('table').addClass('hide');//esconde os detalhes
								$(this).parent().find('.icone').removeClass('fa-minus-circle').addClass('fa-plus-circle');//muda sinal do icone de - para +
							}
						}
						return false;
					});
				};
		       //console.log(events);
		    },

		    destroyEvents: function() {
		        // responsible for undoing everything in renderEvents
		    },

		    renderSelection: function(range) {
		        // accepts a {start,end} object made of Moments, and must render the selection
		    },

		    destroySelection: function() {
		        // responsible for undoing everything in renderSelection
		    },

		    renderHtml: function() {

				return '<div class="row">'+
							'<div class="col-lg-12">'+
								'<span class="text">'+
									'<a class="event-expand all" href="#">Expandir Tudo</a>'+
								'</span>'+
								'<span class="text">'+
									'<a class="event-expand all expanded" href="#">Recolher Tudo</a>'+
								'</span>'+
								'<span class="text pull-right">'+
									'<span class="icon-desc">'+
										'<a title="seguinte" href="#">'+
											'<i class="fa fa-sort-desc fa-lg"></i>'+
										'</a>'+
									'</span>'+
									'<span class="text">'+
										'<a title="anterior" href="#">'+
											'<i class="fa fa-sort-asc fa-lg"></i>'+
										'</a>'+
									'</span>'+
								'</span>'+
							'</div>'+
						'</div>' +
					'<div class="comprimessed" id="comprimise">' +


					'</div>';
			},


		});

		FC.views.custom = CustomView; // register our class with the view system



});

