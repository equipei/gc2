/* global recipient */

$(document).ready(function () {
    var id;
      if (typeof badiuModalTableDeleteUrlService == "undefined") {
            badiuModalTableDeleteUrlService='';
     }

    $('#badiu_modal_table_delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        _service = button.data('_service');
        bkey = button.data('bkey');
        _function = button.data('_function');
        id = button.data('id');

        var modal = $(this);
    }),
       $('.btn-badiu-reporter-send').click(function () {
        $.ajax({
            url: badiuModalTableDeleteUrlService,
            dataType: 'json',
            method: "get",
            data: {
                _service: _service,
                bkey: bkey,
                _function: _function,
                id: id

            },
            beforeSend: function () {
                $("#loader-ajax-img").html("<img src={{ asset('bundles/badiuthemedefault/images/ajax-loader.gif') }}>");
            },
            success: function (data) {
                //console.log(data);
                $("#msg-box").append("<div class='alert alert-success alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span></button>Registo removido com sucesso... </div>").hide().fadeIn(2000);
                setTimeout(function () {
                    location.reload();
                }, 3000);
                $('#alert').remove();
            },
            complete: function () {
                $("#loader-ajax-img").remove();
            },
            error: function () {
                $("#msg-box").append("<div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span></button> Ocorreu um erro na requisição ajax... </div>").hide().fadeIn(2000);
                $('#alert').remove();
            }
        });


    });

});

