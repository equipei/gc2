
function autocomplete_newitem(elem){
    if (!(elem instanceof jQuery)) {
        elem = $(elem).closest('span').find('input[name]');
    }
    elem.blur();
    var autocompleteService = undefined;
    $.map($(elem).attr('class').split(' '), function (val, i) {
        if (/(autocomplete-service-[A-Z]*)\w+/.test(val)) {
            autocompleteService = val;
        }
    });
    $.ajax({
        url: 'ajax.php',
        type: 'POST',
        data: {
            s: $(elem).val(),
            q: autocompleteService
        },
        dataType: 'json',
        success: function(data){
            $('input[name="__'+$(elem).attr('name')+'"]').val(data.id);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

$(document).ready(function () {
    var autocompleteService = undefined;
    var autocompleteSource = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'ajax.php',
            prepare: function (query, settings) {
                if (autocompleteService != undefined) {
                    settings.url = settings.url + '?s=' + autocompleteService;
                }
                if (query != undefined) {
                    settings.url = settings.url + '&q=' + encodeURIComponent(query);
                }
                return settings;
            }
        }
    });
    $('.autocomplete').addClass('typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 0
    },
    {
        name: $(this).attr('name'),
        display: 'name',
        limit: 10,
        source: autocompleteSource,
        templates: {
            notFound: '<div class="tt-suggestion empty-message">Nenhum resultado</div>',
            pending: '<div class="tt-suggestion empty-message">Carregando</div>'
        }
    }).bind('typeahead:active', function () {
        $.map($(this).attr('class').split(' '), function (val, i) {
            if (/(autocomplete-service-[A-Z]*)\w+/.test(val)) {
                autocompleteService = val;
            }
        });
    }).bind('typeahead:render', function () {
        if ($(this).val().length > 1 && !$(this).hasClass('autocomplete-static')) {
            $(this).next().next().find('.tt-dataset').append('<div class="tt-suggestion tt-selectable tt-newitem" onclick="autocomplete_newitem(this);">Adicionar <strong class="tt-highlight">' + $(this).val() + '</strong></div>');
        }
    }).bind('typeahead:select', function (event, suggestion) {
        $('input[name="__'+$(this).attr('name')+'"]').val(suggestion.id);
    }).on('keypress', function (e) {
        if (e.which == 13 && $(this).parent('span').find('.tt-newitem').hasClass('tt-cursor')) {
            e.preventDefault();
            autocomplete_newitem($(this));
        }
    });
});
