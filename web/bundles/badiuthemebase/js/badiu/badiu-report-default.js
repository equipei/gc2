/*
 Created on : 20/10/2016, 11:48:29
 Updated on : 27/02/2011, 11:48:29
 Author     : Humberto Lopes
 */
//Formulario de pesquisa inicializa escondido
//var general = 'true';
$('.badiu-search-first').hide();
$('.badiu-search-other').hide();
$('.badiu-search-change-icon').hide();


$(document).ready(function() {

   var idSavedStatus = typeof Cookies.get('collapseStatus') !== 'undefined' ? Cookies.get('collapseStatus').split(',') : [];

    if (typeof badiuReporterFilterSearchOptions == "undefined") {
        badiuReporterFilterSearchOptions = 'single';
    }

    function badiuAccordionControlFirst(index) {

        $.each(index, function(i, val) {

            el = $('.badiu-item-other fieldset legend a')[val];

            el.click();

        });
    }

    function badiuAccordionControlALl(type) {

        if (type == 'advanced') {

            setTimeout(function() {
                teste = $('.badiu-btn-search a');
               // teste.click();
                //isso eh para abrir a primeira aba

                 if (idSavedStatus ==''){
                badiuAccordionControlFirst([0]);

            }
            }, 300);

        } else if (type == 'allfields') {
            setTimeout(function() {
                teste = $('.badiu-btn-search a');

                //teste.click();
                var collection = $('.badiu-item-other fieldset legend a');

                if (idSavedStatus ==''){

                   collection.each(function(val) {
                                collection[val].click();
                                });

                }


            }, 300);

        } else if (type == 'single') {

            // $(".badiu-btn-search").click(function(){
            //     var collection = $('.badiu-item-other fieldset legend a');

            //     collection[0].click();

            // });

        };

    }

    badiuAccordionControlALl(badiuReporterFilterSearchOptions);

    //escuta evento do click no btn pesquisa avancada
    $('.badiu-btn-search a').click(function(e) {
        e.preventDefault();
        $(".badiu-search-first").fadeToggle("slow", "linear");
        //mostra classe com icons
        $(".badiu-search-change-icon").fadeToggle("slow", "linear");
        // esconde todos os fieldset execpto primeiro
        $(".badiu-search-other").fadeToggle("slow", "linear");
        //verifica se existe classe triangle bottom com icone arrow no elemento
        if ($("#advanced-search").hasClass("glyphicon-triangle-bottom")) {
            //caso existe vai ser  removido e adicionado novo classe
            $('#advanced-search').removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
        } else {
            $('#advanced-search').removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
        }

        if (!$(this).hasClass('open')) {
            console.log('abriu');
            Cookies.set('advancedSearch', 'firstcontroll', {expires: 7,
                path: window.location.href
            });
        } else {
            console.log('fechou');

            Cookies.remove('advancedSearch');
        }
        $(this).toggleClass('open');

    });
    if (typeof Cookies.get('advancedSearch') !== 'undefined') {
        console.log('existe cookies');
        $('.badiu-btn-search a').click();
    }

    // change arrow type
    $('fieldset legend a').not('.expand-aria, .close-aria').click(function() {
        // using triangles to collapse
        if ($(this).attr('aria-expanded') == 'false') {
            $(this).children('span').removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
        } else {
            $(this).children('span').removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
        }
    });
    //btn- open-close-all-cllapse
    $('#badiu-btn-click').click(function(e) {
        e.preventDefault();
        if ($(this).hasClass("open-all")) {
            $('fieldset legend a').each(function() {
                console.log($(this).attr('aria-expanded'));
                if ($(this).attr('aria-expanded') === 'false')
                    $(this).click();
            });
        } else {
            $('fieldset legend a').each(function() {
                if ($(this).attr('aria-expanded') === 'true')
                    $(this).click();
            });
        }
    });
    $('fieldset legend a').click(function() {
        total = $('fieldset legend a').length;
        count = 0;
        if ($(this).attr('aria-expanded') === 'false')
            count++;
        else
            count--;
        $('fieldset legend a').each(function() {
            if ($(this).attr('aria-expanded') === 'true')
                count++;
        });
        //console.log(total);
        //console.log(count);
        if (total === count) {
            $("#badiu-btn-click").removeClass("open-all").addClass("closse-all").html('<i class="glyphicon glyphicon-minus"></i>');
        } else if (count === 0) {
            $("#badiu-btn-click").removeClass("closse-all").addClass("open-all").html('<i class="glyphicon glyphicon-plus"></i>');
        }
    });
    //funcao para chekar chekbox
    jQuery('#master').on('click', function(e) {
        if ($(this).is(':checked', true)) {
            $(".sub_chk").prop('checked', true);
        } else {
            $(".sub_chk").prop('checked', false);
        }
    });


    $('.badiu-search-other > fieldset > legend > a').each(function() {
        for (var i in idSavedStatus) {
            if ($(this).attr('href') == '#' + idSavedStatus[i]) {
                $(this).click();
                break;
            }
        }
    });

    //  save collapse status
    $('.badiu-form-search-next.collapse').on('shown.bs.collapse', function() {
        console.log('abriu');

        var id = $(this).attr('id');
         var teste1 = 'badiu-collapse-r';
        // searh in array
        if ($.inArray(id, idSavedStatus) == -1) {
            idSavedStatus.push(id);
             idSavedStatus.push(teste1);
            // console.log('array -- ' + idSavedStatus);
        }

        Cookies.set('collapseStatus', idSavedStatus, {expires: 7,
            path: window.location.href
        });
        console.log('cookies atualizados' + Cookies.get('collapseStatus'));

    }).on('hidden.bs.collapse', function() {
        console.log('fechou');

        var id = $(this).attr('id');

        var elementIndex = $.inArray(id, idSavedStatus);
        if (elementIndex !== -1) {
            idSavedStatus.splice(elementIndex, 1); //remove item from array
            // console.log('array -- ' + idSavedStatus);
        }

        Cookies.set('collapseStatus', idSavedStatus, {expires: 7,
            path: window.location.href
        });
        console.log('cookies atualizados' + Cookies.get('collapseStatus'));

    });

});