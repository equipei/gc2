$(document).ready(function() {

  // nav auto indent list
  // $('ul.root-menu').each(function() {
  //
  //   $(this).find('li > a').each(function(index) {
  //     $(this).css('paddingLeft', 10 * index + 10);
  //     // search normal menu
  //     if ($(this).closest('li').find('ul li a').children()[0] === undefined){
  //       $(this).closest('li').find('ul li a').addClass('regular-link');
  //       $('nav ul .regular-link').css('paddingLeft', 10 * index * 2);
  //     }
  //   });
  //
  // });

  // nav active menu
  $('nav a').click(function() {
    if ($(this).attr('aria-expanded') == 'false') {
      // $('nav ul li a').css('borderLeftColor', 'white');
      // $(this).css('borderLeftColor', '#337ab7');
      $('nav a').css('background', 'white');
      $(this).css('background', '#F4F4F7');
    } else if ($(this).attr('aria-expanded') == 'true') {
      // $('nav ul li a').css('borderLeftColor', 'white');
      $('nav a').css('background', 'white');
    } else { // regular-link
      // $('nav ul li a').css('borderLeftColor', 'white');
      // $(this).css('borderLeftColor', '#337ab7');
      $('nav a').css('background', 'white');
      $(this).css('background', '#F4F4F7');
    }
  });

  //mobile-menu
  $('.mobile-menu').click(function(){
    ($('nav.navbar').hasClass('fadeInLeft'))? $('nav.navbar').removeClass('fadeInLeft').addClass('fadeOutLeft') : $('nav.navbar').removeClass('fadeOutLeft').addClass('fadeInLeft');
  });


  var mainOldClass;
  $('.side-nav h4 .glyphicon-minus').click(function() {
    $('.side-nav').hide();
    mainOldClass = $('main').attr('class');
    $('main').removeAttr('class').addClass('col-sm-12');
    // console.log(mainOldClass);
    $('.sidenav-open').show();
  });

  $('.sidenav-open').click(function() {
    $('.side-nav').show();
    $('main').removeAttr('class').addClass(mainOldClass);
    $(this).hide();
  });


  // expand all closed arias link functionality
  $('.expand-aria').click(function() {
    $('fieldset legend a').each(function() {
      if ($(this).attr('aria-expanded') == 'false') {
        $(this).trigger('click');
        $('.expand-aria').hide();
        $('.close-aria').show();
      }
    });
  });

  // close all opened arias link functionality
  $('.close-aria').click(function() {
    $('fieldset legend a').each(function() {
      if ($(this).attr('aria-expanded') == 'true') {
        $(this).trigger('click');
        $('.expand-aria').show();
        $('.close-aria').hide();
      }
    });
  });

});
