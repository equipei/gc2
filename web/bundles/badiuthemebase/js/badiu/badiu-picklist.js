$(document).ready(function () {
    
    if ((typeof availableList != "undefined") && (typeof enableList != "undefined" )){
       //availableList=availableList.split("amp;").join('');
	   //enableList=enableList.split("amp;").join('');
       pickList(availableList, 'unreg');
       pickList(enableList, 'reg');
    
    } 
    
    //Pick list from file
    function pickList (url, type) {
		url=url.split("amp;").join('');
		
        $.ajax({
            url: url,
            dataType: 'json'
        }).success(function (data) {

           if (type == 'reg' ) {
              // Add registered users to list
            $.each(data['message']['rows'][1], function (key, value) {
                $('#list-in').append('<option value="' + value.id + '">' + value.name + '</option>');
            });

           // console.log(data[1][2].name);

          } else if (type == 'unreg') {

             // Add unregistered users to list
            $.each(data['message']['rows'][1], function (key, value) {
                $('#list-out').append('<option value="' + value.id + '">' + value.name + '</option>');
            });
              
           // console.log(data);
          }

        });
    }

    // Add to registered list
    $('#add-to-register-list').click(function () {
        var addedToRegisterList = {};
        $('#list-out option:selected').each(function (index, el) {
            addedToRegisterList[index] = {
                id: $(this).attr('value'),
                field1: $('#field1-selection-box option:selected').attr('value'),
                field2: $('#field2-selection-box option:selected').attr('value'),
                field3: $('#field3-selection-box option:selected').attr('value'),
                field4: $('#field4-selection-box option:selected').attr('value'),
                field5: $('#field5-selection-box option:selected').attr('value')
            };
            $('#list-in').append($(this)[0]);
        });
        console.log(addedToRegisterList);
		updateList=updateList.split("amp;").join('');
        $.ajax({
            url: updateList,
            type: 'GET',
            data: {add: addedToRegisterList}
        }).success(function (data) {
            console.log(data);
        });


    });

    // Remove from registered list
    $('#remove-from-register-list').click(function () {
        var removedFromRegisterList = [];
        $('#list-in option:selected').each(function () {
            removedFromRegisterList.push($(this).attr('value'));
            $('#list-out').append($(this)[0]);
        });
        console.log(removedFromRegisterList);
		updateList=updateList.split("amp;").join('');
        $.ajax({
            url: updateList,
            type: 'GET',
            data: {rmv: removedFromRegisterList}
        }).success(function (data) {
            console.log(data);
        });

    });


});
