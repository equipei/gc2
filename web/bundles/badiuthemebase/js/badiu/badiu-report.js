
// show and hide expanding link
$('form fieldset legend a').click(function() {
  openedArias = 1;
  closedArias = 1;
  totalArais = $('form fieldset legend a').length;
  // check open aria
  $('form fieldset legend a').each(function() {
    if ($(this).attr('aria-expanded') == 'true')
      openedArias++;
    else
      closedArias++;
  });
  // console.log(openedArias + ' - ' + closedArias);
  if (openedArias == totalArais) {
    $('.expand-aria').hide();
    $('.close-aria').show();
    openedArias = 1;
  } else if (closedArias == totalArais) {
    $('.expand-aria').show();
    $('.close-aria').hide();
    closedArias = 1;
  }
});

// change arrow type
$('a').not('.expand-aria, .close-aria').click(function() {
  // using triangles to collapse
  if ($(this).attr('data-toggle') == 'collapse' && !$(this).hasClass('btn-advanced-search')) {
    if ($(this).attr('aria-expanded') == 'false') {
      $(this).children('span').removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
    } else {
      $(this).children('span').removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
    }
  }
  // using plus and minis icons to collapse
  else if ($(this).hasClass('btn-advanced-search')) {
    if ($(this).attr('aria-expanded') == 'false') {
      $('.search-form .form-group.first div').not('.search-form .form-group.first div:last, .search-form .form-group.first div:first').hide('slow');
      $(this).children('span').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    } else {
      $('.search-form .form-group.first div').not('.search-form .form-group.first div:last, .search-form .form-group.first div:first').show('slow');
      $(this).children('span').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    }

  }
});

// expand all closed arias link functionality
$('.expand-aria').click(function() {
  $('fieldset legend a').each(function() {
    if ($(this).attr('aria-expanded') == 'false') {
      $(this).trigger('click');
      $('.expand-aria').hide();
      $('.close-aria').show();
    }
  });
});

// close all opened arias link functionality
$('.close-aria').click(function() {
  $('fieldset legend a').each(function() {
    if ($(this).attr('aria-expanded') == 'true') {
      $(this).trigger('click');
      $('.expand-aria').show();
      $('.close-aria').hide();
    }
  });
});
