$(document).ready(function () {
    $('#loader').hide();
    if ((typeof availableList != "undefined") && (typeof enableList != "undefined")) {
        search(availableList, 'unreg');
        search(enableList, 'reg');
    }
    function search(url, type) {
		url=url.split("amp;").join('');
        if (type == 'reg') {

            $('#search_enable').keyup(function () {
                var txt = $(this).val();
                var n = txt.length;
                if ((n >= 3) || (n == 0)) {

                    $.ajax({
                        url: url,
                        method: "get",
                        data: {
                            gsearch: txt
                        },
                        dataType: 'json',
                        success: function (data) {

                            $('#list-in').empty();
                            $.each(data['message']['rows'][1], function (key, value) {
                                $('#list-in').append('<option value="' + value.id + '">' + value.name + '</option>');
                            });
                        }
                    });
                } else {
                    $('#result').html('');
                }
            });

            } else if (type == 'unreg') {

            $('#search_available').keyup(function () {
                var txt = $(this).val();
                var n = txt.length;
                if ((n >= 3) || (n == 0))
                {
                    $.ajax({
                        url: url,
                        method: "get",
                        data: {gsearch: txt},
                        dataType: 'json',
                        success: function (data)
                        {
                            $('#list-out').empty();
                            $.each(data['message']['rows'][1], function (key, value) {
                                $('#list-out').append('<option value="' + value.id + '">' + value.name + '</option>');
                            });
                        }
                    });
                } else
                {
                    $('#result').html('');
                }
            });
        }
    }

    $(document).ajaxStart(function () {
        $("#loader").show();
    });
    $(document).ajaxStop(function () {
        $("#loader").hide();
    });
});