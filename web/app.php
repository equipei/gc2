<?php

use Symfony\Component\ClassLoader\ApcClassLoader;
use Symfony\Component\HttpFoundation\Request;

$forcedebug=false;
if(isset($_GET['_forcedebug']) && $_GET['_forcedebug']=1){$forcedebug=true;}

$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
require_once __DIR__.'/../app/AppKernel.php';
$parameterfile = __DIR__.'/../config/custom.yml'; 
$kernel = new AppKernel('prod',$forcedebug);
$kernel->initCutomFile($parameterfile);
if($forcedebug){ini_set("display_errors",1);}
$kernel->loadClassCache();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
