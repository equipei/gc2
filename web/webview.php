<?php
class theme_badiumview_factory_webview {
  private $basepath="";
  function __construct() {
		$this->exec();
	}
	function exec() {
		// global $CFG;
		 $file=$_SERVER['PATH_INFO'];
		
		$basefile="/home/service/badiunetdata/badiunet1/files/";
		$pos=stripos($file, $basefile);
         if($pos!==0){echo "diretory not allowed";exit;}
  
		 if(!file_exists($file)){echo "file not found";exit;}
		 $filetype=pathinfo($file, PATHINFO_EXTENSION);
		 $mtype= $this->getMineType($filetype);
        
		header($mtype);
        header('Accept-Ranges: bytes');
        header('Content-Length: ' . filesize($file));
        header('Content-Disposition: inline;');
        header('Content-Transfer-Encoding: binary');
        readfile($file);
		exit;
    }
	function getMineType($type) {
        $mtype="";
        
        if($type=="eot"){
            $mtype="Content-Type: application/vnd.ms-fontobject";

        }
        else if($type=="otf"){

            $mtype="Content-Type: application/x-font-otf";

        } else if($type=="svg"){


            $mtype="Content-Type: image/svg+xml";
        }
        else if($type=="ttf"){

            $mtype="Content-Type: application/x-font-ttf";

        }
        else if($type=="ttc"){

            $mtype="Content-Type: application/x-font-ttf";

        }
        else if($type=="woff"){

            $mtype="Content-Type: application/font-woff";
        }
        else if($type=="woff2"){

            $mtype="Content-Type: application/font-woff2";

        }
        else if($type=="woff"){

            $mtype="Content-Type: application/font-woff";

        }else if($type=="js"){

            $mtype="Content-Type: application/x-javascript";

        }
        else if($type=="css"){

            $mtype="Content-Type: text/css";

        }

        else if($type == 'png'  || $type == 'jpeg' || $type == 'jpg'){

            $mtype="Content-Type: image/$type";

        }
        else if($type == 'html'){

            $mtype="Content-Type: text/html";

        }
        else if($type == 'mp4'){

            $mtype="Content-Type: video/mp4";

        }
        else if($type == 'pdf'){

            $mtype="Content-Type: application/pdf";

        }
        //add here
        else if($type == 'doc'){

            $mtype="Content-Type: application/msword";

        }
        else if($type == 'dot'){

            $mtype="Content-Type: application/msword";

        }
        else if($type == 'gif'){

            $mtype="Content-Type: image/gif";

        }
        else if($type == 'mp3'){

            $mtype="Content-Type: video/mpeg";

        }
        else if($type == 'ppt'){

            $mtype="Content-Type: application/mspowerpoint";

        }
        else if($type == 'mp4'){

            $mtype="Content-Type: audio/mp4";

        }
        else if($type == 'avi'){

            $mtype="Content-Type: video/avi";

        }
        else if($type == 'book'){

            $mtype="Content-Type: application/book";

        }
        else if($type == 'xls'){

            $mtype="Content-Type: application/excel";
        }
        else if($type == 'xml'){

            $mtype="Content-Type: text/xml";

        }else if($type == 'drw'){

            $mtype="Content-Type: application/drafting";


        }else if($type == 'dv'){

            $mtype="Content-Type: video/x-dv";


        }else if($type == 'dvi'){

            $mtype="Content-Type: application/x-dvi";
        }

        return $mtype;
    }
	
	
}

new theme_badiumview_factory_webview();
